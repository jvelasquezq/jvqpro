using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Collections.Specialized;
using System.Security;
using System.Web;

namespace Nhs.Utility.Web
{
    /// <summary>
    /// Summary description for HTTP.
    /// </summary>
    public static class HTTP
    {

        public static string HttpPost(string weburl, NameValueCollection httpheaders, string post)
        {
            try
            {
                //DateTime lStarted = DateTime.Now;
                HttpWebRequest myHttpReq = (HttpWebRequest)WebRequest.Create(weburl);

                myHttpReq.Headers.Add(httpheaders);
                myHttpReq.Method = "POST";
                myHttpReq.AllowAutoRedirect = true;
                myHttpReq.ContentLength = post.Length;
                myHttpReq.ContentType = "application/x-www-form-urlencoded";
                StreamWriter myWriter = new StreamWriter(myHttpReq.GetRequestStream(), Encoding.ASCII);

                try
                {
                    myWriter.Write(post);
                }
                finally
                {
                    myWriter.Close();
                }

                HttpWebResponse myHttpRes = (HttpWebResponse)myHttpReq.GetResponse();

                StreamReader sr = new StreamReader(myHttpRes.GetResponseStream(), Encoding.ASCII);
                string result = sr.ReadToEnd();
                sr.Close();
                //this.timeSpent = DateTime.Now.Subtract( lStarted );
                return result;
            }
            catch (Exception e)
            {
                throw new Exception("Error accessing url:" + weburl + " :post:" + post, e);
            }

        }

        public static string HttpJsonPost(string weburl, string json)
        {
            try
            {
                using (var client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    byte[] data = Encoding.Default.GetBytes(json);
                    byte[] result = client.UploadData(weburl, "POST", data);
                    return Encoding.Default.GetString(result);
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error accessing url:" + weburl + " :json:" + json, e);
            }

        }
        public static string HttpGet(string weburl)
        {
            try
            {
                //DateTime lStarted = DateTime.Now;
                var myHttpReq = (HttpWebRequest)WebRequest.Create(weburl);
                myHttpReq.KeepAlive = false;
                myHttpReq.Method = "GET";
                myHttpReq.AllowAutoRedirect = true;

                var myHttpRes = (HttpWebResponse)myHttpReq.GetResponse();

                var sr = new StreamReader(myHttpRes.GetResponseStream());
                string result = sr.ReadToEnd();
                sr.Close();
                //this.timeSpent = DateTime.Now.Subtract( lStarted );
                return result;
            }
            catch (Exception e)
            {
                throw new Exception("Error accessing url:" + weburl, e);
            }


        }
        public static Boolean CheckURL(string weburl)
        {
            //DateTime lStarted = DateTime.Now;
            try
            {
                HttpWebRequest myHttpReq = (HttpWebRequest)WebRequest.Create(weburl);
                myHttpReq.KeepAlive = false;
                myHttpReq.Method = "GET";
                myHttpReq.Timeout = 180000;
                myHttpReq.AllowAutoRedirect = true;

                HttpWebResponse myHttpRes = (HttpWebResponse)myHttpReq.GetResponse();
                myHttpRes.Close();
                if (myHttpRes.StatusCode == HttpStatusCode.OK)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        //taken from SearchEngineUtilities
        public static string GetHTML(string sURL)
        {
            HttpWebRequest oRequest;
            HttpWebResponse oResponse;
            StreamReader oReader;
            string sOutput;

            oRequest = (HttpWebRequest)WebRequest.Create(sURL);
            oResponse = (HttpWebResponse)oRequest.GetResponse();
            oReader = new StreamReader(oResponse.GetResponseStream());
            sOutput = oReader.ReadToEnd();

            oReader.Close();

            return sOutput;

        }

        public static uint IpToInt32(this string addr)
        {
            // careful of sign extension: convert to uint first;
            // unsigned NetworkToHostOrder ought to be provided.
            return (uint)IPAddress.NetworkToHostOrder((int)IPAddress.Parse(addr).Address);
        }

        public static string Int32ToIp(this uint address)
        {
            return IPAddress.Parse(address.ToString()).ToString();
            // This also works:
            // return new IPAddress((uint) IPAddress.HostToNetworkOrder(
            //    (int) address)).ToString();
        }
        
        public static string GetClientIpAddress(HttpRequestBase request)
        {
            try
            {
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (!string.IsNullOrEmpty(ipAddress))
                {
                    string[] addresses = ipAddress.Split(',');
                    if (addresses.Length != 0)
                    {
                        return addresses[0];
                    }
                }

                return context.Request.ServerVariables["REMOTE_ADDR"];
            }
            catch
            {
                return "0.0.0.0";
            }
        }

        private static bool IsPrivateIpAddress(string ipAddress)
        {
            // http://en.wikipedia.org/wiki/Private_network
            // Private IP Addresses are: 
            //  24-bit block: 10.0.0.0 through 10.255.255.255
            //  20-bit block: 172.16.0.0 through 172.31.255.255
            //  16-bit block: 192.168.0.0 through 192.168.255.255
            //  Link-local addresses: 169.254.0.0 through 169.254.255.255 (http://en.wikipedia.org/wiki/Link-local_address)

            var ip = IPAddress.Parse(ipAddress);
            var octets = ip.GetAddressBytes();

            var is24BitBlock = octets[0] == 10;
            if (is24BitBlock) return true; // Return to prevent further processing

            var is20BitBlock = octets[0] == 172 && octets[1] >= 16 && octets[1] <= 31;
            if (is20BitBlock) return true; // Return to prevent further processing

            var is16BitBlock = octets[0] == 192 && octets[1] == 168;
            if (is16BitBlock) return true; // Return to prevent further processing

            var isLinkLocalAddress = octets[0] == 169 && octets[1] == 254;
            return isLinkLocalAddress;
        }
    }
}