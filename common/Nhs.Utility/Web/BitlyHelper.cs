﻿using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using Nhs.Utility.Common;

namespace Nhs.Utility.Web
{
    /// <summary>
    /// Helper to use with Bitly URL shortening service. 
    /// </summary>
    public static class BitlyHelper
    {
        /// <summary>
        /// Gets Bitly info given input and Bitly method
        /// </summary>
        /// <param name="username">Bitly username.</param>
        /// <param name="apiKey">Bitly API key.</param>
        /// <param name="input">The input (short url, long url etc.)</param>
        /// <param name="method">Refer to BitlyMethod enum.</param>
        /// <returns></returns>
        public static string GetData(string username, string apiKey, string input, BitlyMethod method)
        {
            string bitlyNativeMethod;
            string xmlNodeToQuery;
            string inputParam;
            switch (method)
            {
                case BitlyMethod.Shorten:
                    bitlyNativeMethod = "shorten";
                    xmlNodeToQuery = "shortUrl";
                    inputParam = "longUrl";
                    break;

                case BitlyMethod.GetMetaFromHash:
                    bitlyNativeMethod = "info";
                    xmlNodeToQuery = "htmlMetaDescription";
                    inputParam = "hash";
                    break;

                case BitlyMethod.GetMetaFromShortUrl:
                    bitlyNativeMethod = "info";
                    xmlNodeToQuery = "htmlMetaDescription";
                    inputParam = "shortUrl";
                    break;

                case BitlyMethod.GetExpandedUrlFromHash:
                    bitlyNativeMethod = "expand";
                    xmlNodeToQuery = "longUrl";
                    inputParam = "hash";
                    break;

                case BitlyMethod.GetExpandedUrlFromShortUrl:
                    bitlyNativeMethod = "expand";
                    xmlNodeToQuery = "longUrl";
                    inputParam = "shortUrl";
                    break;

                case BitlyMethod.GetClicksFromShortUrl:
                    bitlyNativeMethod = "stats";
                    xmlNodeToQuery = "clicks";
                    inputParam = "shortUrl";
                    break;

                case BitlyMethod.GetClicksFromHash:
                    bitlyNativeMethod = "stats";
                    xmlNodeToQuery = "clicks";
                    inputParam = "hash";
                    break;

                case BitlyMethod.GetUserFromShortUrl:
                    bitlyNativeMethod = "info";
                    xmlNodeToQuery = "shortenedByUser";
                    inputParam = "shortUrl";
                    break;

                case BitlyMethod.GetUserFromHash:
                    bitlyNativeMethod = "info";
                    xmlNodeToQuery = "shortenedByUser";
                    inputParam = "hash";
                    break;

                default:
                    return string.Empty;
            }

            var bitlyQueryUrl = new StringBuilder();
            bitlyQueryUrl.Append("http://api.bit.ly/");
            bitlyQueryUrl.Append(bitlyNativeMethod);
            bitlyQueryUrl.Append("?version=2.0.1");
            bitlyQueryUrl.Append("&format=xml");
            bitlyQueryUrl.Append("&");
            bitlyQueryUrl.Append(inputParam);
            bitlyQueryUrl.Append("=");
            bitlyQueryUrl.Append(input);
            bitlyQueryUrl.Append("&login=");
            bitlyQueryUrl.Append(username);
            bitlyQueryUrl.Append("&apiKey=");
            bitlyQueryUrl.Append(apiKey);

            try
            {
                var request = WebRequest.Create(bitlyQueryUrl.ToString());
                var rs = new StreamReader(request.GetResponse().GetResponseStream());
                var bitlyRs = rs.ReadToEnd();
                rs.Close();

                var data = ParseResponse(bitlyRs, xmlNodeToQuery); 
                return data == "Error" ? string.Empty : data;
            }
            catch
            {
                return string.Empty;
            }
        }

        private static string ParseResponse(string data, string type)
        {
            var reader = new XmlTextReader(new StringReader(data));
            while (reader.Read())
            {
                var nodeType = reader.NodeType.ToString();

                if (nodeType == "Element" && reader.Name == type)
                {
                    reader.Read();
                    return reader.Value;
                }
            }

            return string.Empty;
        }
    }
}
