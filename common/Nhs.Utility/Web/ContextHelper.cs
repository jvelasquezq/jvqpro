using System;
using System.Web;

namespace Nhs.Utility.Web
{
    /// <summary>
    /// A selection of utilities to simplify interaction with an HTTP session.
    /// </summary>
    public static class ContextHelper
    {
        public static void AddItemToContext(HttpContext httpContext, object keyToAdd, object valueToAdd)
        {
            if (httpContext != null && !httpContext.Items.Contains(keyToAdd))
                httpContext.Items.Add(keyToAdd, valueToAdd);
        }

        public static void AddToContext(object keyToAdd, object valueToAdd)
        {
            if (HttpContext.Current != null && !HttpContext.Current.Items.Contains(keyToAdd))
                HttpContext.Current.Items.Add(keyToAdd, valueToAdd);
        }

        public static object GetItemFromContext(HttpContext httpContext, string key)
        {
            object result;

            if (httpContext != null)
                result = httpContext.Items[key];
            else
                throw new ArgumentNullException("httpContext");

            return result;
        }

        public static object GetFromCurrentContext(string key)
        {
            object result;

            if (HttpContext.Current != null)
                result = HttpContext.Current.Items[key];
            else
                throw new InvalidOperationException("HttpContext.Current is null");

            return result;
        }

        public static Uri GetReferrer()
        {
            return HttpContext.Current.Request.UrlReferrer;
        }

        public static string GetCurrentHostName()
        {
            return HttpContext.Current != null ? HttpContext.Current.Request.Url.Host : string.Empty;
        }
    }
}
