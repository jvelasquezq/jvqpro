using System.Globalization;
using System.Text.RegularExpressions;

namespace Nhs.Utility.Web
{
    /// <summary>
    /// A selection of utilities to simplify interaction with an HTTP session.
    /// </summary>
    public static class UrlHelper
    {
        /// <summary>
        /// Regular expression for Youtube Videos
        /// 06/07: Adding support for Videos with 'watch?feature=player_detailpage&v=#####' structure 
        /// </summary>
        public static Regex YoutubeRegex =
            new Regex(@"^(?:https?\:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v\=|watch\?.+&v=))([\w-]{10,12})(?:[\&\?\#].*?)*?(?:[\&\?\#]t=([\dhm]+s))?$", RegexOptions.IgnoreCase);

        /// <summary>
        /// Regular expression for Vimeo URLs.
        /// </summary>
        public static Regex VimeoRegex = new Regex(@"vimeo\.com/(?:.*#|.*/videos/|channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)?([0-9]+)", RegexOptions.IgnoreCase);


        /// <summary>
        /// Regular expression for Vimeo Channel URLs.
        /// </summary>
        public static Regex VimeoChannelRegex = new Regex(@"vimeo\.com/(?:channels/)(\w)+($|/)(?!.+)", RegexOptions.IgnoreCase);


        public static bool IsYoutubeVideo(string url)
        {
            if (string.IsNullOrEmpty(url)) return false;

            Match videoMatch = YoutubeRegex.Match(url);
            return videoMatch.Success;
        }

        public static bool IsVimeoVideo(string url)
        {
            if (string.IsNullOrEmpty(url)) return false;

            Match videoMatch = VimeoRegex.Match(url);
            return videoMatch.Success;
        }

        public static bool IsVimeoChannel(string url)
        {
            if (string.IsNullOrEmpty(url)) return false;

            Match videoMatch = VimeoChannelRegex.Match(url);
            return videoMatch.Success;
        }

        public static string GetYoutubeVideoId(string url)
        {
            string videoId = null;
            Match videoMatch = YoutubeRegex.Match(url);

            if (videoMatch.Success)
                videoId = videoMatch.Groups[1].Value;
            else
            {
                videoId = (url.IndexOf("v=") != -1) ? url.Substring(url.IndexOf("v=") + 2) : url.Substring(url.LastIndexOf("/") + 1);
                if (videoId.IndexOf("&") != -1) // remove any extra parameter
                    videoId = videoId.Substring(0, videoId.IndexOf("&"));

                if (videoId.IndexOf("?") != -1) // remove any extra parameter
                    videoId = videoId.Substring(0, videoId.IndexOf("?"));
            }

            return videoId;
        }

        public static string GetVimeoVideoId(string url)
        {
            string videoId = null;
            Match videoMatch = VimeoRegex.Match(url);

            if (videoMatch.Success)
                videoId = videoMatch.Groups[3].Value;

            return videoId;
        }

        public static string GetVimeoChannelId(string url)
        {
            string channellId = null;
            Match videoMatch = VimeoChannelRegex.Match(url);

            if (videoMatch.Success)
                channellId = videoMatch.Groups[0].Value.Substring(videoMatch.Groups[0].Value.LastIndexOf('/') + 1);

            return channellId;
        }
    }
}
