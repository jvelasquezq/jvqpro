using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using EO.Pdf;
using Nhs.Utility.Web;

namespace Nhs.Utility.Pdf
{
    /// <summary>
    /// Summary description for PdfGenerator.
    /// </summary>
    public class PdfGenerator
    {
        static PdfGenerator()
        {
            Runtime.AddLicense("RhnyntzCnrWfWZekzQzrpeb7z7iJWZekscufWZfA8g/jWev9ARC8W7zTv/vj" +
                                      "n5mkBxDxrODz/+ihbaW0s8uud4SOscufWbOz8hfrqO7CnrWfWZekzRrxndz2" +
                                      "2hnlqJfo8h/kdpm2wNyyaKm0wt6hWe3pAx7oqOXBs9+hWabCnrWfWZekzR7o" +
                                      "oOXlBSDxnrXFBQ+qgsm24xvCkszL/CXopbDr4e72drTAwB7ooOXlBSDxnrWR" +
                                      "m+eupeDn9hnynrWRm3Xj7fQQ7azcwp61n1mXpM0X6Jzc8gQQyJ21ucHbuGyq" +
                                      "usrcsnWm8PoO5Kfq6doPvUaBpLHLn3Xj7fQQ7azc6c/nrqXg5/Y=");

        }

        public static string GetPdf(string url, string path, string fileName, ref StringBuilder log)
        {
            if (fileName == null || fileName.Equals(""))
                fileName = Guid.NewGuid() + ".pdf";
            else
            {
                if (!fileName.EndsWith(".pdf"))
                    fileName += ".pdf";
            }

            for (var i = 0; i < 3; i++)
            {
                var htmlToPdfResult = GetHtmlAndCreatePdf(url, Path.Combine(path, fileName), ref log);

                if (htmlToPdfResult)
                    break;
            }

            return fileName;
        }

        private static bool GetHtmlAndCreatePdf(string url, string fileName, ref StringBuilder log)
        {
            
            try
            {
                var doc = GetHtmlToPdf(url, ref log);
                if (doc != null)
                {
                    doc.Save(fileName);
                    log.AppendLine(DateTime.Now + ": Save PDF");
                    return true;
                }
                log.AppendLine(DateTime.Now + ": Did not Save PDF, HtmlToPdfResult was null");
                return false;
            }
            catch (Exception ex)
            {
                log.AppendLine(DateTime.Now + ": Error");
                log.AppendLine(ex.Message);
                return false;
            }
        }

        private static PdfDocument GetHtmlToPdf(string url, ref StringBuilder log)
        {
            HtmlToPdf.Options.SaveImageAsJpeg = true;
            HtmlToPdf.Options.NoCache = true;
            HtmlToPdf.Options.NoLink = false;
            HtmlToPdf.Options.MaxLoadWaitTime = 99999999;

            try
            {
                var doc = new PdfDocument();

                log.AppendLine(DateTime.Now + ": Request PDF. Url: " + url);
                var htmlToPdfResult = HtmlToPdf.ConvertUrl(url, doc);

                if (htmlToPdfResult.LastPageIndex > 0)
                {
                    return doc;
                }
                return null;
            }
            catch (Exception ex)
            {
                log.AppendLine(DateTime.Now + ": Error");
                log.AppendLine(ex.Message);
                return null;
            }
        }

        public static MemoryStream GetPdfForDownload(string url, ref StringBuilder log)
        {
            try
            {
                var ms = new MemoryStream();
                var doc = GetHtmlToPdf(url, ref log);

                if (doc != null)
                {
                    doc.Save(ms);
                    ms.Position = 0;
                    log.AppendLine(DateTime.Now + ": Save PDF to MemoryStream");
                    return ms;
                }
                log.AppendLine(DateTime.Now + ": Did not Save PDF, HtmlToPdfResult was null");
                return null;
            }
            catch (Exception ex)
            {
                log.AppendLine(DateTime.Now + ": Error");
                log.AppendLine(ex.Message);
                return null;
            }
        }

        public static bool MergePdfs(string outputPath, IEnumerable<string> inputPaths, ref StringBuilder log)
        {
            log.AppendLine(DateTime.Now + ": Getting Custom PDF");

            try
            {
                var mergedDoc = new PdfDocument();

                foreach (var path in inputPaths)
                {
                    if (path.StartsWith("http://") || path.StartsWith("https://"))
                    {
                        var webClient = new WebClient();
                        var tmpOutputPath = outputPath.TrimEnd(".pdf".ToCharArray()) + ".tmp";
                        log.AppendLine(DateTime.Now + ": Getting PDF Url: " + path);
                        log.AppendLine(DateTime.Now + ": Start DownloadFile");
                        webClient.DownloadFile(path, tmpOutputPath);
                        log.AppendLine(DateTime.Now + ": End DownloadFile");

                        var docDownLoad = new PdfDocument(tmpOutputPath);
                        mergedDoc = PdfDocument.Merge(mergedDoc, docDownLoad);
                        try
                        {
                            File.Delete(tmpOutputPath);
                        }
                        catch
                        {
                            //keep going...we can delete tmp files later.
                        }
                    }
                    else if (File.Exists(path))
                    {
                        log.AppendLine(DateTime.Now + ": Getting PDF: " + path);
                        var doc = new PdfDocument(path);
                        mergedDoc = PdfDocument.Merge(mergedDoc, doc);
                    }
                }

                if (mergedDoc.Pages.Count > 0)
                {
                    mergedDoc.Save(outputPath);
                    log.AppendLine(DateTime.Now + ": Save PDF");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                var message =
                    "Could not merge pdfs. Most likely the source pdfs were not found or the target folder does not exist.";
                log.AppendLine(DateTime.Now + ": Error :" + message);
                log.AppendLine(ex.Message);
                return true;
            }
        }

        public static bool MergePdfs(ref MemoryStream ms, IEnumerable<string> inputPaths, ref StringBuilder log)
        {
            log.AppendLine(DateTime.Now + ": Getting Custom PDF");

            try
            {
                var mergedDoc = new PdfDocument();

                foreach (var path in inputPaths)
                {
                    if (path.StartsWith("http://") || path.StartsWith("https://"))
                    {
                        var webClient = new WebClient();
                        log.AppendLine(DateTime.Now + ": Getting PDF Url: " + path);
                        log.AppendLine(DateTime.Now + ": Start DownloadFile");
                        using (var stream = new MemoryStream(webClient.DownloadData(path)))
                        {
                            log.AppendLine(DateTime.Now + ": End DownloadFile");
                            var docDownLoad = new PdfDocument(stream);
                            mergedDoc = PdfDocument.Merge(mergedDoc, docDownLoad);
                        }
                    }
                    else if (File.Exists(path))
                    {
                        log.AppendLine(DateTime.Now + ": Getting PDF: " + path);
                        var doc = new PdfDocument(path);
                        mergedDoc = PdfDocument.Merge(mergedDoc, doc);
                    }
                }

                if (mergedDoc.Pages.Count > 0)
                {
                    var temp = new MemoryStream();
                    mergedDoc.Save(temp);
                    ms = new MemoryStream(temp.ToArray());
                    log.AppendLine(DateTime.Now + ": Save PDF");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                var message =
                    "Could not merge pdfs. Most likely the source pdfs were not found or the target folder does not exist.";
                log.AppendLine(DateTime.Now + ": Error :" + message);
                log.AppendLine(ex.Message);
                return true;
            }
        }
    }
}
