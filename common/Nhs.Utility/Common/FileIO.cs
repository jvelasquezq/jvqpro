using System;
using System.IO;

namespace Nhs.Utility.Common
{
    /// <summary>
    /// Summary description for FileIO.
    /// </summary>
    public class FileIO
    {
        public static string Log(string dirpath, string content)
        {
            string filename = DateTime.Now.ToShortDateString().Replace("/", "_") + "_" + DateTime.Now.ToShortTimeString().Replace(":", "_") + "_" + Guid.NewGuid().ToString() + ".txt";
            if (!dirpath.EndsWith("\\"))
                dirpath = dirpath + "\\";
            AppendtoFile(dirpath + filename, content);
            return filename;
        }
        public static void LogtoFile(string filepath, string filecontent)
        {
            AppendtoFile(filepath, DateTime.Now.ToString() + ": " + filecontent);
        }

        public static void AppendtoFile(string filepath, string filecontent)
        {
            StreamWriter sw = new StreamWriter(filepath, true);// System.Text.Encoding.ASCII);
            sw.WriteLine(filecontent);
            sw.Close();
        }

        public static void WritetoFile(string filepath, string filecontent)
        {
            //StreamWriter sw = File.CreateText(filepath); 
            StreamWriter sw = new StreamWriter(filepath, false);//System.Text.Encoding.ASCII);
            sw.Write(filecontent);
            sw.Close();
        }

        public static string ReadfromFile(string filepath)
        {
            String filecontent = "";
            if (File.Exists(filepath))
            {
                StreamReader sr = new StreamReader(File.OpenRead(filepath));//System.Text.Encoding.ASCII);
                sr.BaseStream.Seek(0, SeekOrigin.Begin);
                filecontent = sr.ReadToEnd();
                sr.Close();
            }
            return filecontent;
        }
    }
}