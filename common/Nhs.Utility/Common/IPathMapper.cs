﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Utility.Common
{
    public interface IPathMapper
    {
        string MapPath(string relativePath);
        void AddItemToContext(string itemKey, object itemValue);
        object GetItemFromContext(string itemKey);
    }
}
