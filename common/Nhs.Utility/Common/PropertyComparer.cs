using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace Nhs.Utility.Common
{
    public class PropertyComparer<T> : IComparer<T>
    {
        #region "Members"
        private string _propertyName = string.Empty;
        private SortDirection _sortDirection = SortDirection.Ascending;
        #endregion

        #region "Constructor"
        public PropertyComparer(string propertyName)
        {
            _propertyName = propertyName;
        }

        public PropertyComparer(string propertyName, SortDirection sortDirection)
        {
            _propertyName = propertyName;
            _sortDirection = sortDirection;
        }
        #endregion

        #region "Public Methods"
        public int Compare(T x, T y)
        {
            object xValue = GetPropertyValue(x, _propertyName);
            object yValue = GetPropertyValue(y, _propertyName);

            if (xValue == null)
            {
                if (yValue == null)
                {
                    // x and y values are null, therefore equal sort order. 
                    return 0;
                }
                else
                {
                    // x is null, but y has a value, therefore x < y,  
                    return -1 * (int)_sortDirection;
                }

            }
            else
            {
                if (yValue == null)
                {
                    // x has value, y is null.  -> x > y;
                    return 1 * (int)_sortDirection;
                }
                else
                {
                    // Both x and y have values. Do comparison. 

                    // Check for value types to avoid additional reflection call to get CompareTo method. 
                    if (xValue.GetType() == typeof(Int32))
                    {
                        return (Convert.ToInt32(xValue)).CompareTo(Convert.ToInt32(yValue)) * (int)_sortDirection;
                    }

                    if (xValue.GetType() == typeof(Double))
                    {
                        return (Convert.ToDouble(xValue)).CompareTo(Convert.ToDouble(yValue)) * (int)_sortDirection;
                    }

                    if (xValue.GetType() == typeof(DateTime))
                    {
                        return (Convert.ToDateTime(xValue)).CompareTo(Convert.ToDateTime(yValue)) * (int)_sortDirection;
                    }

                    if (xValue.GetType() == typeof(string))
                    {
                        return (xValue.ToString()).CompareTo(yValue.ToString()) * (int)_sortDirection;
                    }

                    // Not value type.  Invoke the "CompareTo" method on the object. 
                    MethodInfo compareToMethod = xValue.GetType().GetMethod("CompareTo");
                    return (int)(compareToMethod.Invoke(xValue, new[] { yValue }));
                }
            }
        }
        #endregion

        #region "Private Methods"
        private bool IsComparable(Type type)
        {
            return type.GetInterface("IComparable") != null;
        }

        private object GetPropertyValue(object obj, string propertyName)
        {
            if (propertyName.Contains("."))
            {
                string firstPropertyName = propertyName.Substring(0, propertyName.IndexOf('.'));
                propertyName = propertyName.Remove(0, firstPropertyName.Length + 1);

                PropertyInfo propertyInfo = obj.GetType().GetProperty(firstPropertyName);

                if (propertyInfo != null)
                {
                    object propertyValue = propertyInfo.GetValue(obj, null);

                    // If property value is null, just stop here. All sub-properties would be null also. 
                    if (propertyValue == null)
                    {
                        return null;
                    }
                    else
                    {
                        // Make recursive call to GetProperty. 
                        return GetPropertyValue(propertyValue, propertyName);
                    }
                }
                else
                {
                    // Property doesn't exist, throw execption message. 
                    string objectType = obj.GetType().ToString();
                    throw new ApplicationException(objectType + " does not contain a property named '" + firstPropertyName + "'");
                }
            }
            else
            {
                PropertyInfo propertyInfo = obj.GetType().GetProperty(propertyName);

                if (propertyInfo != null)
                {
                    object propertyValue = propertyInfo.GetValue(obj, null);
                    return propertyValue;
                }
                else
                {
                    string objectType = obj.GetType().ToString();
                    throw new ApplicationException(objectType + " does not contain a property named '" + propertyName + "'");
                }

            }
        }
        #endregion
    }

    public enum SortDirection
    {
        Ascending = 1,
        Descending = -1
    }
}