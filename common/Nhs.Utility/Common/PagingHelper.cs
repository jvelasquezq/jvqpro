using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Nhs.Utility.Common
{
    public class PagingHelper
    {
        private int _dataStartRecord;
        private int _dataEndRecord;
        private int _pageSize;
        private int _pageNumber;
        private int _totalRecords;
        private IList _data;


        public int TotalPages
        {
            get
            {
                return (int)Math.Ceiling(_totalRecords / (double)_pageSize);
            }
        }


        public PagingHelper(IList data, int dataStartRecord, int dataEndRecord, 
                            int pageSize, int totalRecords)
        {
            _data = data;
            _dataStartRecord = dataStartRecord;
            _dataEndRecord = dataEndRecord;
            _pageSize = pageSize > 0 ? pageSize : 1;
            _totalRecords = totalRecords;
        }

        public bool PageExists(int page)
        {
            int pageStart = (page - 1) * _pageSize;
            int pageEnd = (page * _pageSize) - 1;
            pageEnd = pageEnd > _totalRecords ? _totalRecords : pageEnd;

            if (_dataStartRecord <= pageStart 
                && _dataEndRecord >= pageEnd 
                && pageEnd >= pageStart)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public IList GetPage(int pageNumber)
        {
            _pageNumber = pageNumber > 0 ? pageNumber : 1;
            _pageNumber = pageNumber <= TotalPages ? pageNumber : TotalPages;

            int pageStart = (_pageNumber - 1) * _pageSize;
            int pageEnd = (_pageNumber * _pageSize) - 1;

            // If there is no data, return null.
            if (_data.Count == 0)
            {
                return null;
            }

            // Check to see if the starting index is within the data. 
            if (pageStart < _dataStartRecord || pageStart > _dataEndRecord)
            {
                // data does not contain starting record. 
                return null;
            }

            // Data may not contain a full page of records. 
            if (pageEnd > _dataEndRecord)
            {
                pageEnd = _dataEndRecord;
            }


            // Now we know the logical page start & end records.
            // Need to translate that to the actual records in data list. 
            int startIndex = pageStart - _dataStartRecord;
            int endIndex = startIndex + (pageEnd - pageStart);

            ArrayList pageResults = new ArrayList();

            for (int i = startIndex; i <= endIndex; i++)
            {
                pageResults.Add(_data[i]);
            }

            return pageResults;
        }

        public static int GetPageStartRecord(int page, int pageSize)
        {
            return (page - 1) * pageSize;
        }

        public static int GetPageEndRecord(int page, int pageSize)
        {
            return (page * pageSize) - 1; 
        }

        public static int GetTotalPages(int totalRecords, int pageSize)
        {
            return (int)Math.Ceiling(totalRecords / (double)pageSize);
        }
    }
}