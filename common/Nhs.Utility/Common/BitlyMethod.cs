﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Utility.Common
{
    public enum BitlyMethod
    {
        Shorten,
        GetMetaFromHash,
        GetMetaFromShortUrl,
        GetExpandedUrlFromHash,
        GetExpandedUrlFromShortUrl,
        GetClicksFromHash,
        GetClicksFromShortUrl,
        GetUserFromShortUrl,
        GetUserFromHash
    }
}
