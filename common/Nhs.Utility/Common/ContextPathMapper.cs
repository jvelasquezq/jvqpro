﻿using System;
using System.Web;
using Nhs.Utility.Web;

namespace Nhs.Utility.Common
{
    public class ContextPathMapper : IPathMapper
    {
        public string MapPath(string relativePath)
        {
            return HttpContext.Current.Server.MapPath(relativePath);
        }

        public void AddItemToContext(string itemKey, object itemValue)
        {
            ContextHelper.AddItemToContext(HttpContext.Current, itemKey, itemValue);
        }

        public object GetItemFromContext(string itemKey)
        {
            return ContextHelper.GetItemFromContext(HttpContext.Current, itemKey);
        }
    }
}