﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Security.Cryptography;

namespace Nhs.Utility.Common
{
    public static class CryptoHelper
    {
        static readonly byte[] U8Salt = new byte[] { 0x26, 0x19, 0x81, 0x4E, 0xA0, 0x6D, 0x95, 0x34, 0x26, 0x75, 0x64, 0x05, 0xF6 };

        /// <summary>
        /// Returns the hashed password for the input string
        /// </summary>
        public static string HashUsingAlgo(string stringToHash, string algorithm)
        {
            switch (algorithm.ToLower())
            {
                case "md5":
                    return ComputeHash(stringToHash, new MD5CryptoServiceProvider());

                case "sha1":
                    return ComputeHash(stringToHash, new SHA1CryptoServiceProvider());

                case "sha256":
                    return ComputeHash(stringToHash, new SHA256CryptoServiceProvider());

                case "sha384":
                    return ComputeHash(stringToHash, new SHA384CryptoServiceProvider());

                case "sha512":
                    return ComputeHash(stringToHash, new SHA512CryptoServiceProvider());
            }

            return string.Empty;
        }

        public static string ComputeHash(string input, HashAlgorithm algorithm)
        {
            var inputBytes = Encoding.UTF8.GetBytes(input);
            var hashedBytes = algorithm.ComputeHash(inputBytes);
            return BitConverter.ToString(hashedBytes).ToLower().Replace("-", string.Empty);
        }


        public static string Decrypt(string hexValue)
        {
            var decAgain = int.Parse(hexValue, NumberStyles.HexNumber);
            return decAgain.ToString(CultureInfo.InvariantCulture);
        }

        public static string Encrypt(string decValue)
        {
            var hexValue =Convert.ToInt32(decValue).ToString("X");
            return hexValue;
        }
        
       //***************************************************************

        

        public static string Encrypt(string plainText, string password)
        {
            using (var iAlg = GetRijndaelManaged(password))
            {
                using (var memoryStream = new MemoryStream())
                using (var cryptoStream = new CryptoStream(memoryStream, iAlg.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    var data = Encoding.UTF8.GetBytes(plainText);
                    cryptoStream.Write(data, 0, data.Length);
                    cryptoStream.FlushFinalBlock();

                    return Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
                }
            }
        }

        public static RijndaelManaged GetRijndaelManaged(string password)
        {
            var pdb = new Rfc2898DeriveBytes(password, U8Salt);
            return new RijndaelManaged
                {
                    Key = pdb.GetBytes(32),
                    IV = pdb.GetBytes(16),
                    Mode = CipherMode.CBC,
                    Padding = PaddingMode.Zeros
                };
        }

        public static string Decrypt(string cipherText, string password)
        {
            try
            {               
                using (var iAlg = GetRijndaelManaged(password))
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        using (var cryptoStream = new CryptoStream(memoryStream, iAlg.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            var data = Convert.FromBase64String(cipherText);
                            cryptoStream.Write(data, 0, data.Length);
                            cryptoStream.Flush();

                            return Encoding.UTF8.GetString(memoryStream.ToArray()).Trim('\0');
                        }
                    }
                }
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
