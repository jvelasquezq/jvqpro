﻿using System;

namespace Nhs.Utility.Common
{
    public static class WebApiSecurity
    {
        /// <summary>
        /// Authenticates user using Custom SSO
        /// </summary>
        /// <param name="sessionToken">The session token.</param>
        /// <param name="algorithm">The hashing algorithm.</param>
        /// <param name="partnerSitePassword">The partner site password (Site terms).</param>
        /// <param name="ssoId">The unique sso id provided by the PL.</param>
        /// <returns></returns>
        public static bool Authenticate(string sessionToken, string algorithm, string partnerSitePassword, string ssoId)
        {
            //Use date time in the future for additional validation
            var computedSessionToken = CreateToken(DateTime.UtcNow, algorithm.ToLowerInvariant(), partnerSitePassword, ssoId);
            return sessionToken == computedSessionToken;
        }


        /// <summary>
        /// Authenticates user using Web Api
        /// </summary>
        /// <param name="sessionToken">The session token.</param>
        /// <param name="algorithm">The hashing algorithm.</param>
        /// <param name="partnerSitePassword">The partner site password (Site terms).</param>
        /// <param name="ssoId">The unique sso id provided by the PL.</param>
        /// <returns></returns>
        public static bool AuthenticateWebApi(string sessionToken, string algorithm, string partnerSitePassword, string ssoId = "")
        {
            //Use date time in the future for additional validation
            var computedSessionToken = CreateToken(DateTime.UtcNow, algorithm.ToLowerInvariant(), partnerSitePassword, ssoId);
            var computedSessionToken2 = CreateToken(DateTime.UtcNow.AddHours(-1), algorithm.ToLowerInvariant(), partnerSitePassword, ssoId);

            return sessionToken == computedSessionToken || sessionToken == computedSessionToken2;
        }

        /// <summary>
        /// Create Token
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <param name="algorithm">The hashing algorithm.</param>
        /// <param name="partnerSitePassword">The partner site password (Site terms).</param>
        /// <param name="ssoId">The unique sso id provided by the PL.</param>
        /// <returns></returns>
        public static string CreateToken(DateTime dateTime, string algorithm, string partnerSitePassword, string ssoId = "")
        {
            var valueToHash = partnerSitePassword + ":" + dateTime.ToString("yyyyMMddHH") +
                              (string.IsNullOrEmpty(ssoId) ? "" : ":" + ssoId);
            //Use date time in the future for additional validation
            var computedSessionToken = CryptoHelper.HashUsingAlgo(valueToHash, algorithm.ToLowerInvariant());
            return computedSessionToken;
        }
    }
}
