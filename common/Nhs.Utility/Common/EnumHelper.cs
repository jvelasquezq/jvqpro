﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Nhs.Utility.Common
{
    public static class EnumHelper
    {
        public static T FromInt<T>(this int value) where T : new()
        {
            return (T)Enum.Parse(typeof(T), value.ToType<string>());
        }

        public static T FromString<T>(this string value) where T : struct
        {
            return value.GetEnumValue<T>();
        }

        public static bool EnumExist<T>(this string value) where T : new()
        {
            return Enum.GetNames(typeof (T)).Any(p => p.ToLower() == value.ToLower());
        }

        public static T GetEnumValue<T>(this string value) where T : struct
        {
            T result;
            Enum.TryParse(value, true, out result);
            return result;
        }

        public static string Stringify(this Enum value)
        {
            var description = value.ToString();
            var customAttributes = value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];
            if (customAttributes != null && customAttributes.Length > 0)
                description = customAttributes[0].Description;
            return description;
        }

        public static MvcHtmlString RadioButtonForListEnum<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
                                                                        Expression<Func<TModel, TProperty>> expression)
        {
            var metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            return htmlHelper.RadioButtonForListEnum(expression, metaData.ModelType);
        }

        public static MvcHtmlString RadioButtonForListEnum<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
                                                                         Expression<Func<TModel, TProperty>> expression, Enum enumList)
        {
            return htmlHelper.RadioButtonForListEnum(expression, enumList.GetType());
        }


        private static MvcHtmlString RadioButtonForListEnum<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
                                                                         Expression<Func<TModel, TProperty>> expression, Type enumType)
        {

            //Enum value = (Enum)Enum.ToObject(metaData.ModelType, 0);
            var metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var sb = new StringBuilder();
            sb.Append("<ul>");
            foreach (Enum enumItem in Enum.GetValues(enumType))
            {
                var name = enumItem.Stringify();
                var id = string.Format(
                "{0}_{1}_{2}",
                htmlHelper.ViewData.TemplateInfo.HtmlFieldPrefix,
                metaData.PropertyName,
                name
                );

                var radio = htmlHelper.RadioButtonFor(expression, name, new { id }).ToHtmlString();
                sb.AppendFormat(
                    "<li>{2}<label for=\"{0}\"><span>{1}</span></label></li>",
                    id,
                    HttpUtility.HtmlEncode(name),
                    radio
                    );
            }

            sb.Append("</ul>");
            return MvcHtmlString.Create(sb.ToString());
        }


        public static MvcHtmlString DropDownForListEnum<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
                                                                       Expression<Func<TModel, TProperty>> expression)
        {
            var metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            return DropDownForListEnum(metaData.ModelType, metaData.Model.ToString());
        }

        private static MvcHtmlString DropDownForListEnum(Type enumType, string value)
        {

            var sb = new StringBuilder();
            sb.AppendLine("<select id=\"nhs_" + enumType.Name + "\" class='nhs_SortOption'>");
            foreach (Enum enumItem in Enum.GetValues(enumType))
            {
                string name = enumItem.Stringify();
                sb.AppendLine(string.Format("<option {1} value=\"{0}\" >{2}</option>", enumItem.ToString().ToLower(),
                                            value == name ? "selected=\"selected\"" : string.Empty, name));
            }

            sb.AppendLine("</select>");
            return MvcHtmlString.Create(sb.ToString());
        }
    }
}
