using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Collections;
using System.Data;
using System.Text.RegularExpressions;

namespace Nhs.Utility.Common
{
    public static class CollectionHelper
    {
        public static IEnumerable<T> ToRandom<T>(this IEnumerable<T> source)
        {
            return source.OrderBy(x => Guid.NewGuid());
        }

        public static IEnumerable<IEnumerable<TSource>> GroupBy<TSource>(this IEnumerable<TSource> source,
                                                                         int groupingNumber)
        {
            var data = source.Select((x, i) => new { Index = i, Value = x })
                         .GroupBy(x => x.Index / groupingNumber)
                         .Select(x => x.Select(v => v.Value));
            return data;
        }

        public static void Shuffle<T>(this IList<T> list)
        {
            Random rng = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        /// <summary>
        /// var query = people.DistinctBy(p => p.Id);
        /// var query = people.DistinctBy(p => new { p.Id, p.Name });
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="source"></param>
        /// <param name="keySelector"></param>
        /// <returns></returns>
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            var seenKeys = new HashSet<TKey>();
            return source.Where(element => seenKeys.Add(keySelector(element)));
        }

        public static List<List<TSource>> ToColumns<TSource>(this IEnumerable<TSource> source, int columnCount)
        {
            var finalResults = new List<List<TSource>>();

            int remainder;
            var itemsPerColumn = Math.DivRem(source.Count(), columnCount, out remainder);
            if (remainder != 0)
                itemsPerColumn += 1;

            for (var i = 0; i < columnCount; i++)
            {
                var data = source.Skip(i * itemsPerColumn).Take(itemsPerColumn).ToList();
                if (data.Any())
                    finalResults.Add(data);
            }

            return finalResults;
        }

        public static List<List<TSource>> ToRows<TSource>(this IEnumerable<TSource> source, int rowsItemCount)
        {
            var finalResults = new List<List<TSource>>();

            int remainder;
            var rowNumber = Math.DivRem(source.Count(), rowsItemCount, out remainder);
            if (remainder != 0)
                rowNumber += 1;

            for (var i = 0; i < rowNumber; i++)
            {
                var data = source.Skip(i * rowsItemCount).Take(rowsItemCount).ToList();
                if (data.Any())
                    finalResults.Add(data);
            }

            return finalResults;
        }

        public static List<List<TSource>> ToExactColumns<TSource>(this IQueryable<TSource> source, uint rowsItemCount)
        {
            var finalResults = new List<List<TSource>>();
            var itemCol = new List<TSource>();
            uint count = 1;
            var globalCount = 1;
            var sourceCount = source.Count();
            foreach (var col in source)
            {                
                if (count < rowsItemCount)
                {
                    itemCol.Add(col);
                    count++;
                }
                else if (count == rowsItemCount)
                {
                    itemCol.Add(col);
                    finalResults.Add(itemCol);
                    count = 1;
                    itemCol = new List<TSource>();                 
                }

                if (globalCount == sourceCount && itemCol.Any())
                {                    
                    finalResults.Add(itemCol);
                    break;
                }

                globalCount++;
            }

            return finalResults;
        }

        public static Hashtable CreateHashTable(DataTable dataTable)
        {
            return CreateHashTable(dataTable, false);
        }

        public static Hashtable CreateHashTable(DataTable dataTable, bool ignoreCase, bool swapColumns = false)
        {
            Hashtable result = ignoreCase ? CollectionsUtil.CreateCaseInsensitiveHashtable() : new Hashtable();

            // Traverse Data Table
            foreach (DataRow row in dataTable.Rows)
            {
                // Get Key
                var key = swapColumns ? DBValue.GetString(row[1]) : DBValue.GetString(row[0]);

                // Ignore duplicate Keys
                if (!result.ContainsKey(key))
                {
                    // Add element
                    result.Add(key, swapColumns ? DBValue.GetString(row[0]) : DBValue.GetString(row[1]));
                }
            }

            // Return Hash Table
            return result;
        }
        public static SortedList CreateSortedList(DataTable dataTable)
        {
            // Declare vars
            SortedList result = CollectionsUtil.CreateCaseInsensitiveSortedList();

            // Traverse Data Table
            foreach (DataRow row in dataTable.Rows)
            {
                // Get Key
                string key = DBValue.GetString(row[0]);

                // Ignore duplicate Keys
                if (!result.ContainsKey(key))
                {
                    // Add element
                    result.Add(key, DBValue.GetString(row[1]));
                }
            }

            // Return Sorted List
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataTable">RS with the db call values</param>       
        /// <param name="tableKey">Name of the row field in the Data Table </param>       
        /// <param name="tableValue">Name of the row field in the Data Table</param>
        /// <returns></returns>
        public static SortedList CreateSortedList(DataTable dataTable, string tableKey, string tableValue)
        {
            SortedList result = CollectionsUtil.CreateCaseInsensitiveSortedList();

            foreach (var dataRow in dataTable.AsEnumerable())
            {
                var key = dataRow.Field<string>(tableKey);
                var value = dataRow.Field<string>(tableValue);
                result.Add(key, value);
            }
            
            return result;
        }

        public static NameValueCollection CreateNameValueCollection(DataTable dataTable)
        {
            // Declare vars
            var result = new NameValueCollection();

            // Traverse Data Table
            foreach (DataRow row in dataTable.Rows)
            {
                // Get Key
                string key = DBValue.GetString(row[0]);

                // Ignore duplicate Keys
                if (result[key] == null)
                {
                    // Add element
                    result.Add(key, DBValue.GetString(row[1]));
                }
            }

            // Return Collection
            return result;
        }

        public static OrderedDictionary CreateOrderedDictionary(DataTable dataTable)
        {
            // Declare vars
            OrderedDictionary result = new OrderedDictionary();

            // Traverse Data Table
            foreach (DataRow row in dataTable.Rows)
            {
                // Get Key
                string key = DBValue.GetString(row[0]);

                // Ignore duplicate Keys
                if (!result.Contains(key))
                {
                    // Add element
                    result.Add(key, DBValue.GetString(row[1]));
                }
            }

            // Return Dictionary
            return result;
        }

        public static OrderedDictionary CreateOrderedDictionary(DataTable dataTable, string keyField, string valueField)
        {
            // Declare vars
            OrderedDictionary result = new OrderedDictionary();

            // Traverse Data Table
            foreach (DataRow row in dataTable.Rows)
            {
                // Get Key
                string key = DBValue.GetString(row[keyField]);

                // Ignore duplicate keys
                if (!result.Contains(key))
                {
                    // Add element
                    result.Add(key, DBValue.GetString(row[valueField]));
                }
            }

            // Return Dictionary
            return result;
        }

        /// <summary>
        /// Join each item in the collection together with the glue string, returning a single string
        /// Each object in the collection will be converted to string with ToString()
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="glue"></param>
        /// <returns></returns>
        public static string Join(this ICollection collection, string glue)
        {
            string returnVal = null;
            if (collection != null)
            {
                foreach (object o in collection)
                {
                    if (returnVal == null)
                    {
                        returnVal = o.ToString();
                    }
                    else
                    {
                        returnVal = returnVal + glue + o;
                    }
                }
            }
            return returnVal;
        }


        /// <summary>
        /// Checks whether a collection is the same as another collection
        /// </summary>
        /// <param name="value">The current instance object</param>
        /// <param name="compareList">The collection to compare with</param>
        /// <param name="comparer">The comparer object to use to compare each item in the collection.  If null uses EqualityComparer(T).Default</param>
        /// <returns>True if the two collections contain all the same items in the same order</returns>
        public static bool IsEqualTo<TSource>(this IEnumerable<TSource> value, IEnumerable<TSource> compareList, IEqualityComparer<TSource> comparer)
        {
            if (value == compareList)
            {
                return true;
            }
            if (value == null || compareList == null)
            {
                return false;
            }
            if (comparer == null)
            {
                comparer = EqualityComparer<TSource>.Default;
            }

            IEnumerator<TSource> enumerator1 = value.GetEnumerator();
            IEnumerator<TSource> enumerator2 = compareList.GetEnumerator();

            bool enum1HasValue = enumerator1.MoveNext();
            bool enum2HasValue = enumerator2.MoveNext();

            try
            {
                while (enum1HasValue && enum2HasValue)
                {
                    if (!comparer.Equals(enumerator1.Current, enumerator2.Current))
                    {
                        return false;
                    }

                    enum1HasValue = enumerator1.MoveNext();
                    enum2HasValue = enumerator2.MoveNext();
                }

                return !(enum1HasValue || enum2HasValue);
            }
            finally
            {
                enumerator1.Dispose();
                enumerator2.Dispose();
            }
        }

        /// <summary>
        /// Checks whether a collection is the same as another collection
        /// </summary>
        /// <param name="value">The current instance object</param>
        /// <param name="compareList">The collection to compare with</param>
        /// <returns>True if the two collections contain all the same items in the same order</returns>
        public static bool IsEqualTo<TSource>(this IEnumerable<TSource> value, IEnumerable<TSource> compareList)
        {
            return IsEqualTo(value, compareList, null);
        }

        /// <summary>
        /// Checks whether a collection is the same as another collection
        /// </summary>
        /// <param name="value">The current instance object</param>
        /// <param name="compareList">The collection to compare with</param>
        /// <returns>True if the two collections contain all the same items in the same order</returns>
        public static bool IsEqualTo(this IEnumerable value, IEnumerable compareList)
        {
            return IsEqualTo(value.OfType<object>(), compareList.OfType<object>());
        }

        public static IList<T> Clone<T>(this IList<T> listToClone) where T : ICloneable
        {
            return listToClone.Select(item => (T)item.Clone()).ToList();
        }



    }
}
