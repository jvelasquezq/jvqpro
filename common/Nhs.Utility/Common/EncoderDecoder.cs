using System;
using System.Collections.Generic;
using System.Text;

namespace Nhs.Utility.Common
{
    public static class EncoderDecoder
    {
        public static string base64Encode(string decodedString)
        {
            try
            {
                byte[] byteToEncode = new byte[decodedString.Length];
                byteToEncode = Encoding.UTF8.GetBytes(decodedString);
                string encodedData = Convert.ToBase64String(byteToEncode);
                return encodedData;
            }
            catch (Exception e)
            {
                throw new Exception("Error in EncodeBase64" + e.Message);
            }
        }

        public static string base64Decode(string encodedString)
        {
            try
            {
                UTF8Encoding encoder = new System.Text.UTF8Encoding();
                Decoder utf8Decode = encoder.GetDecoder();

                byte[] byteToDecode = Convert.FromBase64String(encodedString);
                int charCount = utf8Decode.GetCharCount(byteToDecode, 0, byteToDecode.Length);
                char[] decodedChars = new char[charCount];
                utf8Decode.GetChars(byteToDecode, 0, byteToDecode.Length, decodedChars, 0);
                string decodedData = new String(decodedChars);
                return decodedData;
            }
            catch (Exception e)
            {
                throw new Exception("Error in DecodeBase64" + e.Message);
            }
        }
    }
}