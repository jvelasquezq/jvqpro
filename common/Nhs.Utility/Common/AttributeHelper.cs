﻿using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Nhs.Utility.Common
{
    /// <summary>
    /// 
    /// </summary>
    public static class AttributeHelper
    {
        /// <summary>
        /// Helper method to initialize defaults on property attributes
        /// </summary>
        /// <param name="o">The o.</param>
        public static void InitDefaults(this object o)
        {
            PropertyInfo[] props = o.GetType().GetProperties(BindingFlags.Public |
                                                             BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);

            foreach(PropertyInfo prop in props)
            {
                if (prop.GetCustomAttributes(true).Length > 0)
                {
                    object[] defaultValueAttribute = prop.GetCustomAttributes(typeof(DefaultValueAttribute), true);
                    if (defaultValueAttribute.Length > 0)
                    {
                        DefaultValueAttribute dva = defaultValueAttribute[0] as DefaultValueAttribute;
                        if (dva != null)
                            prop.SetValue(o, dva.Value, null);
                    }
                }
            }
        }

        public static T CloneHimself<T>(this T obj) where T : class,new()
        {
            var newObj = new T();
            return newObj.CopyFrom(obj);
        }

        public static T1 CopyFrom<T1, T2>(this T1 obj, T2 otherObject)
            where T1 : class
            where T2 : class
        {
            PropertyInfo[] srcFields = otherObject.GetType().GetProperties(
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty);

            PropertyInfo[] destFields = obj.GetType().GetProperties(
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty);

            foreach (var property in srcFields)
            {
                try
                {
                    var dest = destFields.FirstOrDefault(x => x.Name == property.Name);
                    if (dest != null)
                        dest.SetValue(obj, property.GetValue(otherObject, null), null);
                }
                catch 
                {
                    
                  
                }
            }

            return obj;
        }

    }
}
