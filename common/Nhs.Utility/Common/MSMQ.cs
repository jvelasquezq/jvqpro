using System;
using System.Messaging;
using System.Threading;
using System.Diagnostics;

namespace Nhs.Utility.Common
{
    /// <summary>
    /// Summary description for MSMQ.
    /// </summary>
    public class MSMQ
    {
        public static void PostMessage(string Q, string label, string body)
        {   //body is XML string.			
            MessageQueue myQueue = new MessageQueue(Q);
            myQueue.Formatter = new XmlMessageFormatter();

            System.Messaging.Message myMessage = new Message();
            myMessage.Body = body;
            myMessage.Label = label;
            myMessage.Recoverable = true;
            myQueue.Send(myMessage);
        }

        public static void PostBTSMessage(string Q, string label, string body)
        {
            MessageQueue myQueue = new MessageQueue(Q);
            myQueue.Formatter = new ActiveXMessageFormatter();

            System.Messaging.Message myMessage = new Message();
            myMessage.Body = body;
            myMessage.Label = label;
            myMessage.Recoverable = true;
            myQueue.Send(myMessage);
        }

        public static Message GetMessage(string Q)
        {
            try
            {
                MessageQueue myQueue = new MessageQueue(Q);
                myQueue.Formatter = new XmlMessageFormatter(new[] { typeof(string) });

                Message myMessage = myQueue.Receive(new TimeSpan(0, 0, 1));
                return myMessage;
            }

            catch (MessageQueueException e)
            {
                if (e.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
                    return null;
                else
                    throw new Exception("Could not read from queue " + Q, e);

            }
        }
    }
}