﻿using System;
using System.Collections.Generic;

namespace Nhs.Utility.Common
{
    public class ConcretePathMapper : IPathMapper
    {
        private readonly string _absoluteFolderPath;
        private Dictionary<string, object> _contextItems = new Dictionary<string, object>();

        public ConcretePathMapper(string absoluteFolderPath)
        {
            _absoluteFolderPath = absoluteFolderPath;
        }

        public string MapPath(string relativePath)
        {
            return _absoluteFolderPath + StringHelper.StripLeadingEndingChar(relativePath, '~').Replace("/", @"\");
        }

        public void AddItemToContext(string keyToAdd, object valueToAdd)
        {
            if (!_contextItems.ContainsKey(keyToAdd))
                _contextItems.Add(keyToAdd, valueToAdd);
        }

        public object GetItemFromContext(string itemKey)
        {
            return _contextItems.ContainsKey(itemKey) ? _contextItems[itemKey] : null;
        }
    }
}