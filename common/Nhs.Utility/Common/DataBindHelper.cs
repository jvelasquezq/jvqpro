using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;

namespace Nhs.Utility.Common
{
    public class DataBindHelper
    {
        public static void BindDropDown(object dataSource, DropDownList dropDownList, 
                                        string textField, string valueField)
        {
            dropDownList.DataSource = dataSource;
            dropDownList.DataTextField = textField;
            dropDownList.DataValueField = valueField;
            dropDownList.DataBind();
        }

        public static void BindDropDown(object dataSource, DropDownList dropDownList, 
                                        string textField, string valueField, string defaultName, string defaultValue)
        {
            BindDropDown(dataSource,dropDownList,textField,valueField);
            dropDownList.Items.Insert(0, new ListItem(defaultName, defaultValue));
        }
    }
}