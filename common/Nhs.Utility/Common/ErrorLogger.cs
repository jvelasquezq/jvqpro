/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright (c) 2001-2006
 **by BuilderHomeSite Inc. ( http://www.BuilderHomeSite.com )
 * ========================================================
 * Author: ASolis
 * Date: 11/30/2006 17:31:23
 * Purpose:
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
using System;
using System.Diagnostics;
using System.Net;
using System.Security.Principal;
using System.Threading;
using System.Web;
using log4net;

namespace Nhs.Utility.Common
{
    /// <summary>
    /// Class to log exception errors using the log4net logging framework
    /// </summary>
    /// <remarks>
    /// 
    /// </remarks>
    public sealed class ErrorLogger
    {
        #region Members
        private static volatile  ILog _log;
        #endregion

        #region Private Methods

        /// <summary>
        /// Sets all the parameters required by the PatternLayout to fulfill the format required.
        /// </summary>
        /// <param name="ex">Exception that have the error's information to log on</param>
        private static void SetMessageParameters()
        {
            string fromPage = string.Empty; // information about the URL of the client's previous request that linked to the current URL
            string passedData = string.Empty; // Information of the query string parameters
            string requestMethod = string.Empty; // request method used by the client (GET or POST)
            string pageUrl = string.Empty; // page where the error occurs
            string hostName = string.Empty; // host name, extracted from url
            string clientIP = string.Empty; // Client IP
            string clientPort = string.Empty; // port used for the user to connect to the server (by default 80, it's extracted from the url)
            string sessionID = string.Empty; // session number from the request
            string cookies = string.Empty; // information of data stored in client cookies
            string userAgent = string.Empty; // user agent

            // Set system related paramters
            MDC.Set(LogErrorFields.ProcessID, Process.GetCurrentProcess().Id.ToString());
            MDC.Set(LogErrorFields.ProcessName, Process.GetCurrentProcess().ProcessName);
            MDC.Set(LogErrorFields.ThreadName, Thread.CurrentThread.Name);
            MDC.Set(LogErrorFields.WindowsIdentity, WindowsIdentity.GetCurrent().Name);
            MDC.Set(LogErrorFields.Machine, Environment.MachineName);

            // set asp .net related parameters if exists
            if (HttpContext.Current != null)
            {
                // get values from the current httpcontext
                pageUrl = HttpContext.Current.Request.RawUrl;
                requestMethod = HttpContext.Current.Request.RequestType;
                hostName = HttpContext.Current.Request.Url.Host;
                clientIP = HttpContext.Current.Request.UserHostAddress;
                clientPort = HttpContext.Current.Request.Url.Port.ToString();
                if (HttpContext.Current.Session != null)
                    sessionID = (string.IsNullOrEmpty(HttpContext.Current.Session.SessionID)) ? "Not provided" : HttpContext.Current.Session.SessionID;
                userAgent = HttpContext.Current.Request.UserAgent;

                //Set referer if exists
                if (HttpContext.Current.Request.UrlReferrer != null)
                    fromPage = HttpContext.Current.Request.UrlReferrer.ToString();

                // get the passed data depends of the Request method
                if (requestMethod == WebRequestMethods.Http.Post)
                {
                    // if is the POST http method get it from the Request Form object
                    if (HttpContext.Current.Request.Form.Count > 0)
                    {
                        if (HttpContext.Current.Request.TotalBytes > 200)
                            passedData =
                                HttpContext.Current.Server.HtmlEncode(
                                    (Convert.ToString(HttpContext.Current.Request.Form).Substring(0, 200))) + " . . .";
                        else
                            passedData =
                                HttpContext.Current.Server.HtmlEncode(Convert.ToString(HttpContext.Current.Request.Form));
                    }
                }
                else
                {
                    // if is the GET http method get it from the url query string values
                    passedData = Convert.ToString(HttpContext.Current.Request.RawUrl);
                }

                // get the cookies collection from the request object
                for (int i = 0; i < HttpContext.Current.Request.Cookies.Count; i++)
                    cookies += "," + HttpContext.Current.Request.Cookies[i].Name + "=" + HttpContext.Current.Request.Cookies[i].Value;

                cookies = string.IsNullOrEmpty(cookies) ? cookies : cookies.Substring(1); // remove the initial comma
            }

            // set the log4net MDC obj to pass the field required in the PatternLayout
            MDC.Set(LogErrorFields.FromPage, fromPage);
            MDC.Set(LogErrorFields.HTTPMethod, requestMethod);
            MDC.Set(LogErrorFields.PassingData, passedData);
            MDC.Set(LogErrorFields.ToPage, pageUrl);
            MDC.Set(LogErrorFields.HostName, hostName);
            MDC.Set(LogErrorFields.ClientIP, clientIP);
            MDC.Set(LogErrorFields.ClientPort, clientPort);
            MDC.Set(LogErrorFields.SessionID, sessionID);
            MDC.Set(LogErrorFields.Cookies, cookies);
            MDC.Set(LogErrorFields.UserAgent, userAgent);
        }

        #endregion

        #region Static Methods
        /// <summary>
        /// Logs an error, extracting all the information required from the exception and the http context.
        /// </summary>
        /// <param name="ex">Exception with the error information to log on</param>
        public static void LogError(Exception ex)
        {
            try
            {
                if (_log == null)
                    _log = LogManager.GetLogger(typeof(ErrorLogger));

                SetMessageParameters();

                // set exception specific details
                MDC.Set(LogErrorFields.ErrorDetails, ex.ToString());

                string statusCode = HttpContext.Current.Request.QueryString["statuscode"];

                if(!string.IsNullOrEmpty(statusCode) && statusCode == "404")
                {
                    MDC.Set(LogErrorFields.ErrorType, "INFO");
                    
                }
                else
                {
                    MDC.Set(LogErrorFields.ErrorType, ex.GetType().ToString());
                }

                _log.Error(ex.Message);
            }
            catch
            {
                // ignored
            }
        }

        public static void LogError(string type, string description, string details)
        {
            try
            {
                if (_log == null)
                    _log = LogManager.GetLogger(typeof(ErrorLogger));

                SetMessageParameters();

                // set exception specific details
                MDC.Set(LogErrorFields.ErrorDetails, details);
                MDC.Set(LogErrorFields.ErrorType, type);

                _log.Error(description);
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public static void LogError(string error )
        {
            try
            {
                if (_log == null)
                {
                    _log = LogManager.GetLogger(typeof(ErrorLogger));
                }

                if (_log.IsErrorEnabled)
                {
                    _log.Error(error);
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        #endregion

    }
}
