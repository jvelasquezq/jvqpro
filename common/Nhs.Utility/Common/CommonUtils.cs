using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Nhs.Utility.Common
{
    public static class CommonUtils
    {
        public const string ZipCodeRegex = @"^\d{5}(-\d{4})?$";
        public static bool IsZip(string zip)
        {
            /* +---------------- the beginning of the string
                                        |+--------------- a number
                                        ||  +------------ five times
                                        ||  | +---------- ( begin optional group
                                        ||  | |+--------- literal "-"
                                        ||  | ||+-------- a number
                                        ||  | |||  +----- four times
                                        ||  | |||  |+---- ) end optional group 
                                        ||  | |||  |||+-- this group is optional
                                        ||  | |||  ||||+- the end of the string
                                        ||  | |||  ||||| */
            Regex zipCode = new Regex(ZipCodeRegex);
            return zipCode.IsMatch(zip);
        }

        public static bool IsZip(TextBox txtZip)
        {
            return IsZip(txtZip.Text.Trim());
        }

        public static bool IsPhone(string phone)
        {
            phone = phone.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Replace(".", "").Trim();
            if (phone.Length == 10)
            {
                if (!IsNumeric(phone))
                    return false;
            }
            else
                return false;
            return true;
        }

        public static bool IsNumeric(object Expression)
        {
            bool isNum;
            double retNum;
            isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any,
                                    System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        public static string ConvertPhoneToNumber(string phone)
        {
            if (phone.Length > 0)
                return phone.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Trim();
            else
                return string.Empty;
        }

        public static long GetAdRandomNumber()
        {
            long lngDCMinNum = 1000000;
            long lngDCMaxNum = 999999999;
            long randNum = 0;
            randNum = new Random().Next((int) lngDCMinNum, (int) lngDCMaxNum);
            return randNum;
        }

        public static Int32 ConvertToInt32(string asString)
        {
            double asDouble = new double();
            int asInt32 = new int();

            if (double.TryParse(asString, out asDouble) &&
                (asDouble < int.MaxValue) &&
                (asDouble > int.MinValue))
                asInt32 = Convert.ToInt32(asDouble);

            return asInt32;
        }

        public static bool ToBool(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return false;

            switch (value.ToLower())
            {
                case "y":
                    return true;
                default:
                    return false;
            }
        }

        public static T ToType<T>(this object value)
        {

            //try to convert using modified ChangeType which handles nullables
            try
            {
                if (value == null) return default(T);

                return Convert.IsDBNull(value) ? default(T) : (T) ChangeType(value, typeof (T));
            }
                //if there is an exception then get the default value of parameterized type T
            catch (Exception)
            {
                return default(T);
            }
        }
        /// <summary>
        /// This Method convert an Object to a string and then to a List of Int values.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="values">Values separted by coma</param>
        /// <returns></returns>
        public static IList<int> ToIntList(this object values, char separator = ',')
        {
            //try to convert using modified ChangeType which handles nullables
            try
            {
                if (values == null) return new List<int>();

                var charValues = values.ToType<string>();
                return charValues.Split(separator).Select(n => Convert.ToInt32(n)).ToList();
            }
            //if there is an exception then get the default value of parameterized type T
            catch (Exception)
            {
                return new List<int>();
            }
        }
        /// <summary>
        /// This Method convert an Object to a string and then to a List of String values.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="values">Values separted by coma</param>
        /// <returns></returns>
        public static IList<string> ToStringList(this object values, char separator = ',')
        {
            //try to convert using modified ChangeType which handles nullables
            try
            {
                if (values == null) return new List<string>();

                var charValues = values.ToType<string>();
                return charValues.Split(separator).ToList();
            }
            //if there is an exception then get the default value of parameterized type T
            catch (Exception)
            {
                return new List<string>();
            }
        }
        internal static object ChangeType(object value, Type conversionType)
        {
            if (conversionType == null)
            {
                throw new ArgumentNullException("conversionType");
            } 

            if (conversionType.IsGenericType && conversionType.GetGenericTypeDefinition() == typeof (Nullable<>))
            {
                if (value == null)
                {
                    return null;
                } 
                var nullableConverter = new NullableConverter(conversionType);
                conversionType = nullableConverter.UnderlyingType;
            }
            return Convert.ChangeType(value, conversionType);
        }

        public static Dictionary<string, T> MergeDictionaries<T>(this Dictionary<string, T> target,
                                                                   Dictionary<string, T> options)
        {
            if (options == null)
                return target;

            foreach (var name in options)
            {
                var containsKey = target.ContainsKey(name.Key);
                if (containsKey)
                    target[name.Key] = options[name.Key];
                else
                    target.Add(name.Key, options[name.Key]);
            }

            return target;
        }

        public static Dictionary<string, object> MergeDictionaries(this Dictionary<string, object> target,
                                                                   Dictionary<string, object> options)
        {
            if (options == null)
                return target;

            foreach (var name in options)
            {
                var containsKey = target.ContainsKey(name.Key);
                if (containsKey)
                    target[name.Key] = options[name.Key];
                else
                    target.Add(name.Key, options[name.Key]);
            }

            return target;
        }
    }
}