/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright (c) 2001-2006
 **by BuilderHomeSite Inc. ( http://www.BuilderHomeSite.com )
 * ========================================================
 * Author: RMenon
 * Date: 10/11/2006 22:53:35
 * Purpose:
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
using System.IO;
using System.Net;
using System.Web;

namespace Nhs.Utility.Common
{
    public class PathHelpers
    {
        #region Public Static Methods
        /// <summary>
        /// Gets the absolute URL.
        /// </summary>
        /// <param name="pathWithoutRoot">The path without root.</param>
        /// <returns></returns>
        public static string GetAbsoluteURL(string pathWithoutRoot)
        {
            string host = HttpContext.Current.Request.Url.Host;            
            host = HttpContext.Current.Request.IsSecureConnection ? "https://" + host : "http://" + host;
            
            return host + pathWithoutRoot;
        }

        /// <summary>
        /// Gets the file path in virtual folder.
        /// </summary>
        /// <param name="folderName">Name of the virtual folder.</param>
        /// <param name="fileName">Name of the file or full path.</param>
        /// <param name="pathMapper">The path mapper.</param>
        /// <returns></returns>
        /// TODO: Combine this and the other overload into a single method
        public static string GetFileInVirtualFolder(string folderName, string fileName, IPathMapper pathMapper)
        {
            //check flag
            //if folder is not specified return
            if (folderName == null)
            {
                return string.Empty;
            }
            //if file is empty check for default.ascx 
            //TODO: Remove all dependency on NHS logic from this project
            if (string.IsNullOrEmpty(fileName))
            {
                if (fileName == string.Empty)
                {
                    fileName = "default.ascx";
                }
            }
            var filePathTable = HttpContext.Current.Items;
            //check if filePath to return is already in the static stringdictionary
            string filePath = filePathTable[fileName.ToLower()].ToType<string>();
            if (!string.IsNullOrEmpty(filePath))
            {
                return filePath;
            }
            //create a fileInfo object for the fileName
            FileInfo fileToSearch = new FileInfo(fileName);
            string fileExtension = string.Format("*{0}", fileToSearch.Extension);

            //create DirectoryInfo obj from given virtual folder
            DirectoryInfo di = new DirectoryInfo(pathMapper.MapPath(folderName));
            if (di.Exists)
            {
                foreach (FileInfo f in di.GetFiles(fileExtension, SearchOption.AllDirectories))
                {
                    bool check = (f.Name.ToLower() == fileToSearch.Name.ToLower());

                    if (check)
                    {
                        filePath = PathMap(f.FullName, folderName, pathMapper);
                        lock (filePathTable.SyncRoot)
                        {
                            //key is the filename passed in the method
                            filePathTable[fileName.ToLower()] = filePath;
                        }
                        //control found in one of the Folders; return
                        break;
                    }
                }
            }

            return filePath;
        }

        /// <summary>
        /// Gets file path in a virtual folder - puts in context
        /// </summary>
        /// <param name="folderName">Name of the folder.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="recursive">if set to <c>true</c> [recursive].</param>
        /// <param name="pathMapper">The path mapper.</param>
        /// <returns></returns>
        public static string GetFileInVirtualFolder(string folderName, string fileName, bool recursive, IPathMapper pathMapper)
        {
            string cacheKey = folderName + "_" + fileName;

            //if folder is not specified return
            if (string.IsNullOrEmpty(folderName))
                return string.Empty;

            var filePathTable = HttpContext.Current.Items;
            //check if filePath to return is already in the static stringdictionary
            string filePath = filePathTable[cacheKey].ToType<string>();
            if (!string.IsNullOrEmpty(filePath))
            {
                return filePath;
            }
            //create a fileInfo object for the fileName
            FileInfo fileToSearch = new FileInfo(fileName);
            string fileExtension = string.Format("*{0}", fileToSearch.Extension);

            //create DirectoryInfo obj from given virtual folder
            DirectoryInfo di = new DirectoryInfo(pathMapper.MapPath(folderName));
            if (di.Exists)
            {
                SearchOption so = recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
                foreach (FileInfo f in di.GetFiles(fileExtension, so))
                {
                    bool check = (f.Name.ToLower() == fileToSearch.Name.ToLower());

                    if (check)
                    {
                        filePath = PathMap(f.FullName, folderName);
                        lock (filePathTable.SyncRoot)
                        {
                            filePathTable[cacheKey] = filePath;
                        }
                        //File found in one of the Folders; return
                        break;
                    }
                }
            }
            return filePath;
        }

        public static string PathMap(string path, string folder)
        {
            return PathMap(path, folder, new ContextPathMapper());
        }

        /// <summary>
        /// reverse of Server.MapPath
        /// </summary>
        /// <param name="path">physical path</param>
        /// <param name="folder">The folder.</param>
        /// <param name="pathMapper">The path mapper.</param>
        /// <returns>virtual absolute path</returns>
        public static string PathMap(string path, string folder, IPathMapper pathMapper)
        {
            string approot = pathMapper.MapPath(folder);

            return Combine(folder, path.Replace(approot, string.Empty).Replace('\\', '/'));
        }

        /// <summary>
        /// System.IO.Path.Combine behaves properly when there are no back slashes in second param
        /// <para>
        /// <c>
        /// Path.Combine("d:\temp\", "rajiv.txt")   --> d:\temp\rajiv.txt
        /// </c>
        /// </para>
        /// <c>
        /// Path.Combine("d:\temp\", "\rajiv.txt")  -->  \rajiv.txt 
        /// </c>
        /// <para>
        /// This function is a utility function that combines the specified args by taking care of \ and /
        /// </para>
        /// </summary>
        /// <param name="args">The args.</param>
        /// <returns></returns>
        private static string Combine(params string[] args)
        {
            for (int i = 0; i < args.Length; i++)
            {
                args[i] = args[i].Replace("/", "\\");
                args[i] = args[i].TrimStart("\\".ToCharArray());
                if (i != 0)
                {
                    args[0] = Path.Combine(args[0], args[i]);
                }
            }
            return args.Length == 0 ? string.Empty : args[0].Replace("\\", "/");
        }

        /// <summary>
        /// Cleans the navigate URL. Inserts http if none present
        /// </summary>
        /// <param name="navUrl">The nav URL.</param>
        /// <returns></returns>
        public static string CleanNavigateUrl(string navUrl)
        {
            if (string.IsNullOrEmpty(navUrl) || string.IsNullOrEmpty(navUrl.Trim()))
                return string.Empty;

            return (navUrl.ToLower().IndexOf("http") == -1) ?
                                                                string.Format("http://{0}", navUrl) : navUrl;
        }

        /// <summary>
        /// Cleans the navigate URL. removing http if present
        /// </summary>
        /// <param name="navUrl">The nav URL.</param>
        /// <returns></returns>
        public static string RemoveUrlPrefix(string navUrl)
        {
            return (navUrl.ToLower().IndexOf("http://") != -1) ? navUrl.Replace("http://", string.Empty) : navUrl;
        }

        /// <summary>
        /// Check if file exists is the given url.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <returns></returns>
        public static bool HttpFileExists(string url)
        {
            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)WebRequest.Create(url).GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }

            }
        }
        #endregion

    }
}