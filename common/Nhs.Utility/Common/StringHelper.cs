using System;
using System.Collections.Generic;
using System.Collections;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace Nhs.Utility.Common
{
    public static class StringHelper
    {
        public const string EmailRegex = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
        public const string EmailRegexfull = @"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$";
        public const string ZipRegex = @"^\d{5}(-\d{4})?$";
        //public const string NameLastnameRegex = @"^[a-zA-Z]+\s[a-zA-Z]+$";
        public const string NameLastnameRegex = @"^([A-Za-z]+(-?'?[A-Za-z+'?-?]+))(\s([A-Za-z]+(-?'?[A-Za-z'?-?]+)?))+?$";
        private static readonly Random Random = new Random((int)DateTime.Now.Ticks);

        public static string RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9% / ._-]", String.Empty, RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);
        }

        public static string GetRandomString(int size)
        {
            var builder = new StringBuilder();
            for (var i = 0; i < size; i++)
            {
                var ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * Random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }

        public static string CutLenghtByWords(this string value, int lenght)
        {
            if (string.IsNullOrWhiteSpace(value) || value.Length < lenght)
            {
                return value ?? string.Empty;
            }

            var retValue = value.Substring(0, lenght);
            var lastSpace = retValue.LastIndexOf(' ');

            if (lastSpace > 0)
            {
                retValue = retValue.Substring(0, lastSpace);
            }

            return retValue.Trim();
        }


        public static bool IsSqlLikeMatch(string input, string pattern)
        {
            /* Turn "off" all regular expression related syntax in
            * the pattern string. */
            pattern = Regex.Escape(pattern);

            /* Replace the SQL LIKE wildcard metacharacters with the
            * equivalent regular expression metacharacters. */
            pattern = pattern.Replace("%", ".*?").Replace("_", ".");

            /* The previous call to Regex.Escape actually turned off
            * too many metacharacters, i.e. those which are recognized by
            * both the regular expression engine and the SQL LIKE
            * statement ([...] and [^...]). Those metacharacters have
            * to be manually unescaped here. */
            pattern = pattern.Replace(@"\[", "[").Replace(@"\]",
            "]").Replace(@"\^", "^");

            return Regex.IsMatch(input, pattern);
        }

        public static string CutLenghtByWords(this string value, int lenght, string ellipseText)
        {
            var cutText = CutLenghtByWords(value, lenght);
            var showEllipse = cutText.Length < value.Length;
            return showEllipse ? cutText + ellipseText : cutText;
        }

        public static bool IsValidImage(this string src)
        {
            if (string.IsNullOrEmpty(src))
                return false;

            const string imgRegEx = @".(gif|jpg|jpeg|tiff|tif|png|svg)";

            var rex = new Regex(imgRegEx);
            return rex.Match(src).Success;
        }

        public static bool ExactMatch(string source, string match)
        {
            return Regex.IsMatch(source, string.Format(@"\b{0}\b", Regex.Escape(match)));
        }

        public static string Between(string source, string startString, string endString)
        {
            if (string.IsNullOrEmpty(source))
                return string.Empty;

            var startIndex = source.IndexOf(startString, StringComparison.Ordinal) + startString.Length;
            var endIndex = source.IndexOf(endString, startIndex, StringComparison.Ordinal);
            var substringLength = endIndex - startIndex;
            return substringLength > 0 ? source.Substring(startIndex, substringLength) : string.Empty;
        }

        public static string GetDelimitedString(string delimiter, ArrayList list)
        {
            var s = new StringBuilder();
            foreach (var t in from object t in list where !t.ToString().Equals("") select t)
            {
                if (s.Length > 0) s.Append(delimiter);
                s.Append(t);
            }
            return s.ToString();

        }

        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
                return false;

            const string emailRegEx = EmailRegex;

            var re = new Regex(emailRegEx);
            return
                (
                    (re.Match(email).Success) &&
                    (email.IndexOf(' ') == -1) &&
                    (email.Length <= 50)
                );
        }

        public static bool IsValidEmailFull(string email)
        {
            if (string.IsNullOrEmpty(email))
                return false;

            const string emailRegEx = EmailRegexfull;

            var re = new Regex(emailRegEx);
            return
                (
                    (re.Match(email).Success) &&
                    (email.IndexOf(' ') == -1) &&
                    (email.Length <= 50)
                );
        }

        public static bool IsValidZip(string postalCode)
        {
            // truncate any characters if postal code string is greater than five characters
            //if (postalCode.Length > 5)
            //    postalCode = postalCode.Substring(0, 5);

            // perform regular expression validation
            var valeZipCode = new Regex(ZipRegex);

            return valeZipCode.Match(postalCode).Success;
        }

        public static string FormatPhone(string number)
        {
            return FormatPhone(string.Empty, number, string.Empty);
        }

        public static bool StringContainsAnyLetter(string value)
        {
            return Regex.Matches(value, @"[a-zA-Z]").Count > 0;
        }

        public static T FormatPhoneAsDigitsOnly<T>(string phone)
        {
            string result = Regex.Replace(phone, @"[^\d]", "");
            bool validType = typeof (T) == typeof (string) || typeof (T) == typeof (int);
            if (validType == false)
            {
                throw new ArgumentException("T type must be int or string and you use " + typeof(T).Name);
            }

            return result.ToType<T>();
        }

        public static string FormatPhone(string areaCode, string number, string extension)
        {
            var sPhoneNumber = string.Empty;
            var sAreaCode = string.Empty;
            var sPhoneExt = string.Empty;

            // prepare input
            number = number.Replace("-", "").Replace("(", "").Replace(")", "").Replace(" ", "").Replace(".", "").Trim();

            if (number.Length == 10)
            {
                sPhoneNumber = number.Substring(3);
                sPhoneExt = extension;
                sAreaCode = number.Substring(0, 3);
            }
            else
            {
                if (number.Length == 7)
                {
                    sPhoneNumber = number;
                    sPhoneExt = extension;
                    sAreaCode = areaCode;
                }
                else
                {
                    if ((number.Length > 10) && (number.Substring(0, 2) == "18"))
                    {
                        sPhoneNumber = number.Substring(4, 7);
                        sAreaCode = number.Substring(1, 3);

                        sPhoneExt = number.Length > 11 ? number.Substring(11) : extension;
                    }
                    else if (number.Length > 10)
                    {
                        sPhoneNumber = number.Substring(3, 7);
                        sAreaCode = number.Substring(0, 3);
                        sPhoneExt = number.Substring(10);
                    }

                }
            }
            if (sPhoneNumber.Length > 3)
                sPhoneNumber = sPhoneNumber.Insert(3, "-");

            if (sAreaCode.Length > 0)
                sAreaCode = "(" + sAreaCode + ")";

            if (sPhoneExt.Length > 0)
                sPhoneExt = "Ext. " + sPhoneExt;

            // Added the following lines - Case 5250
            // - dayaker
            string sTelePhoneNumber = string.Empty;
            if (sAreaCode != String.Empty)
            {
                sTelePhoneNumber = sAreaCode + " ";
            }
            if (sPhoneNumber != String.Empty)
            {
                sTelePhoneNumber += sPhoneNumber;
            }

            if (sPhoneExt != String.Empty)
            {
                sTelePhoneNumber += " " + sPhoneExt;
            }

            //return sAreaCode + " " + sPhoneNumber + " " + sPhoneExt;
            return sTelePhoneNumber;
        }

        public static string StripLeadingEndingChar(string stringToProcess, char chr)
        {
            if (stringToProcess.StartsWith(chr.ToString()))
                stringToProcess = stringToProcess.Remove(0, 1);

            if (stringToProcess.EndsWith(chr.ToString()))
                stringToProcess = stringToProcess.Remove(stringToProcess.Length - 1, 1);

            return stringToProcess;
        }

        public static string AddTrailingSlash(string stringToProcess)
        {
            if (!stringToProcess.EndsWith("/") && stringToProcess.Length > 0)
                return stringToProcess + "/";

            return string.Empty;
        }

        public static string AddLeadingSlash(string stringToProcess)
        {
            if (!stringToProcess.StartsWith("/") && stringToProcess.Length > 0)
                return "/" + stringToProcess;

            return string.Empty;
        }

        /// <summary>
        /// Converts string to proper case.
        /// Same as ToTitleCase 
        /// </summary>
        /// <param name="stringToConvert">The string to convert.</param>
        /// <returns></returns>
        public static string ToProperCase(string stringToConvert)
        {
            return ToTitleCase(stringToConvert);
        }

        public static string ToTitleCase(string stringToConvert)
        {
            return Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(stringToConvert);
        }

        public static string ListToString(List<string> list, string separator)
        {
            var data = list.Where(s => (s != null) && (s.Trim() != String.Empty));
            return string.Join(separator, data);
        }

        public static bool IsNumeric(object expression)
        {
            double retNum;
            return Double.TryParse(Convert.ToString(expression), NumberStyles.Any, NumberFormatInfo.InvariantInfo, out retNum);
        }

        public static object EmptyToNullString(string field)
        {
            if (field == string.Empty)
                return DBNull.Value;

            return field;
        }


        /// <summary>
        /// Pretty prints the range.
        /// </summary>
        /// <example><code>PrettyPrintRange(1000, 2000, "c");</code></example>
        /// <param name="lowValue">The low value.</param>
        /// <param name="highValue">The high value.</param>
        /// <param name="forMatType">
        /// A standard numeric format string 
        /// <see cref="System.Globalization.NumberFormatInfo"/>
        /// <seealso cref="http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpguide/html/cpconstandardnumericformatstringsoutputexample.asp"/>
        /// </param>
        /// <param name="fromString">Depending on the language it would be from or desde</param>
        /// <returns></returns>
        public static string PrettyPrintRange(double lowValue, double highValue, string forMatType, string fromString = "From")
        {
            string arg0Format = "{0:" + forMatType + "}";
            string arg1Format = "{1:" + forMatType + "}";
            if (lowValue == 0 & highValue == 0)
            {
                return string.Empty;
            }
            if ((lowValue > 0 & highValue == 0) | (lowValue == highValue))
                return string.Format(fromString + " " + arg0Format, lowValue); // c -> currency, o -> arg index

            if (lowValue == 0 & highValue > 0)
                return string.Format(fromString + " " + arg0Format, highValue);

            return string.Format(fromString + " " + arg0Format + " - " + arg1Format, lowValue, highValue);
        }

        public static string PrettyPrintRange(decimal lowValue, decimal highValue, string forMatType, string fromString = "From")
        {
            return PrettyPrintRange(lowValue.ToType<double>(), highValue.ToType<double>(), forMatType, fromString);
        }

        public static string PrettyPrintRange(int lowValue, int highValue, string forMatType, string fromString = "From")
        {
            return PrettyPrintRange(lowValue.ToType<double>(), highValue.ToType<double>(), forMatType);
        }

        public static string PrettyPrintRangeWithStyles(double lowValue, double highValue, string forMatType)
        {
            var arg0Format = "{0:" + forMatType + "}";
            var arg1Format = "{1:" + forMatType + "}";

            if (lowValue == 0 & highValue == 0)
                return string.Empty;
            if ((lowValue > 0 & highValue == 0) | (lowValue == highValue))
                return string.Format("<p class=\"nhs_Price\">from <strong>" + arg0Format + "</strong></p>", lowValue); // c -> currency, o -> arg index
            if (lowValue == 0 & highValue > 0)
                return string.Format("<p class=\"nhs_Price\">from <strong>" + arg0Format + "</strong></p>", highValue);
            return string.Format("<p class=\"nhs_Price\">from <strong>" + arg0Format + " - " + arg1Format + "</strong></p>", lowValue, highValue);
        }

        public static string PrettyPrintRangeWithStyles(decimal lowValue, decimal highValue, string forMatType)
        {
            return PrettyPrintRangeWithStyles(lowValue.ToType<double>(), highValue.ToType<double>(), forMatType);
        }

        public static string PrettyPrintRangeWithStyles(int lowValue, int highValue, string forMatType)
        {
            return PrettyPrintRangeWithStyles(lowValue.ToType<double>(), highValue.ToType<double>(), forMatType);
        }
        /// <summary>
        /// Simple currency formatting - $100,000
        /// </summary>
        /// <param name="price"></param>
        /// <returns></returns>
        public static string FormatCurrency(object price)
        {
            return Convert.ToDecimal(price).ToString("C0");
        }


        /// <summary>
        /// Search for home or homes words at the end of the string
        /// </summary>
        /// <param name="name">The string to search for</param>
        /// <returns>True if ends with home word, False otherwise</returns>
        public static bool EndsWithHomeWord(string name)
        {
            if (name.ToLower().EndsWith(" home")) name += "s";
            return (name.EndsWith(" Homes") || name.EndsWith(" homes"));
        }

        /// <summary>
        /// Reverses a String.
        /// </summary>
        /// <param name="source">The string to reverse.</param>
        /// <returns>The reversed string.</returns>
        public static string Reverse(string source)
        {
            var charArray = new char[source.Length];
            int len = source.Length - 1;

            for (int i = 0; i <= len; i++)
                charArray[i] = source[len - i];

            return new string(charArray);
        }

        public static List<string> SplitBodyForMessage(string message)
        {
            var newMessage = new List<string>();
            if (message.Length <= 140)
                newMessage.Add(message.Replace("|", Environment.NewLine));
            else
            {
                var tempMessage = message;
                while (tempMessage.Length > 0)
                {
                    if (tempMessage.Length <= 140)
                    {
                        newMessage.Add(tempMessage.Replace("|", Environment.NewLine));
                        break;
                    }
                    var length = 137;
                    if (!tempMessage.Substring(0, length).EndsWith(" ") || !tempMessage.Substring(0, length).EndsWith("|"))
                        length = tempMessage.Substring(0, length).LastIndexOfAny(new[] { ' ', '|' });

                    newMessage.Add(tempMessage.Substring(0, length).Replace("|", Environment.NewLine) + "...");
                    tempMessage = tempMessage.Remove(0, length).TrimStart('|');
                }
            }
            return newMessage;
        }
    }

    public static class StringHelperExtensions
    {
        public static string EncodeToBase64String(this string source)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(source));
        }

        public static string DecodeFromBase64String(this string source)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(source));
        }

        public static string ReplaceString(this string source, string oldValue, string newValue, StringComparison comparison)
        {
            var sb = new StringBuilder();

            int previousIndex = 0;
            int index = source.IndexOf(oldValue, comparison);
            while (index != -1)
            {
                sb.Append(source.Substring(previousIndex, index - previousIndex));
                sb.Append(newValue);
                index += oldValue.Length;

                previousIndex = index;
                index = source.IndexOf(oldValue, index, comparison);
            }
            sb.Append(source.Substring(previousIndex));

            return sb.ToString();
        }

        public static bool ExactMatch(this string source, string match)
        {
            return Regex.IsMatch(source, string.Format(@"\b{0}\b", Regex.Escape(match)));
        }

        public static string StripLeadingEndingChar(this string stringToProcess, char chr)
        {
            if (stringToProcess.StartsWith(chr.ToType<string>()))
                stringToProcess = stringToProcess.Remove(0, 1);

            if (stringToProcess.EndsWith(chr.ToType<string>()))
                stringToProcess = stringToProcess.Remove(stringToProcess.Length - 1, 1);

            return stringToProcess;
        }
        public static string AddTrailingSlash(this string stringToProcess)
        {
            if (!stringToProcess.EndsWith("/") && stringToProcess.Length > 0)
                return stringToProcess + "/";

            return stringToProcess;
        }
        public static string AddLeadingSlash(this string stringToProcess)
        {
            if (!stringToProcess.StartsWith("/") && stringToProcess.Length > 0)
                return "/" + stringToProcess;

            return stringToProcess;
        }

        /// <summary>
        /// Converts string to proper case.
        /// Same as ToTitleCase 
        /// </summary>
        /// <param name="stringToConvert">The string to convert.</param>
        /// <returns></returns>
        public static string ToProperCase(this string stringToConvert)
        {
            return ToTitleCase(stringToConvert);
        }

        public static string ToTitleCase(this string stringToConvert)
        {
            return string.IsNullOrEmpty(stringToConvert) ? string.Empty : CultureInfo.CurrentCulture.TextInfo.ToTitleCase(stringToConvert.ToLowerCase());
        }

        public static string ToLowerCase(this string stringToConvert)
        {
            return string.IsNullOrEmpty(stringToConvert) ? string.Empty : stringToConvert.ToLower();
        }

        public static string ToTrimCase(this string stringToConvert)
        {
            return string.IsNullOrEmpty(stringToConvert) ? string.Empty : stringToConvert.Trim();
        }

        public static string Reverse(this string source)
        {
            var charArray = new char[source.Length];
            var len = source.Length - 1;

            for (var i = 0; i <= len; i++)
                charArray[i] = source[len - i];

            return new string(charArray);
        }

        public static string StripLeadingEndingString(this string stringToProcess, string stringToStrip)
        {
            return string.IsNullOrEmpty(stringToProcess) ? string.Empty : stringToProcess.ReplaceString(stringToStrip, string.Empty, StringComparison.InvariantCultureIgnoreCase);
        }

        public static string LastPart(this string stringToProcess, string delimiter)
        {
            if (!string.IsNullOrEmpty(stringToProcess) && stringToProcess.Contains(delimiter))
                return stringToProcess.Substring(stringToProcess.IndexOf(delimiter, StringComparison.Ordinal) + 1,
                                                              stringToProcess.Length - stringToProcess.IndexOf(delimiter, StringComparison.Ordinal) - 1);

            return string.Empty;
        }

        public static string FirstPart(this string stringToProcess, string delimiter)
        {
            if (!string.IsNullOrEmpty(stringToProcess) && stringToProcess.Contains(delimiter))
                return stringToProcess.Substring(0, stringToProcess.IndexOf(delimiter, StringComparison.Ordinal));

            return string.Empty;
        }

        public static string Flatten(this List<string> stringList, char delimiter)
        {
            if (stringList == null)
                return string.Empty;

            if (stringList.Count == 0)
                return string.Empty;
            var output = stringList.Aggregate(string.Empty, (current, str) => current + (str + delimiter));
            return StringHelper.StripLeadingEndingChar(output, delimiter);
        }

        public static string EscapeSingleQuote(this string strVar)
        {
            return string.IsNullOrEmpty(strVar) ? strVar : strVar.Replace("'", "\\'");
        }

        public static bool Contains(this string stringToProcess, string stringToCheck, StringComparison comparison)
        {
            if (string.IsNullOrEmpty(stringToProcess))
                return false;

            return stringToProcess.IndexOf(stringToCheck, comparison) >= 0;
        }

        public static string RemoveSpecialCharacters(this string input)
        {
            if (string.IsNullOrEmpty(input))
                return null;

            input = input.Replace("-", " ");
            var r = new Regex("(?:[^a-z0-9 ]|(?<=['\"])s)", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);
            return r.Replace(input, String.Empty);
        }

        public static string ReplaceSpecialCharacters(this string str, string replace)
        {
            if (string.IsNullOrEmpty(str))
                return string.Empty;
            var text = Regex.Replace(str, @"[^0-9a-z]", replace, RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);
            text = Regex.Replace(text, @"\" + replace + "+", replace);
            return text.TrimEnd('-');
        }

        public static string GetPhoneNumber(this string phone)
        {
            try
            {
                return phone.Replace("(", "").Replace(")", "").Replace("-", "").Trim().Substring(0, 10);
            }
            catch (Exception)
            {

                return "";
            }
        }

        public static string FormatPhone(this string input)
        {
            return StringHelper.FormatPhone(input);
        }

        public static bool EqualNullSaveLowercase(this string source, string toCompare)
        {
            var s = source ?? string.Empty;
            var cmp = toCompare ?? string.Empty;

            return String.Equals(s, cmp, StringComparison.InvariantCultureIgnoreCase);
        }


    }
}
