﻿using System;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace Nhs.Utility.Common
{
    public static class XmlSerialize
    {
        private static XmlDocument Clean(XmlDocument doc)
        {
            doc.LoadXml(Clean(doc.OuterXml));
            return doc;
        }

        private static string Clean(string xml)
        {
            xml = XElement.Parse(xml).ToString();
            var doc=new XmlDocument();
            doc.LoadXml(xml);

            XmlNode first = doc.FirstChild;
            if (first != null && first.Attributes != null)
            {
                XmlAttribute a = first.Attributes["xmlns:xsd"];
                if (a != null) { first.Attributes.Remove(a); }
                a = first.Attributes["xmlns:xsi"];
                if (a != null) { first.Attributes.Remove(a); }
            }
            xml = XElement.Parse(doc.OuterXml).ToString();
            return xml;
        }

        #region Serialize
        public static void SerializeToXmlFile<T>(string path, T source) where T : new()
        {
            var formatter = new XmlSerializer(typeof(T));

            if (File.Exists(path))
                File.Delete(path);

            using (FileStream stream = File.Create(path))
                formatter.Serialize(stream, source);
        }
    
        public static XmlDocument ToSerialize<T>(this object objecto) where T : new()
        {
            return Serialize<T>(objecto);
        }

        public static XmlDocument Serialize<T>(Object objecto) where T : new()
        {
            var xml = StringSerialize<T>(objecto);
            var doc = new XmlDocument {PreserveWhitespace = true};
            doc.LoadXml(xml);
            //doc = Clean(doc);
            return doc;
        }

        public static TextWriter ToWriterSerialize<T>(this object objecto) where T : new()
        {
            return WriterSerialize<T>(objecto);
        }

        public static TextWriter WriterSerialize<T>(Object objecto) where T : new()
        {
            var w = new StringWriter();
            var s = new XmlSerializer(typeof(T));
            s.Serialize(w, objecto);
            w.Flush();
            return w;

        }
    
        public static String ToStringSerialize<T>(this object objecto) where T : new()
        {
            return StringSerialize<T>(objecto);
        }

        public static String StringSerialize<T>(Object objecto) where T : new()
        {
            TextWriter w = WriterSerialize<T>(objecto);
            var xml = w.ToString();
            w.Close();
            return Clean(xml.Trim());
        }
        #endregion

        #region Deserialize
    
        public static T Deserialize<T>(String xml) where T : new()
        {
            TextReader reader = new StringReader(xml);
            return Deserialize<T>(reader);
        }

        public static T Deserialize<T>(XmlDocument doc) where T : new()
        {
            TextReader reader = new StringReader(doc.OuterXml);
            return Deserialize<T>(reader);
        }

        public static T Deserialize<T>(TextReader reader) where T : new()
        {
            var ser = new XmlSerializer(typeof(T));
            var newobj = (T)ser.Deserialize(reader);
            reader.Close();
            return newobj;
        }

        public static T FromXml<T>(String xml) where T : new()
        {
            var ser = new XmlSerializer(typeof(T));
            var xmlReader = new XmlTextReader(new StringReader(xml));
            var newobj = (T)ser.Deserialize(xmlReader);
            xmlReader.Close();
            return newobj;
        }

        public static T DeserializeFromXmlFile<T>(String path) where T : new()
        {
            T newobj;

            if (String.IsNullOrEmpty(path))
                throw new NullReferenceException("The 'path' argument cannot be empty.");

            if (!File.Exists(path))
                throw new FileNotFoundException(String.Format("The file path is invalid. {{{0}}}", path));

            // Create a Stream
            using (FileStream stream = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                // Serialize to Stream
                var xformatter = new XmlSerializer(typeof(T));
                newobj = (T)xformatter.Deserialize(stream);
            }

            return newobj;
        }
        #endregion
    }
}