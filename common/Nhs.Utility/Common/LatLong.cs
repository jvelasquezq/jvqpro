using System;
using System.Collections.Generic;
using System.Linq;

namespace Nhs.Utility.Common
{
    [Serializable]
    public class LatLong
    {
        private const double EARTH_RADIUS = 3963;  // Earths Radius in Miles. 
        private double _latitude;
        private double _longitude;

        public double Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }

        public double Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }

        public LatLong()
        {
        }

        public LatLong(double latitude, double longitude)
        {
            _latitude = latitude;
            _longitude = longitude;
        }


        public LatLong(decimal latitude, decimal longitude)
        {
            _latitude = latitude.ToType<double>();
            _longitude = longitude.ToType<double>();
        }

        public double ArcDistance(LatLong latLong)
        {
            return ArcDistance(latLong.Latitude,latLong.Longitude);
        }

        public double ArcDistance(double latitude, double longitude)
        {
            var p1Latitude = DegreesToRadians(_latitude);
            var p1Longitude = DegreesToRadians(_longitude);
            var p2Latitude = DegreesToRadians(latitude);
            var p2Longitude = DegreesToRadians(longitude);

            var a = Math.Sin(p1Latitude);
            var b = Math.Sin(p2Latitude);
            var c = Math.Cos(p1Latitude);
            var d = Math.Cos(p2Latitude);
            var e = Math.Cos(p2Longitude - p1Longitude);

            return Math.Acos(a * b + c * d * e) * EARTH_RADIUS;
        }

        public LatLong GetCenter(LatLong latLong)
        {
            double latDiff = (Math.Abs(_latitude) - Math.Abs(latLong.Latitude)) / 2.0;
            double lngDiff = (Math.Abs(_longitude) - Math.Abs(latLong.Longitude)) / 2.0;

            double centerLat = _latitude - latDiff;
            double centerLng = _longitude + lngDiff;

            return new LatLong(centerLat, centerLng);
        }

        private double DegreesToRadians(double degrees)
        {
            return degrees * Math.PI / 180;
        }

        public static LatLong GetCenterPointFromListOfCoordinates(IEnumerable<LatLong> coords)
        {
            var total = coords.Count();
            if (total == 0)
                return new LatLong();

            double X = 0;
            double Y = 0;
            double Z = 0;

            foreach (var i in coords)
            {
                var lat = i.Latitude * Math.PI / 180;
                var lon = i.Longitude * Math.PI / 180;

                var x = Math.Cos(lat) * Math.Cos(lon);
                var y = Math.Cos(lat) * Math.Sin(lon);
                var z = Math.Sin(lat);

                X += x;
                Y += y;
                Z += z;
            }

            X = X / total;
            Y = Y / total;
            Z = Z / total;

            var @long = Math.Atan2(Y, X);
            var hyp = Math.Sqrt(X * X + Y * Y);
            var lati = Math.Atan2(Z, hyp);
            return new LatLong(lati * 180 / Math.PI, @long * 180 / Math.PI);
        }
    }
}