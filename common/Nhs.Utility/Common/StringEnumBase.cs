﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Nhs.Utility.Common
{
    public abstract class StringEnumBase<T> where T : StringEnumBase<T>
    {
        protected static List<T> EnumValues = new List<T>();

        public readonly int Value;
        public readonly string Key;
        public readonly object ExtraData;

        protected StringEnumBase(string key, int value, object extraData = null)
        {
            Key = key;
            Value = value;
            ExtraData = extraData;
            EnumValues.Add((T)this);
        }

        protected static ReadOnlyCollection<T> GetBaseValues()
        {
            return EnumValues.AsReadOnly();
        }

        protected static T GetBaseByKey(string key)
        {
            return EnumValues.FirstOrDefault(t => t.Key == key);
        }

        /// <summary>
        /// In this case use the Value as string 
        /// </summary>
        /// <returns>the Value property as a String</returns>
        public override string ToString()
        {
            return Key;
        }
    }        
}
