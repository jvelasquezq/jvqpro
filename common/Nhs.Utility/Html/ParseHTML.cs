using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Web;
using System.Net;
using Nhs.Utility.Common;
using Nhs.Utility.Html.HtmlElements;

namespace Nhs.Utility.Html
{
    /// <summary>
    /// Summary description for ParseHTML.
    /// </summary>
    public class ParseHTML
    {
        string _html = string.Empty;
        string _attributes = string.Empty;

        public ParseHTML(string html)
        {
            this._html = html;
        }
   
        /// <summary>
        /// Gives raw HTML between 'tag's given a 'tag'
        /// </summary>
        /// <param name="htmlElement"></param>
        /// <returns></returns>
            
        public string GetInnerHTML(IElement htmlElement, ref string attributes)
        {
            string innerHTML = this.GetInnerHTML(htmlElement);
            attributes = _attributes;
            return innerHTML;
        }

        public string GetInnerHTML(IElement htmlElement)
        {
            string htmlElementStartTag = "<" + htmlElement.Name; //removed end '>'; was omitting tags with attributes
            string htmlElementEndTag = "</" + htmlElement.Name + ">";
            string innerHTML = StringHelper.Between(_html, htmlElementStartTag, htmlElementEndTag);

            //check if starts with '>' - strip it - tags with no attributes
            if (innerHTML.StartsWith(">"))
                innerHTML = innerHTML.Remove(0, 1);
            else if(innerHTML.Length>0) //tag has attributes 
            {
                _attributes = innerHTML.Remove(innerHTML.IndexOf('>'), innerHTML.Length - innerHTML.IndexOf('>'));
                innerHTML = innerHTML.Remove(0, innerHTML.IndexOf('>') + 1);
            }
            return innerHTML;
        }

        /// <summary>
        /// Strips undesired HTML code from an string source.
        /// Some tags are allowed: 
        ///  - Paragraph <p></p>
        ///  - Bulleted/Unordered List<ul></ul>
        ///  - Numbered/Ordered List<ol></ol>
        ///  - List Item (inside bulleted or numbered list)<li></li>
        ///  - Bold<strong></strong> (recommended)or<b></b>
        ///  - Italics<em></em> (recommended)or <i></i>
        /// </summary>
        /// <param name="source">source text</param>
        /// <returns>source text without html tags</returns>
        public static string StripHTML(string source)
        {

            try
            {

                string result;

                // Remove HTML Development formatting
                // Replace line breaks with space
                // because browsers inserts space
                result = source.Replace("\r", " ");
                // Replace line breaks with space
                // because browsers inserts space
                result = result.Replace("\n", " ");
                // Remove step-formatting
                result = result.Replace("\t", string.Empty);
                // Remove repeating speces becuase browsers ignore them
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"( )+", " ");

                // Remove the header (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"<( )*head([^>])*>", "<head>",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"(<( )*(/)( )*head( )*>)", "</head>",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      "(<head>).*(</head>)", string.Empty,
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // remove all scripts (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"<( )*script([^>])*>", "<script>",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"(<( )*(/)( )*script( )*>)", "</script>",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                //result = System.Text.RegularExpressions.Regex.Replace(result, 
                //         @"(<script>)([^(<script>\.</script>)])*(</script>)",
                //         string.Empty, 
                //         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"(<script>).*(</script>)", string.Empty,
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // remove all styles (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"<( )*style([^>])*>", "<style>",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"(<( )*(/)( )*style( )*>)", "</style>",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      "(<style>).*(</style>)", string.Empty,
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert tabs in spaces of <td> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"<( )*td([^>])*>", "\t",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert line breaks in places of <BR> and <LI> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"<( )*br( )*>", "\r",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"<( )*li( )*>", "\r",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert line paragraphs (double line breaks) in place
                // if <P>, <DIV> and <TR> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"<( )*div([^>])*>", "\r\r",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"<( )*tr([^>])*>", "\r\r",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"<( )*p([^>])*>", "\r\r",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"<( )*/p([^>])*>", " \r\r ",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // Remove remaining tags like <a>, links, images,
                // comments etc - anything thats enclosed inside < >
                //////////////////result = System.Text.RegularExpressions.Regex.Replace(result,
                //////////////////         @"<[^>]*>", string.Empty,
                //////////////////         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                System.Text.RegularExpressions.Regex regex =
                    new System.Text.RegularExpressions.Regex(@"<(?![!/]?[ABIU][>\s])[^>]*>");
                System.Text.RegularExpressions.MatchCollection matches = regex.Matches(result);
                foreach (System.Text.RegularExpressions.Match match in matches)
                {
                    if (match.Value.Replace("/", "") == "<p>") continue;
                    if (match.Value.Replace("/", "") == "<ul>") continue;
                    if (match.Value.Replace("/", "") == "<ol>") continue;
                    if (match.Value.Replace("/", "") == "<li>") continue;
                    if (match.Value.Replace("/", "") == "<strong>") continue;
                    if (match.Value.Replace("/", "") == "<b>") continue;
                    if (match.Value.Replace("/", "") == "<em>") continue;
                    if (match.Value.Replace("/", "") == "<i>") continue;
                    if (match.Value.Replace("/", "") == "<br>") continue;
                    result = result.Replace(match.Value, "");
                }


                // replace special characters:
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"&nbsp;", " ",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"&bull;", " * ",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"&lsaquo;", "<",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"&rsaquo;", ">",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"&trade;", "(tm)",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"&frasl;", "/",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"&lt;", "<",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"&gt;", ">",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"&copy;", "(c)",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"&reg;", "(r)",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"&apos;", "'",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // Remove all others. More can be added, see
                // http://hotwired.lycos.com/webmonkey/reference/special_characters/
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"&(.{2,6});", string.Empty,
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // for testng
                //System.Text.RegularExpressions.Regex.Replace(result, 
                //       this.txtRegex.Text,string.Empty, 
                //       System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // make line breaking consistent
                result = result.Replace("\n", "\r");

                // Remove extra line breaks and tabs:
                // replace over 2 breaks with 2 and over 4 tabs with 4. 
                // Prepare first to remove any whitespaces inbetween
                // the escaped characters and remove redundant tabs inbetween linebreaks
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      "(\r)( )+(\r)", "\r\r",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      "(\t)( )+(\t)", "\t\t",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      "(\t)( )+(\r)", "\t\r",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      "(\r)( )+(\t)", "\r\t",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove redundant tabs
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      "(\r)(\t)+(\r)", "\r\r",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove multible tabs followind a linebreak with just one tab
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      "(\r)(\t)+", "\r\t",
                                                                      System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Initial replacement target string for linebreaks
                string breaks = "\r\r\r";
                // Initial replacement target string for tabs
                string tabs = "\t\t\t\t\t";
                for (int index = 0; index < result.Length; index++)
                {
                    result = result.Replace(breaks, "\r\r");
                    result = result.Replace(tabs, "\t\t\t\t");
                    breaks = breaks + "\r";
                    tabs = tabs + "\t";
                }

                if (result.StartsWith("&nbsp;"))
                {
                    result = result.Trim();
                }

                if (result.StartsWith(""))
                {
                    result = result.Trim();
                }

                if (result.StartsWith(" "))
                {
                    result = result.Trim();
                }


                // Thats it.
                return result;

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return source;
            }
        }

    }
}