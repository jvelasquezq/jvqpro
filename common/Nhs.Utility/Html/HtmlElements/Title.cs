using System;
using System.Collections.Generic;
using Nhs.Utility.Html.HtmlElements;

namespace Nhs.Utility.Html.HtmlElements
{
    public class TitleElement : IElement
    {
        private string _innerMarkup;
        private string _value;

        public string Name
        {
            get{return "title";}
        }

        public string InnerMarkup
        {
            get {return _innerMarkup; }
            set 
            {
                _value = string.Format("<{0}>{1}</{0}>", this.Name, value);
                _innerMarkup = value; 
            }
        }

        public string Value
        {
            get { return _value; }
        }

        public string Attributes
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }
    }
}