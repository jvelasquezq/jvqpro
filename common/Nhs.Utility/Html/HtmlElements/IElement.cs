using System;
using System.Collections.Generic;
using System.Text;

namespace Nhs.Utility.Html.HtmlElements
{
    public interface IElement
    {
        string Name{get;}
        string Value { get; }
        string InnerMarkup{get; set;}
        string Attributes { get; set;}
    }
}