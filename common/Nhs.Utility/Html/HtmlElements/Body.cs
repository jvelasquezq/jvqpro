using System;
using System.Collections.Generic;

namespace Nhs.Utility.Html.HtmlElements
{
    public class BodyElement : IElement
    {
        private string _innerMarkup;
        private string _value;
        private string _attributes;

        public string Name
        {
            get{return "body";}
        }
       
        public string InnerMarkup
        {
            get { return _innerMarkup; }

            set 
            {
                _innerMarkup = value;
                this.Value = string.Format("<{0}>{1}</{0}>", this.Name, value);
            }
        }

        public string Value
        {
            get { return _value; }
            set
            {
                _value = value;
            }
        }

        public string Attributes
        {
            get { return _attributes; }
            set { 
                _attributes = value;
                this.SetValue();
            }
        }

        private void SetValue()
        {
            this.Value = string.Format("<{0} {1}>{2}</{0}>", this.Name, this.Attributes.Trim(), _innerMarkup);
        }
    }
}