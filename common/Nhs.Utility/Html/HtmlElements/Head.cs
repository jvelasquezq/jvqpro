using System.Collections.Generic;

namespace Nhs.Utility.Html.HtmlElements
{
    public class HeadElement : IElement
    {
      
        private string _innerMarkup;
        private string _value;

        public string Name
        {
            get{return "head";}
        }


        public string InnerMarkup
        {
            get { return _innerMarkup; }
            set 
            {
                _value = string.Format("<{0}>{1}</{0}>", this.Name, value);
                _innerMarkup = value; 
            }
        }

        public string Value
        {
            get { return _value; }
            
        }

        public string Attributes
        {
            get
            {
                throw new System.Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new System.Exception("The method or operation is not implemented.");
            }
        }
    }
}