using System;
using System.Collections.Generic;
using System.Text;

namespace Nhs.Utility.Html
{
    public interface IAttributes
    {
        string Delimiter {get; set;}
        string Name { get; set;}
        string Value { get; set;}
    }
}