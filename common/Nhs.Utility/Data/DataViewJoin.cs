using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Nhs.Utility.Common
{
    public class DataViewJoin
    {
        private DataView _view1;
        private DataView _view2;
        private string _joinColumn1 = "";
        private string _joinColumn2 = "";
        private List<int> _view1MatchingRows = new List<int>();
        private List<int> _view2MatchingRows = new List<int>();

        public List<int> View1MatchingRows
        {
            get { return _view1MatchingRows; }
        }

        public List<int> View2MatchingRows
        {
            get { return _view2MatchingRows; }
        }        

        public DataViewJoin(DataView view1, DataView view2, string joinColumn1, string joinColumn2)
        {
            this._view1 = view1;
            this._view2 = view2;

            this._joinColumn1 = joinColumn1;
            this._joinColumn2 = joinColumn2;

            BuildJoinRows();
        }

        private void BuildJoinRows()
        {
            Hashtable view1Keys = new Hashtable();
            Hashtable view2Keys = new Hashtable();

            // Get distinct list of join column values from view 1.
            foreach (DataRowView row in _view1)
            {
                if (!view1Keys.Contains(row[_joinColumn1]))
                {
                    view1Keys.Add(row[_joinColumn1], null);
                }
            }

            // Get distict list of join column valus from view 2. 
            foreach (DataRowView row in _view2)
            {
                if (!view2Keys.Contains(row[_joinColumn2]))
                {
                    view2Keys.Add(row[_joinColumn2], null);
                }
            }

            // Create list of rows from view 1 that have matching keys from view 2. 
            for (int i = 0; i < _view1.Count; i++)
            {
                if (view2Keys.Contains(_view1[i][_joinColumn1]))
                {
                    _view1MatchingRows.Add(i);
                }
                else
                {
                    // key does not exist in view 2,  remove key. 
                    view1Keys.Remove(_view1[i][_joinColumn1]);
                }
            }

            // Create list of rows from view2 that have matching keys in view 1. 
            // Note, at this point, view 1 keys only contain the ones that match view 2. 
            for (int i = 0; i < _view2.Count; i++)
            {
                if (view1Keys.Contains(_view2[i][_joinColumn2]))
                {
                    _view2MatchingRows.Add(i);
                }

            }
        }
    }
}