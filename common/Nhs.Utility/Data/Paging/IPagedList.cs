﻿using System.Collections;
using System.Collections.Generic;

namespace Nhs.Utility.Data.Paging
{
    public interface IPagedList : IEnumerable
    {
        int PageCount { get; }
        int TotalItemCount { get; }
        int PageIndex { get; }
        int PageNumber { get; }
        int PageSize { get; }
        bool HasPreviousPage { get; }
        bool HasNextPage { get; }
        bool IsFirstPage { get; }
        bool IsLastPage { get; }
    }

    public interface IPagedList<T> : IPagedList, IEnumerable<T>
    {

    }
}
