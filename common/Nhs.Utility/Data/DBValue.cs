using System;

namespace Nhs.Utility.Common
{
    public class DBValue
    {
        public static string GetString(object dbValue)
        {
            return dbValue != DBNull.Value ? dbValue.ToString() : string.Empty;
        }

        public static string GetFormattedString(object dbValue, string formatString)
        {
            return dbValue != DBNull.Value ? String.Format(formatString, dbValue) : "";
        }

        public static char GetChar(object dbValue)
        {
            return dbValue != DBNull.Value ? Char.Parse(dbValue.ToString()) : char.MinValue;
        }

        public static Decimal GetDecimal(object dbValue)
        {
            if (dbValue != DBNull.Value)
            {
                decimal result;
                Decimal.TryParse(dbValue.ToString(), out result);
                return result;
            }
            
            return 0;
        }

        public static DateTime GetDateTime(object dbValue)
        {
            if (dbValue != DBNull.Value)
            {
                try
                {
                    return DateTime.Parse(dbValue.ToString());
                }
                catch
                {
                    return DateTime.MinValue;
                }
            }
            return DateTime.MinValue;
        }

        public static int GetInt(object dbValue)
        {
            if (dbValue != DBNull.Value)
            {
                try
                {
                    return Convert.ToInt32(dbValue);
                }
                catch
                {
                    try
                    {
                        return Int32.Parse(dbValue.ToString().Trim());
                    }
                    catch
                    {
                        return 0;
                    }
                }
            }
            return 0;
        }

        public static bool GetBool(object dbValue)
        {
            if (dbValue != DBNull.Value)
            {
                try
                {
                    return Convert.ToBoolean(dbValue);
                }
                catch
                {
                    return false;
                }
            }
            return false;
        }

        public static object NegativeIntToDbNull(int value)
        {
            return value < 0 ? (object) DBNull.Value : value;
        }

        public static object EmptyStringToNull(string value)
        {
            return value == string.Empty ? (object) DBNull.Value : value;
        }

        public static double GetDouble(object dbValue)
        {
            if (dbValue != DBNull.Value)
            {
                try
                {
                    return Convert.ToDouble(dbValue.ToString());
                }
                catch
                {
                    return 0.0;
                }
            }
            return 0.0;
        }
    }
}