﻿using System;
using System.Web;
using System.Web.Caching;

namespace Nhs.Utility.Data
{
    public static class Caching
    {
        #region "Public Static Methods"

        public static void AddObjectToCache(object data, string cacheItemKey, TimeSpan timespan)
        {
            if (HttpRuntime.Cache != null)
                HttpRuntime.Cache.Insert(cacheItemKey, data, null, Cache.NoAbsoluteExpiration, timespan, CacheItemPriority.Normal, null);
        }

        /// <summary>
        /// Adds to cache for 24h
        /// </summary>
        /// <param name="data"></param>
        /// <param name="cacheItemKey"></param>
        public static void AddObjectToCache(object data, string cacheItemKey)
        {
            TimeSpan ts = new TimeSpan(24, 0, 0);
            AddObjectToCache(data, cacheItemKey, ts);
        }

        public static void AddObjectToCache(object data, string cacheItemKey, TimeSpan timespan, Cache cacheRef, CacheItemPriority priority)
        {
            cacheRef.Insert(cacheItemKey, data, null, Cache.NoAbsoluteExpiration, timespan, priority, null);
        }

        public static object GetObjectFromCache(string cacheItemKey)
        {
            return HttpRuntime.Cache != null ? HttpRuntime.Cache[cacheItemKey] : null;
        }

        public static object GetObjectFromCache(string cacheItemKey, Cache cacheRef)
        {
            return cacheRef != null ? cacheRef[cacheItemKey] : null;
        }

        public static void RemoveObjectFromCache(string cacheItemKey, Cache cacheRef)
        {
            if (cacheRef != null && cacheRef[cacheItemKey] != null)
                cacheRef.Remove(cacheItemKey);
        }
        #endregion
    }
}