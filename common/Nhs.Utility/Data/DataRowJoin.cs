using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Nhs.Utility.Common
{
    public class DataRowJoin
    {
        DataRow[] _rowsTable1;
        DataRow[] _rowsTable2;
        private int _joinColIndex1;
        private int _joinColIndex2;
        List<DataRow> _joinedRows1 = new List<DataRow>();
        List<DataRow> _joinedRows2 = new List<DataRow>();

        public List<DataRow> JoinedRows1
        {
            get { return _joinedRows1; }
        }

        public List<DataRow> JoinedRows2
        {
            get { return _joinedRows2; }
        }

        public DataRowJoin(DataRow[] rowsTable1, DataRow[] rowsTable2, int joinColIndex1, int joinColIndex2)
        {
            this._rowsTable1 = rowsTable1;
            this._rowsTable2 = rowsTable2;

            this._joinColIndex1 = joinColIndex1;
            this._joinColIndex2 = joinColIndex2;

            BuildJoinRows();
        }

        private void BuildJoinRows()
        {
            Hashtable table1Keys = new Hashtable();
            Hashtable table2Keys = new Hashtable();

            // Find all distinct join column values for table 1. 
            for (int i = 0; i < _rowsTable1.Length; i++)
            {
                if (!table1Keys.Contains(_rowsTable1[i][_joinColIndex1]))
                {
                    table1Keys.Add(_rowsTable1[i][_joinColIndex1], null);
                }
            }

            // Add rows from table 2 that have matching join column values in table 1
            // to the output list. 
            for (int i = 0; i < _rowsTable2.Length; i++)
            {
                if (table1Keys.Contains(_rowsTable2[i][_joinColIndex2]))
                {
                    // Add this row to the output. 
                    _joinedRows2.Add(_rowsTable2[i]);

                    if (!table2Keys.Contains(_rowsTable2[i][_joinColIndex2]))
                    {
                        table2Keys.Add(_rowsTable2[i][_joinColIndex2], null);
                    }
                }
            }

            for (int i = 0; i < _rowsTable1.Length; i++)
            {
                if (table2Keys.Contains(_rowsTable1[i][_joinColIndex1]))
                {
                    // Add this row to the output. 
                    _joinedRows1.Add(_rowsTable1[i]);
                }

            }
        }
    }
}