using System;
using System.Data;
using System.Web;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Exceptions;
using Nhs.Library.Web;
using Nhs.Mvc.Data.Repositories;
using Nhs.Utility.Common;
using Nhs.Mvc.Domain.Model.Profiles;

namespace Nhs.Library.Business
{
    [Serializable]
    public class Profile : IProfile
    {
        #region "Members"
        private string _userID = string.Empty;
        private string _genericGUID = string.Empty;        
        private int _partnerID;
        private string _ssoId = string.Empty;
        private string _logonName = string.Empty;
        private string _firstName = string.Empty;
        private string _lastName = string.Empty;
        private string _email = string.Empty;
        private string _address1 = string.Empty;
        private string _address2 = string.Empty;
        private DateTime _dateRegistered = DateTime.MinValue;
        private DateTime _dateChanged = DateTime.MinValue;
        private string _middleName = string.Empty;
        private string _metro = string.Empty;
        private string _password = string.Empty;
        private string _state = string.Empty;
        private string _city = string.Empty;
        private string _dayPhone = string.Empty;
        private string _dayPhoneExt = string.Empty;
        private string _evePhone = string.Empty;
        private string _evePhoneExt = string.Empty;
        private string _postalCode = string.Empty;
        private string _fax = string.Empty;
        private string _mailList = string.Empty;
        private string _referrerName = string.Empty;
        private string _userAccountState = string.Empty;
        private string _leadOptIn = string.Empty;
        private string _marketOptIn = string.Empty;
        private string _regMetro = string.Empty;
        private string _regPrefer = string.Empty;
        private string _regState = string.Empty;
        private string _regPrice = string.Empty;
        private string _age = string.Empty;
        private string _agencyName = string.Empty;
        private string _realEstateLicense = string.Empty;
        private bool _regBOYLInterest;
        private bool _regActiveAdultInterest;
        private string _regCity = string.Empty;
        private bool _regComingSoonInterest;
        private DateTime _boxRequestedDate = DateTime.MinValue;
        private bool _weeklyNotifierOptIn;
        private DateTime _initialMatchDate = DateTime.MinValue;
        private WebActors _actorStatus = WebActors.GuestUser;
        private IPlanner _planner;
        //move in date default value is -1, because 0 is for type "inmediate"
        private int _moveInDate = -1;
        private int _financePref;
        private int _reason;

        #endregion

        #region "Properties"

        public string UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }

        public string GenericGUID
        {
            get { return _genericGUID; }
            set { _genericGUID = value; }
        }
        
        public string LogonName
        {
            get { return _logonName; }
            set { _logonName = value;
                  _email = value;    }
        }
        public string SsoId
        {
            get { return _ssoId; }
            set { _ssoId = value; }
        }

        public bool? HomeWeeklyOptIn { get; set; }

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        public int PartnerID
        {
            get { return _partnerID; }
            set { _partnerID = value; }
        }

        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                CookieManager.EncEmail = CryptoHelper.Encrypt(value, CookieConst.EncEmail);
            }
        }
        public string Address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }
        public string Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }
        public DateTime DateRegistered
        {
            get { return _dateRegistered; }
            set { _dateRegistered = value; }
        }
        public DateTime DateChanged
        {
            get { return _dateChanged; }
            set { _dateChanged = value; }
        }
        public string MiddleName
        {
            get { return _middleName; }
            set { _middleName = value; }
        }
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }
        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
        public string Metro
        {
            get { return _metro; }
            set { _metro = value; }
        }
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        public string DayPhone
        {
            get { return _dayPhone; }
            set { _dayPhone = value; }
        }
        public string DayPhoneExt
        {
            get { return _dayPhoneExt; }
            set { _dayPhoneExt = value; }
        }
        public string EvePhone
        {
            get { return _evePhone; }
            set { _evePhone = value; }
        }
        public string EvePhoneExt
        {
            get { return _evePhoneExt; }
            set { _evePhoneExt = value; }
        }
        public string PostalCode
        {
            get { return _postalCode; }
            set { _postalCode = value; }
        }
        public string Fax
        {
            get { return _fax; }
            set { _fax = value; }
        }
        public string MailList
        {
            get { return _mailList; }
            set { _mailList = value; }
        }
        public string ReferrerName
        {
            get { return _referrerName; }
            set { _referrerName = value; }
        }
        public string UserAccountState
        {
            get { return _userAccountState; }
            set { _userAccountState = value; }
        }
        public string LeadOptIn
        {
            get { return _leadOptIn; }
            set { _leadOptIn = value; }
        }
        public string MarketOptIn
        {
            get { return _marketOptIn; }
            set { _marketOptIn = value; }
        }
        public string RegMetro
        {
            get { return _regMetro; }
            set { _regMetro = value; }
        }
        public string RegCity
        {
            get { return _regCity; }
            set { _regCity = value; }
        }
        public string RegPrefer
        {
            get { return _regPrefer; }
            set { _regPrefer = value; }
        }
        public string RegState
        {
            get { return _regState; }
            set { _regState = value; }
        }
        public string RegPrice
        {
            get { return _regPrice; }
            set { _regPrice = value; }
        }
        public string Age
        {
            get { return _age; }
            set { _age = value; }
        }
        public bool RegBOYLInterest
        {
            get { return _regBOYLInterest; }
            set { _regBOYLInterest = value; }
        }
        public bool RegActiveAdultInterest
        {
            get { return _regActiveAdultInterest; }
            set { _regActiveAdultInterest = value; }
        }
        public bool RegComingSoonInterest
        {
            get { return _regComingSoonInterest; }
            set { _regComingSoonInterest = value; }
        }
        public DateTime BoxRequestedDate
        {
            get { return _boxRequestedDate; }
            set { _boxRequestedDate = value; }
        }
        public bool WeeklyNotifierOptIn
        {
            get { return _weeklyNotifierOptIn; }
            set { _weeklyNotifierOptIn = value; }
        }
        public DateTime InitialMatchDate
        {
            get { return _initialMatchDate; }
            set { _initialMatchDate = value; }
        }
        public WebActors ActorStatus
        {
            get { return _actorStatus; }
            set { _actorStatus = value; }
        }
        public IPlanner Planner
        {
            get
            {
                if (_planner != null)
                {
                    return _planner;
                }
                else
                {
                    _planner = new Planner(_userID);
                    _planner.DataBind(_userID);
                    return _planner;
                }
            }
        }

        public int MoveInDate
        {
            get { return _moveInDate; }
            set { _moveInDate = value; }
        }

        public int FinancePreference
        {
            get { return _financePref; }
            set { _financePref = value; }
        }

        public int Reason
        {
            get { return _reason; }
            set { _reason = value; }
        }

        public string AgencyName
        {
            get { return _agencyName; }
            set { _agencyName = value; }
        }

        public string RealEstateLicense
        {
            get { return _realEstateLicense; }
            set { _realEstateLicense = value; }
        }
        #endregion

        #region "Constructor"
        public Profile(string ssoId, int partnerId)
        {
            _ssoId = ssoId;
            _partnerID = partnerId;
        }

        public Profile(int partnerId, string emailAddress)
        {
            _logonName = emailAddress;
            _partnerID = partnerId;
        }
        public Profile(int partnerId)
        {
            // Create unique user id for this profile. 
            _userID = "{" + Guid.NewGuid().ToString() + "}";
            _partnerID = partnerId;
        }
        public Profile(string userId)
        {
            _userID = userId;
        }


        public Profile()
        {
            // serialization purposes
        }
        #endregion

        #region "Public Methods"

        public void SignIn(string logonName, string password)
        {
            // Get data for specified logon name. 
            DataTable data = DataProvider.Current.SignIn(_partnerID, logonName.Trim());


            if (data.Rows.Count == 1)
            {
                // User name is valid, get password returned from dataset. 
                string dbPassword = DBValue.GetString(data.Rows[0]["u_user_security_password"]);

                // Verify password. 
                if (dbPassword == password)
                {
                    // User name and password are valid.  Get unique user id and bind
                    // profile data. 
                    string userId = DBValue.GetString(data.Rows[0]["g_user_id"]);
                    DataBind(userId);

                    // Promote to active user. 
                    _actorStatus = WebActors.ActiveUser;
                    WriteProfileCookie();
                }
                else
                {
                    // Password invalid.  
                    throw new InvalidPasswordException(_partnerID>10000?"Lo siento, la contraseña que ha introducido no es correcto. Por favor, compruebe la ortografía y vuelva a intentarlo.":"Sorry, the password you entered is not correct. Please check your spelling and try again.");
                }
            }
            else
            {
                // Logon name does not exist. 
                throw new UnknownUserException(_partnerID > 10000 ? "Lo sentimos, la dirección de correo electrónico que ha introducido no coincide con ninguna cuenta en los archivos. Por favor, inténtelo de nuevo." : "Sorry, the email address you entered did not match any accounts on file. Please try again.");
            }
        }

        public void WriteProfileCookie()
        {
            if (HttpContext.Current != null)
            {
                HttpCookie ProfileCookie = new HttpCookie("Profile_" + _partnerID);
                ProfileCookie.Value = _userID;
                ProfileCookie.Expires = new DateTime(2025, 1, 1, 1, 1, 1, 1);
                HttpContext.Current.Response.Cookies.Add(ProfileCookie);
            }
        }
        public void ClearProfile()
        {
             _userID = "{" + Guid.NewGuid().ToString() + "}";
             _logonName = string.Empty;
             _firstName = string.Empty;
             _lastName = string.Empty;
             _email = string.Empty;
             _address1 = string.Empty;
             _address2 = string.Empty;
             _dateRegistered = DateTime.MinValue;
             _dateChanged = DateTime.MinValue;
             _middleName = string.Empty;
             _password = string.Empty;
             _state = string.Empty;
             _metro = string.Empty;
             _city = string.Empty;
             _dayPhone = string.Empty;
             _dayPhoneExt = string.Empty;
             _evePhone = string.Empty;
             _evePhoneExt = string.Empty;
             _postalCode = string.Empty;
             _fax = string.Empty;
             _mailList = string.Empty;
             _referrerName = string.Empty;
             _userAccountState = string.Empty;
             _leadOptIn = string.Empty;
             _marketOptIn = string.Empty;
             _regMetro = string.Empty;
             _regCity = string.Empty;
             _regPrefer = string.Empty;
             _regState = string.Empty;
             _regPrice = string.Empty;
             _age = string.Empty;
             _regBOYLInterest = false;
             _regActiveAdultInterest = false;
             _regComingSoonInterest = false;
             _boxRequestedDate = DateTime.MinValue;
             _weeklyNotifierOptIn = false;
             _initialMatchDate = DateTime.MinValue;
             _actorStatus = WebActors.GuestUser;
             _agencyName = string.Empty;
             _realEstateLicense = string.Empty;
             _planner = null;

            // Clear profile cookie.
             if (HttpContext.Current.Response.Cookies["Profile_" + _partnerID] != null)
             {
                 HttpContext.Current.Response.Cookies["Profile_" + _partnerID].Expires = DateTime.Now;
             }
        }
        public void SaveProfile()
        {
            DataProvider.Current.SetProfile(UserID, LogonName, FirstName, LastName, Password, 
                PartnerID, DateRegistered, MiddleName, Address1, Address2, State, City, DayPhone, 
                EvePhone, Fax, PostalCode, MailList, ReferrerName, MarketOptIn, RegMetro, RegPrefer, 
                DayPhoneExt, EvePhoneExt, LeadOptIn, RegCity, RegPrefer, RegState, Age, RegBOYLInterest, 
                RegComingSoonInterest, RegActiveAdultInterest, MoveInDate, FinancePreference, Reason, 
                BoxRequestedDate, WeeklyNotifierOptIn, InitialMatchDate, AgencyName, RealEstateLicense, SsoId, HomeWeeklyOptIn);
            
        }

        public IProfile SetValuesForBrochureTable(IProfile oldProfile, AgentBrochureTemplate agentTemplate)
        {
            var res = oldProfile;

            if (oldProfile.FirstName != FirstName)
                res.FirstName = FirstName;
            else
            {
                if (agentTemplate != null)
                    res.FirstName = agentTemplate.FirstName;
            }
            if (oldProfile.LastName != LastName)
                res.LastName = LastName;
            else
            {
                if (agentTemplate != null)
                    res.LastName = agentTemplate.LastName;
            }
            if (oldProfile.Email != Email)
                res.Email = Email;
            else
            {
                if (agentTemplate != null)
                    res.Email = agentTemplate.Email;
            }
            if (oldProfile.LogonName != LogonName)
                res.LogonName = LogonName;
            else
            {
                if (agentTemplate != null)
                    res.LogonName = agentTemplate.Email;
            }
            if (oldProfile.PostalCode != PostalCode)
                res.PostalCode = PostalCode;
            else
            {
                if (agentTemplate != null)
                    res.PostalCode = agentTemplate.PostalCode;
            }
            if (oldProfile.City != City)
                res.City = City;
            else
            {
                if (agentTemplate != null)
                    res.City = agentTemplate.City;
            }
            if (oldProfile.State != State)
                res.State = State;
            else
            {
                if (agentTemplate != null)
                    res.State = agentTemplate.State;
            }
            if (oldProfile.DayPhone != DayPhone)
                res.DayPhone = DayPhone;
            else
            {
                if (agentTemplate != null)
                    res.DayPhone = agentTemplate.OfficePhone;
            }
            if (oldProfile.EvePhone != EvePhone)
                res.EvePhone = EvePhone;
            else
            {
                if (agentTemplate != null)
                    res.EvePhone = agentTemplate.MobilePhone;
            }
            if (oldProfile.AgencyName != AgencyName)
                res.AgencyName = AgencyName;
            else
            {
                if (agentTemplate != null)
                    res.AgencyName = agentTemplate.AgencyName;
            }
            if (oldProfile.RealEstateLicense != RealEstateLicense)
                res.RealEstateLicense = RealEstateLicense;            


            return res;
        }



        public void DataBind(string userId)
        {
            DataTable profileData = DataProvider.Current.GetProfileByGUID(userId);
            DataBindProfile(profileData);
        }
        public void DataBindByLogonName()
        {
            DataTable profileData = DataProvider.Current.GetProfileByLogonName(_partnerID, _logonName);
            DataBindProfile(profileData);
        }
        public void DataBindBySsoId()
        {
            DataTable profileData = DataProvider.Current.GetProfileBySsoId(_ssoId, _partnerID);
            DataBindProfile(profileData);
        }

       public bool IsLoggedIn()
        {
            return UserSession.UserProfile.ActorStatus == WebActors.ActiveUser;
        }

        #endregion

        #region "Private Methods"


     
        private void DataBindProfile(DataTable profileData)
        {
            if (profileData.Rows.Count > 0)
            {
                // Profile data found.  Upgrade to passive user. 
                _actorStatus = WebActors.PassiveUser;

                // Populate local members with values from db. 
                DataRow row = profileData.Rows[0];
                _userID = DBValue.GetString(row[ProfileConst.UserID]);
                _logonName = DBValue.GetString(row[ProfileConst.LogonName]);
                _firstName = DBValue.GetString(row[ProfileConst.FirstName]);
                _lastName = DBValue.GetString(row[ProfileConst.LastName]);
                _partnerID = DBValue.GetInt(row[ProfileConst.PartnerID]);
                _email = DBValue.GetString(row[ProfileConst.Email]);
                _address1 = DBValue.GetString(row[ProfileConst.Address1]);
                _address2 = DBValue.GetString(row[ProfileConst.Address2]);
                _dateRegistered = DBValue.GetDateTime(row[ProfileConst.DateRegistered]);
                _dateChanged = DBValue.GetDateTime(row[ProfileConst.DateChanged]);
                _middleName = DBValue.GetString(row[ProfileConst.MiddleName]);
                _password = DBValue.GetString(row[ProfileConst.Password]);
                _state = DBValue.GetString(row[ProfileConst.State]);
                _metro = string.Empty;
                _city = DBValue.GetString(row[ProfileConst.City]);
                _postalCode = DBValue.GetString(row[ProfileConst.PostalCode]);
                _dayPhone = DBValue.GetString(row[ProfileConst.DayPhone]);
                _dayPhoneExt = DBValue.GetString(row[ProfileConst.DayPhoneExt]);
                _evePhone = DBValue.GetString(row[ProfileConst.EvePhone]);
                _evePhoneExt = DBValue.GetString(row[ProfileConst.EvePhoneExt]);
                _fax = DBValue.GetString(row[ProfileConst.Fax]);
                _mailList = DBValue.GetString(row[ProfileConst.MailList]);
                _referrerName = DBValue.GetString(row[ProfileConst.ReferrerName]);
                _userAccountState = DBValue.GetString(row[ProfileConst.UserAccountState]);
                _leadOptIn = DBValue.GetString(row[ProfileConst.LeadOptIn]);
                _marketOptIn = DBValue.GetString(row[ProfileConst.MarketOptIn]);
                _regMetro = DBValue.GetString(row[ProfileConst.RegMetro]);
                _regCity = DBValue.GetString(row[ProfileConst.RegCity]);
                _regPrefer = DBValue.GetString(row[ProfileConst.RegPrefer]);
                _regState = DBValue.GetString(row[ProfileConst.RegState]);
                _regPrice = DBValue.GetString(row[ProfileConst.RegPrice]);
                _age = DBValue.GetString(row[ProfileConst.Age]);
                _regBOYLInterest = DBValue.GetBool(row[ProfileConst.RegBOYLInterest]);
                _regActiveAdultInterest = DBValue.GetBool(row[ProfileConst.RegActiveAdultInterest]);
                _regComingSoonInterest = DBValue.GetBool(row[ProfileConst.RegComingSoonInterest]);
                _boxRequestedDate = DBValue.GetDateTime(row[ProfileConst.BoxRequestedDate]);
                _weeklyNotifierOptIn = DBValue.GetBool(row[ProfileConst.WeeklyNotifierOptIn]);
                _address1 = DBValue.GetString(row[ProfileConst.Address1]);
                _initialMatchDate = DBValue.GetDateTime(row[ProfileConst.InitialMatchDate]);
                _planner = new Planner(_userID);
                _planner.DataBind(_userID);

                _moveInDate = DBValue.GetInt(row[ProfileConst.MoveInDateId]);
                _financePref = DBValue.GetInt(row[ProfileConst.FinancePreferenceId]);
                _reason = DBValue.GetInt(row[ProfileConst.MovingReasonId]);
                _agencyName = DBValue.GetString(row[ProfileConst.AgencyName]);
                _realEstateLicense = DBValue.GetString(row[ProfileConst.RealEstateLicense]);

            }
            else
            {
                // No data found.  Defult to new profile. 
                _userID = "{" + Guid.NewGuid().ToString() + "}";
                _partnerID = Configuration.PartnerId;
            }
        }
        #endregion
       
    }
}
