﻿
namespace Nhs.Library.Constants.Enums
{
    public enum SeoDetailPagesTypeEnum : byte
    {
        Default = 0,
        ComDetail,
        SpecDetail,
        PlanDetail
    }
}
