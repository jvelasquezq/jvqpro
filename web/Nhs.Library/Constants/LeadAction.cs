namespace Nhs.Library.Constants
{
    public class LeadAction
    {
        public const string RequestInfo = "ri";
        public const string ContactBuilder = "cb";
        public const string FreeBrochure = "fb";
        public const string RequestApointment = "ra";
        public const string CommRequestApointment = "cdra";
        public const string HomeRequestApointment = "hmra";
        public const string RequestPromotion = "rp";
        public const string EmailFreeBrochure = "emfb";
        public const string QuickMoveIn = "qmi";
        public const string HotHome = "hh";
        public const string QuickConnect = "QCONN";
        public const string Recommended = "reco";

        public const string HovFreeBrochure = "hovfb";
        public const string HovRequestApointment = "hovra";
        public const string HovRequestPromotion = "hovrp";
        public const string HovQuickMoveIn = "hovqmi";

        public const string CaRequestInfo = "cari";
        public const string CaContactBuilder = "cacb";
        public const string CaFreeBrochure = "cafb";
        public const string CaRequestApointment = "cara";
        public const string CaRequestPromotion = "carp";
        public const string CaRequestBuilderInfo = "carbi";
        public const string CaQuickMoveIn = "caqmi";
        public const string CaHotHome = "cahh";

        public const string SearchAlert = "srcalrt";
        
        public const string FreeBrochureSimpleModal = "fb-sim";
        public const string FreeBrochureReturningUser = "fb-ru";
        public const string RecommendedReturningUser = "recoru";
        public const string RecommendedModal = "recomodal";
        public const string RecommendedOnPage = "reonpage";
        public const string RecommendedSimpleModal = "recsimmod";
        public const string RecommendedSimpleModalDirect = "recsimdir";
        public const string RecommendedThankYouPage = "rectypage";
        public const string ThankYouLead = "tyl";

        public const string AgentCompensation = "agcom";
    }
}
