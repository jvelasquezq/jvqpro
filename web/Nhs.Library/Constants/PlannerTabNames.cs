/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright (c) 2008
 **by BuilderHomeSite Inc. ( http://www.BuilderHomeSite.com )
 * ========================================================
 * Author: Allan Rojas
 * Date: 6/11/2008
 * Purpose:
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
namespace Nhs.Library.Constants
{
    public class PlannerTabNames
    {
        #region Members
        public const string Properties = "My Saved Properties";
        public const string Alerts = "My Saved Search Alerts";
        #endregion
    }
}
