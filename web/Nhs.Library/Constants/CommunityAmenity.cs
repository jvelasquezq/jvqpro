/*
 **NewHomeSource = http://www.NewHomeSource.com
 **Copyright (c) 2001=2006
 **by BuilderHomeSite Inc. ( http://www.BuilderHomeSite.com )
 * ========================================================
 * Author: RMenon
 * Date: 10/23/2006 17:44:03
 * Purpose:
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
namespace Nhs.Library.Constants
{
    public class CommunityAmenity
    {
        #region Members
        public const int GolfCourse = 2;
        public const int Tennis = 3;
        public const int Pool = 4;
        public const int Playground = 5;
        public const int Greenbelt = 6;
        public const int Views = 9;
        public const int Park = 10;
        public const int Trails = 11;
        public const int Lake = 12;
        public const int Pond = 13;
        public const int Waterfrontlots = 14;
        public const int Volleyball = 15;
        public const int Basketball = 16;
        public const int Soccer = 17;
        public const int Baseball = 18;
        public const int Clubhouse = 19;
        public const int HOA = 20;
        public const int Marina = 21;
        public const int GroundsCare = 22;
        public const int Communitycenter = 23;
        public const int Security = 24;
        public const int Nature = 25;
        public const int Other = 26;
        public const int Shopping = 27;
        public const int Medical = 28;
        public const int Beach = 30;

        public const string Waterfront = "WF";
        public const string CondoTownHome = "CT";
        public const string GolfCource = "GC";

        #endregion
     
    }
}
