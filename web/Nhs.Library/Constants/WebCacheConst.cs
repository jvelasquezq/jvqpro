namespace Nhs.Library.Constants
{
    public class WebCacheConst
    {
        public static string PartnerCanonicals = "PartnerCanonicals";
        public const string DefaultIndex = "NHS";
        public const int DefaultHours = 24;
        public const int DefaultMins = 60;
        public const int DefaultSeconds = 60;

        public const string WebCacheKeyPrefix = "WebData_";

        public const string GlobalsPrefix = "GLOBALS_";
        public const string MetaTagDataKey = "MetaTagDataKey";

        public const string FeaturedSpotlightKey = "FeaturedSpotlight_";
        public const string LocationsIndex = "LocationsIndex_";
        public const string PartnerInfo = "PartnerInfo_";
        public const string FeaturedListings = "FeaturedListings_";
        public const string PartnerMarkets = "PartnerMarkets_";
        public const string SyntheticGeoLocations = "SyntheticGeoLocations_";
        public const string ZipCodeByPartner = "ZipCodeByPartner_";

        public const string BoylResult = "BoylResults";
        public const string AllStatesList = "AllStatesList";
        public const string AllStatesWithCoodinatesList = "AllStatesWithCoordinatesList";
        public const string PartnerStates = "PartnerStates_";
        public const string BuildersAll = "BuildersAll";
        public const string CMS_HomeCatPositionData = "CMS_HomeCatPositionData_";
        public const string CMS_AllCategories = "CMS_AllCategories";
        public const string MarketBCs = "MarketBCs_";
        public const string Market = "Market_";
        public const string MarketBCBrand = "MarketBCBrand";

        public const string PartnerMarketPremiumUpgrade = "PartnerMarketPremiumUpgrade";
        public const string SeoCommunityNames = "SeoCommsName";

        public const string StateAmenityCounts = "StateAmenityCounts_";
        public const string DeveloperNamesAdded = "DeveloperNamesAdded";

        public const string PartnerMaskIndexList = "PartnerMaskIndexList";
        public const string PartnerSiteTermsList = "PartnerSiteTermsList";

        public const string BuyNewSectionsContentList = "BuyNewSectionsContentList";

        public const string TvMarkets = "TvMarkets";    
        public const string TvMarketContent = "TvMarketContent_{0}_{1}";

        public const string OwnerStoriesContent = "OwnerStoriesContent";
        public const string OwnerStoriesList = "OwnerStoriesList";

        // This information is used in the Property Information section of the Community Details and Home Details page.
        public const string CreditScoreLinks = "CredictScoreLinks";
        public static string TypeaheadAllCommunities = "TypeaheadAllCommunities";
        public static string TypeaheadAllBuilders = "TypeaheadAllBuilders";

        public const string AffiliateLinks = "AffiliateLinks_{0}_{1}";
        public const string TdvMetrics = "TdvMetrics";
        public const string Synthetics = "SyntheticGeoNames";
        public const string MarketDFUContent = "MarketDFUContent_${0}";
        public const string TvMarketsPromos = "TvMarketsPromos";
    }
}
