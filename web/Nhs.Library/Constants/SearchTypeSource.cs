namespace Nhs.Library.Constants
{
    public class SearchTypeSource
    {
        public const string QuickSearchCommunityResults = "qscr";
        public const string QuickSearchHomeResults = "qshr";
        public const string AdvancedSearchCommunityResults = "advcr";
        public const string AdvancedSearchHomeResults = "advhr";
        public const string BoylSearchResults = "boylr";
        public const string HotDealsSearchCommunityResults = "hdcr";
        public const string SearchUnknown = "unk";
        public const string GreenCommunityResults = "grncr";
    }
}
