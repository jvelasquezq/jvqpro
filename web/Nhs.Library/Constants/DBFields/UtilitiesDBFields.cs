namespace Nhs.Library.Constants
{
   public class UtilitiesDBFields
   {
    public const string Description = "community_service_description";
    public const string Phone = "community_service_phone";
    public const string Code = "community_service_type_code";
    public const string Name = "community_service_type_name";
   }
}
