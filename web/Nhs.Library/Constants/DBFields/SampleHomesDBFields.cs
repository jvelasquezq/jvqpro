namespace Nhs.Library.Constants.DBFields
{
    public class SampleHomesDBFields
    {
        public const string Price = "price";
        public const string Bedrooms = "no_bedrooms";
        public const string Bathrooms = "no_baths";
        public const string HalfBaths = "no_half_baths";
        public const string Garages = "no_car_garage";
        public const string Stories = "no_stories";
        public const string ListingId = "listing_id";
        public const string PlanId = "plan_id";
        public const string SpecId = "specification_id";
        public const string SqFt = "square_feet";
        public const string PlanName = "plan_name";
        public const string MarketingDesc = "marketing_description";
        public const string Address = "address";
        public const string ImgThumbnail = "image_thumbnail";
        public const string MoveInDate = "move_in_date";
        public const string PriceLow = "price_low";
        public const string PriceHigh = "price_high";
        public const string HomeCount = "no_home_total";
        public const string HomeStatus = "home_status";
        public const string PlanDescription = "plan_description";
        public const string Phone = "phone";
        public const string HotHome = "hot_home";

    }
}
