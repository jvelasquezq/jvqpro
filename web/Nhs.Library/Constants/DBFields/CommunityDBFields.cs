/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright (c) 2001-2006
 **by BuilderHomeSite Inc. ( http://www.BuilderHomeSite.com )
 * ========================================================
 * Author:
 * Date:
 * Purpose:
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
namespace Nhs.Library.Constants
{
    public class CommunityDBFields
    {
        #region Members
        public const string CommunityDescription = "community_description";        
        public const string Address1 = "address1";
        public const string HomeCount = "home_count";        
        public const string MarketName = "market_name";              
        public const string Status = "status";
        public const string AmenityDescription = "amenity_description";        
        public const string LogoMedium = "logo_url_med";
        public const string LogoSmall = "logo_url_small";
        public const string BuilderUrl = "builderurl";
        public const string BCDescription = "builder_community_description";
        public const string BCUrl = "builder_community_url";
        public const string StatusName = "status_name";
        public const string LotPath = "lot_path";
        public const string LotName = "lot_name";
        public const string ImageDescription = "image_desc";
        public const string ImageTitle = "image_title";
        public const string LocalPath = "local_path";
        public const string ImageName = "image_name";
        public const string BannerImage = "banner_image";
        public const string ImageCount = "pic_count";
        public const string HoursOfOperation = "hours_of_operation";
        public const string DrivingDirections = "driving_directions";        
        public const string SalesOfficeID = "sales_office_id";
        public const string Email = "email";
        public const string Phone = "phone_number";
        public const string Fax = "fax_number";
        public const string Agents = "agents";
        public const string PromoID = "promo_id";
        public const string PromoUrl = "promo_url";
        public const string PromoShortText = "promo_text_short";
        public const string PromoLongText = "promo_text_long";
        public const string HotHomesTitle = "title";
        public const string HotHomesDescription = "detail_description";
        public const string Mapurl = "mapurl";
        public const string EnvisionLink = "envision_link";                 
        public const string LineCounter = "linectr";
        public const string GeoLocationId = "geo_location_id";
        public const string CommunityId = "community_id";
        public const string CommunityName = "community_name";
        public const string MarketId = "market_id";
        public const string City = "city";
        public const string State = "state";
        public const string PostalCode = "postal_code";
        public const string County = "county";
        public const string Country = "country";
        public const string MasterPlanned = "COMMMaster";
        public const string Adult = "COMMAdult";
        public const string Gated = "COMMGated";
        public const string Condo = "COMMCondo";
        public const string Golf = "COMMAMNGolf";
        public const string Green = "COMMGreen";
        public const string Pool = "COMMAMNPool";
        public const string GreenNatureAreas = "COMMAMNGreen";
        public const string Views = "COMMAMNViewOnly";
        public const string Waterfront = "COMMAMNWaterfrontOnly";
        public const string Parks = "COMMAMNPark";
        public const string SportsFacilities = "COMMAMNSport";
        public const string AgeRestricted = "COMMAMNAgeRestricted";
        public const string Latitude = "latitude";
        public const string Longitude = "longitude";
        public const string MappingLevel = "mapping_level";
        public const string StandardGeoLocationId = "standard_geo_location_id";
        public const string ParentCommunityId = "parent_community_id";
        public const string PriceLow = "price_low";
        public const string PriceHigh = "price_high";
        public const string CommunityLatitude = "community_latitude";
        public const string CommunityLongitude = "community_longitude";
        public const string CommunityMappingLevel = "community_mapping_level";
        public const string BCType = "bc_type";
        public const string CommunityImageRebuildFlag = "community_image_rebuild_flag";
        public const string SquareFeetLow = "square_feet_low";
        public const string SquareFeetHigh = "square_feet_high";
        public const string BuilderId = "builder_id";
        public const string CustomBuilderFlag = "custom_builder_flag";
        public const string ListingTypeFlag = "listing_type_flag";
        public const string NumberHomesTotal = "no_home_total";
        public const string SubVideoFlag = "subvideo_flag";
        public const string SpotlightScore = "spotlight_score";
        public const string ImageThumbnail = "image_thumbnail";
        public const string SpotlightThumbnail = "spotlight_thumbnail";
        public const string BuilderName = "builder_name";
        public const string BrandId = "brand_id";
        public const string BrandName = "brand_name";
        public const string StateName = "statename";
        public const string VirtualTourPath = "virtual_tour_path";
        public const string PlanHotHomesTitle = "plan_hot_homes_title";
        public const string PlanHotHomesDescription = "plan_hot_homes_description";
        public const string SpecHotHomesTitle = "spec_hot_homes_title";
        public const string SpecHotHomesDescription = "spec_hot_homes_description";
        public const string HasHotHome = "has_hot_home";
        public const string HotHomeSpecId = "hot_home_spec_id";
        public const string HotHomePlanId = "hot_home_plan_id";
        public const string FlyerFilePath = "flyer_file_path";
        public const string ShowMortgageLink = "ShowMortgageLink";
        #endregion
        
    }
}
