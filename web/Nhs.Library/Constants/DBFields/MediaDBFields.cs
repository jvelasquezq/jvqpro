namespace Nhs.Library.Constants
{
    public class MediaDBFields
    {
        public const string Name = "image_name";
        public const string Type = "image_type_code";
        public const string Path = "local_path";
        public const string Title = "image_title";
        public const string Description = "image_description";
        public const string BrandName = "brand_name";
        public const string OriginalPath = "original_path";
        public const string ClickThruUrl = "clickthru_url";
    }
}
