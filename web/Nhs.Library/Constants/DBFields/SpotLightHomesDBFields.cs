namespace Nhs.Library.Constants
{
   public class SpotLightHomesDBFields
    {
       public const string PlanId = "plan_id";
       public const string SpecificationId = "specification_id";
       public const string PlanName = "plan_name";
       public const string City = "city";
       public const string State = "state";
       public const string BrandName = "brand_name";
       public const string Price = "price";
       public const string Bedrooms = "no_bedrooms";
       public const string Bathrooms = "no_baths";
       public const string HalfBathrooms = "no_half_baths";
       public const string ImageThumbnail = "image_thumbnail";
       public const string SpotlightScore = "spotlight_score";
       public const string SpecType = "spec_type";
       public const string ListingId = "listing_id";
       public const string HotHome = "hot_home";
       public const string SquareFeet = "square_feet";
       public const string StreetAddress = "street_address";
       public const string NoCarGarage = "no_car_garage";
       public const string CommunityName = "community_name";
       public const string MarketName = "market_name";
       
    }
}
