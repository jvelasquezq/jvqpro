namespace Nhs.Library.Constants
{
   public class AmenityDBFields
   {
        #region Members
        public const string Description = "amenity_description";
        public const string IconUrl = "amenity_icon_url"; // community amenity specific
        public const string Url = "amenity_url"; //open amenity specific
        public const string Group = "group_id";
       public const string AmenityId = "amenity_id";
        #endregion
    }
}
