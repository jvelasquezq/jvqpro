namespace Nhs.Library.Constants
{
   public class SchoolDBFields
    {
       //public const string DistrictName = "district_name";
       //public const string DistrictId = "district_Id";
       //public const string DistrictNcesId = "nces_id";
       //public const string SchoolName = "school_name";
       //public const string SchoolLevel = "school_level";
       //public const string NcesSchoolId = "nces_school_id";
        internal const string SchoolName = @"school_name";
        internal const string SchoolLevelId = @"school_level_id";
        internal const string NcesSchoolId = @"nces_school_id";
       internal const string DistrictId = @"district_Id";
       internal const string DistrictName = @"district_name";
       internal const string NcesId = @"nces_id";
    }
}
