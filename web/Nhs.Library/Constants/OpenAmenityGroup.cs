/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright (c) 2001-2006
 **by BuilderHomeSite Inc. ( http://www.BuilderHomeSite.com )
 * ========================================================
 * Author: RMenon
 * Date: 10/19/2006 17:53:53
 * Purpose:
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
namespace Nhs.Library.Constants
{
    public class OpenAmenityGroup
    {
        #region Members
        public const int HealthAndFitness = 1;
        public const int CommunityServices = 2;
        public const int SocialActivities = 3;
        public const int Educational = 4;
        public const int LocalAreaAmenities = 5;
        #endregion
        
    }
}
