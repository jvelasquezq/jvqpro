using Nhs.Utility.Constants;
using Nhs.Utility.Common;
using Nhs.Mvc.Routing.Interface;

namespace Nhs.Library.Constants
{
    public static class ModalWindowsConst 
    {

        public const int FloorPlanGalleryWidth = 782;

        // Listing Actions
        //public const int LeadsRequestInfoWidth = 800;
        //public const int LeadsRequestInfoHeight = 315;

        public const int LeadsRequestInfoShortWidth = 708;
        public const int LeadsRequestInfoShortHeight = 458;

        public const int LeadsRequestInfoLoggedWidth = 320;
        public const int LeadsRequestInfoLoggedHeight = 400;
        public const int LeadsThankYouWidth = 700;
        public const int LeadsThankYouHeight = 380;

        // Account & Profile
        public const int RegisterModalWidth = 530;
        public const int RegisterModalHeight = 544;
        public const int RegisterFromLeadsModalWidth = 700;
        public const int RegisterFromLeadsModalHeight = 245;
        public const int SignInModalWidth = 530;
        public const int SignInModalHeight = 420;
        public const int CreateAlertModalWidth = 748;
        public const int CreateAlertModalHeight = 370;
        public const int UpdateMyAccountModalWidth = 825;
        public const int UpdateMyAccountModalHeight = 440;
        public const int RecoverPasswordModalWidth = 530;
        public const int RecoverPasswordModalHeight = 420;
        public const int LoginModalWidht = 530;
        public const int LoginModalHeight = 280;

        //public const int ThankYouModalWidth = 700;
        //public const int ThankYouModalHeight = 312;

        // Send to Friend
        public const int SendToFriendWidth = 425;
        public const int SendToFriendHeight = 300;

        //Send To Phone
        public const int SendToPhoneWidth = 270;
        public const int SendToPhoneHeight = 200;

        //SendCustomBrochure
        public const int SendCustomBrochureHeight = 425;
        public const int SendCustomBrochureWidth = 890;

        //RecommendedCommunities
        public const int RecommendedCommunitiesHeight = 425;
        public const int RecommendedCommunitiesWidth = 750;
        public const int RecommendedCommunitiesThanksYouHeight = 160;
        public const int RecommendedCommunitiesThanksYouWidth = 350;

        //Pro CRM
        public const int ProCrmTaskHeight = 425;
        public const int ProCrmTaskWidth = 750;
        public const int ProCrmClientHeight = 425;
        public const int ProCrmClientWidth = 750;
        public const int ProCrmFavoriteHeight = 230;
        public const int ProCrmFavoriteWidth = 750;

        public const int RecoRetunUserHeight = 450;
        public const int RecoRetunUserWidth = 800;

        public const int MapPopupWidth = 520;
        public const int MapPopupHieght = 520;
    }
}
