/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright (c) 2001-2007
 **by BuilderHomeSite Inc. ( http://www.BuilderHomeSite.com )
 * ========================================================
 * Author: RMenon
 * Date: 04/13/2007 12:39:09
 * Purpose:
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
namespace Nhs.Library.Constants
{
    public class ReferTags
    {
        #region Members

        public const string SiteIndex = "DSI";
        public const string StateIndex = "DST";
        public const string NewHomes = "DMD";
        public const string NewHomesBuilders = "DMR";
        public const string StateBuilder = "DSB";
        public const string MarketBuilder = "DMB";
        public const string StateAmenityCondoTown = "DSC";
        public const string StateAmenityGolf = "DSG";
        public const string StateAmenityWaterfront = "DSW";
        public const string MarketAmenityCondoTown = "DMC";
        public const string MarketAmenityGolf = "DMG";
        public const string MarketAmenityWaterfront = "DMW";
        public const string CommunityResultsNoMatchRedirect = "DCRNM";

        #endregion
    }
}
