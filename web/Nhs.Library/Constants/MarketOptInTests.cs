namespace Nhs.Library.Constants
{
    public class MarketOptInTests
    {
        public const string ShortForm_Move = "ShortForm-Move";
        public const string ShortForm_Regular = "ShortForm-Regular";
        public const string UberBanner_6431 = "UberBanner-6.4.3.1";
        public const string CreateAccountAndAlert_6430 = "CreateAccountAndAlert-6.4.3.0";
        public const string RegisterCreateAlert_New = "RegisterCreateAlert-New";
    }
}
