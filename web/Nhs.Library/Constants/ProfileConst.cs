namespace Nhs.Library.Constants
{
    public class ProfileConst
    {
        public const string UserID = "g_user_id";
        public const string LogonName = "u_logon_name";
        public const string FirstName = "u_first_name";
        public const string LastName = "u_last_name";
        public const string PartnerID = "PartnerID";
        public const string Email = "u_logon_name";
        public const string Address1 = "u_address1";
        public const string Address2 = "u_address2";
        public const string DateRegistered = "d_date_registered";
        public const string DateChanged = "d_date_last_changed";
        public const string MiddleName = "middlename";
        public const string Password = "u_user_security_password";
        public const string State = "u_state";
        public const string Metro = "u_StxtMetro";
        public const string City = "u_city";
        public const string DayPhone = "u_dayphone";
        public const string DayPhoneExt = "u_dayPhoneExt";
        public const string EvePhone = "u_evephone";
        public const string EvePhoneExt = "u_evePhoneExt";
        public const string PostalCode = "u_postalcode";
        public const string Fax = "u_fax";
        public const string MailList = "u_MailList";
        public const string ReferrerName = "u_ReferrerName";
        public const string UserAccountState = "i_account_status";
        public const string LeadOptIn = "u_LeadOptIn";
        public const string MarketOptIn = "u_MktOpt";
        public const string RegMetro = "u_regMetro";
        public const string RegCity = "u_RegCity";
        public const string RegPrefer = "u_RegPrefer";
        public const string RegState = "u_RegState";
        public const string RegPrice = "u_RegPrice";
        public const string Age = "age";
        public const string NumberOfViewedHomes = "pNumberViewHomes";
        public const string NumberOfViewedCommunities = "pNumberViewCom";
        public const string NumberOfSavedHomes = "pNumberSaveHomes";
        public const string NumberOfSavedCommunities = "pNumberSaveCom";

        public const string ViewHomesArray = "pViewHomes";
        public const string ViewComArray = "pViewCom";
        public const string SaveHomesArray = "pSaveHomes";
        public const string SaveComArray = "pSaveCom";
        public const string RequestArray = "pRequestArray";
        public const string RecomendedArray = "pRecCommArray";
        public const string GuestProfile = "hbs";

        public const string RegBOYLInterest = "u_RegBOYLInterest";
        public const string RegActiveAdultInterest = "u_RegActiveAdultInterest";
        public const string RegComingSoonInterest = "u_RegComingSoonInterest";
        public const string BoxRequestedDate = "d_BoxRequestedDate";
        public const string WeeklyNotifierOptIn = "u_WeeklyOptIn";
        public const string NumberOfRecommendedCommunities = "pNumberRecCom";
        public const string ArchivedCommArray = "pArchivedCommArray";
        public const string InitialMatchDate = "d_InitialMatchDate";

        public const string MoveInDateId = "move_in_date";
        public const string FinancePreferenceId = "finance_pref_id";
        public const string MovingReasonId = "moving_reason_id";

        public const string AgencyName = "agency_name";
        public const string RealEstateLicense = "license";
        

        public const int MAX_NO_MATCH_EMAIL_TRY = 3;

    }
}

