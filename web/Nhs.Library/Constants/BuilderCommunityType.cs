/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright (c) 2001-2006
 **by BuilderHomeSite Inc. ( http://www.BuilderHomeSite.com )
 * ========================================================
 * Author: RMenon
 * Date: 10/09/2006 14:48:20
 * Purpose: const file for bc type
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
namespace Nhs.Library.Constants
{
    public class BuilderCommunityType
    {
        #region Members

        public const string GrandOpening = "g";
        public const string ComingSoon = "c";
        public const string CloseOut = "x";
        public const string Normal = "n";

        #endregion
    }
}