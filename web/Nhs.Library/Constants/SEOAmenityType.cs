/*
 **NewHomeSource = http://www.NewHomeSource.com
 **Copyright (c) 2001=2006
 **by BuilderHomeSite Inc. ( http://www.BuilderHomeSite.com )
 * ========================================================
 * Author: Asolis
 * Date: 07/18/2007 14:10:00
 * Purpose: Amenity type param for amenity index page
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
namespace Nhs.Library.Constants
{
    public class SEOAmenityType
    {
        #region Members

        public const string StateGolf = "golf";
        public const string StateGolfCourse = "golfcourse";
        public const string StateWaterfront = "waterfront";
        public const string StateCondoTown = "condos-and-townhomes";

        public const string MarketGolfCourse = "golf-homes";
        public const string MarketWaterfront = "lakehomes";
        public const string MarketCondoTown = "townhomes";

        #endregion

    }
}
