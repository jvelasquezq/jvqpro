using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Nhs.Library.Constants
{
    public static class TypeAheadAbbreviations
    {
        private static Dictionary<string, string> _states;

        public static Dictionary<string, string> States
        {
            get
            {
                if (_states == null)
                {
                    //states abbreviation
                    _states = new Dictionary<string, string>();
                    _states.Add("alabama", "AL");
                    _states.Add("alaska", "AK");
                    _states.Add("arizona", "AZ");
                    _states.Add("arkansas", "AR");
                    _states.Add("california", "CA");
                    _states.Add("colorado", "CO");
                    _states.Add("connecticut", "CT");
                    _states.Add("delaware", "DE");
                    _states.Add("florida", "FL");
                    _states.Add("georgia", "GA");
                    _states.Add("hawaii", "HI");
                    _states.Add("idaho", "ID");
                    _states.Add("illinois", "IL");
                    _states.Add("indiana", "IN");
                    _states.Add("iowa", "IA");
                    _states.Add("kansas", "KS");
                    _states.Add("kentucky", "KY");
                    _states.Add("louisiana", "LA");
                    _states.Add("maine", "ME");
                    _states.Add("maryland", "MD");
                    _states.Add("massachusetts", "MA");
                    _states.Add("michigan", "MI");
                    _states.Add("minnesota", "MN");
                    _states.Add("mississippi", "MS");
                    _states.Add("missouri", "MO");
                    _states.Add("montana", "MT");
                    _states.Add("nebraska", "NE");
                    _states.Add("nevada", "NV");
                    _states.Add("new hampshire", "NH");
                    _states.Add("new jersey", "NJ");
                    _states.Add("new mexico", "NM");
                    _states.Add("new york", "NY");
                    _states.Add("north carolina", "NC");
                    _states.Add("north dakota", "ND");
                    _states.Add("ohio", "OH");
                    _states.Add("oklahoma", "OK");
                    _states.Add("oregon", "OR");
                    _states.Add("pennsylvania", "PA");
                    _states.Add("rhode island", "RI");
                    _states.Add("south carolina", "SC");
                    _states.Add("south dakota", "SD");
                    _states.Add("tennessee", "TN");
                    _states.Add("texas", "TX");
                    _states.Add("utah", "UT");
                    _states.Add("vermont", "VT");
                    _states.Add("virginia", "VA");
                    _states.Add("washington", "WA");
                    _states.Add("west virginia", "WV");
                    _states.Add("wisconsin", "WI");
                    _states.Add("wyoming", "WY");
                }

                return _states;
            }
        }

        public static string ChangeAbbreviations(string location, Dictionary<string, string> abbreviations)
        {
            string textChanged = location;
            var keywords = location.Split(' ');
            
            foreach (var word in keywords) 
            {
                if (abbreviations.ContainsKey(word.ToLower()))
                {
                    textChanged = textChanged.Replace(word, abbreviations[word.ToLower()].ToLower());
                } 
            }

            return textChanged;
        }

        public static string ReplaceStateName(string location)
        {
            string[] keywords = Regex.Split(location, @"\W+");
            string textChanged = location;

            if (keywords.Length >= 2) // we are interested in the state string at the end (>= for cases like cedar park texas)
            {
                var stateName = keywords[keywords.Length - 1].ToLower(); //grab the last string (changed to tolower() since States dictionary is all lowercase; uppercase state entry was failing)
                if (stateName.Length > 2)
                {
                    if (States.ContainsKey(stateName))
                        textChanged = textChanged.Replace(stateName, ", " + States[stateName]);
                }
            }
            return textChanged;
        }
    }
}
