namespace Nhs.Library.Enums
{
    using Utility.Common;

    public enum ContentTagKey
    {
        Arrow,
        AlertListText,
        SupportEmail,
        SavedHomesLink,
        SavedHomesText,
        SearchNewHomes,
        StartSearchAlert,
        GetOurNewsletter,
        BrochureUtmEmailSource,
        BrochureUtmEmailCampaign,
        PropertyName,
        RecommendComms,
        Year,
        SiteName,
        MatchingCommunities,
        RequestId,
        CommunityId,
        FirstName,
        RecoCount,
        PolicyUrl,
        UnsubscribeUrl,
        LearnMoreUrl,
        QuestionsUrl,
        GetQualifiedUrl,
        BeaconUrl,
        Email,
        Password,
        SiteLogo,
        BrochureUrl,
        CountyName,
        PartnerName,
        MarketId,
        MarketName,
        StateName,
        StateID,
        City,
        CityName,
        Cities,
        PlanName,
        BuilderName,
        CommunityName,
        Amenity,
        SchoolDistrict,
        SchoolDistrictId,
        BrandName,
        BrandID,
        MarketState,
        MarketStateName,
        ZipPrimaryCity,
        Zip,
        PartnerSiteUrl,
        PageNumber,
        ResourceDomain,
        RedirectUrl,
        RootDomain,
        HomeTitle,
        PriceRangeLow,
        PriceRangeHigh,
        BedroomRangeLow,
        BedroomRangeHigh,
        BathroomRangeLow,
        BathroomRangeHigh,
        SquareFtRangeLow,
        SquareFtRangeHigh,
        CommunityCount,
        BuilderCount,
        HomeCount,
        //PopularCommunities,
        SpecCount,
        PlanCount,
        //TopBuilders,
        Question,
        Url,
        SpecOrPlanPlusId,
        BasicHomeAddress,
        SyntheticName
    }

    public static class ContentTagExtensions
    {
        public static string ToString(this ContentTagKey tagKey)
        {
            return tagKey.ToType<string>();
        }

        public static string Enclose(this ContentTagKey tagKey)
        {
            return string.Format("[{0}]", tagKey.ToType<string>().ToLower());
        }
    }


}
