﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Library.Constants.Enums
{
    public enum ProCrmTaskTypes : byte
    {
        Email = 0,
        Call,
        Mail,
        Appointment,
        Other
    }
}
