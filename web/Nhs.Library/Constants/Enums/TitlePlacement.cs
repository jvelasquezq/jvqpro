namespace Nhs.Library.Enums
{
    public enum TitlePlacement
    {
        UseOriginal,
        Replace,
        AddBefore,
        AddAfter
    }

  
}
