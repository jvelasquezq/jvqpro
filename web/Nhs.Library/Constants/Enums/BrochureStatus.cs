﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Library.Constants.Enums
{
    public enum BrochureStatus
    {
        NotGenerated,
        Active,
        Processing,
        Expired
    }
}
