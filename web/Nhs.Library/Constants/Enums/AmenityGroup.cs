﻿namespace Nhs.Library.Constants.Enums
{
    public enum AmenityGroup
    {
        HealthAndFitnessCommAmenities,
        LocalAreaCommAmenities,
        CommunityServicesCommAmenities,
        SocialActivitiesCommAmenities,

        HealthAndFitnessOpenAmenities,
        CommunityServicesOpenAmenities,
        LocalAreaOpenAmenities,
        EducationalOpenAmenities,
        SocialActivitiesOpenAmenities
    }

    public enum AmenityGroupType
    {
        HealthAndFitness,
        LocalArea,
        CommunityServices,
        Educational,
        SocialActivities
    }
}
