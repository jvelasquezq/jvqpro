/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright (c) 2001-2006
 **by BuilderHomeSite Inc. ( http://www.BuilderHomeSite.com )
 * ========================================================
 * Author: RMenon
 * Date: 10/19/2006 11:09:57
 * Purpose:
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
namespace Nhs.Library.Enums
{
    public enum BuilderService
    {

        DigitalDashboardReport = 1,
        DigitalDashboard_DivisionAccess = 2,
        BDXtractorDataEntry = 3,
        ShowBuilderShowcase = 4
    }
}
