﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Nhs.Library.Constants.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum WebApiSearchType
    {
        Radius,
        Exact,
        Map
    }
}
