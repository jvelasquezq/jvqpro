using System.ComponentModel;

namespace Nhs.Library.Enums
{
    public enum ListingType
    {
        [Description("P")] Plan,
        [Description("S")] Spec,
        [Description("C")] Community,
        BOYLCommunity,
        NonBOYLCommunity
    }
}
