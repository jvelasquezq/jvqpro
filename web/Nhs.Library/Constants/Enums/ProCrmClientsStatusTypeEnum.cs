﻿namespace Nhs.Library.Constants.Enums
{
    public enum ProCrmClientsStatusType:byte
    {
        Inactive = 0,
        Active = 1,
        Deleted = 2
    }
}
