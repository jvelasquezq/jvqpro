namespace Nhs.Library.Enums
{
    public enum WebActors 
    { 
        GuestUser, 
        PassiveUser, 
        ActiveUser 
    }
}
