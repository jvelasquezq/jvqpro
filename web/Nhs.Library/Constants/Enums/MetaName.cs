﻿namespace Nhs.Library.Enums
{
    /// <summary>
    /// Supported head elements names
    /// </summary>
    public enum MetaName
    {
        Title,
        Keywords,
        Description,
        Author,
        Generator,
        Revised,
        Robots,
        DcsWebevents,
        DcsEventcode,
        DcsPid,
        ServerName,
        IpAddress,
        Parameter,
        Canonical 
    }
}
