namespace Nhs.Library.Enums
{
    public enum CommunityType
    {
        Active,
        ComingSoon,
        GrandOpening,
        Closeout
    }
}
