﻿namespace Nhs.Library.Constants.Enums
{
    public enum CommunityDetailTabs
    {
        AllCommunities,
        QuickMoveIn,
        FilterCommunities
    }
}
