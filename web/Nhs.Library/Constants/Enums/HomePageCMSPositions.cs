namespace Nhs.Library.Enums
{
    public enum HomePageCMSPositions
    {
        TopMedia = 1,
        BottomMedia,
        Headline1,
        Headline2,
        Headline3,
        Headline4
    }
}
