namespace Nhs.Library.Enums
{
    public enum CommonListItem
    {
        MinPrice,
        MaxPrice,
        PriceRange,
        Bedrooms,
        Bathrooms,
        Garage,
        LivingArea,
        Stories,
        MasterBed,
        HomeType,
        HomeStatus,
        MoveInDate,
        FinancialPref,
        MoveReason,
        Radius
    }
}
