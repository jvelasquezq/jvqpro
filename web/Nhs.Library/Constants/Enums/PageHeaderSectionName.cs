﻿namespace Nhs.Library.Enums
{
    public enum PageHeaderSectionName
    {
        SeoMetaPages,
        StaticMetaPages
    }
}
