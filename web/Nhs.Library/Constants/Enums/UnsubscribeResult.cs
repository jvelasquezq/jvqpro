namespace Nhs.Library.Enums {
    public enum UnsubscribeResult {
        Successful,
        InvalidEmail,
        UnknownFailure,
        Unknown
    }
}
