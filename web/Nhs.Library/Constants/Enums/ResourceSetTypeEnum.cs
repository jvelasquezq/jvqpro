﻿
namespace Nhs.Library.Constants.Enums
{
    public enum ResourceSetTypeEnum : byte
    {
        Javascript = 0,
        Stylesheet = 1
    }
}
