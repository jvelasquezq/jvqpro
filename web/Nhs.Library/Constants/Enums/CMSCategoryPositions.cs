namespace Nhs.Library.Enums
{
    public enum CMSCategoryPositions
    {
        TopLeftMedia=1,
        TopRightMedia=2,
        BottomLeftMedia=3,
        BottomRightMedia=4,
        Headline1_5Slot=3,
        Headline2_5Slot=4,
        Headline3_5Slot=5,
        Headline1_7Slot=5,
        Headline2_7Slot=6,
        Headline3_7Slot=7
    }
}
