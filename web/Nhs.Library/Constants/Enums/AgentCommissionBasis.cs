﻿using System.ComponentModel;

namespace Nhs.Library.Constants.Enums
{
    public enum AgentCommissionBasis
    {
        [Description("Base Price")] BasePrice,
        [Description("Base Price plus Lot")] BasePriceWithLot,
        [Description("Base Price plus Options")] BasePriceWithOptions,
        [Description("Base Price plus Lot and Options")] BasePriceWithLotWithOptions
    }
}
