namespace Nhs.Library.Enums
{
    public enum ZoomLevel
    {
        National = 2,
        State = 4,
        MarketLarge = 6,
        Market = 7,
        City = 8,
        Area = 9,
        Neighborhood = 10,
        Street = 13
    }
}
