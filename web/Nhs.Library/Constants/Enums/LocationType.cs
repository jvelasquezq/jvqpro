using System.ComponentModel;

namespace Nhs.Library.Enums
{
    public enum LocationType
    {
        Market = 1,
        City = 2,
        County = 3,
        Zip = 4,
        Community = 5,
        Developer = 6
    }
}
