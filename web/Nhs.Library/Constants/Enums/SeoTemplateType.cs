﻿namespace Nhs.Library.Constants.Enums
{
    public enum SeoTemplateType
    {
        None,
        Seo_Market,
        Nhs_Market_Srp,
        Nhs_Market_City_Srp,
        Nhs_CommunityName_Srp,
        Nhs_County_Srp,
        Nhs_Zip_Srp,
        Nhs_Quickconnect_Srp,
        Nhs_Site_Index,
        Nhs_SiteIndex, //This POS is used by /SiteIndex
        Nhs_Market_Coming_Srp,
        Nhs_Market_HotDeals_Srp,
        Nhs_Market_QMI_Srp,
        Nhs_Market_City_QMI_Srp,
        Survey_Modal_Content,
        MNH_HomeV2, //Used on new Home look&feel version (Search Redesign 7.3.0.0)
        Nhs_HomeV2, //Used on new Home look&feel version (Search Redesign 7.3.0.0)
        NHL_Home,
        Pro_Home,
        PRO_Market_City_SRP,
        CommDetail_Res_Center, //Used on Community detail v1 on the right rail
        Nhs_Site_Index_State,        
        NHS_StateBrands,
        NHS_Market_Brand,
        Community,
        Plan,
        Spec,
        AffiliateLinks,
        Community_Market, //Used on Community Results, matches to Builder-Filter folder at Community_Market level
        Community_City,  //Used on Community Results, matches to Builder-Filter folder at Community_City level
        Community_PostalCode,  //Used on Community Results, matches to Builder-Filter folder at Community_PostalCode level
        Community_County,  //Used on Community Results, matches to Builder-Filter folder at Community_County level
        Home_Market, //Used on Community Results, matches to Builder-Filter folder at Community_Market level
        Home_City,  //Used on Community Results, matches to Builder-Filter folder at Community_City level
        Home_PostalCode,  //Used on Community Results, matches to Builder-Filter folder at Community_PostalCode level
        Home_County,  //Used on Community Results, matches to Builder-Filter folder at Community_County level
        Waterfront_Community_Market, //Used on Community Results, matches to Waterfront-Filter folder at Community_Market level
        Waterfront_Home_Market, //Used on Community Results, matches to Waterfront-Filter folder at Home_Market level
        Waterfront_Community_City, //Used on Community Results, matches to Waterfront-Filter folder at Community_City level
        Waterfront_Home_City, //Used on Community Results, matches to Waterfront-Filter folder at Home_City level
        Golf_Community_Market, //Used on Community Results, matches to Golf-Filter folder at Community_Market level
        Golf_Home_Market, //Used on Community Results, matches to Golf-Filter folder at Home_Market level
        Golf_Community_City, //Used on Community Results, matches to Golf-Filter folder at Community_City level
        Golf_Home_City, //Used on Community Results, matches to Golf-Filter folder at Home_City level
        MF_Community_Market, //Used on Community Results, matches to Golf-Filter folder at Community_Market level
        MF_Home_Market, //Used on Community Results, matches to Golf-Filter folder at Home_Market level
        MF_Community_City, //Used on Community Results, matches to Golf-Filter folder at Community_City level
        MF_Home_City, //Used on Community Results, matches to Golf-Filter folder at Home_City level
        BasicCommunityDetail, //Used on Basic Community page, matches to "BasicCommunityDetail" folder.
        BasicHomeDetail, //Used on Basic Home page, matches to "BasicHomeDetail" folder.
        SchoolDistrict_Community_Market, //Used on Community Results, matches to SchoolDistrict-Filter folder at Community_Market level
        SchoolDistrict_Home_Market, //Used on Community Results, matches to SchoolDistrict-Filter folder at Home_Market level
        Adult_Community_Market, //Used on Community Results, matches to Adult-Filter folder at Community_Market level
        Adult_Home_Market, //Used on Community Results, matches to Adultf-Filter folder at Home_Market level
        Adult_Community_City, //Used on Community Results, matches to Adult-Filter folder at Community_City level
        Adult_Home_City, //Used on Community Results, matches to Adult-Filter folder at Home_City level        
        Nhs_Builders_Market_City_Srp,
        Nhs_Builders_Market_Srp,
        Synthetic_Search,                 // Used on Synthetic Search, Page without filers
        Synthetic_Search_Adult,           // Used on Synthetic Search, Page with Adult filter
        Synthetic_Search_Builder,         // Used on Synthetic Search, Page with builder filter
        Synthetic_Search_ComingSoon,      // Used on Synthetic Search, Page with comingsoon filter
        Synthetic_Search_GolfCourse,      // Used on Synthetic Search, Page with golfcourse filter
        Synthetic_Search_HotDeals,        // Used on Synthetic Search, Page with hotdeals filter
        Synthetic_Search_MultiFamily,     // Used on Synthetic Search, Page with hotdeals filter
        Synthetic_Search_QMI,             // Used on Synthetic Search, Page with multifamily filter
        Synthetic_Search_SchoolDistrict,  // Used on Synthetic Search, Page with schooldistrict filter
        Synthetic_Search_WaterFront       // Used on Synthetic Search, Page with Waterfront filter        
    }

    public enum FacebookMetaType
    {
        Type,
        App_Id,
        Url,
        Image,
        Site_Name,
        Description,
        Title
    }
}
