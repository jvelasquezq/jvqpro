/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright (c) 2001-2006
 **by BuilderHomeSite Inc. ( http://www.BuilderHomeSite.com )
 * ========================================================
 * Author: RMenon
 * Date: 10/19/2006 17:53:34
 * Purpose:
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
namespace Nhs.Library.Constants
{
    public class CommunityAmenityGroup
    {
        #region Members
        public const int Parks = 1;
        public const int SportFacilities = 2;
        public const int GreenNatureAreas = 3;
        public const int ViewsWaterFront = 4;
        public const int Services = 5;
        #endregion
        
    }
}
