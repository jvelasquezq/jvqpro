namespace Nhs.Library.Constants
{
    public class LeadType
    {
        public const string AccountCreation = "acc";
        public const string Builder = "rcb";
        public const string Community = "com";
        public const string QuickConnect = "qc";
        public const string Home = "hm";
        public const string Market = "rcm";
        public const string Campaign = "rcc"; 
        public const string Test = "test";
        public const string PasswordRecovery = "pwd";
        public const string Rcm = "RCM-Return";

    }
}
