/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright (c) 2001-2006
 **by BuilderHomeSite Inc. ( http://www.BuilderHomeSite.com )
 * ========================================================
 * Author: RMenon
 * Date: 10/19/2006 16:19:41
 * Purpose:
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
namespace Nhs.Library.Constants
{
    public class CommunityTabNames
    {
        #region Members
        public const string Media = "Videos and Photos";
        public const string CommunityDetails = "Community Details";
        public const string Amenities = "Amenities";
        public const string DrivingDirection = "Driving Directions";
        public const string ViewHomes = "View Homes";
        #endregion

    }
}
