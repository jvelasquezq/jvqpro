namespace Nhs.Library.Constants
{
    public class PlannerPages
    {
        #region Members
        public const string SavedProperties = "properties";
        public const string SavedSearchAlerts = "searchalerts";
        #endregion
    }
}
