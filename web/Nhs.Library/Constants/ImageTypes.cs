/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright (c) 2001-2006
 **by BuilderHomeSite Inc. ( http://www.BuilderHomeSite.com )
 * ========================================================
 * Author: RMenon
 * Date: 10/15/2006 19:42:55
 * Purpose:
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
namespace Nhs.Library.Constants
{
    public class ImageTypes
    {
        #region Members
        public const string Community = "COM";
        public const string LotMap = "LOT";
        public const string CommunityVideoTour = "SVT";
        public const string CommunityBanner = "CBR";
        public const string Accreditation = "ASL";
        public const string FloorPlan = "FLP";
        //public const string HomePhotos = "PHO";
        public const string EBrochureUrl = "EBR";
        public const string Interior = "INT";
        public const string Elevation = "ELE";
        public const string VirtualTour = "VIR";
        public const string BuilderMap = "MAP";


        public const string BrandShowCase = "BRD";
        public const string BrandAwards = "BAA";

        #endregion

    }
}
