
using System;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;

namespace Nhs.Library.Constants
{
    public class Pages
    {
        // General
        public const string Home = "home";
        public const string HomeV2 = "homev2";
        public const string HomePage = "homepage";
        //public const string Error = "error";
        public const string MvcError = "mvcerror";
        public const string AdPage = "adpage";
        public const string FlashPlayer = "flashplayer";


        //BasicHomeDetail 
        public const string BasicDetail = "basicdetail";
        // Utility
        public const string RefreshCache = "refreshcache";
        public const string LogRedirect = "logredirect";
        public const string EventLogger = "eventlogger";
        public const string DrivingDirections = "drivingdirections";

        public const string CommunityIndex = "communityindex";
        public const string HomeIndex = "homeindex";

        // Property Detail
        public const string CommunityDetail = "communitydetail";
        public const string BasicCommunity = "basiccommunity";
        public const string CommunityDetailMove = "community";
        public const string CommunityDetailPreview = "communitydetailpreview";
        public const string CommunityDetailv1 = "communitydetailv1";
        public const string CommunityDetailv2 = "communitydetailv2";
        public const string CommunityAmenities = "communityamenities";
        public const string CommunityMedia = "communitymedia";
        public const string CommunityDrivingDirections = "communitydrivingdirections";
        public const string CommunityHomeResults = "communityhomeresults";
        public const string PlanDetailMove = "plan";
        public const string SpecDetailMove = "home";
        public const string HomeDetail = "homedetail";
        public const string GaleryFloorPlan = "homedetail/galeryfloorplan";
        public const string SpecDetail = "specdetail";
        public const string HomeDetailPreview = "homedetailpreview";
        public const string HomeDetailv1 = "homedetailv1";
        public const string HomeDetailv2 = "homedetailv2";
        public const string HomeDetailv3 = "homedetailv3";
        public const string HomeDetailv4 = "homedetailv4";
        public const string HomeMedia = "homeimages";
        public const string HomeDrivingDirections = "homedrivingdirections";

        // Property Search
        public const string BasicSearch = "basicsearch";
        public const string AdvancedSearch = "advancedsearch";
        public const string BOYLSearch = "boylsearch";
        public const string QuickMoveInSearch = "quickmoveinsearch";
        public const string HotDealsSearch = "hotdealssearch";
        public const string BOYLResults = "boylresults";
        public const string HomeResults = "homeresults";
        public const string HomeResultsv2 = "homeresultsv2";
        public const string CommunityResults = "communityresults";
        public const string CommunityResultsMap = "communityresults/map";
        public const string CommunityResultsv2 = "communityresultsv2";
        public const string CommunityResultsMapPopup = "communityresults/mappopup";
        public const string CommunityResultsBasicListingMapPopup = "communityresults/blmappopup";

        public const string CommunityResultsV2MapPopup = "communityresultsv2/mappopup";

        public const string NoMatchHandler = "nomatchhandler";
        public const string NoMatchHandlerMLS = "nomatchhandlermls";
        public const string LocationHandler = "locationhandler";
        public const string GreenSearch = "greensearch";

        // Listing Actions
        public const string LeadsNarrowRequest = "narrowrequest";
        public const string LeadsRequestInfo = "requestinfo";
        public const string LeadsRequestInfoShort = "requestinfoshort";
        public const string LeadsRequestInfoShortModal = "requestinfoshortmodal";
        public const string LeadsRequestInfoLogged = "requestinfologged";
        public const string LeadsRequestInfoLoggedModal = "requestinfologgedmodal";
        public const string LeadsRequestInfoThanks = "requestinfothanks";

        public const string LeadsConfirmRequest = "leadsconfirmrequest";
        public const string LeadsThankYou = "leadsthankyou";
        public const string LeadsThankYouModal = "leadsthankyoumodal";
        public const string LeadAcceptance = "leadacceptance";

        //New MVC Request Brochure Modals

        public const string LeadsRequestBrochureModal = "freebrochure/requestbrochure";
        
        public const string LeadsRequestBrochureThanksModal = "requestbrochurethanks";
        public const string LeadsRequestBrochureThanksModalConversion = "modalthankyou";

        public const string BrochureInterstitial = "brochureinterstitial";

        // Account & Profile
        public const string MyAccount = "myaccount";
        public const string MyClients = "myclients";
        public const string Login = "login";
        public const string LoginModal = "loginmodal";
        public const string LoginModalOld = "loginmodalold"; //Old login used on old pages
        public const string LogOff = "logoff";
        public const string Register = "register";
        public const string RegisterModal = "registermodal";
        public const string BrochureRegister = "brochureregister";
        public const string RegisterCreateAlert = "registercreatealert";
        public const string RegisterCreateAlertModal = "registercreatealertmodal";
        public const string RegisterThankYou = "registerthankyou";
        public const string RegisterThankYouModal = "registerthankyoumodal";
        public const string EditAccount = "editaccount";
        public const string EditAccountModal = "editaccountmodal";
        public const string Unsubscribe = "unsubscribe";
        public const string SavedHomes = "savedhomes";
        public const string RecentItems = "recentitems";
        public const string RecoverPassword = "recoverpassword";
        public const string RecoverPasswordModal = "recoverpasswordmodal";
        public const string ForgotPasswordModal = "forgotpasswordmodal";
        public const string SendToFriend = "sendtofriend";
        public const string SendToFriendModal = "sendtofriendmodal";
        public const string SendToFriendEmail = "sendtofriendemail";
        public const string GetBrochure = "getbrochure";
        public const string Brochure = "brochure";
        public const string AccountCreationMail = "accountcreationemail";
        public const string RecoverPasswordEmail = "recoverpasswordemail";
        public const string MatchMakerEmail = "matchmakeremail";
        public const string MatchMakerLeads = "matchmakerleads";
        public const string SearchAlertEmail = "searchalertemail";
        //Pro crm
        public const string AddClientModal = "addclientmodal";
        public const string EditClientModal = "editclientmodal";
        public const string ClientDetail = "procrm/clientdetail";
        public const string TaskModal = "procrm/taskmodal";
        public const string GetClients = "procrm/getclients";
        public const string GetClientsDetailInfo = "procrm/getclientsdetailinfo";
        public const string GetClientsTask = "procrm/getclientstask";
        public const string GetSavetoFavorites = "procrm/savetofavoritesclientmodal";
        // Search alerts
        public const string CreateAlert = "createalert";
        public const string CreateAlertModal = "createalertmodal";
        public const string AlertsUnsubscribe = "unsubscribealerts";

        // NoResults
        public const string NoResultsFound = "noresultsfound";

        // Site Info
        public const string SiteIndex = "siteindex";
        public const string StateIndex = "stateindex";
        public const string NewHomeBuilders = "new-home-builders";
        public const string NewHomeBuildersNew = "new-home-builders-new";
        public const string StateBuilder = "home-builders";
        public const string StateBuilderNoMarket = "home-builders-nomarket";
        public const string StateBuilderNew = "home-builders-new";
        public const string MarketBuilder = "new-home-builder";
        public const string AmenityIndexGolf = "golf-course-homes";
        public const string AmenityIndexWaterfront = "waterfront-homes";
        public const string AmenityIndexCondo = "condo-sale";
        public const string AmenityMarketGolf = "golf-homes";
        public const string AmenityMarketWaterfront = "lakehomes";
        public const string AmenityMarketCondo = "townhomes";
        public const string NewHomes = "new-homes";
        public const string AboutUs = "aboutus";
        public const string ContactUs = "contactus";
        public const string PrivacyPolicy = "privacypolicy";
        public const string TermsOfUse = "termsofuse";
        public const string Partners = "partners";
        public const string ListYourHomes = "listyourhomes";

        //CMS
        public const string Article = "article";
        //public const string Articles = "resourcecenter/articles"; not used
        //public const string ResourceCenterAuthors = "resourcecenter/authors"; not used
        public const string ResourceCenterSearch = "resourcecenter/search";
        public const string AgentResourcesSearch = "agentresources/search";
        public const string NewHome101Search = "newhome101/search";

        public const string HomeGuide = "homeguide";
        public const string HomeGuideCategory = "homeguidecategory";
        public const string HomeGuideArticle = "homeguidearticle";
        public const string ArticleSearch = "articlesearch";

        //Map Search
        public const string StateMap = "statemap";
        public const string MarketMap = "marketmap";

        //Ads
        public const string SurveyPopUp = "surveypopupad";

        //mls
        public const string MlsHomeResults = "mlshomeresults";

        //Mortgage Rates
        public const string MortgageRates = "mortgagerates";
        public const string MortgageTable = "mortgagetable";
        public const string MortgageCalculator = "mortgagecalculator";
        public const string MortgagePayments = "mortgagepayments";

        public const string SiteHelp = "sitehelp";
        public const string SignIn = "signin";
        public const string SupportedBrowsers = "supportedbrowsers";

        public const string SendToPhone = "sendtophone";

        public const string ResourceCenter = "resourcecenter";
        public const string AgentResources = "agentresources";
        public const string NewHome101 = "newhome101";
        public const string CentroDeRecursos = "guia-para-casas-nuevas";

        public const string ResourceCenter101 = "resourcecenter/new-home-101";
        public const string SendCustomBrochure = "sendcustombrochure";
        public const string Preview = "brochure/preview";

        public const string PdfBrochure = "pdfbrochure";
        public const string RedirectToPdf = "redirecttopdf";
        public const string GetRcm = "getrcm";

        public const string Builder = "builder";
        public const string BrochureGen = "brochuregen";
        public const string BuilderShowCaseList = "buildershowcaselist";
        public const string Brochuretab = "iframe/brochuretab";

        public const string NewHomeSourceTv = "newhomesourcetv";
        public const string GetallBrochuresPage = "getallbrochurespage";
        public const string RecomendationsPage = "rcommsv2";

        //Newsletter and Registration page (consumer and pro)
        public const string SignUp = "newslettersignup";
        public const string eBook = "ebook";
        public const string OwnerStories = "ownerstories";
        public const string OwnerStoriesCuration = "ownerstories-curation";

        public const string ShowFullSite = "showfullsite";
        public const string ShowMobileSite = "showmobilesite";
        public const string QuickView = "communityresults/quickviewmobile";
        public const string GetHomeImages = "homedetail/gethomeimages";
        public const string GetFloorPlanGallery = "homedetail/getfloorplangallery";
        public const string ContactBuilderModal = "freebrochure/contactbuildermodal";

        public const string UpdateAccount = "account/updateaccount";
        public const string ManageAccount = "account/managemyaccount";
        public const string GetBuilderShowCaseGallery = "buildershowcase/getgallery";
        public const string BuilderShowCaseMoreLocations = "buildershowcase/moremetroareas";
        public const string BuilderShowCaseAskQuestion = "buildershowcase/askaquestion/";
        public const string ComunityDetailFullImageViewer = "communitydetail/mediaviewer";
        public const string HomeDetailFullImageViewer = "Homedetail/mediaviewer";
        public const string SyntheticGeographies = "syntheticgeographies/report";


        public const string Comunidades = "comunidades";
        public const string Casas = "casas";
        public const string Comunidad = "comunidad";
        public const string Casa = "casa";
        public const string CentroDeRecursos101 = "guia-para-casas-nuevas/casa-nueva-101";
        
        public static string GetResourceCenterLink()
        {
            return GetResourceCenterLink((PartnersConst)NhsRoute.BrandPartnerId);
        }

        public static string GetResourceCenterLink(int partnerId)
        {
            return GetResourceCenterLink((PartnersConst)partnerId);
        }

        public static string GetResourceCenterLink(PartnersConst partner)
        {
            return AgentResources;
        }
    }

}
