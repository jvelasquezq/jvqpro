namespace Nhs.Library.Constants
{
    // for url params: these are sub pages
   public  class CommunityDetailPages
    {
       public const string CommunityHomeResults = "homes";
       public const string CommunityDetails = "details";
       public const string CommunityMedia = "media";
       public const string CommunityAmenities = "amenities";
       public const string DrivingDirections = "directions";
    }
}
