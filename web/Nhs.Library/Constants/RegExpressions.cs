namespace Nhs.Library.Constants
{
    public class RegExpressions
    {
        public const string Email = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";

        public const string InternationalPhoneNumber = "^\\d{1}-\\d{3}-\\d{3}-\\d{4}$";
    }
}
