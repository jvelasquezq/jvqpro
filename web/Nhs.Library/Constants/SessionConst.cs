namespace Nhs.Library.Constants
{
    public class SessionConst
    {
        public const string LastError = "LastError";
        public const string LastSearchUrl = "LastSearchUrl";
        public const string PriceLow = "PriceLow";
        public const string City = "City";
        public const string CommResultsView = "CommResultsView";
        public const string QuickCResultsView = "QuickCResultsView";
        public const string HomeResultsView = "HomeResultsView";
        public const string HomeResultsFromApiView = "HomeResultsFromApiView";
        public const string SendFriendData = "SendFriendData";
        public const string HasSentToFriend = "HasSentToFriend";
        public const string HasToOpenModalWindow = "HasToOpenModalWindow";
        public const string ModalWindowUrl = "ModalWindowUrl";
        public const string CommPageNumber = "CommPageNumber";
        public const string MlsPageNumber = "MlsPageNumber";
        public const string BuilderHomesUrl = "BuilderHomesUrl";
        public const string ResultsPerPage = "ResultsPerPage";
        public const string PreviousPage = "PreviousPage";
        public const string HomePageSelectedTab = "HomePageSelectedTab";
        public const string QueryParams = "QueryParams";
        public const string AdBuilderIds = "AdBuilderIds";
        public const string TempUserProfile = "TempUserProfile";
        public const string NhsPropertySearchParams = "NhsPropertySearchParamsV2";
        public const string SearchParametersV2 = "SearchParametersV2";
        public const string PreviewsGroupingBarValueComDetail = "PreviewsGroupingBarValueComDetail";
        public const string PreviewsGroupingBarValue = "PreviewsGroupingBarValue";

        public const string IsBasicListingGroup = "IsBasicListingGroup";
        public const string IsBasicCommunityGroup = "IsBasicCommunityGroup";
        public const string IsNearbyBasicListingGroup = "IsNearbyBasicListingGroup";
        public const string IsNearbyBasicCommunityGroup = "IsNearbyBasicCommunityGroup";

        
    }
}
