/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright (c) 2001-2006
 **by BuilderHomeSite Inc. ( http://www.BuilderHomeSite.com )
 * ========================================================
 * Author: RMenon
 * Date: 10/24/2006 14:38:56
 * Purpose:
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
namespace Nhs.Library.Constants
{
    public class HomeDetailPages
    {
        public const string HomeDetails = "details";
        public const string HomeMedia = "media";
        public const string FloorPlan = "floorplan";
        public const string DrivingDirections = "directions";
    }
}
