﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Nhs.Library.Constants
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum SearchResultsPageType
    {
        CommunityResults,
        HomeResults,
        BuilderResults
    }
}
