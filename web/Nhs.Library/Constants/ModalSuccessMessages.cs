namespace Nhs.Library.Constants
{
    public class ModalSuccessMessages
    {
        public const string SendToFriend = "Your Send to A Friend message was sent.";
        public const string UpdateAccount = "Your account information has been updated.";
    }
}
