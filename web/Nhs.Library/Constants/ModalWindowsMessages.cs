namespace Nhs.Library.Constants
{
    public static class ModalWindowsMessages
    {

        public const string AccountCreated = "Your account has been activated.";
        public const string SearchAlertCreated = "Your Search Alert was saved.";
         
    }
}
