﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;

namespace Nhs.Library.Constants
{
    public class EktronConstants
    {
        //Article Names
        public const string MPASection ="Multipart Article Section Template";
        public const string MPAContainer = "Multipart Article Container Template";
        public const string FhSlideshow = "Featured Homes Slideshow";
        public const string Author = "Author";
        public const string AuthorLandingPage = "ALP";
        
        //******************************************************************************
        //Localizable Strings
        public static string ResourceCenterMainTitle
        {
            get
            {
                return "Articles & Advice";
            }
        }                    

        //******************************************************************************
        //Global Configuration 
        public const string GlobalSettingsArticleName = "Global Settings";       
    }
}
