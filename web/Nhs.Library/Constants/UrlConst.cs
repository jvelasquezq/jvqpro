namespace Nhs.Library.Constants
{
    public class UrlConst
    {
        // Please Update enum NhsLinkParams for intellisense with nhs:link
        // Please Update Getters in NhsUrl too
        #region Members
        // Property Search
        public const string Lat = "lat";
        public const string Lng = "lng";
        public const string CommunityStatus = "communitystatus";
        public const string GoogleAnalytics = "googleanalytics";
        public const string ClientRedirect = "clientredirect";
        public const string State = "state";
        public const string StateName = "statename";
        public const string MarketID = "market";
        public const string MarketName = "marketname";
        public const string Area = "area";
        public const string City = "city";
        public const string CityId = "cityid";
        public const string CityNameFilter = "citynamefilter";
        public const string PostalCode = "postalcode";
        public const string PostalCodeFilter = "postalcodefilter";
        public const string County = "county";
        public const string CountyNameFilter = "countynamefilter";
        public const string CommunityID = "community";
        public const string BuilderID = "builder";
        public const string BuilderName = "buildername";
        public const string BrandId = "brandid";
        public const string BrandName = "brandname";
        public const string SchoolDistrictId = "schooldistrictid";
        public const string SchoolDistrictName = "schooldistrictname";
        public const string ListingTypeFlag = "listingtype";

        public const string Gated = "gated";
        public const string Pool = "pool";
        public const string GolfCourse = "golfcourse";
        public const string NatureAreas = "natureareas";
        public const string Views = "views";
        public const string View = "view";
        public const string WaterFront = "waterfront";
        public const string SportsFacilities = "sportsfacility";
        public const string Parks = "parks";
        public const string Adult = "adult";
        public const string ApptModal = "apptModal";
         
        public const string PriceHigh = "pricehigh";
        public const string PriceLow = "pricelow";
        public const string NumOfBeds = "bedrooms";
        public const string NumOfBaths = "bathrooms";
        public const string SqFtLow = "squarefeet";
        public const string NumOfGarages = "garages";
        public const string NumOfLiving = "livingareas";

        public const string HomeType = "hometype";
        public const string TownHomes = "townhomes";
        public const string Stories = "stories";
        public const string MasterBedLocation = "masterbedlocation";
        public const string BrandID = "brandid";
        public const string HomeStatus = "homestatus";
        public const string QuickMoveIn = "inventory";

        public const string OriginLat = "originlat";
        public const string OriginLong = "originlong";

        public const string PhoneNumber = "phonenumber";

        // Others
        public const string HotDeals = "hotdeals";
        public const string Inventory = "inventory";
        public const string SpecHomes = "spechomes";
        public const string PartnerID = "partnerid";
        public const string ParentCommunityID = "parentcommunity";
        public const string CommunityList = "communitylist";
        public const string CommunityName = "communityname";
        public const string SchoolDistrict = "schooldistrict";
        public const string SpecID = "specid";
        public const string SpecList = "speclist";
        public const string ListingID = "listingid";
        public const string PlanID = "planid";
        public const string PlanList = "planlist";
        public const string CityList = "citylist";
        public const string PriceRangeID = "pricerangeid";
        public const string SearchSeed = "searchseed";
        public const string SearchRange = "range";
        public const string ZipCode = "zipcode";
        public const string UserNameAndLastName = "name";
        public const string UserZip = "userzip";
        public const string Availability = "availability";
        public const string SearchType = "searchtype";
        public const string LeadType = "leadtype";
        public const string LeadAction = "leadaction";
        public const string LeadComments = "leadcomments";
        public const string Registration = "registration";
        public const string AlertCreated = "alertcreated";
        public const string AlertId = "alertid";
        public const string FromPage = "frompage";
        public const string BuilderList = "builderlist";
        public const string BrandList = "brandlist";
        public const string CampaignID = "campaignid";
        public const string OptionID = "optionid";
        public const string AdultCommunity = "adultcommunity";
        public const string SortBy = "sort";
        public const string PageNumber = "pagenumber";
        public const string Page = "page";
        public const string RecordCount = "recordcount";
        public const string Count = "count";
        public const string SortOrder = "sortorder";
        public const string Cached = "cached";
        public const string Refer = "refer";
        public const string ExternalURL = "externalurl";
        public const string LogEvent = "logevent";
        public const string EventCode = "eventcode";
        public const string AmenityType = "amenity"; // amenity type param for amenity index
        public const string Type = "type";//Image Type
        public const string CommunityTabs = "view";//Image Type
        public const string PlannerTabs = "view"; //Image Type
        public const string HomeTabs = "view";  //currently communityTabs and HomeTabs have same param name view
        public const string RegisterPasswordOnly = "registerpasswordonly";
        public const string NextPage = "nextpage";
        public const string NextPageName = "nextpagename";
        public const string HideShell = "hideshell";
        public const string FirstName = "firstname";
        public const string LastName = "lastname";
        public const string License = "license";
        public const string Phone = "phone";
        public const string Mobile = "mobile";
        public const string Zip = "zip";
        public const string AgencyName = "agencyname";
        public const string AgentId = "agentid";
        public const string RequestID = "requestid";
        public const string PromoID = "promoid";
        public const string Campaign = "Campaign";
        public const string SignInOption = "signinoption";
        public const string Email = "email";
        public const string DisplayMessage = "displaymessage";
        public const string FriendName = "friendname";
        public const string FromName = "fromname";
        public const string Note = "leadcomments";
        public const string MoveInDate = "moveindate";
        public const string FinancialPref = "financialpref";
        public const string MoveReason = "movereason";
        public const string CloseWindow = "closewindow";
        public const string LeadSent = "leadsent";
        public const string RegFromLeads = "regfromleads";
        public const string UserId = "userid"; //changed from guid to userid.
        public const string Deactivate = "deactivate";
        public const string ComingSoon = "comingsoon";
        public const string ComingSoonCount = "comingsooncount";
        public const string AdvertiserID = "advertiserid";
        public const string BuilderCommunityIds = "buildercommunitylist";
        public const string ToPage = "topage";
        public const string LeadsGenerated = "leadsgenerated";
        public const string PresentationID = "presentationid";
        public const string Confirmation = "confirmation";
        public const string AgeRestricted = "agerestricted";
        public const string ShowMediaTab = "showmediatab";
        public const string ShowAmenitiesTab = "showamenitiestab";
        public const string ShowViewHomesTab = "showviewhomestab";
        public const string RedirectUrl = "url";
        public const string Category = "category";
        public const string SubCategory = "subcategory";
        public const string CategoryId = "categoryid";
        public const string SubCategoryId = "subcategoryid";
        public const string ArticleId = "articleid";
        public const string Article = "article";
        public const string SearchText = "searchtext";
        public const string SearchPage = "searchpage";
        public const string SessionId = "sessionid";
        public const string MoreResultsFlag = "moreresultsflag";
        public const string ZoomLevel = "zoom";
        public const string MinLat = "minlat";
        public const string MinLng = "minlng";
        public const string MaxLat = "maxlat";
        public const string MaxLng = "maxlng";
        public const string UseSession = "usesession";  
        public const string Message = "message";
        public const string AutoRedirect = "autoredirect";
        public const string ShowAds = "showads";
        public const string SessionToken = "sessiontoken";
        public const string DataToken = "datatoken";
        public const string HashType = "hashtype";
        public const string DateTime = "dt";
        public const string SsoUrlReferrer = "ssourlreferrer";
        //used in community detail and home detail to save listing
        //to planner
        public const string AddToProfile = "addtoprofile"; 
        // Please Update enum NhsLinkParams for intellisense with nhs:link
        // Please Update Getters in NhsUrl too

        public const string PreviewShell = "preview"; //to preview site from extranet mgmt tool
        public const string SpecialOfferComm = "specialofferc";
        public const string SpecialOfferListing = "specialofferl";
        public const string HotHomeComm = "hothomec";
        public const string HotHomeListing = "hothomel";
        public const string WebId = "webid";
        public const string FormName = "formname";
        public const string PopupWindow = "popupwindow";
        public const string SupportEmail = "supportemail";
        public const string UserAlreadyExists = "useralreadyexists";
        public const string FriendEmail = "friendemail";
        public const string ChangedToActiveUser = "changedtoactiveuser";
        public const string SearchLocation = "searchlocation";

        public const string ErrorStatusCode = "statuscode";
        public const string GreenProgram = "green";
        public const string FeaturedListingId = "featuredlisting";
        public const string Bedrooms = "bedrooms";
        public const string AlertSelected = "alertselected";


        public const string RequestUniqueKey = "requestuniquekey";
        public const string IsModal = "ismodal";
        public const string CommunityListGoogle = "communitylistgoogle";
        public const string ReferrerRedirect = "referrerredirect";
        public const string Tab = "tab";
        public const string IsMvc = "IsMvc";
        public const string IsBilled = "IsBilled";


        public const string HeadlineText = "HeadlineText";
        public const string DefaultChecked = "DefaultChecked";
        public const string SubheadText = "SubheadText";
        public const string MaxRecs = "MaxRecs";
        public const string ShowRCM = "ShowRCM";
        public const string SendMeMoreNewHomes = "SendMeMoreNewHomes";
        public const string IsMultiBrochure = "IsMultiBrochure";
        public const string ShowImageSequence = "ShowImageSequence";

        public const string ShowMyClients = "showmyclients";

        public const string PromotionType = "promotiontype";
        public const string HasEvents = "hasevents";
        public const string NewSession = "newsession";
        public const string ShowReturnUserModal = "showrum";
        public const string PopUpHeaderRetUser = "popupheaderretuser";
        public const string PopUpExtraInfoRetUser = "popupextrainforetuser";
        public const string MatchEmail = "matchemail";
        public const string HUrl = "hurl";
        public const string ShowRumTraceCookie = "showrumtracecookie";
        public const string MapSearch = "mapsearch";
        public const string Welcome = "welcome";

        
        #endregion             

        #region Helper Methods
        //used in NHSLink Params to convert UrlParamNames enum to actual UrlParameter  values
        public static string GetFieldValue(string propertyName)
        {
            return typeof(UrlConst).GetField(propertyName).GetValue(null) as string;
        }


     
        #endregion

    }
}
