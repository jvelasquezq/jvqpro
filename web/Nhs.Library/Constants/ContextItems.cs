namespace Nhs.Library.Constants
{
    public class ContextItems
    {
        public const string AdController = "AdController";
        public const string PartnerLayoutConfig = "PartnerLayoutConfig";
        public const string CurrentUrl = "CurrentUrl";
        public const string ReferrerUrl = "ReferrerUrl";
        public const string CopyOfCurrentUrl = "CopyOfCurrentUrl";
        public const string Article = "Article";
        public const string Shell = "Shell";
        public const string CurrentMarketId = "CurrentMarketId";
        public const string CurrentMarketName = "CurrentMarketName";
    }
}
