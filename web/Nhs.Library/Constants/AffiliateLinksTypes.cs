﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Library.Constants
{
    public class AffiliateLinksTypes
    {
        #region Members

        public const string FreeCreditScore = "CreditScoreLink";
        public const string MortgageRates = "MortgageRates";
        public const string CalculateMortgagePayments = "CalculateMortgageRates";

        #endregion
    }
}
