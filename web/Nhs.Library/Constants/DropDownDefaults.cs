namespace Nhs.Library.Constants
{
    public class DropDownDefaults
    {
        public const string City = "No preference";
        public const string CityHbaPartner = "Please Choose";
        public const string Area = "Choose an area";
        public const string State = "Choose a state";
        public const string Any = "Any";
        public const string MinPrice = "No minimum";
        public const string MaxPrice = "No maximum";
        public const string ChooseOne = "Choose one";
        public const string PriceRange = "Select a range";
        public const string SrchAsssitantMin = "Minimum";
        public const string SrchAsssitantMax = "Maximum";
        public const string AllCities = "All Cities";
        public const string SelectRange = "Select a range";
        public const string AllSchoolDistricts = "All School Districts";
        public const string AllCommunities = "All Communities";
        public const string AllBuilders = "All Builders";
        public const string SelectPrice = "Select a price";
    }
}
