namespace Nhs.Library.Constants
{
    /// <summary>
    /// CTA Codes
    /// </summary>
    public static class DataLayerConst
    {
        public const string CtaCode = "ctaCode";
    }
}