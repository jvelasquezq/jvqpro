/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright (c) 2001-2006
 **by BuilderHomeSite Inc. ( http://www.BuilderHomeSite.com )
 * ========================================================
 * Author: RMenon
 * Date: 10/24/2006 14:39:09
 * Purpose:
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
namespace Nhs.Library.Constants
{
    public class HomeTabNames
    {
        #region Members

        public const string PhotosAndTours = "Photos & Tours";
        public const string HomeDetails = "Home Details";
        public const string FloorPlan = "Floor Plan";
        public const string DrivingDirection = "Driving Directions";

        #endregion
    }   
}
