﻿
namespace Nhs.Library.Constants
{
    public enum LeadSectionStateConst
    {
        LoggedInPaid = 1,
        LoggedInUnpaid = 2,
        LoggedOutPaid = 3,
        LoggedOutUnpaid = 4
    }
}
