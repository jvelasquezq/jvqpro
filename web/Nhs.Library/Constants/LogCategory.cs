namespace Nhs.Library.Constants
{
    public class LogCategory
    {
        public const string Default = "Default";
        public const string Leads = "Leads";
        public const string DataProvider = "DataProvider";
        public const string CustomReportLog = "CustomReportLog";
    }
}
