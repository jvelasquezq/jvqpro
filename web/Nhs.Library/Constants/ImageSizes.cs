﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Nhs.Library.Web.ImageResizer;
using Nhs.Utility.Common;

namespace Nhs.Library.Constants
{
    public class ImageSizeIrsAttribute : Attribute
    {
        public UInt32 ImageWidth { get; set; }
        public UInt32 ImageHeight { get; set; }
        /// <summary>
        /// get the widht height fo the url parameter in the format : ?w=xxx&h=zzz
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("?w={0}&h={1}", ImageWidth, ImageHeight);
        }
    }

    public static class ImageSizes
    {
        [ImageSizeIrs(ImageWidth = 240, ImageHeight = 160)]
        public const string CommDetailThumb = "cdthumb";

        [ImageSizeIrs(ImageWidth = 460, ImageHeight = 307)]
        public const string CommDetailMain = "cdmain";

        [ImageSizeIrs(ImageWidth = 460, ImageHeight = 340)]
        public const string CommDetailNhs = "cdmainnhs";

        public const string QuickViewThumb = "qv";

        [ImageSizeIrs(ImageWidth = 180, ImageHeight = 120)]
        public const string NearbyHome = "homnb";

        [ImageSizeIrs(ImageWidth = 460, ImageHeight = 307)]
        public const string HomeMain = "hommain";

        [ImageSizeIrs(ImageWidth = 460, ImageHeight = 340)]
        public const string HomeMainNhs = "hommainnhs";

        [ImageSizeIrs(ImageWidth = 180, ImageHeight = 120)]
        public const string HomeThumb = "homthumb";

        [ImageSizeIrs(ImageWidth = 390, ImageHeight = 478)]
        public const string FloorPlanMedium = "flpmed";

        [ImageSizeIrs(ImageWidth = 700, ImageHeight = 700)]
        public const string FloorPlanLarge = "flplarge";
        
        [ImageSizeIrs(ImageWidth = 120, ImageHeight = 80)]
        public const string Small = "small";

        [ImageSizeIrs(ImageWidth = 75, ImageHeight = 58)]
        public const string Small75X58 = "75x58";
        
        public const string Medium = "medium";
        public const string Large = "large";

        [ImageSizeIrs(ImageWidth = 180, ImageHeight = 120)]
        public const string Spotlight = "spotlight";
        
        public const string Results = "results";
        public const string BasicListingHomeMain = "blhmmain";
        public const string BasicListingHomeThumb = "blhmtb";
        public const string BasicListingAgentPhoto = "blagttb";
        public const string BasicListingBrokerLogo = "blbkltb";

        //Brand Showcase
        public const string BuilderShowcaseLarge = "bsclarge";
        public const string BuilderShowcaseThumb = "bscthumb";
        public const string BuilderShowcaseAward = "bscaward";

        private static readonly IDictionary<string, ResizeCommands> Sizes = new Dictionary<string, ResizeCommands>();

        public static string RemoveAllSizes(this string image)
        {
            if (Sizes.Any())
            {
                image = Sizes.Aggregate(image, (current, size) => current.Replace(size.Key + "_", ""));
            }
            return image;
        }

        static ImageSizes()
        {
            FillAll();
        }

        private static void FillAll()
        {
            if (Sizes.Any())
            {
                return;
            }

            var type = typeof(ImageSizes);

            var internalConst = type.GetFields(BindingFlags.Static | BindingFlags.Public).Where(f => f.IsLiteral);

            foreach (var item in internalConst)
            {
                var valueItem = item.GetValue(type).ToType<string>();
                
                    try
                    {
                        var attribute = item.GetCustomAttributes(typeof(ImageSizeIrsAttribute), true).FirstOrDefault() as ImageSizeIrsAttribute;
                        if (attribute != null)
                        {
                            var resizeCommands = new ResizeCommands
                            {
                                Width = attribute.ImageWidth.ToType<int>(),
                                Height = attribute.ImageHeight.ToType<int>()
                            };
                            Sizes.Add(valueItem, resizeCommands);
                        }
                    }
                    catch (Exception)
                    {
                    }
                
            }
        }

        /// <summary>
        /// Get the ResizeCommands of the value const define in this class
        /// </summary>
        /// <param name="sizeImage">is a const string value defined in this class</param>
        /// <param name="changeExtension"></param>
        /// <param name="format">image extension to be render</param>
        /// <param name="useWhiteBackgroundColor">add a white Background Color to the image</param>
        /// <returns>The ResizeCommands with the values otherwize null</returns>
        public static ResizeCommands GetResizeCommands(string sizeImage, bool changeExtension = false,ImageFormat format = ImageFormat.Jpg, bool useWhiteBackgroundColor = false)
        {            
            var size = useWhiteBackgroundColor 
                ? new ResizeCommands() { Width = 460, Height = 307, Format = format, BackgroundColor = "white"} 
                : new ResizeCommands() { Width = 460, Height = 307, Format = format };

            if (!Sizes.ContainsKey(sizeImage))
            {
                if (changeExtension)
                {
                    size.Format = format;
                }
                return size;
            }

            size = Sizes[sizeImage];

            if (changeExtension)
            {
                size.Format = format;
            }

            if (useWhiteBackgroundColor)
            {
                size.BackgroundColor = "white";
            }

            return size;
        }
    }
}
