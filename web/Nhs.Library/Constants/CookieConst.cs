namespace Nhs.Library.Constants
{
    public class CookieConst
    {
        public const string ProfileCookie = "Profile";
        public const string PersonalCookie = "Personal";
        public const string GoogleUtmzCookie = "__utmz";
        public const string GoogleUtmaCookie = "__utma";
        public const string LastCommunityView = "LastCommunityViewed";
        public const string LastMarketView = "LastMarketViewed";       
        public const string EncEmail = "EncEmail";
        public const string PreviosSessionIdGen = "PreviousSessionId";
        public const string MrisInterstitial = "MrisInterstitial";
        public const string RedirectFromRefer = "301FromRefer"; 

        /// <summary>
        /// Used in terms of Service for MRED partner
        /// </summary>
        public const string MredTempUserProfile = "MredTempUserProfile";
        /// <summary>
        /// Used in terms of Service for East Bay partner
        /// </summary>
        public const string EastBayTempUserProfile = "EastBayTempUserProfile";

        public const string ShowEbookSection = "ShowEbookSection";
        public const string EbookSectionCounter = "EbookSectionCounter";
        
    }
}
