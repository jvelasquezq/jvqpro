namespace Nhs.Library.Constants
{
    public class LeadSubmitPageFields
    {
        public const string FirstName = "first_name";
        public const string LastName = "last_name";
        public const string Email = "email_address";
        public const string Address = "address1";
        public const string City = "city";
        public const string State = "state";
        public const string PostalCode = "postal_code";
        public const string Phone = "day_phone";
        public const string PhoneExtension = "dayphoneExt";
        public const string Title = "title";
        public const string WillMoveInDays = "move_in_days";
        public const string FinancingPref = "financing_preference";
        public const string Comment = "comment_text";

       

       

    }
}
