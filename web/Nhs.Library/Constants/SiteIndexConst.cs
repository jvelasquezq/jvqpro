using System.Security.Policy;

namespace Nhs.Library.Constants
{
    public class SiteIndexConst
    {
        public const string RootFolder = @"SEOContent/SiteIndex/";

        public const string ArticleFolder = RootFolder + @"Articles/";
        public const string LinksFolder = RootFolder + @"Articles/";
        public const string MetaFolder = RootFolder + @"Metas/";
        public const string MarketMetaFolder = RootFolder + @"Metas/Market/";
        public const string MarketNewHomesFolder = RootFolder + @"MarketNewHomes/";

        public const string AmenitiesGolfFolder = @"StateAmenities/Golf/";
        public const string AmenitiesCondoFolder = @"StateAmenities/TownHomes/";
        public const string AmenitiesWaterfrontFolder = @"StateAmenities/Waterfront/";

        public const string SiteIndexStateFolder = @"StateIndex/";
        public const string SiteIndexFolder = @"SiteIndex/";
        public const string StateBrandFolder = @"StateBuilder";

        public const string StateBuilder = @"StateBuilder/";
    }
}
