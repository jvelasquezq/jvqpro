namespace Nhs.Library.Constants
{
    public class LogImpressionConst
    {
        public const string FacebookCreateAccount = "FB_AUTH";
        public const string FacebookLogin = "FB_LOGIN";
        public const string BasicListingBrokerSiteUrl = "BLSRCLINK";
        public const string LeadFormSubmit = "LFSUB";
        public const string CreateAlertSubmit = "CASUB";
        public const string LeadFormThanks = "LFTHANKS";
        public const string HomeDetailEBR = "EBRHD";
        public const string HomeDetailIPIX = "IPIXHD";
        public const string HomeDetailEVW = "EVWHD";
        public const string HomeDetailBuilder = "BUILDHD";
        public const string HomeDetailBuilderPreview = "BUILDHDPRVW";
        public const string HomeDetailBuilderFooter = "BUILDHDFT";
        public const string CommunityDetailBuilder = "BUILDCD";
        public const string CommunityDetailBuilderPreview = "BUILDCDPRVW";
        public const string CommunityDetailSchool = "SDCD";
        public const string SpotLightHomes = "HOMESL";
        public const string HomeResults = "HOMEHR";
        public const string HomeDetail = "HOMEHD";
        public const string SimilarHomes = "HOMESIM";
        public const string BoylResultsBuilder = "BUILDBR";
        public const string BuilderLogo = "BUILDBR";
        public const string BuilderLogoPreviewMode = "BUILDBRPRVW";
        public const string DrivingDirectionsCommunity = "FACTCM";
        public const string DrivingDirectionsHome = "FACTHD";
        public const string IdeaCenter = "IDEACTR";
        public const string CommunityBuilderPhone = "BUILDPHCM";
        public const string CallCommunityDetail = "CALLCD";
        public const string CallHomeDetail = "CALLHD";

        public const string HomeBuilderPhone = "BUILDPH";

        public const string FeaturedSpotlightView = "BASEARCH";
        public const string FeaturedSpotlightClick = "FEATCOMBS";

        public const string FeaturedListingView = "COMFLR";
        public const string FeaturedListingClick = "BUILDFLC";

        public const string CommunityDetailPageLoad = "CDPL";
        public const string BasicCommunityDetailPageLoad = "bcDetailView";
        public const string BasicHomeImpression = "bcHomeImpression";

        public const string CommunityDetailHomeSearchViews = "COMHR";
        public const string FooterBuilderLogo = "BUILDBRFT";
        public const string FooterBuilderPhone = "BUILDPHFT";
        public const string FooterCommunityDetails = "COMDETFT";
        public const string FooterBuilderContact = "CONBLDRFT";

        public const string CommunityResults = "COMR";
        public const string CommunityHomeResults = "COMHR";
        public const string CommunityView = "COMVIEW";
        public const string MapHoverBox = "MAPHOV";
        public const string FreeBrochure = "FBROCH";
        public const string RequestInfo = "RQINFO";
        public const string RequestPromoInfo = "RQPINFO";
        public const string RequestAppt = "RQAPPT";
        public const string CommRequestAppt = "CDRQAPPT";
        public const string HomeRequestAppt = "HDRQAPPT";
        public const string ContactBuilder = "CONBLDR";

        public const string CaFreeBrochure = "CAFBROCH";
        public const string CaRequestInfo = "CARQINFO";
        public const string CaRequestPromoInfo = "CARQPINFO";
        public const string CaRequestAppt = "CARQAPPT";
        public const string CaContactBuilder = "CACONBLDR";

        public const string HovFreeBrochure = "HOVFBROCH";
        public const string HovRequestPromoInfo = "HOVRQPINFO";
        public const string HovRequestAppt = "HOVRQAPPT";

        public const string SEOStateBuilder = "BUILDST";
        public const string SEOMarketBuilder = "BUILDMK";

        public const string OffsiteVideo = "VDCT";
        public const string VirtualTour = "HRVTCT";
        public const string PlanViewer = "HRPVCT";

        public const string SearchAlert = "SEARCHALERT";
        public const string CreateAccountAlertChangeSettings = "CALCHNSTT";

        #region Facets
        //Facets
        public const string FacetPrice = "FAPRICE";
        public const string FacetHomesizeStudio1Br = "FAHS1";
        public const string FacetHomesize2Br = "FAHS2";
        public const string FacetHomesize3Br = "FAHS3";
        public const string FacetHomesize4Br = "FAHS4";
        public const string FacetHomesize5Br = "FAHS5";
        public const string FacetHomeTypeSingleFamily = "FAHTSF";
        public const string FacetHomeTypeCondoTownhome = "FAHTCT";
        public const string FacetAmenityGreen = "FAAMGRN";
        public const string FacetAmenityPool = "FAAMP";
        public const string FacetAmenityGolfCourse = "FAAMGO";
        public const string FacetAmenityGated = "FAAMGA";
        public const string FacetSchool = "FASCH";
        public const string FacetPromo = "FAPROMO"; 
        #endregion

        #region Paging
        //Paging
        public const string PageNext = "PAGENEXT";
        public const string PagePrevious = "PAGEPREV";
        public const string PageGo = "PAGEGO"; 
        #endregion

        //Results
        public const string ViewOnMap = "VIEWMAP";

        #region Comm Resutls Sort
        //Comm Results Sort
        public const string SortByLocation = "SLOC";
        public const string SortByPrice = "SPRICE";
        public const string SortByHomeMatches = "SHM";
        public const string SortByBuilder = "SBR";
        public const string SortByCommunityName = "SCOM";
        public const string SortByPromotions = "SPROMO";

        public const string CommResByComingSoon = "CRGO";
        public const string CommResByBoyl = "CRBOYL";
        public const string CommResByBuilder = "CRBR";
        public const string CommResPromoLink = "BUILDPRC";
        public const string CommResPromoLinkPreview = "BUILDPRCPRVW";
        public const string CommResGreenLink = "BUILDGFC";
        public const string CommResGreenLinkPreview = "BUILDGFCPRVW";
        public const string HomeResGreenLink = "BUILDGFH";
        public const string HomeResGreenLinkPreview = "BUILDGFHPRVW";
        public const string HomeResPromoLink = "BUILDPRH";
        public const string HomeResPromoLinkPreview = "BUILDPRHPRVW";
        public const string CommResVideoTour = "CRVDCT";
        public const string MapUpdate = "MAPUPD";
        public const string CommResFreeBrochure = "CRFBROCH";
        public const string HomeResFreeBrochure = "HRFBROCH"; 
        #endregion

        // Tabs
        public const string CommunityDetailsTabs = "CMDETABS";
        public const string HomeDetailsTabs = "HMDETABS";

        // Popups
        public const string PopupWindowShow = "PPWINSHOW";
        public const string PopupWindowHide = "PPWINHIDE";

        #region Emails becons tracking
        // Emails becons tracking
        public const string OpenEmailCreateAccount = "OPMAILACNT";
        public const string OpenEmailBrochure = "OPMAILBRCH";
        public const string OpenEmailSearchAlert = "OPMAILALRT";
        public const string OpenEmailSendToFriend = "OPMAILFRND"; 
        #endregion

        #region Emails clicktrhoughs tracking
        // Emails clicktrhoughs tracking
        public const string EmailAccountCreationClickThrough = "CTMAILACNT";
        public const string EmailBrochureClickThrough = "CTMAILBRCH";
        public const string EmailSearchAlertClickThrough = "CTMAILALRT";
        public const string EmailSendFriendClickThrough = "CTMAILFRND";
        public const string EmailRecommendedClickThrough = "CTRCMEMAIL";
        public const string EmailBrandClickThrough = "CTMAILBRAND"; 
        #endregion

        // Envision
        public const string CommOpenEnvision = "ENVCOM";
        public const string HomeOpenEnvision = "ENVHOM";

        //Bankrate
        public const string GetMortgateRatesCommDetail = "MRCD";
        public const string GetMortgateRatesHomeDetail = "MRHD";

        //MortgageMatch
        public const string GetMortgateMatchCommDetail = "MMCD";
        public const string GetMortgateMatchHomeDetail = "MMHD";

        #region Detail
        //Detail
        public const string HomeDetailPageLoad = "HDPL";
        public const string HomeDetailBrochureClickThrough = "CTHDBRCH";
        public const string CommunutyDetailBrochureClickThrough = "CTCDBRCH";
        public const string AddCommunityToUserPlanner = "CDSTC";
        public const string AddHomeToUserPlanner = "HOMSTC";
        public const string CommunityDetailInteractiveFloorPlan = "CDPVCT";
        public const string CommunityDetailVirtualTour = "CDVTCT";
        public const string CommunityDetailExternalVideo = "CDEVCT";
        public const string CommunityResultsExternalVideo = "CREVCT";
        public const string HomeDetailVirtualTour = "HDVTCT";
        public const string HomeDetailInteractiveFloorPlan = "HDPVCT"; 
        #endregion

        //Awards
        public const string AwardsClickThrough = "CTAWARD";

        //NextSteps
        public const string NextStepsClickSave = "HDNSSTF";
        public const string NextStepsClickPrint = "HDNSPRN";
        public const string NextStepsClickBrochure = "HDNSFB";
        public const string NextStepsClickSeePhone = "HDNSSPH";
        public const string NextStepsClickWebsite = "HDNSBLDREF";
        public const string NextStepsClickLogo = "HDNSBLDREF";

        public const string NextStepsComClickSave = "CDNSSTF";
        public const string NextStepsComClickPrint = "CDNSPRN";
        public const string NextStepsComClickBrochure = "CDNSFB";
        public const string NextStepsComClickSeePhone = "CDNSSPH";
        public const string NextStepsComClickWebsite = "CDNSBLDREF";
        public const string NextStepsComClickLogo = "CDNSBLDREF";


        #region BasicLsiting
        //BasicLsiting
        public const string PhoneNumberView = "BLPNV";
        public const string BasicListingHomeResults = "BLHomR";
        public const string BasicListingCommunityResults = "BLCOMR";
        public const string BasicListingCommunityDetail = "BLDetail";
        public const string BasicListingCommunityMapping = "BLMAPV";
        public const string BasicListingCommunityShowRoute = "BLDR"; 
        #endregion

        #region ListHub Log
        /// <summary>
        /// This one is for ListHub Log only
        /// </summary>
        public const string BasicListingCommunitySeasrchDisplay = "SEARCH_DISPLAY";

        /// <summary>
        /// This one is for ListHub Log only
        /// </summary>
        public const string BasicListingCommunityDetailPageViewed = "DETAIL_PAGE_VIEWED";

        /// <summary>
        /// This one is for ListHub Log only
        /// </summary>
        public const string BasicListingCommunityAgentPhoneClicked = "AGENT_PHONE_CLICKED";

        #endregion

        /// <summary>
        /// Basic Communities
        /// </summary>
        public const string BasicCommsCommunityResults = "bcImpression";

        #region Pro7
        //Pro7
        public const string SendBrochure = "Pro_Broch";
        public const string GeneralInquiry = "Pro_Inquiry";
        public const string RequestAppointment = "Pro_Appt";
        public const string RequestCommision = "Pro_Commi";
        
        public const string MatchEmail = "MatchEmail";
        public const string EBookEmailKindle = "EBookEmailKindle";
        public const string EBookEmailOtherDevice = "EBookEmailOtherDevice"; 
        #endregion


        public const string MobileHomeDetailShowRoute = "HDSHWRT";
        public const string MobileCommunityDetailShowRoute = "CDSHWRT";
        public const string MobileCommunityResultsShowRoute = "SHOWROUTE_CR";
        public const string MobileBoylResultsShowRoute = "SHOWROUTE_BR";

        
        public const string MobileCommunityDetailShareByEmail = "COMSTF";
        public const string MobileHomedatilDetailShareByEmail = "HOMSTF";

        public const string MobileCommunityDetailSocialMediaShare = "CDSMS";
        public const string MobileHomedatilDetailSocialMediaShare = "HDSMS";
        public const string MobileCommunityResultsSocialMediaShare = "CRSMS";
        public const string MobileBoylResultsSocialMediaShare = "BRSMS";

        public const string MobileCommunityResultsCallSalesOffice = "CRPHONECALL";
        public const string MobileCommunityDetailCallSalesOffice = "CDPHONECALL";
        public const string MobileHomeDetailCallSalesOffice = "HDPHONECALL";
        public const string MobileBoylResultsCallSalesOffice = "BRPHONECALL";

        public const string MobileSaveToFavoriteCommunityResults = "SAVETOFAV_CR_QV";
        public const string MobileSaveToFavoriteBoylResults = "SAVETOFAV_BR_QV";


        public const string SaveToFavoriteCommunityResults = "SAVETOFAV_CR";
        public const string SaveToFavoriteHomeResults = "SAVETOFAV_HR";
        public const string BuilderWebsiteLink = "BSCT";

        public const string DirectLead = "DirectLead";
        public const string RecommendedLead = "RecoLead";
        

        
    }
}
