﻿namespace Nhs.Library.Constants.Route
{
    public class Routes
    {
        public const string DummyResultsWithMarketStateNames = "DummyResultsWithMarketStateNames";
        public const string DummyResultsWithMarketId = "DummyResultsWithMarketId";
        public const string DummyResultsCatchAll = "DummyResultsCatchAll";

        public const string CommunityResultsCatchAll = "CommunityResultsCatchAll";
        public const string CommunityDetail = "CommunityDetail";
        public const string HomeDetailSpec = "HomeDetailSpec";
        public const string HomeDetailPlan = "HomeDetailPlan";

        public const string BoylSearch = "BoylSearch";
        public const string BoylResultsMarket = "BoylResultsMarket";
        public const string BoylResultsPostalCode = "BoylResultsPostalCode";
    }
}