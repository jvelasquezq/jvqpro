/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright (c) 2001-2006
 **by BuilderHomeSite Inc. ( http://www.BuilderHomeSite.com )
 * ========================================================
 * Author: RMenon
 * Date: 02/22/2007 20:36:04
 * Purpose:
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
using System.Web.UI;
using System.Web.UI.Adapters;
using System.Web.UI.HtmlControls;
using Nhs.Library.Common;

namespace Nhs.Library.Web.Adapters
{
    public class HtmlLinkControlAdapter : ControlAdapter
    {
        protected override void Render(HtmlTextWriter writer)
        {
            HtmlLink link = Control as HtmlLink;
            link.Href = ControlUtil.ResolveResourceUrl(link.Href);
            base.Render(writer);

        }
    }
}
