/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright (c) 2001-2006
 **by BuilderHomeSite Inc. ( http://www.BuilderHomeSite.com )
 * ========================================================
 * Author: RMenon
 * Date: 10/30/2006 16:11:38
 * Purpose:
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.Adapters;
using Nhs.Library.Common;

namespace Nhs.Library.Web.Adapters
{
    public class ImageControlAdapter : WebControlAdapter
    {
        protected override void Render(HtmlTextWriter writer)
        {
            Image img = Control as Image;
            img.ImageUrl = ControlUtil.ResolveResourceUrl(img.ImageUrl);
            //the following is to remove the annoying inline style attrib
            //I tried img.Attribute.Remove.. but its on base.render that it adds
            //so this is a way I could think of. 
            StringBuilder result = new StringBuilder();
            base.Render(new HtmlTextWriter(new StringWriter(result))); // renders to result stringbuilder
            writer.Write(new Regex(@"style=.*""\s").Replace(result.ToString(), string.Empty)); //replace style

        }
       
    }
}
