/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright (c) 2001-2006
 **by BuilderHomeSite Inc. ( http://www.BuilderHomeSite.com )
 * ========================================================
 * Author: RMenon
 * Date: 02/22/2007 19:13:56
 * Purpose:
 * Edit History (please mention edits succinctly):
 * =========================================================
 */

using System.Web.UI;
using System.Web.UI.Adapters;
using System.Web.UI.HtmlControls;
using Nhs.Library.Common;

namespace Nhs.Library.Web.Adapters
{
    public class HtmlImageControlAdapter : ControlAdapter
    {
        protected override void Render(HtmlTextWriter writer)
        {
            HtmlImage image = Control as HtmlImage;
            image.Src = ControlUtil.ResolveResourceUrl(image.Src);
            base.Render(writer);

        }
    }
}
