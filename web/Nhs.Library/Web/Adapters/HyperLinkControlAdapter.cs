/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright (c) 2001-2006
 **by BuilderHomeSite Inc. ( http://www.BuilderHomeSite.com )
 * ========================================================
 * Author: RMenon
 * Date: 02/22/2007 19:14:11
 * Purpose:
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
using System.Web.UI;
using System.Web.UI.Adapters;
using System.Web.UI.WebControls;
using Nhs.Library.Common;

namespace Nhs.Library.Web.Adapters
{
    public class HyperLinkControlAdapter : ControlAdapter
    {
        protected override void Render(HtmlTextWriter writer)
        {
            HyperLink link = Control as HyperLink;
            link.ImageUrl = ControlUtil.ResolveResourceUrl(link.ImageUrl);
            link.NavigateUrl = link.ResolveUrl(link.NavigateUrl);
            base.Render(writer);

        }
    }
}
