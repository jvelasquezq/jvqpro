﻿using System.Web;

namespace Nhs.Library.Web
{
    public class HttpContextManager
    {
        private static HttpServerUtilityWrapper _current;
        public static HttpServerUtilityWrapper Current
        {

            get { return _current ?? (_current = new HttpServerUtilityWrapper(HttpContext.Current.Server)); }
            set { _current = value; }
        }
    }
}
