﻿using System;
using System.Linq;
using System.Web;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Utility.Common;

namespace Nhs.Library.Web
{
    /// <summary>
    /// Handle all site cookie set, get, delete
    /// </summary>
    public sealed class CookieManager
    {
        /// <summary>
        /// This is the Email of the customer Cryted with CryptoHelper.Encrypt(string plainText, string password)
        /// </summary>
        public static string EncEmail
        {
            get { return GetItem(CookieConst.EncEmail); }
            set { SetItem(CookieConst.EncEmail, value); }
        }

        public static string PersonalCookie
        {
            get { return GetItem(CookieConst.PersonalCookie); }
            set { SetItem(CookieConst.PersonalCookie, value); }
        }

        public static string TraceDebugCookie
        {
            get { return GetItem("TraceDebugCookie"); }
            set { SetItem("TraceDebugCookie", value); }
        }

        /// <summary>
        /// Last community view in the Community Details page
        /// </summary>
        public static string LastCommunityView
        {
            get { return GetItem(CookieConst.LastCommunityView); }
            set { SetItem(CookieConst.LastCommunityView, value); }
        }

        /// <summary>
        /// Last community view in the Community Details page
        /// </summary>
        public static string MrisInterstitial
        {
            get { return GetItem(CookieConst.MrisInterstitial); }
            set { SetItem(CookieConst.MrisInterstitial, value); }
        }

        /// <summary>
        /// Last markey view in the Community Details page
        /// </summary>
        public static string LastMarketView
        {
            get { return GetItem(CookieConst.LastMarketView); }
            set { SetItem(CookieConst.LastMarketView, value); }
        }

        /// <summary>
        /// Previous asp.net session id 
        /// </summary>
        public static string PreviousSessionId
        {
            get { return GetItem(CookieConst.PreviosSessionIdGen); }
            set { SetItem(CookieConst.PreviosSessionIdGen, value); }
        }

        public static string MredTempUserProfile
        {
            get { return GetItem(CookieConst.MredTempUserProfile); }
            set { SetItem(CookieConst.MredTempUserProfile, value); }
        }

        public static string EastBayTempUserProfile
        {
            get { return GetItem(CookieConst.EastBayTempUserProfile); }
            set { SetItem(CookieConst.EastBayTempUserProfile, value); }
        }


        public static bool ShowEbookSection
        {
            get
            {
                var data = GetItem(CookieConst.ShowEbookSection);
                return string.IsNullOrEmpty(data) || data.ToType<bool>();
            }
            set { SetItem(CookieConst.ShowEbookSection, value); }
        }

        public static bool RedirectFromRefer
        {
            get
            {
                var data = GetItem("301FromRefer");
                if (string.IsNullOrEmpty(data))
                    return false;
                else
                    return Convert.ToBoolean(data);
            }
            set { SetItem(CookieConst.RedirectFromRefer, value, DateTime.MinValue); }
        }

        public static string RefreshCacheAuthorization
        {
            get { return GetItem("RefreshCacheAuthorization"); }
            set { SetItem("RefreshCacheAuthorization", value, DateTime.Now.AddMinutes(1)); }
        }

        /// <summary>
        /// Delete Cookie
        /// </summary>
        /// <param name="key">Use values in CookieConst</param>
        public static void DeleteCookie(string key)
        {
            RemoveItem(CookieKeyName(key));
        }

        #region "Private Methods"

        /// <summary>
        /// Build the Cookie Name with the PartnerId as {key}_{partnerid}
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private static string CookieKeyName(string key)
        {
            return string.Format("{0}_{1}", key, Configuration.PartnerId);
        }

        /// <summary>
        /// Get Coockie Values
        /// </summary>
        /// <param name="key">Use values in CookieConst</param>
        /// <returns></returns>
        private static string GetItem(string key)
        {
            var cookie = HttpContext.Current.Request.Cookies[CookieKeyName(key)];
            var cookieValue = string.Empty;
            if (cookie != null)
            {
                cookieValue = cookie.Value;
            }
            return cookieValue;
        }


        /// <summary>
        /// Set Coockie Values
        /// </summary>
        /// <param name="key">Use values in CookieConst</param>
        /// <param name="value">Cookie value</param>
        /// <param name="expires">Cookie expire date</param>
        /// <returns></returns>
        private static void SetItem(string key, object value, DateTime? expires = null)
        {
            HttpCookie cookie = null;

            key = CookieKeyName(key);

            cookie = HttpContext.Current.Response.Cookies[key];
            if (cookie != null)
            {
                cookie.Value = value.ToString();
            }
            else
                cookie = new HttpCookie(key, value.ToString()) { Expires = DateTime.Now.AddYears(100) };


            if (expires.HasValue)
                cookie.Expires = (DateTime)expires;

            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        /// <summary>
        /// Remove Coockie 
        /// </s
        /// ummary>
        /// <param name="key">Use CookieKeyName(key);</param>
        private static void RemoveItem(string key)
        {
            var httpCookie = HttpContext.Current.Response.Cookies[key];
            if (httpCookie != null)
            {
                httpCookie.Value = string.Empty;
                httpCookie.Expires = DateTime.Now.AddDays(-1d);
                HttpContext.Current.Response.Cookies.Add(httpCookie);
            }
        }
        #endregion

    }
}
