using System;
using System.Collections.Generic;
using Nhs.Library.Constants;
using Nhs.Utility.Common;

namespace Nhs.Library.Web
{
    [Serializable]
    public class PersonalCookie
    {
        #region "Members"

        public PersonalCookie()
        {
            BedRooms = string.Empty;
            BathRooms = string.Empty;
            SearchText = string.Empty;
            City = string.Empty;
            State = string.Empty;
        }

        #endregion

        #region "Public Properties"

        public int MarketId { get; set; }
        public int PriceLow { get; set; }
        public int PriceHigh { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string SearchText { get; set; }
        public string BathRooms { get; set; }
        public string BedRooms { get; set; }
        public decimal LastPrice { get; set; }

        public double Lat { get; set; }
        public double Lng { get; set; }
        #endregion

        #region "Constructor"

        #endregion

        #region "Public Methods"

        public static PersonalCookie Load()
        {
            var personalCookie = new PersonalCookie();

            var personalCookieValue = CookieManager.PersonalCookie;
            if (string.IsNullOrEmpty(personalCookieValue)) return personalCookie;
            var personalCookieSplit = personalCookieValue.Split('&');

            foreach (var cookieValue in personalCookieSplit)
            {
                var cookieSplit = cookieValue.Split('=');

                if (cookieSplit.Length != 2) continue;
                var name = cookieSplit[0];
                var value = cookieSplit[1];

                try
                {
                    switch (name)
                    {
                        case UrlConst.MarketID:
                            personalCookie.MarketId = Int32.Parse(value);
                            break;
                        case UrlConst.PriceHigh:
                            personalCookie.PriceHigh = Int32.Parse(value);
                            break;
                        case UrlConst.PriceLow:
                            personalCookie.PriceLow = Int32.Parse(value);
                            break;
                        case UrlConst.City:
                            personalCookie.City = value;
                            break;
                        case UrlConst.State:
                            personalCookie.State = value;
                            break;
                        case UrlConst.SearchText:
                            personalCookie.SearchText = value;
                            break;
                        case UrlConst.NumOfBaths:
                            personalCookie.BathRooms = value;
                            break;
                        case UrlConst.Bedrooms:
                            personalCookie.BedRooms = value;
                            break;
                        case UrlConst.Lat:
                            personalCookie.Lat = Convert.ToDouble(value);
                            break;
                        case UrlConst.Lng:
                            personalCookie.Lng = Convert.ToDouble(value);
                            break;
                    }
                }
                catch
                {
                    // Ignore any errors assigning values to personal cookie. 
                }
            }


            return personalCookie;
        }

        public void Save()
        {
            var personalCookieValue = new List<string>();

            if (MarketId != 0)
            {
                personalCookieValue.Add(UrlConst.MarketID + "=" + MarketId);
            }

            if (Lng.ToType<decimal>() != 0)
            {
                personalCookieValue.Add(UrlConst.Lng + "=" + Lng);
            }

            if (Lat.ToType<decimal>() != 0)
            {
                personalCookieValue.Add(UrlConst.Lat + "=" + Lat);
            }

            if (PriceLow != 0)
            {
                personalCookieValue.Add(UrlConst.PriceLow + "=" + PriceLow);
            }

            if (PriceHigh != 0)
            {
                personalCookieValue.Add(UrlConst.PriceHigh + "=" + PriceHigh);
            }

            if (!string.IsNullOrEmpty(BathRooms))
            {
                personalCookieValue.Add(UrlConst.NumOfBaths + "=" + BathRooms);
            }

            if (!string.IsNullOrEmpty(BedRooms))
            {
                personalCookieValue.Add(UrlConst.Bedrooms + "=" + BedRooms);
            }

            if (!string.IsNullOrEmpty(State))
            {
                personalCookieValue.Add(UrlConst.State + "=" + State);
            }

            if (!string.IsNullOrEmpty(City))
            {
                personalCookieValue.Add(UrlConst.City + "=" + City);
            }

            if (!string.IsNullOrEmpty(SearchText))
            {
                personalCookieValue.Add(UrlConst.SearchText + "=" + SearchText);
            }

            CookieManager.PersonalCookie = string.Join("&", personalCookieValue);
        }
        #endregion
    }
}
