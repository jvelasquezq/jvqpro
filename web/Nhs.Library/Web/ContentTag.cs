﻿using System.Collections.Generic;
using Nhs.Library.Enums;

namespace Nhs.Library.Web
{
    public class ContentTag
    {
        public ContentTag()
        {
            ToTitleCase = true;
        }

        public ContentTag(ContentTagKey tagKey, string tagValue, bool toTitleCase = true)
        {
            TagKey = tagKey;
            TagValue = tagValue;
            ToTitleCase = toTitleCase;
        }

        public ContentTagKey TagKey { get; set; }
        public string TagValue { get; set; }
        public bool ToTitleCase { get; set; }
    }

    public static class ContentTagExtensions
    {
        public static Dictionary<string, string> ToDictionary(this IList<ContentTag> contentTags)
        {
            Dictionary<string, string> tagDict = new Dictionary<string, string>();

            foreach (var contentTag in contentTags)
                tagDict.Add(contentTag.TagKey.Enclose(), contentTag.TagValue);

            return tagDict;
        }
    }
}
