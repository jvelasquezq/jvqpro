using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;

namespace Nhs.Library.Web
{
    public class UserSession
    {
        private static HttpSessionStateBase _current;
        public static HttpSessionStateBase Current
        {
            get { return _current ?? (_current = new HttpSessionStateWrapper(HttpContext.Current.Session)); }
            set { _current = value; }
        }

        #region "Properties"


        public static IProfile UserProfile
        {
            get
            {
                // Check to see if user profile has already been created
                // in the current session. If not, create it. 
                if (GetItem("UserProfile") == null)
                {
                    IProfile userProfile;

                    // Check to see if user has cookie that contains user id. 
                    if (HttpContext.Current.Request.Cookies[CookieConst.ProfileCookie + "_" + NhsRoute.PartnerId] == null)
                    {
                        // Cookie does not exist, create new blank profile. 
                        int partnerId = Configuration.PartnerId;
                        userProfile = new Profile(partnerId);
                    }
                    else
                    {
                        // Cooke exists, grab value. 
                        string userId = HttpContext.Current.Request.Cookies[CookieConst.ProfileCookie + "_" + NhsRoute.PartnerId].Value;

                        // Create profile based on user id unless empty string. 
                        if (!String.IsNullOrEmpty(userId))
                        {
                            userProfile = new Profile(userId);
                            userProfile.DataBind(userId);
                            userProfile.SignIn(userProfile.LogonName, userProfile.Password);
                        }
                        else
                        {
                            int partnerId = Configuration.PartnerId;
                            userProfile = new Profile(partnerId);
                        }
                    }

                    // Set the profile session value for use next time. 
                    SetItem("UserProfile", userProfile);
                }

                return (IProfile)GetItem("UserProfile");
            }

            set
            {
                SetItem("UserProfile", value);
            }
        }

        public static ProCrmClientsStatusType ProCrmClientsStatus
        {
            get
            {
                return GetItem("ProCrmClientsStatus") == null
                           ? ProCrmClientsStatusType.Deleted
                           : ((string)GetItem("ProCrmClientsStatus")).FromString<ProCrmClientsStatusType>();
            }
            set { SetItem("ProCrmClientsStatus", value.ToString()); }
        }

        public static bool RightAdToDisplayIsPosition
        {
            get
            {
                var adToShow = GetItem("RightAdToDisplayIsPosition");
                return adToShow != null && Convert.ToBoolean(adToShow);
            }
            set
            {
                SetItem("RightAdToDisplayIsPosition", value);
            }
        }

        public static string ReferrerUrl
        {
            get { return GetItem("ReferrerUrl") == null ? string.Empty : (string)GetItem("ReferrerUrl"); }
            set { SetItem("ReferrerUrl", value); }
        }

        public static bool PopUpRetUserBroshure
        {
            get { return GetItem("PopUpRetUserBroshure") != null && (bool)GetItem("PopUpRetUserBroshure"); }
            set { SetItem("PopUpRetUserBroshure", value); }
        }

        public static string DestinationUrl
        {
            get { return GetItem("DestinationUrl") == null ? string.Empty : (string)GetItem("DestinationUrl"); }
            set { SetItem("DestinationUrl", value); }
        }
        public static bool ShowStaticOverlay
        {
            get
            {
                if (GetItem("ShowStaticOverlay") == null)
                    return false;
                return (bool)GetItem("ShowStaticOverlay");
            }
            set
            {
                SetItem("ShowStaticOverlay", value);
            }
        }

        public static string StaticPaths
        {
            get
            {
                if (GetItem("StaticPaths") == null)
                    return string.Empty;
                return (string)GetItem("StaticPaths");
            }
            set
            {
                SetItem("StaticPaths", value);
            }
        }

        //This Refer will be rewriter all the time that the user enter with a new Refer
        public static string DynamicRefer
        {
            get
            {
                return GetItem("DynamicRefer") == null ? string.Empty : GetItem("DynamicRefer").ToString();
            }
            set { SetItem("DynamicRefer", value); }
        }

        //The refer url for traffic categorization
        public static string DynamicReferUrl
        {
            get
            {
                return GetItem("DynamicReferUrl") == null ? string.Empty : GetItem("DynamicReferUrl").ToString();
            }
            set { SetItem("DynamicReferUrl", value); }
        }

        public static string Refer
        {
            get
            {
                if (GetItem("Refer") == null)
                {
                    return string.Empty;
                }

                return GetItem("Refer").ToString();
            }

            set
            {
                // change refer only if is null or empty
                if (GetItem("Refer") == null || string.IsNullOrEmpty(GetItem("Refer").ToString()))
                {
                    SetItem("Refer", value);
                }
            }

        }

        public static GoogleUtmz GoogleUtmz
        {
            get
            {
                var cookie = HttpContext.Current.Request.Cookies[CookieConst.GoogleUtmzCookie];
                return cookie == null ? new GoogleUtmz() : ParseUtmzCookie(cookie);
            }
        }

        public static string GoogleUtmaUId
        {
            get
            {
                var cookie = HttpContext.Current.Request.Cookies[CookieConst.GoogleUtmaCookie];
                var returnValue = string.Empty;
                if (cookie == null)
                {
                    returnValue = string.Empty;
                }
                else
                {
                    var splitCookie = cookie.Value.Split('.');

                    if (splitCookie.Any() && splitCookie.Length > 1)
                    {
                        returnValue = splitCookie[1];
                    }
                }
                return returnValue;
            }
        }

        public static string GoogleUtmaSId
        {
            get
            {
                var cookie = HttpContext.Current.Request.Cookies[CookieConst.GoogleUtmaCookie];
                return cookie == null ? string.Empty : cookie.Value.Split('.')[4];
            }
        }

        private static GoogleUtmz ParseUtmzCookie(HttpCookie cookie)
        {
            var value = cookie.Value.Split('|');
            var utmz = new GoogleUtmz();
            //Parse cookie here

            var val = value.FirstOrDefault(p => p.Contains("utmccn="));
            utmz.UtmCcn = !String.IsNullOrEmpty(val) ? val.Split('=')[1] : "";

            val = value.FirstOrDefault(p => p.Contains("utmcmd="));
            utmz.UtmCmd = !String.IsNullOrEmpty(val) ? val.Split('=')[1] : "";

            val = value.FirstOrDefault(p => p.Contains("utmctr="));
            utmz.UtmCtr = !String.IsNullOrEmpty(val) ? val.Split('=')[1] : "";

            val = value.FirstOrDefault(p => p.Contains("utmgclid="));
            utmz.UtmGclId = !String.IsNullOrEmpty(val) ? val.Split('=')[1] : "";

            val = value.FirstOrDefault(p => p.Contains("utmcsr="));
            utmz.UtmCsr = !String.IsNullOrEmpty(val) ? val.Split('=')[1] : "";

            val = value.FirstOrDefault(p => p.Contains("utmcct="));
            utmz.UtmCct = !String.IsNullOrEmpty(val) ? val.Split('=')[1] : "";

            utmz.UtmaUId = GoogleUtmaUId;

            utmz.UtmaSId = GoogleUtmaSId;

            return utmz;
        }
        public static MultiBrochureList MultiBrochureList
        {
            get
            {
                if (GetItem("MultiBrochureList") == null)
                {
                    var multiBrochureList = new MultiBrochureList();
                    SetItem("MultiBrochureList", multiBrochureList);
                }

                return (MultiBrochureList)GetItem("MultiBrochureList");
            }

            set
            {
                SetItem("MultiBrochureList", value);
            }
        }

        public static string PreviewsGroupingBarValue
        {
            get { return GetItem(SessionConst.PreviewsGroupingBarValue).ToType<string>(); }
            set { SetItem(SessionConst.PreviewsGroupingBarValue, value); }
        }

        public static bool IsBasicListingGroup
        {
            get { return GetItem(SessionConst.IsBasicListingGroup).ToType<bool>(); }
            set { SetItem(SessionConst.IsBasicListingGroup, value); }
        }

        public static bool IsBasicCommunityGroup
        {
            get { return GetItem(SessionConst.IsBasicCommunityGroup).ToType<bool>(); }
            set { SetItem(SessionConst.IsBasicCommunityGroup, value); }
        }

        public static bool IsNearbyBasicListingGroup
        {
            get { return GetItem(SessionConst.IsNearbyBasicListingGroup).ToType<bool>(); }
            set { SetItem(SessionConst.IsNearbyBasicListingGroup, value); }
        }

        public static bool IsNearbyBasicCommunityGroup
        {
            get { return GetItem(SessionConst.IsNearbyBasicCommunityGroup).ToType<bool>(); }
            set { SetItem(SessionConst.IsNearbyBasicCommunityGroup, value); }
        }


        public static string PreviewsGroupingBarValueComDetail
        {
            get { return GetItem(SessionConst.PreviewsGroupingBarValueComDetail).ToType<string>(); }
            set { SetItem(SessionConst.PreviewsGroupingBarValueComDetail, value); }
        }

        public static SearchParams SearchParametersV2
        {
            get { return (SearchParams)GetItem(SessionConst.SearchParametersV2); }
            set{SetItem(SessionConst.SearchParametersV2, value);}
        }

        public static string CommunityHomeResultsLastUrl
        {
            get { return (string)GetItem("CommunityHomeResultsLastUrl"); }
            set { SetItem("CommunityHomeResultsLastUrl", value); }
        }

        public static PersonalCookie PersonalCookie
        {
            get
            {
                if (GetItem("PersonalCookie") == null)
                {
                    PersonalCookie personalCookie = PersonalCookie.Load();
                    SetItem("PersonalCookie", personalCookie);
                    return personalCookie;
                }
                return (PersonalCookie)GetItem("PersonalCookie");
            }
            set
            {
                SetItem("PersonalCookie", value);
            }

        }
        public static string SessionId
        {
            get { return HttpContext.Current.Session.SessionID; }
        }

        public static Hashtable BCList
        {
            get
            {
                return (Hashtable)GetItem("BCList");
            }
            set
            {
                SetItem("BCList", value);
            }
        }

        public static int PageSize
        {
            get
            {
                if (GetItem("PageSize") == null)
                    SetItem("PageSize", Configuration.CommunityResultsPageSize);

                return (int)GetItem("PageSize");
            }
            set
            {
                SetItem("PageSize", value);
            }
        }
       
        public static bool SearchMapAreaConfirmed
        {
            get
            {
                if (GetItem("SearchMapAreaConfirmed") == null)
                    return false;
                return (bool)GetItem("SearchMapAreaConfirmed");
            }
            set
            {
                SetItem("SearchMapAreaConfirmed", value);
            }
        }

        public static bool HasSendLead
        {
            get
            {
                if (GetItem("HasSendLead") == null)
                    return false;

                return (bool)GetItem("HasSendLead");
            }
            set
            {
                SetItem("HasSendLead", value);
            }
        }

        public static bool HasUserOptedIntoMatchMaker
        {
            get
            {
                if (GetItem("HasUserOptedIntoMatchMaker") == null)
                    return false;

                return (bool)GetItem("HasUserOptedIntoMatchMaker");
            }
            set
            {
                SetItem("HasUserOptedIntoMatchMaker", value);
            }
        }

        public static bool HasUserOptedForAlerts
        {
            get
            {
                if (GetItem("HasUserOptedForAlerts") == null)
                    return false;

                return (bool)GetItem("HasUserOptedForAlerts");
            }
            set
            {
                SetItem("HasUserOptedForAlerts", value);
            }
        }

        public static string SearchType
        {
            get
            {
                if (GetItem(UrlConst.SearchType) != null)
                {
                    return GetItem(UrlConst.SearchType).ToString();
                }
                return SearchTypeSource.SearchUnknown;
            }
            set
            {
                SetItem(UrlConst.SearchType, value);
            }
        }

        public static bool IsLocationSearchBox
        {
            get
            {
                if (GetItem("IsLocationSearchBox") == null)
                    return false;

                return (bool)GetItem("IsLocationSearchBox");
            }
            set
            {
                SetItem("IsLocationSearchBox", value);
            }
        }

        public static bool HasSentToFriend
        {
            get
            {
                if (GetItem(SessionConst.HasSentToFriend) == null)
                {
                    SetItem(SessionConst.HasSentToFriend, false);
                    return false;
                }
                return (bool)GetItem(SessionConst.HasSentToFriend);
            }
            set
            {
                SetItem(SessionConst.HasSentToFriend, value);
            }

        }

        public static bool ShowEbookSection
        {
            get
            {
                if (GetItem("ShowEbookSection") == null)
                    return true;

                return (bool)GetItem("ShowEbookSection");
            }
            set
            {
                SetItem("ShowEbookSection", value);
            }
        }

        public static int CommPageNumber
        {
            get
            {
                if (GetItem(SessionConst.CommPageNumber) == null)
                {
                    SetItem(SessionConst.CommPageNumber, 0);
                    return 0;
                }
                return (int)GetItem(SessionConst.CommPageNumber);

            }
            set
            {
                SetItem(SessionConst.CommPageNumber, value);
            }

        }

        public static int MlsPageNumber
        {
            get
            {
                if (GetItem(SessionConst.MlsPageNumber) == null)
                {
                    SetItem(SessionConst.MlsPageNumber, 1);
                    return 0;
                }
                return (int)GetItem(SessionConst.MlsPageNumber);

            }
            set
            {
                SetItem(SessionConst.MlsPageNumber, value);
            }

        }

        public static string PreviousPage
        {
            get
            {
                if (GetItem(SessionConst.PreviousPage) == null)
                    return string.Empty;
                return GetItem(SessionConst.PreviousPage).ToString();
            }
            set
            {
                SetItem(SessionConst.PreviousPage, value);
            }
        }

        public static int ResultsPerPage
        {
            get
            {
                if (GetItem(SessionConst.ResultsPerPage) == null)
                {
                    SetItem(SessionConst.ResultsPerPage, 10);
                    return 0;
                }
                return (int)GetItem(SessionConst.ResultsPerPage);

            }
            set
            {
                SetItem(SessionConst.ResultsPerPage, value);
            }

        }

        public static string HomePageSelectedTab
        {
            get
            {
                if (GetItem(SessionConst.HomePageSelectedTab) == null)
                {
                    SetItem(SessionConst.HomePageSelectedTab, "");
                    return "";
                }
                return GetItem(SessionConst.HomePageSelectedTab).ToString();

            }
            set
            {
                SetItem(SessionConst.HomePageSelectedTab, value);
            }

        }

        public static void PersistUrlParams(Controller controller)
        {
            SetItem(SessionConst.QueryParams, controller.Request.Url.Query);
        }

        public static IList<RouteParam> GetPersistedtUrlParams()
        {
            var queryString = (string)GetItem(SessionConst.QueryParams);

            // Parse params
            var paramz = new List<RouteParam>();
            if (!string.IsNullOrEmpty(queryString))
            {
                var querySegments = queryString.TrimStart('?').ToLowerInvariant().Split(new[] { "&" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                foreach (var querySegment in querySegments)
                {
                    var paramKey = querySegment.FirstPart("=");
                    var paramValue = querySegment.LastPart("=");
                    RouteParams name;
                    if (RouteParams.TryParse(paramKey, true, out name))
                        paramz.Add(new RouteParam(name, paramValue, RouteParamType.QueryString));
                }
            }

            return paramz;
        }

        public static List<int> AdBuilderIds
        {
            get
            {
                if (GetItem(SessionConst.AdBuilderIds) == null)
                {
                    var dummy = new List<int>();
                    SetItem(SessionConst.AdBuilderIds, dummy);
                    return dummy;
                }
                return GetItem(SessionConst.AdBuilderIds) as List<int>;
            }
            set
            {
                SetItem(SessionConst.AdBuilderIds, value);
            }
        }

        public static string TempUserProfile
        {
            get
            {
                return GetItem(SessionConst.TempUserProfile).ToType<string>();
            }
            set
            {
                SetItem(SessionConst.TempUserProfile, value);
            }
        }

        public static short EbookSectionCounter
        {
            get
            {
                var data = GetItem(CookieConst.EbookSectionCounter);

                if (data != null)
                    return data.ToType<short>();

                SetItem(CookieConst.EbookSectionCounter, 0);
                return 0;
            }
            set
            {
                SetItem(CookieConst.EbookSectionCounter, value);
            }
        }

        #endregion

        #region "Public Methods"
        public static object GetItem(string key)
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
            {
                return HttpContext.Current.Session[key + "_" + Configuration.PartnerId];
            }
            
            return Current[key + "_" + Configuration.PartnerId];
            //return HttpContext.Current.Session[key+"_"+ Configuration.PartnerId];
        }
        public static void SetItem(string key, object value)
        {
            Current.Add(key + "_" + Configuration.PartnerId, value);
            //Current[key + "_" + Configuration.PartnerId] = value;
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
            {
                HttpContext.Current.Session.Add(key + "_" + Configuration.PartnerId, value);
            }
            //HttpContext.Current.Session[key+"_"+ Configuration.PartnerId] = value;
        }
        #endregion
    }
}
