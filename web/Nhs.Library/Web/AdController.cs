/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright (c) 2001-2006
 **by BuilderHomeSite Inc. ( http://www.BuilderHomeSite.com )
 * ========================================================
 * Author:
 * Date:
 * Purpose:
 * Edit History (please mention edits succinctly):
 * =========================================================
 * 10/15/07 JBecker: Added city and zip parameters.
 * 01/16/08 JPierce: Refactored.
 * 01/16/08 JPierce: Added AddCommunityParameter().
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;

namespace Nhs.Library.Web
{
    public class AdController
    {
        #region Fields

        private List<string> _allPositions = new List<string>(); //all ad positions
        private string _adDomain;
        private string _adPageName; // adPageName from MappingConfig.xml        
        private List<string> _zipParams = new List<string>();
        private List<string> _cityParams = new List<string>();
        private List<string> _builderParams = new List<string>();
        private List<string> _parameters = new List<string>(); // Parameters are set by control corresponding to function in url
        private int _randomNumber;
        private Hashtable _distinctCities = new Hashtable();
        private Hashtable _distinctZipCodes = new Hashtable();
        private Hashtable _distinctBuilderIds = new Hashtable();
        private static object _lockObj = new object();

        #endregion

        #region Properties

        public List<string> AllPositions
        {
            get { return this._allPositions; }
            set { this._allPositions = value; }
        }

        public string AdPageName
        {
            get { return this._adPageName; }
            set
            {
                lock (_lockObj)
                {
                    this._adPageName = value;
                }
            }
        }

        public int MarketParam { get; set; }

        public int CommunityParam { get; set; }

        public string StateParam { get; set; }

        public List<string> ZipParams
        {
            get { return this._zipParams; }
            set { this._zipParams = value; }
        }

        public List<string> CityParams
        {
            get { return this._cityParams; }
            set { this._cityParams = value; }
        }

        public List<string> BuilderParams
        {
            get { return this._builderParams; }
            set { this._builderParams = value; }
        }

        public List<string> Parameters
        {
            get { return this._parameters; }
            set { this._parameters = value; }
        }

        public int RandomNumber
        {
            get { return this._randomNumber; }
            set { this._randomNumber = value; }
        }

        public string AdDomain
        {
            get { return this._adDomain; }
            set { this._adDomain = value; }
        }

        #endregion

        #region Constructor

        public AdController()
        {
            // Declare vars
            var random = new Random();

            // Set fields
            this._randomNumber = random.Next(100000000, 999999999);
            this._parameters.Add("prt=" + Configuration.PartnerId);
            this._adDomain = "nhspro.com";

            if (NhsRoute.IsBrandPartnerNhsPro && NhsRoute.PartnerId != PartnersConst.Pro.ToType<int>())
            {
                this.AddPartnerIDParameter(NhsRoute.PartnerSiteUrl);
            }

            if (NhsRoute.ShowMobileSite)
                _adDomain = "bdxmobile.com";
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Renders tag for this position.
        /// </summary>
        /// <param name="currentAdPosition">Name of the this position.</param>
        /// <param name="countRef"></param>
        /// <param name="useNoScriptTag"></param>
        /// <returns></returns>
        /// TODO: change hardcoded nhs.com
        public string Render(string currentAdPosition, int countRef = 1)
        {
            const string script = @"<!-- Delivery Attempt via JX tag. -->
                <script type=""text/javascript"" 
				src=""http://oascentral.newhomesource.com/RealMedia/ads/adstream_jx.ads{0}"">
                </script>
                <!-- Delivery Attempt without Javascript --> 
                <noscript> 
				<a rel=""nofollow"" href=""http://oascentral.newhomesource.com/RealMedia/ads/click_nx.ads{0}"">
				<img src=""http://oascentral.newhomesource.com/RealMedia/ads/adstream_nx.ads{0}"" border=""0"" alt="""" />
				</a>
                </noscript>";
            var sb = new StringBuilder(script);

            // Generate output
            return ArgumentFormatter(sb.ToString(),
                currentAdPosition, countRef
                );
        }

        public string RenderAdSkin(string currentAdPosition, int countRef = 1)
        {
            const string script =
                @"<a id=""adPageSkin"" rel=""nofollow"" data-href=""http://oascentral.newhomesource.com/RealMedia/ads/click_nx.ads{0}"" 
                    data-urlimage=""http://oascentral.newhomesource.com/RealMedia/ads/adstream_nx.ads{0}"" 
                        style=""display: inline;"" class=""nhs_FullPageAd"" target=""_blank""></a>";

            // Generate output
            return ArgumentFormatter(script, currentAdPosition, countRef);
        }

        public string RenderIFrame(string currentAdPosition, int countRef = 1)
        {
            const string script = @"<!-- Delivery Attempt via SX (IFrame) tag. -->
				<iframe id=""nhsIFrameAd{1}_#REPLACE#"" class=""nhsIFrameAd{1} adIframeContent"" frameborder=""0"" scrolling=""no"" marginheight=""0"" marginwidth=""0"" src=""http://oascentral.newhomesource.com/RealMedia/ads/adstream_sx.cgi{0}""></iframe>";
            // Generate output
            return ArgumentFormatter(script.Replace("#REPLACE#", countRef.ToString(CultureInfo.InvariantCulture)),
                currentAdPosition, countRef
                );
        }

        public string GetIFrameSrc(string currentAdPosition, int countRef = 1)
        {
            // Generate output
            return ArgumentFormatter(
                @"http://oascentral.newhomesource.com/RealMedia/ads/adstream_sx.cgi{0}",
                currentAdPosition, countRef
                );
        }

        public string RenderJavaScriptParams()
        {
            // Generate output
            return String.Format(
                @"<script type=""text/javascript"">
				/* <![CDATA[ */                
				var nhsPageName = ""{0}"";
				var nhsState = ""{1}"";
				var nhsMarket = {2};
				var nhsCommunity = ""{3}"";
				var nhsCityString = ""{4}"";
				var nhsZipString = ""{5}"";
				var nhsBuilderString = ""{6}"";
				// Build Arrays
				var nhsCities = nhsCityString.split('&amp;');
				var nhsZipString = nhsZipString.split('&amp;');
				var nhsBuilders = nhsBuilderString.split('&amp;');
				/* ]]> */
				</script>",
                this.AdPageName, this.StateParam, this.MarketParam, this.CommunityParam, ParameterBuilder(this.CityParams), ParameterBuilder(this.ZipParams), ParameterBuilder(this.BuilderParams)
                );
        }

        public void AddPriceParameter(int priceLow, int priceHigh)
        {
            // Valid Range?
            if ((priceLow > 0) && (priceHigh > 0))
            {
                // Determine Ranges
                int rangeLow = PriceRange((priceLow > 0) ? priceLow : priceHigh);
                int rangeHigh = PriceRange((priceHigh > 0) ? priceHigh : priceLow);

                // Traverse Range
                for (int i = rangeLow; i <= rangeHigh; i++)
                {
                    this.Parameters.Add("p=" + i);
                }
            }
        }

        public void AddPartnerIDParameter(string partnerSiteUrl)
        {
            this.Parameters.Add("partner=" + partnerSiteUrl);
        }

        public void AddMarketParameter(int marketId)
        {
            if (marketId <= 0) return;

            this.Parameters.Add(marketId.ToString("m=000"));
            this.MarketParam = marketId;
        }

        public void AddStateParameter(string state)
        {
            if (String.IsNullOrEmpty(state)) return;

            this.Parameters.Add("s=" + state);
            this.StateParam = state;
        }

        public void AddZipParameter(string zip)
        {
            if (!String.IsNullOrEmpty(zip) && !this._distinctZipCodes.Contains(zip))
            {
                this.Parameters.Add("z" + zip);
                this.ZipParams.Add("z" + zip);
                this._distinctZipCodes.Add(zip, null);
            }
        }

        public void AddCityParameter(string city)
        {
            if (!String.IsNullOrEmpty(city) && !this._distinctCities.Contains(city))
            {
                this.Parameters.Add(city);
                this.CityParams.Add(city);
                this._distinctCities.Add(city, null);
            }
        }

        public void AddBuilderParameter(int builderId)
        {
            if ((builderId > 0) && !this._distinctBuilderIds.Contains(builderId))
            {
                this.Parameters.Add(builderId.ToString("\\b0000"));
                this.BuilderParams.Add(builderId.ToString("\\b0000"));
                this._distinctBuilderIds.Add(builderId, null);
            }
        }

        public void AddArticleParameter(string articleName)
        {
            if (!String.IsNullOrEmpty(articleName))
            {
                this.Parameters.Add("a=" + articleName);
            }
        }

        public void AddCategoryParameter(string categoryName)
        {
            if (!String.IsNullOrEmpty(categoryName))
            {
                this.Parameters.Add("se=" + categoryName);
            }
        }

        public void AddCommunityParameter(int communityId)
        {
            if (communityId <= 0) return;
            this.Parameters.Add(communityId.ToString("c=00000"));
            this.CommunityParam = communityId;
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Builds an argument mask and the related arguments for use within a call to String.Format()
        /// </summary>
        /// <param name="script">Script to format</param>
        /// <param name="currentAdPosition">Name of the Ad's Position</param>
        /// <param name="countRef">Count of the elements in a current page, this is to tell Chrome to no cahce the URL for the same position</param>
        /// <returns>An array with the argument mask at index 0 and the arguments in the remaining indexes</returns>
        private string ArgumentFormatter(string script, string currentAdPosition, int countRef)
        {
            // Declare vars
            string allAdPositions = String.Empty;
            string argMask = String.Empty;
            string arg4 = String.Empty;
            string arg5 = String.Empty;

            // Has Parameters?
            if (this.Parameters.Count > 0)
            {
                // Traverse Parameters
                var parmsBuilder = new StringBuilder();
                foreach (string paramValue in this.Parameters)
                {
                    // Remove leading/trailing white space and replace spaces with "%20" in param
                    parmsBuilder.Append(paramValue.Trim().Replace(" ", "%20") + "&");
                }

                // Remove trailing '&' and Encode
                //string parms = HttpUtility.HtmlEncode((parmsBuilder.ToString().TrimEnd('&')));
                string parms = parmsBuilder.ToString().TrimEnd('&');

                // Not the only Ad in the Page?
                if (this.AllPositions.Count >= 2)
                {
                    // Traverse Positions
                    var positionsBuilder = new StringBuilder();
                    foreach (string s in this.AllPositions)
                    {
                        positionsBuilder.Append(s + ",");
                    }

                    // Remove trailing ","
                    allAdPositions = positionsBuilder.ToString().TrimEnd(",".ToCharArray());

                    // Set Formatting criteria
                    argMask = "!{4}?{5}";
                    arg4 = currentAdPosition;
                    arg5 = parms;
                }
                else
                {
                    // Set Formatting criteria
                    argMask = "?{4}";
                    arg4 = parms;
                }
            }

            // Generate output
            return String.Format(
                String.Format(script, "/{0}/{1}/{2}@{3}" + argMask + "&ref={6}", currentAdPosition),
                this._adDomain, this._adPageName, this._randomNumber, ((arg5 == String.Empty) ? currentAdPosition : allAdPositions), arg4, arg5, countRef);
        }

        /// <summary>
        /// Builds a delimiter separated string of parameters
        /// </summary>
        /// <param name="parameters">The list of parameters to parse</param>
        /// <returns>A concatenated string of parameters</returns>
        private string ParameterBuilder(List<string> parameters)
        {
            // Declare vars
            var result = new StringBuilder();

            // Traverse Parameters
            foreach (string parm in parameters)
            {
                result.Append(parm.Trim() + "&");
            }

            // Remove trailing ampersand and encode
            //return HttpUtility.HtmlEncode(result.ToString().TrimEnd('&'));
            return result.ToString().TrimEnd('&');
        }

        private int PriceRange(int price)
        {
            // Declare vars
            int result;

            // Determine Range
            if (price >= 500000)
            {
                result = 5;
            }
            else if (price >= 340000)
            {
                result = 4;
            }
            else if (price >= 240000)
            {
                result = 3;
            }
            else if (price >= 140000)
            {
                result = 2;
            }
            else if (price > 0)
            {
                result = 1;
            }
            else
            {
                result = 0;
            }

            // Return result
            return result;
        }

        #endregion
    }
}
