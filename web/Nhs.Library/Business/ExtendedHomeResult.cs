﻿using System;
using Nhs.Mvc.Domain.Model.Web.Interfaces;

namespace Nhs.Library.Business
{
    public class ExtendedHomeResult
    {
        public IPlan Plan { get; set; }
        public ISpec Spec { get; set; }
        public bool IsBilled { get; set; }
        public string MarketName { get; set; }
        public string StateName { get; set; }
        public string FormatedPrice { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string BrandImageThumbnail { get; set; }
        public string BrandName { get; set; }
        public int BuilderId { get; set; }
        public string City { get; set; }
        public int CommunityId { get; set; }
        public string CommunityName { get; set; }
        public string County { get; set; }
        public int HomeStatus { get; set; }
        public int ImageCount { get; set; }
        public bool IsHotHome { get; set; }
        public int ListingId { get; set; }
        public string MarketingDescription { get; set; }
        public int NumBathrooms { get; set; }
        public int NumBedrooms { get; set; }
        public decimal NumGarages { get; set; }
        public int NumHalfBathrooms { get; set; }
        public int PlanId { get; set; }
        public string PlanImageThumbnail { get; set; }
        public string PlanName { get; set; }
        public string PlanTypeCode { get; set; }
        public string PostalCode { get; set; }
        public int Price { get; set; }
        public int PromoId { get; set; }
        public string SpecCity { get; set; }
        public int SpecId { get; set; }
        public string SpecPostalCode { get; set; }
        public string SpecState { get; set; }
        public int SquareFeet { get; set; }
        public string State { get; set; }
        public int Stories { get; set; }
    }

    [Serializable]
    public class CommunityResult
    {

        public bool AlertDeletedFlag { get; set; }
        public int BrandId { get; set; }
        public string BrandImageThumbnail { get; set; }
        public string BrandName { get; set; }
        public int BuilderId { get; set; }
        public string BuilderUrl { get; set; }
        public bool BuildOnYourLot { get; set; }
        public string City { get; set; }
        public int CommunityId { get; set; }
        public string CommunityImageThumbnail { get; set; }
        public string CommunityName { get; set; }
        public string CommunityType { get; set; }
        public string County { get; set; }
        public double DistanceFromCenter { get; set; }
        public int FeaturedListingId { get; set; }
        public string FeaturedPosition { get; set; }
        public bool Green { get; set; }
        public bool HasHotHome { get; set; }
        public bool IsBilled { get; set; }
        public bool IsFeatured { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int MarketId { get; set; }
        public int MatchingHomes { get; set; }
        public string PostalCode { get; set; }
        public int PriceHigh { get; set; }
        public int PriceLow { get; set; }
        public int PromoId { get; set; }
        public string State { get; set; }
        public string SubVideoFlag { get; set; }
        public string Video_url { get; set; }
    }
}
