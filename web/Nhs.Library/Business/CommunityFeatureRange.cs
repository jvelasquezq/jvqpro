﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Library.Business
{
    public class CommunityFeatureRange
    {
        public string MinBedroom { get; set; }
        public string MaxBedroom { get; set; }
        public string MinBathroom { get; set; }
        public string MaxBathroom { get; set; }
        public string MinGarage { get; set; }
        public string MaxGarage { get; set; }
        public decimal PriceLow { get; set; }
        public decimal PriceHigh { get; set; }

    }
}