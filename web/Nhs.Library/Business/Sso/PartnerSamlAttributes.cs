﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Library.Business.Sso
{
    public class PartnerSamlAttributes
    {
        public string PartnerSiteUrl { get; set; }
        public List<XSamlAttribute> SamlAttributes { get; set; }

        public PartnerSamlAttributes()
        {
            SamlAttributes = new List<XSamlAttribute>();
        }
    }
}
