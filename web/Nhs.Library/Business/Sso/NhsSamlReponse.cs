﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ComponentSpace.SAML2.Protocols;

namespace Nhs.Library.Business.Sso
{
    public class NhsSamlReponse
    {
        public SAMLResponse SamlResponse { get; set; }
        public string RelayState { get; set; }

        public NhsSamlReponse()
        {
            SamlResponse = null;
            RelayState = string.Empty;
        }
    }
}
