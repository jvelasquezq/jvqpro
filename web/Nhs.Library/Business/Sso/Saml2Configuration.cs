﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Configuration;
using ComponentSpace.SAML2;

namespace Nhs.Library.Business.Sso
{
    public class Saml2Configuration
    {
        private static class ConfigurationKeys
        {
            public const string SingleSignOnServiceBinding = "SingleSignOnServiceBinding";
            public const string HttpPostSingleSignOnServiceURL = "HttpPostSingleSignOnServiceURL";
            public const string HttpRedirectSingleSignOnServiceURL = "HttpRedirectSingleSignOnServiceURL";
            public const string MlsListingHttpRedirectSingleSignOnServiceURL = "MlsListingHttpRedirectSingleSignOnServiceURL";
            public const string HttpArtifactSingleSignOnServiceURL = "HttpArtifactSingleSignOnServiceURL";
            public const string AssertionConsumerServiceBinding = "AssertionConsumerServiceBinding";
            public const string HttpPostAssertionConsumerServiceURL = "HttpPostAssertionConsumerServiceURL";
            public const string HttpArtifactAssertionConsumerServiceURL = "HttpArtifactAssertionConsumerServiceURL";
            public const string ArtifactResolutionServiceURL = "ArtifactResolutionServiceURL";
            public const string EntityDescriptor = "EntityDescriptor";
        }

        public static SAMLIdentifiers.Binding SingleSignOnServiceBinding
        {
            get
            {
                return SAMLIdentifiers.BindingURIs.URIToBinding(WebConfigurationManager.AppSettings[ConfigurationKeys.SingleSignOnServiceBinding]);
            }
        }


        public static string SingleSignOnServiceURL
        {
            get
            {
                switch (SingleSignOnServiceBinding)
                {
                    case SAMLIdentifiers.Binding.HTTPPost:
                        return HttpPostSingleSignOnServiceURL;

                    case SAMLIdentifiers.Binding.HTTPRedirect:
                        return HttpRedirectSingleSignOnServiceURL;

                    case SAMLIdentifiers.Binding.HTTPArtifact:
                        return HttpArtifactSingleSignOnServiceURL;

                    default:
                        throw new ArgumentException("Invalid single signon service binding");
                }
            }
        }
        //added for MlsListing

        public static string MlsListingSingleSignOnServiceURL
        {
            get
            {
                switch (SingleSignOnServiceBinding)
                {
                    case SAMLIdentifiers.Binding.HTTPPost:
                        return HttpPostSingleSignOnServiceURL;

                    case SAMLIdentifiers.Binding.HTTPRedirect:
                        return MlsListingHttpRedirectSingleSignOnServiceURL;

                    case SAMLIdentifiers.Binding.HTTPArtifact:
                        return HttpArtifactSingleSignOnServiceURL;

                    default:
                        throw new ArgumentException("Invalid single signon service binding");
                }
            }
        }


        public static string HttpPostSingleSignOnServiceURL
        {
            get
            {
                return WebConfigurationManager.AppSettings[ConfigurationKeys.HttpPostSingleSignOnServiceURL];
            }
        }

        public static string HttpRedirectSingleSignOnServiceURL
        {

            get
            {
                return WebConfigurationManager.AppSettings[ConfigurationKeys.HttpRedirectSingleSignOnServiceURL];
            }
        }
        //added for MlsListing

        public static string MlsListingHttpRedirectSingleSignOnServiceURL
        {

            get
            {
                return WebConfigurationManager.AppSettings[ConfigurationKeys.MlsListingHttpRedirectSingleSignOnServiceURL];
            }
        }

        public static string HttpArtifactSingleSignOnServiceURL
        {
            get
            {
                return WebConfigurationManager.AppSettings[ConfigurationKeys.HttpArtifactSingleSignOnServiceURL];
            }
        }

        public static string ArtifactResolutionServiceURL
        {
            get
            {
                return WebConfigurationManager.AppSettings[ConfigurationKeys.ArtifactResolutionServiceURL];
            }
        }


        public static SAMLIdentifiers.Binding AssertionConsumerServiceBinding
        {
            get
            {
                return SAMLIdentifiers.BindingURIs.URIToBinding(WebConfigurationManager.AppSettings[ConfigurationKeys.AssertionConsumerServiceBinding]);
            }
        }

        public static string AssertionConsumerServiceURL
        {
            get
            {
                switch (AssertionConsumerServiceBinding)
                {
                    case SAMLIdentifiers.Binding.HTTPPost:
                        return HttpPostAssertionConsumerServiceURL;

                    case SAMLIdentifiers.Binding.HTTPArtifact:
                        return HttpArtifactAssertionConsumerServiceURL;

                    default:
                        throw new ArgumentException("Invalid assertion consumer service binding");
                }
            }
        }

        public static string HttpPostAssertionConsumerServiceURL
        {
            get
            {
                return WebConfigurationManager.AppSettings[ConfigurationKeys.HttpPostAssertionConsumerServiceURL];
            }
        }

        public static string HttpArtifactAssertionConsumerServiceURL
        {
            get
            {
                return WebConfigurationManager.AppSettings[ConfigurationKeys.HttpArtifactAssertionConsumerServiceURL];
            }
        }

        public static string EntityDescriptor
        {
            get
            {
                return WebConfigurationManager.AppSettings[ConfigurationKeys.EntityDescriptor];
            }
        }
    }
}
