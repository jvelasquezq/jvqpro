﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Library.Business.Sso
{
    public class XSamlAttribute
    {
        public string Name { get; set; }
        public string Key { get; set; }   
    }
}
