﻿using System;
using System.Data;
using Nhs.Library.Enums;
using Nhs.Mvc.Domain.Model.Profiles;

namespace Nhs.Library.Business
{
    public interface IProfile
    {
        #region "Properties"

        WebActors ActorStatus { get; set; }
        string Address1 { get; set; }
        string Address2 { get; set; }
        string Age { get; set; }
        DateTime BoxRequestedDate { get; set; }
        string City { get; set; }
        DateTime DateChanged { get; set; }
        DateTime DateRegistered { get; set; }
        string DayPhone { get; set; }
        string DayPhoneExt { get; set; }
        string Email { get; set; }
        string EvePhone { get; set; }
        string EvePhoneExt { get; set; }
        string Fax { get; set; }
        string FirstName { get; set; }
        DateTime InitialMatchDate { get; set; }
        string LastName { get; set; }
        string LeadOptIn { get; set; }
        string LogonName { get; set; }
        string MailList { get; set; }
        string MarketOptIn { get; set; }
        string Metro { get; set; }
        string MiddleName { get; set; }
        int PartnerID { get; set; }
        string Password { get; set; }
        IPlanner Planner { get; }
        string PostalCode { get; set; }
        string ReferrerName { get; set; }
        bool RegActiveAdultInterest { get; set; }
        bool RegBOYLInterest { get; set; }
        string RegCity { get; set; }
        bool RegComingSoonInterest { get; set; }
        string RegMetro { get; set; }
        string RegPrefer { get; set; }
        string RegPrice { get; set; }
        string RegState { get; set; }
        string State { get; set; }
        string UserAccountState { get; set; }
        string UserID { get; set; }
        string GenericGUID { get; set; }
        bool WeeklyNotifierOptIn { get; set; }
        Nullable<bool> HomeWeeklyOptIn { get; set; }
        int MoveInDate { get; set; }
        int FinancePreference { get; set; }
        int Reason { get; set; }
        string AgencyName { get; set; }
        string RealEstateLicense { get; set; }

        #endregion

        #region "Methods"

        void ClearProfile();
        void DataBind(string userId);
        void SignIn(string logonName, string password);
        void SaveProfile();
        IProfile SetValuesForBrochureTable(IProfile oldProfile, AgentBrochureTemplate agentTemplate);
        void WriteProfileCookie();

        bool IsLoggedIn();

        #endregion
    }
}
