﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;

namespace Nhs.Library.Business
{
    public interface IPlanner
    {
        #region "Properties"
        string UserId { get; }
        ReadOnlyCollection<PlannerListing> RecentlyViewedHomes { get; }
        ReadOnlyCollection<PlannerListing> RecommendedListings { get; }
        ReadOnlyCollection<PlannerListing> RequestedListings { get; }
        ReadOnlyCollection<PlannerListing> SavedCommunities { get; }
        ReadOnlyCollection<PlannerListing> SavedHomes { get; }
        ReadOnlyCollection<SearchAlert> SavedSearchAlerts { get; }
        
        #endregion

        #region "Methods"
        void AddRecentlyViewedHome(int listingId, bool isSpec);
        void AddRecommendedCommunity(int communityId, int builderId);
        void AddSavedCommunity(int communityId, int builderId);
        void AddSavedHome(int listingId, bool isSpec);
        void AddSavedSearchAlert(SearchAlert alert);
        void ClearRecentlyViewedHomes();
        void DataBind(string userId);
        PlannerListing GetCommunityListing(int communityId, int builderId, bool isRecommended);
        PlannerListing GetHomeListing(int listingId, bool isSpec);
        DataTable GetSavedHomes(int partnerId, string plans, string specs);
        void RemoveAllRecommendedCommunities();
        void RemoveArchivedRecommendedCommunities();
        void RemoveRecentlyViewedHome(int listingId, bool isSpec);
        void RemoveRecommendedCommunity(int communityId, int builderId);
        void RemoveRecommendedCommunities(string communityList, int builderId);
        void RemoveSavedCommunity(int communityId, int builderId);
        void RemoveSavedHome(int listingId, bool isSpec);
        void RemoveSavedAlert(SearchAlert alert);
        void RemoveAllSavedAlerts();
        void ExpireAllSavedAlerts();
        void UpdateCommunityRequestDate(int communityId, int builderId,string sourceRequestKey);
        void UpdateHomeRequestDate(int listingId, bool isSpec, string sourceRequestKey);
        IEnumerable<PlannerListing> GetSavedProperties();

        #endregion
    }
}
