using System;

namespace Nhs.Library.Business
{
    public interface ISearchAlert
    {
        #region Properties

        int AlertId { get; set; }
        string UserId { get; set; }
        string Title { get; set; }
        DateTime DateCreated { get; set; }
        string State { get; set; }
        int MarketId { get; set; }
        string City { get; set; }
        string PostalCode { get; set; }
        int? Radius { get; set; }
        int PriceMin { get; set; }
        int PriceMax { get; set; }
        int? Bedrooms { get; set; }
        string HomeType { get; set; }
        
        bool SingleFamilyHomes { get; set; }
        bool CondoTownhomeHomes { get; set; }
        bool BuildOnYourLot { get; set; }

        bool Pool { get; set; }
        bool GolfCourse { get; set; }
        bool GatedCommunity { get; set; }
        int? SchoolDistrictId { get; set; }
        int? BuilderId { get; set; }

        #endregion

        #region Methods

        //void CreateAlert(Page page, int excludedCommunityId);
        //void SaveResults(List<CommunityResult> alertResults);
        void RemoveAlert();

        #endregion
    }
}
