﻿using System.Collections.Generic;

namespace Nhs.Library.Business
{
    /// <summary>
    /// Represents a collection of communities to be displayed as ads on the
    /// community results pages. Each community saved in the form of the community id.
    /// </summary>
    public class FeaturedListing
    {
        public int FeaturedListingId { get; set; }
        public int MarketId { get; set; }
        public int BuilderId { get; set; }
        private int CurrentIndex { get; set; } // used to store current community for the FL

        public List<int> CommunityIds = new List<int>();
        public int CurrentCommunityId()
        {
            if (CurrentIndex == CommunityIds.Count)
            {
                CurrentIndex = 1;
                return this.CommunityIds[0];
            }
            else
                return this.CommunityIds[CurrentIndex++];
        }
    }
}
