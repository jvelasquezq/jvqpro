namespace Nhs.Library.Business
{
    public class MarketInfo
    {
        public MarketInfo()
        {
            State = string.Empty;
            MarketName = string.Empty;
        }

        public int MarketId { get; set; }

        public int TotalCommunities { get; set; }

        public string MarketName { get; set; }

        public string State { get; set; }

        public bool ContainsGolfCommunities { get; set; }

        public bool ContainsCondoTownCommunities { get; set; }

        public bool ContainsWaterFrontCommunities { get; set; }

        public bool ContainsSpanishCommunities { get; set; }

        public bool ContainsRetirementCommunities { get; set; }

        public bool ContainsComingSoonCommunities { get; set; }

        public bool ContainsBoylCommunities { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public double StateLatitude { get; set; }

        public double StateLongitude { get; set; }

        public int Radius { get; set; }
    }
}
