namespace Nhs.Library.Business
{
    public class GeoLocation
    {
        #region "Members"

        private string _city = string.Empty;
        private string _state = string.Empty;
        private string _postalCode = string.Empty;
        private string _county = string.Empty;
        private string _country = string.Empty;
        #endregion

        #region "Properties"

        public int GeoLocationId { get; set; }

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
        public string PostalCode
        {
            get { return _postalCode; }
            set { _postalCode = value; }
        }
        public string County
        {
            get { return _county; }
            set { _county = value; }
        }
        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }
        #endregion

        #region "Constructor"
        public GeoLocation()
        {
        }

        public GeoLocation(int geoLocationId)
        {
            GeoLocationId = geoLocationId;
            Databind(geoLocationId);
        }
        #endregion

        #region "Public Methods"
        public void Databind(int geoLocationId)
        {
        }
        #endregion

    }
}
