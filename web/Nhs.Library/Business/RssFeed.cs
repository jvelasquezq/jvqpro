﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Library.Business
{
    public class RssFeed
    {
        public string Link { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
