namespace Nhs.Library.Business
{
    public class PostalCode
    {
        private string _code = string.Empty;

        public string Code
        {
            get { return _code.Trim(); }
            set { _code = value; }
        }

        public int MarketId { get; set; }
        public string MarketName { get; set; }
        public string State { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int Radius { get; set; }
        public string City { get; set; }
    }
}
