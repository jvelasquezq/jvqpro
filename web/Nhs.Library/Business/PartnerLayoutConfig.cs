﻿namespace Nhs.Library.Business
{
    public class PartnerLayoutConfig
    {
        public bool DoesPartnerHaveHotDealsLink { get; set; }
        public bool PartnerHasNavBarEnabled { get; set; }
        public bool AmIOnDetailPage { get; set; }
        public bool AmIOnNewResultsPage { get; set; }
        public bool AmIOnCommunityDetailPage { get; set; }
        public bool AmIOnHomeDetailPage { get; set; }
        public bool AmIOnCommunityResultsPage { get; set; }
        public bool AmIOnNhsTv { get; set; }
        public bool AmIOnHomeResultsPage { get; set; }
        public bool AmIOnCommDetailv1 { get; set; }
        public bool AmIOnBuilderShowcase { get; set; }
        public bool AmIOnHomePage { get; set; }
        public bool ShowSocialMediaSharing { get; set; }
        public bool AmIOnSiteHelpPages { get; set; }
        public bool PartnerIsABrand { get; set; }
        public bool PartnerIsPrivateLabel { get; set; }
        public bool PartnerAllowsModals { get; set; }
        public bool PartnerAllowsCrossMarketingLinks { get; set; }
        public bool DoesPartnerAllowsSeoLinkPromotions { get; set; }
        public bool DoesPartnerHaveSquareFeetFacet { get; set; }
        public bool DoesPartnerHaveCommunityStatusFacet { get; set; }
        public bool DoesPartnerHavePromotionsFacet { get; set; }
        public bool DoesPartnerHaveSpecialOffers { get; set; }
        public bool AmIOnDrivingDirectionsPage { get; set; }
        public bool IsPartnerRealtor { get; set; }
        public bool IsPartnerNhl { get; set; }
        public bool IsPartnerCna { get; set; }
        public bool IsPartnerYahoo { get; set; }
        public bool IsPartnerNhs { get; set; }
        public bool IsPartnerNhsPro { get; set; }
        public bool IsPartnerMove { get; set; }
        public bool IsBrandPartnerNhs { get; set; }
        public bool IsBrandPartnerCna { get; set; }
        public bool IsBrandPartnerMove { get; set; }
        public bool IsBrandPartnerNhsPro { get; set; }
        public bool DoesPartnerHaveStateDropDown { get; set; }
        public bool DoesPartnerHaveMarketDropDown { get; set; }
        public bool DisplayFullWidthGalleryinDetails { get; set; }
        public bool ShowRightAdColumn { get; set; }
        public bool AmIOnBasicDetailPage { get; set; }
        public bool ShowTopAd { get; set; }

        public bool Use2014Styles { get; set; }
        public bool IncludeBrightcove { get; set; }
        public bool AmIOnBasicCommunityDetailPage { get; set; }
        public bool AmIOnMortgageCalculator { get; set; }
    }
}
