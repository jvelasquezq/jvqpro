﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Nhs.Utility.Common;

namespace Nhs.Library.Business
{
    [Serializable]
    public class GoogleUtmz
    {
        public GoogleUtmz()
        {
            UtmaUId = UtmCsr = UtmCct = UtmCtr = UtmCmd = UtmCcn = UtmGclId = string.Empty;
        }

        [XmlAttribute("utmauid")]
        public string UtmaUId { get; set; }

        [XmlAttribute("utmasid")]
        public string UtmaSId { get; set; }

        [XmlAttribute("utmgclid")]
        public string UtmGclId { get; set; }

        [XmlAttribute("utmccn")]
        public string UtmCcn { get; set; }

        [XmlAttribute("utmcmd")]
        public string UtmCmd { get; set; }

        [XmlAttribute("utmctr")]
        public string UtmCtr { get; set; }

        [XmlAttribute("utmcct")]
        public string UtmCct { get; set; }

        [XmlAttribute("utmcsr")]
        public string UtmCsr { get; set; }

        public bool HaveValues
        {
            get
            {
                return !string.IsNullOrEmpty(UtmCsr) || !string.IsNullOrEmpty(UtmCct) || !string.IsNullOrEmpty(UtmCtr) ||
                       !string.IsNullOrEmpty(UtmCmd) || !string.IsNullOrEmpty(UtmCcn) || !string.IsNullOrEmpty(UtmGclId);
            }
        }

        public string ToXml()
        {
            return this.ToStringSerialize<GoogleUtmz>();
        }
    }
}
