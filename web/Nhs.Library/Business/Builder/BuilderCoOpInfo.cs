﻿using System.Xml.Serialization;
using Nhs.Library.Business.Seo.Attributes;

namespace Nhs.Library.Business.BuilderCoop
{
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class BuilderCoOpInfo
    {
        public int BuilderId { get; set; }
        public int BrandId { get; set; }
        public string LogoUrlSmall { get; set; }
        public string LogoUrlMed { get; set; }
        public string BrandName { get; set; }
        public string BuilderName { get; set; }
        public bool IsCoo { get; set; }
        public bool IsPac { get; set; }

        [XmlElement("content", IsNullable = true)]
        [ReplaceSeoContent]
        public string PartnershipPactHtml { get; set; }
    }
}
