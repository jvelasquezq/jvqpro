﻿using System.Collections.Generic;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Domain.Model.Web;
using System;

namespace Nhs.Library.Business
{
    public class ExtendedCommunityResult 
    {
        public ExtendedCommunityResult()
        {
            SavedPropertysProCrm = new List<PlannerListing>();
        }
        public IEnumerable<PlannerListing> SavedPropertysProCrm { get; set; }
        public string CommunityVideoThumbnail { get; set; }
        public bool HasVideo { get; set; }
        public Video Video { get; set; }
        public string GroupingBarTitle { get; set; }
        public bool ShowBasicListingIndicator { get; set; }
        public IImage CommunityVideoTour { get; set; }
        public List<Event> Events { get; set; }
        public List<Promotion> Promotions { get; set; }
        public bool IsNearbyCommunities { get; set; }
        public string MarketName { get; set; }
        public string StateName { get; set; }
        public bool HasCoop { get; set; }
        public string AlernativeCommunityImageThumbnail { get; set; }
        public double Distance { get; set; }
        public bool HasAgentPromos { get; set; }
        public bool HasConsumerPromos { get; set; }
        public bool HasEvents { get; set; }
        public bool HasPromos { get; set; }
        public bool IsBasicCommunity { get; set; }
        public bool IsBasicListing { get; set; }
        public int QuickMoveinHomesCount { get; set; }
        public int SearchKey { get; set; }
        public bool AlertDeletedFlag { get; set; }
        public int BrandId { get; set; }
        public string BrandImageThumbnail { get; set; }
        public string BrandName { get; set; }
        public int BuilderId { get; set; }
        public string BuilderUrl { get; set; }
        public bool BuildOnYourLot { get; set; }
        public string City { get; set; }
        public int CommunityId { get; set; }
        public string CommunityImageThumbnail { get; set; }
        public string CommunityName { get; set; }
        public string CommunityType { get; set; }
        public string County { get; set; }
        public double DistanceFromCenter { get; set; }
        public int FeaturedListingId { get; set; }
        public string FeaturedPosition { get; set; }
        public bool Green { get; set; }
        public bool HasHotHome { get; set; }
        public bool IsBilled { get; set; }
        public bool IsFeatured { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int MarketId { get; set; }
        public int MatchingHomes { get; set; }
        public string PostalCode { get; set; }
        public int PriceHigh { get; set; }
        public int PriceLow { get; set; }
        public int PromoId { get; set; }
        public string State { get; set; }
        public string SubVideoFlag { get; set; }
        public string Video_url { get; set; }
    }
}
