namespace Nhs.Library.Business
{
    public class StateInfo
    {
        private string _state = string.Empty;
        private string _stateName = string.Empty;

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }

        public string StateName
        {
            get { return _stateName; }
            set { _stateName = value; }
        }

        public bool ContainsGolfCommunities { get; set; }

        public bool ContainsCondoTownCommunities { get; set; }

        public bool ContainsWaterFrontCommunities { get; set; }
    }
}
