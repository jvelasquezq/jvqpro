﻿namespace Nhs.Library.Business
{
    public class AgentQuote
    {
        public string Order { get; set; }
        public string AgentName { get; set; }
        public string AgentLocation { get; set; }
        public string AgentPhotoUrl { get; set; }
        public string QuoteText { get; set; }
    }
}
