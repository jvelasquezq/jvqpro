﻿using System.Collections.Generic;

namespace Nhs.Library.Business.Tracking
{
    public class TrackingScriptPartner
    {
        public int PartnerId { get; set; }
        public string SiteUrl { get; set; }
        public IList<string> Functions { get; set; }

        public TrackingScriptPartner()
        {
            Functions = new List<string>();
        }

        public bool ApplyAllFunctions
        {
            get { return Functions.Count == 0; }
        }
    }
}
