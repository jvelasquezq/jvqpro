﻿using System.Collections.Generic;

namespace Nhs.Library.Business.Tracking
{
    public class TrackingScriptBrandPartner
    {
        public int BrandPartnerId { get; set; }
        public IList<TrackingScriptPartner> Partners;

        public TrackingScriptBrandPartner()
        {
            Partners = new List<TrackingScriptPartner>();
        }

        public bool ApplyAllPartners
        {
            
            get {
                return string.IsNullOrEmpty(Partners[0].SiteUrl.Trim()) &&
                       (Partners.Count == 0 || Partners[0].PartnerId == -1);
            }
        }
    }
}
