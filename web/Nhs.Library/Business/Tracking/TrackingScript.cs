﻿using System.Collections.Generic;

namespace Nhs.Library.Business.Tracking
{
    public class TrackingScript
    {
        public string FileName { get; set; }
        public bool AddToHead { get; set; }

        public IList<TrackingScriptBrandPartner> BrandPartners { get; set; }

        public TrackingScript()
        {
            BrandPartners = new List<TrackingScriptBrandPartner>();
        }

        public bool ApplyAllBrandPartners
        {
            get { return BrandPartners.Count == 0 || BrandPartners[0].BrandPartnerId == -1; }
        }
    }
}