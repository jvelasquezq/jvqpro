using System;

namespace Nhs.Library
{
    public class Category : ICloneable
    {
        private int _categoryId = -1;
        private string _categoryName = string.Empty;
        private string _subCategoryName = string.Empty;

        public int CategoryId
        {
            get { return _categoryId; }
            set { _categoryId = value; }
        }
        public string CategoryName
        {
            get { return _categoryName; }
            set { _categoryName = value; }
        }
        public string SubCategoryName
        {
            get { return _subCategoryName; }
            set { _subCategoryName = value; }
        }

        #region ICloneable Members

        public object Clone()
        {
            Category category = new Category();
            category.CategoryId = this._categoryId;
            category.CategoryName = this._categoryName.Clone().ToString();
            category.SubCategoryName = this._subCategoryName.Clone().ToString();
            return category;
        }

        #endregion
    }
}
