﻿using Nhs.Library.Enums;
using Nhs.Library.Constants.Enums;

namespace Nhs.Library.Business
{
    public struct MetaTag
    {
        public MetaName Name;
        public string Content;
    }

    public class MetaTagFacebook
    {
        public FacebookMetaType Name;
        public string Content;
        public string Abbr = "og";
    }
}
