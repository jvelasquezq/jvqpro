namespace Nhs.Library.Business
{
    public class LeadInfo
    {
        public LeadInfo()
        {
            IsBilled = true;
        }

        private string _leadType = string.Empty;
        private string _leadAction = string.Empty;
        private string _leadComments = string.Empty;
        private LeadUserInfo _leadUserInfo = new LeadUserInfo();
        private string _prefPriceRange = string.Empty;
        private string _bldrCity = string.Empty;
        private string _bldrImportance = string.Empty;
        private string _moveInDate = string.Empty;
        private string _financePref = string.Empty;
        private string _reason = string.Empty;
        private string _fromPage = string.Empty;    //lead source
        private string _referer = string.Empty;
        private string _builderList = string.Empty;
        private string _communityList = string.Empty;
        private string _planList = string.Empty;
        private string _specList = string.Empty;

        private string _realtorInfo = string.Empty;

        private string _prefcity = string.Empty;
        private string _prefstate = string.Empty;
        private string _alertId = string.Empty;
        private string _requestUniqueKey = string.Empty;

        public string AlertId
        {
            get { return _alertId; }
            set { _alertId = value; }
        }

        public bool PrefComingsoon { get; set; }

        public string  Prefstate
        {
            get { return _prefstate; }
            set { _prefstate = value; }
        }   
        public string  Prefcity
        {
            get { return _prefcity; }
            set { _prefcity = value; }
        } 
        public string LeadType
        {
            get { return _leadType; }
            set { _leadType = value; }
        }
        public string LeadAction
        {
            get { return _leadAction; }
            set { _leadAction = value; }
        }
        public string LeadComments
        {
            get { return _leadComments; }
            set { _leadComments = value; }
        }
        public LeadUserInfo LeadUserInfo
        {
            get { return _leadUserInfo; }
            set { _leadUserInfo = value; }
        }

        public int MarketId { get; set; }

        public string BldrCity
        {
            get { return _bldrCity; }
            set { _bldrCity = value; }
        }
        public string BldrImportance
        {
            get { return _bldrImportance; }
            set { _bldrImportance = value; }
        }
        public string MoveInDate
        {
            get { return _moveInDate; }
            set { _moveInDate = value; }
        }
        public string PrefPriceRange
        {
            get { return _prefPriceRange; }
            set { _prefPriceRange = value; }
        }
        public string FinancePref
        {
            get { return _financePref; }
            set
            {   //fb:13677
                if (value != "0")
                {
                    _financePref = value; 
                }
                
            }
        }
        public string Reason
        {
            get { return _reason; }
            set { _reason = value; }
        }
        public string FromPage
        {
            get { return _fromPage; }
            set { _fromPage = value; }
        }
        public string Referer
        {
            get { return _referer; }
            set { _referer = value; }
        }
        public string BuilderList
        {
            get { return _builderList; }
            set { _builderList = value; }
        }
        public string CommunityList
        {
            get { return _communityList; }
            set { _communityList = value; }
        }
        public string PlanList
        {
            get { return _planList; }
            set { _planList = value; }
        }
        public string SpecList
        {
            get { return _specList; }
            set { _specList = value; }
        }

        public int BuilderId { get; set; }

        public bool? IsActiveAdult { get; set; }

        public bool? IsBoyol { get; set; }
        
        public string RealtorInfo
        {
            get { return _realtorInfo; }
            set { _realtorInfo = value; }
        }

        public int RecoCount { get; set; }

        public bool ShowMatchingCommunities { get; set; }
        
        public int SourceCommunityId { get; set; }
        public int SourcePlanId { get; set; }
        public int SourceSpecId { get; set; }
        public string RequestUniqueKey
        {
            get { return _requestUniqueKey; }
            set { _requestUniqueKey = value; }
        }

        public string ClientsInfo { get; set; }
        public bool SuppressEmail { get; set; }
        public bool IsBilled { get; set; }

        public string AgentName { get; set; }
        public string AgentMail { get; set; }
        public int PlanId { get; set; }
        public int SpecId { get; set; }
    }
}
