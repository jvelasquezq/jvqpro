﻿using System.Collections.Generic;

namespace Nhs.Library.Business
{
    public class FeaturedListingMarket
    {
        public int MarketId { get; set; }
        public int CurrentIndex { get; set; } // used to store the current FL for the Market
        private object oLock = new object();

        public List<FeaturedListing> FeaturedListings = new List<FeaturedListing>();
        public FeaturedListing Current()
        {
            lock (oLock)
            {
                if (CurrentIndex == FeaturedListings.Count)
                {
                    CurrentIndex = 1;
                    return this.FeaturedListings[0];
                }
                else
                    return this.FeaturedListings[CurrentIndex++];
            }
        }

        public FeaturedListingMarket(int marketId, List<FeaturedListing> featuredListings)
        {
            MarketId = marketId;
            FeaturedListings = featuredListings;
        }
    }
}
