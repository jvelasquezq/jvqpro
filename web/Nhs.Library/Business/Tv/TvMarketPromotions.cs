﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Nhs.Library.Business.Tv
{
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class TvMarketPromotions
    {        
        [XmlArrayItem()]
        public List<TvMarketPromotion> Promotions { get; set; }
    }

    [XmlRoot("TvMarketPromotion")]
    public class TvMarketPromotion
    {        
        [XmlElement("Headline", IsNullable = true)]
        public string Headline { get; set; }

        [XmlElement("Title")]
        public string Title { get; set; }

        [XmlElement("Description")]
        public string Description { get; set; }

        [XmlElement("Community")]
        public string Community { get; set; }
    }
}
