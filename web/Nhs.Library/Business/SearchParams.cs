﻿using System;
using System.Collections.Generic;
using Bdx.Web.Api.Objects.Constants;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Bdx.Web.Api.Objects;
using Nhs.Library.Business.Seo;
using Nhs.Library.Helpers.WebApiServices;
using Nhs.Mvc.Routing.Interface;

namespace Nhs.Library.Business
{
    [Serializable]
    public class SearchParams : ApiSearchParams
    {
        public void Init(bool fill = false)
        {
            WebApiSearchType = WebApiSearchType.Exact;
            City = string.Empty;
            State = string.Empty;
            PostalCode = string.Empty;
            County = string.Empty;
            MarketName = string.Empty;
            StateName = string.Empty;
            BuilderName = string.Empty;
            SchoolDistrictIds = string.Empty;
            CommName = string.Empty;
            var communityResultsPages = new List<string>
            {
                Pages.CommunityResultsv2.ToLower(),
                Pages.CommunityResults.ToLower(),
                Pages.Comunidades.ToLower()
            };
            SrpType = communityResultsPages.Contains(NhsRoute.CurrentRoute.Function.ToLower())
                                            ? SearchResultsPageType.CommunityResults
                                            : SearchResultsPageType.HomeResults;
            PageSize = 10;
            NoBoyl = true;
            GetLocationCoordinates = true;
            GetRadius = true;
            CustomRadiusSelected = false;
            PartnerId = NhsRoute.PartnerId;
            SortFirstBy = string.Empty;
            SortBy = SortBy.Random;
            PriceLow = 0;
            PriceHigh = 0;
            Bathrooms = 0;
            Bedrooms = 0;
            Garages = 0;
            LivingAreas = 0;
            Stories = 0;
            Green = false;
            Pool = false;
            GolfCourse = false;
            Gated = false;
            Parks = false;
            NatureAreas = false;
            Views = false;
            Waterfront = false;
            Sports = false;
            Adult = false;
            CommunityStatus = String.Empty;
            
            Zoom = 5;
            if (fill)
                this.SetFromRouteParam(NhsRoute.CurrentRoute.Params);
        }
 
        public WebApiSearchType WebApiSearchType { get; set; }
        public bool ComingSoon { get; set; }
        public bool GetLocationCoordinates { get; set; }
        public bool GetRadius { get; set; }
        public bool CustomRadiusSelected { get; set; }
        public SearchResultsPageType SrpType { get; set; }
        public bool IsMapVisible { get; set; }
        public int Zoom { get; set; }
        public bool IsCurrentLocation { get; set; }
        public bool ShowResultsBanner { get; set; }
        public int ResultsBannerPosition { get; set; }
        public bool IsBuilderTabSearch { get; set; }
        public bool IsMultiLocationSearch { get; set; }
        public Synthetic SyntheticInfo { get; set; }

        
    }

    public class MapPoint
    {
        public string Lat { get; set; }
        public string Lng { get; set; }
    }
}
