﻿using System.Collections.Generic;

namespace Nhs.Library.Business.Ad
{    
    public partial class AdsBrandPartner
    {
        public int BrandPartnerId { get; set; }
        public bool AllPartners { get; set; }
        public List<int> Markets { get; set; }

        public AdsBrandPartner()
        {
            Markets = new List<int>();
        }
    }
}
