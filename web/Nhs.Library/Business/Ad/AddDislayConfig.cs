using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Nhs.Library.Business.Ad
{
    [Serializable]
    [XmlRoot("addisplayconfig")]
    public class AddDislayConfig
    {
        public AddDislayConfig()
        {
            AdDisplay = new List<AdDisplay>();
        }

        [XmlElement("addisplay")]
        public List<AdDisplay> AdDisplay { get; set; }
    }

    public class AdDisplay
    {
        public AdDisplay()
        {
            AdSlots = new List<AdDisplaySlot>();
        }

         [XmlAttribute("urlpattern")]
        public string UrlPattern { get; set; }

        [XmlElement("adslot")]
        public List<AdDisplaySlot> AdSlots { get; set; }
    }

    public class AdDisplaySlot
    {
        [XmlAttribute("placeholder")]
        public string PlaceHolder { get; set; }

        [XmlAttribute("show")]
        public bool Show { get; set; }

        [XmlAttribute("position")]
        public int Position { get; set; }
    }
}