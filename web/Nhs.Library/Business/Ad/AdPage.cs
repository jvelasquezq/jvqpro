﻿using System.Collections.Generic;

namespace Nhs.Library.Business.Ad
{
    public class AdPage
    {
        public string Name { get; set; }
        public string AdPageCode { get; set; }
        public List<string> Positions { get; set; }
        public string UrlPattern { get; set; }
    }
}
