﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Library.Business.Ad
{
    public class AdSlot
    {
        public string PlaceHolder { get; set; }
        public List<string> AdPositions { get; set; }
        public string Height { get; set; }
        public string Width { get; set; }
    }
}
