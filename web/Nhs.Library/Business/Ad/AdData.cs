﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Library.Business.Ad
{
    public class AdData
    {
        public string AdPosition { get; set; }
        public string AdHtml { get; set; }
        public string TargetPlaceHolder { get; set; }
        public string Height { get; set; }
        public string Width { get; set; }
    }
}
