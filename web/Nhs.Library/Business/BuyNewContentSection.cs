﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Library.Business
{
    public class BuyNewContentSection
    {
        public int Order { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
        public string ImageText { get; set; }
    }
}
