using System;
using Nhs.Library.Enums;

namespace Nhs.Library.Business
{
    [Serializable]
    public class PlannerListing : IComparable
    {
        #region "Properties"

        public string SourceRequestKey { get; set; }
        public string UserId { get; set; }
        public ListingType ListingType { get; set; }
        public int ListingId { get; set; }
        public int BuilderId { get; set; }
        public DateTime SaveDate { get; set; }
        public DateTime LeadRequestDate { get; set; }
        public bool RecommendedFlag { get; set; }
        public bool SuppressFlag { get; set; }
        public int PlannerId { get; set; }

        #endregion

        public PlannerListing()
        {
            LeadRequestDate = DateTime.MinValue;
            SaveDate = DateTime.MinValue;
        }

        public PlannerListing(int listingId, ListingType listingType) : this()
        {
            ListingId = listingId;
            ListingType = listingType;
        }

        public PlannerListing(int listingId, int builderId, ListingType listingType)
            : this(listingId, listingType)
        {
            BuilderId = builderId;
        }

        public int CompareTo(object obj)
        {
            var listing = (PlannerListing) obj;
            return ListingId.CompareTo(listing.ListingId);
        }

        // GetHashCode() method must be updated to reflect changes
        // made to this method's logic
        public override bool Equals(object obj)
        {
            try
            {
                var listing = (PlannerListing) obj;
                return (ListingType == listing.ListingType && ListingId == listing.ListingId);
            }
            catch
            {
                return false;
            }
        }

        // Equals() method must be updated to reflect changes
        // made to this method's logic
        public override int GetHashCode()
        {
            // Two objects that are equal must return the same hash value.
            // Calculate Hash to match Equals() method
            var hash = 23;
            hash = hash*37 + ListingType.GetHashCode();
            hash = hash*37 + ListingId;
            return hash;
        }
    }
}