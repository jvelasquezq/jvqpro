﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Library.Web.ImageResizer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    public enum CarveCommand
    {
        None,
        True,
        Prewitt,
        V1,
        VSquare,
        Sobel,
        Laplacian
    }

    public enum ScaleCommand
    {
        None,
        Both,
        UpscaleOnly,
        DownscaleOnly,
        UpscaleCanvas
    }

    public enum FlipCommand
    {
        None,
        Horizontal,
        Vertical,
        Both
    }

    public enum ImageFormat
    {
        None,
        Jpg,
        Png,
        Gif
    }

    public enum CacheSetting
    {
        None,
        Default,
        Always,
        No
    }

    public struct CropRectangle
    {
        public CropRectangle(int x1, int y1, int x2, int y2)
        {
            _x1 = x1;
            _y1 = y1;
            _x2 = x2;
            _y2 = y2;
        }

        private readonly int _x1;
        private readonly int _y1;
        private readonly int _x2;
        private readonly int _y2;

        public bool HasArea
        {
            get { return _x1 + _x2 + _y1 + _y2 > 0; }
        }

        public override string ToString()
        {
            return string.Format("({0},{1},{2},{3})", _x1, _y1, _x2, _y2);
        }
    }

    public class ResizeCommands
    {
        public ResizeCommands()
        {
            Format = ImageFormat.Jpg;
            Scale = ScaleCommand.UpscaleCanvas;
        }


        #region Properties


        /// <summary>
        /// Force the final width to certain dimensions. Whitespace will be added if the aspect ratio is different.
        /// </summary>
        public int? Width { get; set; }

        /// <summary>
        /// Force the final height to certain dimensions. Whitespace will be added if the aspect ratio is different.
        /// </summary>
        public int? Height { get; set; }

        /// <summary>
        /// Fit the image within the specified width, preserving aspect ratio.
        /// </summary>
        public int? MaxWidth { get; set; }

        /// <summary>
        /// Fit the image within the specified height, preserving aspect ratio.
        /// </summary>
        public int? MaxHeight { get; set; }

        /// <summary>
        /// Crop the image to the specified rectangle on the source image. 
        /// You can use negative coordinates to specify bottom-right relative locations.
        /// </summary>
        public CropRectangle Crop { get; set; }

        /// <summary>
        /// Crop the image the the size specified by width and height. 
        /// Centers and minimally crops to preserve aspect ratio.
        /// </summary>
        public bool AutoCrop { get; set; }

        /// <summary>
        /// Rotate the image.
        /// </summary>
        public double? Rotate { get; set; }

        /// <summary>
        /// Sets the background/whitespace color.
        /// </summary>
        public string BackgroundColor { get; set; }

        /// <summary>
        /// Stretches the image to width and height if both are specified. 
        /// This is the only way to lose aspect ratio.
        /// </summary>
        public bool Stretch { get; set; }

        /// <summary>
        /// By default, images are never upscaled. Use ScaleCommand.Both to grow an image.
        /// </summary>
        public ScaleCommand Scale { get; set; }

        /// <summary>
        /// Flips the image after resizing.
        /// </summary>
        public FlipCommand Flip { get; set; }

        /// <summary>
        /// Flips the source image before resizing/rotation.
        /// </summary>
        public FlipCommand SourceFlip { get; set; }

        /// <summary>
        /// Thickness of padding to add around edge of image.
        /// </summary>
        public int? PaddingWidth { get; set; }

        /// <summary>
        /// Color of padded area around image.
        /// Defaults to BackgroundColor, which defaults to white.
        /// </summary>
        public string PaddingColor { get; set; }

        /// <summary>
        /// Thickness of border to add around edge of image.
        /// </summary>
        public int? BorderWidth { get; set; }

        /// <summary>
        /// Color of border around image.
        /// </summary>
        public string BorderColor { get; set; }

        /// <summary>
        /// The output image format to use. (Jpg, Gif, Png)
        /// </summary>
        public ImageFormat Format { get; set; }

        /// <summary>
        /// Control the palette size of PNG and GIF images. If omitted, PNGs will be 24-bit.
        /// </summary>
        public int? Colors { get; set; }

        /// <summary>
        /// Choose which frame of an animated GIF to display.
        /// </summary>
        public int? Frame { get; set; }

        /// <summary>
        /// Choose which page of a multi-page TIFF document to display.
        /// </summary>
        public int? Page { get; set; }

        /// <summary>
        /// Jpeg compression value.
        /// 100 = best, 90 = very good balance, 0 = ugly
        /// </summary>
        public int? Quality { get; set; }

        /// <summary>
        /// If true, the ICC profile embedded in the source image will be ignored.
        /// </summary>
        public bool IgnoreICC { get; set; }

        /// <summary>
        /// Always forces the image to be cached even if it wasn't modified by the resizing module. 
        /// No disables caching even if it was.
        /// </summary>
        public CacheSetting Cache { get; set; }

        /// <summary>
        /// Intensity of sharpen filter to apply to image
        /// </summary>
        public double Sharpen { get; set; }

        /// <summary>
        /// Intensity of blur filter to apply to image
        /// </summary>
        public double Blur { get; set; }

        /// <summary>
        /// Provides content-aware image resizing and 5 different algorithms.
        /// </summary>
        public CarveCommand Carve { get; set; }

        /// <summary>
        /// Contrast level used when searching for image contents.
        /// Valid values are 0 through 255
        /// </summary>
        public int? TrimThreshold { get; set; }

        /// <summary>
        /// Percentage of padding to add to the outside edge of trimmed image contents.
        /// </summary>
        public double? TrimPadding { get; set; }


        /// <summary>
        /// Whether or not any values have been set
        /// </summary>
        public bool HasValues
        {
            get { return ToString().Length > 0; }
        }

        #endregion

        #region ToString()

        public IEnumerable<KeyValuePair<string, string>> GetValues()
        {
            if (Width.HasValue)
                yield return new KeyValuePair<string, string>("width", Width.Value.ToString());

            if (Height.HasValue)
                yield return new KeyValuePair<string, string>("height", Height.Value.ToString());

            if (MaxWidth.HasValue)
                yield return new KeyValuePair<string, string>("maxwidth", MaxWidth.Value.ToString());

            if (MaxHeight.HasValue)
                yield return new KeyValuePair<string, string>("maxheight", MaxHeight.Value.ToString());

            if (AutoCrop)
                yield return new KeyValuePair<string, string>("crop", "auto");

            if (Crop.HasArea)
                yield return new KeyValuePair<string, string>("crop", Crop.ToString());

            if (Rotate.HasValue)
                yield return new KeyValuePair<string, string>("rotate", Rotate.Value.ToString());

            if (!string.IsNullOrWhiteSpace(BackgroundColor))
                yield return new KeyValuePair<string, string>("bgcolor", BackgroundColor.Trim());

            if (Stretch)
                yield return new KeyValuePair<string, string>("stretch", "fill");

            if (Scale != ScaleCommand.None)
                yield return new KeyValuePair<string, string>("scale", Scale.ToString().ToLower());

            if (Flip != FlipCommand.None)
                yield return new KeyValuePair<string, string>("flip", FlipCommandToString(Flip));

            if (SourceFlip != FlipCommand.None)
                yield return new KeyValuePair<string, string>("sourceFlip", FlipCommandToString(SourceFlip));

            if (PaddingWidth.HasValue)
                yield return new KeyValuePair<string, string>("paddingWidth", PaddingWidth.Value.ToString());

            if (!string.IsNullOrWhiteSpace(PaddingColor))
                yield return new KeyValuePair<string, string>("paddingColor", PaddingColor.Trim());

            if (BorderWidth.HasValue)
                yield return new KeyValuePair<string, string>("borderWidth", BorderWidth.Value.ToString());

            if (!string.IsNullOrWhiteSpace(BorderColor))
                yield return new KeyValuePair<string, string>("borderColor", BorderColor.Trim());

            if (Format != ImageFormat.None)
                yield return new KeyValuePair<string, string>("format", Format.ToString().ToLower());

            if (Colors.HasValue)
                yield return new KeyValuePair<string, string>("colors", Colors.Value.ToString());

            if (Frame.HasValue)
                yield return new KeyValuePair<string, string>("frame", Frame.Value.ToString());

            if (Page.HasValue)
                yield return new KeyValuePair<string, string>("page", Page.Value.ToString());

            if (Quality.HasValue)
                yield return new KeyValuePair<string, string>("quality", Quality.Value.ToString());

            if (IgnoreICC)
                yield return new KeyValuePair<string, string>("ignoreicc", "true");

            if (Cache != CacheSetting.None)
                yield return new KeyValuePair<string, string>("cache", Cache.ToString().ToLower());

            if (Sharpen > 0)
                yield return new KeyValuePair<string, string>("sharpen", Sharpen.ToString("N1"));

            if (Blur > 0)
                yield return new KeyValuePair<string, string>("blur", Blur.ToString("N1"));

            if (Carve != CarveCommand.None)
                yield return new KeyValuePair<string, string>("carve", Carve.ToString().ToLower());

            if (TrimThreshold.HasValue)
            {
                yield return new KeyValuePair<string, string>("trim.threshold", TrimThreshold.Value.ToString());
                if (TrimPadding.HasValue)
                    yield return new KeyValuePair<string, string>("trim.percentpadding", TrimPadding.Value.ToString("N1"));
            }
        }

        public string Serialize()
        {
            return ToString();
        }

        public override string ToString()
        {
            return string.Join("&", GetValues().Select(x => string.Format("{0}={1}", x.Key, HttpUtility.UrlEncode(x.Value))).ToArray());
        }

        #endregion

        private static string FlipCommandToString(FlipCommand fc)
        {
            var conversions = new Dictionary<FlipCommand, string>
                                {
                                    { FlipCommand.Both, "both"},
                                    { FlipCommand.Horizontal, "h"},
                                    { FlipCommand.Vertical, "v"}
                                };

            return conversions.ContainsKey(fc) ? conversions[fc] : null;
        }
    }
}
