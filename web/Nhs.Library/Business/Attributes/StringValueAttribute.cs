﻿using System;

namespace Nhs.Library.Business.Attributes
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public class StringValueAttribute: Attribute
    {
        public string Value { get; set; }
    }
}
