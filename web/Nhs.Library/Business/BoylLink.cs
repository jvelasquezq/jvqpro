﻿namespace Nhs.Library.Business
{
    public class BoylLink
    {
        public int CommunityId { get; set; }
        public int BuilderId { get; set; }
        public string BrandName { get; set; }
        public string BuilderUrl{ get; set; }
        public string BuilderUrlLog { get; set; }
    }
}
