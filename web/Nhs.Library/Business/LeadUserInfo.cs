using Nhs.Library.Constants;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Web;
using Nhs.Utility.Common;

namespace Nhs.Library.Business
{
    public class LeadUserInfo
    {
        private string _guid = string.Empty;
        private string _email = string.Empty;
        private string _firstName = string.Empty;
        private string _lastName = string.Empty;
        private string _address = string.Empty;
        private string _city = string.Empty;
        private string _state = string.Empty;
        private string _postalCode = string.Empty;
        private string _phone = string.Empty;
        private string _phoneExt = string.Empty;
        private string _sessionId = string.Empty;
        private string _userCookie = string.Empty;
        
        public string Guid
        {
            get { return _guid; }
            set { _guid = value; }
        }
        public string Email
        {
            get
            {                
                return _email;
            }
            set
            {
                _email = value;
                CookieManager.EncEmail = CryptoHelper.Encrypt(value, CookieConst.EncEmail);
            }
        }
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
        public string PostalCode
        {
            get { return _postalCode; }
            set { _postalCode = value; }
        }
        public string Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }
        public string PhoneExt
        {
            get { return _phoneExt; }
            set { _phoneExt = value; }
        }

        public string SecondPhone { get; set; }

        public string SecondPhoneExt { get; set; }

        public string SessionId
        {
            get { return _sessionId; }
            set { _sessionId = value; }
        }
        public string UserCookie
        {
            get { return _userCookie; }
            set { _userCookie = value; }
        }
    }
}
