﻿namespace Nhs.Library.Business
{
    public class BoylResult : CommunityResult
    {
        public bool IsCnh { get; set; }
        public string MarketName { get; set; }
        public string StateName { get; set; }
        public int NhsBuilderId { get; set; }
        public bool HasSpecialOffer { get; set; }
        public int SquareFeetLow { get; set; }
        public int SquareFeetHigh { get; set; }
        public string BrandImageThumbnailMedium { get; set; }
        public bool IsBasicCommunity { get; set; }
        public string Phone { get; set; }

        public BoylResult()
        {
            Phone = "512-123-4444";
        }
    }
}
