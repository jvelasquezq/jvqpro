using System;
using System.Collections.Generic;
using System.Data;
using Nhs.Mvc.Data.Repositories;
using Nhs.Utility.Common;

namespace Nhs.Library.Business
{
    [Serializable]
    public class SearchAlert : ISearchAlert
    {
        #region Members

        public SearchAlert()
        {
            HomeType = string.Empty;
            PostalCode = string.Empty;
            City = string.Empty;
            State = string.Empty;
            Title = string.Empty;
            UserId = string.Empty;
        }

        #endregion

        #region Properties

        public int AlertId { get; set; }
        public string UserId { get; set; }
        public string Title { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateExpired { get; set; }
        public string State { get; set; }
        public int MarketId { get; set; }
        public string MarketName { get; set; }
        public string CityName { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public int? Radius { get; set; }
        public int PriceMin { get; set; }
        public int PriceMax { get; set; }
        public int? Bedrooms { get; set; }
        public string HomeType { get; set; }
        public bool SingleFamilyHomes { get; set; }
        public bool CondoTownhomeHomes { get; set; }
        public bool BuildOnYourLot { get; set; }
        public bool Pool { get; set; }
        public bool GolfCourse { get; set; }
        public bool GatedCommunity { get; set; }
        public string SchoolDistrictName { get; set; }
        public int? SchoolDistrictId { get; set; }
        public string BuilderName { get; set; }
        public int? BuilderId { get; set; }

        #endregion

        #region Methods

        public void DataBind(int alertId)
        {
            DataTable dt = DataProvider.Current.GetSearchAlertByAlertId(alertId);
            if (dt.Rows.Count > 0)
            {
                this.DataBind(dt.Rows[0]);
            }
            else
            {
                throw new Exception("Invalid Alert Id");
            }
        }

        public void DataBind(DataRow dr)
        {
            AlertId = DBValue.GetInt(dr["alert_id"]);
            Bedrooms = DBValue.GetInt(dr["bedrooms"]);
            BuilderId = DBValue.GetInt(dr["builder_id"]);
            BuildOnYourLot = DBValue.GetBool(dr["boyl"]);
            City = DBValue.GetString(dr["city"]);
            CondoTownhomeHomes = DBValue.GetBool(dr["condo_townhome_homes"]);
            DateCreated = DBValue.GetDateTime(dr["date_created"]);
            DateExpired = DBValue.GetDateTime(dr["expiration_date"]);
            GatedCommunity = DBValue.GetBool(dr["gated_community"]);
            GolfCourse = DBValue.GetBool(dr["golf_course"]);
            MarketId = DBValue.GetInt(dr["market_id"]);
            Pool = DBValue.GetBool(dr["pool"]);
            PostalCode = DBValue.GetString(dr["postal_code"]);
            PriceMax = DBValue.GetInt(dr["price_max"]);
            PriceMin = DBValue.GetInt(dr["price_min"]);
            Radius = DBValue.GetInt(dr["radius"]);
            SchoolDistrictId = DBValue.GetInt(dr["school_district_id"]);
            SingleFamilyHomes = DBValue.GetBool(dr["single_family_homes"]);
            State = DBValue.GetString(dr["state"]);
            Title = DBValue.GetString(dr["title"]);
            UserId = DBValue.GetString(dr["g_user_id"]);
        }

        public void SaveResults(List<CommunityResult> alertResults)
        {
            foreach (CommunityResult row in alertResults)
                DataProvider.Current.SearchAlertSaveSingleResult(this.AlertId, row.CommunityId, row.BuilderId);
        }

        public void RemoveAlert()
        {
            DataProvider.Current.SearchAlertRemove(this.AlertId);
        }

        #endregion

        #region Static Methods

        public static string GenerateSuggestedName(string city, string areaText, string areaValue, string zip, string priceFrom, string priceTo, IList<string> userAlertsNames)
        {
            // Checks fields aren't empty
            if ((areaValue != string.Empty || zip != string.Empty) && (priceFrom != string.Empty && priceTo != string.Empty))
            {
                // Sets geography name (city, area and zip in that order)
                string geography;
                if (city != String.Empty)
                    geography = city;
                else if (areaValue != String.Empty)
                    geography = areaText;
                else
                    geography = zip;

                // Generates suggested name
                string suggestedName = String.Format("Alert: [{0}], ${1}-${2}", geography, priceFrom, priceTo);

                // Verifies that it's unique
                string uniqueName = suggestedName;
                if (userAlertsNames.Contains(uniqueName))
                {
                    for (int i = 1; userAlertsNames.Contains(uniqueName); i++)
                        uniqueName = String.Format("{0}_{1}", suggestedName, i);
                }
                return uniqueName;
            }
            return String.Empty;
        }

        #endregion
    }
}
