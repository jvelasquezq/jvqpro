using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using Nhs.Library.Enums;
using Nhs.Library.Helpers;
using Nhs.Library.Web;
using Nhs.Mvc.Data.Repositories;
using Nhs.Utility.Common;

namespace Nhs.Library.Business
{
    [Serializable]
    public class Planner : IPlanner
    {
        #region "Members"
        private string _userId = string.Empty;
        private List<PlannerListing> _savedHomes = new List<PlannerListing>();
        private List<PlannerListing> _savedCommunities = new List<PlannerListing>();
        private List<PlannerListing> _recommendedListings = new List<PlannerListing>();
        private List<PlannerListing> _requestedListings = new List<PlannerListing>();
        private List<PlannerListing> _recentlyViewedHomes = new List<PlannerListing>();
        private List<SearchAlert> _savedSearchAlerts = new List<SearchAlert>();
        #endregion

        #region "Properties"
        public ReadOnlyCollection<PlannerListing> SavedHomes
        {
            get { return new ReadOnlyCollection<PlannerListing>(_savedHomes); }
        }

        public ReadOnlyCollection<PlannerListing> SavedCommunities
        {
            get { return new ReadOnlyCollection<PlannerListing>(_savedCommunities); }
        }

        public ReadOnlyCollection<SearchAlert> SavedSearchAlerts
        {
            get { return new ReadOnlyCollection<SearchAlert>(_savedSearchAlerts); }
        }

        public ReadOnlyCollection<PlannerListing> RecommendedListings
        {
            get { return new ReadOnlyCollection<PlannerListing>(_recommendedListings); }
        }

        public ReadOnlyCollection<PlannerListing> RequestedListings
        {
            get { return new ReadOnlyCollection<PlannerListing>(_requestedListings); }
        }

        public ReadOnlyCollection<PlannerListing> RecentlyViewedHomes
        {
            get { return new ReadOnlyCollection<PlannerListing>(_recentlyViewedHomes); }
        }

        public string UserId
        {
            get { return _userId; }
        }
        #endregion

        #region "Constructor"
        public Planner(string userId)
        {
            _userId = userId;
        }
        #endregion

        #region "Public Methods"
        public void DataBind(string userId)
        {
            // Get all saved listings from data provider. 
            DataTable savedListings = DataProvider.Current.GetSavedListings(userId);

            // Loop through all listings. 
            foreach (DataRow row in savedListings.Rows)
            {
                // Create temp listing object and populate with db data. 
                PlannerListing listing = new PlannerListing
                    {
                        PlannerId = DBValue.GetInt(row["planner_id"]),
                        UserId = DBValue.GetString(row["g_user_id"]),
                        ListingType = EnumConversionUtil.StringToListingType(DBValue.GetString(row["listing_type"])),
                        ListingId = DBValue.GetInt(row["listing_id"]),
                        BuilderId = DBValue.GetInt(row["builder_id"]),
                        SaveDate = DBValue.GetDateTime(row["save_date"]),
                        LeadRequestDate = DBValue.GetDateTime(row["lead_request_date"]),
                        RecommendedFlag = DBValue.GetBool(row["rec_flag"]),
                        SuppressFlag = DBValue.GetBool(row["suppress_flag"]),
                        SourceRequestKey = DBValue.GetString(row["source_request_key"])
                    };

                // Add listing to appropriate saved list. 
                switch (listing.ListingType)
                {
                    case ListingType.Plan:
                        _savedHomes.Add(listing);
                        break;
                    case ListingType.Spec:
                        _savedHomes.Add(listing);
                        break;
                    case ListingType.Community:
                        if (!listing.RecommendedFlag)
                        {
                            _savedCommunities.Add(listing);
                        }
                        break;
                }


                // See if the listing has been requested.
                if (listing.LeadRequestDate != DateTime.MinValue)
                {
                    _requestedListings.Add(listing);
                }

                // If recommended and not suppresed, add to recommended list. 
                if (listing.RecommendedFlag && !listing.SuppressFlag)
                {
                    _recommendedListings.Add(listing);
                }
            }

            // Get saved alerts
            DataTable savedAlerts = DataProvider.Current.GetSearchAlertsByUserId(userId);
            foreach (DataRow row in savedAlerts.Rows)
            {
                SearchAlert alert = new SearchAlert();

                alert.AlertId = DBValue.GetInt(row["alert_id"]);
                alert.UserId = DBValue.GetString(row["g_user_id"]);
                alert.Title = DBValue.GetString(row["title"]);
                alert.State = DBValue.GetString(row["state"]);
                alert.MarketId = DBValue.GetInt(row["market_id"]);
                alert.City = DBValue.GetString(row["city"]);
                alert.PostalCode = DBValue.GetString(row["postal_code"]);
                alert.Radius = DBValue.GetInt(row["radius"]);
                alert.PriceMin = DBValue.GetInt(row["price_min"]);
                alert.PriceMax = DBValue.GetInt(row["price_max"]);
                alert.Bedrooms = DBValue.GetInt(row["bedrooms"]);
                alert.CondoTownhomeHomes = DBValue.GetBool(row["condo_townhome_homes"]);
                alert.SingleFamilyHomes = DBValue.GetBool(row["single_family_homes"]);
                alert.BuildOnYourLot = DBValue.GetBool(row["boyl"]);
                alert.Pool = DBValue.GetBool(row["pool"]);
                alert.GolfCourse = DBValue.GetBool(row["golf_course"]);
                alert.GatedCommunity = DBValue.GetBool(row["gated_community"]);
                alert.SchoolDistrictId = DBValue.GetInt(row["school_district_id"]);
                alert.BuilderId = DBValue.GetInt(row["builder_id"]);
                alert.DateCreated = DBValue.GetDateTime(row["date_created"]);
                alert.DateExpired = DBValue.GetDateTime(row["expiration_date"]);

                _savedSearchAlerts.Add(alert);
            }
        }

        public IEnumerable<PlannerListing> GetSavedProperties()
        {
            // Get all saved listings from data provider. 
            DataTable savedListings = DataProvider.Current.GetSavedListingForProCrm(_userId);
            // Loop through all listings. 
            return savedListings.Rows.Count == 0
                       ? new List<PlannerListing>()
                       : savedListings.Rows.Cast<DataRow>().Select(row => 
                           new PlannerListing {
                               ListingId = DBValue.GetInt(row["listing_id"]),
                               ListingType = EnumConversionUtil.StringToListingType(DBValue.GetString(row["listing_type"]))});
        }

        public void AddRecentlyViewedHome(int listingId, bool isSpec)
        {
            PlannerListing listing = GetHomeListing(listingId, isSpec);

            foreach (PlannerListing pl in _recentlyViewedHomes)
            {
                if (pl.ListingId == listingId)
                {
                    return;
                }
            }

            _recentlyViewedHomes.Add(listing);
        }

        public void AddSavedHome(int listingId, bool isSpec)
        {
            PlannerListing listing = GetHomeListing(listingId, isSpec);

            if (!SavedHomeExists(listingId, isSpec))
            {
                _savedHomes.Add(listing);
                SaveListing(listing);
            }
        }

        private bool SavedHomeExists(int listingId, bool isSpec)
        {
            bool result = false;
            ListingType type = isSpec ? ListingType.Spec : ListingType.Plan;

            foreach (PlannerListing listing in _savedCommunities)
            {
                if (listing.ListingId == listingId && listing.ListingType == type)
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

        public void AddSavedCommunity(int communityId, int builderId)
        {
            PlannerListing listing = GetCommunityListing(communityId, builderId, false);
            if (!SavedCommunityExists(communityId, builderId))
            {
                _savedCommunities.Add(listing);
                SaveListing(listing);
            }
        }

        public void AddSavedSearchAlert(SearchAlert alert)
        {
            _savedSearchAlerts.Add(alert);
        }

        private bool SavedCommunityExists(int communityId, int builderId)
        {
            bool result = false;

            foreach (PlannerListing listing in _savedCommunities)
            {
                if (listing.ListingId == communityId && listing.BuilderId == builderId)
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

        public void AddRecommendedCommunity(int communityId, int builderId)
        {
            PlannerListing listing = GetCommunityListing(communityId, builderId, true);
            _recommendedListings.Add(listing);
            SaveListing(listing);
        }

        public void RemoveRecentlyViewedHome(int listingId, bool isSpec)
        {
            PlannerListing listing = GetHomeListing(listingId, isSpec);
            _recentlyViewedHomes.Remove(listing);
        }

        public void RemoveSavedHome(int listingId, bool isSpec)
        {
            PlannerListing listing = GetHomeListing(listingId, isSpec);
            _savedHomes.Remove(listing);

            DeleteListing(listing);
        }

        public void RemoveSavedCommunity(int communityId, int builderId)
        {
            PlannerListing listing = GetCommunityListing(communityId, builderId, false);
            _savedCommunities.Remove(listing);

            DeleteListing(listing);
        }

        public void RemoveSavedAlert(SearchAlert alert)
        {
            _savedSearchAlerts.Remove(alert);
            alert.RemoveAlert();
        }

        public void RemoveAllSavedAlerts()
        {
            _savedSearchAlerts.Clear();        
        }

        public void ExpireAllSavedAlerts()
        {
            foreach (SearchAlert alert in _savedSearchAlerts)
            {
                alert.DateExpired = DateTime.Now;
            }
        }

        public void RemoveRecommendedCommunity(int communityId, int builderId)
        {
            PlannerListing listing = GetCommunityListing(communityId, builderId, true);
            _recommendedListings.Remove(listing);
        }

        public void RemoveRecommendedCommunities(string communityList, int builderId)
        {
            string[] communities = communityList.Split(',');
            foreach (string cid in communities)
            {
                RemoveRecommendedCommunity(Convert.ToInt32(cid), builderId);
            }
        }

        public void RemoveAllRecommendedCommunities()
        {
            _recommendedListings.Clear();
            DeleteAllRecommendedCommunities();
        }

        public void RemoveArchivedRecommendedCommunities()
        {
            DeleteArchivedRecommendedCommunities();
        }

        public void UpdateCommunityRequestDate(int communityId, int builderId, string sourceRequestKey)
        {
            PlannerListing listing = GetCommunityListing(communityId, builderId, false);
            listing.SourceRequestKey = sourceRequestKey;
            UpdateRequestDate(listing);

            foreach (PlannerListing pl in _savedCommunities)
            {
                if (pl.BuilderId == builderId && pl.ListingId == communityId)
                {
                    pl.LeadRequestDate = DateTime.Now;
                    pl.SourceRequestKey = sourceRequestKey;
                }
            }
            foreach (PlannerListing pl in _recommendedListings)
            {
                if (pl.BuilderId == builderId && pl.ListingId == communityId)
                {
                    pl.LeadRequestDate = DateTime.Now;
                    pl.SourceRequestKey = sourceRequestKey;
                }
            }
        }

        public void UpdateHomeRequestDate(int listingId, bool isSpec, string sourceRequestKey)
        {
            PlannerListing listing = GetHomeListing(listingId, isSpec);
            listing.SourceRequestKey = sourceRequestKey;
            UpdateRequestDate(listing);

            foreach (PlannerListing pl in _savedHomes)
            {
                if (isSpec)
                {
                    if (pl.ListingId == listingId && pl.ListingType == ListingType.Spec)
                    {
                        pl.LeadRequestDate = DateTime.Now;
                        pl.SourceRequestKey = sourceRequestKey;
                    }

                }
                else
                {
                    if (pl.ListingId == listingId && pl.ListingType == ListingType.Plan)
                    {
                        pl.LeadRequestDate = DateTime.Now;
                        pl.SourceRequestKey = sourceRequestKey;
                    }

                }
            }
        }

        public void ClearRecentlyViewedHomes()
        {
            _recentlyViewedHomes.Clear();
        }

        public PlannerListing GetHomeListing(int listingId, bool isSpec)
        {
            PlannerListing listing = new PlannerListing();
            listing.UserId = UserSession.UserProfile.UserID;
            listing.ListingId = listingId;
            listing.RecommendedFlag = false;
            listing.SuppressFlag = false;

            if (isSpec)
            {
                listing.ListingType = ListingType.Spec;
            }
            else
            {
                listing.ListingType = ListingType.Plan;
            }

            return listing;
        }

        public PlannerListing GetCommunityListing(int communityId, int builderId, bool isRecommended)
        {
            PlannerListing listing = new PlannerListing();
            listing.UserId = UserSession.UserProfile.UserID;
            listing.ListingId = communityId;
            listing.BuilderId = builderId;
            listing.ListingType = ListingType.Community;

            if (isRecommended)
            {
                listing.RecommendedFlag = true;
            }
            else
            {
                listing.RecommendedFlag = false;
            }

            return listing;
        }

        public DataTable GetSavedHomes(int partnerId, string plans, string specs)
        {
            return DataProvider.Current.GetSavedHomes(partnerId, plans, specs);
        }

        #endregion

        #region "Private Methods"

        private void SaveListing(PlannerListing listing)
        {
            // Make sure the user id has been set.  
            if (listing.UserId == string.Empty)
            {
                listing.UserId = UserSession.UserProfile.UserID;
            }

            if (listing.RecommendedFlag)
            {
                DataProvider.Current.AddRecommendedListing(listing.UserId,
                    EnumConversionUtil.ListingTypeToString(listing.ListingType),
                    listing.ListingId, listing.BuilderId, listing.RecommendedFlag);
            }
            else
            {
                DataProvider.Current.AddSavedListing(listing.UserId,
                    EnumConversionUtil.ListingTypeToString(listing.ListingType),
                    listing.ListingId, listing.BuilderId);
            }
        }

        private void DeleteListing(PlannerListing listing)
        {
            // Make sure the user id has been set.  
            if (listing.UserId == string.Empty)
            {
                listing.UserId = _userId;
            }

            ListingHelper.RemoveSavedListing(listing);
        }

        private void DeleteAllRecommendedCommunities()
        {
            DataProvider.Current.RemoveRecommendedListing(_userId, "C");
        }

        private void DeleteArchivedRecommendedCommunities()
        {
            DataProvider.Current.RemoveRecommendedListing(_userId, "C", true);
        }

        private void UpdateRequestDate(PlannerListing listing)
        {
            // Make sure the user id has been set.  
            if (listing.UserId == string.Empty)
            {
                listing.UserId = _userId;
            }

            DataProvider.Current.UpdateRequestDate(listing.UserId,
                EnumConversionUtil.ListingTypeToString(listing.ListingType),
                listing.ListingId, listing.SourceRequestKey);
        }

        #endregion
    }
}
