using System.Data;
using Nhs.Mvc.Data.Repositories;

namespace Nhs.Library.Business
{
    public class ArticleSponsor
    {
        private int _contentID = -1;
        private int _sponsorID = -1;
        private int _contentLanguage = -1;
        private string _sponsorName = "";
        private string _sponsorURL = "";

        public ArticleSponsor(int contentID, int contentLanguage)
        {
            this._contentID = contentID;
            this._contentLanguage = contentLanguage;
            this.GetSponsorInfo();
        }

        public string SponsorName
        {
            get { return _sponsorName; }
        }

        public string SponsorURL
        {
            get { return _sponsorURL; }
        }

        public int SponsorID
        {
            get { return _sponsorID; }
        }

        private void GetSponsorInfo()
        {
            DataTable sponsorInfo = DataProvider.Current.GetArticleSponsor(_contentID, _contentLanguage);
            DataRow row = null;

            try
            {
                row = sponsorInfo.Rows[0];
                _sponsorID = int.Parse(row["sponsor_id"].ToString());
                _sponsorName = row["sponsor_name"].ToString();
                _sponsorURL = row["url_company_website"].ToString();
            }
            catch
            {
                // Ignore error
            }
            finally
            {
                sponsorInfo.Dispose();
            }
            sponsorInfo = null;
            row = null;
        }
    }
}
