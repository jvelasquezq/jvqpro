﻿using System.Collections.Generic;
using System.Linq;
using Nhs.Library.Constants.Enums;

namespace Nhs.Library.Business.Seo
{
    public struct SeoDetailPages
    {
        public int? CommunityId { get; set; }
        public SeoDetailPagesTypeEnum SeoDetailPagesType { get; set; }
        public List<MetaTag> MetaTags { get; set; }
        public string Seo { get; set; }
        public string H1 { get; set; }
        public string Canonical { get; set; }

        public bool HasValues
        {
            get 
            { 
                return !string.IsNullOrWhiteSpace(H1) 
                    || (MetaTags != null && MetaTags.Any()) 
                    || !string.IsNullOrWhiteSpace(Seo)
                    || !string.IsNullOrEmpty(Canonical); 
            }
        }
    }
}
