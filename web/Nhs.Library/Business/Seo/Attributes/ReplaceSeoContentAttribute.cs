﻿using System;

namespace Nhs.Library.Business.Seo.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class ReplaceSeoContentAttribute : Attribute { }
}
