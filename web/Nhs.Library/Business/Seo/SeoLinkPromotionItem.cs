﻿namespace Nhs.Library.Business.Seo
{
    public class SeoLinkPromotionItem
    {
        public string Url { get; set; }
        public string Name { get; set; }
        public int Priority { get; set; }
        public double Distance { get; set; }

        public int MarketId { get; set; }
        public string City { get; set; }
        public int CommunityId { get; set; }
        public int PlanId { get; set; }
        public int SpecId { get; set; }
    }
}