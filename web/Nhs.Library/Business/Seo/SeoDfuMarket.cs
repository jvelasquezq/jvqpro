using System;
using System.Xml.Serialization;

namespace Nhs.Library.Business.Seo
{
    [Serializable]
    public class SeoDfuMarket
    {
        [XmlAttribute("id")]
        public int MarketId { get; set; }
    }
}