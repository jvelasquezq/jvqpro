using System.Collections.Generic;
using System.Xml.Serialization;
using Nhs.Library.Helpers.Content;

namespace Nhs.Library.Business.Seo
{
    [XmlType(AnonymousType = true)]
    [XmlRoot("markets")]
    public class SeoDfuMarkets
    {
        public SeoDfuMarkets()
        {
            MarketList = new List<SeoDfuMarket>();
        }

        [XmlElement("market")]
        public List<SeoDfuMarket> MarketList { get; set; }
    }
}