﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Nhs.Library.Business.Seo
{

    [XmlType(AnonymousType = true)]
    [XmlRoot("tdvmetrics")]
    public class TdvMetrics
    {
        public TdvMetrics()
        {
            Metrics = new List<TdvMetric>();
        }

        [XmlElement("metric")]
        public List<TdvMetric> Metrics { get; set; }
    }

    [Serializable]
    public class TdvMetric
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("value")]
        public string Value { get; set; }

        [XmlAttribute("events")]
        public string Events { get; set; }

    }


    public class Metric
    {
        public string Event { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

    }
}
