﻿using System.Xml.Serialization;
using Nhs.Library.Business.Seo.Attributes;

namespace Nhs.Library.Business.Seo
{
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class SeoBaseContent
    {
        [XmlElement("title", IsNullable = true)]
        [ReplaceSeoContent]
        public string Title { get; set; }

        [XmlElement("meta-description", IsNullable = true)]
        [ReplaceSeoContent]
        public string MetaDescription { get; set; }

        [XmlElement("meta-keywords", IsNullable = true)]
        [ReplaceSeoContent]
        public string MetaKeywords { get; set; }

        [XmlElement("robots", IsNullable = true)]    
        public string MetaRobots { get; set; }

        [XmlElement("canonical", IsNullable = true)]
        public string Canonical { get; set; }

        [XmlElement("h1", IsNullable = true)]
        [ReplaceSeoContent]
        public string H1 { get; set; }
    }
}
