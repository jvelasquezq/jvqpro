﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Nhs.Library.Enums;
using Nhs.Utility.Common;

namespace Nhs.Library.Business.Seo
{

    [XmlType(AnonymousType = true)]
    [XmlRoot("synthetics")]
    public class SyntheticGeoNames
    {
        public SyntheticGeoNames()
        {
            Synthetics = new List<Synthetic>();
        }

        [XmlElement("synthetic")]
        public List<Synthetic> Synthetics { get; set; }
    }

    [Serializable]
    public class Synthetic
    {
        /// <summary>
        /// Synthetic name, it can have spaces, and should be unique
        /// </summary>
        [XmlAttribute("name")]
        public string Name { get; set; }

        /// <summary>
        /// Primary market name
        /// </summary>
        [XmlAttribute("market")]
        public int MarketId { get; set; }

        /// <summary>
        /// Locations ids or names separated by semicolon to avoid conflict with Name, State names (eg: 17;256;286;344 or cedar park, tx;round rock, tx; miami, fl)
        /// </summary>
        [XmlAttribute("locations")]
        public string Locations { get; set; }

        /// <summary>
        /// Locations type: could be: "market", "city", "county" or "zip"
        /// </summary>
        [XmlAttribute("locationtype")]
        public string LocationTypeName { get; set; }

        public LocationType LocationType
        {
            get
            {
                return (LocationType)Enum.Parse(typeof(LocationType), LocationTypeName, true);
            }
        }

        public IList<string> ToLocationsList()
        {

            if (!string.IsNullOrEmpty(Locations))
            {
                // if separator is ';' use it, else use ',' 
                var separator = Locations.Contains(";") ? ';' : ',';
                // case when it is a city, state and only one locations 
                if (Locations.Contains(',') && Locations.Split(',')[1].Trim().Length == 2 && (LocationType != LocationType.Zip && LocationType != LocationType.Market)) separator = ',';

                var list = Locations.ToStringList(separator);
            
                if(LocationType == LocationType.County)
                    list = list.Select(c => c.ToLower().Replace(" county", string.Empty).Trim()).ToList(); // remove the 'county' word when present, it is not part of the county name in the db
                
                return list;
            }

            return null;
        }

    }

}
