﻿using System.Collections.Generic;
using Nhs.Library.Business.Seo;

namespace Nhs.Library.Business.Config
{
    public class SeoLinkPromotionPage
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public int Count { get; set; }
        public string LinksText { get; set; }

        public IList<SeoLinkPromotionItem> Links { get; set; }

        public SeoLinkPromotionPage()
        {
            Links = new List<SeoLinkPromotionItem>();
        }
    }
}