﻿using System.Xml.Serialization;
using Nhs.Library.Business.Seo.Attributes;

namespace Nhs.Library.Business.Seo.SiteIndex
{
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class SiteIndexStateContent : SeoBaseContent
    {        
        [XmlElement("header", IsNullable = true)]
        [ReplaceSeoContent]
        public string Header { get; set; }

        [XmlElement("footer", IsNullable = true)]
        [ReplaceSeoContent]
        public string Footer { get; set; }

        [XmlElement("tab1", IsNullable = true)]
        [ReplaceSeoContent]
        public string Tab1 { get; set; }

        [XmlElement("tab2", IsNullable = true)]
        [ReplaceSeoContent]
        public string Tab2 { get; set; }

        [XmlElement("tab3", IsNullable = true)]
        [ReplaceSeoContent]
        public string Tab3 { get; set; }

        [XmlElement("article", IsNullable = true)]
        [ReplaceSeoContent]
        public string Article { get; set; }
    }
}
