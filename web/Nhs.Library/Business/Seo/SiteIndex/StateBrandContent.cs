﻿using System.Xml.Serialization;
using Nhs.Library.Business.Seo.Attributes;

namespace Nhs.Library.Business.Seo.SiteIndex
{
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class StateBrandContent : SeoBaseContent
    {
        [XmlElement("header", IsNullable = true)]
        [ReplaceSeoContent]
        public string Header { get; set; }

        [XmlElement("footer", IsNullable = true)]
        [ReplaceSeoContent]
        public string Footer { get; set; }  
    }
}
