﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Nhs.Library.Business.Seo.Attributes;

namespace Nhs.Library.Business.Seo.SiteIndex
{
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class SiteIndexContent : SeoBaseContent
    {
        [XmlElement("onpagetext", IsNullable = true)]
        [ReplaceSeoContent]
        public string OnPageText { get; set; }
    }
}
