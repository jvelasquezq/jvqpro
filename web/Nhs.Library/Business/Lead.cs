using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Xml;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Utility.Pdf;
using Nhs.Utility.Web;

//using NHS.Components.DataAccess;

namespace Nhs.Library.Business
{
    public class Lead
    {
        #region "Members Variables"
        private const string xpathLeadSource = "/Lead/@Source";
        private const string xpathLeadtype = "/Lead/@Type";
        private const string xpathUserInfo = "/Lead/UserInfo";
        private const string xpathRequests = "/Lead/Request";

        private const string xpathbuilderid = "@builderid";
        private const string xpathcommunityid = "@communityid";
        private const string xpathcommenttextparams = "@commenttextparams";
        private const string xpathplanid = "@planid";
        private const string xpathspecid = "@specificationid";

        private string _leadType;
        private string _version = "6.0";
        private Hashtable _leadAttribs = new Hashtable();
        private DataTable _requests;
        #endregion

        #region "Properties"
        public string PartnerName
        {
            get
            {
                try { return Convert.ToString(_leadAttribs["PartnerName"]); }
                catch (Exception) { return String.Empty; }
            }
        }

        public string Type
        {
            get { return _leadType; }
        }

        public string Version
        {
            get { return _version; }
        }

        public int ID
        {
            get
            {
                int requestid = 0;
                try { requestid = Convert.ToInt32(_leadAttribs["RequestID"]); }
                catch (Exception) { }
                return requestid;
            }
        }

        public int PartnerID
        {
            get
            {
                int partnerid = 0;
                try { partnerid = Convert.ToInt32(_leadAttribs["PartnerID"]); }
                catch (Exception) { }
                return partnerid;
            }
        }

        public string ClientsInfo { get; set; }
        public string UtmCookieParams { get; set; }

        #endregion

        #region "Constructor"
        public Lead(string ltype)
        {
            this.SetLeadType(ltype);
            _requests = GetUserRequestsTable();
        }

        public Lead(XmlDocument xdlead)
        {
            string ltype = this.GetNodeValue(xdlead.DocumentElement, xpathLeadtype);
            this.SetLeadType(ltype);
            _requests = GetUserRequestsTable();

            foreach (XmlAttribute attribute in xdlead.DocumentElement.SelectSingleNode(xpathUserInfo).Attributes)
            {
                _leadAttribs.Add(attribute.Name, attribute.Value);
            }

            foreach (XmlNode node in xdlead.DocumentElement.SelectNodes(xpathRequests))
            {
                switch (_leadType)
                {
                    case LeadType.Builder:
                        try
                        {
                            int bid = Convert.ToInt32(this.GetNodeValue(node, xpathbuilderid));
                            this.AddBuilder(bid);
                        }
                        catch (Exception e)
                        {
                            throw new Exception("Error reading builderid from xml document", e);
                        }
                        break;
                    case LeadType.Community:
                        try
                        {
                            var bid = Convert.ToInt32(this.GetNodeValue(node, xpathbuilderid));
                            var cid = Convert.ToInt32(this.GetNodeValue(node, xpathcommunityid));
                            var dis = this.GetNodeValue(node, xpathcommenttextparams);

                            this.AddBuilderCommunity(bid, cid, dis);
                        }
                        catch (Exception)
                        {
                            try
                            {
                                int cid = Convert.ToInt32(this.GetNodeValue(node, xpathcommunityid));
                                this.AddCommunity(cid);
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Error reading communityid from xml document", e);
                            }
                        }
                        break;
                    case LeadType.Home:
                        try
                        {
                            int sid = Convert.ToInt32(this.GetNodeValue(node, xpathspecid));
                            this.AddSpec(sid);
                        }
                        catch (Exception)
                        {
                            try
                            {
                                int pid = Convert.ToInt32(this.GetNodeValue(node, xpathplanid));
                                this.AddPlan(pid);
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Error reading planid/specid from xml document: " + e.Message, e);
                            }
                        }
                        break;
                    case LeadType.Market:
                        try
                        {
                            int bid = Convert.ToInt32(this.GetNodeValue(node, xpathbuilderid));
                            int cid = Convert.ToInt32(this.GetNodeValue(node, xpathcommunityid));
                            var dis = this.GetNodeValue(node, xpathcommenttextparams);
                            this.AddBuilderCommunity(bid, cid, dis);
                        }
                        catch (Exception e)
                        {
                            throw new Exception("Error reading builderid from xml document", e);
                        }
                        break;
                }//switch (Leadtype)
            }//foreach (XmlNode node in xdlead.DocumentElement.SelectNodes(xpathRequests))
        }//Lead (XmlDocument xdlead)
        #endregion

        #region "Public Methods"
        public string NeedToSendEmailToConsumer()
        {
            if (_leadAttribs["SendEmailToConsumer"] != null)
                return _leadAttribs["SendEmailToConsumer"].ToString();

            return null;
        }

        public string AttachPDF()
        {
            if (_leadAttribs["AttachPDF"] != null)
                return _leadAttribs["AttachPDF"].ToString();

            return null;
        }

        public void AddPlan(int planid)
        {
            if (_leadType != LeadType.Home)
                throw new Exception("This method is not allowed for " + _leadType + " leads!!");

            DataRow request = _requests.NewRow();
            request["planid"] = planid;
            _requests.Rows.Add(request);
        }

        public void AddPlans(string planids, char delimiter)
        {
            if (_leadType != LeadType.Home)
                throw new Exception("This method is not allowed for " + _leadType + " leads!!");

            string[] arpid = planids.Split(delimiter);
            for (int i = 0; i < arpid.Length; i++)
            {
                try
                {
                    int pid = Convert.ToInt32(arpid[i]);
                    this.AddPlan(pid);
                }
                catch (Exception) { }
            }
        }

        public void AddSpec(int specid)
        {
            if (_leadType != LeadType.Home)
                throw new Exception("This method is not allowed for " + _leadType + " leads!!");

            DataRow request = _requests.NewRow();
            request["specificationid"] = specid;
            _requests.Rows.Add(request);
        }

        public void AddSpecs(string specids, char delimiter)
        {
            if (_leadType != LeadType.Home)
                throw new Exception("This method is not allowed for " + _leadType + " leads!!");

            string[] arsid = specids.Split(delimiter);
            for (int i = 0; i < arsid.Length; i++)
            {
                try
                {
                    int sid = Convert.ToInt32(arsid[i]);
                    this.AddSpec(sid);
                }
                catch (Exception) { }
            }
        }

        public void AddCommunity(int communityid)
        {
            if (_leadType != LeadType.Community)
                throw new Exception("This method is not allowed for " + _leadType + " leads!!");

            DataRow request = _requests.NewRow();
            request["communityid"] = communityid;
            _requests.Rows.Add(request);
        }

        public void AddCommunities(string communityids, char delimiter)
        {
            switch (_leadType)
            {
                case LeadType.Community:
                case LeadType.Rcm:
                    string[] arcid = communityids.Split(delimiter);
                    foreach (string t in arcid)
                    {
                        try
                        {
                            int cid = Convert.ToInt32(t);
                            this.AddCommunity(cid);
                        }
                        catch (Exception) { }
                    }
                    break;
                default:
                    throw new Exception("This method is not allowed for " + _leadType + " leads!!");
            }
        }

        public void AddBuilderCommunities(string bcIds, char delimiter)
        {
            if (_leadType != LeadType.Market && _leadType != LeadType.Community)
                throw new Exception("This method is not allowed for " + _leadType + " leads!!");
            string[] arcid = bcIds.Split(delimiter);

            for (int i = 0; i < arcid.Length; i++)
            {
                try
                {
                    var bid = Convert.ToInt32(arcid[i].Split('|')[0]);
                    var cid = Convert.ToInt32(arcid[i].Split('|')[1]);
                    var dis = arcid[i].Split('|').Length > 2 ? arcid[i].Split('|')[2] : "";

                    this.AddBuilderCommunity(bid, cid, dis);
                }
                catch (Exception) { }

            }
        }

        public void AddBuilderCommunity(int builderid, int communityid, string commenttextparams = "")
        {
            if (_leadType != LeadType.Community && _leadType != LeadType.Market)
                throw new Exception("This method is not allowed for " + _leadType + " leads!!");

            DataRow request = _requests.NewRow();
            request["builderid"] = builderid;
            request["communityid"] = communityid;
            request["commenttextparams"] = commenttextparams;
            _requests.Rows.Add(request);
        }

        public void AddBuilder(int builderid)
        {
            DataRow request = _requests.NewRow();
            request["builderid"] = builderid;
            _requests.Rows.Add(request);
        }

        public void AddBuilders(string builderids, char delimiter)
        {
            if (_leadType != LeadType.Builder)
                throw new Exception("This method is not allowed for " + _leadType + " leads!!");

            string[] arbid = builderids.Split(delimiter);
            for (int i = 0; i < arbid.Length; i++)
            {
                try
                {
                    int bid = Convert.ToInt32(arbid[i]);
                    this.AddBuilder(bid);
                }
                catch (Exception) { }
            }
        }

        public void AddInfo(string attname, string attvalue)
        {
            _leadAttribs.Add(attname, attvalue);
        }

        public void AddUserInfo(LeadUserInfo leadUserInfo)
        {
            _leadAttribs.Add("profile_guid", leadUserInfo.Guid);
            _leadAttribs.Add("Email", leadUserInfo.Email.ToLowerCase());
            _leadAttribs.Add("FName", leadUserInfo.FirstName);
            _leadAttribs.Add("LName", leadUserInfo.LastName);
            _leadAttribs.Add("Address1", leadUserInfo.Address);
            _leadAttribs.Add("City", leadUserInfo.City);
            _leadAttribs.Add("State", leadUserInfo.State);
            _leadAttribs.Add("PostalCode", leadUserInfo.PostalCode);
            _leadAttribs.Add("PhoneDay", leadUserInfo.Phone);
            _leadAttribs.Add("PhoneDayExt", leadUserInfo.PhoneExt);
            _leadAttribs.Add("SessionID", leadUserInfo.SessionId);
            _leadAttribs.Add("UserCookie", leadUserInfo.UserCookie);
        }

        public void AddUserPrefs(string pricerangeid, int searchmarketid, string searchcity, string searchpref, string moveindate, string finpref, string reasonid, string comments)
        {
            _leadAttribs.Add("PriceRangeID", pricerangeid);
            _leadAttribs.Add("SearchMarketID", searchmarketid);
            _leadAttribs.Add("SearchCity", searchcity);
            _leadAttribs.Add("SearchPref", searchpref);
            _leadAttribs.Add("TargetMoveInDate", moveindate);
            _leadAttribs.Add("FinancePreference", finpref);
            _leadAttribs.Add("ReasonForMoving", reasonid);
            _leadAttribs.Add("Comments", comments);
        }

        public void AddUserPrefs(string pricerangeid, int searchmarketid, string searchcity, string searchpref, string moveindate, string finpref, string reasonid, string comments, string realtorInfo)
        {
            _leadAttribs.Add("PriceRangeID", pricerangeid);
            _leadAttribs.Add("SearchMarketID", searchmarketid);
            _leadAttribs.Add("SearchCity", searchcity);
            _leadAttribs.Add("SearchPref", searchpref);
            _leadAttribs.Add("TargetMoveInDate", moveindate);
            _leadAttribs.Add("FinancePreference", finpref);
            _leadAttribs.Add("ReasonForMoving", reasonid);
            _leadAttribs.Add("Comments", comments);
            _leadAttribs.Add("RealtorInfo", realtorInfo);
        }

        public void AddSiteInfo(string supportemail, int brandPartnerId, int partnerid, string partnerSiteUrl,
            string refer, string leadpage, string leadpageaction)
        {
            _leadAttribs.Add("SupportEmail", supportemail);
            _leadAttribs.Add("BrandPartnerID", brandPartnerId);
            _leadAttribs.Add("PartnerID", partnerid);
            _leadAttribs.Add("PartnerSiteUrl", partnerSiteUrl);
            _leadAttribs.Add("Refer", refer);
            _leadAttribs.Add("LeadPage", leadpage);

            //Case 78510 --> Mobile Lead Differentiation
            if (NhsRoute.ShowMobileSite)
                leadpageaction = "m - " + leadpageaction;

            _leadAttribs.Add("LeadPageAction", leadpageaction);
        }

        public string GetXml()
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                XmlTextWriter xwriter = new XmlTextWriter(ms, Encoding.UTF8);

                xwriter.WriteStartDocument(true);
                xwriter.WriteStartElement("Lead");

                xwriter.WriteAttributeString("Type", _leadType);
                xwriter.WriteAttributeString("Version", Version);

                xwriter.WriteStartElement("UserInfo");
                foreach (object key in _leadAttribs.Keys)
                {
                    if ((_leadAttribs[key] != null) && !_leadAttribs[key].ToString().Equals(""))
                    {
                        xwriter.WriteAttributeString(key.ToString(), _leadAttribs[key].ToString());
                    }
                }
                xwriter.WriteEndElement();

                if (!string.IsNullOrEmpty(ClientsInfo))
                    xwriter.WriteRaw(ClientsInfo);
                if (!string.IsNullOrEmpty(UtmCookieParams))
                    xwriter.WriteRaw(UtmCookieParams);

                foreach (DataRow request in _requests.Rows)
                {
                    xwriter.WriteStartElement("Request");
                    foreach (DataColumn param in _requests.Columns)
                    {
                        xwriter.WriteAttributeString(param.ColumnName, request[param.ColumnName].ToString());
                    }
                    xwriter.WriteEndElement();
                }

                xwriter.WriteEndElement();
                xwriter.WriteEndDocument();
                xwriter.Flush();

                ms.Position = 0;
                StreamReader sr = new StreamReader(ms);
                string result = sr.ReadToEnd();

                sr.Close();
                xwriter.Close();
                return result;
            }
            catch (Exception e)
            {
                string message = "Error Getting XML (getxml) from Lead object, LeadType:" + this._leadType;
                throw new Exception(message, e);
            }

        }

        public string SendtoQ(string qName)
        {
            if ((_leadType != LeadType.Market) && (_requests.Rows.Count == 0))

                throw new Exception(Type + " Lead should have at least one request");

            string myleadid = DateTime.Now.ToShortDateString().Replace("/", "_").Replace(" ", "") + "_" + DateTime.Now.ToShortTimeString().Replace(":", "_").Replace(" ", "") + "_" + Guid.NewGuid().ToString();
            this.SendtoQ(qName, myleadid);
            return myleadid;
        }

        public void SendtoQ(string qName, string id)
        {
            if ((_leadType != LeadType.Market) && (_requests.Rows.Count == 0))
                throw new Exception(Type + " Lead should have at least one request");

            MSMQ.PostMessage(qName, id, this.GetXml());
        }

        //public void SendCofirmationEmail(string bodyurl, string smtp1, string smtp2, string strBrochureDir)
        //{
        //    bool attachBrochure = false;

        //    if (_leadAttribs["Email"] == null || _leadAttribs["SupportEmail"] == null)
        //        throw new Exception("To Email (Email) or From Email (SupportEmail) is null");

        //    string url = bodyurl + "&rid=" + this.ID + "&pid=" + this.PartnerID;
        //    if (_leadAttribs["FName"] != null)
        //        url += "&fn=" + _leadAttribs["FName"];

        //    if (_leadAttribs["Refer"] != null)
        //        url += "&refer=" + _leadAttribs["Refer"];

        //    if (_leadAttribs["AttachPDF"] != null)
        //    {
        //        url += "&attpdf=" + _leadAttribs["AttachPDF"];
        //        attachBrochure = true;
        //    }

        //    string body = HTTP.HttpGet(url);
        //    string subject = GetSubject();
        //    MailMessage email = new MailMessage(_leadAttribs["SupportEmail"].ToString(), _leadAttribs["Email"].ToString())
        //        {
        //            Subject = subject,
        //            Body = body,
        //            IsBodyHtml = true
        //        };

        //    if (attachBrochure)
        //    {
        //        for (int i = 0; i < _allBrochures.Count; i++)
        //        {
        //            string strBrochurefullname = strBrochureDir + (_allBrochures[i]);
        //            Attachment objPdfMailAttachment = new Attachment(strBrochurefullname);
        //            email.Attachments.Add(objPdfMailAttachment);
        //        }
        //    }

        //    MailHelper.Send(email);
        //}

        public void SendToFriendEmail(string subject, string bodyurl, string smtp1, string smtp2)
        {
            if (_leadAttribs["FriendEmail"] == null || _leadAttribs["SupportEmail"] == null)
                throw new Exception("To Email (FriendEmail) or From Email (SupportEmail) is null");

            string url = bodyurl;

            url += "&cid=" + _requests.Rows[0]["communityid"];
            url += "&spid=" + _requests.Rows[0]["specificationid"];
            url += "&plnid=" + _requests.Rows[0]["planid"];
            if (_leadAttribs["FName"] != null)
                url += "&fn=" + _leadAttribs["FName"];

            if (_leadAttribs["FriendName"] != null)
                url += "&frn=" + _leadAttribs["FriendName"];

            if (_leadAttribs["Comments"] != null)
                url += "&note=" + _leadAttribs["Comments"];

            url += "&pid=" + this.PartnerID;

            if (_leadAttribs["Refer"] != null)
                url += "&refer=" + _leadAttribs["Refer"];


            string body = HTTP.HttpGet(url);

            MailMessage email = new MailMessage(_leadAttribs["SupportEmail"].ToString(), _leadAttribs["FriendEmail"].ToString())
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                };

            MailHelper.Send(email);
        }

        public void AccountNPassEmail(string subject, string bodyurl, string smtp1, string smtp2)
        {
            if (_leadAttribs["Email"] == null || _leadAttribs["SupportEmail"] == null || _leadAttribs["profile_guid"] == null)
                throw new Exception("To Email (Email) or From Email (SupportEmail) is null or profile_guid is null");

            string url = bodyurl;

            url += "&guid=" + _leadAttribs["profile_guid"];
            url += "&pid=" + this.PartnerID;

            if (_leadAttribs["Refer"] != null)
                url += "&refer=" + _leadAttribs["Refer"];


            string body = HTTP.HttpGet(url);

            MailMessage email = new MailMessage(_leadAttribs["SupportEmail"].ToString(), _leadAttribs["Email"].ToString())
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                };

            MailHelper.Send(email);
        }

        public void Save(string connstr)
        {
            int requestId = this.SaveRequest();
            _leadAttribs.Add("RequestID", requestId);
            //BLC
            //			if (Leadtype == LeadType.market)
            //			{
            //				int RequestItemID  =SaveRegRequestItem(Connstr,RequestID);
            //			}

            foreach (DataRow requestItem in _requests.Rows)
            {
                int requestItemId = SaveRequestItem(requestId, requestItem);
                requestItem["requestitemid"] = requestItemId;
            }
        }

        //public ArrayList GenerateBrochures(string bodyurl, string connstr)
        //{
        //    DataSet brochureItems = GetBrochuresList(this.ID);
        //    string querystring = "";
        //    _allBrochures = new ArrayList();

        //    foreach (DataRow brochureItem in brochureItems.Tables[0].Rows)
        //    {
        //        if (!brochureItem.IsNull("builder_id") && !brochureItem.IsNull("community_id"))
        //            //Generate bc brochure querystring
        //            querystring = "&pid=" + PartnerID + "&bid=" + brochureItem["builder_id"] + "&cid=" + brochureItem["community_id"] + "&fn=" + _leadAttribs["FName"];

        //        else if (!brochureItem.IsNull("specification_id"))
        //            //Generate spec brochure querystring
        //            querystring = "&pid=" + PartnerID + "&spid=" + brochureItem["specification_id"] + "&fn=" + _leadAttribs["FName"];

        //        else if (!brochureItem.IsNull("plan_id"))
        //            //Generate plan brochure querystring
        //            querystring = "&pid=" + PartnerID + "&plnid=" + brochureItem["plan_id"] + "&fn=" + _leadAttribs["FName"];

        //        if (_leadAttribs["AttachPDF"] != null)
        //        {
        //            querystring = querystring + "&attpdf=Y";
        //        }

        //        string brochureurl = bodyurl + querystring;
        //        //headerurl = hurl + querystring;
        //        //footerurl = furl + querystring;

        //        if (!brochureurl.Equals(""))
        //        {
        //            if (!HTTP.CheckURL(brochureurl))
        //                throw new Exception(" URL : " + brochureurl + " is throwing an error!!");

        //            string brochurename = brochureItem["request_item_delivery_id"].ToString();

        //            try
        //            {
        //                brochurename = PdfGenerator.GetPdf(brochureurl, "", brochurename);
        //                _allBrochures.Add(brochurename);
        //            }
        //            catch (Exception e)
        //            {
        //                throw new Exception("pdferror", e);
        //            }

        //            UpdateBrochureName(Convert.ToInt32(brochureItem["request_item_delivery_id"].ToString()), brochurename);
        //        }//if (!brochureurl.Equals(""))
        //    }//foreach
        //    return _allBrochures;
        //}

        public void CopyBrochureToServers(ArrayList brochurenames, string brochureDir, string webServerPrefix, int webServerNum)
        {
            string sourceServer = string.Empty;

            //check for multiple servers
            if (webServerNum != 0)
            {
                string serverName;
                for (int i = 1; i <= webServerNum; i++)
                {
                    serverName = webServerPrefix + (i.ToString());
                    //determine the server where brochure source directory resides
                    if (brochureDir.IndexOf(serverName) != -1) { sourceServer = serverName; }
                }

                //copy brochures to all other servers
                for (int i = 1; i <= webServerNum; i++)
                {
                    serverName = webServerPrefix + (i.ToString());

                    if (serverName != sourceServer)
                    {
                        foreach (string brochurename in brochurenames)
                        {
                            try
                            {
                                File.Copy(brochureDir + brochurename, brochureDir.Replace(sourceServer, serverName) + brochurename, true);
                            }
                            catch (Exception e)
                            {
                                throw new Exception("Error copying brochure to server " + serverName + " from server " + sourceServer, e);
                            }

                        }
                    }
                }
            }
        }
        #endregion

        #region "Private Methods"
        private void SetLeadType(string ltype)
        {
            if ((ltype != LeadType.Builder)
                && (ltype != LeadType.Community)
                && (ltype != LeadType.Home)
                && (ltype != LeadType.Market)
                && (ltype != LeadType.Test)
                && (ltype != LeadType.PasswordRecovery)
                && (ltype != LeadType.Rcm))
                throw new Exception("Not a valid lead type!!");
            _leadType = ltype;

        }

        private DataTable GetUserRequestsTable()
        {
            var userRequests = new DataTable("UserRequests");
            userRequests.Columns.Add("requestitemid", typeof(Int32));
            userRequests.Columns.Add("builderid", typeof(Int32));
            userRequests.Columns.Add("communityid", typeof(Int32));
            userRequests.Columns.Add("planid", typeof(Int32));
            userRequests.Columns.Add("specificationid", typeof(Int32));
            userRequests.Columns.Add("commenttextparams", typeof(String));
            return userRequests;
        }

        private string GetSubject()
        {
            string subject;

            switch (_leadType)
            {
                case LeadType.Market:
                    //subject = "New home matches";
                    subject = TranslateSubject("Your requested home matches from ") + PartnerName;
                    break;
                default:
                    subject = TranslateSubject("Your requested brochure from ") + PartnerName;
                    break;
            }
            return subject;
        }

        private string TranslateSubject(string subjectString)
        {
            string subject = subjectString;

            if (this.PartnerID > 10000)
            {
                switch (subjectString)
                {
                    case "Request confirmation":
                        subject = "Solicitar confirmación";
                        break;
                    case "Your requested home matches from ":
                        subject = "Las coincidencias de casas que ha solicitado de ";
                        break;
                    case "Your requested brochure from ":
                        subject = "El folleto que ha solicitado de ";
                        break;
                }
            }

            return subject;
        }

        private string GetNodeValue(XmlNode xmlnode, string xpath)
        {
            string nodeValue = "";
            try
            {
                nodeValue = xmlnode.SelectSingleNode(xpath).InnerText;
            }
            catch (Exception)
            { }

            return nodeValue;
        }

        private int SaveRequest()
        {
            try
            {
                return DataProvider.Current.InsertRequest(_leadAttribs, _leadType);
            }
            catch (Exception e)
            {
                string message = "Error Adding Lead to Request Table, LeadType:" + this._leadType;
                throw new Exception(message, e);
            }
        }

        private int SaveRequestItem(int requestid, DataRow requestItem)
        {
            try
            {
                return DataProvider.Current.InsertRequestItem(requestid, requestItem);
            }
            catch (Exception e)
            {
                string message = "Error Adding Lead to Request Item Table, LeadType:" + this._leadType;
                throw new Exception(message, e);
            }
        }

        //private int SaveRegRequestItem(string Connstr, int Requestid)
        //{
        //    try
        //    {
        //        return DataProvider.Current.InsertRequestItem(Requestid);
        //    }
        //    catch (Exception e)
        //    {
        //        string message = "Error Adding Lead to Request Item Table, LeadType:" + this._leadType;
        //        throw new Exception(message, e);
        //    }
        //}

        private DataSet GetBrochuresList(int requestid)
        {
            try
            {
                return DataProvider.Current.GetRequestItemDelivery(requestid);
            }
            catch (Exception e)
            {
                string message = "Error getting brochure list, stored proc, requestid" + requestid.ToString();
                throw new Exception(message, e);
            }

        }

        private void UpdateBrochureName(int rid, string bname)
        {
            try
            {
                DataProvider.Current.UpdateBrochureName(rid, bname);
            }
            catch (Exception e)
            {
                string message = "Error Updating brochure name, stored proc, requestid, brochurename:" + rid.ToString() + bname;
                throw new Exception(message, e);
            }

        }
        #endregion


    }
}
