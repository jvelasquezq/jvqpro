using System.Collections.Generic;
using Nhs.Library.Business;

namespace Nhs.Library.Search.Comparers
{
    public class CommunityResultSpecialSort: IComparer<CommunityResult>
    {
        public int Compare(CommunityResult x, CommunityResult y)
        {
            int comparison = x.CommunityName.CompareTo(y.CommunityName);
            if (comparison > 0 || comparison < 0)
                return comparison;
            else
            {
                int builderComparison = x.BrandName.CompareTo(y.BrandName);
                return builderComparison;
            }
        }
    }
}
