﻿using System.Collections.Generic;

namespace Nhs.Library.Business
{
    public class Menu
    {
        public string Name   { get; set; }
        public string Url { get; set; }
        public List<Menu> Items { get; set; }
    }
}