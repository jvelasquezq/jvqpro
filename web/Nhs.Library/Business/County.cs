namespace Nhs.Library.Business
{
    public class County
    {
        #region "Members"
        private string _name = string.Empty;
        private string _state = string.Empty;
        private string _country = string.Empty;

        #endregion

        #region "Properties"
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }

        public int MarketId { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public int Radius { get; set; }

        #endregion

        #region "Constructor"

        #endregion
    }
}
