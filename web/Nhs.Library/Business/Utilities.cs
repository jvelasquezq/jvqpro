namespace Nhs.Library.Business
{
   public class Utilities
   {
       private string _description=string.Empty;
       private string _phone=string.Empty;
       private string _code=string.Empty;
       private string _name=string.Empty;

       public string Description
       {
           get { return _description; }
           set { _description = value; }
       }
       

       public string Phone
       {
           get { return _phone; }
           set { _phone = value; }
       }
       

       public string Code
       {
           get { return _code; }
           set { _code = value; }
       }
       

       public string Name
       {
           get { return _name; }
           set { _name = value; }
       }
   }
}
