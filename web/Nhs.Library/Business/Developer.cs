﻿namespace Nhs.Library.Business
{
    public class Developer
    {
        public string Name { get; set; }
        public string City { get; set; }
        public string MarketName { get; set; }
        public string MarketId { get; set; }
        public string State { get; set; }
    }
}