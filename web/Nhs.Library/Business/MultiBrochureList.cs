﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Library.Business
{
    [Serializable]
    public class MultiBrochureList
    {
        public MultiBrochureList()
        {
            BrochureList = new List<MultiBrochureItem>();
        }

        public int Market { get; set; }
        public List<MultiBrochureItem> BrochureList { get; set; }

        public void Add(int communityId, int builderId, bool isBilled, bool isFeatured, int page, string position)
        {
            if (!Contains(communityId, builderId))
                BrochureList.Add(new MultiBrochureItem { CommunityId = communityId, BuilderId = builderId, IsBilled = isBilled, IsFeatured = isFeatured, Page = page, Position = position});
        }

        public void Remove(int communityId, int builderId)
        {
            var index = BrochureList.FindIndex(p => p.CommunityId == communityId && p.BuilderId == builderId);
            BrochureList.RemoveAt(index);
        }

        public bool Contains(int communityId, int builderId)
        {
            return BrochureList.Any(p => p.CommunityId == communityId && p.BuilderId == builderId);
        }

        public int FirstCommunityId
        {
            get
            {
                return BrochureList.Any() ? BrochureList.FirstOrDefault().CommunityId : 0;
            }
        }
    }

    [Serializable]
    public class MultiBrochureItem
    {
        public int CommunityId { get; set; }
        public int BuilderId { get; set; }
        public bool IsBilled { get; set; }
        public bool IsFeatured { get; set; }
        public int Page { get; set; }
        public string Position { get; set; }
    }
}
