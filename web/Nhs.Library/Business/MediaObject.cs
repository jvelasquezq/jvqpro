using System;

namespace Nhs.Library.Business
{
    [Serializable]
    public class MediaObject
    {
        private string _fullPath = string.Empty;

        public MediaObject()
        { }

        public MediaObject(string name, string path, string description)
        {

            Name = name;
            Path = path;
            Description = description;
        }


        public string Type { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public string OriginalPath { get; set; }
        public string ClickThruUrl { get; set; }
        public string FullPath
        {
            get
            {
                return string.IsNullOrEmpty(_fullPath) ? (_fullPath = this.Path + this.Name) : _fullPath;
            }

        }

        public string Path { get; set; }
        public string Name { get; set; }
        public string BrandName { get; set; }
    }
}
