using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using BHI.Configuration;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;

namespace Nhs.Library.Common
{
    public static class Configuration
    {
        private static readonly Dictionary<string, Language> _languages;
        static ReaderWriterLockSlim _getConfigValueLock = new ReaderWriterLockSlim();

        static Configuration()
        {
            _languages = new Dictionary<string, Language>();
            _languages.Add("en", Language.English);
            _languages.Add("sp", Language.Spanish);
            _languages.Add("english", Language.English);
            _languages.Add("spanish", Language.Spanish);
        }

        public static string FavIcon
        {
            get
            {
                return ResourceDomain + "/globalresourcesmvc/Pro/images/favicon.ico";
            }
        }

        public static int PartnerId
        {
            get
            {
                try
                {
                    // Convert to Integer
                    return NhsRoute.PartnerId;
                }
                catch
                {
                    // Default value
                    return 1;
                }
            }
        }
        public static string PartnerSiteUrl
        {
            get { return NhsRoute.PartnerSiteUrl; }
        }
        
        public static string GetConfigurationValue(int partnerId, string key)
        {
            var configGroup = GetConfigGroup();
            string partnerSpecificKey = string.Format("{0}_{1}", key, partnerId.ToString().Trim());
            string brandPartnerSpecificKey = string.Format("{0}_${1}", key, NhsRoute.BrandPartnerId);
            string configValue = GetConfigurationValue(partnerSpecificKey, configGroup);

            if (string.IsNullOrEmpty(configValue))
            {
                configValue = GetConfigurationValue(brandPartnerSpecificKey, configGroup);

                if (string.IsNullOrEmpty(configValue))
                {
                    configValue = GetConfigurationValue(key, configGroup);
                }
            }
            return configValue;
        }

        private static string GetConfigGroup()
        {
            return ConfigurationManager.AppSettings["NHSConfigGroup"];
        }

        private static string GetConfigurationValue(string key, string configGroup)
        {

            string cacheKey = string.Format("ConfigSetting_{0}", key);
            string value = null;
            if (_getConfigValueLock.TryEnterReadLock(new TimeSpan(0, 0, 10)))
            {
                try
                {
                    value = ConfigurationManager.AppSettings[key];
                    if (string.IsNullOrEmpty(value))
                        value = WebCacheHelper.GetObjectFromCache(cacheKey, false) as string;

                    if (value == null)
                    {
                        if (string.IsNullOrEmpty(configGroup))
                        {
                            throw new ArgumentNullException(configGroup);
                        }
                        else
                        {
                            value = new Config().GetValue(configGroup, key) as string;
                        }
                    }
                }
                finally
                {
                    _getConfigValueLock.ExitReadLock();
                }

            }


            if (_getConfigValueLock.TryEnterWriteLock(new TimeSpan(0, 0, 10)))
            {
                try
                {
                    if (value != null)
                        WebCacheHelper.AddObjectToCache(value, string.Format("ConfigSetting_{0}", key),
                                                    new TimeSpan(WebCacheConst.DefaultHours, 0, 0), false);
                }
                finally
                {

                    _getConfigValueLock.ExitWriteLock();
                }
            }



            return value;
        }

        #region Configuration Keys


        public static string WurflPath { get { return GetConfigurationValue(PartnerId, "WurflPath"); } }
        public static string SiteMapFolder { get { return GetConfigurationValue(PartnerId, "SiteMapFolder"); } }
        public static string GoogleMapsApiCryptoKey { get { return GetConfigurationValue(PartnerId, "GoogleMapsApiCryptoKey"); } }
        public static string GoogleMapsApiClientId { get { return GetConfigurationValue(PartnerId, "GoogleMapsApiClientId"); } }
        public static string MrisRestEndpoint { get { return GetConfigurationValue(PartnerId, "MrisRestEndpoint"); } }
        public static string MrisRestPassword { get { return GetConfigurationValue(PartnerId, "MrisRestPassword"); } }
        public static string MrisRestUserAgent { get { return GetConfigurationValue(PartnerId, "MrisRestUserAgent"); } }
        public static string MrisRestUserId { get { return GetConfigurationValue(PartnerId, "MrisRestUserId"); } }
        public static bool OnDemandBrochureGeneration { get { return bool.Parse(GetConfigurationValue(PartnerId, "On_Demand_Brochure_Generation")); } }
        public static string PDFDirectory { get { return GetConfigurationValue(PartnerId, "PDFDirectory"); } }
        public static string CNHResourceDomain { get { return GetConfigurationValue(PartnerId, "CNHResourceDomain"); } }
        public static string CNHDomain { get { return GetConfigurationValue(PartnerId, "CNHDomain"); } }
        public static string ImageSharePath { get { return GetConfigurationValue(PartnerId, "ImageSharePath"); } }
        public static bool ShowEBook { get { return bool.Parse(GetConfigurationValue(PartnerId, "ShowEBook")); } }
        public static bool UseBrochureVer6 { get { return bool.Parse(GetConfigurationValue(PartnerId, "UseBrochureVer6")); } }
        public static string ImageProcessorFTPPath { get { return GetConfigurationValue(PartnerId, "ImageProcessorFTPPath"); } }
        public static int EktronLanguageId { get { return int.Parse(GetConfigurationValue(PartnerId, "EktronLanguageId")); } }
        public static string EktronDomain { get { return GetConfigurationValue(PartnerId, "EktronDomain"); } }
        public static string EktronServiceDomain { get { return GetConfigurationValue(PartnerId, "EktronServiceDomain"); } }
        public static long EktronResourceCenterTaxonomyId { get { return long.Parse(GetConfigurationValue(PartnerId, "EktronResourceCenterTaxonomyId")); } }
        
        public static string WebServerPrefix { get { return GetConfigurationValue(PartnerId, "WebServerPrefix"); } }
        public static int WebServerNum { get { return int.Parse(GetConfigurationValue(PartnerId, "WebServerNum")); } }
        public static string WebOpsEmail { get { return GetConfigurationValue(PartnerId, "WebOpsEmail"); } }
        public static string WebOpsPagerEmail { get { return GetConfigurationValue(PartnerId, "WebOpsPagerEmail"); } }
        public static string CasasNuevasAquiDomain { get { return GetConfigurationValue(PartnerId, "CasasNuevasAquiDomain"); } }
        public static string NewHomeSourceDomain { get { return GetConfigurationValue(PartnerId, "NewHomeSourceDomain"); } }
        public static string NewHomeSourceProfessionalSSODomain { get { return GetConfigurationValue(PartnerId, "NewHomeSourceProfessionalSSODomain"); } }
        public static string SloUrlParameters { get { return GetConfigurationValue(PartnerId, "SloUrlParameters"); } }
        public static string NewRetirementCommunitiesDomain { get { return GetConfigurationValue(PartnerId, "NewRetirementCommunitiesDomain"); } }
        public static string PrimarySMTPServer { get { return GetConfigurationValue(PartnerId, "PrimarySMTPServer"); } }
        public static string SecondarySMTPServer { get { return GetConfigurationValue(PartnerId, "SecondarySMTPServer"); } }
        public static string NHSLocalVideoServerPrefix { get { return GetConfigurationValue(PartnerId, "NHSLocalVideoServerPrefix"); } }
        public static string ExtranetURL { get { return GetConfigurationValue(PartnerId, "ExtranetURL"); } }
        public static string MSMQSendToFriend { get { return GetConfigurationValue(PartnerId, "MSMQSendToFriend"); } }
        public static string MSMQRecoverPassword { get { return GetConfigurationValue(PartnerId, "MSMQRecoverPassword"); } }
        public static string MSMQLeads { get { return GetConfigurationValue(PartnerId, "MSMQLeads"); } }
        public static string MSMQLeadsAck { get { return GetConfigurationValue(PartnerId, "MSMQLeadsAck"); } }
        public static string MSMQAccountCreation { get { return GetConfigurationValue(PartnerId, "MSMQAccountCreation"); } }
        public static string MSMQeBook { get { return GetConfigurationValue(PartnerId, "MSMQeBook"); } }
        public static string BdxContactFormEmail { get { return GetConfigurationValue(PartnerId, "BdxContactFormEmail"); } }

        public static string ListHubTestLogging { get { return GetConfigurationValue(PartnerId, "ListHubTestLogging"); } }
        public static string StaticContentFolder { get { return GetConfigurationValue(PartnerId, "StaticContentFolder"); } }
        public static string NhsContentArea { get { return GetConfigurationValue(PartnerId, "NhsContentArea"); } }
        public static string ShellFilePath { get { return GetConfigurationValue(PartnerId, "ShellFilePath"); } }
        public static string FriendlyURLDelimiter { get { return GetConfigurationValue(PartnerId, "FriendlyURLDelimiter"); } }
        public static Language Language { get { return ParseLanguage(GetConfigurationValue(PartnerId, "Language")); } }
        public static int ActionsPerAdRefresh { get { return int.Parse(GetConfigurationValue(PartnerId, "ActionsPerAdRefresh")); } }
        public static int CommunityResultsPageSize { get { return int.Parse(GetConfigurationValue(PartnerId, "CommunityResultsPageSize")); } }
        public static int CmsNhsHomePageCatId { get { return int.Parse(GetConfigurationValue(PartnerId, "CmsNhsHomePageCatId")); } }
        public static int CmsIndexCatId { get { return int.Parse(GetConfigurationValue(PartnerId, "CmsIndexCatId")); } }
        public static bool EnableCache { get { return bool.Parse(GetConfigurationValue(PartnerId, "EnableCache")); } }
        public static int SiteId { get { return int.Parse(GetConfigurationValue(PartnerId, "SiteId")); } }
        public static string AdControlEnabled { get { return GetConfigurationValue(PartnerId, "AdControlEnabled"); } }
        public static int MaxMapPoints { get { return int.Parse(GetConfigurationValue(PartnerId, "MaxMapPoints")); } }
        public static string DefaultMasterPage { get { return GetConfigurationValue(PartnerId, "DefaultMasterPage"); } }
        
        public static string ListHubProviderId { get { return GetConfigurationValue(PartnerId, "ListHubProviderId"); } }
        public static string FromEmail { get { return GetConfigurationValue(PartnerId, "FromEmail"); } }
        public static string BitlyUser { get { return GetConfigurationValue(PartnerId, "BitlyUser"); } }
        public static string BitlyKey { get { return GetConfigurationValue(PartnerId, "BitlyKey"); } }
        public static string ZipCodesResultsByMarket { get { return GetConfigurationValue(PartnerId, "ZipCodesResultsByMarket"); } }
        public static int DuplicatePreventionFilterDays { get { return int.Parse(GetConfigurationValue(PartnerId, "DuplicatePreventionFilterDays")); } }

        public static string WebServicesApiHashingAlgorithm { get { return GetConfigurationValue(PartnerId, "WebServicesApiHashingAlgorithm"); } }
        public static int RecoComNumberResult { get { return int.Parse(GetConfigurationValue(PartnerId, "RecoComNumberResult")); } }
        public static int RecoComPricePadding { get { return int.Parse(GetConfigurationValue(PartnerId, "RecoComPricePadding")); } }
        public static bool RecoComExcludeComBuilder { get { return bool.Parse(GetConfigurationValue(PartnerId, "RecoComExcludeComBuilder")); } }
        public static bool RecoComUsedMID { get { return bool.Parse(GetConfigurationValue(PartnerId, "RecoComUsedMID")); } }
        public static string WebServicesApiUrl { get { return GetConfigurationValue(PartnerId, "WebServicesApiUrl"); } }
        public static int EmailTemplateCommunitiesCount { get { return int.Parse(GetConfigurationValue(PartnerId, "EmailTemplateCommunitiesCount")); } }
        public static int EmailTemplateQuickMoveInCount { get { return int.Parse(GetConfigurationValue(PartnerId, "EmailTemplateQuickMoveInCount")); } }
        public static int EmailTemplateFeaturedCommunitiesCount { get { return int.Parse(GetConfigurationValue(PartnerId, "EmailTemplateFeaturedCommunitiesCount")); } }
        public static int EmailTemplateNewsCount { get { return int.Parse(GetConfigurationValue(PartnerId, "EmailTemplateNewsCount")); } }
        public static int EmailTemplatePromotionsCount { get { return int.Parse(GetConfigurationValue(PartnerId, "EmailTemplatePromotionsCount")); } }
        public static string TwitterAccountInformation { get { return GetConfigurationValue(PartnerId, "TwitterAccountInformation"); } }
        public static string FacebookAccountUrl { get { return GetConfigurationValue(PartnerId, "FacebookAccountUrl"); } }
        public static string NewsletterVirtualDirectory { get { return GetConfigurationValue(PartnerId, "NewsletterVD"); } }
        public static string MarketMapsVirtualDirectory { get { return GetConfigurationValue(PartnerId, "MarketMapsVD"); } }
        public static string NotOptInProPartners { get { return GetConfigurationValue(PartnerId, "NotOptInProPartners"); } }

        public static string FreeCreditScoreLinks { get { return GetConfigurationValue(PartnerId, "FreeCreditScoreLinks"); } }

        public static string DefaultStyleSheetMvc
        {
            get
            {
                var configGroup = GetConfigGroup();

                string value = GetConfigurationValue("DefaultStyleSheet_" + NhsRoute.PartnerId, configGroup);

                if (string.IsNullOrEmpty(value))
                    value = GetConfigurationValue("DefaultStyleSheet_" + NhsRoute.PartnerType + "_$" + NhsRoute.BrandPartnerId, configGroup);

                if (string.IsNullOrEmpty(value))
                    value = GetConfigurationValue("DefaultStyleSheet_$" + NhsRoute.BrandPartnerId, configGroup);

                return ResourceDomain + value + "?" + ResourceVersionNumber;
            }
        }

        public static string PartnerShellType { get { return GetConfigurationValue(PartnerId, "PartnerShellType"); } }
        public static string BrochureFolder { get { return GetConfigurationValue(PartnerId, "BrochureFolder"); } }
        //do not change serviceurl methods, using partnerid creates errors.
        public static string SearchServiceUrl { get { return GetConfigurationValue("SearchServiceUrl", ConfigurationManager.AppSettings["Nhs.WebService.ConfigGroup"]); } }
        public static string MapServiceUrl { get { return GetConfigurationValue("MapServiceUrl", ConfigurationManager.AppSettings["NHSConfigGroup"]); } }
        public static string CacheRefreshPath { get { return GetConfigurationValue(PartnerId, "CacheRefreshPath"); } }
        public static string MetaTagXmlFilePath { get { return GetConfigurationValue(PartnerId, "MetaTagXmlFilePath"); } }
        public static string ResourceDomain { get { return GetConfigurationValue(PartnerId, "ResourceDomain"); } }
        public static string ResourceDomainPublic { get { return GetConfigurationValue(PartnerId, "ResourceDomainPublic"); } }
        public static string TdvGoogleEventsXmlFilePath { get { return GetConfigurationValue(PartnerId, "TdvGoogleEventsXmlFilePath"); } }

        public static string IRSDomain { get { return GetConfigurationValue(PartnerId, "IRSDomain"); } }
        public static string CustomLogFolder { get { return GetConfigurationValue(PartnerId, "CustomLogFolder"); } }
        public static string MarketOptInFile { get { return GetConfigurationValue(PartnerId, "MarketOptInFile"); } }
        public static string PromoFlyerUrl { get { return GetConfigurationValue(PartnerId, "PromoFlyerUrl"); } }
        public static string EventFlyerUrl { get { return GetConfigurationValue(PartnerId, "EventFlyerUrl"); } }
        public static string ProgramFlyerUrl { get { return GetConfigurationValue(PartnerId, "ProgramFlyerUrl"); } }
        public static string AgentPolicyUrl { get { return GetConfigurationValue(PartnerId, "AgentPolicyUrl"); } }
        public static string SurveyFunctionality { get { return GetConfigurationValue(PartnerId, "SurveyFunctionality"); } }
        public static int SurveyNumberVisitsToTrigger { get { return int.Parse(GetConfigurationValue(PartnerId, "SurveyNumberVisitsToTrigger")); } }
        public static string MoveApiUrl { get { return GetConfigurationValue(PartnerId, "MoveApiUrl"); } }
        public static string ICanBuyApiUrl { get { return GetConfigurationValue(PartnerId, "ICanBuyApiUrl"); } }
        public static string ICanBuyApiToken { get { return GetConfigurationValue(PartnerId, "ICanBuyApiToken"); } }
        public static string BankrateApiUrl { get { return GetConfigurationValue(PartnerId, "BankrateApiUrl"); } }
        public static string BankrateApiWebCode { get { return GetConfigurationValue(PartnerId, "BankrateApiWebCode"); } }
        public static string BankrateApiPartnerId { get { return GetConfigurationValue(PartnerId, "BankrateApiPartnerId"); } }
        public static string ResourceVersionNumber { get { return GetConfigurationValue(PartnerId, "ResourceVersionNumber"); } }
        public static int PropertyDetailDescriptionLength { get { return int.Parse(GetConfigurationValue(PartnerId, "PropertyDetailDescriptionLength")); } }
        public static int PromoDescriptionLength { get { return int.Parse(GetConfigurationValue(PartnerId, "PromoDescriptionLength")); } }
        public static string AppIdFacebook { get { return GetConfigurationValue(PartnerId, "FacebookAppId_$" + NhsRoute.BrandPartnerId); } }
        public static string ResponsysFormsBaseUrl { get { return GetConfigurationValue(PartnerId, "ResponsysFormsBaseUrl"); } }
        public static string ResponsysConsumerNewsletterId { get { return GetConfigurationValue(PartnerId, "ResponsysConsumerNewsletterId"); } }
        public static string ResponsysTvContestId { get { return GetConfigurationValue(PartnerId, "ResponsysTvContestId"); } }


        public static string OwnerStoriesImageSharePath
        {
            get
            {
                return GetConfigurationValue(PartnerId, "ImageSharePath") +
                       GetConfigurationValue(PartnerId, "OwnerStoriesImagesFolder");
            }
        }
        /// <summary>
        /// The key below is used to retrieve only the IIS folder to show the images/videos for owner stories
        /// </summary>
        public static string OwnerStoriesFolder { get { return GetConfigurationValue(PartnerId, "OwnerStoriesImagesFolder"); } }

        public static string BrightcoveEnvSuffix
        {
            get { return GetConfigurationValue(PartnerId, "BrightcoveEnvSuffix"); }
        }

        public static string BrightcoveReadToken
        {
            get { return GetConfigurationValue(PartnerId, "BrightcoveReadToken"); }
        }

        #region CHAT Placement values

        public static int ChatPlacementNhsChatLink
        {
            get { return GetConfigurationValue(PartnerId, "ChatPlacementNhsChatLinkId").ToType<int>(); }
        }
        
        public static int ChatPlacementNhsChatTab
        {
            get { return GetConfigurationValue(PartnerId, "ChatPlacementNhsChatTabId").ToType<int>(); }
        }
        public static int ChatPlacementMoveButton
        {
            get { return GetConfigurationValue(PartnerId, "ChatPlacementMoveButtonId").ToType<int>(); }
        }
        public static int ChatPlacementMoveTab
        {
            get { return GetConfigurationValue(PartnerId, "ChatPlacementMoveTabId").ToType<int>(); }
        }

        public static int ChatPlacementMoveNhsDropInWindowId
        {
            get { return GetConfigurationValue(PartnerId, "ChatPlacementMoveNhsDropInWindowId").ToType<int>(); }
        }


        #endregion

        #endregion


        private static Language ParseLanguage(string language)
        {
            Language result = Language.English;

            if (_languages.ContainsKey(language.ToLower()))
                result = _languages[language.ToLower()];

            return result;
        }

        public static string RefreshCachePassword { get { return GetConfigurationValue(PartnerId, "RefreshCachePassword "); } }

        public static string SyntheticGeographiesReportUser { get { return GetConfigurationValue(PartnerId, "SyntheticGeographiesReportUser"); } }

        public static string SyntheticGeographiesReportPassword { get { return GetConfigurationValue(PartnerId, "SyntheticGeographiesReportPassword"); } }

    }
}
