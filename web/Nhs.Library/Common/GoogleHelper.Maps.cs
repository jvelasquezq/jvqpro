﻿using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using Nhs.Utility.Common;

namespace Nhs.Library.Common
{
    public class GoogleHelperMaps
    {
        public static string StaticMapUrl(string mapPoints, string size, bool usedZoom , int zoom , string icon)
        {
            return StaticMapUrl(mapPoints, size, usedZoom, zoom, icon, "");
        }

        public static string StaticMapUrl(string mapPoints, string size, bool usedZoom = false, int zoom = 13,
                                          string icon = "", string color = "")
        {
            var url = new StringBuilder();
            url.Append("http://maps.googleapis.com/maps/api/staticmap?sensor=false");
            url.AppendFormat("&client={0}", Configuration.GoogleMapsApiClientId);
            url.AppendFormat("&size={0}", size);
            url.Append("&visual_refresh=true");
            url.Append("&scale=1");
            url.Append("&format=png");

            if (usedZoom)
            {
                url.Append("&zoom=" + zoom);
            }

            url.Append("&markers=");
            if (!string.IsNullOrEmpty(icon))
                url.AppendFormat("icon:{0}", icon);
            if (!string.IsNullOrEmpty(color))
                url.AppendFormat("color:{0}", color);
            url.AppendFormat("|{0}", mapPoints);
            var urlSign = Sign(url.ToString(), Configuration.GoogleMapsApiCryptoKey);
            return urlSign;
        }

        private static string Sign(string url, string keyString)
        {
            var encoding = new ASCIIEncoding();

            // converting key to bytes will throw an exception, need to replace '-' and '_' characters first.
            var usablePrivateKey = keyString.Replace("-", "+").Replace("_", "/");
            var privateKeyBytes = Convert.FromBase64String(usablePrivateKey);

            var uri = new Uri(url);
            var encodedPathAndQueryBytes = encoding.GetBytes(uri.LocalPath + uri.Query);

            // compute the hash
            var algorithm = new HMACSHA1(privateKeyBytes);
            var hash = algorithm.ComputeHash(encodedPathAndQueryBytes);

            // convert the bytes to string and make url-safe by replacing '+' and '/' characters
            var signature = Convert.ToBase64String(hash).Replace("+", "-").Replace("/", "_");
            return  uri.Scheme + "://" + uri.Host + uri.LocalPath + uri.Query + "&signature=" +
                               signature;
        }

    }
}
