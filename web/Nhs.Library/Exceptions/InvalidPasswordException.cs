using System;

namespace Nhs.Library.Exceptions
{
    public class InvalidPasswordException : ApplicationException
    {
        public InvalidPasswordException()
        {
        }

        public InvalidPasswordException(string message) 
            : base(message)
        {

        }

        public InvalidPasswordException(string message, Exception innerException) 
            : base(message,innerException)
        {

        }

    }
}
