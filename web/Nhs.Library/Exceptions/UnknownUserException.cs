using System;

namespace Nhs.Library.Exceptions
{
    public class UnknownUserException : ApplicationException
    {
        public UnknownUserException()
        {
        }

        public UnknownUserException(string message) 
            : base(message)
        {

        }

        public UnknownUserException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }
}
