﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Mvc.Routing.Configuration;

namespace Nhs.Library.Extensions
{
    public static class RouteParamsExtensions
    {
        public static string Value(this RouteParams routeParam)
        {
            return NhsRoute.GetValue(routeParam);
        }

        public static T Value<T>(this RouteParams routeParam)
        {
            return NhsRoute.GetValue(routeParam).ToType<T>();
        }

        public static T Value<T>(this RouteParams param, T defaultValue)
        {
            var p = NhsRoute.GetValue(param);
            return string.IsNullOrEmpty(p) ? defaultValue : p.ToType<T>();
        }

        public static T Value<T>(this IList<RouteParam> routeParams, RouteParams paramName)
        {
            return NhsRoute.GetValue(routeParams, paramName).ToType<T>();
        }

        public static List<RouteParam> Clone(this IList<RouteParam> routeParams)
        {
            var routeParametersList = routeParams.Select(x=> x.Clone()).ToList();
            return routeParametersList;
        }


        public static RouteParam ToRouteParam(this RouteParams routeParams)
        {
            var routeParam = NhsRoute.GetRouteParam(routeParams).Clone();
            return routeParam;
        }

        public static RouteParam ToRouteParam(this RouteParams routeParams, RouteParamType paramType)
        {
            var routeParam = NhsRoute.GetRouteParam(routeParams).Clone();
            routeParam.ParamType = paramType;
            return routeParam;
        }

        public static RouteParam Clone(this RouteParam routeParams)
        {
            var routeParam = new RouteParam
                {
                    Name = routeParams.Name,
                    ParamType = routeParams.ParamType,
                    PlaceHolder = routeParams.PlaceHolder,
                    Value = routeParams.Value
                };
            return routeParam;
        }

        public static string Value(this IList<RouteParam> paramz, RouteParams paramName)
        {
            var param = paramz.FirstOrDefault(x => x.Name == paramName);
            if (param == null)
                return string.Empty;
            return string.IsNullOrEmpty(param.Value) ? string.Empty : HttpUtility.UrlDecode(param.Value);
        }

    }
}