﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Utility.Common;

namespace Nhs.Library.Web.ImageResizer
{
    public static class ImageResizerUrlHelpers
    {
        // images path that handle the helper so urls are transformed to the new url format for resizing
        private static readonly List<string> IncludePaths = new List<string>
        {
            "images/homes", 
            "images/basiclisting", 
            "images/brandshowcase"
        };
        // images path that should exclude the helper to be re formated and shouldnt be resized
        private static readonly List<string> ExcludePaths = new List<string> { };

        public static string Image(this UrlHelper helper, string imageFileName)
        {
            return Image(helper, imageFileName, null);
        }

        public static string Image(this UrlHelper helper, string imageFileName, ResizeCommands resizeCommands)
        {
            if (string.IsNullOrWhiteSpace(imageFileName)) throw new ArgumentNullException("imageFileName");

            var parameters = (resizeCommands != null && resizeCommands.HasValues) ? "?" + resizeCommands : string.Empty;

            return imageFileName + parameters;
        }

        public static string FormatToIrsImageName(string thumbnail)
        {
            const string matchCodeTag = @"(.*?)_";

            var thumbnailList = thumbnail.Split(new[] { '/' });

            var fileName = thumbnailList.Last();

            thumbnailList[thumbnailList.Length - 1] = Regex.Replace(fileName, matchCodeTag, string.Empty);

            thumbnail = thumbnailList.Join("/");
            return thumbnail;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imageUrl"></param>
        /// <param name="imageSize">Use the values defined in ImageSizes class</param>
        /// <param name="changeFileExtencion"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static string BuildIrsImageUrl(string imageUrl, string imageSize, bool changeFileExtencion = false, ImageFormat format = ImageFormat.Jpg)
        {
            var url = Configuration.IRSDomain.EndsWith("/") ? imageUrl.TrimStart('/') : imageUrl;

            if (string.IsNullOrWhiteSpace(imageUrl))
            {
                return imageUrl;
            }

            if (url.Contains("http://") == false)
            {
                 url = string.Concat(Configuration.IRSDomain, imageUrl, "?" + ImageSizes.GetResizeCommands(imageSize,changeFileExtencion,format));    
            }
            else if(url.Any(a => a == '?') == false)
            {
                url += ("?" + ImageSizes.GetResizeCommands(imageSize,changeFileExtencion,format));
            }

            return url;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imageUrl"></param>
        /// <param name="resizeCommands">Use the values defined in ImageSizes class</param>
        /// <returns></returns>
        public static string BuildIrsImageUrl(string imageUrl, ResizeCommands resizeCommands)
        {
            var url = Configuration.IRSDomain.EndsWith("/") ? imageUrl.TrimStart('/') : imageUrl;

            if (string.IsNullOrWhiteSpace(imageUrl))
            {
                return imageUrl;
            }

            if (url.Contains("http://") == false)
            {
                url = string.Concat(Configuration.IRSDomain, imageUrl,"?" + resizeCommands);
            }
            else if (url.Any(a => a == '?') == false)
            {
                url += ("?" + resizeCommands);
            }

            return url;
        }

        public static string GetFormatImageUrlForIrs(string imageUrl, ResizeCommands resizeCommands)
        {
            var url = FormatImageUrlForIrs(imageUrl);
            var parameters = (resizeCommands != null && resizeCommands.HasValues) ? "?" + resizeCommands : string.Empty;
            return url + parameters;
        }

        public static string FormatImageUrlForIrs(string imageUrl, bool removeUnderscore = true)
        {
            // IRS Fallback, just set IRSDomain to null so the site doesnt use it
            if (String.IsNullOrEmpty(Configuration.IRSDomain))
            {
                return imageUrl;
            }

            if (string.IsNullOrWhiteSpace(imageUrl))
                return string.Empty;

            if (imageUrl.ToLower().EndsWith("n.jpg"))
            {
                return string.Empty;
            }

            if (IndexOf(imageUrl, ExcludePaths))
            {
                return imageUrl;
            }

            if (imageUrl.IndexOf(Configuration.ResourceDomain, StringComparison.Ordinal) != -1 &&
                IndexOf(imageUrl, IncludePaths))
            {
                imageUrl = imageUrl.Replace(Configuration.ResourceDomain, Configuration.IRSDomain);
            }

            if (!imageUrl.StartsWith("http") && IndexOf(imageUrl, IncludePaths))
            {
                imageUrl = Configuration.IRSDomain + imageUrl;
            }


            // Remove Prefix for home Images 
            if (removeUnderscore && IndexOf(imageUrl.ToLower(), IncludePaths))
            {
                var url = new Uri(imageUrl);
                var prefixPosition = url.Segments[url.Segments.Length - 1].IndexOf("_", StringComparison.InvariantCulture);

                if (prefixPosition != -1)
                {
                    var prefix = url.Segments[url.Segments.Length - 1].Substring(0, prefixPosition + 1);
                    imageUrl = imageUrl.Replace(prefix, String.Empty);
                }
            }

            return imageUrl;
        }

        public static bool IndexOf(string text, IEnumerable<string> itemsToSearch)
        {
            return itemsToSearch.Any(str => text.ToLower().IndexOf(str, StringComparison.Ordinal) != -1);
        }
    }
}
