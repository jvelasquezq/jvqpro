﻿using System;
using System.Collections.Generic;
using System.Text;
using Nhs.Library.Business;

namespace Nhs.Library.Helpers.Utility
{
    public static class ListingUtility
    {
        private static readonly List<string> GrammarArray = new List<string> { "and" };

        /// <summary>
        /// Computes the bathrooms, based in the presentation rule
        /// </summary>
        /// <remarks>
        /// Presentation rule: If >=1 HalfBath exists, the "Baths" output should be appended with ".5"
        /// </remarks>
        /// <param name="fullBaths">The full baths.</param>
        /// <param name="halfBaths">The half baths.</param>
        /// <returns></returns>
        public static string ComputeBathrooms(int fullBaths, int halfBaths)
        {
            double baths = fullBaths + ((halfBaths > 0) ? 0.5 : 0.0);

            if (halfBaths < 1) // i know this is weird. but dont know if halfbaths can be more than 1
                return string.Format("{0:F0}", baths); //to do: check is halfbaths can be more than 1
            return string.Format("{0:F1}", baths); //show only 1 decimal
        }

        /// <summary>
        /// Computes the bathrooms for basic listings, based in the presentation rule
        /// </summary>        
        /// <param name="numBaths">The num of baths with or without decimals.</param>        
        /// <returns></returns>
        public static string ComputeBasicListingBathrooms(string numBaths)
        {
            string result = string.Empty;
            if (string.IsNullOrEmpty(numBaths) || numBaths.Equals("0") || numBaths.Equals("0.0"))
                return result;
            try
            {
                double baths = Convert.ToDouble(numBaths);
                double decimalPart = baths - (int)baths;

                if (decimalPart.Equals(0.0))
                    return string.Format("{0:F0}", baths); //use only int part

                return string.Format("{0:F1}", baths); //show only 1 decimal
            }
            catch
            {
                return result;
            }
        }

        public static string FormatBedrooms(int bedrooms, bool withAbbreviation, bool shortString = false, string studioWord = "Studio", string bedroomsAbbr = "br", string bedroomWord = "bedroom", string bedroomsWord = "bedrooms")
        {
            return FormatBedrooms(bedrooms, true, withAbbreviation, shortString, studioWord,bedroomsAbbr,bedroomWord,bedroomsWord);
        }

        public static string FormatBedrooms(int bedrooms, bool showBedroomsText, bool withAbbreviation, bool shortString, string studioWord, string bedroomsAbbr, string bedroomWord, string bedroomsWord)
        {
            if (bedrooms == 0)
                return studioWord;

            if (!showBedroomsText) return bedrooms.ToString();

            if (withAbbreviation) return string.Format("{0} <abbr title=\"bedrooms\">" + bedroomsAbbr + "</abbr>", bedrooms);

            var text = bedrooms > 1 ? string.Format("{0} " + bedroomsWord, bedrooms) : string.Format("{0} " + bedroomWord, bedrooms);

            return shortString ? text.Replace(bedroomWord, bedroomsAbbr).Replace(bedroomsWord, bedroomsAbbr + "s") : text;
        }

        public static string CapitalizeFirstLetter(string value, bool capitalizeStateCode)
        {
            if (!string.IsNullOrEmpty(value))
            {
                string appDomain = "";

                if (value.Contains("%20"))
                {
                    int spaceIndex = value.IndexOf("%20");
                    value = value.Replace("%20", "").Insert(spaceIndex, "-");
                }
                if (value.Equals("http:", StringComparison.CurrentCultureIgnoreCase))
                {
                    return value.ToLower();
                }
                if (value.Equals(appDomain, StringComparison.CurrentCultureIgnoreCase) ||
                    value.Equals(string.Format("www.{0}", appDomain), StringComparison.CurrentCultureIgnoreCase))
                {
                    return value.ToLower();
                }

                var sb = new StringBuilder(string.Empty);

                string previousSeparator = string.Empty;
                bool firstPart = true;
                foreach (string part in value.Split(" -'()".ToCharArray()))
                {
                    string separator;
                    if (sb.Length + part.Length < value.Length)
                        separator = value[sb.Length + part.Length].ToString();
                    else
                        separator = "";

                    if (part.Length > 0)
                    {
                        if (part.Length == 2 && !firstPart)
                        {
                            //State abbreviations upper cases
                            sb.AppendFormat("{0}{1}", part.ToUpper(), separator);
                        }
                        else if (IsGrammer(part))
                        {
                            sb.AppendFormat("{0}{1}", part.ToLower(), separator);
                        }
                        else if (!"IIIVIXI".Contains(part) && (part.ToUpper() != "LLC")) //roman numbers ?
                        {
                            //no, so capitalized first letter, unless it is one letter after quote or it is 2-letter word
                            if ((previousSeparator == "'") && (part.Length == 1))
                                sb.AppendFormat("{0}{1}", part.ToLower(), separator);
                            else if ((!string.IsNullOrEmpty(previousSeparator)) && (part.Length == 2)) // this happens when capitalizeStateCode = false
                                sb.AppendFormat("{0}{1}", part.ToLower(), separator);
                            else sb.AppendFormat("{0}{1}{2}", part.Substring(0, 1).ToUpper(), part.Substring(1).ToLower(), separator);
                        }
                        else
                        {
                            // upper case for roman numbers 
                            sb.AppendFormat("{0}{1}", part.ToUpper(), separator);
                        }
                    }
                    else
                    {
                        sb.AppendFormat(separator);
                    }
                    previousSeparator = separator;
                    firstPart = false;
                }
                return sb.ToString().TrimEnd();
            }
            return null;
        }

        private static bool IsGrammer(string val)
        {
            return GrammarArray.Contains(val.ToLower());
        }
    }
}
