using System.Net.Mail;
using System.Collections.Generic;

namespace Nhs.Library.Common
{
    public class MailHelper
    {

        public static void Send(MailMessage message)
        {
            SendMessage(message);
        }

        public static void Send(List<string> to, string from, string subject, string body, bool isBodyHtml = true)
        {
            MailMessage message = new MailMessage();
            foreach (var t in to)
                message.To.Add(t);
            message.From = new MailAddress(from);
            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = isBodyHtml;

            SendMessage(message);
        }

        public static void Send(string to, string from, string subject, string body, bool isBodyHtml = true)
        {
            MailMessage message = new MailMessage(from, to, subject, body);
            message.IsBodyHtml = isBodyHtml;

            SendMessage(message);
        }

        private static void SendMessage(MailMessage message)
        {
            SmtpClient stmpClient = new SmtpClient();
            try
            {
                stmpClient.Host = Configuration.PrimarySMTPServer;
                stmpClient.Send(message);
            }
            catch
            {
                stmpClient.Host = Configuration.SecondarySMTPServer;
                stmpClient.Send(message);
            }
        }


    }
}
