﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Library.Constants;

namespace Nhs.Library.Helpers.Utility
{
    internal class RewriterCookieManager
    {
        public static bool ShowEbookSection
        {
            get
            {
                var data = GetItem(CookieConst.ShowEbookSection);
                return string.IsNullOrEmpty(data) || data.ToType<bool>();
            }
            set { SetItem(CookieConst.ShowEbookSection, value); }
        }

        #region "Private Methods"
        /// <summary>
        /// Delete Cookie
        /// </summary>
        /// <param name="key">Use values in CookieConst</param>
        public static void DeleteCookie(string key)
        {
            RemoveItem(CookieKeyName(key));
        }


        /// <summary>
        /// Build the Cookie Name with the PartnerId as {key}_{partnerid}
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private static string CookieKeyName(string key)
        {
            return string.Format("{0}_{1}", key, Configuration.PartnerId);
        }

        /// <summary>
        /// Get Coockie Values
        /// </summary>
        /// <param name="key">Use values in CookieConst</param>
        /// <returns></returns>
        private static string GetItem(string key)
        {
            var cookie = HttpContext.Current.Request.Cookies[CookieKeyName(key)];
            var cookieValue = string.Empty;
            if (cookie != null)
            {
                cookieValue = cookie.Value;
            }
            return cookieValue;
        }

        /// <summary>
        /// Set Coockie Values
        /// </summary>
        /// <param name="key">Use values in CookieConst</param>
        /// <param name="value">Cookie value</param>
        /// <returns></returns>
        private static void SetItem(string key, object value)
        {
            key = CookieKeyName(key);
            RemoveItem(key);
            var cookie = new HttpCookie(key, value.ToString())
            {
                Expires = DateTime.Now.AddYears(100),
                Domain = HttpContext.Current.Request.Url.Host
            };
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        /// <summary>
        /// Set Coockie Values
        /// </summary>
        /// <param name="key">Use values in CookieConst</param>
        /// <param name="value">Cookie value</param>
        /// <param name="expires">Cookie expire date</param>
        /// <returns></returns>
        private static void SetItem(string key, object value, DateTime expires)
        {
            key = CookieKeyName(key);
            RemoveItem(key);
            var cookie = new HttpCookie(key, value.ToString()) { Expires = expires };
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        /// <summary>
        /// Remove Coockie 
        /// </s
        /// ummary>
        /// <param name="key">Use CookieKeyName(key);</param>
        private static void RemoveItem(string key)
        {
            var httpCookie = HttpContext.Current.Response.Cookies[key];

            if (httpCookie == null) return;

            httpCookie.Expires = DateTime.Now.AddDays(-1);
            HttpContext.Current.Response.Cookies.Add(httpCookie);
        }
        #endregion
    }
}
