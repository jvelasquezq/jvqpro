/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright (c) 2001-2006
 **by BuilderHomeSite Inc. ( http://www.BuilderHomeSite.com )
 * ========================================================
 * Author: ASolis
 * Date: 11/26/2010 19:25:11
 * Purpose:
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
using Newtonsoft.Json;

namespace Nhs.Library.Helpers.Utility
{
    /// <summary>
    /// Summary description for JSonHelper
    /// </summary>
    public static class JsonHelper
    {
        public static T ToFromJson<T>(this string jSonString) where T : new()
        {
            return FromJson<T>(jSonString);
        }

        public static T FromJson<T>(string jSonString)
        {            
            return  JsonConvert.DeserializeObject<T>(jSonString);
        }

        public static string ToJson(this object obj)
        {
            var json = JsonConvert.SerializeObject(obj);
            return json;
        }
    }
}
