/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright (c) 2001-2006
 **by BuilderHomeSite Inc. ( http://www.BuilderHomeSite.com )
 * ========================================================
 * Author: RMenon
 * Date: 02/22/2007 19:25:11
 * Purpose:
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
using System.Web;

namespace Nhs.Library.Common
{
    public class ControlUtil
    {
        public static string ResolveResourceUrl(string url)
        {

            if (!string.IsNullOrEmpty(url) && url.Contains("[resource:]"))
                return url.Replace("[resource:]", Configuration.ResourceDomain);

            if (!string.IsNullOrEmpty(url) && url.Contains("[irs:]"))
                return url.Replace("[irs:]", Configuration.IRSDomain);

            return url.StartsWith("~") ? VirtualPathUtility.ToAbsolute(url) : url;
        }

        public static string GetValidPrice(System.Decimal dPrice)
        {
            if (dPrice <= 0)
                return (string.Empty);
            else
                return "From " + (System.String)(System.Convert.ToString(System.Convert.ToDecimal(dPrice).ToString("C0")));
        }
    }
}
