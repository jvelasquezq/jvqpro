﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nhs.Library.Constants;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Utility.Constants;

namespace Nhs.Library.Helpers.Utility
{
    public static class NhsUrlHelper
    {
        public static string BuildResourceCenterUrl(string pageUrl)
        {
            var page = Pages.GetResourceCenterLink((PartnersConst)NhsRoute.BrandPartnerId);

            var url = string.Format("{0}{1}/{2}", NhsRoute.SiteRoot, page, pageUrl);
            return url.Replace("//", "/");
        }

        public static string ToUrl(this List<RouteParam> paramList, string function, bool lowerCaseUrl=false, bool completeUrl = false, string utmParameters = "")
        {
            return GenerateUrlFromParams(function, paramList, lowerCaseUrl, completeUrl, utmParameters);
        }

        public static string ToQueryString(this List<RouteParam> paramList, string function)
        {
            return GenerateUrlFromParams(function, paramList);
        }

        private static string GenerateUrlFromParams(string function, List<RouteParam> paramList)
        {
            var query = string.Empty;
            var url = string.Empty;

            if (!string.IsNullOrEmpty(NhsRoute.PartnerSiteUrl))
                url += "/" + NhsRoute.PartnerSiteUrl;

            url += "/" + function + "/";

            if (paramList != null)
                query = paramList.Aggregate(query, (current, param) => current + (param.Name + "=" + param.Value + "&"));

            if (query.Length > 0)
            {
                query = query.StripLeadingEndingChar('&');
                url += "?" + query;
            }
            return url.TrimEnd('/').ToLower();
        }

        private static string GenerateUrlFromParams(string function, List<RouteParam> paramList, bool lowerCaseUrl, bool completeUrl = false, string utmParameters = "")
        {
            string hash = string.Empty;
            string query = string.Empty;
            string url = string.Empty;

            // Dont process any external link 
            if (string.IsNullOrEmpty(function) || function.IndexOf("www", StringComparison.Ordinal) != -1 || function.IndexOf("http", StringComparison.Ordinal) != -1)
                return function;

            if (!string.IsNullOrEmpty(function))
            {
                if (!string.IsNullOrEmpty(NhsRoute.PartnerSiteUrl) &&
                    function.Contains(NhsRoute.PartnerSiteUrl) == false)
                {
                    url += "/" + NhsRoute.PartnerSiteUrl;
                }

                url += ("/" + function.Trim() + "/").Replace("//", "/");
            }

            if (paramList != null)
            {
                foreach (var param in paramList)
                {
                    switch (param.ParamType)
                    {
                        case RouteParamType.FriendlyNameOnly:
                        case RouteParamType.Friendly:
                            url += param + "/";
                            break;
                        case RouteParamType.QueryString:
                            query += param + "&";
                            break;
                        case RouteParamType.Hash:
                            hash += param + "&";
                            break;
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(query))
                query = utmParameters;
            else
            {
                if (utmParameters.StartsWith("?"))
                {
                    utmParameters = utmParameters.Replace("?", "&");
                }
                query += utmParameters;
            }

            query = query.Replace("&&", "&");
            hash = hash.Replace("&&", "&");

            url = lowerCaseUrl ? url.TrimEnd('/').ToLower() : url.TrimEnd('/');

            if (url.LastPart("/").Contains("."))
                url += "/";

            if (!string.IsNullOrEmpty(query) && query.Length > 0)
            {
                query = query.StripLeadingEndingChar('&');
                url += "?" + (lowerCaseUrl ? query.ToLower() : query);
            }

            if (!string.IsNullOrEmpty(hash) && hash.Length > 0)
            {
                hash = hash.StripLeadingEndingChar('&');
                url += "#" + (lowerCaseUrl ? hash.ToLower() : hash);
            }
            if (completeUrl)
            {
                //url = Configuration.NewHomeSourceDomain.TrimEnd('/') + url;
                url = string.Format("http://{0}{1}", HttpContext.Current.Request.Url.Host.TrimEnd('/'), url);
            }
            return url;
        }

        public static string GetUtmParameters(this List<RouteParam> parameters)
        {
            var testUtmParameters = parameters.Where(p => p.Name.ToString().Contains("utm"));
            var utmParameters = testUtmParameters.Aggregate("?", (current, param) => current + String.Format("{0}={1}&", param.Name, param.Value));
            utmParameters = utmParameters.Remove(utmParameters.Length - 1);
            return utmParameters;
        }
    }
}
