using Bdx.Web.Api.Objects;
using Nhs.Library.Enums;

namespace Nhs.Library.Helpers
{
    public static class EnumConversionUtil
    {
     public static ListingType StringToListingType(string value)
        {
            switch (value.ToUpper())
            {
                case "C": return ListingType.Community;
                case "P": return ListingType.Plan;
                case "S": return ListingType.Spec;
                case "B": return ListingType.BOYLCommunity;
                case "N": return ListingType.NonBOYLCommunity;
                default: return ListingType.Community;
            }
        }

        public static string ListingTypeToString(ListingType listingType)
        {
            switch (listingType)
            {
                case ListingType.Community: return "C";
                case ListingType.Plan: return "P";
                case ListingType.Spec: return "S";
                case ListingType.BOYLCommunity: return "B";
                case ListingType.NonBOYLCommunity: return "N";
                default: return "C";
            }
        }

        public static string HomeStatusTypeToString(object value, 
            string availableNowString = "Available Now",
            string modelHomeString = "Model Home",
            string readyToBuildString = "Ready To Build",
            string underConstructionString = "Under Construction",
            string quickMoveInString = "Quick Move-In")
        {
            if (value == null)
            {
                return null;
            }
            else
            {
                switch ((HomeStatusType)value)
                {
                    case HomeStatusType.AvailableNow:
                        return availableNowString;
                    case HomeStatusType.ModelHome:
                        return modelHomeString;
                    case HomeStatusType.ReadyToBuild:
                        return readyToBuildString;
                    case HomeStatusType.UnderConstruction:
                        return underConstructionString;
                    case HomeStatusType.QuickMoveIn:
                        return quickMoveInString;
                    default:
                        return null;
                }
            }
        }

        public static string HomeStatusTypeToHomeStatus(this int value)
        {
            switch ((HomeStatusType)value)
            {
                case HomeStatusType.AvailableNow:
                    return "A";
                case HomeStatusType.ModelHome:
                    return "M";
                case HomeStatusType.ReadyToBuild:
                    return "R";
                case HomeStatusType.UnderConstruction:
                    return "UC";
                //case HomeStatusType.QuickMoveIn:
                //    return "Q";
                default:
                    return "A";
            }
        }
    }
}
