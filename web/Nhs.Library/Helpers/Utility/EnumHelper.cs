﻿using System;
using System.Linq;
using System.Reflection;

namespace Nhs.Library.Business.Enums
{
    public static class EnumHelper
    {
        public static TExpected GetAttributeValue<T, TExpected>(this Enum enumeration, Func<T, TExpected> expression)
        where T : Attribute
        {
            var firstOrDefault = enumeration
                .GetType()
                .GetMember(enumeration.ToString()).FirstOrDefault(member => member.MemberType == MemberTypes.Field);

            if (firstOrDefault != null)
            {
                T attribute =
                    firstOrDefault
                        .GetCustomAttributes(typeof(T), false)
                        .Cast<T>()
                        .SingleOrDefault();

                if (attribute == null)
                    return default(TExpected);

                return expression(attribute);
            }

            return default(TExpected);
        }
    }
}
