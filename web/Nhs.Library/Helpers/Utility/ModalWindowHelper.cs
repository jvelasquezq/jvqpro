using System;
using System.Text;
using System.Web.UI;
using Nhs.Library.Constants;
using Nhs.Mvc.Routing.Interface;

namespace Nhs.Library.Common
{
    public static class ModalWindowHelper
    {
        public static string UpdateTargetId
        {
            get { return "TelerikWindow"; }
        }

        public static void InitActiveUserFlag(Page page)
        {
            page.ClientScript.RegisterHiddenField("fldChangedToActiveUser", false.ToString().ToLower());
        }

        #region Scripts calls
        
        public static void ShowSuccessMessage(string message, bool refreshParent, bool closeModal, Page page)
        {
            ShowSuccessMessage(message, refreshParent, closeModal, page, false);
        }
        public static void ShowSuccessMessage(string message, bool refreshParent, bool closeModal, Page page, bool inUpdatePanel)
        {
            CallScript(GetScriptShowSuccessMessage(message, refreshParent, closeModal), "showsuccessmessage", false, page, inUpdatePanel);
        }
        public static void CloseWindow(Page page)
        {
            CallScript(GetScriptCloseWindow(), "closewindow", false, page);
        }
        public static void CloseAndRedirectParent(string url, Page page)
        {
            CallScript(GetScriptCloseAndRedirectParent(url), "closeredirectparent", false, page);
        }
        public static void ResizeModalWindow(int width, int height, Page page)
        {
            CallScript(GetScriptResizeWindow(width, height), "resizewindow", false, page);
        }
        public static void ReloadParent(Page page)
        {
            CallScript(GetScriptReloadParent(), "reloadparent", false, page);
        }
        public static void ReloadParent(Page page, bool inUpdatePanel)
        {
            CallScript(GetScriptReloadParent(), "reloadparent", false, page, inUpdatePanel);
        }
        public static void CloseWindowAndReloadParent(Page page)
        {
            CallScript(GetScriptCloseAndReloadParent(), "closewindowandreloadparent", false, page);
        }
        public static void OpenModalWindow(string eventName, string url, int width, int height, bool callInParent, Page page)
        {
            CallScript(GetScriptOpenModalWindow(eventName, url, width, height, callInParent), "openmodalwindow", true, page);
        }
        public static void ChangeToActiveUser(bool value, Page page)
        {
            CallScript(GetScriptChangeToActiveUser(value), "changetoactiveuser", true, page);
        }
        public static void UpdateWindowHeight(Page page)
        {
            CallScript(GetScriptUpdateWindowHeight(), "updatewindowheight", true, page);
        }

        #endregion

        #region Scripts definitions

        public static string GetScriptCloseWindow()
        {
            return "parent.AjaxForm.CloseCurrentWindow();";
        }
        public static string GetScriptReloadParent()
        {
            return "parent.document.location.reload(true);";
        }
        public static string GetScriptCloseAndRedirectParent(string url)
        {
            return String.Format("parent.location='{0}'; {1}", url, GetScriptCloseWindow());
        }
        public static string GetScriptCloseAndReloadParent()
        {
            return String.Format("{0} {1}", GetScriptReloadParent(), GetScriptCloseWindow());
        }
        public static string GetScriptResizeWindow(int width, int height)
        {
            return String.Format("parent.AjaxForm.SetWindowSize({0},{1});", width, height);
        }
        public static string GetScriptShowSuccessMessage(string message)
        {
            StringBuilder strBuilder = new StringBuilder();
            strBuilder.Append(String.Format("document.getElementById('divSuccessMessage').innerHTML='{0}';", message));
            strBuilder.Append("document.getElementById('divSuccessMessage').style.display='inline';");
            return strBuilder.ToString();
        }
        public static string GetScriptShowSuccessMessage(string message, bool refreshParent, bool closeModal)
        {
            StringBuilder strBuilder = new StringBuilder();
            strBuilder.Append(String.Format("parent.document.getElementById('divSuccessMessage').innerHTML='{0}';", message));
            strBuilder.Append("parent.document.getElementById('divSuccessMessage').style.display='inline';");
            if (refreshParent)
                strBuilder.Append("parent.setTimeout('window.location.reload()', 3000);");
            if (closeModal)
                strBuilder.Append(GetScriptCloseWindow());
            return strBuilder.ToString();
        }
        public static string GetScriptOpenModalWindow(string eventName, string url, int width, int height, bool callInParent)
        {
            string script = (callInParent) ? "parent." : String.Empty;
            script += String.Format("AjaxForm.ShowWindow({0}, '{1}', {2}, {3}, '', true);", eventName, url, width, height);
            return script;
        }
        public static string GetScriptChangeToActiveUser(bool value)
        {
            return String.Format("parent.$p('fldChangedToActiveUser').value = {0};", value.ToString().ToLower());
        }
        public static string GetScriptUpdateWindowHeight()
        {
            return "parent.AjaxForm.UpdateHeight();";
        }

        #endregion

        public static void CallScript(string script, string key, bool onWindowOnload, Page page)
        {
            CallScript(script, key, onWindowOnload, page, false);           
        }
        public static void CallScript(string script, string key, bool onWindowOnload, Page page, bool inUpdatePanel)
        {
            StringBuilder strBuilder = new StringBuilder();

            // Checks if onload is required
            if (onWindowOnload)
                strBuilder.Append(String.Format("Event.observe(window, 'load', function(){{ {0} }}); ", script));
            else
                strBuilder.Append(script);

            // Registers script
            if (inUpdatePanel)
                ScriptManager.RegisterClientScriptBlock(page, typeof(Page), key, strBuilder.ToString(), true);
            else
                page.ClientScript.RegisterClientScriptBlock(typeof(Page), key, strBuilder.ToString(), true);
        }
    }
}
