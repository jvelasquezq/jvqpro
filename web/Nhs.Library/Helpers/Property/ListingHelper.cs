﻿using Nhs.Library.Business;
using Nhs.Library.Enums;
using Nhs.Mvc.Data.Repositories;

namespace Nhs.Library.Helpers
{
    public sealed class ListingHelper
    {

        public static void RemoveSavedListing(PlannerListing listing)
        {
            string listingType = EnumConversionUtil.ListingTypeToString(listing.ListingType);
            int builderId = 0;

            if (listing.ListingType == ListingType.Community)
                builderId = listing.BuilderId;

            DataProvider.Current.RemoveSavedListing(listing.UserId, listingType, listing.ListingId, builderId);
        }

        public static string ComputeBathrooms(int fullBaths, int halfBaths)
        {
            var baths = fullBaths + ((halfBaths > 0) ? 0.5 : 0.0);
            return string.Format(halfBaths < 1 ? "{0:F0}" : "{0:F1}", baths);
        }

        public static string GetSpecOrPlanByListingId(int listingId)
        {
            return DataProvider.Current.GetSpecOrPlanByListingId(listingId);
        }
    }
}
