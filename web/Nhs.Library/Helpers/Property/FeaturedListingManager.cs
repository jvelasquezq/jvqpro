﻿using System;
using System.Collections.Generic;
using System.Data;
using Nhs.Library.Business;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;

namespace Nhs.Library.Common
{
    public class FeaturedListingManager
    {
        /// <summary>
        /// This method gets Featured Listings for a Market 
        /// </summary>
        /// <param name="marketId"></param>
        /// <returns></returns>
        public static FeaturedListingMarket GetFeaturedListingMarketInfo(int marketId)
        {
            return FormatFlMarketData(marketId);
        }

        private static FeaturedListingMarket FormatFlMarketData(int marketId)
        {
            var randomizedList = GetFeaturedListingsForMarket(marketId);
            return new FeaturedListingMarket(marketId, randomizedList);
        }

        private static List<FeaturedListing> GetFeaturedListingsForMarket(int marketId)
        {
            // this is the initial list of featurelistingid/builder, can contain duplicates depending
            // on the spots that the builder bought
            int currentPartnerId= NhsRoute.PartnerId;

            List<FeaturedListing> expandedList = DataBindList(DataProvider.Current.GetFeaturedListings(marketId, currentPartnerId), currentPartnerId);
            List<FeaturedListing> randomizedList = new List<FeaturedListing>();

            // randomize list
            Random r = new Random();
            while (expandedList.Count > 0)
            {
                int randomIndex = r.Next(0, expandedList.Count);
                randomizedList.Add(expandedList[randomIndex]);
                expandedList.RemoveAt(randomIndex);
            }

            return randomizedList;
        }

        /// <summary>
        /// This method converts the datatable into a proper FeaturedListing collection
        /// </summary>
        /// <param name="data"></param>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        private static List<FeaturedListing> DataBindList(DataTable data, int partnerId)
        {
            List<FeaturedListing> list = new List<FeaturedListing>();

            foreach (DataRow row in data.Rows)
            {
                FeaturedListing fl = new FeaturedListing();
                fl.FeaturedListingId = DBValue.GetInt(row["featured_listing_id"]);
                fl.BuilderId = DBValue.GetInt(row["builder_id"]);
                fl.MarketId = DBValue.GetInt(row["market_id"]);

                DataTable communities = DataProvider.Current.GetFeaturedListingCommunities(fl.FeaturedListingId, partnerId);
                foreach (DataRow subrow in communities.Rows)
                    fl.CommunityIds.Add(DBValue.GetInt(subrow["community_id"]));

                if (fl.CommunityIds.Count > 0) // empty FLs?
                {
                    list.Add(fl);
                }
                else
                {
                    // try and query for full list of communities
                    DataTable allCommunities = DataProvider.Current.GetAllFeaturedListingCommunities(fl.BuilderId, fl.MarketId, partnerId);
                    foreach (DataRow subrow in allCommunities.Rows)
                        fl.CommunityIds.Add(DBValue.GetInt(subrow["community_id"]));

                    if (fl.CommunityIds.Count > 0)
                        list.Add(fl);
                }
            }

            return list;
        }
    }
}
