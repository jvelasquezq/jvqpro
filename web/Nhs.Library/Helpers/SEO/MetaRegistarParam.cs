﻿namespace Nhs.Library.Helpers.Seo
{
    public class MetaRegistarParam
    {
        public static string StateName = "StateName";
        public static string CommunityName = "CommunityName";
        public static string City = "City";
        public static string StateAbbreviation = "StateAbbreviation";
        public static string BuilderName = "BuilderName";
        public static string BrandName = "BrandName";
        public static string SchoolDistrict = "SchoolDistrict";
        public static string MarketName = "MarketName";
        public static string Bedroom = "BedRoom";
        public static string Bathroom = "BathRoom";
        public static string Cities = "Cities";
        public static string PlanName = "PlanName";
        public static string PartnerName = "PartnerName";
        public static string PostalCode = "PostalCode";
        public static string CommunitiesOnNhs = "CommunitiesOnNHS";
        public static string ParameterBuilderShowCaseState1 = "BuilderShowCaseState1";
        public static string ParameterBuilderShowCaseState2 = "BuilderShowCaseState2";
        public static string Domain = "Domain";
        public static string An = "An";
    }
}
