﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Utility.Common;


namespace Nhs.Library.Helpers.Seo
{
    public static class MarketBackgroundsConfigReader
    {
        public static List<MarketBackground> GetMarketBackgrounds(IPathMapper pathMapper)
        {
            const string cacheKey = "MarketBackgrounds";
            var marketBackgrounds =
                WebCacheHelper.GetObjectFromCache(cacheKey, false) as List<MarketBackground>;

            if (marketBackgrounds != null) return marketBackgrounds;
            
            var xmlFileName = pathMapper.MapPath(@"~/" + Configuration.StaticContentFolder + @"/SEOContent/NHS_Market_SRP_Background/MarketBackgrounds.xml");
            var imagePath = Configuration.StaticContentFolder + @"/SEOContent/NHS_Market_SRP_Background/images/";
            try
            {
                if (xmlFileName.Length <= 0 || !File.Exists(xmlFileName))
                    throw new Exception("Invalid XML file name or file does not exist");

                var documentRoot = XElement.Load(xmlFileName);

                marketBackgrounds =
                    (from markets in documentRoot.Descendants()
                     let id = markets.Attribute("id")
                     let fileName = markets.Attribute("filename")
                     where (!string.IsNullOrEmpty(fileName.Value) && !string.IsNullOrEmpty(id.Value))
                     select new MarketBackground
                     {
                         MarketId = id.Value,
                         ImageFileName = imagePath + fileName.Value
                     }).ToList();

                if (marketBackgrounds.Count > 0)
                    WebCacheHelper.AddObjectToCache(marketBackgrounds, cacheKey,
                        new TimeSpan(WebCacheConst.DefaultHours, 0, 0), false);
            }
            catch (Exception ex)
            {
                Trace.Write("Error in retrieving Market Maps" + ex.Message + Environment.NewLine);
                ErrorLogger.LogError(ex);
            }
            return marketBackgrounds;
        }
    }
}
