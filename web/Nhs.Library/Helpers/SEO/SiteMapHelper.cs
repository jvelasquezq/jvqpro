﻿//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.Linq;
//using Nhs.Library.Business.Sitemap;
//using Nhs.Mvc.Data.Repositories;

//namespace Nhs.Library.Helpers.Seo
//{
//    public sealed class SiteMapHelper
//    {
//        public static List<SiteMapCity> SitemapGetCities(int partnerId)
//        {
//            var lstCities = new List<SiteMapCity>();
//            var citiesDataTable = DataProvider.Current.SitemapGetCities(partnerId);
//            if (citiesDataTable == null) return lstCities;
//            lstCities.AddRange(from DataRow row in citiesDataTable.Rows
//                               select new SiteMapCity
//                                   {
//                                       CityName = Convert.ToString(row["city"]),
//                                       MarketId = Convert.ToInt32(row["market_id"])
//                                   });
//            return lstCities;
//        }

//        public static List<SiteMapCommunity> SitemapGetCommunities(int partnerId)
//        {
//            var lstCommunities = new List<SiteMapCommunity>();
//            var communitiesDataTable = DataProvider.Current.SitemapGetCommunities(partnerId);
//            if (communitiesDataTable == null) return lstCommunities;
//            lstCommunities.AddRange(from DataRow row in communitiesDataTable.Rows
//                                    select new SiteMapCommunity
//                                        {
//                                            CommunityId = Convert.ToInt32(row["community_id"]),
//                                            BuilderId = Convert.ToInt32(row["builder_id"]),
//                                            MarketId = Convert.ToInt32(row["market_id"])
//                                        });
//            return lstCommunities;
//        }

//        public static List<SiteMapSpec> SitemapGetSpecifications(int partnerId)
//        {
//            var lstSpecification = new List<SiteMapSpec>();
//            var specificationsDataTable = DataProvider.Current.SitemapGetSpecifications(partnerId);
//            if (specificationsDataTable != null)
//            {
//                lstSpecification.AddRange(from DataRow row in specificationsDataTable.Rows
//                                          select new SiteMapSpec
//                                              {
//                                                  SpecificationId = Convert.ToInt32(row["specification_id"]),
//                                                  MarketId = Convert.ToInt32(row["market_id"])
//                                              });
//            }
//            return lstSpecification;
//        }

//        public static List<SiteMapPlan> SitemapGetPlans(int partnerId)
//        {
//            var lstPlan = new List<SiteMapPlan>();
//            var plansDataTable = DataProvider.Current.SitemapGetPlans(partnerId);
//            if (plansDataTable != null)
//            {
//                lstPlan.AddRange(from DataRow row in plansDataTable.Rows
//                                 select new SiteMapPlan
//                                     {
//                                         PlanId = Convert.ToInt32(row["plan_id"]),
//                                         MarketId = Convert.ToInt32(row["market_id"])
//                                     });
//            }
//            return lstPlan;
//        }

//        public static List<SiteMapBrand> SitemapGetBrands(int partnerId)
//        {
//            var lstBrand = new List<SiteMapBrand>();
//            var brandsDataTable = DataProvider.Current.SitemapGetBrands(partnerId);

//            if (brandsDataTable == null)
//                return lstBrand;

//            lstBrand.AddRange(from DataRow row in brandsDataTable.Rows
//                              select new SiteMapBrand
//                                  {
//                                      BrandId = Convert.ToInt32(row["brand_id"]),
//                                      MarketId = Convert.ToInt32(row["market_id"])
//                                  });
//            return lstBrand;
//        }

//        public static List<SiteMapPostalCode> SitemapGetMarketPostalCodes(int partnerId)
//        {
//            var lstMarket = new List<SiteMapPostalCode>();
//            var postalCodesDataTable = DataProvider.Current.SitemapGetMarketPostalCodes(partnerId);
//            if (postalCodesDataTable == null)
//                return lstMarket;

//            lstMarket.AddRange(from DataRow row in postalCodesDataTable.Rows
//                               select new SiteMapPostalCode
//                                   {
//                                       PostalCode = Convert.ToInt32(row["postal_code"]),
//                                       MarketId = Convert.ToInt32(row["market_id"])
//                                   });
//            return lstMarket;
//        }

//        public static List<SiteMapCounty> SitemapGetMarketCounties(int partnerId)
//        {
//            var lstMarket = new List<SiteMapCounty>();
//            var contriesDataTable = DataProvider.Current.SitemapGetMarketCounties(partnerId);
//            if (contriesDataTable == null)
//                return lstMarket;

//            lstMarket.AddRange(from DataRow row in contriesDataTable.Rows
//                               select new SiteMapCounty
//                                   {
//                                       County = Convert.ToString(row["county"]),
//                                       MarketId = Convert.ToInt32(row["market_id"])
//                                   });
//            return lstMarket;
//        }
//    }
//}
