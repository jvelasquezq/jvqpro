﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using Nhs.Library.Business;
using Nhs.Library.Business.Attributes;
using Nhs.Library.Business.Enums;
using Nhs.Library.Business.Seo;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;

namespace Nhs.Library.Helpers.Seo
{
    public enum AffiliateLinkPageEnum
    {       
        CommunityDetail,
        HomeDetail,
        BasicCommunity,
        BasicHome
    }

    /// <summary>
    /// Search Result Page type
    /// </summary>
    public enum SrpTypeEnum : byte
    {
        [StringValue(Value = "_CommResults")]
        CommunityResults,

        [StringValue(Value = "_HomeResults")]
        HomeResults
    }

    public class AffiliateLinksData
    {
        public string MortageRatesFormArea { get; set; }
        public string FreeCreditScoreFormArea { get; set; }
        public string CalculateMortagePaymentsNexStepsArea { get; set; }
    }

    public class SeoContentManager
    {
        public string PartnerSiteUrl { get; set; }
        public string PageName { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string County { get; set; }
        public string Market { get; set; }
        public string CommunityName { get; set; }
        public string SyntheticName { get; set; }
        public string State { get; set; }
        public string BrandName { get; set; }
        public string BrandID { get; set; }
        public string SchoolDistrict { get; set; }
        
        public IList<ContentTag> ContentTags = new List<ContentTag>();

        public IDictionary<SeoDetailPagesTypeEnum, IList<SeoDetailPages>> GetAllSeoDetailPages()
        {
            const string cacheKey = "SeoDetailPagesContentData";
            var ob = WebCacheHelper.GetObjectFromCache(cacheKey, true);

            if (ob != null)
            {
                return ob as IDictionary<SeoDetailPagesTypeEnum, IList<SeoDetailPages>>;
            }

            var dic = new Dictionary<SeoDetailPagesTypeEnum, IList<SeoDetailPages>>
            {
                {SeoDetailPagesTypeEnum.Default, new List<SeoDetailPages>()},
                {SeoDetailPagesTypeEnum.ComDetail, new List<SeoDetailPages>()},
                {SeoDetailPagesTypeEnum.PlanDetail, new List<SeoDetailPages>() },
                {SeoDetailPagesTypeEnum.SpecDetail, new List<SeoDetailPages>()}
            };

            var folders = new List<SeoTemplateType>() { SeoTemplateType.Community, SeoTemplateType.Plan, SeoTemplateType.Spec };

            foreach (var folder in folders)
            {
                var files = GetSeoPathFiles(folder);

                foreach (var file in files)
                {
                    var key = file.EndsWith("Default.xml")
                        ? SeoDetailPagesTypeEnum.Default
                        : GetSeoDetailPagesEnumFromSeoTemplateType(folder);

                    var fileNameArr = Path.GetFileNameWithoutExtension(file).Split(new[] { '_' });
                    int? commId = key == SeoDetailPagesTypeEnum.Default ? default(int?) : fileNameArr.Last().ToType<int>();

                    var xml = new XmlDocument();
                    xml.Load(file);

                    var metaTags = new List<MetaTag>
                    {
                        new MetaTag
                        {
                            Name = MetaName.Title,
                            Content = GetHtmlBlock(xml, "title")
                        },
                        new MetaTag
                        {
                            Name = MetaName.Description,
                            Content = GetHtmlBlock(xml, "meta-description")
                        },
                        new MetaTag
                        {
                            Name = MetaName.Keywords,
                            Content = GetHtmlBlock(xml, "meta-keywords")
                        },
                        new MetaTag
                        {
                            Name = MetaName.Canonical,
                            Content = GetHtmlBlock(xml, "canonical")
                        }
                    };

                    dic[key].Add(new SeoDetailPages()
                    {
                        CommunityId = commId,
                        MetaTags = metaTags,
                        H1 = GetHtmlBlock(xml, "h1"),
                        Seo = GetHtmlBlock(xml, "seo"),
                        SeoDetailPagesType = GetSeoDetailPagesEnumFromSeoTemplateType(folder)
                    });
                }
            }

            WebCacheHelper.AddObjectToCache(dic, cacheKey, new TimeSpan(0, 23, 0), true);
            return dic;
        }

        private string GetHtmlBlock(XmlDocument xml, string blockName)
        {
            return LoadContentFromFile(xml, null, blockName, false);
        }

        private static SeoDetailPagesTypeEnum GetSeoDetailPagesEnumFromSeoTemplateType(SeoTemplateType type)
        {
            SeoDetailPagesTypeEnum retValue;
            switch (type)
            {
                case SeoTemplateType.None:
                    retValue = SeoDetailPagesTypeEnum.Default;
                    break;
                case SeoTemplateType.Community:
                    retValue = SeoDetailPagesTypeEnum.ComDetail;
                    break;
                case SeoTemplateType.Plan:
                    retValue = SeoDetailPagesTypeEnum.PlanDetail;
                    break;
                case SeoTemplateType.Spec:
                    retValue = SeoDetailPagesTypeEnum.SpecDetail;
                    break;
                default:
                    throw new InvalidEnumArgumentException(type.ToString(), (int)type, typeof(SeoTemplateType));
            }
            return retValue;
        }

        /// <summary>
        /// This method has a 15 min cache
        /// </summary>
        /// <param name="communityId"></param>
        /// <param name="contentTags"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public SeoDetailPages GetSeoDetailPages(int communityId, IList<ContentTag> contentTags, SeoTemplateType type = SeoTemplateType.Community)
        {
            SeoDetailPages retValue;
            switch (type)
            {
                case SeoTemplateType.Community:
                case SeoTemplateType.Spec:
                case SeoTemplateType.Plan:
                    var dicValues = GetAllSeoDetailPages();
                    retValue =
                        dicValues[GetSeoDetailPagesEnumFromSeoTemplateType(type)].FirstOrDefault(
                            content => content.CommunityId.HasValue && content.CommunityId == communityId);

                    if (retValue.HasValues == false)
                    {
                        retValue = dicValues[GetSeoDetailPagesEnumFromSeoTemplateType(SeoTemplateType.None)].FirstOrDefault(
                                content => content.SeoDetailPagesType == GetSeoDetailPagesEnumFromSeoTemplateType(type));
                    }

                    if (retValue.HasValues)
                    {
                        retValue = ReplaceTagsInSeoObject(retValue, contentTags);
                    }

                    break;
                default:
                    throw new InvalidEnumArgumentException(type.ToString(), (int)type, typeof(SeoTemplateType));
            }

            return retValue;
        }

        private SeoDetailPages ReplaceTagsInSeoObject(SeoDetailPages seoDp, IList<ContentTag> contentTags)
        {
            ContentTags = contentTags;

            var newMetas = seoDp.MetaTags.Select(metaTag => new MetaTag()
            {
                Content = ReplaceTags(metaTag.Content),
                Name = metaTag.Name
            }).ToList();

            var retObjet = new SeoDetailPages
            {
                H1 = ReplaceTags(seoDp.H1),
                Seo = ReplaceTags(seoDp.Seo),
                MetaTags = newMetas
            };

            return retObjet;
        }

        public List<MetaTag> GetMetaTags(SeoTemplateType seoTemplateType, IList<ContentTag> contentTags, SrpTypeEnum strType = SrpTypeEnum.CommunityResults)
        {
            ContentTags = contentTags;

            var metaList = new List<MetaTag>();
            //SeocontentManagerHelper.AddTag();

            var newMetaTitle = new MetaTag
                {
                    Name = MetaName.Title,
                    Content = GetHtmlBlock(seoTemplateType, "title", strType: strType)
                };

            if (contentTags != null && contentTags.Any(t => t.TagKey == ContentTagKey.PageNumber))
            {
                // Add the paging to the title for comm results, doing it here bc supporting it in the seo content xml they should change lots of files and its not conditional
                var pageNumber =
                    (contentTags.FirstOrDefault(t => t.TagKey == ContentTagKey.PageNumber) ?? new ContentTag()).TagValue;
                pageNumber = string.IsNullOrEmpty(pageNumber) ? "0" : pageNumber;

                if (PageName == Pages.CommunityResults && pageNumber.ToType<int>() > 1)
                    newMetaTitle.Content += string.Format(" – Page {0}",
                                                          contentTags.First(t => t.TagKey == ContentTagKey.PageNumber)
                                                                     .TagValue) ?? string.Empty;
            }

            var newMetaDescription = new MetaTag
                {
                    Name = MetaName.Description,
                    Content = GetHtmlBlock(seoTemplateType, "meta-description" ,strType: strType)
                };
            var newMetaKeywords = new MetaTag
                {
                    Name = MetaName.Keywords,
                    Content = GetHtmlBlock(seoTemplateType, "meta-keywords", strType: strType)
                };
            var newMetaRobots = new MetaTag
                {
                    Name = MetaName.Robots,
                    Content = GetHtmlBlock(seoTemplateType, "meta-robots", strType: strType)
                };

            var newMetaCanonical = new MetaTag
                {
                    Name = MetaName.Canonical,
                    Content = GetHtmlBlock(seoTemplateType, "canonical", strType: strType)
                };

            //TODO: when code block is footer?
            metaList.Add(newMetaTitle);
            metaList.Add(newMetaDescription);
            metaList.Add(newMetaKeywords);
            metaList.Add(newMetaRobots);
            metaList.Add(newMetaCanonical);

            return metaList;
        }

        /// <summary>
        /// This method return the content of the part specify by parameter from the xml found, the xml file location depends on the environment, for example
        /// for sprint you can find the xml files here: \\nhsdevfew1\NHS7BHIContent\SEOContent
        /// </summary>
        /// <param name="seoTemplateType">Specify the xml that is going to be search, there are specify xml per each type</param>
        /// <param name="htmlBlockName">Part of the xml that is going to be return</param>
        /// <param name="useDefault">If not xml is found use the general one</param>
        /// <param name="strType">Change the default content in the SRP, files must have _CommResults or _Homeresults</param>
        /// <returns></returns>
        public string GetHtmlBlock(SeoTemplateType seoTemplateType, string htmlBlockName, bool useDefault = true, 
            SrpTypeEnum strType = SrpTypeEnum.CommunityResults)
        {
            var doc = new XmlDocument();
            string xmlPath;

            if (!string.IsNullOrEmpty(PartnerSiteUrl))
            {
                xmlPath = GetXmlPath(seoTemplateType, false, PartnerSiteUrl, true, strType); //try partner-specific custom file first by state

                if (!File.Exists(xmlPath))//partner-specific custom by state not found, try partner-specific custom without state
                    xmlPath = GetXmlPath(seoTemplateType, false, PartnerSiteUrl, false, strType);

                if (!File.Exists(xmlPath))//partner-specific custom not found, try partner-specific default 
                    xmlPath = GetXmlPath(seoTemplateType, true, PartnerSiteUrl, false, strType);

                if (!File.Exists(xmlPath))//partner-specific default not found, try brand custom by state
                    xmlPath = GetXmlPath(seoTemplateType, false, string.Empty, true, strType);

                if (!File.Exists(xmlPath))//brand custom by state not found, try brand custom 
                    xmlPath = GetXmlPath(seoTemplateType, false, string.Empty, false, strType);

                if (!File.Exists(xmlPath))//default custom not found, try brand default 
                    xmlPath = GetXmlPath(seoTemplateType, true, string.Empty, false, strType);
            }
            else
            {
                xmlPath = GetXmlPath(seoTemplateType, false, string.Empty, true, strType); //Search using state-market-city

                if (!File.Exists(xmlPath))
                {
                    xmlPath = GetXmlPath(seoTemplateType, false, string.Empty, false, strType); //search using market-city
                }

                //if (!File.Exists(xmlPath))//default custom not found, try brand default 
                //  xmlPath = GetXmlPath(seoTemplateType, false, string.Empty, false); //brand default
            }


            if (File.Exists(xmlPath))
            {
                var content = LoadContentFromFile(doc, xmlPath, htmlBlockName);
                if (!string.IsNullOrEmpty(content))
                    return content;
            }
            /*else
            {
                var content = GetXmlPathUsingStartsWith(doc, xmlPath, htmlBlockName);
                if (!string.IsNullOrEmpty(content))
                    return content;
            }*/


            if (useDefault)
            {
                //BY DEFAULT
                //Use Default XML file in case of custom file not found
                return ReplaceTags(GetHtmlBlockFromDefaultXmlFile(seoTemplateType, htmlBlockName, strType));
            }
            return String.Empty;
        }

        private string LoadContentFromFile(XmlDocument doc, string xmlPath, string htmlBlockName, bool loadXmlPath = true)
        {
            try
            {
                if (loadXmlPath)
                {
                    doc.Load(xmlPath);
                }

                XmlNode root = doc.DocumentElement;
                if (root != null && string.IsNullOrWhiteSpace(htmlBlockName) == false)
                {
                    XmlNode block = root.SelectSingleNode(htmlBlockName);
                    if (block != null && block.ChildNodes[0] != null)
                    {
                        string blockText = block.ChildNodes[0].Value ?? string.Empty;
                        if (!string.IsNullOrEmpty(blockText.Trim()))
                            return ReplaceTags(blockText.Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError("SEO Content Read Error!", string.Format("Error reading XML File: {0}", xmlPath), "Ask SEO guy to fix XML file. Detailed stack trace in the following entry.");
                ErrorLogger.LogError(ex);
            }

            return string.Empty;
        }
        //TODO:Remove this crap method.
        private string GetXmlPathUsingStartsWith(XmlDocument doc, string xmlPath, string htmlBlockName)
        {
            var myDir = new DirectoryInfo(xmlPath);
            var onlyDir = myDir.FullName.Replace(myDir.Name, "");
            myDir = new DirectoryInfo(onlyDir);
            var matchValue1 = this.State + "_" + this.BrandID;
            var matchValue2 = this.State + "_" + this.Market + "_" + this.BrandID;
            try
            {
                FileInfo[] files1 = myDir.GetFiles(string.Format("{0}*", matchValue1));
                FileInfo[] files2 = myDir.GetFiles(string.Format("{0}*", matchValue2));

                var files = files1.Concat(files2).ToArray();

                if (files.Any())
                {
                    var file = files[0].Name;
                    try
                    {
                        doc.Load(onlyDir + file);
                        XmlNode root = doc.DocumentElement;
                        if (root != null)
                            if (root.SelectSingleNode(htmlBlockName) != null &&
                                !string.IsNullOrEmpty(root.SelectSingleNode(htmlBlockName).ChildNodes[0].Value.Trim()))
                                return ReplaceTags(root.SelectSingleNode(htmlBlockName).ChildNodes[0].Value);
                    }
                    catch (Exception ex)
                    {
                        ErrorLogger.LogError("SEO Content Read Error!", string.Format("Error reading XML File: {0}", xmlPath), "Ask SEO guy to fix XML file. Detailed stack trace in the following entry.");
                        ErrorLogger.LogError(ex);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError("SEO Content Read Error!", string.Format("Error getting XML File: {0}", xmlPath), "Ask SEO guy to fix XML file. Detailed stack trace in the following entry.");
                ErrorLogger.LogError(ex);
            }
            return string.Empty;
        }

        private string GetHtmlBlockFromDefaultXmlFile(SeoTemplateType seoTemplateType, string htmlBlockName, SrpTypeEnum srpType)
        {
            XmlNode root = null;
            var doc = new XmlDocument();
            if (!seoTemplateType.Equals(SeoTemplateType.None))
            {
                var xmlPath = GetXmlPath(seoTemplateType, true, string.Empty, false, srpType);
                if (File.Exists(xmlPath))
                {
                    try
                    {
                        doc.Load(xmlPath);
                        root = doc.DocumentElement;
                    }
                    catch (Exception ex)
                    {
                        ErrorLogger.LogError("SEO Content Read Error!", string.Format("Error reading XML File: {0}", xmlPath), "Ask SEO guy to fix XML file. Detailed stack trace in the following entry.");
                        ErrorLogger.LogError(ex);
                    }
                }
            }
            try
            {
                return root.SelectSingleNode(htmlBlockName).ChildNodes[0].Value;
            }
            catch
            {
                return string.Empty;
            }

        }

        public string[] GetSeoPathFiles(SeoTemplateType seoTemplateType)
        {
            var fullpath = "SEOContent/" + seoTemplateType + "/";
            fullpath = HttpContext.Current.Server.MapPath("~/" + Configuration.StaticContentFolder + "/" + fullpath);
            return Directory.GetFiles(fullpath);
        }

        private string GetXmlPath(SeoTemplateType seoTemplateType, bool useDefaultTemplate, 
            string partnerSiteUrl, bool secondLevelFilter, SrpTypeEnum srpType = SrpTypeEnum.CommunityResults)
        {
            string fullpath;

            //These values get cleaned up in order to search the files properly, as the file names do not include the white spaces.
            if (City != null) City = City.Replace(" ", string.Empty);
            if (County != null) County = County.Replace(" ", string.Empty);
            if (Market != null) Market = Market.Replace(" ", string.Empty);
            if (BrandName != null) BrandName = BrandName.RemoveSpecialCharacters();
            if (SchoolDistrict != null) SchoolDistrict = SchoolDistrict.RemoveSpecialCharacters();

            var strSrp = srpType.GetAttributeValue<StringValueAttribute, string>(x=> x.Value);

            string marketName = (string.IsNullOrEmpty(this.Market) ? string.Empty : this.Market.Replace(" ", string.Empty));

            switch (seoTemplateType)
            {
                case SeoTemplateType.Seo_Market:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/" + seoTemplateType + @"/Default_SEOMarketContentTemplate";
                    else if (secondLevelFilter)
                        fullpath = "SEOContent/" + seoTemplateType + @"/" + this.State + "-" + marketName + @"_SEOMarketContentTemplate";
                    else
                        fullpath = "SEOContent/" + seoTemplateType + @"/" + this.Market + @"_SEOMarketContentTemplate";
                    break;
                case SeoTemplateType.Nhs_Market_Srp:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/" + seoTemplateType + @"/Default" + strSrp + "-MarketContentTemplate";
                    else if (secondLevelFilter)
                        fullpath = "SEOContent/" + seoTemplateType + @"/" + this.State + "-" + marketName + strSrp + @"-MarketContentTemplate";
                    else
                        fullpath = "SEOContent/" + seoTemplateType + @"/" + this.Market.Trim() + strSrp +@"-MarketContentTemplate";
                    break;
                case SeoTemplateType.Nhs_Market_Coming_Srp:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/" + SeoTemplateType.Nhs_Market_Coming_Srp + @"/Default"+strSrp+"-MarketComingSoonContentTemplate";
                    else
                        fullpath = "SEOContent/" + SeoTemplateType.Nhs_Market_Srp + @"/Default"+strSrp+"-MarketContentTemplate";
                    break;
                case SeoTemplateType.Nhs_Market_QMI_Srp:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/" + SeoTemplateType.Nhs_Market_QMI_Srp + @"/Default_HomeResults-MarketQMIContentTemplate";
                    else
                        fullpath = "SEOContent/" + SeoTemplateType.Nhs_Market_Srp + @"/Default_HomeResults-MarketContentTemplate";
                    break;
                case SeoTemplateType.Nhs_Market_City_QMI_Srp:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/" + SeoTemplateType.Nhs_Market_City_QMI_Srp + @"/Default_HomeResults-MarketCityContentTemplate";
                    else if (secondLevelFilter)
                        fullpath = "SEOContent/" + seoTemplateType + @"/" + this.State + "-" + this.Market + "-" + this.City + @"_HomeResults-MarketCityContentTemplate";
                    else
                        fullpath = "SEOContent/" + seoTemplateType + @"/" + this.Market + "-" + this.City + @"_HomeResults-MarketCityContentTemplate";
                    break;
                case SeoTemplateType.Nhs_Market_HotDeals_Srp:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/" + SeoTemplateType.Nhs_Market_HotDeals_Srp + @"/Default"+strSrp+"-MarketHotDealsContentTemplate";
                    else
                        fullpath = "SEOContent/" + SeoTemplateType.Nhs_Market_Srp + @"/Default"+strSrp+"-MarketContentTemplate";
                    break;
                case SeoTemplateType.Nhs_CommunityName_Srp:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/" + seoTemplateType + @"/Default"+strSrp+"-CommunityNameContentTemplate";
                    else
                        fullpath = "SEOContent/" + seoTemplateType + @"/" + this.CommunityName + "-" + this.City + "-" + this.State + strSrp +@"-MarketContentTemplate";
                    break;
                case SeoTemplateType.NHS_Market_Brand:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/" + seoTemplateType + @"/Default_Template";
                    else
                        fullpath = "SEOContent/" + seoTemplateType + @"/" + this.State + "_" + this.Market + "_" + this.BrandID + "_" + this.BrandName.Replace(" ", "_").Replace("-", "_") + @"_Template";
                    break;
                case SeoTemplateType.Waterfront_Community_Market:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/Waterfront-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/Waterfront-Filter/" + seoTemplateType + @"/" + this.State + "_" + this.Market;
                    break;
                case SeoTemplateType.Waterfront_Home_Market:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/Waterfront-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/Waterfront-Filter/" + seoTemplateType + @"/" + this.State + "_" + this.Market;
                    break;
                case SeoTemplateType.Waterfront_Community_City:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/Waterfront-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/Waterfront-Filter/" + seoTemplateType + @"/" + this.State + "_" + this.City;
                    break;
                case SeoTemplateType.Waterfront_Home_City:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/Waterfront-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/Waterfront-Filter/" + seoTemplateType + @"/" + this.State + "_" + this.City;
                    break;
                case SeoTemplateType.Golf_Community_Market:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/Golf-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/Golf-Filter/" + seoTemplateType + @"/" + this.State + "_" + this.Market;
                    break;
                case SeoTemplateType.Golf_Home_Market:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/Golf-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/Golf-Filter/" + seoTemplateType + @"/" + this.State + "_" + this.Market;
                    break;
                case SeoTemplateType.Golf_Community_City:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/Golf-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/Golf-Filter/" + seoTemplateType + @"/" + this.State + "_" + this.City;
                    break;
                case SeoTemplateType.Golf_Home_City:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/Golf-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/Golf-Filter/" + seoTemplateType + @"/" + this.State + "_" + this.City;
                    break;
                case SeoTemplateType.Adult_Community_Market:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/Adult-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/Adult-Filter/" + seoTemplateType + @"/" + this.State + "_" + this.Market;
                    break;
                case SeoTemplateType.Adult_Home_Market:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/Adult-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/Adult-Filter/" + seoTemplateType + @"/" + this.State + "_" + this.Market;
                    break;
                case SeoTemplateType.Adult_Community_City:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/Adult-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/Adult-Filter/" + seoTemplateType + @"/" + this.State + "_" + this.City;
                    break;
                case SeoTemplateType.Adult_Home_City:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/Adult-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/Adult-Filter/" + seoTemplateType + @"/" + this.State + "_" + this.City;
                    break;
                case SeoTemplateType.SchoolDistrict_Community_Market:
                    if (useDefaultTemplate || SchoolDistrict == null)
                        fullpath = "SEOContent/SchoolDistrict-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/SchoolDistrict-Filter/" + seoTemplateType + @"/" + this.State + "_" + this.Market + "_" + this.SchoolDistrict.Replace(" ", "_").Replace("-", "_");
                    break;
                case SeoTemplateType.SchoolDistrict_Home_Market:
                    if (useDefaultTemplate || SchoolDistrict == null)
                        fullpath = "SEOContent/SchoolDistrict-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/SchoolDistrict-Filter/" + seoTemplateType + @"/" + this.State + "_" + this.Market + "_" + this.SchoolDistrict.Replace(" ", "_").Replace("-", "_");
                    break;
                case SeoTemplateType.MF_Community_Market:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/MultiFamily-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/MultiFamily-Filter/" + seoTemplateType + @"/" + this.State + "_" + this.Market;
                    break;
                case SeoTemplateType.MF_Home_Market:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/MultiFamily-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/MultiFamily-Filter/" + seoTemplateType + @"/" + this.State + "_" + this.Market;
                    break;
                case SeoTemplateType.MF_Community_City:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/MultiFamily-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/MultiFamily-Filter/" + seoTemplateType + @"/" + this.State + "_" + this.City;
                    break;
                case SeoTemplateType.MF_Home_City:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/MultiFamily-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/MultiFamily-Filter/" + seoTemplateType + @"/" + this.State + "_" + this.City;
                    break;
                case SeoTemplateType.Community_Market:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/Builder-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/Builder-Filter/" + seoTemplateType + @"/" + this.State + "_" + this.Market + "_" + this.BrandID + "_" + this.BrandName.Replace(" ", "_").Replace("-", "_");
                    break;
                case SeoTemplateType.Community_City:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/Builder-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/Builder-Filter/" + seoTemplateType + @"/" + this.State + "_" + this.City + "_" + this.BrandID + "_" + this.BrandName.Replace(" ", "_").Replace("-", "_");
                    break;
                case SeoTemplateType.Community_PostalCode:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/Builder-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/Builder-Filter/" + seoTemplateType + @"/" + this.Market + "_" + this.Zip + "_" + this.BrandID + "_" + this.BrandName.Replace(" ", "_").Replace("-", "_");
                    break;
                case SeoTemplateType.Community_County:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/Builder-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/Builder-Filter/" + seoTemplateType + @"/" + this.State + "_" + this.County + "_" + this.BrandID + "_" + this.BrandName.Replace(" ", "_").Replace("-", "_");
                    break;
                case SeoTemplateType.Home_Market:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/Builder-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/Builder-Filter/" + seoTemplateType + @"/" + this.State + "_" + this.Market + "_" + this.BrandID + "_" + this.BrandName.Replace(" ", "_").Replace("-", "_");
                    break;
                case SeoTemplateType.Home_City:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/Builder-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/Builder-Filter/" + seoTemplateType + @"/" + this.State + "_" + this.City + "_" + this.BrandID + "_" + this.BrandName.Replace(" ", "_").Replace("-", "_");
                    break;
                case SeoTemplateType.Home_PostalCode:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/Builder-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/Builder-Filter/" + seoTemplateType + @"/" + this.Market + "_" + this.Zip + "_" + this.BrandID + "_" + this.BrandName.Replace(" ", "_").Replace("-", "_");
                    break;
                case SeoTemplateType.Home_County:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/Builder-Filter/" + seoTemplateType + @"/Default";
                    else
                        fullpath = "SEOContent/Builder-Filter/" + seoTemplateType + @"/" + this.State + "_" + this.County + "_" + this.BrandID + "_" + this.BrandName.Replace(" ", "_").Replace("-", "_");
                    break;
                case SeoTemplateType.Nhs_Market_City_Srp:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/" + seoTemplateType + @"/Default"+strSrp+"-MarketCityContentTemplate";
                    else if (secondLevelFilter)
                        fullpath = "SEOContent/" + seoTemplateType + @"/" + this.State + "-" + this.Market + "-" + this.City + strSrp +@"-MarketCityContentTemplate";
                    else
                        fullpath = "SEOContent/" + seoTemplateType + @"/" + this.Market + "-" + this.City + strSrp +@"-MarketCityContentTemplate";
                    break;
                case SeoTemplateType.Nhs_County_Srp:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/" + seoTemplateType + @"/Default"+strSrp+"-CountyContentTemplate";
                    else
                        fullpath = "SEOContent/" + seoTemplateType + @"/" + this.State + "-" + this.County + strSrp +@"-CountyContentTemplate";
                    break;
                case SeoTemplateType.Nhs_Zip_Srp:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/" + seoTemplateType + @"/Default"+strSrp+"-ZipContentTemplate";
                    else
                        fullpath = "SEOContent/" + seoTemplateType + @"/Default"+strSrp+"-ZipContentTemplate";
                    break;
                // Synthetic Search
                case SeoTemplateType.Synthetic_Search: 
                case SeoTemplateType.Synthetic_Search_Adult:
                case SeoTemplateType.Synthetic_Search_ComingSoon:
                case SeoTemplateType.Synthetic_Search_GolfCourse: 
                case SeoTemplateType.Synthetic_Search_HotDeals:
                case SeoTemplateType.Synthetic_Search_MultiFamily:
                case SeoTemplateType.Synthetic_Search_QMI:
                case SeoTemplateType.Synthetic_Search_WaterFront :
                case SeoTemplateType.Synthetic_Search_Builder:
                case SeoTemplateType.Synthetic_Search_SchoolDistrict:
                    var filterName = Enum.GetName(typeof(SeoTemplateType), seoTemplateType).Replace("Synthetic_Search_", string.Empty);
                    filterName = filterName == "Synthetic_Search" ? "SyntheticName" : filterName;
                    var searchTypeName = srpType == SrpTypeEnum.CommunityResults ? "Community" : "Home";
                    var syntheticNameFixed = this.SyntheticName.Replace(" ", "_").Replace("-", "_");

                    if (useDefaultTemplate)
                    {
                        fullpath = string.Format("SEOContent/Synthetic/{0}/{1}_Default", filterName, searchTypeName);
                    }
                    else
                    {
                        if (seoTemplateType == SeoTemplateType.Synthetic_Search_Builder)
                        {
                            fullpath = string.Format("SEOContent/Synthetic/{0}/{1}_{2}_{3}_{4}", filterName, searchTypeName, syntheticNameFixed, this.BrandID, this.BrandName.Replace(" ", "_").Replace("-", "_"));
                        } else if (seoTemplateType == SeoTemplateType.Synthetic_Search_SchoolDistrict)
                        {
                            fullpath = string.Format("SEOContent/Synthetic/{0}/{1}_{2}_{3}", filterName, searchTypeName, syntheticNameFixed, this.SchoolDistrict.Replace(" ", "_").Replace("-", "_"));
                        }
                        else
                        {
                            fullpath = string.Format("SEOContent/Synthetic/{0}/{1}_{2}", filterName, searchTypeName, syntheticNameFixed);
                        }
                    }
                    break;
                case SeoTemplateType.Nhs_Site_Index:
                    if (useDefaultTemplate)
                    {
                        fullpath = "SEOContent/SiteIndex/Default";
                    }
                    else
                    {
                        fullpath = "SEOContent/SiteIndex/" + this.State.ToUpper();
                    }
                    break;
                case SeoTemplateType.Nhs_Site_Index_State:
                    if (useDefaultTemplate)
                    {
                        fullpath = "SEOContent/SiteIndex/" + seoTemplateType + @"/Default";
                    }
                    else
                    {
                        fullpath = "SEOContent/SiteIndex/" + seoTemplateType + @"/" + this.State.ToUpper();
                    }
                    break;
                case SeoTemplateType.NHS_StateBrands:
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/" + seoTemplateType + @"/Default_Template";
                    else if (secondLevelFilter)
                        fullpath = "SEOContent/" + seoTemplateType + @"/" + this.State + "_" + this.BrandID + "_" + this.BrandName.Replace(" ", "_").Replace("-", "_") + @"_Template";
                    else
                        fullpath = "SEOContent/" + seoTemplateType + @"/" + this.State + @"_Default_Template";
                    break;
                case SeoTemplateType.Nhs_Quickconnect_Srp:
                    fullpath = "SEOContent/" + seoTemplateType + @"/Default_QuickResultsContentTemplate";
                    break;

                case SeoTemplateType.Survey_Modal_Content:
                    fullpath = "SEOContent/" + seoTemplateType + @"/SurveyModalContent";
                    break;

                case SeoTemplateType.CommDetail_Res_Center:
                    fullpath = "SEOContent/CommunityDetail/ResourceCenterSection";
                    break;

                case SeoTemplateType.Nhs_HomeV2:
                    fullpath = "SEOContent/" + seoTemplateType + @"/DefaultV2";
                    break;

                case SeoTemplateType.MNH_HomeV2:
                    fullpath = "SEOContent/" + seoTemplateType + @"/DefaultV2";
                    break;

                case SeoTemplateType.Nhs_Builders_Market_Srp:
                    strSrp = "_HomeBuilders";

                    if (useDefaultTemplate)
                        fullpath = "SEOContent/" + seoTemplateType + @"/Default" + strSrp + "-MarketContentTemplate";
                    else
                        fullpath = "SEOContent/" + seoTemplateType + @"/" + marketName + strSrp + @"-MarketContentTemplate"; 
                    break;

                case SeoTemplateType.Nhs_Builders_Market_City_Srp:
                    strSrp = "_HomeBuilders";

                    if (useDefaultTemplate)
                        fullpath = "SEOContent/" + seoTemplateType + @"/Default" + strSrp + "-MarketCityContentTemplate";
                    else if (secondLevelFilter)
                        fullpath = "SEOContent/" + seoTemplateType + @"/" + this.State + "-" + this.Market + "-" + this.City + strSrp + @"-MarketCityContentTemplate";
                    else
                        fullpath = "SEOContent/" + seoTemplateType + @"/" + this.Market + "-" + this.City + strSrp + @"-MarketCityContentTemplate";
                    break;

                default:
                    fullpath = "SEOContent/" + seoTemplateType + @"/Default";
                    break;
            }

            fullpath = !string.IsNullOrEmpty(partnerSiteUrl) ? partnerSiteUrl + @"/" + fullpath + ".xml" : fullpath + ".xml";

            return HttpContext.Current.Server.MapPath(@"~/" + Configuration.StaticContentFolder + @"/" + fullpath);
        }

        /// <summary>
        /// Replaces the Tags from the Hashtable into the XML Chunk
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private string ReplaceTags(string input)
        {
            if (input.IndexOf('[') <= -1) return input;

            // The change here was made in order to fulfill the case 73898 - Builder brand not displaying correctly - McNaughton Homes
            // http://bits.builderhomesite.com/Production/default.asp?73898
            // Special brand names like McNaughton and JDouglas was beign displayed as Mcnaughton and Jdouglas
            foreach (var key in this.ContentTags)
            {
                var value = (!string.IsNullOrEmpty(key.TagValue) && !string.Equals(key.TagKey.ToString(), SeoConstTags.BrandName) && key.ToTitleCase)
                    ? StringHelper.ToTitleCase(key.TagValue)
                    : key.TagValue;
                input = input.Replace("[" + key.TagKey + "]", string.IsNullOrEmpty(value) ? string.Empty : value);
            }
            return input;
        }
    }
}
