﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.XPath;
using Nhs.Library.Business.Config;
using Nhs.Library.Business.Seo;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Utility.Common;

namespace Nhs.Library.Helpers.Seo
{
    public class FriendlyMarketNamesReader
    {
        private readonly string _configFileName = @"~\" + Configuration.StaticContentFolder + @"\SEOContent\FriendlyMarketNames\MarketNames.xml";
        private readonly IPathMapper _pathMapper;

        public FriendlyMarketNamesReader(IPathMapper pathMapper)
        {
            _pathMapper = pathMapper;
        }

        public Dictionary<int, string> ParseScripts()
        {
            var cacheKey = "FriendlyMarketNamesKey";
            var names = WebCacheHelper.GetObjectFromCache(cacheKey, false) as Dictionary<int, string>;

            string path = _pathMapper.MapPath(_configFileName);

            if (names == null)
            {
                names = new Dictionary<int, string>();
                try
                {

                    XPathDocument doc = new XPathDocument(path);
                    XPathNavigator nav = doc.CreateNavigator();

                    // Scripts
                    XPathNodeIterator pageItr = nav.Select("/marketnames/market");
                    while (pageItr.MoveNext())
                    {
                        if (!string.IsNullOrEmpty(pageItr.Current.GetAttribute("name", String.Empty)) &&
                            !string.IsNullOrEmpty(pageItr.Current.GetAttribute("id", String.Empty)))
                        {
                            names.Add(GetNodeAttributeInt("id", pageItr.Current), pageItr.Current.GetAttribute("name", String.Empty));
                        }
                    }

                    WebCacheHelper.AddObjectToCache(names, cacheKey, new TimeSpan(WebCacheConst.DefaultHours, 0, 0), false);
                }
                catch (Exception ex)
                {
                    ErrorLogger.LogError(ex);
                }
            }

            return names;
        }

        private int GetNodeAttributeInt(string key, XPathNavigator navigator)
        {
            string attribute = navigator.GetAttribute(key, String.Empty);
            return (attribute == String.Empty || !attribute.All(Char.IsNumber)) ? 0 : int.Parse(attribute);
        }
    }
}
