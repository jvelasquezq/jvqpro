﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.XPath;
using Nhs.Library.Business.Config;
using Nhs.Library.Business.Seo;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Utility.Common;

namespace Nhs.Library.Helpers.Seo
{
    public class SeoLinkPromotionReader
    {
        private readonly string _configFileName = @"~\" + Configuration.StaticContentFolder + @"\SEOContent\Link_Promotion\seolinkpromotion.xml";
        private readonly IPathMapper _pathMapper;

        public SeoLinkPromotionReader(IPathMapper pathMapper)
        {
            _pathMapper = pathMapper;
        }

        public Dictionary<string, SeoLinkPromotionPage> ParseScripts()
        {
            var cacheKey = "SeoLinkPromotionKey";
            var pages = WebCacheHelper.GetObjectFromCache(cacheKey, false) as Dictionary<string, SeoLinkPromotionPage>;

            string path = _pathMapper.MapPath(_configFileName);

            if (pages == null)
            {
                pages = new Dictionary<string, SeoLinkPromotionPage>();
                try
                {

                    XPathDocument doc = new XPathDocument(path);
                    XPathNavigator nav = doc.CreateNavigator();

                    // Scripts
                    XPathNodeIterator pageItr = nav.Select("/seolinkpromotion/page");
                    while (pageItr.MoveNext())
                    {
                        var page = new SeoLinkPromotionPage();
                        page.Name = pageItr.Current.GetAttribute("name", String.Empty);
                        page.Title = pageItr.Current.GetAttribute("title", String.Empty);
                        page.LinksText = pageItr.Current.GetAttribute("linktext", String.Empty);
                        page.Count = GetNodeAttributeInt("linkcount", pageItr.Current);

                        if (!string.IsNullOrEmpty(page.LinksText) && !string.IsNullOrEmpty(page.Name) && page.Count > 0 && !pages.ContainsKey(page.Name))
                            pages.Add(page.Name, page);

                        // pages
                        XPathNodeIterator linkItr = pageItr.Current.Select("link");
                        while (linkItr.MoveNext())
                        {
                            var link = new SeoLinkPromotionItem();
                            link.Url = linkItr.Current.GetAttribute("url", string.Empty);
                            link.Priority = GetNodeAttributeInt("priority", linkItr.Current);
                            link.Name = linkItr.Current.GetAttribute("linktext", string.Empty);

                            if (link.Priority > 0 && !string.IsNullOrEmpty(link.Url))
                            {
                                if (link.Url.IndexOf(UrlConst.MarketID + "-") != -1)
                                    link.MarketId = link.Url.Split('/').ToList().Where(v => v.IndexOf(UrlConst.MarketID + "-") != -1).Select(v => HttpUtility.UrlDecode(v.Substring(v.IndexOf("-") + 1))).FirstOrDefault().ToType<int>();

                                if (link.Url.IndexOf(UrlConst.City + "-") != -1)
                                    link.City = link.Url.Split('/').ToList().Where(v => v.IndexOf(UrlConst.City + "-") != -1).Select(v => HttpUtility.UrlDecode(v.Substring(v.IndexOf("-") + 1))).FirstOrDefault();

                                if (link.Url.IndexOf(UrlConst.CommunityID + "-") != -1)
                                    link.CommunityId = link.Url.Split('/').ToList().Where(v => v.IndexOf(UrlConst.CommunityID + "-") != -1).Select(v => HttpUtility.UrlDecode(v.Substring(v.IndexOf("-") + 1))).FirstOrDefault().ToType<int>();

                                if (link.Url.IndexOf(UrlConst.PlanID + "-") != -1)
                                    link.PlanId = link.Url.Split('/').ToList().Where(v => v.IndexOf(UrlConst.PlanID + "-") != -1).Select(v => HttpUtility.UrlDecode(v.Substring(v.IndexOf("-") + 1))).FirstOrDefault().ToType<int>();

                                if (link.Url.IndexOf(UrlConst.SpecID + "-") != -1)
                                    link.SpecId = link.Url.Split('/').ToList().Where(v => v.IndexOf(UrlConst.SpecID + "-") != -1).Select(v => HttpUtility.UrlDecode(v.Substring(v.IndexOf("-") + 1))).FirstOrDefault().ToType<int>();

                                page.Links.Add(link);
                            }
                        }

                    }

                    WebCacheHelper.AddObjectToCache(pages, cacheKey, new TimeSpan(WebCacheConst.DefaultHours, 0, 0), false);
                }
                catch (Exception ex)
                {
                    ErrorLogger.LogError(ex);
                }
            }

            return pages;
        }

        private int GetNodeAttributeInt(string key, XPathNavigator navigator)
        {
            string attribute = navigator.GetAttribute(key, String.Empty);
            return (attribute == String.Empty || !attribute.All(Char.IsNumber)) ? 0 : int.Parse(attribute);
        }
    }
}
