﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.Xml.XPath;
using Nhs.Library.Business.Config;
using Nhs.Library.Business.Seo;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Utility.Common;

namespace Nhs.Library.Helpers.Seo
{
    public static class SyntheticGeoReader
    {
        private static IPathMapper _pathMapper = new ContextPathMapper();
        private const string SyntheticsXmlPath = "~/{0}/SEOContent/Synthetic/Synthetics.xml";


        public static List<Synthetic> GetSynthetics()
        {
            var synthetics = WebCacheHelper.GetObjectFromCache(WebCacheConst.Synthetics, false) as List<Synthetic>;
            if (synthetics == null)
            { 
                synthetics = BindSynthetics();
                WebCacheHelper.AddObjectToCache(synthetics, WebCacheConst.Synthetics, new TimeSpan(WebCacheConst.DefaultHours, 0, 0), false);
            }

            return synthetics;
        }

        private static List<Synthetic> BindSynthetics()
        {
            var synthetics = new SyntheticGeoNames();

            var pathFileMarket = _pathMapper.MapPath(string.Format(SyntheticsXmlPath, Configuration.StaticContentFolder));

            try
            {
                synthetics = XmlSerialize.DeserializeFromXmlFile<SyntheticGeoNames>(pathFileMarket);
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }

            return synthetics.Synthetics;
        }
    }
}
