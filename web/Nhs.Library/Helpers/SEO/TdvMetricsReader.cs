﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.Xml.XPath;
using Nhs.Library.Business.Config;
using Nhs.Library.Business.Seo;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Utility.Common;

namespace Nhs.Library.Helpers.Seo
{
    public static class TdvMetricsReader
    {
        private static IPathMapper _pathMapper = new ContextPathMapper();
        private const string AffiliateLinksXmlPath = "~/{0}/SEOContent/TDVMetrics/TDVMetrics.xml";


        public static List<Metric> GetTdvMetrics()
        {
            var metrics = WebCacheHelper.GetObjectFromCache(WebCacheConst.TdvMetrics, false) as List<Metric>;
            if (metrics == null)
                metrics = BindTdvMetrics();

            return metrics;
        }

        private static List<Metric> BindTdvMetrics()
        {
            var tdvMetrics = new TdvMetrics();
            var metrics = new List<Metric>();

            var pathFileMarket = _pathMapper.MapPath(string.Format(AffiliateLinksXmlPath, Configuration.StaticContentFolder));

            try
            {
                tdvMetrics = XmlSerialize.DeserializeFromXmlFile<TdvMetrics>(pathFileMarket);

                if (tdvMetrics.Metrics != null)
                {
                    foreach (var m in tdvMetrics.Metrics)
                    {
                        foreach (var ev in m.Events.Split(','))
                        {
                            if (!string.IsNullOrEmpty(ev) ) { metrics.Add(new Metric() { Event = ev, Name = m.Name, Value = m.Value });}
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }
             
            return metrics;
        }
    }
}
