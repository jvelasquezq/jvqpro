﻿using System;
using System.Collections.Generic;
using System.Xml.XPath;
using Nhs.Library.Business.Tracking;
using Nhs.Utility.Common;

namespace Nhs.Library.Helpers.Tracking
{
    public class TrackingScriptsReader
    {
        private const string TrackingScriptFileName = @"~\Configs\trackingscripts.xml";
        private readonly IPathMapper _pathMapper;

        public TrackingScriptsReader(IPathMapper pathMapper)
        {
            _pathMapper = pathMapper;
        }

        public IList<TrackingScript> ParseScripts()
        {
            IList<TrackingScript> scripts = new List<TrackingScript>();
            string path = _pathMapper.MapPath(TrackingScriptFileName);
            XPathDocument doc = new XPathDocument(path);
            XPathNavigator nav = doc.CreateNavigator();

            // Scripts
            XPathNodeIterator scriptItr = nav.Select("/trackingscripts/script");
            while (scriptItr.MoveNext())
            {
                var script = new TrackingScript();
                script.FileName = scriptItr.Current.GetAttribute("file", String.Empty);
                var addToHead = scriptItr.Current.GetAttribute("addtohead", string.Empty);

                if (string.IsNullOrEmpty(addToHead) || addToHead.ToLower() == "false")
                    script.AddToHead = false;
                else
                    script.AddToHead = true;

                scripts.Add(script);

                // Brand Partners
                XPathNodeIterator brandPartnerItr = scriptItr.Current.Select("brandpartner");
                while (brandPartnerItr.MoveNext())
                {
                    var brandPartner = new TrackingScriptBrandPartner();
                    brandPartner.BrandPartnerId = GetNodeAttributeInt("id", brandPartnerItr.Current);
                    script.BrandPartners.Add(brandPartner);
                    
                    // Partners
                    XPathNodeIterator partnerItr = brandPartnerItr.Current.Select("partner");
                    while (partnerItr.MoveNext())
                    {
                        var partner = new TrackingScriptPartner();
                        partner.PartnerId = GetNodeAttributeInt("id", partnerItr.Current);
                        partner.SiteUrl =  partnerItr.Current.GetAttribute("siteurl", string.Empty);
                       
                        brandPartner.Partners.Add(partner);

                        // Functions
                        XPathNodeIterator functionItr = partnerItr.Current.Select("functions/function");
                        while (functionItr.MoveNext())
                        {
                            partner.Functions.Add(functionItr.Current.Value);
                        }
                    }
                }
            }
            return scripts;
        }

        private int GetNodeAttributeInt(string key, XPathNavigator navigator)
        {
            string attribute = navigator.GetAttribute(key, String.Empty);
            return (attribute == String.Empty) ? -1 : int.Parse(attribute);
        }
    }
}
