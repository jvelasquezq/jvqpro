using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Caching;

namespace Nhs.Library.Common
{
    public static class WebCacheHelper
    {
        #region "Public Static Methods"

        #region Add Methods
        public static void AddObjectToCache(object data, string cacheItemKey, TimeSpan timespan, bool usePartnerId)
        {
            cacheItemKey = GetPartnerCacheItemKey(cacheItemKey, usePartnerId);

            if (HttpContext.Current != null)
                HttpContext.Current.Cache.Insert(cacheItemKey, data, null, Cache.NoAbsoluteExpiration, timespan, CacheItemPriority.Normal, null);
        }

        public static void AddObjectToCache(object data, string cacheItemKey, TimeSpan timespan, Cache cacheRef, CacheItemPriority priority)
        {
            cacheRef.Insert(cacheItemKey, data, null, Cache.NoAbsoluteExpiration, timespan, priority, null);
        }

        public static void AddDataTableToCache(DataTable data, string cacheItemKey, TimeSpan timespan, bool usePartnerId)
        {
            AddObjectToCache(data, cacheItemKey, timespan, usePartnerId);
        }

        public static void AddDataSetToCache(DataSet data, string cacheItemKey, int cacheHours, bool usePartnerId)
        {
            AddObjectToCache(data, cacheItemKey, new TimeSpan(cacheHours, 0, 0), false);
        }
        #endregion

        #region Get Methods

        //Done
        public static object GetObjectFromCache(string cacheItemKey, bool usePartnerId)
        {
            cacheItemKey = GetPartnerCacheItemKey(cacheItemKey, usePartnerId);

            if (HttpContext.Current != null)
                return HttpContext.Current.Cache[cacheItemKey];
            else
                return null;
        }

        //Done
        public static DataTable GetDataTableFromCache(string cacheItemKey, bool usePartnerId)
        {
            if (HttpContext.Current != null)
            {
                cacheItemKey = GetPartnerCacheItemKey(cacheItemKey, usePartnerId);
                object cachedObject = HttpContext.Current.Cache[cacheItemKey];
                if (cachedObject != null)
                {
                    try
                    {
                        return (DataTable)cachedObject;
                    }
                    catch
                    {
                        throw new ApplicationException("Cached Item '" + cacheItemKey + "' is not a datatable");
                    }
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        //Done
        public static DataSet GetDataSetFromCache(string cacheItemKey, bool usePartnerId)
        {
            if (HttpContext.Current != null)
            {
                cacheItemKey = GetPartnerCacheItemKey(cacheItemKey, usePartnerId);
                object cachedObject = HttpContext.Current.Cache[cacheItemKey];
                if (cachedObject != null)
                {
                    try
                    {
                        return (DataSet)cachedObject;
                    }
                    catch
                    {
                        throw new ApplicationException("Cached Item '" + cacheItemKey + "' is not a dataset");
                    }

                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        //Done
        public static List<DataTable> GetMarketSearchDataFromCache(string cacheItemKey, bool usePartnerId)
        {
            if (HttpContext.Current == null)
                throw new ApplicationException("Current HttpContext not available '");

            cacheItemKey = GetPartnerCacheItemKey(cacheItemKey, usePartnerId);
            object cachedObject = HttpContext.Current.Cache[cacheItemKey];
            List<DataTable> cachedDataSet = cachedObject as List<DataTable>;
            return cachedDataSet;
        }
        #endregion

        #region Remove Methods
        public static void RemoveObjectFromCache(string cacheItemKey, Cache cacheRef, bool usePartnerId)
        {
            cacheItemKey = GetPartnerCacheItemKey(cacheItemKey, usePartnerId);
            if (cacheRef != null & cacheRef[cacheItemKey] != null)
                cacheRef.Remove(cacheItemKey);
        }
        #endregion

        private static string GetPartnerCacheItemKey(string cacheKey, bool usePartnerId)
        {
            if (usePartnerId)
                return (cacheKey + "_" + Configuration.PartnerId);
            
            return cacheKey;
        }

        #endregion
    }
}
