﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Nhs.Library.Business.Ad;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Utility.Common;

namespace Nhs.Library.Helpers.Ad
{
    public class AdPageReader
    {
        public static List<AdPage> GetAdPages(IPathMapper pathMapper, int brandPartnerId, bool isMobile)
        {
            var cacheKey = string.Concat(WebCacheConst.WebCacheKeyPrefix, "AdPages", (isMobile ? "_Mobile" : string.Empty), "_", brandPartnerId);
            var adPages = WebCacheHelper.GetObjectFromCache(cacheKey, false) as List<AdPage>;

            if (adPages != null) return adPages;

            var xmlFileName = isMobile
                ? pathMapper.MapPath(string.Format(@"~\Configs\AdConfig\Mobile\AdPages_${0}.xml", brandPartnerId))
                : pathMapper.MapPath(string.Format(@"~\Configs\AdConfig\AdPages_${0}.xml", brandPartnerId));
            try
            {
                if (xmlFileName.Length <= 0 || !File.Exists(xmlFileName))
                    throw new Exception("Invalid XML file name or file does not exist");

                var documentRoot = XElement.Load(xmlFileName);

                adPages = documentRoot.Descendants("adpage").Select(adPage => new AdPage
                {
                    Name = adPage.Attribute("name").Value,
                    Positions =
                        adPage.Attribute("positions")
                            .Value.Split(new[] {',', ' '}, StringSplitOptions.RemoveEmptyEntries)
                            .ToList(),
                    AdPageCode = adPage.Attribute("adpagecode").Value,
                    UrlPattern = adPage.Attribute("urlpattern").Value
                }).ToList();


                WebCacheHelper.AddObjectToCache(adPages, cacheKey,
                    new TimeSpan(WebCacheConst.DefaultHours, 0, 0), false);
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }
            return adPages;
        }
    }
}
