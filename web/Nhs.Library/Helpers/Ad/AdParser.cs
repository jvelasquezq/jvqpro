using System.Text;
using System.Text.RegularExpressions;
using Nhs.Library.Constants;

namespace Nhs.Library.Common
{
    public class AdParser
    {
        private AdParser()
        {

        }

        private static string PrepareFunctionCall(string functionCall)
        {
            return functionCall.Replace("[FUNCTION:", "").Replace("]", "");
        }

        public static string GetFunctionName(string functionCall)
        {
            string parms = PrepareFunctionCall(functionCall);
            string[] funcTokens = parms.Split(";".ToCharArray());
            string[] functionNameTokens = funcTokens[0].ToString().Split("=".ToCharArray());
            return functionNameTokens[1];
        }

        public static string GetLogRedirectURL(string[] parm, string serverName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("http://" + serverName + "/");
            sb.Append(Pages.LogRedirect + "?");
            sb.Append(UrlConst.AdvertiserID + "=" + parm[0].ToString());
            sb.Append("&" + UrlConst.ExternalURL + "=" + parm[1].ToString());
            return sb.ToString();
        }

        public static string[] GetFunctionParameters(string functionCall)
        {
            string parms = PrepareFunctionCall(functionCall);
            string[] funcTokens = parms.Split(";".ToCharArray());
            string[] funcParmsTokens = funcTokens[1].ToString().Split("=".ToCharArray());
            string funcParms = funcParmsTokens[1];

            for (int z = 2; z < funcParmsTokens.Length; z++)
            {
                funcParms += "=" + funcParmsTokens[z];
            }

            return funcParms.Split("|".ToCharArray());
        }

        public static string GetFunctionTarget(string functionCall)
        {
            string parms = PrepareFunctionCall(functionCall);
            string[] funcTokens = parms.Split(";".ToCharArray());
            string[] targetTokens = funcTokens[2].Split("=".ToCharArray());
            return targetTokens[1];
        }

        public static string GetFunctionText(string functionCall)
        {
            string parms = PrepareFunctionCall(functionCall);
            string[] funcTokens = parms.Split(";".ToCharArray());
            string[] clickTextTokens = funcTokens[3].Split("=".ToCharArray());
            return clickTextTokens[1];
        }

        public static string ReplaceInlineTags(string body, string categoryName, string filename, string serverName, string randNum)
        {
            string _out = body;
            _out = ReplaceAdvertisementTags(_out, categoryName, filename, serverName, randNum);
            _out = ReplaceInlineFunctions(_out, categoryName, filename, serverName, randNum);
            return _out;
        }


        private static string ReplaceAdvertisementTags(string body, string categoryName, string filename, string serverName, string randNum)
        {
            string pattern = @"(\[AD.+?\])";
            MatchCollection matches = GetMatches(body, pattern);
            string _out = DoReplacement(body, categoryName, filename, matches, 1, serverName, randNum);
            return _out;
        }


        private static string ReplaceInlineFunctions(string body, string categoryName, string filename, string serverName, string randNum)
        {
            string pattern = @"(\[FUNCTION:.+?\])";
            MatchCollection matches = GetMatches(body, pattern);
            string _out = DoReplacement(body, categoryName, filename, matches, 2, serverName, randNum);
            return _out;
        }


        public static MatchCollection GetMatches(string body, string pattern)
        {
            MatchCollection matches = Regex.Matches(body, pattern, RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture);
            return matches;
        }


        public static string DoReplacement(string body, string categoryName, string filename, MatchCollection matches, int type, string serverName, string randNum)
        {
            string _out = body.Replace("&amp;", "&");
            string result = "";

            foreach (Match match in matches)
            {
                result = match.ToString();

                    StringBuilder sb = new StringBuilder();
                    //string parms = result.Substring(result.IndexOf(" ",1) + 1);

                    string parms = "";

                    switch (type)
                    {
                        case (1):	// Advertisment
                            parms = result.Replace("[AD ", "");
                            string[] tokens = parms.Split("=".ToCharArray());
                            int i = tokens.Length / 2;
                            //long randNumber = new System.Random().Next(1000000, 999999999);
                            string ConNHSRandomNum = randNum;
                            string ConNHSPageName = "";
                            string sServerDomain = serverName;
                            string sSiteName = "";

                            if (sServerDomain.Equals("localhost"))
                                sServerDomain = "www.newhomesource.com";
                            else
                            {
                                // Added for 4.3 active adult release. 
                                if (sServerDomain.IndexOf("newretirementcommunities.com") != -1)
                                {
                                    sServerDomain = "www.newhomesource.com";
                                    sSiteName = "newretirementcommunities.com";
                                }
                                else
                                {
                                    if (!sServerDomain.Equals("www.newhomesource.com") &&
                                        !sServerDomain.Equals("www.casasnuevasaqui.com") &&
                                        !sServerDomain.Equals("www.newretirementcommunities.com"))
                                    {
                                        sServerDomain = "www.newhomesource.com";
                                    }
                                }
                            }

                            sServerDomain = sServerDomain.Substring(sServerDomain.IndexOf(".", 1) + 1);

                            if (sSiteName == "")
                            {
                                sSiteName = sServerDomain;
                            }
                            if (sServerDomain == "newhomesource.com") 
                            {
                                sSiteName = "nhs.com";    
                            }

                            string AdPosition = "Middle";

                            if (sServerDomain.ToLower().Equals("newhomesource.com"))
                                ConNHSPageName = "nhg";

                            string querystring = "se=" + categoryName;
                            querystring += "&amp;amp;a=" + filename;

                            //[AD ALIGN=RIGHT]
                            for (int x = 0; x < i; x++)
                            {
                                switch (tokens[x].ToLower())
                                {
                                    case "align":
                                        sb.Append("<div class=\"nhsAdContainerArticle\" style=\"float: " + tokens[x + 1].Trim().Replace("]", "") + ";\">");
                                        sb.Append("<p class=\"nhsAdHeaderTxt\">Advertisement</p>");
                                        sb.Append("<div id=\"nhsAdTag\">");
                                        sb.Append("<script type=\"text/javascript\" src=\"http://oascentral." + sServerDomain + "/RealMedia/ads/adstream_jx.ads/" + sSiteName + "/" + ConNHSPageName + "/" + ConNHSRandomNum + "@Top," +AdPosition+ "!Middle?" +querystring.Replace(" ", "%20")+ "\"></script>");
                                        sb.Append("<!-- Delivery Attempt without Javascript -->");
                                        sb.Append("<noscript>");
                                        sb.Append("<a href=\"http://oascentral." + sServerDomain + "/RealMedia/ads/click_nx.ads/" + sSiteName + "/" + ConNHSPageName + "/" + ConNHSRandomNum + "@Top," + AdPosition + "!Middle?" + querystring.Replace(" ", "%20") + "\">");
                                        sb.Append("<img src=\"http://oascentral." + sServerDomain + "/RealMedia/ads/adstream_nx.ads/" + sSiteName + "/" + ConNHSPageName + "/" + ConNHSRandomNum + "@Top," + AdPosition + "!Middle?" + querystring.Replace(" ", "%20") + "\" alt=\"\" /></a>");
                                        sb.Append("</noscript>");
                                        sb.Append("</div>");
                                        sb.Append("</div>");
                                        _out = _out.Replace(result, sb.ToString());
                                        break;
                                }
                            }
                            sb = null;
                            break;
                        case (2):	// Inline Functions
                            //[FUNCTION NAME=(name) PARMS=(parm1|parm2|parm3) TARGET=(target) TEXT=(text)]
                            result = result.Replace("&amp;", "&");
                            string functionName = GetFunctionName(result);
                            string[] parm = GetFunctionParameters(result);
                            string target = GetFunctionTarget(result);
                            string clickText = GetFunctionText(result);
                            string functionRet = "";

                            switch (functionName.ToLower())
                            {
                                case ("getsiteurl"):
                                    functionRet = GetSiteURL(parm, serverName);
                                    break;
                                //case ("getcampaignurl"):
                                //    functionRet = GetCampaignURL(parm, serverName);
                                //    break;
                                case ("getlogredirecturl"):
                                    functionRet = GetLogRedirectURL(parm, serverName);
                                    break;
                                case ("getnewarticleurl"):
                                    functionRet = GetNewArticleURL(parm, serverName);
                                    break;
                            }
                            sb.Append("<a href=\"" + functionRet + "\" target=\"" + target + "\">" + clickText + "</a>");

                            _out = _out.Replace(result, sb.ToString());
                            sb = null;
                            break;
                    }
            }
            return _out;
        }

        public static string GetNewArticleURL(string[] parm, string serverName)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("http://" + serverName + "/");
            sb.Append("HomeGuide/");
            sb.Append(parm[1].ToString() + "/");		//category name (section)
            sb.Append(parm[0].ToString() + ".nhg");		//content_title

            return sb.ToString();
        }

        public static string GetSiteURL(string[] parm, string serverName)
        {
            return "http://" + serverName + "/search/" + parm[0] + ".aspx";
        }
    }
}
