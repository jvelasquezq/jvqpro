﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Nhs.Library.Business.Ad;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Utility.Common;
using System.IO;

namespace Nhs.Library.Helpers.Ad
{
    public class AdSlotReader
    {
        public static List<AdSlot> GetAdSlots(IPathMapper pathMapper, bool isMobile)
        {
            string cacheKey = WebCacheConst.WebCacheKeyPrefix + "AdSlots" + (isMobile ? "_Mobile" : "");

            var adSlots =
                WebCacheHelper.GetObjectFromCache(cacheKey, false) as List<AdSlot>;

            if (adSlots == null)
            {
                var xmlFileName = isMobile
                    ? pathMapper.MapPath(@"~\Configs\AdConfig\Mobile\AdSlots.xml")
                    : pathMapper.MapPath(@"~\Configs\AdConfig\AdSlots.xml");
                try
                {
                    if (xmlFileName.Length <= 0 || !File.Exists(xmlFileName))
                        throw new Exception("Invalid XML file name or file does not exist");

                    var documentRoot = XElement.Load(xmlFileName);

                    adSlots = (from slot in documentRoot.Descendants("adslot")
                               select new AdSlot
                                   {
                                       PlaceHolder = slot.Attribute("placeholder").Value,
                                       AdPositions = slot.Attribute("adpositions").Value.Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList(),
                                       Height = slot.Attribute("height").Value,
                                       Width = slot.Attribute("width").Value
                                   }).ToList();


                    WebCacheHelper.AddObjectToCache(adSlots, cacheKey,
                                                        new TimeSpan(WebCacheConst.DefaultHours, 0, 0), false);
                }
                catch (Exception ex)
                {
                    ErrorLogger.LogError(ex);
                }
            }
            return adSlots;
        }
    }
}
