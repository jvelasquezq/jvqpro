﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Nhs.Library.Business.Ad;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Utility.Common;

namespace Nhs.Library.Helpers.Ad
{
    public static class AddDislayConfigReader
    {
        public static AdDisplay GetAdDisplay(IPathMapper pathMapper, string url)
        {
            var adDisplays = AdDisplays(pathMapper);
            var adDisplay =
               adDisplays.FirstOrDefault(
                   a => url.ToLower().Contains(a.UrlPattern.ToLower()) && !string.IsNullOrEmpty(a.UrlPattern));
            return adDisplay;
        }

        private static IEnumerable<AdDisplay> AdDisplays(IPathMapper pathMapper)
        {
            const string cacheKey = WebCacheConst.WebCacheKeyPrefix + "AddDislayConfig";
            var adDisplays = WebCacheHelper.GetObjectFromCache(cacheKey, false) as List<AdDisplay>;

            if (adDisplays == null)
            {
                var xmlFileName =
                    pathMapper.MapPath("~/" + Configuration.StaticContentFolder + "/Ads/AdConfig/AdDisplay.xml");
                try
                {
                    if (xmlFileName.Length <= 0 || !File.Exists(xmlFileName))
                        throw new Exception("Invalid XML file name or file does not exist");
                    var addDislayConfig = XmlSerialize.DeserializeFromXmlFile<AddDislayConfig>(xmlFileName);

                    adDisplays = addDislayConfig.AdDisplay;

                    WebCacheHelper.AddObjectToCache(adDisplays, cacheKey, new TimeSpan(WebCacheConst.DefaultHours, 0, 0),
                        false);
                }
                catch (Exception ex)
                {
                    ErrorLogger.LogError(ex);
                }
            }
            return adDisplays;
        }
    }
}