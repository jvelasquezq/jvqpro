﻿using Nhs.Library.Web;

namespace Nhs.Library.Common
{
    public static class AdControllerHelper
    {
        /// <summary>
        /// Adds Detail specific parameters to an Ad Controller
        /// </summary>
        /// <param name="adController">The Ad Controller.</param>
        /// <param name="marketId">The Market ID.</param>
        /// <param name="priceLow">The Low price.</param>
        /// <param name="priceHigh">The High price.</param>
        /// <param name="state">The State.</param>
        /// <param name="builderId">The Builder ID.</param>
        /// <param name="communityId">The Community ID.</param>
        public static void DetailAdController(AdController adController, int marketId, int priceLow, int priceHigh, string state, int builderId, int communityId)
        {
            // Add parameters to Ad Controller
            adController.AddMarketParameter(marketId);
            adController.AddCommunityParameter(communityId);
            adController.AddPriceParameter(priceLow, priceHigh);
            adController.AddStateParameter(state);
            adController.AddBuilderParameter(builderId);
        }                    
    }
}
