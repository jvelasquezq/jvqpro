﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Nhs.Library.Business.Sso;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Utility.Common;

namespace Nhs.Library.Helpers.Sso
{
    public static class SsoConfigReader
    {
        /// <summary>
        /// Gets a PartnerSamlAttributes object which containt the list of attributes for the partner, filtered by partner site URL
        /// </summary>
        /// <param name="partnerSiteUrl">PartnerSiteURL</param>
        /// <param name="pathMapper">PathMapper</param>
        /// <returns></returns>
        public static PartnerSamlAttributes GetPartnerSamlAttributes(string partnerSiteUrl, IPathMapper pathMapper)
        {
            string cacheKey = "SamlAttributes_" + partnerSiteUrl;
            var partnerSamlAttributes =
                WebCacheHelper.GetObjectFromCache(cacheKey, false) as PartnerSamlAttributes;

            if (partnerSamlAttributes == null)
            {
                string xmlFileName = pathMapper.MapPath(@"~\Configs\SsoConfigs\SsoConfigAttributes.xml");
                try
                {
                    if (xmlFileName.Length <= 0 || !File.Exists(xmlFileName))
                        throw new Exception("Invalid XML file name or file does not exist");

                    XElement documentRoot = XElement.Load(xmlFileName);

                    var partnersList =
                        (from partners in documentRoot.Descendants("partner")
                         let attribute1 = partners.Attribute("siteurl")
                         where attribute1 != null && attribute1.Value == partnerSiteUrl
                         select new
                                    {
                                        SiteUrl = (string)partners.Attribute("siteurl"),
                                        AttributesList = partners.Elements("attributes").Elements(),
                                    }).ToList();

                    if (partnersList.Count > 0)
                    {
                        partnerSamlAttributes = new PartnerSamlAttributes();
                        partnerSamlAttributes.PartnerSiteUrl = partnerSiteUrl;
                        foreach (var attribute in partnersList[0].AttributesList)
                        {
                            XSamlAttribute samlAttribute = new XSamlAttribute();
                            var xAttribute1 = attribute.Attribute("key");
                            if (xAttribute1 != null)
                                samlAttribute.Key = xAttribute1.Value;
                            var xAttribute = attribute.Attribute("name");
                            if (xAttribute != null)
                                samlAttribute.Name = xAttribute.Value;

                            partnerSamlAttributes.SamlAttributes.Add(samlAttribute);
                        }

                    }
                    if (partnerSamlAttributes != null && partnerSamlAttributes.SamlAttributes.Count > 0)
                        WebCacheHelper.AddObjectToCache(partnerSamlAttributes, cacheKey,
                                                        new TimeSpan(WebCacheConst.DefaultHours, 0, 0), false);
                }
                catch (Exception ex)
                {
                    Trace.Write("Error in retrieving Partner SAML Attribs" + ex.Message + Environment.NewLine);
                    ErrorLogger.LogError(ex);
                }
            }
            return partnerSamlAttributes;
        }
    }
}
