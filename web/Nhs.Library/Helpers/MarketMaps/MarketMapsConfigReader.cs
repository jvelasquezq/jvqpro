﻿

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Utility.Common;

namespace Nhs.Library.Helpers.MarketMaps
{
    public static class MarketMapsConfigReader
    {
        public static List<MarketMap> GetMarketsMaps(IPathMapper pathMapper)
        {
            const string cacheKey = "ProMarketsMaps";
            var marketMaps =
                WebCacheHelper.GetObjectFromCache(cacheKey, false) as List<MarketMap>;

            if (marketMaps != null) return marketMaps;

            var xmlFileName = pathMapper.MapPath(@"~/" + Configuration.StaticContentFolder + @"/MarketMaps/ActiveMarketMaps.xml");
            var pdfPath = Configuration.MarketMapsVirtualDirectory;
            try
            {
                if (xmlFileName.Length <= 0 || !File.Exists(xmlFileName))
                    throw new Exception("Invalid XML file name or file does not exist");

                var documentRoot = XElement.Load(xmlFileName);

                marketMaps =
                    (from markets in documentRoot.Descendants()
                        let id = markets.Attribute("id")
                        let fileName = markets.Attribute("filename")
                        where (!string.IsNullOrEmpty(fileName.Value) && !string.IsNullOrEmpty(id.Value))
                        select new MarketMap
                        {
                            MarketId = (int)id,
                            FileName = pdfPath + fileName.Value
                        }).ToList();

                if (marketMaps.Count > 0)
                    WebCacheHelper.AddObjectToCache(marketMaps, cacheKey,
                        new TimeSpan(WebCacheConst.DefaultHours, 0, 0), false);
            }
            catch (Exception ex)
            {
                Trace.Write("Error in retrieving Market Maps for NHS Pro" + ex.Message + Environment.NewLine);
                ErrorLogger.LogError(ex);
            }
            return marketMaps;
        }
    }
}
