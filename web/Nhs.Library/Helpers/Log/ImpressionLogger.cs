using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Xml;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Utility.Common;
using Nhs.Library.Web;
using Nhs.Mvc.Routing.Interface;

namespace Nhs.Library.Common
{
    /// <summary>
    /// Logs Views by tracking clickthroughs - Used for Customer Reports 
    /// Use the LogImpressions constants class to pass in impression names
    /// </summary>
    public class ImpressionLogger
    {
        #region Member Variables

        private List<int> _planIds = new List<int>();
        private List<int> _specIds = new List<int>();
        private List<string> _bc = new List<string>();
        private List<string> _bll = new List<string>();
        private List<string> _fll = new List<string>();
        private static object _lockObject = new object();
        private static object _lockObjectCustom = new object();
        private bool _loggedAlready;
        private bool _multiLogging;
        private bool _isBot;
        #endregion

        #region Properties

        public string Event { get; set; }
        public int BuilderId { get; set; }
        public int CommunityId { get; set; }
        public string PartnerId { get; set; }
        public string Refer { get; set; }
        public string AdvertiserUrl { get; set; }
        public string AdvertiserId { get; set; }
        public int MarketId { get; set; }
        public string FromPage { get; set; }
        public int FeaturedListingId { get; set; }
        public int TotalHomes { get; set; }
        public int BasicListingId { get; set; }
        public bool TestMode { get; set; }
        public bool LogMarketingValues { get; set; }
        public GoogleUtmz GoogleUtmz { get; set; }
        public string GoogleUtmaUId { get; set; }
        public string GoogleUtmaSId { get; set; }
        public string ReferCode { get; set; }
        public string AdaptiveTemplate { get; set; }
        #endregion

        public ImpressionLogger()
            : this(false)
        {
        }
        public ImpressionLogger(bool multiLogging)
        {
            // Update properties
            this._multiLogging = multiLogging;

        }

        #region Public Methods

        /// <summary>
        /// Adds a View Event to the Log - Used to generate customer reports, set appropriate params b4 calling
        /// </summary>
        /// <param name="eventName">Name of the event.</param>
        /// <returns></returns>
        private Hashtable WriteToLogs(string eventName)
        {
            HttpRequest request = HttpContext.Current.Request;
            this._isBot = UserAgentIsSearchBot(request.UserAgent, true);

            Hashtable parameters = new Hashtable();
            AddParam(parameters, "BuilderId", this.BuilderId);
            AddParam(parameters, "CommunityId", this.CommunityId != 0 ? this.CommunityId.ToString() : (_bc != null && _bc.Count > 0? "" :  "NULL"));
            if (LogImpressionConst.CommunityDetailPageLoad == eventName) AddParam(parameters, "TotalHomes", TotalHomes);
            AddParam(parameters, "PID", this.PartnerId);
            AddParam(parameters, "Refer", this.Refer);
            AddParam(parameters, "AdvertiserUrl", this.AdvertiserUrl);
            AddParam(parameters, "AdvertiserID", this.AdvertiserId);
            AddParam(parameters, "MarketId", this.MarketId);
            AddParam(parameters, "FromPage", this.FromPage);
            AddParam(parameters, "IsBot", this._isBot ? "Y" : "N");
            AddParam(parameters, "FeaturedListingId", this.FeaturedListingId != 0 ? this.FeaturedListingId.ToString() : (_fll != null && _fll.Count > 0? "" :  "NULL"));
            AddParam(parameters, "BasicListingId", this.BasicListingId != 0 ? this.BasicListingId.ToString() : (_bll != null && _bll.Count > 0 ? "" : "NULL"));
            // AddParam(parameters, "Test", this.TestMode); Not test mode for now

            StringBuilder plans = new StringBuilder();
            foreach (int planid in _planIds)
                plans.Append(planid + ",");

            StringBuilder specs = new StringBuilder();
            foreach (int specId in _specIds)
                specs.Append(specId + ",");

            AddParam(parameters, "PlanId", plans.ToString().TrimEnd(','));
            AddParam(parameters, "SpecificationId", specs.ToString().TrimEnd(','));
            
            if (_bc != null && _bc.Any())
            {
                parameters["CommunityId"] = this._bc.Select(c => c.Split(',')[1]).ToArray().Join(",");
            }

            if (_bll != null && _bll.Any())
            {
                // Log only BL ids without builder id as it is already made for community lists logs
                parameters["BasicListingId"] = this._bll.Select(bl => bl.Split(',')[1]).ToArray().Join(",");
            }

            if (_fll != null && _fll.Any())
            {
                // Log only BL ids without builder id as it is already made for community lists logs
                parameters["FeaturedListingId"] = this._fll.Select(bl => bl.Split(',')[1]).ToArray().Join(",");  
            }

            AddParam(parameters, "GA_UID", string.IsNullOrEmpty(this.GoogleUtmaUId) ? "NULL" : this.GoogleUtmaUId);
            AddParam(parameters, "GA_SID", string.IsNullOrEmpty(this.GoogleUtmaSId) ? "NULL" : this.GoogleUtmaSId);
            AddParam(parameters, "GA_Medium", this.GoogleUtmz == null ? "NULL" : (string.IsNullOrEmpty(this.GoogleUtmz.UtmCmd) ? "NULL" : this.GoogleUtmz.UtmCmd));
            AddParam(parameters, "GA_Source", this.GoogleUtmz == null ? "NULL" : (string.IsNullOrEmpty(this.GoogleUtmz.UtmCsr) ? "NULL" : this.GoogleUtmz.UtmCsr));
            AddParam(parameters, "GA_Campaign", this.GoogleUtmz == null ? "NULL" : (string.IsNullOrEmpty(this.GoogleUtmz.UtmCcn) ? "NULL" : this.GoogleUtmz.UtmCcn));
            AddParam(parameters, "GA_Content", this.GoogleUtmz == null ? "NULL" : (string.IsNullOrEmpty(this.GoogleUtmz.UtmCct) ? "NULL" : this.GoogleUtmz.UtmCct));
            AddParam(parameters, "GA_Term", this.GoogleUtmz == null ? "NULL" : (string.IsNullOrEmpty(this.GoogleUtmz.UtmCtr) ? "NULL" : this.GoogleUtmz.UtmCtr));
            AddParam(parameters, "Refercode", string.IsNullOrEmpty(this.ReferCode) ? "NULL" : this.ReferCode);
            AddParam(parameters, "AdaptiveTemplate", AdaptiveTemplate);

            
            switch (eventName)
            {
                case LogImpressionConst.FeaturedListingView:
                    if (!UserAgentIsSearchBot(request.UserAgent, false))
                        this.WriteToLog4Db(eventName);
                    break;
                case LogImpressionConst.FeaturedListingClick:
                    if (!UserAgentIsSearchBot(request.UserAgent, false))
                        this.WriteToLog4Db(eventName);
                    break;
                default:
                    //this.WriteToFile("VW", eventName, parameters);
                    this.WriteToLog4Db(eventName);
                    break;
            }

            return parameters;
        }

        public void LogView(string eventName, Page page)
        {
            LogMarketingValues = true;
            GoogleUtmz = UserSession.GoogleUtmz;
            GoogleUtmaUId = UserSession.GoogleUtmaUId;
            GoogleUtmaSId = UserSession.GoogleUtmaSId;
            ReferCode = UserSession.Refer;
         
            Hashtable parameters = WriteToLogs(eventName);
            if (eventName != LogImpressionConst.FeaturedListingView &&
                eventName != LogImpressionConst.FeaturedListingClick)
            {
                WriteToLog("VW", eventName, parameters, page);
            }
        }


        public IList<MetaTag> LogView(string eventName)
        {
            LogMarketingValues = true;
            GoogleUtmz = UserSession.GoogleUtmz;
            GoogleUtmaUId = UserSession.GoogleUtmaUId;
            GoogleUtmaSId = UserSession.GoogleUtmaSId;
            ReferCode = UserSession.Refer;
         
            AdaptiveTemplate = NhsRoute.ShowMobileSite ? "M" : "D";

            Hashtable parameters = WriteToLogs(eventName);
            if (eventName != LogImpressionConst.FeaturedListingView &&
                eventName != LogImpressionConst.FeaturedListingClick)
            {
                return GetLogMetaTags("VW", eventName, parameters);
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userAgent"></param>
        /// <param name="useSearchBotXmlFile">True=Uses an xml file with a list of bots (that can be controlled via BHIContent)
        /// False=Uses a hard-coded list of bots (see below-else part)
        /// </param>
        /// <returns></returns>
        private bool UserAgentIsSearchBot(string userAgent, bool useSearchBotXmlFile)
        {
            if (string.IsNullOrEmpty(userAgent)) //for cases when there is no user agent (e.g. AckManager)
                return false;

            bool isSearchBot = false;

            if (useSearchBotXmlFile)
            {
                XmlDocument doc = new XmlDocument();
                if (HttpContext.Current.Application["searchBotsXML"] == null)
                {
                    StreamReader str = new StreamReader(HttpContext.Current.Server.MapPath(PathHelpers.GetFileInVirtualFolder(@"~/" + Configuration.StaticContentFolder + @"/SearchBotsIndex/", "SearchBots.xml", new ContextPathMapper())));
                    HttpContext.Current.Application["searchBotsXML"] = str.ReadToEnd().ToLower();
                }
                doc.LoadXml(HttpContext.Current.Application["searchBotsXML"].ToString());

                XmlNodeList allBots = doc.SelectNodes("searchbots/bot");
                for (int i = 0; i < allBots.Count; i++)
                {
                    //if (allBots[i].InnerXml.ToLower().Equals(userAgent.ToLower()))
                    if (Regex.IsMatch(userAgent.ToLower(), allBots[i].InnerXml.ToLower()))
                    {
                        isSearchBot = true;
                        break;
                    }
                }

            }
            else //Use default bot list
            {
                string[] defaultBots = ("bot|spider|slurp|mediapartners|crawler|yandex|ia_archiver|teoma").Split('|'); 
                for (int i = 0; i < defaultBots.Length; i++)
                {
                    if (Regex.IsMatch(userAgent.ToLower(), defaultBots[i]))
                    {
                        isSearchBot = true;
                        break;
                    }
                }
            }

            return isSearchBot;
        }

        public void AddPlan(int planId)
        {
            if (planId > 0)
                _planIds.Add(planId);
        }

        public void AddSpec(int specId)
        {
            if (specId > 0)
                _specIds.Add(specId);
        }

        public void AddBuilderCommunity(int builderId, int communityId)
        {
            if (builderId > 0 && communityId > 0)
                _bc.Add(builderId.ToString() + "," + communityId.ToString());
        }

        public void AddBasicListing(int builderId, int blId)
        {
            if (blId > 0)
                _bll.Add(builderId.ToString() + "," + blId.ToString());
        }

        public void AddFeaturedListing(int builderId, int commId, int flId)
        {
            if (builderId > 0 && commId > 0 && flId > 0)
                _fll.Add(builderId.ToString() + "," + commId.ToString() + "," + flId.ToString());
        }

        #endregion

        #region Private Methods

        private void WriteToLog(string classType, string eventName, Hashtable parameters, Page page)
        {
            string message = "CEVT={T=" + classType + ",";

            foreach (object key in parameters.Keys)
            {
                if (parameters[key] != null && parameters[key].ToString() != "" && parameters[key].ToString() != "0")
                    message = message + "," + key + "=" + parameters[key];
            }
            message += "}";

            //Add event code querystring parameter for webtrends reporting
            if (page != null)
            {
                string sdcImpressionLogMetaTag = string.Format(@"<meta name=""DCSext.webevents"" content=""{0}""/>", message);
                LiteralControl litsdc = new LiteralControl(sdcImpressionLogMetaTag);
                page.Header.Controls.Add(litsdc);

                string sdcImpressionLogMetaTagEventCode = string.Format(@"<meta name=""DCSext.eventcode"" content='" + eventName + "'/>");
                LiteralControl litsdcEventCode = new LiteralControl(sdcImpressionLogMetaTagEventCode);
                page.Header.Controls.Add(litsdcEventCode);

                string sdcImpressionLogMetaTagPartnerID = string.Format(@"<meta name=""DCSext.pid"" content='" + PartnerId + "'/>");
                LiteralControl litsdcPartnerID = new LiteralControl(sdcImpressionLogMetaTagPartnerID);
                page.Header.Controls.Add(litsdcPartnerID);
            }
        }

        private IList<MetaTag> GetLogMetaTags(string classType, string eventName, Hashtable parameters)
        {
            string message = "CEVT={T=" + classType + ",";
            foreach (object key in parameters.Keys)
            {
                if (parameters[key] != null && parameters[key].ToString() != "" && parameters[key].ToString() != "0")
                    message = message + "," + key + "=" + parameters[key];
            }
            message += "}";

            //Add event code querystring parameter for webtrends reporting
            var staticContent = new List<MetaTag>();
            staticContent.Add(new MetaTag { Name = MetaName.DcsWebevents, Content = message });
            staticContent.Add(new MetaTag { Name = MetaName.DcsEventcode, Content = eventName });
            staticContent.Add(new MetaTag { Name = MetaName.DcsPid, Content = PartnerId });
            return staticContent;
        }

        //private void WriteToFile(string classType, string eventName, Hashtable parameters)
        //{
        //    string message = "CEVT={T=" + classType + ",EVT=" + eventName;

        //    foreach (object key in parameters.Keys)
        //    {
        //        if (parameters[key] != null && parameters[key].ToString() != "" && parameters[key].ToString() != "0")
        //            message = message + "," + key + "=" + parameters[key];
        //    }
        //    message += "}";

        //    string fileName = GetFileName();

        //    message = DateTime.Now + " " + message;

        //    lock (_lockObject)
        //    {
        //        //Create or append to Log File
        //        File.AppendAllText(fileName, message + Environment.NewLine);
        //    }
        //}

        private void WriteToLog4Db(string eventName)
        {
            //Check for specs
            if (_specIds.Count > 0)
                foreach (int specId in _specIds)
                    LogMessage(eventName, 0, specId, this.BuilderId, this.CommunityId);

            //Check for plans
            if (_planIds.Count > 0)
                foreach (int planId in _planIds)
                    LogMessage(eventName, planId, 0, this.BuilderId, this.CommunityId);

            //Check for BCs
            if (_bc.Count > 0)
                foreach (string bc in _bc)
                {
                    string builderId = bc.Remove(bc.IndexOf(","));
                    string communityId = bc.Remove(0, bc.IndexOf(",") + 1);
                    LogMessage(eventName, 0, 0, int.Parse(builderId), int.Parse(communityId));
                }

            if (_bll.Count > 0)
                foreach (string bl in _bll)
                {
                    string builderId = bl.Split(',')[0];
                    BasicListingId = bl.Split(',')[1].ToType<int>();
                    LogMessage(eventName, 0, 0, int.Parse(builderId), 0);
                }

            if (_fll.Count > 0)
                foreach (string fl in _fll)
                {
                    string builderId = fl.Split(',')[0];
                    string communityId = fl.Split(',')[1];
                    FeaturedListingId = fl.Split(',')[2].ToType<int>();
                    LogMessage(eventName, 0, 0, builderId.ToType<int>(), communityId.ToType<int>());
                }

            //Log event if not any of the above 
            if (!_loggedAlready)
            {
                LogMessage(eventName, 0, 0, this.BuilderId, this.CommunityId);
            }
        }

        private void LogMessage(string eventName, int planId, int specId, int builderId, int communityId)
        {
            StringBuilder message = new StringBuilder();
            message.Append(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "\t");
            message.Append(eventName + "\t");
            if (planId > 0)
                message.Append(planId + "\t");
            else
                message.Append("NULL" + "\t");

            if (communityId > 0)
                message.Append(communityId + "\t");
            else
                message.Append("NULL" + "\t");

            if (builderId > 0)
                message.Append(builderId + "\t");
            else
                message.Append("NULL" + "\t");

            if (specId > 0)
                message.Append(specId + "\t");
            else
                message.Append("NULL" + "\t");

            if (this.Refer != null)
                message.Append(this.Refer + "\t");
            else
                message.Append("NULL" + "\t");

            if (!string.IsNullOrEmpty(this.PartnerId) && this.PartnerId != "0")
                message.Append(this.PartnerId + "\t");
            else
                message.Append("NULL" + "\t");

            if (this.MarketId > 0)
                message.Append(this.MarketId + "\t");
            else
                message.Append("NULL" + "\t");

            if (this.FeaturedListingId > 0)
                message.Append(this.FeaturedListingId + "\t");
            else
                message.Append("NULL" + "\t");

            if (this._isBot)
                message.Append("Y" + "\t");
            else
                message.Append("N" + "\t");

            if (this.BasicListingId > 0)
                message.Append(this.BasicListingId + "\t");
            else
                message.Append("NULL" + "\t");

            message.Append((string.IsNullOrEmpty(this.GoogleUtmaUId) ? "NULL" : this.GoogleUtmaUId) + "\t");
            message.Append((this.GoogleUtmz == null ? "NULL" : (string.IsNullOrEmpty(this.GoogleUtmz.UtmCmd) ? "NULL" : this.GoogleUtmz.UtmCmd)) + "\t");
            message.Append((this.GoogleUtmz == null ? "NULL" : (string.IsNullOrEmpty(this.GoogleUtmz.UtmCsr) ? "NULL" : this.GoogleUtmz.UtmCsr)) + "\t");
            message.Append((this.GoogleUtmz == null ? "NULL" : (string.IsNullOrEmpty(this.GoogleUtmz.UtmCcn) ? "NULL" : this.GoogleUtmz.UtmCcn)) + "\t");
            message.Append((this.GoogleUtmz == null ? "NULL" : (string.IsNullOrEmpty(this.GoogleUtmz.UtmCct) ? "NULL" : this.GoogleUtmz.UtmCct)) + "\t");
            message.Append((this.GoogleUtmz == null ? "NULL" : (string.IsNullOrEmpty(this.GoogleUtmz.UtmCtr) ? "NULL" : this.GoogleUtmz.UtmCtr)) + "\t");
            message.Append((string.IsNullOrEmpty(this.ReferCode) ? "NULL" : this.ReferCode) + "\t");
            message.Append(AdaptiveTemplate + "\t");
            message.Append((string.IsNullOrEmpty(this.GoogleUtmaSId) ? "NULL" : this.GoogleUtmaSId) + "\t");

            string fileName = GetCustomLogFileName();

            lock (_lockObjectCustom)
            {
                //Create or append to Log File
                File.AppendAllText(fileName, message + Environment.NewLine);
            }
            _loggedAlready = !this._multiLogging;
        }

        public void LogPDFGeneration(StringBuilder message)
        {
            var fileName = GetCustomLogFilePDFGenerationName();
            lock (_lockObjectCustom)
            {
                //Create or append to Log File
                File.AppendAllText(fileName, message + Environment.NewLine);
            }
        }

        private void AddParam(Hashtable parameters, string paramName, object param)
        {
            if (param != null)
                parameters.Add(paramName, param);
        }

        //private static string GetFileName()
        //{
        //    return Configuration.CustomLogFolder + DateTime.Now.Date.Month + "-" + DateTime.Now.Date.Day + "-" + DateTime.Now.Date.Year + "_text.log";
        //}

        private static string GetCustomLogFileName()
        {
            return Configuration.CustomLogFolder + DateTime.Now.Date.Month + "-" + DateTime.Now.Date.Day + "-" + DateTime.Now.Date.Year + ".log4db";
        }

        private static string GetCustomLogFilePDFGenerationName()
        {
            return Configuration.CustomLogFolder + DateTime.Now.Date.Month + "-" + DateTime.Now.Date.Day + "-" + DateTime.Now.Date.Year + "_pdf_generate.log";
        }
        #endregion
    }
}
