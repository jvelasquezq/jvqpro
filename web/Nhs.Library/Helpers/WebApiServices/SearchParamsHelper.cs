﻿using System.Collections.Generic;
using System.Linq;
using Bdx.Web.Api.Objects.Constants;
using Nhs.Library.Business;
using Nhs.Library.Constants.Enums;
using Nhs.Mvc.Routing.Configuration;
using System;
using System.Web;
using Nhs.Library.Extensions;
using Nhs.Library.Web;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;

namespace Nhs.Library.Helpers.WebApiServices
{
    public static class SearchParamsHelper
    {
        public static Dictionary<string, object> ToWebApiParameters(this SearchParams searchParams)
        {
            var @params = new Dictionary<string, object> { { ApiUrlConstV2.PartnerId, NhsRoute.PartnerId } };
            //searchParams.SearchLocation = request.GetQueryString(UrlConst.SearchLocation);

            if (searchParams.WebApiSearchType == WebApiSearchType.Exact)
            {
                if (!string.IsNullOrEmpty(searchParams.City))
                    @params.Add(ApiUrlConstV2.City, searchParams.City);
                else if (!string.IsNullOrEmpty(searchParams.County))
                    @params.Add(ApiUrlConstV2.County, searchParams.County);
                else if (!string.IsNullOrEmpty(searchParams.PostalCode))
                    @params.Add(ApiUrlConstV2.PostalCode, searchParams.PostalCode);
                else if (searchParams.Cities != null && searchParams.Cities.Any())
                    @params.Add(ApiUrlConstV2.Cities, searchParams.Cities.ToArray().Join(searchParams.Cities[0].Contains(",") ? ";" : ","));
                else if (searchParams.Counties != null && searchParams.Counties.Any())
                    @params.Add(ApiUrlConstV2.Counties, searchParams.Counties.ToArray().Join(searchParams.Counties[0].Contains(",") ? ";" : ","));
                else if (searchParams.PostalCodes != null && searchParams.PostalCodes.Any())
                    @params.Add(ApiUrlConstV2.Postalcodes, searchParams.PostalCodes.ToArray().Join(searchParams.PostalCodes[0].Contains(",") ? ";" : ","));
                else if (searchParams.Markets != null && searchParams.Markets.Any())
                    @params.Add(ApiUrlConstV2.Markets, searchParams.Markets.ToArray().Join(","));
                else if (searchParams.MarketId > 0)
                    @params.Add(ApiUrlConstV2.MarketId, searchParams.MarketId);
            }
            else if (searchParams.WebApiSearchType == WebApiSearchType.Radius)
            {
                @params.Add(ApiUrlConstV2.OriginLat, searchParams.OriginLat);
                @params.Add(ApiUrlConstV2.OriginLng, searchParams.OriginLng); 
                @params.Add(ApiUrlConstV2.Radius, searchParams.Radius);
            }
            //Location
            else if (searchParams.WebApiSearchType == WebApiSearchType.Map)
            {

                @params.Add(ApiUrlConstV2.MinLat, searchParams.MinLat);
                @params.Add(ApiUrlConstV2.MinLng, searchParams.MinLng);
                @params.Add(ApiUrlConstV2.MaxLat, searchParams.MaxLat);
                @params.Add(ApiUrlConstV2.MaxLng, searchParams.MaxLng);
            }

            //Paging and Sorting
            if (searchParams.PageSize > 0)
                @params.Add(ApiUrlConstV2.PageSize, searchParams.PageSize);

            if (searchParams.PageNumber > 0)
                @params.Add(ApiUrlConstV2.Page, searchParams.PageNumber);

            if (searchParams.SortBy == SortBy.Random)
            {
                if (!string.IsNullOrWhiteSpace(searchParams.City))
                    @params.Add(ApiUrlConstV2.SortBy, SortBy.City);
                else if (!string.IsNullOrWhiteSpace(searchParams.County))
                    @params.Add(ApiUrlConstV2.SortBy, SortBy.County);
                else if (!string.IsNullOrWhiteSpace(searchParams.PostalCode))
                    @params.Add(ApiUrlConstV2.SortBy, SortBy.PostalCode);
                else
                    @params.Add(ApiUrlConstV2.SortBy, SortBy.Random);


                if (!string.IsNullOrEmpty(searchParams.SortFirstBy) &&
                    !string.IsNullOrEmpty(searchParams.City) ||
                    !string.IsNullOrEmpty(searchParams.County) ||
                    !string.IsNullOrEmpty(searchParams.PostalCode))
                    @params.Add(ApiUrlConstV2.SortFirstBy, searchParams.SortFirstBy);
            }
            else
                @params.Add(ApiUrlConstV2.SortBy, searchParams.SortBy);

            if (searchParams.SortOrder && searchParams.SortBy != SortBy.Random)
                @params.Add(ApiUrlConstV2.SortOrder, 1);

            if (searchParams.ExcludeBasicListings)
                @params.Add(ApiUrlConstV2.ExcludeBasicListings, 1);

            if (searchParams.BuilderId > 0)
                @params.Add(ApiUrlConstV2.BuilderId, searchParams.BuilderId);

            //Home Attribs
            if (searchParams.Bathrooms > 0)
                @params.Add(ApiUrlConstV2.Bath, searchParams.Bathrooms);

            if (searchParams.Bedrooms > 0)
                @params.Add(ApiUrlConstV2.Bed, searchParams.Bedrooms);

            if (searchParams.Garages > 0)
                @params.Add(ApiUrlConstV2.Gar, searchParams.Garages);

            //******* BEGIN Amenities ************
            if (searchParams.Pool)
                @params.Add(ApiUrlConstV2.Pool, 1);
            if (searchParams.Green)
                @params.Add(ApiUrlConstV2.Green, 1);
            if (searchParams.NatureAreas)
                @params.Add(ApiUrlConstV2.Nature, 1);
            if (searchParams.GolfCourse)
                @params.Add(ApiUrlConstV2.Golf, 1);
            if (searchParams.Waterfront)
                @params.Add(ApiUrlConstV2.WaterFront, 1);
            if (searchParams.Parks)
                @params.Add(ApiUrlConstV2.Parks, 1);
            if (searchParams.Views)
                @params.Add(ApiUrlConstV2.Views, 1);
            if (searchParams.Sports)
                @params.Add(ApiUrlConstV2.Sports, 1);
            if (searchParams.Adult)
                @params.Add(ApiUrlConstV2.Adult, 1);
            if (searchParams.Gated)
                @params.Add(ApiUrlConstV2.Gated, 1);
            //******* END Amenities ************

            //******* BEGIN Price ************
            if (searchParams.PriceLow > 0)
                @params.Add(ApiUrlConstV2.PrLow, searchParams.PriceLow);

            if (searchParams.PriceHigh > 0)
                @params.Add(ApiUrlConstV2.PrHigh, searchParams.PriceHigh);

            //******* END Price ************
            
            if (!string.IsNullOrEmpty(searchParams.CommunityStatus))
                @params.Add(ApiUrlConstV2.CommStatus, searchParams.CommunityStatus);

            if (!string.IsNullOrEmpty(searchParams.SchoolDistrictIds))
                @params.Add(ApiUrlConstV2.DistrictIds, searchParams.SchoolDistrictIds);

            if (searchParams.HotDeals)
                @params.Add(ApiUrlConstV2.HotDeals, 1);

            if (searchParams.SingleFamily)
                @params.Add(ApiUrlConstV2.Sf, 1);

            if (searchParams.MultiFamily)
                @params.Add(ApiUrlConstV2.Mf, 1);

            if (searchParams.Stories > 0)
                @params.Add(ApiUrlConstV2.Story, searchParams.Stories);

            if (searchParams.MasterBedLocation > 0)
                @params.Add(ApiUrlConstV2.MasterLoc, searchParams.MasterBedLocation);

            if (searchParams.CommId > 0)
                @params.Add(ApiUrlConstV2.CommId, searchParams.CommId);

            if (searchParams.PlanId > 0)
                @params.Add(ApiUrlConstV2.PlanId, searchParams.PlanId);

            if (searchParams.SpecId > 0)
                @params.Add(ApiUrlConstV2.SpecId, searchParams.SpecId);

            if (searchParams.BrandId > 0)
                @params.Add(ApiUrlConstV2.BrandId, searchParams.BrandId);

            if (searchParams.Qmi)
                @params.Add(ApiUrlConstV2.Qmi, 1);

            if (!string.IsNullOrEmpty(searchParams.CommName))
                @params.Add(ApiUrlConstV2.CommName, searchParams.CommName);

            if (searchParams.NoBoyl)
                @params.Add(ApiUrlConstV2.NoBoyl, 1);

            if (searchParams.Cache)
                @params.Add(ApiUrlConstV2.Cache, 1);

            if (searchParams.LastCached)
                @params.Add(ApiUrlConstV2.LastCached, 1);

            if (searchParams.LivingAreas > 0)
                @params.Add(ApiUrlConstV2.Living, searchParams.LivingAreas);

            if (!string.IsNullOrEmpty(searchParams.State))
                @params.Add(ApiUrlConstV2.State, searchParams.State);

            if (searchParams.ExcludeBasiCommunities)
                @params.Add(ApiUrlConstV2.ExcludeBasiCommunities, 1);

            if (searchParams.Builders != null && searchParams.Builders.Any(p => p > 0))
                @params.Add(ApiUrlConstV2.Builders, string.Join(",", searchParams.Builders.Where(p => p > 0)));
            //Generic 
            if (searchParams.CountsOnly)
                @params.Add(ApiUrlConstV2.CountsOnly, 1);
            return @params;
        }

        public static void ResetSearchParameters(this SearchParams paramz, IList<RouteParam> parameters)
        {
        }

        public static void SetFromRouteParam(this SearchParams paramz, IList<RouteParam> parameters)
        {
            foreach (var urlParam in parameters)
            {
                try
                {
                    switch (urlParam.Name)
                    {
                        case RouteParams.Adult:
                            paramz.Adult = Boolean.Parse(urlParam.Value);
                            break;
                        case RouteParams.Area:
                            paramz.MarketName = HttpUtility.UrlDecode(urlParam.Value).ToTitleCase();
                            break;
                        case RouteParams.BedRooms:
                            paramz.Bedrooms = Int32.Parse(urlParam.Value);
                            break;
                        case RouteParams.BathRooms:
                            paramz.Bathrooms = Int32.Parse(urlParam.Value);
                            break;
                        case RouteParams.BrandId:
                            paramz.BrandId = Int32.Parse(urlParam.Value);
                            break;
                        case RouteParams.BrandName:
                            paramz.BrandName = HttpUtility.UrlDecode(urlParam.Value).ToTitleCase();
                            break;
                        case RouteParams.BuilderId:
                            paramz.BuilderId = Int32.Parse(urlParam.Value);
                            break;
                        case RouteParams.BuilderName:
                            paramz.BuilderName = HttpUtility.UrlDecode(urlParam.Value).ToTitleCase();
                            break;
                        case RouteParams.City:
                        case RouteParams.CityNameFilter:
                        case RouteParams.Ciudad:
                            int zipCode;
                            if (int.TryParse(urlParam.Value.ToLower().Replace("postalcode", string.Empty).Trim(), out zipCode))
                                paramz.PostalCode = zipCode.ToString();
                            else
                            {
                                if (!urlParam.Value.StartsWith("refer")) //44797
                                {
                                    paramz.City = HttpUtility.UrlDecode(urlParam.Value).ToTitleCase();
                                    paramz.City = paramz.City.ReturnSpaceAndDash();
                                        
                                }
                                else
                                    UserSession.Refer = RouteParams.City.Value<string>().Replace("refer", "").Trim();
                            }
                            break;
                        case RouteParams.ComingSoon:
                            if (Boolean.Parse(urlParam.Value))
                            {
                                paramz.ComingSoon = true;
                            }
                            break;
                        case RouteParams.CommunityId:
                            paramz.CommId = Int32.Parse(urlParam.Value);
                            break;
                        case RouteParams.CommunityName:
                            paramz.CommName = HttpUtility.UrlDecode(urlParam.Value);
                            break;
                        case RouteParams.CommunityStatus:
                            paramz.CommunityStatus = urlParam.Value;
                            break;
                        case RouteParams.County:
                        case RouteParams.Condado:
                        case RouteParams.CountyNameFilter:
                            paramz.County = HttpUtility.UrlDecode(urlParam.Value).ToTitleCase();
                            paramz.County = paramz.County.ReturnSpaceAndDash();
                               
                            break;
                        //case UrlConst.HasEvents: paramz.HasEvent = true; break;
                        case RouteParams.HomeStatus:
                            paramz.HomeStatus = urlParam.Value;
                            break;
                        case RouteParams.TownHomes:
                            if (urlParam.Value.ToType<bool>())
                            {
                                paramz.MultiFamily = true;
                                paramz.SingleFamily = false;
                            }
                            break;
                        case RouteParams.HomeType:
                            if (urlParam.Value == "mf")
                            {
                                paramz.MultiFamily = true;
                                paramz.SingleFamily = false;
                            }
                            else if (urlParam.Value == "sf")
                            {
                                paramz.MultiFamily = false;
                                paramz.SingleFamily = true;
                            }
                            break;
                        case RouteParams.HotDeals:
                            paramz.HotDeals = Boolean.Parse(urlParam.Value);
                            break;
                        case RouteParams.Inventory:
                        case RouteParams.SpecHomes:
                            paramz.Qmi = Boolean.Parse(urlParam.Value);
                            break;
                        case RouteParams.Gated:
                            paramz.Gated = Boolean.Parse(urlParam.Value);
                            break;
                        case RouteParams.GolfCourse:
                            paramz.GolfCourse = Boolean.Parse(urlParam.Value);
                            break;
                        case RouteParams.GreenProgram:
                            paramz.Green = Boolean.Parse(urlParam.Value);
                            break;
                        //case RouteParams.ListingType: 
                        //    paramz.ListingType = urlParam.Value; break;
                        case RouteParams.Market:
                        case RouteParams.MarketId:
                            paramz.MarketId = Int32.Parse(urlParam.Value);
                            break;
                        case RouteParams.MasterBedLocation:
                            paramz.MasterBedLocation = urlParam.Value.ToType<int>();
                            break;
                        case RouteParams.MaxLat:
                            paramz.MaxLat = Double.Parse(urlParam.Value);
                            break;
                        case RouteParams.MaxLng:
                            paramz.MaxLng = Double.Parse(urlParam.Value);
                            break;
                        case RouteParams.MinLat:
                            paramz.MinLat = Double.Parse(urlParam.Value);
                            break;
                        case RouteParams.MinLng:
                            paramz.MinLng = Double.Parse(urlParam.Value);
                            break;
                        case RouteParams.NatureAreas:
                            paramz.NatureAreas = Boolean.Parse(urlParam.Value);
                            break;
                        case RouteParams.NumOfBaths:
                            paramz.Bathrooms = Int32.Parse(urlParam.Value);
                            break;
                        case RouteParams.NumOfGarages:
                            paramz.Garages = Int32.Parse(urlParam.Value);
                            break;
                        case RouteParams.NumOfLiving:
                            paramz.LivingAreas = Int32.Parse(urlParam.Value);
                            break;
                        case RouteParams.OriginLat:
                            paramz.OriginLat = Double.Parse(urlParam.Value);
                            break;
                        case RouteParams.OriginLong:
                            paramz.OriginLng = Double.Parse(urlParam.Value);
                            break;
                        case RouteParams.Page:
                            paramz.PageNumber = urlParam.Value.ToType<int>() > 0 ? urlParam.Value.ToType<int>() : 1;
                            break;
                        case RouteParams.Parks:
                            paramz.Parks = Boolean.Parse(urlParam.Value);
                            break;
                        case RouteParams.Pool:
                            paramz.Pool = Boolean.Parse(urlParam.Value);
                            break;
                        case RouteParams.PostalCode:
                        case RouteParams.PostalCodeFilter:
                            paramz.PostalCode = urlParam.Value;
                            break;
                        case RouteParams.PriceHigh:
                            paramz.PriceHigh = Int32.Parse(urlParam.Value);
                            break;
                        case RouteParams.PriceLow:
                            paramz.PriceLow = Int32.Parse(urlParam.Value);
                            break;
                        case RouteParams.PromotionType:
                            //paramz.AgentPromo = (urlParam.Value == PromotionType.Both.ToString().ToLower() ||
                            //                     urlParam.Value == PromotionType.Agent.ToString().ToLower());
                            //paramz.ConsumerPromo = (urlParam.Value == PromotionType.Both.ToString().ToLower() ||
                            //                     urlParam.Value == PromotionType.Consumer.ToString().ToLower());
                            break;
                        case RouteParams.SchoolDistrictId:
                            paramz.SchoolDistrictIds = urlParam.Value;
                            break;
                        case RouteParams.SportsFacilities:
                            paramz.Sports = Boolean.Parse(urlParam.Value);
                            break;
                        case RouteParams.SqFtLow:
                            paramz.SqFtLow = Int32.Parse(urlParam.Value);
                            break;
                        case RouteParams.State:
                            paramz.State = HttpUtility.UrlDecode(urlParam.Value);
                            break;
                        case RouteParams.StateName:
                            paramz.StateName = HttpUtility.UrlDecode(urlParam.Value).ToTitleCase();
                            break;
                        case RouteParams.Stories:
                            paramz.Stories = Int32.Parse(urlParam.Value);
                            break;
                        case RouteParams.Views:
                            paramz.Views = Boolean.Parse(urlParam.Value);
                            paramz.Waterfront = Boolean.Parse(urlParam.Value);
                            break;
                        case RouteParams.WaterFront:
                            paramz.Waterfront = Boolean.Parse(urlParam.Value);
                            break;
                    }
                }
                catch
                {
                    // Error trying to assign value to search parameter.  Ignore error and use parameter default value. 
                }
            }
        }
    }
}
