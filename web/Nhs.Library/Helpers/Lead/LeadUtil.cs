using System;
using System.Web;
using BHI.EnterpriseLibrary.Core.ExceptionHandling;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Library.Web;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Utility.Constants;

namespace Nhs.Library.Common
{
    public static class LeadUtil
    {
        public static string GetPriceRangeId(string leadType, string profileRegPrice)
        {
            string priceRangeId = string.Empty;
            //for builder leads retrieve price range id based on price
            if (leadType == LeadType.Builder && profileRegPrice.Length > 0)
                DataProvider.Current.GetPriceRangeId(ref priceRangeId, Convert.ToInt32(profileRegPrice));
            return priceRangeId;
        }   

        public static void AddToPlannerFromLeads(Profile profile, string leadType, string communityList, int builderId, string planList, string specList, string sourceRequestKey)
        {
            string[] communityIds;
            string[] planIds;
            string[] specIds;

            //Update planner with communities and plans
            switch (leadType)
            {
                case LeadType.Community:
                    //Save each community if more than one
                    if (communityList.Contains(","))
                    {
                        communityIds = communityList.Split(",".ToCharArray());
                        foreach (string communityId in communityIds)
                        {
                            int communityID = int.Parse(communityId);
                            profile.Planner.AddSavedCommunity(communityID, builderId);
                            profile.Planner.UpdateCommunityRequestDate(communityID, builderId, sourceRequestKey);
                        }
                    }
                    else //Only one community in list
                    {
                        profile.Planner.AddSavedCommunity(int.Parse(communityList), builderId);
                        profile.Planner.UpdateCommunityRequestDate(int.Parse(communityList), builderId, sourceRequestKey);
                    }
                    break;
                case LeadType.Home:
                    //if plan exists add to planner
                    if (planList.Length > 0)
                    {
                        if (planList.Contains(","))
                        {
                            planIds = planList.Split(",".ToCharArray());
                            foreach (string planId in planIds)
                            {
                                int planID = int.Parse(planId);
                                profile.Planner.AddSavedHome(planID, false);
                                profile.Planner.UpdateHomeRequestDate(planID, false, sourceRequestKey);
                            }
                        }
                        else //Only one plan in list
                        {
                            int planID = int.Parse(planList);
                            profile.Planner.AddSavedHome(planID, false);
                            profile.Planner.UpdateHomeRequestDate(planID, false, sourceRequestKey);
                        }
                    }

                    //if spec exists add to planner
                    if (specList.Length > 0)
                    {
                        if (specList.Contains(","))
                        {
                            specIds = specList.Split(",".ToCharArray());
                            foreach (string specId in specIds)
                            {
                                int specID = int.Parse(specId);
                                profile.Planner.AddSavedHome(specID, true);
                                profile.Planner.UpdateHomeRequestDate(specID, true, sourceRequestKey);
                            }
                        }
                        else //Only one spec in list
                        {
                            int specID = int.Parse(specList);
                            profile.Planner.AddSavedHome(specID, true);
                            profile.Planner.UpdateHomeRequestDate(specID, true, sourceRequestKey);
                        }
                    }
                    break;
            }
        }

 
        public static string GetSubmitButtonClass(string leadAction)
        {
            string cssClass = "btn btnGetMyBrochure";

            switch (leadAction)
            {
                case LeadAction.ContactBuilder:
                case LeadAction.CaContactBuilder:
                    cssClass = "btn btnContactBuilder";
                    break;
                case LeadAction.RequestApointment:
                case LeadAction.CaRequestApointment:
                    cssClass = "btn btnRequestAppt";
                    break;
                case LeadAction.RequestPromotion:
                case LeadAction.CaRequestPromotion:
                    cssClass = "btn btnFindDeals";
                    break;
                case LeadAction.FreeBrochure:
                case LeadAction.CaFreeBrochure:
                    cssClass = "btn btnGetMyBrochure";
                    break;
                case LeadAction.QuickMoveIn:
                case LeadAction.CaQuickMoveIn:
                    cssClass = "btn btnQuickMoveIn";
                    break;
                case LeadAction.HotHome:
                case LeadAction.CaHotHome:
                    cssClass = "btn btnHotHomeInfo";
                    break;
            }

            return cssClass;
        }

        
        public static string GenerateLead(LeadInfo leadInfo)
        {
            string commentsAppendedText = "";
            string requestCode = string.Empty;
            string leadXml = string.Empty;
            string fromEmail = "XGlobals.Partner.FromEmail";
            Lead lead = new Lead(leadInfo.LeadType);

            if (string.IsNullOrWhiteSpace(leadInfo.RequestUniqueKey))
            {
                leadInfo.RequestUniqueKey = leadInfo.SourceCommunityId > 0
                    ? string.Format("{0}-c{1}-", Guid.NewGuid(), leadInfo.SourceCommunityId)
                    : string.Format("{0}-", Guid.NewGuid());
                leadInfo.RequestUniqueKey += leadInfo.SourcePlanId == 0 ? string.Empty : string.Format("p-{0}", leadInfo.SourcePlanId);
                leadInfo.RequestUniqueKey += leadInfo.SourceSpecId == 0 ? string.Empty : string.Format("s-{0}", leadInfo.SourceSpecId);

                if (leadInfo.LeadAction == LeadAction.FreeBrochure)
                {
                    leadInfo.RequestUniqueKey = !string.IsNullOrWhiteSpace(leadInfo.CommunityList)
                   ? string.Format("{0}-c{1}-", Guid.NewGuid(), leadInfo.CommunityList.Split(',')[0])
                   : string.Format("{0}-", Guid.NewGuid());
                    leadInfo.RequestUniqueKey += string.IsNullOrWhiteSpace(leadInfo.PlanList) ? string.Empty : string.Format("p-{0}", leadInfo.PlanList.Split(',')[0]);
                    leadInfo.RequestUniqueKey += string.IsNullOrWhiteSpace(leadInfo.SpecList) ? string.Empty : string.Format("s-{0}", leadInfo.SpecList.Split(',')[0]);
                }
            }

            //Removed because the ticket 57058
            //leadInfo.ReferrerUrl = UserSession.ReferrerUrl;
            //leadInfo.DestinationUrl = UserSession.DestinationUrl;

            //Add user info
            lead.AddUserInfo(leadInfo.LeadUserInfo);

            switch (leadInfo.LeadAction)
            {
                case LeadAction.AgentCompensation:
                    commentsAppendedText += "Agent compensation question "; 
                    break;

                case LeadAction.FreeBrochure:
                case LeadAction.CaFreeBrochure:
                case LeadAction.HovFreeBrochure:
                case LeadAction.SearchAlert:
                    commentsAppendedText += "Brochure";
                    break;

                case LeadAction.RequestApointment:
                case LeadAction.CaRequestApointment:
                case LeadAction.HovRequestApointment:
                    commentsAppendedText += "Appointment";
                    break;

                case LeadAction.ContactBuilder:
                case LeadAction.CaContactBuilder:
                    commentsAppendedText += NhsRoute.BrandPartnerId == PartnersConst.Pro.ToType<int>()
                                                ? "Inquiry"
                                                : "Contact Builder";
                    break;

                case LeadAction.RequestPromotion:
                case LeadAction.CaRequestPromotion:
                case LeadAction.HovRequestPromotion:
                    commentsAppendedText += "Events and Promotions";
                    break;

                case LeadAction.RequestInfo:
                case LeadAction.CaRequestInfo:
                    commentsAppendedText += "Request Info";
                    break;

                case LeadAction.QuickMoveIn:
                case LeadAction.CaQuickMoveIn:
                case LeadAction.HovQuickMoveIn:
                    commentsAppendedText += "Quick Move-in";
                    break;

                case LeadAction.HotHome:
                    commentsAppendedText += "Hot Home";
                    break;
                case LeadAction.QuickConnect:
                    commentsAppendedText += "QuickConnect";
                    break;
            }

            if (commentsAppendedText != "") commentsAppendedText = "Lead Type: " + commentsAppendedText;
            if (commentsAppendedText != "" && leadInfo.LeadComments != "") commentsAppendedText += " - ";

            //Add User preferences
            // Change is made for Realtor
            if (!string.IsNullOrEmpty(leadInfo.RealtorInfo))
            {
                lead.AddUserPrefs(leadInfo.PrefPriceRange, leadInfo.MarketId, leadInfo.Prefcity, leadInfo.BldrImportance,
                              leadInfo.MoveInDate, leadInfo.FinancePref, leadInfo.Reason, commentsAppendedText + leadInfo.LeadComments, leadInfo.RealtorInfo);
            }
            else
            {
                lead.AddUserPrefs(leadInfo.PrefPriceRange, leadInfo.MarketId, leadInfo.Prefcity, leadInfo.BldrImportance,
                              leadInfo.MoveInDate, leadInfo.FinancePref, leadInfo.Reason, commentsAppendedText + leadInfo.LeadComments);
            }

            //Add site info
            lead.AddSiteInfo(fromEmail, NhsRoute.BrandPartnerId, NhsRoute.PartnerId, NhsRoute.PartnerSiteUrl, leadInfo.Referer, leadInfo.FromPage, leadInfo.LeadAction);
            if (!string.IsNullOrEmpty(leadInfo.RequestUniqueKey))
                lead.AddInfo("RequestUniqueKey", leadInfo.RequestUniqueKey);

            lead.AddInfo("SuppressEmail", leadInfo.SuppressEmail.ToString());

            if (leadInfo.ShowMatchingCommunities)
                lead.AddInfo("MachingCommunities", leadInfo.ShowMatchingCommunities.ToString());

            if (NhsRoute.BrandPartnerId == PartnersConst.Pro.ToType<Int32>())
            {
                lead.AddInfo("AgentName", leadInfo.AgentName);
                lead.AddInfo("AgentMail", leadInfo.AgentMail);
                lead.AddInfo("IsBilled", leadInfo.IsBilled.ToString());
            }

            lead.AddInfo("UseBrochureVer6", Configuration.UseBrochureVer6.ToString());
            //Removed because the ticket 57058
            //if (!string.IsNullOrEmpty(leadInfo.ReferrerUrl))
            //    lead.AddInfo("ReferrerUrl", leadInfo.ReferrerUrl);

            //if (!string.IsNullOrEmpty(leadInfo.DestinationUrl))
            //    lead.AddInfo("DestinationUrl", leadInfo.DestinationUrl);



            if (leadInfo.SourceCommunityId > 0)
                lead.AddInfo("SourceCommunityId", leadInfo.SourceCommunityId.ToString());

            if (leadInfo.SourcePlanId > 0)
                lead.AddInfo("SourcePlanId", leadInfo.SourcePlanId.ToString());

            if (leadInfo.SourceSpecId > 0)
                lead.AddInfo("SourceSpecId", leadInfo.SourceSpecId.ToString());

            if (leadInfo.RecoCount > 0)
                lead.AddInfo("RecoCount", leadInfo.RecoCount.ToString());

            if (leadInfo.IsActiveAdult != null)
                lead.AddInfo("includeActiveAdult", leadInfo.IsActiveAdult.ToString());

            if (leadInfo.IsBoyol != null)
                lead.AddInfo("includeBOYOL", leadInfo.IsBoyol.ToString());

            if (leadInfo.LeadUserInfo.SecondPhone != null)
                lead.AddInfo("SecondPhone", leadInfo.LeadUserInfo.SecondPhone);

            if (leadInfo.LeadUserInfo.SecondPhoneExt != null)
                lead.AddInfo("SecondPhoneExt", leadInfo.LeadUserInfo.SecondPhoneExt);

            if (!string.IsNullOrEmpty(leadInfo.ClientsInfo))
                lead.ClientsInfo = leadInfo.ClientsInfo;

            if (UserSession.GoogleUtmz.HaveValues)
                lead.UtmCookieParams = UserSession.GoogleUtmz.ToXml();

            switch (leadInfo.LeadType)
            {
                case LeadType.Builder:
                    lead.AddBuilders(leadInfo.BuilderList, ',');
                    break;
                case LeadType.Community:
                case LeadType.Rcm:
                    if (leadInfo.BuilderId != 0)
                    {
                        string[] communityIds = leadInfo.CommunityList.Split(',');
                        foreach (string cid in communityIds)
                        {
                            lead.AddBuilderCommunity(leadInfo.BuilderId, Convert.ToInt32(cid));
                        }
                    }
                    else
                    {
                        if (leadInfo.LeadAction == LeadAction.QuickConnect)
                            lead.AddBuilderCommunities(leadInfo.CommunityList, ',');
                        else
                            lead.AddCommunities(leadInfo.CommunityList, ',');
                    }
                    break;
                case LeadType.Home:
                    if (leadInfo.PlanList.Length != 0)
                        lead.AddPlans(leadInfo.PlanList, ',');
                    if (leadInfo.SpecList.Length != 0)
                        lead.AddSpecs(leadInfo.SpecList, ',');
                    break;
                case LeadType.Market:
                    if (leadInfo.CommunityList.Length > 0)
                        lead.AddBuilderCommunities(leadInfo.CommunityList, ',');
                    lead.AddInfo("alertid", leadInfo.AlertId);
                    break;
            }

            try
            {
                requestCode = lead.SendtoQ(Configuration.MSMQLeads);
                UserSession.HasSendLead = true;
            }
            catch (Exception ex)
            {
                try
                {
                    //Get Lead XML
                    leadXml = lead.GetXml();
                }
                catch (Exception ex3)
                {
                    leadXml = "Could not retrieve lead xml";
                    ErrorLogger.LogError(ex3.Message, "ex3", "ex3");
                }

                //Log Exception
                Exception e = new Exception("Error sending lead to queue;leadtype=" + leadInfo.LeadType + ";leadxml=" + leadXml);
                e.Data.Add("LeadType", leadInfo.LeadType);
                e.Data.Add("LeadXML", leadXml);
                e.Data.Add("Exception", ex);
                try
                {
                    ExceptionPolicy.HandleException(e, ExceptionPolicies.LeadPolicy);
                }
                catch (Exception ex2)
                {
                    ErrorLogger.LogError(ex2.Message, "ex2", "ex2");
                }
                MailHelper.Send(Configuration.WebOpsPagerEmail, "consumerweb@newhomesource.com", "MSMQ-Error Posting to Queue (" + Configuration.MSMQLeads + ")", "Lead type '" + leadInfo.LeadType + "' failed posting to " + Configuration.MSMQLeads);
                ErrorLogger.LogError(ex.Message, "ex", "ex");
            }

            if (requestCode.Length > 0)
                HttpContext.Current.Response.AppendToLog("leadtype=" + leadInfo.LeadType + ";leadid=" + requestCode);
            return commentsAppendedText;
        }

        public static int GetListingId(int planOrSpecId, bool isSpec)
        {
            return DataProvider.Current.GetListingId(planOrSpecId, isSpec);
        }

        //public static ILocation GetListing(string planList, string specList)
        //{
        //    ILocation listing = null;

        //    if (!(string.IsNullOrEmpty(specList) || specList.Contains(",")))
        //        listing =
        //            XListingFactory.CreateListing(GetListingId(CommonUtils.ConvertToInt32(specList), true));
        //    else if (!(string.IsNullOrEmpty(planList) || planList.Contains(",")))
        //        listing =
        //            XListingFactory.CreateListing(GetListingId(CommonUtils.ConvertToInt32(planList), false));

        //    return listing;
        //}

        //public static ILocation GetCommunity(string communityList, int builderId)
        //{
        //    ILocation community = null;

        //    if (!(string.IsNullOrEmpty(communityList) || communityList.Contains(",")))
        //        community =
        //            XCommunityFactory.CreateCommunity(CommonUtils.ConvertToInt32(communityList), XGlobals.Partner.PartnerId, builderId);

        //    return community;
        //}

        public static LeadInfo GetLeadInfoFromSession()
        {
            var lead = new LeadInfo();
            if (UserSession.UserProfile.UserID != null)
            { lead.LeadUserInfo.Guid = UserSession.UserProfile.UserID.Trim(); }

            var cryptEmail = CookieManager.EncEmail;
            if (string.IsNullOrWhiteSpace(cryptEmail) == false)
            {
                lead.LeadUserInfo.Email = CryptoHelper.Decrypt(cryptEmail, CookieConst.EncEmail);
                if (string.IsNullOrWhiteSpace(lead.LeadUserInfo.Email))
                {
                    if (UserSession.UserProfile.Email != null)
                    {
                        lead.LeadUserInfo.Email = UserSession.UserProfile.Email.Trim();
                    }
                }
            }
            else
            {
                if (UserSession.UserProfile.Email != null)
                {
                    lead.LeadUserInfo.Email = UserSession.UserProfile.Email.Trim();
                }
            }

            if (UserSession.UserProfile.LastName != null)
            { lead.LeadUserInfo.LastName = UserSession.UserProfile.LastName.Trim(); }
            if (UserSession.UserProfile.FirstName != null)
            { lead.LeadUserInfo.FirstName = UserSession.UserProfile.FirstName.Trim(); }
            if (UserSession.UserProfile.Address1 != null)
            { lead.LeadUserInfo.Address = UserSession.UserProfile.Address1.Trim(); }
            if (UserSession.UserProfile.City != null)
            { lead.LeadUserInfo.City = UserSession.UserProfile.City.Trim(); }
            if (UserSession.UserProfile.State != null)
            { lead.LeadUserInfo.State = UserSession.UserProfile.State.Trim(); }
            if (UserSession.UserProfile.PostalCode != null)
            { lead.LeadUserInfo.PostalCode = UserSession.UserProfile.PostalCode.Trim(); }
            if (UserSession.UserProfile.DayPhone != null)
            { lead.LeadUserInfo.Phone = UserSession.UserProfile.DayPhone.Trim(); }
            if (UserSession.UserProfile.DayPhoneExt != null)
            { lead.LeadUserInfo.PhoneExt = UserSession.UserProfile.DayPhoneExt.Trim(); }
            if (UserSession.UserProfile.RegState != null)
            { lead.Prefstate = UserSession.UserProfile.RegState.Trim(); }
            if (!string.IsNullOrEmpty(UserSession.UserProfile.RegMetro))
            { lead.MarketId = Convert.ToInt32(UserSession.UserProfile.RegMetro.Trim()); }
            if (UserSession.UserProfile.RegCity != null)
            { lead.Prefcity = UserSession.UserProfile.RegCity.Trim(); }
            if (UserSession.UserProfile.RegPrice != null)
            { lead.PrefPriceRange = UserSession.UserProfile.RegPrice.Trim(); }

            return lead;
        }

    }
}
