using System;
using System.Collections;
using System.Web;
using System.Xml;
using Nhs.Mvc.Routing.Interface;

namespace Nhs.Library.Common
{
    public static class MarketOptInManager
    {
        private static string _testerXml;

        public static string TesterXml
        {
            get { return _testerXml; }
            set { _testerXml = value; }
        }

        public static Hashtable OptInTable
        {
            get
            {
                // Declare vars
                Hashtable optInTable;

                // Cached?
                if (HttpRuntime.Cache["OptInTable"] != null)
                {
                    optInTable = (Hashtable)HttpRuntime.Cache["OptInTable"];
                }
                else
                {
                    // Create Hashtable
                    optInTable = new Hashtable();

                    // Parse MarketOptIn.xml into optInTable
                    optInTable = ParseXmlToHashtable(optInTable);

                    // Add Hashtable to Cache
                    if(optInTable!=null)
                    HttpRuntime.Cache.Insert("OptInTable", optInTable);

                }

                // Return Hashtable
                return optInTable;
            }
        }

        /// <summary>
        /// Verify if the test name contains in the specified market
        /// </summary>
        public static bool Contains(int marketID, string test)
        {
            //If there is no MarketOptIn.xml for this partner, just return false
            if (OptInTable == null)
                return false;

            // Does Market exist?
            if (!OptInTable.Contains(marketID.ToString()))
            {
                // No match found
                return false;
            }
            else
            {
                // Get list of Tests for Market
                string tests = (string)OptInTable[marketID.ToString()];

                // Find Test
                return tests.Contains(test);
            }
        }


        /// <summary>
        /// Parse market opt-in xml into a hashtable 
        /// </summary>
        private static Hashtable ParseXmlToHashtable(Hashtable optInTable)
        {
            string marketId;
            string partnerId;
            string testName;

            XmlDocument xmlDocMarketOptIn = new XmlDocument();

            if (String.IsNullOrEmpty(_testerXml))
            {
                string path = HttpContext.Current.Server.MapPath(Configuration.MarketOptInFile);
                if (!String.IsNullOrEmpty(path))
                {
                    try
                    {
                        xmlDocMarketOptIn.Load(path);
                    }
                    catch
                    {
                        return null;
                    }
                    
                }
            }
            else
            {
                xmlDocMarketOptIn.LoadXml(_testerXml);
            }

            XmlNodeList marketsNodeList = xmlDocMarketOptIn.GetElementsByTagName("Market");

            foreach (XmlNode marketNode in marketsNodeList)
            {
                marketId = "";
                partnerId = "";
                testName = "";

                marketId = marketNode.Attributes["ID"].Value;
                partnerId = marketNode.Attributes["PARTNER"].Value;

                XmlNodeList testNodesList = marketNode.ChildNodes;

                int i = 1;

                foreach (XmlNode testNode in testNodesList)
                {
                    testName += testNode.Attributes["Name"].Value;

                    //if there is more test nodes
                    if (i != testNodesList.Count)
                        testName += ",";

                    i = i + 1;
                }

                //if data exists
                if (!marketId.Equals("") && !testName.Equals("") && !partnerId.Equals(""))
                {
                    // only add those key/values that correspond to current partner
                    if (partnerId == NhsRoute.PartnerId.ToString())
                        optInTable.Add(marketId, testName);
                }
            }

            return optInTable;

        }

    }
}
