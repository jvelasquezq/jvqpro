﻿using System.Collections.Generic;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Helpers.Utility;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Common;

namespace Nhs.Library.Helpers.Lead
{
    public static class ViewBrochureHelper
    {
        public static string ViewBrochureLink(string firstName, string requestItemId, string email, string extraParams)
        {
            var brochureUrl = new List<RouteParam>
            {
                new RouteParam(RouteParams.FirstName, firstName, RouteParamType.QueryString, true, false),
                new RouteParam(RouteParams.RequestItemId, CryptoHelper.Encrypt(requestItemId), RouteParamType.QueryString, true, true),
                new RouteParam(RouteParams.Email, email, RouteParamType.QueryString, true, true)
            };

          return Configuration.NewHomeSourceDomain + brochureUrl.ToUrl(Pages.BrochureGen) + extraParams.Replace("?", "&");
        }
    }
}
