﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;

namespace Nhs.Library.Helpers
{
    public static class TemplateWordingHelper
    {
        private const string Home =
            "This prospect was matched to your community based on their request for homes similar to: a {0} home in the {1}, with {2} bedrooms, {3} baths. {4}";

        private const string Community =
            "This prospect was matched to your community based on their request for homes similar to: a {0} community from the {1} to {2}, with {3} bedrooms, {4} baths.";

        private const string NoOriginalLead =
            "This prospect was matched to your community based on their request for recommendations meeting the following criteria: {0}{1}; and a price range from {2} to {3}. {4}";

        
        public static string GetMessageHome(Community comm, IPlan plan, ISpec spec = null)
        {
            bool isPlan = spec == null;
            var planName = isPlan ? "plan" : "spec";
            var price = FromOrToPrice((isPlan ? plan.Price : spec.Price));
            var bedroomRange = ((isPlan ? plan.Bedrooms : spec.Bedrooms) > 0)
                                   ? (isPlan ? plan.Bedrooms : spec.Bedrooms).ToString()
                                   : "0";
            var bathroomRange = ((isPlan ? plan.Bathrooms : spec.Bathrooms) > 0)
                                    ? ListingHelper.ComputeBathrooms((isPlan ? plan.Bathrooms : spec.Bathrooms) ?? 0,
                                                                     (isPlan ? plan.HalfBaths : spec.HalfBaths) ?? 0)
                                    : "0";
            
            var homeType = "";

            if ((comm.IsCondo.HasValue  && comm.IsCondo == true) || (comm.IsAdult != null && comm.IsAdult == true) || comm.IsTownHome)
                homeType = string.Format("The community is also of the type: {0}.", comm.IsCondo == true || comm.IsTownHome ? "condo/townhome" : "adult/retirement");
            return string.Format(Home, planName, price, bedroomRange, bathroomRange, homeType);
        }

        public static string GetMessageCommunity(Community comm)
        {
            var homeType = string.Empty;

            if ((comm.IsCondo != null && comm.IsCondo == true) || (comm.IsAdult != null && comm.IsAdult == true))
                homeType = comm.IsCondo == true ? "condo/townhome " : "adult/retirement ";

            var priceLow = FromOrToPrice(comm.PriceLow);
            var priceHigh = FromOrToPrice(comm.PriceHigh);
            var bedroomRange = comm.MinBedroom + "-" + comm.MaxBedroom;
            var bathroomRange = comm.MinBath + "-" + comm.MaxBath;

            return string.Format(Community, homeType, priceLow, priceHigh, bedroomRange, bathroomRange);
        }

        public static string GetNoOriginalLead(string marketName, string city, string postalCode, int? radius, int priceLow, int priceHigh,
            int? numOfBeds, string districtName, string builderName, bool singleFamilyHomes, bool condoTownhomeHomes,
            bool pool, bool golfCourse, bool gatedCommunity)
        {
            var marketcity = marketName + (!string.IsNullOrEmpty(city) ? "/" + city : "");

            var zipRadius = !string.IsNullOrEmpty(postalCode)
                                ? string.Format(" {0} and a {1} mile radius", postalCode, radius)
                                : string.Empty;
            var minPrice = priceLow == 0 ? "Any" : priceLow.ToString("$###,####");
            var maxPrice = priceHigh == 0 ? "Any" : priceHigh.ToString("$###,####");

            var data = new List<string>();
            var amenity = new List<string>();
            if (numOfBeds > 0)
                data.Add(numOfBeds + "bedrooms");

            if (singleFamilyHomes)
                data.Add("Single Family home type");

            if (condoTownhomeHomes)
                data.Add("Condo/Townhome home type");
            
            if (pool)
                amenity.Add("Pool");

            if (golfCourse)
                amenity.Add("Golf course");

            if (gatedCommunity)
                amenity.Add("Gated community");

            if (amenity.Any())
                data.Add(string.Join(", ", amenity) + " amenity");

            if (!string.IsNullOrEmpty(districtName))
                data.Add(districtName);

            if (!string.IsNullOrEmpty(builderName))
                data.Add("and builder of " + builderName);

            var moreData = string.Empty;
            for (var i = 0; i < data.Count; i++)
                moreData += string.Format("{0}{1}; ", i == data.Count - 1 ? "and " : string.Empty, data[i]);

            return string.Format(NoOriginalLead, marketcity, zipRadius, minPrice, maxPrice,
                                 moreData.Length > 0 ?"Also specified: " + moreData.Trim().TrimEnd(';') + "." : string.Empty);
        }

        private static string FromOrToPrice(decimal price)
        {
            price = Math.Round(price/1000);

            if (price >= 50 && price <= 99)
                return "less than $100s";
            if (price >= 100 && price <= 135)
                return "low $100's";
            if (price >= 136 && price <= 170)
                return "mid $100's";
            if (price >= 171 && price <= 200)
                return "high $100's";
            if (price >= 201 && price <= 230)
                return "low $200's";
            if (price >= 231 && price <= 270)
                return "mid $200's";
            if (price >= 271 && price <= 300)
                return "high $200's";
            if (price >= 301 && price <= 335)
                return "low $300's";
            if (price >= 336 && price <= 370)
                return "mid $300's";
            if (price >= 371 && price <= 400)
                return "high $300's";
            if (price >= 401 && price <= 435)
                return "low $400's";
            if (price >= 436 && price <= 470)
                return "mid $400's";
            if (price >= 471 && price <= 500)
                return "high $400's";
            if (price >= 501 && price <= 535)
                return "low $500's";
            if (price >= 536 && price <= 570)
                return "mid $500's";
            if (price >= 571 && price <= 600)
                return "high $500's";
            if (price >= 601 && price <= 700)
                return "$600's";
            if (price >= 701 && price <= 800)
                return "$700's";
            if (price >= 801 && price <= 900)
                return "$800's";
            if (price >= 901 && price <= 1000)
                return "$900's";
            if (price >= 1001)
                return "$1 million+";
            return price == 0 ? "not indicated" : string.Empty;
        }

        public static string LookUpDistance(double from)
        {
            if (from >= 0 && from <= 2.9)
                return "within 3";
            if (from >= 3 && from <= 5.4)
                return "3-5";
            if (from >= 5.5 && from <= 10.5)
                return "5-10";
            if (from >= 10.6 && from <= 15)
                return "10-15";
            return @from >= 15.5 ? "15-25" : string.Empty;
        }
    }
}