﻿using System;
using System.IO;
using System.Net;
using System.Xml;
using System.Web;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Nhs.Library.Helpers.Vimeo
{
    public class VimeoAPI
    {


        /// <summary>
        /// API URL format
        /// </summary>						
        protected const string VimeoBaseUrlFormat = "https://vimeo.com/api/v2/video/{0}.{1}";
        protected const string VimeoBaseUrlFormatChannel = "https://vimeo.com/api/v2/channel/{0}/videos.{1}";
        
        /// <summary>
        /// The output formats supported by the simple API.
        /// </summary>
        public enum OutputFormatType
        {
            JSON,
            XML
        }

        /// <summary>
        /// Executes an HTTP GET command and retrives the information.		
        /// </summary>
        /// <param name="url">The URL to perform the GET operation</param>
        /// <returns>The response of the request, or null if we got 404 or nothing.</returns>
        protected static string ExecuteGetCommand(string url)
        {
            using (WebClient wc = new WebClient())
            {

                try
                {
                    using (Stream stream = wc.OpenRead(url))
                    {
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            return reader.ReadToEnd();
                        }
                    }
                }
                catch (WebException ex)
                {
                    //
                    // Handle HTTP 404 errors gracefully and return a null string to indicate there is no content.
                    //
                    if (ex.Response is HttpWebResponse)
                    {
                        if ((ex.Response as HttpWebResponse).StatusCode == HttpStatusCode.NotFound)
                        {
                            return null;
                        }
                    }

                    throw ex;
                }
            }
            return null;
        }


        private static XmlDocument GetVideoXml(string videoId)
        {
            string url = string.Format(VimeoBaseUrlFormat, videoId, OutputFormatType.XML.ToString().ToLower());
            var xmlStr = ExecuteGetCommand(url);
            var xmlVideo = new XmlDocument();
            xmlVideo.LoadXml(xmlStr);

            return xmlVideo;
        }

        private static XmlDocument GetChannelXml(string channnelId)
        {
            string url = string.Format(VimeoBaseUrlFormatChannel, channnelId, OutputFormatType.XML.ToString().ToLower());
            var xmlStr = ExecuteGetCommand(url);
            var xmlVideo = new XmlDocument();
            xmlVideo.LoadXml(xmlStr);

            return xmlVideo;
        }

        /// <summary>
        /// Gets the video using video id.
        /// </summary>
        /// <param name="videoId">The video id.</param>
        /// <returns>VimeoVideo object</returns>
        public static VimeoVideo GetVideobyId(string videoId)
        {
            var xmldoc = GetVideoXml(videoId);

            var serializer = new XmlSerializer(typeof(VimeoVideoWrapper));

            var stringReader = new StringReader(xmldoc.OuterXml);
            var xmlReader = new XmlTextReader(stringReader);

            var video = (VimeoVideoWrapper)serializer.Deserialize(xmlReader);

            if (video.Videos == null || video.Videos.Length == 0)
                return null;

            return video.Videos[0];
        }

        public static IList<VimeoVideo> GetVideoListByChannelId(string channelId)
        {
            var xmldoc = GetChannelXml(channelId);

            var serializer = new XmlSerializer(typeof(VimeoVideoWrapper));

            var stringReader = new StringReader(xmldoc.OuterXml);
            var xmlReader = new XmlTextReader(stringReader);

            var video = (VimeoVideoWrapper)serializer.Deserialize(xmlReader);

            return video.Videos;
        }

    }

    [SerializableAttribute]
    [XmlRootAttribute(Namespace = "", IsNullable = false, ElementName = "videos")]
    public class VimeoVideoWrapper
    {
        [XmlElement("video")]
        public VimeoVideo[] Videos { get; set; }
    }

    [SerializableAttribute]
    public class VimeoVideo
    {
        public string id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string url { get; set; }
        public string upload_date { get; set; }
        public string thumbnail_small { get; set; }
        public string thumbnail_medium { get; set; }
        public string thumbnail_large { get; set; }
        public string user_name { get; set; }
        public string user_url { get; set; }
        public string user_portrait_small { get; set; }
        public string user_portrait_medium { get; set; }
        public string user_portrait_large { get; set; }
        public string user_portrait_huge { get; set; }
        public string stats_number_of_likes { get; set; }
        public string stats_number_of_plays { get; set; }
        public string stats_number_of_comments { get; set; }
        public string duration { get; set; }
        public string width { get; set; }
        public string height { get; set; }
        public string tags { get; set; }
    }
}