﻿using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;

namespace Nhs.Library.Helpers.Content
{
    public static class PartnerLayoutHelper
    {
        public static PartnerLayoutConfig GetPartnerLayoutConfig()
        {

            var partnerLayoutConfig = new PartnerLayoutConfig();
            partnerLayoutConfig.IsPartnerRealtor = IsPartnerARealtor();
            partnerLayoutConfig.DisplayFullWidthGalleryinDetails = NhsRoute.IsBrandPartnerNhsPro;
            partnerLayoutConfig.IsBrandPartnerNhsPro = NhsRoute.IsBrandPartnerNhsPro;
            partnerLayoutConfig.PartnerIsABrand = NhsRoute.PartnerId == NhsRoute.BrandPartnerId;
            partnerLayoutConfig.IsPartnerNhs = NhsRoute.IsPartnerNhsPro;
            partnerLayoutConfig.AmIOnDetailPage = AmIOnDetailPage(NhsRoute.CurrentRoute.Function);
            partnerLayoutConfig.AmIOnNewResultsPage = AmIOnNewResultsPage();
            partnerLayoutConfig.AmIOnCommunityDetailPage = AmIOnCommunityDetailPage();
            partnerLayoutConfig.AmIOnBasicDetailPage = AmIOnBasicDetailPage(NhsRoute.CurrentRoute.Function);
            partnerLayoutConfig.AmIOnHomeDetailPage = AmIOnHomeDetailPage();
            partnerLayoutConfig.AmIOnBasicCommunityDetailPage = AmIOnBasicCommunity(NhsRoute.CurrentRoute.Function);
            partnerLayoutConfig.AmIOnCommunityResultsPage = AmIOnCommunityResultsPage();
            partnerLayoutConfig.AmIOnNhsTv = AmIOnNhsTv();
            partnerLayoutConfig.AmIOnHomeResultsPage = AmIOnHomeResultsPage();
            partnerLayoutConfig.AmIOnCommDetailv1 = NhsRoute.CurrentRoute.Function.ToLower() == Pages.CommunityDetailv1;
            partnerLayoutConfig.AmIOnBuilderShowcase = NhsRoute.CurrentRoute.Function.ToLower() == Pages.Builder;
            partnerLayoutConfig.AmIOnHomePage = AmIOnHomePage();
            partnerLayoutConfig.AmIOnSiteHelpPages = AmIOnSiteHelpPages();
            partnerLayoutConfig.AmIOnMortgageCalculator = AmIOnMortgageCalculator();
            partnerLayoutConfig.PartnerAllowsModals = DoesPartnerAllowModals();
            partnerLayoutConfig.PartnerAllowsCrossMarketingLinks = DoesPartnerAllowCrossMarketingLinks();
            partnerLayoutConfig.AmIOnDrivingDirectionsPage = AmIOnDrivingDirectionsPage();
            partnerLayoutConfig.DoesPartnerHaveHotDealsLink = DoesPartnerHaveHotDealsLink;
            partnerLayoutConfig.DoesPartnerHaveStateDropDown = DoesPartnerHaveStateDropDown;
            partnerLayoutConfig.DoesPartnerHaveMarketDropDown = DoesPartnerHaveMarketDropDown;
            partnerLayoutConfig.DoesPartnerAllowsSeoLinkPromotions = DoesPartnerAllowSeoPromotionLinks();
            partnerLayoutConfig.DoesPartnerHaveSquareFeetFacet = DoesPartnerHaveSquareFeetFacet();
            partnerLayoutConfig.DoesPartnerHaveCommunityStatusFacet = DoesPartnerHaveCommunityStatusFacet();
            partnerLayoutConfig.DoesPartnerHavePromotionsFacet = DoesPartnerHavePromotionsFacet();
            partnerLayoutConfig.Use2014Styles = Use2014Styles();

            //Keep this assignment at the end since there is dependency on the above properties
            partnerLayoutConfig.IncludeBrightcove = (partnerLayoutConfig.AmIOnCommunityDetailPage ||
                                                     partnerLayoutConfig.AmIOnHomeDetailPage)
                                                || (partnerLayoutConfig.AmIOnCommunityResultsPage &&
                                                     partnerLayoutConfig.IsBrandPartnerNhsPro);

            partnerLayoutConfig.ShowRightAdColumn = partnerLayoutConfig.AmIOnCommunityDetailPage ||
                                                    partnerLayoutConfig.AmIOnSiteHelpPages || partnerLayoutConfig.AmIOnBasicDetailPage;
            partnerLayoutConfig.ShowTopAd = (AmIOnCommunityResultsPage() || AmIOnHomeResultsPage() || AmIOnHomePage() || 
                                                AmIOnNhsTv() || AmIOnNewSiteIndex()) == false;
            
            return partnerLayoutConfig;

        }

        private static bool Use2014Styles()
        {
            var amIOnNewResultsPage = AmIOnNewResultsPage();
            var amIOnHomePage = AmIOnHomePage();
            var amIOnNhsTv = AmIOnNhsTv();
            var amIOnNewSiteIndex = AmIOnNewSiteIndex();
            var amIOnEbookPage = NhsRoute.CurrentRoute.Function.ToLower() == Pages.eBook;
            var amIOnCmsPage = AmIOnCmsPage();
            var amIOnSearchPage = NhsRoute.CurrentRoute.Function.ToLower() == Pages.GreenSearch ||
                                NhsRoute.CurrentRoute.Function.ToLower() == Pages.HotDealsSearch ||
                                NhsRoute.CurrentRoute.Function.ToLower() == Pages.BOYLSearch ||
                                NhsRoute.CurrentRoute.Function.ToLower() == Pages.LocationHandler;
            var amIOnOwnerStoriesPage = NhsRoute.CurrentRoute.Function.ToLower() == Pages.OwnerStories || NhsRoute.CurrentRoute.Function.ToLower() == Pages.OwnerStoriesCuration;
            var amIOnCommunityDetailv2 = NhsRoute.CurrentRoute.Function.ToLower() == Pages.CommunityDetailv2;
            var amIOnHomeDetailv2 = NhsRoute.CurrentRoute.Function.ToLower() == Pages.HomeDetailv2;
            var amIOnSyntheticGeo = NhsRoute.CurrentRoute.Function.ToLower() == Pages.SyntheticGeographies;
            bool use2014Styles;

            if (NhsRoute.IsBrandPartnerNhsPro)
            {
                use2014Styles = amIOnSearchPage || amIOnNhsTv || amIOnNewSiteIndex;
            }
            else
            {
                use2014Styles = amIOnHomePage  || amIOnNewResultsPage || amIOnNhsTv || amIOnEbookPage || 
                                amIOnSearchPage || amIOnOwnerStoriesPage ||
                                amIOnNewSiteIndex || amIOnCmsPage || amIOnCommunityDetailv2 || amIOnHomeDetailv2 || amIOnSyntheticGeo;
            }

            return use2014Styles;
        }


        private static bool AmIOnHomePage()
        {
            return (NhsRoute.CurrentRoute.Function.ToLower() == Pages.Home && NhsRoute.CurrentRoute.Name != "HomeDetailSpecMove") ||
                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.HomeV2 ||
                   NhsRoute.CurrentRoute.Function.ToLower() == "";
        }

        private static bool AmIOnBasicDetailPage(string function)
        {
            return function.ToLower() == Pages.BasicDetail;
        }

        public static bool AmIOnBasicCommunity(string function)
        {
            return function.ToLower() == Pages.BasicCommunity;
        }

        public static bool AmIOnDetailPage(string function)
        {
            return function.ToLower() == Pages.CommunityDetail ||
                   function.ToLower() == Pages.BasicCommunity ||
                   function.ToLower() == Pages.CommunityDetailv1 ||
                   function.ToLower() == Pages.CommunityDetailPreview ||
                   function.ToLower() == Pages.HomeDetail ||
                   function.ToLower() == Pages.HomeDetailPreview ||
                   function.ToLower() == Pages.BasicDetail ||
                   function.ToLower() == Pages.PlanDetailMove ||
                   function.ToLower() == Pages.SpecDetailMove ||
                   function.ToLower() == Pages.CommunityDetailMove ||
                   function.ToLower() == "homedetailv1" ||
                   function.ToLower() == "homedetailv2" ||
                   function.ToLower() == "homedetailv3" ||
                   function.ToLower() == "homedetailv4";
        }

        private static bool AmIOnNewResultsPage()
        {
            var page = AmIOnHomeResultsPage() || AmIOnCommunityResultsPage();
            return page;
        }

        private static bool AmIOnHomeDetailPage()
        {
            return NhsRoute.CurrentRoute.Function.ToLower() == Pages.HomeDetail ||
                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.HomeDetailPreview ||
                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.PlanDetailMove ||
                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.SpecDetailMove ||
                   NhsRoute.CurrentRoute.Function.ToLower() == "homedetailv1" ||
                   NhsRoute.CurrentRoute.Function.ToLower() == "homedetailv2" ||
                   NhsRoute.CurrentRoute.Function.ToLower() == "homedetailv3" ||
                   NhsRoute.CurrentRoute.Function.ToLower() == "homedetailv4";
        }

        private static bool AmIOnCommunityDetailPage()
        {
            return NhsRoute.CurrentRoute.Function.ToLower() == Pages.BasicCommunity ||
                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.CommunityDetail ||
                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.CommunityDetailv1 ||
                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.CommunityDetailv2 ||
                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.CommunityDetailPreview ||
                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.CommunityDetailMove ||
                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.Comunidad;
        }

        private static bool AmIOnCommunityResultsPage()
        {
            return NhsRoute.CurrentRoute.Function.ToLower() == Pages.CommunityResults || NhsRoute.CurrentRoute.Function.ToLower() == Pages.Comunidades;
        }

        private static bool AmIOnNhsTv()
        {
            return NhsRoute.CurrentRoute.Function.ToLower() == Pages.NewHomeSourceTv;
        }

        private static bool AmIOnHomeResultsPage()
        {
            return NhsRoute.CurrentRoute.Function.ToLower() == Pages.HomeResults || NhsRoute.CurrentRoute.Function.ToLower() == Pages.Casas;
        }
        
        private static bool AmIOnMortgageCalculator()
        {
            return NhsRoute.CurrentRoute.Function.ToLower() == Pages.MortgageCalculator;
        }

        private static bool AmIOnSiteHelpPages()
        {
            return NhsRoute.CurrentRoute.Function.ToLower() == Pages.AboutUs.ToLower() ||
                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.ContactUs.ToLower() ||
                   //NhsRoute.CurrentRoute.Function.ToLower() == Pages.HomeGuideCategory ||
                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.SupportedBrowsers.ToLower() ||
                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.TermsOfUse.ToLower() ||
                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.PrivacyPolicy.ToLower() ||
                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.Partners.ToLower() ||
                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.SiteHelp.ToLower() ||
                   //NhsRoute.CurrentRoute.Function.ToLower() == Pages.LocationHandler.ToLower() ||
                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.SiteIndex.ToLower() ||
                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.NewHomeBuilders.ToLower() ||
                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.NewHomeBuildersNew.ToLower() ||
                   //NhsRoute.CurrentRoute.Function.ToLower() == Pages.BOYLSearch.ToLower() ||
                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.BOYLResults.ToLower() ||
                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.QuickMoveInSearch.ToLower() ||
                   //NhsRoute.CurrentRoute.Function.ToLower() == Pages.GreenSearch.ToLower() ||
                   //NhsRoute.CurrentRoute.Function.ToLower() == Pages.HotDealsSearch.ToLower() ||
                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.MortgageRates.ToLower() ||
                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.MortgageCalculator.ToLower();
        }

        private static bool AmIOnNewSiteIndex()
        {
            var amIOnNewSiteIndex = NhsRoute.CurrentRoute.Function.ToLower() == Pages.StateIndex ||
                                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.StateBuilder ||
                                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.AmenityIndexCondo ||
                                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.AmenityIndexGolf ||
                                   NhsRoute.CurrentRoute.Function.ToLower() == Pages.AmenityIndexWaterfront;
            return amIOnNewSiteIndex;
        }

        private static bool AmIOnCmsPage()
        {
            var amIOnCmsPage = NhsRoute.CurrentRoute.Function.ToLower() == Pages.Article ||
                                NhsRoute.CurrentRoute.Function.ToLower() == Pages.ArticleSearch ||
                                NhsRoute.CurrentRoute.Function.ToLower() == Pages.ResourceCenter ||
                                NhsRoute.CurrentRoute.Function.ToLower() == Pages.ResourceCenter101 ||
                                NhsRoute.CurrentRoute.Function.ToLower() == Pages.ResourceCenterSearch ||
                                NhsRoute.CurrentRoute.Function.ToLower() == Pages.NewHome101 ||
                                NhsRoute.CurrentRoute.Function.ToLower() == Pages.NewHome101Search ||
                                NhsRoute.CurrentRoute.Function.ToLower() == Pages.CentroDeRecursos ||
                                NhsRoute.CurrentRoute.Function.ToLower() == Pages.CentroDeRecursos101;
            return amIOnCmsPage;
        }
        
        private static bool DoesPartnerAllowModals()
        {
            return NhsRoute.PartnerType != PartnerType.Realtor;
        }

        private static bool DoesPartnerHaveHotDealsLink
        {
            get
            {
                if (NhsRoute.IsBrandPartnerNhsPro)
                    return false;
                switch (NhsRoute.PartnerSiteUrl.ToLowerInvariant())
                {
                    case "tampaparadeofhomes":
                        return false;
                    default:
                        return true;
                }
            }
        }

        private static bool DoesPartnerHaveStateDropDown
        {
            get
            {
                switch (NhsRoute.PartnerSiteUrl.ToLowerInvariant())
                {
                    case "hbaswil":
                        return false;
                    default:
                        return true;
                }
            }
        }

        private static bool DoesPartnerHaveMarketDropDown
        {
            get
            {
                switch (NhsRoute.PartnerSiteUrl.ToLowerInvariant())
                {
                    case "hbaswil":
                        return false;
                    default:
                        return true;
                }
            }
        }

        private static bool DoesPartnerAllowCrossMarketingLinks()
        {
            if (NhsRoute.IsBrandPartnerNhsPro)
                return false;
            return NhsRoute.PartnerSiteUrl.ToLower() != "tampaparadeofhomes";
        }

        private static bool DoesPartnerHaveSquareFeetFacet()
        {
            if (NhsRoute.IsBrandPartnerNhsPro)
                return true;
            return false;
        }

        private static bool DoesPartnerHaveCommunityStatusFacet()
        {
            if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
                return true;
            return false;
        }


        private static bool DoesPartnerAllowSeoPromotionLinks()
        {
            if (NhsRoute.IsBrandPartnerNhsPro)
                return false;
            return true;
        }

        private static bool DoesPartnerHavePromotionsFacet()
        {
            if (NhsRoute.IsBrandPartnerNhsPro)
                return true;
            return false;
        }

        private static bool AmIOnDrivingDirectionsPage()
        {
            return NhsRoute.CurrentRoute.Function.ToLower() == Pages.DrivingDirections;
        }

        private static bool IsPartnerARealtor()
        {
            return NhsRoute.PartnerType.ToLower() == "rlt";
        }

    }
}
