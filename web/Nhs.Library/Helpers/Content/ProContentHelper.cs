﻿using System;
using System.Collections.Generic;
using System.Xml.XPath;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Utility.Common;

namespace Nhs.Library.Helpers.Content
{
    public class ProContentHelper
    {
        private readonly string _configFileName = @"~\" + Configuration.StaticContentFolder + @"\SEOContent\Pro_Home\Default.xml";
        private readonly IPathMapper _pathMapper;

        public ProContentHelper(IPathMapper pathMapper)
        {
            _pathMapper = pathMapper;
        }

        public List<AgentQuote> GetAgentQuotes()
        {
            var cacheKey = "AgentQuotes";
            var agentQuotes = WebCacheHelper.GetObjectFromCache(cacheKey, false) as List<AgentQuote>;
            string path = _pathMapper.MapPath(_configFileName);

            if (agentQuotes == null)
            {
                agentQuotes = new List<AgentQuote>();
                try
                {
                    XPathDocument doc = new XPathDocument(path);
                    XPathNavigator nav = doc.CreateNavigator();

                    // Scripts
                    XPathNodeIterator pageItr = nav.Select("/content/agentquotes/agentquote");
                    while (pageItr.MoveNext())
                    {
                        var agentQuote = new AgentQuote();
                        agentQuote.Order = pageItr.Current.GetAttribute("order", string.Empty);
                        agentQuote.AgentName = pageItr.Current.GetAttribute("agentname", string.Empty);
                        agentQuote.AgentLocation = pageItr.Current.GetAttribute("agentlocation", string.Empty);
                        agentQuote.AgentPhotoUrl = pageItr.Current.GetAttribute("photourl", string.Empty);
                        agentQuote.QuoteText = pageItr.Current.Value.Trim();

                        if (!string.IsNullOrEmpty(agentQuote.AgentName) && !string.IsNullOrEmpty(agentQuote.QuoteText))
                            agentQuotes.Add(agentQuote);
                    }

                    WebCacheHelper.AddObjectToCache(agentQuotes, cacheKey, new TimeSpan(WebCacheConst.DefaultHours, 0, 0), false);
                }
                catch (Exception ex)
                {
                    ErrorLogger.LogError(ex);
                }
            }
            return agentQuotes;
        }

       
    }
}
