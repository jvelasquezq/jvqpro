﻿using System;
using System.Collections.Generic;
using System.Xml.XPath;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Utility.Common;

namespace Nhs.Library.Helpers.Seo
{
    public class DevelopersReader
    {
        private string configFileName = @"~\" + Configuration.StaticContentFolder + @"\DeveloperNames\developernames.xml";
        private readonly IPathMapper _pathMapper;

        public DevelopersReader(IPathMapper pathMapper)
        {
            _pathMapper = pathMapper;
        }

        public List<Developer> ParseScripts()
        {
            var cacheKey = "DeveloperNames";
            var developers = WebCacheHelper.GetObjectFromCache(cacheKey, false) as List<Developer>;

            string path = _pathMapper.MapPath(configFileName);

            if (developers == null)
            {
                developers = new List<Developer>();
                try
                {

                    XPathDocument doc = new XPathDocument(path);
                    XPathNavigator nav = doc.CreateNavigator();

                    // Scripts
                    XPathNodeIterator pageItr = nav.Select("/developernames/developer");
                    while (pageItr.MoveNext())
                    {
                        var developer = new Developer();
                        developer.Name = pageItr.Current.GetAttribute("name", String.Empty);
                        developer.City = pageItr.Current.GetAttribute("city", String.Empty);
                        developer.MarketName = pageItr.Current.GetAttribute("marketName", String.Empty);
                        developer.MarketId= pageItr.Current.GetAttribute("marketId", String.Empty);
                        developer.State = pageItr.Current.GetAttribute("state", String.Empty);

                        if (!string.IsNullOrEmpty(developer.Name) && !string.IsNullOrEmpty(developer.City) && !string.IsNullOrEmpty(developer.MarketName) && !string.IsNullOrEmpty(developer.State))
                            developers.Add(developer);
                    }

                    WebCacheHelper.AddObjectToCache(developers, cacheKey, new TimeSpan(WebCacheConst.DefaultHours, 0, 0), false);
                }
                catch (Exception ex)
                {
                    ErrorLogger.LogError(ex);
                }
            }

            return developers;
        }
    }
}
