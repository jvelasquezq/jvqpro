﻿using System;
using System.Collections.Generic;
using System.Xml.XPath;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Utility.Common;

namespace Nhs.Library.Helpers.Content
{
    public class AbbreviationsReader
    {
        private string configFileName = @"~\" + Configuration.StaticContentFolder + @"\TypeAhead\abbreviations.xml";
        private readonly IPathMapper _pathMapper;

        public AbbreviationsReader(IPathMapper pathMapper)
        {
            _pathMapper = pathMapper;
        }

        public Dictionary<string, string> ParseScripts()
        {
            var cacheKey = "Abbreviations";
            var abbrs = WebCacheHelper.GetObjectFromCache(cacheKey, false) as Dictionary<string, string>;

            string path = _pathMapper.MapPath(configFileName);

            if (abbrs == null)
            {
                abbrs = new Dictionary<string, string>();
                try
                {

                    XPathDocument doc = new XPathDocument(path);
                    XPathNavigator nav = doc.CreateNavigator();

                    // Scripts
                    XPathNodeIterator pageItr = nav.Select("/abbreviations/abbr");
                    while (pageItr.MoveNext())
                    {
                        var abbr = pageItr.Current.GetAttribute("abbreviation", String.Empty);
                        var name = pageItr.Current.GetAttribute("name", String.Empty);

                        if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(abbr) && !abbrs.ContainsKey(abbr))
                            abbrs.Add(abbr, name);
                    }

                    WebCacheHelper.AddObjectToCache(abbrs, cacheKey, new TimeSpan(WebCacheConst.DefaultHours, 0, 0), false);
                }
                catch (Exception ex)
                {
                    ErrorLogger.LogError(ex);
                }
            }

            return abbrs;
        }
    }
}
