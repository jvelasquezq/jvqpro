using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml;
using Nhs.Library.Constants;
using Nhs.Library.Proxy;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Domain.Model.CMS;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;

namespace Nhs.Library.Common
{
    public static class CmsReader
    {
        public static DataTable GetTopLevelCategories(int siteId)
        {
            string cacheKey = "TopLevelCategories" + siteId.ToString();
            DataTable topLevelCategories = WebCacheHelper.GetDataTableFromCache(cacheKey, false);
            if (topLevelCategories == null)
            {
                topLevelCategories = DataProvider.Current.GetTopLevelCategories(siteId);
                WebCacheHelper.AddDataTableToCache(topLevelCategories, cacheKey,
                                                   new TimeSpan(WebCacheConst.DefaultHours, 0, 0), false);
            }
            return topLevelCategories;
        }

        public static RcArticle ParseArticle(string xml, string contentTitle = "", string contentTeaser = "",
                                             int searchRank = 0)
        {
            if (string.IsNullOrEmpty(xml)) return null;

            var doc = new XmlDocument();
            doc.LoadXml(xml);
            var cRArticle = new RcArticle()
                {
                    ContentTitle = contentTitle,
                    Title = doc.GetNodeValue<string>("//ArticleTitle"),
                    Subtitle = doc.GetNodeValue<string>("//ArticleSubtitle"),
                    Subhead = doc.GetNodeValue<string>("//ArticleSubhead"),
                    Copy = doc.GetNodeValue<string>("//ArticleCopy").ProcessImage(),
                    FeaturedImage = doc.GetImage(),
                    FeaturedImageCaption = doc.GetNodeValue<string>("//FeaturedImageCaption"),
                    FeaturedImageAlign = doc.GetNodeValue<string>("//FeaturedImageAlign") ?? "Right",
                    Source = doc.GetNodeValue<string>("//ArticleSource"),
                    Author = doc.GetNodeValue<string>("//ArticleAuthor"),
                    Type = doc.GetNodeValue<string>("//ArticleType"),
                    RelatedArticles = doc.GetRelatedArticles(),
                    Promoted = doc.GetNodeValue<bool>("//Promoted"),
                    SequenceNumber = doc.GetNodeValue<int>("//SequenceNumber"),
                    Teaser = contentTeaser.Replace("<p>", "").Replace("</p>", ""),
                    SearchRank = searchRank
                };

            return cRArticle;
        }

        public static IList<RcContentInfo> ParseFolderArticleIds(string xml)
        {
            var doc = new XmlDocument();
            doc.LoadXml(xml);
            IList<RcContentInfo> articleInfo;
            var content = doc.SelectNodes("//Result//Content");
            if (content != null)
            {
                articleInfo = (from XmlNode cn in content 
                                    let xmlAttributeCollection = cn.Attributes 
                                    where xmlAttributeCollection != null 
                                    select new RcContentInfo
                                    { 
                                      Id = Convert.ToInt64(xmlAttributeCollection["ID"].Value),
                                      Title = xmlAttributeCollection["Title"].Value,
                                      ContentType = Convert.ToInt32(xmlAttributeCollection["ContentType"].Value),
                                      LanguageId = Convert.ToInt32(xmlAttributeCollection["Language"].Value)
                                    }).ToList();
            }
            else
            {
                articleInfo = new List<RcContentInfo>();
            }
            return articleInfo;
        }

        public static RcContentInfo ParseFolderArticleIdByArticleName(string xml, string articleName)
        {
            var info = ParseFolderArticleIds(xml);

            var contentInfo = new RcContentInfo();

            if (info.Any())
            {
                contentInfo = info.FirstOrDefault(content => content.Title == articleName);
            }

            return contentInfo;
        }

        public static RcGlobalBusinessConfig ParseEktronGlobalBusinessConfig(string xml)
        {
            var doc = new XmlDocument();
            doc.LoadXml(xml);            
            var config = new RcGlobalBusinessConfig
                {
                    NumberFeaturedArticlesToDisplay = doc.GetNodeValue<int>("//EktronConfigOptions//NFADCP")
                };
            return config;
        }

        public static RcFeaturedHomesSlideShow ParseFeaturedHomesSlideShow(string xml)
        {
            if (string.IsNullOrWhiteSpace(xml)) { return null; }

            var returnData = new RcFeaturedHomesSlideShow();

            var doc = new XmlDocument();
            doc.LoadXml(xml);

            returnData.SlideshowMainTitle = doc.GetNodeValue<string>("//ArticleTitle");            
            returnData.UseIntroductorySlide = doc.GetNodeValue<bool>("//UseIntroductorySlide");
            returnData.SlideInformationAreaText = doc.GetNodeValue<string>("//SlideInformationArea");
            returnData.ArticleType = doc.GetNodeValue<string>("//ArticleType");
            returnData.FeaturedImage = doc.GetNodeValue<string>("//FeaturedImage");

            var root = doc.SelectSingleNode("//root");
            if (root != null)
            {
                var articles = root.SelectNodes("RelatedArticles//RelatedArticle");

                if (articles != null)
                {
                    var articlesList = (from XmlNode article in articles select article.InnerXml.ToType<long>()).ToList();
                    returnData.RelatedArticles = articlesList;
                }
            }

            if (returnData.UseIntroductorySlide == true)
            {
                if (root != null)
                {
                    var selectSingleNode = root.SelectSingleNode("IntroductorySlideInfo//IntroductoryPicture");
                    if (selectSingleNode != null)
                        returnData.IntroductoryPicture = selectSingleNode.InnerXml.ProcessImage();
                }

                returnData.IntroductoryMainTitle = doc.GetNodeValue<string>("//IntroductorySlideInfo//MainTitle");
                returnData.IntroductoryTitle = doc.GetNodeValue<string>("//IntroductorySlideInfo//IntroductoryTitle");
                returnData.IntroductoryText = doc.GetNodeValue<string>("//IntroductorySlideInfo//IntroductoryText");
                returnData.IntroductoryAuthor = doc.GetNodeValue<string>("//IntroductorySlideInfo//ArticleAuthor");
            }

            if (root != null)
            {
                var slideImages = root.SelectNodes("FeatureHomes//FeaturedHomeSlide");

                if (slideImages != null)
                    foreach (XmlNode item in slideImages)
                    {
                        var node = new XmlDocument();
                        node.LoadXml("<root>" + item.InnerXml + "</root>");
                        var image = new RcFeaturedHomesSlideShowImage()
                        {
                            SequenceNumber = node.GetNodeValue<int>("SequenceNumber"),
                            PictureUrl = node.GetNodeValue<string>("Picture") != null
                            ? node.GetNodeValue<string>("Picture").ProcessImage() : string.Empty,
                            OverlayTitle = node.GetNodeValue<string>("OverlayTitle"),                            
                            OverlayAreaOne = node.GetNodeValue<string>("OverlayAreaOne"),
                            OverlayLinkText = node.GetNodeValue<string>("OverlayLink//a"),
                            OverlayLink = node.GetNodeValue<string>("OverlayLink//a//@href"),
                            DetailsColor = node.GetNodeValue<string>("FontColorDetail") ?? "#FFFFFF",
                            Title = node.GetNodeValue<string>("Title"),
                            SubTitle = node.GetNodeValue<string>("SubTitle"),
                            AuthorId = node.GetNodeValue<long>("SlideAuthor")
                        };

                        var xmlNodeList = node.SelectNodes("//NameValuePairList//NameValuePair");
                        if (xmlNodeList != null)
                        {
                            var rightCol = false;
                            foreach (XmlNode homeData in xmlNodeList)
                            {
                                var keyValue = new XmlDocument();
                                keyValue.LoadXml("<root>" + homeData.InnerXml + "</root>");

                                image.OverlayAreaTwoItems.Add(
                                    new OverlayAreaTwoItem()
                                        {
                                            Name = keyValue.GetNodeValue<string>("Name"),
                                            Value = keyValue.GetNodeValue<string>("Value"),
                                            RightCol = rightCol
                                        }
                                    );
                                rightCol = !rightCol;
                            }
                        }
                        returnData.Slides.Add(image);
                    }
            } // */

            return returnData;
        }

        public static RcArticle ParseArticle(this ContentBase contentBase)
        {
            var cRArticle = ParseArticle(contentBase.Html, contentBase.Title, contentBase.Teaser);
            return cRArticle;
        }

        public static RcArticle ParceArticleWithTitleAndTeaser(this DataTable dt)
        {
            RcArticle article = null;
            if (dt.Rows.Count > 0)
            {
                var xml = dt.Rows[0]["content_html"].ToType<string>();
                var title = dt.Rows[0]["content_title"].ToType<string>();
                var teaser = dt.Rows[0]["content_teaser"].ToType<string>();
                article = ParseArticle(xml, title, teaser);
            }

            return article ?? new RcArticle();
        }

        public static RcAuthorLandingPage ParseAuthorLandingPage(string xml)
        {
            if (string.IsNullOrWhiteSpace(xml)) return new RcAuthorLandingPage();

            var doc = new XmlDocument();
            doc.LoadXml(xml);

            var landing = new RcAuthorLandingPage()
                {
                    ImagePath = string.IsNullOrWhiteSpace(doc.GetNodeValue<string>("//FeaturedImage")) == false 
                    ? Configuration.EktronDomain + (doc.GetNodeValue<string>("//FeaturedImage")) : string.Empty,
                    SubTitle = doc.GetNodeValue<string>("//ArticleSubtitle"),
                    Title = doc.GetNodeValue<string>("//ArticleTitle")
                };


            return landing;
        }

        public static RcAuthor ParseAuthor(string xml, long authorId, string title)
        {
            if (string.IsNullOrEmpty(xml)) return new RcAuthor();

            var doc = new XmlDocument();
            doc.LoadXml(xml);
            var cRAuthor = new RcAuthor
                {
                    Title = title,
                    AuthorId = authorId,
                    Name = doc.GetNodeValue<string>("//AuthorName"),
                    Bio = doc.GetNodeValue<string>("//AuthorBio"),
                    Expertise = doc.GetNodeValue<string>("//Expertise"),
                    Active = (doc.GetNodeValue<string>("//Status") ?? "On") != "Off",
                    LongBio = doc.GetNodeValue<string>("//LongBio") ?? string.Empty,
                    Headline = doc.GetNodeValue<string>("//Headline") ?? string.Empty,
                    ImagePath = (doc.GetNodeValue<string>("//AuthorImage") ?? string.Empty).Replace(@"src=""",
                                                                                                    string.Format(
                                                                                                        @"src=""{0}",
                                                                                                        Configuration
                                                                                                            .EktronDomain))
                };

            var xmlNodeList = doc.SelectNodes("//SocialNetworks//Network");
            
            if (xmlNodeList != null)
            {                
                foreach (XmlNode homeData in xmlNodeList)
                {
                    var keyValue = new XmlDocument();
                    keyValue.LoadXml("<root>" + homeData.InnerXml + "</root>");

                    cRAuthor.SocialNetworks.Add(
                        new AuthorSocialNetwork()
                            {
                               NetworkName = keyValue.GetNodeValue<string>("NetworkName"),
                               Url = keyValue.GetNodeValue<string>("NetworkUrl"),
                               Rel = keyValue.GetNodeValue<string>("Rel")
                            }                        
                        );
                }
                
            }

            return cRAuthor;
        }

        public static IEnumerable<long> ParseArticleIds(string xml)
        {
            if (string.IsNullOrEmpty(xml)) return new List<long>();

            var articleIds = new List<long>();
            var doc = new XmlDocument();
            doc.LoadXml(xml);

            var root = doc.SelectSingleNode("//root");
            var articles = root.SelectNodes("Article");

            foreach (XmlNode article in articles)
            {
                articleIds.Add(article.InnerXml.ToType<long>());
            }
            return articleIds;
        }

        public static RCSlideShow ParseSlideShow(string xml)
        {
            if (string.IsNullOrEmpty(xml)) return new RCSlideShow();

            var slideShow = new RCSlideShow();
            var doc = new XmlDocument();
            doc.LoadXml(xml);

            var root = doc.SelectSingleNode("//root");
            var slideImages = root.SelectNodes("SlideImage");

            foreach (XmlNode slideImage in slideImages)
            {
                var image = new RCSlideImage()
                    {
                        Category = slideImage.SelectSingleNode("Category") != null
                                ? slideImage.SelectSingleNode("Category").InnerXml
                                : string.Empty,
                        
                        ImageDescription = slideImage.SelectSingleNode("ImageDescription").InnerXml,
                        
                        ImagePath = slideImage.SelectSingleNode("ImagePath").InnerXml.Replace(@"src=""",
                                                        string.Format(@"src=""{0}", Configuration.EktronDomain)),
                        
                        ImageTitle = slideImage.SelectSingleNode("ImageTitle").InnerXml,
                        
                        Link = slideImage.SelectSingleNode("Link") != null
                                ? slideImage.SelectSingleNode("Link").InnerXml
                                : string.Empty,
                        
                        ImageCaption = slideImage.SelectSingleNode("ImageCaption") != null
                                    ? slideImage.SelectSingleNode("ImageCaption").InnerXml
                                    : string.Empty,
                        
                        ImageSource = slideImage.SelectSingleNode("ImageSource") != null
                                    ? slideImage.SelectSingleNode("ImageSource").InnerXml
                                    : string.Empty,

                        ImageWidth = slideImage.SelectSingleNode("ImageWidth") != null
                                    ? slideImage.SelectSingleNode("ImageWidth").InnerXml
                                    : string.Empty,

                        ImageAlignment = slideImage.SelectSingleNode("ImageAlignment") != null
                                    ? slideImage.SelectSingleNode("ImageAlignment").InnerXml
                                    : string.Empty

                    };
                slideShow.Images.Add(image);
            }
            return slideShow;
        }

        private static T GetNodeValue<T>(this XmlDocument xdlead, string xpath)
        {
            try
            {
                XmlNode xmlnode = xdlead.DocumentElement;
                return xmlnode.SelectSingleNode(xpath).InnerXml.ToType<T>();
            }
            catch (Exception)
            {
                return default(T);
            }
        }

        private static IEnumerable<long> GetRelatedArticles(this XmlDocument xdlead)
        {
            try
            {
                XmlNode xmlnode = xdlead.DocumentElement;
                var RelatedArticle = xmlnode.SelectNodes("//RelatedArticle");
                return RelatedArticle != null
                           ? RelatedArticle.Cast<XmlNode>().Select(article => article.InnerText.ToType<long>()).ToList()
                           : new List<long>();
            }
            catch (Exception)
            {
                return new List<long>();
            }
        }

        private static string GetImage(this XmlDocument xdlead)
        {
            try
            {
                XmlNode xmlnode = xdlead.DocumentElement;
                var img = xmlnode.SelectSingleNode("//FeaturedImage");
                return img.InnerXml.ProcessImage();
            }
            catch (Exception)
            {
                return "";
            }
        }

        private static string ProcessImage(this string xml)
        {
            return string.IsNullOrEmpty(xml)
                       ? ""
                       : xml.Replace("src=\"/uploadedImages/",
                                     string.Format("src=\"{0}uploadedImages/", Configuration.EktronDomain));
        }
    }
}
