﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Utility.Common;
using System.Xml.Linq;

namespace Nhs.Library.Helpers.Content
{
    public class ContentHelper
    {
        

        public static List<BuyNewContentSection> GetBuyNewSections(SeoTemplateType seoTemplateType)
        {
            IPathMapper pathMapper = new ContextPathMapper();
            var sections = WebCacheHelper.GetObjectFromCache(WebCacheConst.BuyNewSectionsContentList, true) as List<BuyNewContentSection>;
            if (sections != null) return sections;
            var configFileName = string.Concat(@"~\", Configuration.StaticContentFolder, @"\SEOContent\");
           
            string path = string.Concat(configFileName, seoTemplateType, @"\DefaultV2.xml");
            string fullPath = pathMapper.MapPath(path);
            try
            {
                if (File.Exists(fullPath))
                {
                    XDocument xDoc = XDocument.Load(fullPath);
                    var randomSections = xDoc.Element("content").Element("buynewrandomsection");
                    if (randomSections != null)
                    {
                        var buyNewSections = randomSections.Elements().Select(xElement => new BuyNewContentSection {
                            Order = xElement.Attribute("order").Value.ToType<int>(), 
                            ImageUrl = xElement.Attribute("imageurl").Value.ToType<string>(), 
                            ImageText = xElement.Attribute("imagetext").Value.ToType<string>(),
                            // ReSharper disable PossibleNullReferenceException
                            Description = xElement.Element("description").Value.ToType<string>()
                            // ReSharper restore PossibleNullReferenceException
                        }).ToList();

                        WebCacheHelper.AddObjectToCache(buyNewSections, WebCacheConst.BuyNewSectionsContentList, new TimeSpan(WebCacheConst.DefaultHours, 0, 0), true);
                        return buyNewSections;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(new Exception(string.Format("Could not read SEO Content file {0} - Exception: {1}", fullPath, ex.Message)));
            }
            return null;
        }
    }
}
