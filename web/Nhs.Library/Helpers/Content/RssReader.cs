﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Nhs.Library.Business;
using Nhs.Utility.Common;

namespace Nhs.Library.Helpers.Content
{
    public static class RssReader
    {
        public static List<RssFeed> GetRssFeed(string rssUrl)
        {
            var feeds = new List<RssFeed>();
            try
            {
                XDocument feedXml = XDocument.Load(rssUrl);
                feeds = (from feed in feedXml.Descendants("item")
                             select new RssFeed
                             {
                                 Title = feed.Element("title").Value,
                                 Link = feed.Element("link").Value,
                                 Description = Regex.Match(feed.Element("description").Value, @"^.{1,300}\b(?<!\s)").Value
                             }).Take(5).ToList();

                
            }
            catch (Exception exception)
            {
                ErrorLogger.LogError(exception);
            }
            return feeds;
        }
    }
}
