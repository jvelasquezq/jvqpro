﻿namespace Nhs.Library.Proxy
{
    public class WebApiMethods
    {
        public const string Recommendations = "recommendations";
        public const string CommunityLocations = "communitylocations";
        public const string HomeLocations = "homelocations";
        public const string Communities = "communities";
        public const string Homes = "homes";
        public const string Builders = "builders";
    }
} 