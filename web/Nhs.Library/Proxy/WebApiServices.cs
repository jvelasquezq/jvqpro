﻿using System;
using System.Collections.Generic;
using System.Text;
using Nhs.Library.Common;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Helpers.Utility;
using Nhs.Utility.Common;
using Nhs.Utility.Web;

namespace Nhs.Library.Proxy
{
    public class WebApiServices
    {
        private readonly Dictionary<string, object> _parameters = new Dictionary<string, object>();

        public WebApiServices()
        {
            WebApiVersions = WebApiVersions.V2;
            _parameters.Add("sessiontoken", GenerateSessionToken());
            _parameters.Add("algorithm", Configuration.WebServicesApiHashingAlgorithm);
        }

        public WebApiServices(Dictionary<string, object> parameters): this()
        {
            foreach (var p in parameters)
                _parameters.Add(p.Key, p.Value);
        }

        public void AddParameter(string key, object value)
        {
            if (!_parameters.ContainsKey(key))
                _parameters.Add(key, value);
        }

        public T GetData<T>(string page) where T : new()
        {
            var newobj = new T();

            try
            {
                var url = Url(page);
                var jsonResult = HTTP.HttpGet(url);
                var model = jsonResult.ToFromJson<T>();

                if (model != null)
                    newobj = model;
            }
            catch (FormatException ex)
            {
                ErrorLogger.LogError(ex);
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }

            return newobj;
        }
        public string Url(string page)
        {
            var searchUrl = new StringBuilder();
            searchUrl.Append(Configuration.WebServicesApiUrl);
            searchUrl.AppendFormat("{0}/search/{1}?", WebApiVersions, page);
            foreach (var parameter in _parameters)
            {
                searchUrl.AppendFormat("{0}={1}&", parameter.Key, parameter.Value);
            }
            return searchUrl.ToString().TrimEnd('&');
        }

        private string GenerateSessionToken()
        {
            var token = WebApiSecurity.CreateToken(DateTime.UtcNow, Configuration.WebServicesApiHashingAlgorithm, PartnerSitePassword);
            return token;
        }

        public string PartnerSitePassword { get; set; }
        public WebApiVersions WebApiVersions { get; set; }
    }
}
