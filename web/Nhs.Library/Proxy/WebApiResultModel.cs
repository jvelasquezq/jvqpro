﻿using Bdx.Web.Api.Objects;

namespace Nhs.Library.Proxy
{
    public class WebApiCommonResultModel<T> : ApiCommonResultModel<T>
    {
        
    }

    public class WebApiResultModel<T> : ApiRecoResultModel<T>
    {
    }


    public class ApiRecoCommunityResultEx : ApiRecoCommunityResult
    {
        public string BrandImageThumbnail { get; set; }
    }
}