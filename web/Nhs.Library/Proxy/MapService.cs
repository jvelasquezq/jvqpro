using Nhs.Library.Common;

namespace Nhs.Library.Proxy
{
    /// <remarks/>
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "MapSoap", Namespace = "http://newhomesource.com/webservices")]
    public class MapService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {

        /// <remarks/>
        public MapService()
        {
            this.Url = Configuration.MapServiceUrl;
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://newhomesource.com/webservices/GetGeoCode", RequestNamespace = "http://newhomesource.com/webservices", ResponseNamespace = "http://newhomesource.com/webservices", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string GetGeoCode(string street, string city, string state, string postalCode)
        {
            object[] results = this.Invoke("GetGeoCode", new object[] {
                    street,
                    city,
                    state,
                    postalCode});
            return ((string)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetGeoCode(string street, string city, string state, string postalCode, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetGeoCode", new object[] {
                    street,
                    city,
                    state,
                    postalCode}, callback, asyncState);
        }

        /// <remarks/>
        public string EndGetGeoCode(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((string)(results[0]));
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://newhomesource.com/webservices/GetDrivingDirections", RequestNamespace = "http://newhomesource.com/webservices", ResponseNamespace = "http://newhomesource.com/webservices", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string GetDrivingDirections(System.Double startLat, System.Double startLng, System.Double endLat, System.Double endLng)
        {
            object[] results = this.Invoke("GetDrivingDirections", new object[] {
                    startLat,
                    startLng,
                    endLat,
                    endLng});
            return ((string)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetDrivingDirections(System.Double startLat, System.Double startLng, System.Double endLat, System.Double endLng, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetDrivingDirections", new object[] {
                    startLat,
                    startLng,
                    endLat,
                    endLng}, callback, asyncState);
        }

        /// <remarks/>
        public string EndGetDrivingDirections(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((string)(results[0]));
        }
    }
}