﻿/*
 * NewHomeSource - http://www.NewHomeSource.com
 * Copyright © 2001-2010 Builders Digital Experience, LLC.
 * ========================================================
 * Author: Praveen Guggarigoudar
 * Date: 07/28/2010
 * Edit History: 01/15/2011 - Praveen: Upgraded to MVC 3, 
 *                            Changed calls to WebFormView constructor
 *               05/01/2012 - Added support for Razor Views
 * =========================================================
 */
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Routing.Web
{
    public class NhsViewEngine : VirtualPathProviderViewEngine
    {
        public NhsViewEngine()
        {
            /* {0} = view name or master page name 
             * {1} = controller name      */

            //These are dummy locations that will be overridden below
            MasterLocationFormats = new[] {  
                                              "~/MasterPages/Mvc/{0}.master"  
                                          };

            ViewLocationFormats = new[] {  
                                            "~/Views/{1}/{0}.aspx",  
                                            "~/Views/Shared/{0}.aspx"
                                        };

            PartialViewLocationFormats = new[] {  
                                                   "~/PartialViews/{1}/{0}.ascx"               
                                               };
        }

        protected override IView CreatePartialView(ControllerContext controllerContext, string partialPath)
        {
            if (partialPath.Contains(".cshtml"))
            {
                return new RazorView(controllerContext, partialPath, null, false, base.FileExtensions);
            }

            return new WebFormView(controllerContext, partialPath, null);


        }

        protected override IView CreateView(ControllerContext controllerContext, string viewPath, string masterPath)
        {
            if (viewPath.Contains(".cshtml"))
            {
                return new RazorView(controllerContext, viewPath, masterPath, true, base.FileExtensions);
            }

            return new WebFormView(controllerContext, viewPath, masterPath);
        }

        /// <summary>
        /// Finds the specified view by using the specified controller context and master view name.
        /// </summary>
        /// <param name="controllerContext">The controller context.</param>
        /// <param name="viewName">The name of the view.</param>
        /// <param name="masterName">The name of the master view.</param> 
        /// <param name="useCache">true to use the cached view.</param>
        /// <returns>The page view.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="controllerContext"/> parameter is null (Nothing in Visual Basic).</exception>
        /// <exception cref="T:System.ArgumentException">The <paramref name="viewName"/> parameter is null or empty.</exception>
        public override ViewEngineResult FindView(ControllerContext controllerContext, string viewName, string masterName, bool useCache)
        {
            //SET ROOT VIEWS FOLDER
            var rootViewsFolder = NhsRoute.ShowMobileSite ? MvcConfig.MobileViewsFolderRoot : MvcConfig.ViewsFolderRoot;

            var viewPath = string.Empty;
            var controllerName = controllerContext.Controller.GetType().Name.Replace("Controller", string.Empty);
            var actionName = viewName;
            var mobileViewFound = false;

            if (viewName.StartsWith("~/"))
                viewPath = viewName;

            // Check for partial views that are actionable
            if (string.IsNullOrEmpty(viewPath))
            {
                if (controllerName == "PartialViews")
                {
                    viewPath = GetCurrentPartialViewPath(actionName, rootViewsFolder);

                    if (string.IsNullOrEmpty(viewPath)) //mobile view was not found. 
                        viewPath = GetCurrentPartialViewPath(actionName, MvcConfig.ViewsFolderRoot); //find desktop view
                    else
                        mobileViewFound = true;
                }
                else
                {
                    if (!string.IsNullOrEmpty(NhsRoute.CurrentRoute.ViewFolder)) //views folder defined at route level
                        controllerName = NhsRoute.CurrentRoute.ViewFolder;

                    viewPath = GetCurrentViewPath(controllerName, actionName, rootViewsFolder);

                    if (string.IsNullOrEmpty(viewPath)) //mobile view was not found. 
                        viewPath = GetCurrentViewPath(controllerName, actionName, MvcConfig.ViewsFolderRoot); //find desktop view
                    else
                        mobileViewFound = true;
                }
            }

            //figure master page path
            var masterPath = controllerName == "PartialViews" ? string.Empty : GetMasterPath(NhsRoute.CurrentRoute, viewPath, mobileViewFound);

            return base.FindView(controllerContext, viewPath, masterPath, useCache);
        }

        /// <summary>
        /// Gets the current master path.
        /// </summary>
        /// <param name="cr">The current route.</param>
        /// <param name="viewPath">The view path.</param>
        /// <param name="mobileViewFound"></param>
        /// <returns></returns>
        private string GetMasterPath(RouteElement cr, string viewPath, bool mobileViewFound)
        {
            //Mobile
            if (NhsRoute.ShowMobileSite && (MvcConfig.ForceMobile || cr.UseDesktopView.ToType<bool>() || mobileViewFound || viewPath.ToLower().Contains("viewsmobile")))
                return MvcConfig.MobileMasterDefault;

            //Desktop
            if (!string.IsNullOrEmpty(cr.MasterPage))
                return cr.MasterPage;

            if (viewPath.Contains(".cshtml") && !string.IsNullOrEmpty(MvcConfig.MasterPageDefault))
                return MvcConfig.MasterPageDefault.Insert(MvcConfig.MasterPageDefault.LastIndexOf('/') + 1, "_").ToLower().Replace(".master", ".cshtml");

            return MvcConfig.MasterPageDefault;
        }

        /// <summary>
        /// Gets the current view path.
        /// </summary>
        /// <param name="controllerName">Name of the controller.</param>
        /// <param name="actionName">Name of the action.</param>
        /// <param name="rootViewsFolder"></param>
        /// <returns></returns>
        private string GetCurrentViewPath(string controllerName, string actionName, string rootViewsFolder)
        {
            //Get Views folder for current route
            var view = LocateViewsFolder(controllerName, actionName, rootViewsFolder);
            if (string.IsNullOrEmpty(view)) //if there is no 'View' file associated with passed action, fallback on current route's default action.
            {
                actionName = NhsRoute.CurrentRoute.Controller.Action;
                view = LocateViewsFolder(controllerName, actionName, rootViewsFolder);
            }
            return view;
        }

        /// <summary>
        /// Gets the view path for an actionable partial view (.aspx).
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="rootViewsFolder"></param>
        /// <returns></returns>
        private string GetCurrentPartialViewPath(string viewName, string rootViewsFolder)
        {
            var partialViewName = viewName + ".aspx";

            //Get Partial Views folder for current route
            string partialViewPath = PathHelpers.GetFileInVirtualFolder(rootViewsFolder + MvcConfig.ViewsFolderPartial,
                                                       partialViewName, true, new ContextPathMapper());

            if (!string.IsNullOrEmpty(partialViewPath))
                return partialViewPath;

            partialViewName = viewName + ".cshtml";

            //Get Partial Views folder for current route
            partialViewPath = PathHelpers.GetFileInVirtualFolder(rootViewsFolder + MvcConfig.ViewsFolderPartial,
                                                       partialViewName, true, new ContextPathMapper());

            if (!string.IsNullOrEmpty(partialViewPath))
                return partialViewPath;

            return string.Empty;
        }

        /// <summary>
        ///     Gets the appropriate Views folder for a given View
        ///     e.g.  
        ///     YahooRealEstate/Communities/Display.aspx
        ///     Partner_GroupViewsFolder_1/Communities/Display.aspx
        ///     PartnerBrand_GroupViewsFolder_RLT_348/Communities/Display.aspx
        ///     PartnerBrand_GroupViewsFolder_333/Communities/Display.aspx
        ///     Default/Communities/Display.aspx
        /// </summary>
        /// <param name="controllerName">In the above example, Communities would be the controller</param>
        /// <param name="viewName">and Display.aspx the View name.</param>
        /// <param name="rootViewsFolder">Views Folder Root</param>
        /// <returns></returns>
        private static string LocateViewsFolder(string controllerName, string viewName, string rootViewsFolder)
        {
            var razorViewName = viewName.EndsWith(".cshtml") ? viewName : viewName + ".cshtml";
            var webformViewName = viewName.EndsWith(".aspx") ? viewName : viewName + ".aspx";
            var viewFolders = new List<string>();

            //NOTE: Order Matters!!
            if (!string.IsNullOrEmpty(NhsRoute.PartnerSiteUrl))
                viewFolders.Add(rootViewsFolder + NhsRoute.PartnerSiteUrl);

            //viewFolders.Add(string.Format("{0}_{1}", rootViewFolder + MvcConfig.ViewsFolderPartnerGroup, NhsRoute.PartnerId)); EXCLUDING THIS FOLDER UNTIL WE HAVE VIEWS SPECIFIC TO THIS TYPE
            viewFolders.Add(string.Format("{0}_{1}_{2}", rootViewsFolder + MvcConfig.ViewsFolderPartnerBrandGroup, NhsRoute.PartnerType, NhsRoute.PartnerId));
            viewFolders.Add(string.Format("{0}_{1}", rootViewsFolder + MvcConfig.ViewsFolderPartnerBrandGroup, NhsRoute.BrandPartnerId));
            viewFolders.Add(rootViewsFolder + MvcConfig.ViewsFolderPartnerBrandGroupShared);
            viewFolders.Add(rootViewsFolder + MvcConfig.ViewsFolderDefault);

            foreach (var viewFolder in viewFolders)
            {
                var view = PathHelpers.GetFileInVirtualFolder(viewFolder + "/" + controllerName, razorViewName, false, new ContextPathMapper());
                if (!string.IsNullOrEmpty(view))
                    return view;

                view = PathHelpers.GetFileInVirtualFolder(viewFolder + "/" + controllerName, webformViewName, false, new ContextPathMapper());

                if (!string.IsNullOrEmpty(view))
                    return view;

            }

            return string.Empty;
        }

        
    }
}
