﻿This project is a Route helper that provides the following functions-
- Assist the NHS UrlRewriterModule in identifying if a given route is MVC or not.
- Replace the Default MVC View Engine with a custom View Engine. 
	This engine helps load the right view for a given route (NHSViewEngine).
- Expose several Helper Properties via the NhsRoute class.
- Make use of various context items that get added for a request by the UrlRewriterModule. This behavior will have to be ported over once rewriter goes away.
NOTE:	This project will be referenced ONLY by Nhs.Web.UI and Nhs.UrlRewriter projects.