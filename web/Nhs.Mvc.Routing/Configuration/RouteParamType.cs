﻿namespace Nhs.Mvc.Routing.Configuration
{
    public enum RouteParamType
    {
        Friendly,
        FriendlyNameOnly,
        Hash,
        QueryString
    }
}
