﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Routing.Configuration
{
    [Serializable]
    public class RouteParam
    {
        public RouteParams Name { get; set; }
        public string Value { get; set; }
        public string PlaceHolder { get; set; }
        public RouteParamType ParamType { get; set; }
        public bool LowerCaseParamName { get; set; }
        public bool LowerCaseParamValue { get; set; }

        public RouteParam()
        {
            LowerCaseParamName = true;
            LowerCaseParamValue = true;
        }

        public RouteParam(RouteParams name, string value, RouteParamType paramType = RouteParamType.Friendly)
            : this()
        {
            Name = name;
            Value = value;
            ParamType = paramType;
        }

        public RouteParam(RouteParams name, string value, RouteParamType paramType, bool lowerCaseParamName, bool lowerCaseParamValue)
            : this(name, value, paramType)
        {
            LowerCaseParamName = lowerCaseParamName;
            LowerCaseParamValue = lowerCaseParamValue;
        }

        public RouteParam(RouteParams name, object value, RouteParamType paramType = RouteParamType.Friendly)
            : this()
        {
            Name = name;
            Value = value.ToType<string>();
            ParamType = paramType;
        }

        public RouteParam(RouteParams name, object value, RouteParamType paramType, bool lowerCaseParamName, bool lowerCaseParamValue)
            : this(name, value.ToString(), paramType)
        {
            LowerCaseParamName = lowerCaseParamName;
            LowerCaseParamValue = lowerCaseParamValue;
        }

        public RouteParam(RouteParams name, object value, RouteParamType paramType, bool lowerCaseParamName, bool lowerCaseParamValue, string placeHolder)
        {
            Name = name;
            Value = value.ToType<string>();
            ParamType = paramType;
            LowerCaseParamName = lowerCaseParamName;
            LowerCaseParamValue = lowerCaseParamValue;
            PlaceHolder = placeHolder;
        }
        public static bool operator ==(RouteParam x, RouteParams y)
        {
            if (x == null)
                return false;

            return x.Name == y;
        }

        public static bool operator !=(RouteParam x, RouteParams y)
        {
            if (x == null)
                return false;

            return x.Name != y;
        }

        public override bool Equals(object obj)
        {
            // ReSharper disable once BaseObjectEqualsIsObjectEquals
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            // ReSharper disable once BaseObjectEqualsIsObjectEquals
            return base.GetHashCode();
        }

        public override string ToString()
        {
            var paramName = LowerCaseParamName ? Name.ToString().ToLower() : Name.ToString();
            var paramValue = string.IsNullOrEmpty(Value)? "": LowerCaseParamValue ? Value.ToLowerCase().Trim() : Value.Trim();

            switch (ParamType)
            {
                case RouteParamType.Friendly:
                    return paramName + "-" + paramValue;
                case RouteParamType.Hash:
                case RouteParamType.QueryString:
                    return paramName + "=" + paramValue;
                default:
                    return paramValue;
            }
        }
    }
}
