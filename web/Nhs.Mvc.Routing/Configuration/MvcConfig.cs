﻿/*
 * NewHomeSource - http://www.NewHomeSource.com
 * Copyright © 2001-2010 Builders Digital Experience, LLC.
 * ========================================================
 * Author: Praveen Guggarigoudar
 * Date: 07/28/2010
 * Edit History (please mention edits succinctly):
 * =========================================================
*/
using System;
using System.Collections;
using System.Configuration;
using BHI.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Utility.Data;

namespace Nhs.Mvc.Routing.Configuration
{
    internal class MvcConfig
    {
        #region Properties using BHI Config Manager

        public static string IsMobileRegularExpression1 { get { return GetValue<string>("IsMobileRegularExpression1"); } }
        public static string IsMobileRegularExpression2 { get { return GetValue<string>("IsMobileRegularExpression2"); } }
        public static string IsMobileRegexExclude { get { return GetValue<string>("IsMobileRegexExclude"); } }
        
        internal static string MasterPageDefault
        {
            get
            {
                string value = GetValue<string>("MasterPageDefault");
                if (String.IsNullOrEmpty(value))
                {
                    value = GetValue<string>("MasterPagePartnerBrandDefault_" + NhsRoute.BrandPartnerId);
                }
                return value;
            }
        }

        internal static string MobileMasterDefault
        {
            get
            {
                string value = GetValue<string>("MobileMasterDefault");
                if (String.IsNullOrEmpty(value))
                {
                    value = GetValue<string>("MobileMasterPartnerBrandDefault_" + NhsRoute.BrandPartnerId);
                }
                return value;
            }
        }

        internal static bool ForceMobile
        {
            get { return ConfigurationManager.AppSettings["ForceMobile"].ToType<bool>(); }
        }

        internal static string MobileViewsFolderRoot
        { get { return "~/ViewsMobile/"; } }

        internal static string ViewsFolderRoot
        { get { return "~/Views/"; } }

        internal static string ViewsFolderDefault
        { get { return "Default"; } }

        internal static string ViewsFolderPartnerBrandGroupShared
        { get { return "PartnerBrandGroupShared"; } }

        internal static string ViewsFolderPartnerBrandGroup
        { get { return "PartnerBrandGroupViews"; } }

        internal static string ViewsFolderPartial
        { get { return "PartialViews"; } }

        //internal static string ViewsFolderPartnerGroup
        //{ get { return "~/Views/PartnerGroupViews"; } }

        #endregion

        /// <summary>
        /// Gets value from config DB corresponding to configKey.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="configKey">The config key.</param>
        /// <returns></returns>
        private static T GetValue<T>(string configKey)
        {
            var valueForPartner = ConfigEntryTable[(configKey + "_" + NhsRoute.PartnerId)];
            if (valueForPartner == null || valueForPartner.Equals(default(T)))
                valueForPartner = ConfigEntryTable[configKey];

            return valueForPartner.ToType<T>();
        }

        private static Hashtable ConfigEntryTable
        {
            get
            {
                const string cacheKey = "MvcConfigEntryTable";
                Hashtable ht = (Hashtable)Caching.GetObjectFromCache(cacheKey);
                if (ht == null)
                {
                    Config c = new Config();
                    ht =
                        new Hashtable(
                            c.GetGroupDictionary(ConfigurationManager.AppSettings[MvcConfigConst.AppSettingsGroup]),
                            StringComparer.InvariantCultureIgnoreCase);

                    Caching.AddObjectToCache(ht, cacheKey);
                }
                return ht;
            }
        }
    }
}
