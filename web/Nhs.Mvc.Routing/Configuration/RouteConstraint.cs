﻿
using System;

namespace Nhs.Mvc.Routing.Configuration
{
    [Serializable]
    public class RouteConstraint
    {
        public string Key
        {
            get; set;
        }

        public string Value
        {
            get; set;
        }
    }
}
