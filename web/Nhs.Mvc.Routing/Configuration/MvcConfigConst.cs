﻿namespace Nhs.Mvc.Routing.Configuration
{
    internal class MvcConfigConst
    {
        internal const string ContextItemsCurrentRoute = "ContextItemsCurrentRoute";
        internal const string ContextItemsCurrentPartnerId = "ContextItemsCurrentPartnerId";
        internal const string ContextItemsCurrentViewPath = "ContextItemsCurrentViewPath";
        internal const string ContextItemsCurrentMasterPath = "ContextItemsCurrentMasterPath";
        internal const string ContextItemsCurrentBrandPartnerId = "ContextItemsCurrentBrandPartnerId";
        internal const string ContextItemsCurrentPartnerSiteUrl = "ContextItemsCurrentPartnerSiteUrl";
        internal const string ContextItemsCurrentPartnerType = "ContextItemsCurrentPartnerType";
        internal const string AppSettingsGroup = "Nhs.Mvc.Routing.ConfigGroup";

    }
}