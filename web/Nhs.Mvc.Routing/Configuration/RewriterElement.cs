﻿using System;
using System.Xml.Serialization;

namespace Nhs.Mvc.Routing.Configuration
{
    [Serializable]
    public class RewriterElement
    {
        [XmlAttribute("url")]
        public string Pattern { get; set; }
        [XmlAttribute("to")]
        public string RedirectTo { get; set; }
        [XmlAttribute("permanent")]
        public bool Permanent { get; set; }
    }
}
