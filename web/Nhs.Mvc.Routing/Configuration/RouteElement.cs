﻿using System;
using System.Collections.Generic;
using Nhs.Mvc.Routing.Interface;
using Nhs.Mvc.Routing.Utils;

namespace Nhs.Mvc.Routing.Configuration
{
    [Serializable]
    public class RouteElement
    {
        public string Name { get; set; }

        private string _routeUrl;
        public string RouteUrl
        {
            get
            {
                if (NhsRoute.BrandPartnerId != NhsRoute.PartnerId && !string.IsNullOrEmpty(NhsRoute.PartnerSiteUrl) && !_routeUrl.StartsWith("{partnersiteurl}"))
                    return "{partnersiteurl}/" + _routeUrl;

                return _routeUrl;
            }

            set
            {
                _routeUrl = value;
            }
        }

        private RouteController _controller = new RouteController();
        public RouteController Controller
        {
            get { return _controller; }
            set { _controller = value; }
        }

        public string MasterPage { get; set; }

        public string UseDesktopView { get; set; }

        public string HostUrl { get; set; }

        public string Function { get { return this.RouteUrl.GetFunction(); } }

        public List<RouteParam> Params { get; set; }

        public List<RouteConstraint> Constraints { get; set; }

        public List<RouteDefault> Defaults { get; set; }

        public string AdPageName { get; set; }

        public string AdGroup { get; set; }

        public List<string> AdPositions { get; set; }

        public RouteElement()
        {
            Name = string.Empty;
            AdPageName = string.Empty;
            AdPositions = new List<string>();
            Defaults = new List<RouteDefault>();
            Constraints = new List<RouteConstraint>();
            Params = new List<RouteParam>();
            HostUrl = string.Empty;
            MasterPage = string.Empty;
            UseDesktopView = string.Empty;
        }

        public string ViewFolder { get; set; }
    }
}
