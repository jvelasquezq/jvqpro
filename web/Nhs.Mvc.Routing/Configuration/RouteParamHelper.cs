﻿using System.Globalization;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Routing.Configuration
{
    public static class RouteParamHelper
    {
        public static RouteParam RemoveSpaceAndDash(this RouteParam parameter)
        {
            parameter.Value = parameter.Value.RemoveSpaceAndDash();
            return parameter;
        }

        public static string RemoveSpaceAndDash(this string value)
        {
            return value.Replace("-", "--").Replace(" ", "-");
        }

        public static RouteParam ReturnSpaceAndDash(this RouteParam parameter)
        {
            parameter.Value = parameter.Value.ReturnSpaceAndDash();
            return parameter;
        }

        public static string ReturnSpaceAndDash(this string value)
        {
            return value.Replace("--", "+").Replace("-", " ").Replace("+", "-");
        }

        public static string ReturnRouteParamName(this RouteParam parameter, bool isLower)
        {
            return isLower ? parameter.Name.ToType<string>().ToLower(CultureInfo.InvariantCulture)
                           : parameter.Name.ToType<string>();
        }
    }
}