﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Nhs.Mvc.Routing.Configuration
{
    [XmlType(AnonymousType = true)]
    [XmlRoot("rewriter")]
    public class RewriterElements
    {
        public RewriterElements()
        {
            Rules = new List<RewriterElement>();
        }
        [XmlElement("redirect")]
        public List<RewriterElement> Rules { get; set; }
    }
}
