﻿using System;

namespace Nhs.Mvc.Routing.Configuration
{
    [Serializable]
    public class RouteDefault
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
