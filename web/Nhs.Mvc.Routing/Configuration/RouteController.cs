﻿
using System;

namespace Nhs.Mvc.Routing.Configuration
{
    [Serializable]
    public class RouteController
    {
        public RouteController()
        {
            Name = "";
            Action = "";
        }

        public string Name
        {
            get;
            set;
        }

        public string Action
        {
            get;
            set;
        }
    }
}
