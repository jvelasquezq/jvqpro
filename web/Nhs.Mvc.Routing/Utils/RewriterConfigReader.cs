﻿using System;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Common;
using Nhs.Utility.Data;

namespace Nhs.Mvc.Routing.Utils
{
    internal class RewriterConfigReader
    {
        const string RewriterConfigFileName = @"~\configs\webconfigs\rewriter.config";

        private readonly IPathMapper _pathMapper;

         /// <summary>
        /// Configuration Reader to read rewrite configuration file.
        /// </summary>
        /// <param name="pathMapper">The path mapper.</param>
        internal RewriterConfigReader(IPathMapper pathMapper)
        {
            _pathMapper = pathMapper;
        }

        #region Private Methods

        internal RewriterElements GetRewriterRules()
        {
            const string cacheKey = "RewriterRulesItems";
            var rules = Caching.GetObjectFromCache(cacheKey) as RewriterElements;
            if (rules != null) return rules;
            string fullPath;
            try
            {
                fullPath = _pathMapper.MapPath(RewriterConfigFileName);
                rules = XmlSerialize.DeserializeFromXmlFile<RewriterElements>(fullPath);
                Caching.AddObjectToCache(rules, cacheKey);
            }
            catch (Exception ex)
            {
                fullPath = _pathMapper.MapPath(RewriterConfigFileName);
                ErrorLogger.LogError(new Exception(string.Format("Could not read Rewriter Content file {0} - Exception: {1}", fullPath, ex.Message)));
            }

            return rules;
        }
        #endregion
    }
}
