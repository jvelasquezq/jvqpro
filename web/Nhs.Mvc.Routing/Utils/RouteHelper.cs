﻿/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright © 2001-2010 Builders Digital Experience, LLC.
 * ========================================================
 * Author: Praveen Guggarigoudar
 * Date: 09/17/2010
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Routing.Utils
{
    public static class RouteHelper
    {
        /// <summary>
        /// Gets the function.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <returns></returns>
        public static string GetFunction(this string url)
        {
            if (!string.IsNullOrEmpty(url))
            {
                var partnerSiteUrl = GetCurrentPartnerSiteUrl();

                var urlPathSegments = url.ToLowerInvariant()
                    .Split(new[] {"/"}, StringSplitOptions.RemoveEmptyEntries);
                if (urlPathSegments.Length == 1)
                {
                    return urlPathSegments[0] == partnerSiteUrl || urlPathSegments[0] == "{partnersiteurl}"
                        ? string.Empty
                        : urlPathSegments[0];
                }
                if (urlPathSegments.Length > 1)
                {
                    return urlPathSegments[0] == partnerSiteUrl || urlPathSegments[0] == "{partnersiteurl}"
                        ? urlPathSegments[1]
                        : urlPathSegments[0];
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the current partner id.
        /// </summary>
        /// <returns></returns>
        internal static int GetCurrentPartnerId()
        {
            if (HttpContext.Current != null)
                return HttpContext.Current.Items[MvcConfigConst.ContextItemsCurrentPartnerId].ToType<int>();

            return 1;
        }

        /// <summary>
        /// Gets the type of the current partner.
        /// </summary>
        /// <returns></returns>
        internal static string GetCurrentPartnerType()
        {
            if (HttpContext.Current != null)
                return HttpContext.Current.Items[MvcConfigConst.ContextItemsCurrentPartnerType].ToType<string>();

            return string.Empty;
        }

        /// <summary>
        /// Gets the current brand partner id.
        /// </summary>
        /// <returns></returns>
        internal static int GetCurrentBrandPartnerId()
        {
            if (HttpContext.Current != null)
                return HttpContext.Current.Items[MvcConfigConst.ContextItemsCurrentBrandPartnerId].ToType<int>();

            return 1;
        }

        /// <summary>
        /// Gets the current partner site URL.
        /// </summary>
        /// <returns></returns>
        internal static string GetCurrentPartnerSiteUrl()
        {
            if (HttpContext.Current != null)
                return HttpContext.Current.Items[MvcConfigConst.ContextItemsCurrentPartnerSiteUrl].ToType<string>();

            return string.Empty;
        }

        public static RouteElement GetRouteByFunction(string function)
        {
            return (from routes in MvcRoutes.Routes
                    where routes.Function.ToLowerInvariant() == function.ToLowerInvariant()
                    select routes).FirstOrDefault();
        }

        /// <summary>
        /// Gets route by route name.
        /// </summary>
        /// <param name="routeName">Name of the route.</param>
        /// <returns></returns>
        internal static RouteElement GetRouteByName(string routeName)
        {
            return (from routes in MvcRoutes.Routes
                    where routes.Name.ToLowerInvariant() == routeName.ToLowerInvariant()
                    select routes).FirstOrDefault();
        }

        /// <summary>
        /// Gets the current route by URL.
        /// </summary>
        /// <returns></returns>
        internal static RouteElement GetRoute()
        {
            var context = NhsRoute.CurrentContext ?? new HttpContextWrapper(HttpContext.Current);
            var matchingRoute = (from rt in RouteTable.Routes where rt.GetRouteData(context) != null select rt as Route).FirstOrDefault();

            if (matchingRoute != null)
            {
                var routeResult = (from route in MvcRoutes.Routes
                        where route.RouteUrl.ToLowerInvariant() == matchingRoute.Url.ToLowerInvariant()
                        select route).FirstOrDefault();

                return routeResult;
            }

            return null;
        }

        public static RouteElement GetRouteByUrl(Uri url)
        {

            try
            {
                var matchingRoute = RouteTable.Routes.GetRouteData(new InternalHttpContext(url, HttpContext.Current.Request.ApplicationPath));

                RouteElement route = null;

                if (matchingRoute != null)
                    route = (from r in MvcRoutes.Routes
                             where r.RouteUrl.ToLowerInvariant() == (matchingRoute.Route as Route).Url.ToLowerInvariant()
                             select r).FirstOrDefault();

                if (route != null)
                {
                    route.HostUrl = url.AbsolutePath;
                    route.Params = GetRouteParams();

                    return route;
                }

            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }

            return new RouteElement();
        }
        /// <summary>
        /// Get the RouteParam from the RouteData and QueryString
        /// </summary>
        public static List<RouteParam> GetRouteParams()
        {
            var paramz = new List<RouteParam>();
            try
            {
                var context = NhsRoute.CurrentContext ?? new HttpContextWrapper(HttpContext.Current);
                var routeData = RouteTable.Routes.GetRouteData(context);

                if (routeData != null)
                {
                    var query = HttpContext.Current.Request.Url.Query;
                    var currentUrl = HttpContext.Current.Request.Url.LocalPath;

                    //Get the url, the same url that is in RouteConfig_Global.xml
                    var routeUrl = ((Route) (routeData.Route)).Url;
                    string function = routeUrl.GetFunction().ToLowerCase();
                    var segments = routeUrl.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries).Where(p => p.Contains("{")).Select(p=>p.ToLower()).ToList();
                    var querySegments = query.TrimStart('?').Split(new[] { "&" }, StringSplitOptions.RemoveEmptyEntries).Select(p => p).ToList();
                    var routeUrlPathSegments = currentUrl.ToLowerInvariant().Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries).Select(p => p.ToLower()).ToList();

                    //Remove action and controller from the Values and remove keys that are not in RouteParams
                    var filterValues = routeData.Values.Where(p => p.Key != "action" && p.Key != "controller" && p.Key != "partnersiteurl" && p.Key.EnumExist<RouteParams>()).ToList();
                    var especialUrls = new List<string> { "plan", "home", "community", "communitydetail", "communitydetailpreview" };
                    var resultsFunctions = new List<string> { "communityresults", "homeresults"};

                    if (especialUrls.Contains(function))
                    {
                        paramz = ProcessEspecialUrl(function, routeUrlPathSegments,ref filterValues);
                    }
                    else
                    {
                        var isRouteWithOptionals = routeUrl.Contains(function + "/{*optionals}");  // matches the rule (communityresults|homeresults)/{*optionals}
                        // Hack for synthetic search, it has the form communityresults|homeresults/Name (2 route segments without param name) 
                        if (resultsFunctions.Contains(function) && isRouteWithOptionals &&
                            (routeUrlPathSegments.Count >= 2 && routeUrlPathSegments.Count(p => p.Contains("-area") || p.Contains("market-")) == 0))
                        {
                            var paramName = NhsRoute.BrandPartnerId == NhsRoute.PartnerId? routeUrlPathSegments[1] : routeUrlPathSegments[2];
                            paramz.Add(new RouteParam { Name = RouteParams.SyntheticName, Value = paramName, ParamType = RouteParamType.FriendlyNameOnly });
                        }
                        
                        foreach (var filterValue in filterValues)
                        {
                            var parameter = segments.FirstOrDefault(p => p.Contains("{" + filterValue.Key + "}"));
                            var paramType = parameter != null && parameter.Contains("-{" + filterValue.Key + "}")
                                ? RouteParamType.Friendly
                                : RouteParamType.FriendlyNameOnly;
                            var value = filterValue.Value.ToString();
                            if (value.Contains("?") && value.Contains("="))
                            {
                                var index = value.IndexOf("?", StringComparison.Ordinal);
                                if (index > 0)
                                    value = value.Substring(0, index);
                            }

                            var key = filterValue.Key.FromString<RouteParams>();

                            var routeParam = new RouteParam(key, value, paramType);
                            if (parameter != null && !routeUrlPathSegments.Remove(routeParam.ToString()))
                            {
                                var remove = parameter.Replace("{" + filterValue.Key + "}", value);
                                routeUrlPathSegments.Remove(remove);
                            }
                            paramz.Add(routeParam);
                        }
                    }

                    foreach (var segment in routeUrlPathSegments.Where(p=> p.Contains("-")))
                    {
                        var first = segment.FirstPart("-");
                        var second = segment.LastPart("-");
                        if (paramz.Any(p => p.Name == first.FromString<RouteParams>())) continue;
                        if (first.EnumExist<RouteParams>())
                        {
                            paramz.Add(new RouteParam(first.FromString<RouteParams>(), second));
                        }
                    }

                    //Gather query string params
                    foreach (var querySegment in querySegments)
                    {
                        var paramKey = querySegment.FirstPart("=").ToLowerInvariant();
                        var paramValue = querySegment.LastPart("=");
                        if (paramz.Any(p => p.Name == paramKey.FromString<RouteParams>())) continue;
                        paramz.Add(new RouteParam
                        {
                            Name = paramKey.FromString<RouteParams>(),
                            PlaceHolder = paramKey,
                            Value = paramValue,
                            ParamType = RouteParamType.QueryString
                        });
                    }

                    ////Gather query string params
                    //foreach (var querySegment in querySegments)
                    //{
                    //    var keyvalpairs = querySegment.Split('=');
                    //    var paramKey = keyvalpairs[0];
                    //    var paramValue = keyvalpairs[1];
                    //    if (paramz.Any(p => p.Name == paramKey.FromString<RouteParams>())) continue;
                    //    paramz.Add(new RouteParam
                    //    {
                    //        Name = paramKey.FromString<RouteParams>(),
                    //        PlaceHolder = paramKey,
                    //        Value = paramValue,
                    //        ParamType = RouteParamType.QueryString
                    //    });
                    //}
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }

            return paramz;
        }
        
        private static List<RouteParam> ProcessEspecialUrl(string function, List<string> urlPathSegments,ref List<KeyValuePair<string, object>> filterValues)
        {
            urlPathSegments.RemoveAt(0);
            var paramz = new List<RouteParam>();
            if (function == "plan")
            {
                paramz.Add(new RouteParam { Name = RouteParams.Page, Value = "plan", ParamType = RouteParamType.Friendly });
                paramz.Add(new RouteParam { Name = RouteParams.PlanId, Value = urlPathSegments[0], ParamType = RouteParamType.Friendly });
                paramz.Add(new RouteParam { Name = RouteParams.State, Value = urlPathSegments[1], ParamType = RouteParamType.Friendly });
                paramz.Add(new RouteParam { Name = RouteParams.MarketName, Value = urlPathSegments[2].Replace("-area", string.Empty), ParamType = RouteParamType.Friendly });
                paramz.Add(new RouteParam { Name = RouteParams.City, Value = urlPathSegments[3], ParamType = RouteParamType.Friendly });
                paramz.Add(new RouteParam { Name = RouteParams.Name, Value = urlPathSegments[4].FirstPart("-at-"), ParamType = RouteParamType.Friendly });
                paramz.Add(new RouteParam { Name = RouteParams.CommunityName, Value = urlPathSegments[4].LastPart("-at-").Replace("at-", string.Empty), ParamType = RouteParamType.Friendly });
                urlPathSegments.RemoveRange(0, 4);
            }
            else if (function == "home")
            { 
                paramz.Add(new RouteParam { Name = RouteParams.Page, Value = "home", ParamType = RouteParamType.Friendly });
                paramz.Add(new RouteParam { Name = RouteParams.SpecId, Value = urlPathSegments[0], ParamType = RouteParamType.Friendly });
                paramz.Add(new RouteParam { Name = RouteParams.MarketName, Value = urlPathSegments[1].Replace("-area", string.Empty), ParamType = RouteParamType.Friendly });
                paramz.Add(new RouteParam { Name = RouteParams.Name, Value = urlPathSegments[2].FirstPart("-at-"), ParamType = RouteParamType.Friendly });
                paramz.Add(new RouteParam { Name = RouteParams.Address, Value = urlPathSegments[2].LastPart("-at-").Replace("at-", string.Empty), ParamType = RouteParamType.Friendly });
                urlPathSegments.RemoveRange(0, 2);
            }
            else if (function == "community")
            {
                paramz.Add(new RouteParam { Name = RouteParams.Page, Value = "community", ParamType = RouteParamType.Friendly });
                paramz.Add(new RouteParam { Name = RouteParams.CommunityId, Value = urlPathSegments[0], ParamType = RouteParamType.Friendly });
                paramz.Add(new RouteParam { Name = RouteParams.State, Value = urlPathSegments[1], ParamType = RouteParamType.Friendly });
                paramz.Add(new RouteParam { Name = RouteParams.MarketName, Value = urlPathSegments[2].Replace("-area", string.Empty), ParamType = RouteParamType.Friendly });
                paramz.Add(new RouteParam { Name = RouteParams.City, Value = urlPathSegments[3], ParamType = RouteParamType.Friendly });
                paramz.Add(new RouteParam { Name = RouteParams.CommunityName, Value = urlPathSegments[4].FirstPart("-by-"), ParamType = RouteParamType.Friendly });
                paramz.Add(new RouteParam { Name = RouteParams.BuilderName, Value = urlPathSegments[4].LastPart("-by-").Replace("by-", string.Empty), ParamType = RouteParamType.Friendly });
                urlPathSegments.RemoveRange(0, 4);
            }
            else if (function == "communitydetail" || function == "communitydetailpreview")
            {
                var builder = filterValues.FirstOrDefault(p => p.Key == "builderid");
                paramz.Add(new RouteParam { Name = RouteParams.Builder, Value = builder.Value.ToString(), ParamType = RouteParamType.Friendly });
                var community = filterValues.FirstOrDefault(p => p.Key == "communityid");
                paramz.Add(new RouteParam { Name = RouteParams.Community, Value = community.Value.ToString(), ParamType = RouteParamType.Friendly });
                urlPathSegments.RemoveRange(0, 2);
            }
            return paramz;
        }

        //TODO: Unit Test and look for Bugs - Performance?
        /// <summary>
        /// Gets Route params for a given url and route url.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="routeUrl">The route URL.</param>
        /// <param name="query">The query.</param>
        /// <returns></returns>
        public static List<RouteParam> GetRouteParams(string url, string routeUrl, string query)
        {
            //routeUrl = routeUrl.Replace("{", "").Replace("}", "");
            url = url.Replace("{", "").Replace("}", "");

            var paramz = new List<RouteParam>();
            var urlPathSegments = url.ToLowerInvariant().Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries).ToList();
            var routeUrlPathSegments = routeUrl.ToLowerInvariant().Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries).ToList();
            var querySegments = query.TrimStart('?').Split(new[] { "&" }, StringSplitOptions.RemoveEmptyEntries).ToList();

            string paramKey;
            string paramValue;

            if (urlPathSegments.Count > 0) //save some CPU
            {
                //Get rid of function
                string function = url.GetFunction();
                urlPathSegments.Remove(function);
                routeUrlPathSegments.Remove(function);

                //Get rid of partnersiteurl
                if (GetCurrentPartnerSiteUrl() != null)
                    urlPathSegments.Remove(GetCurrentPartnerSiteUrl());

                if (GetCurrentPartnerSiteUrl() != null)
                    routeUrlPathSegments.Remove("{partnersiteurl}");

                //Get rid of optionals
                urlPathSegments.Remove("*optionals");
                routeUrlPathSegments.Remove("{*optionals}");

                // Specific case of friendly urls for details pages, that doesnt follow the standard param-value that requires the routing framework
                if ((function == "community" || function == "plan" || function == "home") && urlPathSegments.Count >= 3)
                {
                    if (function == "plan")
                    {
                        paramz.Add(new RouteParam(RouteParams.Page, "plan", RouteParamType.FriendlyNameOnly));
                        paramz.Add(new RouteParam(RouteParams.PlanId, urlPathSegments[0], RouteParamType.FriendlyNameOnly));
                        paramz.Add(new RouteParam(RouteParams.State, urlPathSegments[1], RouteParamType.FriendlyNameOnly));
                        paramz.Add(new RouteParam(RouteParams.MarketName,
                            urlPathSegments[2].Replace("-area", string.Empty), RouteParamType.FriendlyNameOnly));
                        paramz.Add(new RouteParam(RouteParams.City, urlPathSegments[3], RouteParamType.FriendlyNameOnly));
                        paramz.Add(new RouteParam(RouteParams.Name, urlPathSegments[4].FirstPart("-at-"),
                            RouteParamType.Friendly));
                        paramz.Add(new RouteParam(RouteParams.CommunityName,
                            urlPathSegments[4].LastPart("-at-").Replace("at-", string.Empty), RouteParamType.FriendlyNameOnly));
                    }
                    else if (function == "home")
                    {
                        paramz.Add(new RouteParam(RouteParams.Page, "home", RouteParamType.FriendlyNameOnly));
                        paramz.Add(new RouteParam(RouteParams.SpecId, urlPathSegments[0], RouteParamType.FriendlyNameOnly));
                        paramz.Add(new RouteParam(RouteParams.MarketName, urlPathSegments[1].Replace("-area", string.Empty), RouteParamType.FriendlyNameOnly));
                        paramz.Add(new RouteParam(RouteParams.Name, urlPathSegments[2].FirstPart("-at-"), RouteParamType.FriendlyNameOnly));
                        paramz.Add(new RouteParam(RouteParams.Address, urlPathSegments[2].LastPart("-at-").Replace("at-", string.Empty), RouteParamType.FriendlyNameOnly));
                    }
                    else if (function == "community")
                    {
                        paramz.Add(new RouteParam(RouteParams.Page, "community", RouteParamType.FriendlyNameOnly));
                        paramz.Add(new RouteParam(RouteParams.CommunityId, urlPathSegments[0], RouteParamType.FriendlyNameOnly));
                        paramz.Add(new RouteParam(RouteParams.State, urlPathSegments[1], RouteParamType.FriendlyNameOnly));
                        paramz.Add(new RouteParam(RouteParams.MarketName, urlPathSegments[2].Replace("-area", string.Empty), RouteParamType.FriendlyNameOnly));
                        paramz.Add(new RouteParam(RouteParams.City, urlPathSegments[3], RouteParamType.FriendlyNameOnly));
                        paramz.Add(new RouteParam(RouteParams.CommunityName, urlPathSegments[4].FirstPart("-by-"), RouteParamType.FriendlyNameOnly));
                        paramz.Add(new RouteParam(RouteParams.BuilderName, urlPathSegments[4].LastPart("-by-").Replace("by-", string.Empty), RouteParamType.FriendlyNameOnly));
                    }
                }
                else
                {
                    //Gather friendlies with dashes/no-dashes
                    for (var i = 0; i < urlPathSegments.Count; i++)
                    {
                        var urlPathSegment = urlPathSegments[i];
                        string placeHolder;
                        if (routeUrlPathSegments.Count > i)
                        {
                            string routeUrlPathSegment = routeUrlPathSegments[i];
                            if (routeUrlPathSegment.Contains("-"))
                            {
                                string[] keyvalpairs = routeUrlPathSegment.Split('-');
                                string first = keyvalpairs[0];
                                string second = keyvalpairs[1];

                                if (first.StartsWith("{") && first.EndsWith("}"))
                                {
                                    placeHolder = first.Replace("{", "").Replace("}", "");
                                    paramKey = second;
                                    paramValue = urlPathSegment.Replace("-" + second, "");
                                }
                                else
                                {
                                    paramKey = first;
                                    placeHolder = second.Replace("{", "").Replace("}", "");
                                    paramValue = urlPathSegment.Replace(first + "-", "");
                                }
                            }
                            else
                            {
                                paramKey = placeHolder = routeUrlPathSegment.Replace("{", "").Replace("}", "");
                                paramValue = urlPathSegment;
                            }
                        }
                        else
                        {
                            if (urlPathSegment.Contains("-"))
                                paramValue = urlPathSegment.LastPart("-");
                            else
                                paramValue = urlPathSegment;

                            paramKey = placeHolder = urlPathSegment.FirstPart("-");
                        }

                        //var x = (from p in paramz
                        //         where p.Name == paramKey
                        //         select p).FirstOrDefault();
                        if (paramKey.EnumExist<RouteParams>())
                        {
                            if (paramz.All(p => p.Name != paramKey.FromString<RouteParams>()))
                                paramz.Add(new RouteParam
                                {
                                    Name = paramKey.FromString<RouteParams>(),
                                    PlaceHolder = placeHolder,
                                    Value = paramValue.Replace("-", " "),
                                    ParamType = RouteParamType.Friendly
                                });
                        }
                    }
                }


                //Gather query string params
                foreach (var querySegment in querySegments)
                {
                    paramKey = querySegment.FirstPart("=").ToLowerInvariant();
                    paramValue = querySegment.LastPart("=");
                    if (paramz.All(p => p.Name != paramKey.FromString<RouteParams>()))
                        paramz.Add(new RouteParam
                        {
                            Name = paramKey.FromString<RouteParams>(),
                            PlaceHolder = paramKey,
                            Value = paramValue,
                            ParamType = RouteParamType.QueryString
                        });
                }
            }
            return paramz;
        }
        /// <summary>
        /// Maps the route and adds name to datatokens.
        /// </summary>
        /// <param name="routes">The routes.</param>
        /// <param name="name">The name.</param>
        /// <param name="url">The URL.</param>
        /// <param name="defaults">The defaults.</param>
        /// <returns></returns>
        internal static Route MapRouteWithName(this RouteCollection routes, string name, string url, object defaults)
        {
            Route route = routes.MapRoute(name, url, defaults);
            route.DataTokens = new RouteValueDictionary();
            route.DataTokens.Add("RouteName", name);
            return route;
        }

        /// <summary>
        /// Maps the route and adds name to datatokens.
        /// </summary>
        /// <param name="routes">The routes.</param>
        /// <param name="name">The name.</param>
        /// <param name="url">The URL.</param>
        /// <returns></returns>
        internal static Route MapRouteWithName(this RouteCollection routes, string name, string url)
        {
            Route route = routes.MapRoute(name, url);
            route.DataTokens = new RouteValueDictionary();
            route.DataTokens.Add("RouteName", name);
            return route;
        }
        
        public static string GetRouteNameFromAction(RouteValueDictionary routeValues)
        {
            string routeName = string.Empty;

            //Find the controller that is mapped to this route
            var controller = (from rv in routeValues
                              where rv.Key.ToLower() == "controller"
                              select rv.Value).FirstOrDefault();

            //Find action that is mapped to this route
            var action = (from rv in routeValues
                          where rv.Key.ToLower() == "action"
                          select rv.Value).FirstOrDefault();

            //Get the matching routes for this controller (a controller could be mapped to multiple routes)
            var matchingRoutes = (from r in MvcRoutes.Routes
                                  where r.Controller.Name == controller.ToString()
                                  select r);

            //We will match up the keys for the current route with the target route
            var rvKeys = routeValues.Keys.OrderBy(x => x.ToString()).ToList();
            StripActionControllerKeys(ref rvKeys); //we dont need to match these up

            foreach (var matchingRoute in matchingRoutes)
            {
                var mrKeys = (from p in matchingRoute.Params
                              orderby p.PlaceHolder
                              select p.PlaceHolder).ToList();

                StripActionControllerKeys(ref mrKeys);

                if (rvKeys.SequenceEqual(mrKeys, StringComparer.InvariantCultureIgnoreCase)) //All keys match, route found!
                {
                    routeName = matchingRoute.Name;
                    break;
                }
            }

            //Worst-cast scenario: pick route using action and controller names
            if (string.IsNullOrEmpty(routeName))
            {
                var defaultRoute = (from r in MvcRoutes.Routes
                                    where r.Controller.Name == controller.ToString() &&
                                          r.Controller.Action == action.ToString()
                                    select r).FirstOrDefault();

                if (defaultRoute != null)
                    return defaultRoute.Name;
            }
            return routeName;
        }

        private static void StripActionControllerKeys(ref List<string> list)
        {
            list.Remove("controller");
            list.Remove("Controller");
            list.Remove("action");
            list.Remove("Action");
            list.Remove("partnersiteurl");
        }

        public static bool GetJsDebug()
        {
            var debug = HttpContext.Current.Request.QueryString["jsdebug"];
            bool isdebug = false;

            if (debug != null)
            {
                bool.TryParse(debug, out isdebug);
            }
            
            return isdebug;
        }

        public static bool GetDisableVwo()
        {
            var debug = HttpContext.Current.Request.QueryString["disablevwo"];
            bool isdebug = false;

            if (debug != null)
            {
                bool.TryParse(debug, out isdebug);
            }

            return isdebug;
        }
    }
}
