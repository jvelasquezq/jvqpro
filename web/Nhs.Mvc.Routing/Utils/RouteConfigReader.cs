/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright � 2001-2010 Builders Digital Experience, LLC.
 * ========================================================
 * Author: Praveen Guggarigoudar
 * Date: 10/17/2010
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.XPath;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Utility.Data;

namespace Nhs.Mvc.Routing.Utils
{
    /// <summary>
    /// Object to Read Config Settings
    /// </summary>
    public class RouteConfigReader
    {
        const string RouteConfigFileName = "RouteConfig";
        private readonly IPathMapper _pathMapper;

        #region Public Methods
        /// <summary>
        /// Configuration Reader to read route configuration.
        /// </summary>
        /// <param name="pathMapper">The path mapper.</param>
        public RouteConfigReader(IPathMapper pathMapper)
        {
            _pathMapper = pathMapper;
        }

        /// <summary>
        /// Gets all global routes.
        /// </summary>
        /// <returns></returns>
        public List<RouteElement> GetRoutes()
        {
            string routeConfigFilePath = GetFileMatchingPattern("RouteConfig_Global.xml");

            if (string.IsNullOrEmpty(routeConfigFilePath))
                return null;

            List<RouteElement> routes = Caching.GetObjectFromCache(routeConfigFilePath) as List<RouteElement>;
            if (routes == null)
            {
                routes = GetRoutes(routeConfigFilePath);
                Caching.AddObjectToCache(routes, routeConfigFilePath);
            }
            return routes;
        }

        /// <summary>
        /// Gets all routes from RouteConfig*.xml file
        /// </summary>
        /// <returns></returns>
        public List<RouteElement> GetPartnerRouteOverrides(bool isBrandPartner)
        {
            var routeConfigFilePath = isBrandPartner ? GetBrandPartnerRouteConfigFilePath() : GetPartnerRouteConfigFilePath();

            if (string.IsNullOrEmpty(routeConfigFilePath))
                return null;

            List<RouteElement> routes = Caching.GetObjectFromCache(routeConfigFilePath) as List<RouteElement>;
            if (routes == null)
            {
                routes = GetRoutes(routeConfigFilePath);
                Caching.AddObjectToCache(routes, routeConfigFilePath);
            }
            return routes;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Gets the brand partner route config file path.
        /// </summary>
        /// <returns></returns>
        private string GetBrandPartnerRouteConfigFilePath()
        {
            if (NhsRoute.PartnerId != 0)
            {
                var partnerRouteConfigSuffix = string.Format("_BrandPartner*${0}.xml", NhsRoute.BrandPartnerId);
                var partnerRouteConfigFilePattern = RouteConfigFileName + partnerRouteConfigSuffix;
                return GetFileMatchingPattern(partnerRouteConfigFilePattern);
            }
            return null;
        }

        /// <summary>
        /// Gets the partner route config file path.
        /// </summary>
        /// <returns></returns>
        private string GetPartnerRouteConfigFilePath()
        {
            if (NhsRoute.PartnerId != 0)
            {
                var partnerRouteConfigSuffix = string.Format("*_{0}.xml", NhsRoute.PartnerId);
                var partnerRouteConfigFilePattern = RouteConfigFileName + partnerRouteConfigSuffix;
                return GetFileMatchingPattern(partnerRouteConfigFilePattern);
            }
            return null;
        }

        private string GetFileMatchingPattern(string fileNamePattern)
        {
            var routeConfigDir = @"~\configs\routeconfigs\";
            routeConfigDir = _pathMapper.MapPath(routeConfigDir);
            DirectoryInfo dir = new DirectoryInfo(routeConfigDir);

            if (dir.Exists)
            {
                FileInfo fi = dir.GetFiles(fileNamePattern, SearchOption.TopDirectoryOnly).FirstOrDefault();
                return (fi != null ? fi.FullName : null);
            }

            return null;
        }

        private List<RouteElement> GetRoutes(string documentPath)
        {
            List<RouteElement> routes = new List<RouteElement>();
            XPathDocument doc = new XPathDocument(documentPath);
            XPathNavigator nav = doc.CreateNavigator();

            XPathNodeIterator routeXml = nav.Select("/routes/route");

            while (routeXml.MoveNext())
            {
                var name = routeXml.Current.GetAttribute("name", string.Empty);
                var routeUrl = routeXml.Current.GetAttribute("url", string.Empty);
                var masterPage = routeXml.Current.GetAttribute("masterpage", string.Empty);
                var useDesktopView = routeXml.Current.GetAttribute("usedesktopview", string.Empty);
                var controller = routeXml.Current.GetAttribute("controller", string.Empty);
                var viewFolder = routeXml.Current.GetAttribute("viewfolder", string.Empty);
                var action = routeXml.Current.GetAttribute("action", string.Empty);
                var adPageName = routeXml.Current.GetAttribute("adpagename", string.Empty);
                var adPositions = routeXml.Current.GetAttribute("adpositions", string.Empty);

                XPathNodeIterator constraintXml = routeXml.Current.Select("constraints/constraint");

                var constraints = new List<RouteConstraint>();
                while (constraintXml.MoveNext())
                {
                    var constraint = constraintXml.Current.Select("constraint");
                    var constraintKey = constraint.Current.GetAttribute("key", string.Empty);
                    var constraintValue = constraint.Current.GetAttribute("value", string.Empty);
                    constraints.Add(new RouteConstraint
                                        {
                                            Key = constraintKey,
                                            Value = constraintValue
                                        });

                }

                RouteController routeController = new RouteController { Name = controller, Action = action };

                XPathNodeIterator defaultXml = routeXml.Current.Select("defaults/default");
                List<RouteDefault> defaults = new List<RouteDefault>();
                while (defaultXml.MoveNext())
                {
                    var def = defaultXml.Current.Select("default");
                    defaults.Add(new RouteDefault
                                        {
                                            Key = def.Current.GetAttribute("key", string.Empty),
                                            Value = def.Current.GetAttribute("value", string.Empty)
                                        });
                }

                routes.Add(new RouteElement
                               {
                                   Name = name,
                                   RouteUrl = routeUrl,
                                   MasterPage = masterPage,
                                   UseDesktopView = useDesktopView,
                                   Constraints = constraints,
                                   Controller = routeController,
                                   ViewFolder = viewFolder,
                                   AdPageName = adPageName,
                                   AdPositions = this.ParseAdPositions(adPositions),
                                   Params = RouteHelper.GetRouteParams(routeUrl, routeUrl, string.Empty),
                                   Defaults = defaults
                               });
            }
            return routes;
        }

        private List<string> ParseAdPositions(string adPositions)
        {
            try
            {
                return adPositions.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).ToList();
            }
            catch
            {
                return new List<string>();
            }
        }

        #endregion
    }
}
