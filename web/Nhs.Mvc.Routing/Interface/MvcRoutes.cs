﻿/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright © 2001-2010 Builders Digital Experience, LLC.
 * ========================================================
 * Author: Praveen Guggarigoudar
 * Date: 09/17/2010
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Utils;
using Nhs.Utility.Common;
using Nhs.Utility.Data;

namespace Nhs.Mvc.Routing.Interface
{
    public static class MvcRoutes
    {
        private static List<RouteElement> _globalRoutes;
        private static IPathMapper _pathMapper;

        /// <summary>
        /// All routes for current partner
        /// </summary>
        /// <value>The routes.</value>
        public static List<RouteElement> Routes
        {
            get
            {
                if (_globalRoutes == null)
                    throw new InvalidOperationException("Routes have not been initialized. Please call MvcRoutes.Initialize() from Global.asax");

                //Gather additional configuration by partner/brand
                string cacheKey = "PartnerRoutes_" + NhsRoute.PartnerId;
                List<RouteElement> partnerRoutes = Caching.GetObjectFromCache(cacheKey) as List<RouteElement>;
                if (partnerRoutes != null)
                    return partnerRoutes;

                partnerRoutes = new List<RouteElement>();
                RouteConfigReader configReader = new RouteConfigReader(_pathMapper);

                var brandPartnerRouteConfigs = configReader.GetPartnerRouteOverrides(true); //grab brandpartner-specific config overrides
                var partnerRouteConfigs = configReader.GetPartnerRouteOverrides(false); //grab partner-specific config overrides

                foreach (var globalRoute in _globalRoutes)
                {
                    //Get brand-partner route config
                    RouteElement brandPartnerRouteConfig = null;

                    if (brandPartnerRouteConfigs != null)
                        brandPartnerRouteConfig = (from rc in brandPartnerRouteConfigs
                                                   where rc.Name == globalRoute.Name
                                                   select rc).FirstOrDefault();

                    //Get partner route config
                    RouteElement partnerRouteConfig = null;

                    if (partnerRouteConfigs != null)
                        partnerRouteConfig = (from rc in partnerRouteConfigs
                                              where rc.Name == globalRoute.Name
                                              select rc).FirstOrDefault();

                    RouteElement partnerRoute = new RouteElement();
                    partnerRoute.Name = globalRoute.Name;
                    partnerRoute.Params = globalRoute.Params;
                    partnerRoute.RouteUrl = globalRoute.RouteUrl;
                    partnerRoute.HostUrl = globalRoute.HostUrl;
                    partnerRoute.Constraints = globalRoute.Constraints;
                    partnerRoute.Controller = globalRoute.Controller;
                    partnerRoute.ViewFolder = globalRoute.ViewFolder;

                    //Apply brand-partner-level settings first
                    if (brandPartnerRouteConfig != null)
                    {
                        partnerRoute.MasterPage = brandPartnerRouteConfig.MasterPage;
                        partnerRoute.UseDesktopView = brandPartnerRouteConfig.UseDesktopView;
                        partnerRoute.AdPageName = brandPartnerRouteConfig.AdPageName;
                        partnerRoute.AdPositions = brandPartnerRouteConfig.AdPositions;
                    }

                    //Apply any partner-specific settings next
                    if (partnerRouteConfig != null)
                    {
                        if (!string.IsNullOrEmpty(partnerRouteConfig.MasterPage))
                            partnerRoute.MasterPage = partnerRouteConfig.MasterPage;
                        if (!string.IsNullOrEmpty(partnerRouteConfig.UseDesktopView))
                            partnerRoute.UseDesktopView = partnerRouteConfig.UseDesktopView;
                        if (!string.IsNullOrEmpty(partnerRouteConfig.AdPageName))
                            partnerRoute.AdPageName = partnerRouteConfig.AdPageName;
                        if (partnerRouteConfig.AdPositions.Count > 0)
                            partnerRoute.AdPositions = partnerRouteConfig.AdPositions;
                    }
                    partnerRoutes.Add(partnerRoute);
                }

                Caching.AddObjectToCache(partnerRoutes, cacheKey);
                return partnerRoutes;
            }
        }

        /// <summary>
        /// Initializes routes for brands and partners 
        /// </summary>
        /// <param name="routes">Routes collection</param>
        /// <param name="pathMapper">The path mapper.</param>
        public static void Initialize(RouteCollection routes, IPathMapper pathMapper)
        {
            _pathMapper = pathMapper;
            //routes.MapRoute("sitemapindex", "sitemapindex.xml", new { controller = "DownLoadFiles", action = "SiteMapIndex" });
            AddIgnoreRoutes(routes);
            RouteConfigReader configReader = new RouteConfigReader(_pathMapper);
            _globalRoutes = configReader.GetRoutes();
            //routes.RouteExistingFiles = true;
         
           
            if (_globalRoutes == null)
                throw new InvalidOperationException("MVC Routes have not been defined in configuration file.");

            foreach (RouteElement routeElement in _globalRoutes)
            {
                // Map route for brand
                Route route = MapRoute(routeElement, routes);

                // Add route constraints
                AddRouteConstraint(route, routeElement);
                AddRouteDefaults(route, routeElement);

                //Map route for partner
                RouteElement partnerRouteElement = new RouteElement();
                partnerRouteElement.Name = "p" + routeElement.Name;
                partnerRouteElement.RouteUrl = "{partnersiteurl}/" + routeElement.RouteUrl;
                partnerRouteElement.Constraints = routeElement.Constraints;
                partnerRouteElement.Controller = routeElement.Controller;
                partnerRouteElement.Defaults = routeElement.Defaults;

                Route partnerRoute = MapRoute(partnerRouteElement, routes);

                // Add route constraints
                AddRouteConstraint(partnerRoute, partnerRouteElement);
                AddRouteDefaults(partnerRoute, partnerRouteElement);
            }
        }

        private static void AddIgnoreRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.htc");
            routes.IgnoreRoute("{resource}.txt");
            routes.IgnoreRoute("{resource}.flv");
            routes.IgnoreRoute("{resource}.xml");
            routes.IgnoreRoute("{resource}.html");
            routes.IgnoreRoute("{resource}.htm");
            routes.IgnoreRoute("{resource}.aspx");
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            //routes.IgnoreRoute("{*allaspx}", new { allaspx = @".*\.aspx(/.*)?" });
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.gif(/.*)?" });
        }

        private static Route MapRoute(RouteElement routeElement, RouteCollection routes)
        {
            if (routeElement.Controller == null)
            {
                return routes.MapRouteWithName(routeElement.Name, routeElement.RouteUrl);
            }

            if (!string.IsNullOrEmpty(routeElement.Controller.Action))
            {
                return routes.MapRouteWithName(routeElement.Name, routeElement.RouteUrl,
                                       new
                                           {
                                               controller = routeElement.Controller.Name,
                                               action = routeElement.Controller.Action
                                           });
            }

            throw new Exception("Route configuration error! Controller Action cannot be null when a controller is defined.");
        }

        private static void AddRouteConstraint(Route route, RouteElement routeElement)
        {
            foreach (RouteConstraint constraint in routeElement.Constraints)
                route.Constraints.Add(constraint.Key, constraint.Value);
        }

        private static void AddRouteDefaults(Route route, RouteElement routeElement)
        {
            foreach (RouteDefault def in routeElement.Defaults)
            {
                route.Defaults.Add(def.Key, def.Value);
            }
        }
    }
}
