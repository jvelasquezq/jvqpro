﻿using System.Text.RegularExpressions;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Utils;
using Nhs.Utility.Common;
using Nhs.Utility.Data;

namespace Nhs.Mvc.Routing.Interface
{
    public static class NhsRewriterRoutes
    {
        /// <summary>
        /// We had some bugs with nonexistent urls (like about-us) in our routetables
        /// when this is not exist we are always sending 404 because it's always executing in
        /// RewriterModule Get404RedirectUrl and rewriter didn't have enought time to rewrite the url.
        /// </summary>
        /// <param name="urlToMatch">Url to make the match against rewriter</param>
        /// <returns></returns>
        public static bool CheckIfRewriterRuleExist(string urlToMatch)
        {
            string cacheKey = "NhsPartnerRewriteRoutes_" + NhsRoute.BrandPartnerId;
            var rules = Caching.GetObjectFromCache(cacheKey) as RewriterElements;
            if(rules == null)
            { 
                var rewriterConfiguration = new RewriterConfigReader(new ContextPathMapper());
                rules = rewriterConfiguration.GetRewriterRules();
                Caching.AddObjectToCache(rules, cacheKey);
            }
            // ReSharper disable once LoopCanBeConvertedToQuery
            // For now let's keep as a foreach in case we need to debug our code.
            foreach (var rule in rules.Rules)
            {
                var expression = new Regex(rule.Pattern);
                if (!expression.IsMatch(urlToMatch)) continue;
                    return true;
            }
            return false;
        }
    }
}
