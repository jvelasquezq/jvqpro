﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Utils;
using Nhs.Utility.Common;
using Nhs.Utility.Constants;
using WURFL;

namespace Nhs.Mvc.Routing.Interface
{
    public static class NhsRoute
    {
        public static HttpContextBase CurrentContext { get; set; }
        static NhsRoute()
        {
            CurrentContext = null;
        }


        public static string GetReferrerUrl
        {
            get
            {
                return HttpContext.Current.Request.UrlReferrer != null ? HttpContext.Current.Request.UrlReferrer.PathAndQuery : "";
            }
        }
        /// <summary>
        /// Provides a fully-hydrated RouteElement for current request
        /// </summary>
        /// <returns></returns>
        public static RouteElement CurrentRoute
        {
            get
            {
                var currentRoute = HttpContext.Current.Items[MvcConfigConst.ContextItemsCurrentRoute] as RouteElement;

                if (currentRoute != null)
                    return currentRoute;

                string currentUrl = HttpContext.Current.Request.Url.LocalPath;
                //string function = currentUrl.GetFunction();
                currentRoute = RouteHelper.GetRoute().DeepCloneObject<RouteElement>();
                if (currentRoute == null)
                    return null;

                currentRoute.HostUrl = currentUrl;
                currentRoute.Params = RouteHelper.GetRouteParams();

                HttpContext.Current.Items.Add(MvcConfigConst.ContextItemsCurrentRoute, currentRoute);
                return currentRoute;
            }
        }


        public static T DeepCloneObject<T>(this object obj)
        {
            if (obj == null) return default(T);
            using (MemoryStream memStream = new MemoryStream())
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter(null,
                     new StreamingContext(StreamingContextStates.Clone));
                binaryFormatter.Serialize(memStream, obj);
                memStream.Seek(0, SeekOrigin.Begin);
                return binaryFormatter.Deserialize(memStream).ToType<T>();
            }
        }

        //public static RouteElement GetRouteByName(string routeName)
        //{
        //    return RouteHelper.GetCurrentRouteByName(routeName);
        //}

        /// <summary>
        /// Gets the value of a url parameter
        /// </summary>
        /// <param name="paramName">Name of the parameter.</param>
        /// <returns></returns>
        public static string GetValue(RouteParams paramName)
        {
            return GetValue(CurrentRoute.Params, paramName);
        }

        public static string GetValue(IList<RouteParam> routeParams, RouteParams paramName)
        {
            var param = routeParams.FirstOrDefault(x => x == paramName);
            if (param == null)
                return string.Empty;

            return string.IsNullOrEmpty(param.Value) ? string.Empty : HttpUtility.UrlDecode(param.Value);
        }


        public static RouteParam GetRouteParam(RouteParams paramName)
        {
            return GetRouteParam(CurrentRoute.Params, paramName);
        }

        public static RouteParam GetRouteParam(IList<RouteParam> routeParams, RouteParams paramName)
        {
            var param = routeParams.FirstOrDefault(x => x == paramName);
            return param;
        }


        public static string GetPlaceHolder(IList<RouteParam> routeParams, RouteParams paramName)
        {

            var param = routeParams.FirstOrDefault(x => x == paramName);
            if (param == null)
                return string.Empty;

            return string.IsNullOrEmpty(param.Value) ? string.Empty : HttpUtility.UrlDecode(param.Value);
        }

        /// <summary>
        /// Indicates whether request was a postback
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this request was a postback; otherwise, <c>false</c>.
        /// </value>
        public static bool IsPostBack
        {
            get
            {
                if (HttpContext.Current != null)
                    return HttpContext.Current.Request.HttpMethod == "POST";

                return false;
            }
        }

        public static bool ShowMobileSite
        {
            get
            {
                return IsMobileDevice && !IsFullSiteCookieSet && (PartnerId == 1 || PartnerId == 10001);//80383 - HF
            }
        }

        private static bool IsFullSiteCookieSet
        {
            get
            {
                var fullSiteCookie = HttpContext.Current.Request.Cookies["ShowFullSiteToUser"];
                return fullSiteCookie != null && fullSiteCookie.Value == "TRUE";
            }
        }

    

        public static bool IsMobileDevice
        {
            get
            {
                var userAgent = HttpContext.Current.Request.UserAgent;
                ////51 degrees code for mobile detection
                if (string.IsNullOrEmpty(userAgent))
                    return false;
                //var provider = WebProvider.ActiveProvider;
                //var match = provider.Match(userAgent);
                //var isMobile = match.Results["IsMobile"][0].ToType<bool>();
                
                //var isNotTablet = IsTabletDevice == false;

                var device = WURFLManagerBuilder.Instance.GetDeviceForRequest(userAgent);
                var isMobileDevice = Boolean.Parse(device.GetCapability("is_smartphone"));
                return isMobileDevice;
            }
        }

        //Change it to used 51 degrees
        public static bool IsTabletDevice
        {
            get
            {
                var userAgent = HttpContext.Current.Request.UserAgent;
                if (string.IsNullOrEmpty(userAgent))
                    return false;

                //var t = new Regex(MvcConfig.IsMobileRegexExclude, RegexOptions.IgnoreCase | RegexOptions.Multiline);
                //var isNotTablet = t.IsMatch(userAgent.ToLower());

                var device = WURFLManagerBuilder.Instance.GetDeviceForRequest(userAgent);
                var isTablet = Boolean.Parse(device.GetCapability("is_tablet"));
                return isTablet;
            }
        }
        /// <summary>
        /// Determines whether current route is an MVC route.
        /// An MVC route is a route defined in the MvcRoutes.config file.
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if current route is an MVC route; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsRouteAnMvcRoute()
        {
            return CurrentRoute != null ? true : false;
        }

        //todo: These properties are getting added to context by urlrewriter right now. Once rewriter goes away, these need to be computed in a different manner.
        #region Properties using context

        public static int PartnerId
        { get { return RouteHelper.GetCurrentPartnerId(); } }

        public static string PartnerType
        { get { return RouteHelper.GetCurrentPartnerType(); } }

        public static int BrandPartnerId
        { get { return RouteHelper.GetCurrentBrandPartnerId(); } }

        public static string PartnerSiteUrl
        { get { return RouteHelper.GetCurrentPartnerSiteUrl(); } }

        public static string SiteRoot
        { get { return string.IsNullOrEmpty(PartnerSiteUrl) ? string.Empty : (PartnerSiteUrl + "/"); } }

        public static bool JsDebug
        {
            get { return RouteHelper.GetJsDebug(); }
        }

        public static bool DisableVwo
        {
            get { return RouteHelper.GetDisableVwo(); }
        }


        public static bool IsPartnerNhsPro
        {
            get { return PartnerId == PartnersConst.Pro.ToType<int>(); }
        }

        public static bool IsBrandPartnerNhsPro
        {
            get { return BrandPartnerId == PartnersConst.Pro.ToType<int>(); }
        }
        #endregion
    }
}
