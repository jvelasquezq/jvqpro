using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using BHI.EnterpriseLibrary.Core.Data;
using Nhs.Mvc.Domain.Model.BhiTransaction;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Data.Repositories
{
    public class DataProvider
    {
        public string HubDbConnection = "";
        public string WebDbConnection = "";

        private const string HubDb = "Nhs.Hub";
        private const string WebDb = "Nhs.Web";
        private const string HubEsDb = "Nhs.EsHub";
        private const string WebEsDb = "Nhs.EsWeb";

        private const string CnhDb = "Nhs.CNH";
        private const string ProfilesDb = "Nhs.Profiles";
        private const string TransactionDb = "Nhs.Transaction";
        private const string CmsDb = "Nhs.Cms";
        private const string CmsDbEktron = "Nhs.Cms.Ektron";
        private const int CommandTimeout = 90;
        private static readonly DataProvider Provider = new DataProvider();

        #region Current object

        public static DataProvider Current
        {
            get
            {
                Provider. HubDbConnection = NhsRoute.BrandPartnerId == 10001 ? HubEsDb : HubDb;
                Provider.WebDbConnection = NhsRoute.BrandPartnerId == 10001 ? WebEsDb : WebDb;
                return Provider;
            }
        }

        // mark constructor private so other classes can only access through the Current property
        private DataProvider()
        {
        }

        #endregion

        #region "Generic"

        public DataRowCollection GetBuilderByCommunityMarket(int communityId, int marketId)
        {
            string proc = "pr_NHS_GetBuilderByCommunityMarket";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            db.AddInParameter(cmd, "@community_id", DbType.Int32, communityId);
            db.AddInParameter(cmd, "@market_id", DbType.Int32, marketId);
            cmd.CommandTimeout = CommandTimeout;
            return db.ExecuteDataSet(cmd).Tables[0].Rows;
        }

        public string GetStateFromAbbreviation(string stateAbbreviation)
        {
            string proc = "pr_NHS_GetStateFromAbbrev";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            db.AddInParameter(cmd, "@stateabbrev", DbType.String, stateAbbreviation);
            cmd.CommandTimeout = CommandTimeout;
            return db.ExecuteScalar(cmd).ToString();
        }

        public DataTable GetMarketsByState(string stateAbbreviation, string stateName)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string proc = "pr_NHS_GetMarketsbyState";
            string[] parms = new[] { stateAbbreviation, stateName };

            return db.ExecuteDataSet(proc, parms).Tables[0];
        }

        public DataTable GetMarketsByAmenity(string stateAbbreviation, int partnerID, string amenityType)
        {
            string proc = "pr_NHS_GetMarketsbyAmenity";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "StateAbbr", DbType.String, stateAbbreviation);
            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerID);

            if (!string.IsNullOrEmpty(amenityType))
                db.AddInParameter(cmd, "AmenityType", DbType.String, amenityType);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public string GetBuilderName(int builderId)
        {
            string proc = "pr_NHS_GetBuilderName";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "BuilderId", DbType.String, builderId);

            return (string)db.ExecuteScalar(cmd);
        }

        public DataTable GetBrand(int brandId)
        {
            string proc = "pr_NHS_GetBrand";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "BrandId", DbType.Int32, brandId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }


        public DataTable GetMarketandStateByBrand(int brandId, int partnerId)
        {
            string proc = "pr_NHS_GetMarketandStateByBrand";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "BrandId", DbType.Int32, brandId);
            db.AddInParameter(cmd, "PartnerID", DbType.Int32, partnerId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }


        public DataTable GetShowCaseImagesForBrand(int brandId)
        {
            const string proc = "pr_NHS_GetShowCaseImagesForBrand";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "BrandId", DbType.Int32, brandId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public string GetSpecOrPlanByListingId(int listingId)
        {
            string proc = "pr_NHS_GetSpecOrPlanByListingId";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "ListingId", DbType.Int32, listingId);

            return (string)db.ExecuteScalar(cmd);
        }

        public string GetCommunityName(int communityId)
        {
            string proc = "pr_NHS_GetCommunityName";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "CommunityId", DbType.String, communityId);

            return (string)db.ExecuteScalar(cmd);
        }

        public DataTable GetStates()
        {
            string proc = "pr_NHS_GetStates";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetLeadPriceRangeItems()
        {
            string proc = "pr_NHS_GetPriceRanges";

            Database db = DatabaseFactory.CreateDatabase(TransactionDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataSet GetGroupList(string groupName)
        {
            string proc = "pr_NHS_GetGroupList";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "GroupName", DbType.String, groupName);

            return db.ExecuteDataSet(cmd);
        }

        public DataTable GetMarketNames()
        {
            string proc = "pr_NHS_GetMarketName";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetAllMarkets()
        {
            string proc = "pr_NHS_GetAllMarkets";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetMarket(int marketId, int partnerId)
        {
            string proc = "pr_NHS_GetMarket";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "MarketId", DbType.Int32, marketId);
            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetMarket(string stateName, string marketName)
        {
            string proc = "pr_NHS_GetMarketByStateNameAndMarketName";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "StateName", DbType.String, stateName);
            db.AddInParameter(cmd, "MarketName", DbType.String, marketName);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetSimilarPostalCodes(string postalCode, int partnerId)
        {
            string proc = "pr_NHS_GetSimilarPostalCodes";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);
            db.AddInParameter(cmd, "PostalCode", DbType.String, postalCode);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetStatesWithCommunities(int partnerId)
        {
            string proc = "pr_NHS_GetStatesWithCommunities";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetStateMapLocations()
        {
            string proc = "pr_NHS_GetStateMapLocations";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetFinancePrefItems()
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string storedProcedureName = "pr_NHS_GetGroupList";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@GroupName", DbType.String, "FINANCEPR");
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetMoveInDateItems()
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string storedProcedureName = "pr_NHS_GetGroupList";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@GroupName", DbType.String, "MOVEINDT");
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetMoveReasonItems()
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string storedProcedureName = "pr_NHS_GetGroupList";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@GroupName", DbType.String, "MOVEREASON");
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetSalesAgents(int salesOfficeId, bool useHub)
        {
            Database db = useHub ? DatabaseFactory.CreateDatabase(HubDbConnection) : DatabaseFactory.CreateDatabase(WebDbConnection);
            var proc = "pr_NHS_GetSalesAgents";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@sales_office_id", DbType.Int32, salesOfficeId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetSalesOffice(int salesOfficeId)
        {
            string proc = "pr_NHS_GetSalesOfficeById";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "SalesOfficeId", DbType.Int32, salesOfficeId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetIPLocation(long ipAddress)
        {
            string proc = "pr_NHS_GetIPLocation";

            Database db = DatabaseFactory.CreateDatabase(TransactionDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "IPAddress", DbType.Int64, ipAddress);

            return db.ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region "Profile & Login"

        /// <summary>
        /// Checks for valid logon name existance.
        /// </summary>
        /// <param name="logonName">Valid email ID</param>
        /// <param name="partnerId"></param>
        /// <returns>True if logon name exist else false</returns>
        public bool LogonNameExists(string logonName, int partnerId)
        {
            string proc = "pr_NHS_CheckLogonName";

            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "LogonName", DbType.String, logonName);
            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);

            int x = (int)db.ExecuteScalar(cmd);

            return x == 1;
        }

        public DataTable GetSavedListings(string userId)
        {
            string proc = "pr_NHS_GetSavedListing";

            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "UserId", DbType.String, userId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetSavedListingForProCrm(string userId)
        {
            string proc = "pr_NHS_GetSavedListingForProCrm";

            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "g_user_id", DbType.String, userId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public void AddSavedListing(string userId, string listingType, int listingId, int builderId)
        {
            string proc = "pr_NHS_AddSavedListing";

            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "g_user_id", DbType.String, userId);
            db.AddInParameter(cmd, "listing_type", DbType.String, listingType);
            db.AddInParameter(cmd, "listing_id", DbType.Int32, listingId);
            db.AddInParameter(cmd, "builder_id", DbType.Int32, builderId);

            db.ExecuteNonQuery(cmd);
        }

        public void AddRecommendedListing(string userId, string listingType, int listingId, int builderId,
                                          bool recFlag)
        {
            string proc = "pr_NHS_AddRecommendedListing";

            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "g_user_id", DbType.String, userId);
            db.AddInParameter(cmd, "listing_type", DbType.String, listingType);
            db.AddInParameter(cmd, "listing_id", DbType.Int32, listingId);
            db.AddInParameter(cmd, "builder_id", DbType.Int32, builderId);
            db.AddInParameter(cmd, "rec_flag", DbType.Int32, recFlag);

            db.ExecuteNonQuery(cmd);
        }

        public void RemoveSavedListing(string userId, string listingType, int listingId, int builderId)
        {
            string proc = "pr_NHS_RemoveSavedListing";

            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "g_user_id", DbType.String, userId);
            db.AddInParameter(cmd, "listing_type", DbType.String, listingType);
            db.AddInParameter(cmd, "listing_id", DbType.Int32, listingId);

            if (builderId > 0)
                db.AddInParameter(cmd, "builder_id", DbType.Int32, builderId);

            db.ExecuteNonQuery(cmd);
        }

        public void RemoveRecommendedListing(string userId, string listingType, int listingId, int builderId)
        {
            string proc = "pr_NHS_RemoveRecommendedListing";

            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "g_user_id", DbType.String, userId);
            db.AddInParameter(cmd, "listing_type", DbType.String, listingType);
            db.AddInParameter(cmd, "listing_id", DbType.Int32, listingId);
            db.AddInParameter(cmd, "builder_id", DbType.Int32, builderId);

            db.ExecuteNonQuery(cmd);
        }

        public void RemoveRecommendedListing(string userId, string listingType)
        {
            string proc = "pr_NHS_RemoveRecommendedListing";

            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "g_user_id", DbType.String, userId);
            db.AddInParameter(cmd, "listing_type", DbType.String, listingType);

            db.ExecuteNonQuery(cmd);
        }

        public void RemoveRecommendedListing(string userId, string listingType, bool removeArchived)
        {
            string proc = "pr_NHS_RemoveRecommendedListing";

            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "g_user_id", DbType.String, userId);
            db.AddInParameter(cmd, "listing_type", DbType.String, listingType);
            db.AddInParameter(cmd, "remove_archived", DbType.Boolean, removeArchived);

            db.ExecuteNonQuery(cmd);
        }

        public void UpdateRequestDate(string userId, string listingType, int listingId, string sourceRequestKey)
        {
            string proc = "pr_NHS_UpdateRequestDate";

            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "g_user_id", DbType.String, userId);
            db.AddInParameter(cmd, "listing_type", DbType.String, listingType);
            db.AddInParameter(cmd, "listing_id", DbType.Int32, listingId);
            db.AddInParameter(cmd, "source_request_key", DbType.String, sourceRequestKey);
            db.ExecuteNonQuery(cmd);
        }

        /// <summary>
        /// Allows user to signin, called on login page.
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="logonName"></param>
        /// <returns></returns>
        public DataTable SignIn(int partnerId, string logonName)
        {
            string proc = "pr_NHS_SignIn";

            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);
            db.AddInParameter(cmd, "LogonName", DbType.String, logonName);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetProfileByGUID(string userId)
        {
            string proc = "pr_NHS_GetProfileByGUID";
            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "g_user_id", DbType.String, userId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        /// <summary>
        /// This is used only for Single Sign On.  The ssoId is a unique identifier
        /// </summary>
        /// <param name="ssoId">Single Sign On Id, unique id</param>
        /// <param name="partnerId">Partner Id</param>
        /// <returns></returns>
        public DataTable GetProfileBySsoId(string ssoId, int partnerId)
        {
            string proc = "pr_NHS_GetProfileBySsoId";
            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "sso_user_id", DbType.String, ssoId);
            db.AddInParameter(cmd, "pid", DbType.Int16, partnerId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        /// <summary>
        /// This is specifically used on Emails where User ID is not accesible
        /// Ack Manager just passes the Logon Name/Email
        /// </summary>
        /// <param name="partnerID"></param>
        /// <param name="logonName">Valid Email</param>
        /// <returns></returns>
        public DataTable GetProfileByLogonName(int partnerID, string logonName)
        {
            string proc = "pr_NHS_GetProfile";
            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "u_logon_name", DbType.String, logonName);
            db.AddInParameter(cmd, "pid", DbType.Int32, partnerID);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetAgentBrochure(string user_id)
        {
            var proc = "pr_NHS_GetAgentBrochure";
            var db = DatabaseFactory.CreateDatabase(ProfilesDb);
            var cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@g_user_id", DbType.String, user_id);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public void SetAgentRecord(string userId, string agencyLogo, string imageUrl, string agencyName,
                                   string firstName, string lastName,
                                   string email, string address, string city, string state, string postalCode,
                                   string officePhone, string mobilePhone,
                                   DateTime createdDate, DateTime? dateLastChange, string updateImages)
        {
            string proc = "pr_NHS_SetAgentBrochure";
            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "g_user_id", DbType.String, userId);
            db.AddInParameter(cmd, "agency_logo", DbType.String, agencyLogo);
            db.AddInParameter(cmd, "image_url", DbType.String, imageUrl);
            db.AddInParameter(cmd, "agency_name", DbType.String, agencyName);
            db.AddInParameter(cmd, "first_name", DbType.String, firstName);
            db.AddInParameter(cmd, "last_name", DbType.String, lastName);
            db.AddInParameter(cmd, "email", DbType.String, email);
            db.AddInParameter(cmd, "address", DbType.String, address);
            db.AddInParameter(cmd, "city", DbType.String, city);
            db.AddInParameter(cmd, "state", DbType.String, state);
            db.AddInParameter(cmd, "postal_code", DbType.String, postalCode);
            db.AddInParameter(cmd, "office_phone", DbType.String, officePhone);
            db.AddInParameter(cmd, "mobile_phone", DbType.String, mobilePhone);
            db.AddInParameter(cmd, "updateImages", DbType.String, updateImages);

            db.ExecuteNonQuery(cmd);

        }

        public void SetProfile(string userId, string logonName, string firstName, string lastName, string password,
                               int partnerId,
                               DateTime dateRegistered, string middleName, string address1, string address2,
                               string state, string city, string dayPhone,
                               string evePhone, string fax, string postalCode, string mailList, string referrerName,
                               string marketOptIn, string regMetro, string regPrice, string DayPhoneExt,
                               string evePhoneExt, string leadOptin,
                               string regCity, string regPrefer, string regState, string age, bool regBoylInterest,
                               bool regComingSoonInterest,
                               bool regActiveAdultInterest, int moveInDate, int financePreference, int reason,
                               DateTime boxRequestedDate,
                               bool weeklyNotifierOptin, DateTime initialMatchDate, string agencyName,
                               string realEstateLicense, string ssoId, bool? homeWeeklyOptIn = false)
        {
            string proc = "pr_NHS_SetProfile";
            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "g_user_id", DbType.String, userId);
            db.AddInParameter(cmd, "u_logon_name", DbType.String, logonName);
            db.AddInParameter(cmd, "u_first_name", DbType.String, firstName);
            db.AddInParameter(cmd, "u_last_name", DbType.String, lastName);
            // Password added 10/27/06
            db.AddInParameter(cmd, "u_user_security_password", DbType.String, password);
            db.AddInParameter(cmd, "PartnerId", DbType.String, partnerId);
            db.AddInParameter(cmd, "i_account_status", DbType.String, 1);
            if (dateRegistered == DateTime.MinValue)
                db.AddInParameter(cmd, "d_date_registered", DbType.DateTime, DBNull.Value);
            else
                db.AddInParameter(cmd, "d_date_registered", DbType.DateTime, dateRegistered);
            db.AddInParameter(cmd, "d_date_last_changed", DbType.String, DateTime.Now);
            db.AddInParameter(cmd, "middlename", DbType.String, middleName);
            db.AddInParameter(cmd, "lastlogon", DbType.String, DBNull.Value);
            db.AddInParameter(cmd, "u_address1", DbType.String, address1);
            db.AddInParameter(cmd, "u_address2", DbType.String, address2);
            db.AddInParameter(cmd, "u_state", DbType.String, state);
            db.AddInParameter(cmd, "u_city", DbType.String, city);
            db.AddInParameter(cmd, "u_dayphone", DbType.String, dayPhone);
            db.AddInParameter(cmd, "u_evephone", DbType.String, evePhone);
            db.AddInParameter(cmd, "u_fax", DbType.String, fax);
            db.AddInParameter(cmd, "u_postalcode", DbType.String, postalCode);
            db.AddInParameter(cmd, "u_MailList", DbType.String, mailList);
            db.AddInParameter(cmd, "u_ReferrerName", DbType.String, referrerName);
            db.AddInParameter(cmd, "u_MktOpt", DbType.String, marketOptIn);
            db.AddInParameter(cmd, "u_regMetro", DbType.String, regMetro);
            db.AddInParameter(cmd, "u_regPrice", DbType.String, regPrice);
            db.AddInParameter(cmd, "u_dayPhoneExt", DbType.String, DayPhoneExt);
            db.AddInParameter(cmd, "u_evePhoneExt", DbType.String, evePhoneExt);
            db.AddInParameter(cmd, "u_LeadOptIn", DbType.String, leadOptin);
            db.AddInParameter(cmd, "u_RegCity", DbType.String, regCity);
            db.AddInParameter(cmd, "u_RegPrefer", DbType.String, regPrefer);
            db.AddInParameter(cmd, "u_RegState", DbType.String, regState);
            db.AddInParameter(cmd, "age", DbType.String, age);
            db.AddInParameter(cmd, "u_RegBOYLInterest", DbType.Boolean, regBoylInterest);
            db.AddInParameter(cmd, "u_RegComingSoonInterest", DbType.Boolean, regComingSoonInterest);
            db.AddInParameter(cmd, "u_RegActiveAdultInterest", DbType.Boolean, regActiveAdultInterest);

            db.AddInParameter(cmd, "move_in_date", DbType.Int16, moveInDate);

            if (financePreference.Equals(0))
                db.AddInParameter(cmd, "finance_pref_id", DbType.Int16, DBNull.Value);
            else
                db.AddInParameter(cmd, "finance_pref_id", DbType.Int16, financePreference);

            if (reason.Equals(0))
                db.AddInParameter(cmd, "moving_reason_id", DbType.Int16, DBNull.Value);
            else
                db.AddInParameter(cmd, "moving_reason_id", DbType.Int16, reason);

            if (boxRequestedDate == DateTime.MinValue)
                db.AddInParameter(cmd, "d_BoxRequestedDate", DbType.DateTime, DBNull.Value);
            else
                db.AddInParameter(cmd, "d_BoxRequestedDate", DbType.DateTime, boxRequestedDate);
            db.AddInParameter(cmd, "u_WeeklyNotifierOptIn", DbType.Boolean, weeklyNotifierOptin);
            if (initialMatchDate == DateTime.MinValue)
                db.AddInParameter(cmd, "d_InitialMatchDate", DbType.DateTime, DBNull.Value);
            else
                db.AddInParameter(cmd, "d_InitialMatchDate", DbType.DateTime, initialMatchDate);

            db.AddInParameter(cmd, "agency_name", DbType.String, agencyName);
            db.AddInParameter(cmd, "license", DbType.String, realEstateLicense);
            db.AddInParameter(cmd, "sso_user_id", DbType.String, ssoId);
            db.AddInParameter(cmd, "u_WeeklyHomeOptIn", DbType.Boolean, homeWeeklyOptIn);

            db.ExecuteNonQuery(cmd);
        }

        /// <summary>
        /// User chooses not to continue as a NHS registered user and opt out from NHS mailers etc.
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="emailAddress"></param>
        /// <param name="marketOptOut"></param>
        /// <param name="deactivate"></param>
        /// <returns></returns>
        public Boolean UnSubscribe(int partnerId, string emailAddress, string marketOptOut, string deactivate)
        {
            Boolean valid = true;
            string proc = "pr_NHS_Unsubscribe";

            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerId);
            db.AddInParameter(cmd, "email_address", DbType.String, emailAddress);
            db.AddInParameter(cmd, "MarketingOptout", DbType.String, marketOptOut);
            db.AddInParameter(cmd, "Deactivate", DbType.String, deactivate);

            db.AddInParameter(cmd, "change_email", DbType.String, "N");
            db.AddInParameter(cmd, "new_email_address", DbType.String, DBNull.Value);

            int retVal = (int)db.ExecuteScalar(cmd);

            if (retVal == 0)
            {
                valid = false;
            }

            return valid;
        }

        //public DataTable GetMatchMakerResults(string userId, int partnerId, string regMetro, string regPrice,
        //                                      string regCity, string regPrefer, string regState, bool regBoylInterest,
        //                                      bool regComingSoonInterest,
        //                                      bool regActiveAdultInterest, DateTime initialMatchDate,
        //                                      int MoreResultsFlag, string GetMatchMakerCriteria)
        //{
        //    string proc = "pr_NHS_MatchMakerSearch";

        //    Database db = DatabaseFactory.CreateDatabase(_webDB);
        //    DbCommand cmd = db.GetStoredProcCommand(proc);
        //    cmd.CommandTimeout = COMMAND_TIMEOUT;

        //    db.AddInParameter(cmd, "userid", DbType.String, userId);
        //    db.AddInParameter(cmd, "partnerid", DbType.Int32, partnerId);
        //    db.AddInParameter(cmd, "marketid", DbType.Int32, Convert.ToInt32(regMetro));
        //    db.AddInParameter(cmd, "pricerangeid", DbType.Int32, Convert.ToInt32(regPrice));

        //    db.AddInParameter(cmd, "city", DbType.String, regCity);
        //    db.AddInParameter(cmd, "state", DbType.String, regState);
        //    db.AddInParameter(cmd, "boylflag", DbType.Int32, Convert.ToInt32(regBoylInterest));
        //    db.AddInParameter(cmd, "adultflag", DbType.Int32, Convert.ToInt32(regActiveAdultInterest));
        //    db.AddInParameter(cmd, "comingflag", DbType.Int32, Convert.ToInt32(regComingSoonInterest));
        //    db.AddInParameter(cmd, "Records", DbType.Int32, MoreResultsFlag == 1 ? 5 : 10);
        //    db.AddInParameter(cmd, "exclude", DbType.String, GetMatchMakerCriteria);
        //    db.AddInParameter(cmd, "regprefer", DbType.String, regPrefer);

        //    return db.ExecuteDataSet(cmd).Tables[0];
        //}

        public int GetListingId(int planSpecId, bool isSpec)
        {
            int listingID = 0;
            string proc = "pr_NHS_GetListingId";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "@planOrSpecId", DbType.Int32, planSpecId);
            db.AddInParameter(cmd, "@isSpec", DbType.Boolean, isSpec);
            db.AddOutParameter(cmd, "@listingId", DbType.Int32, 4);
            db.ExecuteNonQuery(cmd);

            string listingId = DBValue.GetString(db.GetParameterValue(cmd, "@listingId"));
            if (listingId.Length > 0)
                listingID = int.Parse(listingId);
            return listingID;
        }

        public void UserUpdateLastMatchDate(string userId, DateTime lastMatchesSentDate)
        {
            string proc = "pr_NHS_UserUpdateLastMatchDate";
            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "g_user_id", DbType.String, userId);
            db.AddInParameter(cmd, "d_LastMatchesSentDate", DbType.DateTime, lastMatchesSentDate);

            db.ExecuteNonQuery(cmd);
        }

        #endregion

        #region Search Alerts

        public int AddSearchAlert(string userId, string title, string state, int marketId, string city,
                                  string postalCode, int? radius
                                  , int priceMin, int priceMax, int? bedrooms, bool singleFamilyHomes,
                                  bool condoTownHomes, bool buildOnYourLot, bool pool,
                                  bool golfCourse, bool gatedCommunity, int? schoolDistrictId, int? builderId)
        {
            string proc = "pr_NHS_AddSearchAlert";

            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddOutParameter(cmd, "@alert_id", DbType.Int32, 4);
            db.AddInParameter(cmd, "@g_user_id", DbType.String, userId);
            db.AddInParameter(cmd, "@title", DbType.String, title);
            db.AddInParameter(cmd, "@state", DbType.String, state);
            db.AddInParameter(cmd, "@market_id", DbType.Int32, marketId);
            db.AddInParameter(cmd, "@city", DbType.String, city);
            db.AddInParameter(cmd, "@postal_code", DbType.String, postalCode);
            db.AddInParameter(cmd, "@radius", DbType.Int32, radius);
            db.AddInParameter(cmd, "@price_min", DbType.Double, priceMin);
            db.AddInParameter(cmd, "@price_max", DbType.Double, priceMax);
            db.AddInParameter(cmd, "@bedrooms", DbType.Int32, bedrooms);
            db.AddInParameter(cmd, "@single_family_homes", DbType.Boolean, singleFamilyHomes);
            db.AddInParameter(cmd, "@condo_townhome_homes", DbType.Boolean, condoTownHomes);
            db.AddInParameter(cmd, "@boyl", DbType.Boolean, buildOnYourLot);
            db.AddInParameter(cmd, "@pool", DbType.Boolean, pool);
            db.AddInParameter(cmd, "@golf_course", DbType.Boolean, golfCourse);
            db.AddInParameter(cmd, "@gated_community", DbType.Boolean, gatedCommunity);
            db.AddInParameter(cmd, "@school_district_id", DbType.Int32, schoolDistrictId);
            db.AddInParameter(cmd, "@builder_id", DbType.Int32, builderId);

            db.ExecuteNonQuery(cmd);

            return cmd.Parameters[0].Value != DBNull.Value ? (int)cmd.Parameters[0].Value : -1;
        }

        public void SearchAlertIncrementNoMatchCount(string userId)
        {
            string proc = "pr_NHS_SearchAlertIncrementNoMatchCount";
            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@g_user_id", DbType.String, userId);
            db.ExecuteNonQuery(cmd);
        }

        public void SearchAlertResetNoMatchCount(string userId)
        {
            string proc = "pr_NHS_SearchAlertResetNoMatchCount";
            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@g_user_id", DbType.String, userId);
            db.ExecuteNonQuery(cmd);
        }

        public DataTable SearchAlertGetNoMatchCount(string userId)
        {
            string proc = "pr_NHS_SearchAlertGetNoMatchCount";
            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@g_user_id", DbType.String, userId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        //public void SearchAlertUpdateNoMatchCount(string userId, int noMatchEmailCount)
        //{
        //    string proc = "pr_NHS_SearchAlertUpdateNoMatchCount";
        //    Database db = DatabaseFactory.CreateDatabase(_profilesDB);
        //    DbCommand cmd = db.GetStoredProcCommand(proc);
        //    cmd.CommandTimeout = COMMAND_TIMEOUT;

        //    db.AddInParameter(cmd, "@g_user_id", DbType.String, userId);
        //    db.AddInParameter(cmd, "@no_match_email_count", DbType.Int32, noMatchEmailCount);

        //    db.ExecuteNonQuery(cmd);
        //}

        public DataTable GetSearchAlertsByUserId(string userId)
        {
            string proc = "pr_NHS_GetSearchAlertsByUserId";
            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "@g_user_id", DbType.String, userId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetSearchAlertByAlertId(int alertId)
        {
            string proc = "pr_NHS_GetSearchAlertByAlertId";
            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "@alert_id", DbType.Int32, alertId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public void SearchAlertSaveSingleResult(int alertId, int commId, int builderId)
        {
            string proc = "pr_NHS_SearchAlertSaveSingleResult";
            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@alert_id", DbType.Int32, alertId);
            db.AddInParameter(cmd, "@comm_id", DbType.Int32, commId);
            db.AddInParameter(cmd, "@builder_id", DbType.Int32, builderId);
            db.AddInParameter(cmd, "@status_id", DbType.Int32, 1);
            db.AddInParameter(cmd, "@date_created", DbType.DateTime, DateTime.Now);
            db.AddInParameter(cmd, "@date_removed", DbType.DateTime, DBNull.Value);
            db.ExecuteNonQuery(cmd);
        }

        public void SearchAlertRemove(int alertId)
        {
            // delete results linked to alert
            // delete alert
            string proc = "pr_NHS_SearchAlertRemove";
            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "@alert_id", DbType.Int32, alertId);
            db.ExecuteNonQuery(cmd);
        }

        //public void SearchAlertRemoveResult(int alertId, int communityId, int builderId)
        //{
        //    string proc = "pr_NHS_SearchAlertRemoveResult";
        //    Database db = DatabaseFactory.CreateDatabase(_profilesDB);
        //    DbCommand cmd = db.GetStoredProcCommand(proc);
        //    cmd.CommandTimeout = COMMAND_TIMEOUT;

        //    db.AddInParameter(cmd, "@alert_id", DbType.Int32, alertId);
        //    db.AddInParameter(cmd, "@comm_id", DbType.Int32, communityId);
        //    db.AddInParameter(cmd, "@builder_id", DbType.Int32, builderId);
        //    db.ExecuteNonQuery(cmd);
        //}

        //public void SearchAlertRemoveResults(int communityId, int builderId)
        //{
        //    string proc = "pr_NHS_SearchAlertRemoveResults";
        //    Database db = DatabaseFactory.CreateDatabase(_profilesDB);
        //    DbCommand cmd = db.GetStoredProcCommand(proc);
        //    cmd.CommandTimeout = COMMAND_TIMEOUT;
        //    db.AddInParameter(cmd, "@comm_id", DbType.Int32, communityId);
        //    db.AddInParameter(cmd, "@builder_id", DbType.Int32, builderId);
        //    db.ExecuteNonQuery(cmd);
        //}

        //public DataTable SearchAlertGetResults(int alertId)
        //{
        //    string proc = "pr_NHS_SearchAlertGetResults";
        //    Database db = DatabaseFactory.CreateDatabase(_profilesDB);
        //    DbCommand cmd = db.GetStoredProcCommand(proc);
        //    cmd.CommandTimeout = COMMAND_TIMEOUT;

        //    db.AddInParameter(cmd, "@alert_id", DbType.Int32, alertId);

        //    return db.ExecuteDataSet(cmd).Tables[0];
        //}

        public void SearchAlertUnsubscribeUser(string userId)
        {
            string proc = "pr_NHS_SearchAlertUnsubscribeUser";
            Database db = DatabaseFactory.CreateDatabase(ProfilesDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "@user_id", DbType.String, userId);
            db.ExecuteNonQuery(cmd);
        }

        public int GetQuickMoveInHomesCountForMarket(int marketId, int partnerId)
        {
            string proc = "pr_NHS_GetQuickMoveInHomesCountForMarket";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "@marketid", DbType.Int32, marketId);
            db.AddInParameter(cmd, "@partnerid", DbType.Int32, partnerId);

            return (int)db.ExecuteScalar(cmd);
        }

        public int GetCommunitiesCountForMarket(int marketId, int partnerId)
        {
            string proc = "pr_NHS_GetCommunitiesCountForMarket";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "@marketid", DbType.Int32, marketId);
            db.AddInParameter(cmd, "@partnerid", DbType.Int32, partnerId);

            return (int)db.ExecuteScalar(cmd);
        }

        #endregion

        #region "Leads & Lead Actions"

        public int InsertRequest(Hashtable userInfo, string leadType)
        {
            string proc = "pr_InsertRequest";
            SqlParameter[] aParms = new SqlParameter[31];

            aParms[0] = new SqlParameter("@profile_guid", SqlDbType.VarChar, 38);
            aParms[0].Value = userInfo["profile_guid"];
            aParms[1] = new SqlParameter("@first_name", SqlDbType.VarChar, 50);
            aParms[1].Value = userInfo["FName"];
            aParms[2] = new SqlParameter("@last_name", SqlDbType.VarChar, 50);
            aParms[2].Value = userInfo["LName"];
            aParms[3] = new SqlParameter("@address1", SqlDbType.VarChar, 100);
            aParms[3].Value = userInfo["Address1"];
            aParms[4] = new SqlParameter("@city", SqlDbType.VarChar, 50);
            aParms[4].Value = userInfo["City"];
            aParms[5] = new SqlParameter("@state", SqlDbType.Char, 2);
            aParms[5].Value = userInfo["State"];
            aParms[6] = new SqlParameter("@postal_code", SqlDbType.VarChar, 5);
            aParms[6].Value = userInfo["PostalCode"];
            aParms[7] = new SqlParameter("@email_address", SqlDbType.VarChar, 50);
            aParms[7].Value = userInfo["Email"];
            aParms[8] = new SqlParameter("@day_phone", SqlDbType.VarChar, 20);
            aParms[8].Value = userInfo["PhoneDay"];
            aParms[9] = new SqlParameter("@dayphoneExt", SqlDbType.VarChar, 10);
            aParms[9].Value = userInfo["PhoneDayExt"];
            aParms[10] = new SqlParameter("@Partner_ID", SqlDbType.Int, 4);
            aParms[10].Value = Convert.ToInt32(userInfo["PartnerID"]);
            aParms[11] = new SqlParameter("@Refer", SqlDbType.VarChar, 10);
            aParms[11].Value = userInfo["Refer"];
            aParms[12] = new SqlParameter("@move_in_days", SqlDbType.VarChar, 3);
            aParms[12].Value = userInfo["TargetMoveInDate"];
            aParms[13] = new SqlParameter("@financing_preference", SqlDbType.VarChar, 40);
            aParms[13].Value = userInfo["FinancePreference"];
            aParms[14] = new SqlParameter("@comment_text", SqlDbType.VarChar, 1000);
            aParms[14].Value = userInfo["Comments"];
            aParms[15] = new SqlParameter("@market_id", SqlDbType.Int, 4);
            aParms[15].Value = Convert.ToInt32(userInfo["SearchMarketID"]);
            aParms[16] = new SqlParameter("@price_range_id", SqlDbType.Int, 4);

            try
            {
                aParms[16].Value = Convert.ToInt32(userInfo["PriceRangeID"].ToString());
            }
            catch (Exception)
            {
            }

            aParms[17] = new SqlParameter("@request_type_code", SqlDbType.VarChar, 3);
            aParms[17].Value = leadType;
            aParms[18] = new SqlParameter("@prefer_flag", SqlDbType.Char, 1);
            aParms[18].Value = userInfo["SearchPref"];
            aParms[19] = new SqlParameter("@prefer_city", SqlDbType.VarChar, 50);
            aParms[19].Value = userInfo["SearchCity"];
            aParms[20] = new SqlParameter("@lead_page", SqlDbType.VarChar, 10);
            aParms[20].Value = userInfo["LeadPage"];
            aParms[21] = new SqlParameter("@lead_page_action", SqlDbType.VarChar, 10);
            aParms[21].Value = userInfo["LeadPageAction"];
            aParms[22] = new SqlParameter("@reason_id", SqlDbType.Int, 4);

            try
            {
                aParms[22].Value = Convert.ToInt32(userInfo["ReasonForMoving"]);
            }
            catch (Exception)
            {
            }

            aParms[23] = new SqlParameter("@include_boyol", SqlDbType.Char, 1);

            try
            {
                if (Convert.ToBoolean(userInfo["includeBOYOL"]))
                    aParms[23].Value = 'Y';
            }
            catch (Exception)
            {
            }

            aParms[24] = new SqlParameter("@include_active_adult", SqlDbType.Char, 1);

            try
            {
                if (Convert.ToBoolean(userInfo["includeActiveAdult"]))
                    aParms[24].Value = 'Y';
            }
            catch (Exception)
            {
            }

            aParms[25] = new SqlParameter("@request_id", SqlDbType.Int, 4);
            aParms[25].Direction = ParameterDirection.Output;

            aParms[26] = new SqlParameter("@session_guid", SqlDbType.VarChar, 38);
            aParms[26].Value = userInfo["SessionID"];
            aParms[27] = new SqlParameter("@GUID", SqlDbType.VarChar, 38);
            aParms[27].Value = userInfo["UserCookie"];
            aParms[28] = new SqlParameter("@evening_phone", SqlDbType.VarChar, 20);
            aParms[28].Value = userInfo["SecondPhone"];
            aParms[29] = new SqlParameter("@evephoneExt", SqlDbType.VarChar, 10);
            aParms[29].Value = userInfo["SecondPhoneExt"];
            aParms[30] = new SqlParameter("@title", SqlDbType.VarChar, 3);
            aParms[30].Value = userInfo["title"];

            Database db = DatabaseFactory.CreateDatabase(TransactionDb);
            db.ExecuteNonQuery(proc, aParms);

            return (int)aParms[25].Value;
        }

        public int InsertRequestItem(int requestId, DataRow requestItem)
        {
            string proc = "pr_InsertRequestItem";
            SqlParameter[] aParms = new SqlParameter[6];

            aParms[0] = new SqlParameter("@request_id", SqlDbType.Int, 4);
            aParms[0].Value = requestId;

            aParms[1] = new SqlParameter("@builder_id", SqlDbType.Int, 4);
            if (!requestItem.IsNull("builderid"))
            {
                aParms[1].Value = Convert.ToInt32(requestItem["builderid"].ToString());
            }

            aParms[2] = new SqlParameter("@community_id", SqlDbType.Int, 4);
            if (!requestItem.IsNull("communityid"))
            {
                aParms[2].Value = Convert.ToInt32(requestItem["communityid"].ToString());
            }

            aParms[3] = new SqlParameter("@plan_id", SqlDbType.Int, 4);
            if (!requestItem.IsNull("planid"))
            {
                aParms[3].Value = Convert.ToInt32(requestItem["planid"].ToString());
            }

            aParms[4] = new SqlParameter("@specification_id", SqlDbType.Int, 4);
            if (!requestItem.IsNull("specificationid"))
            {
                aParms[4].Value = Convert.ToInt32(requestItem["specificationid"].ToString());
            }

            aParms[5] = new SqlParameter("@request_item_id", SqlDbType.Int, 4);
            aParms[5].Direction = ParameterDirection.Output;

            Database db = DatabaseFactory.CreateDatabase(TransactionDb);
            db.ExecuteNonQuery(proc, aParms);

            return (int)aParms[5].Value;
        }

        public int InsertRequestItem(int requestId)
        {
            string proc = "pr_InsertRequestItem";
            SqlParameter[] aParms = new SqlParameter[6];
            aParms[0] = new SqlParameter("@request_id", SqlDbType.Int, 4);
            aParms[0].Value = requestId;
            aParms[1] = new SqlParameter("@builder_id", SqlDbType.Int, 4);
            aParms[2] = new SqlParameter("@community_id", SqlDbType.Int, 4);
            aParms[3] = new SqlParameter("@plan_id", SqlDbType.Int, 4);
            aParms[4] = new SqlParameter("@specification_id", SqlDbType.Int, 4);
            aParms[5] = new SqlParameter("@request_item_id", SqlDbType.Int, 4);
            aParms[5].Direction = ParameterDirection.Output;

            Database db = DatabaseFactory.CreateDatabase(TransactionDb);
            db.ExecuteNonQuery(proc, aParms);

            return (int)aParms[5].Value;
        }

        public DataSet GetRequestItemDelivery(int requestId)
        {
            string proc = "pr_GetRequestItemDelivery";

            SqlParameter[] aParms = new SqlParameter[1];
            aParms[0] = new SqlParameter("@request_id", SqlDbType.Int, 4);
            aParms[0].Value = requestId;

            Database db = DatabaseFactory.CreateDatabase(TransactionDb);

            return db.ExecuteDataSet(proc, aParms);
        }

        public void UpdateBrochureName(int requestItemDeliveryId, string brochureName)
        {
            string proc = "pr_UpdateBrochureName";

            Database db = DatabaseFactory.CreateDatabase(TransactionDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "request_item_delivery_id", DbType.Int32, requestItemDeliveryId);
            db.AddInParameter(cmd, "brochure_name", DbType.String, brochureName);
            db.ExecuteNonQuery(cmd);
        }

        public DataTable GetBrochure(string email, int communityId, int builderId, int planId, int specId,
                                     int partnerId, string requestTypeCode, string requestItemId)
        {
            var proc = string.IsNullOrWhiteSpace(requestItemId)
                              ? "pr_NHS_GetBrochure"
                              : "pr_NHS_GetBrochureByRequestItemId";
            Database db = DatabaseFactory.CreateDatabase(TransactionDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            if (string.IsNullOrWhiteSpace(requestItemId))
            {
                db.AddInParameter(cmd, "email", DbType.String, DBValue.EmptyStringToNull(email));
                db.AddInParameter(cmd, "community_id", DbType.Int32, communityId);
                db.AddInParameter(cmd, "builder_id", DbType.Int32, builderId);
                db.AddInParameter(cmd, "plan_id", DbType.Int32, planId);
                db.AddInParameter(cmd, "specification_id", DbType.Int32, specId);
                db.AddInParameter(cmd, "request_type_code", DbType.String, DBValue.EmptyStringToNull(requestTypeCode));
            }
            else
                db.AddInParameter(cmd, "request_item_id", DbType.Int32, Convert.ToInt32(requestItemId));
            db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        #endregion

        #region "Partner"

        public DataTable GetPartnerMaskIndexes()
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            const string proc = "pr_NHS_GetPartnerMaskIndexes";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetPartnerShell(int partnerId, bool workingCopy)
        {
            Database db = DatabaseFactory.CreateDatabase(TransactionDb);
            string proc = "pr_NHS_GetPartnerShell";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerId);
            db.AddInParameter(cmd, "working_copy", DbType.Boolean, workingCopy);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetPartnerInfo(int partnerId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string proc = "pr_NHS_GetPartnerInfo";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetPartnerSiteTerms(int partnerId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string proc = "pr_NHS_GetPartnerSiteTerms";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }


        public DataTable GetAllActivePrivateLabelPartners()
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string proc = "pr_NHS_GetAllActivePrivateLabelPartners";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetMarketsByBrand(int partnerId, string state, int brandId)
        {
            string proc = "pr_NHS_GetMarketsByBrand";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);
            db.AddInParameter(cmd, "BrandId", DbType.Int32, brandId);
            if (!string.IsNullOrEmpty(state))
                db.AddInParameter(cmd, "State", DbType.String, state);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetMarkets(string markets)
        {
            const string proc = "pr_NHS_GetMarketLatLongs";

            var db = DatabaseFactory.CreateDatabase(WebDbConnection);
            var cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "MarketIDs", DbType.String, markets);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetPartnerPaidMarkets(int partnerId)
        {
            string proc = "pr_NHS_GetPartnerMarkets";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetBasicListingMarkets(int partnerId)
        {
            string proc = "pr_NHS_GetBasicListingMarkets";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        //public DataTable GetPartnerMarketsWithHotDeals(int partnerId)
        //{
        //    string proc = "pr_NHS_GetPartnerMarketsWithHotDeals";

        //    Database db = DatabaseFactory.CreateDatabase(_webDB);
        //    DbCommand cmd = db.GetStoredProcCommand(proc);
        //    cmd.CommandTimeout = COMMAND_TIMEOUT;
        //    db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);

        //    return db.ExecuteDataSet(cmd).Tables[0];
        //}

        public DataTable GetAllPartnerSiteUrls()
        {
            string proc = "pr_NHS_GetAllPartnerSiteUrls";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataSet GetTypeAheadSuggestions(int partnerId)
        {
            string proc = "pr_NHS_GetPartnerLocationsIndex";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);

            return db.ExecuteDataSet(cmd);
        }

        public DataSet GetPartnerMarketsInfo(int partnerId)
        {
            string proc = "pr_NHS_GetPartnerMarketsInfo";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);

            return db.ExecuteDataSet(cmd);
        }
        #endregion

        #region "Realtor"

        public DataTable GetRealtorPartnerInfo(int partnerId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string proc = "pr_NHS_GetRealtorPartnerInfo";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetRealtorPartnerSiteTerms(int partnerId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string proc = "pr_NHS_GetPartnerSiteTerms";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetRealtorPartnerMarkets(int partnerId)
        {
            string proc = "pr_NHS_GetPartnerMarkets";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetRealtorQuestions()
        {
            string proc = "pr_NHS_GetRealtorInfoQuestions";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        #endregion

        #region Community

        public DataTable GetActiveCommunitiesForBrand(int partnerId, int brandId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string proc = "pr_NHS_GetActiveCommunitiesByPartnerAndBrand";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);
            db.AddInParameter(cmd, "BrandId", DbType.Int32, brandId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public bool GetCommunityBilledStatus(int partnerId, int builderId, int communityId)
        {
            const string proc = "pr_NHS_GetCommunityBilledStatus";
            bool status = false;
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "PartnerID", DbType.Int32, partnerId);
            db.AddInParameter(cmd, "BuilderID", DbType.Int32, builderId);
            db.AddInParameter(cmd, "CommunityID", DbType.Int32, communityId);

            DataSet ds = db.ExecuteDataSet(cmd);
            if (ds.Tables.Count == 1)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    var billed = DBValue.GetString(ds.Tables[0].Rows[0]["billed"]);
                    status = !string.IsNullOrEmpty(billed) &&
                             (billed.ToLower().Equals("y"));
                }
            }

            return status;

        }

        public DataTable IsCommunityActiveOnParentBrandPartner(int communityId, int brandPartnerId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string proc = "pr_NHS_IsCommunityActiveOnParentBrand";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "CommunityId", DbType.Int32, communityId);
            db.AddInParameter(cmd, "BrandPartnerId", DbType.Int32, brandPartnerId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetCommunity(int partnerID, int communityID, int builderID)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string proc = "pr_NHS_GetCommunityDetailType";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "community_id", DbType.Int32, communityID);
            db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerID);
            db.AddInParameter(cmd, "builder_id", DbType.Int32, builderID);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetCommunityMapCard(int partnerId, string communityList, string basicListingsIds)
        {
            var db = DatabaseFactory.CreateDatabase(WebDbConnection);
            const string proc = "pr_NHS_GetMapCardInfoForCommunities";
            var cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@PartnerId", DbType.Int32, partnerId);
            db.AddInParameter(cmd, "@CommunityIds", DbType.String, communityList);
            db.AddInParameter(cmd, "@BasicListingsIds", DbType.String, basicListingsIds);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetCommunityDetailByBuilder(int communityID, int partnerID, int builderID)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string proc = "pr_NHS_GetCommunityDetailBuilder";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "community_id", DbType.Int32, communityID);
            db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerID);
            db.AddInParameter(cmd, "builder_id", DbType.Int32, builderID);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public String GetCommunityIdBuilderIdBySubId(int subid)
        {
            string proc = "pr_NHS_GetCommunityIdBuilderIdBySubId";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "subid", DbType.Int32, subid);

            return (string)db.ExecuteScalar(cmd);

        }

        public DataTable GetCommunityAmenities(int communityId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string proc = "pr_NHS_GetCommunityAmenities";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "community_id", DbType.Int32, communityId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetOpenAmenities(int communityId, int builderId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string proc = "pr_NHS_GetOpenAmenities";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            db.AddInParameter(cmd, "community_id", DbType.Int32, communityId);
            db.AddInParameter(cmd, "builder_id", DbType.Int32, builderId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetCommunitySchools(int communityId, int partnerId, bool useHub)
        {
            Database db = useHub ? DatabaseFactory.CreateDatabase(HubDbConnection) : DatabaseFactory.CreateDatabase(WebDbConnection);
            string proc = "pr_NHS_GetCommunitySchool";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "community_id", DbType.Int32, communityId);
            db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetCommunityUtilities(int communityId, int partnerId, bool useHub)
        {
            Database db = useHub ? DatabaseFactory.CreateDatabase(HubDbConnection) : DatabaseFactory.CreateDatabase(WebDbConnection);
            string proc = "pr_NHS_GetCommunityUtilities";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "community_id", DbType.Int32, communityId);
            db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }


        public DataTable GetCommunitiesForTvBuilder(string communityIds, int marketId)
        {
            var db = DatabaseFactory.CreateDatabase(WebDbConnection);
            const string proc = "pr_NHS_GetCommunitiesForTvBuilder";
            var cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "CommunityList", DbType.String, communityIds);
            db.AddInParameter(cmd, "MarketId", DbType.Int32, marketId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        

        //public DataTable GetRelatedCommunities(int communityId, int partnerId, string factFlag)
        //{
        //    Database db = DatabaseFactory.CreateDatabase(_webDB);
        //    string proc = "pr_NHS_GetRelatedCommunities";
        //    DbCommand cmd = db.GetStoredProcCommand(proc);
        //    cmd.CommandTimeout = COMMAND_TIMEOUT;
        //    db.AddInParameter(cmd, "community_id", DbType.Int32, communityId);
        //    db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerId);
        //    db.AddInParameter(cmd, "fact_flag", DbType.Int32, factFlag);
        //    return db.ExecuteDataSet(cmd).Tables[0];
        //}

        public DataTable GetAmenityCounts(int partnerId, string state, int marketId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string proc = "pr_NHS_GetMarketAmmenityCounts";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerId);
            db.AddInParameter(cmd, "state", DbType.String, state);

            if (marketId != 0)
                db.AddInParameter(cmd, "market_id", DbType.Int32, marketId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        //public DataTable GetCommunityImages(int communityId, int builderId)
        //{
        //    Database db = DatabaseFactory.CreateDatabase(_webDB);
        //    string proc = "pr_NHS_GetCommunityImages";
        //    DbCommand cmd = db.GetStoredProcCommand(proc);
        //    cmd.CommandTimeout = COMMAND_TIMEOUT;
        //    db.AddInParameter(cmd, "community_id", DbType.Int32, communityId);
        //    db.AddInParameter(cmd, "builder_id", DbType.Int32, builderId);
        //    return db.ExecuteDataSet(cmd).Tables[0];
        //}

        public DataTable GetCustomerQuotes(int maxNoOfQuotes, int communityId, int builderId, bool useHub)
        {
            Database db = useHub ? DatabaseFactory.CreateDatabase(HubDbConnection) : DatabaseFactory.CreateDatabase(WebDbConnection);
            string proc = "pr_NHS_GetCustomerQuotes";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "community_id", DbType.Int32, communityId);
            db.AddInParameter(cmd, "max_rows", DbType.Int32, maxNoOfQuotes);
            db.AddInParameter(cmd, "builder_id", DbType.Int32, builderId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetBuilderCommunityDetail(int builderId, int communityId, int partnerId)
        {
            throw new NotImplementedException("this method has not been implemented");
        }

        public DataTable GetModelHomes(int builderId, int communityId, int partnerId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string proc = "pr_NHS_GetModelHomes";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "community_id", DbType.Int32, communityId);
            db.AddInParameter(cmd, "builder_id", DbType.Int32, builderId);
            db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        //public string GetPrintAdurl(int builderId, int communityId, int partnerId)
        //{
        //    Database db = DatabaseFactory.CreateDatabase(_webDB);
        //    string proc = "pr_NHS_GetPrintAdUrlforBC";
        //    DbCommand cmd = db.GetStoredProcCommand(proc);
        //    cmd.CommandTimeout = COMMAND_TIMEOUT;
        //    db.AddInParameter(cmd, "community_id", DbType.Int32, communityId);
        //    db.AddInParameter(cmd, "builder_id", DbType.Int32, builderId);
        //    db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerId);
        //    return (string)db.ExecuteScalar(cmd);

        //}

        public DataSet GetSampleHomes(int builderId, int communityId, int partnerId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string proc = "pr_NHS_GetSampleHomes";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "community_id", DbType.Int32, communityId);
            db.AddInParameter(cmd, "builder_id", DbType.Int32, builderId);
            db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerId);
            return db.ExecuteDataSet(cmd);
        }

        public DataTable GetSpotlightCommunities(int partnerId, int marketId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string proc = "pr_NHS_GetCommunitySpotlights";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);

            if (marketId != 0)
            {
                db.AddInParameter(cmd, "MarketId", DbType.Int32, marketId);
            }
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetBrandSpotlightCommunities(int partnerId, int brandId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string proc = "pr_NHS_GetBrandSpotlights";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);
            db.AddInParameter(cmd, "BrandId", DbType.Int32, brandId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetFeaturedSpotlightCommunities(int partnerID)
        {
            // Get Database
            Database database = DatabaseFactory.CreateDatabase(WebDbConnection);

            // Set Command
            DbCommand dbCommand = database.GetStoredProcCommand("pr_NHS_GetCommunityFeaturedSpotlights");
            dbCommand.CommandTimeout = CommandTimeout;

            // Set Parameters
            database.AddInParameter(dbCommand, "partner_id", DbType.Int32, partnerID);

            // Query Database
            DataSet dataSet = database.ExecuteDataSet(dbCommand);

            // Nothing found?
            return dataSet.Tables.Count == 0 ? null : dataSet.Tables[0];
        }

        public string GetCommunityVideoUrl(int communityId, int builderId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string proc = "pr_NHS_GetCommunityVideoUrl";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "community_id", DbType.Int32, communityId);
            db.AddInParameter(cmd, "builder_id", DbType.Int32, builderId);
            db.AddOutParameter(cmd, "video_url", DbType.String, 255);
            db.ExecuteNonQuery(cmd);
            return cmd.Parameters[2].Value != DBNull.Value ? (string)cmd.Parameters[2].Value : string.Empty;
        }

        public DataTable GetNearbyCommunities(int builderID, int communityID, int partnerID, int? geoLocationID,
                                              bool useHub)
        {
            // Get Database
            Database database = useHub ? DatabaseFactory.CreateDatabase(HubDbConnection) : DatabaseFactory.CreateDatabase(WebDbConnection);

            // Set Command
            DbCommand dbCommand = database.GetStoredProcCommand("pr_NHS_GetNearbyCommunities");
            dbCommand.CommandTimeout = CommandTimeout;

            // Set Parameters
            database.AddInParameter(dbCommand, "builder_id", DbType.Int32, builderID);
            database.AddInParameter(dbCommand, "community_id", DbType.Int32, communityID);
            database.AddInParameter(dbCommand, "partner_id", DbType.Int32, partnerID);
            database.AddInParameter(dbCommand, "geo_location_id", DbType.Int32, geoLocationID);

            // Query Database
            DataSet dataSet = database.ExecuteDataSet(dbCommand);

            // Nothing found?
            return dataSet.Tables.Count == 0 ? null : dataSet.Tables[0];
        }

        public DataTable GetNearbyCommunitiesByPlan(int builderID, int communityID, int planID, int partnerID)
        {
            // Get Database
            Database database = DatabaseFactory.CreateDatabase(WebDbConnection);

            // Set Command
            DbCommand dbCommand = database.GetStoredProcCommand("pr_NHS_GetNearbyCommunitiesByPlan");
            dbCommand.CommandTimeout = CommandTimeout;

            // Set Parameters
            database.AddInParameter(dbCommand, "builder_id", DbType.Int32, builderID);
            database.AddInParameter(dbCommand, "community_id", DbType.Int32, communityID);
            database.AddInParameter(dbCommand, "plan_id", DbType.Int32, planID);
            database.AddInParameter(dbCommand, "partner_id", DbType.Int32, partnerID);

            // Query Database
            DataSet dataSet = database.ExecuteDataSet(dbCommand);

            // Nothing found?
            return dataSet.Tables.Count == 0 ? null : dataSet.Tables[0];
        }

        public DataTable GetNearbyCommunitiesByPlanFeatures(int builderID, int communityID, double price,
                                                            int bedroomCount, int bathroomCount, int partnerID)
        {
            // Get Database
            Database database = DatabaseFactory.CreateDatabase(WebDbConnection);

            // Set Command
            DbCommand dbCommand = database.GetStoredProcCommand("pr_NHS_GetNearbyCommunitiesByPlanFeatures");
            dbCommand.CommandTimeout = CommandTimeout;

            // Set Parameters
            database.AddInParameter(dbCommand, "builder_id", DbType.Int32, builderID);
            database.AddInParameter(dbCommand, "community_id", DbType.Int32, communityID);
            database.AddInParameter(dbCommand, "price", DbType.Currency, price);
            database.AddInParameter(dbCommand, "bedroom_count", DbType.Int32, bedroomCount);
            database.AddInParameter(dbCommand, "bathroom_count", DbType.Int32, bathroomCount);
            database.AddInParameter(dbCommand, "partner_id", DbType.Int32, partnerID);

            // Query Database
            DataSet dataSet = database.ExecuteDataSet(dbCommand);

            // Nothing found?
            return dataSet.Tables.Count == 0 ? null : dataSet.Tables[0];
        }

        public DataTable GetCommunityPlansByFeatures(int builderID, int communityID, int planID, double price,
                                                     int bedroomCount, int bathroomCount)
        {
            // Get Database
            Database database = DatabaseFactory.CreateDatabase(WebDbConnection);

            // Set Command
            DbCommand dbCommand = database.GetStoredProcCommand("pr_NHS_GetCommunityPlansByFeatures");
            dbCommand.CommandTimeout = CommandTimeout;

            // Set Parameters
            database.AddInParameter(dbCommand, "builder_id", DbType.Int32, builderID);
            database.AddInParameter(dbCommand, "community_id", DbType.Int32, communityID);
            database.AddInParameter(dbCommand, "plan_id", DbType.Int32, planID);
            database.AddInParameter(dbCommand, "price", DbType.Currency, price);
            database.AddInParameter(dbCommand, "bedroom_count", DbType.Int32, bedroomCount);
            database.AddInParameter(dbCommand, "bathroom_count", DbType.Int32, bathroomCount);

            // Query Database
            DataSet dataSet = database.ExecuteDataSet(dbCommand);

            // Nothing found?
            return dataSet.Tables.Count == 0 ? null : dataSet.Tables[0];
        }

        public DataTable GetCommunityPromotions(int communityId, int builderId, bool useHub)
        {
            Database db = useHub ? DatabaseFactory.CreateDatabase(HubDbConnection) : DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand("pr_NHS_GetCommunityPromotions");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "community_id", DbType.Int32, communityId);
            db.AddInParameter(cmd, "builder_id", DbType.Int32, builderId);

            // Query Database
            DataSet dataSet = db.ExecuteDataSet(cmd);

            // Nothing found?
            return dataSet.Tables.Count == 0 ? null : dataSet.Tables[0];
        }

        public DataTable GetCommunityGreenPrograms(string communityId, bool useHub)
        {
            Database db = useHub ? DatabaseFactory.CreateDatabase(HubDbConnection) : DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand("pr_NHS_GetCommunityGreenPrograms");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "CommunityIDs", DbType.String, communityId);

            // Query Database
            DataSet dataSet = db.ExecuteDataSet(cmd);

            // Nothing found?
            return dataSet.Tables.Count == 0 ? null : dataSet.Tables[0];
        }

        public DataTable GetCommunityHotDeals(string communityIds)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand("pr_NHS_GetCommunityHotHomes");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@CommunityIDs", DbType.String, communityIds);

            // Query Database
            DataSet dataSet = db.ExecuteDataSet(cmd);

            // Nothing found?
            return dataSet.Tables.Count == 0 ? null : dataSet.Tables[0];
        }

        public DataTable GetCommunityHotDeals(int communityId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand("pr_NHS_GetCommunityHotHomes");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "community_id", DbType.Int32, communityId);

            // Query Database
            DataSet dataSet = db.ExecuteDataSet(cmd);

            // Nothing found?
            return dataSet.Tables.Count == 0 ? null : dataSet.Tables[0];
        }

        public DataTable GetCommunityGreenHomes(int communityId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand("pr_NHS_GetCommunityGreenHomes");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "community_id", DbType.Int32, communityId);

            // Query Database
            DataSet dataSet = db.ExecuteDataSet(cmd);

            // Nothing found?
            return dataSet.Tables.Count == 0 ? null : dataSet.Tables[0];
        }

        public DataTable GetFeaturedListingCommunities(int featuredListingId, int partnerId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string proc = "pr_NHS_GetFeaturedListingCommunities";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "FeaturedListingId", DbType.Int32, featuredListingId);
            db.AddInParameter(cmd, "PartnerID", DbType.Int32, partnerId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetAllFeaturedListingCommunities(int builderId, int marketId, int partnerId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string proc = "pr_NHS_GetAllFeaturedListingCommunities";
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "BuilderId", DbType.Int32, builderId);
            db.AddInParameter(cmd, "MarketId", DbType.Int32, marketId);
            db.AddInParameter(cmd, "PartnerID", DbType.Int32, partnerId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable CountRequestBrosure(int partherId, int marketId, int communityId, string email, string requestType, int timePeriodDays)
        {
            var db = DatabaseFactory.CreateDatabase(TransactionDb);
            const string proc = "pr_NHS_GetSentBrochureCountForUser";
            var cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "PartnerID", DbType.Int32, partherId);
            db.AddInParameter(cmd, "MarketID", DbType.Int32, marketId);
            db.AddInParameter(cmd, "CommunityID", DbType.Int32, communityId);
            db.AddInParameter(cmd, "Email", DbType.String, email);
            db.AddInParameter(cmd, "RequestType", DbType.String, requestType);
            db.AddInParameter(cmd, "TimePeriodDays", DbType.Int32, timePeriodDays);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        #endregion

        #region OwnerStories
        public void InsertOwnerStory(OwnerStory ownerStory)
        {
            string proc = "pr_NHS_InsertOwnerStory";

            Database db = DatabaseFactory.CreateDatabase(TransactionDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "first_name", DbType.String, ownerStory.FirstName);
            db.AddInParameter(cmd, "last_name", DbType.String, ownerStory.LastName);
            db.AddInParameter(cmd, "email", DbType.String, ownerStory.Email);
            db.AddInParameter(cmd, "builder_name", DbType.String, ownerStory.BuilderName);
            db.AddInParameter(cmd, "community_name", DbType.String, ownerStory.CommunityName);
            db.AddInParameter(cmd, "city", DbType.String, ownerStory.City);
            db.AddInParameter(cmd, "category", DbType.String, ownerStory.Category);
            db.AddInParameter(cmd, "testimonial", DbType.String, ownerStory.StoryDescription);
            db.AddInParameter(cmd, ownerStory.IsImage ? "image_path" : "video_path", DbType.String, ownerStory.FilePath);
            db.AddInParameter(cmd, "builder_id", DbType.Int32, ownerStory.BuilderId);
            db.AddInParameter(cmd, "community_id", DbType.Int32, ownerStory.CommunityId);

            db.ExecuteDataSet(cmd);
        }

        public void UpdateOwnerStory(OwnerStory ownerStory)
        {
            string proc = "pr_NHS_UpdateOwnersStories";

            Database db = DatabaseFactory.CreateDatabase(TransactionDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "testimonial_id", DbType.Int32, ownerStory.OwnerStoryId);
            db.AddInParameter(cmd, "status", DbType.Boolean, ownerStory.Status);
            db.AddInParameter(cmd, "city", DbType.String, ownerStory.City);
            db.AddInParameter(cmd, "testimonial", DbType.String, ownerStory.StoryDescription);
            db.AddInParameter(cmd, "category", DbType.String, ownerStory.Category);
            db.AddInParameter(cmd, "last_name", DbType.String, ownerStory.LastName);
            db.AddInParameter(cmd, "first_name", DbType.String, ownerStory.FirstName);
            db.AddInParameter(cmd, "builder_name", DbType.String, ownerStory.BuilderName);
            db.AddInParameter(cmd, "email", DbType.String, ownerStory.Email);
            db.AddInParameter(cmd, "community_name", DbType.String, ownerStory.CommunityName);
            db.AddInParameter(cmd, "builder_id", DbType.Int32, ownerStory.BuilderId);
            db.AddInParameter(cmd, "community_id", DbType.Int32, ownerStory.CommunityId);

            
            db.ExecuteNonQuery(cmd);
        }

        /// <summary>
        /// Proc used to retrieve all the owner stories saved in the transaction DB
        /// </summary>
        /// <param name="builderId">If builder id is provided, results will be filtered for that builder only.  This will be applied from Builder Show Case</param>
        /// <param name="includeAllStatus">if true is sent, then "status" parameter should be null. This means all the status (pending, not published and published) will be returned.</param>
        /// <param name="status">null for pending, true for published and false for not published</param>
        /// <returns></returns>
        public DataTable GetOwnerStoriesList(int? builderId, bool? includeAllStatus, bool? status)
        {
            string proc = "pr_NHS_SelectOwnersStories";
            Database db = DatabaseFactory.CreateDatabase(TransactionDb);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "builder_id", DbType.Int32, builderId);
            db.AddInParameter(cmd, "include_all_status", DbType.Boolean, includeAllStatus);
            db.AddInParameter(cmd, "status", DbType.Boolean, status);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        #endregion

        #region Market

        public DataTable GetMarketCities(int marketId, int partnerId)
        {
            string proc = "pr_NHS_GetMarketCities";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "MarketId", DbType.Int32, marketId);
            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetZipCodesByMarket(int marketId, string city, int returnCount)
        {
            string proc = "pr_NHS_GetMarketZipByCommunityCount";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "market_id", DbType.Int32, marketId);
            db.AddInParameter(cmd, "city", DbType.String, string.IsNullOrEmpty(city) ? null : city);
            db.AddInParameter(cmd, "return_count", DbType.Int16, returnCount);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        //public DataTable GetMarketCitiesByAmenity(int marketId, int partnerId, string amenity)
        //{
        //    string proc = "pr_NHS_GetMarketCitiesByAmenity";

        //    Database db = DatabaseFactory.CreateDatabase(_webDB);
        //    DbCommand cmd = db.GetStoredProcCommand(proc);
        //    cmd.CommandTimeout = COMMAND_TIMEOUT;

        //    db.AddInParameter(cmd, "MarketId", DbType.Int32, marketId);
        //    db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);
        //    db.AddInParameter(cmd, "AmenityType", DbType.String, amenity);

        //    return db.ExecuteDataSet(cmd).Tables[0];
        //}

        public DataTable GetMarketCitiesByBrand(int marketId, int partnerId, int brandId)
        {
            string proc = "pr_NHS_GetMarketCitiesByBrand";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "MarketId", DbType.Int32, marketId);
            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);
            db.AddInParameter(cmd, "BrandId", DbType.Int32, brandId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        //public DataTable GetMarketPostalCodes(int marketId, int partnerId)
        //{
        //    string proc = "pr_NHS_GetMarketPostalCodes";

        //    Database db = DatabaseFactory.CreateDatabase(_webDB);
        //    DbCommand cmd = db.GetStoredProcCommand(proc);
        //    cmd.CommandTimeout = COMMAND_TIMEOUT;

        //    db.AddInParameter(cmd, "MarketId", DbType.Int32, marketId);
        //    db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);

        //    return db.ExecuteDataSet(cmd).Tables[0];
        //}

        public DataTable GetMarketSchoolDistricts(int marketId, int partnerId)
        {
            string proc = "pr_NHS_GetMarketSchoolDistricts";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "MarketId", DbType.Int32, marketId);
            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        //public DataTable GetMarketCommunities(int marketId, int partnerId, string city)
        //{
        //    string proc = "pr_NHS_GetMarketCommunities";

        //    Database db = DatabaseFactory.CreateDatabase(_webDB);
        //    DbCommand cmd = db.GetStoredProcCommand(proc);
        //    cmd.CommandTimeout = COMMAND_TIMEOUT;

        //    db.AddInParameter(cmd, "MarketId", DbType.Int32, marketId);
        //    db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);

        //    if (!String.IsNullOrEmpty(city))
        //        db.AddInParameter(cmd, "City", DbType.String, city);

        //    return db.ExecuteDataSet(cmd).Tables[0];
        //}

        public DataTable GetMarketBCs(int marketId, int partnerId, string city)
        {
            string proc = "pr_NHS_GetMarketBCs";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "MarketId", DbType.Int32, marketId);
            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);

            if (!String.IsNullOrEmpty(city))
            {
                db.AddInParameter(cmd, "City", DbType.String, city);
            }
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetPartnerMarketPremiumUpgrade()
        {
            var db = DatabaseFactory.CreateDatabase(WebDbConnection);
            var cmd = db.GetStoredProcCommand("pr_NHS_GetPartnerMarketPremiumUpgrade");
            cmd.CommandTimeout = CommandTimeout;
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetStateBrands(int partnerId, string state)
        {
            string proc = "pr_NHS_GetStateBrands";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "State", DbType.String, state);
            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        //Created for the case 78231
        public DataTable GetBrandsByPartner(int partnerId)
        {
            const string proc = "pr_NHS_GetBrandsByPartner";
            var db = DatabaseFactory.CreateDatabase(WebDbConnection);
            var cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetMarketBrands(int marketId, int partnerId)
        {
            string proc = "pr_NHS_GetMarketBrands";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            if (marketId > 0)
                db.AddInParameter(cmd, "MarketId", DbType.Int32, marketId);
            else
                db.AddInParameter(cmd, "MarketId", DbType.Int32, DBNull.Value);

            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public int GetMarketIdFromPostalCode(string postalCode, int partnerId, bool boylFlag)
        {
            string proc = "pr_Nhs_GetMarketIdFromPostalCode";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);
            db.AddInParameter(cmd, "PostalCode", DbType.String, postalCode);
            db.AddInParameter(cmd, "BoylFlag", DbType.Boolean, boylFlag);
            db.AddOutParameter(cmd, "MarketId", DbType.Int32, 4);

            db.ExecuteNonQuery(cmd);

            return (int)cmd.Parameters[3].Value;
        }

        public string GetMarketInfoForCreateAccount(string postalCode)
        {
            string proc = "pr_Nhs_GetMarketInfoByPostalCode";
            var mkInfo = string.Empty;
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "PostalCode", DbType.String, postalCode);

            DataSet ds = db.ExecuteDataSet(cmd);
            if (ds.Tables.Count == 1)
            {
                if (ds.Tables[0].Rows.Count > 0)
                    mkInfo = DBValue.GetString(ds.Tables[0].Rows[0]["market_name"]) + "," +
                             DBValue.GetString(ds.Tables[0].Rows[0]["market_id"]) + "," +
                             DBValue.GetString(ds.Tables[0].Rows[0]["city"]) + "," +
                             DBValue.GetString(ds.Tables[0].Rows[0]["state"]);
            }

            return mkInfo;

        }

        public int GetMarketIdFromCounty(string county, string state)
        {
            string proc = "pr_NHS_GeoCodeCounty";
            int marketId = 0;
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "county", DbType.String, county);
            db.AddInParameter(cmd, "state", DbType.String, state);

            DataSet ds = db.ExecuteDataSet(cmd);
            if (ds.Tables.Count == 1)
            {
                if (ds.Tables[0].Rows.Count > 0)
                    marketId = DBValue.GetInt(ds.Tables[0].Rows[0]["primary_market_id"]);
            }

            return marketId;
        }

        public void GetMarketIdFromCity(string city, string state, bool isBasicListing, ref int marketId,
                                        ref string cityOutput)
        {
            string proc = "pr_Nhs_GetMarketIdFromCity";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "City", DbType.String, city);
            db.AddInParameter(cmd, "State", DbType.String, state);
            if (isBasicListing)
                db.AddInParameter(cmd, "IsBasicListing", DbType.Boolean, isBasicListing);
            else
                db.AddInParameter(cmd, "IsBasicListing", DbType.Boolean, DBNull.Value);

            db.AddOutParameter(cmd, "MarketId", DbType.Int32, 4);
            db.AddOutParameter(cmd, "CityOutput", DbType.String, 50);

            db.ExecuteNonQuery(cmd);

            if (cmd.Parameters[3].Value != DBNull.Value)
            {
                marketId = (int)cmd.Parameters[3].Value;
            }

            if (cmd.Parameters[4].Value != DBNull.Value)
            {
                cityOutput = (string)cmd.Parameters[4].Value;
            }
            else
            {
                cityOutput = string.Empty;
            }
        }

        public DataTable GetMarketIdsFromCity(string city, string state)
        {
            string proc = "pr_NHS_GetMarketIdsFromCity";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "city", DbType.String, city);
            db.AddInParameter(cmd, "state", DbType.String, state);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetFeaturedListings(int marketId, int partnerId)
        {
            const string proc = "pr_NHS_FeaturedListingsByMarket";

            var db = DatabaseFactory.CreateDatabase(WebDbConnection);
            var cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "MarketId", DbType.Int32, marketId > 0 ? marketId : 0);

            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetRandomFeaturedCommunities(int marketId, int partnerId, int returnRecords = 3)
        {
            const string proc = "pr_NHS_GetRandomFeaturedListingCommunities";
            var db = DatabaseFactory.CreateDatabase(WebDbConnection);
            var cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);
            db.AddInParameter(cmd, "MarketId", DbType.Int32, marketId > 0 ? marketId : 0);
            db.AddInParameter(cmd, "ReturnCount", DbType.Int32, returnRecords);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetRandomCommunitiesInMarket(int marketId, int partnerId, int returnRecords = 3)
        {
            const string proc = "pr_NHS_GetRandomCommunitiesInMarket";
            var db = DatabaseFactory.CreateDatabase(WebDbConnection);
            var cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);
            db.AddInParameter(cmd, "MarketId", DbType.Int32, marketId > 0 ? marketId : 0);
            db.AddInParameter(cmd, "ReturnCount", DbType.Int32, returnRecords);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        #endregion

        #region Listing

        #region Plan

        public string GetTollFreeNumber(int partnerId, int communityId, int builderId)
        {
            string proc = "pr_NHS_GetTollFreeNumber";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerId);
            db.AddInParameter(cmd, "community_id", DbType.Int32, communityId);
            db.AddInParameter(cmd, "builder_id", DbType.Int32, builderId);
            object tollFreeNumber = db.ExecuteScalar(cmd);
            return tollFreeNumber != null ? DBValue.GetString(tollFreeNumber) : string.Empty;
        }

        public DataTable GetPlanInfo(int planID, int partnerID)
        {
            string proc = "pr_NHS_GetHomeDetailByPlan";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "planid", DbType.Int32, planID);
            db.AddInParameter(cmd, "partnerid", DbType.Int32, partnerID);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        /// <summary>
        /// Deprecated? Not referenced in code.
        /// </summary>
        /// <param name="planID"></param>
        /// <param name="partnerID"></param>
        /// <param name="includeDeactivePlans"></param>
        /// <returns></returns>
        public DataTable GetPlanInfo(int planID, int partnerID, bool includeDeactivePlans)
        {
            if (includeDeactivePlans)
            {
                string proc = "pr_NHS_GetHomeDetailByPlanPlusDeactives";
                Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
                DbCommand cmd = db.GetStoredProcCommand(proc);
                cmd.CommandTimeout = CommandTimeout;
                db.AddInParameter(cmd, "planid", DbType.Int32, planID);
                return db.ExecuteDataSet(cmd).Tables[0];
            }
            return this.GetPlanInfo(planID, partnerID);
        }

        public DataTable GetHomeSummaryByPlan(int planID, bool useHub)
        {
            string proc = "pr_NHS_GetHomeSummaryByPlan";
            Database db = useHub ? DatabaseFactory.CreateDatabase(HubDbConnection) : DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "planid", DbType.Int32, planID);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetHomeOptionsForSearchOption(int planID, int builderID, int communityID,
                                                       int partnerID, bool useHub)
        {
            string proc = "pr_NHS_GetHomeOptionsForSearchOption";
            Database db = useHub ? DatabaseFactory.CreateDatabase(HubDbConnection) : DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "planid", DbType.Int32, planID);
            db.AddInParameter(cmd, "builderid", DbType.Int32, builderID);
            db.AddInParameter(cmd, "communityid", DbType.Int32, communityID);
            db.AddInParameter(cmd, "partnerid", DbType.Int32, partnerID);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetRelatedHomes(int builderID, int planID, int bedRooms)
        {
            string proc = "pr_NHS_GetRelatedHomes";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "planid", DbType.Int32, planID);
            db.AddInParameter(cmd, "builderid", DbType.Int32, builderID);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetQuickMoveIn(int communityID, int builderID, int planID, int specificationID)
        {
            // Get Database
            Database database = DatabaseFactory.CreateDatabase(WebDbConnection);

            // Set Command
            DbCommand dbCommand = database.GetStoredProcCommand("pr_NHS_GetQuickMoveIn");
            dbCommand.CommandTimeout = CommandTimeout;

            // Set Parameters
            database.AddInParameter(dbCommand, "community_id", DbType.Int32, communityID);
            database.AddInParameter(dbCommand, "builder_id", DbType.Int32, builderID);
            database.AddInParameter(dbCommand, "plan_id", DbType.Int32, planID);
            database.AddInParameter(dbCommand, "specification_id", DbType.Int32, specificationID);

            // Query Database
            DataSet dataSet = database.ExecuteDataSet(dbCommand);

            // Nothing found?
            return dataSet.Tables.Count == 0 ? null : dataSet.Tables[0];
        }

        #endregion

        #region Spec

        public DataTable GetSpecInfo(int specID)
        {
            string proc = "pr_NHS_GetHomeDetailBySpec";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "specificationid", DbType.Int32, specID);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        /// <summary>
        /// This method is used to retrieve Home Details by Spec, including homes that are not active.
        /// </summary>
        /// <param name="specID">spec ID</param>
        /// <returns>a populated Datatable</returns>
        public DataTable GetSpecInfoIncludingDeactives(int specID)
        {
            string proc = "pr_NHS_GetHomeDetailBySpecPlusDeactives";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "specificationid", DbType.Int32, specID);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetHomeSummaryBySpec(int specID, bool useHub)
        {
            string proc = "pr_NHS_GetHomeSummaryBySpec";
            Database db = useHub ? DatabaseFactory.CreateDatabase(HubDbConnection) : DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "specificationid", DbType.Int32, specID);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        #endregion

        #region Common

        public DataTable GetListing(int listingId)
        {
            string proc = "pr_NHS_GetListing";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "listing_id", DbType.Int32, listingId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetImages(int planID, int communityID, int builderID, int specID, string type)
        {
            string proc = "pr_NHS_GetImages";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "planid", DbType.Int32, planID);
            db.AddInParameter(cmd, "communityid", DbType.Int32, communityID);
            db.AddInParameter(cmd, "builderid", DbType.Int32, builderID);
            db.AddInParameter(cmd, "specid", DbType.Int32, specID);
            db.AddInParameter(cmd, "type", DbType.String, type);
            var ds = db.ExecuteDataSet(cmd);
            return ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();
        }

        public DataTable GetFloorPlanImages(int planID, int specID)
        {
            string proc = "pr_NHS_GetFloorPlanImages";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "plan_id", DbType.Int32, planID);
            db.AddInParameter(cmd, "specification_id", DbType.Int32, specID);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetSpotLightHomes(string partnerId, string marketId, string communityId, string builderId, string listingTypeFlag)
        {
            return GetSpotLightHomes(partnerId.ToType<int>(), communityId.ToType<int>(), marketId.ToType<int>(), builderId.ToType<int>(), listingTypeFlag);
        }

        public DataTable GetSpotLightHomes(int partnerId, int communityId, int marketId, int builderId, string listingTypeFlag, int maxNoHomes = 5, bool includeZeroPriced = true)
        {
            const string proc = "pr_NHS_GetSpotLightHomes";
            var db = DatabaseFactory.CreateDatabase(WebDbConnection);
            var cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "partner_id", DbType.String, partnerId);

            if (communityId > 0)
                db.AddInParameter(cmd, "community_id", DbType.String, communityId);

            if (marketId > 0)
                db.AddInParameter(cmd, "market_id", DbType.String, marketId);

            if (builderId > 0)
                db.AddInParameter(cmd, "builder_id", DbType.String, builderId);

            if (!string.IsNullOrEmpty(listingTypeFlag))
                db.AddInParameter(cmd, "listing_type_flag", DbType.String, listingTypeFlag);


            db.AddInParameter(cmd, "max_no_homes", DbType.Int32, maxNoHomes);
            db.AddInParameter(cmd, "include_zero_priced", DbType.Boolean, includeZeroPriced);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetAwardsAndRecognationLogos(int maxNoOfLogos, int communityId, int builderId)
        {
            string proc = "pr_NHS_GetAccreditationImages";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "max_no_images", DbType.Int32, maxNoOfLogos);
            db.AddInParameter(cmd, "community_id", DbType.Int32, communityId);
            db.AddInParameter(cmd, "builder_id", DbType.Int32, builderId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        #endregion

        #endregion

        #region CMS

        public DataTable GetCategoriesBySite(int siteId)
        {
            Database db = DatabaseFactory.CreateDatabase(CmsDb);
            string storedProcedureName = "pr_NHS_Get_Category_List_By_Site";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "site_id", DbType.Int32, siteId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetCategoryPositionData(DateTime targetDate, int siteId, int categoryId)
        {
            Database db = DatabaseFactory.CreateDatabase(CmsDb);
            string storedProcedureName = "pr_NHS_Get_Category_Position_Data";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            // target date is optional.  If passed as null, position data will
            // only contain "default" features.  If target date is specified, position
            // data will also contain any scheduled features. 
            if (targetDate == DateTime.Parse("1/1/1900"))
            {
                db.AddInParameter(cmd, "@target_date", DbType.DateTime, DBNull.Value);
            }
            else
            {
                db.AddInParameter(cmd, "@target_date", DbType.DateTime, targetDate);
            }
            db.AddInParameter(cmd, "@site_id", DbType.Int32, siteId);
            db.AddInParameter(cmd, "@category_id", DbType.Int32, categoryId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetCategoryBasic(int categoryId)
        {
            Database db = DatabaseFactory.CreateDatabase(CmsDb);
            string storedProcedureName = "pr_NHS_Get_Category_Basic";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@category_id", DbType.Int32, categoryId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetArticleByTitle(string articleTitle)
        {
            Database db = DatabaseFactory.CreateDatabase(CmsDb);
            string storedProcedureName = "pr_NHS_Get_Article_By_Title";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@content_title", DbType.String, articleTitle);
            return db.ExecuteDataSet(cmd).Tables[0];
        }



        public DataTable GetRelatedArticles(string fullTextSearchExpression, int contentId, int siteId)
        {
            Database db = DatabaseFactory.CreateDatabase(CmsDb);
            string storedProcedureName = "pr_NHS_Get_Related_Articles";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@full_text_expression", DbType.String, fullTextSearchExpression);
            db.AddInParameter(cmd, "@content_id", DbType.Int32, contentId);
            db.AddInParameter(cmd, "@site_id", DbType.Int32, siteId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetArticleKeywords(int contentId, int contentLanguage)
        {
            Database db = DatabaseFactory.CreateDatabase(CmsDb);
            string storedProcedureName = "pr_NHS_Get_Article_Keywords";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@content_id", DbType.Int32, contentId);
            db.AddInParameter(cmd, "@content_language", DbType.Int32, contentLanguage);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetArticleSponsor(int contentId, int contentLanguage)
        {
            Database db = DatabaseFactory.CreateDatabase(CmsDb);
            string storedProcedureName = "pr_NHS_Get_Article_Sponsor";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "@content_id", DbType.Int32, contentId);
            db.AddInParameter(cmd, "@content_language", DbType.Int32, contentLanguage);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetContentInfo(long contentId, int contentLanguage)
        {
            var db = DatabaseFactory.CreateDatabase(CmsDb);
            const string storedProcedureName = "pr_NHS_Get_Content_Info";
            var cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@content_id", DbType.Int32, contentId);
            db.AddInParameter(cmd, "@content_language", DbType.Int32, contentLanguage);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetCategoryIdForArticle(int articleId)
        {
            Database db = DatabaseFactory.CreateDatabase(CmsDb);
            string storedProcedureName = "pr_NHS_Get_Selected_Categories_By_Article";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@content_id", DbType.Int32, articleId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable FrontEndArticleSearch(int siteId, string keyword1, string keyword2, string keyword3,
                                               string keyword4)
        {
            Database db = DatabaseFactory.CreateDatabase(CmsDb);
            string storedProcedureName = "pr_NHS_Front_End_Article_Search";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@site_id", DbType.Int32, siteId);
            db.AddInParameter(cmd, "@keyword1", DbType.String, StringHelper.EmptyToNullString(keyword1));
            db.AddInParameter(cmd, "@keyword2", DbType.String, StringHelper.EmptyToNullString(keyword2));
            db.AddInParameter(cmd, "@keyword3", DbType.String, StringHelper.EmptyToNullString(keyword3));
            db.AddInParameter(cmd, "@keyword4", DbType.String, StringHelper.EmptyToNullString(keyword4));
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetTopLevelCategories(int siteId)
        {
            Database db = DatabaseFactory.CreateDatabase(CmsDb);
            string storedProcedurename = "pr_NHS_Get_Top_Level_Categories";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedurename);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@site_id", DbType.Int32, siteId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        #endregion

        #region Leads

        public DataTable GetLeadBuilderList(int partnerId, int marketId, string brandList)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string storedProcedureName = "pr_NHS_GetLeadBldrList";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@partner_id", DbType.Int32, partnerId);
            db.AddInParameter(cmd, "@market_id", DbType.Int32, marketId);
            db.AddInParameter(cmd, "@brand_ids", DbType.String, brandList);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetLeadCommunityList(int partnerId, int communityId, ref string parentCommunityName,
                                              ref string city, ref string state, ref string postalCode)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string storedProcedureName = "pr_NHS_GetLeadCommList";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@partner_id", DbType.Int32, partnerId);
            db.AddInParameter(cmd, "@community_id", DbType.Int32, communityId);
            db.AddOutParameter(cmd, "@parent_community_name", DbType.String, 100);
            db.AddOutParameter(cmd, "@city", DbType.String, 50);
            db.AddOutParameter(cmd, "@state", DbType.String, 2);
            db.AddOutParameter(cmd, "@postal_code", DbType.String, 11);
            DataTable communityList = db.ExecuteDataSet(cmd).Tables[0];
            parentCommunityName = DBValue.GetString(db.GetParameterValue(cmd, "@parent_community_name"));
            city = DBValue.GetString(db.GetParameterValue(cmd, "@city"));
            state = DBValue.GetString(db.GetParameterValue(cmd, "@state"));
            postalCode = DBValue.GetString(db.GetParameterValue(cmd, "@postal_code"));
            return communityList;
        }

        public DataTable GetBuildersByBrand(int partnerId, int marketId, object price, string brandList)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            string storedProcedureName = "pr_NHS_GetBuildersByBrand";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@partner_id", DbType.Int32, partnerId);
            db.AddInParameter(cmd, "@market_id", DbType.Int32, marketId);
            db.AddInParameter(cmd, "@price", DbType.Currency, price);
            db.AddInParameter(cmd, "@brand_ids", DbType.String, brandList);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public void GetPriceRangeId(ref string priceRangeId, int price)
        {
            Database db = DatabaseFactory.CreateDatabase(TransactionDb);
            string storedProcedureName = "pr_NHS_GetPriceRangeID";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@Price", DbType.Int32, price);
            db.AddOutParameter(cmd, "@PriceRangeID", DbType.String, 2);
            db.ExecuteNonQuery(cmd);
            priceRangeId = DBValue.GetString(db.GetParameterValue(cmd, "@PriceRangeId"));
        }

        public string VerifyStateZip(string state, string zip)
        {
            string storedProcedureName = "pr_NHS_VerifyStateZip";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@State", DbType.String, state);
            db.AddInParameter(cmd, "@zip", DbType.String, zip);
            db.AddOutParameter(cmd, "@verify", DbType.String, 1);
            db.ExecuteNonQuery(cmd);
            var verify = DBValue.GetString(db.GetParameterValue(cmd, "@verify"));
            return verify;
        }

        public DataTable GetRequestItemDeliveryInfo(int requestItemId)
        {
            string storedProcedureName = "pr_NHS_GetRequestItemDeliveryInfo";
            Database db = DatabaseFactory.CreateDatabase(TransactionDb);
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@request_item_id", DbType.Int32, requestItemId);
            DataSet dsReq = db.ExecuteDataSet(cmd);
            return dsReq != null ? dsReq.Tables[0] : null;
        }

        public DataTable GetRequest(int requestId)
        {
            string storedProcedureName = "pr_NHS_GetRequest";
            Database db = DatabaseFactory.CreateDatabase(TransactionDb);
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@Request_Id", DbType.Int32, requestId);
            DataSet dsReq = db.ExecuteDataSet(cmd);
            return dsReq != null ? dsReq.Tables[0] : null;
        }

        public void InsertPresentedCommunyties(int sourceCommunityId, string communityList, string email, string firstName,
                                               string lastName, string postalCode, int partnerId, int marketId)
        {
            const string proc = "pr_NHS_InsertPresentedCommunities";
            var db = DatabaseFactory.CreateDatabase(TransactionDb);
            var cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "source_community_id", DbType.Int32, sourceCommunityId);
            db.AddInParameter(cmd, "community_list", DbType.String, communityList);
            db.AddInParameter(cmd, "email ", DbType.String, email);
            db.AddInParameter(cmd, "postal_code", DbType.String, postalCode);
            db.AddInParameter(cmd, "first_name", DbType.String, firstName);
            db.AddInParameter(cmd, "last_name", DbType.String, lastName);
            db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerId);
            //db.AddInParameter(cmd, "market_id", DbType.Int32, marketId);

            db.ExecuteNonQuery(cmd);
        }

        public DataTable GetRequestBySourceRequestKey(string sourceRequestKey)
        {
            string storedProcedureName = "pr_NHS_GetRequestBySourceRequestKey";
            Database db = DatabaseFactory.CreateDatabase(TransactionDb);
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@source_request_key", DbType.String, sourceRequestKey);
            DataSet dsReq = db.ExecuteDataSet(cmd);
            return dsReq != null ? dsReq.Tables[0] : null;
        }

        public DataTable GetRequestItems(int requestId)
        {
            string storedProcedureName = "pr_NHS_GetRequestItems";
            Database db = DatabaseFactory.CreateDatabase(TransactionDb);
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@Request_Id", DbType.Int32, requestId);
            DataSet dsReq = db.ExecuteDataSet(cmd);
            return dsReq != null ? dsReq.Tables[0] : null;
        }

        public DataTable GetSavedHomes(int partnerId, string planList, string specList)
        {
            string storedProcedureName = "pr_NHS_GetSavedHomes";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@partner_id", DbType.String, partnerId.ToString());
            db.AddInParameter(cmd, "@plan_id", DbType.String, planList);
            db.AddInParameter(cmd, "@specification_id", DbType.String, specList);
            DataSet dsSavedHomes = db.ExecuteDataSet(cmd);
            return dsSavedHomes != null ? dsSavedHomes.Tables[0] : null;
        }

        public DataTable GetSavedCommunities(string partnerId, string communityId)
        {
            string storedProcedureName = "pr_NHS_GetSavedCommunities";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@partner_id", DbType.String, partnerId.ToString());
            db.AddInParameter(cmd, "@community_id", DbType.String, communityId.ToString());
            DataSet comms = db.ExecuteDataSet(cmd);
            return comms != null ? comms.Tables[0] : null;
        }

        //public DataTable GetSavedCommunities(string partnerId, string communityList, string builderList)
        //{
        //    return GetSavedCommunities(partnerId, communityList);
        //}

        public DataTable GetCommunitiesWithDistance(int partnerId, string communityList, string builderList,
                                                    decimal sourceCommLat, decimal sourceCommLng)
        {
            string storedProcedureName = "pr_NHS_GetSavedCommunities";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@partner_id", DbType.String, partnerId.ToString());
            db.AddInParameter(cmd, "@community_id", DbType.String, communityList.ToString());
            //db.AddInParameter(cmd, "@builder_id", DbType.String, builderList);
            db.AddInParameter(cmd, "@source_comm_lat", DbType.Decimal, sourceCommLat);
            db.AddInParameter(cmd, "@source_comm_lng", DbType.Decimal, sourceCommLng);
            DataSet comms = db.ExecuteDataSet(cmd);
            return comms != null ? comms.Tables[0] : null;
        }

        public DataTable GetCommunityBrochures(int communityId, int builderId)
        {
            const string storedProc = "pr_NHS_GetCommunityBrochurePaths";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(storedProc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@community_id", DbType.String, communityId.ToString());
            db.AddInParameter(cmd, "@builder_id", DbType.String, builderId.ToString());
            var brochures = db.ExecuteDataSet(cmd);
            return brochures != null ? brochures.Tables[0] : null;

        }

        public DataTable GetHomeBrochures(int planId, int specificationId)
        {
            const string storedProc = "pr_NHS_GetHomeBrochurePaths";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(storedProc);
            cmd.CommandTimeout = CommandTimeout;

            if (planId > 0)
                db.AddInParameter(cmd, "@plan_id", DbType.String, planId.ToString());
            else
                db.AddInParameter(cmd, "@plan_id", DbType.String, DBNull.Value);

            if (specificationId > 0)
                db.AddInParameter(cmd, "@specification_id", DbType.String, specificationId.ToString());
            else
                db.AddInParameter(cmd, "@specification_id", DbType.String, DBNull.Value);

            var brochures = db.ExecuteDataSet(cmd);
            return brochures != null ? brochures.Tables[0] : null;
        }

        public int GetBrandByBuilder(int builderId)
        {
            string proc = "pr_NHS_GetBrandByBuilder";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "BuilderId", DbType.Int32, builderId);

            return (int)db.ExecuteScalar(cmd);
        }


        #endregion

        #region "BoylSearch"

        public DataTable GetBoylResults(int marketId, int partnerId, int brandId = 0)
        {
            string proc = "pr_NHS_GetBoylResults";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);
            db.AddInParameter(cmd, "MarketId", DbType.Int32, marketId);
            if (brandId > 0)
                db.AddInParameter(cmd, "BrandId", DbType.Int32, brandId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetBoylResults(int partnerId)
        {
            string proc = "pr_NHS_GetBoylResults";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }


        public DataTable GetBoylResultsCnh(string marketName, string marketState)
        {
            const string proc = "pr_GetBuildersByMarket";

            var db = DatabaseFactory.CreateDatabase(CnhDb);
            var cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "MarketName", DbType.String, marketName);
            db.AddInParameter(cmd, "MarketState", DbType.String, marketState);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetZipCodesByPartner(int partnerId)
        {
            string proc = "pr_NHS_GetZipCodesByPartnerID";

            var db = DatabaseFactory.CreateDatabase(WebDbConnection);
            var cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        #endregion

        #region "BOYLlinks"

        /// <summary>
        /// Used to retrieve the BOYL builder/brand links on the BOYL search page
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public DataTable GetBoylLinks(int partnerId)
        {
            string proc = "pr_NHS_GetBoylLinks";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "PartnerId", DbType.Int32, partnerId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        #endregion

        #region "Search"

        public DataSet GetBuilderCache()
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand("pr_NHS_GetBuilderCache");

            return db.ExecuteDataSet(cmd);
        }

        public DataSet GetBrandCache()
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand("pr_NHS_GetBrandCache");

            return db.ExecuteDataSet(cmd);
        }

        public DataSet GetMarketCache()
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand("pr_NHS_GetMarketCache");
            cmd.CommandTimeout = 90;

            return db.ExecuteDataSet(cmd);
        }

        public DataSet GetCommunityCache()
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand("pr_NHS_GetCommunityCachev2");
            cmd.CommandTimeout = 90;

            DataSet ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetZipSearchData(string postalCode, int radius)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand("pr_NHS_GetSearchResultsByZip");
            cmd.CommandTimeout = 180;
            db.AddInParameter(cmd, "postalCode", DbType.String, postalCode);
            db.AddInParameter(cmd, "radius", DbType.Int16, radius);
            return db.ExecuteDataSet(cmd);
        }

        public DataSet GetMarketSearchCache(int marketId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);

            DbCommand cmd = db.GetStoredProcCommand("pr_NHS_GetSearchCachev2");
            cmd.CommandTimeout = 180;
            db.AddInParameter(cmd, "MarketId", DbType.Int32, marketId);
            return db.ExecuteDataSet(cmd);
        }

        public Hashtable GetNearbyCommsByZipCodes(string zipCode, int radius)
        {
            string storedProcedureName = "pr_NHS_GetNearbyCommsByZipCode";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "@zipcode", DbType.String, zipCode);
            db.AddInParameter(cmd, "@radius", DbType.Int32, radius);
            DataTable zipcodes = db.ExecuteDataSet(cmd).Tables[0];

            Hashtable filteredZipCodes = new Hashtable();
            foreach (DataRow row in zipcodes.Rows)
                filteredZipCodes.Add(row["community_id"].ToString() + "_" + row["builder_id"], row["postal_code"]);
            return filteredZipCodes;
        }

        #endregion

        #region Planner

        public DataTable GetRecommendedCommunities(string partnerId, string communityIds, string builderIds)
        {
            string storedProcedureName = "pr_NHS_GetRecommendedCommunities";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@partner_id", DbType.String, partnerId);
            db.AddInParameter(cmd, "@community_id", DbType.String, communityIds);
            db.AddInParameter(cmd, "@Builder_id", DbType.String, builderIds);
            DataSet dsRecomComms = db.ExecuteDataSet(cmd);
            return dsRecomComms != null ? dsRecomComms.Tables[0] : null;
        }

        #endregion

        #region LeadSubmitPage Procs

        public bool ValidateBuilderCommunity(int builderId, int communityId, int partnerId)
        {
            string proc = "pr_NHS_ValidateBuilderCommunity";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "builder_id", DbType.String, builderId);
            db.AddInParameter(cmd, "community_id", DbType.Int32, communityId);
            db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerId);
            db.AddOutParameter(cmd, "@isValid", DbType.String, 100);
            db.ExecuteNonQuery(cmd);
            string isValid = DBValue.GetString(db.GetParameterValue(cmd, "@isValid")).ToLower();
            return (isValid == "y");
        }

        public bool ValidatePlanSpec(int planId, int specId, int partnerId)
        {
            string proc = "pr_NHS_ValidatePlanSpec";
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            if (planId == 0)
                db.AddInParameter(cmd, "spec_id", DbType.Int32, specId);
            else
                db.AddInParameter(cmd, "plan_id", DbType.String, planId);

            db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerId);
            db.AddOutParameter(cmd, "@is_valid", DbType.Int16, 2);
            db.ExecuteNonQuery(cmd);
            int isValid = DBValue.GetInt(db.GetParameterValue(cmd, "@is_valid"));
            return (isValid == 1);
        }

        #endregion

        #region Sitemap

        public List<Int16> SitemapGetMarkets(int partnerId)
        {
            List<Int16> lstMarkets = new List<Int16>();
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand("pr_NHS_Sitemap_GetMarkets");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerId);

            using (IDataReader dataReader = db.ExecuteReader(cmd))
            {
                while (dataReader.Read())
                {
                    lstMarkets.Add(Convert.ToInt16(dataReader["market_id"]));
                }
            }

            return lstMarkets;
        }

        public DataTable SitemapGetCities(int partnerId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand("pr_NHS_Sitemap_GetCities");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerId);

            // Query Database
            DataSet dataSet = db.ExecuteDataSet(cmd);

            // Nothing found?
            return dataSet.Tables.Count == 0 ? null : dataSet.Tables[0];
        }

        public DataTable SitemapGetCommunities(int partnerId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand("pr_NHS_Sitemap_GetCommunities");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerId);

            // Query Database
            DataSet dataSet = db.ExecuteDataSet(cmd);

            // Nothing found?
            return dataSet.Tables.Count == 0 ? null : dataSet.Tables[0];
        }

        public DataTable SitemapGetSpecifications(int partnerId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand("pr_NHS_Sitemap_GetSpecifications");
            cmd.CommandTimeout = 180;
            db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerId);

            // Query Database
            DataSet dataSet = db.ExecuteDataSet(cmd);

            // Nothing found?
            return dataSet.Tables.Count == 0 ? null : dataSet.Tables[0];
        }

        public DataTable SitemapGetPlans(int partnerId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand("pr_NHS_Sitemap_GetPlans");
            cmd.CommandTimeout = 180;
            db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerId);
            // Query Database
            DataSet dataSet = db.ExecuteDataSet(cmd);

            // Nothing found?
            return dataSet.Tables.Count == 0 ? null : dataSet.Tables[0];
        }

        public DataTable SitemapGetBrands(int partnerId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand("pr_NHS_Sitemap_GetBrands");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerId);
            // Query Database
            DataSet dataSet = db.ExecuteDataSet(cmd);

            // Nothing found?
            return dataSet.Tables.Count == 0 ? null : dataSet.Tables[0];
        }

        public DataTable SitemapGetMarketPostalCodes(int partnerId)
        {
            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand("pr_NHS_Sitemap_GetMarketPostalCodes");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerId);
            // Query Database
            DataSet dataSet = db.ExecuteDataSet(cmd);

            // Nothing found?
            return dataSet.Tables.Count == 0 ? null : dataSet.Tables[0];
        }

        public DataTable SitemapGetMarketCounties(int partnerId)
        {

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand("pr_NHS_Sitemap_GetMarketCounties");
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "partner_id", DbType.Int32, partnerId);
            // Query Database
            DataSet dataSet = db.ExecuteDataSet(cmd);

            // Nothing found?
            return dataSet.Tables.Count == 0 ? null : dataSet.Tables[0];
        }

        #endregion

        #region GeoCode

        public DataTable GetCityGeoCode(string city, string state)
        {
            string proc = "pr_NHS_GeoCodeCity";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "city", DbType.String, city);
            db.AddInParameter(cmd, "state", DbType.String, state);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetCityGeoCodeByMarket(string city, int marketId, string state = "")
        {
            string proc = "pr_NHS_GeoCodeCityByMarket";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "city", DbType.String, city);
            db.AddInParameter(cmd, "marketId", DbType.Int16, marketId);
            
            if(!string.IsNullOrEmpty(state))
                db.AddInParameter(cmd, "state", DbType.String, state);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetCountyGeoCode(string county, string state)
        {
            string proc = "pr_NHS_GeoCodeCounty";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "county", DbType.String, county);
            db.AddInParameter(cmd, "state", DbType.String, state);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetCountyGeoCode(string county, int marketId)
        {
            string proc = "pr_NHS_GeoCodeCountyByMarket";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "county", DbType.String, county);
            db.AddInParameter(cmd, "marketId", DbType.Int16, marketId);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetPostalCodeGeoCode(string postalCode)
        {
            string proc = "pr_NHS_GeoCodePostalCode";

            Database db = DatabaseFactory.CreateDatabase(WebDbConnection);
            DbCommand cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "postal_code", DbType.String, postalCode);

            return db.ExecuteDataSet(cmd).Tables[0];
        }

        #endregion

        #region Ektron - New

        public DataTable GetContentInfo(long contentId, long taxonomyId, int contentLanguage)
        {
            var db = DatabaseFactory.CreateDatabase(CmsDbEktron);
            const string storedProcedureName = "pr_NHS_GetContentByIdAndTaxonomy";
            var cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@content_id", DbType.Int64, contentId);
            db.AddInParameter(cmd, "@taxonomy_id", DbType.Int64, taxonomyId);
            db.AddInParameter(cmd, "@content_language", DbType.Int32, contentLanguage);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable FrontEndArticleSearchEktron(string keyword1, string keyword2, string keyword3,
                                                     string keyword4, long taxonomyId, int languageId)
        {
            var db = DatabaseFactory.CreateDatabase(CmsDbEktron);
            const string storedProcedureName = "pr_NHS_ArticleSearchByTaxonomy";
            var cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@keyword1", DbType.String, StringHelper.EmptyToNullString(keyword1));
            db.AddInParameter(cmd, "@keyword2", DbType.String, StringHelper.EmptyToNullString(keyword2));
            db.AddInParameter(cmd, "@keyword3", DbType.String, StringHelper.EmptyToNullString(keyword3));
            db.AddInParameter(cmd, "@keyword4", DbType.String, StringHelper.EmptyToNullString(keyword4));
            db.AddInParameter(cmd, "@taxonomy_id", DbType.Int64, taxonomyId);
            db.AddInParameter(cmd, "@content_language", DbType.Int32, languageId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        [Obsolete("use GetContentByContentTitleAndTaxonomy", true)]
        public DataTable GetContentByContentTitle(string title, int languageId)
        {
            Database db = DatabaseFactory.CreateDatabase(CmsDbEktron);
            const string storedProcedureName = "pr_NHS_GetContentByTitle";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@content_title", DbType.String, title);
            db.AddInParameter(cmd, "@content_language", DbType.Int32, languageId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetContentByContentTitleAndTaxonomy(string title, long taxonomyId, int languageId)
        {
            Database db = DatabaseFactory.CreateDatabase(CmsDbEktron);
            const string storedProcedureName = "pr_NHS_GetContentByTitleAndTaxonomy";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@content_title", DbType.String, title);
            db.AddInParameter(cmd, "@content_language", DbType.Int32, languageId);
            db.AddInParameter(cmd, "@taxonomy_id", DbType.Int64, taxonomyId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetContentByAuthorName(string authorName, int languageId, long taxonomyId)
        {
            var db = DatabaseFactory.CreateDatabase(CmsDbEktron);
            const string storedProcedureName = "pr_NHS_GetContentByAuthorNameAndTaxonomy";
            var cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@author", DbType.String, authorName);
            db.AddInParameter(cmd, "@content_language", DbType.Int32, languageId);
            db.AddInParameter(cmd, "@taxonomy_id", DbType.Int64, taxonomyId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetAllAuthors(long taxonomyId, int languageId)
        {
            var db = DatabaseFactory.CreateDatabase(CmsDbEktron);
            const string storedProcedureName = "pr_NHS_GetAllAuthorsUnderTaxonomy";
            var cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@taxonomy_id", DbType.Int64, taxonomyId);
            db.AddInParameter(cmd, "@content_language", DbType.Int32, languageId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetAuthorsLandingPage(long taxonomyId, int languageId)
        {
            var db = DatabaseFactory.CreateDatabase(CmsDbEktron);
            const string storedProcedureName = "pr_NHS_GetAuthorsLangingPageUnderTaxonomy";
            var cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@taxonomy_id", DbType.Int64, taxonomyId);
            db.AddInParameter(cmd, "@content_language", DbType.Int32, languageId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetMetasByContentTitle(string title, long taxonomyId, int languageId)
        {
            Database db = DatabaseFactory.CreateDatabase(CmsDbEktron);
            const string storedProcedureName = "pr_NHS_GetMetaByTitleUnderTaxonomy";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@content_title", DbType.String, title);
            db.AddInParameter(cmd, "@taxonomy_id", DbType.Int64, taxonomyId);
            db.AddInParameter(cmd, "@content_language", DbType.Int32, languageId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetArticlesByCategory(string categoryName, long taxonomyId, int languageId)
        {
            Database db = DatabaseFactory.CreateDatabase(CmsDbEktron);
            const string storedProcedureName = "pr_NHS_GetArticlesByCategoryUnderTaxonomy";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@category_name", DbType.String, categoryName);
            db.AddInParameter(cmd, "@taxonomy_id", DbType.Int64, taxonomyId);
            db.AddInParameter(cmd, "@content_language", DbType.Int32, languageId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }


        public DataTable GetCategoriesByArticleId(long articleId, string categoryName, int languageId)
        {
            Database db = DatabaseFactory.CreateDatabase(CmsDbEktron);
            const string storedProcedureName = "pr_NHS_GetCategoriesByArticleId";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            if (string.IsNullOrEmpty(categoryName)) categoryName = null;
            db.AddInParameter(cmd, "@article_id", DbType.Int64, articleId);
            db.AddInParameter(cmd, "@category_name", DbType.String, categoryName);
            db.AddInParameter(cmd, "@content_language", DbType.Int32, languageId);
            return db.ExecuteDataSet(cmd).Tables[0];

        }

        public DataTable GetParentCategoriesByCategoryName(string categoryName, int languageId, long taxonomyId)
        {
            Database db = DatabaseFactory.CreateDatabase(CmsDbEktron);
            const string storedProcedureName = "pr_NHS_GetParentCategoriesByCategoryNameAndTaxonomy";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@category_name", DbType.String, categoryName);
            db.AddInParameter(cmd, "@content_language", DbType.Int32, languageId);
            db.AddInParameter(cmd, "@taxonomy_id", DbType.Int64, taxonomyId);
            return db.ExecuteDataSet(cmd).Tables[0];

        }

        public DataTable GetMultiPartArticleByTitle(string title, long taxonomyId, int languageId)
        {
            var db = DatabaseFactory.CreateDatabase(CmsDbEktron);
            const string storedProcedureName = "pr_NHS_GetMultiPartArticleByTitleUnderTaxonomy";
            var cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@ArticleTitle", DbType.String, title);
            db.AddInParameter(cmd, "@taxonomy_id", DbType.Int64, taxonomyId);
            db.AddInParameter(cmd, "@content_language", DbType.Int32, languageId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetMultiPartArticleSectionsById(string sectionIds, long taxonomyId, int languageId)
        {
            var db = DatabaseFactory.CreateDatabase(CmsDbEktron);
            const string storedProcedureName = "pr_NHS_GetMultiPartArticleSectionsByIdUnderTaxonomy";
            var cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@SectionIDs", DbType.String, sectionIds);
            db.AddInParameter(cmd, "@taxonomy_id", DbType.Int64, taxonomyId);
            db.AddInParameter(cmd, "@content_language", DbType.Int32, languageId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetArticleAuthorById(long authorId, int languageId)
        {
            var db = DatabaseFactory.CreateDatabase(CmsDbEktron);
            const string storedProcedureName = "pr_NHS_GetMultiPartArticleAuthorByID";
            var cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@AuthorID", DbType.Int64, authorId);
            db.AddInParameter(cmd, "@LanguageID", DbType.Int32, languageId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetMultiPartArticleBySectionTitle(string title, long taxonomyId, int languageId)
        {
            Database db = DatabaseFactory.CreateDatabase(CmsDbEktron);
            const string storedProcedureName = "pr_NHS_GetMultiPartSectionByTitleUnderTaxonomy";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@SectionTitle", DbType.String, title);
            db.AddInParameter(cmd, "@taxonomy_id", DbType.Int64, taxonomyId);
            db.AddInParameter(cmd, "@content_language", DbType.Int32, languageId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetMultiPartArticleContinerBySection(long sectionId, string title, long taxonomyId, int languageId)
        {
            var db = DatabaseFactory.CreateDatabase(CmsDbEktron);
            const string storedProcedureName = "pr_NHS_GetMPArticleContinerBySectionUnderTaxonomy";
            var cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@SectionID", DbType.Int64, sectionId);
            db.AddInParameter(cmd, "@SectionTitle", DbType.String, title);
            db.AddInParameter(cmd, "@taxonomy_id", DbType.Int64, taxonomyId);
            db.AddInParameter(cmd, "@content_language", DbType.Int32, languageId);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public string GetArticleSmartFormTypeDesc(long contentId, int languageId)
        {
            Database db = DatabaseFactory.CreateDatabase(CmsDbEktron);
            string storedProcedureName = "pr_NHS_GetSmartFormTypeForArticle";
            DbCommand cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@ContentID", DbType.Int64, contentId);
            db.AddInParameter(cmd, "@languageId", DbType.Int32, languageId);
            var dtResult = db.ExecuteDataSet(cmd).Tables[0];

            if (dtResult.Rows.Count > 0)
            {
                return dtResult.Rows[0]["collection_title"].ToString();
            }
            else
            {
                return string.Empty;
            }
        }


        #endregion

        #region BrandShowCase

        public int SubmitBuilderShowCaseTestimonial(int testimonialId, int brandId, int sequence, string citation, string description)
        {
            const string proc = "pr_NHS_InsertBrandTestimonial";
            var db = DatabaseFactory.CreateDatabase(HubDbConnection);
            var cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "testimonial_id", DbType.Int32, testimonialId);
            db.AddInParameter(cmd, "brand_id", DbType.Int32, brandId);
            db.AddInParameter(cmd, "sequence", DbType.Int32, sequence);
            db.AddInParameter(cmd, "citation", DbType.String, citation);
            db.AddInParameter(cmd, "description", DbType.String, description);
            db.AddOutParameter(cmd, "@ID", DbType.Int32, 5);

            db.ExecuteNonQuery(cmd);

            return DBValue.GetInt(db.GetParameterValue(cmd, "@ID"));
        }

        public void DeleteBuilderShowCaseTestimonial(int testimonialId)
        {
            const string proc = "pr_NHS_DeleteBrandTestimonial";
            var db = DatabaseFactory.CreateDatabase(HubDbConnection);
            var cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "testimonial_id", DbType.Int32, testimonialId);
            db.ExecuteNonQuery(cmd);
        }

        public void SubmitBrandShowCase(int brandId, string overview, string description, string rssFeed,
                                        string twitterWidge, string facebookWidget, string contactEmail,
                                        string brandUrl)
        {
            const string proc = "pr_NHS_SubmitBrandShowCase";
            var db = DatabaseFactory.CreateDatabase(HubDbConnection);
            var cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "brand_id", DbType.Int32, brandId);
            db.AddInParameter(cmd, "overview", DbType.String, overview);
            db.AddInParameter(cmd, "description", DbType.String, description);
            db.AddInParameter(cmd, "rss_feed", DbType.String, rssFeed);
            db.AddInParameter(cmd, "twitter_widge", DbType.String, twitterWidge);
            db.AddInParameter(cmd, "facebook_widget", DbType.String, facebookWidget);
            db.AddInParameter(cmd, "created_by", DbType.String, 1);
            db.AddInParameter(cmd, "changed_by", DbType.String, 1);
            db.AddInParameter(cmd, "date_last_changed", DbType.String, DateTime.Now);
            db.AddInParameter(cmd, "date_created", DbType.String, DateTime.Now);
            db.AddInParameter(cmd, "contact_email", DbType.String, contactEmail);
            db.AddInParameter(cmd, "brand_ur", DbType.String, brandUrl);

            db.ExecuteNonQuery(cmd);
        }

        public void DeleteBrandShowCaseImage(string imagesList)
        {
            const string proc = "pr_NHS_DeleteImages";
            var db = DatabaseFactory.CreateDatabase(HubDbConnection);
            var cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "ImagesList", DbType.String, imagesList);
            db.ExecuteNonQuery(cmd);
        }

        public int AddBrandShowCaseImage(string imageType, int builderId, string originalUri, string name,
                                         string description, string title, int sequence, string accreditationSealUrl)
        {
            const string proc = "pr_NHS_InsertImage";
            var db = DatabaseFactory.CreateDatabase(HubDbConnection);
            var cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "ImageType", DbType.String, imageType);
            db.AddInParameter(cmd, "BuilderId", DbType.Int32, builderId);
            db.AddInParameter(cmd, "OriginalUri", DbType.String, originalUri);
            db.AddInParameter(cmd, "Name", DbType.String, name);
            db.AddInParameter(cmd, "Description", DbType.String, description);
            db.AddInParameter(cmd, "Title", DbType.String, title);
            db.AddInParameter(cmd, "Sequence", DbType.Int32, sequence);
            db.AddInParameter(cmd, "AccreditationSealUrl", DbType.String, accreditationSealUrl);
            db.AddOutParameter(cmd, "@ImageID", DbType.Int32, 5);

            db.ExecuteNonQuery(cmd);

            return DBValue.GetInt(db.GetParameterValue(cmd, "@ImageID"));
        }

        public void UpdateBrandShowCaseImage(int imageId, string description, string title, string accreditationSealUrl)
        {
            const string proc = "pr_NHS_UpdateImage";
            var db = DatabaseFactory.CreateDatabase(HubDbConnection);
            var cmd = db.GetStoredProcCommand(proc);
            cmd.CommandTimeout = CommandTimeout;

            db.AddInParameter(cmd, "ImageID", DbType.Int32, imageId);
            db.AddInParameter(cmd, "Description", DbType.String, description);
            db.AddInParameter(cmd, "Title", DbType.String, title);
            db.AddInParameter(cmd, "AccreditationUrl", DbType.String, accreditationSealUrl);

            db.ExecuteNonQuery(cmd);
        }
        //public DataTable GetAllImagesBrandShowCase(string builders)
        //{
        //    string proc = "select * from image where builder_id in (" + builders + ") and (load_status_id = 1 AND processor_status_id = 4 OR processor_status_id = 9)";
        //    Database db = DatabaseFactory.CreateDatabase(_hubDB);
        //    DbCommand cmd = db.GetSqlStringCommand(proc);
        //    cmd.CommandTimeout = COMMAND_TIMEOUT;
        //    return db.ExecuteDataSet(cmd).Tables[0];
        //}

        #endregion

        #region PRO CRM

        public DataTable GetProCrmMyClientsList(string userId)
        {
            var db = DatabaseFactory.CreateDatabase(WebDbConnection);
            const string storedProcedureName = "cvbcvn";
            var cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@id", DbType.String, userId);
            throw new NotImplementedException();
            //return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetProCrmRelatedContacs(long clientId)
        {
            var db = DatabaseFactory.CreateDatabase(WebDbConnection);
            const string storedProcedureName = "cvbcvn";
            var cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@clientId", DbType.UInt64, clientId);
            throw new NotImplementedException();
            //return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetProCrmPendingTasks(long clientId)
        {
            var db = DatabaseFactory.CreateDatabase(WebDbConnection);
            const string storedProcedureName = "cvbcvn";
            var cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@clientId", DbType.UInt64, clientId);
            throw new NotImplementedException();
            //return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetProCrmClientDetails(long clientId)
        {
            var db = DatabaseFactory.CreateDatabase(WebDbConnection);
            const string storedProcedureName = "cvbcvn";
            var cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@clientId", DbType.UInt64, clientId);
            throw new NotImplementedException();
            //return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetAllCommunitiesForPartner(int partner)
        {
            var db = DatabaseFactory.CreateDatabase(WebDbConnection);
            var storedProcedureName = "pr_NHS_GetCommunitiesByPartner";
            var cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            db.AddInParameter(cmd, "@partnerid", DbType.Int32, partner);
            return db.ExecuteDataSet(cmd).Tables[0];
        }


        #endregion

        #region Typeahead

        public DataTable GetAllCommunities()
        {
            var db = DatabaseFactory.CreateDatabase(WebDbConnection);
            const string storedProcedureName = "pr_NHS_GetAllCommunities";
            var cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;            
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        public DataTable GetAllBuilders()
        {
            var db = DatabaseFactory.CreateDatabase(WebDbConnection);
            const string storedProcedureName = "pr_NHS_GetAllBuilders";
            var cmd = db.GetStoredProcCommand(storedProcedureName);
            cmd.CommandTimeout = CommandTimeout;
            return db.ExecuteDataSet(cmd).Tables[0];
        }
        
        #endregion


        #region Builders

        public DataTable GetAllBuilderCoOpInfo()
        {
            var db = DatabaseFactory.CreateDatabase(WebDbConnection);
            const string storedProcedureName = "pr_NHS_GetBuilderCoOpInfo";
            var cmd = db.GetStoredProcCommand(storedProcedureName);            
            cmd.CommandTimeout = CommandTimeout;            
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        #endregion

        #region Synthetic Geography

        public DataTable GetPostalCodesByLocation(int? marketId, string city, string county, string state)
        {
            var db = DatabaseFactory.CreateDatabase(WebDbConnection);
            var cmd = db.GetStoredProcCommand("pr_NHS_GetPostalCodesbyLocation");

            db.AddInParameter(cmd, "@MarketId", DbType.Int32, marketId);
            db.AddInParameter(cmd, "@City", DbType.String, city);
            db.AddInParameter(cmd, "@County", DbType.String, county);
            db.AddInParameter(cmd, "@State", DbType.String, state);
            return db.ExecuteDataSet(cmd).Tables[0];
        }

        #endregion
    }
}
