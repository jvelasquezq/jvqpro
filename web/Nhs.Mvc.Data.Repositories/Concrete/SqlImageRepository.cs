﻿using System.Linq;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Hub;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Data.Repositories.Concrete
{
    public class SqlImageRepository:IImageRepository
    {
        private WebEntities _webCtx;
        private HubEntities _hubCtx;
        private bool _useHub;

        public bool UseHub
        {
            get { return _useHub; }
            set { _useHub = value; }
        }

        public SqlImageRepository(WebEntities webctx, HubEntities hubCtx)
        {
            _webCtx = webctx;
            _hubCtx = hubCtx;
        }

        public IQueryable<IImage> Images
        {
            get { return _useHub?_hubCtx.HubImages: _webCtx.Images as IQueryable<IImage>; }
        }

        public IQueryable<HubImage> HubImages
        {
            get { return _hubCtx.HubImages; }
        }
    }
}
