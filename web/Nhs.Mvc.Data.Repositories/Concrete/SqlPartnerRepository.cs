﻿using System;
using System.Linq;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Data.Repositories.Concrete
{
    internal class SqlPartnerRepository : IPartnerRepository
    {
        private WebEntities _ctx;

        public SqlPartnerRepository(WebEntities ctx)
        {
            _ctx = ctx;
        }

        public IQueryable<Partner> Partners
        {
            get { return _ctx.Partners; }
        }

        public IQueryable<PartnerMarketAbout> PartnersMarketFacebookUrls
        {
            get { return _ctx.PartnerMarketAbouts; }
        }
    }
}
