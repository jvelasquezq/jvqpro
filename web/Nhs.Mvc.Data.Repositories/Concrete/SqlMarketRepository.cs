﻿using System.Collections.Generic;
using System.Linq;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Data.Repositories.Concrete
{
    internal class SqlMarketRepository : IMarketRepository
    {
        private WebEntities _ctx;

        public SqlMarketRepository()
        {

        }

        public SqlMarketRepository(WebEntities ctx)
        {
            _ctx = ctx;
        }

        //DO NOT USE THIS METHOD. USE MarketService.GetMarkets(partnerId) INSTEAD.
        public IQueryable<PartnerMarket> GetPartnerMarkets(int partnerId)
        {
            return (from pm in _ctx.PartnerMarkets.Include("Market.Cities").Include("Market.State")
                       where pm.PartnerId == partnerId
                       select pm);
        }

        public IEnumerable<PartnerMarketCity> GetPartnerMarketsInfo(int partnerId)
        {
            var markets = _ctx.GetPartnerMarketCities(partnerId);
            return markets.ToList();
        }

        public GeoLocation GetGeoLocationByPostalCode(string postalCode)
        {
            return (from pm in _ctx.GeoLocations
                    where pm.PostalCode == postalCode
                    select pm).FirstOrDefault(); 
        }
        public IQueryable<Market> Markets
        {
            get { return _ctx.Markets; }
        }
    }
}
