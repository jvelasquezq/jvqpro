﻿using System.Linq;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Hub;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Data.Repositories.Concrete
{
    internal class SqlBrandRepository : IBrandRepository
    {
        private WebEntities _ctx;
        private HubEntities _hubCtx;

        public SqlBrandRepository(WebEntities ctx, HubEntities hubCtx)
        {
            _ctx = ctx;
            _hubCtx = hubCtx;
        }

        public IQueryable<Brand> Brands
        {
            get { return _ctx.Brands; }
        }

        public IQueryable<HubBrand> HubBrands
        {
            get { return _hubCtx.HubBrands; }
        }

        public IQueryable<HubBrandShowCase> HubBrandShowCase
        {
            get { return _hubCtx.HubBrandShowCases; }
        }
        
    }
}
