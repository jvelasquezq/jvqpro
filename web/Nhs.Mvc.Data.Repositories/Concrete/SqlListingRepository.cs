﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Hub;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;

namespace Nhs.Mvc.Data.Repositories.Concrete
{
    internal class SqlListingRepository : IListingRepository
    {
        private WebEntities _webCtx;
        private HubEntities _hubCtx;
        private bool _useHub;

        public bool UseHub
        {
            get { return _useHub; }
            set { _useHub = value; }
        }

        public SqlListingRepository(WebEntities webCtx, HubEntities hubCtx)
        {
            _webCtx = webCtx;            
            _hubCtx = hubCtx;
        }

        public IQueryable<IPlan> Plans
        {
            get { return _useHub ? _hubCtx.HubPlans: _webCtx.Plans as IQueryable<IPlan>; }
        }

        public IQueryable<ISpec> Specs
        {
            get { return _useHub ? _hubCtx.HubSpecs : _webCtx.Specs as IQueryable<ISpec>; }
        }

        public IQueryable<IPlan> PlansWithImages
        {
            get { return _useHub ?      _hubCtx.HubPlans.Include("AllPlanHubImages")
                                    : _webCtx.Plans.Include("AllPlanImages") as IQueryable<IPlan>;
            }
        }

        public  IQueryable<ISpec> SpecsWithImages
        {
            get { return _useHub ?      _hubCtx.HubSpecs.Include("HubPlan").Include("HubPlan.AllPlanHubImages")
                                    : _webCtx.Specs.Include("Plan").Include("Plan.AllPlanImages") as IQueryable<ISpec>;
            }
        }

        //public IQueryable<HubPlan> HubPlans
        //{
        //    get { return _hubCtx.HubPlans; }
        //}

        //public IQueryable<HubSpec> HubSpecs
        //{
        //    get { return _hubCtx.HubSpecs; }
        //}

        //public IQueryable<HubPlan> HubPlansWithImages
        //{
        //    get { return _hubCtx.HubPlans.Include("AllPlanImages"); }
        //}

        //public IQueryable<HubSpec> HubSpecsWithImages
        //{
        //    get { return _hubCtx.HubSpecs.Include("HubPlan").Include("HubPlan.AllPlanImages"); }
        //}
    }
}
