﻿using System.Linq;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Hub;


namespace Nhs.Mvc.Data.Repositories.Concrete
{
    internal class SqlCommunityRepository : ICommunityRepository
    {
        private WebEntities _webCtx;
        private HubEntities _hubCtx;
        private bool _useHub;

        public bool UseHub
        {
            get { return _useHub; }
            set { _useHub = value; }
        }

        public SqlCommunityRepository(WebEntities webCtx, HubEntities hubCtx)
        {
            _webCtx = webCtx;            
            _hubCtx = hubCtx;
        }

        public IQueryable<Community> Communities
        {
            get { return _webCtx.Communities; }
        }

        public IQueryable<CommunityAgentPolicy> CommunityAgentPolicies
        {
            get { return _webCtx.CommunityAgentPolicies; }
        }

        public IQueryable<CommunityPromotion> CommunityPromotions
        {
            get { return _webCtx.CommunityPromotions; }
        }

        public IQueryable<CommunityEvent> CommunityEvents
        {
            get { return _webCtx.CommunityEvents; }
        }

        public IQueryable<HubCommunity> HubCommunities
        {
            get { return _hubCtx.HubCommunities; }
        }

        public IQueryable<TollFreeNumber> TollFreeNumbers
        {
            get { return _webCtx.TollFreeNumbers; }
        }

        public IQueryable<HubTollFreeNumber> HubTollFreeNumbers
        {
            get { return _hubCtx.HubTollFreeNumbers; }
        }
    }
}