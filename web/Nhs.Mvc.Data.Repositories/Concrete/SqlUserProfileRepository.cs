﻿using System;
using System.Data.Objects;
using System.Linq;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Data.Repositories.Concrete
{
    internal class SqlUserProfileRepository : IUserProfileRepository
    {
        private ProfileEntities _ctx;
        public SqlUserProfileRepository(ProfileEntities ctx)
        {
            _ctx = ctx;
        }

        public IQueryable<UserProfile> Users
        {
            get { return _ctx.UserProfiles; }
        }

        public void CreateProfile(UserProfile profile)
        {
            try
            {
                profile.UserGuid = "{" + Guid.NewGuid().ToString() + "}";
                profile.AccountStatus = 1;
                profile.NoMatchEmailCount = 0;
                _ctx.UserProfiles.AddObject(profile);
                _ctx.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }
        }

        public void UpdateProfile(UserProfile profile)
        {
            try
            {
                _ctx.ApplyCurrentValues("UserProfiles", profile);
                _ctx.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }
        }

    }
}
