﻿using System.Linq;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Data.Repositories.Concrete
{
    internal class SqlStateRepository : IStateRepository
    {
        private WebEntities _ctx;
        public SqlStateRepository(WebEntities ctx)
        {
            _ctx = ctx;
        }

        public IQueryable<State> States
        {
            get { return _ctx.States; }
        }
    }

}