﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.BasicListings;

namespace Nhs.Mvc.Data.Repositories.Concrete
{
    internal class SqlBasicListingRepository : IBasicListingRepository
    {
        private BasicListingsEntities _ctx;

        public SqlBasicListingRepository(BasicListingsEntities ctx)
        {
            _ctx = ctx;
        }

        public IQueryable<BasicListing> BasicListings
        {
            get { return _ctx.BasicListings; }
        }

        public IQueryable<MarketGeoLocation> MarketGeoLocations
        {
            get { return _ctx.MarketGeoLocations; }
        }

        public IQueryable<PartnerBasicListing> PartnerBasicListings
        {
            get { return _ctx.PartnerBasicListings; }
        }

        public IQueryable<BasicListingMarket> Markets
        {
            get { return _ctx.BasicListingMarkets.Include("BasicListingMarket.GeoLocationInfo"); }
        }

        public IQueryable<BasicListingImage> BasicListingImages
        {
            get { return _ctx.BasicListingImages; }
        }
    }
}
