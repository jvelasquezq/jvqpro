﻿using System.Linq;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Data.Repositories.Concrete
{
    internal class SqlBuilderRepository : IBuilderRepository
    {
        private WebEntities _ctx;

        public SqlBuilderRepository(WebEntities ctx)
        {
            _ctx = ctx;
        }

        public IQueryable<Builder> Builders
        {
            get { return _ctx.Builders; }
        }

        public IQueryable<BuilderService> BuilderServices 
        {
            get { return _ctx.BuilderServices; }
        }
    }
}
