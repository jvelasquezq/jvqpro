﻿using System;
using System.Data.Objects;
using System.Linq;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Data.Repositories.Concrete
{
    public class SqlSearchAlertRepository : ISearchAlertRepository
    {
        private ProfileEntities _ctx;

        public SqlSearchAlertRepository(ProfileEntities ctx)
        {
            _ctx = ctx;
        }

        public IQueryable<SearchAlert> SearchAlerts
        {
            get { return _ctx.SearchAlerts; }
        }

        public bool CreateSearchAlert(SearchAlert alert)
        {
            try
            {
                _ctx.SearchAlerts.AddObject(alert);
                _ctx.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                return true;
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
                return false;
            }
        }

        public void RemoveSearchAlert(SearchAlert searchAlert)
        {
            try
            {
                foreach (var alertResult in _ctx.AlertResults.Where(result => result.AlertId == searchAlert.AlertId))
                {
                    _ctx.AlertResults.DeleteObject(alertResult);
                }
                _ctx.SearchAlerts.DeleteObject(searchAlert);
                _ctx.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }
        }

        public void RemoveAlertResult(int searchAlertId, int communityId, int builderId)
        {
            try
            {
                _ctx.AlertResults.DeleteObject(_ctx.AlertResults.FirstOrDefault(result => result.AlertId == searchAlertId && result.CommunityId == communityId && result.BuilderId == builderId));
                _ctx.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }
        }


        public IQueryable<AlertResult> AlertResults
        {
            get { return _ctx.AlertResults; }
        }
    }
}
