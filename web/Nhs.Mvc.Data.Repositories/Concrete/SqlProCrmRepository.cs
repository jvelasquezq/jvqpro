﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Profiles;
namespace Nhs.Mvc.Data.Repositories.Concrete
{
    internal class SqlProCrmRepository : IProCrmRepository
    {
        private readonly ProfileEntities _ctx;

        public SqlProCrmRepository(ProfileEntities ctx)
        {
            _ctx = ctx;
        }

        public IQueryable<AgentClient> Clients
        {
            get { return _ctx.AgentClients; }
        }

        public IQueryable<ClientEmail> ClientEmails
        {
            get { return _ctx.ClientEmails; }
        }

        public IQueryable<ClientTaskListingName> ClientTaskListingNames
        {
            get { return _ctx.ClientTaskListingNames; }
        }


        public IQueryable<ClientEmail> Emails
        {
            get { return _ctx.ClientEmails; }
        }

        public bool UpdateTask(ClientTask task, IEnumerable<ClientTaskListing> clientTaskListings, IEnumerable<int> clientTaskListingListDelete, bool taskComplete = false)
        {
            try
            {
                var options = new TransactionOptions
                   {
                       IsolationLevel = IsolationLevel.ReadCommitted,
                       Timeout = TransactionManager.DefaultTimeout
                   };
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                {
                    using (var context = new ProfileEntities())
                    {
                        //update
                        var client = context.AgentClients.FirstOrDefault(a => a.ClientId == task.ClientId);
                        if (client != null) 
                            client.DateLastChanged = DateTime.Now;

                        var taskDb = context.ClientTasks.FirstOrDefault(t => t.ClientTaskId == task.ClientTaskId);
                        if (taskDb != null)
                        {
                            foreach (var listing in clientTaskListings)
                            {
                                if (listing.TaskListingId == 0)
                                {
                                    listing.ClientTask = taskDb;
                                    context.ClientTaskListings.AddObject(listing);
                                }
                                else
                                {
                                    var tempTaskListings = taskDb.ClientTaskListings.FirstOrDefault(p => p.TaskListingId == listing.TaskListingId);
                                    if (tempTaskListings != null)
                                    {
                                        tempTaskListings.ListingType = listing.ListingType;
                                        tempTaskListings.PropertyId = listing.PropertyId;
                                    }
                                }
                            }

                            foreach (var tempTaskListings in clientTaskListingListDelete.Select(
                                        taskListingId =>taskDb.ClientTaskListings.FirstOrDefault(p => p.TaskListingId == taskListingId)).Where(tempTaskListings => tempTaskListings != null))
                            {
                                context.ClientTaskListings.DeleteObject(tempTaskListings);
                            }

                            taskDb.DateLastChanged = DateTime.Now;
                            taskDb.Description = task.Description;
                            taskDb.Notes = task.Notes;
                            taskDb.TaskType = task.TaskType;
                            taskDb.TaskDate = task.TaskDate;

                            if (taskComplete)
                            {
                                taskDb.TaskCompleteDate = DateTime.Now;
                            }
                        }
                        context.SaveChanges();
                        scope.Complete();
                    }
                }
                return true;
            }
            catch
            {

                return false;
            }
        }

        public bool InsertTask(ClientTask task, IEnumerable<ClientTaskListing> clientTaskListings, int clientId)
        {
            try
            {
                var options = new TransactionOptions
                    {
                        IsolationLevel = IsolationLevel.ReadCommitted,
                        Timeout = TransactionManager.DefaultTimeout
                    };
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                {
                    using (var context = new ProfileEntities())
                    {
                        //insert
                        var client = context.AgentClients.FirstOrDefault(a => a.ClientId == task.ClientId);
                        if (client != null)
                        {
                            client.DateLastChanged = DateTime.Now;
                            task.AgentClient = client;
                            context.ClientTasks.AddObject(task);
                            foreach (var listing in clientTaskListings.Where(listing => listing.PropertyId > 0))
                            {
                                listing.ClientTask = task;
                                context.ClientTaskListings.AddObject(listing);
                            }
                        }
                        context.SaveChanges();
                        scope.Complete();
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteTask(long taskId)
        {
            var options = new TransactionOptions
                    {
                        IsolationLevel = IsolationLevel.ReadCommitted,
                        Timeout = TransactionManager.DefaultTimeout
                    };
            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
            {
                using (var context = new ProfileEntities())
                {

                    var task = context.ClientTasks.FirstOrDefault(t => t.ClientTaskId == taskId);
                    var client = context.AgentClients.FirstOrDefault(a => a.ClientId == task.ClientId);
                    client.DateLastChanged = DateTime.Now;

                    if (task != null)
                    {
                        foreach (var clientTaskListing in task.ClientTaskListings.ToList())
                        {
                            context.ClientTaskListings.DeleteObject(clientTaskListing);
                        }                        
                        context.ClientTasks.DeleteObject(task);
                        context.SaveChanges();
                        scope.Complete(); 
                    }
                }
            }

            return true;
        }

        public bool UpdateStatusClient(long clientId, byte accountStatus)
        {
            try
            {
                using (var context = new ProfileEntities())
                {
                    var options = new TransactionOptions
                        {
                            IsolationLevel = IsolationLevel.ReadCommitted,
                            Timeout = TransactionManager.DefaultTimeout
                        };
                    using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                    {
                        var tempClient = context.AgentClients.FirstOrDefault(p => clientId == p.ClientId);
                       
                        if (tempClient != null)
                        {
                            tempClient.AccountStatus = accountStatus;
                            tempClient.DateLastChanged = DateTime.Now;
                        }
                        context.SaveChanges();
                        scope.Complete();
                    }
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool InsertClient(AgentClient client, IEnumerable<ClientEmail> emails, IEnumerable<ClientPhone> phones, ClientTask task = null)
        {
            try
            {
                var options = new TransactionOptions
                    {
                        IsolationLevel = IsolationLevel.ReadCommitted,
                        Timeout = TransactionManager.DefaultTimeout
                    };
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                {
                    using (var context = new ProfileEntities())
                    {
                        client.DateLastChanged = DateTime.Now;
                        context.AgentClients.AddObject(client);
                        //Save the changes
                        //context.SaveChanges();

                        foreach (var clientPhone in phones)
                        {
                            clientPhone.AgentClient = client;
                            context.ClientPhones.AddObject(clientPhone);
                        }

                        foreach (var clientEmail in emails)
                        {
                            clientEmail.AgentClient = client;
                            context.ClientEmails.AddObject(clientEmail);
                        }

                        #region task

                        if (task != null)
                        {
                            task.AgentClient = client;
                            context.ClientTasks.AddObject(task);
                        }

                        #endregion

                        //Save the changes
                        context.SaveChanges();

                        // commit the transaction
                        scope.Complete();
                    }
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool InsertEmailsToClient(int clientId, IEnumerable<ClientEmail> emails, ClientTask task = null)
        {
            try
            {
                var options = new TransactionOptions
                {
                    IsolationLevel = IsolationLevel.ReadCommitted,
                    Timeout = TransactionManager.DefaultTimeout
                };
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                {
                    using (var context = new ProfileEntities())
                    {
                        var tempClient = context.AgentClients.FirstOrDefault(p => p.ClientId == clientId);
                        foreach (var clientEmail in emails)
                        {
                            clientEmail.AgentClient = tempClient;                            
                            context.ClientEmails.AddObject(clientEmail);
                        }

                        #region task

                        if (task != null)
                        {
                            task.AgentClient = tempClient;
                            context.ClientTasks.AddObject(task);
                        }

                        #endregion

                        if (tempClient != null) tempClient.DateLastChanged = DateTime.Now;

                        //Save the changes
                        context.SaveChanges();

                        // commit the transaction
                        scope.Complete();
                    }
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateClient(AgentClient client, IEnumerable<ClientEmail> emails, IEnumerable<ClientPhone> phones)
        {
            try
            {
                var options = new TransactionOptions
                    {
                        IsolationLevel = IsolationLevel.ReadCommitted,
                        Timeout = TransactionManager.DefaultTimeout
                    };
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                {
                    using (var context = new ProfileEntities())
                    {
                        var tempClient = context.AgentClients.FirstOrDefault(p => client.ClientId == p.ClientId);

                        if (tempClient != null)
                        {
                            tempClient.AccountStatus = client.AccountStatus;
                            tempClient.Address = client.Address;
                            tempClient.City = client.City;
                            tempClient.FirstName = client.FirstName;
                            tempClient.LastName = client.LastName;
                            tempClient.RelatedFirstName = client.RelatedFirstName;
                            tempClient.RelatedLastName = client.RelatedLastName;
                            tempClient.State = client.State;
                            tempClient.DateLastChanged = DateTime.Now;
                            tempClient.Notes = client.Notes;
                            tempClient.PostalCode = client.PostalCode;
                            while (tempClient.ClientPhones.Any(p => p.ClientId == tempClient.ClientId))
                            {
                                var tempPhone =
                                    tempClient.ClientPhones.FirstOrDefault(p => p.ClientId == tempClient.ClientId);
                                context.ClientPhones.DeleteObject(tempPhone);
                            }

                            while (tempClient.ClientEmails.Any(p => p.ClientId == tempClient.ClientId))
                            {
                                var tempEmail =
                                    tempClient.ClientEmails.FirstOrDefault(p => p.ClientId == tempClient.ClientId);
                                context.ClientEmails.DeleteObject(tempEmail);
                            }

                            foreach (var clientPhone in phones)
                            {
                                clientPhone.AgentClient = tempClient;
                                context.ClientPhones.AddObject(clientPhone);
                            }

                            foreach (var clientEmail in emails)
                            {
                                clientEmail.AgentClient = tempClient;
                                context.ClientEmails.AddObject(clientEmail);
                            }

                            //Save the changes
                            context.SaveChanges();

                            // commit the transaction
                            scope.Complete();
                        }
                        else
                        {
                            return false;
                        }
                    }

                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateClients(IEnumerable<int> clients, ClientTask task)
        {
            try
            {
                var options = new TransactionOptions
                    {
                        IsolationLevel = IsolationLevel.ReadCommitted,
                        Timeout = TransactionManager.DefaultTimeout
                    };
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                {
                    using (var context = new ProfileEntities())
                    {
                        foreach (var tempClient in clients.Select(client => context.AgentClients.FirstOrDefault(p => client == p.ClientId)).Where(tempClient => tempClient != null))
                        {
                            tempClient.DateLastChanged = DateTime.Now;
                            var newTask = new ClientTask
                                {
                                    Description = task.Description,
                                    TaskType = task.TaskType,
                                    TaskDate = DateTime.Now,
                                    DateLastChanged = DateTime.Now,
                                    TaskCompleteDate = task.TaskCompleteDate,
                                    Notes = task.Notes,                                       
                                };

                            foreach (var clientTaskListings in task.ClientTaskListings)
                            {
                                newTask.ClientTaskListings.Add(new ClientTaskListing
                                    {
                                        ListingType = clientTaskListings.ListingType,
                                        PropertyId = clientTaskListings.PropertyId,                                           
                                    });
                            }
                            tempClient.ClientTasks.Add(newTask);
                        }

                        // Save the changes
                        context.SaveChanges();

                        // commit the transaction
                        scope.Complete();
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool RelateClient(int clientId, string relatedFirstName, string relatedLastName, IEnumerable<ClientEmail> emails, ClientTask task = null)
        {
            try
            {
                var options = new TransactionOptions
                    {
                        IsolationLevel = IsolationLevel.ReadCommitted,
                        Timeout = TransactionManager.DefaultTimeout
                    };
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                {
                    using (var context = new ProfileEntities())
                    {
                        var tempClient = context.AgentClients.FirstOrDefault(p => p.ClientId == clientId);

                        if (tempClient != null)
                        {
                            tempClient.RelatedFirstName = relatedFirstName;
                            tempClient.RelatedLastName = relatedLastName;
                            tempClient.DateLastChanged = DateTime.Now;
                            foreach (var clientEmail in emails)
                            {
                                clientEmail.AgentClient = tempClient;
                                context.ClientEmails.AddObject(clientEmail);
                            }

                            #region task

                            if (task != null)
                            {
                                task.AgentClient = tempClient;
                                context.ClientTasks.AddObject(task);
                            }

                            #endregion

                            //Save the changes
                            context.SaveChanges();

                            // commit the transaction
                            scope.Complete();
                        }
                        else
                        {
                            return false;
                        }
                    }

                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool InsertClients(IEnumerable<AgentClient> clients)
        {
            try
            {
                var options = new TransactionOptions
                {
                    IsolationLevel = IsolationLevel.ReadCommitted,
                    Timeout = TransactionManager.DefaultTimeout
                };
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                {
                    using (var context = new ProfileEntities())
                    {
                        foreach (var client in clients)
                        {
                            client.DateLastChanged = DateTime.Now;
                            context.AgentClients.AddObject(client);
                        }

                        //Save the changes
                        context.SaveChanges();
                        // commit the transaction
                        scope.Complete();
                    }
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool InsertToFavoritesClient(IEnumerable<ClientPlanner> clientPlanner)
        {
            try
            {
                var options = new TransactionOptions
                {
                    IsolationLevel = IsolationLevel.ReadCommitted,
                    Timeout = TransactionManager.DefaultTimeout
                };
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                {
                    using (var context = new ProfileEntities())
                    {
                        foreach (var planner in clientPlanner)
                        {
                            var client = context.AgentClients.FirstOrDefault(p => p.ClientId == planner.ClientId);
                            if (client != null)
                            {
                                if (!context.ClientPlanners.Any(
                                        p => p.ClientId == planner.ClientId && p.PropertyId == planner.PropertyId))
                                {
                                    client.DateLastChanged = DateTime.Now;
                                    planner.DateCreated = DateTime.Now;
                                    planner.AgentClient = client;                                    
                                    context.ClientPlanners.AddObject(planner);
                                }
                            }
                        }

                        //Save the changes
                        context.SaveChanges();
                        // commit the transaction
                        scope.Complete();
                    }
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteToFavoritesClient(int propertyId, int clientId)
        {
            try
            {
                var options = new TransactionOptions
                {
                    IsolationLevel = IsolationLevel.ReadCommitted,
                    Timeout = TransactionManager.DefaultTimeout
                };
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                {
                    using (var context = new ProfileEntities())
                    {
                        var clientPlanner =
                            context.ClientPlanners.FirstOrDefault(
                                p => p.ClientId == clientId && p.PropertyId == propertyId);

                        if (clientPlanner != null)
                        {
                            context.ClientPlanners.DeleteObject(clientPlanner);
                        }

                        //Save the changes
                        context.SaveChanges();
                        // commit the transaction
                        scope.Complete();
                    }
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }
        public int Save()
        {
            _ctx.AcceptAllChanges();
            var retValue = _ctx.SaveChanges();
            return retValue;
        }
    }
}
