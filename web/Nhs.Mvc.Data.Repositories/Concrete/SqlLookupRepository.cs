﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Data.Repositories.Concrete
{
    public class SqlLookupRepository : ILookupRepository
    {
        private WebEntities _ctx;

        public SqlLookupRepository(WebEntities ctx)
        {
            _ctx = ctx;
        }

        public IQueryable<LookupGroup> LookupItems
        {
            get { return _ctx.LookupGroups; }
        }
    }
}
