﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Data.Repositories.Concrete
{
    internal class SqlVideoRepository : IVideoRepository
    {
        private WebEntities _ctx;
        public SqlVideoRepository(WebEntities ctx)
        {
            _ctx = ctx;
        }

        public IQueryable<Video> Videos
        {
            get { return _ctx.Videos; }
        }

        public IQueryable<BrandShowcaseVideo> BrandShowcaseVideos { get { return _ctx.BrandShowcaseVideos; } }
    }
}
