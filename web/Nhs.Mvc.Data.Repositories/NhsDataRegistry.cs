﻿using System;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Data.Repositories.Concrete;
using Nhs.Mvc.Domain.Model.BasicListings;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Utility.Common;
using StructureMap.Configuration.DSL;
using StructureMap.Pipeline;
using Nhs.Mvc.Domain.Model.Hub;
using Nhs.Mvc.Routing.Interface;

namespace Nhs.Mvc.Data.Repositories
{
    public class NhsDataRegistry : Registry
    {
        public NhsDataRegistry()
        {
            For<WebEntities>().LifecycleIs(new HybridLifecycle()).Use(() =>  new WebEntities());
            try
            {
                For<HubEntities>().LifecycleIs(new HybridLifecycle()).Use(() =>  new HubEntities());
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }

            For<BasicListingsEntities>().LifecycleIs(new HybridLifecycle()).Use(() => new BasicListingsEntities());
            For<ProfileEntities>().LifecycleIs(new HybridLifecycle()).Use(()=>new ProfileEntities());

            For<IPartnerRepository>().Use<SqlPartnerRepository>();
            For<IMarketRepository>().Use<SqlMarketRepository>();
            For<IStateRepository>().Use<SqlStateRepository>();
            For<ICommunityRepository>().Use<SqlCommunityRepository>();
            For<IBuilderRepository>().Use<SqlBuilderRepository>();
            For<IBrandRepository>().Use<SqlBrandRepository>();
            For<IProCrmRepository>().Use<SqlProCrmRepository>();
            For<IUserProfileRepository>().Use<SqlUserProfileRepository>();
            For<ILookupRepository>().Use<SqlLookupRepository>();
            For<IListingRepository>().Use<SqlListingRepository>();
            For<IVideoRepository>().Use<SqlVideoRepository>();
            For<ISearchAlertRepository>().Use<SqlSearchAlertRepository>();
            For<IImageRepository>().Use<SqlImageRepository>();
            For<IBasicListingRepository>().Use<SqlBasicListingRepository>();
        }

        //private  WebEntities GetWebEntities()
        //{
        //    if (HttpContext.Current.Cache == null)
        //        return new WebEntities();

        //    var ocKey = "ocw_" + HttpContext.Current.GetHashCode().ToString("x");
        //    if (HttpContext.Current.Cache[ocKey] == null)
        //        HttpContext.Current.Cache.Insert(ocKey, new WebEntities());

        //    return HttpContext.Current.Cache[ocKey] as WebEntities;
        //}

        //private ProfileEntities GetProfileEntities()
        //{
        //    if (HttpContext.Current.Cache == null)
        //        return new ProfileEntities();

        //    var ocKey = "ocw_" + HttpContext.Current.GetHashCode().ToString("y");
        //    if (HttpContext.Current.Cache[ocKey] == null)
        //        HttpContext.Current.Cache.Insert(ocKey, new ProfileEntities());

        //    return HttpContext.Current.Cache[ocKey] as ProfileEntities;
        //}
    }
}
