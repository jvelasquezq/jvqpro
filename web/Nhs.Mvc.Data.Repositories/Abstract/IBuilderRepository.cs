﻿using System.Linq;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Data.Repositories.Abstract
{
    public interface IBuilderRepository
    {
        IQueryable<Builder> Builders { get; }
        IQueryable<BuilderService> BuilderServices { get; }
    }
}
