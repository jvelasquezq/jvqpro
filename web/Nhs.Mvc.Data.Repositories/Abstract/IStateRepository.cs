﻿using System.Linq;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Data.Repositories.Abstract
{
    public interface IStateRepository
    {
        IQueryable<State> States { get; }
    }
}