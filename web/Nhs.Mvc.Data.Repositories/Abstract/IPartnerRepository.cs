﻿using System.Linq;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Data.Repositories.Abstract
{
    public interface IPartnerRepository
    {
        IQueryable<Partner> Partners { get; }
        IQueryable<PartnerMarketAbout> PartnersMarketFacebookUrls { get; }
    }
}
