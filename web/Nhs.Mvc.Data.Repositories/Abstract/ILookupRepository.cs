﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Data.Repositories.Abstract
{
    public interface ILookupRepository
    {
        IQueryable<LookupGroup> LookupItems { get; }
    }
}
