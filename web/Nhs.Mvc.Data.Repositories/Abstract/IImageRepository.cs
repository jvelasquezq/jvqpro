﻿using System.Linq;
using Nhs.Mvc.Domain.Model.Hub;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Data.Repositories.Abstract
{
    public interface IImageRepository
    {
        bool UseHub { get; set; }
        IQueryable<IImage> Images { get; }

        IQueryable<HubImage> HubImages { get; }
    }
}
