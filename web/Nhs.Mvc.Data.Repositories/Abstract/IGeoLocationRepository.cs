﻿using System.Linq;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Data.Repositories.Abstract
{
    public interface IGeoLocationRepository
    {
        //DO NOT USE THIS METHOD. USE MarketService.GetMarkets(partnerId) INSTEAD.
        IQueryable<PartnerMarket> GetPartnerMarkets(int partnerId);
        IQueryable<Market> Markets { get; }
    }
}
