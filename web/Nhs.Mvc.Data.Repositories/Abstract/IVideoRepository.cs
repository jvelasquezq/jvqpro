﻿using System.Linq;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Data.Repositories.Abstract
{
    public interface IVideoRepository
    {
        IQueryable<Video> Videos { get; }
        IQueryable<BrandShowcaseVideo> BrandShowcaseVideos { get; }  
    }
}
