﻿using System.Collections.Generic;
using System.Linq;
using Nhs.Mvc.Domain.Model.Profiles;

namespace Nhs.Mvc.Data.Repositories.Abstract
{
    public interface IProCrmRepository
    {
        bool InsertClients(IEnumerable<AgentClient> clients);
        IQueryable<AgentClient> Clients { get; }
        IQueryable<ClientEmail> Emails { get; }
        IQueryable<ClientEmail> ClientEmails { get; }
        bool UpdateTask(ClientTask task, IEnumerable<ClientTaskListing> clientTaskListings, IEnumerable<int> clientTaskListingListDelete, bool taskComplete = false);
        bool InsertTask(ClientTask task, IEnumerable<ClientTaskListing> clientTaskListings, int clientId);
        bool UpdateStatusClient(long clientId, byte accountStatus);
        bool DeleteTask(long taskId);
        bool InsertClient(AgentClient client, IEnumerable<ClientEmail> emails, IEnumerable<ClientPhone> phones, ClientTask task = null);
        bool UpdateClient(AgentClient client, IEnumerable<ClientEmail> emails, IEnumerable<ClientPhone> phones);
        bool RelateClient(int clientId, string relatedFirstName, string relatedLastName, IEnumerable<ClientEmail> emails, ClientTask task = null);
        bool InsertEmailsToClient(int clientId, IEnumerable<ClientEmail> emails, ClientTask task = null);
        bool InsertToFavoritesClient(IEnumerable<ClientPlanner> clientPlanner);
        bool DeleteToFavoritesClient(int clientId, int propertyId);
        bool UpdateClients(IEnumerable<int> clients, ClientTask task);
        int Save();
    }
}
