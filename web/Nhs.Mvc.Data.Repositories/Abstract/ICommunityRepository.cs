﻿using System.Linq;
using Nhs.Mvc.Domain.Model.Hub;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Data.Repositories.Abstract
{
    public interface ICommunityRepository
    {
        bool UseHub { get; set; }
        IQueryable<Community> Communities { get; }
        IQueryable<CommunityPromotion> CommunityPromotions { get; }
        IQueryable<CommunityEvent> CommunityEvents { get; }
        IQueryable<CommunityAgentPolicy> CommunityAgentPolicies { get; }
        IQueryable<HubCommunity> HubCommunities { get; }
        IQueryable<TollFreeNumber> TollFreeNumbers { get; }
        IQueryable<HubTollFreeNumber> HubTollFreeNumbers { get; }
    }
}
