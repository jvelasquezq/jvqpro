﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Mvc.Domain.Model.Profiles;

namespace Nhs.Mvc.Data.Repositories.Abstract
{
    public interface IUserProfileRepository
    {
        IQueryable<UserProfile> Users { get; }
        void CreateProfile(UserProfile profile);
        void UpdateProfile(UserProfile profile);
    }
}
