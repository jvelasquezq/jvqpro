﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Mvc.Domain.Model;
using Nhs.Mvc.Domain.Model.Hub;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Data.Repositories.Abstract
{
    public interface IBrandRepository
    {
        IQueryable<Brand> Brands { get; }
        IQueryable<HubBrand> HubBrands { get; }
        IQueryable<HubBrandShowCase> HubBrandShowCase { get; }
        
    }
}
