﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Mvc.Domain.Model.BasicListings;

namespace Nhs.Mvc.Data.Repositories.Abstract
{
    public interface IBasicListingRepository
    {
        IQueryable<BasicListing> BasicListings { get; }
        IQueryable<MarketGeoLocation> MarketGeoLocations { get; }
        IQueryable<PartnerBasicListing> PartnerBasicListings { get; }
        IQueryable<BasicListingMarket> Markets { get; }
        IQueryable<BasicListingImage> BasicListingImages { get; }
    }
}
