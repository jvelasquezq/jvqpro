﻿using System.Linq;
using Nhs.Mvc.Domain.Model.Profiles;

namespace Nhs.Mvc.Data.Repositories.Abstract
{
    public interface ISearchAlertRepository
    {
        IQueryable<SearchAlert> SearchAlerts { get; }
        IQueryable<AlertResult> AlertResults { get; }
        bool CreateSearchAlert(SearchAlert alert);
        void RemoveSearchAlert(SearchAlert searchAlert);
        void RemoveAlertResult(int searchAlertId, int communityId, int builderId);
    }
}
