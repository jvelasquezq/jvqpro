﻿using System.Linq;
using Nhs.Mvc.Domain.Model.Web.Interfaces;

namespace Nhs.Mvc.Data.Repositories.Abstract
{
    public interface IListingRepository
    {
        //IQueryable<HubPlan> HubPlans { get; }
        //IQueryable<HubSpec> HubSpecs { get; }
        bool UseHub { get; set; }
        IQueryable<IPlan> Plans { get; }
        IQueryable<ISpec> Specs { get; }
        IQueryable<IPlan> PlansWithImages { get; }
        IQueryable<ISpec> SpecsWithImages { get; }
        //IQueryable<HubPlan> HubPlansWithImages { get; }
        //IQueryable<HubSpec> HubSpecsWithImages { get; }

    }
}
