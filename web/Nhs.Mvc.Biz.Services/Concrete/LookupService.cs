﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Nhs.Library.Enums;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Filters;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Web;
namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class LookupService : ILookupService
    {
        private ILookupRepository _lookupRepository;
        private IWebCacheService _webCacheService;

        public LookupService(IWebCacheService webCacheService, ILookupRepository lookupRepository)
        {
            _webCacheService = webCacheService;
            _lookupRepository = lookupRepository;
        }

        public IList<LookupGroup> GetCommonListItems(CommonListItem commonListItem, string any = "Any")
        {
            string groupKey = string.Empty;

            switch (commonListItem)
            {
                case CommonListItem.Bedrooms: groupKey = "NUMBED"; break;
                case CommonListItem.Bathrooms: groupKey = "NUMBATH"; break;
                case CommonListItem.FinancialPref: groupKey = "FINANCEPR"; break;
                case CommonListItem.Garage: groupKey = "NUMGARAGE"; break;
                case CommonListItem.HomeStatus: groupKey = "HOMESTATUS"; break;
                case CommonListItem.HomeType: groupKey = "HOMETYPE"; break;
                case CommonListItem.LivingArea: groupKey = "NUMLIVING"; break;
                case CommonListItem.MasterBed: groupKey = "MASTERBED"; break;
                case CommonListItem.MoveInDate: groupKey = "MOVEINDT"; break;
                case CommonListItem.MoveReason: groupKey = "MOVEREASON"; break;
                case CommonListItem.PriceRange: groupKey = "PRICEDD"; break;
                case CommonListItem.MinPrice: groupKey = "PRICEDD"; break;
                case CommonListItem.MaxPrice: groupKey = "PRICEDD"; break;
                case CommonListItem.Stories: groupKey = "NUMSTORIES"; break;
                case CommonListItem.Radius: groupKey = "RADIUS"; break;
            }

            if (groupKey == string.Empty)
                return new List<LookupGroup>();

            string cacheKey = "MVCListItems_" + groupKey;
            return _webCacheService.Get(cacheKey, false, () => GetLookupGroupFromDB(groupKey));
        }

        public IList<LookupGroup> GetSquareFeetListItems(string any = "Any")
        {
            var lookupGroup = new List<LookupGroup> {new LookupGroup {LookupText = any, LookupValue = "0"}};

            for (var i = 500; i <= 10000; i = i + 500)
                lookupGroup.Add(new LookupGroup { LookupText = i + "+", LookupValue = i.ToString(CultureInfo.InvariantCulture) });

            return lookupGroup;
        }

        private IList<LookupGroup> GetLookupGroupFromDB(string name)
        {
            return this._lookupRepository.LookupItems.GetByGroupName(name).ToList();
        }
    }
}
