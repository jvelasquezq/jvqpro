﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Domain.Model.Web.Partials;
using Nhs.Utility.Common;
using Nhs.Library.Constants;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class LeadService : ILeadService
    {
        private IListingService _listingService;
        private ICommunityService _communityService;

        public LeadService(IListingService listingService, ICommunityService communityService)
        {
            _listingService = listingService;
            _communityService = communityService;
        }

        public DataTable GetCommunityBrochures(int communityId, int builderId)
        {
            return DataProvider.Current.GetCommunityBrochures(communityId, builderId);
        }

        public DataTable GetHomeBrochures(int planId, int specificationId)
        {
            return DataProvider.Current.GetHomeBrochures(planId, specificationId);
        }

        /// <summary>
        /// Gets al the communities or homes for a given lead request
        /// </summary>
        /// <param name="requestId">The request id.</param>
        /// <returns></returns>
        public IList<LeadEmailProperty> GetCommsHomesForRequest(int requestId)
        {
            var requestInfo = new List<LeadEmailProperty>();
            var req = DataProvider.Current.GetRequest(requestId);

            if (req != null && req.Rows.Count>0)
            {
                requestInfo.AddRange(from DataRow row in req.Rows
                                     select new LeadEmailProperty
                                         {
                                             RequestTypeCode = DBValue.GetString(row["request_type_code"]),
                                             EmailAddress = DBValue.GetString(row["email_address"]),
                                             CommunityId = row["community_id"].ToType<int>(),
                                             BuilderId = row["builder_id"].ToType<int>(),
                                             PlanId = row["plan_id"].ToType<int>(),
                                             SpecId = row["specification_id"].ToType<int>(),
                                             UniqueKey = row["source_request_key"].ToType<string>(),
                                             FirstName = row["first_name"].ToType<string>(),
                                             LastName = row["last_name"].ToType<string>(),
                                             RequestItemId = row["request_item_id"].ToType<string>(),
                                             PostalCode = row["postal_code"].ToType<string>(),
                                         });
            }

            return requestInfo;
        }

        public DataTable GetRequestItemInformation(int requestId)
        {
            var req = DataProvider.Current.GetRequest(requestId);
            return req;
        }

        public IList<LeadEmailProperty> GetCommsHomesForRequestBySourceRequestKey(string sourceRequestKey)
        {
            var requestInfo = new List<LeadEmailProperty>();
            var req = DataProvider.Current.GetRequestBySourceRequestKey(sourceRequestKey);

            if (req != null && req.Rows.Count > 0)
            {
                requestInfo.AddRange(from DataRow row in req.Rows
                                     select new LeadEmailProperty
                                     {
                                         RequestTypeCode = DBValue.GetString(row["request_type_code"]),
                                         EmailAddress = DBValue.GetString(row["email_address"]),
                                         CommunityId = row["community_id"].ToType<int>(),
                                         BuilderId = row["builder_id"].ToType<int>(),
                                         PlanId = row["plan_id"].ToType<int>(),
                                         SpecId = row["specification_id"].ToType<int>(),
                                         UniqueKey = row["source_request_key"].ToType<string>(),
                                         FirstName = row["first_name"].ToType<string>(),
                                         LastName = row["last_name"].ToType<string>(),
                                         RequestItemId = row["request_item_id"].ToType<string>()
                                     });
            }

            return requestInfo;
        }

        public void InsertPresenteComunitys(int sourceCommunityId, string communityList, string email, string firstName,
                                            string lastName, string postalCode, int partnerId, int marketId)
        {

            DataProvider.Current.InsertPresentedCommunyties(sourceCommunityId, communityList, email, firstName, lastName, postalCode, 
                                                            partnerId, marketId);
        }
        
        public LeadEmailProperty GetGetRequestItemDeliveryInfo(int requestItemId)
        {
            var req = DataProvider.Current.GetRequestItemDeliveryInfo(requestItemId);

            if (req != null && req.Rows.Count > 0)
            {
                var row = req.Rows[0];
                return new LeadEmailProperty
                    {
                        BrochureName = DBValue.GetString(row["brochure_name"]),
                        CommunityId = row["community_id"].ToType<int>(),
                        BuilderId = row["builder_id"].ToType<int>(),
                        PlanId = row["plan_id"].ToType<int>(),
                        SpecId = row["specification_id"].ToType<int>(),
                        RequestItemDeliveryId = DBValue.GetString(row["request_item_delivery_id"]) ,
                        BrochureExpiration = DBNull.Value.Equals(row[ "brochure_date_expire"])? DateTime.MinValue :DBValue.GetDateTime(row["brochure_date_expire"])
                    };
            }
            return new LeadEmailProperty();
        }

        public void UpdateBrochureName(int requestItemDeliveryId, string brochureName)
        {
            DataProvider.Current.UpdateBrochureName(requestItemDeliveryId, brochureName);
        }        

        /// <summary>
        /// Gets request attributes like type code, page action and market id
        /// </summary>
        /// <param name="requestId">The request id.</param>
        /// <param name="requestTypeCode">The request type code.</param>
        /// <param name="marketId">The market id.</param>
        public void GetRequestAttribs(int requestId, out string requestTypeCode, out int marketId)
        {
            var attribs = DataProvider.Current.GetRequestItems(requestId);

            requestTypeCode = string.Empty;
            marketId = 0;

            if (attribs != null && attribs.Rows.Count > 0)
            {
                DataRow row = attribs.Rows[0];
                requestTypeCode = DBValue.GetString(row["request_type_code"]).ToLower();
                marketId = row["market_id"].ToType<int>();
            }
        }

        public LeadEmail GetLeadEmailInfo(int requestId)
        {
            var info = new LeadEmail();
            var attribs = DataProvider.Current.GetRequestItems(requestId);

            if (attribs != null && attribs.Rows.Count > 0)
            {
                var row = attribs.Rows[0];
                info.RequestTypeCode = DBValue.GetString(row["request_type_code"]).ToLower();
                info.MarketId = row["market_id"].ToType<int>();
            }
            return info;
        }
        
        public IList<LeadEmailProperty> GetLeadEmailCommunities(IList<int> commIds)
        {
            var comms = _communityService.GetCommunities(commIds.ToList());
            var props = new List<LeadEmailProperty>();

            foreach (var comm in comms)
            {
                var prop = new LeadEmailProperty();
                prop.CommunityId = comm.CommunityId;
                prop.BuilderId = comm.BuilderId;
                prop.CommunityName = comm.CommunityName;
                prop.BrandName = comm.Brand.BrandName;
                prop.Address = comm.Address1 ?? comm.SalesOffice.Address1;
                prop.City = comm.City;
                prop.State = comm.StateAbbr;
                prop.PostalCode = comm.PostalCode;
                prop.MarketName = comm.Market.MarketName;
                prop.ImageThumbnail = comm.SpotlightThumbnail;
                prop.PriceHigh = comm.PriceHigh;
                prop.PriceLow = comm.PriceLow;
                prop.BuilderUrl = comm.Builder.Url;
                props.Add(prop);
            }

            return props;
        }

        public IList<LeadEmailProperty> GetLeadEmailHomes(IList<int> planIds, IList<int> specIds)
        {
            var homes = new List<LeadEmailProperty>();
            List<IListing> listings = new List<IListing>(); //C# 4.0 - covariance

            if (specIds.Count > 0)
            {
                var specs = _listingService.GetSpecsForSpecIds(specIds, true);
                listings.AddRange(specs);
            }

            if (planIds.Count > 0)
            {
                var plans = _listingService.GetPlansForPlanIds(planIds, true);
                listings.AddRange(plans);
            }

            foreach (var listing in listings)
            {
                var home = new LeadEmailProperty();
                home.PlanName = listing.PlanName;
                home.CommunityName = listing.Community.CommunityName;
                home.BrandName = listing.Community.Brand.BrandName;
                home.NumBedrooms = listing.Bedrooms.ToType<int>();
                home.NumBaths = listing.Bathrooms.ToType<int>();
                home.NumHalfBaths = listing.HalfBaths.ToType<int>();
                home.NumGarages = listing.Garages.ToType<decimal>();
                home.IsHotHome = listing.IsHotHome;
                home.HotHomeTitle = listing.HotHomeTitle;
                home.CommunityId = listing.CommunityId;
                home.ImageThumbnail = string.Empty;
                home.Price = listing.Price;
                home.City = listing.Community.City;
                home.State = listing.Community.State.StateAbbr;
                home.PostalCode = listing.Community.PostalCode;

                if (listing is Spec)
                {
                    home.SpecId = (listing as Spec).SpecId;
                    home.PlanId = (listing as Spec).PlanId;
                }
                else
                {
                    home.PlanId = (listing as Plan).PlanId;
                }

                var image = _listingService.GetHomeImagesForSpec(home.PlanId, home.SpecId).FirstOrDefault();
                if (image != null)
                    home.ImageThumbnail = string.Format("{0}/{1}_{2}", image.ImagePath, ImageSizes.HomeThumb, image.ImageName);

                homes.Add(home);

            }
            return homes;
        }
    }
}
