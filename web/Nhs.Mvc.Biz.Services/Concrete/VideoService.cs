﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Library.Business;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Biz.Services.Filters;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class VideoService : IVideoService
    {
        private IVideoRepository _videoRepository;

        public VideoService(IVideoRepository videoRepository)
        {
            _videoRepository = videoRepository;
        }

        #region IVideoService Members

        public IList<Video> GetVideosForCommunityList(IList<CommunityResult> communities)
        {
            return _videoRepository.Videos.GetByCommunityList(communities);
        }

        public IList<Video> GetVideosForBrand(int brandId)
        {
            // Get the ids of the videos of the brandshowcasevideo table that are related to the brand
            var videoIds = _videoRepository.BrandShowcaseVideos.Where(video => video.BrandId == brandId).Select(video => video.VideoId).ToList();
            return _videoRepository.Videos.Where(video => videoIds.Contains(video.VideoId) ).ToList();
        }

        #endregion
    }
}
