﻿using System.Collections.Generic;
using Nhs.Library.Constants;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Utility.Common;
using Nhs.Mvc.Data.Repositories.Abstract;
using State = Nhs.Mvc.Domain.Model.Web.State;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class HotGreenSearchService : IHotGreenSearchService
    {
        private IWebCacheService _webCacheService;
        private IMarketRepository _marketRepository;
        private IStateService _stateService;

        public HotGreenSearchService(IMarketRepository marketRepository, IStateService stateService, IWebCacheService webCacheService)
        {
            _marketRepository = marketRepository;
            _stateService = stateService;
            _webCacheService = webCacheService;
        }

        public IList<State> GetHGStates(int partnerId)
        {
            string key = WebCacheConst.WebCacheKeyPrefix + WebCacheConst.PartnerStates + partnerId.ToString();
            
            var states = _stateService.GetPartnerStates(partnerId);
            var quickStates = new List<State>();

            foreach (State state in states)
            {                
                quickStates.Add(state);
            }
            
            quickStates.Sort(new PropertyComparer<State>("StateName"));
            return quickStates;
        }
    }
}
