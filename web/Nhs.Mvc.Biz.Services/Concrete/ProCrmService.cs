﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using Nhs.Library.Constants.Enums;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Biz.Services.Concrete
{

    public class ProCrmService : IProCrmService
    {
        private readonly IProCrmRepository _proCrmRepository;
        private IWebCacheService _webCacheService;

        public ProCrmService(IProCrmRepository proCrmRepository, IWebCacheService webCacheService)
        {
            _proCrmRepository = proCrmRepository;
            _webCacheService = webCacheService;
        }

        public IEnumerable<ClientEmail> GetClientEmails(string userId)
        {
            return _proCrmRepository.ClientEmails.Where(c => c.AgentClient.UserId == userId && c.AgentClient.AccountStatus == (byte)ProCrmClientsStatusType.Active);
        }

        private IEnumerable<AgentClient> Clients
        {
            get { return _proCrmRepository.Clients; }
        }

        public IEnumerable<AgentClient> GetMyClientList(string userId)
        {
            var data = Clients.Where(c =>
                                            c.UserId == userId &&
                                            (c.AccountStatus == (byte)ProCrmClientsStatusType.Active ||
                                             c.AccountStatus == (byte)ProCrmClientsStatusType.Inactive));

            return data;
        }

        public IEnumerable<AgentClient> GetMyClientList(string userId, ProCrmClientsStatusType status)
        {
            var data = Clients.Where(c => c.UserId == userId && c.AccountStatus == (byte)status);

            return data;
        }

        public IList<AgentClient> GetRecentUsedClients(string userGuid, ProCrmClientsStatusType clientStatus = ProCrmClientsStatusType.Active)
        {
            var masterClients = GetMyClientList(userGuid, clientStatus).ToList();
            IList<AgentClient> masterClientFiltered = null;

            if (!masterClients.Any()) return new List<AgentClient>();

            masterClientFiltered = masterClients
                        .Where(c => c.DateLastChanged != null)
                        .GroupBy(g => g.DateLastChanged.Value.ToString("MM/dd/yyyy hh:mm"))
                        .OrderByDescending(p => p.Key)
                        .FirstOrDefault()
                        .ToArray();

            if (masterClientFiltered.Count() < 3)
            {
                masterClientFiltered = masterClients
                    .Where(c => c.DateLastChanged != null)
                    .OrderByDescending(p => p.DateLastChanged.Value)
                    .Take(3)
                    .ToArray();
            }

            return masterClientFiltered;
        }

        public bool EmailExists(string userId, long clientId, IList<string> emails, out Dictionary<string, string> emailDuplicate)
        {
            emailDuplicate =
                emails.Select(
                    email =>
                    _proCrmRepository.Emails.Where(p => p.AgentClient.UserId == userId && p.ClientId != clientId
                                                   && (p.AgentClient.AccountStatus == (byte)ProCrmClientsStatusType.Active
                                                   || p.AgentClient.AccountStatus == (byte)ProCrmClientsStatusType.Inactive))
                                     .FirstOrDefault(p => email.ToLower().Trim() == p.Email.ToLower().Trim()))
                      .Where(tempEmail => tempEmail != null)
                      .ToDictionary(
                          tempEmail => tempEmail.AgentClient.FirstName + " " + tempEmail.AgentClient.LastName,
                          tempEmail => tempEmail.Email);
            return emailDuplicate.Any();
        }

        public IEnumerable<ClientTask> GetPendingTasks(long clientId, string userGuid)
        {
            var data = GetClientDetails(clientId, userGuid);

            var tasks = data != null ? data.ClientTasks : new Collection<ClientTask>();
            return tasks;
        }

        public AgentClient GetClientDetails(long clientId, string userGuid)
        {
            var data = _proCrmRepository.Clients.FirstOrDefault(c => c.ClientId == clientId && c.UserId == userGuid && (c.AccountStatus == (byte)ProCrmClientsStatusType.Active ||
                                             c.AccountStatus == (byte)ProCrmClientsStatusType.Inactive));
            return data;
        }

        public ClientTask GetTask(long clientId, string userGuid, int taskId)
        {
            ClientTask task = null;
            var user = GetClientDetails(clientId, userGuid);
            if (user != null && user.ClientTasks.Any())
            {
                task = user.ClientTasks.FirstOrDefault(t => t.ClientTaskId == taskId);
            }

            return task;
        }

        public bool UpdateStatusClient(long agentClientId, ProCrmClientsStatusType status)
        {
            return _proCrmRepository.UpdateStatusClient(agentClientId, (byte)status);
        }

        public bool InsertToFavoritesClient(IEnumerable<ClientPlanner> clientPlanner)
        {
            return _proCrmRepository.InsertToFavoritesClient(clientPlanner);
        }

        public bool DeleteToFavoritesClient(int clientId, int propertyId)
        {
            return _proCrmRepository.DeleteToFavoritesClient(clientId, propertyId);
        }

        public bool InsertClient(AgentClient client, IEnumerable<ClientEmail> emails, IEnumerable<ClientPhone> phones, ClientTask task = null)
        {
            return _proCrmRepository.InsertClient(client, emails, ValidatePhoneNumbers(phones), task);
        }

        public bool InsertClients(IEnumerable<AgentClient> clients)
        {
            return _proCrmRepository.InsertClients(clients);
        }

        public bool UpdateClient(AgentClient client, IEnumerable<ClientEmail> emails, IEnumerable<ClientPhone> phones)
        {
            return _proCrmRepository.UpdateClient(client, emails, ValidatePhoneNumbers(phones));
        }

        public bool RelateClient(int clientId, string relatedFirstName, string relatedLastName, IEnumerable<ClientEmail> emails, ClientTask task = null)
        {
            return _proCrmRepository.RelateClient(clientId, relatedFirstName, relatedLastName, emails, task);
        }

        public bool UpdateTask(ClientTask task, IEnumerable<ClientTaskListing> clientTaskListings, IEnumerable<int> clientTaskListingListDelete, out string errorMessage, bool taskComplete = false)
        {
            try
            {
                errorMessage = string.Empty;
                return _proCrmRepository.UpdateTask(task, clientTaskListings, clientTaskListingListDelete);
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return false;
            }
        }

        public bool InsertTask(ClientTask task, IEnumerable<ClientTaskListing> clientTaskListings, int clientId, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;
                return _proCrmRepository.InsertTask(task, clientTaskListings, clientId);
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return false;
            }
        }

        public bool DeleteTask(long clientId, long taskId, string userGuid)
        {
            var success = false;

            try
            {
                var client = this.GetClientDetails(clientId, userGuid);
                if (client != null)
                {
                    var task = client.ClientTasks.FirstOrDefault(t => t.ClientTaskId == taskId);
                    if (task != null)
                    {
                        _proCrmRepository.DeleteTask(taskId);
                        success = true;
                    }
                }
            }
            catch (Exception)
            {
                success = false;
            }

            return success;
        }

        public bool UpdateClientStatus(long clientId, string userGuid, ProCrmClientsStatusType status)
        {
            var success = true;

            try
            {
                var client = GetClientDetails(clientId, userGuid);
                client.AccountStatus = (byte)status;
                client.DateLastChanged = DateTime.Now;
                _proCrmRepository.Save();
            }
            catch (Exception)
            {
                success = false;
            }

            return success;
        }

        public bool InsertEmailsToClient(int clientId, IEnumerable<ClientEmail> emails, ClientTask task = null)
        {
            return _proCrmRepository.InsertEmailsToClient(clientId, emails, task);
        }

        public bool UpdateClients(IEnumerable<int> clients, ClientTask task)
        {
         return _proCrmRepository.UpdateClients(clients, task);
        }

        public DataTable GetSavedCommunities(string communityId, int partnerId)
        {
           return DataProvider.Current.GetSavedCommunities(partnerId.ToType<string>(), communityId);
        }

        public DataTable GetSavedHomes(string planIds, string specIds, int partnerId)
        {
            return DataProvider.Current.GetSavedHomes(partnerId, planIds, specIds);
        }

        #region Private Methods

        private IEnumerable<ClientPhone> ValidatePhoneNumbers(IEnumerable<ClientPhone> phones)
        {
            var phoneList = new List<ClientPhone>();
            if (phones != null)
            {
                phoneList.AddRange(from clientPhone in phones.ToList()
                                   let validPhone = clientPhone.Phone.Replace("_", string.Empty)
                                   .Replace("(", string.Empty).Replace(")", string.Empty).Replace("-", string.Empty).Trim()
                                   where string.IsNullOrWhiteSpace(validPhone) == false || validPhone.Length > 5
                                   select clientPhone);
            }

            return phoneList.AsEnumerable();
        }

        public string FillDefaultDescription(string description, long clientId, string userId, ProCrmTaskTypes taskType)
        {
            /*  
                (1)	Email [CLIENT NAME] (e.g. “Email Susan Sasser)
                (2)	Call [CLIENT NAME]
                (3)	Send mail to [CLIENT NAME] 
                (4)	Appointment with [CLIENT NAME] 
             */
            if (string.IsNullOrWhiteSpace(description))
            {
                var clientDetails = GetClientDetails(clientId, userId);

                switch (taskType)
                {
                    case ProCrmTaskTypes.Appointment:
                        description = string.Format("{0} with {1} {2}", taskType, clientDetails.FirstName, clientDetails.LastName);
                        break;
                    case ProCrmTaskTypes.Mail:
                        description = string.Format("Send mail to {0} {1}", clientDetails.FirstName, clientDetails.LastName);
                        break;
                    case ProCrmTaskTypes.Call:
                    case ProCrmTaskTypes.Email:
                        description = string.Format("{0} {1} {2}", taskType, clientDetails.FirstName, clientDetails.LastName);
                        break;
                    case ProCrmTaskTypes.Other:
                        description = string.Format("{0} {1}", clientDetails.FirstName, clientDetails.LastName);
                        break;
                    default:
                        description = string.Format("{0} {1} {2}", taskType, clientDetails.FirstName, clientDetails.LastName);
                        break;
                }
            }

            return description;
        }

        #endregion
    }
}
