﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Nhs.Library.Business.Seo.Attributes;
using Nhs.Library.Common;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class SeoContentOptions
    {
        private const string DefaultContentFileName = "Default.xml";

        public SeoContentOptions(string path, string fileName, IList<string> defaultFileNameList = null,
            bool automaticReplaceAttributes = true, Type replaceAttributeType = null, 
            IList<ContentTag> replaceTags = null)
        {
            if (automaticReplaceAttributes && replaceAttributeType == null)
            {
                replaceAttributeType = typeof(ReplaceSeoContentAttribute);
            }            

            if (replaceTags == null)
            {
                replaceTags = new List<ContentTag>();
            }

            if (defaultFileNameList == null)
            {
                defaultFileNameList = new List<string> { DefaultContentFileName };
            }

            if (defaultFileNameList.Any() == false)
            {
                defaultFileNameList.Add(DefaultContentFileName);
            }

            Path = path;
            FileName = fileName;
            DefaultFileNameList = defaultFileNameList;
            AutomaticReplaceAttributes = automaticReplaceAttributes;
            ReplaceAttributeType = replaceAttributeType;
           
            ReplaceTags = replaceTags;
        }
        
        public IList<ContentTag> ReplaceTags { get; private set; }

        /// <summary>
        /// This path must be a folder under SeoContent, like "Builder-Filter/Community_County"
        /// </summary>
        public string Path { get; private set; }
        /// <summary>
        /// File name with the info
        /// </summary>
        public string FileName { get; private set; }
        /// <summary>
        /// Default name is Default.xml
        /// </summary>
        public IList<string> DefaultFileNameList { get; private set; }
        /// <summary>
        /// Ddefault is typeof(ReplaceSeoContentAttribute)
        /// </summary>
        public Type ReplaceAttributeType { get; private set; }
        /// <summary>
        /// Default is true
        /// </summary>
        public bool AutomaticReplaceAttributes { get; private set; }
    }   

    public class SeoContentService : ISeoContentService
    {
        private readonly IWebCacheService _webCacheService;
        private readonly IPathMapper _pathMapper;

        public SeoContentService(IWebCacheService webCacheService, IPathMapper pathMapper)
        {
            _webCacheService = webCacheService;
            _pathMapper = pathMapper;
        }

        public T GetSeoContent<T>(SeoContentOptions options) where T : new()
        {
            var content = GetSeoContent<T>(options.Path,options.FileName,options.DefaultFileNameList);

            ReplaceTags(content, options);

            return content;
        }

        public string ReplaceTags(string input, IList<ContentTag> replaceTags)
        {
            if (replaceTags.Any() == false) return input;

            if (string.IsNullOrWhiteSpace(input)) return string.Empty;

            if (input.Contains("[") == false) return input;

            var replaced = new StringBuilder(input);

            foreach (var key in replaceTags)
            {
                var value = (!string.IsNullOrEmpty(key.TagValue) && !string.Equals(key.TagKey.ToString(), SeoConstTags.BrandName))
                    ? StringHelper.ToTitleCase(key.TagValue)
                    : key.TagValue;

                replaced.Replace(string.Format("[{0}]", key.TagKey), string.IsNullOrEmpty(value) ? string.Empty : value);
            }
            return replaced.ToString();
        }

        #region Private

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T">Class where the content is deserialize</typeparam>
        /// <param name="path">Tha path is the folder(s) under the SeoContentFolder
        /// ej : ~/BHIContent/SEOContent/{path} </param>
        /// <param name="fileName">The name of the file</param>
        /// <param name="defaultFileNameList">The default file to use if file name do not exist</param>
        /// <returns>The Class filled with the xml</returns>
        private T GetSeoContent<T>(string path, string fileName, IList<string> defaultFileNameList) where T : new()
        {
            if (path.EndsWith("/") == false)
            {
                path = path + "/";
            }

            if (path.StartsWith("/") == false)
            {
                path = "/" + path;
            }

            var commonPath = @"~/" + Configuration.StaticContentFolder + @"/SeoContent" + path;
            var pathFile = _pathMapper.MapPath(commonPath + fileName);

            
            if (File.Exists(pathFile) == false)
            {
                foreach (var defaultFileName in defaultFileNameList)
                {
                    pathFile = _pathMapper.MapPath(commonPath + defaultFileName);
                     if (File.Exists(pathFile)) break;
                }               
            }

            if (File.Exists(pathFile) == false)
            {
                ErrorLogger.LogError(new FileNotFoundException(pathFile));
                return new T();
            }

            var result = XmlSerialize.DeserializeFromXmlFile<T>(pathFile);
            //XmlSerialize.SerializeToXmlFile(pathFile + ".bk", new T());

            return result;
        }
        
        private void ReplaceTags<T>(T content, SeoContentOptions options) where T : new()
        {
            if (options.AutomaticReplaceAttributes)
            {
                var type = typeof(T);
                foreach (PropertyInfo prop in type.GetProperties())
                {
                    object[] attributes = prop.GetCustomAttributes(options.ReplaceAttributeType, true);
                    if (attributes.Length == 1)
                    {
                        var value = prop.GetValue(content, null).ToType<string>();
                        prop.SetValue(content, ReplaceTags(value, options.ReplaceTags), null);
                    }
                }
            }
        }

        #endregion
    }
}
