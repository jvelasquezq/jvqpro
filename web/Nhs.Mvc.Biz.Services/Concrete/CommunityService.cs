﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using Bdx.Web.Api.Objects;
using Bdx.Web.Api.Objects.Constants;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Helpers;
using Nhs.Library.Helpers.Vimeo;
using Nhs.Library.Helpers.WebApiServices;
using Nhs.Library.Proxy;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Filters;
using Nhs.Mvc.Biz.Services.Helpers.PropertyInformation;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Hub;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Entities;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Domain.Model.Web.Partials;
using Nhs.Utility.Common;
using Nhs.Utility.Data;
using Nhs.Utility.Web;
using Builder = Nhs.Mvc.Domain.Model.Web.Builder;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class CommunityService : ICommunityService
    {
        private readonly ICommunityRepository _communityRepository;
        private readonly ISessionCacheService _sessionCache;
        private readonly IImageRepository _imageRepository;
        private readonly IListingRepository _listingRepository;
        private readonly IVideoRepository _videoRepository;
        private readonly IWebCacheService _webCacheService;
        private readonly IPartnerMarketPremiumUpgradeService _partnerMarketPremiumUpgrade;
        private readonly IStateService _stateService;
        private readonly IPartnerService _partnerService;
        private readonly IMarketService _marketService;
        private readonly IPathMapper _pathMapper;
        private IBoylService _boylService;
        private bool _useHub;
        private const string CreditScoreLinksXmlPath = "~/{0}/PropertyInformation/CreditScore/CreditScoreLinks.xml";

        public bool UseHub
        {
            get { return _useHub; }
            set
            {
                _useHub = _imageRepository.UseHub = _listingRepository.UseHub = _communityRepository.UseHub = value;
            }
        }

        public CommunityService(IWebCacheService webCacheService, ICommunityRepository communityRepository, ISessionCacheService sessionCache,
             IImageRepository imageRepository, IListingRepository listingRepository, IVideoRepository videoRepository,
            IPartnerMarketPremiumUpgradeService partnerMarketPremiumUpgrade, IStateService stateService, IPartnerService partnerService, IMarketService marketService, IPathMapper pathMapper, IBoylService boylService)
        {
            _partnerService = partnerService;
            _webCacheService = webCacheService;
            _communityRepository = communityRepository;
            _sessionCache = sessionCache;
            _imageRepository = imageRepository;
            _listingRepository = listingRepository;
            _videoRepository = videoRepository;
            _partnerMarketPremiumUpgrade = partnerMarketPremiumUpgrade;
            _stateService = stateService;
            _marketService = marketService;
            _pathMapper = pathMapper;
            _boylService = boylService;
        }

        public IList<BoylLink> GetBoylResults(int marketId, int partnerId)
        {
            var results = _boylService.GetBoylResultsByPartner(partnerId);
            var boylLinks = results.Where(p => p.MarketId == marketId).Select(p => new BoylLink
            {
                BuilderId = p.BuilderId,
                CommunityId = p.CommunityId,
            }).ToList();
            return boylLinks;
        }

        public string GetTollFreeNumber(int partnerId, int communityId, int builderId)
        {
            var tollFreeInfo = _useHub ? _communityRepository.HubTollFreeNumbers.GetById(partnerId, communityId, builderId) : _communityRepository.TollFreeNumbers.GetById(partnerId, communityId, builderId);
            return tollFreeInfo != null ? tollFreeInfo.PhoneNumber : string.Empty;
        }

        public IEnumerable<Community> GetAllCommunitiesForPartner(int partnerId)
        {
            var cacheKey = WebCacheConst.WebCacheKeyPrefix + "CommByPartner_" + partnerId.ToString(CultureInfo.InvariantCulture);
            var comm = _webCacheService.Get<IEnumerable<Community>>(cacheKey, false, new TimeSpan(0, 0, WebCacheConst.DefaultSeconds),
            () =>
            {

                var commTable = DataProvider.Current.GetAllCommunitiesForPartner(partnerId);
                return commTable.AsEnumerable().Select(dataRow => new Community
                {
                    CommunityName = dataRow.Field<string>("CommunityName"),
                    CommunityId = dataRow.Field<int>("CommunityId"),
                    State = new State { StateName = dataRow.Field<string>("State") },
                    Market = new Market
                    {
                        MarketName = dataRow.Field<string>("Market"),
                        MarketId = dataRow.Field<int>("MarketId")
                    }
                });
            });
            return comm;
        }

        public IEnumerable<MediaPlayerObject> GetCommunityVideoIDs(List<int> commIds, Dictionary<int, bool> commsBilledStatus)
        {
            // If there are comms with billed status (NHSPro) filter only for billed communities
            var filteredCommIds = commIds.Where(c => commsBilledStatus.Count == 0 || (commsBilledStatus.Keys.Contains(c) && commsBilledStatus[c])).ToList();

            return filteredCommIds.Join(_videoRepository.Videos.Where(v => v.PlanId == null), c => c, v => v.CommunityId, (c, v) => new { CommunityID = c, Sequence = v.Order, Title = v.Title, VideoID = v.VideoId })
                                .Select(newobj => new MediaPlayerObject
                                {
                                    VideoID = string.Format("cid-{0}-{1}-{2}", newobj.CommunityID, newobj.VideoID, Configuration.BrightcoveEnvSuffix),
                                    Title = newobj.Title.EscapeSingleQuote(),
                                    Type = MediaPlayerObjectTypes.Video,
                                    RefID = newobj.VideoID,
                                    Sort = newobj.Sequence
                                });
        }

        //This returns a dictionary of communityid and it's corresponding js call to start playing a video in the comm results video player
        public List<Video> GetCommunityVideos(IEnumerable<CommunityResult> communityResults)
        {
            var videos = communityResults
                                  .Join(_videoRepository.Videos.Where(v => v.PlanId == null), c => c.CommunityId, v => v.CommunityId, (c, v) => new { CommunityID = c.CommunityId, Sequence = v.Order, v.Title, VideoID = v.VideoId })
                                  .Select(newobj => new Video
                                  {
                                      CommunityId = newobj.CommunityID,
                                      RefId = string.Format("cid-{0}-{1}-{2}", newobj.CommunityID, newobj.VideoID, Configuration.BrightcoveEnvSuffix),
                                      Title = newobj.Title.EscapeSingleQuote(),
                                      VideoId = newobj.Sequence
                                  }).OrderBy(v => v.VideoId);

            return videos.ToList();
        }


        public IEnumerable<ExtendedHomeResult> GetMatchingHomesForHubCommunity(int communityId)
        {
            var homes = new List<ExtendedHomeResult>();

            var specs = _listingRepository.Specs
                .Join(_communityRepository.HubCommunities, s => s.CommunityId, c => c.CommunityId,
                      (s, c) => new { Spec = s, Community = c })
                      .Where(s => s.Spec.CommunityId == communityId && s.Spec.Status == "Active").Select(s => s.Spec).ToList();

            foreach (var spec in specs)
            {
                var result = new ExtendedHomeResult();
                result.SpecId = spec.SpecId;
                result.Address1 = spec.Address1;
                result.Address2 = spec.Address2;
                result.Price = spec.Price.ToType<int>();
                result.SquareFeet = spec.SqFt.ToType<int>();
                //result.ImageCount = spec.SpecImages.Count();
                result.HomeStatus = spec.HomeStatus;
                result.IsHotHome = spec.IsHotHome.ToType<bool>();

                if (spec.HubPlan != null) BindPlanHomeResult(result, spec.HubPlan);
                homes.Add(result);
            }

            var plans = _communityRepository.HubCommunities
                        .Join(_listingRepository.Plans, c => c.CommunityId, p => p.CommunityId,
                        (c, p) => new { Community = c, Plan = p })
                        .Where(c => c.Community.CommunityId == communityId && c.Plan.StatusId == 1).Select(p => p.Plan).ToList();

            foreach (var plan in plans)
            {
                var result = new ExtendedHomeResult();
                result.Price = plan.Price.ToType<int>();
                result.SquareFeet = plan.SqFt.ToType<int>();
                //result.ImageCount = plan.PlanImages.Count();
                BindPlanHomeResult(result, plan);
                homes.Add(result);
            }
            return homes;
        }

        private void BindPlanHomeResult(ExtendedHomeResult result, IPlan plan)
        {
            result.Plan = plan;
            result.PlanId = plan.PlanId;
            result.PlanName = plan.PlanName;
            result.NumBathrooms = plan.Bathrooms.ToType<int>();
            result.NumHalfBathrooms = plan.HalfBaths.ToType<int>();
            result.NumBedrooms = plan.Bedrooms.ToType<int>();
            result.NumGarages = plan.Garages.ToType<int>();
            //result.HomeStatus = plan.Status.ToType<int>();
        }

        public IList<NearbyCommunity> GetCommunitiesForRecoEmail(int partnerId, string commList, string builderList, decimal sourceCommLat, decimal sourceCommLng)
        {
            var recoComms = new List<NearbyCommunity>();
            var comms = DataProvider.Current.GetCommunitiesWithDistance(partnerId, commList, builderList, sourceCommLat,
                                                                        sourceCommLng);

            if (comms != null)
            {
                recoComms.AddRange(from DataRow commRow in comms.Rows
                                   select new NearbyCommunity
                                       {
                                           CommunityId = DBValue.GetInt(commRow["community_id"]),
                                           CommunityName = DBValue.GetString(commRow["community_name"]),
                                           CommunityDescription = string.Empty,
                                           City = DBValue.GetString(commRow["city"]),
                                           PostalCode = DBValue.GetString(commRow["postal_code"]),
                                           PriceLow = DBValue.GetDecimal(commRow["price_low"]),
                                           PriceHigh = DBValue.GetDecimal(commRow["price_high"]),
                                           HomeCount = (short)DBValue.GetInt(commRow["no_home_total"]),
                                           SpotlightThumbnail = DBValue.GetString(commRow["spotlight_thumbnail"]),
                                           DistanceFromCommunity = DBValue.GetDecimal(commRow["distance"]),
                                           HasHotHome = DBValue.GetInt(commRow["has_hot_home"]),
                                           BuilderId = DBValue.GetInt(commRow["builder_id"]),
                                           Builder = new Builder
                                           {
                                               BuilderId = DBValue.GetInt(commRow["builder_id"]),
                                               Url = DBValue.GetString(commRow["builder_url"])
                                           },
                                           StateAbbr = DBValue.GetString(commRow["state"]),
                                           BrandName = DBValue.GetString(commRow["brand_name"]),
                                           State = new State { StateAbbr = DBValue.GetString(commRow["state"]) },
                                           Brand = new Brand { BrandName = DBValue.GetString(commRow["brand_name"]) }
                                       });
            }

            return recoComms.DistinctBy(p => p.CommunityId).ToList();
        }

        /// <summary>
        /// Gets the nearby communities.
        /// </summary>
        /// <param name="partnerId">The partner id.</param>
        /// <param name="communityId">The community id.</param>
        /// <param name="builderId">The builder id.</param>
        /// <param name="geoLocationID">The geo location ID.</param>
        /// <returns></returns>
        public IList<NearbyCommunity> GetNearbyCommunities(int partnerId, int communityId, int builderId, int? geoLocationID)
        {
            var dt = DataProvider.Current.GetNearbyCommunities(builderId, communityId, partnerId, geoLocationID, _useHub);
            var communities = new List<NearbyCommunity>();

            if (dt == null) return communities;

            communities.AddRange(from DataRow dr in dt.Rows
                                 let market = _marketService.GetMarkets(partnerId).GetById(DBValue.GetInt(dr["market_id"]))
                                 select new NearbyCommunity
                                 {
                                     CommunityName = DBValue.GetString(dr["community_name"]),
                                     CommunityId = DBValue.GetInt(dr["community_id"]),
                                     BuilderId = DBValue.GetInt(dr["builder_id"]),
                                     SpotlightThumbnail = DBValue.GetString(dr["spotlight_thumbnail"]),
                                     PriceLow = DBValue.GetDecimal(dr["price_low"]),
                                     DistanceFromCommunity = DBValue.GetDecimal(dr["distance_from_community"]),
                                     HasVideo = (DBValue.GetString(dr["subvideo_flag"]).ToBool()) ? 1 : 0,
                                     Latitude = DBValue.GetDecimal(dr["latitude"]),
                                     Longitude = DBValue.GetDecimal(dr["longitude"]),
                                     City = DBValue.GetString(dr["city"]),
                                     PostalCode = DBValue.GetString(dr["postal_code"]),
                                     MarketId = DBValue.GetInt(dr["market_id"]),
                                     BrandId = DBValue.GetInt(dr["brand_id"]),
                                     BrandName = DBValue.GetString(dr["brand_name"]),
                                     PriceHigh = DBValue.GetDecimal(dr["price_high"]),
                                     MarketName = market.MarketName,
                                     StateName = market.State.StateName,
                                     StateAbbr = market.State.StateAbbr
                                 });

            communities.RemoveAll(comm => comm.Latitude == 0 || comm.Longitude == 0);
            communities = communities.DistinctBy(p => p.CommunityId).OrderBy(p => p.DistanceFromCommunity).ToList();

            return communities;
        }


        //TODO: Combine all image type calls into one call
        //public IList<IImage> GetCommunityImages(int communityId)
        //{
        //    var community = _useHub ? this.GetHubCommunity(communityId) : this.GetCommunity(communityId) as Domain.Model.ICommunity;
        //    return community.AllImages.AsQueryable().GetByType(ImageTypes.Community).ToList();
        //}

        public IList<IImage> GetLotMapImages(int communityId)
        {
            var images = new List<IImage>();
            images.AddRange(_useHub ?
                            GetHubCommunity(communityId).AllHubImages.AsQueryable().GetByType(ImageTypes.LotMap).ToList() :
                            GetCommunity(communityId).AllImages.AsQueryable().GetByType(ImageTypes.LotMap).ToList());
            return images;
        }

        public IImage GetBuilderMap(IEnumerable<IImage> allImages)
        {
            return allImages.AsQueryable().GetByType(ImageTypes.BuilderMap).FirstOrDefault();
        }

        public IEnumerable<IImage> GetVideoTourImages(int communityId)
        {
            //get external video/virtual tour/plan viewer links
            //but per [8] RULE: Only one (1) of each type of special item can display within the viewer. 
            //hence group by imagetype and select 1st img in each group

            var allImages = new List<IImage>();

            if (communityId <= 0) return allImages;

            if (_useHub)
                allImages.AddRange(GetHubCommunity(communityId).AllHubImages);
            else
                allImages.AddRange(GetCommunity(communityId).AllImages);

            return allImages.GetByTypes(new[] { ImageTypes.CommunityVideoTour }).OrderBy(img => img.ImageSequence);
        }

        public IEnumerable<IImage> GetAllVideoTourImages(List<int?> communityIds)
        {
            var images = new List<IImage>();
            images.AddRange(_useHub
                            ? _imageRepository.Images.GetByType(ImageTypes.CommunityVideoTour).OrderBy(
                                img => img.ImageSequence).WhereIn(i => i.CommunityId, communityIds).ToList()

                            : _imageRepository.Images.GetByType(ImageTypes.CommunityVideoTour).OrderBy(
                                img => img.ImageSequence).WhereIn(i => i.CommunityId, communityIds).ToList());

            return images;
        }

        /// <summary>
        /// Get List of Images for the Awards received by the builder of the community.
        /// </summary>
        /// <param name="communityId">Selected community id</param>
        /// <returns>List of 5 award images at the max</returns>
        public IList<IImage> GetAwardImages(IEnumerable<IImage> allImages)
        {
            var images = allImages.AsQueryable().GetByType(ImageTypes.Accreditation).Take(5).ToList();
            return images;
        }

        public IList<IImage> GetBannerImage(int communityId)
        {
            var images = new List<IImage>();
            images.AddRange(_useHub
                                ? GetHubCommunity(communityId).AllHubImages.AsQueryable().GetByType(
                                ImageTypes.CommunityBanner).ToList()
                                : GetCommunity(communityId).AllImages.AsQueryable().GetByType(
                                    ImageTypes.CommunityBanner).ToList());
            return images;
        }

        public bool GetCommunityBilledStatus(int partnerId, int builderId, int communityId, int marketid, bool useHub = false)
        {
            //return DataProvider.Current.GetCommunityBilledStatus(partnerId, builderId, communityId);
            return _partnerMarketPremiumUpgrade.GetPartnerMarketPremiumUpgrade(partnerId, marketid) || DataProvider.Current.GetCommunityBilledStatus(partnerId, builderId, communityId);
            //return true;
        }

        public List<MediaPlayerObject> GetMediaPlayerObjects(int communityId, bool addVideo = true, bool useImageTitle = false, bool isMobile = false, string size = ImageSizes.CommDetailMain)
        {
            const int mediathreshold = 6;
            const StringComparison igCase = StringComparison.InvariantCultureIgnoreCase;

            var videos = new List<IVideo>();
            var allImages = new List<IImage>();

            if (_useHub)
            {
                var hc = GetHubCommunity(communityId);
                if (addVideo)
                    videos.AddRange(hc.Videos);
                allImages.AddRange(hc.AllHubImages);
            }
            else
            {
                var c = GetCommunity(communityId);
                if (addVideo)
                    videos.AddRange(c.Videos);
                allImages.AddRange(c.AllImages);
            }

            //get videos
            var communityVideos = videos
                                    .Where(v => v.PlanId == null) //ensure only comm videos                                    
                                    .OrderBy(video => video.Order)
                                    .Select(video => new { video.VideoId, video.Order, video.Title, video.VideoURL });


            //get images
            var communityImages = allImages
                                    .GetByTypes(new[] { ImageTypes.Community, ImageTypes.LotMap })
                                    .OrderByDescending(img => img.PrimaryFlag)
                                    .OrderBy(img => img.ImageTypeCode)  //com then lot
                                    .ThenBy(img => img.ImageSequence);



            //add community video mediaObjects
            var mediaObjects = communityVideos.Select(video => new MediaPlayerObject
                                                                       {
                                                                           Type = MediaPlayerObjectTypes.Video,
                                                                           SubType = MediaPlayerObjectTypes.SubTypes.BrightcoveVideo,
                                                                           VideoID = string.Format("cid-{0}-{1}-{2}", communityId, video.VideoId, Configuration.BrightcoveEnvSuffix),
                                                                           Title = video.Title.EscapeSingleQuote(),
                                                                           RefID = video.VideoId,
                                                                           Url = video.VideoURL,
                                                                           Sort = video.Order
                                                                       }).ToList();
            mediaObjects.AddRange(communityImages.Select(image => new MediaPlayerObject
            {
                Type = MediaPlayerObjectTypes.Image,
                SubType = image.ImageTypeCode.Equals(ImageTypes.LotMap, igCase)
                        ? MediaPlayerObjectTypes.SubTypes.LotMap
                        : MediaPlayerObjectTypes.SubTypes.CommunityInterior,
                Title = GetConstructedImageTitle(image, useImageTitle),
                Thumbnail = image.ImageTypeCode.Equals(ImageTypes.LotMap, igCase)
                        ? GetConstructedImagePath(image, ImageSizes.Large, isMobile)
                        : GetConstructedImagePath(image, ImageSizes.CommDetailThumb, isMobile),
                Url = GetConstructedImagePath(image, size, isMobile),
                Caption = useImageTitle ? image.ImageDescription.EscapeSingleQuote() : image.ImageTitle.EscapeSingleQuote(),
                PreferredImage = image.PrimaryFlag != null
            }));

            //do we have community media thats at least equal to threshold
            if (mediaObjects.Count() < mediathreshold)
            {
                //Preferred Image can be an elevation from any spec or plan for the community
                var pciImage = _listingRepository.Specs
                    .Join(_imageRepository.Images, s => s.SpecId, i => i.SpecificationId, (s, i) => new { Specification = s, Images = i })
                    .Where(s => s.Images.PrimaryFlag != null
                            && s.Images.ImageTypeCode.Equals(ImageTypes.Elevation, igCase)
                            && s.Specification.CommunityId == communityId)
                    .Select(s => s.Images)
                    .FirstOrDefault();

                if (pciImage == null)
                {
                    pciImage = _listingRepository.Plans
                    .Join(_imageRepository.Images, p => p.PlanId, i => i.PlanId, (p, i) => new { Plan = p, Images = i })
                    .Where(p => p.Images.PrimaryFlag != null
                            && p.Images.ImageTypeCode.Equals(ImageTypes.Elevation, igCase)
                            && p.Plan.CommunityId == communityId)
                    .Select(p => p.Images)
                    .FirstOrDefault();
                }

                if (pciImage != null)
                {
                    mediaObjects.Add(new MediaPlayerObject
                    {
                        Type = MediaPlayerObjectTypes.Image,
                        SubType = MediaPlayerObjectTypes.SubTypes.ElevationImages,
                        Title = GetConstructedImageTitle(pciImage, useImageTitle),
                        Thumbnail = GetConstructedImagePath(pciImage, ImageSizes.HomeThumb, isMobile),
                        Caption = useImageTitle ? pciImage.ImageDescription.EscapeSingleQuote() : pciImage.ImageTitle.EscapeSingleQuote(),
                        Url = GetConstructedImagePath(pciImage, ImageSizes.HomeMain, isMobile),
                        PreferredImage = pciImage.PrimaryFlag != null
                    });
                }

                //if not try to get at least mediathreshold level media from homes using following business rules

                //1) Model home elevation images, ordered by model home ID, then by order specified for elevations for that home.
                //2) Model home interior images, ordered by model home ID, then by order specified for interiors for that home

                var modelImages = _listingRepository.Specs
                    .Join(_imageRepository.Images, s => s.SpecId, i => i.SpecificationId, (s, i) => new { Specification = s, Images = i })
                    .Where(s => s.Specification.SpecType.Equals("M", igCase) && //model homes
                                (s.Images.ImageTypeCode.Equals(ImageTypes.Elevation, igCase) || s.Images.ImageTypeCode.Equals(ImageTypes.Interior, igCase)) &&
                                s.Specification.CommunityId == communityId)
                    .OrderByDescending(s => s.Specification.Price)
                    .ThenBy(s => s.Images.ImageTypeCode)
                    .ThenBy(s => s.Images.ImageSequence)
                    .Select(s => s.Images)
                    .Take(mediathreshold - mediaObjects.Count())
                    .AsEnumerable();

                //fallback on plan images immediately without going all the way to the bottom
                if (!modelImages.Any())
                {
                    var planIdsForModels = (from s in _listingRepository.Specs
                                            where
                                                s.CommunityId == communityId &&
                                                s.SpecType.Equals("M", igCase) //model home
                                            orderby s.Price descending
                                            select s.PlanId).Take(mediathreshold - mediaObjects.Count()).ToList();

                    if (planIdsForModels.Any())
                    {
                        modelImages = ((_listingRepository.Plans
                                            .Join(_imageRepository.Images, p => p.PlanId, i => i.PlanId,
                                            (p, i) => new { Plans = p, Images = i }))
                                            .WhereIn(p => p.Plans.PlanId, planIdsForModels)
                                            .Where(
                                                    p => p.Images.ImageTypeCode.ToLower() == ImageTypes.Elevation ||
                                                    p.Images.ImageTypeCode.ToLower() == ImageTypes.Interior
                                                  )
                                            )
                                            .OrderByDescending(p => p.Plans.Price)
                                            .ThenBy(p => p.Images.ImageTypeCode)
                                            .ThenBy(p => p.Images.ImageSequence)
                                            .Take(mediathreshold - mediaObjects.Count())
                                            .Select(p => p.Images).ToList();
                    }
                }

                mediaObjects.AddRange(modelImages.Select(image => new MediaPlayerObject
                {
                    Type = MediaPlayerObjectTypes.Image,
                    SubType = image.ImageTypeCode.Equals(ImageTypes.Interior, igCase)
                            ? MediaPlayerObjectTypes.SubTypes.HomeInterior
                            : MediaPlayerObjectTypes.SubTypes.ElevationImages,
                    Title = GetConstructedImageTitle(image, useImageTitle),
                    //EF wierdness that I need to use Concat 
                    Thumbnail = GetConstructedImagePath(image, ImageSizes.HomeThumb, isMobile),
                    Caption = useImageTitle ? image.ImageDescription.EscapeSingleQuote() : image.ImageTitle.EscapeSingleQuote(),
                    Url = GetConstructedImagePath(image, ImageSizes.HomeMain, isMobile)
                }));
            }

            List<IImage> specImages = new List<IImage>();

            //grab spec images 
            if (mediaObjects.Count() < mediathreshold)
            {
                //3) Spec home elevations (excluding those that are still under construction and models), sorted by highest-priced spec
                specImages = _listingRepository.Specs
                   .Join(_imageRepository.Images, s => s.SpecId, i => i.SpecificationId, (s, i) => new { Specification = s, Images = i })
                   .Where(s => (s.Specification.SpecType == string.Empty || s.Specification.SpecType == null) &&  //specs
                       // s.Specification.MoveInDate < DateTime.Now && //ready to move in's  <= case# 47988
                            s.Images.ImageTypeCode.Equals(ImageTypes.Elevation, igCase) && s.Specification.CommunityId == communityId) //spec elevations
                   .OrderByDescending(s => s.Specification.Price)
                   .ThenBy(s => s.Images.ImageSequence)
                   .Take(mediathreshold - mediaObjects.Count())
                   .Select(s => s.Images).ToList();

                mediaObjects.AddRange(specImages.Select(image => new MediaPlayerObject()
                {
                    Type = MediaPlayerObjectTypes.Image,
                    SubType = MediaPlayerObjectTypes.SubTypes.ElevationImages,
                    Title = GetConstructedImageTitle(image, useImageTitle),
                    Thumbnail = GetConstructedImagePath(image, ImageSizes.HomeThumb, isMobile),
                    Caption = useImageTitle ? image.ImageDescription : image.ImageTitle,
                    Url = GetConstructedImagePath(image, ImageSizes.HomeMain, isMobile)
                }));
            }

            //if specs have no images, get images from plans attached to specs
            List<IImage> planImages = null;
            if (mediaObjects.Count() < mediathreshold) //dont worry about models at this point (if model has images then specs will have images: models are always specs)
            {
                var planIdsForSpecsAlreadyFound = (from s in specImages
                                                   select s.PlanId
                                                  ).ToList();

                var planIdsForSpecs = (from s in _listingRepository.Specs
                                       where
                                           s.CommunityId == communityId &&
                                           (s.SpecType == string.Empty || s.SpecType == null) && //exclude models when choosing fallback plan images
                                            s.MoveInDate < DateTime.Now && //ready to move in's 
                                           !planIdsForSpecsAlreadyFound.Contains(s.PlanId)
                                       orderby s.Price descending
                                       select s.PlanId).Take(mediathreshold - mediaObjects.Count()).ToList();

                if (planIdsForSpecs.Any())
                {
                    planImages = ((_listingRepository.Plans
                                        .Join(_imageRepository.Images, p => p.PlanId, i => i.PlanId,
                                        (p, i) => new { Plans = p, Images = i }))
                                        .WhereIn(p => p.Plans.PlanId, planIdsForSpecs)
                                        .Where(p => p.Images.ImageTypeCode.Equals(ImageTypes.Elevation, igCase) && //plan elevations
                                        (p.Images.ImageSequence == 1 || p.Images.ImageSequence == 2)))
                                        .OrderByDescending(p => p.Plans.Price)
                                        .ThenBy(p => p.Images.ImageSequence)
                                        .Take(mediathreshold - mediaObjects.Count())
                                        .Select(p => p.Images).ToList();

                    mediaObjects.AddRange(planImages.Select(image => new MediaPlayerObject()
                    {
                        Type = MediaPlayerObjectTypes.Image,
                        SubType = MediaPlayerObjectTypes.SubTypes.ElevationImages,
                        Title = GetConstructedImageTitle(image, useImageTitle),
                        Thumbnail = GetConstructedImagePath(image, ImageSizes.HomeThumb, isMobile),
                        Caption = useImageTitle ? image.ImageDescription.EscapeSingleQuote() : image.ImageTitle.EscapeSingleQuote(),
                        Url = GetConstructedImagePath(image, ImageSizes.HomeMain, isMobile)
                    }));
                }
            }

            //pull plan images
            if (mediaObjects.Count() < mediathreshold)
            {
                if (planImages == null)
                    planImages = new List<IImage>();

                var fallbackPlanIds = (from p in planImages //we need to ignore plans that have been added already
                                       select p.PlanId).ToList();

                planImages = (_listingRepository.Plans
                        .Join(_imageRepository.Images, p => p.PlanId, i => i.PlanId,
                              (p, i) => new { Plans = p, Images = i })
                        .Where(p => p.Images.ImageTypeCode.Equals(ImageTypes.Elevation, igCase) &&
                                    !fallbackPlanIds.Contains(p.Plans.PlanId) &&
                                    (p.Images.ImageSequence == 1 || p.Images.ImageSequence == 2) &&
                                    p.Plans.CommunityId == communityId && p.Images.SpecificationId == null))
                        .OrderByDescending(p => p.Plans.Price)
                        .ThenBy(p => p.Images.ImageSequence)
                        .Take(mediathreshold - mediaObjects.Count())
                        .Select(p => p.Images).ToList();

                mediaObjects.AddRange(planImages.Select(image => new MediaPlayerObject()
                {
                    Type = MediaPlayerObjectTypes.Image,
                    SubType = MediaPlayerObjectTypes.SubTypes.ElevationImages,
                    Title = GetConstructedImageTitle(image, useImageTitle),
                    Thumbnail = GetConstructedImagePath(image, ImageSizes.HomeThumb, isMobile),
                    Caption = useImageTitle ? image.ImageDescription.EscapeSingleQuote() : image.ImageTitle.EscapeSingleQuote(),
                    Url = GetConstructedImagePath(image, ImageSizes.HomeMain, isMobile),
                }));
            }

            //still not enough images, pull more plan images with sequence other than 1 and 2.
            if (mediaObjects.Count() < mediathreshold)
            {
                planImages = (_listingRepository.Plans
                        .Join(_imageRepository.Images, p => p.PlanId, i => i.PlanId,
                              (p, i) => new { Plans = p, Images = i })
                        .Where(p => p.Images.ImageTypeCode.Equals(ImageTypes.Elevation, igCase) &&
                                    (p.Images.ImageSequence != 1 && p.Images.ImageSequence != 2) &&
                                    p.Plans.CommunityId == communityId && p.Images.SpecificationId == null))
                        .OrderByDescending(p => p.Plans.Price)
                        .ThenBy(p => p.Images.ImageSequence)
                        .Take(mediathreshold - mediaObjects.Count())
                        .Select(p => p.Images).ToList();

                mediaObjects.AddRange(planImages.Select(image => new MediaPlayerObject()
                {
                    Type = MediaPlayerObjectTypes.Image,
                    SubType = MediaPlayerObjectTypes.SubTypes.ElevationImages,
                    Title = GetConstructedImageTitle(image, true),
                    Thumbnail = GetConstructedImagePath(image, ImageSizes.HomeThumb, isMobile),
                    Caption = useImageTitle ? image.ImageDescription.EscapeSingleQuote() : image.ImageTitle.EscapeSingleQuote(),
                    Url = GetConstructedImagePath(image, ImageSizes.HomeMain, isMobile)
                }));
            }

            //Add Vimeo video list from channel
            mediaObjects.Any(m =>
            {
                if (UrlHelper.IsVimeoChannel((m.Url)))
                {
                    var channelList = VimeoAPI.GetVideoListByChannelId(UrlHelper.GetVimeoChannelId(m.Url));
                    mediaObjects.AddRange(channelList.Select(video => new MediaPlayerObject()
                    {
                        Type = MediaPlayerObjectTypes.Video,
                        SubType = MediaPlayerObjectTypes.SubTypes.Vimeo,
                        Title = video.title,
                        Thumbnail = video.thumbnail_large,
                        Url = video.url,
                        IsVimeoVideo = true
                    }));
                    return true;
                }
                return false;
            });

            mediaObjects.ForEach(m =>
            {
                if (!string.IsNullOrEmpty(m.Url))
                {
                    m.IsYouTubeVideo = UrlHelper.IsYoutubeVideo(m.Url);
                    m.IsVimeoVideo = UrlHelper.IsVimeoVideo(m.Url);

                    if (m.IsYouTubeVideo)
                    {
                        m.SubType = MediaPlayerObjectTypes.SubTypes.YoutubeVideo;
                        var youtubeVideoId = UrlHelper.GetYoutubeVideoId(m.Url);

                        m.Thumbnail = string.Format("http://img.youtube.com/vi/{0}/hqdefault.jpg", youtubeVideoId);
                        m.Url = string.Format("http://img.youtube.com/vi/{0}/0.jpg", youtubeVideoId);
                        m.VideoID = youtubeVideoId;
                        m.OnlineVideoID = m.VideoID;
                    }
                    else if (m.IsVimeoVideo)
                    {
                        m.SubType = MediaPlayerObjectTypes.SubTypes.Vimeo;
                        var vimeoId = UrlHelper.GetVimeoVideoId(m.Url);

                        var vimeoVideo = VimeoAPI.GetVideobyId(vimeoId);

                        if (vimeoVideo != null)
                            m.Thumbnail = vimeoVideo.thumbnail_medium;

                        m.VideoID = vimeoId;
                        m.OnlineVideoID = m.VideoID;
                    }


                }
            });

            return mediaObjects.OrderByDescending(m => m.PreferredImage).ToList();
        }

        private string GetConstructedImageTitle(IImage image, bool useOnlyTitle = false)
        {
            return useOnlyTitle
                ? image.ImageTitle
                : string.Concat(image.ImageTitle, (!String.IsNullOrEmpty(image.ImageTitle) && !string.IsNullOrEmpty(image.ImageDescription)) ? ": " : string.Empty, image.ImageDescription);
        }

        private string GetConstructedImagePath(IImage image, string imageSize, bool isMobile)
        {
            var resizeCommands = ImageSizes.GetResizeCommands(imageSize);

            var url = image.ImagePathSafe.TrimEnd('/') + "/" +
                      (image.ImageNameSafe.IsValidImage()
                      ? image.ImageNameSafe
                      : image.ImageNameSafe);

            if (isMobile)
                return url;

            var parameters = (resizeCommands.HasValues) ? "?" + resizeCommands : String.Empty;

            return url + parameters;
        }

        public List<MediaPlayerObject> GetMediaPlayerVideoObjects(int communityId)
        {
            var videos = new List<IVideo>();

            if (_useHub)
                videos.AddRange(GetHubCommunity(communityId).Videos);
            else
                videos.AddRange(GetCommunity(communityId).Videos);

            //get videos
            var communityVideos = videos
                                    .Where(v => v.PlanId == null) //ensure only comm videos
                                    .OrderBy(video => video.Order)
                                    .Select(video => new { video.VideoId, video.Order, video.Title });

            //add community video mediaObjects
            var mediaObjects = communityVideos.Select(video => new MediaPlayerObject
            {
                Type = MediaPlayerObjectTypes.Video,
                VideoID = string.Format("cid-{0}-{1}-{2}", communityId, video.VideoId, Configuration.BrightcoveEnvSuffix),
                Title = video.Title.EscapeSingleQuote(),
                RefID = video.VideoId,
                Sort = video.Order
            }).ToList();

            return mediaObjects;
        }

        //TODO: Move to EDMX if possible
        public IList<Testimonial> GetCustomerTestimonials(int maxNoOfQuotes, int communityId, int builderId)
        {
            DataTable dt = DataProvider.Current.GetCustomerQuotes(maxNoOfQuotes, communityId, builderId, _useHub);

            return (from DataRow dr in dt.Rows
                    select new Testimonial
                    {
                        Description = DBValue.GetString(dr["testimonial_description"]),
                        Citation = DBValue.GetString(dr["citation"])
                    }).ToList();
        }

        //This return type uses the C# 4.0 CoVariance feature - IAmenity could be Amenity or OpenAmenity!
        public IDictionary<AmenityGroup, IList<IAmenity>> GetAmenities(IEnumerable<IAmenity> commAmenities, IEnumerable<IAmenity> commOpenAmenities)
        {
            IDictionary<AmenityGroup, IList<IAmenity>> amenities = new Dictionary<AmenityGroup, IList<IAmenity>>();

            var healthAndFitnessOpenAmenities = new List<IAmenity>();
            var communityServicesOpenAmenities = new List<IAmenity>();
            var localAreaOpenAmenities = new List<IAmenity>();
            var socialActivitiesOpenAmenities = new List<IAmenity>();
            var educationalOpenAmenities = new List<IAmenity>();

            var healthAndFitnessCommAmenities = new List<IAmenity>();
            var communityServicesCommAmenities = new List<IAmenity>();
            var localAreaCommAmenities = new List<IAmenity>();
            var socialActivitiesCommAmenities = new List<IAmenity>();

            foreach (var oa in commOpenAmenities)
            {
                switch (oa.GroupId)
                {
                    case OpenAmenityGroup.HealthAndFitness:
                        healthAndFitnessOpenAmenities.Add(oa);
                        break;
                    case OpenAmenityGroup.CommunityServices:
                        communityServicesOpenAmenities.Add(oa);
                        break;
                    case OpenAmenityGroup.LocalAreaAmenities:
                        localAreaOpenAmenities.Add(oa);
                        break;
                    case OpenAmenityGroup.SocialActivities:
                        socialActivitiesOpenAmenities.Add(oa);
                        break;
                    case OpenAmenityGroup.Educational:
                        educationalOpenAmenities.Add(oa);
                        break;
                }
            }

            /*  Business logic for grouping community amenities
             *  under Open Amenity Groups is as shown below. Although there seems be a relation betn
             *  OpenAmenity group and CommAmenity groups, a decision was made
             *  to do the correspondance as below
             *
             *   "OpenAmenity Group"
             *    -->   Included Community Amenities
             * 
                “Health and Fitness”  
                     -->   “Golf course”
                     -->   “Pool”
                     -->   “Trails”
                     -->   “Baseball”
                     -->   “Basketball”
                     -->   “Soccer”
                     -->   “Tennis”
                     -->   “Volleyball”
                “Community Services”
                     -->   “Community center”
                     -->   “Park”
                     -->   “Playground”
                     -->   “HOA”
                     -->   “Groundscare”
                     -->   “Security”
                     -->   “Maintenance” - N/A right now
                     -->   “Medical care”
                “Local Area Amenities”  
                     -->   “Views”
                     -->   “Greenbelt”
                     -->   “Nature”
                     -->   “Beach”
                     -->   “Lake”
                     -->   “Pond”
                     -->   “Waterfront lots”
                     -->   “Shopping” 
                “Social Activities”  
                     -->  “Clubhouse”
              */
            var tmpCommAmenityList = new List<int>();

            foreach (IAmenity a in commAmenities.Where(a => tmpCommAmenityList.IndexOf(a.AmenityId) == -1))
            {
                switch (a.AmenityId)
                {
                    case CommunityAmenity.GolfCourse:
                    case CommunityAmenity.Pool:
                    case CommunityAmenity.Trails:
                    case CommunityAmenity.Baseball:
                    case CommunityAmenity.Basketball:
                    case CommunityAmenity.Soccer:
                    case CommunityAmenity.Tennis:
                    case CommunityAmenity.Volleyball:
                        if (!HasSameAmenityText(healthAndFitnessOpenAmenities, a))
                            healthAndFitnessCommAmenities.Add(a);
                        break;
                    case CommunityAmenity.Clubhouse:
                        if (!HasSameAmenityText(socialActivitiesOpenAmenities, a))
                            socialActivitiesCommAmenities.Add(a);
                        break;
                    case CommunityAmenity.Communitycenter:
                    case CommunityAmenity.Park:
                    case CommunityAmenity.Playground:
                    case CommunityAmenity.HOA:
                    case CommunityAmenity.GroundsCare:
                    case CommunityAmenity.Security:
                    case CommunityAmenity.Medical:
                        if (!HasSameAmenityText(communityServicesOpenAmenities, a))
                            communityServicesCommAmenities.Add(a);
                        break;
                    case CommunityAmenity.Views:
                    case CommunityAmenity.Greenbelt:
                    case CommunityAmenity.Nature:
                    case CommunityAmenity.Beach:
                    case CommunityAmenity.Lake:
                    case CommunityAmenity.Pond:
                    case CommunityAmenity.Waterfrontlots:
                    case CommunityAmenity.Shopping:
                        if (!HasSameAmenityText(localAreaOpenAmenities, a))
                            localAreaCommAmenities.Add(a);
                        break;
                }
                tmpCommAmenityList.Add(a.AmenityId);
            }

            amenities.Add(AmenityGroup.HealthAndFitnessCommAmenities, healthAndFitnessCommAmenities.OrderBy(o => o.ShortNumber).ToList());
            amenities.Add(AmenityGroup.HealthAndFitnessOpenAmenities, healthAndFitnessOpenAmenities.OrderBy(o => o.ShortNumber).ToList());
            amenities.Add(AmenityGroup.CommunityServicesCommAmenities, communityServicesCommAmenities.OrderBy(o => o.ShortNumber).ToList());
            amenities.Add(AmenityGroup.CommunityServicesOpenAmenities, communityServicesOpenAmenities.OrderBy(o => o.ShortNumber).ToList());
            amenities.Add(AmenityGroup.LocalAreaCommAmenities, localAreaCommAmenities.OrderBy(o => o.ShortNumber).ToList());
            amenities.Add(AmenityGroup.LocalAreaOpenAmenities, localAreaOpenAmenities.OrderBy(o => o.ShortNumber).ToList());
            amenities.Add(AmenityGroup.EducationalOpenAmenities, educationalOpenAmenities.OrderBy(o => o.ShortNumber).ToList());
            amenities.Add(AmenityGroup.SocialActivitiesCommAmenities, socialActivitiesCommAmenities.OrderBy(o => o.ShortNumber).ToList());
            amenities.Add(AmenityGroup.SocialActivitiesOpenAmenities, socialActivitiesOpenAmenities.OrderBy(o => o.ShortNumber).ToList());
            return amenities;
        }

        private static bool HasSameAmenityText(IEnumerable<IAmenity> openAmenities, IAmenity a)
        {
            return openAmenities.Any(amenity => amenity.Description.ToLower().Trim() == a.Description.ToLower().Trim());
        }

        public Community GetCommunity(int communityId)
        {
            var cacheKey = WebCacheConst.WebCacheKeyPrefix + "Community_" + communityId.ToString();
            return _webCacheService.Get(cacheKey, false, new TimeSpan(0, 0, WebCacheConst.DefaultSeconds), () => _communityRepository.Communities.GetById(communityId));
        }

        public HubCommunity GetHubCommunity(int communityId)
        {
            var cacheKey = WebCacheConst.WebCacheKeyPrefix + "HubCommunity_" + communityId.ToString();
            return _webCacheService.Get(cacheKey, false, new TimeSpan(0, 0, 2), () => _communityRepository.HubCommunities.GetById(communityId));
        }

        public IList<Community> GetCommunitiesByMarketId(int marketId)
        {
            var cacheKey = WebCacheConst.WebCacheKeyPrefix + "CommByMarket_" + marketId.ToString();
            return _webCacheService.Get(cacheKey, false, new TimeSpan(0, 0, WebCacheConst.DefaultSeconds), () => _communityRepository.Communities.GetByMarketId(marketId).OrderBy(comm => comm.CommunityName).ToList());
        }



        public List<Community> GetCommunitiesByBrandId(int partnerId, int brandId, Brand brand = null)
        {
            string cacheKey = "CommunitiesByBrandId_" + partnerId + "_" + brandId;
            return _webCacheService.Get(cacheKey, false, () =>
            {
                var dtComms = DataProvider.Current.GetActiveCommunitiesForBrand(partnerId, brandId);
                var comms = new List<Community>();
                if (dtComms != null)
                {
                    comms = (from dr in dtComms.AsEnumerable()
                             select new Community
                             {
                                 CommunityName = dr.Field<string>("CommunityName"),
                                 CommunityId = dr.Field<int>("CommunityId"),
                                 BuilderId = dr.Field<int>("BuilderId"),
                                 MarketId = dr.Field<int>("MarketId"),
                                 City = dr.Table.Columns.Contains("City") ? dr.Field<string>("City") : string.Empty,
                                 Market = new Market
                                 {
                                     MarketId = dr.Field<int>("MarketId"),
                                     MarketName = dr.Field<string>("MarketName"),
                                     State =
                                         new State
                                         {
                                             StateAbbr = dr.Field<string>("StateAbbr"),
                                             StateName = _stateService.GetStateName(dr.Field<string>("StateAbbr"))
                                         }
                                 },
                                 BrandId = brandId,
                                 Brand = brand
                             }).ToList();
                }
                return comms;
            });
        }

        public string GetNonPdfBrochureUrl(int communityId, int planId, int specId, int builderId)
        {
            DataTable dtBrochs = null;
            if (planId != 0 || specId != 0)
                dtBrochs = DataProvider.Current.GetHomeBrochures(planId, specId);
            else if (communityId != 0)
            {
                dtBrochs = DataProvider.Current.GetCommunityBrochures(communityId, builderId);
            }

            var brochureUrl = string.Empty;
            if (dtBrochs == null) return brochureUrl;

            foreach (DataRow brochureRow in dtBrochs.Rows)
            {
                if (!string.IsNullOrEmpty(brochureRow["OriginalUrl"].ToType<string>()) &&
                    !brochureRow["OriginalUrl"].ToType<string>().ToLower().Contains(".pdf"))
                {
                    brochureUrl = brochureRow["OriginalUrl"].ToType<string>();
                    break;
                }
            }
            return brochureUrl;
        }

        public IList<Community> GetCommunities(List<int> communityIdList)
        {
            return _communityRepository.Communities.GetByIdList(communityIdList).ToList();
        }


        public IList<CommunityPromotion> GetCommunityPromotions(List<int> communityIdList)
        {
            return _communityRepository.CommunityPromotions.GetByIdList(communityIdList).ToList();
        }

        public object GetCommunityHomeMapPoints(SearchParams searchParameters, SearchResultsPageType srpType, int partnerId)
        {
            searchParameters.PageNumber = 0;
            searchParameters.PageSize = 0;
            var partnerSitePassword = _partnerService.GetPartner(partnerId).LeadPostingPassword;
            var was = new WebApiServices(searchParameters.ToWebApiParameters())
            {
                PartnerSitePassword = partnerSitePassword
            };
            //was.AddParameter(ApiUrlConstV2.PageSize, 5000);
            var data =
                was.GetData<ApiCommonResultModel<List<ApiMapPoint>>>(srpType == SearchResultsPageType.CommunityResults
                                                                                  ? WebApiMethods.CommunityLocations
                                                                                  : WebApiMethods.HomeLocations);
            var result = data.Result ?? new List<ApiMapPoint>();

            var group =
                result.Where(p => !string.IsNullOrEmpty(p.Lat) && !string.IsNullOrEmpty(p.Lng)).GroupBy(n => new { n.Lat, n.Lng })
                      .Select(
                          g =>
                          new
                          {
                              g.Key.Lat,
                              g.Key.Lng,
                              IsBasic = g.All(p => p.IsBasic == 1),
                              IsBl = g.All(p => p.IsBl == 1),
                              ResultType = g.All(p => p.IsBl == 1)
                                               ? "Homes"
                                               : g.Count(p => p.IsBl == 1) == 0 ? "Comms" : "CommsHomes",
                              Name = g.Count(),
                              HomesCount = g.Sum(p => p.NumHomes),
                              Price = g.All(p => p.IsBasic == 1 && p.NumHomes == 0) ? string.Empty :
                                                                StringHelper.PrettyPrintRangeWithStyles(g.Min(p => p.PrLo).ToType<double>(),
                                                                  g.Max(p => p.PrHi).ToType<double>(), "c0"),
                              MarketPoints = g,
                              BrandImage = g.Select(p => p.BrandLogo)
                          });

            return
                new
                {
                    Total = result.Count,
                    Results = group,
                    NoPlotted = result.Count(p => string.IsNullOrEmpty(p.Lat) && string.IsNullOrEmpty(p.Lng)),
                    HomeCount = data.ResultCounts.HomeCount + data.ResultCounts.BlCount,
                    data.ResultCounts.CommCount
                };
        }

        public IEnumerable<ApiFacetOption> GetBuilders(SearchParams searchParameters, int partnerId)
        {
            var builderSearchParams = new SearchParams
            {
                OriginLat = searchParameters.OriginLat,
                OriginLng = searchParameters.OriginLng,
                MaxLat = searchParameters.MaxLat,
                MaxLng = searchParameters.MaxLng,
                MinLat = searchParameters.MinLat,
                MinLng = searchParameters.MinLng,
                WebApiSearchType = searchParameters.WebApiSearchType,
                Radius = searchParameters.Radius,
                MarketId = searchParameters.MarketId,
                County = searchParameters.County,
                PostalCode = searchParameters.PostalCode,
                City = searchParameters.City
            };

            var partnerSitePassword = _partnerService.GetPartner(partnerId).LeadPostingPassword;
            var was = new WebApiServices(builderSearchParams.ToWebApiParameters())
            {
                PartnerSitePassword = partnerSitePassword
            };
            var data = was.GetData<ApiResultModel<IEnumerable<ApiFacetOption>>>(WebApiMethods.Builders);
            var result = data.Result ?? new List<ApiFacetOption>();

            return result;
        }

        public IList<CommunityPromotion> GetCommunityDetailPromotions(int communityId, int builderId)
        {
            return _communityRepository.CommunityPromotions.GetById(communityId, builderId).ToList();
        }

        public IList<CommunityEvent> GetCommunityEvents(List<int> communityIdList)
        {
            return _communityRepository.CommunityEvents.GetByIdList(communityIdList).ToList();
        }

        public IList<CommunityEvent> GetCommunityEvents(int communityId, int builderId)
        {
            return _communityRepository.CommunityEvents.GetByIdList(communityId, builderId).ToList();
        }

        public IList<CommunityAgentPolicy> GetCommunityAgentPolicies(int communityId, int builderId)
        {
            return _communityRepository.CommunityAgentPolicies.GetByIdList(communityId, builderId).ToList();
        }

        public ICollection<Domain.Model.Web.Utility> GetCommunityUtilities(int communityId, int partnerId)
        {
            var utilities = new List<Domain.Model.Web.Utility>();
            DataTable dtUtilities = DataProvider.Current.GetCommunityUtilities(communityId, partnerId, _useHub);

            if (dtUtilities != null && dtUtilities.Rows.Count > 0)
            {
                utilities.AddRange(from DataRow dr in dtUtilities.Rows
                                   select new Domain.Model.Web.Utility()
                                   {
                                       CommunityServiceDescription = Convert.ToString(dr["community_service_description"]),
                                       CommunityServicePhone = Convert.ToString(dr["community_service_phone"]),
                                       CommunityServiceTypeCode = Convert.ToString(dr["community_service_type_code"]),
                                       CommunityServiceTypeName = Convert.ToString(dr["community_service_type_name"]),
                                       CommunityServiceYearlyFee = DBValue.GetDecimal(dr["community_service_yearly_fee"]),
                                       CommunityServiceMonthlyFee = DBValue.GetDecimal(dr["community_service_monthly_fee"])
                                   });
            }

            return utilities;
        }

        public string GetSalesAgents(int salesOfficeId)
        {
            var agents = string.Empty;
            var dtAgents = DataProvider.Current.GetSalesAgents(salesOfficeId, _useHub);

            if (dtAgents != null && dtAgents.Rows.Count > 0)
                agents = Convert.ToString(dtAgents.Rows[0][0]);

            return agents;
        }

      
        public IEnumerable<Promotion> GetCommunityPromotions(int communityId, int builderId)
        {
            var promos = new List<Promotion>();
            var dtPromos = DataProvider.Current.GetCommunityPromotions(communityId, builderId, _useHub);

            if (dtPromos != null && dtPromos.Rows.Count > 0)
            {
                promos.AddRange(from DataRow dr in dtPromos.Rows
                                select new Promotion
                                {
                                    PromoId = Convert.ToInt32(dr["promo_id"]),
                                    PromoName = Convert.ToString(dr["promo_name"]),
                                    PromoUrl = Convert.ToString(dr["promo_url"]),
                                    PromoTextShort = Convert.ToString(dr["promo_text_short"]),
                                    PromoTextLong = Convert.ToString(dr["promo_text_long"]),
                                    PromoFlyerUrl = Convert.ToString(dr["flyer_file_path"]),
                                    PromoStartDate = Convert.ToDateTime(dr["start_date"]),
                                    PromoEndDate = Convert.ToDateTime(dr["end_date"])
                                });
            }
            return promos;
        }

        public IEnumerable<HotHome> GetCommunityHotDeals(IEnumerable<int> communityId)
        {
            IEnumerable<HotHome> hotHomes = new List<HotHome>();
            var dtHotHomes = DataProvider.Current.GetCommunityHotDeals(string.Join(",", communityId));

            if (dtHotHomes != null && dtHotHomes.Rows.Count > 0)
            {
                hotHomes = dtHotHomes.AsEnumerable().Select(dr => new HotHome
                {
                    NumBathrooms = Convert.ToString(dr["no_baths"]),
                    NumBedrooms = Convert.ToString(dr["no_bedrooms"]),
                    NumGarages = Convert.ToString(dr["no_car_garage"]),
                    NumHalfBathrooms = Convert.ToString(dr["no_half_baths"]),
                    Description = Convert.ToString(dr["detail_description"]),
                    Title = Convert.ToString(dr["title"]),
                    CommunityId = Convert.ToInt32(dr["community_id"]),
                    Name = Convert.ToString(dr["plan_name"]),
                    PlanId = Convert.ToString(dr["plan_id"]),
                    SpecId = Convert.ToString(dr["specification_id"]),
                    SpecAddress = Convert.ToString(dr["street_address"])
                });
            }
            return hotHomes;
        }

        public IEnumerable<GreenProgram> GetCommunityGreenPrograms(int communityId)
        {
            var greenPrograms = new List<GreenProgram>();
            var dtGreenPrograms = DataProvider.Current.GetCommunityGreenPrograms(communityId.ToString(), _useHub);

            if (dtGreenPrograms != null && dtGreenPrograms.Rows.Count > 0)
            {
                DataRow[] commPrograms = dtGreenPrograms.Select(string.Format("community_id = {0}", communityId));

                if (commPrograms.Any())
                {
                    greenPrograms.AddRange(from DataRow dr in commPrograms
                                           select new GreenProgram()
                                           {
                                               ProgramName = Convert.ToString(dr["display_name"]),
                                               ProgramUrl = !String.IsNullOrEmpty(Convert.ToString(dr["green_url"])) ?
                                                             Convert.ToString(dr["green_url"]) :
                                                             String.Format("{0}{1}", Configuration.ProgramFlyerUrl, Convert.ToString(dr["flyer_file_path"])),
                                               ProgramFlyerFilePath = Convert.ToString(dr["flyer_file_path"])
                                           });
                }
                else
                    greenPrograms.Add(new GreenProgram()
                    {
                        ProgramName = Convert.ToString(dtGreenPrograms.Rows[0]["display_name"]),
                        ProgramUrl = !string.IsNullOrEmpty(Convert.ToString(dtGreenPrograms.Rows[0]["green_url"])) ?
                                      Convert.ToString(dtGreenPrograms.Rows[0]["green_url"]) :
                                      string.Format("{0}{1}", Configuration.ProgramFlyerUrl, Convert.ToString(dtGreenPrograms.Rows[0]["flyer_file_path"])),
                        ProgramFlyerFilePath = Convert.ToString(dtGreenPrograms.Rows[0]["flyer_file_path"])
                    });
            }
            return greenPrograms;
        }

        public IEnumerable<GreenProgram> GetCommunityGreenPrograms(IEnumerable<int> communityId)
        {
            IEnumerable<GreenProgram> greenPrograms = new List<GreenProgram>();
            var dtGreenPrograms = DataProvider.Current.GetCommunityGreenPrograms(string.Join(",", communityId), _useHub);

            if (dtGreenPrograms != null && dtGreenPrograms.Rows.Count > 0)
            {
                greenPrograms = dtGreenPrograms.AsEnumerable().Select(dr => new GreenProgram
                {
                    ProgramName = Convert.ToString(dr["display_name"]),
                    ProgramUrl = !string.IsNullOrEmpty(Convert.ToString(dr["green_url"]))
                                     ? Convert.ToString(dr["green_url"])
                                     : string.Format("{0}{1}", Configuration.ProgramFlyerUrl,
                                                     Convert.ToString(dr["flyer_file_path"])),
                    ProgramFlyerFilePath = Convert.ToString(dr["flyer_file_path"]),
                    CommunityId = Convert.ToInt32(dr["community_id"])
                });
            }
            return greenPrograms;
        }

        public T GetRecommendedCommunities<T>(string email, int partnerId, int communityid, int specid, int planid, int basicHomeId, int recoNumber = 0, bool presentedExclude = false, bool recoOnlyCurrentBuilderComms = false) where T : new()
        {
            var commResults = GetRecommendedCommunitiesWebApi<T>(email, partnerId, communityid,
                                                                                     specid, planid, basicHomeId, recoNumber,
                                                                                     presentedExclude, recoOnlyCurrentBuilderComms);
            return commResults;
        }

        public List<ApiRecoCommunityResult> GetRecommendedCommunities(string email, int partnerId, int communityid, int specid, int planid, int basicHomeId,
                                                                      int recoNumber = 0, bool presentedExclude = false, bool recoOnlyCurrentBuilderComms = false,
                                                                      bool relaxSearchCriteria = false)
        {
            var commResults = GetRecommendedCommunitiesWebApi<List<ApiRecoCommunityResult>>(email, partnerId, communityid,
                                                                                     specid, planid, basicHomeId, recoNumber,
                                                                                     presentedExclude, recoOnlyCurrentBuilderComms, relaxSearchCriteria);
            return commResults;
        }

        public IList<NearbyCommunity> GetNearbyCommunities(int partnerId, int communityid, int basicHomeId, int recoNumber = 0, bool presentedExclude = false,
                                                           bool relaxSearchCriteria = false)
        {
            var commResults = GetRecommendedCommunities(string.Empty, partnerId, communityid, 0, 0, basicHomeId, recoNumber, presentedExclude, false, relaxSearchCriteria);

            return (from community in commResults
                    let market = _marketService.GetMarket(partnerId, community.MarketId, false)
                    select new NearbyCommunity
                    {
                        CommunityId = community.CommunityId,
                        CommunityName = community.CommunityName,
                        SpotlightThumbnail =
                            string.IsNullOrEmpty(community.CommunityImageThumbnail)
                                ? string.Empty
                                : community.CommunityImageThumbnail,
                        Latitude = community.Latitude.ToType<decimal>(),
                        Longitude = community.Longitude.ToType<decimal>(),
                        BuilderId = community.BuilderId,
                        BrandName = community.BrandName,
                        PriceHigh = community.PriceHigh,
                        PriceLow = community.PriceLow,
                        DistanceFromCommunity = community.Distance.ToType<decimal>(),
                        StateAbbr = community.State,
                        StateName = market.State.StateName,
                        City = community.City,
                        MarketName = market.MarketName
                    }).ToList();
        }

        private T GetRecommendedCommunitiesWebApi<T>(string email, int partnerId, int communityid, int specid, int planid, int basicHomeId, int recoNumber = 0,
                                                     bool presentedExclude = false, bool recoOnlyCurrentBuilderComms = false, bool relaxSearchCriteria = false)
            where T : new()
        {
            var partnerSitePassword = _partnerService.GetPartner(partnerId).LeadPostingPassword;
            var webApiServices = new WebApiServices { PartnerSitePassword = partnerSitePassword };

            if (communityid > 0)
                webApiServices.AddParameter(ApiUrlConstV2.CommunityId, communityid);

            if (specid > 0)
                webApiServices.AddParameter(ApiUrlConstV2.SpecId, specid);

            if (planid > 0)
                webApiServices.AddParameter(ApiUrlConstV2.PlanId, planid);

            if (basicHomeId > 0)
                webApiServices.AddParameter(ApiUrlConstV2.BasicHomeId, basicHomeId);

            if (recoNumber == 0)
                recoNumber = Configuration.RecoComNumberResult;

            webApiServices.AddParameter(ApiUrlConstV2.Email, email);
            webApiServices.AddParameter(ApiUrlConstV2.PartnerId, partnerId);
            webApiServices.AddParameter(ApiUrlConstV2.NumberOfRecomendations, recoNumber);
            webApiServices.AddParameter(ApiUrlConstV2.PricePadding, Configuration.RecoComPricePadding);
            webApiServices.AddParameter(ApiUrlConstV2.DaysExclude, Configuration.DuplicatePreventionFilterDays);
            webApiServices.AddParameter(ApiUrlConstV2.ExcludeBuilderFromResults, Configuration.RecoComExcludeComBuilder);
            webApiServices.AddParameter(ApiUrlConstV2.UsedMid, Configuration.RecoComUsedMID);
            webApiServices.AddParameter(ApiUrlConstV2.PresentedExclude, presentedExclude);
            webApiServices.AddParameter(ApiUrlConstV2.OnlyResultFromBuilder, recoOnlyCurrentBuilderComms);
            webApiServices.AddParameter(ApiUrlConstV2.RelaxSearchCriteria, relaxSearchCriteria);

            var commResults = webApiServices.GetData<WebApiResultModel<T>>(WebApiMethods.Recommendations).Result;
            if (commResults == null)
                commResults = new T();

            return commResults;
        }

        //These are sent right away after a lead is submitted on the on-page lead form. (Check box on first load)
        public int SendRecommendedProperties(LeadInfo leadInfo, IList<ApiRecoCommunityResult> recoComms, Community community)
        {
            if (recoComms.Count > 0)
            {
                var bcIds = string.Empty;
                foreach (var r in recoComms)
                {
                    if (!leadInfo.CommunityList.Split(',').Contains(r.CommunityId.ToString()))
                    {
                        //var distance =TemplateWordingHelper.LookUpDistance(commLocation.ArcDistance(new LatLong(r.Latitude.ToType<double>(), r.Longitude.ToType<double>())));
                        bcIds += string.Format("{0}|{1},", r.BuilderId, r.CommunityId);

                        var pl = new PlannerListing(r.CommunityId, ListingType.Community);
                        if (!UserSession.UserProfile.Planner.SavedHomes.Contains(pl))
                        {
                            UserSession.UserProfile.Planner.AddSavedCommunity(r.CommunityId, r.BuilderId);
                        }
                    }
                }

                UserSession.HasUserOptedForAlerts = true;

                leadInfo.CommunityList = bcIds.TrimEnd(',');
                leadInfo.LeadType = LeadType.Market;
                leadInfo.LeadAction = LeadAction.Recommended;
                leadInfo.Referer = UserSession.Refer;
                LeadUtil.GenerateLead(leadInfo);
                return recoComms.Count;
            }

            return 0;
        }

        public bool IsCommunityActiveOnParentBrandPartner(int communityId, int brandPartnerId)
        {
            var dtComm = DataProvider.Current.IsCommunityActiveOnParentBrandPartner(communityId, brandPartnerId);
            return dtComm != null && dtComm.Rows.Count > 0;
        }

        public bool AlreadyRequestBrosure(int partherId, int marketId, int communityId, string email, string requestType = "COM", int timePeriodDays = 60)
        {
            var ret = false;
            var data = DataProvider.Current.CountRequestBrosure(partherId, marketId, communityId, email, requestType, timePeriodDays);
            ret = data.Rows.Count > 0 && data.Rows[0][0].ToType<int>() > 0;

            return ret;
        }

        public List<CommunityMapCard> GetCommunityMapCards(int partnerId, string communityList, string basicListingsIds)
        {
            var commCards = new List<CommunityMapCard>();
            var dtComms = DataProvider.Current.GetCommunityMapCard(partnerId, communityList, basicListingsIds);
            if (dtComms != null && dtComms.Rows.Count > 0)
                commCards.AddRange(dtComms.AsEnumerable().Select(dr => new CommunityMapCard
                {
                    Id = dr["Id"].ToType<string>(),
                    Name = dr["Name"].ToType<string>(),
                    BId = dr["BuilderId"].ToType<int>(),
                    MId = dr["MarketId"].ToType<int>(),
                    Brand = dr["BrandName"].ToType<string>(),
                    City = dr["City"].ToType<string>(),
                    Image = dr["Image"].ToType<string>(),
                    PrHi = dr["PriceHigh"].ToType<int>(),
                    PrLo = dr["PriceLow"].ToType<int>(),
                    St = dr["State"].ToType<string>(),
                    Ct = dr["CommType"].ToType<string>(),
                    Zip = dr["PostalCode"].ToType<string>().TrimEnd(),
                    IsBasic = dr["IsBasic"].ToType<bool>(),
                    IsBasicListing = dr["IsBasicListing"].ToType<bool>(),
                    NunHom = dr["IsBasicListing"].ToType<int>(),
                    MinBath = dr["min_bath"].ToType<int>(),
                    MaxBath = dr["max_bath"].ToType<int>(),
                    MinBedroom = dr["min_bedroom"].ToType<int>(),
                    MaxBedroom = dr["max_bedroom"].ToType<int>(),
                    PhoneNumber = dr["phone_number"].ToType<string>(),
                    BrandImageUrl = dr["BrandLogoSmall"].ToType<string>()
                }));
            return commCards;
        }

        /// <summary>
        /// This method get the value of the two credit score links depending on the information saved in the XML file,
        /// if the property called UseOldCreditScoreLinks is true, then the two old credit score links will be used otherwise
        /// the new link will be used for both links
        /// </summary>
        /// <returns>CreditScoreLinks</returns>
        public void GetCreditScoreLinks(ref string creditScoreLink1, ref string creditScoreLink2)
        {
            try
            {
                var creditScoreLinks = _webCacheService.Get(WebCacheConst.CreditScoreLinks, false, BindCreditScoreLinks);

                if (creditScoreLinks.UseOldCreditScoreLinks)
                {
                    creditScoreLink1 = creditScoreLinks.CreditScoreLink1Old;
                    creditScoreLink2 = creditScoreLinks.CreditScoreLink2Old;
                }
                else
                {
                    creditScoreLink1 = creditScoreLink2 = creditScoreLinks.CreditScoreLink;
                }
            }
            catch (NullReferenceException ex)
            {
                ErrorLogger.LogError(ex);
            }
            catch (FileNotFoundException ex)
            {
                ErrorLogger.LogError(ex);
            }
        }

        public Community GetSavedCommunity(int partnerId, int communityId)
        {
            var dataProvider = DataProvider.Current;

            var dataTable = dataProvider.GetSavedCommunities(partnerId.ToString(CultureInfo.InvariantCulture), communityId.ToString(CultureInfo.InvariantCulture)).AsEnumerable();

            // As I am using the communityId I always get only One community
            var dataRow = dataTable.FirstOrDefault();

            Community community = null;

            if (dataRow != null)
            {
                community = new Community
                {
                    CommunityId = Int32.Parse(dataRow["community_id"].ToString()),
                    CommunityName = dataRow["community_name"].ToString(),
                    City = dataRow["city"].ToString(),
                    State = _stateService.GetState(dataRow["state"].ToString()),
                    PostalCode = dataRow["postal_code"].ToString(),
                    Brand = new Brand
                    {
                        BrandName = dataRow["brand_name"].ToString()
                    },
                    PriceLow = Convert.ToDecimal(dataRow["price_low"].ToString()),
                    PriceHigh = Convert.ToDecimal(dataRow["price_high"].ToString()),
                    HomeCount = Convert.ToInt16(dataRow["no_home_total"].ToString()),
                    BuilderId = Convert.ToInt32(dataRow["builder_id"].ToString()),
                    Builder = new Builder
                    {
                        BuilderId = Convert.ToInt32(dataRow["builder_id"].ToString()),
                        Url = dataRow["builder_url"].ToString()
                    },
                    ImageThumbnail = dataRow["image_thumbnail"].ToString(),
                    SpotlightThumbnail = dataRow["spotlight_thumbnail"].ToString(),
                    Status = dataRow["status_id"].ToString(),
                    ListingTypeFlag = dataRow["listing_type_flag"].ToString(),
                    Market = new Market
                    {
                        MarketId = Convert.ToInt32(dataRow["market_id"].ToString()),
                        MarketName = dataRow["market_name"].ToString()
                    },
                    MarketId = Convert.ToInt32(dataRow["market_id"].ToString()),
                    HasHotHome = Convert.ToInt32(dataRow["has_hot_home"].ToString()),
                    Latitude = Convert.ToDecimal(dataRow["Latitude"].ToString()),
                    Longitude = Convert.ToDecimal(dataRow["Longitude"].ToString()),
                    HasVideo = Convert.ToInt32(dataRow["HasVideo"].ToString())
                };
            }
            return community;
        }

        public CreditScoreLinks BindCreditScoreLinks()
        {

            var pathToXml =
            _pathMapper.MapPath(string.Format(CreditScoreLinksXmlPath, Configuration.StaticContentFolder));

            return XmlSerialize.DeserializeFromXmlFile<CreditScoreLinks>(pathToXml);

        }
    }

    [Serializable]
    public class MediaPlayerObject : ICloneable, IEquatable<MediaPlayerObject>
    {
        public string Type { get; set; }
        public string SubType { get; set; }
        public string Title { get; set; }
        public string TypeDescription { get; set; }
        public string Thumbnail { get; set; }
        public string Url { get; set; }
        public string Event { get; set; }
        public string VideoID { get; set; }
        public string OnlineVideoID { get; set; }
        public int RefID { get; set; }
        public int Sort { get; set; }
        public bool PreferredImage { get; set; }
        public bool IsYouTubeVideo { get; set; }
        public bool IsVimeoVideo { get; set; }
        public string Caption { get; set; }

        public bool Equals(MediaPlayerObject mediaObject)
        {
            return VideoID == mediaObject.VideoID;
        }

        public override int GetHashCode()
        {
            int hashVideoId = VideoID == null ? 0 : VideoID.GetHashCode();

            return hashVideoId;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

    }

    public class MediaPlayerObjectTypes
    {
        public static Dictionary<string, string> MediaDescriptions = new Dictionary<string, string>() { { "l-flp", "Floor Plan" }, { "l-vt", "Virtual Tour" }, { "l-ev", "External Video" } };
        public static string Video = "v";
        public static string Image = "i";
        public static string Link = "l";
        public static class SubTypes
        {
            public static string CommunityInterior = "ci";
            public static string HomeInterior = "hi";
            public static string ExternalVideo = "ev";
            public static string PlanVideo = "pv";
            public static string VirtualTour = "vt";
            public static string ElevationImages = "ele";
            public static string FloorPlanImages = "flp";

            public static string LotMap = "lm";

            public static string BrightcoveVideo = "bc";
            public static string YoutubeVideo = "yt";
            public static string Vimeo = "vm";
        }
    }


}
