﻿using System;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Library.Web;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class SessionCacheService : ISessionCacheService
    {

        public T Get<T>(string cacheId, bool refreshCache, Func<T> getItemCallback) where T : class
        {
            T item = null; 
            if (!refreshCache)
            {
                item = UserSession.GetItem(cacheId) as T;
            }
            
            if (item == null)
            {
                item = getItemCallback();
                UserSession.SetItem(cacheId, item);
            }
            return item;

        }
    } 
}
