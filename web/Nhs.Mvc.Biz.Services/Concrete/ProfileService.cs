﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Nhs.Library.Business;
using Nhs.Library.Enums;
using Nhs.Library.Helpers;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Data.Repositories;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class ProfileService : IProfileService
    {
        public IEnumerable<IGrouping<ListingType, PlannerListing>> GetSavedListings(string userId)
        { 
            var savedListings = DataProvider.Current.GetSavedListings(userId);
            var data = savedListings.AsEnumerable().Select(row => new PlannerListing
            {
                PlannerId = DBValue.GetInt(row["planner_id"]),
                UserId = DBValue.GetString(row["g_user_id"]),
                ListingType = EnumConversionUtil.StringToListingType(DBValue.GetString(row["listing_type"])),
                ListingId = DBValue.GetInt(row["listing_id"]),
                BuilderId = DBValue.GetInt(row["builder_id"]),
                SaveDate = DBValue.GetDateTime(row["save_date"]),
                LeadRequestDate = DBValue.GetDateTime(row["lead_request_date"]),
                RecommendedFlag = DBValue.GetBool(row["rec_flag"]),
                SuppressFlag = DBValue.GetBool(row["suppress_flag"]),
                SourceRequestKey = DBValue.GetString(row["source_request_key"])
            }).GroupBy(p=> p.ListingType);
            return data;
        }

    }
}
