using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Filters;
using Nhs.Mvc.Biz.Services.Helpers.Tv;
using Nhs.Mvc.Data.Repositories;
using Nhs.Utility.Common;
using System.Linq;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Utility.Constants;
using MarketMap = Nhs.Mvc.Biz.Services.Helpers.Tv.MarketMap;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class TvService : ITvService
    {
        private readonly IWebCacheService _webCacheService;
        private readonly IMarketService _marketService;
        private readonly IPathMapper _pathMapper;
        private readonly IListingService _listingService;
        private readonly ICommunityService _communityService;
        private readonly IApiService _apiService;
        private readonly ISeoContentService _seoContentService;

        private const string MarketMapXmlPath = "~/{0}/tv/MarketMap.xml";
        private const string MarketContentXmlPath = "~/{0}/tv/{1}/default.xml";

        #region PUBLIC

        public TvService(IWebCacheService webCacheService, IMarketService marketService, IPathMapper pathMapper, IListingService listingService,
            ICommunityService communityService, IApiService apiService, ISeoContentService seoContentService)
        {
            _webCacheService = webCacheService;
            _marketService = marketService;
            _pathMapper = pathMapper;
            _listingService = listingService;
            _communityService = communityService;
            _apiService = apiService;
            _seoContentService = seoContentService;
        }

        private void BindHomeData(TvMarketContent content)
        {
            var homeIds = new List<int>();
            foreach (var comm in content.Communities)
            {
                homeIds.AddRange(comm.Homes.Select(home => home.Id));
            }

            var homePlans = _listingService.GetPlansForPlanIds(homeIds, true);

            foreach (var home in content.GetAllHomes())
            {
                var plan = homePlans.FirstOrDefault(f => f.PlanId == home.Id);

                if (plan == null) continue;

                home.CommunityName = plan.Community.CommunityName;
                home.CommunityId = plan.CommunityId;
                home.BuilderLogoUrl = plan.Community.Builder.Brand.LogoMedium;
                home.BrandName = plan.Community.Builder.Brand.BrandName;
                home.BrandSiteUrl = plan.Community.Builder.Url;
                home.BuilderId = plan.Community.BuilderId;
                home.PlanName = plan.PlanName;
                home.PlanPrice = string.Format("{0:C0} - {1:C0}", plan.Community.PriceLow, plan.Community.PriceHigh);
                home.CityStateZip = string.Format("{0}, {1} {2}", plan.Community.City, plan.Community.State.StateAbbr, plan.Community.PostalCode);
                //home.
            }

        }

        public TvMarketContent GetTvMarketContent(string keyword, int marketId, int partnerId)
        {
            var cacheKey = string.Format(WebCacheConst.TvMarketContent, partnerId, keyword);
            return _webCacheService.Get(cacheKey, false, () => BindTvMarketContent(keyword, marketId));
        }

        public int GetPartnerMarketIdIfOnlyHaveOne(int partnerId, int brandPartnerId)
        {
            if (partnerId != brandPartnerId && brandPartnerId == PartnersConst.Pro.ToType<int>())
            {
                var markets = _marketService.GetMarkets(partnerId).AsQueryable();

                return markets.Count() == 1 ? markets.First().MarketId : 0;
            }

            return 0;
        }

        public IEnumerable<TvMarket> GetTvMarkets(int partnerId)
        {
            var cacheKey = WebCacheConst.TvMarkets + "_" + partnerId;
            return _webCacheService.Get(cacheKey, false, BindTvMarkets);
        }
        
        #endregion

        private MarketMap GetMarketMap()
        {
            var pathFile = _pathMapper.MapPath(string.Format(MarketMapXmlPath, Configuration.StaticContentFolder));
            var markets = XmlSerialize.DeserializeFromXmlFile<MarketMap>(pathFile);
            return markets;
        }

        private IEnumerable<TvMarket> BindTvMarkets()
        {
            var imagePath = "http://" + HttpContext.Current.Request.Url.Host + "/" + Configuration.StaticContentFolder + "/tv/markers/{0}";
            var tvmarkets = GetMarketMap().TvMarkets;
            var markets = string.Join(",", tvmarkets.Select(p => p.MarketId));
            var marketLatLongs = _marketService.GetMarkets(markets);
            var results =
                from c in tvmarkets
                join p in marketLatLongs on c.MarketId equals p.MarketId
                select
                    new TvMarket
                    {
                        Image = string.Format(imagePath, c.Image),
                        Keyword = c.Keyword,
                        MarketId = c.MarketId,
                        Latitude = p.LatLong.Latitude,
                        Longitude = p.LatLong.Longitude
                    };
            return results;
        }

        private TvMarketContent BindTvMarketContent(string keyword, int marketId)
        {
            var content = new TvMarketContent();
            var pathFileMarket = _pathMapper.MapPath(string.Format(MarketContentXmlPath, Configuration.StaticContentFolder, keyword));
            var coords = new List<LatLong>();
            try
            {
                content = XmlSerialize.DeserializeFromXmlFile<TvMarketContent>(pathFileMarket);
                var commIds = content.Communities.Select(p => p.Id).ToList();
                var communitiesByBuilders = GetCommunitiesInformation(commIds, marketId).ToList();

                content.CommunitiesForTvBuilder = new List<TvMarketCommunity>();

                foreach (var comm in communitiesByBuilders)
                {
                    var contentComm = content.Communities.FirstOrDefault(p => p.Id == comm.CommunityId);
                    coords.Add(new LatLong(comm.Latitude, comm.Longitude));

                    if (contentComm == null)
                    {
                        content.Communities.Add(new TvMarketContentCommunity
                        {
                            BuilderId = comm.BuilderId,
                            CommunityName = comm.CommunityName,
                            Id = comm.CommunityId,
                            IsFeature = false,
                            Latitude = comm.Latitude,
                            Longitude = comm.Longitude,
                            PriceRage = StringHelper.PrettyPrintRange(comm.PriceLow, comm.PriceHigh, "c0").Trim(),
                            Homes = new List<TvMarketContentCommunityHome>()
                        });
                    }
                    else
                    {
                        contentComm.Latitude = comm.Latitude;
                        contentComm.Longitude = comm.Longitude;
                        contentComm.BuilderId = comm.BuilderId;
                        contentComm.PriceRage = StringHelper.PrettyPrintRange(comm.PriceLow, comm.PriceHigh, "c0").Trim();
                        contentComm.CommunityName = comm.CommunityName;
                        contentComm.IsFeature = true;
                    }
                }

                var latlog = LatLong.GetCenterPointFromListOfCoordinates(coords);
                content.Latitude = latlog.Latitude;
                content.Longitude = latlog.Longitude;

                BindHomeData(content);
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }

            return content;
        }

        private IEnumerable<Community> GetCommunitiesInformation(List<int> commIds, int marketId)
        {
            var commInformation = _communityService.GetCommunities(commIds).ToList();
            var builderIDs = commInformation.Select(x => x.BuilderId).ToList();
            return  _communityService.GetCommunitiesByMarketId(marketId).Where(x => builderIDs.Contains(x.BuilderId)).ToList();
        }

        private IEnumerable<TvMarketCommunity> GetCommunitiesForTvBuilder(string communityIdList, int marketId)
        {
            var tvMarketCommunities = new List<TvMarketCommunity>();
            var dtTvMarketCommunities = DataProvider.Current.GetCommunitiesForTvBuilder(communityIdList, marketId);

            if (dtTvMarketCommunities != null && dtTvMarketCommunities.Rows.Count > 0)
            {
                tvMarketCommunities.AddRange(from DataRow dr in dtTvMarketCommunities.Rows
                                             select new TvMarketCommunity
                                             {
                                                 Latitude = Convert.ToDecimal(dr["Latitude"]),
                                                 Longitude = Convert.ToDecimal(dr["Longitude"]),
                                                 CommunityName = Convert.ToString(dr["CommunityName"]),
                                                 PriceRage = StringHelper.PrettyPrintRange(Convert.ToDecimal(dr["PriceLow"]), Convert.ToDecimal(dr["PriceHigh"]), "c0"),
                                                 BuilderId = Convert.ToInt32(dr["BuilderId"]),
                                                 Id = Convert.ToInt32(dr["CommunityId"])
                                             });
            }

            return tvMarketCommunities;
        }

        
    }
}
