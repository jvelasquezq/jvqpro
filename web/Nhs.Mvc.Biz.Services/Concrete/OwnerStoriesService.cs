﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Domain.Model;
using Nhs.Mvc.Domain.Model.BhiTransaction;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class OwnerStoriesServiceError
    {
        private IDictionary<string, string> _modelErros;
        private bool _hasError;
        public OwnerStoriesServiceError()
        {
            _modelErros = new Dictionary<string, string>();
        }

        public bool HasErrors
        {
            get { return _modelErros.Count > 0 || _hasError; }
            set { _hasError = value; }
        }
        public IDictionary<string,string> ModelErros {
            get { return _modelErros ; }            
        }
        

        public void AddModelError(string key, string description)
        {
            if (_modelErros.ContainsKey(key) == false)
            {
                _modelErros.Add(key, description);
            }
        }
    }

    public class OwnerStoriesService : IOwnerStoriesService
    {
        private static ConcurrentDictionary<string, string> MimeTypeToExtension = new ConcurrentDictionary<string, string>();

        private readonly IPathMapper _pathMapper;
        private readonly IWebCacheService _webCacheService;

        private const string OwnerStoriesXmlPath = "~/{0}/OwnerStories/OwnerStories.xml";

        #region Constructor
        public OwnerStoriesService(IWebCacheService webCacheService, IPathMapper pathMapper)
        {
            _pathMapper = pathMapper;
            _webCacheService = webCacheService;
        } 
        #endregion

        #region Public Methods

        public OwnerStoriesServiceError InsertOwnerStory(OwnerStory ownerStory, string shareFolderOwnerStoriesMediaPath)
        {
            var retValue = new OwnerStoriesServiceError();

            try
            {
                if (IsValidData(ownerStory, retValue) == false)
                {
                    return retValue;
                }

                SaveOwnerStoryFile(ownerStory, shareFolderOwnerStoriesMediaPath);
                DataProvider.Current.InsertOwnerStory(ownerStory);

                retValue.HasErrors = false;
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError("File Exception Owner Stories", "FilePath", shareFolderOwnerStoriesMediaPath);
                ErrorLogger.LogError(ex);
                retValue.AddModelError("Form", "Server Error, please try again");
                ErrorLogger.LogError(ex.ToString());
            }

            return retValue;
        }

        public OwnerStoriesServiceError CurateOnwerStory(OwnerStory curatedOwnerStory)
        {
            var retValue = new OwnerStoriesServiceError();

            try
            {
                if (IsValidData(curatedOwnerStory, retValue) == false)
                {
                    return retValue;
                }

                DataProvider.Current.UpdateOwnerStory(curatedOwnerStory);

                retValue.HasErrors = false;
            }
            catch (Exception ex)
            {
                retValue.AddModelError("Form", "Server Error, please try again");
                ErrorLogger.LogError(ex.ToString());
            }

            return retValue;
        }

        public OwnerStoriesContent GetOwnerStoriesContent()
        {
            return _webCacheService.Get(WebCacheConst.OwnerStoriesContent, false, BindOwnerStoriesContent);
        }

        public List<OwnerStory> BindOwnerStoriesList(int? builderId, bool? includeAllStatus, bool? status)
        {
            var dtOwnerStories = DataProvider.Current.GetOwnerStoriesList(builderId, includeAllStatus, status);
            var result = new List<OwnerStory>();
            foreach (var row in dtOwnerStories.AsEnumerable())
            {
                var story = new OwnerStory
                {
                    OwnerStoryId = row[0].ToType<int>(),
                    FirstName = row[1].ToType<string>(),
                    LastName = row[2].ToType<string>(),
                    Email = row[3].ToType<string>(),
                    BuilderName = row[4].ToType<string>(),
                    CommunityName = row[5].ToType<string>(),
                    City = row[6].ToType<string>(),
                    Category = row[7].ToType<string>(),
                    StoryDescription = row[8].ToType<string>(),
                    Status = row[10].ToType<bool?>(),
                    CreationDateTimeime = row[11].ToType<string>(),
                    BuilderId = row[12].ToType<int>(),
                    CommunityId = row[13].ToType<int>(),
                    ModificationDateTimeime = row[14].ToType<string>()
                };
                if (!String.IsNullOrEmpty(row[9].ToType<string>()))
                {
                    story.FilePath = string.Format("{0}/images/{1}", Configuration.IRSDomain,
                        row[9].ToType<string>().Replace(Configuration.ImageSharePath, "")).Replace("\\", "/");
                    story.HasImage = true;
                }
                else if (!String.IsNullOrEmpty(row[19].ToType<string>()))
                {
                    story.ProcessedVideoUrl = row[19].ToType<string>();
                    story.ProcessedVideoThumbnailUrl = row[19].ToType<string>();
                    story.ProcessedVideoTitle = string.Format("Owner Story Video by {0}", row[17].ToType<string>());
                    story.ProcessedVideoId = row[20].ToType<int>();
                    story.HasVideo = true;
                }
                else
                {
                    story.FilePath = string.Empty;
                }
                if (!string.IsNullOrEmpty(story.FilePath) && story.FilePath.EndsWith(".mov"))
                {
                    story.FilePath = string.Empty;
                    story.HasVideo = false;
                }
                result.Add(story);
            }
            return result;
        }

        public List<OwnerStory> GetOwnerStoriesList(int? builderId, bool? includeAllStatus, bool? status)
        {
            //The caching was removed as the curators need to see the changes applied immediately.  This still needs
            //to be discussed with Praveen/Scott
            //return _webCacheService.Get(WebCacheConst.OwnerStoriesList, false, () => BindOwnerStoriesList(builderId, includeAllStatus, status));
            return BindOwnerStoriesList(builderId, includeAllStatus, status);
        }

        #endregion

        #region Private Methods

        private OwnerStoriesContent BindOwnerStoriesContent()
        {
            var pathFile = _pathMapper.MapPath(string.Format(OwnerStoriesXmlPath, Configuration.StaticContentFolder));
            var ownerStory = XmlSerialize.DeserializeFromXmlFile<OwnerStoriesContent>(pathFile);

            for (int i = 1; i < ownerStory.OwnerStoryContentList.Count; i++)
            {
                ownerStory.OwnerStoryContentList[i].Sequence = i;
            }

            return ownerStory;
        }

        private static bool IsValidData(OwnerStory ownerStory, OwnerStoriesServiceError retValue)
        {
            bool isValid = true;

            if (string.IsNullOrWhiteSpace(ownerStory.Email) || StringHelper.IsValidEmail(ownerStory.Email) == false)
            {
                retValue.AddModelError("Email", @"Please provide a valid email address");
                isValid = false;
            }

            if (string.IsNullOrEmpty(ownerStory.FirstName) || string.IsNullOrEmpty(ownerStory.LastName))
            {
                retValue.AddModelError("Email", @"Please provide a valid first and last name");
                isValid = false;
            }

            if (string.IsNullOrEmpty(ownerStory.BuilderName))
            {
                retValue.AddModelError("Email", @"Please provide a valid builder name");
                isValid = false;
            }

            if (string.IsNullOrEmpty(ownerStory.City))
            {
                retValue.AddModelError("Email", @"Please provide a valid city name");
                isValid = false;
            }

            if (string.IsNullOrEmpty(ownerStory.StoryDescription))
            {
                retValue.AddModelError("Email", @"Please provide a story description");
                isValid = false;
            }

            if (string.IsNullOrEmpty(ownerStory.Category))
            {
                retValue.AddModelError("Email", @"Please provide a category");
                isValid = false;
            }

            if (string.IsNullOrWhiteSpace(ownerStory.MediaContentType) == false)
            {
                if (new List<string> { "video", "image" }.Any(a => ownerStory.MediaContentType.StartsWith(a)) == false)
                {
                    retValue.AddModelError("InvalidFormat", @"Please select a video or image");
                    isValid = false;
                }
                else if (new List<string> { "jpeg", "png", "gif", "mp4", "m4v", "mov", "quicktime" }.Any(a => ownerStory.MediaContentType.EndsWith(a)) == false)
                {
                    retValue.AddModelError("InvalidFormat", @"Please select a valid format for your video or image");
                    isValid = false;
                }
            }

            if (isValid)
            {                
                bool? isImage = null;

                if (ownerStory.MediaContentType.StartsWith("video"))
                {
                    isImage = false;
                }
                else if (ownerStory.MediaContentType.StartsWith("image"))
                {
                    isImage = true;
                }

                if (isImage != null)
                {
                    var size = isImage.Value ? 10485760 : 78643200;
                    var fileSizeInMegas = ownerStory.FileStream.Length;

                    if (size < fileSizeInMegas)
                    {
                        retValue.AddModelError("InvalidFormat", @"Please select a valid file size");
                        isValid = false;
                    }
                }
            }

            return isValid;
        }

        private static string ConvertMimeTypeToExtension(string mimeType)
        {
            var extensions = new Dictionary<string, string>
            {
                { @"image/jpeg", ".jpg" }, { @"image/gif", ".gif" }, { @"image/png", ".png" },
                { @"video/mp4", ".mp4" },  { @"video/m4v", ".m4v" }, { @"video/quicktime", ".mov" }
            };

            return extensions.ContainsKey(mimeType) ? extensions[mimeType] : string.Empty;
        }

        private bool SaveStreamToFile(string fileFullPath, Stream stream)
        {
            if (stream.Length == 0 || string.IsNullOrWhiteSpace(fileFullPath)) return false;

            try
            {
                using (FileStream fileStream = File.Create(fileFullPath, (int)stream.Length))
                {
                    var bytesInStream = new byte[stream.Length];
                    stream.Read(bytesInStream, 0, bytesInStream.Length);
                    fileStream.Write(bytesInStream, 0, bytesInStream.Length);

                }
                return true;
            }
            catch(Exception ex)
            {
                ErrorLogger.LogError("File Exception Owner Stories", "FilePath", fileFullPath);
                ErrorLogger.LogError(ex);
                return false;
            }
        }

        private void SaveOwnerStoryFile(OwnerStory ownerStory, string shareFolderOwnerStoriesMediaPath)
        {
            if (ownerStory.FileStream != null && ownerStory.FileStream.Length > 0)
            {
                var fileType = ConvertMimeTypeToExtension(ownerStory.MediaContentType.ToLower());
                var fullPath = shareFolderOwnerStoriesMediaPath + Guid.NewGuid() + fileType;
                var saveSuccess = SaveStreamToFile(fullPath, ownerStory.FileStream);
                if (saveSuccess == false)
                {
                    ErrorLogger.LogError("Error Savin the file : " + fullPath);
                    throw new IOException("Error saving the file");
                }
                ownerStory.FilePath = fullPath;
            }
        } 


        #endregion
    }
}
