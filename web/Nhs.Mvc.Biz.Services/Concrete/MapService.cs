﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Nhs.Library.Business;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class MapService : IMapService
    {
        private readonly IMarketService _marketService;
            
        public MapService(IMarketService marketService)
        {
            _marketService = marketService;
        }

        public City GeoCodeCityByMarket(string city, int marketId, string state)
        {
            City c = null;
            var dt = DataProvider.Current.GetCityGeoCodeByMarket(city, marketId, state);

            if (dt.Rows.Count > 0)
            {
                DataRow row;
                if (dt.Rows.Count > 1)
                {
                    row = (from myRow in dt.AsEnumerable()
                           where myRow.Field<string>("state") == state
                           select myRow).FirstOrDefault() ?? dt.Rows[0];
                }
                else
                {
                    row = dt.Rows[0];
                }
                

                c = new City
                    {
                        Name = DBValue.GetString(row["city"]),
                        State = DBValue.GetString(row["state"]),
                        County = DBValue.GetString(row["primary_county"]),
                        MarketId = DBValue.GetInt(row["primary_market_id"]),
                        Latitude = DBValue.GetDouble(row["latitude"]),
                        Longitude = DBValue.GetDouble(row["longitude"]),
                        Radius = DBValue.GetInt(row["radius"])
                    };
            }

            return c;
        }

        public PostalCode GeoCodePostalCode(string postalCode)
        {
            PostalCode p = null;
            var dt = DataProvider.Current.GetPostalCodeGeoCode(postalCode);

            if (dt.Rows.Count > 0)
            {
                var row = dt.Rows[0];

                p = new PostalCode
                    {
                        Radius = DBValue.GetInt(row["radius"]),
                        Code = DBValue.GetString(row["postal_code"]),
                        State = DBValue.GetString(row["state"]),
                        MarketId = DBValue.GetInt(row["market_id"]),
                        Latitude = DBValue.GetDouble(row["latitude"]),
                        Longitude = DBValue.GetDouble(row["longitude"]),
                        City = DBValue.GetString(row["city"]),
                    };
            }

            return p;
        }

        public County GeoCodeCountyByMarket(string county, int marketId)
        {
            County c = null;
            var dt = DataProvider.Current.GetCountyGeoCode(county, marketId);

            if (dt.Rows.Count > 0)
            {
                var row = dt.Rows[0];

                c = new County
                    {
                        Name = DBValue.GetString(row["county"]),
                        State = DBValue.GetString(row["state"]),
                        MarketId = DBValue.GetInt(row["primary_market_id"]),
                        Latitude = DBValue.GetDouble(row["latitude"]),
                        Longitude = DBValue.GetDouble(row["longitude"]),
                        Radius = DBValue.GetInt(row["radius"])
                    };
            }

            return c;
        }

        public Market GetMarketInfo(int marketId, int partnerId)
        {
            return _marketService.GetMarket(partnerId, marketId, false);
        }

        public List<Market> GetMarketsInfo(List<int> marketsId, int partnerId)
        {
            List<Market> markets = new List<Market>();

            foreach (var marketId in marketsId)
            {
                markets.Add(GetMarketInfo(marketId, partnerId));
            }

            return markets;
        }
    }
}
