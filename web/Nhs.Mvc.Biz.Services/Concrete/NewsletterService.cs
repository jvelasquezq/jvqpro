﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using Nhs.Library.Common;
using Nhs.Mvc.Biz.Services.Abstract;

namespace Nhs.Mvc.Biz.Services.Concrete
{    
    public class NewsletterService : INewsletterService
    {
        private readonly IMarketService _marketService;

        public NewsletterService(IMarketService marketService)
        {
            _marketService = marketService;
        }

        public IEnumerable<Nhs.Mvc.Domain.Model.Web.Market> GetMarkets(int partnerId)
        {
            var markets = _marketService.GetMarkets(partnerId).OrderBy(o => o.State.StateName).ThenBy(o => o.MarketName);
            return markets;
        }
        
        public bool SendRequest(string email, int marketId, string marketName, int partnerId, string partnerName, string partnerUrl, int brandPartnerId)
        {            
            var isSuccess = true;
            var sb = new StringBuilder(Configuration.ResponsysFormsBaseUrl);
            sb.Append(Configuration.ResponsysConsumerNewsletterId + "&");
            sb.AppendFormat("EMAIL={0}&", email);
            sb.AppendFormat("NHS_MARKET_ID={0}&", marketId);
            sb.AppendFormat("NHS_MARKET_NAME={0}&", marketName);
            sb.AppendFormat("NHS_PARTNER_ID={0}&", partnerId);
            sb.AppendFormat("NHS_PARTNER_NAME={0}&", partnerName);
            sb.AppendFormat("NHS_PARTNER_URL={0}&", partnerUrl);
            sb.AppendFormat("NHS_BRAND_PARTNER_ID={0}&", brandPartnerId);
            sb.AppendFormat("TIMESTAMP_={0}", DateTime.Now.ToString("MMM dd, yyyy hh:mm tt")); //Nov 19, 2013 04:03 PM
            var client = new WebClient();
            try
            {
                var responce = client.DownloadString(sb.ToString());
            }
            catch
            {
                isSuccess = false;
            }
            return isSuccess;
        }
        /// <summary>
        /// Used to send the contest entries from NHS TV to the Responsys list
        /// </summary>
        public bool SendContestRequest(string email, string fullName, int marketId, int brandPartnerId)
        {

            var isSuccess = true;
            var sb = new StringBuilder(Configuration.ResponsysFormsBaseUrl);
            sb.Append(Configuration.ResponsysTvContestId + "&");
            sb.AppendFormat("EMAIL={0}&", email);
            sb.AppendFormat("NHS_FULL_NAME={0}&", fullName);
            sb.AppendFormat("NHS_MARKET_ID={0}&", marketId);
            sb.AppendFormat("NHS_BRAND_PARTNER_ID={0}&", brandPartnerId);
            sb.AppendFormat("TIMESTAMP_={0}", DateTime.Now.ToString("MMM dd, yyyy hh:mm tt")); //Nov 19, 2013 04:03 PM
            var client = new WebClient();
            try
            {
                var responce = client.DownloadString(sb.ToString());
            }
            catch
            {
                isSuccess = false;
            }
            return isSuccess;
        }
    }
}
