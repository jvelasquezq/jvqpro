﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Filters;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Hub;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class BrandService : IBrandService
    {
        private readonly IBrandRepository _brandRepository;
        private readonly IWebCacheService _webCacheService;
        private readonly IStateService _stateService;

        public BrandService(IBrandRepository brandRepository, IWebCacheService webCacheService,  IStateService stateService)
        {
            _brandRepository = brandRepository;
            _webCacheService = webCacheService;
            _stateService = stateService;
        }

        public List<Brand> GetBrandsByState(int partnerId, string state)
        {
            var brands = GetBrandsByPartner(partnerId);
            var stateBrands = brands.Where(p => p.State.ToLower() == state.ToLower()).DistinctBy(p => p.BrandId).ToList();
            return stateBrands;
        }

        public List<Brand> GetBrandsByPartner(int partnerId)
        {
            var cacheKey = "BrandsByPartner_" + partnerId;
            var brands = _webCacheService.Get(cacheKey, false,
                () =>
                {
                    var dtBrands = DataProvider.Current.GetBrandsByPartner(partnerId);
                    var result = dtBrands.AsEnumerable().Select(row => new Brand
                    {
                        BrandId = (int) row[1],
                        BrandName = row[0].ToString(),
                        LogoSmall = row[2] != null ? row[2].ToString() : string.Empty,
                        LogoMedium = row[3] != null ? row[3].ToString() : string.Empty,
                        IsBasic = row[4] != null && row[4].ToType<bool>(),
                        MarketId = row[5] != null ? row[5].ToType<int>() : 0,
                        State = row[6] != null ? row[6].ToString() : string.Empty,
                    });
                    return result.ToList();
                });


            return brands;
        }


        public IList<Brand> GetBrandsByMarket(int partnerId, int marketId)
        {
            var brands = GetBrandsByPartner(partnerId);

            var brandsResult = marketId == 0
                ? brands
                : brands.Where(p => p.MarketId == marketId);

            return brandsResult.DistinctBy(p => p.BrandId).ToList();
        }

        /// <summary>
        /// Randomize a list of brands
        /// </summary>
        /// <param name="brands">The brands.</param>
        /// <param name="numBrands">Return list size</param>
        /// <param name="checkForLogo">if set to <c>true</c> [check for logo].</param>
        /// <returns></returns>
        /// DO NOT USE THIS METHOD FOR ANY NEW BRAND SORTING - Use brands.Shuffle() in conjunction with GetOnesWithLogos()
        public IList<Brand> GetRandomizedBrands(IList<Brand> brands, int numBrands, bool checkForLogo, int partnerId)
        {
            IList<Brand> promotedBrands = new List<Brand>();

            var tmpBrandIds = new List<int>();
            var random = new Random();

            // check to see if market has at least the number of brands being requested
            if (brands.Count() <= numBrands)
                numBrands = brands.Count();

            var j = 0; //counter to break out of loop...Do NOT use promotedBrands.Count() to break; app will crash.

            for (var i = 0; i < brands.Count(); i++)
            {
                var shrinkingBrandList = (from b in brands where !tmpBrandIds.Contains(b.BrandId) select b).ToList();
                var brd = shrinkingBrandList[random.Next(0, shrinkingBrandList.Count() - 1)];
                tmpBrandIds.Add(brd.BrandId);

                // check if this brand has the logo associated with it (if list was retrieved from MarketBC, it doesn't)
                if (checkForLogo && brd.LogoSmall == null)
                {
                    var b = GetBrandById(brd.BrandId, partnerId);

                    if (b != null)
                    {
                        brd.LogoSmall = b.LogoSmall;
                        brd.LogoMedium = b.LogoMedium;
                    }
                }

                if (!string.IsNullOrEmpty(brd.LogoSmall) && !string.IsNullOrEmpty(brd.LogoMedium) && !brd.LogoSmall.Contains("1x1.gif") && !brd.LogoMedium.Contains("1x1.gif"))
                {
                    j++;
                    promotedBrands.Add(brd);
                }

                if (j == numBrands)
                    break;
            }

            return promotedBrands;
        }

        public Brand GetBrandById(int brandId, int partnerId)
        {
            return this.GetAllBrands(partnerId).AsQueryable().GetById(brandId);
        }

        public IList<Brand> GetBrandsByIds(IList<int> brandIds, int partnerId)
        {
            return this.GetAllBrands(partnerId).AsQueryable().GetByIdList(brandIds).ToList();
        }

        public IEnumerable<IImage> GetShowCaseImages(int brandId)
        {
            string cacheKey = "ShowCaseImages_" + brandId;
            return _webCacheService.Get(cacheKey, false, () =>
            {
                var data = DataProvider.Current.GetShowCaseImagesForBrand(brandId);

                var images = new List<Image>();
                if (data != null && data.Rows.Count > 0)
                {
                    images = data.Rows.Cast<DataRow>().Select(row => new Image
                    {
                        ImageId = DBValue.GetInt(row["ImageId"]),
                        ImageName = DBValue.GetString(row["ImageName"]),
                        ImagePath = DBValue.GetString(row["ImagePath"]),
                        ClickThruUrl = DBValue.GetString(row["ClickThruUrl"]),
                        ImageTitle = DBValue.GetString(row["ImageTitle"]),
                        ImageDescription = DBValue.GetString(row["ImageDescription"]),
                        ImageTypeCode = DBValue.GetString(row["ImageTypeCode"]),
                        ImageSequence = DBValue.GetInt(row["ImageSequence"]),
                    }).ToList();
                }

                return images.OrderBy(i => i.ImageSequence);
            });
        }

        public List<Brand> GetMarketandStateByBrand(int brandId, int partnerId)
        {
            string cacheKey = "MarketandStateByBrand_" + partnerId + "_" + brandId;
            return _webCacheService.Get(cacheKey, false, () =>
            {
                DataTable data = DataProvider.Current.GetMarketandStateByBrand(brandId, partnerId);
                var brand = new List<Brand>();
                if (data != null && data.Rows.Count > 0)
                {
                    brand = data.Rows.Cast<DataRow>().Select(row => new Brand
                    {
                        BrandId = brandId,
                        State = DBValue.GetString(row["state_name"]),
                        MarketId = DBValue.GetInt(row["market_id"]),
                        MarketName = DBValue.GetString(row["market_name"])
                    }).ToList();
                }
                return brand;
            });
        }

        public IList<Brand> GetAllBrands(int partnerId)
        {
            string cacheKey = "AllBrands_" + (partnerId > 1000 ? "CNA" : "ALL");
            return _webCacheService.Get(cacheKey, false, () => _brandRepository.Brands.ToList());
        }

        public IEnumerable<ExtendedCommunityResult> GetBrandSpotlights(int partnerId, int brandId)
        {
            IList<ExtendedCommunityResult> brandSpotlights = new List<ExtendedCommunityResult>();
            var dtComms = DataProvider.Current.GetBrandSpotlightCommunities(partnerId, brandId);
            if (dtComms != null)
            {
                foreach (DataRow communityResult in dtComms.Rows)
                {
                    var cr = new ExtendedCommunityResult
                        {
                            CommunityId = DBValue.GetInt(communityResult[CommunityDBFields.CommunityId]),
                            CommunityName = DBValue.GetString(communityResult[CommunityDBFields.CommunityName]),
                            BrandName = DBValue.GetString(communityResult[CommunityDBFields.BrandName]),
                            BuilderId = DBValue.GetInt(communityResult[CommunityDBFields.BuilderId]),
                            City = DBValue.GetString(communityResult[CommunityDBFields.City]),
                            State = DBValue.GetString(communityResult[CommunityDBFields.State]),
                            PriceLow = DBValue.GetDecimal(communityResult[CommunityDBFields.PriceLow]).ToType<int>(),
                            PriceHigh = DBValue.GetDecimal(communityResult[CommunityDBFields.PriceHigh]).ToType<int>(),
                            CommunityImageThumbnail = DBValue.GetString(communityResult[CommunityDBFields.SpotlightThumbnail]),
                            BrandImageThumbnail = DBValue.GetString(communityResult[CommunityDBFields.SpotlightThumbnail]),
                            MatchingHomes = DBValue.GetInt(communityResult[CommunityDBFields.NumberHomesTotal]),
                            MarketId = DBValue.GetInt(communityResult[CommunityDBFields.MarketId]),
                            MarketName = DBValue.GetString(communityResult[CommunityDBFields.MarketName]),
                            StateName = _stateService.GetStateName(DBValue.GetString(communityResult[CommunityDBFields.State]))
                        };

                    brandSpotlights.Add(cr);
                }

            }
            return brandSpotlights;
        }

        public void SubmitBrandShowCase(int brandId, string overview, string description, string rssFeed,
                                        string twitterWidge, string facebookWidget, string contactEmail,
                                        string brandUrl)
        {
            DataProvider.Current.SubmitBrandShowCase(brandId, overview, description, rssFeed, twitterWidge,
                                                     facebookWidget, contactEmail, brandUrl);
        }

        public int SubmitBuilderShowCaseTestimonial(int testimonialId, int brandId, int sequence, string citation,
                                                    string description)
        {
            return DataProvider.Current.SubmitBuilderShowCaseTestimonial(testimonialId, brandId, sequence, citation, description);
        }

        public void DeleteBuilderShowCaseTestimonial(int testimonialId)
        {
            DataProvider.Current.DeleteBuilderShowCaseTestimonial(testimonialId);
        }

        public void UpdateBrandShowCaseImage(int imageId, string description, string title, string accreditationSealUrl)
        {
            DataProvider.Current.UpdateBrandShowCaseImage(imageId, description, title, accreditationSealUrl);
        }

        public int AddBrandShowCaseImage(string imageType, int builderId, string originalUri, string name,
                                         string description, string title, int sequence, string accreditationSealUrl)
        {
            return DataProvider.Current.AddBrandShowCaseImage(imageType, builderId, originalUri, name, description, title,
                                                         sequence, accreditationSealUrl);
        }

        public void DeleteBrandShowCaseImage(string imageids)
        {
            DataProvider.Current.DeleteBrandShowCaseImage(imageids);
        }

        public IEnumerable<HubBrand> HubBrands
        {
            get { return _brandRepository.HubBrands; }
        }

        public IEnumerable<HubBrandShowCase> HubBrandShowCase
        {
            get { return _brandRepository.HubBrandShowCase; }
        }

    }
}