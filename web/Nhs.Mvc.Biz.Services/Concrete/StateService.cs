﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Filters;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Utility.Common;
using Nhs.Utility.Data;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class StateService : IStateService
    {
        private IStateRepository _stateRepository;
        private IMarketService _marketService;
        private IWebCacheService _webCacheService;

        public StateService(IWebCacheService webCacheService, IStateRepository stateRepository, IMarketService marketService)
        {
            _webCacheService = webCacheService;
            _stateRepository = stateRepository;
            _marketService = marketService;
        }

        public IList<State> GetSearchAlertStates(int partnerId)
        {
            string key = WebCacheConst.WebCacheKeyPrefix + WebCacheConst.PartnerMarkets + partnerId.ToString();
            IEnumerable<Market> markets = (List<Market>)Caching.GetObjectFromCache(key);

            if (markets == null)
            {
                markets = _marketService.GetMarkets(partnerId);
                Caching.AddObjectToCache(markets, key);
            }

            var alertStates = new List<State>();

            var alertMarkets = markets.Where(m => m.TotalCommunities > 0);
            var states = this.GetAllStates();

            foreach (var market in alertMarkets)
            {
                var st = states.SingleOrDefault(m => m.StateAbbr == market.StateAbbr);
                if (!alertStates.Contains(st)) alertStates.Add(st);
            }

            alertStates.Sort(new PropertyComparer<State>("StateName"));
            return alertStates;
        }

        public string GetStateAbbreviation(string name)
        {
            var st = this.GetAllStates().AsQueryable().GetByNameOrAbbr(name);
            return st != null ? st.StateAbbr : null;
        }

        public string GetStateName(string abbreviation)
        {
            var st = this.GetAllStates().AsQueryable().GetByNameOrAbbr(abbreviation);
            return st != null ? st.StateName : null;
        }

        public State GetState(string abbreviation)
        {
            return this.GetAllStates().AsQueryable().GetByNameOrAbbr(abbreviation);
        }

        public IList<State> GetAllStates()
        {
            const string cacheKey = WebCacheConst.WebCacheKeyPrefix + WebCacheConst.AllStatesList;
            return _webCacheService.Get(cacheKey, false, () => _stateRepository.States.ToList());
        }

        public IList<State> GetPartnerStates(int partnerId)
        {
            return this.GetActiveStatesForPartner(partnerId);
        }

        public State GetStateCoodinatesFromStateName(string statename)
        {
            var cacheKey = WebCacheConst.WebCacheKeyPrefix + WebCacheConst.AllStatesWithCoodinatesList;
            var data = _webCacheService.Get(cacheKey, false, new TimeSpan(0, 0, WebCacheConst.DefaultMins, 0), () => DataProvider.Current.GetStateMapLocations());
            var row = data.AsEnumerable().FirstOrDefault(p => p["statename"].ToString().Equals(statename, StringComparison.CurrentCultureIgnoreCase));

            State stateCoordinates = row != null
                                       ? new State()
                                       {
                                           StateAbbr = row["state"].ToString(),
                                           Latitude = Convert.ToDecimal(row["StateLat"]),
                                           Longitude = Convert.ToDecimal(row["StateLong"])
                                       }
                                       : null;

            return stateCoordinates;
        }

        public State GetStateCoodinates(string state)
        {
            var cacheKey = WebCacheConst.WebCacheKeyPrefix + WebCacheConst.AllStatesWithCoodinatesList;
            var data = _webCacheService.Get(cacheKey, false, new TimeSpan(0, 0, WebCacheConst.DefaultMins, 0), () => DataProvider.Current.GetStateMapLocations());
            var row = data.AsEnumerable().FirstOrDefault(p => p["state"].ToString().Equals(state, StringComparison.CurrentCultureIgnoreCase));

            State stateCoordinates = row != null
                                       ? new State()
                                           {
                                               StateAbbr = row["state"].ToString(),
                                               Latitude = Convert.ToDecimal(row["StateLat"]),
                                               Longitude = Convert.ToDecimal(row["StateLong"]),
                                               StateName = row["statename"].ToString()
                                           }
                                       : null;

            return stateCoordinates;
        }

        public StateInfo GetStateInfo(int partnerId, string stateName)
        {
            string stateAbbr = GetStateAbbreviation(stateName);
            var cacheKey = WebCacheConst.WebCacheKeyPrefix + WebCacheConst.StateAmenityCounts + stateAbbr;
            DataTable data = _webCacheService.Get(cacheKey, false, new TimeSpan(0, 0, WebCacheConst.DefaultMins, 0), () => DataProvider.Current.GetAmenityCounts(partnerId, stateAbbr, 0));

            StateInfo stateInfo = null;

            if (data.Rows.Count > 0)
            {
                stateInfo = new StateInfo();
                stateInfo.State = stateAbbr;
                stateInfo.StateName = stateName;
                stateInfo.ContainsGolfCommunities = data.Rows[0]["GolfCourse"].ToType<int>() > 0;
                stateInfo.ContainsCondoTownCommunities = data.Rows[0]["CondoTownhome"].ToType<int>() > 0;
                stateInfo.ContainsWaterFrontCommunities = data.Rows[0]["WaterFront"].ToType<int>() > 0;
            }

            return stateInfo;
        }


        private IList<State> GetActiveStatesForPartner(int partnerId)
        {
            var markets = _marketService.GetMarkets(partnerId);
            return (from market in markets
                    select new State
                    {
                        StateName = market.State.StateName,
                        StateAbbr = market.State.StateAbbr,
                        Latitude = market.State.Latitude,
                        Longitude = market.State.Longitude
                    }).GroupBy(s => s.StateAbbr).Select(g => g.First()).OrderBy(s => s.StateName).ToList();
        }
    }
}
