﻿using System;
using System.Linq;
using Nhs.Library.Constants;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Utility.Common;
using Nhs.Library.Common;
using Nhs.Mvc.Biz.Services.Helpers.AffiliateLinks;
using Nhs.Mvc.Domain.Model.BHIContent;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class AffiliateLinkService : IAffiliateLinkService
    {
        private readonly IWebCacheService _webCacheService;
        private readonly IPathMapper _pathMapper;
        private const string AffiliateLinksXmlPath = "~/{0}/SEOContent/AffiliateLinks/{1}.xml";

        public AffiliateLinkService(IWebCacheService webCacheService, IPathMapper pathMapper)
        {
            _webCacheService = webCacheService;
            _pathMapper = pathMapper;
        }

        public AffiliateLinks GetAffiliateLinks(string page, int partnerId)
        {
            var cacheKey = string.Format(WebCacheConst.AffiliateLinks, partnerId, page);
            return _webCacheService.Get(cacheKey, false, () => BindAffiliateLinks(page));
        }

        private AffiliateLinks BindAffiliateLinks(string page)
        {
            var links = new AffiliateLinks();
            var pathFileMarket = _pathMapper.MapPath(string.Format(AffiliateLinksXmlPath, Configuration.StaticContentFolder, page));

            try
            {
                links = XmlSerialize.DeserializeFromXmlFile<AffiliateLinks>(pathFileMarket);
                var defaultLinks = DefaultAffiliateLinks.GetDefaultAffiliateLinks();
                links.LinksList.AddRange(defaultLinks.Where(x => links.LinksList.FindIndex(y => y.Keyword == x.Keyword) == -1));
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }

            return links;
        }
    }
}
