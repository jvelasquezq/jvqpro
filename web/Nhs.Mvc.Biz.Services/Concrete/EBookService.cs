﻿using System;
using System.Net;
using System.Text;
using BHI.EnterpriseLibrary.Core.ExceptionHandling;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class EBookService : IEBookService
    {
        private readonly IPartnerService _partnerService;
        private readonly INewsletterService _newsletterService;
        private readonly IMarketService _marketService;

        public EBookService(IPartnerService partnerService, INewsletterService newsletterService, IMarketService marketService)
        {
            _partnerService = partnerService;
            _newsletterService = newsletterService;
            _marketService = marketService;
        }

        /// <summary>
        /// Creates the eBook lead.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="partnerId"></param>
        /// <param name="source"></param>
        public bool CreateeBookLead(string email, int partnerId, string source)
        {
            var partner = _partnerService.GetPartner(partnerId);
            const string leadType = "eBook";
            var queue = Configuration.MSMQeBook;
            var lead = new Library.Business.Lead(LeadType.Market);

            //Create lead user object and assign values
            var leadUserInfo = new Library.Business.LeadUserInfo { Guid = UserSession.SessionId, Email = email };

            lead.AddUserInfo(leadUserInfo);
            lead.AddSiteInfo(partner.FromEmail, partner.BrandPartnerId, partnerId, partner.PartnerSiteUrl, UserSession.Refer, "", "");

            try
            {
                lead.SendtoQ(queue);
                return SendRequestLead(email, partnerId, source);
            }
            catch (Exception ex)
            {
                string leadXml;
                try
                {
                    leadXml = lead.GetXml();
                }
                catch
                {
                    leadXml = "Could not retrieve lead xml";
                }

                var exception = new Exception("Error sending lead to eBook queue;leadtype=" + LeadType.Market + "(" + leadType + ");leadxml=" + leadXml, ex);
                exception.Data.Add("LeadType", LeadType.Market + "(" + leadType + ")");
                exception.Data.Add("LeadXml", leadXml);
                exception.Data.Add("Queue", queue);
                try
                {
                    ExceptionPolicy.HandleException(exception, ExceptionPolicies.LeadPolicy);
                }
                catch (Exception ex2)
                {
                    ErrorLogger.LogError(ex2.Message, "ex2", "ex2");
                }
                ErrorLogger.LogError(ex.Message, "ex", "ex");
                return false;
            }
        }

        public bool SignUpeBookModal(bool homeOfTheWeek, bool weeklyMarketUpdate, int marketId, string email, int partnerId, int brandPartnerId,string partnerSiteUrl)
        {
            var isSuccess = false;
            if (homeOfTheWeek)
            {
                var sb = new StringBuilder("http://info.newhomesource.com/pub/rf?");
                sb.Append("NHS_HOTW_PERMISSION_STATUS=1&NHS_ACCOUNT_STATUS=1&NHS_LEAD_SOURCE=HOTW NHS Site Opt-in&");
                sb.AppendFormat("EMAIL_ADDRESS_={0}&", email);
                sb.AppendFormat("NHS_LEAD_DATE={0}&", DateTime.Now.ToString("MMM dd, yyyy hh:mm tt"));
                sb.Append("_ri_=X0Gzc2X%3DWQpglLjHJlYQGNjdmjmza96dOmzeRUdsYvjF6GPRVwjpnpgHlpgneHmgJoXX0Gzc2X%3DWQpglLjHJlYQGtzdESYzdtTi9suDuiD2Mqnceqtn");
                var client = new WebClient();
                try
                {
                    var responce = client.DownloadString(sb.ToString());
                    isSuccess = true;
                }
                catch
                {
                    isSuccess = false;
                }
            }

            if (weeklyMarketUpdate)
            {
                var partner = _partnerService.GetPartner(partnerId);
                var market = _marketService.GetMarket(marketId);

                isSuccess = _newsletterService.SendRequest(email, marketId, market.MarketName, partnerId, partner.PartnerName, partnerSiteUrl, brandPartnerId);

            }

            return isSuccess;
        }

        private bool SendRequestLead(string email, int partnerId, string source)
        {
            var sb = new StringBuilder("http://info.newhomesource.com/pub/rf?");
            sb.AppendFormat("NHS_EBOOK_PERMISSION_STATUS=1&NHS_ACCOUNT_STATUS=1&NHS_LEAD_SOURCE={0}&", source);
            sb.AppendFormat("EMAIL_ADDRESS_={0}&", email);
            sb.AppendFormat("NHS_PARTNER_ID={0}&", partnerId);
            sb.AppendFormat("TIMESTAMP_={0}&", DateTime.Now.ToString("MMM dd, yyyy hh:mm tt")); //Nov 19, 2013 04:03 PM
            sb.Append("_ri_=X0Gzc2X%3DWQpglLjHJlYQGhzadcaLzd6CNKzaLNuh61czfHzfWsKBPVwjpnpgHlpgneHmgJoXX0Gzc2X%3DWQpglLjHJlYQGkzdezdnNzfClejrF3PL1iJTi4O45GK");
            var client = new WebClient();
            try
            {
                var responce = client.DownloadString(sb.ToString());
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
