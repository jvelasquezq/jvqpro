﻿using System;
using System.Data;
using System.Web;
using BHI.EnterpriseLibrary.Core.ExceptionHandling;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Exceptions;
using Nhs.Library.Helpers;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Filters;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Utility.Constants;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class UserProfileService : IUserProfileService
    {
        private readonly IUserProfileRepository _userProfileRepository;
        private readonly IPartnerService _partnerService;
        private readonly IMarketService _marketService;

        public UserProfileService(IUserProfileRepository userProfileRepository, IPartnerService partnerService, IMarketService marketService)
        {
            _userProfileRepository = userProfileRepository;
            _partnerService = partnerService;
            _marketService = marketService;
        }

        public bool LogonNameExists(string logonName, int partnerId)
        {
            var profile = GetUserByLogonName(logonName, partnerId);
            return profile != null;
        }

        public UserProfile GetUserByLogonName(string logonName, int partnerId)
        {
            return _userProfileRepository.Users.GetByLogonName(logonName, partnerId);
        }

        public string GetUserIdByEmail(string email)
        {
            return _userProfileRepository.Users.GetUserIdByEmail(email);
        }

        public UserProfile GetUserBySsoId(string ssoId, int partnerId)
        {
            return _userProfileRepository.Users.GetBySsoId(ssoId, partnerId);
        }

        public UserProfile GetUserByGuid(string guid)
        {
            return _userProfileRepository.Users.GetByGuid(guid);
        }
        public void SignIn(string logonName, int partnerId, string wrongEmailMessage)
        {
            // Get data for specified logon name. 
            var profile = this.GetUserByLogonName(logonName, partnerId);

            if (profile != null)
            {
                // User name and password are valid, set active session
                SetActiveSession(profile);

                //Write profile cookie
                WriteProfileCookie(profile);
            }
            else
            {
                throw new UnknownUserException(wrongEmailMessage);
       }
        }

        public void SignIn(string logonName, string password, int partnerId, string wrongPasswordMessage, string wrongEmailMessage)
        {
            // Get data for specified logon name. 
            var profile = this.GetUserProfile(logonName, password, partnerId);

            if (profile != null)
            {
                // User name and password are valid, set active session
                SetActiveSession(profile);

                //Write profile cookie
                WriteProfileCookie(profile);
            }
            else
            {
                if (LogonNameExists(logonName, partnerId))
                {
                    throw new InvalidPasswordException(wrongPasswordMessage);
                }
                // Logon name does not exist. 
                throw new UnknownUserException(wrongEmailMessage);
            }
        }

        /// <summary>
        /// Gets the user profile.
        /// </summary>
        /// <param name="logonName">Name of the logon.</param>
        /// <param name="password">The password.</param>
        /// <param name="partnerId">The partner id.</param>
        /// <returns></returns>
        public UserProfile GetUserProfile(string logonName, string password, int partnerId)
        {
            return _userProfileRepository.Users.GetByUserNamePassword(logonName, password, partnerId);
        }

        /// <summary>
        /// Clears the profile.
        /// </summary>
        /// <param name="partnerId">The partner id.</param>
        public void ClearProfile(int partnerId)
        {
            UserSession.UserProfile = null;

            // Clear profile cookie.
            if (HttpContext.Current.Response.Cookies["Profile_" + partnerId] != null)
                HttpContext.Current.Response.Cookies["Profile_" + partnerId].Expires = DateTime.Now;
        }

        /// <summary>
        /// Signs out.
        /// </summary>
        /// <param name="partnerId">The partner id.</param>
        public void SignOut(int partnerId)
        {
            ClearProfile(partnerId);
        }

        /// <summary>
        /// Creates a new record for agent (Pro) or updates the info if already exist.
        /// </summary>
        /// <param name="agent">The agent info to be saved.</param>
        public void SetAgentRecord(AgentBrochureTemplate agent)
        {

            DataProvider.Current.SetAgentRecord(agent.UserGuid, agent.AgencyLogo, agent.ImageUrl, agent.AgencyName, agent.FirstName, agent.LastName,
                agent.Email, agent.Address, agent.City, agent.State, agent.PostalCode, agent.OfficePhone, agent.MobilePhone, agent.DateCreated, agent.DateLastChanged, "1");

        }

        /// <summary>
        /// Creates the profile.
        /// </summary>
        /// <param name="profile">The profile.</param>
        /// <param name="brandPartnerId">The main brand partner Id from the Current configuration</param>
        /// <param name="wrongPasswordMessage"></param>
        /// <param name="wrongEmailMessage"></param>
        /// <param name="userAlreadyExistMessage"></param>
        public void CreateProfile(UserProfile profile, int brandPartnerId, string wrongPasswordMessage, string wrongEmailMessage, string userAlreadyExistMessage)
        {
            var legacyProfile = GetHydratedLegacyProfile(profile);
            legacyProfile.UserID = "{" + Guid.NewGuid().ToString() + "}";
            profile.UserGuid = legacyProfile.UserID;

            DataProvider.Current.SetProfile(legacyProfile.UserID, legacyProfile.LogonName, legacyProfile.FirstName, legacyProfile.LastName, legacyProfile.Password,
                legacyProfile.PartnerID, legacyProfile.DateRegistered, legacyProfile.MiddleName, legacyProfile.Address1, legacyProfile.Address2, legacyProfile.State, legacyProfile.City, legacyProfile.DayPhone,
                legacyProfile.EvePhone, legacyProfile.Fax, legacyProfile.PostalCode, legacyProfile.MailList, legacyProfile.ReferrerName, legacyProfile.MarketOptIn, legacyProfile.RegMetro, legacyProfile.RegPrefer,
                legacyProfile.DayPhoneExt, legacyProfile.EvePhoneExt, legacyProfile.LeadOptIn, legacyProfile.RegCity, legacyProfile.RegPrefer, legacyProfile.RegState, legacyProfile.Age, legacyProfile.RegBOYLInterest,
                legacyProfile.RegComingSoonInterest, legacyProfile.RegActiveAdultInterest, legacyProfile.MoveInDate, legacyProfile.FinancePreference, legacyProfile.Reason,
                legacyProfile.BoxRequestedDate, legacyProfile.WeeklyNotifierOptIn, legacyProfile.InitialMatchDate, legacyProfile.AgencyName, legacyProfile.RealEstateLicense, legacyProfile.SsoId, legacyProfile.HomeWeeklyOptIn);

            if (brandPartnerId == PartnersConst.Pro.ToType<int>())
            {
                var agentInfo = ParseProfileToAgentInfo(legacyProfile);

                SetAgentRecord(agentInfo);
            }

            try
            {
                this.SignIn(profile.LogonName, profile.Password, profile.PartnerId, wrongPasswordMessage, wrongEmailMessage);
            }
            catch (InvalidPasswordException)
            {
                throw new InvalidPasswordException(userAlreadyExistMessage);
            }

        }

        ///// <summary>
        ///// Updates the profile.
        ///// </summary>
        ///// <param name="profile">The profile.</param>
        public void UpdateProfile(UserProfile profile)
        {
            var legacyProfile = GetHydratedLegacyProfile(profile);
            DataProvider.Current.SetProfile(legacyProfile.UserID, legacyProfile.LogonName, legacyProfile.FirstName, legacyProfile.LastName, legacyProfile.Password,
                legacyProfile.PartnerID, legacyProfile.DateRegistered, legacyProfile.MiddleName, legacyProfile.Address1, legacyProfile.Address2, legacyProfile.State, legacyProfile.City, legacyProfile.DayPhone,
                legacyProfile.EvePhone, legacyProfile.Fax, legacyProfile.PostalCode, legacyProfile.MailList, legacyProfile.ReferrerName, legacyProfile.MarketOptIn, legacyProfile.RegMetro, legacyProfile.RegPrefer,
                legacyProfile.DayPhoneExt, legacyProfile.EvePhoneExt, legacyProfile.LeadOptIn, legacyProfile.RegCity, legacyProfile.RegPrefer, legacyProfile.RegState, legacyProfile.Age, legacyProfile.RegBOYLInterest,
                legacyProfile.RegComingSoonInterest, legacyProfile.RegActiveAdultInterest, legacyProfile.MoveInDate, legacyProfile.FinancePreference, legacyProfile.Reason,
                legacyProfile.BoxRequestedDate, legacyProfile.WeeklyNotifierOptIn, legacyProfile.InitialMatchDate, legacyProfile.AgencyName, legacyProfile.RealEstateLicense, legacyProfile.SsoId, legacyProfile.HomeWeeklyOptIn);
        }

        public void AddCommunityToUserPlanner(UserProfile profile, int communityId, int builderId, ListingType listingType)
        {
            var plannerListing = GetCommunityListing(profile.UserGuid, communityId, builderId, false);
            var pl = GetHydratedLegacyPlannerListing(plannerListing);
            if (!UserSession.UserProfile.Planner.SavedCommunities.Contains(pl))
            {
                //if listing was not saved then add same to planner
                UserSession.UserProfile.Planner.AddSavedCommunity(communityId, builderId);
            }
        }


        private AgentBrochureTemplate ParseProfileToAgentInfo(Profile profile)
        {
            var agentInfo = new AgentBrochureTemplate
            {
                UserGuid = profile.UserID,
                AgencyLogo = string.Empty,
                ImageUrl = string.Empty,
                AgencyName = profile.AgencyName,
                FirstName = profile.FirstName,
                LastName = profile.LastName,
                Email = profile.Email,
                Address = string.Empty,
                City = profile.RegCity,
                State = profile.RegState,
                PostalCode = profile.PostalCode,
                OfficePhone = profile.DayPhone,
                MobilePhone = profile.EvePhone,
                DateCreated = profile.DateRegistered,
                DateLastChanged = profile.DateChanged
            };

            return agentInfo;
        }


        private PlannerListing GetHydratedLegacyPlannerListing(UserPlannerListing plannerListing)
        {
            PlannerListing legacyListing = new PlannerListing
            {
                ListingId = plannerListing.ListingId,
                BuilderId = plannerListing.BuilderId.ToType<int>(),
                LeadRequestDate = plannerListing.LeadRequestDate.ToType<DateTime>(),
                ListingType = EnumConversionUtil.StringToListingType(plannerListing.ListingType),
                PlannerId = plannerListing.PlannerId,
                RecommendedFlag = plannerListing.RecommendedFlag,
                SaveDate = plannerListing.SaveDate.ToType<DateTime>(),
                SuppressFlag = plannerListing.SuppressFlag,
                UserId = plannerListing.UserGuid
            };
            return legacyListing;
        }

        private UserPlannerListing GetCommunityListing(string userGuid, int communityId, int builderId, bool isRecommended)
        {
            UserPlannerListing listing = new UserPlannerListing
            {
                UserGuid = userGuid,
                ListingId = communityId,
                BuilderId = builderId,
                ListingType = EnumConversionUtil.ListingTypeToString(ListingType.Community),
                RecommendedFlag = isRecommended
            };
            return listing;
        }

        /// <summary>
        /// Creates the registration lead.
        /// </summary>
        /// <param name="profile">The profile.</param>
        public bool CreateRegistrationLead(UserProfile profile)
        {
            var partner = _partnerService.GetPartner(profile.PartnerId);
            return SendToQueue(partner.FromRegEmail, profile.UserGuid, profile.LogonName, profile.PartnerId, Configuration.MSMQAccountCreation);
        }

        /// <summary>
        /// Creates the recover password lead.
        /// </summary>
        /// <param name="profile">The profile.</param>
        public bool CreateRecoverPasswordLead(UserProfile profile)
        {
            var partner = _partnerService.GetPartner(profile.PartnerId);
            return SendToQueue(partner.FromRegEmail, profile.UserGuid, profile.LogonName, profile.PartnerId,
                        Configuration.MSMQRecoverPassword);
        }

        /// <summary>
        /// Creates the send to friend lead.
        /// </summary>
        /// <param name="profile">The profile.</param>
        /// <param name="fromEmail">From email.</param>
        /// <param name="friendName">Name of the friend.</param>
        /// <param name="friendEmail">The friend email.</param>
        /// <param name="specId">The spec id.</param>
        /// <param name="planId">The plan id.</param>
        /// <param name="communityId">The community id.</param>
        /// <param name="builderId">The builder id.</param>
        /// <param name="notes">The notes.</param>
        public bool CreateSendToFriendLead(UserProfile profile, string fromEmail, string friendName, string friendEmail, int specId, int planId, int communityId, int builderId, string notes)
        {
            var partner = _partnerService.GetPartner(profile.PartnerId);
            Lead lead = new Lead(LeadType.Home);

            if (communityId != 0)
                lead = new Lead(LeadType.Community);

            //Create lead user object and assign values
            var leadUserInfo = new LeadUserInfo
            {
                Guid = profile.UserGuid,
                Email = profile.LogonName,
                FirstName = profile.FirstName,
                LastName = profile.LastName
            };
            lead.AddUserInfo(leadUserInfo);

            string refer = UserSession.Refer ?? string.Empty;

            lead.AddSiteInfo(fromEmail, partner.BrandPartnerId, partner.PartnerId, partner.PartnerSiteUrl, refer, "", "");
            lead.AddUserPrefs("", 0, "", "", "", "", "", notes);
            lead.AddInfo("FriendName", friendName);
            lead.AddInfo("FriendEmail", friendEmail);

            if (communityId != 0 & builderId != 0)
                lead.AddBuilderCommunity(builderId, communityId);
            else if (specId != 0)
                lead.AddSpec(specId);
            else if (planId != 0)
                lead.AddPlan(planId);
            else
                throw new Exception("Error Sending to Friend: Missing either cid/bid or plan or spec id");

            try
            {
                lead.SendtoQ(Configuration.MSMQSendToFriend);
                return true;
            }
            catch (Exception ex)
            {
                string leadXml;
                try
                {
                    leadXml = lead.GetXml();
                }
                catch
                {
                    leadXml = "Could not retrieve lead xml";
                }

                Exception exception = new Exception("Error sending lead to queue.", ex);
                exception.Data.Add("LeadType", lead.Type + "(SendToFriend)");
                exception.Data.Add("LeadXml", leadXml);
                ExceptionPolicy.HandleException(exception, ExceptionPolicies.LeadPolicy);
                return false;
            }
        }

        public void WriteProfileCookie(UserProfile profile)
        {
            if (HttpContext.Current != null)
            {
                HttpCookie profileCookie = new HttpCookie("Profile_" + profile.PartnerId);
                profileCookie.Value = profile.UserGuid;
                profileCookie.Expires = DateTime.Now.AddYears(10);
                HttpContext.Current.Response.Cookies.Add(profileCookie);
            }
        }

        public UnsubscribeResult Unsubscribe(string emailAddress, int partnerId, string marketOptOut, string deactivate)
        {
            UnsubscribeResult outcome;

            var isValidEmail = DataProvider.Current.LogonNameExists(emailAddress, partnerId);
            if (isValidEmail)
            {
                bool isUnsubscribeSuccessful = DataProvider.Current.UnSubscribe(partnerId, emailAddress, marketOptOut, deactivate);
                if (isUnsubscribeSuccessful)
                {
                    UserSession.UserProfile.Planner.ExpireAllSavedAlerts();
                    outcome = UnsubscribeResult.Successful;
                }
                else
                {
                    outcome = UnsubscribeResult.UnknownFailure;
                }
            }
            else
            {
                outcome = UnsubscribeResult.InvalidEmail;
            }

            return outcome;
        }
        
        public void UpdateLastMatchDate(string userId)
        {
            DataProvider.Current.UserUpdateLastMatchDate(userId, DateTime.Now);
        }

        private bool SendToQueue(string fromEmail, string userGuid, string logonName, int partnerId, string queue)
        {
            var partner = _partnerService.GetPartner(partnerId);
            string leadType = queue.Equals(Configuration.MSMQAccountCreation) ? "AccountCreation" : "PasswordRecovery";

            Lead lead = new Lead(LeadType.Market);

            //Create lead user object and assign values
            LeadUserInfo leadUserInfo = new LeadUserInfo { Guid = userGuid, Email = logonName };

            lead.AddUserInfo(leadUserInfo);
            lead.AddSiteInfo(fromEmail, partner.BrandPartnerId, partnerId, partner.PartnerSiteUrl, UserSession.Refer, "", "");

            try
            {
                lead.SendtoQ(queue);
                return true;
            }
            catch (Exception ex)
            {
                string leadXml;
                try
                {
                    leadXml = lead.GetXml();
                }
                catch
                {
                    leadXml = "Could not retrieve lead xml";
                }

                Exception exception = new Exception("Error sending lead to queue;leadtype=" + LeadType.Market + "(" + leadType + ");leadxml=" + leadXml, ex);
                exception.Data.Add("LeadType", LeadType.Market + "(" + leadType + ")");
                exception.Data.Add("LeadXml", leadXml);
                exception.Data.Add("Queue", queue);
                ExceptionPolicy.HandleException(exception, ExceptionPolicies.LeadPolicy);
                return false;
            }
        }

        private void SetActiveSession(UserProfile profile)
        {
            UserSession.UserProfile = this.GetHydratedLegacyProfile(profile);
            UserSession.UserProfile.ActorStatus = WebActors.ActiveUser;
            UserSession.UserProfile.GenericGUID = GetUserIdByEmail(UserSession.UserProfile.Email);

        }

        private Profile GetHydratedLegacyProfile(UserProfile profile)
        {
            Profile legacyProfile = new Profile
            {
                Address1 = profile.Address1,
                Address2 = profile.Address2,
                Age = profile.Age,
                BoxRequestedDate = profile.BoxRequestedDate.ToType<DateTime>(),
                DateChanged = profile.DateLastChanged.ToType<DateTime>(),
                DateRegistered = profile.DateRegistered.ToType<DateTime>(),
                DayPhone = profile.DayPhone,
                DayPhoneExt = profile.DayPhoneExt,
                Email = profile.LogonName,
                EvePhone = profile.EveningPhone,
                EvePhoneExt = profile.EveningPhoneExt,
                Fax = profile.Fax,
                FinancePreference = profile.FinancePreference.ToType<int>(),
                FirstName = profile.FirstName,
                InitialMatchDate = profile.InitialMatchDate.ToType<DateTime>(),
                LastName = profile.LastName,
                LeadOptIn = profile.LeadOptin,
                LogonName = profile.LogonName,
                MailList = profile.MailingList,
                MarketOptIn = profile.MarketOptin,
                Metro = profile.RegistrationMarket,
                MiddleName = profile.MiddleName,
                MoveInDate = profile.MoveInDate.ToType<int>(),
                PartnerID = profile.PartnerId,
                Password = profile.Password,
                PostalCode = profile.PostalCode,
                Reason = profile.MovingReason.ToType<int>(),
                ReferrerName = profile.ReferrerName,
                RegActiveAdultInterest = profile.ActiveAdultInterest.ToType<bool>(),
                RegBOYLInterest = profile.BoylInterest.ToType<bool>(),
                RegCity = profile.RegistrationCity,
                RegComingSoonInterest = profile.ComingSoonInterest.ToType<bool>(),
                RegMetro = profile.RegistrationMarket,
                RegPrefer = profile.RegistrationPreference,
                RegPrice = profile.RegistrationPrice,
                RegState = profile.RegistrationState,
                State = profile.State,
                City = profile.City,
                UserID = profile.UserGuid,
                AgencyName = profile.AgencyName,
                RealEstateLicense = profile.RealStateLicense,
                WeeklyNotifierOptIn = profile.WeeklyOptin,
                SsoId = profile.SsoUserId,
                HomeWeeklyOptIn = profile.HomeWeeklyOptIn
            };
            return legacyProfile;
        }

        public UserProfile GetProfileFromLegacyProfile(Profile legacyProfile)
        {
            var profile = new UserProfile
            {
                Address1 = legacyProfile.Address1,
                Address2 = legacyProfile.Address2,
                Age = legacyProfile.Age,
                BoxRequestedDate = legacyProfile.BoxRequestedDate,
                DateLastChanged = legacyProfile.DateChanged,
                DateRegistered = legacyProfile.DateRegistered,
                DayPhone = legacyProfile.DayPhone,
                DayPhoneExt = legacyProfile.DayPhoneExt,
                LogonName = legacyProfile.Email,
                EveningPhone = legacyProfile.EvePhone,
                EveningPhoneExt = legacyProfile.EvePhoneExt,
                Fax = legacyProfile.Fax,
                FinancePreference = legacyProfile.FinancePreference.ToType<byte>(),
                FirstName = legacyProfile.FirstName,
                InitialMatchDate = legacyProfile.InitialMatchDate,
                LastName = legacyProfile.LastName,
                LeadOptin = legacyProfile.LeadOptIn
            };
            profile.LogonName = legacyProfile.LogonName;
            profile.MailingList = legacyProfile.MailList;
            profile.MarketOptin = legacyProfile.MarketOptIn;
            profile.RegistrationMarket = legacyProfile.Metro;
            profile.MiddleName = legacyProfile.MiddleName;
            profile.MoveInDate = legacyProfile.MoveInDate.ToType<short>();
            profile.PartnerId = legacyProfile.PartnerID;
            profile.Password = legacyProfile.Password;
            profile.PostalCode = legacyProfile.PostalCode;
            profile.MovingReason = legacyProfile.Reason.ToType<byte>();
            profile.ReferrerName = legacyProfile.ReferrerName;
            profile.ActiveAdultInterest = legacyProfile.RegActiveAdultInterest;
            profile.BoylInterest = legacyProfile.RegBOYLInterest;
            profile.RegistrationCity = legacyProfile.RegCity;
            profile.ComingSoonInterest = legacyProfile.RegComingSoonInterest;
            profile.RegistrationMarket = legacyProfile.RegMetro;
            profile.RegistrationPreference = legacyProfile.RegPrefer;
            profile.RegistrationPrice = legacyProfile.RegPrice;
            profile.RegistrationState = legacyProfile.RegState;
            profile.State = legacyProfile.State;
            profile.City = legacyProfile.City;
            profile.UserGuid = legacyProfile.UserID;
            profile.AgencyName = legacyProfile.AgencyName;
            profile.RealStateLicense = legacyProfile.RealEstateLicense;
            profile.WeeklyOptin = legacyProfile.WeeklyNotifierOptIn;
            return profile;
        }

        public BrochureStatus GetBrochureStatus(string email, int communityId, int builderId, int planId, int specId, int partnerId)
        {
            DataTable dt = DataProvider.Current.GetBrochure(email, communityId, builderId, planId, specId, partnerId, string.Empty, string.Empty);

            if (dt.Rows.Count == 0)
                return BrochureStatus.NotGenerated;

            if (dt.Rows.Count == 1)
            {
                DateTime brochureExpiration = DBValue.GetDateTime(dt.Rows[0]["brochure_date_expire"]);
                if (brochureExpiration == DateTime.MinValue)
                    return BrochureStatus.Processing;
                if (brochureExpiration < DateTime.Now)
                    return BrochureStatus.Expired;

                return BrochureStatus.Active;
            }

            return BrochureStatus.Active;
        }


        public string GetMoveInDate(int moveInDate)
        {
            var moveInDateItems = DataProvider.Current.GetMoveInDateItems();
            string date = moveInDateItems.Rows[0]["Text"].ToString(); // default value

            if (moveInDate != -1)
            {
                for (int i = 0; i < moveInDateItems.Rows.Count; i++)
                {
                    int currentValue = Convert.ToInt32(moveInDateItems.Rows[i]["Value"]);
                    if (moveInDate == currentValue)
                    {
                        date = moveInDateItems.Rows[i]["Text"].ToString();
                    }
                }
            }

            return date;
        }

        public DataTable GetMoveInDateItems()
        {
            return DataProvider.Current.GetMoveInDateItems();
        }

        public DataTable GetFinancePrefItems()
        {
            return DataProvider.Current.GetFinancePrefItems();
        }

        public DataTable GetMoveReasonItems()
        {
            return DataProvider.Current.GetMoveReasonItems();
        }

        public DataTable GetStates()
        {
            var table = DataProvider.Current.GetStates();

            table.Columns["state"].ColumnName = "Text";
            table.Columns["statename"].ColumnName = "Value";

            return table;
        }

        public string GetMarketName(int marketId, int partnerId)
        {
            var name = _marketService.GetMarketName(partnerId, marketId);
            return name;
        }

        public bool EmailExists(string email, int partnerId)
        {
            return DataProvider.Current.LogonNameExists(email, partnerId);
        }

        public void SendToFriend(int brandPartnerId, int partnerId, string userId, string logonName, string firstName, string lastName, string fromEmail, string friendName, string friendEmail, int specId, int planId, int communityId, int builderId, string notes)
        {
            string leadXml = string.Empty;

            Lead lead = new Lead(LeadType.Home);

            if (communityId != 0)
            {
                lead = new Lead(LeadType.Community);
            }

            //Create lead user object and assign values
            var leadUserInfo = new LeadUserInfo
            {
                Guid = userId,
                Email = logonName,
                FirstName = firstName,
                LastName = lastName
            };
            lead.AddUserInfo(leadUserInfo);

            var refer = UserSession.Refer ?? "";
            lead.AddSiteInfo(fromEmail, brandPartnerId, partnerId, Configuration.PartnerSiteUrl, refer, "", "");
            lead.AddUserPrefs("", 0, "", "", "", "", "", notes);
            lead.AddInfo("FriendName", friendName);
            lead.AddInfo("FriendEmail", friendEmail);

            if (communityId != 0 & builderId != 0)
            {
                lead.AddBuilderCommunity(builderId, communityId);
            }
            else
            {
                if (specId != 0)
                {
                    lead.AddSpec(specId);
                }
                else
                {
                    if (planId != 0)
                    {
                        lead.AddPlan(planId);
                    }
                    else
                    {
                        throw new Exception("Error Sending to Friend: Missing either cid/bid or plan or spec id");
                    }
                }
            }

            try
            {
                lead.SendtoQ(Configuration.MSMQSendToFriend);
            }
            catch (Exception ex)
            {
                try
                {
                    leadXml = lead.GetXml();
                }
                catch
                {
                    leadXml = "Could not retrieve lead xml";
                }

                Exception exception = new Exception("Error sending lead to queue.", ex);
                exception.Data.Add("LeadType", lead.Type + "(SendToFriend)");
                exception.Data.Add("LeadXml", leadXml);

                ExceptionPolicy.HandleException(exception, ExceptionPolicies.LeadPolicy);
            }
        }

        public void SaveBrochureTemplate(IProfile profile)
        {
            var agentInfo = ParseProfileToAgentInfo(profile.UserID, string.Empty, string.Empty, profile.AgencyName, profile.FirstName, profile.LastName,
                    profile.LogonName, profile.Address1, profile.City, profile.State, profile.PostalCode, profile.DayPhone, profile.EvePhone, profile.DateRegistered, DateTime.Now);

            SetAgentRecord(agentInfo);
        }

        private AgentBrochureTemplate ParseProfileToAgentInfo(string userId, string agencyLogo, string agentPhoto, string agencyName,
            string firstName, string lastName, string email, string address, string city, string state, string postalCode,
            string businessPhone, string mobilePhone, DateTime createdDate, DateTime lastChangeDate)
        {
            var agentInfo = new AgentBrochureTemplate();

            agentInfo.UserGuid = userId;
            agentInfo.AgencyLogo = agencyLogo;
            agentInfo.ImageUrl = agentPhoto;
            agentInfo.AgencyName = agencyName;
            agentInfo.FirstName = firstName;
            agentInfo.LastName = lastName;
            agentInfo.Email = email;
            agentInfo.Address = address;
            agentInfo.City = city;
            agentInfo.State = state;
            agentInfo.PostalCode = postalCode;
            agentInfo.OfficePhone = businessPhone;
            agentInfo.MobilePhone = mobilePhone;
            agentInfo.DateCreated = createdDate;
            agentInfo.DateLastChanged = lastChangeDate;

            return agentInfo;

        }
    }
}
