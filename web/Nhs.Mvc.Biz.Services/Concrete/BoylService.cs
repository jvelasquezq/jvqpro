﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Data.Repositories;
using Nhs.Utility.Constants;
using Nhs.Utility.Common;
using Nhs.Utility.Data;
using Market = Nhs.Mvc.Domain.Model.Web.Market;
using State = Nhs.Mvc.Domain.Model.Web.State;


namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class BoylService : IBoylService
    {
        private readonly IMarketService _marketService;
        private readonly IStateService _stateService;
        private readonly IWebCacheService _webCacheService;

        public BoylService(IMarketService marketService, IStateService stateService, IWebCacheService webCacheService)
        {
            _marketService = marketService;
            _stateService = stateService;
            _webCacheService = webCacheService;
        }

        public IList<State> GetBoylStates(int partnerId)
        {
            string key = WebCacheConst.WebCacheKeyPrefix + WebCacheConst.PartnerMarkets + partnerId;
            IEnumerable<Market> markets = (List<Market>)Caching.GetObjectFromCache(key);

            if (markets == null)
            {
                markets = _marketService.GetMarkets(partnerId);
                Caching.AddObjectToCache(markets, key);
            }

            var boylStates = new List<State>();

            var boylMarkets = markets.Where(m => m.TotalCustomBuilders > 0);
            var states = _stateService.GetAllStates();

            foreach (State st in boylMarkets.Select(market => states.SingleOrDefault(m => m.StateAbbr == market.StateAbbr)).Where(st => !boylStates.Contains(st)))
            {
                boylStates.Add(st);
            }
            
            
            boylStates.Sort(new PropertyComparer<State>("StateName"));
            return boylStates;
        }

        public List<Market> GetBoylMarketsByState(int partnerId, string stateAbbr)
        {
            var partnerMarkets = _marketService.GetMarkets(partnerId);

            var markets = partnerMarkets.ToList();

            return markets.Where(m => m.TotalCustomBuilders > 0 && m.StateAbbr == stateAbbr).ToList();
        }
        //Case 81052: Remove BoylLinks
        //public IList<BoylLink> GetBoylLinks(int partnerId)
        //{
        //    var dt = SqlHelper.SelectDistinct(string.Empty, DataProvider.Current.GetBoylLinks(partnerId), "brand_name");
        //    const string prefix = "http://";
        //    return (from DataRow dr in dt.Rows select new BoylLink
        //        {
        //            BrandName = DBValue.GetString(dr["brand_name"]),
        //            BuilderId = DBValue.GetInt(dr["builder_id"]),
        //            CommunityId = DBValue.GetInt(dr["community_id"]),
        //            BuilderUrl = !string.IsNullOrEmpty(DBValue.GetString(dr["builder_url"])) && !DBValue.GetString(dr["builder_url"]).Contains(prefix)
        //                       ? string.Concat(prefix, DBValue.GetString(dr["builder_url"]))
        //                       : DBValue.GetString(dr["builder_url"])
        //        }).ToList();
        //}
         
        public List<BoylResult> GetBoylResultsByPartner(int partnerId)
        {
            var cacheKey = WebCacheConst.WebCacheKeyPrefix + WebCacheConst.BoylResult + "ByPartner_" + partnerId;
            var boyls = _webCacheService.Get(cacheKey, false, new TimeSpan(WebCacheConst.DefaultHours, 0, 0),
                () =>
                {
                    var dtResults = DataProvider.Current.GetBoylResults(partnerId);
                    var result = dtResults.AsEnumerable().Select(BindBoylResult);
                    return result.ToList();
                });

            return boyls;
        }
        
        public IEnumerable<BoylResult> GetBoylResults(int partnerId, int marketId, string marketName, string marketState)
        {
            var results = GetBoylResultsByPartner(partnerId);
            var boylResults = results.Where(p => p.MarketId == marketId).ToList();
            return boylResults;
        }

        public IEnumerable<BoylResult> GetBoylResultsCnh(string marketName, string marketState)
        {
            var key = WebCacheConst.WebCacheKeyPrefix + WebCacheConst.BoylResult + "_" + marketName + "_" + marketState;
            var boylResults = (List<BoylResult>)Caching.GetObjectFromCache(key);

            if (boylResults != null) 
                return boylResults;
            
            boylResults = new List<BoylResult>();
            var resultsCnh = DataProvider.Current.GetBoylResultsCnh(marketName, marketState);

            boylResults.AddRange(from DataRow row in resultsCnh.Rows select new BoylResult
            {
                BuilderId = DBValue.GetInt(row["BuilderID"]),
                BrandName = DBValue.GetString(row["BuilderName"]),
                NhsBuilderId = DBValue.GetInt(row["NHSBuilderID"]),
                CommunityImageThumbnail = DBValue.GetString(row["BuilderImage"]),
                BrandImageThumbnail = DBValue.GetString(row["BuilderLogo"]),
                IsCnh = true,
                MarketName = DBValue.GetString(row["MarketName"]),
                State = DBValue.GetString(row["StateCode"]),
                StateName = _stateService.GetStateName(DBValue.GetString(row["StateCode"]))
            });

            Caching.AddObjectToCache(boylResults, key);
            return boylResults;
        }

        public IEnumerable<ExtendedHomeResult> GetSpotHomes(int partnerId, int marketId)
        {
            IList<ExtendedHomeResult> spotHomes = new List<ExtendedHomeResult>();
            DataTable homes = DataProvider.Current.GetSpotLightHomes(Configuration.PartnerId, 0, marketId, 0, "B", 5, false);

            foreach (DataRow dr in homes.Rows)
            {
                var home = new ExtendedHomeResult();
                BindSpotHome(dr, home);
                spotHomes.Add(home);
            }
            return spotHomes;
        }

        private BoylResult BindBoylResult(DataRow row)
        {
            var result = new BoylResult();
            result.CommunityId = DBValue.GetInt(row["community_id"]);
            result.BuilderId = DBValue.GetInt(row["builder_id"]);
            result.MarketId = DBValue.GetInt(row["market_id"]);
            result.BuilderUrl = DBValue.GetString(row["builder_url"]);
            result.CommunityName = DBValue.GetString(row["community_name"]);
            result.SubVideoFlag = DBValue.GetString(row["subvideo_flag"]);
            result.City = DBValue.GetString(row["city"]);
            result.State = DBValue.GetString(row["state"]);
            result.StateName = DBValue.GetString(row["statename"]);
            result.PostalCode = DBValue.GetString(row["postal_code"]);
            result.PriceLow = (int)DBValue.GetDecimal(row["price_low"]);
            result.PriceHigh = (int)DBValue.GetDecimal(row["price_high"]);
            result.MatchingHomes = DBValue.GetInt(row["no_home_total"]);
            result.CommunityImageThumbnail = DBValue.GetString(DBValue.GetString(row["spotlight_thumbnail"]));
            result.BrandImageThumbnail = DBValue.GetString(DBValue.GetString(row["logo_url_small"]));
            result.CommunityType = DBValue.GetString(row["listing_type_flag"]);
            result.BuildOnYourLot = (DBValue.GetString(row["custom_builder_flag"]) == "Y");
            result.HasHotHome = DBValue.GetBool(row["has_hot_home"]);
            result.HasSpecialOffer = DBValue.GetBool(row["has_special_offer"]);
            result.SquareFeetLow = DBValue.GetInt(row["square_feet_low"]);
            result.SquareFeetHigh = DBValue.GetInt(row["square_feet_high"]);
            result.Latitude = DBValue.GetDouble(row["latitude"]);
            result.Longitude = DBValue.GetDouble(row["longitude"]);
            result.BrandName = DBValue.GetString(row["brand_name"]);
            result.MarketName = DBValue.GetString(row["market_name"]);
            result.BrandImageThumbnailMedium = DBValue.GetString(row["BrandImageThumbnailMedium"]);
            result.IsBasicCommunity = DBValue.GetBool(row["is_basic_community"]);
            return result;
        }

        private void BindSpotHome(DataRow row, ExtendedHomeResult home)
        {
            home.PlanName = DBValue.GetString(row[SpotLightHomesDBFields.PlanName]);
            home.ListingId = DBValue.GetInt(row["listing_id"]);
            home.PlanId = DBValue.GetInt(row["plan_id"]);
            home.SpecId = DBValue.GetInt(row["specification_id"]);
            string imageUrl = DBValue.GetString(row[SpotLightHomesDBFields.ImageThumbnail]);
            home.PlanImageThumbnail = (imageUrl.ToLower() == "n") ? string.Format("{0}/globalresources14/default/images/no_photo/no_photos_75x58.png", Configuration.ResourceDomain) : Configuration.IRSDomain + imageUrl;
            home.NumBathrooms = DBValue.GetInt(row[SpotLightHomesDBFields.Bathrooms]);
            home.NumHalfBathrooms = DBValue.GetInt(row[SpotLightHomesDBFields.HalfBathrooms]);
            home.NumBedrooms = DBValue.GetInt(row[SpotLightHomesDBFields.Bedrooms]);
            home.IsHotHome = DBValue.GetBool(row[SpotLightHomesDBFields.HotHome]);
            home.FormatedPrice = DBValue.GetFormattedString(row[SpotLightHomesDBFields.Price], "{0:c0}");
            home.State = DBValue.GetString(row[SpotLightHomesDBFields.State]);
            home.City = DBValue.GetString(row[SpotLightHomesDBFields.City]);
            home.Address1 = DBValue.GetString(row[SpotLightHomesDBFields.StreetAddress]);
            home.MarketName = DBValue.GetString(row[SpotLightHomesDBFields.MarketName]);

            
        }
    }
}
