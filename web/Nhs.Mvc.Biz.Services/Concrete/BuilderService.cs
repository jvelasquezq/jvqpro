﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Nhs.Library.Business.BuilderCoop;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Filters;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Entities;
using Nhs.Utility.Constants;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class BuilderService : IBuilderService
    {
        private readonly IWebCacheService _webCacheService;
        private readonly IPartnerMarketPremiumUpgradeService _partnerMarketPremiumUpgrade;
        private readonly IBuilderRepository _builderRepository;
        private readonly ISeoContentService _seoContentService;

        public BuilderService(IBuilderRepository buildeRepository, IWebCacheService webCacheService, 
            IPartnerMarketPremiumUpgradeService partnerMarketPremiumUpgrade, ISeoContentService seoContentService)
        {
            _builderRepository = buildeRepository;
            _webCacheService = webCacheService;
            _partnerMarketPremiumUpgrade = partnerMarketPremiumUpgrade;
            _seoContentService = seoContentService;
        }

        public IList<Builder> GetMarketBuilders(int partnerId, int marketId)
        {
            var dtBuilderComms = this.GetMarketBcs(partnerId, marketId);

            var builders = (from DataRow dr in dtBuilderComms.Rows
                 select new Builder
                 {
                     BuilderId = DBValue.GetInt(dr["builder_id"]),
                     BuilderName = DBValue.GetString(dr["builder_name"]),
                     BrandId = DBValue.GetInt(dr["brand_id"]),
                     IsBilled = DBValue.GetString(dr["billed"]) == "Y"
                 }).ToList();

            return builders.Distinct(new BuilderComparer()).OrderBy(b => b.BuilderId).ToList();
        }

        public DataTable GetMarketBcs(int partnerId, int marketId)
        {
            var cacheKey = WebCacheConst.WebCacheKeyPrefix + WebCacheConst.MarketBCs + partnerId + "_" + marketId;
            var dtBuilderComms = _webCacheService.Get(cacheKey, false,
                                                      new TimeSpan(0, WebCacheConst.DefaultHours, 0, 0),
                                                      () => GetMarketTable(partnerId, marketId));

            return dtBuilderComms;
        }

        private DataTable GetMarketTable(int partnerId, int marketId)
        {
            var datable= DataProvider.Current.GetMarketBCs(marketId, partnerId, null);
            if (_partnerMarketPremiumUpgrade.GetPartnerMarketPremiumUpgrade(partnerId, marketId))
            {
                foreach (DataRow rows in datable.Rows)
                    rows["billed"] = "Y";
            }
            return datable;
        }

        public IList<Builder> GetBuilders(List<int> builderIdList)
        {
            return _builderRepository.Builders.GetByIdList(builderIdList).ToList();
        }

        //TODO:: change this has very bad performance, also is no use in this Solution
        [Obsolete("This method have performance issues please do not use it", true)]
        public Builder GetBuilder(int builderId)
        {
            return _builderRepository.Builders.GetById(builderId);
        }

        public Domain.Model.Web.BuilderService GetServiceByBuilderId(int builderId, int serviceId)
        {
            return _builderRepository.BuilderServices.GetByService(builderId, serviceId);
        }

        #region COOP

        public BuilderCoOpInfo GetBuilderCoOpInfo(int builderId, int brandPartnerId,bool loadPartnershipPactHtml = false)
        {
            if (brandPartnerId != PartnersConst.Pro.ToType<int>())
            {
                return new BuilderCoOpInfo();
            }

            var info = GetAllBuilderCoOpInfo().FirstOrDefault(i => i.BuilderId == builderId);

            if (info == null)
            {
                return new BuilderCoOpInfo();
            }

            info.PartnershipPactHtml = loadPartnershipPactHtml && info.IsPac
                ? GetPartnershipPactHtml(info.BrandName)
                : string.Empty;

            return info;
        }

        private string GetPartnershipPactHtml(string brandName)
        {
            var file = string.Format("BuilderAgentPartnership_{0}.xml", "Default");
            var failBackFiles = new List<string>
            {                
                "Default.xml"
            };            
            var result = _seoContentService.GetSeoContent<BuilderCoOpInfo>(new SeoContentOptions
                (
                    "Coop",
                    file,
                    failBackFiles,
                    replaceTags: new List<ContentTag>
                    {                      
                        new ContentTag(ContentTagKey.BrandName, brandName)
                    }
                ));

            return result.PartnershipPactHtml;
        }

        private IEnumerable<BuilderCoOpInfo> GetAllBuilderCoOpInfo()
        {
            const string cacheKey = "AllCoOpInfo";
            var data = _webCacheService.Get(cacheKey, false, new TimeSpan(WebCacheConst.DefaultHours, 0, 0),
                () =>
                {
                    var dtBrands = DataProvider.Current.GetAllBuilderCoOpInfo();
                    var result = dtBrands.AsEnumerable().Select(row => new BuilderCoOpInfo
                    {
                        BuilderId = row.Field<int>("builderId"),
                        BrandId = row.Field<int>("brandId"),
                        IsCoo = row.Field<bool>("coo"),
                        IsPac = row.Field<bool>("pac"),
                        LogoUrlSmall = row.Field<string>("logoSmall") ?? string.Empty,
                        LogoUrlMed = row.Field<string>("logoMed") ?? string.Empty,
                        BrandName = row.Field<string>("brandName") ?? string.Empty,
                        BuilderName = row.Field<string>("builderName") ?? string.Empty,
                    });
                    return result;
                });

            return data;
        }

        #endregion 

    }
}
