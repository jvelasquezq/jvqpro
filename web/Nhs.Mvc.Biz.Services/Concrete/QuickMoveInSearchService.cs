﻿using System.Collections.Generic;
using System.Linq;
using Nhs.Library.Constants;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Utility.Common;
using Nhs.Utility.Data;
using Market = Nhs.Mvc.Domain.Model.Web.Market;
using State = Nhs.Mvc.Domain.Model.Web.State;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class QuickMoveInSearchService: IQuickMoveInSearchService
    {
        private IWebCacheService _webCacheService;
        private IMarketService _marketService;
        private IStateService _stateService;

        public QuickMoveInSearchService(IMarketService marketService, IStateService stateService, IWebCacheService webCacheService)
        {
            _marketService = marketService;
            _stateService = stateService;
            _webCacheService = webCacheService;
        }

        public IList<State> GetQuickMoveStates(int partnerId)
        {
            //TODO: Remove Caching and use webcacheservice
            string key = WebCacheConst.WebCacheKeyPrefix + WebCacheConst.PartnerMarkets + partnerId.ToString();
            var markets = (List<Market>)Caching.GetObjectFromCache(key);

            if (markets == null)
            {
                markets = _marketService.GetMarkets(partnerId).ToList();
                Caching.AddObjectToCache(markets, key);
            }

            var quickStates = new List<State>();

            var quickMarkets = markets.Where(m => m.TotalQuickMoveInHomes > 0);
            var states = _stateService.GetAllStates();

            foreach (Market market in quickMarkets)
            {
                State st = states.Where(m => m.StateAbbr == market.StateAbbr).SingleOrDefault();
                if (!quickStates.Contains(st)) quickStates.Add(st);
            }

            quickStates.Sort(new PropertyComparer<State>("StateName"));
            return quickStates;
        }

        public List<Market> GetQuickMoveMarketsByState(int partnerId, string stateAbbr)
        {
            var partnerMarkets = _marketService.GetMarkets(partnerId);

            List<Market> markets = partnerMarkets.ToList();

            return markets.Where(m => m.TotalQuickMoveInHomes > 0 && m.StateAbbr == stateAbbr).ToList();
        }
    }
}
