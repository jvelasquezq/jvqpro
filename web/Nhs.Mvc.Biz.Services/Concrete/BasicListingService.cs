﻿using System.Collections.Generic;
using System.Linq;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.BasicListings;
using Nhs.Mvc.Biz.Services.Filters;
using Nhs.Utility.Common;
using Nhs.Utility.Data;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class BasicListingService : IBasicListingService
    {
        private IBasicListingRepository _basicListingRepository;

        public BasicListingService(IBasicListingRepository basicListingRepository)
        {
            _basicListingRepository = basicListingRepository;
        }

        public BasicListing GetBasicListing(int listingId)
        {
            var basicListing = _basicListingRepository.BasicListings.GetByListingId(listingId);
            if (basicListing != null)
                basicListing.MarketId = this.GetMarketIdForGeolocation(basicListing.GeoLocationid);

            return basicListing;
        }

        public BasicListingImage GetBasicListingImage(int listingId)
        {
            return _basicListingRepository.BasicListingImages.GetByListingId(listingId);
        }

        private int GetMarketIdForGeolocation(int? geoLocationId)
        {
            if (geoLocationId == null)
                return 0;

            var marketGeoLocation = _basicListingRepository.MarketGeoLocations.GetByGeoLocationId(geoLocationId.ToType<int>());
            return marketGeoLocation != null ? marketGeoLocation.MarketId : 0;
        }

        public IEnumerable<BasicListingImage> GetImagesForBasicListings(IEnumerable<int> listingIds)
        {
            IList<int?> ids = listingIds.Select(listingId => (int?)listingId).ToList();
            return _basicListingRepository.BasicListingImages.GetDisplayReady().GetListingOnly().GetFirstImage().WhereIn(p => p.ListingId, ids).ToList();
        }
    }
}
