﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Nhs.Library.Constants;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Data.Repositories;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class PartnerMarketPremiumUpgradeService : IPartnerMarketPremiumUpgradeService
    {
        private IWebCacheService _webCacheService;

        public PartnerMarketPremiumUpgradeService(IWebCacheService webCacheService)
        {
            _webCacheService = webCacheService;
        }

        public bool GetPartnerMarketPremiumUpgrade(int partnerId, int marketId)
        {
            const string cacheKey = WebCacheConst.WebCacheKeyPrefix + WebCacheConst.PartnerMarketPremiumUpgrade;
            var dataInfo = _webCacheService.Get(cacheKey, false, new TimeSpan(0, WebCacheConst.DefaultHours, 0, 0),
                                                GetData);

            return dataInfo.Any(row => row["partner_id"].ToType<Int32>() == partnerId && row["market_id"].ToType<Int32>() == marketId);
        }

        private IEnumerable<DataRow> GetData()
        {
            var datable = DataProvider.Current.GetPartnerMarketPremiumUpgrade();
            var rows = datable.Rows.Cast<DataRow>();
            return rows;
        }
    }
}
