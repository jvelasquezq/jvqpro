﻿using System.Data;
using System.Globalization;
using System.Linq;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class PlannerService : IPlannerService
    {
        public DataTable GetSavedCommunities(string communityId, int partnerId)
        {
            return DataProvider.Current.GetSavedCommunities(partnerId.ToType<string>(), communityId);
        }

        public DataTable GetSavedHomes(string planIds, string specIds, int partnerId)
        {
            return DataProvider.Current.GetSavedHomes(partnerId, planIds, specIds);
        }

        public Spec GetSavedHome(int planId, int specId, int partnerId)
        {
            Spec home = null;
            var dataTable = DataProvider.Current.GetSavedHomes(partnerId, planId.ToString(CultureInfo.InvariantCulture), specId.ToString(CultureInfo.InvariantCulture)).AsEnumerable();
            // The sp always returns one home, this is because we are using the id
            var dataRow = dataTable.FirstOrDefault();

            if (dataRow != null)
            {
                home = new Spec
                {
                    CommunityId = dataRow["community_id"].ToType<int>(),
                    Community = new Community
                    {
                        CommunityId = dataRow["community_id"].ToType<int>(),
                        CommunityName = dataRow["community_name"].ToType<string>(),
                        City = dataRow["city"].ToType<string>(),
                        State = new State
                        {
                            StateAbbr = dataRow["state"].ToType<string>()
                        },
                        PostalCode = dataRow["postal_code"].ToType<string>(),
                        BrandId = dataRow["brand_id"].ToType<int>(),
                        Brand = new Brand
                        {
                            BrandId = dataRow["brand_id"].ToType<int>(),
                            BrandName = dataRow["brand_name"].ToType<string>()
                        }
                    },
                    Price = dataRow["price"].ToType<decimal>(),
                    Bedrooms = dataRow["no_bedrooms"].ToType<int>(),
                    Bathrooms = dataRow["no_baths"].ToType<int>(),
                    HalfBaths = dataRow["no_half_baths"].ToType<int>(),
                    Garages = dataRow["no_car_garage"].ToType<decimal>(),
                    PlanId = dataRow["plan_id"].ToType<int>(),
                    SpecId = dataRow["specification_id"].ToType<int>(),
                    PlanName = dataRow["plan_name"].ToType<string>(),
                    SqFt = dataRow["square_feet"].ToType<int>(),
                    HomeMarketingDescription = dataRow["marketing_description"].ToType<string>(),
                    ImageThumbnail = dataRow["image_thumbnail"].ToType<string>(),
                    Status = dataRow["home_status"].ToType<string>(),
                    IsHotHome = dataRow["hot_home"].ToType<int>(),
                    Address1 = dataRow["address"].ToType<string>()
                };
            }

            return home;
        }
    }
}
