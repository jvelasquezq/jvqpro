﻿using System;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Mvc.Biz.Services.Abstract;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class WebCacheService : IWebCacheService
    {
        #region ICacheService Members

        //NOTE: All partner-specific items to be cached have to be suffixed with partnerid by caller
        public T Get<T>(string cacheId, bool refreshCache, Func<T> getItemCallback) where T : class
        {
            return this.Get(cacheId, refreshCache, new TimeSpan(WebCacheConst.DefaultHours, 0, 0), getItemCallback);
        }

        //NOTE: All partner-specific items to be cached have to be suffixed with partnerid by caller
        public T Get<T>(string cacheId, bool refreshCache, TimeSpan timeSpan, Func<T> getItemCallback) where T : class
        {
            bool usePartnerId = false;
            T item = null;
            if (!refreshCache)
            {
                item = WebCacheHelper.GetObjectFromCache(cacheId, usePartnerId) as T;
            }

            if (item == null)
            {
                item = getItemCallback();
                if(Configuration.EnableCache && item!=null)
                    WebCacheHelper.AddObjectToCache(item, cacheId, timeSpan, usePartnerId);
            }
            return item;
        }
        #endregion
    }
}
