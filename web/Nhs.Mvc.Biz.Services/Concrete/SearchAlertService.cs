﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
//using Nhs.Library.Business;
using Bdx.Web.Api.Objects;
using Bdx.Web.Api.Objects.Constants;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Proxy;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Filters;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Utility.Common;
using Nhs.Library.Helpers;
using SearchAlert = Nhs.Mvc.Domain.Model.Profiles.SearchAlert;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class SearchAlertService : ISearchAlertService
    {
        private readonly IApiService _apiService;
        private readonly ISearchAlertRepository _searchAlertRepository;
        private readonly IUserProfileService _userProfileService;

        public SearchAlertService(ISearchAlertRepository searchAlertRepository, IApiService apiService, IUserProfileService userProfileService)
        {
            _apiService = apiService;
            _searchAlertRepository = searchAlertRepository;
            _userProfileService = userProfileService;
        }

        public SearchAlert GetSearchAlert(int alertId)
        {
            return _searchAlertRepository.SearchAlerts.GetById(alertId);
        }


        public IList<SearchAlert> GetSearchAlertsByUserGuid(string guid)
        {
            return (from sa in _searchAlertRepository.SearchAlerts
             where sa.UserGuid == guid
             select sa).ToList();
        }

        public bool CreateAlert(int partnerId, SearchAlert alert, IProfile profile, int sourceCommunityId,
            string comments = "")
        {
            // Save alert to DB
            //var result = _searchAlertRepository.CreateSearchAlert(alert);

            alert.AlertId = DataProvider.Current.AddSearchAlert(alert.UserGuid, alert.Title, alert.State,
                alert.MarketId.ToType<int>(),
                alert.City, alert.PostalCode, alert.Radius, alert.PriceMin.ToType<int>(), alert.PriceMax.ToType<int>(),
                alert.Bedrooms, alert.IsSingleFamily, alert.IsCondo, alert.IsBoyl, alert.HasPool, alert.HasGolfCourse,
                alert.IsGatedCommunity, alert.SchoolDistrictId, alert.BuilderId);

            //Add to planner
            alert.DateCreated = DateTime.Now;
            alert.ExpirationDate = DateTime.Now.AddMonths(9);
            profile.Planner.AddSavedSearchAlert(GetLegacyAlert(alert));

            // Update user LastMatchSentDate only on first alert so that weekly notifier 
            // doesnt pick this account up and resend an email tonight
            if (GetSearchAlertsByUserGuid(profile.UserID).Count == 1)
                _userProfileService.UpdateLastMatchDate(profile.UserID);

            // Gets results & generates leads
            //var searchService = new SearchService();

            var searchParameters = ToSearchParams(partnerId, alert);
            
            var apiResults = GetCommunitiesFromApi(searchParameters);
            var communityResults = apiResults.Result;

            if (sourceCommunityId > 0)
                communityResults = communityResults.Where(p => p.Id != sourceCommunityId).ToList();

            //AlertResultsView searchResults = searchService.AlertsSearch(searchParameters);

            if (communityResults.Any())
            {
                var latlong = new LatLong(alert.Lat, alert.Lng);
                communityResults =
                    communityResults.DistinctBy(p => p.BuilderId)
                        .OrderBy(p => latlong.ArcDistance(p.Lat.ToType<double>(), p.Lng.ToType<double>())).ToList();
                        

                communityResults.ForEach(v2 =>
                        {
                            v2.Dist = latlong.ArcDistance(v2.Lat.ToType<double>(), v2.Lng.ToType<double>());
                        });

                communityResults = communityResults.Take(10).ToList();

                //searchResults.CommunityResults = searchResults.CommunityResults.OrderBy(p => p.DistanceFromCenter).ToList();
                GenerateLeads(profile, communityResults, alert, comments);
                SaveResults(communityResults, alert.AlertId);
            }

            return alert.AlertId > 0;
        }
         
        private WebApiCommonResultModel<List<ApiCommunityResultV2>> GetCommunitiesFromApi(SearchParams searchParams)
        {
            var data = _apiService.GetResultsWebApi<ApiCommunityResultV2>(searchParams, SearchResultsPageType.CommunityResults);
            return data;
        }

        private void SaveResults(IEnumerable<ApiCommunityResultV2> alertResults, int alertId)
        {
            foreach (var row in alertResults)
                DataProvider.Current.SearchAlertSaveSingleResult(alertId, row.Id, row.BuilderId);
        }

        public void SaveResults(IEnumerable<Community> alertResults, int alertId)
        {
            foreach (var row in alertResults)
                DataProvider.Current.SearchAlertSaveSingleResult(alertId, row.CommunityId, row.BuilderId);
        }

        public void RemoveSavedAlert(SearchAlert searchAlert)
        {
            _searchAlertRepository.RemoveSearchAlert(searchAlert);
        }

        public void RemoveAlertResult(int searchAlertId, int communityId, int builderId)
        {
            _searchAlertRepository.RemoveAlertResult(searchAlertId, communityId, builderId);
        }

        public IEnumerable<AlertResult> GetAlertResults(int alertId)
        {
            return _searchAlertRepository.AlertResults.GetByAlertId(alertId);
        }

        private void GenerateLeads(IProfile profile, IEnumerable<ApiCommunityResultV2> results, SearchAlert alert, string comments)
        {
            var leadInfoDetails = new LeadInfo();
            string bcids = string.Empty;

            // Formats results to generate leads. Format = bid1|cid1,bid2|cid2,bid3|cid3
            // Add results to recommended list, this was done so that we have a way to identify that leads were generated for these comm
            // and just show the View Brochure link on the alert results page //24584
            foreach (var result in results)
            {
                bcids += String.Format("{0}|{1},", result.BuilderId, result.Id);
                // If you add this the communities cannot be saved as favorites and they are not displayed there
                //profile.Planner.AddRecommendedCommunity(result.Id, result.BuilderId);
                profile.Planner.UpdateCommunityRequestDate(result.Id, result.BuilderId, null);
            }
            bcids = bcids.TrimEnd(',');

            // Gets user details
            if (UserSession.Refer != null)
                leadInfoDetails.Referer = UserSession.Refer;
            if (profile.UserID != null)
                leadInfoDetails.LeadUserInfo.Guid = profile.UserID.Trim();
            if (profile.Email != null)
                leadInfoDetails.LeadUserInfo.Email = profile.Email.Trim();
            if (profile.LastName != null)
                leadInfoDetails.LeadUserInfo.LastName = profile.LastName.Trim();
            if (profile.FirstName != null)
                leadInfoDetails.LeadUserInfo.FirstName = profile.FirstName.Trim();
            if (profile.Address1 != null)
                leadInfoDetails.LeadUserInfo.Address = profile.Address1.Trim();
            if (profile.City != null)
                leadInfoDetails.LeadUserInfo.City = profile.City.Trim();
            if (profile.State != null)
                leadInfoDetails.LeadUserInfo.State = profile.State.Trim();
            if (profile.PostalCode != null)
                leadInfoDetails.LeadUserInfo.PostalCode = profile.PostalCode.Trim();
            if (profile.DayPhone != null)
                leadInfoDetails.LeadUserInfo.Phone = profile.DayPhone.Trim();
            if (profile.DayPhoneExt != null)
                leadInfoDetails.LeadUserInfo.PhoneExt = profile.DayPhoneExt.Trim();
            if (profile.RegState != null)
                leadInfoDetails.Prefstate = profile.RegState.Trim();
            if (!string.IsNullOrEmpty(profile.RegMetro))
                leadInfoDetails.MarketId = Convert.ToInt32(profile.RegMetro.Trim());

            if (leadInfoDetails.MarketId == 0) // marketId can't be 0 // ticket 36344
                leadInfoDetails.MarketId = DataProvider.Current.GetMarketIdFromPostalCode(profile.PostalCode, Configuration.PartnerId, false);

            if (profile.RegCity != null)
                leadInfoDetails.Prefcity = profile.RegCity.Trim();
            if (profile.RegPrice != null)
                leadInfoDetails.PrefPriceRange = profile.RegPrice.Trim();

            leadInfoDetails.PrefComingsoon = profile.RegComingSoonInterest;
            leadInfoDetails.IsBoyol = profile.RegBOYLInterest;
            leadInfoDetails.IsActiveAdult = profile.RegActiveAdultInterest;
            if (!string.IsNullOrEmpty(profile.RegPrefer))
                leadInfoDetails.BldrImportance = profile.RegPrefer;

            // Setting action and leadType and community List
            leadInfoDetails.LeadAction = LeadAction.SearchAlert;
            leadInfoDetails.FromPage = Pages.RegisterCreateAlert;
            leadInfoDetails.LeadType = LeadType.Market;
            leadInfoDetails.CommunityList = bcids;
            leadInfoDetails.LeadUserInfo.SessionId = UserSession.SessionId;

            // Sets alert reference
            leadInfoDetails.AlertId = alert.AlertId.ToString();

            // Add International to comments, for users withough ZIP
            if (string.IsNullOrEmpty(profile.PostalCode))
                leadInfoDetails.LeadComments = "International lead - " + comments;

            // Add appropriate comment for Chat Agent
            if (profile.UserID == profile.Email)
                leadInfoDetails.LeadComments = "** ONLINE CHAT **" + comments;

            leadInfoDetails.LeadComments += " " +
                                            TemplateWordingHelper.GetNoOriginalLead(alert.MarketName, alert.City, alert.PostalCode, alert.Radius, alert.PriceMin.ToType<int>(),
                                            alert.PriceMax.ToType<int>(), alert.Bedrooms, alert.SchoolDistrictName, alert.BuilderName, alert.IsSingleFamily, alert.IsCondo,
                                            alert.HasPool, alert.HasGolfCourse, alert.IsGatedCommunity);

            // Generates lead
            LeadUtil.GenerateLead(leadInfoDetails);

            UserSession.BCList = null;
        }

        private SearchParams ToSearchParams(int partnerId, SearchAlert alert)
        {

            //TBD - need to change builderid in searchalert to brandid
            //var psp = new PropertySearchParams(alert.MarketId.ToType<int>());
            var psp = new SearchParams();

            if (!string.IsNullOrEmpty(alert.PostalCode))
            {
                psp.WebApiSearchType = WebApiSearchType.Exact;
            }

            psp.MarketId = alert.MarketId.ToType<int>();
            psp.PartnerId = partnerId;
            psp.State = alert.State;
            //psp.CityNameFilter = alert.City;
            psp.City = alert.City;
            psp.PostalCode = alert.PostalCode;
            psp.PageSize = 100;
            psp.ExcludeBasiCommunities = true;
            //psp.PostalCodeFilter = alert.PostalCode;
            //psp.FacetPriceHigh = alert.PriceMax.ToType<int>();
            //psp.FacetPriceLow = alert.PriceMin.ToType<int>();
            psp.Radius = alert.Radius.ToType<int>();
            psp.PriceLow = alert.PriceMin.ToType<int>();
            psp.PriceHigh = alert.PriceMax.ToType<int>();
            psp.Bedrooms = alert.Bedrooms.HasValue ? alert.Bedrooms.Value : 0;
            psp.NoBoyl = !alert.IsBoyl;
            psp.GolfCourse = alert.HasGolfCourse;
            psp.Gated = alert.IsGatedCommunity;
            psp.SchoolDistrictIds = alert.SchoolDistrictId.HasValue ? alert.SchoolDistrictId.ToType<string>() : "";
            psp.BrandId = alert.BuilderId.HasValue ? alert.BuilderId.Value : 0;
            //psp.AlertId = alert.AlertId;
            psp.OriginLat = alert.Lat;
            psp.OriginLng = alert.Lng;
            return psp;
        }

        private void DataBind(DataRow dr, SearchAlert alert)
        {
            alert.AlertId = DBValue.GetInt(dr["alert_id"]);
            alert.Bedrooms = DBValue.GetInt(dr["bedrooms"]);
            alert.BuilderId = DBValue.GetInt(dr["builder_id"]);
            alert.IsBoyl = DBValue.GetBool(dr["boyl"]);
            alert.City = DBValue.GetString(dr["city"]);
            alert.IsCondo = DBValue.GetBool(dr["condo_townhome_homes"]);
            alert.DateCreated = DBValue.GetDateTime(dr["date_created"]);
            alert.ExpirationDate = DBValue.GetDateTime(dr["expiration_date"]);
            alert.IsGatedCommunity = DBValue.GetBool(dr["gated_community"]);
            alert.HasGolfCourse = DBValue.GetBool(dr["golf_course"]);
            alert.MarketId = DBValue.GetInt(dr["market_id"]);
            alert.HasPool = DBValue.GetBool(dr["pool"]);
            alert.PostalCode = DBValue.GetString(dr["postal_code"]);
            alert.PriceMax = DBValue.GetInt(dr["price_max"]);
            alert.PriceMin = DBValue.GetInt(dr["price_min"]);
            alert.Radius = DBValue.GetInt(dr["radius"]);
            alert.SchoolDistrictId = DBValue.GetInt(dr["school_district_id"]);
            alert.IsSingleFamily = DBValue.GetBool(dr["single_family_homes"]);
            alert.State = DBValue.GetString(dr["state"]);
            alert.Title = DBValue.GetString(dr["title"]);
            alert.UserGuid = DBValue.GetString(dr["g_user_id"]);
        }

        private Library.Business.SearchAlert GetLegacyAlert(SearchAlert alert)
        {
            Library.Business.SearchAlert legacyAlert = new Library.Business.SearchAlert();
            legacyAlert.AlertId = alert.AlertId;
            legacyAlert.Bedrooms = alert.Bedrooms;
            legacyAlert.BuilderId = alert.BuilderId;
            legacyAlert.BuildOnYourLot = alert.IsBoyl;
            legacyAlert.City = alert.City;
            legacyAlert.CondoTownhomeHomes = alert.IsCondo;
            legacyAlert.DateCreated = alert.DateCreated;
            legacyAlert.DateExpired = alert.ExpirationDate.ToType<DateTime>();
            legacyAlert.GatedCommunity = alert.IsGatedCommunity;
            legacyAlert.GolfCourse = alert.HasGolfCourse;
            legacyAlert.MarketId = alert.MarketId.ToType<int>();
            legacyAlert.Pool = alert.HasPool;
            legacyAlert.PostalCode = alert.PostalCode;
            legacyAlert.PriceMax = alert.PriceMax.ToType<int>();
            legacyAlert.PriceMin = alert.PriceMin.ToType<int>();
            legacyAlert.Radius = alert.Radius;
            legacyAlert.SchoolDistrictId = alert.SchoolDistrictId;
            legacyAlert.SingleFamilyHomes = alert.IsSingleFamily;
            legacyAlert.State = alert.State;
            legacyAlert.Title = alert.Title;
            legacyAlert.UserId = alert.UserGuid;
            return legacyAlert;
        }

        public void SetGeoLocation(Market market, string stateName, string cityName, string zipCode, int radius, Mvc.Domain.Model.Profiles.SearchAlert alert, IMapService mapService)
        {

            if (string.IsNullOrWhiteSpace(stateName) == false && string.IsNullOrWhiteSpace(cityName) == false)
            {
                var city = mapService.GeoCodeCityByMarket(cityName, market.MarketId.ToType<int>(),
                                market.StateAbbr);
                alert.Lat = city.Latitude;
                alert.Lng = city.Longitude;
                alert.Radius = city.Radius;
                return;
            }

            if (string.IsNullOrWhiteSpace(zipCode) == false)
            {
                var zip = mapService.GeoCodePostalCode(zipCode);
                alert.Lat = zip.Latitude;
                alert.Lng = zip.Longitude;
                alert.Radius = radius;
                return;
            }

            if (market.LatLong != null)
            {
                alert.Lat = market.LatLong.Latitude;
                alert.Lng = market.LatLong.Longitude;
            }
            else
            {
                alert.Lat = market.Latitude.ToType<double>();
                alert.Lng = market.Longitude.ToType<double>();
            }
            alert.Radius = market.Radius;
        }

        public int GetNoMatchCount(string userId)
        {
            DataTable dt = DataProvider.Current.SearchAlertGetNoMatchCount(userId);
            return dt.Rows.Count > 0 ? DBValue.GetInt(dt.Rows[0][0]) : 0;
        }

        public void IncrementNoMatchCount(string userId)
        {
            DataProvider.Current.SearchAlertIncrementNoMatchCount(userId);
        }
        
        public void ResetNoMatchCount(string userId)
        {
            DataProvider.Current.SearchAlertResetNoMatchCount(userId);
        }

    }
}
