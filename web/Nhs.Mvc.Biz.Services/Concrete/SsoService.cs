﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using ComponentSpace.SAML2.Assertions;
using ComponentSpace.SAML2.Bindings;
using ComponentSpace.SAML2.Profiles.SSOBrowser;
using ComponentSpace.SAML2.Profiles.SingleLogout;
using ComponentSpace.SAML2.Protocols;
using Nhs.Library.Business.Sso;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class SsoService : ISsoService
    {
        #region MRIS REST Service fiels and properties
        //The user credentials
        private readonly string _userId;
        private readonly string _password;
        //The user agent
        private readonly string _userAgent;
        //The endpoint
        private readonly string _endpoint;
        //Create the cookie container for all requests
        private CookieContainer cookieJar = new CookieContainer();
        //Create the credential cache for all requests
        private NetworkCredential _credentials;

        public bool Debug { get; set; }
        public bool SupportGZip { get; set; }

        #endregion

        private readonly IUserProfileService _userService;
        private readonly IPathMapper _pathMapper;
        private readonly IMarketService _marketService;
        private readonly IPartnerService _partnerService;

        public SsoService(IUserProfileService userService, IPathMapper pathMapper, IPartnerService partnerService, IMarketService marketService)
        {
            _userService = userService;
            _pathMapper = pathMapper;
            _partnerService = partnerService;
            _marketService = marketService;

            #region MRIS Rest Service init

            _userId = Configuration.MrisRestUserId;
            _password = Configuration.MrisRestPassword;
            _userAgent = Configuration.MrisRestUserAgent;
            _endpoint = Configuration.MrisRestEndpoint;
            if ((_endpoint.EndsWith("/") == false))
            {
                // Ensure that the endpoint has a trailing "/"
                _endpoint = _endpoint + "/";
            }

            // Update the credentials
            _credentials = new NetworkCredential(_userId, _password);
            #endregion
        }

        /// <summary>
        /// Receives the SAML Response by either HTTPPost or HTTPArtifact.  HTTPPost will be used in most cases (default)
        /// </summary>
        /// <param name="currentRequest">HTTPCurrentRequest</param>
        /// <param name="siteUrl">siteUrl</param>
        public NhsSamlReponse ReceiveSamlResponse(HttpRequest currentRequest, string siteUrl)
        {
            var nhsSamlReponse = new NhsSamlReponse();
            try
            {
                //Receive the SAML response over the specified binding (HTTP by default).
                XmlElement samlResponseXml;
                string relayState;

                Trace.Write("Receiving SAML Response" + Environment.NewLine);

                if (currentRequest != null)
                    Trace.Write("Current Request Present" + Environment.NewLine);

                ServiceProvider.ReceiveSAMLResponseByHTTPPost(currentRequest, out samlResponseXml, out relayState);
                Trace.Write("SAML Response Received from Provider. Checking signature..." + Environment.NewLine);

                if (samlResponseXml == null || string.IsNullOrEmpty(samlResponseXml.InnerXml))
                    Trace.Write("SAML Response Empty!" + Environment.NewLine);

                // Verify the response's signature.
                if (SAMLMessageSignature.IsSigned(samlResponseXml))
                {
                    var x509Certificate = LoadCertificate(siteUrl, null);

                    if (!SAMLMessageSignature.Verify(samlResponseXml, x509Certificate))
                    {
                        throw new ArgumentException("The SAML response signature failed to verify." + Environment.NewLine);
                    }
                }

                Trace.Write("Writing relay state in next line..." + Environment.NewLine);
                Trace.Write("Relay State: " + relayState + Environment.NewLine);
                Trace.Write("Getting Resource URL..." + Environment.NewLine);

                //68340
                //nhsSamlReponse.RelayState = RelayStateCache.Get(relayState).ResourceURL;
                if (relayState == null)
                    nhsSamlReponse.RelayState = string.Format("http://www.newhomesourceprofessional.com/{0}", siteUrl);
                else
                {
                    nhsSamlReponse.RelayState = RelayStateCache.Get(relayState) != null
                    ? RelayStateCache.Get(relayState).ResourceURL
                    : string.Format("http://www.newhomesourceprofessional.com/{0}", siteUrl);
                }

                //Deserialize the XML.
                Trace.Write("Deserializing SAML Response" + Environment.NewLine);
                nhsSamlReponse.SamlResponse = new SAMLResponse(samlResponseXml);
                Trace.Write("SAML Response Deserialized successfully." + Environment.NewLine);
            }
            catch (Exception ex)
            {
                Trace.Write("Failed to receive SAML Response: " + ex.Message + Environment.NewLine);
                ErrorLogger.LogError(ex);
            }
            return nhsSamlReponse;
        }

        /// <summary>
        /// Process a Successful SAML Response
        /// </summary>
        /// <param name="samlResponse">SAML Response</param>
        /// <param name="relayState">RelayState (caching)</param>
        /// <param name="attributes">List of SAML Attributes for the Partner</param>
        /// <param name="partnerId">Current Partner ID</param>
        /// <param name="partnerSiteUrl">Current Partner Site URL</param>
        /// <param name="brandPartnerId">Main Brand Id</param>
        /// <param name="wrongPasswordMessage"></param>
        /// <param name="wrongEmailMessage"></param>
        /// <param name="userAlreadyExistMessage"></param>
        public void ProcessSuccessSamlResponse(SAMLResponse samlResponse, string relayState, PartnerSamlAttributes attributes, int partnerId, string partnerSiteUrl, int brandPartnerId, string wrongPasswordMessage, string wrongEmailMessage, string userAlreadyExistMessage)
        {
            try
            {
                // Extract the asserted identity from the SAML response.
                SAMLAssertion samlAssertion;

                //Gets the SAML Assertion from the SAML Response
                if (samlResponse.GetAssertions().Count > 0)
                    samlAssertion = samlResponse.GetAssertions()[0];
                //Gets the SAML Assertion if encrypted
                else if (samlResponse.GetEncryptedAssertions().Count > 0)
                {
                    X509Certificate2 x509Certificate = LoadCertificate(string.Empty, null);
                    samlAssertion = samlResponse.GetEncryptedAssertions()[0].Decrypt(x509Certificate.PrivateKey, null, null);
                }
                else if (samlResponse.GetSignedAssertions().Count > 0)
                {
                    XmlElement samlAssertionXml = samlResponse.GetSignedAssertions()[0];
                    X509Certificate2 x509Certificate = LoadCertificate(partnerSiteUrl, null);
                    // not shown but you’ll need to load the IdP’s certificate

                    if (!SAMLAssertionSignature.Verify(samlAssertionXml, x509Certificate))
                    {
                        throw new ArgumentException("The SAML assertion signature failed to verify." +
                                                    Environment.NewLine);
                    }

                    samlAssertion = new SAMLAssertion(samlAssertionXml);
                }

                else
                {
                    throw new ArgumentException("No assertions in response" + Environment.NewLine);
                }

                //Case 61257, MRIS SSO
                samlAssertion = AddSamlAttributeStatement(samlAssertion, partnerSiteUrl);

                ValidateAssertion(samlAssertion, attributes, partnerId, brandPartnerId, partnerSiteUrl, wrongPasswordMessage, wrongEmailMessage, userAlreadyExistMessage);
            }
            catch (Exception ex)
            {
                Trace.Write("Error:" + ex.Message + Environment.NewLine);
                ErrorLogger.LogError(ex);
            }
        }

        private SAMLAssertion AddSamlAttributeStatement(SAMLAssertion assertion, string partnerUrl)
        {
            try
            {
                if (partnerUrl.ToLower() == "mris")
                {
                    string agentId = assertion.Subject.NameID.NameIdentifier;

                    string resourceKey =
                        GetResourceKeyByAgentId(ExecuteRequest("Agent", string.Format("(AgentID EQ '{0}')", agentId)));
                    string query = string.Format(
                        "{0}/?Select=AgentKey,AgentFirstName,AgentLastName,AgentEmail,AgentHomePhone,AgentCellPhone,AgentPostalCode,OfficeName",
                        resourceKey);
                    string userInfo = ExecuteRequest("Agent", query, true).ToString();
                    ParseUserInfo(userInfo, assertion);
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }
            return assertion;
        }

        private void ParseUserInfo(string userInfo, SAMLAssertion assertion)
        {
            try
            {
                byte[] byteArray = Encoding.UTF8.GetBytes(userInfo);
                var stream = new MemoryStream(byteArray);
                XElement documentRoot = XElement.Load(stream);
                var agentAttributes = documentRoot.Descendants("MRISAgent").Elements().Descendants();
                var statement = new AttributeStatement();
                statement.InitDefaults();
                foreach (var attribute in agentAttributes)
                {
                    var samlAttribute = new SAMLAttribute(attribute.Name.LocalName,
                                                                    "urn:oasis:names:tc:SAML:2.0:attrname-format:uri",
                                                                    attribute.Name.LocalName, attribute.Value);
                    statement.Attributes.Add(samlAttribute);
                }

                assertion.Statements.Add(statement);
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }
        }

        private string GetResourceKeyByAgentId(object executeRequest)
        {
            // convert string to stream
            byte[] byteArray = Encoding.UTF8.GetBytes((string)executeRequest);
            var stream = new MemoryStream(byteArray);
            XElement documentRoot = XElement.Load(stream);

            string resourceKey = documentRoot.Descendants("MRISAgents").Elements().First().Attribute("ResourceKey").Value;

            return resourceKey;
        }




        public void ProcessErrorSamlResponse(SAMLResponse samlResponse)
        {
            try
            {
                if (samlResponse.Status.StatusMessage != null)
                {
                    Trace.Write("Error SAML Response:  " + samlResponse.Status.StatusMessage + Environment.NewLine);
                    throw new ArgumentException(samlResponse.Status.StatusMessage.Message);
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }
        }

        /// <summary>
        /// Validates the assertion.  First if check if the email exists in the DB (per case 56907).  
        /// This allows multiple users to use the same email address for SSO.  Then it checks by SSO ID and Partner ID.
        /// Finally it created the account in case the previous validations returned null
        /// </summary>
        /// <param name="assertion">SAML Assertion from which the values are going to be read</param>
        /// <param name="attributes">List of SAML Attributes for the partner, filtered by partner site URL</param>        
        /// <param name="partnerId">Current Partner ID</param>
        /// <param name="brandPartnerId">Main brand Id</param>
        /// <param name="wrongPasswordMessage"></param>
        /// <param name="wrongEmailMessage"></param>
        /// <param name="userAlreadyExistMessage"></param>
        public void ValidateAssertion(SAMLAssertion assertion, PartnerSamlAttributes attributes, int partnerId, int brandPartnerId, string partnerSiteUrl, string wrongPasswordMessage, string wrongEmailMessage, string userAlreadyExistMessage)
        {
            try
            {
                var newProfile = new UserProfile();
                string fullName = GetSamlAttributeValueByName(attributes, "FullName", assertion);
                if (fullName != string.Empty)
                {
                    var names = fullName.Split(' ');
                    newProfile.FirstName = names[0];
                    newProfile.LastName = names[1];
                }
                else
                {
                    newProfile.FirstName = GetSamlAttributeValueByName(attributes, "FirstName", assertion);
                    newProfile.LastName = GetSamlAttributeValueByName(attributes, "LastName", assertion);
                }
                newProfile.LogonName = GetSamlAttributeValueByName(attributes, "Email", assertion);
                newProfile.SsoUserId = GetSamlAttributeValueByName(attributes, "SsoId", assertion);

                newProfile.AgencyName = GetSamlAttributeValueByName(attributes, "AgencyName", assertion);
                newProfile.PartnerId = partnerId;

                if (!string.IsNullOrEmpty(newProfile.LogonName))
                {
                    UserProfile ssoUserProfile = _userService.GetUserByLogonName(newProfile.LogonName, partnerId);
                    if (ssoUserProfile != null)
                    {
                        //Signs the user in
                        var finalProfile = CompareUserProfiles(ssoUserProfile, newProfile);
                        _userService.UpdateProfile(finalProfile);
                        _userService.SignIn(finalProfile.LogonName, finalProfile.PartnerId, wrongPasswordMessage);
                    }
                    else
                    {
                        ssoUserProfile = _userService.GetUserBySsoId(newProfile.SsoUserId, partnerId);
                        //Signs the user in
                        if (ssoUserProfile != null)
                        {
                            var finalProfile = CompareUserProfiles(ssoUserProfile, newProfile);
                            _userService.UpdateProfile(finalProfile);
                            _userService.SignIn(finalProfile.LogonName, finalProfile.PartnerId, wrongPasswordMessage);
                        }
                        else //Creates the user account
                        {
                            var subscribeToNewsletter = VerifyPartnerAllowsWeeklyOptIn(partnerSiteUrl);
                            CreateSsoUserAccount(assertion, attributes, partnerId, brandPartnerId, wrongPasswordMessage, wrongEmailMessage, userAlreadyExistMessage, subscribeToNewsletter);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }
        }

        /// <summary>
        /// Creates the User Account after validating if the email address doesn't exist in the system.  It also validates by SSO id and partner id
        /// </summary>
        /// <param name="assertion"></param>
        /// <param name="attributes"></param>
        /// <param name="partnerId"></param>
        /// <param name="brandPartnerId"></param>
        /// <param name="userAlreadyExistMessage"></param>
        /// <param name="subcribeNewsletter"></param>
        /// <param name="wrongPasswordMessage"></param>
        /// <param name="wrongEmailMessage"></param>
        /// <returns></returns>
        public bool CreateSsoUserAccount(SAMLAssertion assertion, PartnerSamlAttributes attributes, int partnerId, int brandPartnerId, string wrongPasswordMessage, string wrongEmailMessage, string userAlreadyExistMessage, bool subcribeNewsletter = true)
        {
            Boolean result = false;
            try
            {
                var profile = new UserProfile();
                string fullName = GetSamlAttributeValueByName(attributes, "FullName", assertion);
                if (fullName != string.Empty)
                {
                    var names = fullName.Split(' ');
                    profile.FirstName = names[0];
                    profile.LastName = names[1];
                }
                else
                {
                    profile.FirstName = GetSamlAttributeValueByName(attributes, "FirstName", assertion);
                    profile.LastName = GetSamlAttributeValueByName(attributes, "LastName", assertion);
                }
                profile.LogonName = GetSamlAttributeValueByName(attributes, "Email", assertion);
                profile.SsoUserId = GetSamlAttributeValueByName(attributes, "SsoId", assertion);
                profile.DayPhone = GetSamlAttributeValueByName(attributes, "Phone", assertion);
                profile.EveningPhone = GetSamlAttributeValueByName(attributes, "MobilePhone", assertion);
                profile.PartnerId = partnerId;
                profile.Password = CreateRandomPassword(12);
                profile.DateRegistered = DateTime.Now;
                profile.DateLastChanged = DateTime.Now;
                profile.PartnerId = partnerId;
                profile.AgencyName = GetSamlAttributeValueByName(attributes, "AgencyName", assertion);
                string postalCode = GetSamlAttributeValueByName(attributes, "PostalCode", assertion);
                var isValidZip = false;
                if (!string.IsNullOrEmpty(postalCode))
                {
                    //Check if postal code matches a market in our system
                    var marketInfo = _marketService.GetMarketInfoForCreateAccount(postalCode);
                    if (!string.IsNullOrEmpty(marketInfo))
                    {
                        profile.PostalCode = postalCode;
                        profile.RegistrationMarket = marketInfo.Split(',')[1];
                        isValidZip = true;
                    }
                }

                if (!isValidZip)
                {
                    //Invalid ZipCode or just doesn't match one in our system
                    //Set default market                    
                    var partnerInfo = _partnerService.GetPartner(partnerId);
                    if (!string.IsNullOrEmpty(partnerInfo.SelectMetroId))
                        profile.RegistrationMarket = partnerInfo.SelectMetroId;
                }
                profile.MailingList = "0";
                profile.MarketOptin = "0";
                //When creating an SSO account, the default is to registrate the user to the combined email
                profile.WeeklyOptin = subcribeNewsletter;

                // Setting those values ... required in stored proc.
                profile.BoxRequestedDate = DateTime.Now;
                profile.InitialMatchDate = DateTime.Now;
                profile.LastMatchesSentDate = DateTime.Now;

                // Create user.  The user won't get a registration lead
                _userService.CreateProfile(profile, brandPartnerId, wrongPasswordMessage, wrongEmailMessage, userAlreadyExistMessage);
                //Signs the user in
                _userService.SignIn(profile.LogonName, partnerId, wrongPasswordMessage);
                //true is returned if the whole process of creating the account was successful
                result = true;
                Trace.Write("SSO User account created succesfully." + Environment.NewLine);
            }
            catch (Exception ex)
            {
                Trace.Write("SSO User account creation failed:" + ex.Message + Environment.NewLine);
                ErrorLogger.LogError(ex);
            }
            return result;
        }

        /// <summary>
        /// Verifies if the partner url belongs to the not opt in list (ticket 82118).  
        /// If it exists, a false is returned, indicating the value for the OptWeeklyIn property of the user profile
        /// Otherwise, it returns true, as by default the user should get subscribed to the email.
        /// </summary>
        /// <param name="partnerUrl"></param>
        /// <returns></returns>
        public bool VerifyPartnerAllowsWeeklyOptIn(string partnerUrl)
        {
            var isAllowed = true;
            var partners = Configuration.NotOptInProPartners.ToLower().Split(',').ToList();
            if (partners.Contains(partnerUrl))
                isAllowed = false;
            return isAllowed;
        }

        public string CreateRandomPassword(int passwordLength)
        {
            const string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@$?_-";
            var chars = new char[passwordLength];
            var rd = new Random();

            for (int
                     i = 0;
                 i < passwordLength;
                 i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            return new string(chars);
        }

        /// <summary>
        /// Returns the attribute value from the assertion
        /// The method checks the attributes returned from the SsoConfigAttributes.xml (filtered by partner).
        /// Then it gets the key in order to obtain the value of the attribute with it.  This method avoid having to specify
        /// partner names like "armls" or "mls listings", making the attributes configurable through the XML file.
        /// If the attribute name passed as parameter doesn't exist returns string.empty
        /// </summary>
        /// <param name="attributes">List of attributes for the partner</param>
        /// <param name="name">Attribute Name that will be used to match the Attribute Key</param>
        /// <param name="assertion">SAML assertion from which the values will be read</param>
        /// <returns></returns>
        public string GetSamlAttributeValueByName(PartnerSamlAttributes attributes, string name, SAMLAssertion assertion)
        {
            string attributeValue = string.Empty;
            try
            {
                if (attributes != null)
                {
                    foreach (XSamlAttribute xSamlAttribute in attributes.SamlAttributes)
                    {
                        if (xSamlAttribute.Name == name)
                        {
                            attributeValue = assertion.GetAttributeValue(xSamlAttribute.Key);
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }
            return attributeValue;
        }

        /// <summary>
        /// Creates the SAML Authentication Request to initiate the SSO process
        /// </summary>
        /// <param name="siteUrl"></param>
        /// <param name="partnerSsoDestinationUrl"></param>
        /// <returns></returns>
        private XmlElement CreateAuthnRequest(string siteUrl, string partnerSsoDestinationUrl)
        {
            var authnRequest = new AuthnRequest();
            try
            {
                //Creates the authentication request.
                Trace.Write(String.Format("Creating SAML Authn Request for partner {0}", siteUrl) + Environment.NewLine);
                authnRequest.ForceAuthn = false;
                authnRequest.Issuer =
                    new Issuer(string.Format("http://www.newhomesourceprofessional.com/SPEntityDescriptors/{0}.xml",
                                             siteUrl));
                authnRequest.AssertionConsumerServiceURL =
                    string.Format("http://www.newhomesourceprofessional.com/{0}/SSO/AssertionConsumerService?binding=post",
                                  siteUrl);
                authnRequest.Destination = partnerSsoDestinationUrl;
                //authnRequest.AssertionConsumerServiceIndex = 0;

                authnRequest.NameIDPolicy = new NameIDPolicy(null, null, true);
                Trace.Write("SAML Authn Request created successfully" + Environment.NewLine);
            }
            catch (Exception ex)
            {
                Trace.Write("SAML Authn Request creation failed: " + ex.Message + Environment.NewLine);
                ErrorLogger.LogError(ex);
            }
            // Serialize the authentication request to XML for transmission.
            return authnRequest.ToXml();
        }

        /// <summary>
        /// Creates the SAML Authentication Request to initiate the SSO process
        /// </summary>
        /// <param name="siteUrl"></param>
        /// <param name="partnerSloDestinationUrl"></param>
        /// <returns></returns>
        private XmlElement CreateLogOutRequest(string siteUrl, string partnerSloDestinationUrl)
        {
            //Creates the authentication request.
            var logOutRequest = new LogoutRequest();
            try
            {
                Trace.Write(String.Format("Creating SAML LogOut Request for partner {0}", siteUrl) + Environment.NewLine);
                //authnRequest.ForceAuthn = false;

                logOutRequest.Reason = "User requested logout from NHS Pro site";
                logOutRequest.Issuer =
                    new Issuer(string.Format("http://www.newhomesourceprofessional.com/SPEntityDescriptors/{0}.xml",
                                             siteUrl));

                logOutRequest.ID = string.Format("http://www.newhomesourceprofessional.com/SPEntityDescriptors/{0}.xml",
                                                 siteUrl);
                Trace.Write("SAML LogOut Request created successfully");
            }
            catch (Exception ex)
            {
                Trace.Write("SAML LogOut Request creation failed" + Environment.NewLine);
                ErrorLogger.LogError(ex);
            }
            // Serialize the authentication request to XML for transmission.
            return logOutRequest.ToXml();
        }

        /// <summary>
        /// Request the Login at the Identity Provider.  NHS Pro acts as Service Provider
        /// </summary>
        /// <param name="partnerSiteUrl"></param>
        /// <param name="currentHttpResponse"></param>
        /// <param name="partnerSsoDestinationUrl"></param>
        public bool RequestLoginAtIdentityProvider(HttpResponse currentHttpResponse, string partnerSiteUrl,
                                                   string partnerSsoDestinationUrl)
        {
            bool successfulRequest = false;
            try
            {
                Trace.Write("Requesting Login at the Identity Provider" + Environment.NewLine);
                var authnRequestXml = CreateAuthnRequest(partnerSiteUrl, partnerSsoDestinationUrl);
                string spResourceUrl = string.Format("http://www.newhomesourceprofessional.com/{0}", partnerSiteUrl);
                //TODO: Once the Home Results page is updated and we stop using the old version, there will not be need to check for the "SsoUrlReferrer", as it 
                //could be retrieved via Request.UrlReferrer.  We ended up addng this as the site works on 'Classic Mode' and in order to write custom headers to the request
                //the configuration needs to be set to 'Integrated Mode'.  This the fix for the comment added to the ticket 63892 on 27/09/201 by Jose Morales.
                if (!String.IsNullOrEmpty(HttpContext.Current.Request.Params[UrlConst.SsoUrlReferrer]))
                    spResourceUrl = HttpContext.Current.Request.Params[UrlConst.SsoUrlReferrer];

                // Create and cache the relay state so we remember which SP resource the user wishes to access after SSO.
                string relayState = RelayStateCache.Add(new RelayState(spResourceUrl, null));

                // Send the authentication request to the identity by HTTPRedirect or HTTPPost (MRIS only).
                if (partnerSiteUrl.ToLower() == "mris")
                {
                    ServiceProvider.SendAuthnRequestByHTTPPost(currentHttpResponse, partnerSsoDestinationUrl,
                                                               authnRequestXml, relayState);
                    SetMrisInterstitial();
                }
                else
                    ServiceProvider.SendAuthnRequestByHTTPRedirect(currentHttpResponse, partnerSsoDestinationUrl,
                                                                   authnRequestXml, relayState, null);

                successfulRequest = true;
            }
            catch (Exception ex)
            {
                Trace.Write(ex.Message + Environment.NewLine);
                ErrorLogger.LogError(ex);
            }
            return successfulRequest;
        }

        /// <summary>
        /// Request the Logout (Single Sign On) at the Identity Provider.  NHS Pro acts as Service Provider
        /// </summary>
        /// <param name="partnerSiteUrl"></param>
        /// <param name="currentHttpResponse"></param>
        /// <param name="partnerSloDestinationUrl"></param>
        public bool RequestLogOutAtIdentityProvider(HttpResponse currentHttpResponse, string partnerSiteUrl, string partnerSloDestinationUrl)
        {
            bool successfulRequest = false;
            try
            {
                Trace.Write("Requesting LogOut at the Identity Provider" + Environment.NewLine);
                var logOutRequestXml = CreateLogOutRequest(partnerSiteUrl, partnerSloDestinationUrl);
                var spResourceUrl = string.Format("http://www.newhomesourceprofessional.com/{0}", partnerSiteUrl);

                // Create and cache the relay state so we remember which SP resource the user wishes to access after SSO.
                string relayState = RelayStateCache.Add(new RelayState(spResourceUrl, null));

                partnerSloDestinationUrl = partnerSloDestinationUrl + Configuration.SloUrlParameters;

                // Send the logout request to the identity Provider
                SingleLogoutService.SendLogoutRequestByHTTPPost(currentHttpResponse, partnerSloDestinationUrl, logOutRequestXml, relayState);

                successfulRequest = true;
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }
            return successfulRequest;
        }


        private void SetMrisInterstitial()
        {
            var referUrl = HttpContext.Current.Request.UrlReferrer != null ? HttpContext.Current.Request.UrlReferrer.Host : "";

            if (string.IsNullOrWhiteSpace(referUrl) == false && referUrl.Contains(".mris.com"))
            {
                CookieManager.MrisInterstitial = referUrl;
            }
        }

        // Loads the certificate from file.
        // A password is only required if the file contains a private key.
        // The machine key set is specified so the certificate is accessible to the IIS process.
        private X509Certificate2 LoadCertificate(string siteUrl, string password)
        {
            Trace.Write(string.Format("Loading certificate for partner {0}", siteUrl) + Environment.NewLine);
            string cacheKey = "SsoCertificate_" + siteUrl;
            var ssoCertificate =
                WebCacheHelper.GetObjectFromCache(cacheKey, false) as X509Certificate2;
            if (ssoCertificate == null)
            {

                string certificateName = _pathMapper.MapPath(string.Format(@"~\Configs\SsoConfigs\{0}.cer", siteUrl));
                if (!File.Exists(certificateName))
                {
                    throw new ArgumentException("The certificate file " + certificateName + " doesn't exist.");
                }
                try
                {
                    ssoCertificate = new X509Certificate2(certificateName, password, X509KeyStorageFlags.MachineKeySet);
                    WebCacheHelper.AddObjectToCache(ssoCertificate, cacheKey,
                                                    new TimeSpan(WebCacheConst.DefaultHours, 0, 0), false);
                    Trace.Write("Certificate loaded succesfully" + Environment.NewLine);
                }
                catch (Exception ex)
                {
                    Trace.Write("Failed to load the certificate" + Environment.NewLine);
                    ErrorLogger.LogError(ex);
                    return null;
                }
            }
            return ssoCertificate;
        }

        public bool ValidateMredTermsOfService(UserProfile newUserProfile, int brandPartnerId, string partnerUrl, string wrongPasswordMessage, string wrongEmailMessage, string userAlreadyExistMessage)
        {
            bool showTerms = true;
            var ssoUserProfile = _userService.GetUserByLogonName(newUserProfile.LogonName, newUserProfile.PartnerId);

            if (ssoUserProfile != null)
            {
                //CreateUserProfile validate if exist in order to return the previous created account
                var subscribeToNewsletter = VerifyPartnerAllowsWeeklyOptIn(partnerUrl);
                CreateUserProfile(newUserProfile, brandPartnerId, wrongPasswordMessage, wrongEmailMessage, userAlreadyExistMessage, subscribeToNewsletter);
                showTerms = false;
            }

            return showTerms;
        }

        public void CreateUserProfile(UserProfile newUserProfile, int brandPartnerId, string wrongPasswordMessage, string wrongEmailMessage, string userAlreadyExistMessage, bool suscribedNewletter = true)
        {
            try
            {
                UserProfile ssoUserProfile = _userService.GetUserByLogonName(newUserProfile.LogonName, newUserProfile.PartnerId);
                if (ssoUserProfile != null)
                {
                    //Signs the user in
                    var finalProfile = CompareUserProfiles(ssoUserProfile, newUserProfile);
                    _userService.UpdateProfile(finalProfile);
                    _userService.SignIn(finalProfile.LogonName, finalProfile.PartnerId, wrongEmailMessage);
                }
                else
                {
                    UserProfile userProfile = _userService.GetUserBySsoId(newUserProfile.SsoUserId, newUserProfile.PartnerId);
                    if (userProfile != null)
                    {
                        var finalProfile = CompareUserProfiles(userProfile, newUserProfile);
                        _userService.UpdateProfile(finalProfile);
                        _userService.SignIn(finalProfile.LogonName, finalProfile.PartnerId, wrongEmailMessage);
                    }
                    else
                    {

                        userProfile = new UserProfile
                        {
                            LogonName = newUserProfile.LogonName,
                            FirstName = newUserProfile.FirstName,
                            LastName = newUserProfile.LastName,
                            PostalCode = newUserProfile.PostalCode,
                            DayPhone = newUserProfile.DayPhone,
                            SsoUserId = newUserProfile.SsoUserId,
                            PartnerId = newUserProfile.PartnerId,
                            RealStateLicense = newUserProfile.RealStateLicense,
                            EveningPhone = newUserProfile.EveningPhone,
                            AgencyName = newUserProfile.AgencyName,
                            Password = CreateRandomPassword(12),
                            DateRegistered = DateTime.Now,
                            DateLastChanged = DateTime.Now,
                            MailingList = "0",
                            MarketOptin = "0",
                            BoxRequestedDate = DateTime.Now,
                            InitialMatchDate = DateTime.Now,
                            LastMatchesSentDate = DateTime.Now,
                            WeeklyOptin = suscribedNewletter
                        };
                        var isValidZip = false;
                        if (!string.IsNullOrEmpty(userProfile.PostalCode))
                        {
                            //Check if postal code matches a market in our system
                            var marketInfo = _marketService.GetMarketInfoForCreateAccount(userProfile.PostalCode);
                            if (!string.IsNullOrEmpty(marketInfo))
                            {
                                userProfile.PostalCode = userProfile.PostalCode;
                                userProfile.RegistrationMarket = marketInfo.Split(',')[1];
                                isValidZip = true;
                            }
                        }

                        if (!isValidZip)
                        {
                            //Invalid ZipCode or just doesn't match one in our system
                            userProfile.PostalCode = null;
                            //Set default market                    
                            var partnerInfo = _partnerService.GetPartner(userProfile.PartnerId);
                            if (!string.IsNullOrEmpty(partnerInfo.SelectMetroId))
                                userProfile.RegistrationMarket = partnerInfo.SelectMetroId;
                        }
                        // Setting those values ... required in stored proc.
                        // Create user.  The user won't get a registration lead
                        _userService.CreateProfile(userProfile, brandPartnerId, wrongPasswordMessage, wrongEmailMessage, userAlreadyExistMessage);
                        //Signs the user in
                        _userService.SignIn(userProfile.LogonName, userProfile.PartnerId, wrongEmailMessage);
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }
        }

        /// <summary>
        /// Authenticates user using Custom SSO
        /// </summary>
        /// <param name="sessionToken">The session token.</param>
        /// <param name="dateTime">The date time.</param>
        /// <param name="algorithm">The hashing algorithm.</param>
        /// <param name="partnerSitePassword">The partner site password (Site terms).</param>
        /// <param name="ssoId">The unique sso id provided by the PL.</param>
        /// <returns></returns>
        public bool Authenticate(string sessionToken, DateTime dateTime, string algorithm, string partnerSitePassword,
                                 string ssoId)
        {
            return WebApiSecurity.Authenticate(sessionToken, algorithm, partnerSitePassword, ssoId);
        }

        /// <summary>
        /// Checks if the existing Profile in the DB is different to what was received by the SSO or EncrytedData.  If so, it updates the Profile.
        /// </summary>
        /// <param name="oldProfile"></param>
        /// <param name="newProfile"></param>
        /// <returns></returns>
        private UserProfile CompareUserProfiles(UserProfile oldProfile, UserProfile newProfile)
        {
            var finalProfile = oldProfile;
            if (oldProfile.FirstName != newProfile.FirstName && !String.IsNullOrEmpty(newProfile.FirstName))
                finalProfile.FirstName = newProfile.FirstName;
            if (oldProfile.LastName != newProfile.LastName && !String.IsNullOrEmpty(newProfile.LastName))
                finalProfile.LastName = newProfile.LastName;
            if (oldProfile.LogonName != newProfile.LogonName && !String.IsNullOrEmpty(newProfile.LogonName))
                finalProfile.LogonName = newProfile.LogonName;
            var isValidZip = false;
            if (!string.IsNullOrEmpty(newProfile.PostalCode))
            {
                //Check if postal code matches a market in our system
                var marketInfo = _marketService.GetMarketInfoForCreateAccount(newProfile.PostalCode);
                if (!string.IsNullOrEmpty(marketInfo))
                {
                    oldProfile.PostalCode = newProfile.PostalCode;
                    oldProfile.RegistrationMarket = marketInfo.Split(',')[1];
                    isValidZip = true;
                }
            }
            if (!isValidZip)
            {
                //Invalid ZipCode or just doesn't match one in our system
                //Set default market                    
                var partnerInfo = _partnerService.GetPartner(oldProfile.PartnerId);
                if (!string.IsNullOrEmpty(partnerInfo.SelectMetroId))
                    oldProfile.RegistrationMarket = partnerInfo.SelectMetroId;
            }
            if (oldProfile.City != newProfile.City && !String.IsNullOrEmpty(newProfile.City))
                finalProfile.City = newProfile.City;
            if (oldProfile.State != newProfile.State && !String.IsNullOrEmpty(newProfile.State))
                finalProfile.State = newProfile.State;
            if (oldProfile.DayPhone != newProfile.DayPhone && !String.IsNullOrEmpty(newProfile.DayPhone))
                finalProfile.DayPhone = newProfile.DayPhone;
            if (oldProfile.EveningPhone != newProfile.EveningPhone && !String.IsNullOrEmpty(newProfile.EveningPhone))
                finalProfile.EveningPhone = newProfile.EveningPhone;
            if (oldProfile.AgencyName != newProfile.AgencyName && !String.IsNullOrEmpty(newProfile.AgencyName))
                finalProfile.AgencyName = newProfile.AgencyName;
            if (oldProfile.RealStateLicense != newProfile.RealStateLicense && !String.IsNullOrEmpty(newProfile.RealStateLicense))
                finalProfile.RealStateLicense = newProfile.RealStateLicense;

            return finalProfile;
        }

        #region MRIS REST Service Methods
        /// <summary>
        /// <para>Performs a request for the Logout Service.</para>
        /// </summary>
        public void Logout()
        {
            string url = GetResourceUrl("Logout");
            ProcessRequest(url);
        }

        /// <summary>
        /// <para>Performs a request for the UserSessionInfo Service.</para>
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public string GetUserSessionInfo()
        {
            string url = GetResourceUrl("UserSessionInfo");
            return ProcessRequest(url);
        }

        /// <summary>
        /// <para>Executes a request</para>
        /// </summary>
        /// <param name="resourcePath">The resource path</param>
        /// <param name="query">The MDS Query value</param>
        /// <param name="isAgentDataQuery"> </param>
        /// <param name="count">Optional, if <code>TRUE</code>, indicates that only the record count is to be requested.</param>
        /// <param name="limit">Optional, specifies the Limit</param>
        /// <returns>The MDS Response</returns>
        private object ExecuteRequest(string resourcePath, string query, bool isAgentDataQuery = false, bool count = false, int limit = 0)
        {
            string url = GetResourceUrl(resourcePath);
            if (isAgentDataQuery)
                url = url + "/" + query;
            else
                url = url + "/?Query=" + query;
            if (count)
                url = url + "&Count=1";

            if (limit > 0)
                url = url + "&Limit=" + limit;

            return ProcessRequest(url);
        }

        /// <summary>
        /// <para>Executes a resource request</para>
        /// </summary>
        /// <param name="resourcePath">The resource path</param>
        /// <param name="resourceKeys">The comma delimited list of resource keys</param>
        /// <param name="selectFields">Optional, specifies the comma delimited list of Select fields</param>
        /// <returns>The MDS Response</returns>
        public object ExecuteRequest(string resourcePath, string[] resourceKeys, string selectFields = null)
        {
            try
            {
                string url = GetResourceUrl(resourcePath);
                string resourceKeyList = null;
                foreach (string resourceKey in resourceKeys)
                {
                    if (resourceKeyList == null)
                    {
                        resourceKeyList = resourceKey;
                    }
                    else
                    {
                        resourceKeyList = "," + resourceKey;
                    }
                }
                url = url + "/" + resourceKeyList;
                if (((selectFields != null)))
                {
                    url = url + "?Select=" + selectFields;
                }
                return ProcessRequest(url);
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
                return null;
            }
        }

        /// <summary>
        /// <para>Processes an MDS request.</para>
        /// </summary>
        /// <param name="url">The URL to request</param>
        /// <returns>The MDS Response</returns>
        /// <exception cref="System.Net.WebException">Thrown if there was an error executing the request.</exception>
        /// <exception cref="System.Exception">Thrown if there was an unknown error executing the request.</exception>
        private string ProcessRequest(string url)
        {
            HttpWebRequest req = CreateRequest(url);

            try
            {
                // Issue the request.
                var response = (HttpWebResponse)req.GetResponse();

                // Calls the method GetResponseStream to return the stream associated with the response.
                var receiveStream = response.GetResponseStream();
                if ((response.ContentEncoding.ToLower().Contains("gzip")))
                    receiveStream = new System.IO.Compression.GZipStream(receiveStream,
                                                                         System.IO.Compression.CompressionMode.
                                                                             Decompress);

                string responseStr = "";
                // Pipes the response stream to a higher level stream reader with the required encoding format.
                Encoding encode = Encoding.GetEncoding("UTF-8");
                if (receiveStream != null)
                {
                    var readStream = new StreamReader(receiveStream, encode);

                    var read = new Char[257];
                    // Reads 256 characters at a time.   
                    int count = readStream.Read(read, 0, 256);


                    while (count > 0)
                    {
                        // Dumps the 256 characters to a string
                        var str = new String(read, 0, count);
                        responseStr = responseStr + str;
                        count = readStream.Read(read, 0, 256);
                    }

                    // Releases the resources of the Stream.
                    readStream.Close();
                }
                // Releases the resources of the response.
                response.Close();

                return responseStr;
            }
            catch (WebException ex)
            {
                ErrorLogger.LogError(ex);
                return null;
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
                return null;
            }
        }

        /// <summary>
        /// <para>Creates a <code>HttpWebRequest</code> for the specified URL</para>
        /// </summary>
        /// <param name="url">The request URL</param>
        /// <returns>The <code>HttpWebRequest</code></returns>
        private HttpWebRequest CreateRequest(string url)
        {
            try
            {
                var req = (HttpWebRequest)WebRequest.Create(url);

                //Set the User Agent
                req.UserAgent = _userAgent;

                //Define the http method.
                req.Method = "GET";

                req.CookieContainer = cookieJar;

                //Set the Digest credentials for authentication
                dynamic credentialCache = new CredentialCache();
                credentialCache.Add(new Uri(url), "Digest", _credentials);

                req.Credentials = credentialCache;

                return req;
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
                return null;
            }
        }

        /// <summary>
        /// Gets the fully qualified URL for the specified resource path.
        /// </summary>
        /// <param name="resourcePath">The path to the resource</param>
        /// <returns>The fully qualified resource URL</returns>
        private string GetResourceUrl(string resourcePath)
        {
            if (resourcePath.StartsWith("/"))
            {
                resourcePath = resourcePath.Remove(0, 1);
            }
            dynamic url = _endpoint + resourcePath;

            return url;
        }
        #endregion
    }
}
