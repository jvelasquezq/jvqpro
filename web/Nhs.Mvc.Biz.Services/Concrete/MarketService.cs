﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using Nhs.Library.Business;
using Nhs.Library.Business.Seo;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Filters;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Utility.Common;
using GeoLocation = Nhs.Mvc.Domain.Model.Web.GeoLocation;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class MarketService : IMarketService
    {
        private readonly object _partnerMarketLock = new object();
        private readonly IMarketRepository _marketRepository;
        private readonly IBasicListingRepository _basicListingRepository;
        private readonly IBuilderService _builderService;

        public MarketService(IMarketRepository marketRepository, IBasicListingRepository basicListingRepository, IBuilderService builderService)
        {
            _marketRepository = marketRepository;
            _basicListingRepository = basicListingRepository;
            _builderService = builderService;
        }

        public IEnumerable<Market> GetMarketsByAmenity(string state, string amenityType, int partnerId)
        {
            DataTable data = DataProvider.Current.GetMarketsByAmenity(state, partnerId, amenityType);
            var markets = GetMarkets(partnerId);
            var marketsByState = from c in markets
                                 join p in data.AsEnumerable() on c.MarketId equals p[CommunityDBFields.MarketId].ToType<int>()
                                 select c;
            return marketsByState;
        }

        public GeoLocation GetGeoLocationByPostalCode(string postalCode)
        {
            return _marketRepository.GetGeoLocationByPostalCode(postalCode);
        }

        /// <summary>
        /// DO NOT CACHE ITEMS RETRIEVED FROM THIS METHOD - ALREADY CACHED
        /// </summary>
        /// <param name="partnerId">The partner id.</param>
        /// <param name="stateNameOrId">The state name or id.</param>
        /// <param name="marketName">Name of the market.</param>
        /// <param name="loadBrandsAndSchools">if set to <c>true</c> [load brands and schools].</param>
        /// <returns></returns>
        public Market GetMarket(int partnerId, string stateNameOrId, string marketName, bool loadBrandsAndSchools)
        {
            var market = GetMarkets(partnerId).GetByStateNameOrIdAndMarketName(stateNameOrId, marketName); //cached already
            if (market != null && loadBrandsAndSchools)
            {
                market.Brands = GetMarketBCBrands(partnerId, market.MarketId);
                market.SchoolDistricts = GetMarketSchoolDistricts(partnerId, market.MarketId);
            }

            return market;
        }

        public Market GetMarketStateNameAndMarketName(int partnerId, string stateNameOrId, string marketName, bool loadBrandsAndSchools)
        {
            var market = GetMarkets(partnerId).GetByStateNameAndMarketName(stateNameOrId, marketName); //cached already
            if (market != null && loadBrandsAndSchools)
            {
                market.Brands = this.GetMarketBCBrands(partnerId, market.MarketId);
                market.SchoolDistricts = this.GetMarketSchoolDistricts(partnerId, market.MarketId);
            }

            return market;
        }

        /// <summary>
        /// DO NOT CACHE ITEMS RETRIEVED FROM THIS METHOD - ALREADY CACHED
        /// </summary>
        /// <param name="partnerId">The partner id.</param>
        /// <param name="marketId">The market id.</param>
        /// <param name="loadBrandsAndSchools">if set to <c>true</c> [load brands and schools].</param>
        /// <returns></returns>
        public Market GetMarket(int partnerId, int marketId, bool loadBrandsAndSchools)
        {
            var market = GetMarkets(partnerId).GetById(marketId);

            if (market == null || !loadBrandsAndSchools) return market;

            market.Brands = GetMarketBCBrands(partnerId, market.MarketId);

            market.SchoolDistricts = GetMarketSchoolDistricts(partnerId, market.MarketId);
            return market;
        }

        /// <summary>
        /// Get the market object by Id without partnerid
        /// </summary>
        /// <param name="marketId">The market id.</param>
        /// <returns></returns>
        public Market GetMarket(int marketId)
        {
            var cacheKey = WebCacheConst.WebCacheKeyPrefix + WebCacheConst.Market + marketId;
            var market = WebCacheHelper.GetObjectFromCache(cacheKey, false) as Market;

            if (market != null)
                return market;

            market = _marketRepository.Markets.GetByIdFromDb(marketId);

            if (market != null)
            {
                market.Brands = new List<Brand>();
            }

            WebCacheHelper.AddObjectToCache(market, cacheKey, new TimeSpan(WebCacheConst.DefaultHours, 0, 0), false);
            return market;
        }

        /// <summary>
        /// USE Market.Brands
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="marketId"></param>
        /// <returns></returns>
        private IList<Brand> GetMarketBCBrands(int partnerId, int marketId)
        {
            var dtComms = _builderService.GetMarketBcs(partnerId, marketId);

            List<Brand> brands = (from DataRow dr in dtComms.Rows
                                  select new Brand
                                  {
                                      BrandId = DBValue.GetInt(dr["brand_id"]),
                                      BrandName = DBValue.GetString(dr["brand_name"]),
                                      IsBoyl = DBValue.GetString(dr["listing_type_flag"]) == "B",
                                      IsCustomBuilder = DBValue.GetString(dr["custom_builder_flag"]) == "Y"
                                  }).ToList();

            foreach (var brand in brands) //these are basically builder-community records
            {
                var dupeComms = from b in brands
                                where b.BrandId == brand.BrandId
                                select b;

                var nonBoylComm = from b in dupeComms //see if there are any non-boyl communities for this brand
                                  where !b.IsBoyl
                                  select b;

                brand.HasBilledCommununities = dtComms.AsEnumerable().Any(dr => dr["brand_id"].ToType<int>() == brand.BrandId && dr["billed"].ToType<string>() == "Y");

                if (nonBoylComm.Any()) //at least one regular comm makes a brand non-boyl
                    brand.IsBoyl = false;
            }

            return brands.Distinct(new BrandComparer()).OrderBy(b => b.BrandName).ToList();
        }

        //TODO: Move this to EDMX
        public int GetMarketIdFromPostalCode(string postalCode, int partnerId)
        {
            return DataProvider.Current.GetMarketIdFromPostalCode(postalCode, partnerId, false);
        }

        public List<Location> GetSimilarPostalCodes(string postalCode, int partnerId)
        {

            DataTable zipCodes = DataProvider.Current.GetSimilarPostalCodes(postalCode, partnerId);

            return (from DataRow mkt in zipCodes.Rows
                    select new Location
                    {
                        MarketId = mkt[CommunityDBFields.MarketId].ToType<int>(),
                        Name = mkt[CommunityDBFields.PostalCode].ToType<string>(),
                        Type = (int)LocationType.Zip
                    }).ToList();
        }

        public List<Community> GetFeaturedCommunities(int partnerId, int marketId, int maxCount)
        {
            if (marketId <= 0) return GetRandomCommunitiesInMarketFromDb(partnerId, 0, maxCount, true);

            //Get for market
            var comms = GetRandomCommunitiesInMarketFromDb(partnerId, marketId, maxCount, true);

            if (comms.Count < maxCount)
                comms.AddRange(GetRandomCommunitiesInMarketFromDb(partnerId, marketId, maxCount - comms.Count, false).ToList()); //Backfill comms for market

            return comms;

            //Return featured for all markets
        }

        public List<Community> GetRandomCommunitiesInMarketFromDb(int partnerId, int marketId, int recordCount, bool featured)
        {
            var comms = new List<Community>();
            var dtComms = featured ? DataProvider.Current.GetRandomFeaturedCommunities(marketId, partnerId, recordCount) :
                DataProvider.Current.GetRandomCommunitiesInMarket(marketId, partnerId, recordCount);

            if (dtComms != null && dtComms.Rows.Count > 0)
            {
                comms.AddRange(from DataRow dr in dtComms.Rows
                               select new Community()
                               {
                                   CommunityId = DBValue.GetInt(dr["community_id"]),
                                   CommunityName = DBValue.GetString(dr["community_name"]),
                                   //City = DBValue.GetString(dr["city"]),
                                   PriceLow = DBValue.GetInt(dr["price_low"]),
                                   PriceHigh = DBValue.GetInt(dr["price_high"]),
                                   MarketId = marketId,
                                   SpotlightThumbnail = DBValue.GetString(dr["spotlight_thumbnail"]),
                                   BuilderId = DBValue.GetInt(dr["builder_id"]),
                                   Builder = new Builder
                                       {
                                           BuilderId = DBValue.GetInt(dr["builder_id"]),
                                           Url = DBValue.GetString(dr["builderurl"]),
                                           BuilderName = DBValue.GetString(dr["builder_name"]),
                                           Brand = new Brand
                                           {
                                               BrandName = DBValue.GetString(dr["brand_name"]),
                                               State = DBValue.GetString(dr["state"])
                                           }
                                       },
                                   FeaturedListingId = DBValue.GetInt(dr["featured_listing_id"]),
                                   City = DBValue.GetString(dr["city"]),
                                   Brand = new Brand
                                   {
                                       BrandName = DBValue.GetString(dr["brand_name"]),
                                       State = DBValue.GetString(dr["state"])
                                   },
                                   Market = GetMarket(partnerId, marketId, false)
                               });
            }

            return comms;
        }

        public bool IsMarketValidForPartner(int partnerId, int marketId)
        {
            var market = this.GetMarkets(partnerId).GetById(marketId);
            return market != null;
        }

        public string GetStateNameForMarket(int marketId)
        {
            return this._marketRepository.Markets.GetById(marketId).State.StateName;
        }

        //TODO: Make this private
        /// <summary>
        /// CACHED ALREADY - NO NEED TO CACHE ITEMS RETRIEVED FROM THIS METHOD
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public IEnumerable<Market> GetMarkets(int partnerId)
        {
            var cacheKey = WebCacheConst.WebCacheKeyPrefix + WebCacheConst.PartnerMarkets + partnerId;
            var markets = WebCacheHelper.GetObjectFromCache(cacheKey, false) as IEnumerable<Market>;

            if (markets != null)
                return markets;

            lock (_partnerMarketLock)
            {
                markets = WebCacheHelper.GetObjectFromCache(cacheKey, false) as IEnumerable<Market>;
                if (markets == null)
                {
                    markets = this.GetPartnerMarkets(partnerId);
                    WebCacheHelper.AddObjectToCache(markets, cacheKey, new TimeSpan(WebCacheConst.DefaultHours, 0, 0), false);
                }
            }

            return markets;
        }

        public IEnumerable<Market> GetMarkets(string markets)
        {
            var dataTable = DataProvider.Current.GetMarkets(markets);
            var marketsList = new List<Market>();

            foreach (DataRow mkt in dataTable.Rows)
                marketsList.Add(new Market
                    {
                        LatLong = new LatLong
                            {
                                Latitude = mkt[CommunityDBFields.Latitude].ToType<double>(),
                                Longitude = mkt[CommunityDBFields.Longitude].ToType<double>()
                            },
                        MarketId = mkt[CommunityDBFields.MarketId].ToType<int>()
                    });

            return marketsList;
        }

        private IEnumerable<Market> GetPartnerMarkets(int partnerId)
        {
            //            var markets = _marketRepository.GetPartnerMarkets(partnerId).ToMarket().ToList();
            var markets = _marketRepository.GetPartnerMarketsInfo(partnerId).ToMarket().ToList();

            var blmarkets = (from m in _basicListingRepository.Markets
                             join p in _basicListingRepository.PartnerBasicListings
                                 on m.MarketId equals p.MarketId
                             where p.PartnerId == partnerId
                             select new Market()
                             {
                                 MarketId = m.MarketId,
                                 MarketName = m.MarketName,
                                 StateAbbr = m.StateAbbr,
                                 Latitude = m.Latitude,
                                 Longitude = m.Longitude,
                                 Radius = m.MarketRadius,
                                 State = new State()
                                 {
                                     StateAbbr = m.State.StateAbbr,
                                     StateName = m.State.StateName,
                                     Latitude = m.State.Latitude,
                                     Longitude = m.State.Longitude
                                 }
                             }).Distinct();

            foreach (var blMarket in blmarkets)
            {
                if (markets.All(m => m.MarketId != blMarket.MarketId))
                    markets.Add(blMarket);
            }

            return markets;
        }

        public IEnumerable<Market> GetMarketsByState(int partnerId, string stateAbbr)
        {
            var partnerMarkets = this.GetMarkets(partnerId);

            var markets = partnerMarkets.ToList();

            return markets.Where(m => m.TotalCommunities > 0 && m.StateAbbr.ToLower() == stateAbbr.ToLower()).ToList().OrderBy(m => m.MarketName);
        }

        public List<Market> GetMarketsByBrand(int partnerId, string state, int brandId)
        {

            var markets = DataProvider.Current.GetMarketsByBrand(partnerId, state, brandId);
            var markets2 = GetMarkets(partnerId);
            var marketsByBrand = (from c in markets2
                                  join p in markets.AsEnumerable() on c.MarketId equals p[CommunityDBFields.MarketId].ToType<int>()
                                  select c).ToList();

            return marketsByBrand;
        }

        public string GetMarketName(int partnerId, int marketId)
        {
            var market = this.GetMarkets(partnerId).GetById(marketId);
            return market != null ? market.MarketName : string.Empty;
        }

        public IList<City> GetCitiesForMarket(int partnerId, int marketId)
        {
            if (marketId <= 0) return new List<City>();

            var markets = GetMarkets(partnerId).GetById(marketId);
            if (markets != null && markets.Cities.Any())
            {
                return markets.Cities.ToList();
            }

            return new List<City>();
        }

        public int GetMarketIdFromStateCounty(string stateName, string county)
        {
            return DataProvider.Current.GetMarketIdFromCounty(county, stateName);
        }

        public string GetMarketInfoForCreateAccount(string zip)
        {
            return DataProvider.Current.GetMarketInfoForCreateAccount(zip);
        }

        public int GetMarketIdFromStateCity(string stateName, string city)
        {
            var cityOutput = string.Empty;
            var marketId = 0;
            DataProvider.Current.GetMarketIdFromCity(city, stateName, false, ref marketId, ref cityOutput);
            return marketId;
        }

        public Market GetMarketByName(string stateName, string marketName)
        {
            DataTable marketData = DataProvider.Current.GetMarket(stateName, marketName);
            Market market = null;

            if (marketData != null && marketData.Rows.Count > 0)
                foreach (DataRow mkt in marketData.Rows)
                {
                    market = new Market() { MarketId = mkt["market_id"].ToType<int>(), MarketName = mkt["market_name"].ToType<string>(), Cities = new Collection<City>(), Brands = new List<Brand>(), State = new State() { StateAbbr = mkt["state"].ToType<string>(), StateName = stateName } };
                    break;
                }
            return market;
        }

        public IList<int> GetZipCodesByMarket(int marketId, string city, int returnCount)
        {
            DataTable marketData = DataProvider.Current.GetZipCodesByMarket(marketId, city, returnCount);
            var zipCodes = new List<int>();

            if (marketData == null || marketData.Rows.Count <= 0) return zipCodes;
            zipCodes.AddRange(from DataRow zipCode in marketData.Rows select zipCode["postal_code"].ToType<int>());
            return zipCodes;
        }

        public Market GetMarketFromIp(long ipAddress)
        {
            var market = new Market();
            DataTable dtIpLocation = DataProvider.Current.GetIPLocation(ipAddress);

            if (dtIpLocation == null || dtIpLocation.Rows.Count <= 0)
                return market;

            var results = from row in dtIpLocation.AsEnumerable()
                          where row.Field<int?>("market_id") != null
                          select row;

            if (results.Any())
                market = new Market()
                {
                    MarketId = results.First()["market_id"].ToType<int>(),
                    StateAbbr = results.First()["state"].ToType<string>()
                };

            return market;
        }

        /// <summary>
        /// Retrieves all the Zip Codes Associated to a Synthetic Geography
        /// </summary>
        /// <param name="synthetic">Synthetic Geography location to search its associated postal codes</param>
        /// <returns>All postal codes associated to the Synthetic Geography separated by comma</returns>
        public string GetZipCodesForSynthetic(Synthetic synthetic)
        {
            var cacheKey = WebCacheConst.WebCacheKeyPrefix + WebCacheConst.SyntheticGeoLocations + synthetic.Name + synthetic.MarketId;
            var postalCodes = new StringBuilder();
            var primaryMarket = GetMarket(synthetic.MarketId);
            var locations = synthetic.ToLocationsList();

            postalCodes.Append(WebCacheHelper.GetObjectFromCache(cacheKey, false) as string);

            if (locations == null || !string.IsNullOrEmpty(postalCodes.ToString()))
                return postalCodes.ToString();

            switch (synthetic.LocationType)
            {
                case LocationType.Zip:
                    postalCodes.Append(string.Join(", ", locations));
                    break;
                case LocationType.City:
                case LocationType.County:
                    foreach (var location in locations)
                    {
                        var splitLocation = location.Split(',');
                        var state = splitLocation.Length == 2 ? splitLocation[1].Trim() : primaryMarket.StateAbbr;
                        var results = synthetic.LocationType == LocationType.City ?
                            DataProvider.Current.GetPostalCodesByLocation(null, splitLocation[0], null, state) :
                            DataProvider.Current.GetPostalCodesByLocation(null, null, splitLocation[0], state);

                        postalCodes.Append(string.Join(", ", results.AsEnumerable().Select(r => r.Field<string>(0).Trim())));

                        if (results.Rows.Count > 0)
                            postalCodes.Append(", ");
                    }

                    if (postalCodes.Length >= 2)
                        postalCodes.Remove(postalCodes.Length - 2, 2);
                    break;
                case LocationType.Market:
                    foreach (var results in locations.Select(location => DataProvider.Current.GetPostalCodesByLocation(location.ToType<int>(), null, null, null)))
                    {
                        postalCodes.Append(string.Join(", ", results.AsEnumerable().Select(r => r.Field<string>(0).Trim())));
                    }
                    break;
            }

            WebCacheHelper.AddObjectToCache(postalCodes, cacheKey, new TimeSpan(WebCacheConst.DefaultHours, 0, 0), false);
            return postalCodes.ToString();
        }

        #region Private
        //TODO: Move to EDMX
        public IList<SchoolDistrict> GetMarketSchoolDistricts(int partnerId, int marketId)
        {
            DataTable table = DataProvider.Current.GetMarketSchoolDistricts(marketId, partnerId);

            return (from DataRow row in table.Rows
                    select new SchoolDistrict
                        {
                            DistrictId = DBValue.GetInt(row["district_Id"]),
                            DistrictName = DBValue.GetString(row["district_name"])
                        }).ToList();
        }
        #endregion
    }
}
