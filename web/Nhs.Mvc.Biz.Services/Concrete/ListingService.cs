﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Helpers.Vimeo;
using Nhs.Library.Proxy;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Filters;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Utility.Common;
using Nhs.Utility.Data;
using Nhs.Utility.Web;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class ListingService : IListingService
    {
        private readonly ISessionCacheService _sessionCacheService;
        private readonly IListingRepository _listingRepository;
        private readonly IImageRepository _imageRepository;
        private readonly IApiService _apiService;
        private readonly IMarketService _marketService;
        private readonly IStateService _stateService;
        private bool _useHub;

        public bool UseHub
        {
            get { return _useHub; }
            set
            {
                _useHub = _imageRepository.UseHub = _listingRepository.UseHub = value;
            }
        }

        public ListingService(ISessionCacheService sessionCacheService, IListingRepository listingRepository,
            IImageRepository imageRepository, IMarketService marketService, IStateService stateService, IApiService apiService)
        {
            _sessionCacheService = sessionCacheService;
            _listingRepository = listingRepository;
            _imageRepository = imageRepository;
            _marketService = marketService;
            _stateService = stateService;
            _apiService = apiService;
        }
        
        public WebApiCommonResultModel<List<T>> GetHomesResultsFromApi<T>(SearchParams searchParams, int partnerId)
        {
            return _apiService.GetResultsWebApi<T>(searchParams, SearchResultsPageType.HomeResults);
        }

        public IPlan GetPlan(int planId)
        {
            return _listingRepository.Plans.GetById(planId);
        }

        public ISpec GetSpec(int specId)
        {
            return _listingRepository.Specs.GetById(specId);
        }

        public IList<ISpec> GetSpecsForSpecIds(IList<int> specIds, bool includeImages)
        {
            IList<int?> ids = specIds.Select(specId => (int?)specId).ToList();
            return includeImages 
                ? _listingRepository.SpecsWithImages.Where(s => ids.Contains(s.SpecId)).ToList() 
                // ? _listingRepository.SpecsWithImages.WhereIn(s => s.SpecId, ids).ToList() 
                :  _listingRepository.Specs.WhereIn(s => s.SpecId, ids).ToList();
        }

        public IList<IPlan> GetPlansForPlanIds(IList<int> planIds, bool includeImages)
        {
            if (planIds.Any() == false)
            {
                return new List<IPlan>(0);
            }

            var ids = planIds.Select(planId => (int?)planId);

            var query = includeImages
                ? _listingRepository.PlansWithImages.WhereIn(p => p.PlanId, ids)
                : _listingRepository.Plans.WhereIn(p => p.PlanId, ids);

            return query.ToList();
        }

       
        public IList<IImage> GetFloorPlanImagesForPlan(int planId)
        {
            return this.GetPlan(planId).PlanImages.AsQueryable().GetByType(ImageTypes.FloorPlan).OrderBy(i => i.ImageSequence).ToList();
        }

        public IList<IImage> GetFloorPlanImagesForSpec(IPlan plan, ISpec spec)
        {
            if (spec != null)
            {
                var specImages = spec.SpecImages.AsQueryable().GetByType(ImageTypes.FloorPlan).OrderBy(i => i.ImageSequence).ToList();
                return specImages.Count > 0
                           ? specImages
                           : plan.PlanImages.AsQueryable().GetByType(ImageTypes.FloorPlan).OrderBy(i => i.ImageSequence).ToList(); //fallback on plan images
            }

            return plan.PlanImages.AsQueryable().GetByType(ImageTypes.FloorPlan).OrderBy(i => i.ImageSequence).ToList();
        }

        public int GetFloorPlanImageCount(IPlan plan, ISpec spec)
        {
            var imageCount = 0;
            if (spec != null)
            {
                imageCount = spec.SpecImages.AsQueryable().GetByType(ImageTypes.FloorPlan).Count();

                if (imageCount > 0) //else fallback on plan images below
                    return imageCount;
            }

            if (plan != null)
                imageCount = plan.PlanImages.AsQueryable().GetByType(ImageTypes.FloorPlan).Count();

            return imageCount;
        }

        public IEnumerable<IImage> GetHomeImagesForPlan(IPlan plan)
        {
            return plan.PlanImages.Where(
                image => image.ImageTypeCode == ImageTypes.Elevation || image.ImageTypeCode == ImageTypes.Interior).
                OrderBy(image => image.ImageTypeCode)
                .ThenBy(image => image.ImageSequence);
        }

        public IEnumerable<IImage> GetHomeImagesForPlan(int planId)
        {
            return GetPlan(planId)
                                .PlanImages
                                .Where(image => image.ImageTypeCode == ImageTypes.Elevation || image.ImageTypeCode == ImageTypes.Interior)
                                .OrderBy(image => image.ImageTypeCode)
                                .ThenBy(image => image.ImageSequence);
        }

        public IEnumerable<IImage> GetHomeImagesForSpec(int planId, int specId)
        {
            if (specId > 0)
            {
                var specImages = GetSpec(specId)
                                .SpecImages
                                .Where(image => image.ImageTypeCode == ImageTypes.Elevation || image.ImageTypeCode == ImageTypes.Interior)
                                .OrderBy(image => image.ImageTypeCode)
                                .ThenBy(image => image.ImageSequence);

                return specImages.Any()
                           ? specImages
                           : planId > 0 ? GetHomeImagesForPlan(planId) : new List<IImage>(); //fallback on plan images
            }

            return GetHomeImagesForPlan(planId);
        }

        public IEnumerable<IImage> GetHomeImagesForSpec(IPlan plan, ISpec spec)
        {
            if (spec == null) return GetHomeImagesForPlan(plan);
            
            var specImages = spec
                .SpecImages
                .Where(image => image.ImageTypeCode == ImageTypes.Elevation || image.ImageTypeCode == ImageTypes.Interior)
                .OrderBy(image => image.ImageTypeCode)
                .ThenBy(image => image.ImageSequence);

            return specImages.Any()
                ? specImages
                : GetHomeImagesForPlan(plan); //fallback on plan images
        }


        public IList<HomeOption> GetHomeOptionsForPlan(IPlan plan, int partnerId)
        {
            IList<HomeOption> options = new List<HomeOption>();
            //var plan = this.GetPlan(planId);

            if (plan != null && ((_useHub && plan.HubCommunity != null) || plan.Community != null))
            {
                int builderId = _useHub ? plan.HubCommunity.BuilderId : plan.Community.BuilderId;
                DataTable dt = DataProvider.Current.GetHomeOptionsForSearchOption(plan.PlanId, builderId,
                                                                                  plan.CommunityId, partnerId, _useHub);

                foreach (DataRow row in dt.Rows)
                {
                    var option = new HomeOption();
                    option.OptionId = DBValue.GetInt(row["option_id"]);
                    option.OptionName = DBValue.GetString(row["plan_option_description"]);
                    option.Description = DBValue.GetString(row["comp_desc"]);
                    option.Living = DBValue.GetInt(row["living"]);
                    option.Price = DBValue.GetDouble(row["price"]);
                    option.Beds = DBValue.GetInt(row["beds"]);
                    option.Garages = DBValue.GetInt(row["garage"]);
                    option.Baths = DBValue.GetInt(row["baths"]);
                    option.SquareFootage = DBValue.GetInt(row["square_feet"]);
                    options.Add(option);
                }
            }
            return options;
        }

        public IList<NearbyHome> GetNearbyHomes(int partnerId, int communityId, int builderId, decimal basePrice, int bedrooms, int baths)
        {
            var dt = DataProvider.Current.GetNearbyCommunitiesByPlanFeatures(builderId, communityId, (double)basePrice, bedrooms, baths, partnerId);
            var homes = new List<NearbyHome>();

            if (dt != null)
            {
                homes.AddRange(from DataRow dr in dt.Rows
                               select new NearbyHome
                                          {
                                              CommunityName = DBValue.GetString(dr["community_name"]),
                                              CommunityId = DBValue.GetInt(dr["community_id"]),
                                              PlanId = DBValue.GetInt(dr["plan_id"]),
                                              PlanName = DBValue.GetString(dr["plan_name"]),
                                              BuilderId = DBValue.GetInt(dr["builder_id"]),
                                              ImageThumbnail = DBValue.GetString(dr["image_thumbnail"]),
                                              Price = DBValue.GetDecimal(dr["price"]),
                                              DistanceFromCommunity = DBValue.GetDecimal(dr["distance_from_community"]),
                                              HasVideo = DBValue.GetString(dr["subvideo_flag"]).ToBool(),
                                              BathroomCount = DBValue.GetInt(dr["bathroom_count"]),
                                              BedroomCount = DBValue.GetInt(dr["bedroom_count"]),
                                              BcType = DBValue.GetString(dr["image_thumbnail"]),
                                              SquareFeet = DBValue.GetInt(dr["square_feet"])
                                          });
            }
            return homes;
        }

        public string GetFloorPlanViewerUrl(IPlan plan, ISpec spec)
        {
            IImage image = null;
            //TODO: Remove this crapy if-else-if logic
            if (spec != null)
            {
                if (_useHub)
                {
                    if (spec.HubPlan.AllPlanHubImages == null)
                        return string.Empty;

                    image = spec.HubPlan.AllPlanHubImages.AsQueryable().GetByType(ImageTypes.EBrochureUrl).FirstOrDefault();
                    //no plan viewer at spec level, always use plan
                }
                else
                {
                    if (spec.Plan.AllPlanImages == null)
                        return string.Empty;
                    image = spec.Plan.AllPlanImages.AsQueryable().GetByType(ImageTypes.EBrochureUrl).FirstOrDefault();
                    //no plan viewer at spec level, always use plan
                }
            }
            else
            {
                if (_useHub)
                {
                    if (plan.AllPlanHubImages == null)
                        return string.Empty;

                    if (plan != null)
                        image = plan.AllPlanHubImages.AsQueryable().GetByType(ImageTypes.EBrochureUrl).FirstOrDefault();
                }
                else
                {
                    if (plan.AllPlanImages == null)
                        return string.Empty;

                    if (plan != null)
                        image = plan.AllPlanImages.AsQueryable().GetByType(ImageTypes.EBrochureUrl).FirstOrDefault();
                }
            }

            return image != null ? image.OriginalPath : string.Empty;
        }


        public IList<IImage> GetHomeImagesForSpecs(IList<int> specIds)
        {
            var specImages = new List<IImage>();

            if (!specIds.Any())
                return specImages;

            IList<int?> ids = specIds.Select(specId => (int?)specId).ToList();

            specImages.AddRange(_imageRepository.Images.WhereIn(p => p.SpecificationId, ids).ToList());
            return specImages;
        }

        public IList<IImage> GetHomeImagesForPlans(IList<int> planIds)
        {
            var planImages = new List<IImage>();
            if (!planIds.Any())
                return planImages;

            IList<int?> ids = planIds.Select(planId => (int?)planId).ToList();
            var images = (from i in _imageRepository.Images
                          where i.SpecificationId == null
                          select i).AsQueryable();

            
            planImages.AddRange(images.WhereIn(p => p.PlanId, ids).ToList());
            return planImages;
        }

        public string GetInteriorImageWhenElevationAbsent(string imageThumbnail, int planId, int specId)
        {
            if (!string.IsNullOrEmpty(imageThumbnail) && !imageThumbnail.Equals("N"))
                return imageThumbnail;

            var image = GetHomeImagesForSpec(planId, specId).FirstOrDefault();
            return (image != null && !string.IsNullOrEmpty(image.ImagePath) && !string.IsNullOrEmpty(image.ImageName)) ?
                string.Format("{0}/{1}_{2}", image.ImagePath, ImageSizes.HomeThumb, image.ImageName.Replace(Path.GetExtension(image.ImageName) ?? string.Empty, ".jpg")) :
                "N";
        }

        public List<MediaPlayerObject> GetMediaPlayerObjects(IPlan plan, ISpec spec, bool addVideo = true, bool useTitleAndCaption = false, string size = ImageSizes.HomeMain)
        {
            const StringComparison igCase = StringComparison.InvariantCultureIgnoreCase;
            List<MediaPlayerObject> mediaPlayerObjects;

            if (spec != null)
            {
                mediaPlayerObjects = GetHomeImagesForSpec(plan, spec).Where(p => p.ImageName.IsValidImage()).Select(image => new MediaPlayerObject
                {
                    Type = MediaPlayerObjectTypes.Image,
                    SubType = image.ImageTypeCode.Equals(ImageTypes.Interior, igCase) ? MediaPlayerObjectTypes.SubTypes.HomeInterior : MediaPlayerObjectTypes.SubTypes.ElevationImages,
                    Title = useTitleAndCaption ? image.ImageTitle : string.Concat(image.ImageTitle, (!String.IsNullOrEmpty(image.ImageTitle) && !string.IsNullOrEmpty(image.ImageDescription)) ? ": " : string.Empty, image.ImageDescription),
                    Thumbnail = string.Format("{0}/{1}?{2}", image.ImagePath, image.ImageName, ImageSizes.GetResizeCommands(ImageSizes.HomeThumb)),
                    Caption = useTitleAndCaption ? image.ImageDescription : image.ImageTitle,
                    Url = string.Format("{0}/{1}?{2}", image.ImagePath, image.ImageName, ImageSizes.GetResizeCommands(size))

                }).ToList();

                if (addVideo)
                    mediaPlayerObjects.AddRange(spec.SpecVideos
                                .OrderBy(video => video.Order)
                                .Select(video => new MediaPlayerObject
                                {
                                    Type = MediaPlayerObjectTypes.Video,
                                    SubType = MediaPlayerObjectTypes.SubTypes.BrightcoveVideo,
                                    VideoID = string.Format("sid-{0}-{1}-{2}", spec.SpecId, video.VideoId, Configuration.BrightcoveEnvSuffix),
                                    Title = video.Title,
                                    RefID = video.VideoId,
                                    Url = video.VideoURL,
                                    Sort = video.Order
                                }));

            }
            else
            {
                mediaPlayerObjects = GetHomeImagesForPlan(plan).Where(p => p.ImageName.IsValidImage()).Select(image => new MediaPlayerObject
                {
                    Type = MediaPlayerObjectTypes.Image,
                    SubType = image.ImageTypeCode.Equals(ImageTypes.Interior, igCase) ? MediaPlayerObjectTypes.SubTypes.HomeInterior : MediaPlayerObjectTypes.SubTypes.ElevationImages,
                    Title = useTitleAndCaption ? image.ImageTitle : string.Concat(image.ImageTitle, (!String.IsNullOrEmpty(image.ImageTitle) && !string.IsNullOrEmpty(image.ImageDescription)) ? " : " : string.Empty, image.ImageDescription),
                    Thumbnail = string.Format("{0}/{1}?{2}", image.ImagePath, image.ImageName, ImageSizes.GetResizeCommands(ImageSizes.HomeThumb)),
                    Caption = useTitleAndCaption ? image.ImageDescription : image.ImageTitle,
                    Url = string.Format("{0}/{1}?{2}", image.ImagePath, image.ImageName, ImageSizes.GetResizeCommands(size))
                }).ToList();

                if (addVideo)
                    mediaPlayerObjects.AddRange(plan
                                .PlanVideos
                                .OrderBy(video => video.Order)
                                .Select(video => new MediaPlayerObject()
                                {
                                    Type = MediaPlayerObjectTypes.Video,
                                    SubType = MediaPlayerObjectTypes.SubTypes.BrightcoveVideo,
                                    VideoID = string.Format("pid-{0}-{1}-{2}", plan.PlanId, video.VideoId, Configuration.BrightcoveEnvSuffix),
                                    Title = video.Title,
                                    RefID = video.VideoId,
                                    Url = video.VideoURL,
                                    Sort = video.Order
                                }));
            }

            //Add Vimeo video list from channel
            mediaPlayerObjects.Any(m =>
            {
                if (UrlHelper.IsVimeoChannel((m.Url)))
                {
                    var channelList = VimeoAPI.GetVideoListByChannelId(UrlHelper.GetVimeoChannelId(m.Url));
                    mediaPlayerObjects.AddRange(channelList.Select(video => new MediaPlayerObject()
                    {
                        Type = MediaPlayerObjectTypes.Video,
                        SubType = MediaPlayerObjectTypes.SubTypes.Vimeo,
                        Title = video.title,
                        Thumbnail = video.thumbnail_large,
                        Url = video.url,
                        IsVimeoVideo = true
                    }));
                    return true;
                }
                return false;
            });

            mediaPlayerObjects.ForEach(m =>
            {
                if (!string.IsNullOrEmpty(m.Url) && m.Type == "v")
                {
                    m.IsYouTubeVideo = UrlHelper.IsYoutubeVideo(m.Url);
                    m.IsVimeoVideo = UrlHelper.IsVimeoVideo(m.Url);

                    if (m.IsYouTubeVideo)
                    {
                        m.SubType = MediaPlayerObjectTypes.SubTypes.YoutubeVideo;
                        var youtubeVideoId = UrlHelper.GetYoutubeVideoId(m.Url);
                        m.Thumbnail = string.Format("http://img.youtube.com/vi/{0}/hqdefault.jpg", youtubeVideoId);
                        m.Url = string.Format("http://img.youtube.com/vi/{0}/0.jpg", youtubeVideoId);
                        m.VideoID = youtubeVideoId;
                        m.OnlineVideoID = m.VideoID;
                    }
                    else if (m.IsVimeoVideo)
                    {
                        m.SubType = MediaPlayerObjectTypes.SubTypes.Vimeo;
                        var vimeoId = UrlHelper.GetVimeoVideoId(m.Url);

                        var vimeoVideo = VimeoAPI.GetVideobyId(vimeoId);

                        if (vimeoVideo != null)
                            m.Thumbnail = vimeoVideo.thumbnail_medium;

                        m.VideoID = vimeoId;
                        m.OnlineVideoID = m.VideoID;
                    }
                }
            });

            return mediaPlayerObjects;
        }
    }
}
