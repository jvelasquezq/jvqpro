﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Proxy;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Domain.Model.CMS;
using Nhs.Utility.Constants;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class CmsService : ICmsService
    {
        private readonly IWebCacheService _webCacheService;
        private readonly ILookupService _lookupService;

        [Obsolete("Please use CMSService(IWebCacheService webCacheService, ILookupService lookupService)")]
        public CmsService(IWebCacheService webCacheService)
        {
            _webCacheService = webCacheService;
            //   _lookupService = lookupService;
        }

        public CmsService(IWebCacheService webCacheService, ILookupService lookupService)
        {
            _webCacheService = webCacheService;
            _lookupService = lookupService;
        }

        #region Public Methods

        public CmsProSearch FillSearchViewModel(string searchText, string selectedPriceLo,
                                        string selectedPriceHi,int brandPartnerId, string selectedBathRoom = "",
                                        string selectedBedRoom = "")
        {

            var model = new CmsProSearch
                {
                    SearchText = searchText,
                    PriceLoRange = new NhsCmsSelectList(_lookupService.GetCommonListItems(CommonListItem.MinPrice ),
                                                  "LookupValue", "LookupText", selectedPriceLo),
                    PriceHiRange = new NhsCmsSelectList(_lookupService.GetCommonListItems(CommonListItem.MaxPrice),
                                                  "LookupValue", "LookupText", selectedPriceHi),
                };

            //All partners

            //NHS Pro only
            if (brandPartnerId == PartnersConst.Pro.ToType<int>())
            {
                model.BathList = new NhsCmsSelectList(_lookupService.GetCommonListItems(CommonListItem.Bathrooms),
                                                    "LookupValue", "LookupText", selectedBathRoom);
                model.BedRoomsList = new NhsCmsSelectList(_lookupService.GetCommonListItems(CommonListItem.Bedrooms),
                                                        "LookupValue", "LookupText", selectedBedRoom);

                model.SqFtList = new NhsCmsSelectList(_lookupService.GetSquareFeetListItems(), "LookupValue", "LookupText",
                                                    "");
            }

            return model;
        }


        public IList<CategoryPosition> GetCategoryPositionData(DateTime targetDate, int siteId, int categoryId)
        {
            return GetCategoryPositionDataFromDB(targetDate, siteId, categoryId);
        }

        private IList<CategoryPosition> GetCategoryPositionDataFromDB(DateTime targetDate, int siteId, int categoryId)
        {
            var tbl = DataProvider.Current.GetCategoryPositionData(targetDate, siteId, categoryId);

            return (tbl.Rows.Cast<DataRow>().Select(row => new CategoryPosition
                {
                    ArticleId = DBValue.GetInt(row["target_article_id"]),
                    ArticleTitle = DBValue.GetString(row["target_article_title"]),
                    Headline = DBValue.GetString(row["headline"]),
                    PositionNumber = DBValue.GetInt(row["position_number"]),
                    ShortTeaser = DBValue.GetString(row["short_teaser_html"]),
                    TeaserB = DBValue.GetString(row["teaser_b_html"])
                }).Where(cp => cp.ArticleId > 0)).ToList();
        }

        public IEnumerable<RcArticle> GetArticlesByCategory(string categoryName, long taxonomyId, int languageId)
        {
            var cacheKey = WebCacheConst.WebCacheKeyPrefix + "_EktronCategory_" + categoryName + taxonomyId + languageId;
            var dtArticles = _webCacheService.Get(cacheKey, false, new TimeSpan(0, 0, WebCacheConst.DefaultSeconds),
                () => DataProvider.Current.GetArticlesByCategory(categoryName, taxonomyId, languageId));

            //var dtArticles = DataProvider.Current.GetArticlesByCategory(categoryName, Configuration.EktronLanguageId);
            return dtArticles.Rows.Cast<DataRow>().Select(row =>
                CmsReader.ParseArticle(
                    DBValue.GetString(row["content_html"]),
                    DBValue.GetString(row["content_title"]),
                    DBValue.GetString(row["content_teaser"])))
                    .Where(article => article != null).ToList();
        }

        public IEnumerable<RcArticle> GetArticleResultsByKeywords(string keyWord1, string keyWord2, string keyWord3,
            string keyWord4, long taxonomyId, int languageId)
        {
            var dtArticles = SearchArticlesUnderTaxonomy(keyWord1, keyWord2, keyWord3, keyWord4, taxonomyId, languageId);
            dtArticles.Columns.Add("SearchRank", typeof(int));

            // Loop through each row and add the rank. 
            foreach (DataRow row in dtArticles.Rows)
            {
                row["SearchRank"] = GetRowRank(row, keyWord1) + GetRowRank(row, keyWord2) + GetRowRank(row, keyWord3) + GetRowRank(row, keyWord4);
            }

            var articles = dtArticles.Rows.Cast<DataRow>().Select(row => CmsReader.ParseArticle(DBValue.GetString(row["content_html"]),
                DBValue.GetString(row["content_title"]), DBValue.GetString(row["content_teaser"]), DBValue.GetInt(row["SearchRank"])));
            return articles.OrderByDescending(x => x.SearchRank);
        }

        private static DataTable SearchArticlesUnderTaxonomy(string keyWord1, string keyWord2, string keyWord3, string keyWord4,
                                                             long taxonomyId, int languageId)
        {
            if (string.IsNullOrWhiteSpace(keyWord2))
            {
                keyWord2 = "#NULL#";
            }

            if (string.IsNullOrWhiteSpace(keyWord3))
            {
                keyWord3 = "#NULL#";
            }

            if (string.IsNullOrWhiteSpace(keyWord4))
            {
                keyWord4 = "#NULL#";
            }

            var dtArticles = DataProvider.Current.FrontEndArticleSearchEktron(keyWord1, keyWord2, keyWord3, keyWord4, taxonomyId,
                                                                              languageId);
            return dtArticles;
        }

        public IEnumerable<RCCategory> GetCategoriesByArticleId(long articleId, string categoryName)
        {
            var dtcategories = DataProvider.Current.GetCategoriesByArticleId(articleId, categoryName, Configuration.EktronLanguageId);
            var categories = dtcategories.Rows.Cast<DataRow>().Select(row => new RCCategory() { CategoryName = DBValue.GetString(row["taxonomy_name"]), CategoryId = DBValue.GetDouble(row["taxonomy_id"]) });
            return categories;
        }


        public IEnumerable<RCCategory> GetParentCategoriesByCategoryName(string categoryName)
        {
            var dtcategories = DataProvider.Current.GetParentCategoriesByCategoryName(categoryName, Configuration.EktronLanguageId, Configuration.EktronResourceCenterTaxonomyId);
            var categories = dtcategories.Rows.Cast<DataRow>().Select(row => new RCCategory() { CategoryName = DBValue.GetString(row["taxonomy_name"]), CategoryId = DBValue.GetDouble(row["taxonomy_id"]) });
            return categories;
        }

        /// <summary>
        /// Gives all Categories(Taxonomy) under Resource Center 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<RCCategory> GetCategories(int brandPartnerId)
        {
            var cacheKey = WebCacheConst.WebCacheKeyPrefix + WebCacheConst.CMS_AllCategories + "_" + brandPartnerId;
            var tcats = _webCacheService.Get(cacheKey, false, new TimeSpan(0, 0, WebCacheConst.DefaultSeconds), () => GetSubCategories(Configuration.EktronResourceCenterTaxonomyId));

            return tcats.Select(tcat => new RCCategory
            {
                CategoryName = tcat.TaxonomyName,
                //CategoryDescription = tcat.TaxonomyDescription,
                CategoryHtml = GetCategoryHtml(tcat.TaxonomyName, brandPartnerId)
            }).ToList();
        }

        private string GetCategoryHtml(string taxonomyName, int brandPartnerId)
        {
            long articleId;
            const string articleFormat = "PRO {0} HTML";
            var categoryHtml = string.Format(GetArticleByTitle(string.Format(articleFormat, taxonomyName), out articleId),
                NhsUrlHelper.BuildResourceCenterUrl(string.Empty));
            return categoryHtml;
        }

        public IEnumerable<RCCategory> GetCategoriesHierarchy(int brandPartnerId)
        {
            var cats = GetCategories(brandPartnerId).ToList();

            foreach (var cat in cats)
            {
                cat.SubCategories = GetSubCategories(cat.CategoryName).Select(c => new RCCategory() { CategoryName = c.TaxonomyName, CategoryId = (int)c.TaxonomyId }).ToList();
            }

            return cats;
        }


        public RCCategory GetCategory(string categoryName, int brandPartnerId)
        {
            return GetCategories(brandPartnerId).AsQueryable().FirstOrDefault(c => c.CategoryName == categoryName);
        }

        public IEnumerable<TaxonomyData> GetSubCategories(long taxonomyId)
        {
            var ektronService = new EktronService();
            var tr = new TaxonomyRequest { TaxonomyId = taxonomyId, TaxonomyLanguage = Configuration.EktronLanguageId };
            var taxonomy = ektronService.LoadTaxonomy(ref tr);
            if (taxonomy != null)
                return taxonomy.Taxonomy;

            return new List<TaxonomyData>();
        }

        public long GetCategoryId(string categoryName)
        {
            var ektronService = new EktronService();
            var taxonomyLanguage = Configuration.EktronLanguageId;
            var taxonomyPath = string.Format(@"\Resource Center\{0}", categoryName);
            var taxonomyId = ektronService.GetTaxonomyIdByPath(taxonomyPath, 0, taxonomyLanguage);

            return taxonomyId > 0 ? taxonomyId : -1;
        }

        public IEnumerable<TaxonomyData> GetSubCategories(string categoryName)
        {
            var cacheKey = WebCacheConst.WebCacheKeyPrefix + "CMSSubCategories_" + categoryName;
            var categoryId = GetCategoryId(categoryName);
            var subCategories = _webCacheService.Get(cacheKey, false, new TimeSpan(0, 0, WebCacheConst.DefaultSeconds), () => GetSubCategories(categoryId));
            return subCategories ?? new List<TaxonomyData>();
        }

        public RcArticle GetArticle(string articleTitle)
        {
            return CmsReader.ParseArticle(GetArticleByTitle(articleTitle));
        }

        public IEnumerable<RcArticle> GetRelatedArticlesByAuthorName(string authorName, long taxonomyId, int languageId)
        {
            var articles = DataProvider.Current.GetContentByAuthorName(authorName, languageId, taxonomyId);

            var ids = (from row in articles.AsEnumerable()
                       select GetRcArticleById(row.Field<long>("content_id"), taxonomyId, languageId)).Where(id => id.Author != null).ToList();

            return ids;
        }

        public IEnumerable<RcAuthor> GetAllAuthorsInfo(long taxonomyId, int languageId)
        {
            var cacheKey = WebCacheConst.WebCacheKeyPrefix + "All_CMS_Authors_" + taxonomyId + "_" + languageId;
            var authorsData = _webCacheService.Get(cacheKey, false, new TimeSpan(0, 0, WebCacheConst.DefaultSeconds), () => DataProvider.Current.GetAllAuthors(taxonomyId, languageId));

            var authors = (from row in authorsData.AsEnumerable()
                           select CmsReader.ParseAuthor(row.Field<string>("content_html"), row.Field<long>("content_id"), row.Field<string>("content_title"))).OrderBy(o => o.Name).ToList();

            return authors.Where(w => w.Active);
        }

        public RcAuthorLandingPage GetAuthorsLandingPage(long taxonomyId, int languageId)
        {
            var cacheKey = WebCacheConst.WebCacheKeyPrefix + "AuthorsLandingPage" + taxonomyId + "_" + languageId;
            var landingData = _webCacheService.Get(cacheKey, false, new TimeSpan(0, 0, WebCacheConst.DefaultSeconds),
                () => DataProvider.Current.GetAuthorsLandingPage(taxonomyId, languageId));

            if (landingData.Rows.Count == 0)
                return new RcAuthorLandingPage();

            var landingPageInfo = (from row in landingData.AsEnumerable()
                                   select CmsReader.ParseAuthorLandingPage(row.Field<string>("content_html"))).FirstOrDefault();

            return landingPageInfo;
        }

        public string GetArticleByTitle(string articleTitle, out long articleId)
        {
            var dtArticles = DataProvider.Current.GetContentByContentTitleAndTaxonomy(articleTitle,
                Configuration.EktronResourceCenterTaxonomyId, Configuration.EktronLanguageId);

            articleId = 0;

            if (dtArticles.Rows.Count > 0)
                articleId = dtArticles.Rows[0]["content_id"].ToType<long>();

            return dtArticles.Rows.Count > 0 ? dtArticles.Rows[0]["content_html"].ToType<string>() : string.Empty;
        }

        public string GetArticleByTitle(string articleTitle)
        {
            long id = 0;
            return GetArticleByTitle(articleTitle, out id);
        }

        public ContentBase GetArticleById(long articleId)
        {
            var ektronService = new EktronService();
            var contentBase = ektronService.GetContentByID(articleId, true);
            return contentBase ?? new ContentBase();
        }

        public RcArticle GetRcArticleById(long articleId, long taxonomyId, int languageId)
        {
            var dtArticles = DataProvider.Current.GetContentInfo(articleId, taxonomyId, languageId);

            return dtArticles.ParceArticleWithTitleAndTeaser();
        }

        public string GetArticleById(long articleId, long taxonomyId, int languageId)
        {
            var dtArticles = DataProvider.Current.GetContentInfo(articleId, taxonomyId, languageId);
            var article = dtArticles.Rows.Count > 0 ? dtArticles.Rows[0]["content_html"].ToType<string>() : string.Empty;
            return article;
        }

        /// <summary>
        /// The path must have the Format with the "\" so if you want to get the content
        /// from a folder in Ektron under Home -> NewHomeSource -> Configuration
        /// you must use "\NewHomeSource\Configuration"        
        /// </summary>
        /// <param name="path">Path in Ektron folder estructure</param>     
        /// <param name="loadArticleHtml">If true call to get article by id and load the HTML content of the article</param>       
        /// <param name="languageId"></param> 
        /// <param name="taxonomyId"></param> 
        public IList<RcContentInfo> GetContentInfoByFolderName(string path, long taxonomyId, int languageId, bool loadArticleHtml = true)
        {
            var content = GetXmlFolderContent(path);

            var articleInfoList = CmsReader.ParseFolderArticleIds(content);

            if (loadArticleHtml)
            {
                foreach (var rcContentInfo in articleInfoList.Where(rcContentInfo => rcContentInfo.Id > 0))
                {
                    rcContentInfo.ArticleHtml = GetArticleById(rcContentInfo.Id).Html;
                }
            }

            return articleInfoList;
        }

        public RcContentInfo GetConfiguration(string articleName)
        {
            long articleId;        
            var content = GetArticleByTitle(articleName, out articleId);

            var articleInfo = CmsReader.ParseFolderArticleIdByArticleName(content, articleName);

            if (articleInfo != null && articleInfo.Id > 0)
            {
                articleInfo.ArticleHtml = GetArticleById(articleInfo.Id).Html;
            }

            return articleInfo ?? new RcContentInfo();
        }

        /// <summary>
        /// The path must have the Format with the "\" so if you want to get the content
        /// from a folder in Ektron under Home -> NewHomeSource -> Configuration
        /// you must use "\NewHomeSource\Configuration"        
        /// </summary>
        /// <param name="path">Path in Ektron folder estructure</param>       
        /// <param name="articleName">Article Name in Ektron folder estructure</param> 
        /// <param name="taxonomyId"></param>  
        /// <param name="languageId"></param>     
        public RcContentInfo GetContentInfoByFolderNameAndArticleName(string path, string articleName, long taxonomyId, int languageId)
        {
            var content = GetXmlFolderContent(path);
     
            var articleInfo = CmsReader.ParseFolderArticleIdByArticleName(content, articleName);

            if (articleInfo != null && articleInfo.Id > 0)
            {
                articleInfo.ArticleHtml = GetArticleById(articleInfo.Id).Html;
            }

            return articleInfo ?? new RcContentInfo();
        }

        public RcGlobalBusinessConfig GetBusinessConfiguration(int partnerId, string configArticleName)
        {
            var cacheKey = WebCacheConst.WebCacheKeyPrefix + "EktronConfig_" + configArticleName + "_" + partnerId;
            
            var info = _webCacheService.Get(cacheKey, false, new TimeSpan(0, 0, WebCacheConst.DefaultSeconds), 
                () => GetConfiguration(configArticleName));

            var configData = new RcGlobalBusinessConfig();

            if (string.IsNullOrWhiteSpace(info.ArticleHtml) == false)
            {
                configData = CmsReader.ParseEktronGlobalBusinessConfig(info.ArticleHtml);
            }

            return configData;
        }

        public DataTable GetArticleSearchResultsByKeywords(string keyWord1, string keyWord2, string keyWord3,
            string keyWord4, long taxonomyId, int languageId)
        {
            var searchResults = SearchArticlesUnderTaxonomy(keyWord1, keyWord2, keyWord3, keyWord4, taxonomyId,
                                                            languageId);
            return searchResults;
        }

        //Content titled 'Home Page SlideShow'
        public RCSlideShow GetHomePageSlideShow(int partnerId)
        {
            var page = "Home Page";
            return GetCategorySlideShow(page, partnerId);
        }

        //Content titled '[CategoryName] SlideShow'
        public RCSlideShow GetCategorySlideShow(string categoryName, int partnerId)
        {
            var slideShowXml = GetArticleByTitle( string.Format("PRO {0} SlideShow", categoryName));
            return ParseSlideShowXml(slideShowXml);
        }

        public RCSlideShow ParseSlideShowXml(string xml)
        {
            return !string.IsNullOrEmpty(xml) ? CmsReader.ParseSlideShow(xml) : new RCSlideShow();
        }

        public IEnumerable<RcArticle> GetTrendingArticles(long taxonomyId, int languageId)
        {
            var xml = GetArticleByTitle("Trending Articles");

            var articles = new List<RcArticle>();
            if (!string.IsNullOrEmpty(xml))
            {
                var articleIds = CmsReader.ParseArticleIds(xml);

                articles.AddRange(articleIds.Select(articleId => GetArticleById(articleId).ParseArticle())
                    .Where(article => article != null));
            }

            return articles;
        }

        public IList<MetaTag> GetMetasForCategory(string categoryName, long taxonomyId, int languageId)
        {
            return GetMetasForArticle(string.Format("{0} Meta", categoryName), taxonomyId, languageId);
        }

        public IList<MetaTag> GetMetasForHomePage(long taxonomyId, int languageId)
        {
            return GetMetasForArticle("Home Page Meta", taxonomyId, languageId);
        }

        /// <summary>
        /// Gets Meta Info by Article Name - Can use GetContentMetaData() WS method too but this one saves a DB roundtrip.
        /// </summary>
        /// <param name="articleTitle"></param>
        /// <param name="taxonomyId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        public IList<MetaTag> GetMetasForArticle(string articleTitle, long taxonomyId, int languageId)
        {
            var metas = new List<MetaTag>();
            var dtMetas = DataProvider.Current.GetMetasByContentTitle(articleTitle, taxonomyId, languageId);

            foreach (DataRow drMeta in dtMetas.Rows)
            {
                var meta = new MetaTag();

                var metaName = DBValue.GetString(drMeta["meta_name"]);
                switch (metaName)
                {
                    case "title":
                        meta.Name = MetaName.Title;
                        meta.Content = DBValue.GetString(drMeta["meta_value"]);
                        break;

                    case "keywords":
                        meta.Name = MetaName.Keywords;
                        meta.Content = DBValue.GetString(drMeta["meta_value"]);
                        break;

                    case "description":
                        meta.Name = MetaName.Description;
                        meta.Content = DBValue.GetString(drMeta["meta_value"]);
                        break;
                }

                metas.Add(meta);
            }

            return metas;
        }

        /// <summary>
        /// Get multipart article with its section details.
        /// </summary>
        /// <param name="articleTitle">Title of the Article</param>
        /// <param name="taxonomyId">Id of the Base Taxonomy Id</param>
        /// <param name="languageId">Id of the lang ej : en-us = 1033</param>
        /// <returns>Multipart article with container and its section details loaded</returns>
        public RcMpArticleContainer GetMultiPartArticle(string articleTitle, long taxonomyId, int languageId)
        {
            var rcmpa = new RcMpArticleContainer
                {
                    ArticleSections = new List<RcMpArticleSection>()
                };

            var dtRcMpa = DataProvider.Current.GetMultiPartArticleByTitle(articleTitle, taxonomyId, languageId);

            if (dtRcMpa.Rows.Count > 0)
            {
                rcmpa.ArticleID = dtRcMpa.Rows[0].Field<long>("content_id");
                rcmpa.ContentTitle = articleTitle;

                string sectionIDs = string.Empty;
                var xDoc = new XmlDocument();
                xDoc.LoadXml(dtRcMpa.Rows[0].Field<string>("content_html"));

                rcmpa.ArticleMainImage = xDoc.SelectSingleNode("/root/FeaturedImage").InnerXml;
                rcmpa.ArticleShortTitle = xDoc.SelectSingleNode("/root/ArticleSubhead").InnerText;
                rcmpa.ArticleSubTitle = xDoc.SelectSingleNode("/root/ArticleSubtitle").InnerText;
                rcmpa.ArticleSummary = xDoc.SelectSingleNode("/root/ArticleCopy").InnerXml;
                rcmpa.ArticleTitle = xDoc.SelectSingleNode("/root/ArticleTitle").InnerText;

                foreach (XmlNode xNode in xDoc.SelectNodes("/root/ArticleSection"))
                {
                    sectionIDs += xNode.InnerText + ",";
                }

                var dtRcmps = DataProvider.Current.GetMultiPartArticleSectionsById(sectionIDs, taxonomyId, languageId);
                if (dtRcmps.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtRcmps.Rows)
                    {
                        var rcmps = new RcMpArticleSection();
                        rcmps.SectionID = dr.Field<long>("content_id");
                        rcmps.ContentTitle = dr.Field<string>("content_title");
                        var xChildDoc = new XmlDocument();
                        xChildDoc.LoadXml(dr.Field<string>("content_html"));

                        rcmps.SectionTitle = xChildDoc.SelectSingleNode("/root/SectionTitle").InnerText;
                        rcmps.SectionImage = xChildDoc.SelectSingleNode("/root/SectionImage").InnerText;
                        rcmps.SectionSummary = xChildDoc.SelectSingleNode("/root/SectionSummary").InnerText;
                        rcmps.SectionContent = xChildDoc.SelectSingleNode("/root/SectionContent").InnerXml;
                        rcmps.PhotoCaptionAndSource = xChildDoc.SelectSingleNode("/root/PhotoCaptionAndSource").InnerText;

                        rcmpa.ArticleSections.Add(rcmps);
                    }
                }

                var dtRcAuthor = DataProvider.Current.GetArticleAuthorById(long.Parse(xDoc.SelectSingleNode("/root/ArticleAuthor").InnerText), languageId);
                if (dtRcAuthor.Rows.Count > 0)
                {
                    var xAuthorDoc = new XmlDocument();
                    xAuthorDoc.LoadXml(dtRcAuthor.Rows[0].Field<string>("content_html"));

                    var rcAuthor = new RcAuthor();
                    rcAuthor.Name = xAuthorDoc.SelectSingleNode("/root/AuthorName").InnerText;
                    rcAuthor.Bio = xAuthorDoc.SelectSingleNode("/root/AuthorBio").InnerText;

                    rcmpa.ArticleAuthor = rcAuthor;
                }
            }

            if (string.IsNullOrWhiteSpace(rcmpa.ArticleMainImage) == false)
            {
                var xDoc = new XmlDocument();
                xDoc.LoadXml(rcmpa.ArticleMainImage);

                var selectSingleNode = xDoc.SelectSingleNode("/img");
                if (selectSingleNode != null)
                    if (selectSingleNode.Attributes != null)
                        rcmpa.ArticleMainImage = selectSingleNode.Attributes["src"]
                            .Value.Replace("/uploadedImages/",
                             string.Format("{0}uploadedImages/", Configuration.EktronDomain));
            }

            return rcmpa;
        }

        public bool CatagoryExist(string categoryName, int brandPartnerId)
        {
            var existCategory = false;
            if (string.IsNullOrWhiteSpace(categoryName) == false)
            {
                var categories = GetCategories(brandPartnerId);
                existCategory = categories.Any(c => c.CategoryName.ToLower() == categoryName.ToLower());
            }
            return existCategory;
        }

        public RcMpArticleContainer GetMultiPartArticleBySection(string sectionTitle, long taxonomyId, int languageId)
        {
            var rcmpa = new RcMpArticleContainer
                {
                    ArticleSections = new List<RcMpArticleSection>()
                };

            var dtRcMps = DataProvider.Current.GetMultiPartArticleBySectionTitle(sectionTitle, taxonomyId, languageId);

            if (dtRcMps.Rows.Count > 0)
            {
                long sectionId = dtRcMps.Rows[0].Field<long>("content_id");
                var dtRcmpa = DataProvider.Current.GetMultiPartArticleContinerBySection(sectionId, sectionTitle, taxonomyId, languageId);
                if (dtRcmpa.Rows.Count > 0)
                {
                    var articleTitle = dtRcmpa.Rows[0].Field<string>("content_title");
                    rcmpa = GetMultiPartArticle(articleTitle, taxonomyId, languageId);
                }
            }

            return rcmpa;
        }

        public string GetArticleSmartFormType(long articleId)
        {
            return DataProvider.Current.GetArticleSmartFormTypeDesc(articleId, Configuration.EktronLanguageId);
        }

        public bool HasAuthorPageAndIsActive(string authorName, long taxonomyId, int languageId)
        {
            var isValid = false;

            if (string.IsNullOrWhiteSpace(authorName) == false)
            {
                var allAuthors = GetAllAuthorsInfo(taxonomyId, languageId);
                authorName = authorName.Trim().ToLower();
                isValid = allAuthors.Any
                    (
                        author => author.Name.Trim().ToLower() == authorName
                        || author.Headline.ToLower().Contains(authorName)
                    );
            }

            return isValid;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Get all Articles info under a folder, this method just get info and not the HTML of the article
        /// </summary>
        /// <param name="path">Ektron folder Path</param>
        /// <returns></returns>
        private string GetXmlFolderContent(string path)
        {
            var ektronService = new EktronService();
            var folderId = ektronService.GetFolderID(path);

            if (folderId <= 0)
            {
                throw new ArgumentException("path");
            }

            var content = ektronService.GetContentList(Configuration.EktronLanguageId, folderId, "desc",
                                                       new LoginInfo() { });
            return content;
        }

        private int GetRowRank(DataRow row, string keyword)
        {
            var rowRank = 0;
            if (keyword != string.Empty)
            {
                var contentTitle = DBValue.GetString(row["content_title"]);
                var contentTeaser = DBValue.GetString(row["content_title"]);
                var headline = DBValue.GetString(row["content_teaser"]);
                var contentText = DBValue.GetString(row["content_text"]);

                rowRank += GetKeywordCount(contentTitle.ToLower(), keyword.ToLower()) * 2;	// keyword in title counts double.				
                rowRank += GetKeywordCount(contentTeaser.ToLower(), keyword.ToLower());
                rowRank += GetKeywordCount(headline.ToLower(), keyword.ToLower()) * 5;		// keyword in headline counts 5 times.				
                rowRank += GetKeywordCount(contentText.ToLower(), keyword.ToLower());
            }
            return rowRank;
        }

        private int GetKeywordCount(string text, string keyword)
        {
            var keywordCount = 0;
            var startIndex = 0;
            var firstPass = true;

            while (startIndex != -1)
            {
                if (firstPass)
                {
                    startIndex = text.IndexOf(keyword, startIndex, StringComparison.Ordinal);
                    firstPass = false;
                }
                else
                    startIndex = text.IndexOf(keyword, startIndex + keyword.Length, StringComparison.Ordinal);

                if (startIndex != -1)
                    ++keywordCount;
            }
            return keywordCount;
        }

        #endregion
    }
}
