﻿using System.Collections.Generic;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Library.Helpers.WebApiServices;
using Nhs.Library.Proxy;
using Nhs.Mvc.Biz.Services.Abstract;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class ApiService : IApiService
    {
        private readonly IPartnerService _partnerService;

        public ApiService(IPartnerService partnerService)
        {
            _partnerService = partnerService;
        }

        public WebApiCommonResultModel<List<T>> GetResultsWebApi<T>(SearchParams searchParams, SearchResultsPageType srpType)
        {
            var searchType = srpType == SearchResultsPageType.CommunityResults
                                 ? WebApiMethods.Communities
                                 : WebApiMethods.Homes;

            var data = GetDataWebApi<WebApiCommonResultModel<List<T>>>(searchParams, searchType);
            return data;
        }

        public WebApiCommonResultModel<List<T>> GetResultsWebApi<T>(SearchParams searchParams, string searchType)
        {
            var data = GetDataWebApi<WebApiCommonResultModel<List<T>>>(searchParams, searchType);
            return data;
        }

        public T GetDataWebApi<T>(SearchParams searchParams, string searchType) where T : new()
        {
           
            var partner = _partnerService.GetPartner(searchParams.PartnerId);
            if (partner == null) return default(T);
            
            var partnerSitePassword = partner.LeadPostingPassword;
            var webApiServices = new WebApiServices(searchParams.ToWebApiParameters())
            {
                PartnerSitePassword = partnerSitePassword
            };

            var data = webApiServices.GetData<T>(searchType);
            return data;
        }
    }
}
