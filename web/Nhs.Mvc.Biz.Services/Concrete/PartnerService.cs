﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using Bdx.Web.Api.Objects;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Content;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Utility.Common;
using Partner = Nhs.Mvc.Domain.Model.Web.Partner;

namespace Nhs.Mvc.Biz.Services.Concrete
{
    public class PartnerService : IPartnerService
    {
        private IWebCacheService _webCacheService;
        private IPartnerRepository _partnerRepository;
        private IMarketService _marketService;
        private const int MAX_RESULTS = 20;
        protected IPathMapper _pathMapper;

        public PartnerService(IWebCacheService webCacheService, IMarketService marketService, IPartnerRepository partnerRepository, IPathMapper mapper)
        {
            _webCacheService = webCacheService;
            _partnerRepository = partnerRepository;
            _marketService = marketService;
            _pathMapper = mapper;
        }


        public List<SpotLightHomes> GetSpotLightHomes(int partnerId, int marketId)
        {
            var dataTable = DataProvider.Current.GetSpotLightHomes(partnerId, 0, marketId, 0, "n", 25);

            var home = from p in dataTable.AsEnumerable()
                let isSpec = p[SpotLightHomesDBFields.SpecType].ToType<string>() != "P"
                select new SpotLightHomes
                {
                    HomeId =
                        isSpec
                            ? p[SpotLightHomesDBFields.SpecificationId].ToType<int>()
                            : p[SpotLightHomesDBFields.PlanId].ToType<int>(),
                    IsSpec = isSpec.ToType<int>(),
                    PlanName = p[SpotLightHomesDBFields.PlanName].ToType<string>(),
                    City = p[SpotLightHomesDBFields.City].ToType<string>(),
                    State = p[SpotLightHomesDBFields.State].ToType<string>(),
                    Brand = new ApiBrand {Name = p[SpotLightHomesDBFields.BrandName].ToType<string>()},
                    Price = p[SpotLightHomesDBFields.Price].ToType<string>(),
                    Br = p[SpotLightHomesDBFields.Bedrooms].ToType<int>(),
                    Ba = p[SpotLightHomesDBFields.Bathrooms].ToType<int>(),
                    HBa = p[SpotLightHomesDBFields.HalfBathrooms].ToType<int>(),
                    Thumb1 = p[SpotLightHomesDBFields.ImageThumbnail].ToType<string>(),
                    IsHotHome = p[SpotLightHomesDBFields.HotHome].ToType<int>(),
                    Gr = p[SpotLightHomesDBFields.NoCarGarage].ToType<int>(),
                    Sft = p[SpotLightHomesDBFields.SquareFeet].ToType<int>(),
                    CommName = p[SpotLightHomesDBFields.CommunityName].ToType<string>(),
                    Addr = p[SpotLightHomesDBFields.StreetAddress].ToType<string>(),
                    MarketName = p[SpotLightHomesDBFields.MarketName].ToType<string>()
                };
            return home.ToList();
        }

        public Partner GetPartner(int partnerId)
        {
            var cacheKey = WebCacheConst.WebCacheKeyPrefix + WebCacheConst.PartnerInfo + partnerId;

            return _webCacheService.Get(cacheKey, false, () =>
                (from p in _partnerRepository.Partners
                 where p.PartnerId == partnerId
                 select p).FirstOrDefault());
        }

        public Dictionary<string, string> GetPartnerSiteTerms(int partnerId)
        {
            string cacheKey = WebCacheConst.WebCacheKeyPrefix + WebCacheConst.PartnerSiteTermsList + partnerId;
            var dtSiteTerm= _webCacheService.Get(cacheKey, false, () => DataProvider.Current.GetPartnerSiteTerms(partnerId));
            return dtSiteTerm.AsEnumerable().ToDictionary(row => row["term_key"].ToString(), row => row["term_value"].ToString());
        }
         
        public int GetPartnerMaskIndex(int partnerId)
        {
            var cacheKey = WebCacheConst.WebCacheKeyPrefix + WebCacheConst.PartnerMaskIndexList  + (partnerId > 10000 ? "_CNA" : "_NHS");
            var dtMaskIndexes = _webCacheService.Get(cacheKey, false, () => DataProvider.Current.GetPartnerMaskIndexes());

            var maskIndex = (from dr in dtMaskIndexes.AsEnumerable()
                             where dr.Field<int>("partner_id") == partnerId
                             select dr).FirstOrDefault();

            return maskIndex != null ? maskIndex["mask_index"].ToType<int>() : 0;
        }

        public List<Location> GetTypeAheadSuggestions(int partnerId, string searchText, bool includeCommunities, bool locationHandlerSearch = false)
        {
            string cacheKey = WebCacheConst.WebCacheKeyPrefix + WebCacheConst.LocationsIndex + partnerId;
            var marketName = string.Empty;
            string state = null;
            string locationText = string.Empty;
            string locationTextWithAlias = string.Empty;
            var isCommunityName = false;
            var hasAbbrReplacement = false;
            
            var markets = _marketService.GetMarkets(partnerId);

            DataSet ds = _webCacheService.Get(cacheKey, false, () => DataProvider.Current.GetTypeAheadSuggestions(partnerId));
            DataTable dt = ds.Tables[0];

            if (!WebCacheHelper.GetObjectFromCache(WebCacheConst.DeveloperNamesAdded, true).ToType<bool>())
            {
                var developerHelper = new DevelopersReader(_pathMapper);
                var developers = developerHelper.ParseScripts();

                // Filter invalid market names 
                developers = developers.Where(d => markets.Any(m => m.MarketName == d.MarketName && m.StateAbbr.ToLower() == d.State.ToLower())).ToList();

                // insert the developer Names so they are proccessed in the same way the locations are                
                developers.ForEach(d => dt.Rows.Add(new string[] { d.Name, d.State, d.City, markets.Where(m => d.MarketName == m.MarketName && d.State.ToLower() == m.StateAbbr.ToLower()).FirstOrDefault().MarketId.ToString(), ((int)LocationType.Developer).ToString() }));
                WebCacheHelper.AddObjectToCache(true, WebCacheConst.DeveloperNamesAdded, new TimeSpan(WebCacheConst.DefaultHours, 0, 0), true);
            }

            IEnumerable<DataRow> dr = from locations in dt.AsEnumerable()                                      
                                      select locations;

            // Check if search text comes with state name instead abbreviation
            searchText = TypeAheadAbbreviations.ReplaceStateName(searchText.ToLower().Trim()).ToLower();

            locationText = locationTextWithAlias = searchText ;
            marketName = searchText;

            // Separate Location text from State 
            if (searchText.IndexOf(",") != -1)
            {
                marketName = searchText.Substring(0, searchText.IndexOf(",")).Trim();
                locationText = marketName;

                //if (searchText.IndexOf(",") + 2 <= searchText.Length)
                //    state = searchText.Substring(searchText.IndexOf(",") + 2).Trim().ToLower();

                var splitValue = searchText.Split(new char[]{','});
                if (splitValue.Count() > 1)
                {
                    state = splitValue[1].Trim();
                }

            }

            var market = markets.FirstOrDefault(m => m.MarketName == marketName);

            // separate comm name from market name when community search
            if (searchText.IndexOf(" in ") != -1 && market == null) // if have the "in" part that separates coomm names from market and there isnt a market with with the searchText name without state
            {
                marketName = searchText.Substring(searchText.IndexOf(" in ") + 3).Trim();
                
                if (marketName.IndexOf(",") != -1)
                    marketName = marketName.Substring(0, marketName.IndexOf(","));

                locationText = searchText.Substring(0, searchText.LastIndexOf(" in "));
                isCommunityName = true;
            }

            if (CommonUtils.IsNumeric(locationText)) 
            {
                state = null; // zip code search doesnt uses state
            }

            // Check for Abbreviation variations in the location to search
            if (!string.IsNullOrEmpty(locationText) && !isCommunityName)
            {

                var abbrsHelper = new AbbreviationsReader(_pathMapper);
                var abbreviations = abbrsHelper.ParseScripts();
                locationTextWithAlias = TypeAheadAbbreviations.ChangeAbbreviations(locationText, abbreviations);
                hasAbbrReplacement = locationTextWithAlias != locationText;
            }

            List<Location> finalLocations = null;

            IEnumerable<Location> names = from locations in dr
                                          let location = locations.Field<string>("location").ToLowerCase().Replace("''", "'")
                                          where (location.Contains(locationText) || 
                                          (location.Contains(locationTextWithAlias) && locations.Field<int>("type") < (int)LocationType.Community)) && 
                                          (state == null || (state != null && locations.Field<string>("state").ToLower().Equals(state)))
                select new Location()
                {
                    Name = locations.Field<string>("location").Replace("''", "'"), // replace for final display the ' change that was made in the proc so search work ok for comms with the ' caracter
                    Type = locations.Field<int>("type"),
                    State = locations.Field<string>("state"),
                    MarketName = locations.Field<string>("market"),
                    MarketId = locations.Field<int>("marketId"),
                    StartWith = locations.Field<string>("location").StartsWith(locationText, StringComparison.CurrentCultureIgnoreCase)  || locations.Field<string>("location").StartsWith(locationTextWithAlias, StringComparison.CurrentCultureIgnoreCase),
                };
            
            if (isCommunityName)
                names = names.Where(n => n.Type >= (int)LocationType.Community);

            if (locationHandlerSearch)
                finalLocations = names.ToList();
            else // Sort first the ones that start with the text // then sort in this orther market, city, county, zipcode, development names, comms / in the city group order by comms count, other groups by name
                finalLocations = names.Where(l => l.Type != (searchText.Length > 4 ? 0 : (int)LocationType.Community)).
                                    OrderBy(l => l.StartWith ? 1 : 2).
                                    ThenBy(l => (l.Type == (int)LocationType.Community ? 6 : (l.Type == (int)LocationType.Developer ? 5 : l.Type))).
                                    ThenBy(l => l.Name).ToList();

            // Exclude case when the abrreviations replacement is used, remove any location for a single abbreviation search that doesnt match the entire word eg: searching for ft or ft. should match fort worth not Beaufort
            var oneWordSearch = searchText.Split(' ').Length == 1;
            if (oneWordSearch && hasAbbrReplacement)
                finalLocations.RemoveAll(l => l.Name.ToLower().Contains(locationTextWithAlias) && !Regex.IsMatch(l.Name, "\\b" + locationTextWithAlias + "\\b", RegexOptions.IgnoreCase));

            if (includeCommunities) // if communities included limit the final result set
                finalLocations = finalLocations.Take(MAX_RESULTS).ToList();
            
            return finalLocations;
        }

        public string GetPartnerMarketFacebook(int partnerId, int brandPartnerId, int marketId)
        {
            var url = (from pma in _partnerRepository.PartnersMarketFacebookUrls
                       where pma.MarketId == marketId && pma.PartnerId == partnerId
                       select pma.FacebookUrl);

            if(string.IsNullOrEmpty(url.FirstOrDefault()))
            {
                url = (from pma in _partnerRepository.PartnersMarketFacebookUrls
                       where pma.MarketId == marketId && pma.PartnerId == brandPartnerId
                       select pma.FacebookUrl);
            }

            return string.IsNullOrEmpty(url.FirstOrDefault())? null : url.FirstOrDefault();
        }
    }
}
