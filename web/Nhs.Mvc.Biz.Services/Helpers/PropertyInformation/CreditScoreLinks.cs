﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Nhs.Mvc.Biz.Services.Helpers.PropertyInformation
{
    /// <summary>
    /// The credit score links that are displayed in the Community Details and Home details page comes
    /// from an XML file, previouly the credit score links comes from a property in the BHI Config, and there
    /// we had two links, for the ticket 72239 now we are using just one link but we have to keep the old logic in case
    /// the business people needs to use them again, so now we have a property called UseOldCreditScoreLinks which says
    /// if the two old links have to be used or if we need to use the new Credic Score Link
    /// </summary>
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class CreditScoreLinks
    {
        [XmlElement("CreditScoreLink1Old")]
        public String CreditScoreLink1Old { get; set; }

        [XmlElement("CreditScoreLink2Old")]
        public String CreditScoreLink2Old { get; set; }

        [XmlElement("CreditScoreLink")]
        public String CreditScoreLink { get; set; }

        [XmlElement("UseOldCreditScoreLinks")]
        public bool UseOldCreditScoreLinks { get; set; }
    }
}
