﻿using System.Collections.Generic;
using Nhs.Library.Constants;
using Nhs.Mvc.Domain.Model.BHIContent;

namespace Nhs.Mvc.Biz.Services.Helpers.AffiliateLinks
{

    /// <summary>
    /// Case 76677 - Business Control of Affiliate Links on Detail Pages
    /// http://bits.builderhomesite.com/Production/default.asp?76677
    /// 
    /// This class should serve as a parent class to handle the Affiliate Links, here should be added all those properties for news links
    /// that are shared by Comm Details, Home Details, Basic Comm Details and Basic Home Details; to make changes for specific page links
    /// a new class should be created to the specific page, and an override or a new property should be added in that class. 
    /// </summary>
    public abstract class DefaultAffiliateLinks
    {
        /// <summary>
        /// Default properties for the Free Credit Score link
        /// </summary>
        public static AffiliateLink FreeCreditScore
        {
            get
            {
                return new AffiliateLink
                    {
                        Href = @"http://www.freescoreonline.com/EnterCampaign.aspx?id=3227&ord=1",
                        Target = "_blank",
                        Keyword = AffiliateLinksTypes.FreeCreditScore,
                        CssClass = "nhs_CreditScoreLink",
                        OnClick = "jQuery.googlepush('Outbound Links','Community - Gallery','Free Credit Score');",
                        LinkText = "Free Credit Score"
                    };
            }
        }

        /// <summary>
        /// Default properties for the Mortgage Rates link
        /// </summary>
        public static AffiliateLink MortgageRates
        {
            get
            {
                return new AffiliateLink
                    {
                        Href = @"http://info.prospectmortgagedirect.com/preapprovallink/",
                        Keyword = AffiliateLinksTypes.MortgageRates,
                        CssClass = "MortgageRates",
                        Target = "_blank",
                        LinkText = "Mortgage Rates",
                        OnClick = "jQuery.googlepush('Outbound Links','Community - Gallery','Free Credit Score');"
                    };
            }
        }

        /// <summary>
        /// Default properties for the Calculate Mortgage Payments link
        /// </summary>
        public static AffiliateLink CalculateMortgagePayments
        {
            get
            {
                return new AffiliateLink
                    {
                        Href = @"http://info.prospectmortgagedirect.com/preapprovallink/",
                        OnClick = "jQuery.googlepush('Outbound Links','Community - Next Steps 123','Prospect Mortgage');",
                        Keyword = AffiliateLinksTypes.CalculateMortgagePayments,
                        LinkText = "Get Preapproved",
                        CssClass = "nhs_CalculateMortgagePayments"
                    };
            }
        }

        public static List<AffiliateLink> GetDefaultAffiliateLinks()
        {
            return new List<AffiliateLink>
            {
                CalculateMortgagePayments,
                FreeCreditScore,
                MortgageRates
            };
        }
    }
}
