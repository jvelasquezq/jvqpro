﻿using System.Xml.Serialization;

namespace Nhs.Mvc.Biz.Services.Helpers.Tv
{
    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class TvMarketContentCommunityHomeImage
    {
        /// <remarks/>
        [XmlAttribute("path")]
        public string Path { get; set; }

        /// <remarks/>
        [XmlAttribute("caption")]
        public string Caption { get; set; }
    }
}