﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Nhs.Mvc.Biz.Services.Helpers.Tv
{
    [Serializable]
    [XmlRoot("marketmap")]
    public class TvMarkets
    {
        public TvMarkets()
        {
            TvMarketColletion = new List<TvMarket>();
        }

        [XmlElement("market")]
        public List<TvMarket> TvMarketColletion { get; set; }
    }

    [Serializable]
    public class TvMarket
    {
        [XmlAttribute("keyword")]
        public string Keyword { get; set; }

        [XmlAttribute("id")]
        public int MarketId { get; set; }

        [XmlIgnore]
        public TvMarketContent MarketContent { get; set; }
    }

    #region TvMarketContent

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class TvMarketContent
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("community", IsNullable = false)]
        public List<TvMarketContentCommunity> Communities { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("video", IsNullable = false)]
        public List<TvMarketContentVideo> Videos { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("sponsor", IsNullable = false)]
        public List<TvMarketContentSponsor> Sponsors { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("name")]
        public string Name { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("contesturl")]
        public string ContestUrl { get; set; }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class TvMarketContentCommunity
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("home", IsNullable = false)]
        public List<TvMarketContentCommunityHome> Homes { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("id")]
        public string Id { get; set; }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class TvMarketContentCommunityHome
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlElement("image", IsNullable = false)]
        public TvMarketContentCommunityHomeImage Image { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("id")]
        public string Id { get; set; }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class TvMarketContentCommunityHomeImage
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("path")]
        public string Path { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("caption")]
        public string Caption { get; set; }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class TvMarketContentVideo
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("order")]
        public byte Order { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("url")]
        public string url { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("title")]
        public string Title { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("caption")]
        public string Caption { get; set; }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class TvMarketContentSponsor
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("logo")]
        public string Logo { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("url")]
        public string Url { get; set; }
    }

    #endregion

}