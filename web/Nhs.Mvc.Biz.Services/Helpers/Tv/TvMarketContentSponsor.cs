﻿using System.Xml.Serialization;

namespace Nhs.Mvc.Biz.Services.Helpers.Tv
{
    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class TvMarketContentSponsor
    {
        /// <remarks/>
        [XmlAttribute("logo")]
        public string Logo { get; set; }

        /// <remarks/>
        [XmlAttribute("url")]
        public string Url { get; set; }
    }
}