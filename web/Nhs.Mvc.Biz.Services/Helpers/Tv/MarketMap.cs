﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Nhs.Mvc.Biz.Services.Helpers.Tv
{
    [Serializable]
    [XmlRoot("marketmap")]
    public class MarketMap
    {
        public MarketMap()
        {
            TvMarkets = new List<TvMarket>();
        }

        [XmlElement("market")]
        public List<TvMarket> TvMarkets { get; set; }
    }

    [Serializable]
    public class TvMarket
    {
        [XmlAttribute("keyword")]
        public string Keyword { get; set; }

        [XmlAttribute("id")]
        public int MarketId { get; set; }

        [XmlAttribute("image")]
        public string Image { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }
        
    }
}