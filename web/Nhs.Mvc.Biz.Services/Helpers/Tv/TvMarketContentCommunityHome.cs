﻿using System.Xml.Serialization;

namespace Nhs.Mvc.Biz.Services.Helpers.Tv
{
    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class TvMarketContentCommunityHome
    {
        /// <remarks/>
        [XmlElement("image", IsNullable = false)]
        public TvMarketContentCommunityHomeImage Image { get; set; }

        /// <remarks/>
        [XmlAttribute("id")]
        public int Id { get; set; }

        [XmlIgnore]
        public string CommunityName { get; set; }

        [XmlIgnore]
        public string PlanName { get; set; }

        [XmlIgnore]
        public string BuilderLogoUrl { get; set; }

        [XmlIgnore]
        public string BrandName { get; set; }

        [XmlIgnore]
        public string PlanPrice { get; set; }

        [XmlIgnore]
        public string CityStateZip { get; set; }

        [XmlIgnore]
        public string BrandSiteUrl { get; set; }

        [XmlIgnore]
        public int BuilderId { get; set; }

        [XmlIgnore]
        public int CommunityId { get; set; }
    }
}