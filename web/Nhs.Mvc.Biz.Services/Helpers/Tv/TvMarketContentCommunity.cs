﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Nhs.Mvc.Biz.Services.Helpers.Tv
{
    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class TvMarketContentCommunity : TvMarketCommunity
    {
        /// <remarks/>
        [XmlArrayItem("home", IsNullable = false)]
        public List<TvMarketContentCommunityHome> Homes { get; set; }
    }


    [XmlType(AnonymousType = true)]
    public class TvMarketCommunity
    {
        /// <remarks/>
        [XmlAttribute("id")]
        public int Id { get; set; }

        [XmlIgnore]
        public decimal Latitude { get; set; }

        [XmlIgnore]
        public decimal Longitude { get; set; }

        [XmlIgnore]
        public int BuilderId { get; set; }

        [XmlIgnore]
        public string PriceRage { get; set; }

        [XmlIgnore]
        public string CommunityName { get; set; }

        [XmlIgnore]
        public bool IsFeature { get; set; }
    }
}