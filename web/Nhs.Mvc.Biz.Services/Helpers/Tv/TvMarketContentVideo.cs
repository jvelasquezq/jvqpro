﻿using System.Xml.Serialization;

namespace Nhs.Mvc.Biz.Services.Helpers.Tv
{
    /// <remarks/>
    [XmlType(AnonymousType = true)]
    public partial class TvMarketContentVideo
    {
        /// <remarks/>
        [XmlAttribute("order")]
        public byte Order { get; set; }

        /// <remarks/>
        [XmlAttribute("url")]
        public string url { get; set; }

        /// <remarks/>
        [XmlAttribute("title")]
        public string Title { get; set; }

        /// <remarks/>
        [XmlAttribute("caption")]
        public string Caption { get; set; }

        [XmlIgnore]
        public string Thumbnail { get; set; }

        [XmlIgnore]
        public string VideoId { get; set; }

        
    }
}