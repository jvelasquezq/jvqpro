﻿using System.Collections.Generic;
using System.Xml.Serialization;
using Nhs.Library.Business.Tv;

namespace Nhs.Mvc.Biz.Services.Helpers.Tv
{
    /// <remarks/>
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public partial class TvMarketContent
    {
        /// <remarks/>
        [XmlArrayItem("community", IsNullable = false)]
        public List<TvMarketContentCommunity> Communities { get; set; }

         [XmlIgnore]
        public IEnumerable<TvMarketCommunity> CommunitiesForTvBuilder { get; set; }

        /// <remarks/>
        [XmlArrayItem("video", IsNullable = false)]
        public List<TvMarketContentVideo> Videos { get; set; }

        [XmlArrayItem("TvMarketPromotion", IsNullable = true)]
        public List<TvMarketPromotion> Promotions { get; set; }

        /// <remarks/>
        [XmlArrayItem("sponsor", IsNullable = false)]
        public List<TvMarketContentSponsor> Sponsors { get; set; }

        /// <remarks/>
        [XmlAttribute("name")]
        public string Name { get; set; }

        /// <remarks/>
        [XmlAttribute("contesturl")]
        public string ContestUrl { get; set; }

        [XmlElement]
        public string SlideShow { get; set; }

        [XmlIgnore]
        public double Latitude { get; set; }

        [XmlIgnore]
        public double Longitude { get; set; }
    }
}