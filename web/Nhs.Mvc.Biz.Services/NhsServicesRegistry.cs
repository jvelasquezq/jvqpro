﻿using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Concrete;
using StructureMap.Configuration.DSL;

namespace Nhs.Mvc.Biz.Services
{
    public class NhsServicesRegistry : Registry
    {
        public NhsServicesRegistry()
        {
            For<IPartnerService>().Use<PartnerService>();
            For<IMapService>().Use<MapService>();
            For<ICommunityService>().Use<CommunityService>();
            For<IMarketService>().Use<MarketService>();
            For<ISessionCacheService>().Use<SessionCacheService>();
            //TODO: See if we can use a single interface and inject different types
            For<IWebCacheService>().Use<WebCacheService>();
            For<ITvService>().Use<TvService>();
            For<IBrandService>().Use<BrandService>();
            For<IBuilderService>().Use<BuilderService>();
            For<IUserProfileService>().Use<UserProfileService>();
            For<ICmsService>().Use<CmsService>();
            For<IListingService>().Use<ListingService>();
            For<ILookupService>().Use<LookupService>();
            For<IStateService>().Use<StateService>();
            For<ISearchAlertService>().Use<SearchAlertService>();
            For<IBoylService>().Use<BoylService>();
            For<IQuickMoveInSearchService>().Use<QuickMoveInSearchService>();
            For<IHotGreenSearchService>().Use<HotGreenSearchService>();
            For<IVideoService>().Use<VideoService>();
            For<IBasicListingService>().Use<BasicListingService>();
            For<ILeadService>().Use<LeadService>();
            For<ISsoService>().Use<SsoService>();
            For<IPartnerMarketPremiumUpgradeService>().Use<PartnerMarketPremiumUpgradeService>();
            For<IProCrmService>().Use<ProCrmService>();
            For<INewsletterService>().Use<NewsletterService>();
            For<IApiService>().Use<ApiService>();
            For<IEBookService>().Use<EBookService>();
            For<IPlannerService>().Use<PlannerService>();
            For<IOwnerStoriesService>().Use<OwnerStoriesService>();
            For<ISeoContentService>().Use<SeoContentService>();
            For<IAffiliateLinkService>().Use<AffiliateLinkService>();       

            //Global Injection - Basically, replaces all instances of IMarketService with MarketService
            SetAllProperties(c => c.OfType<IMarketService>());

            //Replaces all instances of IPartnerService with PartnerService
            SetAllProperties(c => c.OfType<IPartnerService>());

            //Replaces all instances of ICMSService with CMSService
            SetAllProperties(c => c.OfType<ICmsService>());



        }
    }
}
