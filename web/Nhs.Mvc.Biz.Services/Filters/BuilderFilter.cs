﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Mvc.Domain.Model;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Biz.Services.Filters
{
    public static class BuilderFilter
    {
        public static Builder GetById(this IEnumerable<Builder> builders, int builderId)
        {
            return (from b in builders
                    where b.BuilderId == builderId
                    select b).FirstOrDefault();
        }

        public static BuilderService GetByService(this IEnumerable<BuilderService> builderServices, int builderId, int serviceId)
        {
            return (from b in builderServices
                    where b.BuilderId == builderId && b.ServiceId == serviceId
                    select b).FirstOrDefault();
        }
        
        public static IQueryable<BuilderService> GetById(this IQueryable<BuilderService> builderServices, int builderId)
        {
            return (from b in builderServices
                    where b.BuilderId == builderId
                    select b);
        }
        public static IQueryable<Builder> GetByIdList(this IQueryable<Builder> builders, List<int> builderIdList)
        {
            return (from b in builders
                    where builderIdList.Contains(b.BuilderId)
                    select b);
        }
    }

}
