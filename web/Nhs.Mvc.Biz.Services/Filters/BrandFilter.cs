﻿using System.Collections.Generic;
using System.Linq;
using Nhs.Mvc.Domain.Model;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Biz.Services.Filters
{
    public static class BrandFilter
    {
        public static Brand GetById(this IQueryable<Brand> brands, int brandId)
        {
            return (from b in brands
                    where b.BrandId == brandId
                    select b).FirstOrDefault();
        }

        public static IQueryable<Brand> GetByIdList(this IQueryable<Brand> brands, IList<int> builderIdList)
        {
            return (from b in brands
                    where builderIdList.Contains(b.BrandId)
                    select b).OrderBy(o => o.BrandName);
        }
        
        public static IEnumerable<Brand> GetOnesWithLogos(this IEnumerable<Brand> brands)
        {
            return brands.Where(brd => !string.IsNullOrEmpty(brd.LogoSmall) && !string.IsNullOrEmpty(brd.LogoMedium) &&
                                       !brd.LogoSmall.Contains("1x1.gif") && !brd.LogoMedium.Contains("1x1.gif"));
        }
    }
}
