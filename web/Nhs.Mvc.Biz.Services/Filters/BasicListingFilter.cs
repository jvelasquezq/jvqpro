﻿using System.Linq;
using Nhs.Mvc.Domain.Model.BasicListings;

namespace Nhs.Mvc.Biz.Services.Filters
{
    public static class BasicListingFilter
    {

        public static BasicListing GetByListingId(this IQueryable<BasicListing> basicListings, int listingId)
        {
            return (from l in basicListings
                    where l.ListingId == listingId
                    select l).FirstOrDefault();
        }

        public static MarketGeoLocation GetByGeoLocationId(this IQueryable<MarketGeoLocation> marketGeoLocations, int geoLocationId)
        {
            return (from l in marketGeoLocations
                    where l.GeoLocationId == geoLocationId
                    select l).FirstOrDefault();
        }

        public static BasicListingImage GetByListingId(this IQueryable<BasicListingImage> basicListingImages, int listingId)
        {
            return (from i in basicListingImages
                    where i.ListingId == listingId && i.ImageSequence == 1 && i.ImageTypeCode == "LST"
                    select i).FirstOrDefault();
        }

        public static IQueryable<BasicListingImage> GetDisplayReady(this IQueryable<BasicListingImage> basicListingImages)
        {
            return (from i in basicListingImages
                    where i.DisplayStatusId == 5
                    select i);
        }
        
        public  static IQueryable<BasicListingImage> GetListingOnly(this IQueryable<BasicListingImage> basicListingImages)
        {
            return (from i in basicListingImages
                    where i.ImageTypeCode == "LST"
                    select i);
        }

        public static IQueryable<BasicListingImage> GetFirstImage(this IQueryable<BasicListingImage> basicListingImages)
        {
            return (from i in basicListingImages
                    where i.ImageSequence == 1
                    select i);
        }
    }
}
