﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Biz.Services.Filters
{
    public static class StateFilter
    {
        public static State GetByAbbr(this IQueryable<State> states, string abbr)
        {
            return (from s in states
                    where s.StateAbbr.Equals(abbr, StringComparison.CurrentCultureIgnoreCase)
                    select s).FirstOrDefault();
        }

        public static State GetByName(this IList<State> states, string name)
        {
            return (from s in states
                    where s.StateName.Equals(name, StringComparison.CurrentCultureIgnoreCase)
                    select s).FirstOrDefault();
        }

        public static State GetByNameOrAbbr(this IQueryable<State> states, string stateNameOrAbbr)
        {
            return (from s in states
                    where s.StateName.Equals(stateNameOrAbbr, StringComparison.CurrentCultureIgnoreCase) ||
                    s.StateAbbr.Equals(stateNameOrAbbr, StringComparison.CurrentCultureIgnoreCase)
                    select s).FirstOrDefault();
        }

        public static State GetByNameOrAbbr(this IList<State> states, string stateNameOrAbbr)
        {
            return (from s in states
                    where s.StateName.Equals(stateNameOrAbbr, StringComparison.CurrentCultureIgnoreCase) ||
                    s.StateAbbr.Equals(stateNameOrAbbr, StringComparison.CurrentCultureIgnoreCase)
                    select s).FirstOrDefault();
        }
    }
}
