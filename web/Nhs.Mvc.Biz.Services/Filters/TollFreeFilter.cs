﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;

namespace Nhs.Mvc.Biz.Services.Filters
{
    public static class TollFreeFilter
    {
        public static ITollFreeNumber GetById(this IQueryable<ITollFreeNumber> tollFreeNumbers, int partnerId,
                                             int communityId, int builderId)
        {
            return (from t in tollFreeNumbers
                    where t.PartnerId == partnerId &&
                          t.CommunityId == communityId &&
                          t.BuilderId == builderId
                    select t).FirstOrDefault();
        }
    }
}
