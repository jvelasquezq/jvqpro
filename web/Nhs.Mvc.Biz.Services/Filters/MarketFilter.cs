﻿using System.Collections.Generic;
using System.Linq;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Biz.Services.Filters
{
    public static class MarketFilter
    {
        public static Market GetById(this IEnumerable<Market> markets, int marketId)
        {
            return (from m in markets
                    where m.MarketId == marketId
                    select m).FirstOrDefault();
        }

        public static Market GetByIdFromDb(this IQueryable<Market> markets, int marketId)
        {
            return (from m in markets
                    where m.MarketId == marketId
                    select m).FirstOrDefault();
        }

        public static Market GetByStateNameOrIdAndMarketName(this IEnumerable<Market> markets, string stateNameOrId, string marketName)
        {
            stateNameOrId = stateNameOrId.ToLower();
            marketName = marketName.ToLower().ToTrimCase().Replace(" ", "-");
            //Case 78718: ToTrimCase() added to fix Market 214 DFU issue.
            return (from m in markets 
                    where m.State.StateAbbr.ToLower() == stateNameOrId ||
                    m.State.StateName.ToLower() == stateNameOrId &&
                    m.MarketName.ToTrimCase().ToLower().Replace(" ", "-") == marketName
                    select m).FirstOrDefault();
        }

        public static Market GetByStateNameAndMarketName(this IEnumerable<Market> markets, string stateName, string marketName)
        {
            stateName = stateName.ToLower();
            marketName = marketName.ToLower();
            return (from m in markets
                    where (m.State.StateAbbr.ToLower() == stateName || m.State.StateName.ToLower() == stateName) &&
                        m.MarketName.ToLower().Replace(" ", "-") == marketName.Replace(" ", "-")
                    select m).FirstOrDefault();
        }

        public static IQueryable<Market> ToMarket(this IEnumerable<PartnerMarketCity> partnerMarketCities)
        {

            var mkts = from m in partnerMarketCities
                       group m by new
                       {
                           m.MarketName,
                           m.State
                       } into grp
                       select grp.First();
            
            var markets = (from pm in mkts
                           select new Market
                           {
                               MarketId = pm.MarketID,
                               MarketName = pm.MarketName,
                               StateAbbr = pm.State,
                               TotalComingSoon = pm.TotalComingSoon.ToType<int>(),
                               TotalCommsWithPromos = pm.TotalCommsWithPromo,
                               TotalCommunities = pm.TotalCommunities.ToType<int>(),
                               TotalCustomBuilders = pm.TotalCustomBuilders.ToType<int>(),
                               TotalHomes = pm.TotalHomes.ToType<int>(),
                               TotalQuickMoveInHomes = pm.TotalQuickMoveInHomes,
                               Radius = pm.MarketRadius.ToType<int>(),
                               PriceHigh = pm.MarketPriceHigh.ToType<int>(),
                               PriceLow = pm.MarketPriceLow.ToType<int>(),
                               Latitude = pm.MarketLatitude.ToType<decimal>(),
                               Longitude = pm.MarketLongitude.ToType<decimal>(),
                               LatLong = new LatLong(pm.MarketLatitude.ToType<double>(), pm.MarketLongitude.ToType<double>()),
                               StateLatLong = new LatLong(pm.StateLatitude.ToType<double>(), pm.StateLongitude.ToType<double>()),
                               State = new State
                               {
                                   StateAbbr = pm.State,
                                   StateName = pm.StateName,
                                   Latitude = pm.StateLatitude,
                                   Longitude = pm.StateLongitude
                               },
                               Cities = (from pmc in partnerMarketCities
                                         where pmc.MarketID == pm.MarketID
                                         select new City()
                                         {
                                             Name = pmc.CityName,
                                             CityName = pmc.CityName,
                                             County = pmc.County,
                                             MarketId = pmc.MarketID,
                                             MarketName = pmc.MarketName,
                                             State = pmc.State
                                         }).DistinctBy(p => p.CityName).ToList()
                           });

            return markets.AsQueryable();
        }
    }
}
