﻿using System.Collections.Generic;
using System.Linq;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Biz.Services.Filters
{
    public static class SearchAlertFilter
    {
        public static SearchAlert GetById(this IQueryable<SearchAlert> searchAlerts, int alertId)
        {
            return (from s in searchAlerts
                    where s.AlertId == alertId
                    select s).FirstOrDefault();
        }

        public static IEnumerable<AlertResult> GetByAlertId(this IQueryable<AlertResult> alertResults, int alertId)
        {
            return (from r in alertResults
                where r.AlertId == alertId && r.StatusId == 1
                select r);
        }

        public static IEnumerable<AlertResult> ToAlertResults(this IQueryable<Community> communities, int alertId)
        {
            return communities.Select(c => new AlertResult()
            {
                AlertId = alertId,
                BuilderId = c.BuilderId,
                CommunityId = c.CommunityId,
                StatusId = 1
            }).ToList();
        }

    }
}
