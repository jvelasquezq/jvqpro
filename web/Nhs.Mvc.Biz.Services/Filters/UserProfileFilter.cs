﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Biz.Services.Filters
{
    public static class UserProfileFilter
    {
        public static UserProfile GetByGuid(this IQueryable<UserProfile> users, string guid)
        {
            return (from u in users
                    where u.UserGuid == guid
                    select u).FirstOrDefault();
        }

        public static UserProfile GetByLogonName(this IQueryable<UserProfile> users, string logonName, int partnerId)
        {
            string pid = partnerId.ToString();
            return (from u in users
                    where u.LogonName.Equals(logonName, StringComparison.CurrentCultureIgnoreCase) && u.StrPartnerId == pid 
                    select u).FirstOrDefault();
        }

        public static UserProfile GetByUserNamePassword(this IQueryable<UserProfile> users, string userName, string password, int partnerId)
        {
            string pid = partnerId.ToString();
            return (from u in users
                    where u.LogonName.Equals(userName, StringComparison.CurrentCultureIgnoreCase)
                          && u.Password == password && u.StrPartnerId == pid
                    select u).FirstOrDefault();
        }
          
        public static UserProfile GetBySsoId(this IQueryable<UserProfile> users, string ssoId, int partnerId)
        {
            string pid = partnerId.ToString();
            return (from u in users
                    where u.SsoUserId.Equals(ssoId, StringComparison.CurrentCultureIgnoreCase) && u.StrPartnerId == pid
                    select u).FirstOrDefault();
        }

        public static string GetUserIdByEmail(this IQueryable<UserProfile> users, string email)
        {
            return (from u in users
                    where u.LogonName.Equals(email, StringComparison.CurrentCultureIgnoreCase) 
                    orderby u.DateRegistered ascending 
                    select u.UserGuid 
                     ).FirstOrDefault();
        }

    }
}
