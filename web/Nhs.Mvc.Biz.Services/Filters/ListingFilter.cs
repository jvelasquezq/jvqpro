﻿using System.Linq;
using Nhs.Mvc.Domain.Model.Web.Interfaces;

namespace Nhs.Mvc.Biz.Services.Filters
{
    public static class ListingFilter
    {
        public static IPlan GetById(this IQueryable<IPlan> plans, int planId)
        {
            return (from p in plans
                    where p.PlanId == planId
                    select p).FirstOrDefault();
        }

        public static ISpec GetById(this IQueryable<ISpec> specs, int specId)
        {
            return (from s in specs
                    where s.SpecId == specId
                    select s).FirstOrDefault();
        }

        //public static HubPlan GetById(this IQueryable<HubPlan> plans, int planId)
        //{
        //    return (from p in plans
        //            where p.PlanId == planId
        //            select p).FirstOrDefault();
        //}

        //public static HubSpec GetById(this IQueryable<HubSpec> specs, int specId)
        //{
        //    return (from s in specs
        //            where s.SpecId == specId
        //            select s).FirstOrDefault();
        //}
    }
}
