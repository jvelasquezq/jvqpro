﻿using System.Collections.Generic;
using System.Linq;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;

namespace Nhs.Mvc.Biz.Services.Filters
{
    public static class ImageFilter
    {

        public static IEnumerable<IImage> GetByTypes(this IEnumerable<IImage> images, params string[] imageTypeCodes)
        {
            return (from i in images
                    where imageTypeCodes.Contains(i.ImageTypeCode)
                    select i);
        }

        public static IQueryable<IImage> GetByType(this IQueryable<IImage> images, string imageTypeCode)
        {
            return (from i in images
                    where i.ImageTypeCode == imageTypeCode
                    select i);
        }
    }
}
