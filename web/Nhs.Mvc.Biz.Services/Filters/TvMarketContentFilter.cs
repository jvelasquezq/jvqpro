﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Mvc.Biz.Services.Helpers.Tv;

namespace Nhs.Mvc.Biz.Services.Filters
{
    public static class TvMarketContentFilter
    {
        public static List<TvMarketContentCommunityHome> GetAllHomes(this TvMarketContent content)
        {
            return content.Communities.SelectMany(comm => comm.Homes).ToList();
        }
    }
}
