﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nhs.Library.Business;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Biz.Services.Filters
{
    public static class VideoFilter
    {
        public static IList<Video> GetByCommunityList(this IQueryable<Video> videos, IList<CommunityResult> communities)
        {
            var comm = communities.Select((community, index) => new {CommunityID = community.CommunityId, Sequence = index + 1});

            var communityIds = (from c in communities
                            select c.CommunityId).Distinct();

            var videoListWithoutNullValues = (from v in videos
                where v.CommunityId != null
                select v).ToList(); 

            var videoList = (from v in videoListWithoutNullValues
                    //where v.CommunityId != null && communityIds.Contains((int)v.CommunityId) Trying to avoid casting in each element.
                    where communityIds.Count(item=> item == v.CommunityId) > 0
                    select v).ToList();

            return (from v in videoList
                   join c in comm on v.CommunityId equals c.CommunityID
                   orderby c.Sequence, v.Order
                   select v).ToList();

        }

        
    }

}
