﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Biz.Services.Filters
{
    public static class LookupFilter
    {
        public static IQueryable<LookupGroup> GetByGroupName(this IQueryable<LookupGroup> lookup, string name)
        {
            return (from l in lookup
                    where l.GroupName == name
                    orderby l.SortOrder
                    select l);
        }
    }
}
