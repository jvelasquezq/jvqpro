﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Mvc.Domain.Model;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Biz.Services.Filters
{
    public static class PartnerFilter
    {
        public static Partner GetById(this IQueryable<Partner> partners, int partnerId)
        {
            return (from p in partners
                    where p.PartnerId == partnerId
                    select p).FirstOrDefault();
        }
    }
}
