﻿using System.Collections.Generic;
using System.Linq;
using Nhs.Library.Common;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Hub;

namespace Nhs.Mvc.Biz.Services.Filters
{
    public static class CommunityFilter
    {

        public static HubCommunity GetById(this IQueryable<HubCommunity> communities, int communityId)
        {
            return (from c in communities
                    where c.CommunityId == communityId
                    select c).FirstOrDefault();
        }

        public static IQueryable<Community> GetByMarketId(this IQueryable<Community> communities, int marketId)
        {
            return (from c in communities
                    where c.MarketId == marketId
                    select c);
        }

        public static IQueryable<Community> GetByPartnerMarketId(this IQueryable<Community> communities, int partnerId, int marketId)
        {
            return (from c in communities where
                    c.Market.PartnerMarkets.Any(p => p.MarketId == c.MarketId && p.PartnerId == partnerId)
                    select c);
        }

       public static Community GetById(this IQueryable<Community> communities, int communityId)
       {

           return (from c in communities
                   where c.CommunityId == communityId &&
                   c.Market.PartnerMarkets.Any(p => p.MarketId == c.MarketId && p.PartnerId == Configuration.PartnerId)
                   select c).FirstOrDefault();
        }

       public static IQueryable<Community> GetByIdList(this IQueryable<Community> communities, List<int> communityIdList)
       {

           return (from c in communities
                   where communityIdList.Contains(c.CommunityId) &&
                   c.Market.PartnerMarkets.Any(p => p.MarketId == c.MarketId && p.PartnerId == Configuration.PartnerId)
                   select c);
        }

       public static IQueryable<CommunityPromotion> GetByIdList(this IQueryable<CommunityPromotion> communityPromotions, List<int> communityIdList)
       {

           return (from c in communityPromotions
                   where communityIdList.Contains((int)c.CommunityID) 
                   select c);
       }


       public static IQueryable<CommunityPromotion> GetById(this IQueryable<CommunityPromotion> communityPromotions, int communityId, int builderId)
       {
           return communityPromotions.Where(c => c.CommunityID == communityId && c.BuilderID == builderId);
       }

       public static IQueryable<CommunityEvent> GetByIdList(this IQueryable<CommunityEvent> communityEvents, List<int> communityIdList)
       {

           return (from c in communityEvents
                   where communityIdList.Contains((int)c.CommunityID)
                   select c);
       }

       public static IQueryable<CommunityEvent> GetByIdList(this IQueryable<CommunityEvent> communityEvents, int communityId, int builderId)
       {

           return (from c in communityEvents
                   where ((int)c.CommunityID) == communityId && c.BuilderID == builderId
                   select c);
       }

       public static IQueryable<CommunityAgentPolicy> GetByIdList(this IQueryable<CommunityAgentPolicy> agentPolicies, int communityId, int builderId)
       {

           return (from c in agentPolicies
                   where ((int)c.CommunityID) == communityId && c.BuilderID == builderId
                   select c);
       }
    }

    
}
