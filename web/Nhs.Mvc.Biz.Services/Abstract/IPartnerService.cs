﻿using System.Collections.Generic;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface IPartnerService
    {
        Partner GetPartner(int partnerId);
        List<Location> GetTypeAheadSuggestions(int partnerId, string searchText, bool includeCommunities, bool locationHandlerSearch = false);
        int GetPartnerMaskIndex(int partnerId);
        Dictionary<string, string> GetPartnerSiteTerms(int partnerId);
        string GetPartnerMarketFacebook(int partnerId, int brandPartnerId, int marketId);
        List<SpotLightHomes> GetSpotLightHomes(int partnerId, int marketId);
    }
}
