﻿namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface IPartnerMarketPremiumUpgradeService
   {
       bool GetPartnerMarketPremiumUpgrade(int partnerId, int marketId);
   }
}
