﻿using System.Collections.Generic;
using System.Data;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface IPlannerService
    {
        DataTable GetSavedCommunities(string communityId, int partnerId);
        DataTable GetSavedHomes(string planIds, string specIds, int partnerId);
        Spec GetSavedHome(int planId, int specId, int partnerId);
    }
}
