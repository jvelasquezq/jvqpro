﻿using System.Collections.Generic;
using System.Data;
using Nhs.Mvc.Domain.Model.Web.Partials;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface ILeadService
    {
        IList<LeadEmailProperty> GetCommsHomesForRequestBySourceRequestKey(string sourceRequestKey);
        IList<LeadEmailProperty> GetCommsHomesForRequest(int requestId);
        IList<LeadEmailProperty> GetLeadEmailHomes(IList<int> planIds, IList<int> specIds);
        IList<LeadEmailProperty> GetLeadEmailCommunities(IList<int> commIds);
        LeadEmailProperty GetGetRequestItemDeliveryInfo(int requestItemId);
        void GetRequestAttribs(int requestId, out string requestTypeCode, out int marketId);
        LeadEmail GetLeadEmailInfo(int requestId);
        DataTable GetCommunityBrochures(int communityId, int builderId);
        DataTable GetHomeBrochures(int planId, int specificationId);
        void UpdateBrochureName(int requestItemDeliveryId, string brochureName);
        DataTable GetRequestItemInformation(int requestId);
        void InsertPresenteComunitys(int sourceCommunityId, string communityList, string email, string firstName,
                                     string lastName, string postalCode, int partnerId, int marketId);
    }
}
