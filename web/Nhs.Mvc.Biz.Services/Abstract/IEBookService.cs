﻿namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface IEBookService
    {
        /// <summary>
        /// Creates the eBook lead.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="partnerId"></param>
        /// <param name="source"></param>
        bool CreateeBookLead(string email, int partnerId, string source);

        bool SignUpeBookModal(bool homeOfTheWeek, bool weeklyMarketUpdate, int marketId, string email, int partnerId, int brandPartnerId,string partnerSiteUrl);
    }
}
