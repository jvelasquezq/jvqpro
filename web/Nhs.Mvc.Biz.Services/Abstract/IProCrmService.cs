﻿using System.Collections.Generic;
using System.Data;
using Nhs.Library.Constants.Enums;
using Nhs.Mvc.Domain.Model.Profiles;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface IProCrmService
    {
        /// <summary>
        /// Get las updated/Used Clients list, this list is a Clients LastUpdateDate in the same minute
        /// is using a "MM/dd/yyyy hh:mm" in orde to get the result
        /// </summary>
        /// <param name="userGuid">Current login user</param>
        /// <param name="clientStatus">Status of the clients to be filter, default Active</param>
        /// <returns>List of Agents with the same LastUpdateDate minute in the DB</returns>
        IList<AgentClient> GetRecentUsedClients(string userGuid,
                                                ProCrmClientsStatusType clientStatus = ProCrmClientsStatusType.Active);

        IEnumerable<AgentClient> GetMyClientList(string userId, ProCrmClientsStatusType status);
        IEnumerable<AgentClient> GetMyClientList(string userId);
        IEnumerable<ClientEmail> GetClientEmails(string userId);
        IEnumerable<ClientTask> GetPendingTasks(long clientId, string userGuid);
        AgentClient GetClientDetails(long clientId, string userGuid);
        ClientTask GetTask(long clientId, string userGuid, int taskId);
        bool UpdateTask(ClientTask task, IEnumerable<ClientTaskListing> clientTaskListings, IEnumerable<int> clientTaskListingListDelete, out string errorMessage, bool taskComplete = false);
        bool InsertTask(ClientTask task, IEnumerable<ClientTaskListing> clientTaskListings, int clientId, out string errorMessage);
        bool DeleteTask(long clientId, long taskId, string userGuid);
        bool UpdateStatusClient(long agentClientId, ProCrmClientsStatusType status);
        bool InsertClient(AgentClient client, IEnumerable<ClientEmail> emails, IEnumerable<ClientPhone> phones, ClientTask task = null);
        bool UpdateClient(AgentClient client, IEnumerable<ClientEmail> emails, IEnumerable<ClientPhone> phones);
        bool EmailExists(string userId, long clientId, IList<string> emails, out Dictionary<string, string> emailDuplicate);
        bool RelateClient(int clientId, string relatedFirstName, string relatedLastName, IEnumerable<ClientEmail> emails, ClientTask task = null);
        string FillDefaultDescription(string description, long clientId, string userId, ProCrmTaskTypes taskType);
        bool InsertEmailsToClient(int clientId, IEnumerable<ClientEmail> emails, ClientTask task = null);
        bool InsertToFavoritesClient(IEnumerable<ClientPlanner> clientPlanner);
        bool DeleteToFavoritesClient(int clientId, int property);
        bool InsertClients(IEnumerable<AgentClient> clients);
        DataTable GetSavedCommunities(string communityId, int partnerId);
        DataTable GetSavedHomes(string planIds, string specIds, int partnerId);
        /// <summary>
        /// Update taks for CRM
        /// </summary>
        /// <param name="clients">List of clients to add the task</param>
        /// <param name="task">the info of the task to add</param>
        /// <returns>TRUE if all was success otherwise FALSE</returns>
        bool UpdateClients(IEnumerable<int> clients, ClientTask task);
    }
}
