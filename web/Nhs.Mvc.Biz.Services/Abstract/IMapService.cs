﻿using System.Collections.Generic;
using Nhs.Library.Business;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface IMapService
    {
        City GeoCodeCityByMarket(string city, int marketId, string state);
        PostalCode GeoCodePostalCode(string postalCode);
        County GeoCodeCountyByMarket(string county, int marketId);
        Market GetMarketInfo(int marketId, int partnerId);
        List<Market> GetMarketsInfo(List<int> marketsId, int partnerId);
   }
}
