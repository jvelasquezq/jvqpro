using System.Collections.Generic;
using Bdx.Web.Api.Objects;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Biz.Services.Helpers.PropertyInformation;
using Nhs.Mvc.Domain.Model.Hub;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Entities;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Domain.Model.Web.Partials;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface ICommunityService
    {
        bool UseHub { get; set; }
        IList<BoylLink> GetBoylResults(int marketId, int partnerId);
        string GetTollFreeNumber(int partnerId, int communityId, int builderId);
        IEnumerable<Community> GetAllCommunitiesForPartner(int partnerId);
        IEnumerable<MediaPlayerObject> GetCommunityVideoIDs(List<int> commIds, Dictionary<int, bool> commsBilledStatus);
        List<Video> GetCommunityVideos(IEnumerable<CommunityResult> communityResults);
        IEnumerable<ExtendedHomeResult> GetMatchingHomesForHubCommunity(int communityId);
        IList<NearbyCommunity> GetCommunitiesForRecoEmail(int partnerId, string commList, string builderList, decimal sourceCommLat, decimal sourceCommLng);

        /// <summary>
        /// Gets the nearby communities.
        /// </summary>
        /// <param name="partnerId">The partner id.</param>
        /// <param name="communityId">The community id.</param>
        /// <param name="builderId">The builder id.</param>
        /// <param name="geoLocationID">The geo location ID.</param>
        /// <returns></returns>
        IList<NearbyCommunity> GetNearbyCommunities(int partnerId, int communityId, int builderId, int? geoLocationID);

        IList<IImage> GetLotMapImages(int communityId);
        IImage GetBuilderMap(IEnumerable<IImage> allImages);
        IEnumerable<IImage> GetVideoTourImages(int communityId);
        IEnumerable<IImage> GetAllVideoTourImages(List<int?> communityIds);

        /// <summary>
        /// Get List of Images for the Awards received by the builder of the community.
        /// </summary>
        /// <param name="communityId">Selected community id</param>
        /// <returns>List of 5 award images at the max</returns>
        IList<IImage> GetAwardImages(IEnumerable<IImage> allImages);

        IList<IImage> GetBannerImage(int communityId);
        bool GetCommunityBilledStatus(int partnerId, int builderId, int communityId, int marketid, bool useHub = false);
        List<MediaPlayerObject> GetMediaPlayerObjects(int communityId, bool addVideo = true, bool useImageTitle = false, bool isMobile = false, string size = ImageSizes.CommDetailMain);
        List<MediaPlayerObject> GetMediaPlayerVideoObjects(int communityId);
        IList<Testimonial> GetCustomerTestimonials(int maxNoOfQuotes, int communityId, int builderId);
        IDictionary<AmenityGroup, IList<IAmenity>> GetAmenities(IEnumerable<IAmenity> commAmenities, IEnumerable<IAmenity> commOpenAmenities);
        Community GetCommunity(int communityId);
        HubCommunity GetHubCommunity(int communityId);
        IList<Community> GetCommunitiesByMarketId(int marketId);
        List<Community> GetCommunitiesByBrandId(int partnerId, int brandId, Brand brand = null);
        string GetNonPdfBrochureUrl(int communityId, int planId, int specId, int builderId);
        IList<Community> GetCommunities(List<int> communityIdList);
        IList<CommunityPromotion> GetCommunityPromotions(List<int> communityIdList);
        object GetCommunityHomeMapPoints(SearchParams searchParameters, SearchResultsPageType srpType, int partnerId);
        IEnumerable<ApiFacetOption> GetBuilders(SearchParams searchParameters, int partnerId);
        IList<CommunityPromotion> GetCommunityDetailPromotions(int communityId, int builderId);
        IList<CommunityEvent> GetCommunityEvents(List<int> communityIdList);
        IList<CommunityEvent> GetCommunityEvents(int communityId, int builderId);
        IList<CommunityAgentPolicy> GetCommunityAgentPolicies(int communityId, int builderId);
        ICollection<Domain.Model.Web.Utility> GetCommunityUtilities(int communityId, int partnerId);
        string GetSalesAgents(int salesOfficeId);
        IEnumerable<Promotion> GetCommunityPromotions(int communityId, int builderId);
        IEnumerable<HotHome> GetCommunityHotDeals(IEnumerable<int> communityId);
        IEnumerable<GreenProgram> GetCommunityGreenPrograms(int communityId);
        IEnumerable<GreenProgram> GetCommunityGreenPrograms(IEnumerable<int> communityId);
        T GetRecommendedCommunities<T>(string email, int partnerId, int communityid, int specid, int planid, int basicHomeId, int recoNumber = 0, bool presentedExclude = false, bool recoOnlyCurrentBuilderComms = false) where T : new();

        List<ApiRecoCommunityResult> GetRecommendedCommunities(string email, int partnerId, int communityid, int specid, int planid, int basicHomeId,
            int recoNumber = 0, bool presentedExclude = false, bool recoOnlyCurrentBuilderComms = false,
            bool relaxSearchCriteria = false);

        IList<NearbyCommunity> GetNearbyCommunities(int partnerId, int communityid, int basicHomeId, int recoNumber = 0, bool presentedExclude = false,
            bool relaxSearchCriteria = false);

        int SendRecommendedProperties(LeadInfo leadInfo, IList<ApiRecoCommunityResult> recoComms, Community community);
        bool IsCommunityActiveOnParentBrandPartner(int communityId, int brandPartnerId);
        bool AlreadyRequestBrosure(int partherId, int marketId, int communityId, string email, string requestType = "COM", int timePeriodDays = 60);
        List<CommunityMapCard> GetCommunityMapCards(int partnerId, string communityList, string basicListingsIds);

        /// <summary>
        /// This method get the value of the two credit score links depending on the information saved in the XML file,
        /// if the property called UseOldCreditScoreLinks is true, then the two old credit score links will be used otherwise
        /// the new link will be used for both links
        /// </summary>
        /// <returns>CreditScoreLinks</returns>
        void GetCreditScoreLinks(ref string creditScoreLink1, ref string creditScoreLink2);

        Community GetSavedCommunity(int partnerId, int communityId);
        CreditScoreLinks BindCreditScoreLinks();
    }
}
