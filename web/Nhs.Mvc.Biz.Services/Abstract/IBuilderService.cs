﻿using System.Collections.Generic;
using System.Data;
using Nhs.Library.Business.BuilderCoop;
using Nhs.Mvc.Domain.Model.Web;


namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface IBuilderService
    {
        IList<Builder> GetMarketBuilders(int partnerId, int marketId);
        IList<Builder> GetBuilders(List<int> builderIdList);
        DataTable GetMarketBcs(int partnerId, int marketId);
        Builder GetBuilder(int builderId);
        BuilderService GetServiceByBuilderId(int builderId, int serviceId);
        BuilderCoOpInfo GetBuilderCoOpInfo(int builderId, int brandPartnerId, bool loadPartnershipPactHtml = false);
    }
}
