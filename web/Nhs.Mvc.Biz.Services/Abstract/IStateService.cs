﻿using System.Collections.Generic;
using System.Linq;
using Nhs.Library.Business;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface IStateService
    {
        IList<State> GetAllStates();
        string GetStateAbbreviation(string name);
        string GetStateName(string abbreviation);
        State GetState(string abbreviation);
        IList<State> GetPartnerStates(int partnerId);
        IList<State> GetSearchAlertStates(int partnerId);
        State GetStateCoodinates(string state);
        StateInfo GetStateInfo(int partnerId, string stateName);
        State GetStateCoodinatesFromStateName(string statename);
    }
}
