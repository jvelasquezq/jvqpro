﻿using System.Collections.Generic;
using Nhs.Library.Business;
using Market = Nhs.Mvc.Domain.Model.Web.Market;
using State = Nhs.Mvc.Domain.Model.Web.State;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface IBoylService
    {
        IList<State> GetBoylStates(int partnerId);
        List<Market> GetBoylMarketsByState(int partnerId, string stateAbbr);
        IEnumerable<BoylResult> GetBoylResults(int partnerId, int marketId, string marketName, string marketState);
        IEnumerable<BoylResult> GetBoylResultsCnh(string marketName, string marketState);
        IEnumerable<ExtendedHomeResult> GetSpotHomes(int partnerId, int marketId);
        //Case 81052
        //IList<BoylLink> GetBoylLinks(int partnerId);
        List<BoylResult> GetBoylResultsByPartner(int partnerId);
    }
}
