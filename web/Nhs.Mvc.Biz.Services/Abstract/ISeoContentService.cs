﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Concrete;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface ISeoContentService
    {
        T GetSeoContent<T>(SeoContentOptions options) where T : new();
        string ReplaceTags(string input, IList<ContentTag> replaceTags);
    }
}
