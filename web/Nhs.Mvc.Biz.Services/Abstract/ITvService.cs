﻿using System.Collections.Generic;
using System.Linq;
using Nhs.Library.Business.Tv;
using Nhs.Mvc.Biz.Services.Helpers.Tv;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface ITvService
    {
        IEnumerable<TvMarket> GetTvMarkets(int partnerId);
        TvMarketContent GetTvMarketContent(string keyword, int marketId, int partnerId);        
        int GetPartnerMarketIdIfOnlyHaveOne(int partnerId, int brandPartnerId);
    }
}
