﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Mvc.Domain.Model.BasicListings;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface IBasicListingService
    {
        BasicListing GetBasicListing(int listingId);
        BasicListingImage GetBasicListingImage(int listingId);
        IEnumerable<BasicListingImage> GetImagesForBasicListings(IEnumerable<int> listingIds);
    }
}
