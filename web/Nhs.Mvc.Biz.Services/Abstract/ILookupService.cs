﻿using System;
using System.Collections.Generic;
using Nhs.Library.Enums;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface ILookupService
    {
        IList<LookupGroup> GetCommonListItems(CommonListItem commonListItem, string any="Any");
        IList<LookupGroup> GetSquareFeetListItems(string any = "Any");
    }
}
