﻿using System.Collections.Generic;
using Nhs.Library.Business;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface IVideoService
    {
        IList<Video> GetVideosForCommunityList(IList<CommunityResult> communities);

        IList<Video> GetVideosForBrand(int brandId);
    }
}
