﻿using System;
using System.Web;
using ComponentSpace.SAML2.Assertions;
using ComponentSpace.SAML2.Protocols;
using Nhs.Library.Business.Sso;
using Nhs.Mvc.Domain.Model.Profiles;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface ISsoService
    {
        NhsSamlReponse ReceiveSamlResponse(HttpRequest currentRequest, string siteUrl);
        void ProcessSuccessSamlResponse(SAMLResponse samlResponse, string relayState, PartnerSamlAttributes attributes, int partnerId, string partnerSiteUrl, int brandPartnerId, string wrongPasswordMessage, string wrongEmailMessage, string userAlreadyExistMessage);
        void ProcessErrorSamlResponse(SAMLResponse samlResponse);
        void ValidateAssertion(SAMLAssertion assertion, PartnerSamlAttributes attributes, int partnerId, int brandPartnerId, string partnerUrl, string wrongPasswordMessage, string wrongEmailMessage, string userAlreadyExistMessage);
        bool CreateSsoUserAccount(SAMLAssertion assertion, PartnerSamlAttributes attributes, int partnerId, int brandPartnerId, string wrongPasswordMessage, string wrongEmailMessage, string userAlreadyExistMessage, bool subcribeNewsletter = true);
        bool VerifyPartnerAllowsWeeklyOptIn(string partnerUrl);
        string CreateRandomPassword(int passwordLength);
        string GetSamlAttributeValueByName(PartnerSamlAttributes attributes, string name, SAMLAssertion assertion);
        bool RequestLoginAtIdentityProvider(HttpResponse currentHttpResponse, string partnerSiteUrl, string partnerSsoDestinationUrl);
        bool RequestLogOutAtIdentityProvider(HttpResponse currentHttpResponse, string partnerSiteUrl, string partnerSloDestinationUrl);
        void CreateUserProfile(UserProfile userProfile, int brandPartnerId, string wrongPasswordMessage, string wrongEmailMessage, string userAlreadyExistMessage, bool suscribedNewletter = true);
        bool Authenticate(string sessionToken, DateTime dateTime, string algorithm, string partnerSitePassword, string ssoId);
        bool ValidateMredTermsOfService(UserProfile userProfile, int brandPartnerId, string partnerSiteUrl, string wrongPasswordMessage, string wrongEmailMessage, string userAlreadyExistMessage);
    }
}
