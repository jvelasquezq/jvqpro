﻿using System.Collections.Generic;
using System.Linq;
using Nhs.Library.Business;
using Nhs.Library.Enums;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface IProfileService
    {
        IEnumerable<IGrouping<ListingType, PlannerListing>> GetSavedListings(string userId);
    }
}
