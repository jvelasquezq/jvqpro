﻿using System;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface IWebCacheService
    {
        T Get<T>(string cacheId, bool refreshCache, Func<T> getItemCallback) where T : class;
        T Get<T>(string cacheId, bool refreshCache, TimeSpan timeSpan, Func<T> getItemCallback) where T : class;
    }
}
