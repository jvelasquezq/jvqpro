﻿using System.Collections.Generic;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface INewsletterService
    {
        bool SendRequest(string email, int marketId, string marketName, int partnerId, string partnerName,
                         string partnerUrl, int brandPartnerId);
        IEnumerable<Nhs.Mvc.Domain.Model.Web.Market> GetMarkets(int partnerId);

        bool SendContestRequest(string email, string fullName, int marketId, int brandPartnerId);
    }
}
