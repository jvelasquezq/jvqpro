using System.Collections.Generic;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Library.Proxy;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface IListingService
    {
        bool UseHub { get; set; }
        WebApiCommonResultModel<List<T>> GetHomesResultsFromApi<T>(SearchParams searchParams, int partnerId);
        IPlan GetPlan(int planId);
        ISpec GetSpec(int specId);
        IList<ISpec> GetSpecsForSpecIds(IList<int> specIds, bool includeImages);
        IList<IPlan> GetPlansForPlanIds(IList<int> planIds, bool includeImages);
        IList<IImage> GetFloorPlanImagesForPlan(int planId);
        IList<IImage> GetFloorPlanImagesForSpec(IPlan plan, ISpec spec);
        int GetFloorPlanImageCount(IPlan plan, ISpec spec);
        IEnumerable<IImage> GetHomeImagesForPlan(IPlan plan);
        IEnumerable<IImage> GetHomeImagesForPlan(int planId);
        IEnumerable<IImage> GetHomeImagesForSpec(int planId, int specId);
        IEnumerable<IImage> GetHomeImagesForSpec(IPlan plan, ISpec spec);
        IList<HomeOption> GetHomeOptionsForPlan(IPlan plan, int partnerId);
        IList<NearbyHome> GetNearbyHomes(int partnerId, int communityId, int builderId, decimal basePrice, int bedrooms, int baths);
        string GetFloorPlanViewerUrl(IPlan plan, ISpec spec);
        IList<IImage> GetHomeImagesForSpecs(IList<int> specIds);
        IList<IImage> GetHomeImagesForPlans(IList<int> planIds);
        string GetInteriorImageWhenElevationAbsent(string imageThumbnail, int planId, int specId);
        List<MediaPlayerObject> GetMediaPlayerObjects(IPlan plan, ISpec spec, bool addVideo = true, bool useTitleAndCaption = false, string size = ImageSizes.HomeMain);
    }
}
