﻿using Nhs.Mvc.Domain.Model.BHIContent;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface IAffiliateLinkService
    {
        AffiliateLinks GetAffiliateLinks(string page, int partnerId);
    }
}
