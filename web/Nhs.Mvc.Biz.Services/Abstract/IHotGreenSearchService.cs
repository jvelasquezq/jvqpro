﻿using System.Collections.Generic;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface IHotGreenSearchService
    {
        IList<State> GetHGStates(int partnerId);        
    }
}
