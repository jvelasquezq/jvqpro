﻿using System.Collections.Generic;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model;
using Nhs.Mvc.Domain.Model.BhiTransaction;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface IOwnerStoriesService
    {
        OwnerStoriesServiceError InsertOwnerStory(OwnerStory ownerStory, string shareFolderOwnerStoriesMediaPath);
        OwnerStoriesContent GetOwnerStoriesContent();
        List<OwnerStory> GetOwnerStoriesList(int? builderId, bool? includeAllStatus, bool? status);
        OwnerStoriesServiceError CurateOnwerStory(OwnerStory curatedOwnerStory);
    }

}
