using System.Collections.Generic;
using Nhs.Library.Business;
using Nhs.Library.Business.Seo;
using Nhs.Mvc.Domain.Model.Web;
using GeoLocation = Nhs.Mvc.Domain.Model.Web.GeoLocation;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface IMarketService
    {
        IEnumerable<Market> GetMarketsByAmenity(string state, string amenityType, int partnerId);
        GeoLocation GetGeoLocationByPostalCode(string postalCode);

        /// <summary>
        /// DO NOT CACHE ITEMS RETRIEVED FROM THIS METHOD - ALREADY CACHED
        /// </summary>
        /// <param name="partnerId">The partner id.</param>
        /// <param name="stateNameOrId">The state name or id.</param>
        /// <param name="marketName">Name of the market.</param>
        /// <param name="loadBrandsAndSchools">if set to <c>true</c> [load brands and schools].</param>
        /// <returns></returns>
        Market GetMarket(int partnerId, string stateNameOrId, string marketName, bool loadBrandsAndSchools);

        Market GetMarketStateNameAndMarketName(int partnerId, string stateNameOrId, string marketName, bool loadBrandsAndSchools);

        /// <summary>
        /// DO NOT CACHE ITEMS RETRIEVED FROM THIS METHOD - ALREADY CACHED
        /// </summary>
        /// <param name="partnerId">The partner id.</param>
        /// <param name="marketId">The market id.</param>
        /// <param name="loadBrandsAndSchools">if set to <c>true</c> [load brands and schools].</param>
        /// <returns></returns>
        Market GetMarket(int partnerId, int marketId, bool loadBrandsAndSchools);

        /// <summary>
        /// Get the market object by Id without partnerid
        /// </summary>
        /// <param name="marketId">The market id.</param>
        /// <returns></returns>
        Market GetMarket(int marketId);

        int GetMarketIdFromPostalCode(string postalCode, int partnerId);
        List<Location> GetSimilarPostalCodes(string postalCode, int partnerId);
        List<Community> GetFeaturedCommunities(int partnerId, int marketId, int maxCount);
        List<Community> GetRandomCommunitiesInMarketFromDb(int partnerId, int marketId, int recordCount, bool featured);
        bool IsMarketValidForPartner(int partnerId, int marketId);
        string GetStateNameForMarket(int marketId);

        /// <summary>
        /// CACHED ALREADY - NO NEED TO CACHE ITEMS RETRIEVED FROM THIS METHOD
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        IEnumerable<Market> GetMarkets(int partnerId);

        IEnumerable<Market> GetMarkets(string markets);
        IEnumerable<Market> GetMarketsByState(int partnerId, string stateAbbr);
        List<Market> GetMarketsByBrand(int partnerId, string state, int brandId);
        string GetMarketName(int partnerId, int marketId);
        IList<City> GetCitiesForMarket(int partnerId, int marketId);
        int GetMarketIdFromStateCounty(string stateName, string county);
        string GetMarketInfoForCreateAccount(string zip);
        int GetMarketIdFromStateCity(string stateName, string city);
        Market GetMarketByName(string stateName, string marketName);
        IList<int> GetZipCodesByMarket(int marketId, string city, int returnCount);
        Market GetMarketFromIp(long ipAddress);

        /// <summary>
        /// Retrieves all the Zip Codes Associated to a Synthetic Geography
        /// </summary>
        /// <param name="synthetic">Synthetic Geography location to search its associated postal codes</param>
        /// <returns>All postal codes associated to the Synthetic Geography separated by comma</returns>
        string GetZipCodesForSynthetic(Synthetic synthetic);

        IList<SchoolDistrict> GetMarketSchoolDistricts(int partnerId, int marketId);
    }
}
