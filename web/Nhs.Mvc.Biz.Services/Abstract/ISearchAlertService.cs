﻿using System.Collections.Generic;
using Nhs.Library.Business;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Mvc.Domain.Model.Web;
using SearchAlert = Nhs.Mvc.Domain.Model.Profiles.SearchAlert;
//using Nhs.Library.Business;

//using SearchAlert = Nhs.Mvc.Domain.Model.Profiles.SearchAlert;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface ISearchAlertService
    {
        SearchAlert GetSearchAlert(int alertId);
        IList<SearchAlert> GetSearchAlertsByUserGuid(string guid);
        bool CreateAlert(int partnerId, SearchAlert alert, IProfile profile, int sourceCommunityId, string comments = "");
        void SetGeoLocation(Market market, string stateName, string cityName, string zipCode, int radius,SearchAlert alert, IMapService mapService);
        int GetNoMatchCount(string userId);
        void IncrementNoMatchCount(string userId);
        void ResetNoMatchCount(string userId);
        IEnumerable<AlertResult> GetAlertResults(int alertId);
        void SaveResults(IEnumerable<Community> alertResults, int alertId);
        void RemoveSavedAlert(SearchAlert searchAlert);
        void RemoveAlertResult(int searchAlertId, int communityId, int builderId);
    }
}
