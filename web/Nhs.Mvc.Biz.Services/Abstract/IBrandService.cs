﻿using System.Collections.Generic;
using Nhs.Library.Business;
using Nhs.Mvc.Domain.Model.Hub;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface IBrandService
    {
        List<Brand> GetBrandsByState(int partnerId, string state);
        List<Brand> GetBrandsByPartner(int partnerId);
        IList<Brand> GetBrandsByMarket(int partnerId, int marketId);
        IList<Brand> GetRandomizedBrands(IList<Brand> brands, int numBrands, bool checkForLogo, int partnerId);

        Brand GetBrandById(int brandId, int partnerId);
        IList<Brand> GetBrandsByIds(IList<int> brandIds, int partnerId);
        IEnumerable<IImage> GetShowCaseImages(int brandId);
        List<Brand> GetMarketandStateByBrand(int brandId, int partnerId);
        IList<Brand> GetAllBrands(int partnerId);
        IEnumerable<ExtendedCommunityResult> GetBrandSpotlights(int partnerId, int brandId);

        void SubmitBrandShowCase(int brandId, string overview, string description, string rssFeed,string twitterWidge, string facebookWidget, string contactEmail,string brandUrl);

        int SubmitBuilderShowCaseTestimonial(int testimonialId, int brandId, int sequence, string citation,string description);

        void DeleteBuilderShowCaseTestimonial(int testimonialId);
        void UpdateBrandShowCaseImage(int imageId, string description, string title, string accreditationSealUrl);

        int AddBrandShowCaseImage(string imageType, int builderId, string originalUri, string name,
            string description, string title, int sequence, string accreditationSealUrl);

        void DeleteBrandShowCaseImage(string imageids);
        IEnumerable<HubBrand> HubBrands { get; }
        IEnumerable<HubBrandShowCase> HubBrandShowCase { get; }
    }
}
