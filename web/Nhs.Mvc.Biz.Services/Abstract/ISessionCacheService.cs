﻿using System;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface ISessionCacheService
    {
        T Get<T>(string cacheId, bool refreshCache, Func<T> getItemCallback) where T : class;
    }
}
