﻿using System.Collections.Generic;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Library.Proxy;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface IApiService
    {
        WebApiCommonResultModel<List<T>> GetResultsWebApi<T>(SearchParams searchParams, SearchResultsPageType srpType);
        WebApiCommonResultModel<List<T>> GetResultsWebApi<T>(SearchParams searchParams, string searchType);
        T GetDataWebApi<T>(SearchParams searchParams, string searchType) where T : new();
    }
}
