﻿using System.Data;
using Nhs.Library.Business;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Mvc.Domain.Model.Profiles;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface IUserProfileService
    {
        bool LogonNameExists(string userName, int partnerId);

        void SignIn(string userName, string password, int partnerId, string wrongPasswordMessage, string wrongEmailMessage);
        void SignIn(string userName, int partnerId, string wrongEmailMessage);
        void SignOut(int partnerId);
        void ClearProfile(int partnerId);
        void SetAgentRecord(AgentBrochureTemplate agentBrochureTemplate);
        void CreateProfile(UserProfile profile, int brandPartnerId, string wrongPasswordMessage, string wrongEmailMessage, string userAlreadyExistMessage);
        void UpdateProfile(UserProfile profile);
        UserProfile GetUserByLogonName(string logonName, int partnerId);
        UserProfile GetUserBySsoId(string ssoId, int partnerId);
        UserProfile GetUserProfile(string userName, string password, int partnerId);

        //Leads
        bool CreateRegistrationLead(UserProfile profile);
        bool CreateRecoverPasswordLead(UserProfile profile);
        bool CreateSendToFriendLead(UserProfile profile, string fromEmail, string friendName, string friendEmail, int specId, int planId, int communityId, int builderId, string notes);
        
        void WriteProfileCookie(UserProfile profile);
        UnsubscribeResult Unsubscribe(string emailAddress, int partnerId, string marketOptOut, string deactivate);
        void AddCommunityToUserPlanner(UserProfile profile, int communityId, int builderId, ListingType listingType);

        UserProfile GetProfileFromLegacyProfile(Library.Business.Profile legacyProfile);
        BrochureStatus GetBrochureStatus(string email, int communityId, int builderId, int planId, int specId, int partnerId);

        string GetMoveInDate(int moveInDate);

        DataTable GetMoveInDateItems();
        DataTable GetFinancePrefItems();
        DataTable GetMoveReasonItems();
        UserProfile GetUserByGuid(string guid);
        DataTable GetStates();
        string GetMarketName(int marketId, int partnerId);
        void UpdateLastMatchDate(string userId);
        bool EmailExists(string email, int partnerId);
        string GetUserIdByEmail(string email);
        void SaveBrochureTemplate(IProfile profile);
        void SendToFriend(int brandPartnerId, int partnerId, string userId, string logonName, string firstName, string lastName, string fromEmail, string friendName, string friendEmail, int specId, int planId, int communityId, int builderId, string notes);
    }
}
