﻿using System.Collections.Generic;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface IQuickMoveInSearchService
    {
        IList<State> GetQuickMoveStates(int partnerId);
        List<Market> GetQuickMoveMarketsByState(int partnerId, string stateAbbr);
    }
}
