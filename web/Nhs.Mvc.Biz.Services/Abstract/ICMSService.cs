﻿using System;
using System.Collections.Generic;
using Nhs.Library.Business;
using Nhs.Library.Proxy;
using Nhs.Mvc.Domain.Model.CMS;

namespace Nhs.Mvc.Biz.Services.Abstract
{
    public interface ICmsService
    {
        RcGlobalBusinessConfig GetBusinessConfiguration(int partnerId, string configArticleName);

        RcContentInfo GetContentInfoByFolderNameAndArticleName(string path, string articleName, long taxonomyId,
                                                               int languageId);

        /// <summary>
        /// The path must have the Format with the "\" so if you want to get the content
        /// from a folder in Ektron under Home -> NewHomeSource -> Configuration
        /// you must use "\NewHomeSource\Configuration"        
        /// </summary>
        /// <param name="path">Path in Ektron folder estructure</param>  
        /// <param name="loadArticleHtml">If true call to get article by id and load the HTML content of the article</param>  
        /// <param name="languageId"></param> 
        /// <param name="taxonomyId"></param> 
        IList<RcContentInfo> GetContentInfoByFolderName(string path,  long taxonomyId,int languageId, bool loadArticleHtml = true);

        IList<CategoryPosition> GetCategoryPositionData(DateTime targetDate, int siteId, int categoryId);
        
        string GetArticleByTitle(string articleName, out long articleId);
        
        string GetArticleByTitle(string articleName);
            
        /// <summary>
        /// Get the article using ektron service, this method do not validate the taxonomy or lang
        /// </summary>
        /// <param name="articleId"></param>
        /// <returns></returns>
        [Obsolete("Change this to use taxonomy and lang")]
        ContentBase GetArticleById(long articleId);

        /// <summary>
        /// Get the html article content due an id, taxonomy and lang
        /// </summary>
        /// <param name="articleId"></param>
        /// <param name="taxonomyId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        string GetArticleById(long articleId, long taxonomyId, int languageId);

        /// <summary>
        /// Get Article due taxonomy and lang
        /// </summary>
        /// <param name="articleId"></param>
        /// <param name="taxonomyId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        RcArticle GetRcArticleById(long articleId, long taxonomyId, int languageId);

        /// <summary>
        /// For PRO in the left community search, this method fill all the facets 
        /// </summary>
        /// <param name="searchText"></param>
        /// <param name="selectedPriceLo"></param>
        /// <param name="selectedPriceHi"></param>
        /// <param name="selectedBathRoom"></param>
        /// <param name="selectedBedRoom"></param>
        /// <param name="brandPartnerId"></param>
        /// <returns></returns>
        CmsProSearch FillSearchViewModel(string searchText, string selectedPriceLo,
                                         string selectedPriceHi, int brandPartnerId, string selectedBathRoom = "",
                                         string selectedBedRoom = "");

        /// <summary>
        /// Gives all Categories under Resource Center
        /// </summary>       
        /// <returns></returns>
        IEnumerable<RCCategory> GetCategories(int brandPartnerId);

        /// <summary>
        /// Gives all Categories under Resource Center with subcategories
        /// </summary>
        /// <returns></returns>
        IEnumerable<RCCategory> GetCategoriesHierarchy(int brandPartnerId);

        /// <summary>
        /// Gives All the parent categories for an article
        /// </summary>
        /// <param name="articleId">Article Id to retrieve the categories hierarchy</param>
        /// <param name="categoryName"></param>
        /// <returns></returns>
        IEnumerable<RCCategory> GetCategoriesByArticleId(long articleId, string categoryName);

        /// <summary>
        /// Gives All the parent categories for a sub category
        /// </summary>
        /// <param name="categoryName"></param>
        /// <returns></returns>
        IEnumerable<RCCategory> GetParentCategoriesByCategoryName(string categoryName);

            /// <summary>
        /// Gives Subcategories under a given Taxonomy
        /// </summary>
        /// <param name="taxonomyId"></param>
        /// <returns></returns>
        IEnumerable<TaxonomyData> GetSubCategories(long taxonomyId);

        /// <summary>
        /// Gives Subcategories under a given Category
        /// </summary>
        /// <param name="categoryName"></param>
        /// <returns></returns>
        IEnumerable<TaxonomyData> GetSubCategories(string categoryName);

        /// <summary>
        /// Returns Category Id given a Category Name
        /// </summary>
        /// <param name="categoryName"></param>
        /// <returns></returns>
        long GetCategoryId(string categoryName);
                
        /// <summary>
        /// Returns Home Page Slide Show Content ('Home Page SlideShow')
        /// </summary>
        /// <returns></returns>
        RCSlideShow GetHomePageSlideShow(int partnerId);

        /// <summary>
        /// Returns Slide Show for a given Category ('[CategoryName] SlideShow')
        /// </summary>
        /// <param name="categoryName"></param>
        /// <returns></returns>
        RCSlideShow GetCategorySlideShow(string categoryName, int partnerId);

        /// <summary>
        /// Parses the slide show XML.
        /// </summary>
        /// <param name="xml">The XML.</param>
        /// <returns></returns>
        RCSlideShow ParseSlideShowXml(string xml);

        /// <summary>
        /// Returns a category given a Category name
        /// </summary>
        /// <param name="categoryName"></param>
        /// <returns></returns>
        RCCategory GetCategory(string categoryName, int brandPartnerId);

        /// <summary>
        /// Get Article by Title
        /// </summary>
        /// <param name="articleTitle"></param>
        /// <returns></returns>
        RcArticle GetArticle(string articleTitle);

        /// <summary>
        /// Get All articles under a category by taxonomy and lang
        /// </summary>
        /// <param name="categoryName"></param>
        /// <param name="taxonomyId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        IEnumerable<RcArticle> GetArticlesByCategory(string categoryName, long taxonomyId, int languageId);

        /// <summary>
        /// get all the trendong articles due a taxonomy and lang
        /// </summary>
        /// <param name="taxonomyId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        IEnumerable<RcArticle> GetTrendingArticles(long taxonomyId,int languageId);

        /// <summary>
        /// Returns metas for a given article (title)
        /// </summary>
        /// <param name="articleTitle"></param>
        /// <param name="taxonomyId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        IList<MetaTag> GetMetasForArticle(string articleTitle, long taxonomyId, int languageId);

        /// <summary>
        /// Return the list of articles that matches the keywords requested in the search
        /// </summary>
        /// <param name="keyWord1"></param>
        /// <param name="keyWord2"></param>
        /// <param name="keyWord3"></param>
        /// <param name="keyWord4"></param>
        /// <param name="taxonomyId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        IEnumerable<RcArticle> GetArticleResultsByKeywords(string keyWord1, string keyWord2, string keyWord3,
                                                          string keyWord4, long taxonomyId, int languageId);

        IList<MetaTag> GetMetasForHomePage(long taxonomyId, int languageId);

        IList<MetaTag> GetMetasForCategory(string categoryName, long taxonomyId, int languageId);

        /// <summary>
        /// Get multipart article with its section details.
        /// </summary>
        /// <param name="articleTitle">Title of the Article</param>
        /// <param name="taxonomyId"></param>
        /// <param name="languageId"></param>
        /// <returns>Multipart article with container and its section details loaded</returns>
        RcMpArticleContainer GetMultiPartArticle(string articleTitle, long taxonomyId, int languageId);

        /// <summary>
        /// Get multipart article with its section details by section title.
        /// </summary>
        /// <param name="sectionTitle">Title of the Section</param>
        /// <param name="taxonomyId"></param>
        /// <param name="languageId"></param>
        /// <returns>Multipart article with container and its section details loaded</returns>
        RcMpArticleContainer GetMultiPartArticleBySection(string sectionTitle, long taxonomyId, int languageId);


        /// <summary>
        /// Get the smart form ID associated with the article.
        /// </summary>
        /// <param name="articleId"></param>
        /// <returns></returns>
        string GetArticleSmartFormType(long articleId);

        /// <summary>
        /// Get all the articles that bellon to an Author
        /// </summary>
        /// <param name="authorName">Name of the Author defined in Authors folder in Ektron</param>
        /// <param name="languageId">The configurated language id</param>
        /// <returns>IEnumerable list of RcArticles</returns>
        IEnumerable<RcArticle> GetRelatedArticlesByAuthorName(string authorName, long taxonomyId,int languageId);

        /// <summary>
        /// Get all the Authors on Ektron
        /// </summary>
        /// <returns>List of RcAuthor object with all the authors</returns>
        IEnumerable<RcAuthor> GetAllAuthorsInfo(long taxonomyId, int languageId);

        /// <summary>
        /// Get the landing page data of the Authors
        /// </summary>
        /// <returns>the landing page data of the Authors</returns>
        RcAuthorLandingPage GetAuthorsLandingPage(long taxonomyId, int languageId);

        /// <summary>
        /// Validate id a category name exist in ektron
        /// </summary>
        /// <param name="catagoryName"></param>
        /// <returns>true if Exist otherwise false</returns>
        bool CatagoryExist(string catagoryName, int brandPartnerId);

        /// <summary>
        /// Validate if an Author has a page ingo due the taxonomy 
        /// </summary>
        /// <param name="authorName"></param>
        /// <param name="taxonomyId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        bool HasAuthorPageAndIsActive(string authorName, long taxonomyId, int languageId);
    }
}
