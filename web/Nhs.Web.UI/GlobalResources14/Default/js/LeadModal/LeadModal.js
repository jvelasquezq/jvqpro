﻿NHS.Scripts.LeadModal = function (parameters) {
    this.parameters = parameters;
};

NHS.Scripts.LeadModal.prototype =
{
    initialize: function () {
        var self = this;
        var form = jQuery("#nhs_ModalRequest");
        form.find("#Phone").mask("(999) 999-9999");
        var international = form.find("#International");
        var zip = form.find("#Zip");
        var labelZip = form.find('label[for="Zip"]');
        this.checkCounts = 0;

        international.change(function () {
            if (international.is(':checked')) {
                zip.attr("disabled", "disabled");
                zip.val("");
                zip.removeClass("nhs_Error");
                labelZip.removeClass("nhs_ErrorLabel");
            } else {
                zip.removeAttr("disabled");
            }
        });

        form.find("#Zip, #Name, #Email").keyup(function () {
            var control = jQuery(this);
            control.removeClass("nhs_Error");
            control.prev("label[for]").removeClass("nhs_ErrorLabel");
        });

        form.find("#IncludeRecoComm").change(function () {
            if (jQuery(this).is(":checked") && self.checkCounts == 0) {
                jQuery.googlepush('Lead Events', NHS.Scripts.Helper.readCookie('CTAName'), 'Option Checked - Send Me More');
                self.checkCounts++;
            }
        });

        jQuery(document).on("click", "#nhs_GotomyAccount", function () {
            jQuery.googlepush('Lead Events', NHS.Scripts.Helper.readCookie('CTAName'), 'Submit Form - Go To My Account');
        });

        jQuery(document).on("click", ".k-datepicker .k-select", function () {
            jQuery.googlepush('Lead Events', NHS.Scripts.Helper.readCookie('CTAName'), 'Option Checked - Appointment Calendar');
        });

        form.find("#AppointmentDateTime").attr("disabled", "disabled");
        jQuery("#nhs_ajaxContent").on("change", "input[type=checkbox]", function () {
            var control = jQuery(this);
            control.val(control.is(":checked"));
        });

        form.find("#nhs_GetBrochure").click(function () {
            var errorList = jQuery("#nhs_ajaxContent #nhs_error");
            errorList.hide();

            var valid = true;
            var control = jQuery(this);
            control.attr("disabled", "disabled");

            //Start Name
            var name = form.find("#Name");
            var labelName = jQuery('label[for="Name"]');
            labelName.removeClass("nhs_ErrorLabel");

            name.removeClass("nhs_Error");
            if (!jQuery.ValidFullNameWithNumberLastName(name.val())) {
                name.addClass("nhs_Error");
                labelName.addClass("nhs_ErrorLabel");
                valid = false;
            }
            //End Name

            //Start Email
            var email = form.find("#Email");
            var labelEmail = jQuery('label[for="Email"]');
            labelEmail.removeClass("nhs_ErrorLabel");

            email.removeClass("nhs_Error");

            if (!jQuery.ValidEmail(email.val())) {
                email.addClass("nhs_Error");
                labelEmail.addClass("nhs_ErrorLabel");
                valid = false;
            }
            //End Email

            //Start Zip
            zip.removeClass("nhs_Error");
            labelZip.removeClass("nhs_ErrorLabel");

            if (!international.is(':checked')) {
                if (!jQuery.ValidZipcode(zip.val())) {
                    zip.addClass("nhs_Error");
                    labelZip.addClass("nhs_ErrorLabel");
                    valid = false;
                }
            }
            //End Zip 

            if (valid) {
                jQuery.googlepush('Lead Events', NHS.Scripts.Helper.readCookie('CTAName'), 'Submit Form - Get Brochure');

                var data = form.find("input, textarea").serialize() + "&AppointmentDateTime=" + form.find("#AppointmentDateTime").val();
                self.GeneralAjaxCallAction(control, data, self.parameters.BrochureUrl, self.initializeRecoModal, self);
            } else {
                errorList.show();
                control.removeAttr("disabled");
            }
        });
    },

    initializeRecoModal: function () {
        var self = this;
        if (jQuery("#nhs_RecoCommsBox").length > 0) {

            jQuery("#nhs_RecoCommsBox .nhs_RecoCommsCheckbox").change(function () {
                if (jQuery("#nhs_RecoCommsBox .nhs_RecoCommsCheckbox").is(":checked")) {
                    //jQuery("#nhs_RecoCommsBox #nhs_GetFreeBrochureFake").hide();
                    //jQuery("#nhs_RecoCommsBox #nhs_GetFreeBrochure").show();
                    jQuery("#nhs_RecoCommsBox #nhs_GetFreeBrochure").removeClass("Inactive");
                    jQuery("#nhs_RecoCommsBox #nhs_GetFreeBrochure").removeAttr("disabled");
                } else {
                    //jQuery("#nhs_RecoCommsBox #nhs_GetFreeBrochure").hide();
                    //jQuery("#nhs_RecoCommsBox #nhs_GetFreeBrochureFake").show();
                    jQuery("#nhs_RecoCommsBox #nhs_GetFreeBrochure").addClass("Inactive");
                    jQuery("#nhs_RecoCommsBox #nhs_GetFreeBrochure").attr("disabled", "disabled");
                }
            });

            jQuery("#nhs_RecoCommsBox #nhs_GetFreeBrochure").click(function () {
                jQuery.googlepush('Lead Events', NHS.Scripts.Helper.readCookie('CTAName'), 'Submit Form - Get Free Brochures');

                var data = jQuery("#nhs_RecoCommsBox input").serialize();
                self.GeneralAjaxCallAction(jQuery("#nhs_RecoCommsBox #nhs_GetFreeBrochure, #nhs_RecoCommsBox #nhs_Skip"), data, self.parameters.RequestRecoCommsUrl, self.initializeThankYouModal, self);
            });

            jQuery("#nhs_RecoCommsBox #nhs_Skip").click(function () {
                jQuery.googlepush('Lead Events', NHS.Scripts.Helper.readCookie('CTAName'), 'Submit Form - Get Free Brochures (Skip)');

                jQuery("#nhs_RecoCommsBox .nhs_RecoCommsCheckbox").removeAttr("checked");
                var data = jQuery("#nhs_RecoCommsBox input").serialize();
                self.GeneralAjaxCallAction(jQuery("#nhs_RecoCommsBox #nhs_GetFreeBrochure, #nhs_RecoCommsBox #nhs_Skip"), data, self.parameters.RequestRecoCommsUrl, self.initializeThankYouModal, self);
            });
        } else {
            self.initializeThankYouModal();
        }
    },

    initializeThankYouModal: function () {
        var self = this;
        jQuery("#nhs_AccountInformation #nhs_Forgot_Password").click(function () {
            var control = jQuery(this);
            var data = { email: jQuery("#nhs_AccountInformation #Email").val() };
            self.GeneralAjaxCallAction(control, data, self.parameters.RecoverPasswordUrl, function () {
                control.replaceWith(self.parameters.YourPasswordWillBeSendText);
            }, self);
        });

        jQuery("#nhs_AccountInformation #nhs_SignIn").click(function () {
            var control = jQuery(this);
            control.attr("disabled", "disabled");
            var valid = true;

            var errorList = jQuery("#nhs_AccountInformation #nhs_error");
            var password = jQuery("#nhs_AccountInformation #password");
            errorList.html("");
            errorList.hide();
            password.removeClass("nhs_Error");
            if (password.val().length < 5 || password.val().length > 20) {
                password.addClass("nhs_Error");
                errorList.show();
                errorList.append("<li>Your password must be between 5 and 20 characters</li>");
                valid = false;
            }

            if (jQuery("#nhs_AccountInformation #IsExistingUser").val() == "False") {
                var confirmPassword = jQuery("#nhs_AccountInformation #confirmPassword");
                confirmPassword.removeClass("nhs_Error");
                if (password.val() != confirmPassword.val()) {
                    errorList.show();
                    password.addClass("nhs_Error");
                    confirmPassword.addClass("nhs_Error");
                    errorList.append("<li>Passwords don't match</li>");
                    valid = false;
                }
            }

            if (valid) {
                if (control.val() == 'Sign in') {
                    jQuery.googlepush('Lead Events', NHS.Scripts.Helper.readCookie('CTAName'), 'Submit Form - Login');
                } else {
                    jQuery.googlepush('Lead Events', NHS.Scripts.Helper.readCookie('CTAName'), 'Submit Form - Create Account');
                }
                
                var data = jQuery("#nhs_AccountInformation input").serialize();
                self.GeneralAjaxCallAction(control, data, self.parameters.ThankYouModalUrl, function (validation) {
                    if (validation)
                        window.location.reload();
                    else {
                        control.removeAttr("disabled");
                        errorList.show();
                        errorList.append("<li>Sorry, the password you entered is not correct. Please check your spelling and try again.</li>");
                    }
                }, self);
            } else
                control.removeAttr("disabled");
        });
    },

    GeneralAjaxCallAction: function (control, data, url, action, self) {

        control.attr("disabled", "disabled");
        jQuery.ajax({
            url: url,
            type: "POST",
            data: data,
            success: function (html) {
                if (typeof html === 'string') {
                    jQuery("#nhs_ajaxContent").html(html);
                    action.call(self);
                } else {
                    action.call(self, html);
                }
            }, error: function () {
                control.removeAttr("disabled");
            }
        });
    }


};