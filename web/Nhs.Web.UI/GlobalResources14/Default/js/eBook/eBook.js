﻿var createeBook = true;

NHS.Scripts.eBook = function (parameters) {
    this.parameters = parameters;
};

NHS.Scripts.eBook.prototype = {
    init: function () {
        if (createeBook) {
            createeBook = false;
            this.AttachEvents();

            var self = this;

            jQuery("#nhs_BookPage .btn_eBookSend").click(function () {
                var control = jQuery("#nhs_BookPage .nhs_eBookEmail");
                self.submit(control, 'eBook Landing Page Opt-in');
                jQuery.googlepush('Lead Events', 'eBook Landing', 'Submit Form - Get the Book');
            });

            jQuery("#nhs_eBookHeader .btn_eBookSend").click(function () {
                var control = jQuery("#nhs_eBookHeader .nhs_eBookEmail");
                self.submit(control, 'eBook Header Opt-in');
                jQuery.googlepush('Lead Events', 'eBook Top Box', 'Submit Form - Get the Book');
            });

            jQuery("#nhs_eBookHomepage .btn_eBookSend").click(function () {
                var control = jQuery("#nhs_eBookHomepage .nhs_eBookEmail");
                self.submit(control, 'eBook Home Page Opt-in');
                jQuery.googlepush('Lead Events', 'Main', 'Submit Form - Get the Book');
            });

            jQuery("#nhs_eBookResourceCenter .btn_eBookSend").click(function () {
                var control = jQuery("#nhs_eBookResourceCenter .nhs_eBookEmail");
                self.submit(control, 'eBook Resource Center Opt-in');
                //jQuery.googlepush('Lead Events', 'Main', 'Submit Form - Get the Book');
            });
            
        }
    },
    submit: function (control, source) {
        var self = this;
        control.removeAttr("style");
        var email = control.val();
        var isValidEmail = jQuery.ValidEmail(email);
        if (email == '' || !isValidEmail) {
            control.attr("style", "border: 1px solid #FF0000");
            jQuery('.nhs_EbookSignUpErrors').html('<p class="nhs_Error">' + self.parameters.ErrorMessage + '</p>').show();
            control.focus();
        } else {
            jQuery('.nhs_EbookSignUpErrors').hide();
            jQuery.ajax({
                url: self.parameters.postUrl,
                data: { 'email': email, 'source': source },
                cache: false,
                type: "POST",
                async: false,
                success: function (data) {
                    if (data) {
                        jQuery('#nhs_eBookHeader').remove();
                        var t = "";
                        var a = self.parameters.popUrl + "?email=" + email + "&marketId=" + self.parameters.marketId;
                        var g = false;
                        if (!self.parameters.IsMobile) {
                            tb_show(t, a, g, function() { return self.LoadDefault(self.parameters.marketId); });
                        }
                        else {
                            window.modalPopUp.SetContentModal(a);
                        }
                        var parentControl = control.parent().parent();
                        parentControl.find(".nhs_eBookFields").remove();
                        parentControl.find(".nhs_eBookPrivacy").remove();
                        parentControl.find(".nhs_eBookThanks").show();

                    }
                },
                error: function (error) {

                }
            });
        }
    },

    AttachEvents: function () {

        var self = this;
        jQuery(document).on("click", ".nhs_CloseModal", function() {
            if (!self.parameters.IsMobile) {
                tb_remove();
            }
            else {
                window.modalPopUp.CloseModal();
            }
        });

        jQuery(document).on("click", "#nhs_eBookSingUp", function () {
            var valid = true;
            var homeOfTheWeek = jQuery("#nhs_homeOfTheWeek").is(":checked");
            var weeklyMarketUpdate = jQuery("#nhs_weeklyMarketUpdate").is(":checked");
            var marketId = jQuery(".nhs_newletter_dropMarket").val();

            jQuery('#nhs_EbookSignUpErrorsModal').html('');
            if (!homeOfTheWeek && !weeklyMarketUpdate) {
                jQuery('#nhs_EbookSignUpErrorsModal').append('<p class="nhs_Error">Please select an option.</p>');
                valid = false;
            }

            if (weeklyMarketUpdate && marketId === "") {
                jQuery('#nhs_EbookSignUpErrorsModal').append('<p class="nhs_Error">Please select a market from the list.</p>');
                valid = false;
            }

            if (valid) {
                var data = {
                    'homeOfTheWeek': homeOfTheWeek,
                    'weeklyMarketUpdate': weeklyMarketUpdate,
                    'marketId': marketId === "" ? 0 : marketId,
                    'email': jQuery("#nhs_eBookEmail").val()
                };
                jQuery.ajax({
                    url: self.parameters.popUrl,
                    data: data,
                    cache: false,
                    type: "POST",
                    success: function (data) {
                        if (data) {
                            jQuery("#nhs_EbookModal").remove();
                            jQuery("#nhs_ebookClose").show();
                        }
                    },
                    error: function (error) {

                    }
                });
            }
        });

        jQuery(document).on("click", ".nhs_eBookOption", function () {
            if ((jQuery("#nhs_homeOfTheWeek").is(":checked") || jQuery("#nhs_weeklyMarketUpdate").is(":checked"))) {
                jQuery("#nhs_eBookSingUp").removeAttr("disabled");
                jQuery("#nhs_eBookSingUp").removeClass("nhs_eBookSingUpDisable");
            } else {
                jQuery("#nhs_eBookSingUp").attr("disabled", "disabled");
                jQuery("#nhs_eBookSingUp").addClass("nhs_eBookSingUpDisable");
            }
        });


        jQuery(".nhs_newletter_dropMarket").change(function () {
            if (jQuery(".nhs_newletter_dropMarket").val() === "") {
                jQuery('#nhs_EbookSignUpErrorsModal').html('');
            }
        });
    },

    LoadDefault: function (marketId) {
        if (marketId > 0)
            jQuery(".nhs_newletter_dropMarket").val(marketId);
    }
};
