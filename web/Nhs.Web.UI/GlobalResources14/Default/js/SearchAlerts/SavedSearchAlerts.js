﻿if (typeof NHS == "undefined") {
    NHS = {};
    NHS.Scripts = {};
}

NHS.Scripts.SavedSearchAlerts = function(parameters) {
    this.parameters = parameters;
}

NHS.Scripts.SavedSearchAlerts.prototype = {
    init: function() {
        this.checkMaxAlertsSaved();
        this.deleteSearchAlertConfirmation();
        this.addToFavorites();
        this.removeAlertResults();
        this.sortAlertResults();
    },

    checkMaxAlertsSaved: function () {
        var self = this;

        if (self.parameters.numberOfAlerts >= 5) {
            jQuery("#addSearchAlertLink").click(function (e) {

                // Do not performed the action
                e.preventDefault();
                alert(self.parameters.savedAlertTopReachedMessage);
            });
        }
    },

    deleteSearchAlertConfirmation: function() {
        var self = this;
        jQuery("#nhs_DeleteSavedSearchAlert").click(function (e) {

            // Show the loading
            jQuery.ShowLoading();

            // Do not perform the action right away
            e.preventDefault();

            var url = jQuery(this).attr("href");

            var confirmation = confirm(self.parameters.deleteConfirmationMessage);

            if (confirmation === true) {
                window.location.href = url;
            } else {
                // Hide the loading
                jQuery.HideLoading();
            }
        });
    },

    addToFavorites: function () {
        var self = this;
        jQuery("#SavedAlertSaveCommunities").click(function (e) {

            // Show the loading
            jQuery.ShowLoading();
            
            // Prevent the default function of the link
            e.preventDefault();
                
            var serializedForm = jQuery("#communities input").serialize();

            jQuery.ajax({
                url: self.parameters.saveToFavoritesUrl,
                data: serializedForm,
                cache: false,
                type: "POST",
                success: function (data) {
                    var messagesDiv = jQuery("#searchAlertMessages");
                    messagesDiv.show();
                    messagesDiv.html(data);
                    jQuery.ScrollTo(0);

                    // Deselect the checkboxes
                    jQuery(".selectCommunity").prop("checked", false);

                    // Hide the loading
                    jQuery.HideLoading();
                },
                error: function (error) {
                    // Hide the loading
                    jQuery.HideLoading();
                }
            });
        });
    },

    removeAlertResults: function() {
        var self = this;

        jQuery("#SavedAlertRemoveCommunities").click(function (e) {

            var confirmation = confirm(self.parameters.SavedAlertRemoveCommunitiesConfirmationText);

            if (confirmation === true) {
                // Show the loading
                jQuery.ShowLoading();

                // Prevent the default function of the link
                e.preventDefault();

                var serializedForm = jQuery("#communities input").serialize();

                jQuery.ajax({
                    url: self.parameters.removeAlertResultUrl,
                    data: serializedForm,
                    cache: false,
                    type: "POST",
                    success: function (data) {
                        // Set the new content to the communities container
                        jQuery("#searchAlertResults").html(data);

                        // The add and remove buttons are displayed on the page as new, so the 
                        // events has to be attached again.
                        self.addToFavorites();
                        self.removeAlertResults();

                        // Deselect the checkboxes
                        jQuery(".selectCommunity").prop("checked", false);

                        // Hide the loading
                        jQuery.HideLoading();
                    },
                    error: function (error) {
                        // Hide the loading
                        jQuery.HideLoading();
                    }
                });
            } 
        });
    },

    sortAlertResults: function() {
        var self = this;

        jQuery("#sortOptions").change(function() {
            // Show the loading
            jQuery.ShowLoading();

            var sortOption = jQuery(this).val();
            var alertId = jQuery("#SelectedSearchAlert_AlertId").val();

            jQuery.ajax({
                url: self.parameters.sortAlertResultUrl,
                data: {
                    alertId: alertId,
                    sortOption: sortOption
                },
                cache: false,
                type: "POST",
                success: function (data) {
                    // Set the new content to the communities container
                    jQuery("#searchAlertResults").html(data);

                    // The add and remove buttons are displayed on the page as new, so the 
                    // events has to be attached again.
                    self.addToFavorites();
                    self.removeAlertResults();

                    // Hide the loading
                    jQuery.HideLoading();
                },
                error: function (error) {
                    // Hide the loading
                    jQuery.HideLoading();
                }
            });
        });
    }
}