﻿NHS.Scripts.OwnerStories = function (parameters) {
    this.parameters = parameters;
    this.youtubePlayer = null;


    this.sliderOptions = {
        $FillMode: 4,                                   //[Optional] The way to fill image in slide, 0 stretch, 1 contain (keep aspect ratio and put all inside slide), 2 cover (keep aspect ratio and cover whole slide), 4 actuall size, default value is 0
        $AutoPlay: false,                               //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
        $DragOrientation: 1,                            //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
        $ArrowKeyNavigation: true,   			        //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
        $SlideDuration: 800,                            //Specifies default duration (swipe) for slide in milliseconds
        $StartIndex: 0,
        $Loop: 0,
        $ArrowNavigatorOptions: {                       //[Optional] Options to specify and enable arrow navigator or not
            $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
            $ChanceToShow: 2,                           //[Required] 0 Never, 1 Mouse Over, 2 Always
            $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
        },
        $BulletNavigatorOptions: {                                //[Optional] Options to specify and enable navigator or not
            $Class: $JssorBulletNavigator$,                       //[Required] Class to create navigator instance
            $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
            $AutoCenter: 1,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
            $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
            $Lanes: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
            $SpacingX: 10,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
            $SpacingY: 10,                                   //[Optional] Vertical space between each item in pixel, default value is 0
            $Orientation: 1                                 //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
        }
    };
};

NHS.Scripts.OwnerStories.prototype = {
    init: function() {
        var self = this;
        jQuery('#tabstrip').show();

        if (self.parameters.showThanksPreviewWindow) {
            self.ShowPreview();
        }

        jQuery('.nhs_OwnersLinkShareStory').click(function() {
            var $anchor = jQuery(this);
            var thankYouActive = jQuery('#nhs_OwnersFormThankYou').length > 0;
            if (!thankYouActive) {
                jQuery('#nhs_OwnersMainDiv').show();
                jQuery.ScrollTo(jQuery("#FirstName").position().top - 50);

                var type = $anchor.attr('id');
                switch (type) {
                case "nhs_OwnersShareDesigned":
                    jQuery("#Category").prop("selectedIndex", 1);
                    break;
                case "nhs_OwnersShareSaves":
                    jQuery("#Category").prop("selectedIndex", 2);
                    break;
                case "nhs_OwnersShareFun":
                    jQuery("#Category").prop("selectedIndex", 3);
                    break;
                default:
                    break;
                }
            }
            return false;
        });

        jQuery(".form_left #FirstName, .form_left #LastName, .form_left #Email, .form_left #BuilderName, .form_left #Category, .form_left #CommunityName, " +
            "   .form_left #City").keyup(function() {
            var control = jQuery(this);
            control.removeClass("nhs_Error");
            control.prev("label[for]").removeClass("nhs_ErrorLabel");
        });

        jQuery(".form_right #StoryDescription").keyup(function() {
            var control = jQuery(this);
            var labelDescription = jQuery('label[for="StoryDescription"]');
            labelDescription.removeClass("nhs_Error");
            control.removeClass("nhs_Error");
        });

        jQuery(".form_left #Category").click(function() {
            var control = jQuery(this);
            control.removeClass("nhs_Error");
            control.prev("label[for]").removeClass("nhs_ErrorLabel");
        });

        jQuery('#nhs_BtnSubmitCurationChanges').click(function() {
            var isValid = self.validateOwnerStoryValues(jQuery("#nhs_BtnSubmitCurationChanges"), false, true);
            if (isValid) {
                return self.curateOwnerStory();
            } else {
                return false;
            }
        });

        jQuery('#nhs_OwnersSendForm').click(function() {
            return self.validateOwnerStoryValues();
        });


        if (jQuery("#nhs_error") && jQuery("#nhs_error").find('li').length > 1) {
            jQuery("#nhs_error").show();
        }
        this.slider = new $JssorSlider$(this.parameters.sliderContainerId, this.sliderOptions);

        jQuery('.nhs_OwnersLinkOthersSaid a').click(function() {
            jQuery('#nhs_OwnersSliderPopup').kendoWindow({
                actions: ["Close"],
                draggable: true,
                modal: true,
                resizable: false,
                title: false,
                minWidth: 800,
                minHeight: 500,
                close: function(e) {

                }
            });
            jQuery('#nhs_OwnersSliderPopup').data("kendoWindow").wrapper.addClass("nhs_OwnersSliderPopup");
            jQuery('#nhs_OwnersSliderPopup').data("kendoWindow").center().open();
            jQuery('#nhs_OwnersSliderPopup').closest(".k-window").css({
                position: 'fixed',
                margin: 'auto',
                top: '20%'
            });
            jQuery(".nhs_OwnersSliderPopup").append('<a class="nhs_OwnersPopupClose" href="javascript:void(0)">X</a>');
            jQuery('.k-overlay').bind('click', function() { self.closeModal(); });
            jQuery('.nhs_OwnersPopupClose').bind('click', function() { self.closeModal(); });
            return false;
        });

        jQuery('.nhs_videoThumbIcon').click(function() {
            var id = jQuery(this).data('id');
            var controlId = jQuery(this).data('control');
            jQuery('#' + controlId).css('display', 'block');

            self.youtubePlayer = new YT.Player(controlId, {
                height: self.parameters.videoHeight,
                width: self.parameters.videoWidth,
                videoId: id,
                playerVars: { autoplay: 1 }
            });

        });

        jQuery('.nhs_OwnersSliderBulletNav, #nhs_OwnersSliderLeftArrow, #nhs_OwnersSliderRightArrow').click((function() { this.stopVideo(); }).bind(this));

        jQuery("#nhs_OwnersCategoriesList a#nhs_OwnersShareDesigned").click(function() {
            jQuery.googlepush('Owner Stories', 'Share Story', 'Share Story - Is Designed');
        });

        jQuery("#nhs_OwnersCategoriesList a#nhs_OwnersSeeDesigned").click(function() {
            jQuery.googlepush('Owner Stories', 'See What Others', 'See What Others - Is Designed');
        });

        jQuery("#nhs_OwnersCategoriesList a#nhs_OwnersShareSaves").click(function() {
            jQuery.googlepush('Owner Stories', 'Share Story', 'Share Story - Saves Me Time');
        });

        jQuery("#nhs_OwnersCategoriesList a#nhs_OwnersSeeSaves").click(function() {
            jQuery.googlepush('Owner Stories', 'See What Others', 'See What Others - Saves Me Time');
        });

        jQuery("#nhs_OwnersCategoriesList a#nhs_OwnersShareFun").click(function() {
            jQuery.googlepush('Owner Stories', 'Share Story', 'Share Story - Was Fun and Easy');
        });

        jQuery("#nhs_OwnersCategoriesList a#nhs_OwnersSeeFun").click(function() {
            jQuery.googlepush('Owner Stories', 'See What Others', 'See What Others - Was Fun and Easy');
        });

        jQuery("input#uploadFile").click(function() {
            jQuery.googlepush('Owner Stories', 'Upload Photo or Video', 'Upload Photo or Video');
        });
    },

    initCuration: function() {
        var self = this;

        jQuery(".form_left #FirstName, .form_left #LastName, .form_left #Email, .form_left #BuilderName, .form_left #Category, .form_left #CommunityName, " +
            "   .form_left #City").keyup(function() {
            var control = jQuery(this);
            control.removeClass("nhs_Error");
            control.prev("label[for]").removeClass("nhs_ErrorLabel");
        });

        jQuery(".form_right #StoryDescription").keyup(function() {
            var control = jQuery(this);
            var labelDescription = jQuery('label[for="StoryDescription"]');
            labelDescription.removeClass("nhs_Error");
            control.removeClass("nhs_Error");
        });

        jQuery(".form_left #Category").click(function() {
            var control = jQuery(this);
            control.removeClass("nhs_Error");
            control.prev("label[for]").removeClass("nhs_ErrorLabel");
        });

        jQuery('#nhs_BtnSubmitCurationChanges').click(function() {
            var isValid = self.validateOwnerStoryValues(jQuery("#nhs_BtnSubmitCurationChanges"), false, true);
            if (isValid) {
                return self.curateOwnerStory();
            } else {
                return false;
            }
        });

        jQuery('#nhs_OwnerStoryCurationPreview').click(function (e) {
            e.preventDefault();
            var isBrightCoveVideo = jQuery("#isBrightCoveVideo").val();
            if (isBrightCoveVideo) {
                var brightCoveImage = jQuery("#ownerStoryBrightcoveImage");
                brightCoveImage.click();
            } else {
                self.showPreviewModal();}
            return false;
        });

        jQuery('#nhs_OwnersSendForm').click(function() {
            return self.validateOwnerStoryValues();
        });


        if (jQuery("#nhs_error") && jQuery("#nhs_error").find('li').length > 1) {
            jQuery("#nhs_error").show();
        }
    },

    showPreviewModal: function() {

        var window = jQuery("#storyImageVideoPreview");
        
        window.kendoWindow({
            actions: [ "Close" ],
            draggable: true,
            width: "340px",
            height: "168px",
            modal: true,
            resizable: false,
            close: function (e) {
                var vid = document.getElementById("ownerStoryVideo");
                if (vid) {
                    vid.pause();
                    vid.currentTime = 0;
                }
                window.hide();
            }
        }).data("kendoWindow").center().open();

        window.closest(".k-window").css({
            position: 'fixed',
            margin: 'auto',
            top: '10%'
        });

        window.show();
    },

    hideLoading: function () {
        jQuery('#fakeInputLoading').hide();
    },
    closeModal: function () {
        jQuery('#nhs_OwnersSliderPopup').data("kendoWindow").close();
        this.stopVideo();
    },
    stopVideo: function () {
        if (this.youtubePlayer != undefined && this.youtubePlayer != null)
            this.youtubePlayer.stopVideo();
    },

    validateOwnerStoryValues: function (inputBtn, isUploadHandler, isCurationProcess) {
        var self = this;
        var returnValue = true;

        jQuery('ul#nhs_error li').each(function() {
            jQuery(this).remove();
        });        

        //Start Name
        var name = jQuery(".form_left #FirstName");
        var lastName = jQuery(".form_left #LastName");
        var labelName = jQuery('label[for="FirstName"]');
        labelName.removeClass("nhs_ErrorLabel");
        name.removeClass("nhs_Error");
        lastName.removeClass("nhs_Error");

        if (name.val().trim() === "") {
            name.addClass("nhs_Error");
            labelName.addClass("nhs_ErrorLabel");
            returnValue = false;
        }

        if (lastName.val().trim() === "") {
            lastName.addClass("nhs_Error");
            labelName.addClass("nhs_ErrorLabel");
            returnValue = false;
        }
        //End Name

        //Start Email
        var email = jQuery(".form_left #Email");
        var labelEmail = jQuery('label[for="Email"]');
        labelEmail.removeClass("nhs_ErrorLabel");
        email.removeClass("nhs_Error");

        if (!ValidEmail(email.val())) {
            email.addClass("nhs_Error");
            labelEmail.addClass("nhs_ErrorLabel");
            returnValue = false;
        }
        //End Email

        //Start Builder
        var builder = jQuery(".form_left #BuilderName");
        var builderId = jQuery(".form_left #BuilderId");
        var validBuilderName = jQuery(".form_left #ValidBuilderName");
        var labelBuilder = jQuery('label[for="BuilderName"]');
        labelBuilder.removeClass("nhs_ErrorLabel");
        builder.removeClass("nhs_Error");

        if (builder.val().trim() === "" || builderId.val().trim() === "0"
            || (builder.val().trim() !== validBuilderName.val().trim())) {
            builder.addClass("nhs_Error");
            labelBuilder.addClass("nhs_ErrorLabel");
            returnValue = false;
        }
        //End Builder

        //Start Community
        var community = jQuery(".form_left #CommunityName");
        var validCommunityName = jQuery(".form_left #ValidCommunityName");
        var labelCommunity = jQuery('label[for="CommunityName"]');
        labelCommunity.removeClass("nhs_ErrorLabel");
        community.removeClass("nhs_Error");

        if (community.val().trim() !== "" && (community.val().trim() !== validCommunityName.val().trim())) {
            community.addClass("nhs_Error");
            labelCommunity.addClass("nhs_ErrorLabel");
            returnValue = false;
        }
        //End Community

        //Start City
        var city = jQuery(".form_left #City");
        var validCity = jQuery(".form_left #ValidCityName");
        var labelCity = jQuery('label[for="City"]');
        labelCity.removeClass("nhs_ErrorLabel");
        city.removeClass("nhs_Error");

        if (city.val().trim() === "" || (validCity.val().trim() !== city.val().trim())) {
            city.addClass("nhs_Error");
            labelCity.addClass("nhs_ErrorLabel");
            returnValue = false;
        }
        //End City

        //Start Description
        var description = jQuery(".form_right #StoryDescription");
        var labelDescription = jQuery('label[for="StoryDescription"]');
        description.removeClass("nhs_Error");

        if (description.val().trim() === "") {
            description.addClass("nhs_Error");
            labelDescription.addClass("nhs_Error");
            returnValue = false;
        }
        //End Description

        //Start Category
        var category = jQuery(".form_left #Category");
        var labelCategory = jQuery('label[for="Category"]');
        category.removeClass("nhs_Error");
        labelCategory.removeClass("nhs_ErrorLabel");

        if (category.val() === "") {
            category.addClass("nhs_Error");
            labelCategory.addClass("nhs_ErrorLabel");
            returnValue = false;
        }
        //End Category

        if (isCurationProcess) {
            if (!returnValue) {
                jQuery("#nhs_error").append('<li>Please correct the highlighted fields above.</li>');
                jQuery(".nhs_Owners_error_wrapper #nhs_error").show();
            }
            return returnValue;
        }

        //Start File
        try {
            var fileExt = jQuery("#nhs_osFormContainer input[type='file']")[0].value.split('.')[1];
            if (fileExt) {
                switch (fileExt.toLowerCase()) {
                case 'png':
                case 'jpg':
                case 'jpeg':
                case 'gif':
                case 'mp4':
                case 'm4v':
                case 'mov':
                    break;
                default:
                    returnValue = false;
                    jQuery("#nhs_error").append('<li>Please select a valid file type.</li>');
                    jQuery('ul.k-upload-files.k-reset').css('border-color', 'red');
                    break;
                }
            }
        } catch (e) {
        }         
        //End File

        if (returnValue) {
            jQuery.googlepush('Owner Stories', 'Submit Form - Owner Stories', 'Submit Form - Owner Stories');
            jQuery(".nhs_Owners_error_wrapper #nhs_error").hide();
            if (!isUploadHandler)
                self.ShowPreview();
        } else {
            jQuery("#nhs_error").append('<li>Please correct the highlighted fields above.</li>');
            jQuery(".nhs_Owners_error_wrapper #nhs_error").show();
            HideLoading();
            jQuery(inputBtn).show();
        }
        return returnValue;
    },

    ShowPreview: function () {
        var self = this;
       
        if (self.parameters.showThanksPreviewWindow == false) {

            var $dataPreview = jQuery('#nhs_OwnersPreviewText');
            var files = jQuery("#nhs_osFormContainer input[type='file']")[0].files;
            
            //old IE do not support .files
            if (files && files.length == 0 || jQuery("#nhs_osFormContainer input[type='file']")[0].value.length == 0) {
                jQuery('.nhs_OwnersPopupMediaPreview').hide();                
                jQuery('#nhs_OwnersPreviewText').removeClass().addClass('nhs_OwnersPreviewPopupFull');
            } else {
                jQuery('.nhs_OwnersPopupMediaPreview').show();
                jQuery('#nhs_OwnersPreviewText').removeClass().addClass('nhs_OwnersPreviewText');
            }

            var template = kendo.template(jQuery('#nhs_OwnersPreviewTextTemplate').html());
            var data = {
                fname: jQuery('#FirstName').val(),
                lname: jQuery('#LastName').val(),
                builder: jQuery('#BuilderName').val(),
                comm: jQuery('#CommunityName').val(),
                city: jQuery('#City').val(),
                desc: jQuery('#StoryDescription').val().split('\n').join('<br />')
            };

            var titlePreview = jQuery('#Category').val();
            jQuery('.nhs_OwnersPopupPreviewContent h2').html(titlePreview); 

            var dataHtml = template(data);

            $dataPreview.html(dataHtml);
        }
        jQuery("#nhs_OwnersPreview").show();

        jQuery('#nhs_OwnersPreview').kendoWindow({
            actions: ["Close"],
            draggable: true,
            modal: true,
            resizable: false,
            title: false,
            close: function (e) {                                
            }
        });

        jQuery('#nhs_OwnersPreviewThanks').hide();
        jQuery('.nhs_OwnersPopupPreviewContent').show();
        jQuery('.nhs_OwnersFormActions input').show();

        jQuery('#nhs_OwnersPreview').data("kendoWindow").wrapper.addClass("nhs_OwnersPreview");
        self.SetPreviewStep(2);
        jQuery('#nhs_OwnersPreview').data("kendoWindow").center().open();
        jQuery('#nhs_OwnersPreview').closest(".k-window").css({
            position: 'fixed',
            margin: 'auto',
            top: '10%'
        });
        jQuery(".nhs_OwnersPreview").append('<a class="nhs_OwnersPopupClose" href="javascript:void(0)">X</a>');
        jQuery('.nhs_OwnersPreview a.nhs_OwnersPopupClose').click(function() {
            jQuery('#nhs_OwnersPreview').data("kendoWindow").close();
            jQuery("#nhs_OwnersPreview").hide();
        });

        jQuery('#nhs_OwnersPreviewSubmit').unbind("click");

        jQuery('#nhs_OwnersPreviewSubmit').click(function() {
            jQuery('.nhs_OwnersSlides .nhs_OwnersFormActions input').hide();
            jQuery('.nhs_OwnersPopupMediaPreview').hide();
            jQuery('#nhs_OwnersPreviewText').removeClass();
            jQuery('#nhs_OwnersPreviewText').html("<div id='nhs_OwnersLoadingSubmit'><img src='"
                + self.parameters.loadingImage
                + "' /></div>");
            self.SetPreviewStep(3);
            self.upLoadHandlerData();                      
        });

        if (self.parameters.showThanksPreviewWindow) {
            jQuery('.nhs_OwnersPopupPreviewContent').hide();
            jQuery('#nhs_OwnersPreviewSubmit').hide();
            jQuery('#nhs_OwnersPreviewThanks').show();
            self.SetPreviewStep(3);
            jQuery.ScrollTo(jQuery("#nhs_OwnersCategories").position().top - 50);
        } else {
            jQuery.ScrollTo(jQuery("#FirstName").position().top - 50);
        }
    },

    SetPreviewStep: function(step) {
        jQuery('.nhs_OwnersSlides .nhs_OwnersFormActions li span').each(function () {
            var $span = jQuery(this);
            $span.removeClass('nhs_Active');
            var dataStep = $span.data('step');
            if (dataStep && dataStep == step) {
                $span.addClass('nhs_Active');
            }            
        });
    },
  
    ErrorHandler: function (status) {
        jQuery("#nhs_error").html('');       
        jQuery('#nhs_OwnersSendForm').show();
        if (status) {
            jQuery("#nhs_error").append('<li>Please check the file size.</li>');
        } else {
            jQuery("#nhs_error").append('<li>Communication Error, please try again.</li>');
        }
        jQuery("#nhs_error").show();
        try {
            jQuery('#nhs_OwnersPreview').data("kendoWindow").close();
        } catch (e) {} 
    },

    upLoadHandlerData: function () {
        var self = this;
        var isIE = typeof FormData == "undefined";

        if (isIE) {
            if (self.validateOwnerStoryValues(jQuery("#nhs_OwnersSendForm"), true, false)) {
                jQuery('#nhs_OwnersIeForm').submit();
            }
        } else {
            var selector = '#nhs_osFormContainer input[type=text], #nhs_osFormContainer textarea,#nhs_osFormContainer select, #nhs_osFormContainer input[type=hidden]';
            var formData = jQuery('#nhs_osFormContainer').serializefiles(selector);
            jQuery.ajax({
                url: '/ownerstoriessaveform',
                type: 'POST',
                cache: false,
                contentType: false,
                dataType: false,
                data: formData,
                processData: false
            }).done(function (data) {
                if (data.HasErrors) {
                    jQuery("#nhs_error").html('');                
                    jQuery('#nhs_OwnersSendForm').show();

                    if (data.Errors && data.Errors.length > 0) {
                        for (var i in data.Errors) {
                            if (data.Errors.hasOwnProperty(i)) {
                                var error = data.Errors[i];
                                jQuery("#nhs_error").append('<li>' + error + '</li>');
                            }
                        }
                    }

                    jQuery("#nhs_error").show();
                    try {
                        jQuery('#nhs_OwnersPreview').data("kendoWindow").close();
                    } catch (e) {}
                } else {
                    self.SetPreviewStep(3);
                    jQuery('#nhs_OwnersMainDiv').html(data.html);
                    jQuery('body,html').scrollTop(jQuery('#nhs_OwnersFormThankYou').position().top - 10);
                    jQuery('.nhs_OwnersPopupPreviewContent').hide();
                    jQuery('#nhs_OwnersPreviewThanks').show();
                    jQuery('#FirstName').val('');
                    jQuery('#LastName').val('');
                    jQuery('#BuilderName').val('');
                    jQuery('#CommunityName').val('');
                    jQuery('#City').val('');
                    jQuery('#StoryDescription').val('');
                }
            }).fail(function (error) {
                    self.ErrorHandler(error.status);
            });
        }
    },

    curateOwnerStory: function() {
        var self = this;

        var selector = '#nhs_osFormContainer input[type=text], #nhs_osFormContainer textarea,#nhs_osFormContainer select, #nhs_osFormContainer input[type=hidden]';
        var isPublished = jQuery("input[id='NoNullOwnerStoryStatus']").is(":checked");
        var formData = jQuery('#nhs_osFormContainer').serializefiles(selector);
        formData.append("OwnerStoryStatus", isPublished);
        jQuery.ajax({
            url: '/ownerstories-curationsaveform',
            type: 'POST',
            cache: false,
            contentType: false,
            dataType: false,
            data: formData,
            processData: false
        }).done(function(data) {
            if (data.HasErrors) {
                jQuery("#nhs_error").html('');
                jQuery('#nhs_OwnersSendForm').show();

                if (data.Errors && data.Errors.length > 0) {
                    for (var i in data.Errors) {
                        var error = data.Errors[i];
                        jQuery("#nhs_error").append('<li>' + error + '</li>');
                    }
                }
                jQuery("#nhs_error").show();
            } else {
                alert("Your changes have been successfuly saved.");
                
            }
        }).fail(function(error) {
            self.ErrorHandler(error.status);
        });
    }
};
