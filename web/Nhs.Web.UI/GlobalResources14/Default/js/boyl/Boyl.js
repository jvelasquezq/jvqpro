﻿NHS.Scripts.Boyl = function (parameters) {
    this.parameters = parameters;
    this._typeAheadUrl = parameters.typeAheadUrl;
}

NHS.Scripts.Boyl.prototype = {
    init: function() {
        var self = this;

        // Set the first market as default
        jQuery('#ddlState').selectedIndex = 0;

        // Load the markets of the selected state
        jQuery('#ddlState').change(function () {
            self.getMarketsByState();
        });

        // validate form on submit
        jQuery('.btn_FindBuilders').click(self.validate);

        // init the zip code autocomplete
        self.initAutoComplete();
    },

    validate: function() {
        var txtZip = $jq('#txtPostalCode');
        var zip = txtZip.val();
        var state = $jq('#ddlState').val();
        var market = $jq('#ddlMarket').val();
        var messageContainer = $jq('#divMessage');
        var message = jQuery('<p class=\'field-validation-error\'>' + this.parameters.message + '</p>');

        messageContainer.html('');

        if (zip == '') {
            if (state == '' || market == '0') {    //Please uncomment that when market list be fixed
                messageContainer.html(message);
                return false;
            }
        }
        else {
            if (zip.length < 5) {
                messageContainer.html(message);
                txtZip.val('');
                return false;
            }
            else {
                if (zip.length > 5) {
                    txtZip.val(zip.substring(0, 5));
                }

                if (ValidZipcode(zip) == false) {
                    messageContainer.html(message);
                    txtZip.val('');
                    return false;
                }
            }
        }

        return true;
    },

    getMarketsByState: function () {
        // Get the selected state
        var stateAbbreviation = jQuery("#ddlState").val();

        // Make the ajax called in order to get the Markets
        var actionUrl = "/boylsearch/ChageState";

        jQuery.ajax({
            type: "POST",
            url: actionUrl,
            data: { 'StateAbbr': stateAbbreviation },
            dataType: "html",
            success: function (htmlResult) {      // This is how I call the callback function and pass it parameters
                jQuery("#pnlMarket").html(htmlResult);
            },
            error: function (xhr) {

            }
        });
    },

    initAutoComplete: function () {
        var self = this;
        var searchbox = jQuery("#txtPostalCode");
        searchbox.autocomplete({
            source: self._typeAheadUrl,
            open: function () {
                //jQuery(this).data("ui-autocomplete").menu.element.scrollTop(0);        
                jQuery('.ui-menu').scrollTop(0);
            }
        });
        
    }
}