﻿if (typeof NHS == "undefined") {
    NHS = {};
    NHS.Scripts = {};
}

NHS.Scripts.GoogleMapApi = function (options) {
    var defaultOptiosTemp = {};
    jQuery.extend(true, defaultOptiosTemp, this.defaultOptios);
    this.options = jQuery.extend(true, defaultOptiosTemp, options);
    this.markersArray = [];
    this.infowindow = new google.maps.InfoWindow(this.options.InfoWindowOptions);
    this.infowindowTooltip = new google.maps.InfoWindow(this.options.InfoWindowOptions);
    this.map = null;
    this.bounds = new google.maps.LatLngBounds();
    this.isMapCreate = false;
};

NHS.Scripts.GoogleMapApi.prototype =
{
    defaultOptios: {
        MapOptions: {
            Latitude: null,
            Longitude: null,
            Zoom: 8,
            ContainerName: 'map-canvas',
            zoomControl: true,
            ZoomControlStyle: 'LARGE',
            zoomControlPosition: null,
            disableDefaultUI: false,
            Draggable: true,
            Scrollwheel: true,
            DisableDoubleClickZoom: false,
            panControl: false,
            overviewControl: false,
            streetViewControl: false
        },
        CreateMapWrap: true,
        DirectionsDisplayPanel: null,
        MarkerPointOptions: {
            optimized: true
        },
        Loader: "",
        MinZoom: 3,
        MaxZoom: null,
        MarkerClustererOptions: {
            gridSize: 50,
            maxZoom: 15,
            cssName: null,
            imagePath: null,
            zoomOnClick: true,
            minimumClusterSize: null
        },
        UseClusterer: false,
        InfoWindowOptions: {
            disableAutoPan: false
        },
        MarkerPointAsynOptions: {
            Listener: 'idle',
            UrlServer: null
        },
        ProcessResultOptions: {
            Latitude: '',
            Longitude: '',
            Name: ''
        },
        Events: {
            OnMapCreate: null, //Triggers when the maps was created
            OnMarkersCreate: null,
            OnMarkersRemove: function () {
            }, //Triggers when the marks was remove for the map
            OnGetMarkerPointAsyn: function (opt) {
            },
            MarkerClickCallBack: null,
            IdleCallBack: null,
            MarkerMouseOverCallBack: null,
            MarkerMouseOutCallBack: null,
            ZoomChangesCallBack: null,
            MarkerClusterClickCallBack: null,
            DragChangesCallBack: null,
            ClickCallBack: null,
            InfowindowTooltipReadyCallBack: null,
            InfowindowCloseCallBack: null,
            InfowindowTooltipCloseCallBack: null,
            InfowindowReadyCallBack: null,
            tInfowindowOffsetCallBack: null,
            OnBoundsChanged: null
        },
        Autocomplete: {
            CreateControl: false,
            BindToBound: false,
            Autocompletecontrol: 'pac-input'
        },
        pOIIcons: {
            school: "http://maps.google.com/mapfiles/kml/pal2/icon2.png",
            church: "http://maps.google.com/mapfiles/kml/pal2/icon11.png",
            park: "http://maps.google.com/mapfiles/kml/pal2/icon12.png",
            university: "http://maps.google.com/mapfiles/kml/pal2/icon14.png",
            _default: "http://maps.google.com/mapfiles/kml/pal4/icon39.png"
        }
    },
    //Create the map in the html
    createMap: function () {
        var self = this;
        var intMapOptions = self.options.MapOptions;
        var mcOptions = self.options.MarkerClustererOptions;
        var mapOptions = {
            zoom: intMapOptions.Zoom,
            center: new google.maps.LatLng(intMapOptions.Latitude, intMapOptions.Longitude),
            zoomControl: intMapOptions.zoomControl,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle[intMapOptions.ZoomControlStyle],
                position: google.maps.ControlPosition[intMapOptions.zoomControlPosition]
            },
            disableDefaultUI: intMapOptions.disableDefaultUI,
            draggable: intMapOptions.Draggable,
            scrollwheel: intMapOptions.Scrollwheel,
            disableDoubleClickZoom: intMapOptions.DisableDoubleClickZoom,
            panControl: intMapOptions.panControl,
            streetViewControl: intMapOptions.streetViewControl,
            overviewMapControl: intMapOptions.overviewMapControl,
            maxZoom: self.options.MaxZoom,
            minZoom: self.options.MinZoom
        };

        if (self.options.CreateMapWrap)
            self.createMapWrap();

        self.map = new google.maps.Map(document.getElementById(intMapOptions.ContainerName), mapOptions);

        google.maps.Map.prototype.clearOverlays = function () {
            self.clearOverlays();
        };

        if (jQuery.isFunction(self.options.Events.ClickCallBack)) {
            google.maps.event.addListener(self.map, 'click', function () {
                self.options.Events.ClickCallBack.call(self);
            });
        }

        google.maps.event.addListener(self.map, 'dragend', function () {
            if (jQuery.isFunction(self.options.Events.DragChangesCallBack))
                self.options.Events.DragChangesCallBack.call(self);
        });

        if (jQuery.isFunction(self.options.Events.OnBoundsChanged))
            google.maps.event.addListener(self.map, 'bounds_changed', function () {
                self.options.Events.OnBoundsChanged.call(self);
            });

        if (window['MarkerClusterer'])
            self.markerCluster = new MarkerClusterer(self.map, [], mcOptions);

        self.getMarkerPointAsyn();
        self.CreateOverlay();

        if (jQuery.isFunction(self.options.Events.InfowindowTooltipReadyCallBack)) {
            google.maps.event.addListener(self.infowindowTooltip, 'domready', function () {
                self.options.Events.InfowindowTooltipReadyCallBack.call(self);
            });
        }

        if (jQuery.isFunction(self.options.Events.InfowindowReadyCallBack)) {
            google.maps.event.addListener(self.infowindow, 'domready', function () {
                self.options.Events.InfowindowReadyCallBack.call(self);
            });
        }

        if (jQuery.isFunction(self.options.Events.InfowindowTooltipCloseCallBack)) {
            google.maps.event.addListener(self.infowindowTooltip, 'closeclick', function () {
                self.options.Events.InfowindowTooltipCloseCallBack.call(self);
            });
        }

        if (jQuery.isFunction(self.options.Events.InfowindowCloseCallBack)) {
            google.maps.event.addListener(self.infowindow, 'closeclick', function () {
                self.options.Events.InfowindowCloseCallBack.call(self);
            });
        }

        if (jQuery.isFunction(self.options.Events.MarkerClusterClickCallBack)) {
            google.maps.event.addListener(self.markerCluster, 'clusterclick', function (cluster) {
                self.options.Events.MarkerClusterClickCallBack.call(self, cluster);
            });
        }

        if (jQuery.isFunction(self.options.Events.ZoomChangesCallBack)) {
            google.maps.event.addListener(self.map, 'zoom_changed', function () {
                self.options.Events.ZoomChangesCallBack.call(self, self.map.getZoom());
            });

        }


        google.maps.event.addListener(self.map, 'idle', function () {
            self.RemoveGoogleInfo();
            if (jQuery.isFunction(self.options.Events.IdleCallBack))
                self.options.Events.IdleCallBack.call(self);

            if (jQuery.isFunction(self.options.Events.OnMapCreate) && !self.isMapCreate) {
                self.options.Events.OnMapCreate();
            }

            google.maps.event.trigger(self.map, 'resize');
            self.isMapCreate = true;
        });
        google.maps.event.addListenerOnce(self.map, 'tilesloaded', self.RemoveGoogleInfo);
        self.hideLoading();
    },
    createMapWrap: function () {
        var self = this;
        var intMapOptions = self.options.MapOptions;
        jQuery('#' + intMapOptions.ContainerName).wrap('<div id="nhs_mapwrap" style="position:relative;" />');
        jQuery('#nhs_mapwrap').append('<div id="nhs_OverlayMap" style="position: absolute;z-index:99999;top: 0px;left: 0px;height:100%;width:100%;background-color: #000;filter: alpha(opacity=35);-moz-opacity: 0.35;opacity: 0.35;"></div>');
        jQuery('#nhs_mapwrap').append('<div id="nhs_LoadingMap" class="nhs_Loading" style="position:absolute;"><p>Updating</p></div>');
        self.showLoading();
    },

    hideLoading: function () {
        jQuery("#nhs_LoadingMap, #nhs_OverlayMap").hide();
        if (jQuery(".gm-style > div > a").attr("href")) {
            setTimeout(function () {
                var aux;
                var len = jQuery('.gm-style-cc').length;
                for (aux = 1; aux < len; aux += 1) {
                    jQuery('.gm-style-cc').eq('-1').remove();
                }
                jQuery(".gm-style > div > a").removeAttr("href").removeAttr("title");
            }, 800);
        }
    },

    showLoading: function () {
        jQuery("#nhs_LoadingMap, #nhs_OverlayMap").show();
    },

    //Remove the Marker Points
    clearOverlays: function () {
        var self = this;
        for (var i = 0; i < self.markersArray.length; i++) {
            self.markersArray[i].setMap(null);
        }
        self.markersArray = [];
        self.bounds = new google.maps.LatLngBounds();
        if (self.markerCluster)
            self.markerCluster.clearMarkers();

        self.options.Events.OnMarkersRemove();
    },

    //Get the information for the Markers from the server
    getMarkerPointAsyn: function () {
        var self = this;
        var optionsMpa = self.options.MarkerPointAsynOptions;
        if (optionsMpa.Listener && optionsMpa.UrlServer) {
            self.options.Events.OnGetMarkerPointAsyn(optionsMpa);
            google.maps.event.addListener(self.map, optionsMpa.Listener, function () {
                self.showLoading();
                var data = self.getBoundsFromMap();
                self.map.clearOverlays();
                jQuery.getJSON(optionsMpa.UrlServer, data, function (results) {
                    self.processResult(results);
                });
            });
        } else {
            self.hideLoading();
        }
    },

    // Get the Bounds from the map
    getBoundsFromMap: function () {
        var self = this;
        var bounds = self.map.getBounds();
        var ne = bounds.getNorthEast();
        var sw = bounds.getSouthWest();
        var data = {
            minLat: sw.lat(),
            minLng: sw.lng(),
            maxLat: ne.lat(),
            maxLng: ne.lng()
        };
        return data;
    },

    processResult: function (results) {
        var self = this;
        self.showLoading();
        var prOptions = self.options.ProcessResultOptions;
        if (results !== null) {
            for (var i = 0; i < results.length; i++) {
                var html = self.getHtmlInfowindow(results, results[i]);
                var icon = self.getIconMarkerPoint(results, results[i]);
                var name = self.getNameMarkerPoint(results, results[i]);
                self.createMarkerPoint(results[i][prOptions.Latitude], results[i][prOptions.Longitude], name, html, icon, results[i]);
            }
        }
        self.hideLoading();
    },

    getHtmlInfowindow: function (sources, data) {
        return null;
    },

    getIconMarkerPoint: function (sources, data) {
        return null;
    },

    getNameMarkerPoint: function (sources, data) {
        var self = this;
        var prOptions = self.options.ProcessResultOptions;
        return data[prOptions.Name];
    },

    // A function to create the marker and set up the event window
    createMarkerPoint: function (lat, lng, name, html, icon, info) {
        var self = this;
        var marker = self._createMarkerPoint(lat, lng, name, html, icon, info);
        if (self.options.UseClusterer == false)
            marker.setMap(self.map);
        else if (self.markerCluster)
            self.markerCluster.addMarker(marker);

        self.markersArray.push(marker);
    },

    _createMarkerPoint: function (lat, lng, name, html, icon, info) {
        var self = this;
        var position = new google.maps.LatLng(lat, lng);
        var markerPointOptions = self.options.MarkerPointOptions;
        var options = {
            position: position,
            icon: icon,
            title: name,
            optimized: markerPointOptions.optimized,
            zIndex: 99
        };

        self.bounds.extend(position);
        var marker = new google.maps.Marker(options);

        if (jQuery.isFunction(self.options.Events.OnMarkersCreate))
            self.options.Events.OnMarkersCreate(info, self.infowindow, marker);

        if (typeof (html) == "string") {
            google.maps.event.addListener(marker, 'click', function () {
                //self.marker = marker;
                self.infowindow.setContent(html);
                self.infowindow.open(self.map, marker);
                if (jQuery.isFunction(self.options.Events.MarkerClickCallBack)) {
                    self.options.Events.MarkerClickCallBack.call(self, info, self.infowindow, self.infowindowTooltip, marker);
                }
            });
        }

        if (typeof (html) != "string" && jQuery.isFunction(self.options.Events.MarkerClickCallBack)) {
            google.maps.event.addListener(marker, 'click', function () {
                self.options.Events.MarkerClickCallBack.call(self, info, self.infowindow, self.infowindowTooltip, marker);
            });
        }

        if (jQuery.isFunction(self.options.Events.MarkerMouseOverCallBack)) {
            google.maps.event.addListener(marker, 'mouseover', function () {
                self.options.Events.MarkerMouseOverCallBack.call(self, info, self.infowindow, self.infowindowTooltip, marker);
            });
        }

        if (jQuery.isFunction(self.options.Events.MarkerMouseOutCallBack)) {
            google.maps.event.addListener(marker, 'mouseout', function () {
                self.options.Events.MarkerMouseOutCallBack.call(self, info, self.infowindow, self.infowindowTooltip, marker);
            });
        }
        return marker;
    },

    AutoFit: function () {
        var self = this;
        self.map.fitBounds(self.bounds);
        self.map.panToBounds(self.bounds);
    },
    CreateOverlay: function () {
        this.ourOverlay = new google.maps.OverlayView();
        this.ourOverlay.draw = function () {
        };
        this.ourOverlay.setMap(this.map);
    },
    GetInfowindowOffset: function (iwWidth, iwHeight, map, marker) {
        var xOffset = 0;
        var yOffset = 0;
        // Our point of interest
        var location = this.ourOverlay.getProjection().fromLatLngToContainerPixel(marker.getPosition());

        // Get Edges of map in pixels: Sout West corner and North East corner
        var swp = this.ourOverlay.getProjection().fromLatLngToContainerPixel(map.getBounds().getSouthWest());
        var nep = this.ourOverlay.getProjection().fromLatLngToContainerPixel(map.getBounds().getNorthEast());

        // Horizontal Adjustment
        if (location.x < iwWidth / 2) {
            xOffset = iwWidth / 2 - location.x;
        } else if (location.x > nep.x - iwWidth / 2) {
            xOffset = (nep.x - iwWidth / 2) - location.x;
        }

        // Vertical Adjustment
        if (location.y < iwHeight) {
            yOffset = location.y + iwHeight - (location.y - nep.y);
        } else {
            yOffset = location.y + 20 - (location.y - nep.y);
        }
        // Return it
        return new google.maps.Size(xOffset, yOffset);

    },

    GetPixelFromLatLng: function (latLng) {
        var projection = this.map.getProjection();
        var point = projection.fromLatLngToPoint(latLng);
        return point;
    },

    RemoveGoogleInfo: function () {
        if (jQuery(".gm-style > div > a").attr("href")) {
            var aux;
            var len = jQuery('.gm-style-cc').length;
            for (aux = 1; aux < len; aux += 1) {
                jQuery('.gm-style-cc').eq('-1').remove();
            }
            //jQuery(".gm-style > .gmnoprint").css("right", " 0");
            jQuery(".gm-style > div > a").removeAttr("href").removeAttr("title");
        }
    }
};
