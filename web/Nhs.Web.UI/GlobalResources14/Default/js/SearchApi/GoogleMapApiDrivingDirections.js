﻿//Star Driving Directions
NHS.Scripts.GoogleMapApi.prototype.initializeDrivingDirections = function (start, end) {
    var self = this;
    var directionsService = new google.maps.DirectionsService();
    var directionsDisplay = new google.maps.DirectionsRenderer();

    directionsDisplay.setMap(self.map);
    if (self.options.DirectionsDisplayPanel)
        directionsDisplay.setPanel(document.getElementById(self.options.DirectionsDisplayPanel));
    var request = {
        origin: start,
        destination: end,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService.route(request, function (response, status) {
        self.proccessDirectionsService(response, status, directionsDisplay);
    });
};

NHS.Scripts.GoogleMapApi.prototype.proccessDirectionsService = function (response, status, directionsDisplay) {
    if (status == google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
    }
};
//End Driving Directions