﻿

//Star Points of Interests
NHS.Scripts.GoogleMapApi.prototype.addShowPointOfInt = function (opt) {
    var self = this;

    // Services: Places
    var request = {
        bounds: self.map.getBounds(),
        types: opt.Types
    };

    for (var i = 0; i < opt.Array.length; i++) {
        opt.Array[i].setMap(null);
    }

    opt.Array = [];

    if (opt.Show) {
        var service = new google.maps.places.PlacesService(self.map);
        service.search(request, function(results, status) {
            if (status == google.maps.places.PlacesServiceStatus.OK) {
                if (results.length > 0) {
                    for (var i = 0; i < results.length; i++) {
                        var marker = self.createMarkerPOI(results[i], opt);
                        opt.Array.push(marker);
                    }
                }
            }
        });
    } 
};


NHS.Scripts.GoogleMapApi.prototype.createMarkerPOI = function (place, opt) {
    var self = this;
    var iconUrl = self.GetPOIIcon(place, opt);

    var marker = new google.maps.Marker({
        map: self.map,
        position: place.geometry.location,
        icon: iconUrl,
        title: place.name
    });

    google.maps.event.addListener(marker, 'click', function () {
      self.infowindow.setContent(place.name + '<br/>' + place.vicinity);
      self.infowindow.open(self.map, this);
    });
    return marker;
};


NHS.Scripts.GoogleMapApi.prototype.GetPOIIcon = function (place, opt) {
    var self = this;
    var iconUrl;
    switch (place.types[0]) {
        case 'school':
            iconUrl = self.options.pOIIcons.school;
            break;
        case 'church':
            iconUrl = self.options.pOIIcons.church;
            break;
        case 'park':
            iconUrl = self.options.pOIIcons.park;
            break;
        case 'university':
            iconUrl = self.options.pOIIcons.university;
            break;
        default:
            iconUrl = self.options.pOIIcons._default;
    }
    return iconUrl;
};

