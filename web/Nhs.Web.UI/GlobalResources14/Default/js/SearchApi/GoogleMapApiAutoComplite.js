﻿ //Start AutoComplite
NHS.Scripts.GoogleMapApi.prototype.initializeAutoComplete = function() {
    var self = this;
    var control;
    if (self.options.Autocomplete.CreateControl)
        control = self.createControles();
    else
        control = document.getElementById(self.options.Autocomplete.Autocompletecontrol);


    jQuery(control).on("paste", function () {
        return setTimeout(function () {
            var field, val;
            field = jQuery(control);
            val = field.val();
            field.focus();
            field.val(val);
            return field.focus();
        }, 1);
    });

    var autocomplete = new google.maps.places.Autocomplete(control);

    if (self.options.Autocomplete.CreateControl)
        self.initializeAutoCompleteControles(autocomplete);
    if (self.options.Autocomplete.BindToBound)
        autocomplete.bindTo('bounds', self.map);

    var marker = new google.maps.Marker({
        map: self.map
    });

    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        self.placeChangedProcess(marker, autocomplete);
    });
};

NHS.Scripts.GoogleMapApi.prototype.placeChangedProcess = function(marker, autocomplete) {
    var self = this;
    self.infowindow.close();
    marker.setVisible(false);
    var place = autocomplete.getPlace();
    if (!place.geometry) {
        return;
    }

    // If the place has a geometry, then present it on a map.
    if (place.geometry.viewport) {
        self.map.fitBounds(place.geometry.viewport);
    } else {
        self.map.setCenter(place.geometry.location);
        self.map.setZoom(17); // Why 17? Because it looks good.
    }

    var icon = self.iconAutoComplite(place);
    marker.setIcon(icon);
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);

    var address = '';
    if (place.address_components) {
        address = [
            (place.address_components[0] && place.address_components[0].short_name || ''),
            (place.address_components[1] && place.address_components[1].short_name || ''),
            (place.address_components[2] && place.address_components[2].short_name || '')
        ].join(' ');
    }

    self.infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
    self.infowindow.open(self.map, marker);
};

NHS.Scripts.GoogleMapApi.prototype.iconAutoComplite = function(place) {
    var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(35, 35)
    };
    return icon;
};

NHS.Scripts.GoogleMapApi.prototype.createControles = function() {
    var self = this;
    var map = document.getElementById(self.options.MapOptions.ContainerName);
    var newNode = jQuery('<div><input id="pac-input" class="controls" type="text" placeholder="Enter a location" / >' +
        '<div id="type-selector" class="controls">' +
        '<input type="radio" name="type" id="changetype-all" checked="checked">' +
        '<label for="changetype-all">All</label>' +
        '<input type="radio" name="type" id="changetype-establishment">' +
        '<label for="changetype-establishment">Establishments</label>' +
        '<input type="radio" name="type" id="changetype-geocode">' +
        '<label for="changetype-geocode">Geocodes</label>' +
        '</div></div>')[0];
    map.parentNode.insertBefore(newNode, map.nextSibling);
    return document.getElementById('pac-input');
};

    // Sets a listener on a radio button to change the filter type on Places
    // Autocomplete.
NHS.Scripts.GoogleMapApi.prototype.initializeAutoCompleteControles = function(autocomplete) {
    var self = this;
    jQuery("#changetype-all").change(function() {
        autocomplete.setTypes([]);
    });

    jQuery("#changetype-establishment").change(function() {
        autocomplete.setTypes(['establishment']);
    });

    jQuery("#changetype-geocode").change(function() {
        autocomplete.setTypes(['geocode']);
    });
    var input = document.getElementById('pac-input');
    var types = document.getElementById('type-selector');
    self.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    self.map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);
};
    //End AutoComplite