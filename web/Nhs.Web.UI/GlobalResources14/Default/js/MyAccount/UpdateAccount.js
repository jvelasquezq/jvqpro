﻿NHS.Scripts.UpdateAccount = function (params) {
    this.params = params;
};

NHS.Scripts.UpdateAccount.prototype =
{
    initialize: function() {
        var self = this;
        
        var nonInternationalPanelShow = self.params.InternationalPhone;
        jQuery("#InternationalPhone").change(function() {
            jQuery('#nonInternationalPanel').toggle(nonInternationalPanelShow);
            nonInternationalPanelShow = !nonInternationalPanelShow;
        });

        jQuery("#btnUpdate").click(function() {

            jQuery(this).hide();
            jQuery("#btnUpdateFake").show();
            jQuery(".validation-summary-errors").remove();

            var info = jQuery("#nhs_ModalContainer input,select").serialize();

            jQuery.ajax({
                type: "Post",
                url: self.params.UpdateAccount,
                data: info,
                success: function(data) {
                    if (typeof data == "boolean") {
                        alert(self.params.Message);
                        if (jQuery('#TelerikWindow').data('kendoWindow') !== null && jQuery('#TelerikWindow').data('kendoWindow') !== "undefined")
                            jQuery('#TelerikWindow').data('kendoWindow').close();
                        else {
                            jQuery("#btnUpdateFake").hide();
                            jQuery("#btnUpdate").show();
                        }
                    } else {
                        jQuery("#nhs_ModalContainer").replaceWith(data);
                        jQuery("#btnUpdateFake").hide();
                        jQuery("#btnUpdate").show();
                    }
                }
            });
        });
    },

    initializePro: function () {
        var self = this;
        jQuery('#InternationalPhone').change(function () {
            jQuery('nonInternationalPanel').toggle();
        });

        var zbox = '#ZipCode';
        var mkbox = '#HomeMarket';
        var hdnbox = '#hdnHomeMarket';
        var cbox = '#CityPro';
        var sbox = '#StatePro';
        var hdncity = '#hdnCity';
        var hdnstate = '#hdnState';

        self.PrePopValuesAccount(zbox, mkbox, hdnbox, cbox, sbox, hdncity, hdnstate);
        var enteredZip = jQuery(zbox).val();
        if (ValidZipcode(enteredZip)) {
            self.FillHomeMarketUpdate(enteredZip, mkbox, hdnbox, cbox, sbox, hdncity, hdnstate);
        } else {
            jQuery(mkbox).val("");
            jQuery(cbox).val("");
            jQuery(sbox).val("");
            jQuery(hdncity).val("");
            jQuery(hdnstate).val("");
        }
    },

    PrePopValuesAccount: function (zBoxid, mkBoxid, hdnBoxid, cBoxid, sBoxid, hdnBoxCid, hdnBoxSid) {
        var self = this;
        var enteredZip = "";

        jQuery(zBoxid).keyup(function() {
            enteredZip = jQuery(zBoxid).val();
            if (IsValidZipCommon(enteredZip)) {
                self.FillHomeMarketUpdate(enteredZip, mkBoxid, hdnBoxid, cBoxid, sBoxid, hdnBoxCid, hdnBoxSid);
            } else {
                jQuery(mkBoxid).val("");
                jQuery(cBoxid).val("");
                jQuery(sBoxid).val("");
                jQuery(hdnBoxCid).val("");
                jQuery(hdnBoxSid).val("");
            }
        });
    },

    FillHomeMarketUpdate: function (zip, mkBoxid, hdnBoxid, cBoxid, sBoxid, hdnBoxCid, hdnBoxSid) {
        var self = this;
        try {
            var homeMarketBox = jQuery(mkBoxid);
            var homeMarketId = jQuery(hdnBoxid);
            var cityBox = jQuery(cBoxid);
            var stateBox = jQuery(sBoxid);
            var cityBoxH = jQuery(hdnBoxCid);
            var stateBoxH = jQuery(hdnBoxSid);


            if (zip != null && zip != '') {
                jQuery.ajax({
                    type: "GET",
                    url: self.params.FillHomeMarketUpdate,
                    data: { zip: zip },
                    success: function(data) {
                        if (data != null && data != '') {
                            homeMarketBox.val(data.split(',')[0]);
                            homeMarketId.val(data.split(',')[1]);
                            cityBox.val(data.split(',')[2]);
                            stateBox.val(data.split(',')[3]);
                            cityBoxH.val(data.split(',')[2]);
                            stateBoxH.val(data.split(',')[3]);
                        }
                    }
                });
            }
        } catch (ex) {

        }

    }
};
