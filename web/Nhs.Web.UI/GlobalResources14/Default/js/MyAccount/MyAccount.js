﻿NHS.Scripts.MyAccount = function (params) {
    this.params = params;
};

NHS.Scripts.MyAccount.prototype =
{
    initialize: function () {
        var self = this;
        this.checkMaxAlertsSaved();
        jQuery("#pro_SelectAllHome").change(function () {
            jQuery(".pro_SavedHomeCheckBox").prop('checked', jQuery("#pro_SelectAllHome").is(':checked'));
        });

        jQuery('.pro_SaveCommFreeBrochure .btn_FreeBrochure, .pro_SaveHomeFreeBrochure .btn_FreeBrochure').on('click', function () {
            NHS.Scripts.Helper.createCookie('CTAName', 'Saved Properties CTA Free Brochure');
        });

        jQuery("#pro_SelectAllCom").change(function () {
            jQuery(".pro_SavedCommCheckBox").prop('checked', jQuery("#pro_SelectAllCom").is(':checked'));
        });

        jQuery("#pro_SavedHomeRemoveLink").click(function () {

            var deletes = jQuery(".pro_SavedHomeCheckBox:checked");

            if (deletes.length > 0)
                jQuery.ShowLoading();
            
            for (var i = 0; i < deletes.length; i++) {
                var process =0;
                var id = jQuery(deletes[i]).attr("id").split('_');
                var isSpec = id[1];
                var listingId = id[0];
                var data = { isSpec: isSpec, listingId: listingId };
                jQuery.ajax({
                    type: "Post",
                    url: self.params.DeleteFavoriteHome,
                    data: data,
                    success: function (result) {
                        process++;
                        if (result.valid) {
                            jQuery("#pro_HomeRow_" + result.id).remove();
                            jQuery("#pro_homefavorite").html(self.params.Homes + ": " + jQuery(".nhs_HomeResultsRow").length + " " + self.params.SavedPluralFemale);
                            if (jQuery(".nhs_HomeResultsRow").length === 0) {
                                jQuery("#pro_SavedHomeRemove, #nhs_TabsRemoveHome").remove();
                            }
                        }
                        if (process == deletes.length)
                            jQuery.HideLoading();
                    }
                });
            }
        });

        jQuery("#pro_SavedCommRemoveLink").click(function () {

            var deletes = jQuery(".pro_SavedCommCheckBox:checked");
            if (deletes.length > 0)
                jQuery.ShowLoading();

             for (var i = 0; i < deletes.length; i++) {
                var process = 0;
                var id = jQuery(deletes[i]).attr("id").split('_');
                var builderId = id[1];
                var communityId = id[0];
                var data = { builderId: builderId, communityId: communityId };
                jQuery.ajax({
                    type: "Post",
                    url: self.params.DeleteFavoriteComm,
                    data: data,
                    success: function (result) {
                        process++;
                        if (result.valid) {
                            jQuery("#pro_CommRow_" + result.id).remove();
                            jQuery("#pro_commfavorite").html(self.params.Communities + ": " + jQuery(".nhs_CommResultsRow").length  + " " + self.params.SavedPluralFemale);
                            if (jQuery(".nhs_CommResultsRow").length == 0) {
                                jQuery("#pro_SavedCommRemove, #nhs_TabsRemoveCom").remove();
                            }
                        }
                        if (process == deletes.length)
                            jQuery.HideLoading();
                    }
                });
            }
        });

    },

    checkMaxAlertsSaved: function () {
        var self = this;

        if (self.params.numberOfAlerts >= 5) {
            jQuery("#addSearchAlertLink").click(function (e) {

                // Do not performed the action
                e.preventDefault();
                alert(self.params.savedAlertTopReachedMessage);
            });
        }
    }
};
