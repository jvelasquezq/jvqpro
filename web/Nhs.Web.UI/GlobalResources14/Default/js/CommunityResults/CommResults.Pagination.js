﻿NHS.Scripts.CommunityResults.CommResults.prototype.initPagination = function () {
    var self = this;

    jQuery(".nhs_Previous").click(function (event) {
        jQuery.NhsCancelEvent(event);
        if (jQuery(this).is(".nhs_Disabled") === false) {
            var page = parseInt(self.searchParameters.PageNumber);
            if (page > 1) {
                jQuery(".nhs_Page").removeClass("nhs_Active");
                self.searchParameters.PageNumber = parseInt(self.searchParameters.PageNumber) - 1;
                self.bindResults(false);
                jQuery("a.nhs_Page[data-page='" + self.searchParameters.PageNumber + "']").addClass("nhs_Active");
                self.movePagination(self.searchParameters.PageNumber);

                if (self.searchParameters.IsMapVisible) {
                    jQuery("#nhs_ResFacets").ScrollTo();
                } else {
                    jQuery.ScrollTo(0);
                }

                var lastValue = jQuery(".nhs_Page.nhs_lastPage").attr("data-page");
                self.checkPreviousNextPager(self.searchParameters.PageNumber, lastValue);
            }
        }
    });

    jQuery(".nhs_Next").click(function (event) {
        jQuery.NhsCancelEvent(event);
        if (jQuery(this).is(".nhs_Disabled") === false) {
            var page = parseInt(self.searchParameters.PageNumber);
            var lastPage = parseInt(jQuery(".nhs_lastPage").attr("data-page"));
            if (page < lastPage) {
                jQuery(".nhs_Page").removeClass("nhs_Active");
                self.searchParameters.PageNumber = page + 1;
                self.bindResults(false);
                jQuery("a.nhs_Page[data-page='" + self.searchParameters.PageNumber + "']").addClass("nhs_Active");
                self.movePagination(self.searchParameters.PageNumber);

                if (self.searchParameters.IsMapVisible) {
                    jQuery("#nhs_ResFacets").ScrollTo();
                } else {
                    jQuery.ScrollTo(0);
                }

                var lastValue = jQuery(".nhs_Page.nhs_lastPage").attr("data-page");
                self.checkPreviousNextPager(self.searchParameters.PageNumber, lastValue);
            }
        }
    });

    jQuery("#nhs_ResPaging").on("click", ".nhs_Page", function (event) {
        jQuery.NhsCancelEvent(event);
        var control = jQuery(this);
        if (!control.is(".nhs_Active")) {
            jQuery(".nhs_Page").removeClass("nhs_Active");
            var page = control.attr("data-page");

            self.searchParameters.PageNumber = page;
            self.bindResults(false);
            control.addClass("nhs_Active");
            self.movePagination(page);
            jQuery("#nhs_ResFacets").ScrollTo();
            var lastValue = jQuery(".nhs_Page.nhs_lastPage").attr("data-page");
            self.checkPreviousNextPager(page, lastValue);

        }
    });

    jQuery("#nhs_PageCountSelect").change(function () {
        var value = jQuery(this).val();
        self.defaultSearchParameters.PageSize = value;
        self.searchParameters.PageSize = value;
        self.searchParameters.PageNumber = 1;
        self.defaultSearchParameters.PageNumber = 1;
        self.bindResults(true);
        if (self.searchParameters.IsMapVisible)
            jQuery("#nhs_ResFacets").ScrollTo();
        else
            jQuery.ScrollTo(0);
    });
};

NHS.Scripts.CommunityResults.CommResults.prototype.checkPreviousNextPager = function (page, lastValue) {
    if (lastValue <= 1) {
        jQuery("div.nhs_PagingLinks").hide();
        return;
    }

    if (parseInt(page) === 1) {
        jQuery(".nhs_Previous").addClass("nhs_Disabled");
    } else {
        jQuery(".nhs_Previous").removeClass("nhs_Disabled");
    }

    if (parseInt(page) === parseInt(lastValue)) {
        jQuery(".nhs_Next").addClass("nhs_Disabled");
    } else {
        jQuery(".nhs_Next").removeClass("nhs_Disabled");
    }

    var activeLastAnchor = jQuery("a.nhs_Active.nhs_lastPage");
    var totalInernal = jQuery("ul#nhs_PagingLinksConteiner li").size();

    if (parseInt(totalInernal) < 14 || parseInt(page) <= 9) {
        jQuery(".nhs_PagerDots.nhs_Previous").hide();
    } else {
        jQuery(".nhs_PagerDots.nhs_Previous").show();
    }

    if (parseInt(totalInernal) < 14 || jQuery(activeLastAnchor).is(".nhs_lastPage") || parseInt(page) + 6 >= parseInt(lastValue) - 1) {
        jQuery(".nhs_PagerDots.nhs_Next").hide();
        jQuery(".nhs_Next").attr("data-event", "false");
    } else {
        jQuery(".nhs_PagerDots.nhs_Next").show();
    }
};

NHS.Scripts.CommunityResults.CommResults.prototype.movePagination = function (number) {
    var scroll = number < 10 ? 0 : (number - 8) * 34;
    jQuery(".nhs_PagingOut").stop().animate({ scrollLeft: scroll });
    var outerWidth = jQuery(".nhs_PagingOut").width();
    var innerWidth = jQuery(".nhs_PagingIn").width();
    var numOfPages = jQuery("#nhs_PagingLinksConteiner").find("li").length + 2;
    var c = jQuery("#nhs_PagingLinks")[0].offsetWidth;
    if (numOfPages < 14) {
        jQuery(".nhs_PagingOut").css("width", innerWidth);
    } else {
        jQuery(".nhs_PagingOut").css("width", "");
    }
    var width = c - jQuery(".nhs_lastPage")[0].offsetWidth - jQuery("a.nhs_Page[data-page='1']")[0].offsetWidth;

    if (c <= 500)
        jQuery(".nhs_PagingOut").css("width", "244px");

    if (innerWidth > outerWidth && outerWidth > 0) {
        if (number > 9) {
            jQuery("#nhs_PagingLinks").addClass("nhs_PagingMissingLeft");
        } else {
            jQuery("#nhs_PagingLinks").removeClass("nhs_PagingMissingLeft");
        }

        if (number < (numOfPages - 8)) {
            jQuery("#nhs_PagingLinks").addClass("nhs_PagingMissingRight");
        } else {
            jQuery("#nhs_PagingLinks").removeClass("nhs_PagingMissingRight");
        }
    } else {
        if (innerWidth > width)
            jQuery(".nhs_PagingOut").css("width", "");
        else
            jQuery(".nhs_PagingOut").css("width", innerWidth + "px");
    }
};

NHS.Scripts.CommunityResults.CommResults.prototype.bindPager = function (totalResults, pagesize) {
    var self = this;
    jQuery(".nhs_Page").each(function () { jQuery(this).removeClass("nhs_Active"); });
    var url = self.parameters.PaggingUrl.replace("[function]", self.currentResultType === "CommunityResults" ? "CommunityResults" : "HomeResults");
    var numberOfPages = Math.ceil(totalResults / pagesize);
    jQuery(".nhs_PagingLinks, .nhs_PagingOut").show();
    var control = jQuery("#nhs_PagingLinksConteiner");
    jQuery("a.nhs_Page[data-page='1']").addClass("nhs_Active");
    control.empty();

    if (numberOfPages > 2) {
        for (var i = 2; i < numberOfPages; i++) {
            var a = self.generatePageLink(i, url);
            control.append("<li>" + a + "</li>");
        }
    }

    if (numberOfPages === 2) {
        jQuery(".nhs_PagingOut").hide();
    }

    if (numberOfPages === 1) {
        jQuery(".nhs_PagingLinks").hide();
    }

    var href = jQuery(self.generatePageLink(numberOfPages, url)).attr("href");
    jQuery(".nhs_lastPage").attr("href", href).attr("data-page", numberOfPages).text(numberOfPages).show();
    var insidewidth = (numberOfPages - 2) * 34;
    control = jQuery("#nhs_PagingLinksConteiner");
    control.css("width", insidewidth);
    jQuery("a.nhs_Next").removeClass("nhs_Disabled");
    jQuery("a.nhs_Previous").addClass("nhs_Disabled");
    self.movePagination(1);
    self.checkPreviousNextPager(1, numberOfPages);
};

NHS.Scripts.CommunityResults.CommResults.prototype.generatePageLink = function (pageNumber, linkUrl) {
    if (linkUrl.indexOf("?") !== -1) {
        return "<a class=\"nhs_Page\" data-page=\"" + pageNumber + "\" href=\"" + linkUrl + "&page=" + pageNumber + "\">" + pageNumber + "</a>";
    }

    return "<a class=\"nhs_Page\" data-page=\"" + pageNumber + "\" href=\"" + linkUrl + "/page-" + pageNumber + "\">" + pageNumber + "</a>";
};