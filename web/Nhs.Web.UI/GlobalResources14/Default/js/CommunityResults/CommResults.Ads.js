﻿NHS.Scripts.CommunityResults.CommResults.prototype.loadAds = function () {
    var self = this;
    if (self.searchParameters.IsMapVisible) {
        self.loadMapViewAds();
    } else {
        self.loadListViewAds();
    }
},

NHS.Scripts.CommunityResults.CommResults.prototype.loadMapViewAds = function () {
    var self = this;
    var pageFunction = "/mapresultsv2/";
    var commAdUpdater = new NHS.Scripts.AdUpdater("#nhs_", self.hostUrl, self.adParams, self.partnerSiteUrl);

    if (commAdUpdater.partnerSiteUrl !== null) {
        commAdUpdater.currentUrl = commAdUpdater.partnerSiteUrl.length > 0 ? commAdUpdater.partnerSiteUrl + pageFunction : pageFunction;
    }

    commAdUpdater.RenderSkinAd(self.searchParameters.MarketId);
    commAdUpdater.RenderAllAds();
},

NHS.Scripts.CommunityResults.CommResults.prototype.loadListViewAds = function () {
    var self = this;
    var pageFunction = self.currentResultType === "CommunityResults" ? "/communityresultsv2/" : "/homeresultsv2/";
    var commAdUpdater = new NHS.Scripts.AdUpdater("#nhs_", self.hostUrl, self.adParams, self.partnerSiteUrl);

    if (commAdUpdater.partnerSiteUrl !== null) {
        commAdUpdater.currentUrl = commAdUpdater.partnerSiteUrl.length > 0 ? commAdUpdater.partnerSiteUrl + pageFunction : pageFunction;
    }

    commAdUpdater.RenderSkinAd(self.searchParameters.MarketId);
    commAdUpdater.RenderAllAds();
},

NHS.Scripts.CommunityResults.CommResults.prototype.loadSkinViewAd = function () {
    var self = this;
    var pageFunction = self.currentResultType === "CommunityResults" ? "/communityresultsv2/" : "/homeresultsv2/";

    if (self.searchParameters.IsMapVisible) {
        pageFunction = "/mapresultsv2/";
    }

    var commAdUpdater = new NHS.Scripts.AdUpdater("#nhs_", self.hostUrl, self.adParams, self.partnerSiteUrl);

    if (commAdUpdater.partnerSiteUrl !== null) {
        commAdUpdater.currentUrl = commAdUpdater.partnerSiteUrl.length > 0 ? commAdUpdater.partnerSiteUrl + pageFunction : pageFunction;
    }

    commAdUpdater.RenderSkinAd(self.searchParameters.MarketId);
},

NHS.Scripts.CommunityResults.CommResults.prototype.updateAdsPosition = function () {
    if (jQuery("#nhs_ResultList > div").length > 0) {

        // Floating column
        this._adsHeight = jQuery("#nhs_ResAdCol").height();
        var colHeight = jQuery("#nhs_ResultList > div").height();
        var colBottom = colHeight + jQuery("#nhs_ResAdCol").offset().top;
        var adsTop = colHeight - this._adsHeight - 10;
        var adsBottom = this._adsHeight + jQuery("#nhs_ResAdCol").offset().top;
        var browserHeight = jQuery(window).height();
        var browserBottomY;

        jQuery(window).scroll((function () {
            var y = jQuery(document).scrollTop();
            colHeight = jQuery("#nhs_ResultList > div").height();
            colBottom = colHeight + jQuery("#nhs_ResAdCol").offset().top;
            adsTop = colHeight - this._adsHeight - 10;
            adsBottom = this._adsHeight + jQuery("#nhs_ResAdCol").offset().top;
            browserHeight = jQuery(window).height();
            browserBottomY = y + browserHeight;

            // Is list column longer than ad column and whether scroll is below the ad's top position
            if (colBottom > adsBottom) {
                if (y >= (adsBottom - browserHeight)) {
                    // If so, ad the fixed class          
                    if (browserBottomY > colBottom) {
                        jQuery("#nhs_ResAdCol > div").removeClass("fixedBottom");
                        jQuery("#nhs_ResAdCol > div").addClass("absolute");
                        jQuery("#nhs_ResAdCol > div").css("top", (adsTop + "px"));
                        jQuery("#nhs_ResAdCol > div").css("bottom", "auto");
                    } else {
                        jQuery("#nhs_ResAdCol > div").removeClass("absolute");
                        jQuery("#nhs_ResAdCol > div").addClass("fixedBottom");
                        jQuery("#nhs_ResAdCol > div").css("top", "auto");
                        jQuery("#nhs_ResAdCol > div").css("bottom", 0);
                    }
                } else {
                    // Otherwise remove it
                    jQuery("#nhs_ResAdCol > div").removeClass("fixedBottom");
                    jQuery("#nhs_ResAdCol > div").removeClass("absolute");
                    jQuery("#nhs_ResAdCol > div").css("top", "auto");
                    jQuery("#nhs_ResAdCol > div").css("bottom", "auto");
                }
            }
        }).bind(this));
    }
}