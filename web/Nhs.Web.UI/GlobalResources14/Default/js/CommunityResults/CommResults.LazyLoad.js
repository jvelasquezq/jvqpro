﻿NHS.Scripts.LazyLoad = function (parameters) {
    this.parameters = parameters;
};

NHS.Scripts.LazyLoad.prototype =
{
    LazyLoadImages: function () {
        var self = this;
        jQuery("img.async").lazy({
            bind: "event",
            enableThrottle: true,
            throttle: 300,
            effect: "fadeIn",
            effectTime: 50,
            visibleOnly: true,
            onError: function (element) {
                var parentClass = element.parent()[0].getAttribute("class");
                if (parentClass) {
                    if (self.parameters && (parentClass === "mainImage" || parentClass === "nhs_ItemImages")) {
                        element[0].setAttribute("src", self.parameters.NoPhotoBig);
                    } else if (self.parameters && !(parentClass.indexOf(" nhs_BrandImage ") > 0 || parentClass.indexOf("ad-thumb") >= 0)) {
                        element[0].setAttribute("src", self.parameters.NoPhotoSmall);
                    } else {
                        element[0].setAttribute("src", "http://nhs-static.bdxcdn.com/globalResources14/default/images/1x1.gif");
                    }
                }
                jQuery(element).removeClass("async");
            },
            afterLoad: function (element) {
                jQuery(element).removeClass("async");
            }
        });
    },

    LazyLoadImagesGroup: function (elements) {
        var self = this;
        jQuery(elements).lazy({
            bind: "event",
            enableThrottle: true,
            throttle: 300,
            effect: "fadeIn",
            effectTime: 50,
            visibleOnly: true,
            onError: function (element) {
                var parentClass = element.parent()[0].getAttribute("class");
                if (parentClass) {
                    if (self.parameters && (parentClass === "mainImage" || parentClass === "nhs_ItemImages")) {
                        element[0].setAttribute("src", self.parameters.NoPhotoBig);
                    } else if (self.parameters && !(parentClass.indexOf(" nhs_BrandImage ") > 0 || parentClass.indexOf("ad-thumb") >= 0)) {
                        element[0].setAttribute("src", self.parameters.NoPhotoSmall);
                    } else {
                        element[0].setAttribute("src", "http://nhs-static.bdxcdn.com/globalResources14/default/images/1x1.gif");
                    }
                }
                jQuery(element).removeClass("async");
            },
            afterLoad: function (element) {
                jQuery(element).removeClass("async");
            }
        });
    },
}