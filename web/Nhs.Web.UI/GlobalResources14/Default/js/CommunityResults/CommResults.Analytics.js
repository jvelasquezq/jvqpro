﻿NHS.Scripts.CommunityResults.CommResults.prototype.clearGAFlags = function () {
    self.priceLowTyped = false;
    self.priceHighTyped = false;
    self.relatedArea = false;
    self.bedsSelected = "";
    self.bathsSelected = "";
    self.homeType = false;
    self.homeStatus = false;
    self.specialOffers = false;
    self.garagesSelected = false;
    self.livingAreasSelected = false;
    self.storiesSelected = false;
    self.masterBedroomsSelected = false;
    self.commNameSelected = false;
    self.commStatusSelected = false;
    self.builderSelected = false;
    self.schoolSelected = false;
    self.amenitySelected = false;
    self.distanceSelected = false;
    self.priceLowSlider = false;
    self.priceHighSlider = false;
},

NHS.Scripts.CommunityResults.CommResults.prototype.fireGAFacetUpdate = function () {
    var self = this;

    if (jQuery("#nhs_FacetLocation").hasClass("nhs_Active")) {
        if (self.relatedArea) {
            jQuery.googlepush("Search Events", "Search Refinement", "Related Area");
            self.relatedArea = false;
        }

        if (self.distanceSelected) {
            jQuery.googlepush("Search Events", "Search Refinement", "Select Distance");
            self.distanceSelected = false;
        }
    } else if (jQuery("#nhs_FacetPrice").hasClass("nhs_Active")) {
        if (self.priceLowTyped) {
            jQuery.googlepush("Search Events", "Search Refinement", "Select Price - Low (Type)");
            self.priceLowTyped = false;
        }

        if (self.priceHighTyped) {
            jQuery.googlepush("Search Events", "Search Refinement", "Select Price - High (Type)");
            self.priceHighTyped = false;
        }

        if (self.priceLowSlider) {
            jQuery.googlepush("Search Events", "Search Refinement", "Select Price - Low");
            self.priceLowSlider = false;
        }

        if (self.priceHighSlider) {
            jQuery.googlepush("Search Events", "Search Refinement", "Select Price - High");
            self.priceHighSlider = false;
        }
    } else if (jQuery("#nhs_FacetBedBath").hasClass("nhs_Active")) {
        if (self.bedsSelected !== "") {
            if (self.bedsSelected === "0") {
                jQuery.googlepush("Search Events", "Search Refinement", "Bedrooms - Any");
            } else if (self.bedsSelected === "5") {
                jQuery.googlepush("Search Events", "Search Refinement", "Bedrooms - 5+");
            } else {
                jQuery.googlepush("Search Events", "Search Refinement", "Bedrooms - " + self.bedsSelected);
            }

            self.bedsSelected = "";
        }

        if (self.bathsSelected !== "") {
            if (self.bathsSelected === "0") {
                jQuery.googlepush("Search Events", "Search Refinement", "Bathrooms - Any");
            } else if (self.bathsSelected === "5") {
                jQuery.googlepush("Search Events", "Search Refinement", "Bathrooms - 5+");
            } else {
                jQuery.googlepush("Search Events", "Search Refinement", "Bathrooms - " + self.bathsSelected);
            }

            self.bathsSelected = "";
        }
    } else if (jQuery("#nhs_FacetSchools").hasClass("nhs_Active")) {
        if (self.schoolSelected) {
            jQuery.googlepush("Search Events", "Search Refinement", "Schools");
            self.schoolSelected = false;
        }
    } else if (jQuery("#nhs_FacetMore").hasClass("nhs_Active")) {
        if (self.homeType) {
            jQuery.googlepush("Search Events", "Search Refinement", "Home Type");
            self.homeType = false;
        }

        if (self.homeStatus) {
            jQuery.googlepush("Search Events", "Search Refinement", "Home Status");
            self.homeStatus = false;
        }

        if (self.specialOffers) {
            jQuery.googlepush("Search Events", "Search Refinement", "Special Offers");
            self.specialOffers = false;
        }

        if (self.garagesSelected) {
            jQuery.googlepush("Search Events", "Search Refinement", "Garages");
            self.garagesSelected = false;
        }

        if (self.livingAreasSelected) {
            jQuery.googlepush("Search Events", "Search Refinement", "Living Areas");
            self.livingAreasSelected = false;
        }

        if (self.storiesSelected) {
            jQuery.googlepush("Search Events", "Search Refinement", "Stories");
            self.storiesSelected = false;
        }

        if (self.masterBedroomsSelected) {
            jQuery.googlepush("Search Events", "Search Refinement", "Master Bedroom");
            self.masterBedroomsSelected = false;
        }

        if (self.commNameSelected) {
            jQuery.googlepush("Search Events", "Search Refinement", "Community Name");
            self.commNameSelected = false;
        }

        if (self.commStatusSelected) {
            jQuery.googlepush("Search Events", "Search Refinement", "Community Status");
            self.commStatusSelected = false;
        }

        if (self.builderSelected) {
            jQuery.googlepush("Search Events", "Search Refinement", "Builder");
            self.builderSelected = false;
        }

        if (self.amenitySelected) {
            jQuery.googlepush("Search Events", "Search Refinement", "Type/Amenities");
            self.amenitySelected = false;
        }
    }
}