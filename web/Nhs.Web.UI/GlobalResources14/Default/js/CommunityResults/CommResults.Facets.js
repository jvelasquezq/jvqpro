﻿NHS.Scripts.CommunityResults.CommResults.prototype.attachEventsToFacets = function () {
    var self = this;
    var facets = jQuery("#nhs_ResFacets");

    // Start Facet Menus
    jQuery(document).click(function (event) {
        var target = event.srcElement || event.target;
        if (target.id !== "nhs_LocationSearchTextBox" && target.id !== "nhs_LocationSubmit" && target.id.indexOf("ui-id") === -1 &&
            target.offsetParent && target.offsetParent.id.indexOf("ui-id") === -1 && target.id !== "nhs_UpdateLocationFacet") {
            self.closeFacets();
        }
    });

    facets.click(function (event) {
        var target = event.srcElement || event.target;
        if (target.id !== "nhs_LocationSearchTextBox" && target.id !== "nhs_LocationSubmit" && target.id.indexOf("ui-id") === -1 &&
            target.offsetParent && target.offsetParent.id.indexOf("ui-id") === -1) {
            event.stopPropagation();
        }
    });

    facets.find(">div>div:first-child").each(function () {

        // Add hovers to submenu parents
        jQuery(this).on("touchstart mouseover", function (event) {
            if (event.type === "touchstart") {
                return false;
            }

            jQuery(this).parent().addClass("nhs_Hover");
        });

        jQuery(this).on("touchstart mouseup", function (event) {
            var target = event.srcElement || event.target;
            var tab = jQuery(this).parent();

            if (tab.hasClass("nhs_Active") && target.id !== "nhs_LocationSearchTextBox" && target.id !== "nhs_LocationSubmit" &&
                target.id.indexOf("ui-id") === -1 && target.offsetParent.id.indexOf("ui-id") === -1) {

                if (event.type === "touchstart") {
                    tab.find(".nhs_FacetBox").stop(true, true).hide();
                    tab.removeClass("nhs_Active");
                    tab.removeClass("nhs_Hover");
                    return true;
                }

                // Hide submenus
                if (event.type === "mouseup") {
                    tab.find(".nhs_FacetBox").stop(true, true).hide();
                    tab.removeClass("nhs_Active");
                    self.resetLocationBox();
                }
            } else {

                // Show subnav    
                var isLocation = jQuery(target).parents("#nhs_FacetLocation").length === 1;
                if (target.id !== "nhs_LocationSubmit" && !self.isNewSearh && !tab.is(".nhs_Active") && (!self.searchWithinMap || !isLocation)) {
                        self.resetFacetsToDefault(tab);
                        tab.parent().find(".nhs_FacetBox").hide();
                        tab.parent().find(">div").removeClass("nhs_Active");
                        tab.find(".nhs_FacetBox").stop(true, true).show();
                        tab.addClass("nhs_Active");
                }
            }

            if (event.type === "touchstart") {
                return false;
            }
        });

        // Hide subnavs on exit  
        jQuery(this).on("mouseleave", function () {
            jQuery(this).parent().removeClass("nhs_Hover");
        });
    });
    // Ends Facet Menus

    // Start Radius
    jQuery("#nhs_LocationSlider").slider({
        range: "min",
        min: 0,
        max: (self.FacetsOptions.LocationSliderText.length - 1),
        step: 1,
        value: 0,
        disabled: self.searchParameters.IsMultiLocationSearch,
        slide: function (event, ui) {
            if (ui.value > 0) {
                self.searchParameters.WebApiSearchType = "Radius";
            } else {
                self.searchParameters.WebApiSearchType = "Exact";
            }

            self.searchParameters.Radius = self.FacetsOptions.LocationSliderValues[ui.value];
            jQuery("#DistanceSet").html(self.FacetsOptions.LocationSliderText[ui.value]);
            self.searchParameters.GetRadius = false;
            self.searchParameters.GetLocationCoordinates = false;
            self.searchParameters.CustomRadiusSelected = true;
        },
        stop: function () {
            self.updateCounts(true, false);
            if (self.plotradius)
                self.showRadius(self.searchParameters.Radius);
            self.distanceSelected = true;
        }
    });
    // Ends Radius

    // Start City
    jQuery(".nhs_FacetBoxCol").on("click", ".nhs_CityFilters", function (event) {
        jQuery.NhsCancelEvent(event);
        jQuery("a.nhs_CityFilters").removeClass("nhs_Active");
        var control = jQuery(this);
        self.searchParameters.GetRadius = true;
        self.searchParameters.GetLocationCoordinates = true;
        self.searchParameters.City = control.data("value").replace(self.parameters.Translation.AllAreas, "");
        self.searchParameters.State = control.data("state");
        self.searchParameters.MarketId = control.data("market");
        self.searchParameters.PostalCode = "";
        self.searchParameters.County = "";

        self.searchParameters.Markets = null;
        self.searchParameters.Counties = null;
        self.searchParameters.Cities = null;
        self.searchParameters.PostalCodes = null;
        self.searchParameters.IsMultiLocationSearch = false;
        self.searchParameters.SyntheticInfo = null;

        jQuery("#nhs_LocationSlider").slider( "enable" );
        
        if (self.searchParameters.City.length === 0) {
            self.searchParameters.Radius = 0;
            self.searchParameters.WebApiSearchType = "Exact";
            jQuery("#nhs_LocationSearchTextBox").val(self.locationBoxSummary(self.searchParameters));
        } else {
            jQuery("#nhs_LocationSearchTextBox").val(self.searchParameters.City + ", " + self.searchParameters.State);
            self.searchParameters.WebApiSearchType = "Radius";
        }

        self.searchParameters.SortOrder = false;
        self.searchParameters.SortBy = "Random";
        self.updateCounts(true, false);
        self.relatedArea = true;
    });
    // End City

    // Start Price Padding
    jQuery("#nhs_PriceSlider").slider({
        range: true,
        animate: true,
        min: 0,
        max: (self.FacetsOptions.PriceSliderValues.length - 1),
        step: 1,
        values: [0, (self.FacetsOptions.PriceSliderValues.length - 1)],
        start: function (event, ui) {
            if (ui.values[1] === ui.values[0] && ui.values[0] > 27) {
                jQuery("#nhs_PriceSlider").slider("option", "values", [27, ui.values[1]]);
                self.searchParameters.PriceLow = self.FacetsOptions.PriceSliderValues[27];
                self.facetPriceFormat(jQuery("#PriceLow").val(self.searchParameters.PriceLow));
            }
        },
        slide: function (event, ui) {
            var index = jQuery(ui.handle).siblings("a").andSelf().index(ui.handle);

            if (index === 1) {
                if ((self.FacetsOptions.PriceSliderValues.length - 1) === ui.values[1]) {
                    self.searchParameters.PriceHigh = 0;
                    jQuery("#PriceHigh").val(self.parameters.Translation.NoMax);
                } else {
                    self.searchParameters.PriceHigh = self.FacetsOptions.PriceSliderValues[ui.values[1]];
                    self.facetPriceFormat(jQuery("#PriceHigh").val(self.searchParameters.PriceHigh));
                }
                self.priceHighSlider = true;
            }

            if (index === 0) {
                if (ui.values[0] === 0) {
                    self.searchParameters.PriceLow = 0;
                    jQuery("#PriceLow").val(self.parameters.Translation.NoMin);
                } else {
                    self.searchParameters.PriceLow = self.FacetsOptions.PriceSliderValues[ui.values[0]];
                    self.facetPriceFormat(jQuery("#PriceLow").val(self.searchParameters.PriceLow));
                }

                self.priceLowSlider = true;
            }
        },
        stop: function () {
            self.updateCounts(false, false);
        }
    });
    // End Price Padding

    // Start Beds
    jQuery("input[name=Beds]").click(function () {
        var value = jQuery(this).val();
        self.searchParameters.Bedrooms = value;
        self.updateCounts(false, false);
        self.bedsSelected = value;
    });
    // End Beds

    // Start Baths
    jQuery("input[name=Baths]").click(function () {
        var value = jQuery(this).val();
        self.searchParameters.Bathrooms = value;
        self.updateCounts(false, false);
        self.bathsSelected = value;
    });
    // End Baths

    // Start School Districts
    jQuery("#nhs_Facets_School", ".nhs_FacetBox").on("click", "input", function () {
        if (!jQuery(this).is("#SchoolsAny")) {
            var inputsChecked = jQuery("#nhs_Facets_School input:checked").not("#SchoolsAny");
            var ids = [];

            if (inputsChecked.length > 0) {
                jQuery("#SchoolsAny").removeAttr("checked");

                for (var i = 0; i < inputsChecked.length; i++) {
                    ids.push(inputsChecked[i].value);
                }

                self.searchParameters.SchoolDistrictIds = ids.join();
                self.updateCounts(false, false);
            } else {
                self.searchParameters.SchoolDistrictIds = "";
                jQuery("#SchoolsAny").prop("checked", true);
                self.updateCounts(false, false);
            }
        }

        self.schoolSelected = true;
    });

    jQuery("#nhs_Facets_School").on("click", "#SchoolsAny", function () {
        jQuery("#nhs_Facets_School input:checked").not("#SchoolsAny").removeAttr("checked");
        self.searchParameters.SchoolDistrictIds = "";
        self.updateCounts(false, false);
    });
    // End School Districts

    // Start Home Type
    jQuery("#HomeTypeAny").click(function () {
        self.searchParameters.SingleFamily = false;
        self.searchParameters.MultiFamily = false;
        self.updateCounts(false, true);
        self.homeType = true;
    });

    jQuery("#HomeTypeSingle").click(function () {
        self.searchParameters.SingleFamily = true;
        self.searchParameters.MultiFamily = false;
        self.updateCounts(false, true);
        self.homeType = true;
    });

    jQuery("#HomeTypeCondo").click(function () {
        self.searchParameters.SingleFamily = false;
        self.searchParameters.MultiFamily = true;
        self.updateCounts(false, true);
        self.homeType = true;
    });
    // End Home Type

    // Start Home Status
    jQuery("input[name=HomeStatus]").click(function () {
        var innerSelf = self;
        var value = jQuery(this).val();
        innerSelf.searchParameters.Qmi = value.toLowerCase() === "1" ? true : false;
        innerSelf.updateCounts(false, true);
        innerSelf.homeStatus = true;
    });
    // End Home Status

    // Start Special Offers
    jQuery("input[name=Offers]").click(function () {
        var value = jQuery(this).val();
        self.searchParameters.HotDeals = value.toLowerCase() === "1" ? true : false;
        self.updateCounts(false, true);
        self.specialOffers = true;
    });
    // End Special Offers

    // Start Garages
    jQuery("input[name=Garages]").click(function () {
        var value = jQuery(this).val();
        self.searchParameters.Garages = value;
        self.updateCounts(false, true);
        self.garagesSelected = true;
    });
    // End Garages

    // Start Living Areas
    jQuery("input[name=LivingAreas]").click(function () {
        var value = jQuery(this).val();
        self.searchParameters.LivingAreas = value;
        self.updateCounts(false, true);
        self.livingAreasSelected = true;
    });
    // End Living Areas

    // Start Stories/Floors
    jQuery("input[name=Stories]").click(function () {
        var value = jQuery(this).val();
        self.searchParameters.Stories = value;
        self.updateCounts(false, true);
        self.storiesSelected = true;
    });
    // End Stories/Floors

    // Start Master Bedroom
    jQuery("input[name=MasterBed]").click(function () {
        var value = jQuery(this).val();
        self.searchParameters.MasterBedLocation = value;
        self.updateCounts(false, true);
        self.masterBedroomsSelected = true;
    });
    // End Master Bedroom

    // Start Amenities
    jQuery("#nhs_Facet_Amenities").on("change", "input", function () {
        if (jQuery(this).is("#AmenitiesAny")) {
            self.searchParameters.Pool = false;
            self.searchParameters.Views = false;
            self.searchParameters.Parks = false;
            self.searchParameters.Green = false;
            self.searchParameters.Gated = false;
            self.searchParameters.GolfCourse = false;
            self.searchParameters.Waterfront = false;
            self.searchParameters.NatureAreas = false;
            self.searchParameters.Sports = false;
            self.searchParameters.Adult = false;
            jQuery("#nhs_Facet_Amenities input").not("#AmenitiesAny").removeAttr("checked");
            jQuery("#AmenitiesAny").prop("checked", "checked");
        } else {
            self.searchParameters.Pool = jQuery("#AmenitiesPool").is(":checked");
            self.searchParameters.Adult = jQuery("#AmenitiesAdultCommunity").is(":checked");
            self.searchParameters.Views = jQuery("#AmenitiesView").is(":checked");
            self.searchParameters.Parks = jQuery("#AmenitiesPark").is(":checked");
            self.searchParameters.Gated = jQuery("#AmenitiesGated").is(":checked");
            self.searchParameters.Green = jQuery("#AmenitiesGreen").is(":checked");
            self.searchParameters.Sports = jQuery("#AmenitiesSportFacility").is(":checked");
            self.searchParameters.Waterfront = jQuery("#AmenitiesWaterfront").is(":checked");
            self.searchParameters.GolfCourse = jQuery("#AmenitiesGolf").is(":checked");
            self.searchParameters.NatureAreas = jQuery("#AmenitiesNatureArea").is(":checked");

            if (self.searchParameters.Pool || self.searchParameters.GolfCourse || self.searchParameters.Gated || self.searchParameters.Adult ||
                self.searchParameters.Waterfront || self.searchParameters.Views || self.searchParameters.Parks || self.searchParameters.Green ||
                self.searchParameters.NatureAreas || self.searchParameters.Sports) {
                jQuery("#AmenitiesAny").removeAttr("checked");
            } else {
                jQuery("#AmenitiesAny").prop("checked", "checked");
            }
        }

        self.amenitySelected = true;
        self.updateCounts(false, true);
    });
    // End Amenities

    // Start Builder Name
    jQuery(".nhs_FacetBoxCol").on("change", "#BuilderName", function () {
        var value = jQuery(this).val();
        self.searchParameters.BrandId = value;
        self.searchParameters.CommId = 0;
        self.searchParameters.CommName = "";
        jQuery("#CommName").val(self.searchParameters.CommId);
        self.updateCounts(false, true);
        self.builderSelected = true;
    });
    // End Builder Name

    // Start Community Name
    jQuery(".nhs_FacetBoxCol").on("change", "#CommName", function () {
        var value = jQuery(this).val();
        self.searchParameters.CommId = value;
        self.searchParameters.CommName = "";
        self.updateCounts(false, true);
        self.commNameSelected = true;
    });
    // End Community Name

    // Start Community Status
    jQuery(".nhs_FacetBoxCol").on("change", "#CommStatus", function () {
        var value = jQuery(this).val();
        self.searchParameters.CommunityStatus = value;
        self.updateCounts(false, true);
        self.commStatusSelected = true;
    });
    //End Community Status

    jQuery(".nhs_ApplyFacets").click(function (event) {
        jQuery("span#nhs_PriceSummaryHigh").html(jQuery("#PriceHigh").val());
        jQuery("span#nhs_PriceSummaryLow").html(jQuery("#PriceLow").val());
        var isAllAreas = (self.searchParameters.GetLocationCoordinates === true && (!self.searchParameters.City || self.searchParameters.City.length === 0) &&
                         (!self.searchParameters.PostalCode || self.searchParameters.PostalCode.length === 0) &&
                         (!self.searchParameters.County || self.searchParameters.County.length === 0) && self.relatedArea);
        var lastSearch = self.locationBoxSummary(self.defaultSearchParameters);
        self.fireGAFacetUpdate();
        var locationBox = jQuery("#nhs_LocationSearchTextBox");
        var selectedCity = jQuery("a.nhs_CityFilters.nhs_Active").attr("data-value");

        if (selectedCity) {
            selectedCity = selectedCity.replace(self.parameters.Translation.AllAreas, "");
            selectedCity += ", " + self.searchParameters.State;
            jQuery("#nhs_LocationSlider").slider(self.defaultSearchParameters.IsMultiLocationSearch ? "disable" : "enable");
        }

        if (jQuery(this).attr("id") === "nhs_UpdateLocationFacet" && selectedCity !== locationBox.val() && lastSearch !== locationBox.val() && !isAllAreas) {
            jQuery("#nhs_LocationSubmit").click();
        } else {
            jQuery.NhsCancelEvent(event);
            self.searchParameters.PageNumber = 1;
            self.updateResults = true;
            self.updateMap = true;
            self.searchParameters.PriceHigh = jQuery("span#nhs_PriceSummaryHigh").html().replace(/\$|,/g, "");
            self.searchParameters.PriceLow = jQuery("span#nhs_PriceSummaryLow").html().replace(/\$|,/g, "");

            if (self.searchParameters.PriceHigh === self.parameters.Translation.NoMax) {
                self.searchParameters.PriceHigh = 0;
            }

            if (self.searchParameters.PriceLow === self.parameters.Translation.NoMin) {
                self.searchParameters.PriceLow = 0;
            }

            self.UpdateResults();
        }
    });

    jQuery(".nhs_CancelFacets").click(function (event) {
        jQuery.NhsCancelEvent(event);
        var tab = jQuery(this).parent().parent().parent();
        var facetBoxs = jQuery(".nhs_FacetBox");
        facetBoxs.stop(true, true).hide();
        facetBoxs.parent().removeClass("nhs_Active");
        self.resetFacetsToDefault(tab);
        self.resetLocationBox();
        self.clearGAFlags();
    });
}

NHS.Scripts.CommunityResults.CommResults.prototype.writeLoadingFacetsImage = function () {
    jQuery(".nhs_FacetCounts").html("<span class='nhs_FacetsLoading'>Updating</span>");
},

NHS.Scripts.CommunityResults.CommResults.prototype.facetPriceFormat = function (self) {
    self.priceFormat({
        prefix: "$",
        thousandsSeparator: ",",
        centsLimit: 0,
        insertPlusSign: false,
        limit: 7
    });
},

NHS.Scripts.CommunityResults.CommResults.prototype.initLocationBox = function () {
    var self = this;

    jQuery("#nhs_LocationSubmit").click(function () {
        var innerSelf = self;
        var value = jQuery("#nhs_LocationSearchTextBox").val();
        jQuery("#nhs_LocationSubmitFake").show();

        if (!innerSelf.searchTypeahead.showErrorSearchTextMessage(value)) {
            value = jQuery.trim(value);

            if (value.length > 0 && value !== self.parameters.Translation.EnterCityStZip) {
                self.isNewSearh = true;
                jQuery.googlepush("Search Events", "Search Action", "Search - Location");
                return true;
            } else {
                alert(self.parameters.Translation.MGS_HOMESEARCH_DEFAULT_ERROR_MSN);
                jQuery("#nhs_LocationSubmitFake").hide();
                return false;
            }
        } else {
            alert(self.parameters.Translation.MGS_HOMESEARCH_DEFAULT_ERROR_MSN);
            jQuery("#nhs_LocationSubmitFake").hide();
            return false;
        }
    });

    self.searchTypeahead.setupSearchBox("nhs_LocationSearchTextBox", "nhs_LocationSearchType");
    self.searchTypeahead.AutoCompleteSelect = function (value, text) {
        self.searchParameters.GetRadius = true;
        self.searchParameters.GetLocationCoordinates = true;
        self.searchParameters.CustomRadiusSelected = false;
        jQuery("#nhs_LocationSlider").slider("option", "disabled", false);
        self.updateCounts(true, false, value, text);
    };
    SetTextBoxesDescriptions();
},

NHS.Scripts.CommunityResults.CommResults.prototype.locationBoxSummary = function (searchParameters) {
    var text = "";
    var self = this;
    var hasName = false;

    if (searchParameters.MarketId > 0 && searchParameters.MarketName.length > 0) {
        text = searchParameters.MarketName + ", " + searchParameters.State + " Area";
        hasName = true;
    }

    if (searchParameters.City && searchParameters.City.length > 0) {
        text = searchParameters.City + ", " + searchParameters.State;
        hasName = true;
    }

    if (searchParameters.County && searchParameters.County.length > 0) {
        text = searchParameters.County + " " + self.parameters.Translation.County + ", " + searchParameters.State;
        hasName = true;
    }

    if (searchParameters.PostalCode && searchParameters.PostalCode.length > 0) {
        text = searchParameters.PostalCode + ", " + searchParameters.State;
        hasName = true;
    }

    if (searchParameters.IsMultiLocationSearch) {
        text = searchParameters.SyntheticInfo.Name;
        hasName = true;
    }

    if (!hasName) {
        text = this.locationBoxSummary(this.defaultSearchParameters);
    }

    return text;
},

NHS.Scripts.CommunityResults.CommResults.prototype.facetsSummary = function () {
    var self = this;
    var moreSummary = [];
    jQuery("#nhs_LocationSearchTextBox").val(self.locationBoxSummary(self.searchParameters));

    // Start Radius
    jQuery("#nhs_LocationSumary").html(self.FacetsOptions.LocationSliderSmallText[self.FacetsOptions.LocationSliderValues.indexOf(self.searchParameters.Radius)]);

    // Start Price Padding
    var priceLow = self.parameters.Translation.NoMin;
    var hidePrice = self.parameters.Translation.NoMax;

    if (self.searchParameters.PriceLow > 0) {
        priceLow = self.searchParameters.PriceLow;
        jQuery("#PriceLow").val(priceLow);
        self.facetPriceFormat(jQuery("#PriceLow"));
    }

    if (self.searchParameters.PriceHigh > 0) {
        hidePrice = self.searchParameters.PriceHigh;
        jQuery("#PriceHigh").val(hidePrice);
        self.facetPriceFormat(jQuery("#PriceHigh"));
    }

    jQuery("#nhs_PriceSummary").html("<li><span>" + self.parameters.Translation.Min + ": </span><span id='nhs_PriceSummaryLow'>" + jQuery("#PriceLow").val() +
                                     "</span></li><li><span>" + self.parameters.Translation.Max + ": </span><span id='nhs_PriceSummaryHigh'>" + jQuery("#PriceHigh").val() + "</span></li>");
    jQuery("#nhs_BedsAndBathSummary").html("");

    // Start Beds
    if (self.searchParameters.Bedrooms > 0) {
        jQuery("#nhs_BedsAndBathSummary").append("<li>" + self.searchParameters.Bedrooms + "+ " + (self.searchParameters.Bedrooms > 1 ? self.parameters.Translation.Bedrooms : self.parameters.Translation.Bedroom) + "</li>");
    } else {
        jQuery("#nhs_BedsAndBathSummary").append("<li>" + self.parameters.Translation.AllBedrooms + "</li>");
    }
    // Start Baths
    if (self.searchParameters.Bathrooms > 0) {
        jQuery("#nhs_BedsAndBathSummary").append("<li>" + self.searchParameters.Bathrooms + "+  " + (self.searchParameters.Bathrooms > 1 ? self.parameters.Translation.Bathrooms : self.parameters.Translation.Bath) + "</li>");
    } else {
        jQuery("#nhs_BedsAndBathSummary").append("<li>" + self.parameters.Translation.AllBathrooms + "</li>");
    }

    // Start School Districts
    if (self.searchParameters.SchoolDistrictIds) {
        self.updateSchoolDistricts(self.parameters.SchoolDistricts);
        var schools = self.searchParameters.SchoolDistrictIds.split(",");

        if (schools.length > 0) {
            var i;
            jQuery("#SchoolsAny").removeAttr("checked");

            for (i = 0; i < schools.length; i++) {
                jQuery("#nhs_Facets_School input").filter("[value='" + schools[i] + "']").prop("checked", "checked");
            }

            var inputsChecked = jQuery("#nhs_Facets_School input:checked").not("#SchoolsAny");

            if (inputsChecked.length > 0) {
                jQuery("#nhs_SchoolDistrictsSummary").html("");

                if (inputsChecked.length <= 2) {
                    for (i = 0; i < inputsChecked.length; i++) {
                        jQuery("#nhs_SchoolDistrictsSummary").append("<li>" + jQuery(inputsChecked[i]).next().text() + "</li>");
                    }
                } else if (inputsChecked.length > 2) {
                    var firstSchool = jQuery(inputsChecked[0]).next().text();
                    jQuery("#nhs_SchoolDistrictsSummary").append("<li>" + firstSchool + "</li><li><span>" + (inputsChecked.length - 1) + self.parameters.Translation.More + "</span></li>");
                }
            } else {
                jQuery("#SchoolsAny").prop("checked", "checked");
                jQuery("#nhs_SchoolDistrictsSummary").html(self.parameters.Translation.AllSchoolDistricts);
            }
        } else {
            jQuery("#SchoolsAny").prop("checked", "checked");
            jQuery("#nhs_SchoolDistrictsSummary").html(self.parameters.Translation.AllSchoolDistricts);
        }
    } else {
        jQuery("#nhs_SchoolDistrictsSummary").html(self.parameters.Translation.AllSchoolDistricts);
    }
    // End School Districts

    // Start Home Type
    if (self.searchParameters.SingleFamily) {
        moreSummary.push(self.parameters.Translation.SingleFamily);
    } else if (self.searchParameters.MultiFamily) {
        moreSummary.push(self.parameters.Translation.CondoTownhome);
    }
    // End Home Type

    // Start Home Status
    if (self.searchParameters.Qmi) {
        moreSummary.push(self.parameters.Translation.QuickMoveIn);
        jQuery("#nhs_allQmi").removeClass("nhs_Active");
        jQuery("#nhs_qmilnk").addClass("nhs_Active");
    } else {
        jQuery("#nhs_allQmi").addClass("nhs_Active");
        jQuery("#nhs_qmilnk").removeClass("nhs_Active");
    }
    // End Home Status

    // Start Special Offers
    if (self.searchParameters.HotDeals) {
        moreSummary.push(self.parameters.Translation.HotDeals);
        jQuery("#nhs_allHotDeals").removeClass("nhs_Active");
        jQuery("#nhs_HotHomeslnk").addClass("nhs_Active");
    } else {
        jQuery("#nhs_allHotDeals").addClass("nhs_Active");
        jQuery("#nhs_HotHomeslnk").removeClass("nhs_Active");
    }
    // End Special Offers

    // Start Garages
    if (self.searchParameters.Garages > 0) {
        moreSummary.push(self.searchParameters.Garages + "+" + self.parameters.Translation.Garage + (self.searchParameters.Garages > 0 ? "s" : ""));
    }
    // End Garages

    // Start Living Areas
    if (self.searchParameters.LivingAreas > 0) {
        moreSummary.push(self.searchParameters.LivingAreas + "+" +  (self.searchParameters.LivingAreas > 0 ? self.parameters.Translation.LivingAreas : self.parameters.Translation.LivingArea));
    }
    // End Living Areas

    // Start Stories/Floors
    if (self.searchParameters.Stories > 0) {
        moreSummary.push(self.searchParameters.Stories + "+" + (self.searchParameters.Stories > 0 ? self.parameters.Translation.Stories : self.parameters.Translation.Story));
    }
    // End Stories/Floors

    // Start Master Bedroom
    if (self.searchParameters.MasterBedLocation === "1") {
        moreSummary.push(self.parameters.Translation.MasterBedUpstairs);
    } else if (self.searchParameters.MasterBedLocation === "2") {
        moreSummary.push(self.parameters.Translation.MasterBedUpstairs);
    }
    // End Master Bedroom

    //Start Amenities
    if (self.searchParameters.Green) {
        moreSummary.push(self.parameters.Translation.Green);
    }

    if (self.searchParameters.Pool) {
        moreSummary.push(self.parameters.Translation.Pool);
    }

    if (self.searchParameters.GolfCourse) {
        moreSummary.push(self.parameters.Translation.GolfCourse);
    }

    if (self.searchParameters.Gated) {
        moreSummary.push(self.parameters.Translation.Gated);
    }

    if (self.searchParameters.Waterfront) {
        moreSummary.push(self.parameters.Translation.WaterFront);
    }

    if (self.searchParameters.Views) {
        moreSummary.push(self.parameters.Translation.Views);
    }

    if (self.searchParameters.Parks) {
        moreSummary.push(self.parameters.Translation.Parks);
    }

    if (self.searchParameters.NatureAreas) {
        moreSummary.push(self.parameters.Translation.NatureAreas);
    }

    if (self.searchParameters.Sports) {
        moreSummary.push(self.parameters.Translation.SportFacilities);
    }

    if (self.searchParameters.Adult) {
        moreSummary.push(self.parameters.Translation.AdultSeniorCommunity);
    }
    // End Amenities

    // Start Community Status
    if (self.searchParameters.CommunityStatus) {
        jQuery("#CommStatus").val(self.searchParameters.CommunityStatus);
        moreSummary.push(jQuery("#CommStatus option:selected").text());
    }
    // End Community Status

    // Start Community Name
    if (self.searchParameters.CommId > 0) {
        self.updateDropDowns("CommName", self.parameters.Communities, self.parameters.Translation.AllCommunities);
        jQuery("#CommName").val(self.searchParameters.CommId);
        moreSummary.push(self.parameters.Translation.Community + ": " + jQuery("#CommName option:selected").text());
    }
    // End Community Name

    // Start Builder Name
    if (self.searchParameters.BrandId > 0) {
        self.updateDropDowns("BuilderName", self.parameters.Brands, self.parameters.Translation.AllBuilders);
        jQuery("#BuilderName").val(self.searchParameters.BrandId);
        moreSummary.push(self.parameters.Translation.Builder + ": " + jQuery("#BuilderName option:selected").text());
    }
    // End Builder Name

    jQuery("#nhs_MoreSummary").html("");

    if (moreSummary.length === 0) {
        jQuery("#nhs_MoreSummary").append("<li>" + self.parameters.Translation.All + "</li>");
    } else if (moreSummary.length <= 2) {
        for (var f = 0; f < moreSummary.length; f++) {
            jQuery("#nhs_MoreSummary").append("<li>" + moreSummary[f] + "</li>");
        }
    } else if (moreSummary.length > 2) {
        var first = moreSummary[0];
        jQuery("#nhs_MoreSummary").append("<li>" + first + "</li><li><span>" + (moreSummary.length - 1) + " " + self.parameters.Translation.More + "</span></li>");
    }
},

NHS.Scripts.CommunityResults.CommResults.prototype.resetFacetsToDefault = function (tab) {
    var self = this;
    var html;
    var commCount = parseInt(jQuery("#nhs_CommCount").val());
    var homeCount = parseInt(jQuery("#nhs_CommHomeCount").val());
    var blCount = parseInt(jQuery("#nhs_BlCount").val());
    self.searchParameters = jQuery.extend(true, {}, self.defaultSearchParameters);

    jQuery("#nhs_LocationSlider").slider(self.defaultSearchParameters.IsMultiLocationSearch? "disable" : "enable");
    
    self.facetCountsHtml(commCount, homeCount, blCount);
    self.updateResults = false;
    self.clearGAFlags();
    self.setPriceSliderValues();

    if (tab.is("#nhs_FacetLocation")) {

        // Start Radius
        jQuery("#nhs_LocationSlider").slider("option", "value", self.FacetsOptions.LocationSliderValues.indexOf(self.searchParameters.Radius));
        jQuery("#DistanceSet").html(self.FacetsOptions.LocationSliderText[self.FacetsOptions.LocationSliderValues.indexOf(self.searchParameters.Radius)]);
        // Ends Radius

        // Start City
        self.updateCities(self.parameters.Citys);

        var city = self.searchParameters.City;
        if (self.searchParameters.PostalCode)
            city = self.parameters.City;

        jQuery("a.nhs_CityFilters").removeClass("nhs_Active");
        if (city)
            jQuery("a.nhs_CityFilters[data-value='" + city + "']").addClass("nhs_Active");
        else
            jQuery("a.nhs_CityFilters[data-value='All areas']").addClass("nhs_Active");
        // End City
    } else if (tab.is("#nhs_FacetPrice")) {

        // Start Price Padding
        self.resetLocationBox();
        // End Price Padding
    } else if (tab.is("#nhs_FacetBedBath")) {

        // Start Beds
        jQuery("input[name=Beds]").filter("[value='" + self.searchParameters.Bedrooms + "']").prop("checked", "checked");
        // End Beds

        self.resetLocationBox();

        // Start Baths
        jQuery("input[name=Baths]").filter("[value='" + self.searchParameters.Bathrooms + "']").prop("checked", "checked");
        // End Baths
    } else if (tab.is("#nhs_FacetSchools")) {
        self.updateSchoolDistricts(self.parameters.SchoolDistricts);
        self.resetLocationBox();

        // Start School Districts
        if (self.searchParameters.SchoolDistrictIds) {
            var schools = self.searchParameters.SchoolDistrictIds.split(",");

            if (schools.length > 0) {
                jQuery("#SchoolsAny").removeAttr("checked");

                for (var i = 0; i < schools.length; i++) {
                    jQuery("#nhs_Facets_School input").filter("[value='" + schools[i] + "']").prop("checked", "checked");
                }
            } else {
                jQuery("#SchoolsAny").prop("checked", "checked");
            }
        } else {
            jQuery("#SchoolsAny").prop("checked", "checked");
        }
        // End School Districts
    } else if (tab.is("#nhs_FacetMore")) {

        // Start Home Type
        if (self.searchParameters.SingleFamily) {
            jQuery("#HomeTypeSingle").prop("checked", "checked");
        } else if (self.searchParameters.MultiFamily) {
            jQuery("#HomeTypeCondo").prop("checked", "checked");
        } else {
            jQuery("#HomeTypeAny").prop("checked", "checked");
        }
        // End Home Type

        self.resetLocationBox();

        // Start Home Status
        if (self.searchParameters.Qmi) {
            jQuery("#HomeStatusQuick").prop("checked", "checked");
        } else {
            jQuery("#HomeStatusAll").prop("checked", "checked");
        }
        // End Home Status

        // Start Special Offers
        if (self.searchParameters.HotDeals) {
            jQuery("#OffersDeals").prop("checked", "checked");
        } else {
            jQuery("#OffersAll").prop("checked", "checked");
        }
        // End Special Offers

        // Start Garages
        jQuery("input[name=Garages]").filter("[value='" + self.searchParameters.Garages + "']").prop("checked", "checked");
        // End Garages

        // Start Living Areas
        jQuery("input[name=LivingAreas]").filter("[value='" + self.searchParameters.LivingAreas + "']").prop("checked", "checked");
        // End Living Areas

        // Start Stories/Floors
        jQuery("input[name=Stories]").filter("[value='" + self.searchParameters.Stories + "']").prop("checked", "checked");
        // End Stories/Floors

        // Start Master Bedroom
        jQuery("input[name=MasterBed]").filter("[value='" + self.searchParameters.MasterBedLocation + "']").prop("checked", "checked");
        // End Master Bedroom

        // Start Amenities
        jQuery("#AmenitiesPool").prop("checked", self.searchParameters.Pool);
        jQuery("#AmenitiesView").prop("checked", self.searchParameters.Views);
        jQuery("#AmenitiesPark").prop("checked", self.searchParameters.Parks);
        jQuery("#AmenitiesGolf").prop("checked", self.searchParameters.GolfCourse);
        jQuery("#AmenitiesGreen").prop("checked", self.searchParameters.Green);
        jQuery("#AmenitiesGated").prop("checked", self.searchParameters.Gated);
        jQuery("#AmenitiesWaterfront").prop("checked", self.searchParameters.Waterfront);
        jQuery("#AmenitiesNatureArea").prop("checked", self.searchParameters.NatureAreas);
        jQuery("#AmenitiesSportFacility").prop("checked", self.searchParameters.Sports);
        jQuery("#AmenitiesAdultCommunity").prop("checked", self.searchParameters.Adult);

        if (self.searchParameters.Pool || self.searchParameters.GolfCourse || self.searchParameters.Gated || self.searchParameters.Adult
            || self.searchParameters.Waterfront || self.searchParameters.Views || self.searchParameters.Parks || self.searchParameters.Green
            || self.searchParameters.NatureAreas || self.searchParameters.Sports) {
            jQuery("#AmenitiesAny").removeAttr("checked");
        } else {
            jQuery("#AmenitiesAny").prop("checked", "checked");
        }
        // End Amenities

        self.updateDropDowns("BuilderName", self.parameters.Brands, self.parameters.Translation.AllBuilders);
        self.updateDropDowns("CommName", self.parameters.Communities, self.parameters.Translation.AllCommunities);

        // Start Community Status
        jQuery("#CommStatus").val(self.searchParameters.CommunityStatus);
        // End Community Status

        // Start Community Name
        jQuery("#CommName").val(self.searchParameters.CommId);
        // End Community Name

        // Start Builder Name
        jQuery("#BuilderName").val(self.searchParameters.BrandId);
        // End Builder Name
    }
},

NHS.Scripts.CommunityResults.CommResults.prototype.getPricePadding = function (price) {
    var self = this;

    if (price < 100000) {
        return 0;
    }

    if (price > 999999) {
        return self.FacetsOptions.PriceSliderValues.length;
    }

    var index = parseInt(self.FacetsOptions.PriceSliderValues.indexOf(price));
    var priceLow, priceHigh;

    if (index !== -1) {
        return index;
    }

    for (var i = 1; i < self.FacetsOptions.PriceSliderValues.length; i++) {
        priceLow = self.FacetsOptions.PriceSliderValues[i - 1];
        priceHigh = self.FacetsOptions.PriceSliderValues[i];
        if (price >= priceLow && price <= priceHigh) {
            return i;
        }
    }

    return 0;
},

NHS.Scripts.CommunityResults.CommResults.prototype.updateDropDowns = function (id, options, defaultOption) {
    var dropDown = jQuery("#" + id);
    var commCount = parseInt(jQuery("#nhs_CommCount").val());
    var homeCount = parseInt(jQuery("#nhs_CommHomeCount").val());
    var blCount = parseInt(jQuery("#nhs_BlCount").val());
    var totalCount = commCount + homeCount + blCount;

    if (options.length <= 0 && totalCount > 0) return false;

    dropDown.html("<option value=\"0\" selected=\"selected\">" + defaultOption + "</option>");

    if (options.length > 0 && totalCount === 0) return false;

    for (var i = 0; i < options.length; i++) {
        dropDown.append("<option value='" + options[i].Key + "'>" + options[i].Value + "</option>");
    }

    return true;
},

NHS.Scripts.CommunityResults.CommResults.prototype.updateSchoolDistricts = function (options) {
    var self = this;
    var ul = jQuery("#nhs_Facets_School");
    ul.html("<li><input id=\"SchoolsAny\" type=\"checkbox\" value=\"All\" checked=\"checked\" /><label for=\"SchoolsAny\"><span>" + self.parameters.Translation.AllSchoolDistricts + "</span></label></li>");

    for (var i = 0; i < options.length; i++) {
        ul.append("<li><input id=\"Schools" + (i + 1) + "\" type=\"checkbox\" value=\"" + options[i].Key + "\" /><label for=\"Schools" + (i + 1) + "\"><span>" +
                   options[i].Value + "</span></label></li>");
    }
},

NHS.Scripts.CommunityResults.CommResults.prototype.updateCities = function (options) {
    var self = this;
    var div = jQuery("#nhs_CityFacets");
    var html = "<p>" + self.parameters.Translation.CitiesInTheArea + "</p>";
    var url = self.parameters.CityNameFilterUrl.replace("[function]", "communityresults");
    var href = "";
    var state;
    var market;
    var city;
    var column;

    if (!options[0][0]) {
        var x = Math.floor(options.length / 3);
        var r = options.length / 3;
        var index = 0;

        if (r !== 0 && r !== x) {
            x += 1;
        }

        for (var a = 0; a < 3; a += 1) {
            html += "<ul>";

            for (var c = 0; c < x; c += 1) {
                column = options[index];

                if (column) {
                    state = column.State;
                    market = column.Key;
                    city = column.Value;
                    href = column.Key > 0 ? (url + (url.indexOf("?") !== -1 ? "&citynamefilter=" : "/citynamefilter-") + city) : "javascript:void(0)";
                    html += "<li><a class=\"nhs_CityFilters\" data-value=\"" + city + "\" data-state=\"" + state + "\" data-market=\"" + market +
                            "\" href=\"" + href + "\">" + city + "</a></li>";
                } else {
                    break;
                }

                index += 1;
            }

            html += "</ul>";
        }
    } else {
        for (var i = 0; i < options.length; i++) {
            html += "<ul>";

            for (var j = 0; j < options[i].length; j++) {
                column = options[i][j];
                state = column.State;
                market = column.Key;
                city = column.Value;
                href = column.Key > 0 ? (url + (url.indexOf("?") !== -1 ? "&citynamefilter=" : "/citynamefilter-") + city) : "javascript:void(0)";
                html += "<li><a class=\"nhs_CityFilters\" data-value=\"" + city + "\" data-state=\"" + state + "\" data-market=\"" + market +
                        "\" href=\"" + href + "\">" + city + "</a></li>";
            }

            html += "</ul>";
        }
    }

    div.html(html);
},

NHS.Scripts.CommunityResults.CommResults.prototype.resetFacets = function (resetLocation) {
    var self = this;

    // Reset price
    self.searchParameters.PriceHigh = 0;
    self.searchParameters.PriceLow = 0;
    jQuery("#PriceHigh").val(self.parameters.Translation.NoMax);
    jQuery("#PriceLow").val(self.parameters.Translation.NoMin);

    // Beds & Bath       
    self.searchParameters.Bedrooms = 0;
    self.searchParameters.Bathrooms = 0;

    // School             
    self.searchParameters.SchoolDistrictIds = "";
    self.searchParameters.SingleFamily = false;
    self.searchParameters.MultiFamily = false;
    self.searchParameters.Qmi = false;
    self.searchParameters.HotDeals = false;
    self.searchParameters.Garages = 0.0;
    self.searchParameters.LivingAreas = 0;
    self.searchParameters.Stories = 0;
    self.searchParameters.MasterBedLocation = 0;
    jQuery("input[name=MasterBed][value=0]").prop("checked", true);
    self.searchParameters.CommunityStatus = "";
    self.searchParameters.CommId = 0;
    self.searchParameters.CommName = "";
    self.searchParameters.BrandId = 0;

    // Start Amenities
    self.searchParameters.Pool = false;
    self.searchParameters.Views = false;
    self.searchParameters.Parks = false;
    self.searchParameters.Green = false;
    self.searchParameters.Gated = false;
    self.searchParameters.GolfCourse = false;
    self.searchParameters.Waterfront = false;
    self.searchParameters.NatureAreas = false;
    self.searchParameters.Sports = false;
    self.searchParameters.Adult = false;

    if (resetLocation) {
        self.searchParameters.County = "";
        self.searchParameters.City = "";
        self.searchParameters.PostalCode = "";
        self.searchParameters.Radius = 0;
        self.searchParameters.WebApiSearchType = "Exact";
    }

    self.searchParameters.GetRadius = !self.searchWithinMap;
    self.searchParameters.GetLocationCoordinates = !self.searchWithinMap;
    self.updateResults = true;
    self.updateMap = self.searchParameters.IsMapVisible;
    self.UpdateResults();
    self.updateMap = false;
    self.plotMap();
},

NHS.Scripts.CommunityResults.CommResults.prototype.setPriceSliderValues = function () {
    var self = this;
    var priceLow = self.parameters.Translation.NoMin;
    var hidePrice = self.parameters.Translation.NoMax;

    if (self.searchParameters.PriceLow > 0) {
        priceLow = self.searchParameters.PriceLow;
        jQuery("#PriceLow").val(priceLow).keyup();
    } else {
        jQuery("#PriceLow").val(priceLow);
    }

    if (self.searchParameters.PriceHigh > 0) {
        hidePrice = self.searchParameters.PriceHigh;
        jQuery("#PriceHigh").val(hidePrice).keyup();
    } else {
        jQuery("#PriceHigh").val(hidePrice);
    }

    var indexPriceLow = self.getPricePadding(self.searchParameters.PriceLow);
    var indexPriceHide = self.getPricePadding(self.searchParameters.PriceHigh);
    if (indexPriceHide === 0)
        indexPriceHide = self.FacetsOptions.PriceSliderValues.length - 1;

    jQuery("#nhs_PriceSlider").slider("option", "values", [indexPriceLow, indexPriceHide]);
},

NHS.Scripts.CommunityResults.CommResults.prototype.resetLocationBox = function () {
    var self = this;
    var text = "";
    if (self.searchParameters.MarketId > 0)
        text = self.searchParameters.MarketName + ", " + self.searchParameters.State + " Area";

    if (self.searchParameters.City)
        text = self.searchParameters.City + ", " + self.searchParameters.State;

    if (self.searchParameters.County)
        text = self.searchParameters.County + " " + self.parameters.Translation.County + ", " + self.searchParameters.State;

    if (self.searchParameters.PostalCode)
        text = self.searchParameters.PostalCode + ", " + self.searchParameters.State;

    if (self.searchParameters.IsMultiLocationSearch)
        text = self.searchParameters.SyntheticInfo.Name;
    
    jQuery("#nhs_LocationSearchTextBox").val(text);
},

NHS.Scripts.CommunityResults.CommResults.prototype.closeFacets = function () {
    var self = this;
    var facetBoxs = jQuery(".nhs_FacetBox");
    var tab = facetBoxs.parent(".nhs_Active");
    facetBoxs.stop(true, true).hide();
    facetBoxs.parent().removeClass("nhs_Active");
    self.resetFacetsToDefault(tab);
    self.resetLocationBox();
},

NHS.Scripts.CommunityResults.CommResults.prototype.facetCountsHtml = function (commCount, homeCount, blCount) {
    var self = this;
    homeCount += blCount;
    var html = commCount === 0 && homeCount === 0 ? self.parameters.Translation.OopsNoResults : (commCount + " " +
               (commCount > 1 ? self.parameters.Translation.Communities : self.parameters.Translation.Community) + " <span>|</span> " + homeCount + " " +
               (homeCount > 1 ? self.parameters.Translation.Homes : self.parameters.Translation.Home));
    jQuery(".nhs_FacetCounts").html(html);
}
