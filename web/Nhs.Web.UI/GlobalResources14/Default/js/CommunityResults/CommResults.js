﻿// Global Class for Community Results Page
NHS.Scripts.CommunityResults.CommResults = function (parameters) {
    this.parameters = parameters;
    this.defaultSearchParameters = JSON.parse(JSON.stringify(parameters.searchParameters)); //jQuery.extend(true, {}, parameters.searchParameters);
    this.searchParameters = jQuery.extend(true, {}, parameters.searchParameters);
    this.FacetsOptions = jQuery.extend(true, this.FacetsOptions, parameters.FacetsOptions);
    this.currentResultType = parameters.currentResultType;
    this.searchWithinMap = parameters.searchWithinMap;
    this.updateMap = parameters.updateMap;
    this.isMapCreate = false;
    this.updateResults = false;
    this.CurrentTab = parameters.currentResultType;
    this.isNewSearh = false;
    this.SaveCommFavoriteUrl = parameters.SaveCommFavoriteUrl;
    this.searchTypeahead = new NHS.Scripts.Search(this.parameters.AutoCompliteUrl);
    this.plotradius = parameters.plotradius;
    this.showBasics = parameters.showBasics;
    this.facetsLoadingImagePath = parameters.facetsLoadingImagePath;
    this.isFirstCall = true;
    this.priceLowTyped = false;
    this.priceHighTyped = false;
    this.relatedArea = false;
    this.bedsSelected = "";
    this.bathsSelected = "";
    this.homeType = false;
    this.homeStatus = false;
    this.specialOffers = false;
    this.garagesSelected = false;
    this.livingAreasSelected = false;
    this.storiesSelected = false;
    this.masterBedroomsSelected = false;
    this.commNameSelected = false;
    this.commStatusSelected = false;
    this.builderSelected = false;
    this.amenitySelected = false;
    this.schoolSelected = false;
    this.distanceSelected = false;
    this.priceLowSlider = false;
    this.priceHighSlider = false;
    this.jsLog = typeof parameters.jsLog === "undefined" ? false : parameters.jsLog;
    this.hostUrl = parameters.HostUrl;
    this.partnerSiteUrl = parameters.PartnerSiteUrl;
    this.adParams = parameters.AdParams;
    this.persistSearchParametersUrl = parameters.persistSearchParametersUrl;
    this.listHubLogger = new NHS.Scripts.CommunityResults.CommResultsListHub(parameters);
    this.tempCities = null;
    this.lazyLoad = new NHS.Scripts.LazyLoad(parameters);
    this.FacetsOptions = parameters.FacetsOptions;
};

NHS.Scripts.CommunityResults.CommResults.prototype =
{
    init: function () {
        this.initMap(); // Not move this call form this possition       
        this.attachEventsToFacets();
        this.attachEvents();
        this.facetsSummary();
        this.initLocationBox();
        this.initializeNewsLetterForm();
        this.initSearchWithMapPopUp();
        this.updateAdsPosition();
        this.setupCTAEventNames();
    },

    setupCTAEventNames: function () {
        jQuery(".nhs_CommResItem .btn_FreeBrochure, .nhs_HomeResItem .btn_FreeBrochure").click(function () {
            NHS.Scripts.Helper.createCookie("CTAName", "Search Results CTA Free Brochure");
        });
    },

    UpdateResults: function () {
        var self = this;
        self.plotMap();

        if (self.searchWithinMap || self.updateResults) {
            self.updateResults = false;
            self.searchParameters.PageNumber = 1;
            self.bindResults(true);
        }
    },

    bindResults: function (createPagination) {
        var self = this;
        var facetBoxs = jQuery(".nhs_FacetBox");

        jQuery.ShowLoading();
        self.googleApi.hideLoading();
        facetBoxs.stop(true, true).hide();
        facetBoxs.parent().removeClass("nhs_Active");
        self.searchParameters.SrpType = self.currentResultType;
        //Case 83626: With Decimal changes on garage we need to make sure we are sending garage as a decimal instead Integer
        self.searchParameters.Garages = parseInt(self.searchParameters.Garages).toFixed(1);

        var url = self.parameters.getResultsUrl;

        jQuery.ajax({
            url: url,
            type: "POST",
            data : JSON.stringify(self.searchParameters),
            contentType: 'application/json',
            cache: false,
            success: function (results) {
                jQuery("#nhs_Results").html(results);

                if (jQuery.isFunction(self.vwoButtonsTest)) {
                    self.vwoButtonsTest();
                }

                jQuery(".nhs_tabArea").removeClass("nhs_Active");
                jQuery("#nhs_CrumbsContainer").load(self.parameters.BreadCrumbUrl, { refresh: true });
                var hd = jQuery("#nhs_HotHomeslnk");
                var href = hd.attr("href");

                if (self.currentResultType === "CommunityResults") {
                    jQuery("#nhs_communityTabArea").addClass("nhs_Active");
                    href = href.replace("homeresults", "communityresults");
                } else {
                    jQuery("#nhs_homeTabArea").addClass("nhs_Active");
                    jQuery("#nhs_QmiOptions").show();
                    href = href.replace("communityresults", "homeresults");
                }

                jQuery("#nhs_ResPaging").show();
                jQuery("#totalCounts").show();
                jQuery(".nhs_ResRefine").show();
                jQuery(".nhs_GroupingBar").show();
                hd.attr("href", href);

                var html;
                var commCount = parseInt(jQuery("#nhs_CommCount").val());
                var homeCount = parseInt(jQuery("#nhs_CommHomeCount").val());
                var blCount = parseInt(jQuery("#nhs_BlCount").val());

                homeCount += blCount;
                self.parameters.Brands = Brands;
                self.parameters.Communities = Communities;
                self.parameters.SchoolDistricts = SchoolDistricts;
                self.parameters.Citys = Citys;
                self.defaultSearchParameters = jQuery.extend(true, {}, SearchParameters);
                self.searchParameters = jQuery.extend(true, {}, SearchParameters);
                self.facetsSummary();
                jQuery(".nhs_SortBy option[value='" + self.searchParameters.SortBy + "']" +
                       "[sortorder='" + self.searchParameters.SortOrder + "']").prop("selected", "selected");


                if (commCount > 0 || homeCount > 0) {
                    if (self.currentResultType === "CommunityResults") {
                        html = self.parameters.Translation.MachingCommunityResultTitle;
                        html = html.replace("{4}", (commCount > 1 ? "s" : ""));
                    } else {
                        html = self.parameters.Translation.MachingHomeResultTitle;
                        html = html.replace("{4}", (homeCount > 1 ? "s" : ""));
                    }
                    html = html.replace("{0}", commCount).replace("{1}", (commCount > 1 ? self.parameters.Translation.ies : self.parameters.Translation.y)).
                        replace("{2}", homeCount).replace("{3}", (homeCount > 1 ? "s" : ""));
                } else {
                    html = "<strong>" + self.parameters.Translation.OopsNoResults + "</strong>";
                }

                jQuery("#totalCounts").html(html);

                if (createPagination) {
                    var resultsCount = self.currentResultType === "CommunityResults" ? commCount : homeCount;
                    self.bindPager(resultsCount, self.searchParameters.PageSize);
                }

                jQuery(".nhs_Page").removeClass("nhs_Active");
                jQuery("a.nhs_Page[data-page='" + self.searchParameters.PageNumber + "']").addClass("nhs_Active");
                var pageFunction = self.currentResultType === "CommunityResults" ? "/communityresultsv2/" : "/homeresultsv2/";

                if (self.searchParameters.IsMapVisible) {
                    pageFunction = "/mapresultsv2/";
                }

                var commAdUpdater = new NHS.Scripts.AdUpdater("#nhs_", self.hostUrl, self.adParams, self.partnerSiteUrl);

                if (commAdUpdater.partnerSiteUrl !== null) {
                    commAdUpdater.currentUrl = commAdUpdater.partnerSiteUrl.length > 0 ? commAdUpdater.partnerSiteUrl + pageFunction : pageFunction;
                }

                commAdUpdater.RenderAllAds();
                jQuery.HideLoading();
                tb_init("#nhs_Results a.thickbox");

                // I have to log the basic homes in ListHub each any are displayed on the page
                // this method is added to the commresults prototype in the CommResults.ListHub.js
                commResults.listHubLogger.logBasicHomesInListHub();

                // Show the map and list Icons
                // This method is called when the community tab or the home tab is displayed
                jQuery(".nhs_ViewTypeMap").show();
                jQuery(".nhs_ViewTypeList").show();
                jQuery("#nhs_GlobalSearchNavLocation").val(self.locationBoxSummary(self.searchParameters));
                self.updateSeoContent(false);
                self.LazyLoadImages();
            }
        }).fail(function() {
             jQuery.HideLoading();
        });
    },

    updateSeoContent: function (isBuilderTab) {
        var self = this;
        var isListView = jQuery("#nhs_List").hasClass("nhs_Active");
        // Update the Seo Content, just the information about the market is updated
        self.searchParameters.IsBuilderTabSearch = isBuilderTab;
        var updateSeoUrl = self.parameters.getUpdateSeoContentUrl;
        jQuery.ajax({
            url: updateSeoUrl,
            type: "POST",
            data : JSON.stringify(self.searchParameters),
            contentType: 'application/json',
            dataType: 'json',
            cache: false,
            success: function (seoResult) {
                if (jQuery("#nhs_seoContentHeader").length > 0) {
                    jQuery("#nhs_seoContentHeader").html(seoResult);
                } else {
                    jQuery("#nhs_ResMarketInfo").html(seoResult);
                    self.attachMoveSeoToggle();
                    self.attachListMapEvents();
                }
                if (isBuilderTab) {
                    jQuery(".nhs_ViewTypeMap").hide();
                    jQuery(".nhs_ViewTypeList").hide();
                } else {
                    jQuery(".nhs_ViewTypeMap").show();
                    jQuery(".nhs_ViewTypeList").show();
                }

                if (self.searchParameters.IsMapVisible) {
                    if (isBuilderTab === false) {
                        jQuery(".nhs_ResMapAreaToggle").show();
                        jQuery("#nhs_MapContainer").show();
                    }
                    jQuery("#nhs_List").removeClass("nhs_Active");
                    jQuery("#nhs_MapShow").removeClass("nhs_Active");
                    jQuery("#nhs_MapShow").addClass("nhs_Active");
                } else {
                    jQuery("#nhs_List").removeClass("nhs_Active");
                    jQuery("#nhs_MapShow").removeClass("nhs_Active");
                    jQuery("#nhs_List").addClass("nhs_Active");
                }

                self.addScrollToSeoMarketContent();
            }
        });
    },

    updateCounts: function (isLocationFacet, isMoreFacetsTab, value, text) {
        var self = this;
        self.writeLoadingFacetsImage();
        self.searchParameters.SrpType = self.currentResultType;
        var url = self.parameters.getCountsUrl;

        self.updateFacets = true;

        if (self._ajaxCall) {
            self._ajaxCall.abort();
        }
        //Case 83626: With Decimal changes on garage we need to make sure we are sending garage as a decimal instead Integer
        self.searchParameters.Garages = parseInt(self.searchParameters.Garages).toFixed(1);
        var data = JSON.stringify({ 'searchParameters': self.searchParameters, 'searchText': text, 'searchType': value, marketId: self.parameters.MarketId });
        this._ajaxCall = jQuery.ajax({
            url: url,
            type: "POST",
            contentType: 'application/json',
            dataType: 'json',
            data: data,
            success: function (results) {
                var commCount = results.facetsCounts.CommCount;
                var homeCount = results.facetsCounts.HomeCount;
                var blCount = results.facetsCounts.BlCount;
                self.facetCountsHtml(commCount, homeCount, blCount);

                self.searchParameters = jQuery.extend(true, {}, results.searchParameters);

                // Case 83914. GetCounts method on Community Results Controller Actions returns a number instead of the text for enums
                self.searchParameters.WebApiSearchType = (self.searchParameters.WebApiSearchType === 0 ? "Radius" :
                                                         (self.searchParameters.WebApiSearchType === 1 ? "Exact"  : "Map"));
           
                if (isLocationFacet) {
                    self.updateCities(results.facetsCounts.Facets.Cities);
                    var li = jQuery("li").find("[data-value='" + self.searchParameters.City + "']");
                    if (li.length > 0) {
                        li.addClass("nhs_Active");
                    } else {
                        jQuery("li").find("[data-value='All areas']").addClass("nhs_Active");
                    }
                }

                var index = self.FacetsOptions.LocationSliderValues.indexOf(results.facetsCounts.Radius);
                jQuery("#nhs_LocationSlider").slider("option", "value", index);
                jQuery("#DistanceSet").html(self.FacetsOptions.LocationSliderText[index]);

                if (isMoreFacetsTab) {
                    if (self.searchParameters.CommId === 0) {
                        self.updateDropDowns("CommName", results.facetsCounts.Facets.Communities, self.parameters.Translation.AllCommunities);
                    }

                    jQuery("#CommStatus").val(self.searchParameters.CommunityStatus);
                    jQuery("#CommName").val(self.searchParameters.CommId);
                }
            }
        });
    },

    ToString: function (obj) {
        var json = [];

        for (n in obj) {
            if (obj.hasOwnProperty(n)) {
                var v = obj[n];
                var t = typeof (v);

                if (t !== "object" && v !== null) {
                    if (v !== false && v !== 0) {
                        json.push(n + "=" + v);
                    }
                }
            }
        }

        return json.join("&");
    },

    showRadius: function (radius) {
        var self = this;
        var mapOptions = self.parameters.OptionsForMap.MapOptions;

        if (self.googleApi) {
            if (self.cityCircle) {
                self.cityCircle.setMap(null);
            }

            var center = new google.maps.LatLng(mapOptions.Latitude, mapOptions.Longitude);
            var populationOptions = {
                strokeColor: "#FF0000",
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: "#FF0000",
                fillOpacity: 0.35,
                map: self.googleApi.map,
                center: center,
                radius: radius * 1609.34
            };
            self.cityCircle = new google.maps.Circle(populationOptions);
        }
    },

    changeRadiusAndSearchType: function () {
        var self = this;
        jQuery.ajax({
            type: "POST",
            url: self.parameters.changeRadiusAndSearchTypeUrl,
            data: self.searchParameters,
            async: false
        });
    },

    LazyLoadImages: function () {
        var self = this;
        self.lazyLoad.LazyLoadImages();
    },

    lazyLoadBrands: function () {
        var self = this;
        self.lazyLoad.LazyLoadImagesGroup(jQuery("div.nhs_BuilderRes img.asyncBrand"));
    },

    updateAllInformation: function () {
        var self = this;
        self.searchParameters.PageNumber = 1;
        self.updateResults = true;
        self.updateMap = self.searchParameters.IsMapVisible;
        self.UpdateResults();
    }
};
