﻿NHS.Scripts.CommunityResults.CommResults.prototype.initMap = function () {
    var parentSelf = this;
    parentSelf.lastMarker = null;
    var mapOptions = this.parameters.OptionsForMap;
    parentSelf.googleApi = new NHS.Scripts.GoogleMapApi(mapOptions);
    var googleApi = parentSelf.googleApi;

    googleApi.options.Events.InfowindowTooltipReadyCallBack = function () {
        var card = jQuery(".nhs_MapHoverCard").parent().parent().parent();
        var arrow = card.parent().find("> div").first().find("> div");
        arrow.first().hide();
        arrow.eq(2).hide();
        card.next("div").hide();
        var width = card.width() + 75;
        var height = card.height() + 75;
        this.infowindowTooltip.setOptions({ pixelOffset: this.GetInfowindowOffset(width, height, googleApi.map, this.infowindowTooltip.getAnchor(), false) });
    };

    googleApi.options.Events.InfowindowReadyCallBack = function () {
        var card = jQuery("#nhs_MapCards").parent().parent().parent();
        var arrow = card.parent().find("> div").first().find("> div");
        arrow.first().hide();
        arrow.eq(2).hide();
        card.next("div").show();
        var width = card.width() + 75;
        var height = card.height() + 75;
        this.infowindow.setOptions({ pixelOffset: this.GetInfowindowOffset(width, height, googleApi.map, this.infowindow.getAnchor(), true) });
    };

    googleApi.GetInfowindowOffset = function (iwWidth, iwHeight, map, marker, altPosstion) {
        var xOffset = 0;
        var yOffset;

        // Our point of interest
        var location = this.ourOverlay.getProjection().fromLatLngToContainerPixel(marker.getPosition());

        // Get Edges of map in pixels: Sout West corner and North East corner
        var nep = this.ourOverlay.getProjection().fromLatLngToContainerPixel(map.getBounds().getNorthEast());

        // Horizontal Adjustment
        if (location.x < iwWidth / 2) {
            xOffset = iwWidth / 2 - location.x;
        } else if (location.x > nep.x - iwWidth / 2) {
            xOffset = (nep.x - iwWidth / 2) - location.x;
        }

        // Vertical Adjustment
        if (location.y < iwHeight) {
            yOffset = location.y + iwHeight - (location.y - nep.y);
        } else {
            yOffset = location.y + 20 - (location.y - nep.y);
        }

        if (altPosstion) {
            if (location.y >= 131 && location.y <= 304) {
                if (location.y >= 282 && location.y <= 305) {
                    yOffset = 120;
                } else if (location.y >= 130 && location.y <= 140) {
                    yOffset = 160;
                } else {
                    yOffset = 150;
                }

                if (location.x > (nep.x / 2)) {
                    xOffset = -230;
                } else if (location.x < (nep.x / 2)) {
                    xOffset = 230;
                }

            }
        }

        return new google.maps.Size(xOffset, yOffset);
    };

    googleApi.options.Events.MarkerMouseOverCallBack = function (info, infowindow, infowindowTooltip, marker) {
        //Google Analytics Map - Pin Hover    
        jQuery.googlepush("Search Events", "Search Results - Map", "Map - Pin Hover");
        var mapCard = jQuery("div.nhs_MapCard");

        if (mapCard != null && mapCard.length > 0) {
            return false;
        }

        var addImage = (info.MarketPoints.length === 1) && !info.IsBasic && !info.IsBl;
        var name = (info.MarketPoints.length > 1) ? info.MarketPoints.length.toString() + " " + parentSelf.parameters.Translation.Communities.toLowerCase() : info.MarketPoints[0]["Name"];
        var information = info.Price.replace("from", parentSelf.parameters.Translation.From);

        if (parentSelf.currentResultType !== "CommunityResults") {
            if (info.ResultType === "CommsHomes") {
                name = info.MarketPoints.length.toString() + " " + (info.MarketPoints.length > 1 ? parentSelf.parameters.Translation.Communities : parentSelf.parameters.Translation.Community).toLowerCase();
                information = info.HomesCount + " " +
                              (info.HomesCount > 1 ? parentSelf.parameters.Translation.Homes : parentSelf.parameters.Translation.Home).toLowerCase() + " " +
                              (info.HomesCount > 1 ? parentSelf.parameters.Translation.Matching2 : parentSelf.parameters.Translation.Matching);
            } else if (info.ResultType === "Comms") {
                information = info.HomesCount + " " + (info.HomesCount > 1 ? parentSelf.parameters.Translation.Homes : parentSelf.parameters.Translation.Home).toLowerCase() + " " +
                                                      (info.HomesCount > 1 ? parentSelf.parameters.Translation.Matching2 : parentSelf.parameters.Translation.Matching);
            } else {
                if (info.MarketPoints.length === 1) {
                    name = info.MarketPoints[0]["Name"];
                    information = "1 " + parentSelf.parameters.Translation.Home.toLowerCase() + " " +
                                  (info.HomesCount > 1 ? parentSelf.parameters.Translation.Matching2 : parentSelf.parameters.Translation.Matching);
                } else {
                    name = info.HomesCount + " " + (info.HomesCount > 1 ? parentSelf.parameters.Translation.Homes : parentSelf.parameters.Translation.Home).toLowerCase() + " " +
                                                   (info.HomesCount > 1 ? parentSelf.parameters.Translation.Matching2 : parentSelf.parameters.Translation.Matching);
                    information = "";
                }
            }
        }

        var html = "<div class=\"nhs_MapHoverCard\">";
        if (addImage)
            html += "<img src='" + info.BrandImage[0] + "'/>";
        html += "<h2>" + name + "</h2> " + information + "</div>";
        infowindow.close();
        infowindowTooltip.setContent(html);
        infowindowTooltip.open(googleApi.map, marker);
    };

    googleApi.options.Events.MarkerMouseOutCallBack = function (info, infowindow, infowindowTooltip, marker) {
        infowindowTooltip.close();
    };

    tb_initLive("#nhs_MapCards a.thickbox");

    googleApi.options.Events.MarkerClickCallBack = function (info, infowindow, infowindowTooltip, marker) {
        //Google Analytics Map - Pin Click
        jQuery.googlepush("Search Events", "Search Results - Map", "Map - Pin Click");
        infowindowTooltip.close();
        marker.setCursor("wait");
        if (parentSelf.lastMarker != null) {
            parentSelf.lastMarker.setIcon(parentSelf.lastMarker.defaultIcon);
        }
        marker.setIcon(marker.selectedIcon);
        parentSelf.lastMarker = marker;
        var commIds = [];
        var basicIds = [];

        for (var i = 0; i < info.MarketPoints.length; i++) {
            if (info.MarketPoints[i].IsBl) {
                basicIds.push(info.MarketPoints[i]["Id"]);
            } else {
                commIds.push(info.MarketPoints[i]["Id"] + "|" + info.MarketPoints[i]["NumHomes"]);
            }
        }

        jQuery.ajax({
            url: parentSelf.parameters.getCommunityMapCards,
            type: "GET",
            cache: false,
            data: {
                commIds: commIds.join(),
                basicIds: basicIds.join(),
                forceHeader: true
            },
            success: function (html) {
                if (html) {
                    infowindow.setContent(html);
                    infowindow.open(googleApi.map, marker);
                    marker.setCursor("pointer");
                    NHS.Scripts.Helper.createCookie("CTAName", "Search Results - Map");
                }
            }
        });
    };

    googleApi.getMarkerPointAsyn = function () {
        var self = this;
        var optionsMpa = self.options.MarkerPointAsynOptions;
        google.maps.event.addListener(self.map, optionsMpa.Listener, function () {
            parentSelf.UpdateResults();
        });
    };

    googleApi.options.Events.OnMapCreate = function () {
        parentSelf.plotMap();
    };

    googleApi.options.Events.ZoomChangesCallBack = function () {
        if (!this.isAClusterClick) {
            if (parentSelf.searchWithinMap) {
                parentSelf.UpdateResults();
            }

            if (jQuery(".gm-style > div > a").attr("href")) {
                var aux;
                var len = jQuery(".gm-style-cc").length;

                for (aux = 1; aux < len; aux += 1) {
                    jQuery(".gm-style-cc").eq("-1").remove();
                }

                jQuery(".gm-style > div > a").removeAttr("href").removeAttr("title");
            }
        }
    };

    googleApi.options.Events.MarkerClusterClickCallBack = function (cluster) {
        this.isAClusterClick = true;

        // workaround to wait the map finsh the zoom and real bounds are sended 
        setTimeout((function () {
            if (parentSelf.searchWithinMap) {
                parentSelf.UpdateResults();
            }

            this.isAClusterClick = false;
        }).bind(this), 300);
    };


    googleApi.options.Events.InfowindowCloseCallBack = function () {
        if (parentSelf.lastMarker != null) {
            parentSelf.lastMarker.setIcon(parentSelf.lastMarker.defaultIcon);
        }
    };



    googleApi.options.Events.OnMarkersCreate = function (info, infowindow, marker) {
        var count = info.MarketPoints.length;
        marker.defaultIcon = marker.getIcon();
        if (info.IsBasic)
            marker.selectedIcon = count > 1 ? parentSelf.parameters.iconSelectedMultiBasic : parentSelf.parameters.iconSelectedBasic;
        else
            marker.selectedIcon = count > 1 ? parentSelf.parameters.iconSelectedMulti : parentSelf.parameters.iconSelected;
    };

    googleApi.processResult = function (results) {
        var self = this;
        var prOptions = self.options.ProcessResultOptions;
        results = results.Results;

        for (var i = 0; i < results.length; i++) {
            var icon = self.getIconMarkerPoint(results, results[i]);

            for (var j = 0; j < results[i].MarketPoints.length; j++) {
                self.createMarkerPoint(results[i][prOptions.Latitude], results[i][prOptions.Longitude], null, null, icon, results[i]);
            }
        }

        google.maps.event.addListener(self.infowindow, "closeclick", function () {
            //Google Analytics Map - Quick Comm Close
            jQuery.googlepush("Search Events", "Search Results - Map", "Map - Quick Comm Close");
        });
    };

    googleApi.getIconMarkerPoint = function (results, result) {
        var count = result.MarketPoints.length;
        if (parentSelf.currentResultType !== "CommunityResults") {
            count = result.HomesCount;
        }

        if (result.IsBl) {
            return parentSelf.parameters.iconBasic;
        }

        if (result.IsBasic) {
            if (count > 1) {
                return parentSelf.parameters.iconMultiBasic;
            }

            return parentSelf.parameters.iconBasic;
        }

        if (count > 1) {
            return parentSelf.parameters.iconMulti;
        }

        return parentSelf.parameters.icon;
    };
};


NHS.Scripts.CommunityResults.CommResults.prototype.initSearchWithMapPopUp = function () {
    var self = this;

    if (self.searchParameters.IsMapVisible) {
        if (self.searchParameters.IsMultiLocationSearch)
            jQuery(".nhs_ResMapAreaToggle").hide();
        else
            jQuery(".nhs_ResMapAreaToggle").show();
    }

    jQuery("#btn_SearchWithinMap").click(function () {
        var searchMapArea = jQuery("#SearchMapArea");
        jQuery.googlepush("Search Events", "Search Results - Map", "Search Within Map - Popup Yes");

        if (searchMapArea && !searchMapArea.is(":checked")) {
            searchMapArea.attr("checked", "checked");
            searchMapArea.change();
        }

        jQuery("#SearchMapAreaConfirm").remove();
        jQuery.post(self.parameters.SearchMapAreaConfirmedUrl);
    });

    jQuery("#btn_CloseSearchWithinMap").click(function () {
        jQuery("#SearchMapAreaConfirm").remove();
        jQuery.googlepush("Search Events", "Search Results - Map", "Search Within Map - Popup Close");
        jQuery.post(self.parameters.SearchMapAreaConfirmedUrl);
    });

    jQuery("#btn_NoSearchWithinMap").click(function () {
        var searchMapArea = jQuery("#SearchMapArea");
        jQuery.googlepush("Search Events", "Search Results - Map", "Search Within Map - Popup No");

        if (searchMapArea && searchMapArea.is(":checked")) {
            searchMapArea.removeAttr("checked");
            searchMapArea.change();
        }

        jQuery("#SearchMapAreaConfirm").remove();
        jQuery.post(self.parameters.SearchMapAreaConfirmedUrl);
    });

    jQuery("#SearchMapArea").change(function () {
        var searchMapArea = jQuery("#SearchMapArea");
        jQuery("#SearchMapAreaConfirm").remove();

        if (searchMapArea && !searchMapArea.is(":checked")) {
            jQuery.googlepush("Search Events", "Search Results - Map", "Search Within Map - Off");
        } else {
            jQuery.googlepush("Search Events", "Search Results - Map", "Search Within Map - On");
        }

        jQuery.post(self.parameters.SearchMapAreaConfirmedUrl);
    });
},

NHS.Scripts.CommunityResults.CommResults.prototype.openMapView = function (event, self) {
    jQuery.NhsCancelEventAndStopPropagation(event);
    var control = jQuery("#nhs_MapShow");

    if (!control.is(".nhs_Active")) {
        jQuery("#nhs_List").removeClass("nhs_Active");
        control.addClass("nhs_Active");
        
        if (!self.searchParameters.IsMultiLocationSearch)
            jQuery(".nhs_ResMapAreaToggle").show();

        self.loadMapViewAds();

        if (logger) {
            logger.logEvent("CRMAPO", 0, 0);
            logger.logEvent("VIEWMAP", 0, 0);
        }

        jQuery("#nhs_MapContainer").fadeIn("slow");
        self.searchParameters.IsMapVisible = true;
        self.defaultSearchParameters.IsMapVisible = true;
        self.changeMapStatus();

        if (!self.isMapCreate) {
            self.isMapCreate = true;
            self.updateMap = true;
            self.googleApi.createMap();

            if (self.plotradius) {
                self.showRadius(self.searchParameters.Radius);
            }
        } else {
            self.plotMap();
        }

        jQuery("#nhs_ResMap").ScrollToPosLess(60);

        if (event.originalEvent) {
            jQuery.googlepush("Search Events", "Search Results", "Map Button");
            jQuery.googlepush("Search Events", "Search Results", "Expand Map");
        }
    }
},

NHS.Scripts.CommunityResults.CommResults.prototype.plotMap = function () {
    var self = this;
    var mapObject = self.googleApi;
    var optionsMpa = mapObject.options.MarkerPointAsynOptions;

    if ((self.searchWithinMap || self.updateMap) && self.searchParameters.IsMapVisible) {
        if (self.searchWithinMap) {
            self.searchParameters.WebApiSearchType = "Map";
            var data = mapObject.getBoundsFromMap();
            self.searchParameters.MinLat = data.minLat;
            self.searchParameters.MinLng = data.minLng;
            self.searchParameters.MaxLat = data.maxLat;
            self.searchParameters.MaxLng = data.maxLng;
        } else {
            mapObject.showLoading();
            self.searchParameters.WebApiSearchType = self.searchParameters.Radius > 0 ? "Radius" : "Exact";
        }

        self.searchParameters.Garages = parseInt(self.searchParameters.Garages).toFixed(1);
        self.searchParameters.SrpType = self.currentResultType;

        var url = optionsMpa.UrlServer;
        jQuery.ajax({
            url: url,
            type: "POST",
            data: JSON.stringify(self.searchParameters),
            contentType: 'application/json',
            dataType: "json",
            success: function(results) {
                mapObject.map.clearOverlays();
                self.updateMapMessage(results.Total, results.NoPlotted);
                var count = results.Results.length;
                mapObject.processResult(results);
                self.updateMap = false;

                if (!self.searchWithinMap && count > 0) {
                    mapObject.AutoFit();
                }

                mapObject.hideLoading();
            }
        });
    }},

NHS.Scripts.CommunityResults.CommResults.prototype.updateMapMessage = function (total, noPlotted) {
    jQuery(".nhs_Map_Msg").hide();
    jQuery("#liMap_Msg1").show();

    if (noPlotted > 0) {
        jQuery("#liMap_Msg2").show();
    }
},

NHS.Scripts.CommunityResults.CommResults.prototype.changeMapStatus = function () {
    var self = this;
    jQuery.post(self.parameters.ChangeMapStatusUrl, { isMapVisible: self.searchParameters.IsMapVisible });
}