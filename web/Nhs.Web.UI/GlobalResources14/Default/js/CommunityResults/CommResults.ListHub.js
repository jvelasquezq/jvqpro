﻿// ListHub is a tracking service similar to Google Analitics but just for Real State.
// More infor here http://app.listhub.com/tracking-docs/v3/listing-metrics-js.html

//Constructor
NHS.Scripts.CommunityResults.CommResultsListHub = function(parameters) {
    this.eventLogger = new NHS.Scripts.Globals.EventLogger({ siteRoot: parameters.siteRoot, partnerID: parameters.PartnerId, marketId: parameters.MarketId });
    this.listHubProviderId = parameters.ListHubProviderId;
    this.listHubTestLogging = parameters.ListHubTestLogging;
    this.desckey = parameters.Desckey;
}

// This function query the html looking for basic homes (the ones who has the nhs_BasicItem class)
// And for each of them trigger a search display event
NHS.Scripts.CommunityResults.CommResultsListHub.prototype.logBasicHomesInListHub = function () {
    jQuery('.nhs_BasicItem').each(function() {
        // Get the id of the basic home, it is in a hidden field inside it.
        var homeId = jQuery(this).find('#HomeId').val();

        // Log the event using the method in the EventLogger.js
        // in the commResults Global object we have a property called eventLogger with is an instance of the EventLogger js
        commResults.listHubLogger.eventLogger.logListHubEvent(commResults.listHubLogger.listHubProviderId, commResults.listHubLogger.listHubTestLogging, commResults.listHubLogger.desckey, homeId);
    });
}