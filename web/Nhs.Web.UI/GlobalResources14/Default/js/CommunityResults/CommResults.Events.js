﻿NHS.Scripts.CommunityResults.CommResults.prototype.initializeNewsLetterForm = function () {
    var self = this;
    var newsLetterParams = self.parameters.NewsLetterSignUpForm;

    if (!newsLetterParams.showNewsletterForm) {
        jQuery("#nhs_NewsletterContainer").hide();
        return true;
    }

    var html = newsLetterParams.signupTemplate.replace("#MarketName#", newsLetterParams.marketName).replace("#MarketName#", newsLetterParams.marketName)
                                              .replace("#MarketId#", newsLetterParams.marketId).replace("#PartnerName#", newsLetterParams.partnerName)
                                              .replace("#PartnerSiteUrl#", newsLetterParams.partnerSiteUrl).replace("#Email#", "");

    // Set the template
    jQuery("#nhs_NewsletterContainer").html(html);
    jQuery("div#nhs_NewsletterSignUpV2").on("click", "input#nhs_NewsletterSignUpBtnV2", function (event) {
        jQuery.NhsCancelEventAndStopPropagation(event);
        jQuery(this).hide();
        jQuery("div#nhs_NewsletterSignUpV2 #fakeInputLoading").show();
        var innerSelf = newsLetterParams;
        var email = jQuery("div#nhs_NewsletterSignUpV2 input#Email").val();

        if (ValidEmail(email) === false) {
            jQuery("input#nhs_NewsletterSignUpBtnV2").show();
            jQuery("div#nhs_NewsletterSignUpV2 #fakeInputLoading").hide();
            jQuery("div.nhs_NewsletterSignUpErrorsV2").html("<p class=\"nhs_Error\">Please provide a valid email</p>");
            return;
        }

        var dataFields = jQuery("#nhs_NewsletterSignUpV2 input").serialize();
        jQuery.ajax({
            url: newsLetterParams.postUrl,
            data: dataFields,
            cache: false,
            type: "GET",
            success: function (data) {
                if (data.HasError === false) {
                    var thanks = innerSelf.thanksTemplate.replace("#MarketName#", innerSelf.marketName);
                    jQuery("#nhs_NewsletterContainer").html(thanks);
                } else {
                    jQuery("input#nhs_NewsletterSignUpBtnV2").show();
                    jQuery("div#nhs_NewsletterSignUpV2 #fakeInputLoading").hide();
                    jQuery("div.nhs_NewsletterSignUpErrorsV2").html("<p class=\"nhs_Error\">" + data.Message + "</p>");
                }
            },
            error: function () {
                jQuery("input#nhs_NewsletterSignUpBtnV2").show();
                jQuery("div#nhs_NewsletterSignUpV2 #fakeInputLoading").hide();
                jQuery("div.nhs_NewsletterSignUpErrorsV2").html("<p class=\"nhs_Error\">Internal Server Error</p>");
            }
        });

        jQuery("input#Email").val("");
        jQuery("input#Email").removeAttr("value");
    });
},

NHS.Scripts.CommunityResults.CommResults.prototype.attachEvents = function () {
    var self = this;
    jQuery(".nhs_BrandLink").live("click", ".nhs_BrandLink", function () {
        if (jQuery(this).is(".isBoyl")) {
            window.location.href = jQuery(this).attr("href");
            return true;
        }

        if (self.searchParameters.IsMapVisible && !jQuery("#nhs_MapContainer").is(":visible")) {
            jQuery("#nhs_List").removeClass("nhs_Active");
            jQuery("#nhs_MapShow").addClass("nhs_Active");
        } else {
            jQuery("#nhs_MapShow").removeClass("nhs_Active");
            jQuery("#nhs_List").addClass("nhs_Active");
        }
       
        jQuery("#nhs_ResFacets").ScrollToFast();
        self.updateResults = true;
        self.updateMap = true;
        self.searchParameters.PageNumber = 1;
        self.searchParameters.BrandId = jQuery(this).attr("rel");
        self.searchParameters.CommId = 0;
        self.builderSelected = true;
        self.UpdateResults();
        return false;
    });

    jQuery(".fakeViewHomes").on("click", function () { jQuery.googlepush("Search Events", "Search Results", "View Homes"); });

    jQuery("#nhs_ClearFacets a").on("click", function (event) {
        jQuery.NhsCancelEventAndStopPropagation(event);

        if (jQuery("#SearchMapArea").is(":checked")) {
            self.searchWithinMap = false;
            jQuery("#SearchMapArea").attr("checked", false);
            self.updateMap = true;
            self.updateResults = true;
            self.resetSearchWithinMap();
        }

        self.resetFacets(false);
    });

    jQuery("#ReloadComResults").on("click", function (event) {
        jQuery.NhsCancelEventAndStopPropagation(event);
        self.resetFacets(true);
    });

    if (jQuery("#nhs_Page #nhs_ResMktTxt > div").length > 0) {
        self.addScrollToSeoMarketContent();
    }

    jQuery("#PriceLow, #PriceHigh").on("keyup", function () {
        self.facetPriceFormat(jQuery(this));
        self.searchParameters.PriceLow = jQuery("#PriceLow").val().replace(/\$|,/g, "");
        self.searchParameters.PriceHigh = jQuery("#PriceHigh").val().replace(/\$|,/g, "");
        var indexPriceLow = self.getPricePadding(self.searchParameters.PriceLow);
        var indexPriceHide = self.getPricePadding(self.searchParameters.PriceHigh);

        if (indexPriceHide === 0) {
            indexPriceHide = self.FacetsOptions.PriceSliderValues.length - 1;
        }

        jQuery("#nhs_PriceSlider").slider("option", "values", [indexPriceLow, indexPriceHide]);

        if (jQuery(this).attr("id") === "PriceLow") {
            self.priceLowTyped = true;
        }

        if (jQuery(this).attr("id") === "PriceHigh") {
            self.priceHighTyped = true;
        }
    });

    jQuery("#PriceLow, #PriceHigh").on("blur", function () {
        if (parseInt(self.searchParameters.PriceHigh) < parseInt(self.searchParameters.PriceLow)) {
            jQuery("#PriceHigh").val(self.searchParameters.PriceLow).keyup();
        }

        self.updateCounts(false, false);
    });

    jQuery(document).on("submit", "#nhs_LocationSearchForm", function () {
        var text = jQuery("#nhs_LocationSearchTextBox").val();

        if (text && text !== self.locationBoxSummary(self.defaultSearchParameters)) {
            var val = text.toLowerCase().indexOf("area", text.length - 4);

            if (val !== -1 && !self.distanceSelected) {
                self.searchParameters.Radius = 0;
                self.searchParameters.WebApiSearchType = "Exact";
            }
        }

        self.changeRadiusAndSearchType();
    });

    // Toggles - QMI and Hot Deals
    jQuery(".nhs_ResRefine > p.nhs_ToggleButtons > span, .nhs_ResRefine > p.nhs_ToggleButtons > a").each(function () {
        jQuery(this).on("click", function () {
            if (jQuery(this).hasClass("nhs_Active") === false) {

                // Change button ui
                jQuery(this).parent().children().removeClass("nhs_Active");
                jQuery(this).addClass("nhs_Active");

                if (jQuery(this).attr("id") === "nhs_HotHomeslnk") {
                    jQuery.googlepush("Search Events", "Search Results", "Hot Deals Button");
                } else if (jQuery(this).attr("id") === "nhs_allHotDeals") {
                    jQuery.googlepush("Search Events", "Search Results", "Any Button");
                } else if (jQuery(this).attr("id") === "nhs_allQmi") {
                    jQuery.googlepush("Search Events", "Search Results", "Any Button");
                } else if (jQuery(this).attr("id") === "nhs_qmilnk") {
                    jQuery.googlepush("Search Events", "Search Results", "Quick Move-In Button");
                }
            }

            return false;
        });
    });

    jQuery("a[href=#nhs_Skip]").ScrollClickTo(0);

    self.attachMoveSeoToggle();

    jQuery("#nhs_LocationSearchFormShow").click(function () { jQuery("#SearchMapArea").removeAttr("checked").change(); });

    jQuery("#SearchMapArea").change(function () {
        self.searchWithinMap = jQuery("#SearchMapArea").is(":checked");

        if (self.searchWithinMap) {
            self.searchParameters.CommunityId = 0;
            self.searchParameters.BuilderId = 0;
            self.googleApi.map.setOptions({ minZoom: 8 });


            self.defaultSearchParameters.CommunityId = 0;
            self.defaultSearchParameters.BuilderId = 0;

            if (self.googleApi.options.MinZoom > 0 && self.googleApi.map.getZoom() < self.googleApi.options.MinZoom) {
                self.googleApi.map.setZoom(self.googleApi.options.MinZoom);
            }

            jQuery("#nhs_LocationSearchForm").hide();
            jQuery("#nhs_LocationSearchBlock").show();

        } else {
            self.resetSearchWithinMap();
        }

        self.updateResults = true;
        self.updateMap = true;
        self.UpdateResults(false);
    });

    

    if (self.searchParameters.IsMapVisible || self.parameters.searchWithinMap) {
        if (self.parameters.searchWithinMap) {
            jQuery("#nhs_LocationSearchForm").hide();
            jQuery("#nhs_LocationSearchBlock").show();
        }

        jQuery("#nhs_List").removeClass("nhs_Active");
        jQuery("#nhs_MapShow").addClass("nhs_Active");
        jQuery("#nhs_MapContainer,.nhs_ResMapAreaToggle").show();
        jQuery("#nhs_ResFacets").ScrollToFast();
        self.isMapCreate = true;
        self.updateMap = true;
        self.googleApi.createMap();

        if (self.plotradius) {
            self.showRadius(self.searchParameters.Radius);
        }
    }

    self.attachListMapEvents();    

    jQuery("#nhs_seoContentHeader, #nhs_SeoContentArea").on("click", ".nhs_OpenMap", function (event) { self.openMapView(event, self); });

    jQuery(".nhs_tabArea").click(function (event) {
        jQuery.NhsCancelEvent(event);
        var control = jQuery(this);

        if (!control.is(".nhs_Active")) {
            var qmiLink = jQuery("#nhs_qmilnk").attr("href");

            if (control.is("#nhs_communityTabArea")) {
                jQuery("#nhs_qmilnk").attr("href", qmiLink.replace("homeresults", "communityresults"));
                self.currentResultType = "CommunityResults";
                var isHotDealsActive = jQuery("#nhs_HotHomeslnk").is(".nhs_Active");
                self.searchParameters.HotDeals = isHotDealsActive;
                var isQmiActive = jQuery("#nhs_qmilnk").is(".nhs_Active");
                self.searchParameters.qmi = isQmiActive;
                jQuery("#nhs_CommunitySortBy").show();
                jQuery("#nhs_HomeSortBy").hide();
                jQuery("#nhs_ResPaging").show();
                jQuery("#totalCounts").show();
                jQuery(".nhs_ResRefine").show();
                jQuery(".nhs_GroupingBar").show();
                jQuery("#nhs_ResViewType").show();

                if (jQuery("div.nhs_NoRes").length > 0) {
                    jQuery("div.nhs_NoRes").show();
                }

                if (self.searchParameters.IsMapVisible) {
                    jQuery("#nhs_List").removeClass("nhs_Active");
                    jQuery("#nhs_MapShow").addClass("nhs_Active");
                    jQuery(".nhs_ResMapAreaToggle").show();
                    jQuery("#nhs_MapContainer").show();
                } else {
                    jQuery("#nhs_MapShow").removeClass("nhs_Active");
                    jQuery("#nhs_List").addClass("nhs_Active");
                    jQuery(".nhs_ResMapAreaToggle").hide();
                    jQuery("#nhs_MapContainer").hide();
                }

                jQuery(".nhs_ViewTypeMap").show();
                jQuery(".nhs_ViewTypeList").show();
                jQuery.googlepush("Search Events", "Search Results", "Communities Tab");
            } else if (control.is("#nhs_homeTabArea")) {
                jQuery("#nhs_qmilnk").attr("href", qmiLink.replace("communityresults", "homeresults"));
                self.currentResultType = "HomeResults";
                jQuery("#nhs_CommunitySortBy").hide();
                jQuery("#nhs_HomeSortBy").show();
                jQuery("#nhs_ResPaging").show();
                jQuery("#totalCounts").show();
                jQuery(".nhs_ResRefine").show();
                jQuery(".nhs_GroupingBar").show();
                jQuery("#nhs_ResViewType").show();

                if (jQuery("div.nhs_NoRes").length > 0) {
                    jQuery("div.nhs_NoRes").show();
                }

                if (self.searchParameters.IsMapVisible && !jQuery("#nhs_MapContainer").is(":visible")) {
                    jQuery(".nhs_ResMapAreaToggle").show();
                    jQuery("#nhs_MapContainer").show();
                }

                jQuery(".nhs_ViewTypeMap").show();
                jQuery(".nhs_ViewTypeList").show();
                jQuery.googlepush("Search Events", "Search Results", "Homes Tab");
            } else if (control.is("#nhs_builderTabArea")) {
                var loadContent = Boolean(jQuery.GetQuerystringParameterByName("homebuilders"));
                self.loadBuilderAreaContent();
                self.showBuilderTabInfo(loadContent);
                jQuery.googlepush("Search Events", "Search Results", "Builders Tab");
                jQuery("#nhs_MapContainer").hide();
            }

            self.searchParameters.PageNumber = 1;
            self.searchParameters.SortOrder = false;
            self.searchParameters.SortBy = "Random";
            jQuery(".nhs_tabArea").removeClass("nhs_Active");
            control.addClass("nhs_Active");


            if (control.is("#nhs_builderTabArea") || self.CurrentTab === self.currentResultType) {
                if (self.CurrentTab === self.currentResultType && control.is("#nhs_builderTabArea") === false && self.searchParameters.IsBuilderTabSearch) {
                    self.updateSeoContent(false);
                }

                jQuery(".nhs_tabContents").hide();
                jQuery("#" + control.attr("id") + "Content").show();
            } else {
                self.updateResults = true;
                self.updateMap = true;
                self.CurrentTab = self.currentResultType;
                self.UpdateResults(false);
            }

            if (control.is("#nhs_builderTabArea")) {
                self.lazyLoadBrands();
            }
            else if (self.searchParameters.IsMapVisible && google !== undefined && self.googleApi !== undefined & self.googleApi.map !== null) {
                google.maps.event.trigger(self.googleApi.map, "resize");
                setTimeout(function () { self.googleApi.AutoFit(); }, 1500);
            }
        }

    });

    jQuery("#nhs_ResListAds").on("change", ".nhs_SortBy", function () {
        var control = jQuery(this);
        var value = control.val();
        var order = control.find(":selected").attr("sortorder");
        var text = control.find(":selected").text();

        self.searchParameters.SortBy = value;
        self.searchParameters.PageNumber = 1;
        self.searchParameters.SortOrder = order;

        if (value === "NumHomes") {
            jQuery.googlepush("Search Events", "Search Refinement", "Sort - Number of Homes");
        } else
            jQuery.googlepush("Search Events", "Search Refinement", "Sort - " + text.replace("# ", "#"));

        self.bindResults(false);
        jQuery("a.nhs_Page[data-page='1']").addClass("nhs_Active");
        jQuery("a.nhs_Next").removeClass("nhs_Disabled");
        jQuery("a.nhs_Previous").addClass("nhs_Disabled");
    });

    jQuery(".nhs_MapLink").on("click", "", function (event) {
        jQuery.NhsCancelEventAndStopPropagation(event);
        var id = jQuery(this).attr("id");

        jQuery.googlepush("Search Events", "Search Results", "View on Map");
        if (logger) logger.logEvent("VIEWMAP", id.split("-")[0], id.split("-")[1]);
    });

    jQuery("#nhs_ResListAds").on("click", ".nhs_MapLink", function (event) {
        jQuery.NhsCancelEventAndStopPropagation(event);
        var id = jQuery(this).attr("id");

        jQuery.googlepush("Search Events", "Search Results", "View on Map");
        if (logger) logger.logEvent("VIEWMAP", id.split("-")[0], id.split("-")[1]);

        var t = this.title || this.name || null; // jb - added this.firstChild.alt test
        var a = this.href || this.alt;
        var g = this.rel || false;
        tb_show(t, a, g);
    });

    jQuery("#nhs_ResListAds").on("click", "#nhs_showTabHome", function () {
        jQuery("#nhs_homeTabArea").click();
    });

    jQuery("#nhs_qmilnk").on("click", function (event) {
        jQuery.NhsCancelEvent(event);
        var isHotDealsActive = jQuery("#nhs_HotHomeslnk").is(".nhs_Active");
        self.searchParameters.Qmi = true;
        self.searchParameters.HotDeals = isHotDealsActive;
        self.updateAllInformation();
    });

    jQuery("#nhs_HotHomeslnk").on("click", function (event) {
        jQuery.NhsCancelEvent(event);
        var isQmiActive = jQuery("#nhs_qmilnk").is(".nhs_Active");
        self.searchParameters.Qmi = isQmiActive;
        self.searchParameters.HotDeals = true;
        self.updateAllInformation();
    });

    jQuery("#nhs_allQmi").on("click", function (event) {
        jQuery.NhsCancelEvent(event);
        var isHotDealsActive = jQuery("#nhs_HotHomeslnk").is(".nhs_Active");
        self.searchParameters.Qmi = false;
        self.searchParameters.HotDeals = isHotDealsActive;
        self.updateAllInformation();
    });

    jQuery("#nhs_allHotDeals").on("click", function (event) {
        jQuery.NhsCancelEvent(event);
        var isQmiActive = jQuery("#nhs_qmilnk").is(".nhs_Active");
        self.searchParameters.Qmi = isQmiActive;
        self.searchParameters.HotDeals = false;
        self.updateAllInformation();
    });

    jQuery("#nhs_ResListAds").on("mouseenter", ".nhs_CommResItem, .nhs_HomeResItem, .nhs_ResCustomBox, .nhs_FeaturedResItem", function () {
        jQuery(this).addClass("nhs_Hover");
    });

    jQuery("#nhs_ResListAds").on("mouseleave", ".nhs_CommResItem, .nhs_HomeResItem, .nhs_ResCustomBox, .nhs_FeaturedResItem", function () {
        jQuery(this).removeClass("nhs_Hover");
    });

    jQuery("#nhs_ResListAds").on("click", ".nhs_CommResItem, .nhs_HomeResItem, .nhs_ResCustomBox", function (event) {
        if (event.type === "click") {
            var url = jQuery(this).find("a:first-of-type").attr("href");
            window.location.href = url;
            return false;
        }
    });

    jQuery(".nhs_CommResItem, .nhs_HomeResItem, .nhs_ResCustomBox").on("click", "a.thickbox", function (event) {
        jQuery.NhsCancelEvent(event);
        if (event.stopPropagation) event.stopPropagation();
    });

    self.initPagination();

    var ul = jQuery("#nhs_PagingLinksConteiner");
    var insidewidth = ul.find("li").length * 34;
    ul.css("width", insidewidth + "px");

    self.movePagination(self.searchParameters.PageNumber);
},

NHS.Scripts.CommunityResults.CommResults.prototype.resetSearchWithinMap = function() {
    var self = this;

    jQuery("#nhs_LocationSearchBlock").hide();
    jQuery("#nhs_LocationSearchForm").show();

    self.searchParameters.MinLat = 0;
    self.searchParameters.MinLng = 0;
    self.searchParameters.MaxLat = 0;
    self.searchParameters.MaxLng = 0;
    self.searchParameters.WebApiSearchType = self.searchParameters.Radius > 0 ? "Radius" : "Exact";
    self.googleApi.map.setOptions({ minZoom: 2 });

    self.defaultSearchParameters.MinLat = 0;
    self.defaultSearchParameters.MinLng = 0;
    self.defaultSearchParameters.MaxLat = 0;
    self.defaultSearchParameters.MaxLng = 0;
    self.defaultSearchParameters.WebApiSearchType = self.searchParameters.Radius > 0 ? "Radius" : "Exact";

    self.googleApi.map.setCenter(new google.maps.LatLng(self.parameters.OptionsForMap.MapOptions.Latitude, self.parameters.OptionsForMap.MapOptions.Longitude));
},

NHS.Scripts.CommunityResults.CommResults.prototype.attachListMapEvents = function() {
    var self = this;

    jQuery("#nhs_List").click(function (event) {
        jQuery.NhsCancelEvent(event);

        if (!jQuery("#nhs_List").is(".nhs_Active")) {
            jQuery("#nhs_MapShow").removeClass("nhs_Active");
            jQuery("#nhs_List").addClass("nhs_Active");
            jQuery("#nhs_MapContainer").fadeOut("slow", function () { jQuery(".nhs_ResMapAreaToggle").hide(); });

            if (logger) {
                logger.logEvent("CRMAPC", 0, 0);
            }

            self.searchParameters.IsMapVisible = false;
            self.defaultSearchParameters.IsMapVisible = false;
            self.changeMapStatus();
            jQuery.googlepush("Search Events", "Search Results", "List Button");
            jQuery.googlepush("Search Events", "Search Results", "Collapse Map");
        }

        self.loadListViewAds();
    });

    jQuery("#nhs_MapShow").on("click", function(event) {
        self.openMapView(event, self);
    });

},

NHS.Scripts.CommunityResults.CommResults.prototype.attachMoveSeoToggle = function () {
    jQuery("#nhs_ShowAreaInfo").click(function () {
        var self = this;

        if (jQuery(self).text() === "Show area info") {
            jQuery(self).text("Hide area info");
            jQuery(self).addClass("nhs_Active");
        } else {
            jQuery(self).text("Show area info");
            jQuery(self).removeClass("nhs_Active");
        }

        jQuery("#nhs_SeoContentArea").toggle("fast");
       
    });
},

NHS.Scripts.CommunityResults.CommResults.prototype.showBuilderTabInfo = function (loadSeoContent) {
    var self = this;
    jQuery(".nhs_tabContents").hide();
    jQuery("#totalCounts").hide();
    jQuery(".nhs_ResRefine").hide();
    jQuery("#nhs_ResPaging").hide();
    jQuery("#nhs_ResViewType").hide();
    jQuery(".nhs_ResMapAreaToggle").hide();
    jQuery(".nhs_GroupingBar").hide();

    if (self.searchParameters.IsMapVisible) {
        jQuery("#nhs_MapContainer").hide();
    }

    jQuery(".nhs_ViewTypeMap").hide();
    jQuery(".nhs_ViewTypeList").hide();
    jQuery(".nhs_tabArea").removeClass("nhs_Active");
    
    if (jQuery("div.nhs_NoRes").length > 0) {
        jQuery("div.nhs_NoRes").hide();
    }
    
    jQuery("#nhs_builderTabArea").addClass("nhs_Active");
    self.loadBuilderAreaContent();
    jQuery("#nhs_builderTabAreaContent").show();
    
    if (loadSeoContent) {
        self.searchParameters.IsBuilderTabSearch = Boolean(jQuery.GetQuerystringParameterByName("homebuilders"));
        self.updateSeoContent(self.searchParameters.IsBuilderTabSearch);
    }

    self.lazyLoadBrands();
},

NHS.Scripts.CommunityResults.CommResults.prototype.saveCommunity = function (communityId, builderId) {
    var self = this;
    var community = { communityId: communityId, builderId: builderId };
    var url = self.SaveCommFavoriteUrl;
    jQuery.ajax({
        type: "POST",
        url: url,
        data: community,
        success: function (saved) {
            if (saved) {
                jQuery("#nhs_saveItem_" + communityId).attr("title", self.parameters.Translation.SavedAsFavorite).removeClass("nhs_Save").addClass("nhs_Saved");
            } else {
                jQuery("#nhs_saveItem_" + communityId).attr("title", self.parameters.Translation.Save).removeClass("nhs_Saved").addClass("nhs_Save");
            }
        }
    });
},

NHS.Scripts.CommunityResults.CommResults.prototype.saveHome = function (communityId, builderId, homeId, isSpec) {
    var self = this;
    var home = { communityId: communityId, builderId: builderId, homeId: homeId, isSpec: isSpec };
    var url = self.SaveCommFavoriteUrl;
    jQuery.ajax({
        type: "POST",
        url: url,
        data: home,
        success: function (saved) {
            if (saved) {
                jQuery("#nhs_saveItem_" + homeId).attr("title", self.parameters.Translation.SavedAsFavorite).removeClass("nhs_Save").addClass("nhs_Saved");
            } else {
                jQuery("#nhs_saveItem_" + homeId).attr("title", self.parameters.Translation.Save).removeClass("nhs_Saved").addClass("nhs_Save");
            }
        }
    });
},

NHS.Scripts.CommunityResults.CommResults.prototype.addScrollToSeoMarketContent = function () {
    jQuery("#nhs_Page #nhs_ResMktTxt > div").jScrollPane({ autoReinitialise: true });
},

NHS.Scripts.CommunityResults.CommResults.prototype.loadBuilderAreaContent = function () {
    var self = this;

    if (self.parameters.Brands === null || self.parameters.Brands.length === 0) {
        jQuery("#BuilderName > option").each(function () {
            var option = jQuery(this);
            if (option.val() !== "0") {
                var x = new Object();
                x.Key = parseInt(option.val());
                x.Value = option.text();
                self.parameters.Brands.push(x);
            }
        });
    }

    if (self.parameters.Brands === null || self.parameters.Brands.length === 0) {
        return false;
    }

    jQuery.ajax({
        url: self.parameters.getBuildersTabUrl,
        type: "POST",
        data: JSON.stringify({ 'brands': self.parameters.Brands, 'marketId': self.searchParameters.MarketId }),
        success: function(results) {
            jQuery("#nhs_builderTabAreaContent").html(results);
            self.lazyLoad.LazyLoadImagesGroup(jQuery("div.nhs_BuilderRes img.asyncBrand"));
        },
        contentType: "application/json",
        dataType: "html"
    });
}