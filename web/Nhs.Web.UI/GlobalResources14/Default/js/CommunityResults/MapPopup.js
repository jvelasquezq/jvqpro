﻿NHS.Scripts.CommunityResults.MapPopup = function (parameters) {
    this.parameters = parameters;
};

NHS.Scripts.CommunityResults.MapPopup.prototype = {
    init: function () {
        var self = this;
        var mapOptions = this.parameters.OptionsForMap;
        var googleApi = new NHS.Scripts.GoogleMapApi(mapOptions);

        googleApi.getIconMarkerPoint = function (results, result) {
            return self.parameters.icon;
        };

        googleApi.createMap();
        googleApi.options.Events.OnMarkersCreate = function (info, infowindow, marker) {
            infowindow.setContent(self.GetHtmlPoints(info));
            infowindow.open(googleApi.map, marker);
        };

        var point = [{
            Latitude: self.parameters.latitude,
            Longitude: self.parameters.longitude,
            Name: self.parameters.communityName,
            PriceRange: self.parameters.priceRange
        }];

        googleApi.options.Events.MarkerClickCallBack = function (info, infowindow, infowindowTooltip, marker) {
            infowindow.setContent(self.GetHtmlPoints(info));
            infowindow.open(googleApi.map, marker);
        };

        google.maps.event.addListener(googleApi.map, 'idle', self.RemoveGoogleInfo);
        googleApi.processResult(point);
    },

    GetHtmlPoints: function (info) {
        var onClickEvent = "jQuery('#nhs_CommResultsItemLink" + this.parameters.propertyId + "')[0].click()";
        var html = "<div class=\"nhs_MapHoverCard\"><a class=\"nhs_commDetail\" target='_top' href='javascript:void(null)' onclick=\"" + onClickEvent + "\">";

        //html += "<span>></span>";
        html += "<h2>" + info.Name + "</h2><p class=\"nhs_Price\">" + info.PriceRange + "</p>";
        html += "</a></div>";
        return html;
    },

    RemoveGoogleInfo: function () {
            if (jQuery(".gm-style > div > a").attr("href")) {
                var aux;
                var len = jQuery('.gm-style-cc').length;
                for (aux = 1; aux < len; aux += 1) {
                    jQuery('.gm-style-cc').eq('-1').remove();
                }
                jQuery(".gm-style > div > a").removeAttr("href").removeAttr("title");
            }
    }
};