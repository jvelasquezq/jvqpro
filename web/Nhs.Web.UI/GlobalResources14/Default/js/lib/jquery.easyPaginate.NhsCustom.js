/* 
 * easyPaginate | a jQuery plugin for easy pagination
 * @author Srihari Goud
 * @Link http://w3easystep.info
 * @Modify by Humberto J Schmidt
 */
(function ($) {
    $.fn.easyPaginate = function (option) {
        option = $.extend({}, $.fn.easyPaginate.option, option); //extending defaults
        option.pages = option.pages ? option.pages : Math.ceil(option.items / option.itemsOnPage) ? Math.ceil(option.items / option.itemsOnPage) : 1;
        return this.each(function () {
            var self = this;
            var elem = $(this);
            init(option.currentPage); // initializing the pagination
            function init(page) {
                option.currentPage = page ? page : option.currentPage;

                var next = parseInt(option.currentPage) + parseInt(1),
                    prev = Math.abs(parseInt(option.currentPage) - parseInt(1));

                if (prev <= 0)
                    prev = 1;

                if (next > parseInt(option.pages))
                    next = parseInt(option.pages);

                var p = [];
                //Add the Dropdown
                p.push('<p id="nhs_PageCount"><select id="nhs_PageCountSelect">');
                p.push('<option' + (parseInt(option.itemsOnPage) == 10 ? ' selected="selected" ' : ' ') + 'value="10">10 per page</option>');
                p.push('<option' + (parseInt(option.itemsOnPage) == 25 ? ' selected="selected" ' : ' ') + 'value="25">25 per page</option>');
                p.push('<option' + (parseInt(option.itemsOnPage) == 50 ? ' selected="selected" ' : ' ') + 'value="50">50 per page</option>');
                p.push('</select></p>');

                //Init the pagging
                p.push('<p class="nhs_PagingLinks"><span>');

                var middlePosition = Math.ceil(option.numOfPages / 2);

                var startLoopfrom = parseInt(option.currentPage) - middlePosition;
                var endLoopfrom = parseInt(option.currentPage) + middlePosition; //startLoopfrom + option.numOfPages;

                endLoopfrom = endLoopfrom - startLoopfrom != parseInt(option.numOfPages) ? parseInt(option.numOfPages) : endLoopfrom;

                var startPage = (startLoopfrom > 0) ? startLoopfrom : 2;
                var endPage = (endLoopfrom < parseInt(option.pages)) ? endLoopfrom : option.pages - 1;

                if (startPage + parseInt(option.numOfPages) >= parseInt(option.pages)) {
                    startPage = parseInt(option.pages) - parseInt(option.numOfPages) + 1;
                }

                if (startPage < 1) {
                    startPage = 2;
                }

                if (endPage - startPage < parseInt(option.numOfPages)) {
                    endPage = startPage + parseInt(option.numOfPages);
                } else {
                    endPage = parseInt(option.pages);
                }

                if (endPage > parseInt(option.pages)) {
                    endPage = parseInt(option.pages);
                }

                if (endPage == parseInt(option.pages)) endPage--;

                var url = option.url;
                p.push('<a href="' + url + '/page-' + prev + '" class="btnCss" rel="' + prev + '">Previous</a>');

                //if (startPage > 1 && startPage - 1 != '1') {
                p.push('<a href="' + url + '/page-1' + '" class="' + (option.currentPage == 1 ? 'nhs_Active' : "") + '" rel="' + 1 + '">' + 1 + '</a>');
                //} else if (startPage - 1 == '1') {
                //     p.push('<a class="' + (option.currentPage == 1 ? 'nhs_Active' : "") + '" rel="' + 1 + '">' + 1 + '</a>');
                //}
                p.push('<span>' + option.ellipseText + '</span>');

                var currentPageIterator = 1;
                for (var n = startPage; n <= endPage; n++) {
                    if (option.numOfPages == currentPageIterator) {
                        break;
                    }
                    p.push('<a href="' + url + '/page-' + n + '"  class=" ' + (option.currentPage == n ? 'nhs_Active' : "") + '" rel="' + n + '">' + n + '</a>');
                    currentPageIterator++;
                }

                p.push('<span>' + option.ellipseText + '</span>');

                //if (endPage < option.pages && option.pages - endPage != '1') {                    
                //     p.push('<a class="' + (option.currentPage == option.pages ? 'nhs_Active' : "") + '" rel="' + option.pages + '">' + option.pages + '</a>');

                //} else if (option.pages - endPage == '1') {
                p.push('<a href="' + url + '/page-' + option.pages + '"  class="' + (option.currentPage == option.pages ? 'nhs_Active' : "") + '" rel="' + option.pages + '">' + option.pages + '</a>');
                //}

                //p.push('<a ' + (option.pages > option.currentPage ? '"  rel="' + option.pages + '"  class="btnCss"' : 'class="btnCss"') + '>Next</a>');
                p.push('<a href="' + url + '/page-' + next + '"  class="btnCss" rel="' + next + '">Next</a>');

                p.push('</span></p>');

                var data = p.join('');
                elem.html(data);

                $('span a', elem).click(function (event) {
                    event.preventDefault();
                    var pageTobind = $(this).attr('rel'), ME = $(this);                    
                    init(pageTobind);
                    option.onClickcallback(pageTobind);
                });
            }

            function getPage(page) {
                return option.currentPage;
            }

            //$('a.nhs_Active', elem).trigger('click');
            option.onInit();

            return this;
        });

    };

    $.fn.easyPaginate.option = {
        debug: false,
        items: 0,
        itemsOnPage: 10,
        pages: 0,
        edges: 2,
        sides: 0,
        numOfPages: 16,
        ellipseText: '&hellip;',
        currentPage: 1,
        url: '',
        onInit: function () {
        },
        getPage: function (page) {
        },
        onClickcallback: function (page) {
        }
    };
})(jQuery);

