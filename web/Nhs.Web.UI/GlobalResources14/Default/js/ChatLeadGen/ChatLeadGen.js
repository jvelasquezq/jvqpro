﻿if (typeof NHS == "undefined") {
    NHS = {};
    NHS.Scripts = {};
}

NHS.Scripts.ChatLeadGen = function (parameters) {
    this.parameters = parameters;
    this.jsonActionsPath = '/SearchAlert/';
    this.actionGetSchools = 'GetSchools';
    this.actionGetBuilders = 'GetBuilders';
    this.actionGetAreas = 'GetAreas';
    this.actionGetCities = 'GetCities';
    this.saveAction = parameters.saveAction;
};

NHS.Scripts.ChatLeadGen.prototype = {
    init: function() {
        jQuery("#State").change(this.stateChangeHandler.bind(this));
        jQuery("#Area").change(this.areaChangeHandler.bind(this));
        jQuery("#Zip").change(this.zipChangeHandler.bind(this));
        jQuery("#SaveMyAlertButton").click(this.ValidateForm.bind(this));
        jQuery("#IsInternational").change(this.internationalChangeHandler.bind(this));
        jQuery("#FirstName").focus(this.onFocus.bind(jQuery("#FirstName")));
        jQuery("#LastName").focus(this.onFocus.bind(jQuery("#LastName")));
        jQuery("#EmailAddress").focus(this.onFocus.bind(jQuery("#EmailAddress")));
        jQuery("#State").focus(this.onFocus.bind(jQuery("#State")));
        jQuery("#Area").focus(this.onFocus.bind(jQuery("#Area")));
        jQuery("#Zip").focus(this.onFocus.bind(jQuery("#Zip")));
        jQuery("#PriceFrom").focus(this.onFocus.bind(jQuery("#PriceFrom")));
        jQuery("#PriceTo").focus(this.onFocus.bind(jQuery("#PriceTo")));
        
    },

    internationalChangeHandler: function () {
        var zip = jQuery("#ZipCodeUserInfo");
        var labelZip = jQuery('label[for="ZipCodeUserInfo"]');
        labelZip.removeClass("nhs_ErrorLabel");
        zip.removeClass("nhs_ErrorCLG");

        if (jQuery("#IsInternational").is(":checked")) {
            jQuery("#ZipCodeUserInfo")[0].setAttribute("disabled", "disabled");
        } else {
            jQuery("#ZipCodeUserInfo")[0].removeAttribute("disabled");
        }
    },

    stateChangeHandler: function() {
        var self = this;
        var state = jQuery("#State").val();

        self.clearZipLocation();
        jQuery.getJSON(self.getActionPath(self.actionGetAreas, "state=" + state), (function(data) {
            jQuery("#Area").fillSelect(data);
        }).bind(self));
    },

    areaChangeHandler: function() {
        var self = this;
        var area = jQuery("#Area").val();

        jQuery.getJSON(self.getActionPath(this.actionGetCities, "market=" + area), (function(data) {
            jQuery("#City").fillSelect(data);
        }).bind(this));

        this.fillMarketRelatedDropdowns(area);
    },

    zipChangeHandler: function() {
        var zip = jQuery("#Zip").val();
        if (ValidZipcode(zip)) {
            this.clearStateLocation();
            this.fillMarketRelatedDropdowns(zip);
        }
    },

    fillMarketRelatedDropdowns: function(market) {
        var self = this;
        jQuery.getJSON(self.getActionPath(self.actionGetSchools, "market=" + market), (function(data) {
            jQuery("#School").fillSelect(data);
        }).bind(this));

        jQuery.getJSON(self.getActionPath(self.actionGetBuilders, "market=" + market), (function(data) {
            jQuery("#Builder").fillSelect(data);
        }).bind(this));
    },

    clearZipLocation: function() {
        jQuery("#Zip").val('');
    },

    getActionPath: function(action, params) {
        return this.jsonActionsPath + action + "/?" + params;
    },

    ValidateForm: function() {
        var returnValue = true;

        //Start Name
        var name = jQuery("#FirstName");
        var lastName = jQuery("#LastName");
        var labelName = jQuery('label[for="FirstName"]');
        labelName.removeClass("nhs_ErrorLabel");
        name.removeClass("nhs_ErrorCLG");
        lastName.removeClass("nhs_ErrorCLG");

        if (name.val().trim() === "") {
            name.addClass("nhs_ErrorCLG");
            labelName.addClass("nhs_ErrorLabel");
            returnValue = false;
        }

        if (lastName.val().trim() === "") {
            lastName.addClass("nhs_ErrorCLG");
            labelName.addClass("nhs_ErrorLabel");
            returnValue = false;
        }
        //End Name

        //Start Email
        var email = jQuery("#EmailAddress");
        var labelEmail = jQuery('label[for="EmailAddress"]');
        labelEmail.removeClass("nhs_ErrorLabel");
        email.removeClass("nhs_ErrorCLG");

        if (!ValidEmail(email.val())) {
            email.addClass("nhs_ErrorCLG");
            labelEmail.addClass("nhs_ErrorLabel");
            returnValue = false;
        }
        //End Email

        //Start Zip Code
        var zip = jQuery("#ZipCodeUserInfo");
        var labelZip = jQuery('label[for="ZipCodeUserInfo"]');
        labelZip.removeClass("nhs_ErrorLabel");
        zip.removeClass("nhs_ErrorCLG");

        if (!ValidZipcode(zip.val()) && !jQuery("#IsInternational").is(":checked")) {
            zip.addClass("nhs_ErrorCLG");
            labelZip.addClass("nhs_ErrorLabel");
            returnValue = false;
        }
        //End Zip Code

        var zipFilter = jQuery("#Zip");
        var labelZipFilter = jQuery('label[for="Zip"]');
        zipFilter.removeClass("nhs_ErrorCLG");
        labelZipFilter.removeClass("nhs_ErrorLabel");

        var state = jQuery("#State");
        var labelState = jQuery('label[for="StateList"]');
        state.removeClass("nhs_ErrorCLG");
        labelState.removeClass("nhs_ErrorLabel");

        var area = jQuery("#Area");
        var labelArea = jQuery('label[for="Area"]');
        area.removeClass("nhs_ErrorCLG");
        labelArea.removeClass("nhs_ErrorLabel");

        if ((state.val() === "" && area.val() === "" && zipFilter.val().trim() === "") || ((state.val() === "" || area.val() === "") && zipFilter.val().trim() === "")) {
            state.addClass("nhs_ErrorCLG");
            labelState.addClass("nhs_ErrorLabel");
            area.addClass("nhs_ErrorCLG");
            labelArea.addClass("nhs_ErrorLabel");
            zipFilter.addClass("nhs_ErrorCLG");
            labelZipFilter.addClass("nhs_ErrorLabel");
            returnValue = false;
        }

        var priceFrom = jQuery("#PriceFrom");
        var labelPriceFrom = jQuery('label[for="PriceFrom"]');
        priceFrom.removeClass("nhs_ErrorCLG");
        labelPriceFrom.removeClass("nhs_ErrorLabel");

        if (priceFrom.val() === "") {
            priceFrom.addClass("nhs_ErrorCLG");
            labelPriceFrom.addClass("nhs_ErrorLabel");
            returnValue = false;
        }

        var priceTo = jQuery("#PriceTo");
        var labelPriceTo = jQuery('label[for="PriceTo"]');
        priceTo.removeClass("nhs_ErrorCLG");
        labelPriceTo.removeClass("nhs_ErrorLabel");

        if (priceTo.val() === "") {
            priceTo.addClass("nhs_ErrorCLG");
            labelPriceTo.addClass("nhs_ErrorLabel");
            returnValue = false;
        }
        return returnValue;
    },
    onFocus: function () {
        this.removeClass("nhs_ErrorCLG");
    }
}