/*****************************************************************************\

 Javascript "Widget" File
 
 @version: 1.0 - Dec 05, 2007
 @author: Allan Rojas / arojas @ builderhomesite . com
 
 INSTRUCTIONS:
 :: Only Edit the PARTNER_ID and the WEBSERVICE_URL variables accordingly.
 
\*****************************************************************************/

var PARTNER_ID = 5;
var WEBSERVICE_URL = "http://localhost/nhs.web.service/searchwidget.asmx";

var emptyString = /^\s*$/

// this method is an AJAX call, implementation is in the remote web service; and response is processed on the "GetStatesComplete" method.
function GetStates()
{
	try
	{
		var pl = new SOAPClientParameters();
		pl.add("partnerId", PARTNER_ID);
		SOAPClient.invoke(WEBSERVICE_URL, "GetStates", pl, true, GetStatesComplete);
	}
	catch (err)
	{
		alert(err.description);
	}
}

function GetStatesComplete(result)
{
	var ddlState = document.getElementById("ddlState");
	for(var i = 0; i < result.length; i++)
		ddlState.options[i] = new Option(result[i].Name, result[i].Abbreviation);
}

// this method is an mostly AJAX call, implementation is in the remote web service; and response is processed on the "GetSearchURLComplete" method.
function GetSearchURL()
{
	document.forms.frmSearch.btnSearch.value = "Please wait..";
	
	//validate zip or city.
	zipcity = document.forms.frmSearch.txtZip.value;
	if (emptyString.test(zipcity))
	{
		alert("Please enter a city/state or zip.");
		document.forms.frmSearch.txtZip.focus();
	}
	else
	{
		if (!isNaN(zipcity)) //if it's a number, validate zipcode format
		{
			reZip = new RegExp(/(^\d{5}$)|(^\d{5}-\d{4}$)/);
			if (!reZip.test(zipcity))
			{
				alert("Zip Code Is Not Valid");
				document.forms.frmSearch.txtZip.focus();
				return false;
			}
		}
		
		try
		{
			var pl = new SOAPClientParameters();
			pl.add("partnerId", PARTNER_ID);
			pl.add("cityzip", zipcity);
			pl.add("state", document.forms.frmSearch.ddlState.options[document.forms.frmSearch.ddlState.selectedIndex].value);
			pl.add("priceLow", document.forms.frmSearch.ddlPriceFrom.options[document.forms.frmSearch.ddlPriceFrom.selectedIndex].value);
			pl.add("priceHigh", document.forms.frmSearch.ddlPriceTo.options[document.forms.frmSearch.ddlPriceTo.selectedIndex].value);
			SOAPClient.invoke(WEBSERVICE_URL, "GetSearchURL", pl, true, GetSearchURLComplete);
		}
		catch (err)
		{
			alert(err.description);
		}
	}
	document.forms.frmSearch.btnSearch.value = "Find Homes";
}
	
function GetSearchURLComplete(result)
{
	window.location = result;
}