using System;
using System.Data;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Helpers;
using System.Linq;
using Nhs.Library.Helpers.Content;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.Util
{
    public partial class PartnerLocationsIndex : System.Web.UI.Page
    {
        private const int MAX_RESULTS = 20;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                DataTable locationsIndex = XPartnerHelper.GetLocationsIndex(XGlobals.Partner.PartnerId).Tables[0];
                string searchLocation = Request.Params[UrlConst.SearchLocation];
                string locationsFilter = String.Empty;

                if (!string.IsNullOrEmpty(searchLocation))
                {
                    // Changes abbreviations
                    var pathMapper = new ContextPathMapper();
                    var abbrsHelper = new AbbreviationsReader(pathMapper);
                    var abbreviations = abbrsHelper.ParseScripts();
                    string changedAbbreviations = TypeAheadAbbreviations.ChangeAbbreviations(searchLocation, abbreviations);

                    // Filters index
                    if (changedAbbreviations != searchLocation)
                        locationsFilter = String.Format("location LIKE '{0}%' OR location LIKE '{1}%' And type <> 5", searchLocation, changedAbbreviations);
                    else
                        locationsFilter = String.Format("location LIKE '{0}%' And type <> 5", searchLocation);
                }

                // Filters locations
                DataView partnerMarketsView = new DataView(locationsIndex, locationsFilter, String.Empty, DataViewRowState.CurrentRows);
                rptLocatinos.DataSource = SelectTopFrom(partnerMarketsView, MAX_RESULTS);
                rptLocatinos.DataBind();
            }
        }

        public static DataView SelectTopFrom(DataView dv, int rowCount)
        {
            DataTable dt = dv.ToTable();
            DataTable dtn = dt.Clone();
            rowCount = Math.Min(dt.Rows.Count, rowCount);
            for (int i = 0; i < rowCount; i++)
                dtn.ImportRow(dt.Rows[i]);
            return new DataView(dtn);
        }
    }
}
