﻿//NameSpace Declaration 
NHS = {};
NHS.Scripts = {};
NHS.Scripts.Helper = {};
NHS.Scripts.Helper.JSON = {};
NHS.Scripts.Globals = {};
NHS.Scripts.CommunityResults = {};
NHS.Scripts.CommunityDetail = {};
NHS.Scripts.HomeDetail = {};
NHS.Scripts.PropertyMap = {};
NHS.Scripts.MediaPlayer = {};
NHS.Scripts.SearchAlert = {};

var tempNum;
var fromP;
//Renaming the $ function
var $jq = jQuery.noConflict();

jQuery(document).ready(function () {

    HighlightNav();

    jQuery("#lnkSignIn").click(function () {
        jQuery.googlepush('Account Events', 'Create Account', 'Open Form - Header Link');
    });
    jQuery(".GAPush li a").click(function () {
        var ul = jQuery(this).parent().parent();
        var id = ul.attr("id");
        var listItem = ul.find("li");
        var index = listItem.index(jQuery(this).parent()) + 1;
        jQuery.googlepush('Site Links', 'SEO Links', id + index);
    });

    setTimeout(function () {
        jQuery(".nhs_SpotCommImg a").click(function () {
            var ul = jQuery(this);
            var type = ul.attr("type");
            jQuery.googlepush('Images', 'Spotlight - ' + type, 'Select Images');
        });
    }, 5000);

    // Global Nav
    jQuery("#nhs_Header > nav > ul > li > a").click(function (event) {
        jQuery.NhsCancelEvent(event);
    });
    var globalNav = jQuery("#nhs_Header > nav > ul");
    var timer = 0;
    //add hovers to submenu parents
    globalNav.find("li").each(function () {
        jQuery(this).find(">div").hide();
        if (jQuery(this).find(">div").length > 0) {
            jQuery(this).children("li>a").on("touchstart mouseenter mouseup", function (event) {
                if (jQuery(this).hasClass("nhs_Active")) {
                    if (event.type == "mouseenter") return false;
                    if (event.type == "touchstart") return true;
                    if (event.type == "mouseup") {
                        //hide submenus
                        jQuery(this).parent().find(">div").stop(true, true).fadeOut(500);
                        jQuery(this).removeClass("nhs_Active");
                    }
                }
                else {
                    //show subnav
                    if (event.type == "touchstart") {
                        globalNav.find("li>a").removeClass("nhs_Active");
                        globalNav.find("li>div").stop(true, true).fadeOut(500);
                    }
                    //jQuery(this).parent().find(">div").css("z-index", "9999999");
                    jQuery(this).parent().find(">div").stop(true, true).fadeIn(300);
                    jQuery(this).addClass("nhs_Active");
                }
                if (event.type == "touchstart") return false;
            });
            //hide submenus on exit  
            jQuery(this).mouseleave(function () {
                jQuery(this).find(">div").stop(true, true).fadeOut(500);
                //jQuery(this).find(">div").css("z-index", "9999998");
                jQuery(this).find("a").removeClass("nhs_Active");
            });
        }
    });

    ReferModuleHandler.SetRefer();

});



//This function create a JSON object to send it to the method controller on the server, and log the error in a file.
NHS.Scripts.Helper.logError = function logError(error, href, addInfo) {
    //keep the browser Info to the browserInfo variable to know in which browser occurs the error
    var browserInfo = "Browser: " + navigator.appCodeName + " - " + navigator.appName + " Version: " + navigator.appVersion;

    if (error !== null && typeof (error) !== 'undefined') {
        //Create the JSON object with the information error
        var dataPost = {
            Message: error.hasOwnProperty('message') ? error.message : error,
            Number: error.hasOwnProperty('lineNumber') ? error.lineNumber : error,
            Name: error.hasOwnProperty('message') ? error.name : error,
            Href: href,
            AddInfo: addInfo,
            BrowserInfo: browserInfo
        };

        var nhsJSONObject = jQuery.toJSON(dataPost);

        //Call to the Server method to log the error on the file
        jQuery.ajax({
            type: "POST",
            url: "/Log/LogError",
            data: { nhsJsError: nhsJSONObject },
            dataType: "json"
        });

        //Return true; to avoid show errors to the browsers users
        return true;
    }
};



//create delegate method
//can be used in ajax callbacks etc so that methods
//are executed in the context of the object instance
//and not that of jquery xhr object 
//read more abt this: http://weblogs.asp.net/bleroy/archive/2007/04/10/how-to-keep-some-context-attached-to-a-javascript-event-handler.aspx
NHS.Scripts.Helper.createDelegate = function createDelegate(object, method) {
    return function () { method.apply(object, arguments); };
};

Function.prototype.bind = function (owner) {
    var that = this;
    if (arguments.length <= 1) {
        return function () {
            return that.apply(owner, arguments);
        };
    } else {
        var args = Array.prototype.slice.call(arguments, 1);
        return function () {
            return that.apply(owner, arguments.length === 0 ? args : args.concat(Array.prototype.slice.call(arguments)));
        };
    }
};


// Cookies Write Function
NHS.Scripts.Helper.createCookie = function (name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else {
        var expires = "";
    }

    document.cookie = name + "=" + value + expires + "; path=/";
};

// Cookies Read Function
NHS.Scripts.Helper.readCookie = function (name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
};

// Cookies Erase Function
NHS.Scripts.Helper.eraseCookie = function (name) {
    NHS.Scripts.Helper.createCookie(name, "", -1);
};

NHS.Scripts.Helper.JSON.toString = function toString(obj) {
    var t = typeof (obj);
    if (t != "object" || obj === null) {
        // simple data type  
        //if (t == "string") obj = '"'+obj+'"';  
        return String(obj);
    }
    else {
        // recurse array or object  
        var n, v, json = [], arr = (obj && obj.constructor == Array);
        for (n in obj) {
            v = obj[n]; t = typeof (v);
            if (t == "string") v = '"' + v + '"';
            else if (t == "object" && v !== null) v = JSON.stringify(v);
            json.push((arr ? "" : '"' + n + '":') + String(v));
        }
        return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
    }
};

NHS.Scripts.Helper.stopBubble = function (e) {
    if (!e) var e = window.event;
    e.cancelBubble = true;
    if (e.stopPropagation) e.stopPropagation();
    //alert("bubble stop");
};


//Attach window.onerror to logError function, please do not change the position
window.onerror = function (error, url, lineError) {
    //return NHS.Scripts.Helper.logError(error, url, lineError)
};

NHS.Scripts.Helper.FieldDefaultValue = function (fieldId, defaultValue, defaultValueClass, submitButtonId, submitButton2Id) {
    if (typeof (fieldId) == 'string')
        this._field = jQuery('#' + fieldId);
    else
        this._field = jQuery(fieldId);
    this._defaultValue = defaultValue;
    this._defaultValueClass = defaultValueClass;
   
    if (submitButtonId != null)
        this._submitButton = jQuery('#' + submitButtonId);
    if (submitButton2Id != null) 
        this._submitButton2 = jQuery('#' + submitButton2Id);
};

NHS.Scripts.Helper.FieldDefaultValue.prototype = {

    initialize: function () {

        //Set value initially if none are specified
        if (this._field.val() == '') {
            this._field.addClass(this._defaultValueClass);
            this._field.val(this._defaultValue);
        }

        // Create delegates
        this._focusHandler = NHS.Scripts.Helper.createDelegate(this, this._onFocus);
        this._blurHandler = NHS.Scripts.Helper.createDelegate(this, this._onBlur);
        this._submitHandler = NHS.Scripts.Helper.createDelegate(this, this._onSubmit);

        // Attach events
        this._field.focus(this._focusHandler);
        this._field.blur(this._blurHandler);
        if (this._submitButton != null)
            this._submitButton.click(this._submitHandler);

        if (typeof (this._submitButton2) != "undefined") {
            this._submitButton2.click(this._submitHandler);
        }
    },

    // Remove values on focus
    _onFocus: function () {
        if (this._field.val() == this._defaultValue) {
            this._field.removeClass(this._defaultValueClass);
            this._field.val('');
        }
    },

    // Place values back on blur
    _onBlur: function () {
        if (this._field.val() == '') {
            this._field.addClass(this._defaultValueClass);
            this._field.val(this._defaultValue);
        }
    },

    //Capture form submission
    _onSubmit: function () {
        if (this._field != null && this._field.val() == this._defaultValue) {
            this._field.val('');
        }
    }
};


// Dropdown JSON fill helpers
jQuery.fn.clearSelect = function () {
    return this.each(function () {
        if (this.tagName == 'SELECT')
            this.options.length = 0;
    });
};

jQuery.fn.fillSelect = function (data) {
    return this.clearSelect().each(function () {
        if (this.tagName == 'SELECT') {
            var dropdownList = this;
            jQuery.each(data, function (index, optionData) {
                var option = new Option(optionData.Text, optionData.Value);

                if (jQuery.browser.msie) {
                    dropdownList.add(option);
                }
                else {
                    dropdownList.add(option, null);
                }
            });
        }
    });
};


Number.prototype.formatCurrency = function (p, c, d, t) {
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return p + s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

//What Browser
function UseOnSubmit() {
    var uA = navigator.userAgent.toLowerCase();
    var res = false;
    if (uA.indexOf("firefox") == -1 && uA.indexOf("apple") == -1 && uA.indexOf("msie 9.0") == -1 && uA.indexOf("chrome") == -1 && uA.indexOf("msie 10.0") == -1) {
        res = true;
    }

    return res;
}


//Method to fill Areas dropdown in Boyl search
function FillMarkets() {
    try {
        var areasSelect = jQuery('#ddlMarket');

        var selectedState = jQuery("[id*='ddlState'] :selected").text();
        if (selectedState != null && selectedState != '') {
            jQuery.ajax({
                type: "GET",
                url: "/BoylSearch/GetAreas",
                dataType: 'json',
                data: { state: selectedState },
                success: function (data) {
                    areasSelect.empty();
                    jQuery.each(data, function (index, area) {
                        areasSelect.append(jQuery('<option></option>').text(area.Text).val(area.Value));
                    });
                }
            });
        }
    }
    catch (ex) {

    }

}

//Close Static Content Overlay
/*jQuery(document).ready(function () {
jQuery("#stclose").click(function () {
jQuery('#stcontent').hide();
});
});*/

//Gets the path for the file used in static content
function getThePath(ps, tit) {
    var res;
    for (var i = 0; i < ps.length; i++) {
        var elem = ps[i].replace("\\", "//");
        tit = tit.replace("/", "\\").toLowerCase();
        if ((elem.toLowerCase().indexOf(tit) != -1) && (tit != "")) {
            res = elem;
            break;
        }

    }
    return res;
}

function reload(url) {
    window.location.href = url;
}

//Identify static content regions in the page
function HighlightRegion(listDif) {

    var list = new Array();
    list = listDif.split(',');
    var paths = new Array();
    var num = 1;
    var ind;
    var path;

    for (var i = 0; i < list.length; i++) {
        var elem = list[i].split(';');
        paths.push(elem[1]);
    }

    jQuery("div").each(function () {
        var cn = jQuery(this).attr("class");
        if (cn == "nhsStaticContent") {

            jQuery(this).addClass("nhs_Overlay");
            jQuery(this).mouseover(function () {
                jQuery(this).removeClass("nhs_Overlay_Out");
                jQuery(this).addClass("nhs_Overlay_Over");

                var stnum = jQuery(this).children().children("[class*='stNumber']");
                tempNum = jQuery(stnum).html();
                var tit = jQuery(stnum).attr("title");
                jQuery(stnum).html(tempNum + "   " + "<b class=\"stNumberPath\">" + getThePath(paths, tit) + "</b>");

            });

            jQuery(this).mouseout(function () {
                jQuery(this).removeClass("nhs_Overlay_Over");
                jQuery(this).addClass("nhs_Overlay_Out");

                var stnum = jQuery(this).children().children("[class*='stNumber']");
                jQuery(stnum).html(tempNum);

            });
        }
    });


    jQuery("b").each(function () {
        var cn2 = jQuery(this).attr("class");
        if (cn2 == "stNumber") {

            jQuery(this).html(num);

            if (num > 9)
                $(this).css("left", "8px");
            num = num + 1;
        }
    });
}

//Close Modal
function closeThisModal() {
    reloadParent();
}


// Enable the place holder functionaly in browsers that doesnt support it 
function SetTextBoxesDescriptions() {
    if (!Modernizr.input.placeholder) {
        jQuery('[placeholder]').focus(function () {
            var input = jQuery(this);
            if (input.val() == input.attr('placeholder')) {
                input.val('');
                input.removeClass('placeholder');
            }
        }).blur(function () {
            var input = jQuery(this);
            if (input.val() == '' || input.val() == input.attr('placeholder')) {
                input.addClass('placeholder');
                input.val(input.attr('placeholder'));
            }
        }).blur().parents('form').submit(function () {
            jQuery(this).find('[placeholder]').each(function () {
                var input = jQuery(this);
                if (input.val() == input.attr('placeholder')) {
                    input.val('');
                }
            })
        });
    }
}

function ss(w, h) {
    var el = jQuery('#FromPage').val();
    //alert(el);
    //alert(valid);
    //alert(w + " : " + h);
    if (el.indexOf("createalertmodal") != -1) {
        //alert("try resize");
        jQuery('#nhs_Login').fadeOut();
        tb_resizeWindow(w, h);
    }
}

function getRecoCheckedBox(box) {
    var chkval = box.checked;
    var arr = box.name.split('_');
    var index = arr[3];
    if (box.checked) {
        jQuery('#CblReqComm_' + index.toString()).val(false);
    }
    else {
        jQuery('#CblReqComm_' + index.toString()).val(true);
    }

}

function SetPromosToggle() {
    var charactersToShow = 215;
    var moreText = "...more";
    var lessText = "less";

    if (jQuery('#liPromos').hasClass("option2")) {
        charactersToShow = 100;
    } else if (jQuery('#liPromos').hasClass("option3")) {
        charactersToShow = 55;
    }
    
    

    jQuery('.more').each(function () {
        var content = jQuery(this).html();
        if (content.length > charactersToShow) {
            var chunkText = content.substr(0, charactersToShow);
            var otherText = content.substr(charactersToShow, content.length - charactersToShow + 1);
            var finalContent = chunkText + '<span class="moreellipses">' + '&nbsp;</span><span class="morecontent"><span>' + otherText + '</span>&nbsp;&nbsp;<a href="javascript:void(0)" class="morelink">' + moreText + '</a></span>';

            jQuery(this).html(finalContent);
        }
    });

    jQuery(".morelink").click(function () {
        if (jQuery(this).hasClass("less")) {
            jQuery(this).removeClass("less");
            jQuery(this).html(moreText);
        } else {
            jQuery(this).addClass("less");
            jQuery(this).html(lessText);
        }

        jQuery(this).parent().prev().toggle();
        jQuery(this).prev().toggle();
        return false;
    });
}

//Ensure the parent detail page is refreshed fater modal close event
function tb_refresParentDetailPage() {
    refreshparentwindow = true;
}

//Use console.log freely in your scripts to log through firefox firebug only in debug mode
    jQuery(document).ready(
        function() {
            if (NHS.Scripts.Globals.IsDebug && $j.browser.mozilla) {
                try {
                    window.loadFirebugConsole();
                } catch(err) {
                    if (typeof(console) == 'undefined')
                        console = {
                            log: function(msg) {
                            }
                        };
                }
            } else if (typeof(console) == 'undefined')
                console = {
                    log: function(msg) {
                    }
                };


        });

if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (obj, start) {
        for (var i = (start || 0), j = this.length; i < j; i++) {
            if (this[i] == obj) { return i; }
        }
        return -1;
    };
}


Array.prototype.groupBy = function(func) {
    for (var i = 0, l = this.length, arr = {}, key; i < l; i++) {
        if (!arr[(key = "k:" + func(this[i], i, this))])
            arr[key] = [];
        arr[key].push(this[i]);
    }
    return arr;
};


Array.prototype.groupNSort = function(func, sortFunc) {
    var grps = this.groupBy(func);
    var grpNsrt = new Array();
    for (grp in grps) {
        grps[grp].sort(sortFunc);
        grpNsrt = grpNsrt.concat(grps[grp]);
    }
    return grpNsrt;
};

function Reload() {
    window.location.reload();
}

function loadExternalScript(url, callback) {

    var script = document.createElement("script");
    script.type = "text/javascript";

    if (script.readyState) {  //IE
        script.onreadystatechange = function () {
            if (script.readyState == "loaded" ||
                    script.readyState == "complete") {
                script.onreadystatechange = null;
                if (callback) {
                    callback();
                }
            }
        };
    } else {  //Others
    script.onload = function () {
        if (callback) {
            callback();
        }
    };
    }

    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}

function ValidFullName(name) {
    var pattern = new RegExp(/^[a-zA-Z]+\s[a-zA-Z]+$/i);
    return pattern.test(name);
}

function SendToFriend() {
        var supportEmail = jQuery('#SupportEmail').val();
        var friendEmail = jQuery('#FriendEmail').val();
        var leadComments = jQuery('#LeadComments').val();
        var communityId = jQuery('#CommunityId').val();
        var builderId = jQuery('#BuilderId').val();
        var specId = jQuery('#SpecId').val();
        var planId = jQuery('#PlanId').val();
        var formData = {
            SupportEmail: supportEmail, FriendEmail: friendEmail, LeadComments: leadComments, CommunityId: communityId, BuilderId: builderId,
            SpecId: specId, PlanId: planId};
        jQuery.ajax({
            url: '/sendtofriendmodal',
            type: 'POST',
            data: formData,
            success: function () {
                tb_remove();
            }
        });
    }

function ValidateSendToFriend() {
    var isValid = true;
    var supportEmail = jQuery('#SupportEmail');
    var friendEmail = jQuery('#FriendEmail');
    var validationSummary = jQuery('#sendFriendValidationSummary');
    var errors = "";
    if (supportEmail.val().trim() === "") {
        errors = errors + "* From email address is required. <br />";
        isValid = false;
    }
    if (supportEmail.val().trim() != "" && !ValidEmail(supportEmail.val().trim())) {
        errors = errors + "* From email address is not valid. <br />";
        isValid = false;
    }
    if (friendEmail.val().trim() === "") {
        errors = errors + "* Friend's email address is required. <br />";
        isValid = false;
    } else {
        var friendsEmails = friendEmail.val().trim().split(",");
        if (friendsEmails.length > 3) {
            isValid = false;
            errors = errors + "* You can only send to 3 friend's emails as maximum. <br />";
        } else {
            for (var i = 0; i < friendsEmails.length; i++) {
                if (!ValidEmail(friendsEmails[i])) {
                    isValid = false;
                    errors = errors + "* Friend's email address is not valid. <br />";
                    break;
                }
            }
        }
    }


    if (!isValid) {
        validationSummary.html(errors);
        validationSummary.addClass("nhs_Error");
        validationSummary.show();
    }
    return isValid;
}

function ValidEmail(email) {
    return jQuery.ValidEmail(email);
}

function ValidFullNameWithNumberLastName(name) {
//    var pattern = new RegExp(/^[a-zA-Z,.0-9''-]+ [a-zA-Z,.0-9''-]+/i);
    var pattern = new RegExp(/^([A-Za-z]+(-?'?[A-Za-z+'?-?]+))(\s([A-Za-z]+(-?'?[A-Za-z+'?-?]+)?))+?$/i);
    return pattern.test(name) && name.split(' ').length > 1;
}

function ValidZipcode(zip) {
    var pattern = new RegExp(/^\d{5}(-\d{4})?$/i);
    return pattern.test(zip);
}

function HighlightNav() {
    var path = location.pathname;

    var highlighted = false;

    if (jQuery('#mv_SearchNav li a[href="' + path + '"]') != null) {
        jQuery('#mv_SearchNav li a[href="' + path + '"]').parent().addClass('active');
        highlighted = true;
    }

    if ((path.charAt(path.length - 1) == '/') && (!highlighted)) {
        jQuery('#mv_SearchNav li a[href="' + path.substring(0, path.length - 1) + '"]').parent().addClass('active');
        highlighted = true;
    }

    var pathsSlash = path.split('/');

    if (pathsSlash.length > 2) {
        jQuery('#mv_SearchNav li a[href="/' + pathsSlash[1] + '/"]').parent().addClass('active');
    }
}

function FreeBrochureSubmitGAEvents() {
    if (jQuery('#CreateAlert').prop('checked')) {
        jQuery.googlepush('Lead Events', 'Search Results CTA Free Brochure', 'Option Checked - Send Me More');
    }
    if (jQuery('#Newsletter').prop('checked')) {        
        jQuery.googlepush('Lead Events', 'Search Results CTA Free Brochure', 'Option Checked - Free Newsletter');
    }
    if (jQuery('#Promos').prop('checked')) {
        jQuery.googlepush('Lead Events', 'Search Results CTA Free Brochure', 'Option Checked - Special Promotions');
    }
}

function detectIE() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    var trident = ua.indexOf('Trident/');

    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    if (trident > 0) {
        // IE 11 (or newer) => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    // other browser
    return false;
}

function disableScrollKeys() {
    jQuery(document).keypress(function (e) {
        var k = e.keyCode;
        if (k >= 37 && k <= 40) {
            return false;
        }
    });
}

function getGUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g,
        function(c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
}

function enableScrollKeys() {
    jQuery(document).unbind('keypress', null);
}

function SubmitGetBrochures() {
    var count = jQuery("input[id^='chkReqCommBrochure_']:checked").length + jQuery("input[id^='chkReqHomeBrochure_']:checked").length;
    jQuery.googlepush('Lead Events', 'Search Results CTA Free Brochure', 'Submit Form - Get Free Brochures', count);
    jQuery.SetDataLayerPair('recommendedLead',count);
}


var VWOCountTimer = null;
function VWOReady(action) {
    if (typeof (_vwo_code) != "undefined") {
        if (_vwo_code.finished()) {
            clearTimeout(VWOCountTimer);
            if (jQuery.isFunction(action))
                action.call();
        } else {
            VWOCountTimer = setTimeout(function () { VWOReady(action); }, 500);
        }
    } else if (jQuery.isFunction(action))
        action.call();
}

//This code just check for the refer after a # in the url, if is pass by QueryString or post the controller handle that
var ReferModuleHandler = (function ($) {
    /// the $ is just to handle a common way for jQuery, under this scope
    'use strict';
    var my = {}; // This is our internal object where we define the  

    ///Private Properties    
    var finalUrl = "/common/setrefer";
    ///END Private Properties

    ///Private Methods
    function getHashTagRefer() {
        if ($.GetHashTagParameter) {
            return $.GetHashTagParameter('refer');
        } else {
            return jQuery.GetHashTagParameter('refer');
        }
    }

//    function getHashTagParameter (parameterName) {
//        var exp = "[#|&]" + parameterName + "=" + "([^&]+)(&|$)";
//        var val = (RegExp(exp).exec(decodeURIComponent(location.hash)) || [, null])[1];

//        if (val == "null") {
//            val = "";
//        }

//        return val;
//    }
    
    function setReferTag() {
        var refVal = getHashTagRefer();
        var refUrl = "";

        if (refVal != null && refVal != "") {
            if (typeof (document.referrer) != 'undefined' && document.referrer != "") {
                refUrl = document.referrer;
            }

            $.ajax({
                type: "GET",
                url: finalUrl,
                data: { refVal: refVal, refUrl: refUrl }
            }).done(function (data) {
                //...do something here if is required                
            });
        }
    }
    ///END Private Methods


    ///Public Methods
    my.SetRefer = function () {
        setReferTag();
    };
    ///END Public Methods

    return my; //return the object when you define your public logic
} (jQuery));
