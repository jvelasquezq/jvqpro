﻿_mediaPlayerRef = null;


function BrightCoveGlobalCallback(response) {
    _mediaPlayerRef.popComVideos(response);
}

var _globalCaption = null;

NHS.Scripts.GalleryViewer = function (parameters) {
    this.imgNLinks = null;
    this.youtubeLinks = null;
    this.videos = null;

    this.globalCaption = null;
    this.listingType = 'comm';
    this.mediaItems = null;
    this.gnocdmain = null;
    this.gnoimgMed = null;
    this.gnoimgLarge = null;
    this.toggleVideo = 0;

    this.currentParams = parameters;
    this.logger = parameters.logger || logger; // logger initialized in the detail or results class, if not global logger

    var isPro = jQuery("#pro_LogoDiv").length;
    var vHeight = (isPro && parameters.isCommResults == 0) ? 305 : 307; // hack for pro and detail pages

    // This js class plays any BC, Vimeo or youtube video given the videoId, we pass the html id where the video would be rendered and video sizes, these default values are for Details pages.
    this.player = new NHS.Scripts.WebPlayers({
        videoWidth: (parameters.videoWidth || 460),
        videoHeight: (parameters.videoHeight || vHeight),
        youtubeControlId: 'youtubeP',
        vimeoControlId: 'youtubeP',
        brightcoveControlId: 'brightcoveP',
        brightcoveToken: parameters.brightcoveToken
    });

    this.comID = parameters.hasOwnProperty('communityID') ? parameters.communityID : 0;
    this.planID = parameters.hasOwnProperty('planID') ? parameters.planID : 0;
    this.specID = parameters.hasOwnProperty('specID') ? parameters.specID : 0;
    this.builderID = parameters.hasOwnProperty('builderID') ? parameters.builderID : 0;

    // Code for jquery-ad.gallery.js
    if (parameters.listingType == 'commres') {
        _wrapperW = 300;
        _wrapperH = 194;
    }
    else {
        _wrapperW = 0;
        _wrapperH = 0;
    }

    this.GallerylookUpTable = {
        "cv": this.currentParams.CommunityVideoText,
        "sv": this.currentParams.CommunityVideoText,
        "hv": this.currentParams.HomeVideoText,
        "ci": this.currentParams.CommunityImageText,
        "hi": this.currentParams.HomeImageText,
        "ev": this.currentParams.ExternalVideoText,
        "pv": this.currentParams.PlanViewerText,
        "vir": this.currentParams.VirtualTourText,
        "vt": this.currentParams.VirtualTourText,
        "ele": this.currentParams.ElevationText,
        "flp": this.currentParams.FloorPlanText,
        "int": this.currentParams.InteriorText,
        "ext": this.currentParams.ExteriorText,
        "lm": this.currentParams.CommunityLotMapText
    };
};

NHS.Scripts.GalleryViewer.prototype =
{
    initGallery: function () {
        BCMAPI.token = this.currentParams.brightcoveToken;


        if (this.currentParams.hasOwnProperty('titleBarText')) {
            jQuery('#nhs_ImagePlayerTitle').text(this.currentParams.titleBarText);
        }
        if (this.currentParams.mediaObjs !== null && this.currentParams.mediaObjs !== undefined && this.currentParams.mediaObjs.length == 0 && this.currentParams.externalLinks != undefined &&
            this.currentParams.externalLinks.length == 0 && this.currentParams.type != 'results') {
            this.displayNoMedia();
        } else {
            this.listingType = this.currentParams.hasOwnProperty('specID') ? "spec" : this.currentParams.hasOwnProperty('planID') ? "plan" : this.currentParams.hasOwnProperty('communityID') ? "com" : "commres";

            this.gnoimgLarge = this.currentParams.imgStaticUrl + "globalresources14/default/images/no_photo/no_photos_460x307.png";
            this.gnoimgMed = this.currentParams.imgStaticUrl + "globalresources14/default/images/no_photo/no_photos_180x120.png";
            //this.gnocdmain = this.currentParams.imgStaticUrl + "globalresourcesmvc/default/images/imgs-player/blank.gif";

            if (this.currentParams.mediaObjs !== null && this.currentParams.mediaObjs !== undefined && this.currentParams.mediaObjs.length == 0) {
                this.mediaItems = null;
            } else {
                this.mediaItems = this.currentParams.mediaObjs; //mediaobjects jsonised
            }

            var useAlterText = this.currentParams.hasOwnProperty('captionBarText');

            if (this.listingType == "com") {
                jQuery('#' + this.currentParams.titleBarID).text(useAlterText ? this.currentParams.titleBarText : this.currentParams.CommunityGalleryText);
            } else if (this.listingType == "commres") {
                jQuery('#' + this.currentParams.titleBarID).text(useAlterText ? this.currentParams.TourVideoGalleryText : this.currentParams.CommunityVideoGalleryText);
            } else {
                jQuery('#' + this.currentParams.titleBarID).text(useAlterText ? this.currentParams.titleBarText : this.currentParams.HomeGalleryText);
            }

            if (this.currentParams.type == 'community' || this.currentParams.type == 'home')
                this.loadDetailsMediaPlayer();
            else if (this.currentParams.type == 'results')
                this.loadShowCasePlayer();
        }
    },

    displayNoMedia: function () {
        var img = jQuery('<img src=\"' + this.currentParams.imgStaticUrl + "globalresources14/default/images/no_photo/no_photos_460x307.png" + '\" alt=\"No photo or video available\" ' + '/>');
        jQuery('.ad-gallery .ad-image-wrapper').append(img);
    },

    afterProcess: function () {
        var self = this;
        var options = { resource_root: this.currentParams.imgBaseUrl, listingType: this.listingType, url: this.currentParams.url, description: this.currentParams.description, OfText: self.currentParams.OfText, callbacks: { afterImageVisible: (function () { this.toggleVideo = 0; this.player.stopVideo(); }).bind(this) } };

        if (detectIE() !== false)
            setTimeout(this.startGallery, 100, this, options);
        else
            this.startGallery(self, options);

    },

    startGallery: function(self, options) {
        var galleries = jQuery('.ad-gallery').adGallery(options);

        galleries[0].settings.effect = "fade";
        galleries[0].settings.mp_type = this.listingType;
        galleries[0].settings.nocdmain = this.gnocdmain;

        if (this.listingType == 'com') {
            galleries[0].settings.noimg = this.gnoimgLarge;
        } else {
            galleries[0].settings.noimg = this.gnoimgMed;
            galleries[0].settings.wrapperW = 300;
            galleries[0].settings.wrapperH = 124;
        }
    },

    hideImgTitle: function () {
        jQuery('div.nhs_MediaImageTitle').hide();
    },

    setCaption: function (url, subType, title, index, initialLoad) {
        var caption;
        if (this.listingType == "com") {
            caption = (subType == "ci" || subType == "lm") ? this.currentParams.CommunityImageText : this.currentParams.HomeImageText;
        } else {
            caption = this.GallerylookUpTable[subType];
        }

        if (this.currentParams.hasOwnProperty('captionBarText')) {
            caption = this.currentParams.captionBarText + caption;
        } else {
            caption = this.currentParams.NowShowingText + " : " + caption;
        }

        jQuery('#' + this.currentParams.captionBarID).text(caption);

        if (jQuery.trim(title) == '') {
            jQuery('div.nhs_MediaImageTitle').hide();
        }
    },
    goBack: function () {
        jQuery('#vidP').hide();
        jQuery('#medP').show();
    },

    getCaption: function (asset) {
        var caption;
        if (this.listingType == "com") {
            if (asset.Type == "v") {
                caption = (asset.SubType == "cv" || asset.SubType == "sv" || asset.SubType == "yt" || asset.SubType == "vm" || asset.SubType == null) ? this.currentParams.CommunityVideoText : this.currentParams.HomeVideoText;
            } else if (asset.Type == "i") {
                caption = (asset.SubType == "ci" || asset.SubType == "lm") ? this.currentParams.CommunityImageText : this.currentParams.HomeImageText;
            } else
                caption = this.GallerylookUpTable[asset.SubType];
        } else {
            caption = this.GallerylookUpTable[asset.SubType];
            if (caption == null)
                caption = this.currentParams.HomeVideoText;
        }

        if (this.currentParams.hasOwnProperty('captionBarText')) {
            caption = this.currentParams.captionBarText + caption;
        } else {
            caption = " " + this.currentParams.NowShowingText + ": " + caption;
        }

        return caption;
    },

    galleryloadImage: function (url, subType, title, index, initialLoad, isvideo, nhsid, videoid, videoStillUrl, isYoutube, isVimeo) {

        var caption = null;

        if (this.listingType == "com") {
            if (isvideo)
                caption = (subType == "cv" || subType == "sv") ? this.currentParams.CommunityVideoText : this.currentParams.HomeVideoText;
            else
                caption = (subType == "ci" || subType == "lm") ? this.currentParams.CommunityImageText : this.currentParams.HomeImageText;
        } else {
            caption = this.GallerylookUpTable[subType];
        }

        caption = " " + this.currentParams.NowShowingText + ": " + caption;
        jQuery('#' + this.currentParams.captionBarID).text(caption);

        if (this.listingType == "commres") {
            jQuery.googlepush('Videos', 'Search Results', 'Community');
        }
        var newImgUrl = "";

        jQuery('#medP').show();
        jQuery('#vidP').hide();

        if (this.player != undefined)
            this.player.stopVideo();

        if (isvideo) {
            if (this.toggleVideo != 1) {

                jQuery('#medP').hide();
                jQuery('#vidP').show();
                if (isYoutube || isVimeo) {
                    jQuery('#nhsplayerloading').hide();
                    jQuery('#brightcoveP').hide();
                    jQuery('#youtubeP').css('display', '');
                } else {
                    jQuery('#nhsplayerloading').show();

                    jQuery('#youtubeP').hide();
                    jQuery('#brightcoveP').show();
                }

            }

            var playerTitle = this.currentParams.NowShowingText + ": " + ((this.listingType == "com") ? this.currentParams.CommunityVideoText : this.currentParams.HomeVideoText);
            this.displayTitle(title, playerTitle);
            jQuery('#nhs_MediaMaximize').hide();
            jQuery('#nhsplayerloading').hide();

            if (isYoutube || isVimeo) {

                if (isYoutube) {
                    this.player.playYouTubeVideo(videoid);
                }
                else {
                    this.player.playVimeoVideo(videoid);
                }
            } else {
                this.player.playBrightcoveVideo(videoid);
            }

            this.logVideo(nhsid);
            this.toggleVideo = 1;

            newImgUrl = videoStillUrl;
        } else {
            if (this.toggleVideo == 1) {
                if (this.player != undefined)
                    this.player.stopVideo();

                jQuery('#vidP').hide();
                jQuery('#medP').show();
            }
            this.toggleVideo = 0;
            newImgUrl = this.currentParams.imgBaseUrl + url;

            if (!this.currentParams.isBasicCommunity) {
                if (this.listingType == "spec" || this.listingType == "plan")
                    jQuery.googlepush('Images', 'Home - Gallery', 'Select Images');
                else
                    jQuery.googlepush('Images', 'Community - Gallery', 'Select Images');
            }
        }

        this.updatePinterest(newImgUrl, isvideo);
    },


    logVideo: function (nhsid) {

        var siteVar = "";
        if (this.listingType == 'commres') {
            this.logger.logEventWithParameters('CRVID', new { communityId: nhsid });
        } else if (this.listingType == 'com') {
            this.logger.logEventWithParameters('CDVID', this.currentParams.logInfo);
            siteVar = 'siteCommVV';
        } else {
            this.logger.logEventWithParameters('HDVID', this.currentParams.logInfo);
            siteVar = 'siteHomeVV';
        }

        if (siteVar != "")
            jQuery.SetDataLayerPair(siteVar);
    },

    displayTitle: function (title, caption) {
        jQuery('#' + this.currentParams.captionBarID).text(caption);

        if (jQuery.trim(title) != '') {
            jQuery('#' + this.currentParams.titleBarText).show().text(title);
            jQuery('div.nhs_MediaImageTitle').show();
        } else {
            jQuery('div.nhs_MediaImageTitle').hide();
        }
    },

    updatePinterest: function (imgUrl, isVideo) {
        var pinInstance = jQuery(".nhs_MediaPlayerContainer .pin-it-button");
        if (pinInstance.length > 0) {
            var currentHref = "http://pinterest.com/pin/create/button/?url=" + encodeURIComponent(this.currentParams.url) + "&media=" + encodeURIComponent(imgUrl) + "&description=" + encodeURIComponent(isVideo ? "Video!: " + this.currentParams.description : this.currentParams.description);
            pinInstance.attr("href", currentHref);
            this.currentParams.firstImgUrl = imgUrl;
        }
    },

    loadDetailsMediaPlayer: function () {
        var tags = this.getTags();
        var hasVideos = (tags != '');
        _mediaPlayerRef = this;

        if (hasVideos) {
            BCMAPI.find('find_videos_by_tags',
                {
                    or_tags: tags,
                    fields: "id,referenceId,thumbnailURL,tags,videoStillURL",
                    callback: 'BrightCoveGlobalCallback'
                });
        } else {
            this.popImgnLnksFromMediaItems(false);
        }
    },

    loadShowCasePlayer: function () {
        var tags = this.getTags();
        _mediaPlayerRef = this;

        var marketTagName = this.currentParams.marketName + "_" + this.currentParams.marketID;
        tags += (tags !== '') ? ",showcasevideo," : "showcasevideo";
        tags = marketTagName + "," + "allmarkets-" + this.currentParams.brightCoveSuffix + "," + tags;

        BCMAPI.find('find_videos_by_tags',
            {
                or_tags: tags,
                fields: "name,id,referenceId,thumbnailURL,tags,videoStillURL",
                callback: 'BrightCoveGlobalCallback'
            });
    },

    getTags: function () {
        var tags = '';
        if (this.mediaItems != null) {
            jQuery.each(this.mediaItems, function (index, mi) {
                if (mi.Type === 'v') {
                    tags += mi.VideoID + ',';
                }
            });
            tags = tags.substr(0, tags.length - 1);

        }
        return tags;
    },

    popComVideos: function (response) {
        this.popVideos(response, "cv");
    },

    popShowcaseVideos: function (response) {
        this.popVideos(response, 'sv');
    },

    initializePinterest: function (bcresponse) {
        this.updatePinterest(bcresponse.items[0].videoStillURL, true);
    },

    popVideos: function (response, subtype) {

        this.popImgnLnksFromMediaItems((response.items.length != 0));

        if (response.items.length > 0) {
            this.initializePinterest(response);
        }

        var videos = new Array();
        var showcaseVideos = new Array();
        var marketVideos = new Array();
        var lkup = this.GallerylookUpTable;
        var videoType = subtype;
        var videoTitle = '';
        //add refid check to remove deleted videos
        jQuery.each(response.items, (function (index, video) {
            var nhsid = 0;
            var sort = 0;
            var videoid = 0;

            if (video.referenceId == null)
                video.referenceId = "";

            //get tags and tag info
            if (video.tags.length >= 2) {
                var hash = video.tags[1];
                var tag = video.tags[0];
                if (hash.indexOf('-') != -1) {
                    var tmp = tag;
                    tag = hash;
                    hash = tmp;

                }
                var t = tag.split('-');
                if (t.length > 2) {
                    nhsid = t[1];
                    videoid = t[2];

                }

                var deletedVideo = true;
                //get title,sort
                if (this.mediaItems != null && video.tags[0] != "showcasevideo") {
                    jQuery.each(this.mediaItems, (function (index, mi) {
                        if (mi.RefID == videoid) {
                            deletedVideo = false;
                            videoTitle = mi.Title;
                            sort = mi.Sort * 1;
                            return false;
                        }
                    }).bind(this));
                }
            }

            var duplicateVideo = false;
            //check hashtag check to remove duplicates
            if (videos.length > 0 && hash != "" && video.tags[0] != "showcasevideo") {
                jQuery.each(videos, (function (i, v) {
                    if (hash == v.hash) {
                        duplicateVideo = true;
                        return false;
                    }
                }).bind(this));
            }


            if (!duplicateVideo && !deletedVideo && video.tags[0] != "showcasevideo") {
                videos[videos.length] =
                {
                    Type: "v",
                    SubType: videoType,
                    Thumbnail: video.thumbnailURL,
                    Title: (videoTitle == null || videoTitle == '') ? video.name : videoTitle,
                    Url: video.id,
                    ID: nhsid,
                    Sort: sort,
                    hash: hash,
                    refID: videoid,
                    VideoStillURL: video.videoStillURL,
                    IsYouTubeVideo: false,
                    IsVimeoVideo: false
                };
            }
            ;


            if (video.tags[0] == "showcasevideo") {

                showcaseVideos[showcaseVideos.length] =
                {
                    Type: "v",
                    SubType: videoType,
                    Thumbnail: video.thumbnailURL,
                    Title: video.name,
                    Url: video.id,
                    ID: 0,
                    Sort: 1,
                    hash: hash,
                    VideoStillURL: video.videoStillURL,
                    IsYouTubeVideo: false,
                    IsVimeoVideo: false
                };
            }


            if (jQuery.inArray(this.currentParams.marketName + "_" + this.currentParams.marketID, video.tags) != -1 || jQuery.inArray("allmarkets-" + this.currentParams.brightCoveSuffix, video.tags) != -1) {
                var order = 1000;

                if (video.referenceId != null && video.referenceId.indexOf('_') != -1)
                    order = video.referenceId.substr(video.referenceId.lastIndexOf('_') + 1);

                marketVideos[marketVideos.length] =
                {
                    Type: "v",
                    SubType: videoType,
                    Thumbnail: video.thumbnailURL,
                    Title: video.name,
                    Url: video.id,
                    ID: nhsid,
                    Sort: parseInt(order),
                    hash: hash,
                    VideoStillURL: video.videoStillURL,
                    Ref: video.referenceId,
                    IsYouTubeVideo: false,
                    IsVimeoVideo: false
                };
            }

        }).bind(this));

        if (response.items.length != 0) {

            //sort by comm results
            var newVideo = new Array();
            if (typeof this.mediaItems != 'undefined' && this.mediaItems != null) {
                for (var i = 0, l = this.mediaItems.length; i < l; i++) {
                    jQuery.each(videos, (function (index, v) {
                        if (this.mediaItems[i].RefID.toString() == v.refID)
                            newVideo.push(v);
                    }).bind(this));
                }
            }

            videos = newVideo.groupNSort(function (v) { return v.ID; }, function (v1, v2) { return v1.Sort - v2.Sort; });

            if (showcaseVideos.length > 0) {
                videos = videos.concat(showcaseVideos);
            }

            if (marketVideos.length > 0) {
                marketVideos = marketVideos.sort(function (a, b) {
                    var ref1 = a.Sort;
                    var ref2 = b.Sort;
                    if (ref1 < ref2) return -1;
                    if (ref1 > ref2) return 1;
                    ref1 = a.Ref.indexOf('allmarkets') != -1 ? 'z' : a.Ref;
                    ref2 = b.Ref.indexOf('allmarkets') != -1 ? 'z' : b.Ref;
                    return ref1.localeCompare(ref2);
                });

                videos = marketVideos.concat(videos);
            }


            var noOfItems = videos.length;


            if (!!this.imgNLinks && this.imgNLinks.length != 0) {
                noOfItems = noOfItems + this.imgNLinks.length;
            }
            var itemScrollLength = subtype == "sv" ? 2 : 3;

            var hasImages = this.imgNLinks.length > 0;
            var hasVideos = videos.length > 0;
            this.globalCaption = new Array();
            jQuery.each(videos, (function (index, asset) {
                this.globalCaption.push(this.getCaption(asset));
            }).bind(this));

            if (this.youtubeLinks.length > 0) {
                jQuery.each(this.youtubeLinks, (function (index, asset) {
                    this.globalCaption.push(this.getCaption(asset));
                }).bind(this));
            }

            jQuery.each(this.imgNLinks, (function (index, asset) {
                this.globalCaption.push(this.getCaption(asset));
            }).bind(this));


            if (hasVideos) {
                if (this.listingType == "com") {
                    caption = (videos[0].SubType == "cv") ? this.currentParams.CommunityVideoText : this.currentParams.HomeVideoText;
                } else {
                    caption = this.GallerylookUpTable[videos[0].SubType];
                }
                caption = this.currentParams.NowShowingText + ": " + caption;
                jQuery('#' + this.currentParams.captionBarID).text(caption);
            } else if (hasImages) {
                if (this.listingType == "com") {
                    caption = (this.imgNLinks[0].SubType == "ci" || this.imgNLinks[0].SubType == "lm") ? this.currentParams.CommunityImageText : this.currentParams.HomeImageText;
                } else {
                    caption = this.GallerylookUpTable[this.imgNLinks[0].SubType];
                }
                caption = this.currentParams.NowShowingText + ": " + caption;
                jQuery('#' + this.currentParams.captionBarID).text(caption);
            }

            jQuery.template("mpTmpl", "\
                        {{if IsYouTubeVideo || IsVimeoVideo }} \
                         <li itemprop=\"video\" itemscope itemtype=\"http://schema.org/VideoObject\">\
                        <meta itemprop=\"thumbnail\" content=\"${Thumbnail}\"/>\
                        <a href=\"${Thumbnail}\"   tmpclick=\"_mediaPlayerRef.galleryloadImage(\'${Thumbnail}\',\'${SubType}\',\'${Title}\','${$index+1}', false, true, 0, \'${OnlineVideoID}\',\'${Thumbnail}\',${IsYouTubeVideo},${IsVimeoVideo});\" >\
                         <img src=\"${Thumbnail}\" title=\"${Title} \" data-type=\"yt\" class=\"image0 gallery\" />  \
                         <span></span>\
                         </a>\
                        </li>\
                        {{/if}} \
                        {{if Type === 'v' && !IsYouTubeVideo && !IsVimeoVideo}} \
                        <li  itemprop=\"video\" itemscope itemtype=\"http://schema.org/VideoObject\">\
                        <meta itemprop=\"thumbnail\" content=\"${Thumbnail}\"/>\
                        <a href=\"${VideoStillURL}\"   tmpclick=\"_mediaPlayerRef.galleryloadImage(\'${Thumbnail}\',\'${SubType}\',\'${Title}\','${$index+1}', false, true, ${ID}, \'${Url}\',\'${VideoStillURL}\', false, false);\" >\
                         <img src=\"${Thumbnail}\" title=\"${Title} \" data-type=\"bc\" class=\"image0 gallery\" />  \
                         <span></span>\
                         </a>\
                       </li>\
                        {{/if}} \
                         {{if Type === 'i'}}\
                         <li itemscope itemtype=\"http://schema.org/ImageObject\">\
                         <a href=\"" + this.currentParams.imgBaseUrl + "${Url}\" tmpclick=\"_mediaPlayerRef.galleryloadImage(\'${Url}\',\'${SubType}\',\'${Title}\','${$index+1}', false, false);\"  >\
                         <img src=\"" + this.currentParams.imgBaseUrl + "${Thumbnail}\" title=\"${Title}\" data-type=\"img\" class=\"image0 gallery\" itemprop=\"contentUrl\"/>\
                         </a>\
                         </li>\
                         {{else Type === 'l'}}\
                         {{html Url}}\
                         {{/if}}");
            jQuery('#galleryList').empty();

            _globalCaption = this.globalCaption;

            videos = jQuery.merge(videos, this.youtubeLinks);
            videos = videos.sort(function (a, b) { return a.Sort - b.Sort; });

            jQuery.tmpl('mpTmpl', videos).appendTo("#galleryList");


            jQuery.tmpl('mpTmpl', this.imgNLinks).appendTo("#galleryList");

            this.afterProcess();
        }
    },

    popImgnLnksFromMediaItems: function (hasVideos) {

        this.imgNLinks = new Array();
        this.youtubeLinks = new Array();

        var hasImages = false;
        var hasYTVideos = false;

        if (this.mediaItems != null) {
            var mediaObjs = this.mediaItems;
            jQuery.each(mediaObjs, (function (index, asset) {
                if (asset.Type === 'i') {
                    this.imgNLinks.push(asset);
                    hasImages = true;
                } else if (asset.Type == 'v' && (asset.IsYouTubeVideo || asset.IsVimeoVideo)) {
                    this.youtubeLinks.push(asset);
                    hasYTVideos = true;
                }
            }).bind(this));
        }

        if (this.currentParams.externalLinks != null && this.currentParams.externalLinks.length > 0) {
            var exlinks = this.currentParams.externalLinks;
            jQuery.each(exlinks, (function (index, extLink) {
                this.imgNLinks.push(extLink);
                hasImages = true;
            }).bind(this));
        }


        if (!hasVideos) {
            hasImages = this.imgNLinks.length > 0;
            if (hasImages) {
                if (this.listingType == "com") {
                    caption = (this.imgNLinks[0].SubType == "ci" || this.imgNLinks[0].SubType == "lm") ? this.currentParams.CommunityImageText : this.currentParams.HomeImageText;
                } else {
                    caption = this.GallerylookUpTable[this.imgNLinks[0].SubType];
                }
                caption = this.currentParams.NowShowingText + ": " + caption;
                jQuery('#' + this.currentParams.captionBarID).text(caption);
            }

            if (!hasVideos && !hasImages && !hasYTVideos) {
                this.displayNoMedia();
            } else {
                jQuery.template("mpTmpl", "\
                        {{if IsYouTubeVideo || IsVimeoVideo }} \
                         <li itemprop=\"video\" itemscope itemtype=\"http://schema.org/VideoObject\">\
                        <meta itemprop=\"thumbnail\" content=\"${Thumbnail}\"/>\
                        <a href=\"${Thumbnail}\"   tmpclick=\"_mediaPlayerRef.galleryloadImage(\'${Thumbnail}\',\'${SubType}\',\'${Title}\','${$index+1}', false, true, 0, \'${OnlineVideoID}\',\'${Thumbnail}\',${IsYouTubeVideo},${IsVimeoVideo});\" >\
                         <img src=\"${Thumbnail}\" title=\"${Title} \" data-type=\"yt\" class=\"image0 gallery\" />  \
                         <span></span>\
                         </a>\
                        </li>\
                        {{/if}} \
                        {{if Type === 'v' && !IsYouTubeVideo && !IsVimeoVideo}} \
                        <li  itemprop=\"video\" itemscope itemtype=\"http://schema.org/VideoObject\">\
                        <meta itemprop=\"thumbnail\" content=\"${Thumbnail}\"/>\
                        <a href=\"${VideoStillURL}\"   tmpclick=\"_mediaPlayerRef.galleryloadImage(\'${Thumbnail}\',\'${SubType}\',\'${Title}\','${$index+1}', false, true, ${ID}, \'${Url}\',\'${VideoStillURL}\', false, false);\" >\
                         <img src=\"${Thumbnail}\" title=\"${Title} \" data-type=\"bc\" class=\"image0 gallery\" />  \
                         <span></span>\
                         </a>\
                       </li>\
                        {{/if}} \
                         {{if Type === 'i'}}\
                         <li itemscope itemtype=\"http://schema.org/ImageObject\">\
                         <a href=\"" + this.currentParams.imgBaseUrl + "${Url}\" tmpclick=\"_mediaPlayerRef.galleryloadImage(\'${Url}\',\'${SubType}\',\'${Title}\','${$index+1}', false, false);\"  >\
                         <img src=\"" + this.currentParams.imgBaseUrl + "${Thumbnail}\" title=\"${Title}\" data-type=\"img\" class=\"image0 gallery\" itemprop=\"contentUrl\"/>\
                         </a>\
                         </li>\
                         {{else Type === 'l'}}\
                         {{html Url}}\
                         {{/if}}");

                this.globalCaption = new Array();

                jQuery.each(this.youtubeLinks, (function (index, asset) {
                    this.globalCaption.push(this.getCaption(asset));
                }).bind(this));

                jQuery.each(this.imgNLinks, (function (index, asset) {
                    this.globalCaption.push(this.getCaption(asset));
                }).bind(this));

                // dependecy on jquery.ad-gallery.js
                _globalCaption = this.globalCaption;

                jQuery('#galleryList').empty();
                jQuery.tmpl('mpTmpl', this.youtubeLinks).appendTo("#galleryList");
                jQuery.tmpl('mpTmpl', this.imgNLinks).appendTo("#galleryList");
                this.afterProcess();
            }
        }
    }
};
