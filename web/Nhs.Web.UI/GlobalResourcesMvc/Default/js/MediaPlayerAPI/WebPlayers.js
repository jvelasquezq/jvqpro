﻿// Include the youtube iframe api in the document
loadExternalScript("https://www.youtube.com/iframe_api");

// Include the Brightcove api in the document
loadExternalScript("http://admin.brightcove.com/js/BrightcoveExperiences.js");


NHS.Scripts.WebPlayers = function (parameters) {
    this.parameters = parameters;
    this.autoplay = true;

    this.brightcovePlayer = null;
    this.youtubePlayer = null;
    this.vimeoPlayer = null;

    this.playingVideo = false;

    if (parameters.autoplay != undefined)
        this.autoplay = parameters.autoplay;

    this.brightCovePlayerTemplate = '<object id=\"nhs_brightCovePlayer\" class=\"BrightcoveExperience\"><param name=\"bgcolor\" value=\"#FFF /><param name=\"width\" value=\"{{width}}\" /><param name=\"height\" value=\"{{height}}\" /><param name=\"playerID\" value=\"{{playerID}}\" /><param name=\"playerKey\" value=\"{{playerKey}}\" /><param name=\"isVid\" value=\"true\" /><param name=\"isUI\" value=\"true\" /><param name=\"dynamicStreaming\" value=\"true\" /><param name="wmode" value="transparent"><param name=\"@videoPlayer\" value=\"{{videoID}}\"; /><param name=\"htmlFallback\" value=\"true\" />';
    if (this.autoplay) {
        this.brightCovePlayerTemplate = this.brightCovePlayerTemplate + '<param name=\"autoStart\" value=\"true\" />';
    }
    this.brightCovePlayerTemplate = this.brightCovePlayerTemplate + '</object>';

    this.vimeoPlayerTemplate = '<iframe  id=\"nhs_vimeoPlayer\" src=\"http://player.vimeo.com/video/{{videoID}}?title=0&amp;byline=0&amp;portrait=0';
    if (this.autoplay) {
        this.vimeoPlayerTemplate = this.vimeoPlayerTemplate + '&amp;autoplay=1';
    }
    this.vimeoPlayerTemplate = this.vimeoPlayerTemplate + '&amp;api=1\" width=\"{{width}}\" height=\"{{height}}\" frameborder=\"0\"></iframe>';
            
};

NHS.Scripts.WebPlayers.prototype =
{
    playYouTubeVideo: function(id) {
        this.playingVideo = true;

        this.youtubePlayer = new YT.Player(this.parameters.youtubeControlId, {
            height: this.parameters.videoHeight,
            width: this.parameters.videoWidth,
            videoId: id,
            playerVars: { autoplay: (this.autoplay? 1 : 0) }
        });
        jQuery("#" + this.parameters.youtubeControlId).append("<meta itemprop=\"thumbnail\" content=\"" + jQuery("div.ad-image > img").attr("src") + "\" />");
    },
    playVimeoVideo: function (id) {
        jQuery("#" + this.parameters.youtubeControlId).append("<meta itemprop=\"thumbnail\" content=\"" + jQuery("div.ad-image > img").attr("src") + "\" />");
        var data = { "videoID": id, "width": this.parameters.videoWidth, "height": this.parameters.videoHeight };
        var playerHtml = this.markup(this.vimeoPlayerTemplate, data);
        jQuery('#' + this.parameters.vimeoControlId).empty().html(playerHtml);
        jQuery("#" + this.parameters.vimeoControlId).append("<meta itemprop=\"thumbnail\" content=\"" + jQuery("div.ad-image > img").attr("src") + "\" />");
    },
    playBrightcoveVideo: function (id) {
        var data = { "playerID": '1667945989001', "videoID": id, "width": this.parameters.videoWidth, "height": this.parameters.videoHeight, "playerKey": this.parameters.brightcoveToken };
        var playerHtml = this.markup(this.brightCovePlayerTemplate, data);
        jQuery('#' + this.parameters.brightcoveControlId).html(playerHtml);
        jQuery("#" + this.parameters.brightcoveControlId).append("<meta itemprop=\"thumbnail\" content=\"" + jQuery("div.ad-image > img").attr("src") + "\" />");

        brightcove.createExperiences();
    },
    stopVideo: function(removeDiv) {
        jQuery("#" + this.parameters.youtubeControlId).css('display', 'none');
        jQuery("#" + this.parameters.brightcoveControlId).css('display', 'none');
        jQuery('#' + this.parameters.vimeoControlId).css('display', 'none');

        if (this.youtubePlayer != undefined && this.youtubePlayer != null && this.playingVideo) {
            this.youtubePlayer.stopVideo();
            this.youtubePlayer.destroy();

            this.playingVideo = false;
        }
        
        if (removeDiv) {
            jQuery('#' + this.parameters.youtubeControlId).remove();
            jQuery('#' + this.parameters.vimeoControlId).remove();
            jQuery('#' + this.parameters.brightcoveControlId).remove();
            jQuery(".nhs_LinkGallery").show();
            
        } else {
            jQuery('#' + this.parameters.youtubeControlId).html('');
            jQuery('#' + this.parameters.vimeoControlId).html('');
            jQuery('#' + this.parameters.brightcoveControlId).html('');
        }

    },
    markup: function (html, data) {
        var m;
        var i = 0;
        var match = html.match(data instanceof Array ? /{{\d+}}/g : /{{\w+}}/g) || [];

        while (m = match[i++]) {
            html = html.replace(m, data[m.substr(2, m.length - 4)]);
        }
        return html;
    }
};
