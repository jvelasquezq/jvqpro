﻿NHS.Scripts.Loggin = function (parameters) {
    this._parameters = parameters;
    this.type = (parameters.specId == 0 && parameters.planId == 0) ? 'comm' : 'home';
    this.name = (this.type == 'comm' ? 'Community' : 'Home');
};

NHS.Scripts.Loggin.prototype =
    {
        initialize: function () {
            var self = this;
            
           jQuery("#btnFreeBrochure").live("click", function () {
               jQuery.googlepush('Lead Events', this.name + ' - Gallery CTA Main Form', 'Submit Form - Free Brochure');
            });

           jQuery("#SendAlerts").live("change", function () {
               jQuery.googlepushCheckBox(this, 'Lead Events', self.name + ' - Gallery CTA Main Form', 'Choose Option - Free Info');
            });

           jQuery("#btn_NhsScheduleAppointment").click(function () {
               jQuery.googlepush('Lead Events', self.name + ' - CTA Next Steps Appointment', 'Open Form - Free Brochure');
            });

           jQuery("#btnUpdate").click(function () {
               jQuery.googlepush((self.type == 'comm' ? 'Community' : 'Home') + ' Detail Events', self.name + ' - Map', 'Show Route');
            });

           jQuery("#btn_NhsMapSend").click(function () {
               jQuery.googlepush('Lead Events', self.name + ' - CTA Request Appointment', 'Open Form - Free Brochure');
            });

           jQuery("#nhs_PhoneLink").click(function () {
               jQuery.googlepush(self.name + ' Detail Events', self.name + ' - Gallery', 'See Phone Numbers');

               self.ShowPhoneNumber('nhs_LnkPhoneNum', 'nhs_FormPhone');

                logger.logEventWithParameters((self.type == 'comm' ? 'BUILDPHCM' : 'BUILDPH'), self._parameters);

               return false;
            });

           jQuery('#nhs_FormIPhone a, .Iphone_number a').click(function () {
               //self.LogClick('LogIPhoneNumber');

               logger.logEventWithParameters("PhoneClick", self._parameters);

            });

           jQuery("#nhs_PhoneNumHeaderLink").click(function () {
                self.ShowPhoneNumber('nhs_LnkPhoneNumHeader', 'nhs_FormPhoneHeader');
                return false;
            });

           jQuery(".MortgageRates").click(function () {
               jQuery.googlepush('Community Detail Events', 'Community - Gallery', 'Get Mortgage Rates');
                self.LogClick('LogMortgageRates');
            });

           jQuery("#MortgageMatch").click(function () {
                //$jq.googlepush('Community Detail Events', 'Community - Gallery', 'Get Prequalified Today');
                self.LogClick('LogMortgageMatch');
            });

           jQuery(".nhs_FormMapLink  li a").click(function () {
               jQuery('#nhs_propertyMapCollapsibleOpen').click();
            });

           jQuery("#nhs_StepsPrintLink").click(function () {
               logger.logEventWithParameters((self.type == 'comm' ? 'CDNSPRN' : 'HDNSPRN'), self._parameters);
               window.print();
                self.LogClick('LogNextStepsPrint');
            });

           jQuery(".LogNextStepsBrochure").click(function () {
                self.LogClick('LogNextStepsBrochure');
            });

           jQuery("#nhs_LnkNextStepsPhoneNum2").click(function () {
               logger.logEventWithParameters((self.type == 'comm' ? 'CDNSSPH' : 'HDNSSPH'), self._parameters);

               var lnkPhoneNumber = jQuery('#nhs_LnkNextStepsPhoneNum');
                if (lnkPhoneNumber === null) return;
                lnkPhoneNumber.hide();
               jQuery.googlepush('Community Detail Events', 'Community - Next Steps', 'See Phone Numbers');
               jQuery('#nhs_NextStepsFormPhone').show();
                jQuery.SetDataLayerPair('sitePhoneNumV');
                //self.LogClick('LogNextStepsSeePhone');
            });

           jQuery('td.estaradefaultstyle a').live("click", function () {
                var img = $(this).find("img");
                if (img.length == 1) {
                    if (img.attr("src") == " http: //as00.estara.com/OneCC/200106300209/float_button.gif")
                       jQuery.googlepush('Chat Links', 'Tab', 'Chat Now');
                }
            });
        },
        ShowPhoneNumber: function (buttonSection, numberSection) {
            var lnkPhoneNumber =jQuery('#' + buttonSection);
            if (lnkPhoneNumber === null) return;
            lnkPhoneNumber.hide();
           jQuery('#' + numberSection).show();
            jQuery.SetDataLayerPair('sitePhoneNumV');
            //this.LogClick('LogPhoneNumber');
        },
        LogClick: function (method) {
            var self = this;
            var dataPost = {
                communityId: self._parameters.commId,
                builderId: self._parameters.builderId,
                planId: self._parameters.planId,
                specId: self._parameters.specId
            };
           jQuery.ajax({
                type: "POST",
                url: self._parameters.logActionMethodUrl + method,
                data: dataPost,
                dataType: "json"
            });
        }
    };
