﻿jQuery.extend({
    LazyLoad: function (options) {

        var defaultOptions = {
            Offset: 0,
            AppendScroll: window,
            ElementVisibleId: "",
            UpdateAction: function (isVisible) { }
        }

        var defaultOptiosTemp = {};
        jQuery.extend(true, defaultOptiosTemp, defaultOptions);
        var configuration = jQuery.extend(true, defaultOptiosTemp, options);


        var callbackAction = function () {
            configuration.UpdateAction(visibleY());
        };

        var visibleY = function () {
            el = document.getElementById(configuration.ElementVisibleId);
            var top = el.getBoundingClientRect().top, rect, el = el.parentNode;

            while (el != document.body) {
                rect = el.getBoundingClientRect();
                if (top <= rect.bottom === false)
                    return false;
                el = el.parentNode;
            };

            // Check its within the document viewport
            var isVisible = top - configuration.Offset <= document.documentElement.clientHeight;
            return isVisible;
        };

        jQuery(configuration.AppendScroll).on("scroll", callbackAction);
        jQuery(window).on("resize", callbackAction);

        callbackAction();
    }
});
