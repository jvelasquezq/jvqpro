﻿BDX = {}; BDX.Scripts = {};

// Class EventLogger
// Constructor, it receives a json object with parameters
BDX.Scripts.Tracker = function (parameters) {
    this._testMode = parameters.testMode == null ? false : parameters.testMode;
    this._partnerId = parameters.partnerId;
    this._bhiSite = parameters.bhiSite == null ? 'https://api.newhomesource.com/api/v2/log/partnerevent' : parameters.bhiSite + 'partnereventlogger';
}


BDX.Scripts.Tracker.prototype =
{   
    logEvent: function (eventCode, id) {
        if (eventCode) {
            var url = this._bhiSite + "/partnerId-" + this._partnerId + "/eventCode-" + eventCode + "/propertyId-" + id + "/testmode-false";

            this._logEvent(url);    
        }
    },

    _logEvent : function(url)
    {
        jQuery.ajax({
            type: "Post",
            url: url,
            data: null
        });

    }

}
