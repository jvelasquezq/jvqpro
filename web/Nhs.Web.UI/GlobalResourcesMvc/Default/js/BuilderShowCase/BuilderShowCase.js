﻿NHS.Scripts.BuilderShowCase = function (parameters) {
    this.parameters = parameters;
    if (parameters) {
        this._log = new NHS.Scripts.Globals.EventLogger({ siteRoot: parameters.siteRoot, partnerId: parameters.partnerID, marketId: parameters.marketID, builderId: parameters.brandId });
    }
};

NHS.Scripts.BuilderShowCase.prototype =
{
    get_log: function () { return this._log; },

    initMap: function () {
        var mapOptions = this.parameters.OptionsForMap;
        var parameters = this.parameters;
        var googleApi = new NHS.Scripts.GoogleMapApi(mapOptions);
        googleApi.options.Events.OnMapCreate = function () {
            var bounds = googleApi.getBoundsFromMap();
            var data = {
                partnerId: parameters.partnerID,
                brandId: parameters.brandId,
                minLat: bounds.minLat,
                minLng: bounds.minLng,
                maxLat: bounds.maxLat,
                maxLng: bounds.maxLng
            };
            jQuery.getJSON('/MapSearch/GetBrandMarketMapPoints', data, function (results) {
                googleApi.processResult(results);
            });
        };

        googleApi.processResult = function (results) {
            var self = this;
            self.showLoading();
            for (var i = 0; i < results.length; i++) {

                var lat = results[i].Lat;
                var lng = results[i].Lng;
                var name = results[i].Title.replace('<strong>', '').replace('</strong>', '');
                var urlCommResult = results[i].ActionUrl;

                var contentHtml = "<div class=\"nhs_CommResHoverMarket\">";

                var communityResultsUrl = parameters.siteRoot + urlCommResult;
                communityResultsUrl = communityResultsUrl.replace("//", "/");

                var marketName = name.substring(0, name.indexOf('('));
                var commsCount = name.substring(name.indexOf('('));
                contentHtml += "<h2><a href=\"" + communityResultsUrl + "\" >" + marketName + "</a></h2> <p>by " + parameters.brandName + "</p> <p>" + commsCount + "</p>";
                contentHtml += "</div>";
                self.createMarkerPoint(lat, lng, name, contentHtml, parameters.icon, results[i]);
            }
            self.hideLoading();
            self.AutoFit();
        };

        googleApi.createMap();
    },


    initialize: function () {
        var self = this;
        jQuery("#nhs_ShowcaseTabs li a").unbind("click");
        jQuery("#nhs_ShowcaseTabs li a").click(function (event) {
            jQuery.NhsCancelEventAndStopPropagation(event);

            var control = jQuery(this).parent();
            if (!control.is(".nhs_Selected")) {
                if (control.is("#nhs_TabLocation")) {
                    jQuery.googlepush("Builder Showcase Events", "Builder Showcase - Tab", "Locations");
                    self.loadLocations();
                } else if (control.is("#nhs_TabAbout")) {
                    jQuery.googlepush("Builder Showcase Events", "Builder Showcase - Tab", "About");
                    self.loadAbout();
                } else if (control.is("#nhs_TabGallery")) {
                    jQuery.googlepush("Builder Showcase Events", "Builder Showcase - Tab", "Gallery");
                    self.loadGallery();
                } else {
                    jQuery.googlepush("Builder Showcase Events", "Builder Showcase - Tab", "Owner Stories");
                    self.loadOwnerStories();
                }
            }
        });

        //Pinterest
        $jq("#pinit").click(function () {
            $jq("#pinmarklet").remove();
            var e = document.createElement('script');
            e.setAttribute('type', 'text/javascript');
            e.setAttribute('charset', 'UTF-8');
            e.setAttribute('id', 'pinmarklet');
            e.setAttribute('src', 'http://assets.pinterest.com/js/pinmarklet.js?r=' + Math.random() * 99999999);
            document.body.appendChild(e);
        });


        $jq(".nhs_BuilderLogo a").off("click").click(function () {
            logger.logEventWithParameters('BSCT', { "builderId": jQuery(this).attr('brandId') });
        });


        $jq("#btnSend").live("click", function () {
            $jq('#validationSummary').html('').hide();
            $jq('#formToSend').hide();
            $jq('#formSent').show();
            $jq(this).hide();
            $jq("#btnSendFake").show();

            if (self.validateFakeFields() && self.ValidateLeadModel()) {
                $jq.googlepush('Lead Events', 'Builder Showcase - Right', 'Submit Form - Ask a Question');
                self.askQuestion();
            }
        });


    },

    initializeGallery: function () {
        var self = this;
        jQuery.ajax({
            url: self.parameters.fullGalleryUrl,
            type: "post",
            cache: false,
            data: { 'brandId': self.parameters.brandId },
            success: function (data) {
                jQuery("#nhs_ShowcaseFullGallery").html(data);
                tb_init("a.thickbox");
            }
        });
    },

    initializeLocation: function () {
        var self = this;
        if (typeof google !== "undefined" && self.parameters && self.parameters.OptionsForMap)
            self.initMap();
        if (self._map) {
            self._map.show(true);
            self._map.refresh();
        }

        var option = {
            id: "Pagination",
            maxPage: 8,
            navigation: "nhs_ShowcasePagingBar"
        };

        var isHomesShowed = false;
        var defaultOptions = {
            ElementVisibleId: "nhs_ShowcaseLinks",
            UpdateAction: function (isVisible) {
                if (isVisible && isHomesShowed === false) {
                    isHomesShowed = true;
                    jQuery("#nhs_SpotlightCarousel").load(self.parameters.spotlightCarouselUrl,
                    {
                        brandId: self.parameters.brandId,
                        brandName: self.parameters.brandName
                    }, function () { self.setupCarousel(); });
                }
            }
        };
        jQuery.LazyLoad(defaultOptions);
        self.Pagination(option);
    },

    initializeAbout: function () {
        var self = this;
        self.loadAboutGallery();
        var isAwardShowed = false;
        var isNewsShowed = false;

        var defaultOptions = {
            ElementVisibleId: "nhs_ShowcaseLinks",
            UpdateAction: function (isVisible) {
                if (isVisible && isAwardShowed === false) {
                    isAwardShowed = true;
                    jQuery("#nhs_ShowAboutAwards").load(self.parameters.aboutAwardsUrl,
                    {
                        brandId: self.parameters.brandId
                    });

                    jQuery("#nhs_ShowAboutTestimonials").load(self.parameters.aboutTestimonialsUrl,
                    {
                        brandId: self.parameters.brandId
                    });
                }
            }
        };
        jQuery.LazyLoad(defaultOptions);

        var newsOptions = {
            ElementVisibleId: "nhs_ShowCaseOverview",
            UpdateAction: function (isVisible) {
                if (isVisible && isNewsShowed === false) {
                    isNewsShowed = true;
                    jQuery("#nhs_ShowAboutLatestNews").load(self.parameters.aboutLatestNewsUrl,
                    {
                        brandId: self.parameters.brandId
                    }, function () { self.checkLatestNewsItems(document); });
                }
            }
        };
        jQuery.LazyLoad(newsOptions);
    },

    loadAbout: function () {
        var self = this;
        self.parameters.IsAbout = true;

        jQuery.ajax({
            url: self.parameters.urlAbout + "?requestPartial=true",
            type: "GET",
            success: function (data) {
                if (typeof data == "string" && data.length > 0) {
                    jQuery("#nhs_BuilderShowcaseTab").html(data);
                    self.initialize();
                    self.initializeAbout();
                    jQuery("#nhs_TabAbout").addClass("nhs_Selected");
                    jQuery("#nhs_TabGallery").removeClass("nhs_Selected");
                    jQuery("#nhs_TabLocation").removeClass("nhs_Selected");
                    jQuery("#nhs_TabOwnerStories").removeClass("nhs_Selected");
                }
            }
        });
    },

    loadOwnerStories: function () {
        var self = this;
        self.parameters.IsOwnerStories = true;
        jQuery.ajax({
            url: self.parameters.urlOwnerStories + "?requestPartial=true",
            type: "GET",
            success: function (data) {
                if (typeof data == "string" && data.length > 0) {
                    jQuery("#nhs_BuilderShowcaseTab").html(data);
                    self.initialize();
                    tb_init("a.thickbox");
                    jQuery("#nhs_TabOwnerStories").addClass("nhs_Selected");
                    jQuery("#nhs_TabAbout").removeClass("nhs_Selected");
                    jQuery("#nhs_TabGallery").removeClass("nhs_Selected");
                    jQuery("#nhs_TabLocation").removeClass("nhs_Selected");
                }
            }
        });
    },

    loadGallery: function () {
        var self = this;
        self.parameters.IsAbout = false;

        jQuery.ajax({
            url: self.parameters.urlGallery + "?requestPartial=true",
            type: "GET",
            success: function (data) {
                if (typeof data == "string" && data.length > 0) {
                    jQuery("#nhs_BuilderShowcaseTab").html(data);
                    self.initialize();
                    self.initializeGallery();
                    jQuery("#nhs_TabAbout").removeClass("nhs_Selected");
                    jQuery("#nhs_TabLocation").removeClass("nhs_Selected");
                    jQuery("#nhs_TabOwnerStories").removeClass("nhs_Selected");
                    jQuery("#nhs_TabGallery").addClass("nhs_Selected");
                }
            }
        });

    },

    loadLocations: function () {
        var self = this;
        self.parameters.IsAbout = false;

        jQuery.ajax({
            url: self.parameters.urlLocation + "?requestPartial=true",
            type: "GET",
            success: function (data) {
                if (typeof data == "string" && data.length > 0) {
                    jQuery("#nhs_BuilderShowcaseTab").html(data);
                    self.initialize();
                    self.initializeLocation();

                    var option = {
                        id: "Pagination",
                        maxPage: 8,
                        navigation: "nhs_ShowcasePagingBar"
                    };

                    self.setupCarousel();
                    self.Pagination(option);
                    commResults = self;
                    jQuery("#nhs_TabAbout").removeClass("nhs_Selected");
                    jQuery("#nhs_TabGallery").removeClass("nhs_Selected");
                    jQuery("#nhs_TabOwnerStories").removeClass("nhs_Selected");
                    jQuery("#nhs_TabLocation").addClass("nhs_Selected");
                    self.parameters.currentPage = 1;
                }
            }
        });
    },

    validateFakeFields: function () {
        // Case 73789 - Builder Showcase Contacts are being spammed
        // Fake fields where added to the form ids = MailAddress2 & FirstName & LastName
        // If those fields are filled probably the submit was filled by a robot
        // Also check if at least 8 seconds has passed since the form was loaded
        var now = new Date();
        var time = Math.abs((now.getTime() - $jq('#time').val()) / 1000);
        return ($jq('#MailAddress2').val() === "" && $jq('#FirstName').val() === "" && $jq('#LastName').val() === "" && time > 8);
    },
    ValidateLeadModel: function () {
        var isValid = true;
        var self = this;
        var email = jQuery('#MailAddress');
        var name = jQuery('#Name');
        var validationSummary = jQuery('#validationSummary');
        var errors = "";
        
        if (email.val().trim() === "") {
            errors = "* " + self.parameters.EmailRequiredText + "<br />";
            isValid = false;
        }

        if (name.val().trim() === "") {
            errors = errors + "* " + self.parameters.NameIsRequiredText + "<br />";
            isValid = false;
        }

        if (email.val().trim() !== "" && !ValidEmail(email.val().trim())) {
            errors = errors + "* " + self.parameters.EmailAddressIsNotValidText + "<br />";
            isValid = false;
        }

        if (name.val().trim() !== "" && !ValidFullNameWithNumberLastName(name.val().trim())) {
            errors = errors + "* " + self.parameters.PleaseEnterFirstAndLastNameText + " <br />";
            isValid = false;
        } 

        if (!isValid) {
            validationSummary.html(errors);
            validationSummary.addClass("nhs_Error");
            $jq('#formToSend').show();
            $jq('#formSent').hide();
            $jq("#btnSend").show();
            $jq("#btnSendFake").hide();
            validationSummary.show();
        }
        return isValid;
    },
    askQuestion: function () {
        var self = this;
        var mail = jQuery('#MailAddress').val();
        var name = jQuery('#Name').val();
        var comments = jQuery('#Comments').val();
        var formData = { Comments: comments, MailAddress: mail, Name: name, BrandName: self.parameters.brandName, BrandEmailAddress: self.parameters.brandEmail, BrandId: self.parameters.brandId };

        jQuery.ajax({
            url: '/buildershowcase/askaquestion',
            type: 'POST',
            data: formData,
            success: function (data) {
                 if (data.HasErrors) {
                     $jq('#validationSummary').html('');
                     $jq('#formToSend').show();
                     $jq('#formSent').hide();
                     $jq('#btnSend').show();
                     $jq("#btnSendFake").hide();

                    if (data.Errors && data.Errors.length > 0) {
                        for (var i in data.Errors) {
                            var error = data.Errors[i];
                            $jq('#validationSummary').append('<li>' + error + '</li>');
                        }
                    }

                    $jq('#validationSummary').show();
                }
            }
        });
    },
    setupTwitter: function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = "//platform.twitter.com/widgets.js";
            fjs.parentNode.insertBefore(js, fjs);
        }
    },
    checkLatestNewsItems: function (d) {
        var latestNews = d.getElementById('nhs_ShowcaseLatestNews');
        if (latestNews.childNodes.length < 4) {
            latestNews.style.display = "none";
        }
    },
    initGallery: function (options) {
        $jq("#nhs_ShowcaseSlides").slides(options);

        $jq(".bigprev").hide();
        $jq(".bignext").hide();

        $jq("#nhs_ShowcaseSlides").on("mouseenter", function () {
            $jq(".bigprev").show();
            $jq(".bignext").show();
        });
        $jq("#nhs_ShowcaseSlides").on("mouseleave", function () {
            $jq(".bigprev").hide();
            $jq(".bignext").hide();
        });
    },
    InitVideoGallery: function (brandid, brightcoveEnvSuffix, url) {
        this.Initialize("NHS.Scripts.BuilderShowCase.PopVideos");
        tags = "brandvideo_" + brandid + "_" + brightcoveEnvSuffix;
        Url = url;
        //tags += ",showcasevideo";

        BCMAPI.find('find_videos_by_tags',
            {
                or_tags: tags,
                fields: "name,id,referenceId,thumbnailURL,tags,videoStillURL"
            });
    },
   
    LoadVideo: function (videoId) {
        VideoId = videoId;
    },
    Pagination: function (option) {
        this.currentPage = 1;
        var self = this;
        var div = $jq("#" + option.id).find(".nhs_ShowcaseMetroArea > ul > li");
        div.hide();
        var ul = $jq("." + option.navigation).find("ul");
        $jq(ul).find("li").remove();
        if (div.length > 0) {
            var n = div.length / option.maxPage;
            var pages = parseInt(n, 10);
            if (div.length - (pages * option.maxPage) > 0) {
                pages++;
            }
            if (pages > 1)
                ul.append('<li><a page="back"><</a></li>');
            for (var i = 0; i < pages; i++) {
                for (var j = i * option.maxPage; j < (i + 1) * option.maxPage; j++) {
                    if (j <= div.length) {
                        $jq(div[j]).attr("page", i + 1);
                    }
                }
                if ((i + 1) == 1)
                    ul.append('<li><a class="nhs_Active" page="' + (i + 1) + '">' + (i + 1) + '</a></li>');
                else
                    ul.append('<li><a page="' + (i + 1) + '">' + (i + 1) + '</a></li>');
            }

            $jq("li[page^='1']").show();
            if (pages > 1)
                ul.append('<li><a page="next">></a></li><li><a page="all">' + self.parameters.ViewAllText + '</a></li>');

            if (pages > 1) {
                var a = ul.find("a");
                for (var k = 0; k < a.length; k++) {
                    $jq(a[k]).click(function () {
                        $jq("a.nhs_Active").removeClass("nhs_Active");
                        var pageNumber = $jq(this).attr('page');
                        if (pageNumber == "back") {
                            if (self.currentPage > 1)
                                pageNumber = self.currentPage - 1;
                        } else if (pageNumber == "next") {
                            if (self.currentPage < pages) {
                                pageNumber = self.currentPage + 1;
                            }
                        }

                        if (pageNumber == "all") {
                            if ($jq(this).html() == self.parameters.ViewAllText) {
                                $jq("li[page]").fadeIn("normal");
                                a.hide();
                                $jq("a[page='all']").show().addClass("nhs_Active");
                                $jq("a[page='all']").html(self.parameters.CollapseText);
                            } else {
                                a.show();
                                $jq("a[page^='" + self.currentPage + "']").addClass("nhs_Active");
                                $jq("li[page]").hide();
                                $jq("li[page^='" + self.currentPage + "']").fadeIn("normal");
                                $jq("a[page='all']").html(self.parameters.ViewAllText);
                            }
                        } else if (pageNumber != self.currentPage && pageNumber != "back" && pageNumber != "next") {
                            $jq("a[page^='" + pageNumber + "']").addClass("nhs_Active");
                            $jq("li[page]").hide();
                            $jq("li[page^='" + pageNumber + "']").fadeIn("normal");
                            self.currentPage = parseInt(pageNumber, 10);
                        }
                    });
                }

            }
        }
    },
    setupCarousel: function () {
        $jq("#nhs_ShowcaseSpotlightCarousel").jCarouselLite({
            btnNext: ".bignext",
            btnPrev: ".bigprev",
            visible: 4,
            circular: true
        });
    },

    loadAboutGallery: function () {
        var self = this;
        jQuery.ajax({
            url: self.parameters.aboutGalleryUrl,
            type: "post",
            cache: false,
            data: { 'brandId': self.parameters.brandId, 'brandName': self.parameters.brandName },
            success: function (data) {
                jQuery("#nhs_ShowAboutGallery").html(data);
                jQuery("#nhs_ShowcaseGalleryLink").unbind("click");
                jQuery("#nhs_ShowcaseGalleryLink").click(function (event) {
                    self.loadGallery();
                });
                self.initGallery({
                    play: 5000,
                    pause: 5000,
                    hoverPause: true,
                    bigNextPrev: true
                });
            }
        });
    }
};
var tags = "";
var VideoId;
var Url = "";
var bcVP;
var bcExp;
var modExp;
var modCon;

function myTemplateLoaded(pExperienceID) {
    bcExp = brightcove.api.getExperience(pExperienceID);
    bcVP = bcExp.getModule(brightcove.api.modules.APIModules.VIDEO_PLAYER);
    modExp = bcExp.getModule(brightcove.api.modules.APIModules.EXPERIENCE);
    modCon = bcExp.getModule(brightcove.api.modules.APIModules.CONTENT);
}

function onTemplateReady(evt) {
    bcVP.loadVideoByID(VideoId);
    bcVP.play();
}

NHS.Scripts.BuilderShowCase.PopVideos = function (response) {
    var videos = new Array();
    tags += "_";
    if (response.items.length != 0) {
        $jq("#nhs_ShowcaseVideos").show();

        if (response.items.length == 1) {
            if ($jq("#nhs_ShowcaseVideos").find("a").length > 1)
                $jq("#nhs_ShowcaseVideos .nhs_Title").html('Videos');
            else
                $jq("#nhs_ShowcaseVideos .nhs_Title").html('Video');
        }
        else
            $jq("#nhs_ShowcaseVideos .nhs_Title").html('Videos');

        videos = response.items.sort(function (a, b) {
            var _a = parseInt(a.referenceId.substring(tags.length));
            var _b = parseInt(b.referenceId.substring(tags.length));
            return _a - _b;
        });

        var mytemp = $jq.template("mpTmpl", "<a class=\"thickbox\" href=\"" + Url + "?videoref=${id}&KeepThis=true&TB_iframe=true&width=700&height=500&videowidth=670&videoheight=480&videourl=frombrightcove&title=${name}\"><span></span><img src=\"${thumbnailURL}\" alt=\"\"  /><p>${name}</p></a>");

        //$jq('#nhs_ShowcaseVideosGallery').empty();
        $jq.tmpl('mpTmpl', videos).appendTo("#nhs_ShowcaseVideosGallery");
        tb_init('#nhs_ShowcaseVideosGallery a.thickbox');
    }
};
