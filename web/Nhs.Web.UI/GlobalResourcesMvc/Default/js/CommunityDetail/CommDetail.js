﻿// Global Class for Community Detail Page
NHS.Scripts.CommunityDetail.CommDetail = function (parameters) {
    this._parameters = parameters;
    this.displayFullWidthGallery = parameters.displayFullWidthGallery;
    this.displayNewFullImageViewer = parameters.displayNewFullImageViewer;
    this.NewFullImageViewerUrl = parameters.NewFullImageViewerUrl;
    this.ShowNewFullImageViewerUrl = parameters.ShowNewFullImageViewerUrl;
    this.NextPageUrl = parameters.NextPageUrl;
    this.method = parameters.method;
    this._searchParameters = parameters.searchParameters;
    this._searchAction = parameters.searchAction;
    this._communityId = parameters.communityId;
    this._builderId = parameters.builderId;
    this._totalHomes = parameters.totalHomes;
    this._tabsControlId = parameters.tabsControlId;
    this._loadingControlId = parameters.loadingControlId;
    this._partnerId = parameters.partnerId;
    this._isBilled = parameters.isBilled;
    this._showSocialIcons = parameters.showSocialIcons;
    this._log = new NHS.Scripts.Globals.EventLogger({ siteRoot: parameters.siteRoot, fromPage: parameters.fromPage, partnerID: parameters.partnerId, partnerName: parameters.partnerName, tdvTable : parameters.tdvTable});

    this.selectedTab = parameters.SelectedTab;
    this.allNewHomesCount = parameters.AllNewHomesCount;
    this.quickMoveInCount = parameters.QuickMoveInCount;
    this.filteredHomesCount = parameters.FilteredHomesCount;

    parameters.mapParametes.logInfo = parameters.logInfo;
    parameters.mapParametes.logger = this._log;
    this.googlePropertyMap = new NHS.Scripts.GooglePropertyMap(parameters.mapParametes);

    this._nearbyHomes = new NHS.Scripts.PropertyMap.NearbyComms(parameters.nearByParams);

    this._paging = new NHS.Scripts.Paging({ onGoToPage: this._goToPage.bind(this) });

    this._sort = new NHS.Scripts.CommunityResults.Sorting({ onSort: this._sortResults.bind(this) });
};

NHS.Scripts.CommunityDetail.CommDetail.prototype =
{
    // Public properties
    get_searchParameters: function () { return this._searchParameters; },
    set_searchParameters: function (params) { this._searchParameters = params; },
    get_log: function () { return this._log; },
    get_commId: function () { return this._communityId; },
    get_builderId: function () { return this._builderId; },
    get_totalHomes: function () { return this._totalHomes; },
    get_mp: function () { return this._mp; },

    initialize: function () {
        this._setUpControls();
        this._setupTabs();
        this._paging.initialize();
        this._sort2();

        var self = this;
        if (self._parameters.PreviewMode == "False") {
            var tmp = ""; //resourcecombiner weirdness if we take this out
            this.setUpToolbar();
        }
        
        this.googlePropertyMap.init();
        this.loadGalleryAsync();
        this._updateAdsPosition();
    },

    setUpToolbar: function () {
        if (jQuery("#nhs_Crumbs").length === 0)
            return false;
        var toolBottom = jQuery("#nhs_Crumbs").offset().top - 20;

        jQuery(window).scroll(function (event) {
            if (jQuery("#nhs_ShowToolbar").val() === "true") {
                var y = jQuery(document).scrollTop();

                if (y < toolBottom) {
                    jQuery(".nhs_DetailsToolBar").removeClass("fixed");
                } else {
                    jQuery(".nhs_DetailsToolBar").addClass("fixed");
                }
            }
        });

        jQuery(".nhs_DetailsToolBar ul li a").not(".nhs_MediaPlayerMaximize").click(function (event) {
            jQuery.NhsCancelEventAndStopPropagation(event);
            var control = jQuery(this);
            var id = control.attr("href");
            if (id === "#HomesAndPlans") {
                jQuery.googlepush('Site Links', 'Anchor Links', 'See homes');
            }
            else if (id === "#SchoolsAndAmenities") {
                jQuery.googlepush('Site Links', 'Anchor Links', 'Schools & amenities');
            }
            else if (id === "#Maps") {
                jQuery.googlepush('Site Links', 'Anchor Links', 'Map & directions');
                jQuery('#nhs_propertyMapCollapsibleOpen').click();
            }
            else if (id === "#Contact") {
                jQuery.googlepush('Site Links', 'Anchor Links', 'Contact');
            }
            
            jQuery(id).ScrollToPosLess(5);
        });
    },

    initializeEvents: function () {
        this._setUpControls();
        this._paging.initialize();
    },

    // Hide Progress Indicator
    hideProgressIndicator: function () {
        jQuery('#' + this._loadingControlId).hide();
    },

    SetupNearbyCommunities: function (parameters) {
        if (this.googlePropertyMap.isMapCreate) {
            this.googlePropertyMap.googleApi.processResult(parameters);
            this.googlePropertyMap.googleApi.AutoFit();
        } else {
            this.googlePropertyMap.parameters.NearbyComms = parameters;
        }

    },
    // Initialize comm results view controls
    _setUpControls: function () {
        var self = this;
        if (self._parameters.isBilled == "True" && self._parameters.PreviewMode == "False") {
            var isNearbyShowed = false;

            var defaultOptios = {
                ElementVisibleId: 'SchoolsAndAmenities',
                UpdateAction: function (isVisible) {
                    if (isVisible && isNearbyShowed == false) {
                        isNearbyShowed = true;
                        jQuery("#nhs_NearbyHomesContent").load(self._parameters.GetNearbyCommunities,
                            {
                                communityId: self._parameters.communityId,
                                builderId: self._parameters.builderId,
                                brandName: self._parameters.BrandName
                            },
                            function () {
                                self._nearbyHomes.attachClickEventsToCommLinks();
                            });
                    }
                }
            };
            jQuery.LazyLoad(defaultOptios);
        }



        this.isMediaPlayerOpened = false;
        if (self._showSocialIcons.toLowerCase() == 'false') {
            jQuery('#addthis_bookmark').remove();
            jQuery('.addthis_separator').remove();
            jQuery('.addthis_button_facebook').remove();
            jQuery('.addthis_button_twitter').remove();
            jQuery('.nhs_DetailSmBrandImg').remove();
            jQuery('.builder_link').remove();

            jQuery('#addthis_more_sharing').remove();
            jQuery('#addthis_bookmark_bottom').remove();
            jQuery('.nhs_MediaShareOverlay, .nhs_PinterestOverlayBig').hide();
            jQuery('#brochure_pro_step2').remove();
        }

        jQuery('#nhs_Mediatitle').text('Community Gallery');

        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        jQuery('#nhs_NextStepsBackSearch').attr('href', jQuery('.nhs_BreadcrumbBackSearch').attr('href'));

        jQuery('a[href=#top]').live("click", function () {
            jQuery('html, body').animate({ scrollTop: 0 }, 'slow');
            return false;
        });
        jQuery(window).bind("mousewheel", function () {
            jQuery("html, body").stop();
        });
        jQuery(window).bind("keydown", function (event) {
            var keyCode = event.which;
            /* up, down, left, right, or page up or page down */
            if (keyCode === 33 || keyCode == 34 || (keyCode >= 37 && keyCode <= 40)) {
                jQuery("html, body").stop();
            }
        });

        jQuery('.addthis_button_print').click(function () { setTimeout(function () { commDetail.get_log().logEvent('CDSMS', commDetail.get_commId(), commDetail.get_builderId()); }, 1); });

        // Home gallery tabs
        jQuery('#' + this._tabsControlId + ' li a').live("click", function () {
            self.filterTab = false;
            jQuery(this).parent().parent().find("li").removeClass('nhs_Selected');
            jQuery(this).parent().addClass('nhs_Selected');
            if (jQuery(this).parent().attr('id') == 'allCommunitiesTab') {
                self.selectedTab = "FilterCommunities";
                self._searchParameters.SpecialOfferComm = 0;
                self._searchParameters.HomeStatus = 0;
            } else if (jQuery(this).parent().attr('id') == 'quickMoveInTab') {
                self.selectedTab = "QuickMoveIn";
                self._searchParameters.SpecialOfferComm = 0;
                self._searchParameters.HomeStatus = 5;
            } else if (jQuery(this).parent().attr('id') == 'newHomesTab') {
                self.selectedTab = "AllCommunities";
                self._searchParameters.SpecialOfferComm = 0;
                self._searchParameters.HomeStatus = -1;
            }
            self.update();
            return false;
        });

        jQuery('#divLeadMain input#Name, #divLeadMain input#MailAddress').focus(function () {
            jQuery('.nhs_LeadCollapse').removeClass('nhs_LeadCollapse').addClass('nhs_LeadExpand');
        });

        jQuery('span#nhs_LeadCollapseX').click(function () {
            jQuery('.nhs_LeadExpand, #nhs_LeadCollapseX').removeClass('nhs_LeadExpand').addClass('nhs_LeadCollapse');
            jQuery('.validation-summary-errors').hide();
        });

        jQuery("#nhs_detailDescriptionToggle").live("click", function () {
           if (jQuery('#nhs_detailDescriptionToggle').text().trim() === self._parameters.MoreWord) {
                jQuery('#nhs_detailDescriptionToggle').text(self._parameters.LessWord);
                jQuery("#nhsDetailDescriptionArea").html("");
                jQuery("#nhsDetailDescriptionArea").html(Encoder.htmlDecode(description));
                return false;
            } else {
                jQuery('#nhs_detailDescriptionToggle').text(self._parameters.MoreWord);
                jQuery("#nhsDetailDescriptionArea").html("");
                jQuery("#nhsDetailDescriptionArea").html(Encoder.htmlDecode(chunkedDescription));
                return false;
            }
        });

        jQuery("#nhs_HomesGalleryLink").live("click", function () {
            var url = window.location.pathname;
            if (jQuery(".nhsFlag").hasClass("nhsHideRow")) {
                jQuery(".nhsFlag").removeClass("nhsHideRow");
                jQuery(".nhsFlag").addClass("nhsShowRow");
                if (url.indexOf("communitydetailv1") != -1)
                    jQuery("#nhs_HomesGalleryLink").text(self._parameters.ShowOnlyNineTopMatchingHomesText);
                else
                    jQuery("#nhs_HomesGalleryLink").text(self._parameters.ShowOnlyTwelveTopMatchingHomesText);

                self.lazyLoad();
            }
            else if (jQuery(".nhsFlag").hasClass("nhsShowRow")) {
                jQuery(".nhsFlag").removeClass("nhsShowRow");
                jQuery(".nhsFlag").addClass("nhsHideRow");
                if (url.indexOf("communitydetailv1") != -1)
                    jQuery("#nhs_HomesGalleryLink").text(self._parameters.NineMatchingHomesShownText);
                else
                    jQuery("#nhs_HomesGalleryLink").text(self._parameters.TwelveMatchingHomesShownText);
            }
            metricLogger.logHomeResultsMetrics('CDHV', 0);
            return false;
        });

        self.showAll = false;
        self.hideAll = false;

        jQuery("#nhs_ShowAll").live("click", function (e) {
            self.showAll = true;
            self.hideAll = false;
            jQuery.NhsCancelEventAndStopPropagation(e);
            metricLogger.logHomeResultsMetrics('CDHV', 0);
            self._retrieveResults(2, null);
        });

        jQuery("#nhs_HideAll").live("click",function (e) {
            self.showAll = false;
            self.hideAll = true;
            jQuery.NhsCancelEventAndStopPropagation(e);
            metricLogger.logHomeResultsMetrics('CDHV', 0);
            self._retrieveResults(2, null, function () {
                jQuery("#HomesAndPlans").ScrollTo();
            });
        });

        jQuery('a[href=#top]').click(function () {
            jQuery('html, body').animate({ scrollTop: 0 }, 'slow');
            return false;
        });

        this._setupCTAEventNames();

        jQuery('#nhs_propertyMapCollapsibleClose').click(function () {
            jQuery("#Maps").ScrollToPosLess(50);
        });
    },
    _setupCTAEventNames: function () {
        jQuery('.nhs_DetailsInfoSchoolAmenities .btn_FreeBrochure').click(function () {
            NHS.Scripts.Helper.createCookie('CTAName', 'Community - CTA Community Info');
        });

        jQuery('.nhs_RequestAppointment .btn_NhsMapSend').click(function () {
            NHS.Scripts.Helper.createCookie('CTAName', 'Community - CTA Request Appointment');
            jQuery.googlepush('Lead Events', NHS.Scripts.Helper.readCookie('CTAName'), 'Open Form - Free Brochure');
        });

        jQuery('.nhs_DetailsFormMoreActions .btn_RequestAppointment').click(function () {
            NHS.Scripts.Helper.createCookie('CTAName', 'Community - Gallery CTA Main Form - Request Appointment');
            jQuery.googlepush('Lead Events', NHS.Scripts.Helper.readCookie('CTAName'), 'Open Form - Free Brochure');
        });

        jQuery('.nhs_NextStepsContent .brochure_link a').click(function () {
            NHS.Scripts.Helper.createCookie('CTAName', 'Community - CTA Next Steps Free Brochure');
        });

        jQuery('.nhs_NextStepsContent .special_offers a').click(function () {
            NHS.Scripts.Helper.createCookie('CTAName', 'Community - CTA Next Steps Special Offers');
        });

        jQuery('.nhs_NextStepsContent .schedule a').click(function () {
            NHS.Scripts.Helper.createCookie('CTAName', 'Community - CTA Next Steps Appointment');
            jQuery.googlepush('Lead Events', NHS.Scripts.Helper.readCookie('CTAName'), 'Open Form - Free Brochure');
        });
    },
    _setupTabs: function () {
        jQuery('div.tabdiv').hide();
        jQuery('div.tabdiv:first').show();
        jQuery('#nhs_CommDetailsMainTabs li:first').addClass('nhs_Selected');

        jQuery('#nhs_CommDetailsMainTabs li a').click(function () {
            jQuery('#nhs_CommDetailsMainTabs li').removeClass('nhs_Selected');
            jQuery(this).parent().addClass('nhs_Selected');
            var currentTab = jQuery(this).attr('href');
            jQuery('div.tabdiv').hide();
            jQuery(currentTab).show();
            if (currentTab == '#nhs_CommDetailsTabInfoDiv') {
                FB.XFBML.parse();
            }
            return false;
        });
    },
    _sort2: function () {
        var self = this;

        jQuery('#SortOptionSelected').live("change", function (event) {
            self._sortResults(event.currentTarget.value);
        });

    },
    // Paging
    _goToPage: function (pageNumber) {
        this._retrieveResults(pageNumber, null);
    },

    update: function () {
        this._retrieveResults(1, null);
        this.lazyLoad();
    },

    lazyLoad: function () {
        var parameters = this._parameters.lazyLoadParameters;
        var lazyLoad = new NHS.Scripts.LazyLoad(parameters);
        lazyLoad.LazyLoadImages();
    },

    _sortResults: function (sortColumn) {
        switch (sortColumn) {
            case "homematches":
                this._searchParameters.SortOrder = 3;
                break;
            case "Price":
                this._searchParameters.SortOrder = 2;
                break;
            case "Size":
                this._searchParameters.SortOrder = 6;
                break;
            case "Status":
                this._searchParameters.SortOrder = 5;
                break;
            case "Name":
                this._searchParameters.SortOrder = 7;
                break;
        }

        this._retrieveResults(1, sortColumn);
    },

    _updateAdsPosition: function () {
        if ((jQuery('#nhs_CommunityDetails').length > 0) & (jQuery('#nhs_AdColumn').length > 0)) {
            //floating column
            var colHeight = jQuery('#nhs_CommunityDetails').height();
            this._adsHeight = jQuery('#nhs_AdColumn').height();

            var colBottom = colHeight + jQuery('#nhs_AdColumn').offset().top;

            var adsTop = colHeight - this._adsHeight - 10;
            var adsBottom = this._adsHeight + jQuery('#nhs_AdColumn').offset().top;
            var browserHeight = jQuery(window).height();
            var browserBottomY;

            jQuery(window).scroll((function (event) {

                // what the y position of the scroll is
                var y = jQuery(document).scrollTop();
                colHeight = jQuery('#nhs_CommunityDetails').height();

                colBottom = colHeight + jQuery('#nhs_AdColumn').offset().top;

                adsTop = colHeight - this._adsHeight - 10;
                adsBottom = this._adsHeight + jQuery('#nhs_AdColumn').offset().top;
                browserHeight = jQuery(window).height();
                browserBottomY = y + browserHeight;
                //console.log(colHeight);
                //console.log(this._adsHeight);
                //console.log(adsTop);

                // is list column longer than ad column
                if (colBottom > adsBottom) {
                    // whether scroll is below the ad's top position
                    if (y >= (adsBottom - browserHeight)) {
                        // if so, ad the fixed class          
                        if (browserBottomY > colBottom) {
                            jQuery('#nhs_AdColumn > div').removeClass('fixedBottom');
                            jQuery('#nhs_AdColumn > div').addClass('absolute');
                            jQuery('#nhs_AdColumn > div').css('top', (adsTop + 'px'));
                            jQuery('#nhs_AdColumn > div').css('bottom', 'auto');
                        } else {
                            jQuery('#nhs_AdColumn > div').removeClass('absolute');
                            jQuery('#nhs_AdColumn > div').addClass('fixedBottom');
                            jQuery('#nhs_AdColumn > div').css('top', 'auto');
                            jQuery('#nhs_AdColumn > div').css('bottom', 0);
                        }
                    } else {
                        // otherwise remove it
                        jQuery('#nhs_AdColumn > div').removeClass('fixedBottom');
                        jQuery('#nhs_AdColumn > div').removeClass('absolute');
                        jQuery('#nhs_AdColumn > div').css('top', 'auto');
                        jQuery('#nhs_AdColumn > div').css('bottom', 'auto');
                    }
                }

            }).bind(this));
        }
    },

    _retrieveResults: function (page, sort, action) {
        jQuery.ShowLoading();
        var self = this;
        var url = this._searchAction + "?" + this.ToString(this._searchParameters) + "&page=" + page + "&sortColumn=" + sort + "&selectedTab=" + this.selectedTab +
            "&allNewHomesCount=" + this.allNewHomesCount +
            "&quickMoveInCount=" + this.quickMoveInCount +
            "&filteredHomesCount=" + this.filteredHomesCount +
            "&showAll=" + this.showAll + "&hideAll=" + this.hideAll;
        jQuery.ajax({
            url: url,
            type: "POST",
            success: (function (data) {
                jQuery('#ViewHomes').html(data);
                self._paging.initialize();
                jQuery.HideLoading();
                self.lazyLoad();
                if (jQuery.isFunction(action)) {
                    action();
                }
            }).bind(this),
            error: function (obj, status, error) {
                NHS.Scripts.Helper.logError(error, this._searchAction, "error on search");
                jQuery.HideLoading();
            }
        });
    },
    ToString: function (obj) {
        var json = [];
        for (n in obj) {
            var v = obj[n];
            var t = typeof (v);
            if (t !== "object" && v !== null)
                if (v !== false && v !== 0)
                    json.push(n + '=' + v);
        }
        return json.join("&");
    },
    SaveCommunity: function () {
        var self = this;

        logger.logEventWithParameters('CDSTC', self._parameters.logInfo);

        var community = { communityId: self._communityId, builderId: self._builderId };
        jQuery.post(self.method, community, (function (json) {
            self.ShowSuccessMessage(json);
            if (json.data) {
                jQuery('#nhs_NextStepsSaveThisItem').removeAttr('onclick');
                jQuery('#nhs_NextStepsSaveThisItem').removeAttr('href');
                jQuery('#pSaveThisItem').html('<span class="nhs_ItemSaved" id="nhs_NextStepsSaveThisItem">' + self._parameters.SavedToYourProfileText + '</span>');
                jQuery('#liSaveToPlanner').html('<span id=\"nhs_SaveThisItem\">' + self._parameters.SavedText + '</span>');
                //                this.favText = jQuery('#nhs_SaveThisItem').text();
                //                if (this.isMediaPlayerOpened)
                //                    jQuery('#nhs_SaveThisItem').text('');
            }
        }).bind(this), "json");
    },
    NextStepsSaveCommunity: function () {
        this.SaveCommunity();
    },
    ShowSuccessMessage: function (json) {
        if (!json.data) {
            window.location.href = json.redirectUrl;
        }
        else {
            jQuery("#nhs_SaveThisItem").hide();
            jQuery("#SuccessMessage").show();
        }
    },
    loadGalleryAsync: function () {
        var self = this;
        jQuery.ajax({
            url: self._parameters.galleryParams.urlGallery,
            type: "post",
            cache: false,
            data: { 'communityId': self._parameters.communityId, 'planId': 0, 'specId': 0, 'isPreview': self._parameters.galleryParams.isPreview },
            success: function (data) {
                jQuery(data.ExternalMediaLinks).each(function () {
                    this.Url = this.Url.replace('href="/detailgetgallery"', 'href="' + window.location + '"').
                                        replace('src="http://images.newhomesource.com/images/1x1.gif"', '').
                                        replace('data-src', 'src').replace('class="async"', '');
                });
                self._parameters.galleryParams.externalLinks = data.ExternalMediaLinks;
                self._parameters.galleryParams.firstImgUrl = data.FirstImage;
                self._parameters.galleryParams.description = data.PinterestDescription;
                self._parameters.galleryParams.mediaObjs = data.PropertyMediaLinks;
                self._parameters.galleryParams.logger = self._log;
                self._parameters.galleryParams.logInfo = self._parameters.logInfo;

                // Home Gallery
                self._mp = new NHS.Scripts.GalleryViewer(self._parameters.galleryParams);
                self._mp.initGallery();
                // Popo up Gallery (maximize button)
                self.setupGallery();
            }
        });
    },
    setupGallery: function () {
        var self = this;
        if (this.displayNewFullImageViewer && (this._parameters.galleryParams.mediaObjs.length > 0 || this._parameters.galleryParams.externalLinks.length > 0)) {
            var callmodal = true;
            jQuery(".nhs_MediaPlayerMaximize").click(function (event) {
                jQuery.googlepush('Image Viewer', 'Community - Gallery', 'Image Viewer - Expand');

                if (callmodal) {
                    callmodal = false;
                    jQuery.ajax({
                        url: self.NewFullImageViewerUrl,
                        type: "Get",
                        success: function (html) {
                            jQuery("body").prepend(html);
                            callmodal = true;
                        }
                    });
                }
            });

            if (self.ShowNewFullImageViewerUrl)
                jQuery(".nhs_MediaPlayerMaximize").click();
        }
        //else {
        //    jQuery('.nhs_MediaShareOverlay').css('right', '5px');
        //    jQuery('.nhs_MediaPlayerMaximize').hide();
        //}

        if (self._showSocialIcons.toLowerCase()) {
            jQuery(".nhs_MediaShareOverlay").html("<div id=\"button_pin_it\"><a target=\"_blank\" href=\"http://pinterest.com/pin/create/button/?url=" + encodeURIComponent(window.location) + "&media="
                + encodeURIComponent(this._parameters.galleryParams.firstImgUrl) + "&description=" + encodeURIComponent(this._parameters.galleryParams.description)
                + "onclick=\"\" class=\"pin-it-button\" count-layout=\"horizontal\"><img border=\"0\" src=\"//assets.pinterest.com/images/PinExt.png\" title=\"Pin It\"></a></div>"
                + "<div id=\"button_compact\"><a class=\"addthis_button_compact at300m\" href=\"#\"><span class=\"at4-icon aticon-compact\" style=\"background-color: rgb(252, 109, 76);\">"
                + "<span class=\"at_a11y\"></span></span></a></div>");
        }
    }
};
var paging;
