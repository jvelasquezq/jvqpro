﻿// Global Class for BoylResults Page
NHS.Scripts.BoylResults = function (parameters) {
    this._parameters = parameters;
    this._log = new NHS.Scripts.Globals.EventLogger({ siteRoot: parameters.siteRoot, partnerId: parameters.partnerId, marketId: parameters.marketId });
};

NHS.Scripts.BoylResults.prototype =
{
    get_log: function () { return this._log; },

    initialize: function () {
        this.checkImgs();

        if (this._parameters.currentBrandName != '') {
            setTimeout((function() {
                jQuery('html,body').animate({
                    scrollTop: jQuery('div[data-title="' + this._parameters.currentBrandName + '"]').offset().top
                }, 1000);
            }).bind(this), 1000);
        }
    },

    checkImgs: function () {
        jQuery("div").each(function () {
            var cn = jQuery(this).attr("class");
            if (cn == "nhsBoylBuilderLogo") {
                var s = jQuery(this).html();
                if (s == "") {
                    var pDiv = jQuery(this).parent();
                    pDiv.css("margin-left", "100px");
                }
            }

        });
    }
};  