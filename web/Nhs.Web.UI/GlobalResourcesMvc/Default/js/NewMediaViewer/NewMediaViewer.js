NHS.Scripts.NewMediaViewer = function(parameters) {
    this.parameters = parameters;
    this.logger = parameters.looger || logger;

    this.player = new NHS.Scripts.WebPlayers({ 
        videoWidth: (parameters.videoWidth || 1600),
        videoHeight: (parameters.videoHeight || 800),
        youtubeControlId: 'nhs_VideP',
        vimeoControlId: 'nhs_VideP',
        brightcoveControlId: 'nhs_VideP',
        brightcoveToken: parameters.brightcoveToken,
        autoplay: false
    });

};

NHS.Scripts.NewMediaViewer.prototype =
{
    initialize: function () {
        var self = this;
        jQuery("#nhs_BigMediaBox").toggle("clip"); //clip  puff 

        jQuery("#nhs_BigMediaBox .nhs_Close").click(function () {
            jQuery.googlepush('Image Viewer', 'Image Viewer', 'Image Viewer - Close');
            jQuery("#nhs_BigMediaBox").toggle("clip", function () {
                jQuery("#nhs_BigMediaBox").remove();
            });
        });

        // configuration of media icons 
        var slider = jQuery('.nhs_BigMediaThumbsBox .bxslider');
        if (slider.find("[data-type^='i-']").length === 0)
            jQuery('.nhs_BigMediaIconsImage').hide();
        if (slider.find("[data-type^='l-flp']").length === 0)
            jQuery('.nhs_BigMediaIconsFloorPlan').hide();
        if (slider.find("[data-type^='l-vt']").length === 0)
            jQuery('.nhs_BigMediaIconsTour').hide();
        if (slider.find("[data-type^='l-ev']").length === 0 && slider.find("[data-type^='v-']").length === 0)
            jQuery('.nhs_BigMediaIconsVideo').hide();

        var firstType = jQuery(slider.find("li img")[0]).data("type");
        self.selectType(firstType);

        jQuery('.nhs_BigMediaIconsBar a').click(function (e) {

            jQuery('.nhs_BigMediaIconsBar a').removeClass('active');
            var control = jQuery(this);
            control.addClass('active');
            var type = control.data('type');
            var firstOftheKind = jQuery(slider.find("[data-type^='" + type.split(',')[0] + "']")[0]);

            if (firstOftheKind.length === 0) {
                firstOftheKind = jQuery(slider.find("[data-type^='" + type.split(',')[1] + "']")[0]);
            }

            if (firstOftheKind != null && firstOftheKind.index() !== -1) {
                var pagerIndex = parseInt(firstOftheKind.data("slide"));
                var showing = self.bxslider.getNumberSlidesShowing();
                var count = self.bxslider.getSlideCount();
                if (pagerIndex <= count && pagerIndex >= (count - showing))
                    pagerIndex = -1;
                self.bxslider.goToSlide(pagerIndex);
                firstOftheKind.parent().click();
            }
            return false;
        });

        // end configuration of media icons 

        jQuery("#nhs_BigMediaBrochureButton").click(function (event) {
            self.bxslider.goToSlide(-1);
            jQuery(".bxslider #nhs_BigMediaBrochureSlide").click();
        });

        self.bxslider = slider.bxSlider({
            minSlides: 2,
            maxSlides: 12,
            slideWidth: 126,
            slideMargin: 4,
            infiniteLoop: false,
            touchEnable: true,
            pager: false,
            hideControlOnEnd: true,
            moveSlides: 1,
            speed: 1000,
            onClickNext: function (el, slider) {
                var pagerIndex = parseInt(slider.active.index);
                pagerIndex += 2;

                var showing = el.getNumberSlidesShowing();
                var count = el.getSlideCount();
                if (pagerIndex <= count && pagerIndex >= (count - showing))
                    pagerIndex = -2;
                slider.active.index = pagerIndex;

            },
            onClickPrev: function (el, slider) {
                var pagerIndex = parseInt(slider.active.index);
                pagerIndex -= 2;
                if (pagerIndex < 0)
                    pagerIndex = 1;
                slider.active.index = pagerIndex;
            }
        });

        window.addEventListener("resize", function () {
            self.ResizePlayer();
            self.ResizeMedia();
            slider.reloadSlider();
        }, false);

        self.ResizePlayer();

        jQuery('.bx-prev').click(function (event) {
            jQuery.googlepush('Image Viewer', 'Image Viewer', 'Image Viewer - Icon Backward');
        });
        jQuery('.bx-next').click(function (event) {
            jQuery.googlepush('Image Viewer', 'Image Viewer', 'Image Viewer - Icon Forward');
        });

        jQuery('.nhs_BigMediaPrev').click(function (event) {
            jQuery('.bxslider li.active').prev().click();
            jQuery.googlepush('Image Viewer', 'Image Viewer', 'Image Viewer - Main Backward');
            self.bxslider.goToPrevSlide();

        });
        jQuery('.nhs_BigMediaNext').click(function (event) {
            jQuery('.bxslider li.active').next().click();
            jQuery.googlepush('Image Viewer', 'Image Viewer', 'Image Viewer - Main Forward');
            self.bxslider.goToNextSlide();

        });

        var ajaxCall;
        jQuery('.bxslider li').not("#nhs_BigMediaBrochureSlide").click(function (event) {
            if (jQuery(this).hasClass('active'))
                return false;

            self.player.stopVideo(true);

            if (typeof ajaxCall !== "undefined") {
                ajaxCall.abort();
            }
            var control = jQuery(this).find('img');
            self.bxslider.find('li').removeClass('active');
            jQuery(this).addClass('active');
            var url = control.data('url');
            var newAlt = control.attr('alt');
            var origEle = jQuery('#nhs_BigMediaPlayerContainer > :first-child');
            var type = control.data('type');
            self.selectType(type);

            self.updatePinterest(url);

            if (event.target.localName === "img") {
                if (type.indexOf('i-') !== -1) {
                    jQuery.googlepush('Image Viewer', 'Image Viewer', 'Image Viewer - Media Click');
                }
                self.moveSlider(control);
            }

            if (type.indexOf('v-') !== -1 || type.indexOf('l-ev') !== -1)
                jQuery.googlepush('Image Viewer', 'Image Viewer', 'Image Viewer - Video Slide');
            if (type.indexOf('l-flp') !== -1)
                jQuery.googlepush('Image Viewer', 'Image Viewer', 'Image Viewer - Floorplan Slide');
            else if (type.indexOf('i-') !== -1)
                jQuery.googlepush('Image Viewer', 'Image Viewer', 'Image Viewer - Media Slide');
            else if (type.indexOf('l-vt') !== -1)
                jQuery.googlepush('Image Viewer', 'Image Viewer', 'Image Viewer - Tours Slide');

            jQuery('#nhs_BigMediaPlayerContainer script').remove();

            var origElePos = origEle.offset();
            origEle.css('position', 'absolute');
            if (origEle.attr('id') === 'nhs_ajaxContent') {
                origEle.css('width', '100%');
            }
            origEle.offset(origElePos);
            //origEle.css('z-index','9');
            origEle.fadeTo(500, 0, function () {
                origEle.remove();
            });

            var newEle;
            if (type.indexOf('i-') !== -1) {
                newEle = jQuery('<img style="display:none;" src="' + url + '" />').hide();
                jQuery('#nhs_BigMediaPlayerContainer').html(newEle);

                newEle.load(function () {
                    jQuery('.nhs_BigMediaCaption').hide();
                    jQuery('.nhs_BigMediaCaption').html('<p>' + newAlt + '</p>');
                    jQuery('.nhs_BigMediaCaption').show();
                    self.ResizeMedia();
                    newEle.fadeIn(500);
                    jQuery('.nhs_BigMediaPlayer').css('background-image', 'none');
                    if (type.indexOf('v-') !== -1) {
                        var playIcon = jQuery('<span class="nhs_BtnPlayVideo" title="Play Video"></span>');
                        jQuery('#nhs_BigMediaPlayerContainer').append(playIcon);
                        playIcon.click(function () {
                            var videoElement = jQuery('<div id="nhs_VideP"></div>').css('opacity', 0);
                            jQuery('#nhs_BigMediaPlayerContainer').html(videoElement);

                            var videoId = control.data('id');

                            if (type === 'v-yt' || type === 'v-vm') {
                                if (type === 'v-yt') {
                                    self.player.playYouTubeVideo(videoId);
                                } else {
                                    self.player.playVimeoVideo(videoId);
                                }
                            } else if (type === 'v-bc') {
                                self.player.playBrightcoveVideo(videoId);
                            }

                            self.logVideo();
                            setTimeout(function () { jQuery('#nhs_VideP').fadeTo(500, 1) }, 500);
                        });

                    }
                });
            } else if (type.indexOf('l-') !== -1) {
                newEle = control.clone().hide();
                jQuery('.nhs_BigMediaCaption').hide();
                jQuery(newEle).click(function () {
                    var control = jQuery(this);
                    var url = control.data('url');
                    var type = control.data('type');
                    if (type.indexOf('v-') !== -1 || type.indexOf('l-ev') !== -1)
                        jQuery.googlepush('Image Viewer', 'Image Viewer', 'Image Viewer - Video Click');
                    if (type.indexOf('l-flp') !== -1)
                        jQuery.googlepush('Image Viewer', 'Image Viewer', 'Image Viewer - Floorplan Click');
                    else if (type.indexOf('l-vt') !== -1)
                        jQuery.googlepush('Image Viewer', 'Image Viewer', 'Image Viewer - Tours Click');
                    var eventCode = control.data('event');
                    self.logger.logAndRedirect(event, url, eventCode, self.parameters.communityId, self.parameters.builderId, self.parameters.planId, self.parameters.specId, self.parameters.marketId, true);

                });
                jQuery('.nhs_BigMediaCaption').html('<p>' + newAlt + '</p>');
                jQuery('.nhs_BigMediaCaption').show();
                jQuery('#nhs_BigMediaPlayerContainer').prepend(newEle);
                newEle.fadeIn(500);
                jQuery('.nhs_BigMediaPlayer').css('background-image', 'none');
                self.ResizeMedia();
            } else if (type === 'v-yt' || type === 'v-vm' || type === 'v-bc') {
                jQuery('.nhs_BigMediaCaption').hide();
                jQuery('.nhs_BigMediaCaption').html('<p>' + newAlt + '</p>');
                jQuery('.nhs_BigMediaCaption').show();
                newEle = jQuery('<div id="nhs_VideP"></div>').css('opacity', 0);
                jQuery('#nhs_BigMediaPlayerContainer').html(newEle);
                var videoId = control.data('id');

                if (type === 'v-yt' || type === 'v-vm') {
                    if (type === 'v-yt') {
                        self.player.playYouTubeVideo(videoId);
                    } else {
                        self.player.playVimeoVideo(videoId);
                    }
                } else if (type === 'v-bc') {
                    self.player.playBrightcoveVideo(videoId);
                }

                setTimeout(function () { jQuery('#nhs_VideP').fadeTo(500, 1) }, 500);
            }
        });

        jQuery(".bxslider #nhs_BigMediaBrochureSlide").click(function (event) {
            if (jQuery(this).hasClass('active'))
                return false;
            jQuery('.nhs_BigMediaIconsBar a').removeClass('active');
            self.bxslider.find('li').removeClass('active');
            jQuery(this).addClass('active');
            jQuery.googlepush('Lead Events', 'Image Viewer - Main Form', 'Open Form - Free Brochure');
            var origEle = jQuery('#nhs_BigMediaPlayerContainer > :first-child');

            var origElePos = origEle.offset();
            origEle.css('position', 'absolute');
            origEle.offset(origElePos);
            //origEle.css('z-index', '9');
            origEle.fadeTo(500, 0, function () {
                origEle.remove();
            });

            ajaxCall = jQuery.ajax({
                type: "GET",
                url: self.parameters.brochureUrl,
                success: function (htmlResult) {
                    jQuery('.nhs_BigMediaPlayer #nhs_ajaxContent').remove();
                    jQuery('.nhs_BigMediaCaption').hide();
                    htmlResult = jQuery(htmlResult).hide();
                    jQuery('#nhs_BigMediaPlayerContainer').html(htmlResult);
                    htmlResult.fadeIn(500);                    
                    self.ResizeMedia();
                },
                error: function (xhr) {

                }
            });
        });

        tb_init('#nhs_LogiBigMediaBox');

        jQuery('.bxslider li:first-child').click();

        jQuery("#nhs_Saved").click(function (event) {
            jQuery.googlepush('Image Viewer', 'Image Viewer', 'Image Viewer - Favorite');
            var control = jQuery(this);
            jQuery.NhsCancelEventAndStopPropagation(event);
            jQuery.ajax({
                url: self.parameters.SaveCommunity,
                type: "POST",
                success: function (result) {
                    if (result.data) {
                        control.replaceWith('<span class="nhs_Save nhs_Saved">Saved</span>');
                        jQuery('#nhs_NextStepsSaveThisItem').removeAttr('onclick').removeAttr('href');
                        jQuery('#pSaveThisItem').html('<span class="nhs_ItemSaved" id="nhs_NextStepsSaveThisItem">Saved to your profile</span>');
                        jQuery('#liSaveToPlanner').html('<span id=\"nhs_SaveThisItem\">Saved</span>');
                    }
                }
            });
        });
    },

    logVideo: function () {
        var siteVar = '';

        if (this.parameters.specId == 0 && this.parameters.planId == 0) {
            this.logger.logEvent('CDVID', this.parameters.communityId, this.parameters.builderId);
            siteVar = 'siteCommVV';
        } else {
            this.logger.logEvent('HDVID', this.parameters.builderId, this.parameters.builderId, (this.parameters.specId > 0 ? 0 : this.parameters.planId), this.parameters.specId);
            siteVar = 'siteHomeVV';
        }

        jQuery.SetDataLayerPair(siteVar);
    },

    updatePinterest: function (imgUrl) {
        var pinInstance = jQuery("div#button_pin_it a.pin-it-button");
        if (pinInstance.length > 0) {
            var currentHref = "http://pinterest.com/pin/create/button/?url=" + encodeURIComponent(this.parameters.url) + "&media=" + encodeURIComponent(imgUrl) + "&description=" + encodeURIComponent(this.parameters.description);
            pinInstance.attr("href", currentHref);
        }
    },

    moveSlider: function (img) {
        var self = this;
        var rect = jQuery(".bx-viewport")[0].getBoundingClientRect();
        var li = img.parent().parent();
        var prev = li.prev();
        var index;
        var showing;
        if (prev.length > 0) {
            var prevL = prev[0].getBoundingClientRect();
            if (prevL.left < rect.left) {
                showing = this.bxslider.getNumberSlidesShowing();
                index = parseInt(prev.find("img").data("slide"));
                if (index > showing - 2)
                    index -= showing - 2;
                else
                    index = 0;

                self.bxslider.goToSlide(index, 'prev');

                //self.bxslider.goToPrevSlide();
                return;
            }
        }

        var next = li.next();
        if (next.length > 0) {
            var nextL = next[0].getBoundingClientRect();
            var postion = nextL.left;
            if (postion > rect.right) {
                index = parseInt(img.data("slide"));
                showing = self.bxslider.getNumberSlidesShowing();
                var count = self.bxslider.getSlideCount();
                if (index <= count && index >= (count - showing))
                    index = -1;
                self.bxslider.goToSlide(index, 'next');

                //self.bxslider.goToNextSlide();
                return;
            }
        }

    },

    selectType: function (type) {
        jQuery('.nhs_BigMediaIconsBar a').removeClass('active');
        if (type === 'l-ev' || type.indexOf('v-') !== -1) {
            jQuery('.nhs_BigMediaIconsVideo a').addClass('active');
        } else if (type === 'l-vt') {
            jQuery('.nhs_BigMediaIconsTour a').addClass('active');
        } else if (type === 'l-flp') {
            jQuery('.nhs_BigMediaIconsFloorPlan a').addClass('active');
        } else {
            jQuery('.nhs_BigMediaIconsImage a').addClass('active');
        }
    },

    markup: function (html, data) {
        var m;
        var i = 0;
        var match = html.match(data instanceof Array ? /{{\d+}}/g : /{{\w+}}/g) || [];

        while (m === match[i++]) {
            html = html.replace(m, data[m.substr(2, m.length - 4)]);
        }
        return html;
    },

    ResizePlayer: function () {
        var bHeight = window.innerHeight;
        var bWidth =  window.innerWidth;
        console.clear();
        console.log("height:" + bHeight);
        console.log("width:" + bWidth);
        jQuery("#nhs_BigMediaBox").height(bHeight).width(bWidth);
        var hHeight = jQuery(".nhs_BigMediaHeader").outerHeight();
        var tHeight = jQuery(".nhs_BigMediaThumbsBox").outerHeight();
        var pHeight = bHeight - hHeight - tHeight;
        jQuery(".nhs_BigMediaPlayer, #nhs_BigMediaPlayerContainer").height(pHeight);
    },

    ResizeMedia: function () {
        jQuery("#nhs_BigMediaPlayerContainer > :first-child").css("margin-top", "0");

        var pHeight = jQuery("#nhs_BigMediaPlayerContainer").height();
        var imgHeight = jQuery("#nhs_BigMediaPlayerContainer > :first-child").height();
        var diffHeight = pHeight - imgHeight;

        if (diffHeight > 1) {
            jQuery("#nhs_BigMediaPlayerContainer > :first-child").css("margin-top", diffHeight / 2 + "px");
        }
    }
};

