﻿
NHS.Scripts.HomeDetail.Carousel = function (parameters) {
    this.parameters = parameters;
    this._communityId = parameters.communityId;
    this._builderId = parameters.builderId;
    this._planId = parameters.specId > 0 ? 0 : parameters.planId;
    this._specId = parameters.specId;
    this._totalImagesOnFloorPlanSection = 2;
    this._partnerId = parameters.partnerId;
    this._log = new NHS.Scripts.Globals.EventLogger({ siteRoot: parameters.siteRoot, fromPage: parameters.fromPage, partnerId: parameters.partnerId, marketId: parameters.marketId });
};

NHS.Scripts.HomeDetail.Carousel.prototype =
{
    get_log: function () { return this._log; },
    get_commId: function () { return this._communityId; },
    get_builderId: function () { return this._builderId; },
    get_plan: function () { return this._planId; },
    get_spec: function () { return this._specId; },

    // Initialize carousel controls
    initFloorPlanCarousel: function () {
        
        jQuery(".nhs_floorPlan").click(function (e) {
            jQuery.NhsCancelEventAndStopPropagation(e);
            var control = jQuery(this);
            var de = document.documentElement;
            var h = window.innerHeight || self.innerHeight || (de && de.clientHeight) || document.body.clientHeight;
            h = h - 100;
            var url = control.attr("href");
            url += h;
            var caption = control.attr("alt");
            tb_show(caption, url);
        });

        var lazyLoad = new NHS.Scripts.LazyLoad(this.parameters.lazyLoad);
        lazyLoad.LazyLoadImages();


        var imagesLength = jQuery(".nhs_FloorPlanCarousel").find('a').length;
        if (imagesLength == 1) return;
        jQuery(".nhs_FloorPlanCarousel").jCarouselLite({
            btnNext: ".nhs_FloorPlanImageNext",
            btnPrev: ".nhs_FloorPlanImagePrev",
            visible: this._totalImagesOnFloorPlanSection,
            circular: true,
            autoWidth: true,
            responsive:true,
            start: 0
        });
        // Workarround for ticket 42703, remove thickbox class and rel att (replicated images)
        var images = jQuery(".nhs_FloorPlanCarousel").find('a');
        images.each(function (index) {
            if (index < this._totalImagesOnFloorPlanSection || index >= (images.size() - this._totalImagesOnFloorPlanSection)) {
                if (this._partnerId != 88)
                    jQuery(this).removeClass("thickbox");
                jQuery(this).removeAttr('rel');
            }
        });

    },

    initSimilarHomes: function () {
        jQuery(".nhs_SimilarHomePlanCarousel").jCarouselLite({
            btnNext: ".nhs_SimilarHomePlanCarouselNext",
            btnPrev: ".nhs_SimilarHomePlanCarouselPrev",
            visible: 4,
            circular: true
        });

        jQuery('#lnkSimilarAvailableHomes').click((function () {
            this.get_log().logMultiEvent('COMDETSIM', this._communityId, this._builderId, this._planId, this._specId);
        }).bind(this));


        jQuery('#nhs_expandCollapseMarketingSection').click((function () {
            if (jQuery("#nhs_expandCollapseMarketingSection").hasClass("plus")) {
                jQuery("#nhs_expandCollapseMarketingSection").removeClass("plus");
                jQuery("#nhs_expandCollapseMarketingSection").addClass("minus");
                jQuery('#nhs_expandCollapseLink').text("(click to collapse)");
                jQuery('#nhs_crossMarketinglist').show();
            }
            else {
                jQuery("#nhs_expandCollapseMarketingSection").removeClass("minus");
                jQuery("#nhs_expandCollapseMarketingSection").addClass("plus");
                jQuery('#nhs_expandCollapseLink').text("(click to expand)");
                jQuery('#nhs_crossMarketinglist').hide();
            }
            return false;
        }));

        jQuery('#nhs_expandCollapseLink').click((function () {
            if (jQuery("#nhs_expandCollapseMarketingSection").hasClass("plus")) {
                jQuery("#nhs_expandCollapseMarketingSection").removeClass("plus");
                jQuery("#nhs_expandCollapseMarketingSection").addClass("minus");
                jQuery('#nhs_expandCollapseLink').text("(click to collapse)");
                jQuery('#nhs_crossMarketinglist').show();
            }
            else {
                jQuery("#nhs_expandCollapseMarketingSection").removeClass("minus");
                jQuery("#nhs_expandCollapseMarketingSection").addClass("plus");
                jQuery('#nhs_expandCollapseLink').text("(click to expand)");
                jQuery('#nhs_crossMarketinglist').hide();
            }
            return false;
        }));
    }
}
