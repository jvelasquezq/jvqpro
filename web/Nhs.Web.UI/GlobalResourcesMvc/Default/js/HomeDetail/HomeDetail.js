﻿NHS.Scripts.HomeDetail.HomeDetails = function (parameters) {
    this._parameters = parameters;
    this.displayNewFullImageViewer = parameters.displayNewFullImageViewer;
    this.NewFullImageViewerUrl = parameters.NewFullImageViewerUrl;
    this.ShowNewFullImageViewerUrl = parameters.ShowNewFullImageViewerUrl;
    this.method = parameters.method;
    this._communityId = parameters.communityId;
    this._builderId = parameters.builderId;
    this._planId = parameters.specId > 0 ? 0 : parameters.planId;
    this._specId = parameters.specId;
    this._log = new NHS.Scripts.Globals.EventLogger({ siteRoot: parameters.siteRoot, fromPage: parameters.fromPage, partnerID: parameters.partnerId, partnerName: parameters.partnerName, tdvTable: parameters.tdvTable });
    this._type = parameters.fromPage;
    this._showSocialIcons = parameters.showSocialIcons;

    _commIdHD = parameters.communityId;
    _builderIdHD = parameters.builderId;

    parameters.galleryParams.logger = this._log;

    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    parameters.mapParametes.logInfo = parameters.logInfo;
    parameters.mapParametes.logger = this._log;
    this.googlePropertyMap = new NHS.Scripts.GooglePropertyMap(parameters.mapParametes);

    this._nearbyHomes = new NHS.Scripts.PropertyMap.NearbyComms(parameters.carouselParams);
    this._homeCarousel = new NHS.Scripts.HomeDetail.Carousel(parameters.carouselParams);
};

NHS.Scripts.HomeDetail.HomeDetails.prototype =
{
    get_log: function () { return this._log; },
    get_mp: function () { return this._mp; },
    get_commId: function () { return this._communityId; },
    get_builderId: function () { return this._builderId; },
    get_plan: function () { return this._planId; },
    get_spec: function () { return this._specId; },

    initialize: function () {
        this.loadGalleryAsync();
        this._setUpControls();
        this.setUpToolbar();
        this.googlePropertyMap.init();
        this._homeCarousel.initSimilarHomes();
        this._nearbyHomes.attachClickEventsToCommLinks();
        this._updateAdsPosition();
    },

    SetupNearbyCommunities: function (parameters) {
        if (this.googlePropertyMap.isMapCreate) {
            this.googlePropertyMap.googleApi.processResult(parameters);
            this.googlePropertyMap.googleApi.AutoFit();
        } else {
            this.googlePropertyMap.parameters.NearbyComms = parameters;
        }

    },

    setUpToolbar: function () {
        if (jQuery("#nhs_Crumbs").length === 0)
            return false;

        var toolBottom = jQuery("#nhs_Crumbs").offset().top - 20;

        jQuery(window).scroll(function (event) {
            if (jQuery("#nhs_ShowToolbar").val() === "true") {
                var y = jQuery(document).scrollTop();

                if (y < toolBottom) {
                    jQuery(".nhs_DetailsToolBar").removeClass("fixed");
                } else {
                    jQuery(".nhs_DetailsToolBar").addClass("fixed");
                }
            }
        });

        jQuery(".nhs_DetailsToolBar ul li a").not(".nhs_MediaPlayerMaximize").click(function (event) {
            jQuery.NhsCancelEventAndStopPropagation(event);
            var control = jQuery(this);
            var id = control.attr("href");
            if (id === "#HomesAndPlans") {
                jQuery.googlepush('Site Links', 'Home - Toolbar', 'See homes');
            }
            else if (id === "#SchoolsAndAmenities") {
                jQuery.googlepush('Site Links', 'Home - Toolbar', 'Schools & amenities');
            }
            else if (id === "#Maps") {
                jQuery.googlepush('Home Detail Events', 'Home - Toolbar', 'View on Map');
                jQuery('#nhs_propertyMapCollapsibleOpen').click();
            }
            else if (id === "#Contact") {
                jQuery.googlepush('Site Links', 'Home - Toolbar', 'Contact');
            }
            else if (id === "#floorPlan") {
                jQuery.googlepush('Outbound Links', 'Home - Toolbar', 'Plan Viewer');
            }
            jQuery(id).ScrollToPosLess(5);
        });
    },

    // Initialize home details view controls
    _setUpControls: function () {
        var self = this;


        jQuery("#nhs_HomeDetailv2, #nhs_HomeDetail").on('click', "a[href=#top]", function () {
            jQuery('html, body').animate({ scrollTop: 0 }, 'slow');
            return false;
        });

        var totalPrice = self._parameters.PlanPrice;
        jQuery('#spnTotalOptionPrice').html((totalPrice).formatCurrency('$', 0, '.', ','));
        jQuery('.nhs_FloorPlanOptions').click(function () {
            var finalPrice = 0;

            jQuery('.nhs_FloorPlanOptions').each(function () {
                if (this.checked) {
                    finalPrice += parseInt(jQuery(this).val());
                }
            });

            jQuery("#spnTotalOptionPrice").effect("highlight", { color: '#0091da' }, 1000);
            jQuery('#spnTotalOptionPrice').html((totalPrice + finalPrice).formatCurrency('$', 0, '.', ','));
        });


        jQuery.ajax({
            url: self._parameters.GetFloorPlanGalleryUrl,
            type: "GET",
            success: function (data) {
                if (typeof data == "string" && data.length > 0) {
                    jQuery('#nhs_FloorPlan').html(data);
                    jQuery("#nhs_floorPlansAnchor").show();
                    jQuery("#nhs_Floorplan, #nhs_floorPlanSection").slideDown();
                    self._homeCarousel.initFloorPlanCarousel();
                }
            }
        });


        if (self._parameters.isBilled == "True" && self._parameters.PreviewMode == "False") {
            var isNearbyShowed = false;

            var defaultOptios = {
                ElementVisibleId: 'SchoolsAndAmenities',
                UpdateAction: function (isVisible) {
                    if (isVisible && isNearbyShowed == false) {
                        isNearbyShowed = true;
                        jQuery("#nhs_NearbyHomesContent").load(self._parameters.GetNearbyCommunities,
                            {
                                communityId: self._parameters.communityId,
                                builderId: self._parameters.builderId,
                                brandName: self._parameters.BrandName
                            },
                            function () {
                                self._nearbyHomes.attachClickEventsToCommLinks();
                            });
                    }
                }
            };
            jQuery.LazyLoad(defaultOptios);
        }

        if (this._showSocialIcons.toLowerCase() === 'false') {
            jQuery('#addthis_bookmark').hide();
            jQuery('.addthis_separator').hide();
            jQuery('.addthis_button_facebook').hide();
            jQuery('.addthis_button_twitter').hide();
            jQuery('.nhs_DetailSmBrandImg').hide();
            jQuery('.builder_link').hide();

            jQuery('#addthis_more_sharing').hide();
            jQuery('#addthis_bookmark_bottom').hide();
            jQuery('.nhs_MediaShareOverlay, .nhs_PinterestOverlayBig').hide();
            jQuery('#brochure_pro_step2').hide();
        }

        jQuery('#nhs_NextStepsBackSearch').attr('href', jQuery('.nhs_BreadcrumbBackSearch').attr('href'));

        if (this._type.indexOf('v3') != -1 || this._type.indexOf('v4') != -1) {
            jQuery('#nhs_AdColumn').css('margin-top', '118px');
            jQuery('#mv_AdColumn').css('margin-top', '118px');
        }

        jQuery("#btnFreeBrochure").hover(
              function () {
                  jQuery(this).addClass("btn_FreeBrochureHover");
              },
              function () {
                  jQuery(this).removeClass("btn_FreeBrochureHover");
              }
            );

        jQuery('#nhs_propertyMapCollapsibleClose').click(function () {
            jQuery("#Maps").ScrollToPosLess(100);
            setTimeout(function () {
                self._resetAdsColum();
                //console.log('call resset');
            }, 500);
        });

        jQuery("#nhs_detailDescriptionToggle").click(function () {
            if (jQuery('#nhs_detailDescriptionToggle').text().trim() === "..." + self._parameters.MoreWord) {
                jQuery('#nhs_detailDescriptionToggle').text(self._parameters.LessWord);
                jQuery("#nhsDetailDescriptionArea").html("");
                jQuery("#nhsDetailDescriptionArea").html(Encoder.htmlDecode(description));
                return false;
            } else {
                jQuery('#nhs_detailDescriptionToggle').text("..." + self._parameters.MoreWord);
                jQuery("#nhsDetailDescriptionArea").html("");
                jQuery("#nhsDetailDescriptionArea").html(Encoder.htmlDecode(chunkedDescription));
                return false;
            }
        });

        this._setupCTAEventNames();
    },
    _resetAdsColum: function () {
        jQuery('#nhs_AdColumn > div').removeClass();
        jQuery('#nhs_AdColumn > div').addClass('clearfix');
        jQuery('#nhs_AdColumn > div').css('top', 'auto');
        jQuery('#nhs_AdColumn > div').css('bottom', 'auto');
    },
    _setupCTAEventNames: function () {
        jQuery('.nhs_DetailsInfoSchoolAmenities .btn_FreeBrochure').click(function () {
            NHS.Scripts.Helper.createCookie('CTAName', 'Home - CTA Home Info');
        });

        jQuery('.nhs_RequestAppointment .btn_NhsMapSend').click(function () {
            NHS.Scripts.Helper.createCookie('CTAName', 'Home - CTA Request Appointment');
            jQuery.googlepush('Lead Events', NHS.Scripts.Helper.readCookie('CTAName'), 'Open Form - Free Brochure');
        });

        jQuery('#nhs_RequestAppointmentVWO a.btnCss').click(function () {
            NHS.Scripts.Helper.createCookie('CTAName', 'Home - Gallery CTA Main Form - Request Appointment');
            jQuery.googlepush('Lead Events', NHS.Scripts.Helper.readCookie('CTAName'), 'Open Form - Free Brochure');
        });

        jQuery('.nhs_NextStepsContent .brochure_link a').click(function () {
            NHS.Scripts.Helper.createCookie('CTAName', 'Home - CTA Next Steps Free Brochure');
        });

        jQuery('.nhs_NextStepsContent .special_offers a').click(function () {
            NHS.Scripts.Helper.createCookie('CTAName', 'Home - CTA Next Steps Special Offers');
        });

        jQuery('.nhs_NextStepsContent .schedule a').click(function () {
            NHS.Scripts.Helper.createCookie('CTAName', 'Home - CTA Next Steps Appointment');
            jQuery.googlepush('Lead Events', NHS.Scripts.Helper.readCookie('CTAName'), 'Open Form - Free Brochure');
        });
    },
    _updateAdsPosition: function () {
        if ((jQuery('#nhs_DetailsMain').length > 0) & (jQuery('#nhs_AdColumn').length > 0)) {
            //floating column
            var colHeight = jQuery('#nhs_DetailsMain').height();
            this._adsHeight = jQuery('#nhs_AdColumn').height();

            var colBottom = colHeight + jQuery('#nhs_AdColumn').offset().top;

            var adsTop = colHeight - this._adsHeight - 10;
            var adsBottom = this._adsHeight + jQuery('#nhs_AdColumn').offset().top;
            var browserHeight = jQuery(window).height();
            var browserBottomY;

            jQuery(window).scroll((function (event) {

                // what the y position of the scroll is
                var y = jQuery(document).scrollTop();
                colHeight = jQuery('#nhs_DetailsMain').height();

                colBottom = colHeight + jQuery('#nhs_AdColumn').offset().top;

                adsTop = colHeight - this._adsHeight - 10;
                adsBottom = this._adsHeight + jQuery('#nhs_AdColumn').offset().top;
                browserHeight = jQuery(window).height();
                browserBottomY = y + browserHeight;
                //console.log(colHeight);
                //console.log(this._adsHeight);
                //console.log(adsTop);

                // is list column longer than ad column
                if (colBottom > adsBottom) {
                    // whether scroll is below the ad's top position
                    if (y >= (adsBottom - browserHeight)) {
                        // if so, ad the fixed class          
                        if (browserBottomY > colBottom) {
                            jQuery('#nhs_AdColumn > div').removeClass('fixedBottom');
                            jQuery('#nhs_AdColumn > div').addClass('absolute');
                            jQuery('#nhs_AdColumn > div').css('top', (adsTop + 'px'));
                            jQuery('#nhs_AdColumn > div').css('bottom', 'auto');
                        } else {
                            jQuery('#nhs_AdColumn > div').removeClass('absolute');
                            jQuery('#nhs_AdColumn > div').addClass('fixedBottom');
                            jQuery('#nhs_AdColumn > div').css('top', 'auto');
                            jQuery('#nhs_AdColumn > div').css('bottom', 0);
                        }
                    } else {
                        // otherwise remove it
                        jQuery('#nhs_AdColumn > div').removeClass('fixedBottom');
                        jQuery('#nhs_AdColumn > div').removeClass('absolute');
                        jQuery('#nhs_AdColumn > div').css('top', 'auto');
                        jQuery('#nhs_AdColumn > div').css('bottom', 'auto');
                    }
                }

            }).bind(this));
        }
    },
    SaveHome: function () {
        var self = this;

        logger.logEventWithParameters('HOMSTC', self._parameters.logInfo);

        jQuery.post(self.method, null, (function (json) {
            self.ShowSuccessMessage(json);
            if (json.data) {
                jQuery('#nhs_NextStepsSaveThisItem').removeAttr('onclick');
                jQuery('#nhs_NextStepsSaveThisItem').removeAttr('href');
                jQuery('#pSaveThisItem').html('<span class="nhs_ItemSaved" id="nhs_NextStepsSaveThisItem">' + self._parameters.SavedToYourProfileText + '</span>');
                jQuery('#liSaveToPlanner').html('<span id=\"nhs_SaveThisItem\">' + self._parameters.SavedText + '</span>');
                //                this.favText = jQuery('#nhs_SaveThisItem').text();
                //                if (this.isMediaPlayerOpened)
                //                    jQuery('#nhs_SaveThisItem').text('');
            }
        }).bind(this), "json");
    },
    NextStepsSaveHome: function () {
        this.SaveHome();
        logger.logEventWithParameters('HDNSSTF', this._parameters.logInfo);
    },
    ShowSuccessMessage: function (json) {
        if (json.data) {
            jQuery("#nhs_SaveThisItem").hide();
            jQuery("#SuccessMessage").show();
        }
        else {
            window.location.href = json.redirectUrl;
        }
    },
    loadGalleryAsync: function () {
        var self = this;
        jQuery.ajax({
            url: self._parameters.galleryParams.urlGallery,
            type: "post",
            cache: false,
            data: { 'communityId': self._parameters.communityId, 'planId': self._parameters.planId, 'specId': self._parameters.specId, 'isPreview': self._parameters.galleryParams.isPreview },
            success: function (data) {
                jQuery(data.ExternalMediaLinks).each(function () {
                    this.Url = this.Url.replace('href="/detailgetgallery"', 'href="' + window.location + '"').
                                        replace('src="http://images.newhomesource.com/images/1x1.gif"', '').
                                        replace('data-src', 'src').replace('class="async"', '');
                });
                self._parameters.galleryParams.externalLinks = data.ExternalMediaLinks;
                self._parameters.galleryParams.firstImgUrl = data.FirstImage;
                self._parameters.galleryParams.description = data.PinterestDescription;
                self._parameters.galleryParams.mediaObjs = data.PropertyMediaLinks;
                self._parameters.galleryParams.logger = self._log;
                self._mp = new NHS.Scripts.GalleryViewer(self._parameters.galleryParams);
                self._mp.initGallery();
                self.setupGallery();
            }
        });
    },
    setupGallery: function () {
        var self = this;

	if (this.displayNewFullImageViewer && (this._parameters.galleryParams.mediaObjs.length > 0 || this._parameters.galleryParams.externalLinks.length > 0)) {
            var callmodal = true;
            jQuery(".nhs_MediaPlayerMaximize").click(function (event) {
            jQuery.googlepush('Image Viewer', 'Community - Gallery', 'Image Viewer - Expand');
                if (callmodal) {
                    callmodal = false;
                    jQuery.ajax({
                        url: self.NewFullImageViewerUrl,
                        type: "Get",
                        success: function (html) {
                            jQuery("body").prepend(html);
                            callmodal = true;
                        }
                    });
                }
            });

            if (self.ShowNewFullImageViewerUrl)
                jQuery(".nhs_MediaPlayerMaximize").click();
        }
        //else {
        //    jQuery('.nhs_MediaShareOverlay').css('right', '5px');
        //    jQuery('.nhs_MediaPlayerMaximize').hide();
        //}

        if (this._showSocialIcons.toLowerCase()) {
            jQuery(".nhs_MediaShareOverlay").html("<div id=\"button_pin_it\"><a target=\"_blank\" href=\"http://pinterest.com/pin/create/button/?url=" + encodeURIComponent(window.location) + "&media="
                + encodeURIComponent(this._parameters.galleryParams.firstImgUrl) + "&description=" + encodeURIComponent(this._parameters.galleryParams.description)
                + "onclick=\"\" class=\"pin-it-button\" count-layout=\"horizontal\"><img border=\"0\" src=\"//assets.pinterest.com/images/PinExt.png\" title=\"Pin It\"></a></div>"
                + "<div id=\"button_compact\"><a class=\"addthis_button_compact at300m\" href=\"#\"><span class=\"at4-icon aticon-compact\" style=\"background-color: rgb(252, 109, 76);\">"
                + "<span class=\"at_a11y\"></span></span></a></div>");
        }
    }
}
