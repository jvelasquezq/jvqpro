﻿NHS.Scripts.HomePage = function (searchClass) {
    this.searchClass = searchClass;
};

NHS.Scripts.HomePage.prototype =
{
    initialize: function () {
        this._setUpCommonControls();
    },

    _setUpCommonControls: function (setupCarousel) {

        // Home page
        jQuery('#SearchText').on('focus', function () {
            jQuery('#nhs_HomeSearchOptions').slideDown(250, function () {
                jQuery('#nhs_HomeSearchOptions > fieldset').fadeIn(1000);
            });
        });

        jQuery('#PriceLow, #PriceHigh').on('focus', function () {
            var control = jQuery(this);
            if (!control.is('select') && control.val() != null && control.val().length != 0) {
                control.priceFormat({
                    prefix: '$',
                    thousandsSeparator: ',',
                    centsLimit: 0,
                    insertPlusSign: false,
                    limit: 7
                });
            }
        });

        jQuery('#PriceLow, #PriceHigh').on('blur', function () {
            var control = jQuery(this);
            if (!control.is('select') && control.val() != null && control.val().length != 0) {
                control.priceFormat({
                    prefix: '$',
                    thousandsSeparator: ',',
                    centsLimit: 0,
                    insertPlusSign: false,
                    limit: 7
                });
            }
        });

//        jQuery('#frmHomePage').on('submit', function (e) {
//            if (jQuery('#SearchText').val().trim() == "" || (jQuery('#SearchText').val().indexOf(',') != -1 && jQuery('#SearchText').val().split(',')[1] == '')) {
//                e.preventDefault();
//                jQuery("#nhs_field_validation_error_home").show();
//                return false;
//            }
//            return true;
//        });

        if (typeof this.searchClass != 'undefined') {
            this.searchClass.validateSearchBox(jQuery('#frmHomePage'), jQuery("#nhs_field_validation_error_home"), 
                jQuery('#SearchText'), false);
        }

        //Example Text
        //var fdv = new NHS.Scripts.Helper.FieldDefaultValue('SearchText', this._typeAheadText, 'nhs_LocationDefault', 'Search');
        //fdv.initialize();

        if (setupCarousel) {
            //Carousel
            jQuery("#nhs_HomeBoutiqueBar").jCarouselLite({
                btnNext: "#nhs_HomeBoutiqueCarouselNext",
                btnPrev: "#nhs_HomeBoutiqueCarouselPrev",
                visible: 4,
                circular: true,
                auto: 5000,
                speed: 1000
            });

            //Accordion
            jQuery(".nhs_HomeArticleWPhoto div:not(:first)").hide();
            jQuery(".nhs_HomeArticleWPhoto a[class*='nhs_HomeArticleLink']").click(function () {
                var isSelected = jQuery(this).attr("selected");
                if (isSelected == "true")
                    return false;

                jQuery(".nhs_HomeArticleWPhoto a[class*='nhs_HomeArticleLink']").removeAttr("selected");
                jQuery(this).attr("selected", "true");
                jQuery(".nhs_HomeArticleWPhoto div:visible").slideUp("fast");
                jQuery(this).parent().next().slideDown("fast");
                return false;
            });

            // jQuery('#nhs_HomeSearchForm').corner('top 5px');
        }
    },

    initNhlSlideShow: function () {
        //Slideshow - NHL only
        jQuery('.fadein img:gt(0)').hide();
        setInterval(function () { jQuery('.fadein :first-child').fadeOut(1000).next('img').fadeIn(250).end().appendTo('.fadein'); }, 5000);
    }
}




