﻿// Loads the enhanced ecommerce pluggin inside GA required to used the encanced commands
jQuery(function () {
    setTimeout(function () { ga('require', 'ec') }, 2000);
});

// Constructor, it receives a json object with parameters
// Pass always PartnerName and the TDV values from Content xml 
NHS.Scripts.Globals.PerformanceLogger = function(parameters) {
    this.parameters = parameters;
    this.tdvTable = null;

    if (parameters.tdvTable) {
        this.tdvTable = parameters.tdvTable;
    }
};


NHS.Scripts.Globals.PerformanceLogger.prototype =
{
    logEventWithParams: function (eventName, parameters) {
        if (parameters.communityName || parameters.commName)
            this.parameters.communityName = parameters.communityName || parameters.commName;
        else
            this.parameters.communityName = null;

        if (parameters.builderName)
            this.parameters.builderName = parameters.builderName;
        else
            this.parameters.builderName = null;


        if (parameters.marketName)
            this.parameters.marketName = parameters.marketName;
        else
            this.parameters.marketName = null;


        if (parameters.marketId)
            this.parameters.marketId = parameters.marketId;
        else
            this.parameters.marketId = null;

        this.logEvent(eventName);
    },
    logEvent: function (eventName) {
        var marketInfo = null;

        // If the event is defined in the tdvmetrix.xml so log it with GA enhaced ecommerce commands and retrieve the metric name and value.
        if (this.tdvTable && this.tdvTable[eventName]) {
            var tdvName = this.tdvTable[eventName].name;
            var tdvValue = this.tdvTable[eventName].value;

            var categoryName = this.getCategoryName();
            var guid = getGUID();
            if (this.parameters.marketName) {
                marketInfo = this.parameters.marketName + "|" + this.parameters.marketId;
            }

            dataLayer.push({'ecommerce': {
                                    'purchase': {
                                            'actionField': { 'id': guid, 'affiliation': marketInfo, 'revenue': tdvValue },
                                            'products': [{ 'name': tdvName, 'id': eventName, 'price': tdvValue, 'category': categoryName }]
                                    }
                            }, 'event' : 'transactionComplete'});
        }
    },
    getCategoryName: function() {
        var categoryName = this.parameters.partnerName;

        categoryName = (this.parameters.marketId || "NULL") + "\\" + categoryName;

        categoryName = (this.parameters.marketName || "NULL") + "\\" + categoryName;

        categoryName = (this.parameters.builderName || "NULL") + "\\" + categoryName;

        categoryName = (this.parameters.communityName || "NULL") + "\\" + categoryName;

        return categoryName;
    }
};


