﻿// Global Class for State Index Page
NHS.Scripts.StateIndex = function (parameters) {
    this.parameters = parameters;
};

NHS.Scripts.StateIndex.prototype =
{
    init: function () {
        var self = this;
        var mapOptions = this.parameters.OptionsForMap;
        var googleApi = new NHS.Scripts.GoogleMapApi(mapOptions);



        googleApi.options.Events.InfowindowTooltipReadyCallBack = function () {
            var card = jQuery(".nhs_MapHoverCardStateIndex").parent().parent().parent();
            var arrow = card.parent().find("> div").first().find("> div");
            arrow.first().hide();
            arrow.eq(2).hide();
            card.next('div').hide();
            var width = card.width() + 75;
            var height = card.height() + 75;
            this.infowindowTooltip.setOptions({ pixelOffset: this.GetInfowindowOffset(width, height, googleApi.map, this.infowindowTooltip.getAnchor(), true) });
        };


        googleApi.options.Events.MarkerClickCallBack = function (info, infowindow, infowindowTooltip, marker) {

            if (jQuery("#nhs_marketLink" + info.MarketId).is("a"))
                jQuery("#nhs_marketLink" + info.MarketId)[0].click();
            else
                window.location = jQuery("#nhs_marketLink" + info.MarketId).val();
        };

        googleApi.options.Events.MarkerMouseOverCallBack = function (info, infowindow, infowindowTooltip, marker) {

            var name = info.MarketName.replace(' ', '&nbsp;') + ",&nbsp;" + info.StateAbbr + (self.parameters.useCounts ? "&nbsp;(" + (info.TotalCommunities) + ")" : "");
            var html = "<div class=\"nhs_MapHoverCardStateIndex\" style='color:black'><p>" + name + "</p></div>";
            infowindow.close();
            infowindowTooltip.setContent(html);
            infowindowTooltip.open(googleApi.map, marker);
        };

        googleApi.options.Events.MarkerMouseOutCallBack = function (info, infowindow, infowindowTooltip, marker) {
            infowindowTooltip.close();
        };

        googleApi.getIconMarkerPoint = function (sources, data) {
            return self.parameters.icon;
        };

        googleApi.getNameMarkerPoint = function (sources, data) {
            var prOptions = this.options.ProcessResultOptions;
            return data[prOptions.Name] + "(" + data.TotalCommunities + ")";
        };

        googleApi.createMap();
        google.maps.event.addListener(googleApi.map, 'idle', self.RemoveGoogleInfo);
        self.processResult(self.parameters.MarketPoints, googleApi);

        jQuery('#nhs_StateBrowseDiv0').css('display', 'none');
        jQuery('#nhs_StateBrowseDiv1').css('display', 'none');
        jQuery('#nhs_StateBrowseDiv2').css('display', 'none');

        jQuery('#indexTab1').click((function () { this._switchBoxDisplay('li1', 'nhs_StateBrowseDiv3'); return false; }).bind(this));
        jQuery('#indexTab2').click((function () { this._switchBoxDisplay('li2', 'nhs_StateBrowseDiv2'); return false; }).bind(this));
        jQuery('#indexTab3').click((function () { this._switchBoxDisplay('li3', 'nhs_StateBrowseDiv1'); return false; }).bind(this));
        jQuery('#indexTab4').click((function () { this._switchBoxDisplay('li4', 'nhs_StateBrowseDiv0'); return false; }).bind(this));

        jQuery('#btnSearchHomes').click(function () { if (jQuery('#SelectedMarketId').val() == "0") { alert("Area is required."); return false; } });
        if (jQuery('div#nhs_StateBrowseDiv0 ul li').length == 0) jQuery('li#li4').hide();

        googleApi.AutoFit();

        jQuery('#SearchText').on('focus', function () {
            jQuery('#nhs_IndexSearchOptions').slideDown(250, function () {
                jQuery('#nhs_IndexSearchOptions > fieldset').fadeIn(1000);
            });
        });

        jQuery('#PriceLow, #PriceHigh').on('focus', function () {
            var control = jQuery(this);
            if (control.val().length != 0) {
                control.priceFormat({
                    prefix: '$',
                    thousandsSeparator: ',',
                    centsLimit: 0,
                    insertPlusSign: false,
                    limit: 7
                });
            }
        });

        jQuery('#PriceLow, #PriceHigh').on('blur', function () {
            var control = jQuery(this);
            if (control.val().length != 0) {
                control.priceFormat({
                    prefix: '$',
                    thousandsSeparator: ',',
                    centsLimit: 0,
                    insertPlusSign: false,
                    limit: 7
                });
            }
        });

    },

    processResult: function (results, googleApi) {
        googleApi.showLoading();
        var prOptions = googleApi.options.ProcessResultOptions;
        if (results !== null) {
            for (var i = 0; i < results.length; i++) {
                var html = googleApi.getHtmlInfowindow(results, results[i]);
                var icon = googleApi.getIconMarkerPoint(results, results[i]);
                googleApi.createMarkerPoint(results[i][prOptions.Latitude], results[i][prOptions.Longitude], null, html, icon, results[i]);
            }
        }
        googleApi.hideLoading();
    },

    _switchBoxDisplay: function (linkId, popId) {

        var link = linkId;
        var pop = jQuery('#' + popId);
        var list1 = jQuery('#li1');
        var list2 = jQuery('#li2');
        var list3 = jQuery('#li3');
        var list4 = jQuery('#li4');
        var stateDiv = jQuery('#nhs_StateBrowseDiv');
        list1.removeAttr("class");
        list2.removeAttr("class");
        list3.removeAttr("class");
        list4.removeAttr("class");
        stateDiv.find('.nhs_IndexBox').hide();
        if (link == 'li1') {
        }
        if (link == 'li2') {
            list2.addClass('nhs_Selected');
        }
        if (link == 'li3') {
            list3.addClass('nhs_Selected');
        }
        if (link == 'li4') {
            list4.addClass('nhs_Selected');
        }
        pop.show();
    },
    RemoveGoogleInfo: function () {
        if (jQuery(".gm-style > div > a").attr("href")) {
            var aux;
            var len = jQuery('.gm-style-cc').length;
            for (aux = 1; aux < len; aux += 1) {
                jQuery('.gm-style-cc').eq('-1').remove();
            }
            jQuery(".gm-style > div > a").removeAttr("href").removeAttr("title");
        }
    }
};
