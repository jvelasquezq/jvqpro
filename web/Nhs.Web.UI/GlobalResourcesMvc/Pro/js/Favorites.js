﻿NHS.Scripts.Favorites = function (params) {
    this.params = params;
};

NHS.Scripts.Favorites.prototype =
{
    initialize: function () {
        var self = this;
        $jq("#pro_SelectAllHome").change(function () {
            $jq(".pro_SavedHomeCheckBox").prop('checked', $jq("#pro_SelectAllHome").is(':checked'));
        });

        $jq("#pro_SelectAllCom").change(function () {
            $jq(".pro_SavedCommCheckBox").prop('checked', $jq("#pro_SelectAllCom").is(':checked'));
        });

        $jq(self.params.homeSaveDelete).click(function () {
            $jq("#nhs_Loading").show();
            var deletes = $jq(self.params.homeSave + ":checked");
            var url = !self.params.isClientDetailPage ? "/myaccounnt/deletefavoritehome" : "/procrm/deleteclientfavorite";
            $jq.each(deletes, function () {
                $jq("#nhs_Loading").show();
                var id = $jq(this).attr("id").split('_');
                var isSpec = id[1];
                var listingId = id[0];
                var data = !self.params.isClientDetailPage ? { isSpec: isSpec, listingId: listingId} : { propertyId: listingId, clientId: self.params.clientId };
                $jq.ajax({
                    type: "Post",
                    url: self.params.url + url,
                    data: data,
                    success: function (result) {
                        if (result.valid) {
                            $jq("#pro_HomeRow_" + result.id).remove();
                            $jq("#pro_homefavorite").html("Homes: " + $jq(".nhs_HomeResultsRow").length + " saved");
                            if ($jq(".nhs_HomeResultsRow").length == 0) {
                                $jq("#pro_SavedHomeRemove, #nhs_TabsRemoveHome").remove();
                            }
                        }
                        $jq("#nhs_Loading").hide();
                    }
                });
            });
            $jq("#nhs_Loading").hide();
        });


        $jq(self.params.commSaveDelete).click(function () {
            $jq("#nhs_Loading").show();
            var deletes = $jq(self.params.commSave + ":checked");
            var url = !self.params.isClientDetailPage ? "/myaccounnt/deletefavoritecomm" : "/procrm/deleteclientfavorite";
            $jq.each(deletes, function () {
                $jq("#nhs_Loading").show();
                var id = $jq(this).attr("id").split('_');
                var builderId = id[1];
                var communityId = id[0];
                var data = !self.params.isClientDetailPage ? { builderId: builderId, communityId: communityId} : { propertyId: communityId, clientId: self.params.clientId };
                $jq.ajax({
                    type: "Post",
                    url: url,
                    data: data,
                    success: function (result) {
                        if (result.valid) {
                            $jq("#pro_CommRow_" + result.id).remove();
                            $jq("#pro_commfavorite").html("Communities: " + $jq(".nhs_CommResultsRow").length + " saved");
                            if ($jq(".nhs_CommResultsRow").length == 0) {
                                $jq("#pro_SavedCommRemove, #nhs_TabsRemoveCom").remove();
                            }
                        }
                        $jq("#nhs_Loading").hide();
                    }
                });
            });
            $jq("#nhs_Loading").hide();
        });

    }
};
