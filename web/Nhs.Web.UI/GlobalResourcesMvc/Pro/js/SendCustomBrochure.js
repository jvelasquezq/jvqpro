﻿NHS.Scripts.SendCustomBrochure = function (parameters) {
    this._parameters = parameters;
};


NHS.Scripts.SendCustomBrochure.prototype =
{
    initialize: function () {
        var self = this;

        if (parent.AjaxForm) {
            var width = 893;
            var height = 355;            
            var div = $jq("table.top.table_window td.dialog_n div", parent.document.body);                        
            div.html("Send Customized Brochure to Clients");
            div.css("background-color", "#004a7c");
            div.css("color", "#ffffff");
            div.css("font-size", "22px");
            div.css("font-weight", "400");
            div.css("padding", "5px 5px 20px");
            div.css("text-align", "left");
            div.css("width", "887px");
            parent.AjaxForm.SetWindowSize(width, height);
        }

        $jq(".k-window-titlebar a.k-window-action").click(function () {
            $jq.googlepush('Send Brochure', 'Modal', 'Close');
        });

        $jq("#pro_TableRecipients").on("click", ".pro_AutoCompleteClientName input, .pro_AutoCompleteClientEmail input", function () {
            var autocomplete = $jq(this).data("kendoAutoComplete");
            if (autocomplete.value().length == 0)
                autocomplete.search(" ");
        });

        $jq("#pro_ClearNames").click(function () {
            $jq(".pro_AutoCompleteClean").val("");
            $jq(".pro_AutoCompleteCleanNumber").val("0");
        });

        $jq("#pro_TableRecipients").on("click", ".pro_DeleteClientNameEmail", function () {
            $jq(this).parent().parent().remove();
            $jq("#pro_TableRecipients tr").each(function (index) {
                $jq(this).find(".pro_AutoCompleteCleanNumber").attr("name", "ClientsEmails[" + index + "].ClientId").attr("id", "ClientsEmails_" + index + "__ClientId");
                $jq(this).find(".pro_AutoCompleteClientName").attr("name", "ClientsEmails[" + index + "].Name").attr("id", "ClientsEmails_" + index + "__Name");
                $jq(this).find(".pro_AutoCompleteClientEmail").attr("name", "ClientsEmails[" + index + "].Email").attr("id", "ClientsEmails_" + index + "__Email");
            });
        });

        $jq("#pro_AddRecipients").click(function () {
            $jq.googlepush('Send Brochure', 'Modal', 'Add Another Recipient');
            var count = $jq("#pro_TableRecipients tr").length;
            var $btn = jQuery(this);
            $btn.hide();
            var template = kendo.template(self._parameters.addAnotherRecipientTemplate);
            var templateData = { index: count };
            $jq("#pro_TableRecipients").append(template(templateData));
            automComplete("#ClientsEmails_" + count + "__Name", "#ClientsEmails_" + count + "__Email");
            tb_center();
            $btn.show();           
        });

        automComplete(".pro_AutoCompleteClientName", ".pro_AutoCompleteClientEmail");
        var clientId = 0;
        $jq("input.pro_AutoCompleteClientEmail").on("click", function () {
            var id = $jq(this).attr("id");

            id = id.replace("__Email", "__Name");
            if ($jq("#" + id).val().length == 0) {
                clientId = 0;
                changeValue(id, 0);
            } else {
                id = id.replace("Name", "ClientId");
                clientId = $jq("#" + id).val();
            }
        });


        function automComplete(name, email) {
            $jq(name).kendoAutoComplete({
                "select": function (e) {
                    var dataItem = this.dataItem(e.item.index());
                    var id = $jq(this.element).attr("id");
                    changeValue(id, dataItem.value);
                    changeEmail(id, dataItem.emails);
                },
                "dataBound": function (e) {
                    var id = $jq(this.element).attr("id");
                    changeValue(id, "0");
                },
                placeholder: "Enter a name...",
                "close":
                    function () {

                        var id = $jq(this.element).attr("id");
                        var data = this.dataSource._data;
                        var value = this.value();
                        $jq.each(data, function () {
                            if ($jq.trim(this.label) == $jq.trim(value)) {
                                changeValue(id, this.value);
                                return;
                            }
                        });
                    },
                "dataSource": {
                    "transport": {
                        "read": {
                            "url": self._parameters.clientsautocomplete,
                            "data": function (e) {
                                return {
                                    text: e.filter.filters[0].value,
                                    isSendBrochure: true
                                };
                            }
                        }
                    },
                    "serverFiltering": true
                },
                "dataTextField": "label",
                "template": "<strong>${ data.label }</strong><span>${ data.relatedName }</span>",
                "minLength": 1
            });

            $jq(email).kendoAutoComplete({
                "select": function (e) {
                    var dataItem = this.dataItem(e.item.index());
                    var id = $jq(this.element).attr("id");
                    changeValue2(id, dataItem.value, dataItem.name);
                },
                "separator": "; ",
                "placeholder": "Enter an email...",
                "close":
                    function () {

                        var id = $jq(this.element).attr("id");
                        var data = this.dataSource._data;
                        var value = $jq.trim(this.value());
                        //                        if (value.lastIndexOf(';') == value.length - 1)
                        //                            value = value.substr(0, value.length - 1);
                        this.value(value);
                        $jq.each(data, function () {
                            if ($jq.trim(this.label) == value) {
                                changeValue2(id, this.value, this.name);
                                return;
                            }
                        });
                    },
                "dataSource": {
                    "transport": {
                        "read": {
                            "url": self._parameters.clientsautocompleteemail,
                            "data": function (e) {
                                return {
                                    text: e.filter.filters[0].value,
                                    clientId: clientId
                                };
                            }
                        }
                    },
                    "serverFiltering": true
                },
                "dataTextField": "emails",
                "template": "${ data.label }",
                "minLength": 1
            });
        }

        function changeEmail(id, emails) {
            id = id.replace("Name", "Email");
            $jq("#" + id).val(emails);
        }

        function changeValue2(id, value, name) {
            id = id.replace("__Email", "__Name");
            if ($jq("#" + id).val().length == 0) {
                $jq("#" + id).val(name);
                clientId = value;

                id = id.replace("Name", "ClientId");
                $jq("#" + id).val(value);
            }
        }

        function changeValue(id, value) {
            id = id.replace("Name", "ClientId");
            $jq("#" + id).val(value);
            clientId = value;
        }

        $jq("#pro_sendBrochure").click(function (event) {
            event.preventDefault();
            $jq.googlepush('Send Brochure', 'Modal', 'Send Brochure');
            jQuery.SetDataLayerPair('directLead');
            $jq(this).hide();
            $jq("#pro_SendBrochureFake").show();

            if (self.ValidateFormNamesAndEmails() === false) {
                return false;
            }

            if (self._parameters.HavAgentPhotoOrAgencyLogo) {
                return self.Send();
            } else {
                $jq("#pro_Overlay").show();
                $jq("#pro_Confirm").show();
                return false;
            }

        });

        $jq("#pro_yes").click(function () {
            self.ManageMyTemplate.call(self);
            $jq("#pro_Overlay").hide();
            $jq("#pro_Confirm").hide();
            self.btn();
        });
        $jq("#pro_no").click(function (e) {
            e.preventDefault();
            $jq("#pro_Overlay").hide();
            $jq("#pro_Confirm").hide();
            self.Send.call(self);
        });

        $jq("#pro_ManageMyTemplate").click(function () {
            self.ManageMyTemplate.call(self);
        });

        $jq("#pro_Crm_CancelWindowLink").click(function (e) {
            $jq.googlepush('Send Brochure', 'Modal', 'Cancel');
            e.preventDefault();

            if (parent.AjaxForm) {
                parent.AjaxForm.CloseCurrentWindow();
            } else {
                tb_remove();
            }
        });
    },

    ValidateFormNamesAndEmails: function () {
        var self = this;
        var valid = true;
        var hasInfo = false;
        var table = $jq("#pro_TableRecipients");
        var tr = table.find("tr");

        for (var i = 0; i < tr.length; i++) {
            var control = $jq(tr[i]);

            var clientname = control.find("input.pro_AutoCompleteClientName");
            var emailaddress = control.find("input.pro_AutoCompleteClientEmail");

            clientname.removeAttr("style");
            emailaddress.removeAttr("style");

            if (clientname.val().indexOf('Enter a name...') != -1)
                clientname.val('');

            if (emailaddress.val().indexOf('Enter an email...') != -1)
                emailaddress.val('');

            if ((clientname.val().length > 0 || emailaddress.val().length > 0)) {
                if (!ValidFullNameWithNumberLastName(clientname.val())) {
                    valid = false;
                    clientname.attr("style", "border: 1px solid #FF0000");
                } else {
                    hasInfo = true;
                }

                self.autoFormatTextEmails(emailaddress);

                if (!self.isValidEmailAddress(emailaddress.val())) {
                    valid = false;
                    emailaddress.attr("style", "border: 1px solid #FF0000");
                } else {
                    hasInfo = true;
                }

            }
        }

        if (!valid || !hasInfo) {           
            alert('Please provide first, last name and a valid email or emails.');
            self.btn();
            return false;
        } else {
            return true;
        }
    },

    Send: function () {
        var self = this;                
            var data = $jq(".pro_SendBrochureTable input").serialize();
            $jq.ajax({
                url: self._parameters.Url,
                type: "POST",
                cache: false,
                data: data,
                success: function (html) {
                    if (typeof html === 'string') {
                        $jq(".k-window-titlebar a.k-window-action").off("click");
                        $jq("#pro_AjaxUpdateSendCustomBrochure").html(html);
                        tb_center();
                    } else {
                        eval(html.google);
                    }
                }
            });
        
        return true;
    },

    ManageMyTemplate: function () {

        var left = window.screenX + (window.outerWidth / 2) - (780 / 2);
        var top = window.screenY + (window.outerHeight / 2) - (480 / 2);
        var returnValue = window.open(
            this._parameters.BrochureUrl, '',
            'resizable=1,height=480,width=780,scroll=no,status=no,center=yes' + ', top=' + top + ', left=' + left, null);

        returnValue.focus();
        //TODO:: this must be re-factor in the future
        this._parameters.HavAgentPhotoOrAgencyLogo = true;
    },

    btn: function () {
        $jq("#pro_sendBrochure").show();
        $jq("#pro_SendBrochureFake").hide();
    },
    isValidEmailAddress: function (emailAddress) {

        if (emailAddress.length == 0)
            return false;

        var valid = true;
        var pattern = new RegExp(/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/i);
        $jq.each(emailAddress.split(";"), function (index, value) {
            if (!pattern.test(value.trim()))
                valid = false;
        });
        return valid;
    },
    isValidNameLastName: function (name) {
        var pattern = new RegExp(/^[a-zA-Z]+\s[a-z ,.1-9''-]+$/i);
        return pattern.test(name);

    },
    autoFormatTextEmails: function (control) {
        var value = control.val();
        var format = value.indexOf(";;") != -1;
        while (format) {
            value = value.replace(/\;;/g, ';');
            format = value.indexOf(";;") != -1;
        }
        if (value.lastIndexOf(';') == value.length - 1)
            value = value.substr(0, value.length - 1);
        control.val(value);
    }
};

