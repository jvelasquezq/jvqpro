﻿NHS.Scripts.BrochureTab = function () {
};

NHS.Scripts.BrochureTab.prototype =
{
    initialize: function () {

        $jq("#pro_AgentPhotoImg a").click(function () {
            $jq("#pro_AgentPhotoImg").remove();
            $jq("#pro_AgentPhotoFile").show();
            $jq("#AgentPhotoPath").val('');
        });


        $jq("#pro_AgencyLogoImg a").click(function () {
            $jq("#pro_AgencyLogoImg").remove();
            $jq("#pro_AgencyLogoFile").show();
            $jq("#AgencyLogoPath").val('');
        });
    }
};
