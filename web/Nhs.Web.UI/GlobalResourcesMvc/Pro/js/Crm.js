﻿NHS.Scripts.Crm = function (parameters) {
    this._parameters = parameters;
};


NHS.Scripts.Crm.prototype =
{
    initialize: function () {
        var self = this;
        var url = self._parameters.url;
        clientsStatus = self._parameters.clientsStatus;

        if (!self._parameters.SelectFavoriteTab) {
            jQuery("#pro_FavoritesTab").click(function () {
                if (jQuery("#pro_FavoritesTabInfo").length == 1 && jQuery("#pro_FavoritesTabInfo").html().length < 3) {
                    jQuery('#pro_FavoritesTabInfo').load(url + "/procrm/myfavoritestab", function () {
                        jQuery(".nhs_Loading").hide();
                    });
                }
            });
        }

        tb_initLive("a.thickbox");

        jQuery(".nhs_Tabs a.pro_tabNavitacion").live("click", function () {
            var control = jQuery(this);
            jQuery('.tap').hide();
            jQuery(".nhs_Tabs li").removeClass("nhs_Selected");
            jQuery(".nhs_Tabs a.pro_tabNavitacion").removeClass("active");
            jQuery('#' + control.attr("showTap")).show();
            control.addClass("active");
            jQuery(control.parent()).addClass("nhs_Selected");
        });

        jQuery("#pro_ShowInactiveClients").live("click", function () {
            jQuery.googlepush('My Clients', 'Clients', 'Show Inactive Clients');
            jQuery("#nhs_Loading").show();
            clientsStatus = 0;
            var grid = jQuery('#pro_CRM_ClientsTable').data('kendoGrid');

            grid.dataSource.page(1);
            grid.dataSource.read({ clientsStatus: 0 });
            jQuery("#nhs_Loading").hide();
            jQuery("#pro_ShowInactiveClients").hide();
            jQuery("#pro_ShowActiveClients").show();
        });

        jQuery("#pro_ShowActiveClients").live("click", function () {
            jQuery.googlepush('My Clients', 'Clients', 'Show Active Clients');
            clientsStatus = 1;
            var grid = jQuery('#pro_CRM_ClientsTable').data('kendoGrid');
            grid.dataSource.page(1);
            grid.dataSource.read({ clientsStatus: 1 });
            jQuery("#nhs_Loading").hide();
            jQuery("#pro_ShowInactiveClients").show();
            jQuery("#pro_ShowActiveClients").hide();
        });
        //this.initializeAddEditClient();

        jQuery("#pro_ClientStatusActiveDetail,#pro_ClientStatusInactiveDetail").live("click", function () {
            self.UpdateStatus.call(self, this);
        });

        jQuery("#pro_ClientStatusActiveDetail").live("click", function () {
            jQuery.googlepush('My Clients (Contact)', 'Topnav', 'Active');
            jQuery(this).attr("disabled", "disabled");
            jQuery("#pro_ClientStatusInactiveDetail").removeAttr("disabled");
        });

        jQuery("#pro_ClientStatusInactiveDetail").live("click", function () {
            jQuery.googlepush('My Clients (Contact)', 'Topnav', 'Inactive');
            jQuery(this).attr("disabled", "disabled");
            jQuery("#pro_ClientStatusActiveDetail").removeAttr("disabled");
        });

        jQuery("#pro_CRM_DeleteClient").live("click", function () {
            jQuery.googlepush('My Clients (Contact)', 'Topnav', 'Delete');
            var r = confirm("Are you sure you want to delete this client?");
            if (r == true) {
                self.UpdateStatus.call(self, this, function () {
                    window.location = url + "/myaccount?tab=myclient";
                });
                jQuery("#pro_ClientStatusActiveDetail,#pro_ClientStatusInactiveDetail").attr("disabled", "disabled");
            }
        });

        jQuery(".pro_TooltipShowMore").live("click", function () {
            var html = jQuery(this).html();
            if (html.indexOf("Show more") > -1) {
                jQuery(this).parent().find(".pro_TooltipMore").show();
                jQuery(this).html("Show less >");
            } else {
                jQuery(this).parent().find(".pro_TooltipMore").hide();
                jQuery(this).html("Show more >");
            }
        });

    },

    UpdateStatus: function (control, action) {
        var valueStatus;
        if (jQuery(control).val() == "Active") {
            valueStatus = 1;
        } else if (jQuery(control).val() == "Inactive") {
            valueStatus = 0;
        } else {
            valueStatus = 2;
        }
        var data = {
            clientId: this._parameters.clientId,
            status: valueStatus
        };
        jQuery("#nhs_Loading").show();
        jQuery.ajax({
            url: this._parameters.url + "/procrm/changestatusclient",
            data: data,
            cache: false
        }).done(function () {
            if (action) {
                action();
            }
            jQuery("#nhs_Loading").hide();
        });
    },

    DeleteTask: function (task) {
        var data = {
            clientId: this._parameters.clientId,
            taskId: task
        };
        window.parent.tb_remove();
        jQuery("#nhs_Loading").show();

        jQuery.ajax({
            url: this._parameters.url + "/procrm/deletetask",
            data: data,
            cache: false
        }).done(function () {
            jQuery('#pro_task_' + task).remove();
            jQuery("#nhs_Loading").hide();
        });
    },

    initializeAddEditClient: function () {

        jQuery(".k-window-titlebar a.k-window-action").click(function () {
            jQuery.googlepush('Add/Edit Client', 'Modal', 'Close');
        });

        var self = this;
        var table = jQuery("#pro_EmailList");
        var tr = table.find("div");
        var first;
        if(typeof tr[0] !== "undefined")
            first = tr[0].cloneNode(true);

        jQuery(first).find('select option').removeAttr('selected');
        jQuery(first).find("input").val("");
        jQuery(first).find("label").html(jQuery(first).find("label").html().replace("*", ""));

        jQuery("#Phone2").mask("(999) 999-9999");
        jQuery("#Phone1").mask("(999) 999-9999");

        jQuery("#pro_AddMoreEmails").click(function (event) {
            var ev = event ? event : window.event;
            ev.cancelBubble = true;
            ev.returnValue = false;
            if (ev.stopPropagation) ev.stopPropagation();
            if (ev.preventDefault) ev.preventDefault();

            renameNode();

            var div = jQuery(first.cloneNode(true));
            div.find("input").attr("name", "EmailList[" + jQuery("#pro_EmailList div").length + "].Email").removeAttr("id");
            div.find("select").attr("name", "EmailList[" + jQuery("#pro_EmailList div").length + "].EmailType").removeAttr("id");

            div.find(".pro_DeleteEmails").click(function () {
                jQuery(this.parentNode.parentNode).remove();
                renameNode();
            });

            var html = jQuery(div);
            table.append(html);
        });

        jQuery(".pro_DeleteEmails").click(function () {
            jQuery(this.parentNode.parentNode).remove();
            renameNode();
        });

        jQuery("#btn_save").click('click', function () {
            jQuery.googlepush('Add/Edit Client', 'Modal', 'Save');
            jQuery("#btn_saveFake").show();
            jQuery("#btn_save").hide();

            var valid = self.Validate.call(self);
            if (!valid) {
                jQuery("#btn_saveFake").hide();
                jQuery("#btn_save").show();
            }
            return valid;
        });

        function renameNode() {
            jQuery("#pro_EmailList div").each(function (index) {
                jQuery(this).find("input").attr("name", "EmailList[" + index + "].Email").removeAttr("id");
                jQuery(this).find("select").attr("name", "EmailList[" + index + "].EmailType").removeAttr("id");
            });
        }

        jQuery("#pro_ClientStatusActive").click(function () {
            jQuery.googlepush('Add/Edit Client', 'Modal', 'Active');
            jQuery(this).attr("disabled", "disabled");
            jQuery("#pro_ClientStatusInactive").removeAttr("disabled");
            jQuery("#Status").val(1);
        });

        jQuery("#pro_ClientStatusInactive").click(function () {
            jQuery.googlepush('Add/Edit Client', 'Modal', 'Inactive');
            jQuery(this).attr("disabled", "disabled");
            jQuery("#pro_ClientStatusActive").removeAttr("disabled");
            jQuery("#Status").val(0);
        });
    },

    Validate: function () {
        var menssage = "";
        var valid = true;
        var firstName = jQuery("#pro_CRM_ClientInfoFields").find("#FirstName");
        var lastName = jQuery("#pro_CRM_ClientInfoFields").find("#LastName");

        firstName.removeAttr("style");
        lastName.removeAttr("style");

        var pattern = new RegExp(/^[a-zA-Z,.0-9''-]+$/i);
        var firstValid = true;
        var lastValid = true;
        var emailValid= true;

        if (firstName.val().length == 0) {
            valid = false;
            firstName.attr("style", "border: 1px solid #FF0000");
            menssage += "First Name Required\n";
            firstValid = false;
        }

        if (!pattern.test(firstName.val()) && firstValid) {
            valid = false;
            firstName.attr("style", "border: 1px solid #FF0000");
            menssage += "First name has invalid characters or contains spaces\n";
        }

        if (lastName.val().length == 0) {
            valid = false;
            lastName.attr("style", "border: 1px solid #FF0000");
            menssage += "Last Name Required\n";
            lastValid = false;
        }

        if (pattern.test(lastName.val()) == false && lastValid) {
            valid = false;
            lastName.attr("style", "border: 1px solid #FF0000");
            menssage += "Last name has invalid characters or contains spaces\n";
        }

        var emailmessage = "";
        var selectmessage = "";
        var duplicateEmail = "";
        var patternEmail = new RegExp(/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/i);

        var uniqueValues = {};

        jQuery("#pro_EmailList div").find("input").removeAttr("style");
        jQuery("#pro_EmailList div").find("select").removeAttr("style");

        var txb = jQuery(jQuery("#pro_EmailList div").get(0)).find("input");

        if (txb.val().length === 0) {
            valid = false;
            txb.attr("style", "border: 1px solid #FF0000");
            menssage += "Email Required\n";
            emailValid = false;
        }

        if (!patternEmail.test(txb.val()) && emailValid) {
            valid = false;
            txb.attr("style", "border: 1px solid #FF0000");
            emailmessage = "Invalid email format\n";
        }

        jQuery("#pro_EmailList div").each(function () {

            var txb = jQuery(this).find("input");

            if (!uniqueValues[txb.val()]) {
                uniqueValues[txb.val()] = true;
            } else {
                txb.attr("style", "border: 1px solid #FF0000");
                duplicateEmail = "The email is duplicate\n";
            }

            if (txb.val().length > 0 && !patternEmail.test(txb.val())) {
                valid = false;
                txb.attr("style", "border: 1px solid #FF0000");
                emailmessage = "Invalid email format\n";
            }
        });

        menssage += duplicateEmail;
        menssage += emailmessage;
        menssage += selectmessage;

        jQuery("#Phone1").removeAttr("style");
        jQuery("#Phone2").removeAttr("style");
        var phone1 = jQuery("#Phone1").val() == "(___) ___-____" ? "" : jQuery("#Phone1").val();
        var phone2 = jQuery("#Phone2").val() == "(___) ___-____" ? "" : jQuery("#Phone2").val();
        pattern = new RegExp(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/i);

        if (phone1.length > 0) {
            if (!pattern.test(phone1)) {
                valid = false;
                jQuery("#Phone1").attr("style", "border: 1px solid #FF0000");
                menssage += "Please enter a valid Phone 1 Number\n";
            }
        }

        if (phone2.length > 0) {
            if (!pattern.test(phone2)) {
                valid = false;
                jQuery("#Phone2").attr("style", "border: 1px solid #FF0000");
                menssage += "Please enter a valid Phone 2 Number\n";
            }
        }

        if (!valid)
            alert(menssage);
        return valid;
    },

    UpClients: function () {
        window.parent.tb_remove();
        var grid = jQuery('#pro_CRM_ClientsTable').data('kendoGrid');
        grid.dataSource.read();
    },

    UpClientsInfo: function (url) {
        window.parent.tb_remove();
        jQuery("#nhs_Loading").show();
        jQuery.ajax({
            url: url,
            cache: false
        }).done(function (html) {
            if (typeof html == "string") {
                jQuery("#pro_CRM_DetailsClient").html(html);
            }
            jQuery("#nhs_Loading").hide();
        });
    },

    InsertUpdateTask: function () {
        var grid = jQuery('#pro_CRM_ActivitiesTable,#pro_CRM_ClientsTable').data('kendoGrid');
        grid.dataSource.read();
        tb_remove();
    },

    initializeAddEditTask: function (parameters) {
        var self = this;
        jQuery("#pro_RemoveTask").click(function () {
            jQuery.googlepush('Add/Edit Task', 'Modal', 'Delete');
            var r = confirm("Are you sure you want to delete this activity?");
            if (r === true) {
                jQuery.ajax({
                    url: parameters.deleteTask.replace("{clientId}", parameters.clientId).replace("{taskId}", parameters.taskId),
                    success: function (success) {
                        if (success) {
                            self.InsertUpdateTask();
                        } else {
                            alert('Invalid task to delete');
                            tb_remove();
                        }
                    }
                });
            }
        });


        jQuery(".k-window-titlebar a.k-window-action").click(function () {
            jQuery.googlepush('Add/Edit Task', 'Modal', 'Close');
        });

        jQuery("#_ProCrmTaskTypes_Email").change(function () {
            if (jQuery(this).is(":checked"))
                jQuery.googlepush('Add/Edit Task', 'Modal', 'Email');
        });

        jQuery("#_ProCrmTaskTypes_Call").change(function () {
            if (jQuery(this).is(":checked"))
                jQuery.googlepush('Add/Edit Task', 'Modal', 'Call');
        });

        jQuery("#_ProCrmTaskTypes_Mail").change(function () {
            if (jQuery(this).is(":checked"))
                jQuery.googlepush('Add/Edit Task', 'Modal', 'Mail');
        });

        jQuery("#_ProCrmTaskTypes_Appointment").change(function () {
            if (jQuery(this).is(":checked"))
                jQuery.googlepush('Add/Edit Task', 'Modal', 'Appointment');
        });

        jQuery("#_ProCrmTaskTypes_Other").change(function () {
            if (jQuery(this).is(":checked"))
                jQuery.googlepush('Add/Edit Task', 'Modal', 'Other');
        });

        var table = jQuery("#pro_taskListModal");
        var tr = table.find("div");
        var first = tr[0].cloneNode(true);
        jQuery(first).find("input").val("");

        /*jQuery('#DueDate').datetimepicker({
        altField: "#DueTime",
        altFieldTimeOnly: true,
        timeText: '',
        timeFormat: "hh:mm tt",
        altTimeFormat: "hh:mm tt",
        pickerTimeFormat: "HH:mm ",
        showOn: "both",
        buttonImage: parameters.calendarImage,
        buttonImageOnly: true,
        alwaysSetTime: true,
        hour: new Date().getHours(),
        minute: new Date().getMinutes(),
        onSelect: function (datetimeText, datepickerInstance) {

        var timeSelected = datetimeText;
        var militaryHour = timeSelected.slice(-2);
        if (militaryHour == 'pm') {
        datepickerInstance.amNames = ['pm'];
        jQuery('#PmTime').click();

        } else {
        datepickerInstance.amNames = ['am'];
        jQuery('#AmTime').click();
        }
        }
        }); //*/

        jQuery('#DueTimeSpan').mask('99:99').keypress(function (key) {
            //console.debug(key.charCode);
        });

        jQuery('#AmTime').click(function () {
            //            var currentHourSelected = jQuery("#DueTime").val();
            //            var index = currentHourSelected.lastIndexOf(' ');
            //            var militaryHour = currentHourSelected;
            //            if (index != -1)
            //                militaryHour = currentHourSelected.substring(0, index);
            //            militaryHour += ' am';
            //            jQuery("#DueTime").val(militaryHour);
            jQuery('#DueAmPm').val('AM');
            jQuery('#PmTime').removeClass("pro_CRM_Selected");
            jQuery(this).addClass("pro_CRM_Selected");
        });

        jQuery('#PmTime').click(function () {
            //            var currentHourSelected = jQuery("#DueTime").val();
            //            var index = currentHourSelected.lastIndexOf(' ');
            //            var militaryHour = currentHourSelected;
            //            if (index != -1)
            //                militaryHour = currentHourSelected.substring(0, index);
            //            militaryHour += ' pm';
            //            jQuery("#DueTime").val(militaryHour);
            jQuery('#DueAmPm').val('PM');
            jQuery('#AmTime').removeClass("pro_CRM_Selected");
            jQuery(this).addClass("pro_CRM_Selected");
        });

        jQuery('#addListing').click(function (event) {
            jQuery.googlepush('Add/Edit Task', 'Modal', 'Add Listing');
            var ev = event ? event : window.event;
            ev.cancelBubble = true;
            ev.returnValue = false;
            if (ev.stopPropagation) ev.stopPropagation();
            if (ev.preventDefault) ev.preventDefault();

            renameNode();
            var div = jQuery(first.cloneNode(true));
            var index = jQuery("#pro_taskListModal div").length;
            div.find("input.pro_CommSearchBox").attr("name", "ListingList[" + index + "].Name").attr("id", "ListingList_" + index + "__Name");
            div.find("input.pro_hidevalue").attr("name", "ListingList[" + index + "].Id").attr("id", "ListingList_" + index + "__Id");
            div.find("input.pro_hidevalueDelete").attr("name", "ListingList[" + index + "].Delete").attr("id", "ListingList_" + index + "__Delete");
            div.find("input.pro_hidevalueTaskId").attr("name", "ListingList[" + index + "].TaskListingId").attr("id", "ListingList_" + index + "__TaskListingId");

            div.find(".pro_DeleteTask").click(function () {
                remove(this);
            });

            var html = jQuery(div);
            table.append(html);

            initAutoComplete(div.find("input.pro_CommSearchBox"));
        });

        jQuery(".pro_DeleteTask").click(function () {
            remove(this);
        });


        function remove(control) {
            if (parseInt(jQuery(control.parentNode).find(".pro_hidevalueTaskId").val()) > 0) {
                jQuery(control.parentNode.parentNode).hide();
                jQuery(control.parentNode).find(".pro_hidevalueDelete").val(true);
            } else {
                jQuery(control.parentNode.parentNode).remove();
                renameNode();
            }
        }


        function renameNode() {
            jQuery("#pro_taskListModal div").each(function (index) {
                jQuery(this).find("input.pro_CommSearchBox").attr("name", "ListingList[" + index + "].Name").attr("id", "ListingList_" + index + "__Name");
                jQuery(this).find("input.pro_hidevalue").attr("name", "ListingList[" + index + "].Id").attr("id", "ListingList_" + index + "__Id");
                jQuery(this).find("input.pro_hidevalueDelete").attr("name", "ListingList[" + index + "].Delete").attr("id", "ListingList_" + index + "__Delete");
                jQuery(this).find("input.pro_hidevalueTaskId").attr("name", "ListingList[" + index + "].TaskListingId").attr("id", "ListingList_" + index + "__TaskListingId");
            });
        }

        initAutoComplete('.pro_CommSearchBox');

        function initAutoComplete(name) {
            jQuery(name).kendoAutoComplete({
                "select": function (e) {
                    var dataItem = this.dataItem(e.item.index());
                    var id = jQuery(this.element).attr("id");
                    changeValue(id, dataItem.value);
                },
                "dataBound": function (e) {
                    var id = jQuery(this.element).attr("id");
                    changeValue(id, "0");
                },
                "close":
                    function () {

                        var id = jQuery(this.element).attr("id");
                        var data = this.dataSource._data;
                        var clientId = -1;
                        var value = this.value();
                        jQuery.each(data, function () {
                            if (jQuery.trim(this.label) == jQuery.trim(value)) {
                                clientId = this.value;
                                changeValue(id, clientId);
                                return;
                            }
                        });
                    },
                "dataSource": {
                    "transport": {
                        "prefix": "",
                        "read": {
                            "url": parameters.listingAutoComplete,
                            "data": function (e) {
                                return {
                                    term: e.filter.filters[0].value
                                };
                            }
                        }
                    },
                    "serverFiltering": true,
                    "filter": [],
                    "schema": { "errors": "Errors" }
                },
                "dataTextField": "label",
                "minLength": 1
            });
        }

        function changeValue(id, value) {
            id = id.replace("Name", "Id");
            jQuery("#" + id).val(value);
        }
    },

    ShowLoadingDiv: function () {

        if (global_showCache) {
            jQuery('#pro_ClientTasks tr').each(function () {
                jQuery(this).show();
            });
        } else {
            jQuery('#nhs_Loading').show();
        }

        var bkClick = jQuery('#showAllActLink').attr('onclick');
        var bkHref = jQuery('#showAllActLink').attr('href');
        jQuery('.pro_CRM_ClientsControl').append('<input type="hidden" id="bkOnclick" value="' + bkClick + '" />');
        jQuery('.pro_CRM_ClientsControl').append('<input type="hidden" id="bkHref" value="' + bkHref + '" />');
        jQuery('#showAllActLink').attr('href', 'javascript:crm.ShowLessActivities()');
        jQuery('#showAllActLink').attr('onclick', '');
        jQuery('#showAllActLink').html('See less Activities >');

        if (global_showCache) {
            return false;
        }
    },

    HideLoadingDiv: function () {
        jQuery('#nhs_Loading').hide();
        global_showCache = true;
    },

    ShowLessActivities: function () {
        var count = 0;
        jQuery('#pro_ClientTasks tr').each(function () {

            if (count > 4) {
                jQuery(this).hide();
            }

            count++;
        });
        jQuery('#showAllActLink').attr('onclick', jQuery('#bkOnclick').val());
        jQuery('#showAllActLink').attr('href', jQuery('#bkHref').val());
        jQuery('#showAllActLink').html('See all Activities >');
    },

    initializeAddClientToActionModal: function () {
        var self = this;

        var count = jQuery(".pro_ClientName").length;
        if (count > 5) {
            jQuery("#pro_ClientNameList").addClass("pro_ClientNameListScroll");
        }

        jQuery("#pro_ClearNames").click(function () {
            jQuery(".pro_AutoCompleteClean").val("");
            jQuery(".pro_AutoCompleteCleanNumber").val("0");
        });
        jQuery(".pro_DeleteClientName").live("click", function () {
            jQuery(this).parent().remove();
            jQuery("#pro_ClientNameList .pro_ClientName").each(function (index) {
                jQuery(this).find(".pro_AutoCompleteCleanNumber").attr("name", "Clients[" + index + "].ClientId").attr("id", "Clients_" + index + "__ClientId");
                jQuery(this).find(".pro_AutoCompleteClientName").attr("name", "Clients[" + index + "].Name").attr("id", "Clients_" + index + "__Name");
            });
            jQuery("#pro_ClientNameList").removeClass("pro_ClientNameListScroll");
            if (count > 6) {
                jQuery("#pro_ClientNameList").addClass("pro_ClientNameListScroll");
            }
            count = count - 1;
            tb_center();
        });

        jQuery(".pro_AutoCompleteClientName input").live("click", function () {
            var autocomplete = jQuery(this).data("kendoAutoComplete");
            if (autocomplete.value().length == 0)
                autocomplete.search(" ");
        });

        jQuery("#pro_AddClient").click(function () {

            jQuery("#pro_ClientNameList").removeClass("pro_ClientNameListScroll");
            if (count > 5) {
                jQuery("#pro_ClientNameList").addClass("pro_ClientNameListScroll");
            }
            jQuery.ajax({
                url: self._parameters.autocompletehtmlUrl,
                data: { index: count },
                success: function (html) {
                    jQuery("#pro_ClientNameList").append(html);
                    automComplete("#Clients_" + (jQuery(".pro_ClientName").length - 1) + "__Name");
                    tb_center();
                }
            });
            count = count + 1;
        });

        automComplete(".pro_AutoCompleteClientName");

        function automComplete(name) {
            jQuery(name).kendoAutoComplete({
                "select": function (e) {
                    var dataItem = this.dataItem(e.item.index());
                    var id = jQuery(this.element).attr("id");
                    changeValue(id, dataItem.value);
                },
                "dataBound": function (e) {
                    var id = jQuery(this.element).attr("id");
                    changeValue(id, "0");
                },
                "close":
                    function () {

                        var id = jQuery(this.element).attr("id");
                        var data = this.dataSource._data;
                        var clientId = -1;
                        var value = this.value();
                        jQuery.each(data, function () {
                            if (jQuery.trim(this.label) == jQuery.trim(value)) {
                                clientId = this.value;
                                changeValue(id, clientId);
                                return;
                            }
                        });
                    },
                "dataSource": {
                    "transport": {
                        "prefix": "",
                        "read": {
                            "url": self._parameters.clientsautocomplete,
                            "data": function (e) {
                                return {
                                    text: e.filter.filters[0].value,
                                    isSendBrochure: false
                                };
                            }
                        }
                    },
                    "serverFiltering": true,
                    "filter": [],
                    "schema": { "errors": "Errors" }
                },
                "dataTextField": "label",
                "template": "<strong>${ data.label }</strong><span>${ data.relatedName }</span>",
                "minLength": 1
            });
        }

        function changeValue(id, value) {
            id = id.replace("Name", "ClientId");
            jQuery("#" + id).val(value);
        }

    },
    initializeAddEmailToNewClients: function () {
        var self = this;

        self.CreateStarCheckBox();

        jQuery(".k-window-titlebar a.k-window-action").click(function () {
            jQuery.googlepush('Add Client to List - Comm', 'Modal', 'Close');
        });

        var checkboxes = jQuery(".pro_SelectClient");
        var count = checkboxes.length;

        if (count > 4) {
            jQuery("#pro_ValuesInput").addClass("pro_ValuesScroll");
        }

        jQuery("#pro_AddEmailToNewClients").click(function () {
            var tr = jQuery("#pro_ValuesInput tr");
            var valid = true;
            jQuery("#btnSendMessageFakeAE").hide();
            jQuery.each(tr, function () {
                var checked = jQuery(this).find(".pro_SelectClient").is(':checked');
                var txb = jQuery(this).find(".pro_Email");
                txb.removeAttr("style");
                if (checked) {
                    var pattern = new RegExp(/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/i);
                    if (txb.val().length == 0 || !pattern.test(txb.val())) {
                        valid = false;
                        txb.attr("style", "border: 1px solid #FF0000");
                    }
                }
            });

            if (valid) {
                jQuery.googlepush('Add Client to List - Comm', 'Modal', 'Save New Clients');
                jQuery("#pro_AddEmailToNewClients").hide();
                jQuery('#pro_SendBrochureFakeAE').show();
                var data = jQuery("#pro_ValuesInput input").serialize();
                jQuery.ajax({
                    url: self._parameters.saveUrl,
                    type: "POST",
                    data: data,
                    success: function (html) {
                        if (typeof html === 'string')
                            jQuery("#pro_ModalBody").html(html);
                        else {
                            tb_remove();
                        }
                    }
                });
            } else {
                jQuery("#btnSendMessageFakeAE").show();
                jQuery("#pro_SendBrochureFakeAE").hide();
                jQuery("#pro_AddEmailToNewClients").show();
            }
        });
    },

    initializeSaveToFavorites: function () {
        var self = this;
        var isComm = parseInt(jQuery("#PlanId").val()) == 0 && parseInt(jQuery("#SpecId").val()) == 0;
        var value = isComm ? 'Save Comm to Clients' : 'Save Home to Clients';
        jQuery(".k-window-titlebar a.k-window-action").click(function () {
            jQuery.googlepush(value, 'Modal', 'Close');
        });

        jQuery("#pro_AddClient").click(function () {
            jQuery.googlepush(value, 'Modal', 'Add Another Recipient');

        });

        jQuery("#pro_SendBrochureNo").click(function () {
            jQuery.googlepush(value, 'Modal', 'No');
            tb_remove();
        });

        jQuery("#pro_AddToFavoritesClient").click(function () {
            jQuery.googlepush(value, 'Modal', 'Save New Clients');

            var txbs = jQuery("input.pro_AutoCompleteClientName");

            if (jQuery("input.pro_AutoCompleteClientName").val().length == 0)
                alert("You must enter a client name or save this to your own personal list of favorites.");

            var valid = jQuery("input.pro_AutoCompleteClientName").val().length > 0;

            jQuery("#btnSendMessageFakeAE").hide();
            jQuery.each(txbs, function () {
                var txb = jQuery(this);
                txb.removeAttr("style");
                if (txb.val().length > 0)
                    if (!ValidFullNameWithNumberLastName(txb.val())) {
                        valid = false;
                    }
            });

            if (valid) {
                var data = jQuery("#pro_ValuesInput input").serialize();
                jQuery.ajax({
                    url: self._parameters.saveUrl,
                    type: "POST",
                    data: data,
                    success: function (html) {
                        var propertyId = jQuery("#PropertyId").val();
                        jQuery("#nhs_SaveThisItem, #nhs_NextStepsSaveThisItem, #nhs_SaveThisItem_" + propertyId).addClass("pro_SavedProperty");
                        jQuery("#nhs_SaveThisItem, #nhs_NextStepsSaveThisItem").html("Save to favorites");
                        if (typeof html === 'string') {
                            tb_ChangeTitle("Please add new client email");
                            jQuery("#pro_ModalBody").html(html);
                            tb_center();
                        } else {
                            tb_remove();
                        }
                    }
                });
            } else {
                jQuery("#btnSendMessageFakeAE").show();
                jQuery("#pro_SendBrochureFakeAE").hide();
                jQuery("#pro_AddToFavoritesClient").show();
            }

        });
    },

    SaveToFavorites: function (communityId, builderId, planid, specId, url) {
        var data = { communityId: communityId, builderId: builderId, planid: planid, specId: specId };
        jQuery.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function () {
                var propertyId = jQuery("#PropertyId").val();
                jQuery("#nhs_SaveThisItem, #nhs_NextStepsSaveThisItem, #nhs_SaveThisItem_" + propertyId).addClass("pro_SavedProperty");
                jQuery("#pro_savetofavoritesagent").parent().html('<span class=\"pro_Saved\">Listing saved to your personal list of favorites</span>');
                jQuery("#nhs_SaveThisItem, #nhs_NextStepsSaveThisItem").html("Save to favorites");
                tb_remove();
            }
        });
    },
    initializeContactToBuilder: function () {
        var self = this;

        var name = (self._parameters.RequestAnAppointment ? "_RequestAnAppointment" : "");
        var updateTargetId = self._parameters.UpdateTargetId;
        jQuery(updateTargetId + " " + '#Message').val("");
        if (!self._parameters.RequestAnAppointment) {
            jQuery('#GeneralInquiry').val('true');
            jQuery('#ScheduleAppointment').val('false');
            jQuery('#Message').val('I am interested in information on ...');

            jQuery('#ScheduleAppointmentRadio').click(function () {
                if (jQuery('#Message').val().length == 0 || jQuery('#Message').val() == 'I am interested in information on ...')
                    jQuery('#Message').val('I would like to schedule an appointment on ...');
                jQuery('#GeneralInquiry').val('false');
                jQuery('#ScheduleAppointment').val('true');
            });

            jQuery('#GeneralInquiryRadio').click(function () {
                if (jQuery('#Message').val().length == 0 || jQuery('#Message').val() == 'I would like to schedule an appointment on ...')
                    jQuery('#Message').val('I am interested in information on ...');
                jQuery('#GeneralInquiry').val('true');
                jQuery('#ScheduleAppointment').val('false');
            });
        }
        jQuery("#btnSendMessage" + name).click(function () {
            

            var valid = true;
            var txb = jQuery(updateTargetId + " " + "#Message");
            jQuery(updateTargetId + " " + "#pro_validation_summary" + name).hide();
            txb.removeAttr("style");

            if (txb.val().length == 0) {
                valid = false;
                txb.attr("style", "border: 1px solid #FF0000");
                jQuery("#pro_validation_summary" + name).show();
            }

            if (valid) {
                var data = jQuery(updateTargetId + " " + ".pro_ValuesInputForm input," + updateTargetId + " " + ".pro_ValuesInputForm textarea").serialize();
                var $this = jQuery(this);
                if ($this.hasClass('perform_login')) {
                    // Case when I have to display the sign in Modal
                    // The required attributes are in data properties in the button
                    var url = "/" + $this.data("url") + "?ToPage=ContactBuilder&action=" + self._parameters.contactBuilderAndRequestAppointment + "&UpdateTargetId=" + updateTargetId.substring(1, updateTargetId.length) + "&NextWidth=" + $this.data("nextwidth") + "&NextHeight=" + $this.data("nextheight") + "&" + data + "&width=" + $this.data("width") + "&height=" + $this.data("height");
                    tb_show(false, url);
                    tb_center();
                } else {
                    jQuery(this).hide();
                    jQuery('#btnSendMessageFake' + name).show();

                    // Case when I have to send the message to the builder
                    self.sendMessageToTheBuilder(updateTargetId, self, data, true);    
                }
                
            } else {
                jQuery('#btnSendMessageFake' + name).hide();
                jQuery(this).show();
            }
        });
    },

    sendMessageToTheBuilder: function (updateTargetId, self, data, openModal) {
        jQuery.ajax({
            url: self._parameters.contactToBuilder,
            type: "POST",
            data: data,
            success: function (html) {
                if (typeof html === 'string') {
                    jQuery(updateTargetId).html(html);

                    if (openModal === true) {
                        var url = self._parameters.contactBuilderAndRequestAppointment + "?" + data + "&height=" + self._parameters.height + "&width=" + self._parameters.width;
                        tb_show("Save this activity to a client record?", url);
                        tb_center();
                    }
                }
            }
        });
    },

    initSendMessageCompensationLead: function () {
        var self = this;

        self._parameters.templateAgCompensationLead = kendo.template(jQuery("#templateAgCompensationLead").html());
        self._parameters.templateAgCompensationLeadConfirm = kendo.template(jQuery("#templateAgCompensationLeadConfirmation").html());

        //set the template
        jQuery('#pro_agCompensationLeadFormDivId').html(self._parameters.templateAgCompensationLead);

        jQuery("div#pro_agCompensationLeadFormDivId").on("click", "#pro_agCompensationLeadBtnConfirmId", function () {
            jQuery('#pro_agCompensationLeadFormDivId').html(self._parameters.templateAgCompensationLead);
        });

        jQuery("div#pro_agCompensationLeadFormDivId").on("click", "#pro_agCompensationLeadBtnId", function () {
            var $this = jQuery(this);
            var data = jQuery('#pro_agCompensationLeadFormDivId textarea, #pro_divAgCompensationLead input[type=hidden]').serialize();
            jQuery('#pro_errorAgentCompensation').hide();
            jQuery(this).hide();
            jQuery('#pro_agCompensationLeadBtnFakeId').show();

            var text = jQuery('div#pro_agCompensationLeadFormDivId').find('textarea[name=Message]').val();

            if (typeof text === 'undefined' || text.trim().length == 0) {
                jQuery('#pro_agCompensationLeadBtnFakeId').hide();
                jQuery(this).show();
                jQuery('#pro_errorAgentCompensation').show();
                return;
            }

            if ($this.hasClass('perform_login')) {
                // Case when I have to display the sign in Modal
                // The required attributes are in data properties in the button
                var url = "/" + $this.data("url") + "?ToPage=CompensationLead&action=" + self._parameters.contactToBuilderCompensationLeadUrl + "&NextWidth=" + $this.data("nextwidth") + "&NextHeight=" + $this.data("nextheight") + "&" + data + "&ShowSaveActivityClient=" + self._parameters.showSaveActivityClient + "&width=" + $this.data("width") + "&height=" + $this.data("height");
                tb_show(false, url);
                tb_center();
            } else {
                self.sendMessageCompensationLead(self, data, true, false);    
            }
        });
    },

    sendMessageCompensationLead: function (self, data, showModal, reloadPage) {
        jQuery.ajax({
            url: self._parameters.contactToBuilderCompensationLeadUrl,
            type: "POST",
            data: data,
            success: function (html) {
                if (typeof html === 'object' && html.hasError == false) {
                    jQuery('#pro_agCompensationLeadFormDivId').html(self._parameters.templateAgCompensationLeadConfirm);
                    if (self._parameters.showSaveActivityClient && showModal) {
                        var url = self._parameters.contactToBuilderCompensationLeadUrl + "?" + data + "&height=" + self._parameters.height + "&width=" + self._parameters.width;
                        tb_show("Save this activity to a client record?", url);
                        tb_center();
                    }
                } else {
                    jQuery('#pro_agCompensationLeadBtnFakeId').hide();
                    jQuery(this).show();
                    jQuery.Console.Log(html.ok);
                    jQuery('#pro_errorAgentCompensation').show();
                }

                // Case of NHS Pro when after the sign in I have to reload the page.
                if (reloadPage) {
                    window.parent.tb_remove();
                    window.location.reload();
                }
            }
        });
    },

    initializeReturnToBuilderMessage: function () {
        var self = this;
        var name = (self._parameters.RequestAnAppointment ? "_RequestAnAppointment" : "");
        var updateTargetId = self._parameters.UpdateTargetId;

        jQuery("#lnkBackSendMessage" + name).click(function () {
            var data = jQuery(updateTargetId + " " + ".divConfirmationDefault input").serialize();
            jQuery.ajax({
                url: self._parameters.returnToBuilderMessage,
                type: "POST",
                data: data,
                success: function (html) {
                    if (typeof html === 'string') {
                        jQuery(updateTargetId).html(html);
                        tb_center();
                    }
                }
            });
        });
    },
    CreateStarCheckBox: function () {
        jQuery(".pro_SelectClient").each(function () {
            var checkbox = jQuery(this);
            var id = checkbox.attr("id");
            var parent = jQuery(this).parent().find('span');
            if (!parent)
                jQuery('<label for="' + id + '">Select</label>').insertAfter(checkbox);
        });
    }
};
var global_showCache = false;
