﻿NHS.Scripts.Account = function (params) {
    this.params = params;
};

NHS.Scripts.Account.prototype =
{
    initialize: function () {
        var self = this;
        $jq("#ZipCode").mask("99999", { placeholder: "" });
        var mkName = $jq(this.params.mkbox).val();
        var zipValue = $jq(this.params.zbox).val();
        this.ValidateAndFill.call(self);
        if (!this.IsValidZipCommon(zipValue)) {
            if (mkName != null && mkName != '')
                $jq(this.params.mkbox).val(mkName);
        }
        $jq(this.params.zbox).on("keyup", function () {
            self.ValidateAndFill.call(self);
        });
    },

    iframeCreate: function (url) {
        $jq("#BrochureTab").html('');
        $jq("#BrochureTab").html('<iframe src="' + url + '" width="979" height="1000" frameborder="0"></iframe>');
    },

    ValidateAndFill: function () {
        var self = this;
        var params = self.params;
        var enteredZip = $jq(params.zbox).val();
        
        if (self.IsValidZipCommon(enteredZip)) {
            self.FillHomeMarketUpdate(enteredZip, params.mkbox, params.hdnbox, params.cbox, params.sbox, params.hdncity, params.hdnstate);
        } else {
            
            $jq(params.mkbox).val("");
            $jq(params.hdnbox).val("");
            $jq(params.cbox).val("");
            $jq(params.sbox).val("");
            $jq(params.hdncity).val("");
            $jq(params.hdnstate).val("");
        }
    },
    FillHomeMarketUpdate: function (zip, mkBoxid, hdnBoxid, cBoxid, sBoxid, hdnBoxCid, hdnBoxSid) {
        try {
            var homeMarketBox = $jq(mkBoxid);
            var homeMarketId = $jq(hdnBoxid);
            var cityBox = $jq(cBoxid);
            var stateBox = $jq(sBoxid);
            var cityBoxH = $jq(hdnBoxCid);
            var stateBoxH = $jq(hdnBoxSid);

            if (zip != null && zip != '') {
                $jq.ajax({
                    type: "POST",
                    url: "/Account/GetHomeMarketInfo",
                    data: { zip: zip },
                    success: function (data) {
                        if (data != null && data != '') {
                            homeMarketBox.val(data.split(',')[0]);
                            homeMarketId.val(data.split(',')[1]);
                            cityBox.val(data.split(',')[2]);
                            stateBox.val(data.split(',')[3]);
                            cityBoxH.val(data.split(',')[2]);
                            stateBoxH.val(data.split(',')[3]);
                        } else {
                            homeMarketBox.val("");
                            homeMarketId.val("");
                            cityBox.val("");
                            stateBox.val("");
                            cityBoxH.val("");
                            stateBoxH.val("");
                        }
                    }
                });
            }
        } catch (ex) {

        }
    },
    IsValidZipCommon: function (zip) {
        var reZip = new RegExp(/(^\d{5}$)|(^\d{5}-\d{4}$)/);
        if (!reZip.test(zip)) {
            return false;
        }
        return true;
    }
};
