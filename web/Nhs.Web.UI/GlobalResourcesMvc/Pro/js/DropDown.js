﻿NHS.Scripts.DropDown = function (parameters) {
    this._parameters = parameters;
};


NHS.Scripts.DropDown.prototype =
{
    initialize: function () {
    
        var controls = $jq(".pro_SearchDdl");
        var self = this;
        $jq(document).find("*").click(function (event) {
            var currentclass = $jq(this).attr("class");
            var enter = false;
            if (currentclass) {
                enter = currentclass.indexOf("pro_SearchDdl") > -1 || currentclass.indexOf("trigger") > -1 || currentclass.indexOf("pro_SearchDdlData") > -1 || currentclass.indexOf("resultText") > -1;
                if (enter === false)
                    $jq(".pro_SearchDdlData").hide();
            }
            else $jq(".pro_SearchDdlData").hide();
            var ev = event ? event : window.event;
            if (enter === true && this.nodeName != "HTML")
            {
                ev.cancelBubble = true;
                ev.returnValue = false;
                if (ev.stopPropagation) ev.stopPropagation();
                if (ev.preventDefault) ev.preventDefault();
            }
            
            if (currentclass == "btn_Search" || this.nodeName == "A") {
            }
            else {
                if ($jq(this).find("btn_Search") || $jq(this).find("a[href]")) {
                } else {
                    ev.cancelBubble = true;
                    ev.returnValue = false;
                    if (ev.stopPropagation) ev.stopPropagation();
                    if (ev.preventDefault) ev.preventDefault();
                }
            }
        });

        $jq.each(controls, function () {

            //Get all the control that we going to used
            var control = $jq(this);
            var trigger = control.find(".trigger");
            var data = control.find(".pro_SearchDdlData");
            var resultText = control.find(".resultText");
            resultText.removeAttr("name");
            var resultHidden = control.find(".resultHidden");

            self.CreateTrigger(data, resultText, resultHidden);
            trigger.click(function () {
                controls.find(".pro_SearchDdlData").hide(); //Hide all the ul with class data
                data.show();
            });
        });

    },
    CreateTrigger: function (ul, resultText, resultHidden) {
        var li = ul.find("li");
        li.click(function () {
            var control = $jq(this);

            //Remove the class from all the controsl in the ul and set the Active class to teh selected control
            li.removeClass("pro_Active");
            li.removeAttr("pro_Active");
            control.addClass("pro_Active");

            var value = control.attr("value"); //Get the value
            var text = control.text(); //Get the value

            if (text != "Any") {
                resultHidden.val(value);
                resultText.val(text);
                resultText.removeClass('placeholder');
            } //Set the value
            else {
                resultHidden.val("");
                resultText.val(resultText.attr('placeholder'));
                resultText.addClass('placeholder');
            }
            ul.hide();
        });
    }

};

