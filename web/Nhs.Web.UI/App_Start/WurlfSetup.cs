﻿using System.Web;
using WURFL;
using WURFL.Config;

namespace Nhs.Web.UI
{
    public class WurlfSetup
    {
        public static void Configuration()
        {
            var wurflDataFile = HttpContext.Current.Server.MapPath("~/App_Data/wurfl-latest.zip");
            var configurer = new InMemoryConfigurer().MainFile(wurflDataFile).SetMatchMode(MatchMode.Accuracy);
            WURFLManagerBuilder.Build(configurer);
        }
    }
}