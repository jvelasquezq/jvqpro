﻿
var emptyString = /^\s*$/; var nhsUrlPage = "locationhandler/searchtype-qscr/"; var nhsUrlState = ""; var nhsUrlArea = ""; var nhsUrlCity = ""; var nhsUrlZip = ""; var nhsUrlPriceLow = ""; var nhsUrlPriceHigh = ""; var oldLoad = window.onload; window.onload = function() {
    if (oldLoad)
    { oldLoad(); }
    addEventHandlers();
}
function addEventHandlers() {
    document.getElementById("nhsBtnSearch").onclick = function()
    { ValidateNhsForm(); }
    document.getElementById("nhsTxtZip").onkeypress = document.getElementById("nhsDdlState").onkeypress = document.getElementById("nhsDdlPriceFrom").onkeypress = document.getElementById("nhsDdlPriceTo").onkeypress = function(e) {
        e = e || window.event; var ENTER_KEY = 13; var code = ""; if (window.event)
        { code = e.keyCode; }
        else if (e.which)
        { code = e.which; }
        if (code == ENTER_KEY) { ValidateNhsForm(); return false; }
    }
}

function ValidateNhsForm() {
    zipcity = document.getElementById("nhsTxtZip").value; if (emptyString.test(zipcity))
    { var errorDiv = document.getElementById("nhsErrorDiv"); errorDiv.innerHTML = "<p>Please enter a city or zip.</p>"; return false; }
    else {
        if (!isNaN(zipcity)) {
            reZip = new RegExp(/(^\d{5}$)|(^\d{5}-\d{4}$)/); if (!reZip.test(zipcity))
            { var errorDiv = document.getElementById("nhsErrorDiv"); errorDiv.innerHTML = "<p>Zip Code Is Not Valid.</p>"; return false; }
        }
        else if (document.getElementById("nhsDdlState").value == "")
        { var errorDiv = document.getElementById("nhsErrorDiv"); errorDiv.innerHTML = "<p>Please enter a city/state or zip.</p>"; return false; }
        else
        { nhsUrlState = "state-" + document.getElementById("nhsDdlState").value + "/"; }
    }
    nhsUrlPage = "locationhandler/searchtype-qscr/"; nhsUrlZip = "?searchtext=" + zipcity; var tempLow = parseInt(document.getElementById("nhsDdlPriceFrom").value); var tempHigh = parseInt(document.getElementById("nhsDdlPriceTo").value); if (tempLow > 0 && tempHigh > 0) {
        if (tempLow > tempHigh)
        { var errorDiv = document.getElementById("nhsErrorDiv"); errorDiv.innerHTML = "<p>Please check your price selections.</p>"; return false; }
    }
    if (document.getElementById("nhsDdlPriceFrom").value != "0")
    { nhsUrlPriceLow = "pricelow-" + document.getElementById("nhsDdlPriceFrom").value + "/"; }
    if (document.getElementById("nhsDdlPriceTo").value != "0")
    { nhsUrlPriceHigh = "pricehigh-" + document.getElementById("nhsDdlPriceTo").value + "/"; }
    AssembleNhsUrl();
}

function AssembleNhsUrl() {
    var nhsUrlComplete;

    if (REFER != "") {
        REFER = "refer-" + REFER + "/";
        nhsUrlComplete = "http://newhomes.move.com/" + nhsUrlPage + nhsUrlState + REFER + nhsUrlPriceLow + nhsUrlPriceHigh + nhsUrlZip;
    }
    else {
        nhsUrlComplete = "http://newhomes.move.com/" + nhsUrlPage + nhsUrlState + nhsUrlPriceLow + nhsUrlPriceHigh + nhsUrlZip;
    }

    if (TRACKING_CODE != "") {
        nhsUrlComplete = TRACKING_CODE + "?" + nhsUrlComplete;
    }

    if (typeof NEW_WINDOW != 'undefined' && NEW_WINDOW != true) {
        window.location.href = nhsUrlComplete;
    }
    else {
        window.open(nhsUrlComplete, 'newWindow');
    }
}