/*****************************************************************************\

 Javascript "Widget" File
 
 @version: 1.0 - Dec 05, 2007
 @author: Allan Rojas / arojas @ builderhomesite . com
 @modified: JasonB : modifed js to use DynamiTag script
 INSTRUCTIONS:
 :: Only Edit the PARTNER_ID and the WEBSERVICE_URL variables accordingly.
 minimize js and css using tools here http://del.icio.us/rajiv/min
\*****************************************************************************/

var emptyString = /^\s*$/;
var nhsUrlPartnerPath = "";
var nhsUrlPage = "locationhandler/searchtype-qscr/";
var nhsUrlState = "";
var nhsUrlArea = "";
var nhsUrlCity = "";
var nhsUrlZip = "";
var nhsUrlPriceLow = "";
var nhsUrlPriceHigh = "";

// Combine onload functions
var oldLoad = window.onload;
window.onload = function()
{
	if (oldLoad)
	{
		oldLoad();
	}	
	addEventHandlers();
	DynamicTag(); //service states
}

// Adding onclick to button
function addEventHandlers() {
    document.getElementById("nhsBtnSearch").onclick = function ()
    {
        ValidateNhsForm();
    }
    document.getElementById("nhsTxtZip").onkeypress =
    document.getElementById("nhsDdlState").onkeypress = 
    document.getElementById("nhsDdlPriceFrom").onkeypress =
    document.getElementById("nhsDdlPriceTo").onkeypress = function(e) 
    {
        e = e || window.event;
        var ENTER_KEY = 13;
        var code = "";
  
        if (window.event) // IE
        {
            code = e.keyCode;
        }
        else if (e.which) // Netscape/Firefox/Opera
        {
            code = e.which;
        }
                
        if (code == ENTER_KEY) {
            ValidateNhsForm();
            return false;
        }
    }
}

// Dynamic Script Tag hack
// Adapted from http://www.simple-talk.com/dotnet/asp.net/calling-cross-domain-web-services-in-ajax/
function DynamicTag()
{
    // adding script tag for States JSON
    var request = WEBSERVICE_URL + "/GetStates?partnerId="
                     + PARTNER_ID
                     + "&callbackfunction=GetStatesComplete";
    var head = document.getElementsByTagName("head").item(0);
    var script = document.createElement("script");
    script.setAttribute("type", "text/javascript");
    script.setAttribute("src", request);
    head.appendChild(script);

    if(PARTNER_ID != 1)
    {
    // need to add script tag for PartnerUrl
    var request = WEBSERVICE_URL + "/GetPartnerSiteUrl?partnerId="
                     + PARTNER_ID
                     + "&callbackfunction=SetNhsUrlPartnerPath";
                    
    var head = document.getElementsByTagName("head").item(0);
    var script = document.createElement("script");
    script.setAttribute("type", "text/javascript");
    script.setAttribute("src", request);
    head.appendChild(script);
    }

}

function SetNhsUrlPartnerPath(url)
{
    
    nhsUrlPartnerPath = url+"/";

}

function GetStatesComplete(JSONData)
{
    if (JSONData != null)
    {
	    var ddlState = document.getElementById("nhsDdlState");

	    if (JSONData[JSONData.length-1].Name == "Choose a State")
		    ddlState.options[0] = new Option("Choose a State", "", true, true);
	    else
		    ddlState.options[0] = new Option("Choose a State", "");
    		
    	// traverse the JSONData object for the states.
	    for(var i = 0; i < JSONData.length-1; i++)
	    {
		    if (JSONData[i].Abbreviation == JSONData[JSONData.length-1].Name)
			    ddlState.options[i+1] = new Option(JSONData[i].Name, JSONData[i].Abbreviation, true, true);
		    else
			    ddlState.options[i+1] = new Option(JSONData[i].Name, JSONData[i].Abbreviation);
	    }
	}
}

// Validate form when button is clicked
function ValidateNhsForm() 
{        
    //alert("inside ValidateNhsForm");
    
    // Validate Zip, or State and Area
	zipcity = document.getElementById("nhsTxtZip").value;
	if (emptyString.test(zipcity))
	{
	    var errorDiv = document.getElementById("nhsErrorDiv");
        errorDiv.innerHTML = "<p>Please enter a city or zip.</p>";
        return false;
	}
	else
	{   
        if (!isNaN(zipcity)) //if it's a number, validate zipcode format
	    {
		    reZip = new RegExp(/(^\d{5}$)|(^\d{5}-\d{4}$)/);
		    if (!reZip.test(zipcity))
		    {
			    var errorDiv = document.getElementById("nhsErrorDiv");
                errorDiv.innerHTML = "<p>Zip Code Is Not Valid.</p>";
			    return false;
		    }
	    }
	    // Validate State
	    else if (document.getElementById("nhsDdlState").value == "")
	    {
			    //alert("Please select a State");
			    var errorDiv = document.getElementById("nhsErrorDiv");
                errorDiv.innerHTML = "<p>Please enter a city/state or zip.</p>";
			    return false;
	    }	    
        else // state is ok, set state url
        {
            nhsUrlState = "state-" + document.getElementById("nhsDdlState").value + "/";
        }           
	}	
	// Passed zip, market and state validation
	nhsUrlPage = "locationhandler/searchtype-qscr/";
    nhsUrlZip = "?searchtext=" + zipcity;
	
	// validate prices
	var tempLow = parseInt(document.getElementById("nhsDdlPriceFrom").value);
	var tempHigh = parseInt(document.getElementById("nhsDdlPriceTo").value);
	if (tempLow > 0 && tempHigh > 0)
	{	    
		if (tempLow > tempHigh)
		{
		    var errorDiv = document.getElementById("nhsErrorDiv");
            errorDiv.innerHTML = "<p>Please check your price selections.</p>";
			return false;
		}
	}
	
    // Validate Price Low
    if (document.getElementById("nhsDdlPriceFrom").value != "0")
    {
        nhsUrlPriceLow = "pricelow-" + document.getElementById("nhsDdlPriceFrom").value + "/";
    }
    // Validate Price High
    if (document.getElementById("nhsDdlPriceTo").value != "0")
    {
        nhsUrlPriceHigh = "pricehigh-" + document.getElementById("nhsDdlPriceTo").value + "/";
    }
    
    AssembleNhsUrl();  
}

function AssembleNhsUrl()
{
  
    //alert("inside AssembleNhsUrl");
    var nhsUrlComplete = "http://www.newhomesource.com/" + nhsUrlPartnerPath + nhsUrlPage + nhsUrlState + nhsUrlPriceLow + nhsUrlPriceHigh + nhsUrlZip;
    //alert(nhsUrlComplete);
    window.location.href = nhsUrlComplete;        
}



