/* ---------------------------------------------
Title:      Community Results JS
Updated:    Apr 24 2008 - 2:02 PM
----------------------------------------------- */
    
    //*************************************************************************
    // PAGE VARIABLES 
    //*************************************************************************    
    var nhsMqMap;
    var mqOrigCenter;
    var mqOrigZoom;
    var mqCenter;
    var mqZoom;
    var mqPoiCollection;
    var mqResultsPoiCollection;
    var prevBrowserSize;  
    var nhsMapIcon;
    var updateMapFlag = true;
    var partnerId = document.getElementById('partnerId').value;
    var siteRoot = document.getElementById('siteRoot').value;
    var resourceRoot = document.getElementById('resourceRoot').value;
    var mqIconPath = resourceRoot + "globalresources/default/images/map/marker.gif";
    var mqIconMultPath = resourceRoot + "globalresources/default/images/map/marker_multiple.gif";
    var mqIconShadowPath = resourceRoot + "globalresources/default/images/map/marker_shadow.png";
    var mqIconMktPath = resourceRoot + "globalresources/default/images/map/mkt_icon.gif";
    var pageUrl = document.location.href;
    var marketId = marketIdFromCommResControl;
    //var marketId = getQueryStringValue("market");

    //var mqIconMktShadowPath;       

//    Run PageStart when page load is complete
//    window.onload = function()
//    {    
//    //PageStart();
//    }

/*
var date1;
var date2;
var milliseconds1;
var milliseconds2;

function StartTiming()
{
date1 = new Date();
milliseconds1 = date1.getTime();
}

function StopTiming()
{
date2 = new Date();
milliseconds2 = date2.getTime();
var difference = milliseconds2 - milliseconds1;
alert(difference/1000 + " seconds"); 
}
*/

//StartTiming();


//*************************************************************************
// Check for DOM Loaded from
// Dean Edwards/Matthias Miller/John Resig
// http://dean.edwards.name/weblog/2006/06/again/
//************************************************************************* 

    function init() 
    {
        // quit if this function has already been called
        if (arguments.callee.done) return;

        // flag this function so we don't do the same thing twice
        arguments.callee.done = true;

        // kill the timer
        if (_timer) clearInterval(_timer);

        // do stuff
        //window.alert("Dom Loaded");
        PageStart();
        //StopTiming();
    };

    /* for Mozilla/Opera9 */
    if (document.addEventListener) 
    {
        document.addEventListener("DOMContentLoaded", init, false);
    }

    /* for Internet Explorer */
    /*@cc_on @*/
    /*@if (@_win32)
        document.write("<script id=__ie_onload defer src=javascript:void(0)><\/script>");
        var script = document.getElementById("__ie_onload");
        script.onreadystatechange = function() {
            if (this.readyState == "complete") {
                init(); // call the onload handler
            }
        };
    /*@end @*/

    /* for Safari */
    if (/WebKit/i.test(navigator.userAgent)) 
    { // sniff
        var _timer = setInterval(function() {
            if (/loaded|complete/.test(document.readyState)) {
                init(); // call the onload handler
            }
        }, 10);
    }

    /* for other browsers */
    window.onload = function() 
    {
        //window.alert("Body Loaded");
        init();
    }
    
    
//*************************************************************************
// EVENT HANDLERS
//*************************************************************************

    function PageStart()
    {                                                         
        resizeMapDiv(); 
        //window.alert("Resize Div");
        
        mapCenterLat = document.getElementById('mqCenterLat').value;        
        mapCenterLng = document.getElementById('mqCenterLng').value; 
        
        mqZoom = document.getElementById('mqZoomLevel').value;     
        mqOrigZoom = mqZoom;    
        mqOrigCenter = new MQLatLng(mapCenterLat, mapCenterLng);
        mqCenter = mqOrigCenter;
    
        // Create the map object                      
        nhsMqMap = new MQTileMap(document.getElementById('nhsCommResMapDiv'), mqZoom, mqOrigCenter,'map');
        //window.alert("Map Created");
        
        //Standard NHS Community Icon
        nhsMapIcon = new MQMapIcon();
        nhsMapIcon.setImage(mqIconPath,20,26,false,false);
        nhsMapIcon.setShadow(mqIconShadowPath,0,0,0,0,true);        
        nhsMapIcon.setAnchorOffset(new MQPoint(-9,-26));
        nhsMapIcon.setInfoWindowAnchor(new MQPoint(12,-1));
        
        //NHS Multiple Community Icon
        nhsMapIconMult = new MQMapIcon();
        nhsMapIconMult.setImage(mqIconMultPath,28,28,false,false);
        nhsMapIconMult.setShadow(mqIconShadowPath,0,0,0,0,true);        
        nhsMapIconMult.setAnchorOffset(new MQPoint(-14,-28));
        nhsMapIconMult.setInfoWindowAnchor(new MQPoint(16,-1));
        
        //Market icon
        nhsMapIconMkt = new MQMapIcon();
        nhsMapIconMkt.setImage(mqIconMktPath,40,37,false,false);
        nhsMapIconMkt.setShadow(mqIconShadowPath,0,0,0,0,true); 
        nhsMapIconMkt.setAnchorOffset(new MQPoint(-20,-37));         
                               
        mqMoveRefPoint = nhsMqMap.pixToLL(new MQPoint(0,0));
        
        mqZoomControl = new MQLargeZoomControl();
        nhsMqMap.addControl(mqZoomControl, new MQMapCornerPlacement(MQMapCorner.TOP_LEFT, new MQSize(10,21)));
        
        setMapWindowCoordinates();
                        
        updateMapPoints();                               
    
        // Hide search map area box if zoom level is less than market. 
        if(mqZoom < 7)
        {
            document.getElementById('SearchMapAreaParagraph').style.display = "none";
        }
        else
        {
            document.getElementById('SearchMapAreaParagraph').style.display = "block";
        }

        // Check for IE 7
        if (getBrowserInfo().name == 'msie' && parseFloat(getBrowserInfo().version) >= 7.0) 
        {
            $("popupcenter").style.height = "100%";
        }   

        // Capture the browser size and kick off the 
        // timer for checking browser size. 
        prevBrowserSize = getBrowserSize();  
        BrowserResize();              
        
        //MQEventManager.addListener(nhsMqMap, "click", MapClick);
        MQEventManager.addListener(nhsMqMap, "zoomend", zoomEnd);    
        MQEventManager.addListener(nhsMqMap,"moveend",mapMoveEnd);  
        MQEventManager.addListener(nhsMqMap.getInfoWindow(),"opened",infoWindowOpen);  
        MQEventManager.addListener(nhsMqMap.getInfoWindow(),"closed",infoWindowClose);  
        MQEventManager.addListener(nhsMqMap.getRolloverWindow(),"opened",rolloverWindowOpen);
        MQEventManager.addListener(nhsMqMap.getRolloverWindow(),"closed",rolloverWindowClose);
        
        // Run script
        AutoViewOnMapLink();
        
        // Check cookies and display map tip
        //displayMapTip(); removed for 21735            
    }        
    

    function infoWindowOpen(e)
    {
        updateMapFlag = false;  
        LogEvent('MAPCLICK',0,0);
    }
    function infoWindowClose(e)
    {
        updateMapFlag = true;
    }
    
    function rolloverWindowOpen(e)
    {
        updateMapFlag = false;
    }
    function rolloverWindowClose(e)
    {
        updateMapFlag = true;
    }
    
    function AsyncPageLoad(sender, args) 
    {
        var dataItems = args.get_dataItems();
                
        try 
        {   
            if(dataItems[litSearchParams])
            {
                xmlSearchParams = dataItems[litSearchParams];        
                
                if(dataItems[litNewMapCenterLat])
                {
                    newMapLat = dataItems[litNewMapCenterLat];
                    newMapLng = dataItems[litNewMapCenterLng];
                    newMapZoom = dataItems[litNewMapZoom];
                    var startTime = new Date().getTime();
                    while (typeof nhsMqMap == "undefined")
                    {
                        var currentTime = new Date().getTime();
                        var diffTime = currentTime - startTime;
                        if (diffTime > 5000) 
                        {
                            break;
                        }
                    }
                    if (nhsMqMap)
                    {
                    ZoomToLatLng(newMapLat,newMapLng,newMapZoom);                    
                    }
                }
                else
                {   
                    var startTime = new Date().getTime();
                    while (typeof nhsMqMap == "undefined")
                    {
                        //alert("while");
                        var currentTime = new Date().getTime();
                        var diffTime = currentTime - startTime;
                        if (diffTime > 5000) 
                        {
                            break;
                        }
                    }
                    if (nhsMqMap)
                    {
                        updateMapPoints();                    
                    }
                }                
            }   
            
            if (typeof plhAdUpdateTop != 'undefined') 
            {
            if(dataItems[plhAdUpdateTop])
            {
                var adSrcTop = dataItems[plhAdUpdateTop];
                var adSrcMiddle = dataItems[plhAdUpdateMiddle];
                var adSrcMiddle3 = dataItems[plhAdUpdateMiddle3];
                var adSrcRight2 = dataItems[plhAdUpdateRight2];
                //var adSrcBottom = dataItems[plhAdUpdateBottom];
                
                if(adSrcTop != "" && adSrcMiddle != "" && adSrcMiddle3 != "" && adSrcRight2 != "")
                {
                    updateIFrameAd("Top",adSrcTop);
                    updateIFrameAd("Middle", adSrcMiddle);
                    updateIFrameAd("Middle3", adSrcMiddle3);
                    updateIFrameAd("Right2", adSrcRight2);
                    //updateIFrameAd("Bottom", adSrcBottom);
                }               
            }                   
            }
        }
        catch (err)
        {
           if(typeof err != "undefined")
           {
            //alert(err.message);
            HandleError(err.message, '', -1, "AsyncPageLoad");
           }
        }
    }
    
    function updateIFrameAd(adPosition, adSrc)
    {
        var adId = "nhsIFrameAd" + adPosition;        
        var adElement = document.getElementById(adId);
        
        if (adElement.style.display != 'none') //check if adBlock is hiding iframe
        { 
        //alert("Updating " + adId + " source:\n" + "Current: \n" + adElement.src + "\nNew: \n" + adSrc);
        adElement.src = adSrc;
    }     
    }     
    
    function BrowserResize()
    {
        var newBrowserSize = getBrowserSize();

        if(prevBrowserSize.getWidth() != newBrowserSize.getWidth() 
            || prevBrowserSize.getHeight() != newBrowserSize.getHeight())
        {
            resizeMap();
            // Check cookies and display map tip
            //displayMapTip(); removed for 21735
        }            
        
        prevBrowserSize = newBrowserSize;
        var x = setTimeout("BrowserResize()",250);
    }
    
    //*************************************************************************
    // MAP FUNCTIONS
    //*************************************************************************  
    function RenderPoiCollection(poiCollection)
    {    
        nhsMqMap.replacePois(poiCollection);    
        //document.getElementById('numMapPoints').innerHTML = poiCollection.getSize();
        
//        if (poiCollection.getSize() == 1)
//        {
//            var singlePoi = poiCollection.getAt(0);
//            commPoiLatLng = singlePoi.getLatLng();
//            nhsMqMap.panToLatLng(commPoiLatLng);
//            updateMapFlag = false;
//        }
        hideMapStatus();
    }    
    
    function mapMoveEnd(e)
    {                       
        if(updateMapFlag)
        {
            LogEvent('MAPDRAG', 0, 0);        
            showMapStatus();             
            setMapWindowCoordinates();                         
            updateMapPoints();
        }        
    }
    
    function zoomEnd(e)
    {
        if(e.prevZoom != e.zoom)
        {
            //Log Zoom in/out
            LogEvent('MAPZOOM',0,0);
        
            if(e.zoom < 7)
            {
                document.getElementById('SearchMapAreaParagraph').style.display = "none";
            }
            else
            {
                document.getElementById('SearchMapAreaParagraph').style.display = "block";
            }
            
            setMapWindowCoordinates();   
            updateMapPoints();             
        }
    }   
    
    function hideMapStatus()
    {
        document.getElementById("mapLoading").style.display = "none";
    }
    
    function showMapStatus()
    {
        document.getElementById("mapLoading").style.display = "block";
    }
    
    function updateMapPoints()
    {                              
        minLat = document.getElementById("mqMinLat").value;
        minLng = document.getElementById("mqMinLng").value;
        maxLat = document.getElementById("mqMaxLat").value;
        maxLng = document.getElementById("mqMaxLng").value;
        maxResults = 50;

        currentZoom = nhsMqMap.getZoomLevel();
        
        if(currentZoom < 4)
        {
            getStateMapPois(minLat, minLng, maxLat, maxLng);
        }
        else
        {
            if(currentZoom < 7)
            {
                getMarketMapPois(minLat, minLng, maxLat, maxLng);
            }
            else
            {            
                getCommMapPois(xmlSearchParams, minLat, minLng, maxLat, maxLng, maxResults);                   
            }
        }           
    }    
    
    function resizeMap()
    {
        newMapSize = getMapSize();           
        nhsMqMap.setSize(newMapSize);
        document.getElementById('nhsCommResMapDiv').style.width = newMapSize.getWidth() + "px";
        document.getElementById('nhsCommResMapDiv').style.height = newMapSize.getHeight() + "px";       
    }
        
    function resizeMapDiv()        
    {
        newMapSize = getMapSize();
        document.getElementById('nhsCommResMapDiv').style.width = newMapSize.getWidth() + "px";
        document.getElementById('nhsCommResMapDiv').style.height = newMapSize.getHeight() + "px"; 
    }
    
    function getMapSize()
    {
        mapToolsContainer = document.getElementById('nhsCommResMapTools');        
        mapWidth = mapToolsContainer.offsetWidth - 2;
        mapHeight = 500;                
        return new MQSize(mapWidth,mapHeight);        
    }        
                 
    function setMapWindowCoordinates()
    {
        mpSize = getMapSize();        
        topLeftLL = nhsMqMap.pixToLL(new MQPoint(0,0));
        bottomRightLL = nhsMqMap.pixToLL(new MQPoint(mpSize.getWidth(), mpSize.getHeight()));               
        
        // Set the hidden form values to the current map window coordinates. 
        document.getElementById("mqMinLat").value = topLeftLL.getLatitude();
        document.getElementById("mqMinLng").value = topLeftLL.getLongitude();
        document.getElementById("mqMaxLat").value = bottomRightLL.getLatitude();
        document.getElementById("mqMaxLng").value = bottomRightLL.getLongitude(); 
        document.getElementById("mqCenterLat").value = nhsMqMap.getCenter().getLatitude();
        document.getElementById("mqCenterLng").value = nhsMqMap.getCenter().getLongitude();  
        document.getElementById("mqZoomLevel").value = nhsMqMap.getZoomLevel();
    }

    function ResetSearchArea()
    {    
        LogEvent('MAPRESET',0,0);
        nhsMqMap.setCenter(mqOrigCenter);
        nhsMqMap.setZoomLevel(mqOrigZoom);
    }
    
    function ZoomToLatLng(lat, lng, zoomLevel)
    {        
        nhsMqMap.setCenter(new MQLatLng(lat,lng));
        nhsMqMap.setZoomLevel(zoomLevel);
    }          
    
    function ZoomToLatLngBC(lat, lng, zoomLevel,cid,bid)
    {        
        nhsMqMap.setCenter(new MQLatLng(lat,lng));
        nhsMqMap.setZoomLevel(zoomLevel);
        LogEvent("HOVZOOM",cid,bid);        
    }          
    
    //shows transient hover
    function ViewCommunityOnMap(e,cid,bid,name,cty,st,zip,img,brandimg,price,brand,matches,promoid,type,lat,lng)
    {   
        LogEvent('VIEWMAP', cid, bid);
        // Ignore if Auto Centering
        if (viewOnMapLinkScript == "")
        {
    	    if (!e) var e = window.event;
	        e.cancelBubble = true;
	        if (e.stopPropagation) e.stopPropagation();
	    }
	    
        poiCollection = nhsMqMap.getPois();
        keyToFind = "" + cid + bid;  
        
        pointFound = false;  
    
        for(i=0; i<poiCollection.getSize(); i++)
        {            
            commPoi = poiCollection.getAt(i);
            
            if(commPoi.getKey().match("mkey") == "mkey")
            {
                if(commPoi.getKey().match(keyToFind) == keyToFind)
                {
                    pointFound = true;
                
                    commPoiLatLng = commPoi.getLatLng();
                   
                    nhsMqMap.panToLatLng(commPoiLatLng);
                        
                    // Ignore if Auto Centering
                    if (viewOnMapLinkScript == "")
                    {
                        commPoi.showRolloverWindow(); 
                    }
                    //updateMapFlag = false;
                    
                    break;
                }
            }           
            else if(commPoi.getKey() == keyToFind)
            {
                //nhsMqMap.setCenter(commPoi.getLatLng());
                pointFound = true;
                
                commPoiLatLng = commPoi.getLatLng();
               
                nhsMqMap.panToLatLng(commPoiLatLng);
                    
                // Ignore if Auto Centering
                if (viewOnMapLinkScript == "")
                {
                    commPoi.showRolloverWindow(); 
                }
                //commPoi.showInfoWindow();
                //updateMapFlag = false;
                break;
            }            
        }        
        
        if(!pointFound && nhsMqMap.getZoomLevel() >= 7)
        {        
            commPoi = createCommPoi(cid,bid,name,cty,st,zip,img,brandimg,price,brand,matches,promoid,type,lat,lng);
            nhsMqMap.addPoi(commPoi);
            
            commPoi.setRolloverEnabled(true);
            //nhsMqMap.setCenter(commPoi.getLatLng());
            
            commPoiLatLng = commPoi.getLatLng();            
            
            nhsMqMap.panToLatLng(commPoiLatLng); 
            
            // Ignore if Auto Centering
            if (viewOnMapLinkScript == "")
            {
                commPoi.showRolloverWindow(); 
            }
            //commPoi.showInfoWindow();
            setMapWindowCoordinates();
            //updateMapFlag = false;
        }        
        else if (!pointFound && nhsMqMap.getZoomLevel() < 7)
        {
            commPoi = createCommPoi(cid,bid,name,cty,st,zip,img,brandimg,price,brand,matches,promoid,type,lat,lng);
            nhsMqMap.setZoomLevel(7);
        
            commPoiLatLng = commPoi.getLatLng();
            nhsMqMap.panToLatLng(commPoiLatLng);

            commPoi.setRolloverEnabled(true);
                
            nhsMqMap.addPoi(commPoi);
            // Ignore if Auto Centering
            if (viewOnMapLinkScript == "")
            {
                commPoi.showRolloverWindow(); 
            }

            //commPoi.showInfoWindow();
            setMapWindowCoordinates();
            //updateMapFlag = false;
    }
    }
    
    //shows transient hover on community result hover
    function ViewCommunityOnMapOnHover(e,cid,bid)
    {
        if (nhsMqMap) 
        {        
    	    if (!e) var e = window.event;
	        e.cancelBubble = true;
	        if (e.stopPropagation) e.stopPropagation();
    	    
            poiCollection = nhsMqMap.getPois();
            keyToFind = "" + cid + bid; 
          
            for(i=0; i<poiCollection.getSize(); i++)
            {            
                commPoi = poiCollection.getAt(i);
                
                if(commPoi.getKey().match(keyToFind) == keyToFind)
                {
                    commPoi.showRolloverWindow();                
                    break;
                }            
            } 
        }
    }
    
    function HideCommunityOnMapOnHover(e,cid,bid)
    {   
        if (nhsMqMap)
        {
            if (!e) var e = window.event;
	        e.cancelBubble = true;
	        if (e.stopPropagation) e.stopPropagation();
    	    
            poiCollection = nhsMqMap.getPois();
            keyToFind = "" + cid + bid; 
          
            for(i=0; i<poiCollection.getSize(); i++)
            {            
                commPoi = poiCollection.getAt(i);
                
                if(commPoi.getKey().match(keyToFind) == keyToFind)
                {
                    if(commPoi.mqrw)
                    {
                    commPoi.mqrw.hide();                               
                    }
                    break;
                }            
            }   
            //document.getElementById("minipopup").style.visibility = "hidden";     
        }     
    }
    
    //*************************************************************************
    // Web service calls
    //*************************************************************************  
    function getCommMapPois(xmlSearchParams, minLat, minLng, maxLat, maxLng, maxResults)
    {       
        Nhs.Web.ServiceProxy.NhsServices.GetCommMapPoints(xmlSearchParams, minLat, minLng, maxLat, maxLng, maxResults, onCommPoiSuccess, onError);    
    }

    function getMarketMapPois(minLat, minLng, maxLat, maxLng)
    {
        Nhs.Web.ServiceProxy.NhsServices.GetMarketMapPoints(partnerId, minLat, minLng, maxLat, maxLng, onMarketPoiSuccess, onError);    
    }

    function getStateMapPois(minLat, minLng, maxLat, maxLng)
    {
        Nhs.Web.ServiceProxy.NhsServices.GetStateMapPoints(partnerId, minLat, minLng, maxLat, maxLng, onStatePoiSuccess, onError);    
    }

    function onMarketPoiSuccess(result)
    {
   
        var poiNodes = result.getElementsByTagName("MapPoi");        
        mqPoiCollection = new MQPoiCollection();  
        var poiNodesLength = poiNodes.length;  
        
        for(i=0; i<poiNodesLength; i++)
        {
            var id = getChildNodeValue(poiNodes[i],"Id");
            var lat = getChildNodeValue(poiNodes[i],"Lat");
            var lng = getChildNodeValue(poiNodes[i],"Lng");
            var title = getChildNodeValue(poiNodes[i],"Title");
            var content = getChildNodeValue(poiNodes[i],"Content");
            
            contentHtml = "<div class=\"nhsCommResHoverMarket\">";
            contentHtml += "<a href=\"javascript:void(0);\" onclick=\"ZoomToLatLng(" + lat + "," + lng + ",7);\">View Communities in " + content + "</a>";
            contentHtml += "</div>";
            
            mqPoint = new MQPoi(new MQLatLng(lat,lng), new MQMapIcon(nhsMapIconMkt));            
            mqPoint.setInfoTitleHTML("<strong>" + title + "</strong>");
            mqPoint.setInfoContentHTML(contentHtml);         
            mqPoint.setKey(id);        
            
            mqPoiCollection.add(mqPoint);
        }
                
        RenderPoiCollection(mqPoiCollection);
    }

    function onStatePoiSuccess(result)
    {
        var poiNodes = result.getElementsByTagName("StatePoi");        
        mqPoiCollection = new MQPoiCollection();  
        var poiNodesLength = poiNodes.length; 
        
        for(i=0; i<poiNodesLength; i++)
        {
            var lat = getChildNodeValue(poiNodes[i],"Lat");
            var lng = getChildNodeValue(poiNodes[i],"Lng");
            var stateName = getChildNodeValue(poiNodes[i],"StateName");
            var totalCommunities = getChildNodeValue(poiNodes[i],"TotalCommunities");
            
            contentHtml = "<div class=\"nhsCommResHoverState\">";
            contentHtml += "<a href=\"javascript:void(0);\" onclick=\"ZoomToLatLng(" + lat + "," + lng + ",4);\">View Markets in " + stateName + "</a>";
            contentHtml += "</div>";
            
            mqPoint = new MQPoi(new MQLatLng(lat,lng), new MQMapIcon(nhsMapIconMkt));            
            mqPoint.setInfoTitleHTML("<strong>" + stateName + " ("  + totalCommunities + " communities)</strong>");
            mqPoint.setInfoContentHTML(contentHtml);         
            mqPoint.setKey(stateName);        
            
            mqPoiCollection.add(mqPoint);
        }
                
        RenderPoiCollection(mqPoiCollection);
    }

    function onCommPoiSuccess(result)
    {   
        // Ignore if there is a View On Map Link Script
        if (viewOnMapLinkScript == "")
        {    
            //StartTiming();
            //window.alert("Start Updating POIs");
            mqPoiCollection = BuildCommPois(result);
            RenderPoiCollection(mqPoiCollection);
            //StopTiming();
            //window.alert("End Updating POIs");
        }
        else        
        {
            // Reset
            viewOnMapLinkScript = "";
        }
    }
    
    function createCommPoi(cid,bid,name,cty,st,zip,img,brandimg,price,brand,matches,promoid,type,lat,lng)
    {
        var typeImg = "";
        var currentZoom = nhsMqMap.getZoomLevel();
                
        if(type == "comingsoon"){ typeImg = resourceRoot + "globalresources/default/images/icons/icon_coming_soon.gif";}
        if(type == "grandopening"){ typeImg = resourceRoot + "globalresources/default/images/icons/icon_grand_opening.gif";}
        if(type == "closeout"){ typeImg = resourceRoot + "globalresources/default/images/icons/icon_closeout.gif";}        
        
        if(img == "N" || img == "n" || img == "")
        {
            img = resourceRoot + "globalresources/default/images/small_nophoto.gif";
        }
        else
        {
          img = resourceRoot.substring(0, resourceRoot.length - 1) + img; //remove trailing slash
        }
        
        var contentHtml = new Array();
        
        contentHtml.push("<div class=\"nhsCommResHoverBox\"><div class=\"nhsHoverImgs\"><div class=\"nhsResultsThumb\"><a href=\"");
        contentHtml.push(siteRoot);
        contentHtml.push("communitydetail/community-");
        contentHtml.push(cid);
        contentHtml.push("/builder-");
        contentHtml.push(bid);
        contentHtml.push("\" onclick=\"LogAndRedirect(event,'");
        contentHtml.push(siteRoot);
        contentHtml.push("communitydetail/community-");
        contentHtml.push(cid);
        contentHtml.push("/builder-");
        contentHtml.push(bid);
        contentHtml.push("/logevent-HOVNAME");          
        contentHtml.push("', 'HOVNAME'," + cid + "," + bid + "); return false;\"><img src=\"");
        contentHtml.push(img);
        contentHtml.push("\" alt=\"");
        contentHtml.push(name);
        contentHtml.push("\" /></a></div>");
        if(brandimg != "N" && brandimg != "")
        {
            contentHtml.push("<img src=\"");
            contentHtml.push(brandimg);
            contentHtml.push("\" alt=\"");
            contentHtml.push(brand);
            contentHtml.push("\" />");
        }
        else
        {
          brandimg = resourceRoot.substring(0, resourceRoot.length - 1) + brandimg;
        }
        contentHtml.push("</div>");
        contentHtml.push("<h3><a href=\"");
        contentHtml.push(siteRoot);
        contentHtml.push("communitydetail/community-");
        contentHtml.push(cid);
        contentHtml.push("/builder-");
        contentHtml.push(bid);
        contentHtml.push("\" onclick=\"LogAndRedirect(event,'");
        contentHtml.push(siteRoot);
        contentHtml.push("communitydetail/community-");
        contentHtml.push(cid);
        contentHtml.push("/builder-");
        contentHtml.push(bid);
        contentHtml.push("/logevent-HOVNAME");          
        contentHtml.push("', 'HOVNAME'," + cid + "," + bid + "); return false;\">");
        contentHtml.push(name)
        contentHtml.push("</a></h3>");
        contentHtml.push("<p class=\"nhsResultsBuilder\">by ");
        contentHtml.push(brand);
        contentHtml.push("</p>");
        contentHtml.push("<p class=\"nhsResultsAddress\">");
        contentHtml.push(cty);
        contentHtml.push(", ");
        contentHtml.push(st);
        contentHtml.push(" ");
        contentHtml.push(zip);
        contentHtml.push("</p>");
        
        if(typeImg != "")
        {
            contentHtml.push("<p class=\"nhsResultsCommStatus\"><img src=\"");
            contentHtml.push(typeImg);
            contentHtml.push("\" alt=\"Coming Soon\" /></p>");
        }       
              
        if(price != "$ - $")      
        {
            contentHtml.push("<p class=\"nhsResultsPrice\"><strong>From ");
            contentHtml.push(price);
            contentHtml.push("</strong></p>");
        }   
        
        if(matches > 0)
        {
            contentHtml.push("<p>");
            contentHtml.push(matches)
            contentHtml.push(" matching new homes</p>");
        }
        else
        {
            contentHtml.push("<p>new homes coming soon</p>");
        }
        
        contentHtml.push("<ul><li><a href=\"");
        contentHtml.push(siteRoot);
        contentHtml.push("communitydetail/community-");
        contentHtml.push(cid);
        contentHtml.push("/builder-");
        contentHtml.push(bid);
        contentHtml.push("/view-details\" onclick=\"LogAndRedirect(event,'");
        contentHtml.push(siteRoot);
        contentHtml.push("communitydetail/community-");
        contentHtml.push(cid);
        contentHtml.push("/builder-");
        contentHtml.push(bid);
        contentHtml.push("/logevent-HOVPFPLN");                                                        
        contentHtml.push("/view-details', 'HOVPFPLN'," + cid + "," + bid + "); return false;\">View photos, floor plans and more</a></li>");
        
        //contentHtml += "<li><a href=\"" + siteRoot + "requestinfo/communitylist-" + cid + "/leadtype-com/builderid-" + bid + "/leadaction-cb\" onclick=\"Redirect(event,'" + siteRoot + "requestinfo/communitylist-" + cid + "/leadtype-com/builderid-" + bid + "/leadaction-cb'); return false;\">Contact <br />the builder</a></li>";
        
        contentHtml.push("<li><a href=\"");
        contentHtml.push(siteRoot);
        contentHtml.push("leadsconfirmrequest/communitylist-");
        contentHtml.push(cid);
        contentHtml.push("/leadtype-com/builder-");
        contentHtml.push(bid);
        contentHtml.push("/market-");
        contentHtml.push(marketId);
        contentHtml.push("/leadaction-hovrp\" onclick=\"LogAndRedirect(event,'");
        contentHtml.push(siteRoot);
        contentHtml.push("leadsconfirmrequest/communitylist-");
        contentHtml.push(cid);
        contentHtml.push("/leadtype-com/builder-");
        contentHtml.push(bid);
        contentHtml.push("/market-");
        contentHtml.push(marketId);
        contentHtml.push("/logevent-HOVRQPINFO");          
        contentHtml.push("/leadaction-hovrp', 'HOVRQPINFO'," + cid + "," + bid + "); return false;\">Request promotion and event information</a></li>");
        contentHtml.push("<li><a href=\"");
        contentHtml.push(siteRoot);
        contentHtml.push("leadsconfirmrequest/communitylist-");
        contentHtml.push(cid);
        contentHtml.push("/leadtype-com/builder-");
        contentHtml.push(bid);
        contentHtml.push("/market-");
        contentHtml.push(marketId);
        contentHtml.push("/leadaction-hovra\" onclick=\"LogAndRedirect(event,'");
        contentHtml.push(siteRoot);
        contentHtml.push("leadsconfirmrequest/communitylist-");
        contentHtml.push(cid);
        contentHtml.push("/leadtype-com/builder-");
        contentHtml.push(bid);
        contentHtml.push("/market-");
        contentHtml.push(marketId);
        contentHtml.push("/logevent-HOVRQAPPT");                                                
        contentHtml.push("/leadaction-hovra', 'HOVRQAPPT'," + cid + "," + bid + "); return false;\">Request an appointment</a></li>");
        
        if (currentZoom < 16)
        {
            contentHtml.push("<li><a href=\"javascript:void(0);\" onclick=\"ZoomToLatLngBC(");
            contentHtml.push(lat);
            contentHtml.push(",");
            contentHtml.push(lng);
            contentHtml.push(", ");
            contentHtml.push((currentZoom + 1));
            contentHtml.push(", ");
            contentHtml.push(cid);
            contentHtml.push(", ");                                    
            contentHtml.push(bid);            
            contentHtml.push(");\">Zoom in on map</a></li>");
        }        
        contentHtml.push("</ul>");
        contentHtml.push("<p><input type=\"submit\" value=\"Free brochure\" onclick=\"LogAndRedirect(event,'");
        contentHtml.push(siteRoot);
        contentHtml.push("leadsconfirmrequest/communitylist-");
        contentHtml.push(cid);
        contentHtml.push("/leadtype-com/builder-");
        contentHtml.push(bid);
        contentHtml.push("/market-");
        contentHtml.push(marketId);
        contentHtml.push("/logevent-HOVFBROCH");                                                
        contentHtml.push("/leadaction-hovfb', 'HOVFBROCH'," + cid + "," + bid + "); return false;\" class=\"btn btnFreeBrochure\" /></p>");
        contentHtml.push("</div>");
        
        var contentHtmlString = contentHtml.join("");
        
        mqPoint = new MQPoi(new MQLatLng(lat,lng), new MQMapIcon(nhsMapIcon));            
        mqPoint.setInfoTitleHTML("<strong>" + name + "</strong>");
        mqPoint.setInfoContentHTML(contentHtmlString);         
        mqPoint.setKey(cid+bid);        
        
        return mqPoint;
    }
    
    function CreateMultiCommPoi(numberComms,key,contentHtml,lat,lng)
    {
        //contentHtml = "Test Info";
        mqPoint = new MQPoi(new MQLatLng(lat,lng), new MQMapIcon(nhsMapIconMult));            
        mqPoint.setInfoTitleHTML("<strong>" + numberComms + " communities</strong>");
        mqPoint.setInfoContentHTML(contentHtml);         
        mqPoint.setKey(key);        
        
        return mqPoint;
    }
    
    function BuildCommPois(commArray)
    {
        commPois = new MQPoiCollection();
        
        var dedupArray = new Array();
        var temporaryArray = new Array().concat(commArray);
        
        var commArrayLength = commArray.length;
                
        // comparing comm array and de-dup array
        for (i=commArrayLength-1; i>=0; i--)
        {
            var ary = commArray[i].split(";");
            //alert(ary.length);
            
            var cid = ary[0];
            var bid = ary[1];            
            var lat = ary[13];
            var lng = ary[14];
            var tempNode = "";                 
            
            var totalMatch = 1; 
            
            var temporaryArrayLength = temporaryArray.length;           
            
            for (j=temporaryArrayLength-1; j>=0; j--)
            {
                var tAry = temporaryArray[j].split(";");
                var tCid = tAry[0];
                var tBid = tAry[1];           
                var tLat = tAry[13];
                var tLng = tAry[14];
                
                // if exact Lat-Long and not same Key                
                if (tLat == lat && tLng == lng)
                {
                    // increase match # and key, remove matching Poi
                    totalMatch++;                    
                    tempNode += temporaryArray[j];
                    temporaryArray.splice(j,1);                  
                }        
            }            
            //tempNode += tempArray[i];
            temporaryArray.splice(i,1);
            if (tempNode != "")
            {
                dedupArray.push(tempNode);
            }            
        }
        //alert("Comm Array: " + commArray.length);
        //alert("DeDup Array: " + dedupArray.length);
        dedupArray.reverse();
        commArray = dedupArray;        
        commArrayLength = commArray.length;
        
        // create Pois and add to PoiCollection
        for(i=0; i<commArrayLength; i++)
        {
            var cAry = commArray[i].split(";");
            var cAryLength = cAry.length;
            
            if (cAryLength == 16) 
            {            
                var cid = cAry[0];
                var bid = cAry[1];
                var name = cAry[2];
                var cty = cAry[3];
                var st = cAry[4];
                var zip = cAry[5];
                var img = cAry[6];
                var brandimg = cAry[7];
                var price = cAry[8];
                var brand = cAry[9];
                var matches = cAry[10];
                var promoid = cAry[11];
                var type = cAry[12];
                var lat = cAry[13];
                var lng = cAry[14];
                     
                var commPoint = createCommPoi(cid,bid,name,cty,st,zip,img,brandimg,price,brand,matches,promoid,type,lat,lng);    
                          
            }
            else if (cAryLength > 16)
            {               
                var commNumber = ((cAryLength-1)/15);
                var key = "mkey"; // multipoi key
                
                if (commNumber > 2)
                {
                    var contentHtml = new Array();
                    contentHtml.push("<div class=\"nhsMultiBoxTitle\">");
                    contentHtml.push(commNumber);
                    contentHtml.push(" communities</div>");
                    contentHtml.push("<div class=\"nhsMultiHoverBox nhsMultiBoxScroll\"><div class=\"nhsMultiScroll\">");
                }
                else
                {
                    var contentHtml = new Array();
                    contentHtml.push("<div class=\"nhsMultiHoverBox\">");
                }
                
                for (j=0; j<commNumber; j++)
                {
                    var k = 15 * j;
                    var cid = cAry[(0+k)];
                    var bid = cAry[(1+k)];
                    var name = cAry[(2+k)];
                    var cty = cAry[(3+k)];
                    var st = cAry[(4+k)];
                    var zip = cAry[(5+k)];
                    var img = cAry[(6+k)];
                    var brandimg = cAry[(7+k)];
                    var price = cAry[(8+k)];
                    var brand = cAry[(9+k)];
                    var matches = cAry[(10+k)];
                    var promoid = cAry[(11+k)]; //not used
                    var type = cAry[(12+k)]; //not used
                    var lat = cAry[(13+k)];
                    var lng = cAry[(14+k)];
                    key += "_"+cid+bid;
                    
                    contentHtml.push("<div class=\"nhsMultiHoverRow\">");
                    contentHtml.push("<h3><a href=\"");
                    contentHtml.push(siteRoot);
                    contentHtml.push("communitydetail/community-");
                    contentHtml.push(cid);
                    contentHtml.push("/builder-");
                    contentHtml.push(bid);
                    contentHtml.push("\" onclick=\"LogAndRedirect(event,'");
                    contentHtml.push(siteRoot);
                    contentHtml.push("communitydetail/community-");
                    contentHtml.push(cid);
                    contentHtml.push("/builder-");
                    contentHtml.push(bid);
                    contentHtml.push("/logevent-HOVNAME");                    
                    contentHtml.push("', 'HOVNAME'," + cid + "," + bid + "); return false;\">");
                    contentHtml.push(name);
                    contentHtml.push("</a></h3>");
                    
                    if (img == "N" || img == "n" || img == "")
                    {
                        img = resourceRoot + "globalresources/default/images/small_nophoto.gif";
                    }
                     else
                      {
                        img = resourceRoot.substring(0, resourceRoot.length - 1) + img; //remove trailing slash
                      }
                    contentHtml.push("<div class=\"nhsResultsThumb\"><a href=\"");
                    contentHtml.push(siteRoot);
                    contentHtml.push("communitydetail/community-");
                    contentHtml.push(cid);
                    contentHtml.push("/builder-");
                    contentHtml.push(bid);
                    contentHtml.push("\" onclick=\"LogAndRedirect(event,'");
                    contentHtml.push(siteRoot);
                    contentHtml.push("communitydetail/community-");
                    contentHtml.push(cid);
                    contentHtml.push("/builder-");
                    contentHtml.push(bid);
                    contentHtml.push("/logevent-HOVNAME");                                        
                    contentHtml.push("', 'HOVNAME'," + cid + "," + bid + "); return false;\"><img src=\"");
                    contentHtml.push(img);
                    contentHtml.push("\" alt=\"");
                    contentHtml.push(name);
                    contentHtml.push("\" /></a></div>");
                    contentHtml.push("<p class=\"nhsResultsBuilder\">by ");
                    contentHtml.push(brand);
                    contentHtml.push("</p>");
                    
                    if(price != "$ - $")     
                    {
                        contentHtml.push("<p class=\"nhsResultsPrice\">From ");
                        contentHtml.push(price);
                        contentHtml.push("</p>");
                    }
                    contentHtml.push("<p><a href=\"");
                    contentHtml.push(siteRoot);
                    contentHtml.push("leadsconfirmrequest/communitylist-");
                    contentHtml.push(cid);
                    contentHtml.push("/leadtype-com/builder-");
                    contentHtml.push(bid);
                    contentHtml.push("/market-");
                    contentHtml.push(marketId);
                    contentHtml.push("/leadaction-hovfb\" onclick=\"LogAndRedirect(event,'");
                    contentHtml.push(siteRoot);
                    contentHtml.push("leadsconfirmrequest/communitylist-");
                    contentHtml.push(cid);
                    contentHtml.push("/leadtype-com/builder-");
                    contentHtml.push(bid);
                    contentHtml.push("/market-");
                    contentHtml.push(marketId);
                    contentHtml.push("/logevent-HOVFBROCH");                                        
                    contentHtml.push("/leadaction-hovfb', 'HOVFBROCH'," + cid + "," + bid + "); return false;\">");
                    contentHtml.push("<img src=\"");
                    contentHtml.push(resourceRoot);
                    contentHtml.push("GlobalResources/Default/images/buttons/btn_brochure.gif\" alt=\"Free Brochure\" />");
                    contentHtml.push("</a></p>");
                    contentHtml.push("</div>");                    
                }
                
                /*
                contentHtml.push("<p><input type=\"submit\" value=\"Free brochure\" onclick=\"Redirect(event,'");
                    contentHtml.push(siteRoot);
                    contentHtml.push("leadsconfirmrequest/communitylist-");
                    contentHtml.push(cid);
                    contentHtml.push("/leadtype-com/builder-");
                    contentHtml.push(bid);
                    contentHtml.push("/market-");
                    contentHtml.push(marketId);
                    contentHtml.push("/leadaction-hovfb'); return false;\" class=\"btn btnFreeBrochure\" /></p>");     
                */
             
                contentHtml.push("<br style=\"clear: both;\" />");
                
                if (commNumber > 2)
                {
                    contentHtml.push("</div></div>");
                }
                else
                {
                    contentHtml.push("</div>");
                }
                
                var contentHtmlString = contentHtml.join("");
                
                commPoint = CreateMultiCommPoi(commNumber,key,contentHtmlString,lat,lng);
            }
            commPois.add(commPoint);            
        }          
        return commPois;
    }  

    function onError(error)
    {
        HandleError(error.get_message(), '', -1, "onError");
    }
    
    // Redirect to error page, it will log the error
    function HandleError(message, url, line, fromWhere)
    {
        var errorUrl = siteRoot + 'Error/?Message=' +  GetErrorMessage(message, url, line,fromWhere);
        MakeHttpReq(errorUrl);
    }

    // Create an error description and attach extra client information to log
    function GetErrorMessage(errorMsg, url, line, fromWhere)
    {
        var message = 'Message: ' + errorMsg + ' \n';
        
         message += 'Url: ' + window.location.href + ' \n';
         message += 'Line: ' + line + ' \n';
         message += 'CalledFrom:' + fromWhere;
        return message;
    }
        
    //*************************************************************************
    // Helper Functions
    //*************************************************************************  
   function toggleDisplay(linkId,popId) {
        var pop = document.getElementById(popId);
        var link = document.getElementById(linkId);

        if(pop.className == 'nhsPopOff') 
        {
            pop.className = 'nhsPopOn';
            link.className = 'nhsFacetOn';
        } 
        else 
        {
            pop.className = 'nhsPopOff';
            link.className = 'nhsFacetOff';	        
        }
    }       

    function getChildNodeValue(parentNode, name)
    {
        if(parentNode.getElementsByTagName(name)[0].childNodes.length > 0)
        {
            return parentNode.getElementsByTagName(name)[0].childNodes[0].nodeValue;
        }
        else
        {     
            return "";
        }
    }

    function LogAndRedirect(e, url, eventCode, cid, bid)
    {
        LogEvent(eventCode, cid, bid);
        Redirect(e, url);
    }
    
    // Redirect function
    function Redirect(e, url, newWin)
    {        
	    if (!e) var e = window.event;
	    e.cancelBubble = true;
	    if (e.stopPropagation) e.stopPropagation();
	    if (url)	       
	    {
	        if (newWin)
	        {
	            window.open(url);
	        }
	        else
	        {
	            window.location.href=url;
	        }
	    }
    }
        
    function LogEvent(eventCode,cid,bid)
        {
        if(eventCode)
        {
            var url = siteRoot + "eventlogger/logevent-" + eventCode;        
            
            if (siteRoot.substring(siteRoot.length - 1) != "/")
            {
                url = "/" + url;
            }
            LogView(url, eventCode, cid, bid);            
        }       
        }
        
    function LogView(url, eventCode, cid, bid)
    {
        var wturl = url;
        var fromPageUrl = siteRoot + "communityresults";
        if(siteRoot=='/')
        {
            wturl = "/NewHomeSource" + url;
            fromPageUrl = "/NewHomeSource/communityresults";
        }
        
        //Log to custom via http req 
        url = url + "/builder-" + bid + "/community-" + cid + "/market-" + marketId;        
        MakeHttpReq(url);
    }
    
    function MakeHttpReq(url)
    {
        var xmlhttp;
        xmlhttp = null;
        // code for Mozilla, etc.
        if (window.XMLHttpRequest)
        {
            xmlhttp=new XMLHttpRequest();
        }
        // code for IE
        else if (window.ActiveXObject)
        {
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
     
        if (xmlhttp != null)
        {        
            xmlhttp.open("GET",url,true);
            xmlhttp.send(null);
        }
    }
    
    function getQueryStringValue( name )
    {
        var returnValue = "";
        
        for( var x = pageUrl.indexOf(name) + 6; x< pageUrl.length; x++ )
        {
            var temp = pageUrl.substr(x,1);
            
            if( temp != "-" && temp != "/" )
            {
              returnValue += temp;
            }
            else if( temp == "/" )
            {
              break;
        }       
        }
        
        return returnValue;
    }
        
// Tip Box
    function closeTip()    
    {
        var checkValue = document.getElementById('nhsDontShowCheck').checked;
        
        if (checkValue != false) // Box checked
        { 
            // Set persistant cookie
            createCookie('nhsMapTipPersist','hide',1825);
            //alert('Persistant cookie set');
        }
        else // Box unchecked
        {
            // Set session cookie
            createCookie('nhsMapTipSession','hide',0);
            //alert('Session cookie set');
        }
        document.getElementById('nhsMapTipBox').style.display = "none";            
    }

// Check cookies and display map tip    
    function displayMapTip()
    {    
        var persistCookieValue = readCookie('nhsMapTipPersist');
        var sessionCookieValue = readCookie('nhsMapTipSession');
        var elementToDisplay = document.getElementById('nhsMapTipBox');
        
        if ( (persistCookieValue) || (sessionCookieValue) )
        { // null check
            if ((persistCookieValue == "hide") || (sessionCookieValue == "hide"))
            {
                elementToDisplay.style.display = "none";
            }            
        } 
        else 
        {           
            elementToDisplay.style.display = "block";
            elementToDisplay.style.left = (document.getElementById('nhsCommResMapTools').offsetWidth / 2 ) - (elementToDisplay.offsetWidth / 2) + "px";           
        }
    }
    
// Cookie Functions
    function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
    }

    function readCookie(name) {
	    var nameEQ = name + "=";
	    var ca = document.cookie.split(';');
	    for(var i=0;i < ca.length;i++) {
		    var c = ca[i];
		    while (c.charAt(0)==' ') c = c.substring(1,c.length);
		    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	    }
	    return null;
    }

    function eraseCookie(name) {
	    createCookie(name,"",-1);
    }  
    
    
// Register Event Handlers
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_pageLoading(AsyncPageLoad);
    prm.add_beginRequest(BeginRequestHandler);
    prm.add_endRequest(EndRequestHandler); 

function BeginRequestHandler(sender, args) {
    var elemName = args.get_postBackElement().id; 
    var request = args.get_request(); 
    request.set_userContext(elemName);

}
function EndRequestHandler(sender, args)
{
    if (typeof args.get_error() != "undefined") 
    {        
        if (args.get_error() && (args.get_response().get_timedOut() || args.get_error().name === 'Sys.WebForms.PageRequestManagerServerErrorException'))
        {
            var response = args.get_response(); 
            //HandleError(args.get_error(),response._webRequest.get_userContext());
            HandleError(args.get_error().name, '', -1,"EndRequestHandler");
            // remember to set errorHandled = true to keep from getting a popup from the AJAX library itself
            args.set_errorHandled(true);
        }
    }
    
    // Update client-side variables
    viewOnMapLinkScript = document.getElementById("viewOnMapLinkScript").innerHTML; 
    // Run script
    AutoViewOnMapLink()   
}

function AutoViewOnMapLink()
{
    if (viewOnMapLinkScript != "")
    {
        eval(viewOnMapLinkScript);
        hideMapStatus();
    }
}

Sys.Net.WebServiceProxy.retryOnFailure = 
    function(result, userContext, methodName, retryParams, onFailure)
{
    if( result.get_timedOut() )
    {
        if( typeof retryParams != "undefined" )
        {
            //debug.trace("Retry: " + methodName);
            Sys.Net.WebServiceProxy.original_invoke.apply(this, retryParams );
        }
        else
        {
            if( onFailure ) onFailure(result, userContext, methodName);
        }
    }
    else
    {
        if( onFailure ) onFailure(result, userContext, methodName);
    }
}

Sys.Net.WebServiceProxy.original_invoke = Sys.Net.WebServiceProxy.invoke;
Sys.Net.WebServiceProxy.invoke = 
    function Sys$Net$WebServiceProxy$invoke(servicePath, methodName, useGet, 
        params, onSuccess, onFailure, userContext, timeout)
{       
    var retryParams = [ servicePath, methodName, useGet, params, 
        onSuccess, onFailure, userContext, timeout ];
    
    // Call original invoke but with a new onFailure
    // handler which does the auto retry
    var newOnFailure = Function.createDelegate( this, 
        function(result, userContext, methodName) 
        { 
            Sys.Net.WebServiceProxy.retryOnFailure(result, userContext, 
                methodName, retryParams, onFailure); 
        } );
        
    Sys.Net.WebServiceProxy.original_invoke(servicePath, methodName, useGet, 
        params, onSuccess, newOnFailure, userContext, timeout);
}
