﻿



function init() {
    if ($p('price_slider') != null) {
        startPriceSlider();
    }
}

function startPriceSlider() {
    var price_slider = $p('price_slider');
    var box = $p('zoom_element');
    var tick_values = [4, 10, 17, 23, 29, 36, 42, 48, 55, 61, 67, 74, 80, 86, 93, 99, 105, 112, 118, 124, 130, 137];
    var dol_values = ['Under $100K', '$120,000', '$140,000', '$160,000', '$180,000', '$200,000', '$220,000', '$240,000', '$260,000', '$280,000', '$300,000', '$325,000', '$350,000', '$375,000', '$400,000', '$450,000', '$500,000', '$600,000', '$700,000', '$800,000', '$900,000', '$1.0M & Up'];
    var actual_values = ['100000', '120000', '140000', '160000', '180000', '200000', '220000', '240000', '260000', '280000', '300000', '325000', '350000', '375000', '400000', '450000', '500000', '600000', '700000', '800000', '900000', '1000000'];
    var hdnHigh = $p('hdnHighPrice');
    var hdnLow = $p('hdnLowPrice');
    var priceLow;
    var priceHigh;
    var lo;
    var hi;
    // Low Price 
    for (var i = 0; i < actual_values.length; i++) {

        var iActualVal = parseFloat(actual_values[i]);

        if (parseFloat(hdnLow.value) == iActualVal) {
            //alert("low equal");
            break;
        }
        if (parseFloat(hdnLow.value) < iActualVal) {
            //alert("less than");
            if (i != 0) {
                hdnLow.value = parseFloat(actual_values[i - 1]);
            }
            else {
                hdnLow.value = iActualVal;
            }
            break;
        }
        if ((i == (actual_values.length - 1)) && (parseFloat(hdnLow.value) > iActualVal)) {
            hdnLow.value = parseFloat(actual_values[i - 1]);
            break;
        }
    }

    if (hdnLow.value == '0') {
        lo = 4;
        priceLow = dol_values[tick_values.indexOf(lo)]
    }
    else {
        lo = tick_values[actual_values.indexOf(hdnLow.value)];
        priceLow = dol_values[actual_values.indexOf(hdnLow.value)]
    }
    $p('lowValue').innerHTML = "<span class='valueSpan'>From</span><br /> " + priceLow;
    //hdnLow.value = actual_values[tick_values.indexOf(lo)];	

    // High Price 
    for (var i = actual_values.length; i > 0; i--) {

        var iActualVal = parseFloat(actual_values[i - 1]);

        if (parseFloat(hdnHigh.value) == iActualVal) {
            //alert("high equal");
            break;
        }
        if (parseFloat(hdnHigh.value) > iActualVal) {
            //alert("greater than");
            if (i != actual_values.length) {
                hdnHigh.value = parseFloat(actual_values[i]);
            }
            else {
                hdnHigh.value = iActualVal;
            }
            break;
        }
        if ((parseFloat(hdnHigh.value) != 0) & (i == 1) & (parseFloat(hdnHigh.value) < iActualVal)) {
            hdnHigh.value = parseFloat(actual_values[i]);
            break;
        }
    }

    if (hdnHigh.value == '0') {
        hi = 137;
        priceHigh = dol_values[tick_values.indexOf(hi)]
    }
    else {
        hi = tick_values[actual_values.indexOf(hdnHigh.value)];
        priceHigh = dol_values[actual_values.indexOf(hdnHigh.value)]
    }

    $p('highValue').innerHTML = "<span class='valueSpan'>To</span><br /> " + priceHigh;
    //hdnHigh.value = actual_values[tick_values.indexOf(hi)];	

    new Control.Slider(price_slider.select('.nhsHandle'), price_slider, {
        range: $R(0, 142),
        values: tick_values,
        sliderValue: [lo, hi],
        onSlide: function(values) {
            var lowIndexValue = values[0];
            var lowIndex = tick_values.indexOf(lowIndexValue);
            var hiIndexValue = values[1];
            var hiIndex = tick_values.indexOf(hiIndexValue);
            $p('lowValue').innerHTML = "<span class='valueSpan'>From</span><br /> " + dol_values[lowIndex];
            $p('highValue').innerHTML = "<span class='valueSpan'>To</span><br /> " + dol_values[hiIndex];
        },
        onChange: function(values) {
            var lowIndexValue = values[0];
            var lowIndex = tick_values.indexOf(lowIndexValue);
            var hiIndexValue = values[1];
            var hiIndex = tick_values.indexOf(hiIndexValue);
            $p('lowValue').innerHTML = "<span class='valueSpan'>From</span><br /> " + dol_values[lowIndex];
            $p('highValue').innerHTML = "<span class='valueSpan'>To</span><br /> " + dol_values[hiIndex];
            // re-factor
            $p('hdnHighPrice').value = actual_values[hiIndex];
            $p('hdnLowPrice').value = actual_values[lowIndex];
        },
        restricted: true
    });
}