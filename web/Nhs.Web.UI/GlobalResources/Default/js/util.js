
//Some variables for static content overlay
var globalShowOver;
var globalStaticContent;
var globalPaths;



function IsValidZipCommon(zip) {

    // Check for correct zip code
    var reZip = new RegExp(/(^\d{5}$)|(^\d{5}-\d{4}$)/);

    if (!reZip.test(zip)) {
        // alert("Zip Code Is Not Valid");
        return false;
    }

    return true;
}

function PrePopValuesAccount(zBoxid, mkBoxid, hdnBoxid, cBoxid, sBoxid, hdnBoxCid, hdnBoxSid) {
    var enteredZip = "";

    jQuery(zBoxid).keyup(function () {
        enteredZip = jQuery(zBoxid).val();
        if (IsValidZipCommon(enteredZip)) {
            FillHomeMarketUpdate(enteredZip, mkBoxid, hdnBoxid, cBoxid, sBoxid, hdnBoxCid, hdnBoxSid);
        } else {
            jQuery(mkBoxid).val("");
            jQuery(cBoxid).val("");
            jQuery(sBoxid).val("");
            jQuery(hdnBoxCid).val("");
            jQuery(hdnBoxSid).val("");
        }
    });
}

function FillHomeMarketUpdate(zip, mkBoxid, hdnBoxid, cBoxid, sBoxid, hdnBoxCid, hdnBoxSid) {
    try {
        var homeMarketBox = $(mkBoxid);
        var homeMarketId = $(hdnBoxid);
        var cityBox = $(cBoxid);
        var stateBox = $(sBoxid);
        var cityBoxH = $(hdnBoxCid);
        var stateBoxH = $(hdnBoxSid);


        if (zip != null && zip != '') {
            $.ajax({
                type: "GET",
                url: "/Account/GetHomeMarketInfo",
                data: { zip: zip },
                success: function (data) {
                    if (data != null && data != '') {
                        homeMarketBox.val(data.split(',')[0]);
                        homeMarketId.val(data.split(',')[1]);
                        cityBox.val(data.split(',')[2]);
                        stateBox.val(data.split(',')[3]);
                        cityBoxH.val(data.split(',')[2]);
                        stateBoxH.val(data.split(',')[3]);
                    }
                }
            });
        }
    }
    catch (ex) {

    }

}    


//Sets the static overlay
function jqStaticOverlay() {        
    if (globalShowOver == "True") {
        /*$("#stcontent").show();
        $("#staticList").html(globalStaticContent);
        $("#stclose").click(function () {
            $('#stcontent').hide();
        });*/
        HighlightRegion(globalPaths);        
    }
}

//Gets the path for the file used in the static content
function getThePath(ps, tit) {
    var res;
    for (var i = 0; i < ps.length; i++) {
        var elem = ps[i].replace("\\", "//");
        tit = tit.replace("/", "\\").toLowerCase();         
        if (elem.toLowerCase().indexOf(tit) != -1 && (tit != "")) {
            res = elem;
            break;
        }
    }
    return res;
}

//Identify static content regions in the page
function HighlightRegion(listDif) {

    var list = new Array();
    list = listDif.split(',');
    var paths = new Array();
    var num = 1;
    var ind;
    var path;

    for (var i = 0; i < list.length; i++) {
        var elem = list[i].split(';');
        paths.push(elem[1]);
    }

    $("div").each(function () {
        var cn = $(this).attr("class");
        if (cn == "nhsStaticContent") {
            $(this).addClass("nhs_Overlay");
            
            $(this).mouseover(function () {            
                $(this).removeClass("nhs_Overlay_Out");
                $(this).addClass("nhs_Overlay_Over");

                var stnum = $(this).children().children("[class*='stNumber']");
                tempNum = $(stnum).html();
                var tit = $(stnum).attr("title");
                $(stnum).html(tempNum + "   " + "<b class=\"stNumberPath\">" + getThePath(paths, tit) + "</b>");
            });

            $(this).mouseout(function () {
                $(this).removeClass("nhs_Overlay_Over");
                $(this).addClass("nhs_Overlay_Out");

                var stnum = $(this).children().children("[class*='stNumber']");
                $(stnum).html(tempNum);
            });
        }
    });    

    $("a").each(function () {
        var cn2 = $(this).attr("class");
        var num = 1;
        if (cn2 == "stmark") {
            $(this).css("display", "block");            
        }
    });
    

    $("b").each(function () {
        var cn3 = $(this).attr("class");
        if (cn3 == "stNumber") {
            
            $(this).html(num);            

            if (num > 9)
                $(this).css("left", "8px");            
            num = num + 1;            
        }
    });
}



function toggleBoxDisplay(popId,popId2) {
    var pop1 = $p(popId);
    var pop2 = $p(popId2);
    
    if (pop1 != null && pop2 != null) {
        pop1.style.display = (pop1.style.display == "none") ? "inline" : "none";
        pop2.style.display = (pop2.style.display == "none") ? "inline" : "none";
    }
}

var FieldDefaultValue = Class.create({
  initialize: function(fieldId, defaultValue, defaulValueClass, submitButtonId, submitButton2Id) {
        this.field = $p(fieldId);
        		
        //Set value initially if none are specified
        if(this.field.value == '') {
            this.field.addClassName(defaulValueClass);
	        this.field.value=defaultValue;
        }

        // Remove values on focus
        this.field.observe('focus', function() {
            if (this.value==defaultValue) {
                this.removeClassName(defaulValueClass);
                this.value='';
            }
        });

        // Place values back on blur
        this.field.observe('blur', function() {
            if(this.value=='') {
                this.addClassName(defaulValueClass);
	            this.value=defaultValue;
	        }
        });
        
        //Capture form submission
		$p(submitButtonId).observe('click', function() {
		    var field = $p(fieldId);
			if(field != null && field.value==defaultValue) {
				field.value='';
			}
		});
		if (submitButton2Id != null) {
		    this.submitButton2 = $p(submitButton2Id);
            this.submitButton2.observe('click', function() {
		        var field = $p(fieldId);
			    if(field != null && field.value==defaultValue) {
				    field.value='';
			    }
		    });
		}
  }
});

function stopBubble(e) 
{
    if (!e) var e = window.event;
    e.cancelBubble = true;
    if (e.stopPropagation) e.stopPropagation();
    //alert("bubble stop");
}
