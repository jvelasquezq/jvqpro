/* ---------------------------------------------
Title:      Quick Connect Results JS
Updated:    May 11, 2011
----------------------------------------------- */
    
    //*************************************************************************
    // PAGE VARIABLES 
    //*************************************************************************    
    var nhsMqMap;
    var mqOrigCenter;
    var mqCenter;
    var mqZoom;
    var mqPoiCollection;
    var mqResultsPoiCollection;
    var prevBrowserSize;  
    var nhsMapIcon;
    var nhsMapIconMult;
    var nhsIconMultOffset;
    var nhsMapIconShadow;
    var updateMapFlag = true;
    var brandPartnerId = document.getElementById('brandPartnerId').value;
    var siteRoot = document.getElementById('siteRoot').value;
    var resourceRoot = document.getElementById('resourceRoot').value;
    
    if (brandPartnerId == 333) {
        //move & move partners
        var mqIconPath = resourceRoot + "globalresources/move/images/map/marker.gif";
        var mqIconMultPath = resourceRoot + "globalresources/move/images/map/marker_multiple.gif";
        var mqIconMktPath = resourceRoot + "globalresources/move/images/map/marker_market.gif";
    } else {
        //nhs & nhs partners
        var mqIconPath = resourceRoot + "globalresources/default/images/map/marker.gif";
        var mqIconMultPath = resourceRoot + "globalresources/default/images/map/marker_multiple.gif";
        var mqIconMktPath = resourceRoot + "globalresources/default/images/map/mkt_icon.gif";
    }
    var mqIconShadowPath = resourceRoot + "globalresources/default/images/map/marker_shadow.png";
    
    var pageUrl = document.location.href;
    var iteration = 0;

//*************************************************************************
// Check for DOM Loaded from
// Dean Edwards/Matthias Miller/John Resig
// http://dean.edwards.name/weblog/2006/06/again/
//************************************************************************* 

    function init() 
    {        
        //alert($p('radius_slider'));
        if ($p('radius_slider') != null) {
            startRadiusSlider();
        }
        
        // skip if this function has already been called
        if (!arguments.callee.done) {
            // do stuff
            //window.alert("Dom Loaded");
            PageStart();     
        } else {
            //alert('Just update map');
            updateMapPoints();
        } 
        // flag this function so we don't do the same thing twice
        arguments.callee.done = true;
    }
    
    pageLoad = function()
    {
        init();
    }
    
    //*************************************************************************
    // EVENT HANDLERS
    //*************************************************************************

    function PageStart()
    {                                                         
        resizeMapDiv(); 
        
        mapCenterLat = document.getElementById('mqCenterLat').value;        
        mapCenterLng = document.getElementById('mqCenterLng').value;

        //mqZoom = 6;             
        mqZoom = document.getElementById('mqZoomLevel').value;
        
        mqOrigCenter = new MQA.LatLng(mapCenterLat, mapCenterLng);
        mqCenter = mqOrigCenter;
    
        // Create the map object
        nhsMqMap = new MQA.TileMap(document.getElementById('nhsQCResMapDiv'), mqZoom, mqOrigCenter, 'map');

        nhsMapIconShadow = new MQA.Icon(mqIconShadowPath,0,0);       
        
        //Standard NHS Community Icon
        nhsMapIcon = new MQA.Icon(mqIconPath,20,26);
        mqIconOffset = new MQA.Point(-10,-26);      
        
        //NHS Multiple Community Icon
        nhsMapIconMult = new MQA.Icon(mqIconMultPath,28,28);
        mqIconMultOffset = new MQA.Point(-14,-28);       
        
        mqZoomControl = new MQA.ZoomControl();
        nhsMqMap.addControl(mqZoomControl, new MQA.MapCornerPlacement(MQA.MapCorner.TOP_LEFT, new MQA.Size(10,21)));
        
        setMapWindowCoordinates();
        updateMapPoints();                             
    }        
    
    function AsyncPageLoad(sender, args) 
    {
        var dataItems = args.get_dataItems();
                
        try 
        {   
            if(dataItems[litSearchParams])
            {
                xmlSearchParams = dataItems[litSearchParams];        
                
                if(dataItems[litNewMapCenterLat])
                {
                    newMapLat = dataItems[litNewMapCenterLat];
                    newMapLng = dataItems[litNewMapCenterLng];
                    newMapZoom = dataItems[litNewMapZoom];
                    var startTime = new Date().getTime();
                    while (typeof nhsMqMap == "undefined")
                    {
                        var currentTime = new Date().getTime();
                        var diffTime = currentTime - startTime;
                        if (diffTime > 5000) 
                        {
                            break;
                        }
                    }
                    if (nhsMqMap)
                    {
                    ZoomToLatLng(newMapLat,newMapLng,newMapZoom);                    
                    }
                }
                else
                {   
                    var startTime = new Date().getTime();
                    while (typeof nhsMqMap == "undefined")
                    {
                        //alert("while");
                        var currentTime = new Date().getTime();
                        var diffTime = currentTime - startTime;
                        if (diffTime > 5000) 
                        {
                            break;
                        }
                    }
                    if (nhsMqMap)
                    {
                        updateMapPoints();                    
                    }
                }                
            }   
        }
        catch (err)
        {
           if(typeof err != "undefined")
           {
            //alert(err.message);
            HandleError(err.message, '', -1, "AsyncPageLoad");
           }
        }
    }
    
    //*************************************************************************
    // MAP FUNCTIONS
    //*************************************************************************  
    function RenderPoiCollection(poiCollection)
    {
        nhsMqMap.replaceShapes(poiCollection);    
        hideMapStatus();
    }    
    
    function hideMapStatus()
    {
        document.getElementById("mapLoading").style.display = "none";
    }
    
    function showMapStatus()
    {
        document.getElementById("mapLoading").style.display = "block";
    }
    
    function updateMapPoints()
    {                              
        minLat = document.getElementById("mqMinLat").value;
        minLng = document.getElementById("mqMinLng").value;
        maxLat = document.getElementById("mqMaxLat").value;
        maxLng = document.getElementById("mqMaxLng").value;
        logonName = document.getElementById("logonName").value;
        currentZoom = nhsMqMap.getZoomLevel();
        //window.alert("Calling Service!");
        radius = $p('hdnRadius').value;
        getCommMapPois(xmlSearchParams, minLat, minLng, maxLat, maxLng, radius, logonName);                   
    }    
    
    function resizeMapDiv()        
    {
        newMapSize = getMapSize();
        document.getElementById('nhsQCResMapDiv').style.width = newMapSize.getWidth() + "px";
        document.getElementById('nhsQCResMapDiv').style.height = newMapSize.getHeight() + "px"; 
    }
    
    function getMapSize()
    {
        mapToolsContainer = document.getElementById('nhsQCResMapDiv');        
        mapWidth = mapToolsContainer.offsetWidth - 2;
        mapHeight = 500;                
        return new MQA.Size(mapWidth,mapHeight);        
    }        
                 
    function setMapWindowCoordinates()
    {
        mpSize = getMapSize();        
        topLeftLL = nhsMqMap.pixToLL(new MQA.Point(0,0));
        bottomRightLL = nhsMqMap.pixToLL(new MQA.Point(mpSize.getWidth(), mpSize.getHeight()));               
        
        // Set the hidden form values to the current map window coordinates. 
        document.getElementById("mqMinLat").value = topLeftLL.getLatitude();
        document.getElementById("mqMinLng").value = topLeftLL.getLongitude();
        document.getElementById("mqMaxLat").value = bottomRightLL.getLatitude();
        document.getElementById("mqMaxLng").value = bottomRightLL.getLongitude(); 
        document.getElementById("mqCenterLat").value = nhsMqMap.getCenter().getLatitude();
        document.getElementById("mqCenterLng").value = nhsMqMap.getCenter().getLongitude();  
        document.getElementById("mqZoomLevel").value = nhsMqMap.getZoomLevel();
    }
    
    function ZoomToLatLng(lat, lng, zoomLevel)
    {        
        nhsMqMap.setCenter(new MQA.LatLng(lat,lng));
        nhsMqMap.setZoomLevel(zoomLevel);
    }          
   
    //*************************************************************************
    // Web service calls
    //*************************************************************************  
    function getCommMapPois(xmlSearchParams, minLat, minLng, maxLat, maxLng, radius, logonName)
    {
        Nhs.Web.ServiceProxy.NhsServices.GetQuickCMapPoints(xmlSearchParams, minLat, minLng, maxLat, maxLng, radius, logonName, onCommPoiSuccess, onError);    
    }

    function onCommPoiSuccess(result)
    {   
        //StartTiming();
        //window.alert("Start Updating POIs");
        mqPoiCollection = BuildCommPois(result);
        //window.alert("Collection Built");
        RenderPoiCollection(mqPoiCollection);
        //StopTiming();
        //window.alert("End Updating POIs");
    }
    
    function createCommPoi(cid,bid,name,cty,st,zip,img,brandimg,price,brand,matches,promoid,type,lat,lng,market)
    {
        var currentZoom = nhsMqMap.getZoomLevel();

        if(img == "N" || img == "n" || img == "")
        {
            img = resourceRoot + "globalresources/default/images/small_nophoto.gif";
        }
        else
        {
          img = resourceRoot.substring(0, resourceRoot.length - 1) + img; //remove trailing slash
        }
        
        var contentHtml = new Array();
        
        contentHtml.push("<div class=\"nhsCommResHoverBox\">");
        contentHtml.push("<h3>");
        contentHtml.push(name)
        contentHtml.push("</h3>");
        contentHtml.push("</div>");
        
        var contentHtmlString = contentHtml.join("");
        
        var mqPointLatLng = new MQA.LatLng(lat,lng);
        //var mqPointIcon = new MQA.Icon(nhsMapIcon);        
        mqPoint = new MQA.Poi(mqPointLatLng, nhsMapIcon);
        mqPoint.setValue('iconOffset',mqIconOffset);
        mqPoint.setValue('shadow',nhsMapIconShadow);        
        mqPoint.setValue('infoTitleHTML',"<strong>" + name + "</strong>");
        mqPoint.setValue('infoContentHTML',contentHtmlString);         
        mqPoint.setValue('key',cid+bid);        
        
        return mqPoint;
    }
    
    function CreateMultiCommPoi(numberComms,key,contentHtml,lat,lng)
    {
        //contentHtml = "Test Info";
        var mqPointLatLng = new MQA.LatLng(lat,lng);
        mqPoint = new MQA.Poi(mqPointLatLng, nhsMapIconMult); 
        mqPoint.setValue('iconOffset',mqIconMultOffset);
        mqPoint.setValue('shadow',nhsMapIconShadow);            
        mqPoint.setValue('infoTitleHTML',"<strong>" + numberComms + " communities</strong>");
        mqPoint.setValue('infoContentHTML',contentHtml);         
        mqPoint.setValue('key',key);        
        
        return mqPoint;
    }
    
    function BuildCommPois(commArray)
    {
        commPois = new MQA.ShapeCollection();
        
        var dedupArray = new Array();
        var temporaryArray = new Array().concat(commArray);
        
        var commArrayLength = commArray.length;
                
        // comparing comm array and de-dup array
        for (i=commArrayLength-1; i>=0; i--)
        {
            var ary = commArray[i].split(";");
            //alert(ary.length);
            
            var cid = ary[0];
            var bid = ary[1];            
            var lat = ary[13];
            var lng = ary[14];
            var tempNode = "";                 
            
            var totalMatch = 1; 
            
            var temporaryArrayLength = temporaryArray.length;           
            
            for (j=temporaryArrayLength-1; j>=0; j--)
            {
                var tAry = temporaryArray[j].split(";");
                var tCid = tAry[0];
                var tBid = tAry[1];           
                var tLat = tAry[13];
                var tLng = tAry[14];
                
                // if exact Lat-Long and not same Key                
                if (tLat == lat && tLng == lng)
                {
                    // increase match # and key, remove matching Poi
                    totalMatch++;                    
                    tempNode += temporaryArray[j];
                    temporaryArray.splice(j,1);                  
                }        
            }            
            //tempNode += tempArray[i];
            temporaryArray.splice(i,1);
            if (tempNode != "")
            {
                dedupArray.push(tempNode);
            }            
        }
        //alert("Comm Array: " + commArray.length);
        //alert("DeDup Array: " + dedupArray.length);
        dedupArray.reverse();
        commArray = dedupArray;        
        commArrayLength = commArray.length;
        
        // create Pois and add to PoiCollection
        for(i=0; i<commArrayLength; i++)
        {
            var cAry = commArray[i].split(";");
            var cAryLength = cAry.length;
            //alert(cAryLength);
            
            if (cAryLength == 17) 
            {            
                var cid = cAry[0];
                var bid = cAry[1];
                var name = cAry[2];
                var cty = cAry[3];
                var st = cAry[4];
                var zip = cAry[5];
                var img = cAry[6];
                var brandimg = cAry[7];
                var price = cAry[8];
                var brand = cAry[9];
                var matches = cAry[10];
                var promoid = cAry[11];
                var type = cAry[12];
                var lat = cAry[13];
                var lng = cAry[14];
                var market = cAry[15];
                     
                var commPoint = createCommPoi(cid,bid,name,cty,st,zip,img,brandimg,price,brand,matches,promoid,type,lat,lng,market);
                          
            }
            else if (cAryLength > 17)
            {               
                var commNumber = ((cAryLength-1)/16);
                var key = "mkey"; // multipoi key
                
                if (commNumber > 2)
                {
                    var contentHtml = new Array();
                    contentHtml.push("<div class=\"nhsMultiBoxTitle\">");
                    contentHtml.push(commNumber);
                    contentHtml.push(" communities</div>");
                    contentHtml.push("<div class=\"nhsMultiHoverBox nhsMultiBoxScroll\"><div class=\"nhsMultiScroll\">");
                }
                else
                {
                    var contentHtml = new Array();
                    contentHtml.push("<div class=\"nhsMultiHoverBox\">");
                }
                
                for (j=0; j<commNumber; j++)
                {
                    var k = 16 * j;
                    var cid = cAry[(0+k)];
                    var bid = cAry[(1+k)];
                    var name = cAry[(2+k)];
                    var cty = cAry[(3+k)];
                    var st = cAry[(4+k)];
                    var zip = cAry[(5+k)];
                    var img = cAry[(6+k)];
                    var brandimg = cAry[(7+k)];
                    var price = cAry[(8+k)];
                    var brand = cAry[(9+k)];
                    var matches = cAry[(10+k)];
                    var promoid = cAry[(11+k)]; //not used
                    var type = cAry[(12+k)]; //not used
                    var lat = cAry[(13+k)];
                    var lng = cAry[(14+k)];
                    var market = cAry[(15+k)];
                    key += "_"+cid+bid;
                    
                    contentHtml.push("<div class=\"nhsMultiHoverRow\">");
                    contentHtml.push("<h3>");
                    contentHtml.push(name);
                    contentHtml.push("</h3>");
                    contentHtml.push("</div>");                    
                }
                contentHtml.push("<br style=\"clear: both;\" />");
                
                if (commNumber > 2)
                {
                    contentHtml.push("</div></div>");
                }
                else
                {
                    contentHtml.push("</div>");
                }
                
                var contentHtmlString = contentHtml.join("");
                
                commPoint = CreateMultiCommPoi(commNumber,key,contentHtmlString,lat,lng);
            }
            commPois.add(commPoint);          
        }          
        return commPois;
    }

    function onError(error)
    {
        HandleError(error.get_message(), '', -1, "onError");
    }
    
    // Redirect to error page, it will log the error
    function HandleError(message, url, line, fromWhere)
    {
        var errorUrl = siteRoot + 'Error/?Message=' +  GetErrorMessage(message, url, line,fromWhere);
        MakeHttpReq(errorUrl);
    }

    // Create an error description and attach extra client information to log
    function GetErrorMessage(errorMsg, url, line, fromWhere)
    {
        var message = 'Message: ' + errorMsg + ' \n';
        
         message += 'Url: ' + window.location.href + ' \n';
         message += 'Line: ' + line + ' \n';
         message += 'CalledFrom:' + fromWhere;
        return message;
    }
        
    //*************************************************************************
    // Helper Functions
    //*************************************************************************  

    function MakeHttpReq(url)
    {
        var xmlhttp;
        xmlhttp = null;
        // code for Mozilla, etc.
        if (window.XMLHttpRequest)
        {
            xmlhttp=new XMLHttpRequest();
        }
        // code for IE
        else if (window.ActiveXObject)
        {
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
     
        if (xmlhttp != null)
        {        
            xmlhttp.open("GET",url,true);
            xmlhttp.send(null);
        }
    }
    
    // Register Event Handlers
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_pageLoading(AsyncPageLoad);
    prm.add_beginRequest(BeginRequestHandler);
    prm.add_endRequest(EndRequestHandler); 

function BeginRequestHandler(sender, args) {
    var elemName = args.get_postBackElement().id; 
    var request = args.get_request(); 
    request.set_userContext(elemName);

}
function EndRequestHandler(sender, args)
{
    if (typeof args.get_error() != "undefined") 
    {        
        if (args.get_error() && (args.get_response().get_timedOut() || args.get_error().name === 'Sys.WebForms.PageRequestManagerServerErrorException'))
        {
            var response = args.get_response(); 
            //HandleError(args.get_error(),response._webRequest.get_userContext());
            HandleError(args.get_error().name, '', -1,"EndRequestHandler");
            // remember to set errorHandled = true to keep from getting a popup from the AJAX library itself
            args.set_errorHandled(true);
        }
    }
}

Sys.Net.WebServiceProxy.retryOnFailure = 
    function(result, userContext, methodName, retryParams, onFailure)
{
    if( result.get_timedOut() )
    {
        if( typeof retryParams != "undefined" )
        {
            //debug.trace("Retry: " + methodName);
            Sys.Net.WebServiceProxy.original_invoke.apply(this, retryParams );
        }
        else
        {
            if( onFailure ) onFailure(result, userContext, methodName);
        }
    }
    else
    {
        if( onFailure ) onFailure(result, userContext, methodName);
    }
}

Sys.Net.WebServiceProxy.original_invoke = Sys.Net.WebServiceProxy.invoke;
Sys.Net.WebServiceProxy.invoke = 
    function Sys$Net$WebServiceProxy$invoke(servicePath, methodName, useGet, 
        params, onSuccess, onFailure, userContext, timeout)
{       
    var retryParams = [ servicePath, methodName, useGet, params, 
        onSuccess, onFailure, userContext, timeout ];
    
    // Call original invoke but with a new onFailure
    // handler which does the auto retry
    var newOnFailure = Function.createDelegate( this, 
        function(result, userContext, methodName) 
        { 
            Sys.Net.WebServiceProxy.retryOnFailure(result, userContext, 
                methodName, retryParams, onFailure); 
        } );
        
    Sys.Net.WebServiceProxy.original_invoke(servicePath, methodName, useGet, 
        params, onSuccess, newOnFailure, userContext, timeout);
}


function startRadiusSlider() {
  	
    var radius_slider = $p('radius_slider');
    //var box = $p('zoom_element');
	var tick_values = [4, 70, 137]; 
	var text_values = ['5 Miles from center','15 Miles from center','25 Miles from center'];
	var actual_values = ['5','15','25'];
	var hdnRadius = $p('hdnRadius');
    var radiusTick;	
	var tick;
	
	//alert('Radius: ' + hdnRadius.value);
    
    // Radius Slider Setup 
    for (var i=0; i<actual_values.length; i++) {
        
        var iActualVal = parseFloat(actual_values[i]);        
        
        if (parseFloat(hdnRadius.value) == iActualVal) {
            //alert("low equal");
            break;
        }
        if (parseFloat(hdnRadius.value) < iActualVal) {
            //alert("less than");
            if (i != 0) {
                hdnRadius.value = parseFloat(actual_values[i-1]);
            }
            else {
                hdnRadius.value = iActualVal;
            }            
            break;
        }     
        if ((i==(actual_values.length-1)) && (parseFloat(hdnRadius.value) > iActualVal)){
            hdnRadius.value = parseFloat(actual_values[i-1]);
            break;
        } 
    }
    
    if(hdnRadius.value == '0')
    {
        tick = 2;
        radiusTick = text_values[tick_values.indexOf(tick)];
    }
    else
    {
        tick = tick_values[actual_values.indexOf(hdnRadius.value)];
        radiusTick = text_values[actual_values.indexOf(hdnRadius.value)];
    }    
	$p('radValue').innerHTML = radiusTick;
	//hdnLow.value = actual_values[tick_values.indexOf(lo)];	
	
    new Control.Slider(radius_slider.select('.nhsHandle'), radius_slider, {
    	range: $R(0, 142),		
		values: tick_values,
    	sliderValue: tick,
		onSlide: function(v) {
			var radIndex = tick_values.indexOf(v);
			$p('radValue').innerHTML = text_values[radIndex];
			},
      	onChange: function(v) { 
			var radIndex = tick_values.indexOf(v);
			$p('radValue').innerHTML = text_values[radIndex];
			// re-factor
			$p('hdnRadius').value = actual_values[radIndex];
      	},
	  	restricted: true	 	
    });
}
