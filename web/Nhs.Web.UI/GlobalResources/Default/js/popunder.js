

 function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
    }

    function readCookie(name) {
	    var nameEQ = name + "=";
	    var ca = document.cookie.split(';');
	    for(var i=0;i < ca.length;i++) {
		    var c = ca[i];
		    while (c.charAt(0)==' ') c = c.substring(1,c.length);
		    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	    }
	    return null;
    }

    function eraseCookie(name) {
	    createCookie(name,"",-1);
    }  




function pop(PageName) 
{
	var page = PageName;
	var windowprops = "width=736,height=300,location=no,menubar=no,toolbar=no,status=no,scrollbars=yes,resizable=yes";
	var popWindow = window.open(page,'PopupName',windowprops);
	
	try {
        popWindow.blur(); 
    } catch (e) {}
    try {
        window.focus();
    } catch (e) {}
	
}
//var adSize = newWindow.document.getElementById("adSize");
//newWindow.alert(adSize.offsetWidth);
//newWindow.resizeTo(adSize.offsetWidth, adSize.offsetHeight);	

function checkCountAndPop(PageName)
{
	var count = readCookie('count');   //Get popunder count
	var PageCount = readCookie('PageCount');  //Get Page Count
	// pageCount set through codebehind
	if(parseInt(PageCount) > 1)
	{
		
		if (count == null)
		{
			count=1; //number of pops 
			days=1; //number of days before pop again
			createCookie('count', count, days);
			pop(PageName);
			return;
		}
	}
	
	return;
}

function popunder(PageName)
{
		//return true;
		checkCountAndPop(PageName); //Check If PageCount is more than 1  and not popundered already.
}

