﻿var AjaxForm = {

    currentId: 0,

    ShowWindow: function (e, url, width, height, title, modal, openCallback, closeCallback) {

        if (e != undefined && e != null) {
            // event bubbling
            if (!e) var e = window.event;
            e.cancelBubble = true;
            if (e.stopPropagation) e.stopPropagation();
        }

        // Creates window
        AjaxForm.currentId++;
        var windowName = "window_" + AjaxForm.currentId;
        var windowUrl = url;
        win = new Window(windowName,
            {
                url: windowUrl,
                title: title,
                width: width,
                height: height,
                closeCallback: closeCallback,
                destroyOnClose: true,
                recenterAuto: true,
                minimizable: false,
                maximizable: false,
                resizable: true,
                showEffect: Element.show,
                hideEffect: Element.hide
            }
        );

        win.setZIndex(1000000 + AjaxForm.currentId);

        // Shows window
        win.showCenter(modal);

        // Calls open callback
        //if (openCallback != null)
        //    openCallback();
    },

    CloseWindows: function () {
        Windows.closeAll();
    },

    CloseCurrentWindow: function () {
        var win = Windows.getFocusedWindow();
        Windows.close(win.getId());
    },

    SetWindowSize: function (width, height) {
        //alert("Width: "+width+" Height:"+height);
        var win = Windows.getFocusedWindow();
        win.setSize(width, height);
        win._center(win.centerTop, win.centerLeft);
    },

    UpdateHeight: function () {
        var win = Windows.getFocusedWindow();
        var newHeight = AjaxForm._getContentHeight(win);
        if (newHeight != 0)
            AjaxForm.SetWindowSize(win.width, newHeight + 5);
    },

    _getContentHeight: function (win) {
        if (win == null)
            return 0;
        var iframe = frames[(win.getId() + "_content")];
        if (iframe != null) {
            var contents = iframe.$$('div.nhsContent');
            if (contents.length > 0)
                return contents[0].getHeight();
        }
        return 0;
    },

    UpdateMainPage: function () {
        var win = Windows.getFocusedWindow();
        win.parentWindow.location.reload();
    }
}
