﻿var priceValue;
function init(price, idDownPayment) {
    if ($p('bankrate_slider') != null) {
        startBankRateSlider(price, idDownPayment);
    }
}

function startBankRateSlider(price, idDownPayment) {
    var bankrate_slider = $p('bankrate_slider');
    var bankrate_sliderValue = $p('radValue');
    var bankrate_totalLoanAmount = $p('totalLoanAmount');
    var tick_values = [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 36, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50];
    priceValue = price;
    var downPayment = (price - (price - (price * 0.20))).toNearest(1000);
    var loanAmount = (price - (price * 0.20)).toNearest(1000);
    bankrate_sliderValue.innerHTML = '$' + downPayment.formatMoney(0, ",", ",");
    bankrate_totalLoanAmount.innerHTML = '$' + loanAmount.formatMoney(0, ",", ",") + ' total loan amount';
    $p(idDownPayment).value = downPayment;
    var slider = new Control.Slider(bankrate_slider.select('.nhsHandle'), bankrate_slider, {
        range: $R(5, 50),
        values: tick_values,
        sliderValue: 20,
        onSlide: function(v) {
            var index = tick_values.indexOf(v);
            if (index == -1) return;
            var downPayment = (priceValue - (priceValue - (priceValue * (tick_values[index] / 100)))).toNearest(1000);
            $p(idDownPayment).value = downPayment;
            var loanAmount = (price - (priceValue * (tick_values[index] / 100))).toNearest(1000);
            $p('percentageValue').innerHTML = tick_values[index] + '% down payment';
            bankrate_sliderValue.innerHTML = '$' + downPayment.formatMoney(0, ",", ",");
            bankrate_totalLoanAmount.innerHTML = '$' + loanAmount.formatMoney(0, ",", ",") + ' total loan amount';

        },
        onChange: function(v) {
            var index = tick_values.indexOf(v);
            if (index == -1) return;
            var downPayment = (priceValue - (priceValue - (priceValue * (tick_values[index] / 100)))).toNearest(1000);
            $p(idDownPayment).value = downPayment;
            var loanAmount = (price - (priceValue * (tick_values[index] / 100))).toNearest(1000);
            $p('percentageValue').innerHTML = tick_values[index] + '% down payment';
            bankrate_sliderValue.innerHTML = '$' + downPayment.formatMoney(0, ",", ",");
            bankrate_totalLoanAmount.innerHTML = '$' + loanAmount.formatMoney(0, ",", ",") + ' total loan amount';
        
        }
    });
}

Number.prototype.formatMoney = function(c, d, t) {
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "",
	i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t)
	+ (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};


Number.prototype.toNearest = function(num) { // num is an exponent of 10
    return Math.round(this / num) * num;
}