// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments
#pragma warning disable 1591
#region T4MVC

using System;
using Nhs.Library.Web;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using Nhs.Web.UI.AppCode.Controllers;
namespace Nhs.Web.UI.AppCode.Controllers {
    public partial class EmailTemplateController {
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected EmailTemplateController(Dummy d) { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(ActionResult result) {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoute(callInfo.RouteValueDictionary);
        }

        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.ActionResult AccountCreationEmail() {
            return new T4MVC_ActionResult(Area, Name, ActionNames.AccountCreationEmail);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.ActionResult RecoverPasswordEmail() {
            return new T4MVC_ActionResult(Area, Name, ActionNames.RecoverPasswordEmail);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.ActionResult SendToFriendEmail() {
            return new T4MVC_ActionResult(Area, Name, ActionNames.SendToFriendEmail);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.ActionResult RedirectAjaxForm() {
            return new T4MVC_ActionResult(Area, Name, ActionNames.RedirectAjaxForm);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.RedirectResult RedirectPermanent() {
            return new T4MVC_RedirectResult(Area, Name, ActionNames.RedirectPermanent);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.RedirectResult Redirect() {
            return new T4MVC_RedirectResult(Area, Name, ActionNames.Redirect);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.ActionResult ShowNewsletterEmail() {
            return new T4MVC_ActionResult(Area, Name, ActionNames.ShowNewsletterEmail);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.ActionResult ShowCustomNewsletter() {
            return new T4MVC_ActionResult(Area, Name, ActionNames.ShowCustomNewsletter);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public EmailTemplateController Actions { get { return NhsMvc.EmailTemplate; } }
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Area = "";
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Name = "EmailTemplate";

        static readonly ActionNamesClass s_actions = new ActionNamesClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionNamesClass ActionNames { get { return s_actions; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNamesClass {
            public readonly string ShowLeadEmail = "ShowLeadEmail";
            public readonly string ShowRecoEmail = "ShowRecoEmail";
            public readonly string ShowLeadEmailOld = "ShowLeadEmailOld";
            public readonly string ShowRecoEmailOld = "ShowRecoEmailOld";
            public readonly string ShowPresentedEmail = "ShowPresentedEmail";
            public readonly string AccountCreationEmail = "AccountCreationEmail";
            public readonly string RecoverPasswordEmail = "RecoverPasswordEmail";
            public readonly string SearchAlertEmail = "SearchAlertEmail";
            public readonly string SendToFriendEmail = "SendToFriendEmail";
            public readonly string RedirectAjaxForm = "RedirectAjaxForm";
            public readonly string RedirectPermanent = "RedirectPermanent";
            public readonly string Redirect = "Redirect";
            public readonly string ShowNewsletterEmail = "ShowNewsletterEmail";
            public readonly string ShowCustomNewsletter = "ShowCustomNewsletter";
        }


        static readonly ViewNames s_views = new ViewNames();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewNames Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewNames {
        }
    }

    [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
    public class T4MVC_EmailTemplateController: Nhs.Web.UI.AppCode.Controllers.EmailTemplateController {
        public T4MVC_EmailTemplateController() : base(Dummy.Instance) { }

        public override System.Web.Mvc.ActionResult ShowLeadEmail() {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.ShowLeadEmail);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult ShowRecoEmail() {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.ShowRecoEmail);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult ShowLeadEmailOld() {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.ShowLeadEmailOld);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult ShowRecoEmailOld() {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.ShowRecoEmailOld);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult ShowPresentedEmail() {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.ShowPresentedEmail);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult AccountCreationEmail(string userid) {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.AccountCreationEmail);
            callInfo.RouteValueDictionary.Add("userid", userid);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult RecoverPasswordEmail(string userid) {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.RecoverPasswordEmail);
            callInfo.RouteValueDictionary.Add("userid", userid);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult SearchAlertEmail() {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.SearchAlertEmail);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult SendToFriendEmail(Nhs.Web.UI.AppCode.ViewModels.SendFriendTemplateViewModel model) {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.SendToFriendEmail);
            callInfo.RouteValueDictionary.Add("model", model);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult RedirectAjaxForm(string url, bool usedjavascript) {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.RedirectAjaxForm);
            callInfo.RouteValueDictionary.Add("url", url);
            callInfo.RouteValueDictionary.Add("usedjavascript", usedjavascript);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult RedirectAjaxForm(string function, System.Collections.Generic.IList<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams) {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.RedirectAjaxForm);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            return callInfo;
        }

        public override System.Web.Mvc.RedirectResult RedirectPermanent(string function, System.Collections.Generic.IList<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams) {
            var callInfo = new T4MVC_RedirectResult(Area, Name, ActionNames.RedirectPermanent);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            return callInfo;
        }

        public override System.Web.Mvc.RedirectResult RedirectPermanent(string function, System.Collections.Generic.IList<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams, bool completeUrl) {
            var callInfo = new T4MVC_RedirectResult(Area, Name, ActionNames.RedirectPermanent);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            callInfo.RouteValueDictionary.Add("completeUrl", completeUrl);
            return callInfo;
        }

        public override System.Web.Mvc.RedirectResult Redirect(string function, System.Collections.Generic.IList<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams) {
            var callInfo = new T4MVC_RedirectResult(Area, Name, ActionNames.Redirect);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            return callInfo;
        }

        public override System.Web.Mvc.RedirectResult Redirect(string function, System.Collections.Generic.IList<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams, string extraParameters) {
            var callInfo = new T4MVC_RedirectResult(Area, Name, ActionNames.Redirect);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            callInfo.RouteValueDictionary.Add("extraParameters", extraParameters);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult RedirectAjaxForm(string function, System.Collections.Generic.List<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams) {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.RedirectAjaxForm);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            return callInfo;
        }

        public override System.Web.Mvc.RedirectResult RedirectPermanent(string function, System.Collections.Generic.List<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams) {
            var callInfo = new T4MVC_RedirectResult(Area, Name, ActionNames.RedirectPermanent);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            return callInfo;
        }

        public override System.Web.Mvc.RedirectResult RedirectPermanent(string function, System.Collections.Generic.List<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams, bool completeUrl) {
            var callInfo = new T4MVC_RedirectResult(Area, Name, ActionNames.RedirectPermanent);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            callInfo.RouteValueDictionary.Add("completeUrl", completeUrl);
            return callInfo;
        }

        public override System.Web.Mvc.RedirectResult Redirect(string function, System.Collections.Generic.List<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams) {
            var callInfo = new T4MVC_RedirectResult(Area, Name, ActionNames.Redirect);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            return callInfo;
        }

        public override System.Web.Mvc.RedirectResult Redirect(string function, System.Collections.Generic.List<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams, string extraParameters) {
            var callInfo = new T4MVC_RedirectResult(Area, Name, ActionNames.Redirect);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            callInfo.RouteValueDictionary.Add("extraParameters", extraParameters);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult ShowNewsletterEmail(int marketId, bool? registered) {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.ShowNewsletterEmail);
            callInfo.RouteValueDictionary.Add("marketId", marketId);
            callInfo.RouteValueDictionary.Add("registered", registered);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult ShowCustomNewsletter(string u, string gd, string gdy, string fn, string t) {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.ShowCustomNewsletter);
            callInfo.RouteValueDictionary.Add("u", u);
            callInfo.RouteValueDictionary.Add("gd", gd);
            callInfo.RouteValueDictionary.Add("gdy", gdy);
            callInfo.RouteValueDictionary.Add("fn", fn);
            callInfo.RouteValueDictionary.Add("t", t);
            return callInfo;
        }

    }
}

#endregion T4MVC
#pragma warning restore 1591
