// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments
#pragma warning disable 1591
#region T4MVC

using System;
using Nhs.Library.Web;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using Nhs.Web.UI.AppCode.Controllers;
namespace Nhs.Web.UI.AppCode.Controllers {
    public partial class EBookController {
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected EBookController(Dummy d) { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(ActionResult result) {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoute(callInfo.RouteValueDictionary);
        }

        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.ActionResult ShowEmail() {
            return new T4MVC_ActionResult(Area, Name, ActionNames.ShowEmail);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.JsonResult EBookRegister() {
            return new T4MVC_JsonResult(Area, Name, ActionNames.EBookRegister);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.JsonResult EBookAdRegister() {
            return new T4MVC_JsonResult(Area, Name, ActionNames.EBookAdRegister);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.ActionResult ShowEbookModal() {
            return new T4MVC_ActionResult(Area, Name, ActionNames.ShowEbookModal);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.ActionResult RedirectAjaxForm() {
            return new T4MVC_ActionResult(Area, Name, ActionNames.RedirectAjaxForm);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.RedirectResult RedirectPermanent() {
            return new T4MVC_RedirectResult(Area, Name, ActionNames.RedirectPermanent);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.RedirectResult Redirect() {
            return new T4MVC_RedirectResult(Area, Name, ActionNames.Redirect);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public EBookController Actions { get { return NhsMvc.EBook; } }
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Area = "";
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Name = "EBook";

        static readonly ActionNamesClass s_actions = new ActionNamesClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionNamesClass ActionNames { get { return s_actions; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNamesClass {
            public readonly string Show = "Show";
            public readonly string ShowEmail = "ShowEmail";
            public readonly string EBookRegister = "EBookRegister";
            public readonly string EBookAdRegister = "EBookAdRegister";
            public readonly string ShowEbookModal = "ShowEbookModal";
            public readonly string NotShowMore = "NotShowMore";
            public readonly string ChangeEBookState = "ChangeEBookState";
            public readonly string RedirectAjaxForm = "RedirectAjaxForm";
            public readonly string RedirectPermanent = "RedirectPermanent";
            public readonly string Redirect = "Redirect";
        }


        static readonly ViewNames s_views = new ViewNames();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewNames Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewNames {
        }
    }

    [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
    public class T4MVC_EBookController: Nhs.Web.UI.AppCode.Controllers.EBookController {
        public T4MVC_EBookController() : base(Dummy.Instance) { }

        public override System.Web.Mvc.ActionResult Show() {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.Show);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult ShowEmail(string email) {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.ShowEmail);
            callInfo.RouteValueDictionary.Add("email", email);
            return callInfo;
        }

        public override System.Web.Mvc.JsonResult EBookRegister(string email, string source) {
            var callInfo = new T4MVC_JsonResult(Area, Name, ActionNames.EBookRegister);
            callInfo.RouteValueDictionary.Add("email", email);
            callInfo.RouteValueDictionary.Add("source", source);
            return callInfo;
        }

        public override System.Web.Mvc.JsonResult EBookAdRegister(string email, string source, int partnerId) {
            var callInfo = new T4MVC_JsonResult(Area, Name, ActionNames.EBookAdRegister);
            callInfo.RouteValueDictionary.Add("email", email);
            callInfo.RouteValueDictionary.Add("source", source);
            callInfo.RouteValueDictionary.Add("partnerId", partnerId);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult ShowEbookModal(string email, int marketId) {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.ShowEbookModal);
            callInfo.RouteValueDictionary.Add("email", email);
            callInfo.RouteValueDictionary.Add("marketId", marketId);
            return callInfo;
        }

        public override System.Web.Mvc.JsonResult ShowEbookModal(bool homeOfTheWeek, bool weeklyMarketUpdate, int marketId, string email) {
            var callInfo = new T4MVC_JsonResult(Area, Name, ActionNames.ShowEbookModal);
            callInfo.RouteValueDictionary.Add("homeOfTheWeek", homeOfTheWeek);
            callInfo.RouteValueDictionary.Add("weeklyMarketUpdate", weeklyMarketUpdate);
            callInfo.RouteValueDictionary.Add("marketId", marketId);
            callInfo.RouteValueDictionary.Add("email", email);
            return callInfo;
        }

        public override System.Web.Mvc.JsonResult NotShowMore() {
            var callInfo = new T4MVC_JsonResult(Area, Name, ActionNames.NotShowMore);
            return callInfo;
        }

        public override System.Web.Mvc.JsonResult ChangeEBookState() {
            var callInfo = new T4MVC_JsonResult(Area, Name, ActionNames.ChangeEBookState);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult RedirectAjaxForm(string url, bool usedjavascript) {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.RedirectAjaxForm);
            callInfo.RouteValueDictionary.Add("url", url);
            callInfo.RouteValueDictionary.Add("usedjavascript", usedjavascript);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult RedirectAjaxForm(string function, System.Collections.Generic.IList<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams) {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.RedirectAjaxForm);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            return callInfo;
        }

        public override System.Web.Mvc.RedirectResult RedirectPermanent(string function, System.Collections.Generic.IList<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams) {
            var callInfo = new T4MVC_RedirectResult(Area, Name, ActionNames.RedirectPermanent);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            return callInfo;
        }

        public override System.Web.Mvc.RedirectResult RedirectPermanent(string function, System.Collections.Generic.IList<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams, bool completeUrl) {
            var callInfo = new T4MVC_RedirectResult(Area, Name, ActionNames.RedirectPermanent);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            callInfo.RouteValueDictionary.Add("completeUrl", completeUrl);
            return callInfo;
        }

        public override System.Web.Mvc.RedirectResult Redirect(string function, System.Collections.Generic.IList<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams) {
            var callInfo = new T4MVC_RedirectResult(Area, Name, ActionNames.Redirect);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            return callInfo;
        }

        public override System.Web.Mvc.RedirectResult Redirect(string function, System.Collections.Generic.IList<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams, string extraParameters) {
            var callInfo = new T4MVC_RedirectResult(Area, Name, ActionNames.Redirect);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            callInfo.RouteValueDictionary.Add("extraParameters", extraParameters);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult RedirectAjaxForm(string function, System.Collections.Generic.List<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams) {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.RedirectAjaxForm);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            return callInfo;
        }

        public override System.Web.Mvc.RedirectResult RedirectPermanent(string function, System.Collections.Generic.List<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams) {
            var callInfo = new T4MVC_RedirectResult(Area, Name, ActionNames.RedirectPermanent);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            return callInfo;
        }

        public override System.Web.Mvc.RedirectResult RedirectPermanent(string function, System.Collections.Generic.List<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams, bool completeUrl) {
            var callInfo = new T4MVC_RedirectResult(Area, Name, ActionNames.RedirectPermanent);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            callInfo.RouteValueDictionary.Add("completeUrl", completeUrl);
            return callInfo;
        }

        public override System.Web.Mvc.RedirectResult Redirect(string function, System.Collections.Generic.List<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams) {
            var callInfo = new T4MVC_RedirectResult(Area, Name, ActionNames.Redirect);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            return callInfo;
        }

        public override System.Web.Mvc.RedirectResult Redirect(string function, System.Collections.Generic.List<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams, string extraParameters) {
            var callInfo = new T4MVC_RedirectResult(Area, Name, ActionNames.Redirect);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            callInfo.RouteValueDictionary.Add("extraParameters", extraParameters);
            return callInfo;
        }

    }
}

#endregion T4MVC
#pragma warning restore 1591
