// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments
#pragma warning disable 1591
#region T4MVC

using System;
using Nhs.Library.Web;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using Nhs.Web.UI.AppCode.Controllers;
namespace Nhs.Web.UI.AppCode.Controllers {
    public partial class MortgageController {
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected MortgageController(Dummy d) { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(ActionResult result) {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoute(callInfo.RouteValueDictionary);
        }

        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.ActionResult GetRates() {
            return new T4MVC_ActionResult(Area, Name, ActionNames.GetRates);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.ActionResult GetMatches() {
            return new T4MVC_ActionResult(Area, Name, ActionNames.GetMatches);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.ActionResult RedirectAjaxForm() {
            return new T4MVC_ActionResult(Area, Name, ActionNames.RedirectAjaxForm);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.RedirectResult RedirectPermanent() {
            return new T4MVC_RedirectResult(Area, Name, ActionNames.RedirectPermanent);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.RedirectResult Redirect() {
            return new T4MVC_RedirectResult(Area, Name, ActionNames.Redirect);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public MortgageController Actions { get { return NhsMvc.Mortgage; } }
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Area = "";
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Name = "Mortgage";

        static readonly ActionNamesClass s_actions = new ActionNamesClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionNamesClass ActionNames { get { return s_actions; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNamesClass {
            public readonly string RedirectMortgageTable = "RedirectMortgageTable";
            public readonly string RedirectMortgageRates = "RedirectMortgageRates";
            public readonly string ShowMortgagePayments = "ShowMortgagePayments";
            public readonly string ShowMortgageTable = "ShowMortgageTable";
            public readonly string GetRates = "GetRates";
            public readonly string GetMatches = "GetMatches";
            public readonly string ShowMortgageRates = "ShowMortgageRates";
            public readonly string ShowMortgageCalculator = "ShowMortgageCalculator";
            public readonly string RedirectAjaxForm = "RedirectAjaxForm";
            public readonly string RedirectPermanent = "RedirectPermanent";
            public readonly string Redirect = "Redirect";
        }


        static readonly ViewNames s_views = new ViewNames();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewNames Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewNames {
        }
    }

    [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
    public class T4MVC_MortgageController: Nhs.Web.UI.AppCode.Controllers.MortgageController {
        public T4MVC_MortgageController() : base(Dummy.Instance) { }

        public override System.Web.Mvc.ActionResult RedirectMortgageTable() {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.RedirectMortgageTable);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult RedirectMortgageRates() {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.RedirectMortgageRates);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult ShowMortgagePayments() {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.ShowMortgagePayments);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult ShowMortgageTable() {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.ShowMortgageTable);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult GetRates(Nhs.Web.UI.AppCode.ViewModels.MortgageViewModel mmodel) {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.GetRates);
            callInfo.RouteValueDictionary.Add("mmodel", mmodel);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult GetMatches(Nhs.Web.UI.AppCode.ViewModels.MortgageViewModel mmodel) {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.GetMatches);
            callInfo.RouteValueDictionary.Add("mmodel", mmodel);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult ShowMortgageRates() {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.ShowMortgageRates);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult ShowMortgageCalculator() {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.ShowMortgageCalculator);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult RedirectAjaxForm(string url, bool usedjavascript) {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.RedirectAjaxForm);
            callInfo.RouteValueDictionary.Add("url", url);
            callInfo.RouteValueDictionary.Add("usedjavascript", usedjavascript);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult RedirectAjaxForm(string function, System.Collections.Generic.IList<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams) {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.RedirectAjaxForm);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            return callInfo;
        }

        public override System.Web.Mvc.RedirectResult RedirectPermanent(string function, System.Collections.Generic.IList<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams) {
            var callInfo = new T4MVC_RedirectResult(Area, Name, ActionNames.RedirectPermanent);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            return callInfo;
        }

        public override System.Web.Mvc.RedirectResult RedirectPermanent(string function, System.Collections.Generic.IList<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams, bool completeUrl) {
            var callInfo = new T4MVC_RedirectResult(Area, Name, ActionNames.RedirectPermanent);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            callInfo.RouteValueDictionary.Add("completeUrl", completeUrl);
            return callInfo;
        }

        public override System.Web.Mvc.RedirectResult Redirect(string function, System.Collections.Generic.IList<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams) {
            var callInfo = new T4MVC_RedirectResult(Area, Name, ActionNames.Redirect);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            return callInfo;
        }

        public override System.Web.Mvc.RedirectResult Redirect(string function, System.Collections.Generic.IList<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams, string extraParameters) {
            var callInfo = new T4MVC_RedirectResult(Area, Name, ActionNames.Redirect);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            callInfo.RouteValueDictionary.Add("extraParameters", extraParameters);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult RedirectAjaxForm(string function, System.Collections.Generic.List<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams) {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.RedirectAjaxForm);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            return callInfo;
        }

        public override System.Web.Mvc.RedirectResult RedirectPermanent(string function, System.Collections.Generic.List<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams) {
            var callInfo = new T4MVC_RedirectResult(Area, Name, ActionNames.RedirectPermanent);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            return callInfo;
        }

        public override System.Web.Mvc.RedirectResult RedirectPermanent(string function, System.Collections.Generic.List<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams, bool completeUrl) {
            var callInfo = new T4MVC_RedirectResult(Area, Name, ActionNames.RedirectPermanent);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            callInfo.RouteValueDictionary.Add("completeUrl", completeUrl);
            return callInfo;
        }

        public override System.Web.Mvc.RedirectResult Redirect(string function, System.Collections.Generic.List<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams) {
            var callInfo = new T4MVC_RedirectResult(Area, Name, ActionNames.Redirect);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            return callInfo;
        }

        public override System.Web.Mvc.RedirectResult Redirect(string function, System.Collections.Generic.List<Nhs.Mvc.Routing.Configuration.RouteParam> urlParams, string extraParameters) {
            var callInfo = new T4MVC_RedirectResult(Area, Name, ActionNames.Redirect);
            callInfo.RouteValueDictionary.Add("function", function);
            callInfo.RouteValueDictionary.Add("urlParams", urlParams);
            callInfo.RouteValueDictionary.Add("extraParameters", extraParameters);
            return callInfo;
        }

    }
}

#endregion T4MVC
#pragma warning restore 1591
