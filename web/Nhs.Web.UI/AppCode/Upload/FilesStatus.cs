﻿using System;
using System.IO;
using Nhs.Library.Common;
using Nhs.Mvc.Routing.Interface;

namespace Nhs.Web.UI.AppCode.Upload
{
    public class FilesStatus
    {
        public const string HandlerPath = "/AppCode/Upload/";

        public string group { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public int size { get; set; }
        public string progress { get; set; }
        public string url { get; set; }
        public string thumbnail_url { get; set; }
        public string delete_url { get; set; }
        public string delete_type { get; set; }
        public string error { get; set; }
        public string QueryString { get; set; }
        public int id { get; set; }
        public string description { get; set; }
        public string title { get; set; }
        public string accreditationurl { get; set; }

        public int processorStatus { get; set; }

        public FilesStatus()
        {
        }

        public FilesStatus(FileInfo fileInfo)
        {
            //SetValues(fileInfo.Name, (int)fileInfo.Length, fileInfo.FullName); 
        }

        public FilesStatus(string fileName, int fileLength, string fullPath, string description, int id, int builderid, string title, string accreditationurl,
                           string querryString = null, int processorStatusId = 9)
        {
            SetValues(fileName, fileLength, fullPath, description, id, builderid, title, accreditationurl, querryString, processorStatusId);
        }

        private void SetValues(string fileName, int fileLength, string fullPath, string filedescription, int fileid,
                               int builderid, string imagetitle, string imageaccreditationurl, string querryString,
                               int processorStatusId)
        {
            name = fileName;
            type = "image/png";
            size = fileLength;
            progress = "1.0";
            url = HandlerPath + "UploadHandler.ashx?f=" + fileName + "&imageid=" + fileid + "&builderid=" + builderid +
                  (string.IsNullOrEmpty(querryString) ? "" : "&" + querryString);
            delete_url = HandlerPath + "UploadHandler.ashx?f=" + fileName + "&imageid=" + fileid + "&builderid=" +
                         builderid + (string.IsNullOrEmpty(querryString) ? "" : "&" + querryString);
            delete_type = "DELETE";
            id = fileid;
            description = filedescription;
            processorStatus = processorStatusId;
            title = imagetitle;
            accreditationurl = imageaccreditationurl;
            //var ext = Path.GetExtension(fullPath);
            if (!File.Exists(fullPath))
            {
                thumbnail_url =  Resources.GlobalResources14.Default.images.no_photo.no_photos_120x80_png;
                processorStatus = 0;
            }
            else
                thumbnail_url = @"data:image/png;base64," + EncodeFile(fullPath);
            //thumbnail_url = Path.Combine(Configuration.ResourceDomain, fullPath.Replace(Configuration.AgentBrochureInfoServer, "")).Replace(@"\","/");
        }

        private bool IsImage(string ext)
        {
            return ext == ".gif" || ext == ".jpeg" || ext == ".jpg" || ext == ".png";
        }

        private string EncodeFile(string fileName)
        {
            return Convert.ToBase64String(System.IO.File.ReadAllBytes(fileName));
        }

        private static double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes/1024f)/1024f;
        }
    }
}