using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Domain.Model.Hub;
using Nhs.UrlRewriter.Configuration;
using Nhs.Utility.Common;
using StructureMap;

namespace Nhs.Web.UI.AppCode.Upload
{
    public class UploadHandler : IHttpHandler
    {
        // Fields
        private readonly IBrandService _brandService = ObjectFactory.GetInstance<IBrandService>();
        private readonly JavaScriptSerializer js = new JavaScriptSerializer();

        public void ProcessRequest(HttpContext context)
        {
            context.Items["ContextItemsCurrentPartnerId"] = GetPartnerId(context.Request.Url);
            BrandId = Convert.ToInt32(context.Request["brandid"]);
            BuilderId = Convert.ToInt32(context.Request["builderid"]);
            Brand = _brandService.HubBrands.FirstOrDefault(p => p.BrandId == BrandId);
            ImageTypeCode = context.Request["ImageTypeCode"];
            Description = context.Request["description"];
            Title = context.Request["title"];
            Accreditationurl = context.Request["accreditationurl"];

            context.Response.AddHeader("Pragma", "no-cache");
            context.Response.AddHeader("Cache-Control", "private, no-cache");
            HandleMethod(context);
        }

        // Methods
        public UploadHandler()
        {
            js.MaxJsonLength = 41943040;
        }

        private void DeleteFile(HttpContext context)
        {
            var imageid = context.Request["imageid"];
            _brandService.DeleteBrandShowCaseImage(imageid);
            //string filePath = StorageRoot +
            //if (File.Exists(filePath))
            //{
            //    File.Delete(filePath);
            //}
        }

        private void DeliverFile(HttpContext context)
        {
            string filename = context.Request["f"];
            string filePath = StorageRoot + filename;
            if (File.Exists(filePath))
            {
                context.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
                context.Response.ContentType = "application/octet-stream";
                context.Response.ClearContent();
                context.Response.WriteFile(filePath);
            }
            else
            {
                context.Response.StatusCode = 404;
            }
        }

        private static bool GivenFilename(HttpContext context)
        {
            return !string.IsNullOrEmpty(context.Request["f"]);
        }

        private void HandleMethod(HttpContext context)
        {
            switch (context.Request.HttpMethod)
            {
                case "HEAD":
                case "GET":
                    if (!GivenFilename(context))
                    {
                        ListCurrentFiles(context);
                        break;
                    }
                    DeliverFile(context);
                    break;

                case "POST":
                case "PUT":
                    UploadFile(context);
                    break;

                case "DELETE":
                    DeleteFile(context);
                    break;

                case "OPTIONS":
                    ReturnOptions(context);
                    break;

                default:
                    context.Response.ClearHeaders();
                    context.Response.StatusCode = 405;
                    break;
            }
        }

        private void ListCurrentFiles(HttpContext context)
        {
            var images = Brand.HubBrandImages.Where(p => p.ImageTypeCode == ImageTypeCode).OrderBy(p => p.ImageSequence);
            var querryString = string.Format("brandid={0}&ImageTypeCode={1}", BrandId, ImageTypeCode);
            var files = new List<FilesStatus>();
            foreach (var image in images)
            {
                var imagePath = Path.Combine(StorageRoot, image.OriginalPath);
                var fullName = image.OriginalPath;
                files.Add(new FilesStatus(fullName, 500, imagePath, image.ImageDescription, image.ImageID,
                                          Convert.ToInt32(image.BuilderID), image.ImageTitle, image.ClickThruURL,
                                          querryString, image.ProcessorStatusID.ToType<int>()));
            }
            var jsonObj = js.Serialize(files);
            context.Response.AddHeader("Content-Disposition", "inline; filename=\"files.json\"");
            context.Response.Write(jsonObj);
            context.Response.ContentType = "application/json";
        }

        private int GetPartnerId(Uri uri)
        {
            return RewriterConfiguration.PartnerIdPro;
        }

        private static void ReturnOptions(HttpContext context)
        {
            context.Response.AddHeader("Allow", "DELETE,GET,HEAD,POST,PUT,OPTIONS");
            context.Response.StatusCode = 200;
        }

        private void UploadFile(HttpContext context)
        {
            var statuses = new List<FilesStatus>();
            var headers = context.Request.Headers;
            if (string.IsNullOrEmpty(headers["X-File-Name"]))
            {
                UploadWholeFile(context, statuses);
            }
            else
            {
                UploadPartialFile(headers["X-File-Name"], context, statuses);
            }
            WriteJsonIframeSafe(context, statuses);
        }

        private void UploadPartialFile(string fileName, HttpContext context, List<FilesStatus> statuses)
        {
            if (context.Request.Files.Count != 1)
            {
                throw new HttpRequestValidationException(
                    "Attempt to upload chunked file containing more than one fragment per request");
            }
            var inputStream = context.Request.Files[0].InputStream;
            var fullName = StorageRoot + Path.GetFileName(fileName);
            var imagename = ImageTypeCode + "_" + Path.GetFileName(fileName);
            var fullPath = StorageRoot + imagename;
            using (var fs = new FileStream(fullPath, FileMode.Append, FileAccess.Write))
            {
                var buffer = new byte[1024];
                for (int l = inputStream.Read(buffer, 0, 1024); l > 0; l = inputStream.Read(buffer, 0, 1024))
                {
                    fs.Write(buffer, 0, l);
                }
                fs.Flush();
                fs.Close();
            }
            statuses.Add(new FilesStatus(new FileInfo(fullName)));
        }

        private void UploadWholeFile(HttpContext context, List<FilesStatus> statuses)
        {
            for (var i = 0; i < context.Request.Files.Count; i++)
            {
                var file = context.Request.Files[i];
                var fullName = ImageTypeCode + "_" + Path.GetFileName(file.FileName);
                var fullPath = StorageRoot + fullName;
                file.SaveAs(fullPath);
                var id = _brandService.AddBrandShowCaseImage(ImageTypeCode, BuilderId, fullName,
                                                             fullName, Description, Title, 0,
                                                             Accreditationurl);
                statuses.Add(new FilesStatus(fullName, file.ContentLength, fullPath, Description, id, BuilderId, Title,
                                             Accreditationurl));
            }
        }

        private void WriteJsonIframeSafe(HttpContext context, List<FilesStatus> statuses)
        {
            context.Response.AddHeader("Vary", "Accept");
            try
            {
                context.Response.ContentType = context.Request["HTTP_ACCEPT"].Contains("application/json")
                                                   ? "application/json"
                                                   : "text/plain";
            }
            catch
            {
                context.Response.ContentType = "text/plain";
            }
            string jsonObj = js.Serialize(statuses.ToArray());
            context.Response.Write(jsonObj);
        }

        // Properties
        public HubBrand Brand { get; set; }

        private int BrandId { get; set; }

        private int BuilderId { get; set; }

        public string Description { get; set; }

        public string Title { get; set; }

        public string Accreditationurl { get; set; }

        public string ImageTypeCode { get; set; }

        public bool IsReusable
        {
            get { return false; }
        }

        public string Server
        {
            get { return Configuration.ImageProcessorFTPPath; }
        }

        private string StorageRoot
        {
            get
            {
                string ftpLocation;
                if (BuilderId == 0)
                {
                    var builder = Brand.HubBuilders.FirstOrDefault();
                    ftpLocation = builder.FtpLocation;
                    BuilderId = builder.BuilderId;
                }
                else
                {
                    ftpLocation = Brand.HubBuilders.FirstOrDefault(p => (p.BuilderId == BuilderId)).FtpLocation;
                }
                var folder = Path.Combine(Server, ftpLocation);

                var dirInfo = new DirectoryInfo(folder);
                if (!dirInfo.Exists)
                {
                    dirInfo.Create();
                }
                return (folder + @"\");
            }
        }
    }

}
