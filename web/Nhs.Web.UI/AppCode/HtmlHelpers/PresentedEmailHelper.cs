﻿using System.Collections.Generic;
using System.Web.Mvc;
using Bdx.Web.Api.Objects;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;
using Resources;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class PresentedEmailHelper
    {
        public static MvcHtmlString GenerateLinkGetAllBrochures( this HtmlHelper helper, PresentedEmailViewModel model)
        {
           var @params= new List<RouteParam>
                {
                    new RouteParam(RouteParams.Community, model.SourceCommunityId, RouteParamType.QueryString), 
                    new RouteParam(RouteParams.CommunityList, model.RecoCommunityBuilderList, RouteParamType.QueryString), 
                    new RouteParam(RouteParams.FirstName, model.FirstName, RouteParamType.QueryString, true, false), 
                    new RouteParam(RouteParams.LastName, model.LastName, RouteParamType.QueryString),
                    new RouteParam(RouteParams.Email, model.Email, RouteParamType.QueryString)
                };
           const string utm = "csource=matchemail&utm_source=recommended&utm_medium=email&utm_campaign=recommended+-+no+box&utm_content=getbrochures";
           return helper.NhsLinkWithUtm(LanguageHelper.GetAllBrochures, Pages.GetallBrochuresPage, @params, new { style = "color:#ffffff;text-decoration:none !important;" }, utm);
        }

        public static MvcHtmlString CommPresentedImages(this HtmlHelper helper, ApiRecoCommunityResult comm)
        {
            if (comm.CommunityImageThumbnail.IsValidImage())
            {
                var thumbnail = comm.CommunityImageThumbnail.Replace(ImageSizes.Small + "_", ImageSizes.HomeMain + "_").Replace(ImageSizes.Results + "_", ImageSizes.CommDetailMain + "_");
                

                return helper.NhsImage(thumbnail, comm.CommunityName, new { width = "460", height = "306", border = "0", style = "width:100%;max-width:460px;height:auto;" }, true);
            }
            
            return helper.NhsImage(GlobalResources14.Default.images.no_photo.no_photos_460x307_png, comm.CommunityName, new { width="460", height="306", border="0", style="width:100%;max-width:460px;height:auto;"}, true); 
        }


        public static string  GenerateLinkRecomendationsPage(this PresentedEmailViewModel model, int place)
        {
            var @params = new List<RouteParam>
                {
                    new RouteParam(RouteParams.Community, model.SourceCommunityId, RouteParamType.QueryString), 
                    new RouteParam(RouteParams.CommunityList, model.RecoCommunityList, RouteParamType.QueryString), 
                    new RouteParam(RouteParams.FirstName, model.FirstName, RouteParamType.QueryString, true, false), 
                    new RouteParam(RouteParams.LastName, model.LastName, RouteParamType.QueryString),
                    new RouteParam(RouteParams.Email, model.Email, RouteParamType.QueryString)
                };

            var url = @params.ToUrl(Pages.RecomendationsPage, "csource=matchemail&utm_source=recommended&utm_medium=email&utm_campaign=recommended+-+no+box&utm_content=place" + place);
            url = NhsUrlHelper.ResolveServerUrl(url);
            return url;
        }
    }
}
