﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web.Mvc.Html;
using System.Web.Mvc;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class HomePageDropDownHelper
    {
        /// <summary>
        /// Create the controls for the home page
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="source">source of the data of the list</param>
        /// <param name="placeholderText"></param>
        /// <param name="width">width of the list</param>
        /// <param name="height">height of the list</param>
        /// <param name="classTextBox">class for the textbox that will display the text</param>
        /// <param name="idUl">The id of the list</param>
        /// <param name="addAny">The id of the list</param>
        /// <param name="spanText">The text for the span</param>
        /// <returns>MvcHtmlString</returns>
        public static MvcHtmlString CreateHomePageDropDownFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression,
            SelectList source, string placeholderText, int width, int height, string classTextBox, string idUl, bool addAny = false, string spanText = "", string suffixText = "")
        {
            var controls = new StringBuilder();
            var selectItem = source.FirstOrDefault(p => p.Selected);
            var expName = expression.Body.ToString().Split('.').LastOrDefault();

            var hiddenFor = htmlHelper.HiddenFor(expression, new
                {
                    @id = expName,
                    @Name = expName,
                    value = selectItem != null ? selectItem.Value : string.Empty, 
                    @class = "resultHidden"                    
                });//Create the Hidden


            
            var textBoxFor = htmlHelper.TextBoxFor(expression, new
                {
                    @id = "txt_" + expName,
                    @name = expName,
                    value = selectItem != null ? selectItem.Text : string.Empty,                     
                    @readonly = true, 
                    //title = placeholder, 
                    placeholder = placeholderText, 
                    @class = "resultText trigger " + classTextBox                    
                });//Create the TextBox
          
            controls.AppendLine(hiddenFor.ToHtmlString());
            controls.AppendLine(textBoxFor.ToHtmlString());
            //controls.AppendLine(string.Format("<div class=\"pro_SearchDdlData\" id=\"{0}\" style=\"width:{1}px; height:{2}px;\">", idUl, width, height));
            controls.AppendLine(string.Format("<span class=\"pro_SearchDdlData\" id=\"{0}\">", idUl));
            if (spanText != string.Empty)
                controls.AppendLine(string.Format("<span>{0}</span>", spanText));
            //Create the ul or list 
            controls.AppendLine("<ul>");
            if(addAny)
                controls.AppendLine("<li value=\"\">Any</li>");     
            foreach (var data in source)
            {
                if (data.Text.Trim() == "Any")
                    controls.AppendLine(string.Format("<li value=\"{0}\" {1}>{2}</li>", data.Value,
                                                      data.Selected ? "class=\"pro_Active\"" : "", data.Text));
                else
                {
                    controls.AppendLine(string.Format("<li value=\"{0}\" {1}>{2}</li>", data.Value,
                                                      data.Selected ? "class=\"pro_Active\"" : "", data.Text + " " + suffixText).Trim());
                }
            }
            controls.AppendLine("</ul>");
            controls.AppendLine("</span>");

            return MvcHtmlString.Create(controls.ToString());
        }    
    }
}
