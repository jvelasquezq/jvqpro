﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Nhs.Web.UI.AppCode.Helper;
using Resources;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class AddThisHelper
    {
        [Obsolete("Please use SocialMediaSharing")]
        public static MvcHtmlString AddThisControl(this HtmlHelper helper)
        {
            var addThis = String.Format(@"<div class=""addthis_toolbox addthis_default_style"" addthis:url=""{0}"">", HttpContext.Current.Request.Url);

            return MvcHtmlString.Create(addThis + @"<a href=""http://www.addthis.com/bookmark.php?v=250&pub=newhomesource"" onclick=""return addthis_sendto()"" 
                                                    onmouseout=""addthis_close()"" id=""addthis_bookmark"" onmouseover=""return addthis_open(this, '', '[URL]', '[TITLE]')"">
                                                    <img alt="""" height=""16"" id=""addThisImage"" style=""border: 0"" width=""16"" /></a>
                                                    <span class=""addthis_separator"">|</span>
                                                    <a class=""addthis_button_facebook"" id=""btn_fb_mh""></a>
                                                    <a class=""addthis_button_twitter"" id=""btn_tw_mh""></a>    
                                                    <a class=""addthis_button_email""></a>
                                                    <a class=""addthis_button_print""></a>
                                                    </div>");
        }

        /// <summary>
        /// In order to work the page must have the NHS7_Search\src\web\Nhs.Web.UI\GlobalResourcesMvc\Default\js\Common\AddThisLoader.js
        /// reference.
        /// </summary>
        /// <param name="helper"></param>
        /// <returns>MVC String with the HTML of the Social Media Sharing control</returns>
        public static MvcHtmlString SocialMediaSharing(this HtmlHelper helper)
        {
            var html = SocialMediaSharing();
            return html.ToMvcHtmlString();
        }

        /// <summary>
        /// This method depend on the javascript AddThisLoader.js in the requested page
        /// </summary>
        /// <returns></returns>
        public static string SocialMediaSharing()
        {
            const string html = @"<aside id=""nhs_SocialSideBar""><p>{0} <span><</span></p>
                         <!-- AddThis Button BEGIN -->
                         <div id=""smToolbox"" class=""addthis_toolbox addthis_floating_style addthis_32x32_style"" style=""left:0px;top:50px;""></div>                         
                         <!-- AddThis Button END -->
                        </aside>";

            return string.Format(html, LanguageHelper.Share);
        }

        public static MvcHtmlString RenderPinterest(this HtmlHelper helper, string pageUrl, string imgUrl, string description, object htmlAttributes, bool bigIcon = false, bool addThisButton = true)
        {
            var dictionary = (htmlAttributes == null) ? new RouteValueDictionary() : new RouteValueDictionary(htmlAttributes);
            var clickEvent = dictionary.ContainsKey("onclick") ? dictionary["onclick"].ToString() : string.Empty;
            var pinterestHtmlString = string.Format(@"<div id=""button_pin_it""><a  target=""_blank"" href=""http://pinterest.com/pin/create/button/?url={0}&media={1}&description={2}""
                                                        onclick=""{3}"" class=""pin-it-button"" count-layout=""horizontal""><img border=""0"" 
                                                        src=""{4}"" title=""Pin It"" /></a></div>" + (addThisButton? @"<div id=""button_compact""><a class=""addthis_button_compact""></a></div>" : string.Empty),
                                                          Uri.EscapeUriString(pageUrl), Uri.EscapeUriString(imgUrl), Uri.EscapeUriString(description),
                                                          clickEvent, !bigIcon ? @"//assets.pinterest.com/images/PinExt.png" : GlobalResourcesMvc.Default.images.icons.pinterest_icon_png);
            return MvcHtmlString.Create(pinterestHtmlString);
        }
    }
}
