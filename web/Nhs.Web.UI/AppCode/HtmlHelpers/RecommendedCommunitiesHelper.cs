﻿using System.Web.Mvc;
using Bdx.Web.Api.Objects;
using Nhs.Library.Constants;
using Nhs.Library.Web.ImageResizer;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Resources;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class RecommendedCommunitiesHelper
    {
        public static MvcHtmlString GetRecommendedCommunitiesImage(this HtmlHelper helper, string thumbnail)
        {
            thumbnail =string.IsNullOrWhiteSpace(thumbnail)?"": thumbnail.Replace("//", "/").Replace(":/", "://");
            return !thumbnail.IsValidImage() ? helper.NhsImage( GlobalResources14.Default.images.no_photo.no_photos_75x58_png, "", false) : helper.NhsImage(thumbnail, "", true);
        }


        public static MvcHtmlString GetRecommendedCommunitiesImage(this HtmlHelper helper, ApiRecoCommunityResult model, bool useSchemaOrg = false)
        {
            var thumbnail = string.IsNullOrWhiteSpace(model.CommunityImageThumbnail) ? string.Empty :
                                                                                       model.CommunityImageThumbnail.Replace("//", "/").Replace(":/", "://");

            if (thumbnail.IsValidImage())
            {
                thumbnail = ImageResizerUrlHelpers.FormatImageUrlForIrs(thumbnail);
                thumbnail = ImageResizerUrlHelpers.BuildIrsImageUrl(thumbnail, ImageSizes.HomeMain);
            }
            else
                thumbnail = GlobalResources14.Default.images.no_photo.no_photos_180x120_png;
            return helper.NhsImage(thumbnail, "", useSchemaOrg ? new { itemprop = "contentUrl" } : null, false);
        }



        public static MvcHtmlString GetRecommendedCommunitiesBuilderImage(this HtmlHelper helper, string thumbnail, bool useLarge = false, bool useSchemaOrg = false)
        {
            thumbnail = string.IsNullOrWhiteSpace(thumbnail) ? string.Empty : thumbnail.Replace("//", "/").Replace(":/", "://");
            thumbnail = useLarge ? thumbnail.Replace("SML.", "MED.").Replace("sml.", "med.") : thumbnail.Replace("MED.", "SML.").Replace("med.", "sml.");
            return !thumbnail.IsValidImage() ? MvcHtmlString.Empty : helper.NhsImage(thumbnail, string.Empty, useSchemaOrg ? new { itemprop = "contentUrl" } : null, true);
        }
    }
}
