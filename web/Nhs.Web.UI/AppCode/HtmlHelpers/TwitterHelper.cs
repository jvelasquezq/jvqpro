﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Utility.Common;
using StructureMap.Diagnostics;
using TweetSharp;
using System.Linq;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class TwitterHelper
    {
        public static MvcHtmlString GetTwitterWidget(this HtmlHelper htmlHelper, string twitterWidgetHtml)
        {
            return !String.IsNullOrEmpty(twitterWidgetHtml) ? twitterWidgetHtml.ToMvcHtmlString() : MvcHtmlString.Empty;
        }

        public static List<TwitterStatus> GetTwitterUpdates(int count)
        {
            var tweets = new List<TwitterStatus>();
            string twitterInfo = Configuration.TwitterAccountInformation;

            if (!string.IsNullOrEmpty(twitterInfo))
            {
                string consumerKey = twitterInfo.Split('|')[0];
                string consumerSecret = twitterInfo.Split('|')[1];
                string accessToken = twitterInfo.Split('|')[2];
                string accessTokenSecret = twitterInfo.Split('|')[3];

                try
                {
                    var service = new TwitterService(consumerKey, consumerSecret);
                    service.AuthenticateWith(accessToken, accessTokenSecret);
                    var data = service.ListTweetsOnUserTimeline(new ListTweetsOnUserTimelineOptions() {Count = count});

                    if (data != null)
                        tweets = data.ToList();
                }
                catch (Exception ex) // dont fault for any unhandle exception in the twitter api
                {
                    ErrorLogger.LogError("Twitter Error", "GetTwitterUpdates are not working", ex.StackTrace);
                }
            }

            return tweets;
        }
    }
}
