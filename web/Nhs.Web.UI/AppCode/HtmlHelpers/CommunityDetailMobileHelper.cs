﻿using System.Collections.Generic;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Web.ImageResizer;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Web.UI.AppCode.ViewModels.CommunityDetailMobile;


namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class CommunityDetailHelperMobile
    {

        public static string GetImageGalleryMobile(this MediaPlayerObject media)
        {
            var path = media.Url;

            if (!path.IsValidImage())
                return Resources.GlobalResources14.Default.images.no_photo.no_photos_325x216_png;

            path = ImageResizerUrlHelpers.BuildIrsImageUrl(path, new ResizeCommands { MaxWidth = 360, MaxHeight = 240 });
            return path;
        }

        public static string FloorPlanImage(this IImage image)
        {
            var path = image.ImagePath + image.ImageName;

            if (string.IsNullOrEmpty(image.ImagePath) || string.IsNullOrEmpty(image.ImageName))
            {
                path = image.OriginalPath;
            }

           return ImageResizerUrlHelpers.FormatImageUrlForIrs(path);
        }

        public static string GetHomeGalleryUrl(this HomeItem result)
        {
            var @paramas = new List<RouteParam>();
            if (result.IsSpec.ToType<bool>())
            {
                @paramas.Add(new RouteParam(RouteParams.SpecId, result.HomeId.ToString(), RouteParamType.QueryString));
             @paramas.Add(  new RouteParam(RouteParams.PlanId, 0, RouteParamType.QueryString));
            }
            else
            {
                @paramas.Add(new RouteParam(RouteParams.SpecId, 0, RouteParamType.QueryString));
                @paramas.Add(new RouteParam(RouteParams.PlanId, result.HomeId.ToString(), RouteParamType.QueryString));
            }

            @paramas.Add(new RouteParam(RouteParams.IncludeVideo, "false", RouteParamType.QueryString));
            return @paramas.ToUrl(Pages.GetHomeImages);
        }


        public static string GetFloorPlanGalleryUrl(this HomeItem result, bool previewModel)
        {
            var @paramas = new List<RouteParam>();
            if (result.IsSpec.ToType<bool>())
            {
                @paramas.Add(new RouteParam(RouteParams.SpecId, result.HomeId.ToString(), RouteParamType.QueryString));
                @paramas.Add(new RouteParam(RouteParams.PlanId, 0, RouteParamType.QueryString));
            }
            else
            {
                @paramas.Add(new RouteParam(RouteParams.SpecId, 0, RouteParamType.QueryString));
                @paramas.Add(new RouteParam(RouteParams.PlanId, result.HomeId.ToString(), RouteParamType.QueryString));
            }
            @paramas.Add(new RouteParam(RouteParams.UseHub, previewModel.ToType<string>(), RouteParamType.QueryString));
            return @paramas.ToUrl(Pages.GetFloorPlanGallery);
        }


        public static string GetHomeDetailUrl(this FloorPlanViewModel result)
        {
            var paramas = new List<RouteParam>
            {
                result.IsSpec
                    ? new RouteParam(RouteParams.SpecId, result.SpecId)
                    : new RouteParam(RouteParams.PlanId, result.PlanId)
            };
            return paramas.ToUrl(Pages.HomeDetail);
        }     
    }
}