using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Bdx.Web.Api.Objects;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Web;
using Nhs.Library.Web.ImageResizer;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResultMobile;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class HomeItemMobileHelper
    {

        public static List<RouteParam> GetCommunityParams(this QuickViewViewModel model)
        {
            return new List<RouteParam> {
                new RouteParam(RouteParams.Builder, model.BuilderId),
                new RouteParam(RouteParams.Community, model.CommunityId)};
        }

        public static string HomeResThumb(this ApiHomeResultV2 result)
        {
            return result.Thumb1.IsValidImage()
                ? result.Thumb1
                : Resources.GlobalResources14.Default.images.no_photo.no_photos_325x216_png;
        }

        public static ResizeCommands GetHomeResThumb1Size(this ApiHomeResultV2 result)
        {
            return new ResizeCommands { MaxWidth = 470, MaxHeight = 298, Format = ImageFormat.Jpg, Scale = ScaleCommand.None };
        }

        public static ResizeCommands GetHomeSizeHomeTab(this ApiHomeResultV2 result)
        {
            return new ResizeCommands {MaxWidth = 204, MaxHeight = 148, Format = ImageFormat.Jpg};
        }

        public static string GetHomeName(this ApiHomeResultV2 result)
        {

            if (result.IsSpec != 0 && !string.IsNullOrEmpty(result.Addr))
                return result.Addr + " (" + result.PlanName + ")";

            if (result.IsSpec != 0 || !string.IsNullOrEmpty(result.PlanName))
                return result.PlanName;

            return "";
        }

        public static MvcHtmlString GetFormattedPrice(this ApiHomeResultV2 result)
        {
            if ((result.Status ?? string.Empty).ToLower() == "m")
            {
                return new MvcHtmlString("<p>&nbsp;</p>");
            }

            return result.Price.ToType<decimal>() > 0 ? MvcHtmlString.Create(string.Format(@"<p>" + StringHelper.ToTitleCase(LanguageHelper.From) + " {0}</p>", result.Price.ToType<decimal>().ToString("C0"))) : null;
        }

        public static MvcHtmlString GetFormattedSqFt(this ApiHomeResultV2 result)
        {
            return result.Sft > 0 ? MvcHtmlString.Create(string.Format(@"<p>{0} " + LanguageHelper.MSG_SQ_FT + "</p>", result.Sft)) : MvcHtmlString.Empty;
        }

        public static MvcHtmlString GetHomeInformation(this ApiHomeResultV2 result)
        {
            var information = new List<string>();
            if (result.Br > 0)
                information.Add(string.Format(@"{0} " + LanguageHelper.BedroomAbbreviation, result.Br));

         
            if (result.Ba > 0)
            {
                var bathrooms = ListingUtility.ComputeBathrooms(result.Ba, result.HBa);
                information.Add(string.Format(@"{0} " + LanguageHelper.BathroomAbbreviation, bathrooms));
            }
      
            if (result.Gr > 0)
                information.Add(string.Format(@"{0:0.##} " + LanguageHelper.GarageAbbreviation, result.Gr));

            return ("<p>" + string.Join(" ", information) + "</p>").ToMvcHtmlString();
        }

        public static MvcHtmlString GetHomeInformationV2(this ApiHomeResultV2 result)
        {
            var information = new List<string>();
            if (result.Br > 0)
                information.Add(string.Format(@"{0} " + LanguageHelper.BedroomAbbreviation, result.Br));


            if (result.Ba > 0)
            {
                var bathrooms = ListingUtility.ComputeBathrooms(result.Ba, result.HBa);
                information.Add(string.Format(@"{0} " + LanguageHelper.BathroomAbbreviation, bathrooms));
            }

            if (result.Gr > 0)
                information.Add(string.Format(@"{0:0.##} " + LanguageHelper.GarageAbbreviation, result.Gr));

            if (result.Sft > 0)
                information.Add(string.Format(@"| {0} <abbr title=""square feet"">" + LanguageHelper.MSG_SQ_FT + "</abbr>", result.Sft));


            return ("<p>" + string.Join(" ", information) + "</p>").ToMvcHtmlString();
        }

        public static string HomeStatus(this string homeStatus)
        {
            string retValue;

            switch (homeStatus)
            {
                case "A":
                    retValue = LanguageHelper.ReadyToMoveIn;
                    break;
                case "UC":
                    retValue = LanguageHelper.UnderConstruction;
                    break;
                case "M":
                    retValue = LanguageHelper.ModelHome;
                    break;
                case "R":
                    retValue = LanguageHelper.ReadyToBuild;
                    break;
                default:
                    retValue = LanguageHelper.ModelHome;
                    break;
            }
            return retValue;
        }
    }
}
