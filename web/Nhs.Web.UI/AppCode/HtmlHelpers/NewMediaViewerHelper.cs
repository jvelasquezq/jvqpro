﻿using System.Collections.Generic;
using System.EnterpriseServices.Internal;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Route;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Library.Web.ImageResizer;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels.NewMediaViewer;
using Resources;
using StructureMap.Query;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class NewMediaViewerHelper
    {
        public static string GetNewMediaViewerSmallImage(this MediaPlayerObject media)
        {
            var path = media.Url;

            if (!path.IsValidImage())
                return GlobalResources14.Default.images.no_photo.no_photos_120x80_png;

            path = ImageResizerUrlHelpers.BuildIrsImageUrl(path, new ResizeCommands {Width = 120, Height = 80});
            return path;
        }

        public static string GetNewMediaViewerBigImage(this MediaPlayerObject media)
        {
            var path = media.Url;

            if (!path.IsValidImage())
                return GlobalResources14.Default.images.no_photo.no_photos_325x216_png;

            path = ImageResizerUrlHelpers.BuildIrsImageUrl(path,
                new ResizeCommands {MaxWidth = 1500, MaxHeight = 800, Scale = ScaleCommand.None});
            return path;
        }

        public static string GetFreeBrohureUrl(this NewMediaViewerViewModel model)
        {
            var @params = new List<RouteParam>();
            if (model.IsCommunityDetail)
            {
                @params = new List<RouteParam>
                {
                    new RouteParam(RouteParams.LeadType, LeadType.Community, RouteParamType.QueryString),
                    new RouteParam(RouteParams.LeadAction, LeadAction.FreeBrochure, RouteParamType.QueryString),
                    new RouteParam(RouteParams.CommunityList, model.CommunityId.ToString(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.Builder, model.BuilderId.ToString(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.FromPage, Pages.CommunityResults, RouteParamType.QueryString),
                    new RouteParam(RouteParams.Market, model.MarketId.ToString(), RouteParamType.QueryString)
                };
            }
            else
            {
                @params.Add(new RouteParam(RouteParams.LeadType, LeadType.Home, RouteParamType.QueryString));
                @params.Add(new RouteParam(RouteParams.LeadAction, LeadAction.FreeBrochure, RouteParamType.QueryString));
                if (model.SpecId == 0)
                {
                    @params.Add(new RouteParam(RouteParams.PlanList, model.PlanId.ToString(), RouteParamType.QueryString));
                    @params.Add(new RouteParam(RouteParams.PlanId, model.PlanId.ToString(), RouteParamType.QueryString));
                }
                else
                {
                    @params.Add(new RouteParam(RouteParams.SpecList, model.SpecId.ToString(), RouteParamType.QueryString));
                    @params.Add(new RouteParam(RouteParams.SpecId, model.SpecId.ToString(), RouteParamType.QueryString));
                }
                @params.Add(new RouteParam(RouteParams.Builder, model.BuilderId.ToString(), RouteParamType.QueryString));
                @params.Add(new RouteParam(RouteParams.Community, model.CommunityId.ToString(), RouteParamType.QueryString));
                @params.Add(new RouteParam(RouteParams.Market, model.MarketId.ToString(), RouteParamType.QueryString));
                @params.Add(new RouteParam(RouteParams.FromPage, Pages.HomeDetail, RouteParamType.QueryString));
            }

            @params.Add(new RouteParam(RouteParams.LeadFormType, GoogleGaConst.GoogleGaILeadFormTypeConst.FullScreen, RouteParamType.QueryString));
            return @params.ToUrl(Pages.LeadsRequestBrochureModal, false);
        }

        public static string GetFullImageViewerUrl(this HtmlHelper htmlHelper, int communityId)
        {
            var @params = new List<RouteParam>
            {
                new RouteParam(RouteParams.CommunityId, communityId, RouteParamType.QueryString)
            };

            if (!UserSession.UserProfile.IsLoggedIn())
            {
                var nextPage = NhsRoute.CurrentRoute.HostUrl;
                nextPage += (nextPage.Contains("?") ? "&" : "?") + RouteParams.AddToProfile + "=true";
                @params.Add(new RouteParam(RouteParams.NextPage, HttpUtility.UrlEncode(nextPage), RouteParamType.QueryString));
            }
            return @params.ToUrl(Pages.ComunityDetailFullImageViewer);
        }

        public static string GetFullImageViewerUrl(this HtmlHelper htmlHelper, int speciId, int planId)
        {
            var @params = new List<RouteParam>
            {
                new RouteParam(RouteParams.SpecId, speciId, RouteParamType.QueryString),
                new RouteParam(RouteParams.PlanId, planId, RouteParamType.QueryString)
            };

            if (!UserSession.UserProfile.IsLoggedIn())
            {
                var nextPage = NhsRoute.CurrentRoute.HostUrl;
                nextPage += (nextPage.Contains("?") ? "&" : "?") + RouteParams.AddToProfile + "=true";
                @params.Add(new RouteParam(RouteParams.NextPage, HttpUtility.UrlEncode(nextPage), RouteParamType.QueryString));
            }
            return @params.ToUrl(Pages.HomeDetailFullImageViewer);
        }

        public static string AddToFavorites(this NewMediaViewerViewModel model)
        {
            return model.IsCommunityDetail ? model.Globals.GetAddCommunityToPlannerUrl(model.CommunityId, model.BuilderId) 
                                           : model.Globals.GetAddHomeToPlannerUrl(model.PlanId, model.SpecId);
        }
    }
}