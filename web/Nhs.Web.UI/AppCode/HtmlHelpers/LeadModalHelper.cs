﻿using System.Web.Mvc;
using Bdx.Web.Api.Objects;
using Nhs.Library.Constants;
using Nhs.Library.Web.ImageResizer;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Web.UI.AppCode.ViewModels.LeadView;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class LeadModalHelper
    {

        public static MvcHtmlString GetPropertyImage(this HtmlHelper helper, LeadViewModalModel model)
        {
            var image = model.PropertyImage.IsValidImage()
                ? model.PropertyImage.Replace(ImageSizes.Small, ImageSizes.HomeMain)
                    .Replace(ImageSizes.Results, ImageSizes.CommDetailMain)
                : Resources.GlobalResources14.Default.images.no_photo.hands_290x190_jpg;

            return helper.NhsImage(image, new ResizeCommands {MaxWidth = 500, MaxHeight = 333, Format = ImageFormat.Jpg},
                model.PropertyName, false);
        }


        public static MvcHtmlString GetPropertyImage(this HtmlHelper helper, ApiRecoCommunityResult model)
        {
            var image = model.CommunityImageThumbnail.IsValidImage()
                ? model.CommunityImageThumbnail.Replace(ImageSizes.Small, ImageSizes.HomeMain)
                    .Replace(ImageSizes.Results, ImageSizes.CommDetailMain)
                :  Resources.GlobalResources14.Default.images.no_photo.no_photos_325x216_png;

            return helper.NhsImage(image, new ResizeCommands { MaxWidth = 300, MaxHeight = 300, Format = ImageFormat.Jpg },
                model.CommunityName, false);
        }


        public static MvcHtmlString GetPropertyPrice(this HtmlHelper helper, ApiRecoCommunityResult model)
        {
            return StringHelper.PrettyPrintRange(model.PriceLow, model.PriceHigh, "c0", LanguageHelper.From).ToMvcHtmlString();
        }
    }
}