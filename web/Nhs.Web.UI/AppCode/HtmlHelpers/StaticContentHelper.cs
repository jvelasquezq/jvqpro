﻿/*
 * NewHomeSource - http://www.NewHomeSource.com
 * Copyright © 2001-2010 Builders Digital Experience, LLC.
 * ========================================================
 * Author: Praveen Guggarigoudar
 * Date: 10/14/2010
 * Edit History (please mention edits succinctly):
 * =========================================================
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using BHI.EnterpriseLibrary.Core.ObjectBuilder;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Web;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Html;
using Nhs.Utility.Html.HtmlElements;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    /// <summary>
    /// HTML Helper to render static content from static HTML or static HTML file located in BHIContent folder
    /// </summary>
    public static class StaticContentHelper
    {
        static int pos = 1;
        static bool showAreas;

        #region Public
        /// <summary>
        /// Loads static content using supplied static HTML and replacement tags
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="replacementTags">The replacement tags.</param>
        /// <param name="inlineHtml">The inline HTML.</param>
        /// <returns></returns>
        public static MvcHtmlString StaticContent(this HtmlHelper helper, List<ContentTag> replacementTags, string inlineHtml, string title = "")
        {
            return MvcHtmlString.Create(ProcessHTML(inlineHtml, replacementTags, string.Empty, out title));
        }

        public static MvcHtmlString StaticContent(this HtmlHelper helper, string contentFilePath)
        {
            return helper.StaticContent(contentFilePath, new List<ContentTag>(), new ContextPathMapper(), NhsRoute.PartnerSiteUrl);
        }

        /// <summary>
        /// Loads static content using supplied static HTML file located inside BHIContent and replacement tags
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="contentFilePath">The content file path.</param>
        /// <param name="replacementTags">The replacement tags.</param>
        /// <param name="pathMapper">The path mapper.</param>
        /// <param name="partnerSiteUrl">The partner site URL.</param>
        /// <param name="title">Title part of the HTML</param>
        /// <returns></returns>
        public static MvcHtmlString StaticContent(this HtmlHelper helper, string contentFilePath, List<ContentTag> replacementTags, IPathMapper pathMapper, string partnerSiteUrl, out string title)
        {
            return helper.StaticStringContent(contentFilePath, replacementTags, pathMapper, partnerSiteUrl, out title).ToMvcHtmlString();
        }

        /// <summary>
        /// Loads static content using supplied static HTML file located inside BHIContent and replacement tags
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="contentFilePath">The content file path.</param>
        /// <param name="replacementTags">The replacement tags.</param>
        /// <param name="pathMapper">The path mapper.</param>
        /// <param name="partnerSiteUrl">The partner site URL.</param>
        /// <returns></returns>
        public static MvcHtmlString StaticContent(this HtmlHelper helper, string contentFilePath, List<ContentTag> replacementTags, IPathMapper pathMapper, string partnerSiteUrl)
        {
            return helper.StaticStringContent(contentFilePath, replacementTags, pathMapper, partnerSiteUrl).ToMvcHtmlString();
        }

        /// <summary>
        /// This method replace the {StateAbbr}_ for the defaultState like:
        ///    "AZ_" if the file do not exist the "AZ_" is replace for the valu in defaultState
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="contentFilePath"></param>
        /// <param name="replacementTags"></param>
        /// <param name="pathMapper"></param>
        /// <param name="partnerSiteUrl"></param>
        /// <param name="defaultState">fail back string for the defaul file name to replace</param>
        /// <param name="defaultPatternToReplace">Pattern to replace the state abbr : default '{0}_' </param>
        /// <returns></returns>
        public static MvcHtmlString StaticContentStateFailBack(this HtmlHelper helper, string contentFilePath,
            List<ContentTag> replacementTags, IPathMapper pathMapper, string partnerSiteUrl, string defaultState = "DefaultState", string defaultPatternToReplace = "{0}_")
        {
            var absContentFilePath = MapFilePath(contentFilePath, pathMapper, partnerSiteUrl);
            if (File.Exists(absContentFilePath) == false)
            {
                var state = replacementTags.FirstOrDefault(s => s.TagKey == Library.Enums.ContentTagKey.StateID);                    

                if (state == null)
                {
                    throw new ArgumentNullException("replacementTags", @"Missing the ContentTagKey.StateID");
                }

                contentFilePath = contentFilePath.Replace(string.Format(defaultPatternToReplace, state.TagValue), defaultState);
                absContentFilePath = MapFilePath(contentFilePath, pathMapper, partnerSiteUrl);
            }

            if (File.Exists(absContentFilePath) == false)
            {
                ErrorLogger.LogError(new FileNotFoundException(absContentFilePath));
            }
            
            var retValue = helper.StaticStringContent(contentFilePath, replacementTags, pathMapper, partnerSiteUrl).ToMvcHtmlString();
            
            return retValue;
        }

        public static string StaticStringContent(this HtmlHelper helper, string contentFilePath, List<ContentTag> replacementTags, IPathMapper pathMapper, string partnerSiteUrl, out string title)
        {
            string html = string.Empty;
            title = string.Empty;
            try
            {
                var absContentFilePath = MapFilePath(contentFilePath, pathMapper, partnerSiteUrl); //try partner-specific file first

                if (!File.Exists(absContentFilePath))
                    absContentFilePath = MapFilePath(contentFilePath, pathMapper, string.Empty); //fallback on default

                if (File.Exists(absContentFilePath))
                {
                    html = GetStreamData(absContentFilePath);
                    ProcessStaticEntry(absContentFilePath);
                }
            }
            catch (InvalidOperationException ex)
            {
                ErrorLogger.LogError(ex);
            }

            if (string.IsNullOrEmpty(html))
                return "";

            return ProcessHTML(html, replacementTags, contentFilePath, out title);
        }

        public static string StaticStringContent(this HtmlHelper helper, string contentFilePath, List<ContentTag> replacementTags, IPathMapper pathMapper, string partnerSiteUrl)
        {
            string html = string.Empty;

            try
            {
                var absContentFilePath = MapFilePath(contentFilePath, pathMapper, partnerSiteUrl); //try partner-specific file first

                if (!File.Exists(absContentFilePath))
                    absContentFilePath = MapFilePath(contentFilePath, pathMapper, string.Empty); //fallback on default

                if (File.Exists(absContentFilePath))
                {
                    html = GetStreamData(absContentFilePath);
                    ProcessStaticEntry(absContentFilePath);
                }
            }
            catch (InvalidOperationException ex)
            {
                ErrorLogger.LogError(ex);
            }

            if (string.IsNullOrEmpty(html))
                return "";
            var title = string.Empty;
            return ProcessHTML(html, replacementTags, contentFilePath, out title);
        }

        /// <summary>
        /// Creates a list of elements in order to use them in the static/seo content overlay
        /// </summary>
        /// <param name="filePath">The content file path</param>
        public static void ProcessStaticEntry(string filePath)
        {
            string staticContent = System.Web.HttpContext.Current.Request.QueryString["showcontentareas"];
            bool showstatic;
            if (staticContent != null)
            {
                bool.TryParse(staticContent, out showstatic);
                UserSession.ShowStaticOverlay = showstatic;
            }
            else
                showstatic = UserSession.ShowStaticOverlay;
            showAreas = showstatic;

            if (showstatic)
            {
                List<StaticItem> staticDictionary = new List<StaticItem>();
                StaticItem item = new StaticItem();
                if (UserSession.StaticContentList == null)
                {
                    item.StaticKey = pos.ToString();
                    item.StaticValue = filePath;
                    staticDictionary.Add(item);
                }
                else
                {
                    staticDictionary = UserSession.StaticContentList;
                    item.StaticKey = pos.ToString();
                    item.StaticValue = filePath;
                    staticDictionary.Add(item);
                }
                UserSession.StaticContentList = staticDictionary;
                ++pos;
            }
        }

        /// <summary>
        /// Used to display the overlay when the page loads and the flag for showing static content is set to true in None MVC pages
        /// </summary>
        /// <param name="helper"></param>
        /// <returns></returns>
        public static string DisplayStaticOverlayNoneMvc(this HtmlHelper helper)
        {
            var sb = new StringBuilder();
            var sbList = new StringBuilder();

            string staticContent = System.Web.HttpContext.Current.Request.QueryString["showcontentareas"];
            bool showstatic;
            bool.TryParse(staticContent, out showstatic);
            UserSession.ShowStaticOverlay = showstatic;

            if (UserSession.ShowStaticOverlay && (UserSession.StaticContentList != null))
            {
                string pl = PrepList(UserSession.StaticContentList);
                UserSession.StaticPaths = pl;
                sb.Append("<script type=\"text/javascript\">");
                sb.Append("$jq(document).ready(function () {");
                //sb.Append("$jq(\"#stcontent\").show();");
                sb.Append("$jq(\".stmark\").css(\"display\",\"block\");");

                //sbList.Append("<h3>Static/SEO Content Information</h3>");
                //sbList.Append("<div id=\"stscroll\" class=\"stscroll\"><ul>");
                /*foreach (StaticItem item in UserSession.StaticContentList)
                {
                    sbList.Append(string.Format("<li>{0}</li>", item.StaticValue.Replace("\\", "\\\\")));
                }
                sbList.Append("</ul></div>");*/

                //sbList.Append("<div><br/><br/>");
                //sbList.Append("* Regions with static/seo content are showed within a red border. <br/>");
                //sbList.Append("* Moving the mouse inside the region will highlight the region in yellow. <br/>");
                //sbList.Append("* Each region has a number. Hover the mouse over the number to see path/data of the content. <br/>");
                //sbList.Append("</div>");

                //sb.Append(string.Format("$jq(\"#staticList\").html('{0}');", sbList.ToString()));

                sb.Append("});");
                sb.Append("</script>");
            }
            /*else if (UserSession.ShowStaticOverlay && (UserSession.StaticContentList == null))
            {
                

                sbList.Append("<h3>Static Content Information</h3>");
                sbList.Append("<div>");
                sbList.Append("No content detected.");
                sbList.Append("</div>");

                sbList.Append("<div><br/><br/>");
                sbList.Append("* Regions with static/seo content are showed within a red border. <br/>");
                sbList.Append("* Moving the mouse inside the region will highlight the region in yellow. <br/>");
                sbList.Append("* Each region has a number. Hover the mouse over the number to see path/data of the content. <br/>");
                sbList.Append("</div>");
                
            }*/
            UserSession.StaticContentList = null;
            return sbList.ToString();
        }

        /// <summary>
        /// Used to display the overlay when the page loads and the flag for showing static content is set to true in MVC pages
        /// </summary>
        /// <param name="helper"></param>
        /// <returns></returns>
        public static MvcHtmlString DisplayStaticOverlay(this HtmlHelper helper)
        {
            var sb = new StringBuilder();

            string staticContent = System.Web.HttpContext.Current.Request.QueryString["showcontentareas"];
            bool showstatic;
            bool.TryParse(staticContent, out showstatic);
            UserSession.ShowStaticOverlay = showstatic;

            if (UserSession.ShowStaticOverlay && (UserSession.StaticContentList != null))
            {
                string pl = PrepList(UserSession.StaticContentList);
                sb.Append("<script type=\"text/javascript\">");
                sb.Append("$jq(document).ready(function () {");
                //sb.Append("$jq(\"#stcontent\").show();");                
                sb.Append("$jq(\".stmark\").css(\"display\",\"block\");");

                //sbList.Append("<h3>Static/SEO Content Information</h3>");
                //sbList.Append("<div id=\"stscroll\" class=\"stscroll\"><ul>");                                
                /*foreach (StaticItem item in UserSession.StaticContentList)
                {
                    sbList.Append(string.Format("<li>{0}</li>",item.StaticValue.Replace("\\","\\\\")));
                }
                sbList.Append("</ul></div>");*/

                //sbList.Append("<div><br/><br/>");
                //sbList.Append("* Regions with static/seo content are showed within a red border. <br/>");
                //sbList.Append("* Moving the mouse inside the region will highlight the region in yellow. <br/>");
                //sbList.Append("* Each region has a number. Hover the mouse over the number to see path/data of the content. <br/>");
                //sbList.Append("</div>");               

                //sb.Append(string.Format("$jq(\"#staticList\").html('{0}');",sbList.ToString()));

                sb.Append("});");
                sb.Append(string.Format("HighlightRegion('{0}');", pl));
                sb.Append("</script>");
            }
            /*else if (UserSession.ShowStaticOverlay && (UserSession.StaticContentList == null))
            {
                sb.Append("<script type=\"text/javascript\">");
                sb.Append("$jq(document).ready(function () {");
                sb.Append("$jq(\"#stcontent\").show();");
                
                sbList.Append("<h3>Static Content Information</h3>");
                sbList.Append("<div>");
                sbList.Append("No content detected.");                
                sbList.Append("</div>");

                sbList.Append("<div><br/><br/>");
                sbList.Append("* Regions with static/seo content are showed within a red border. <br/>");
                sbList.Append("* Moving the mouse inside the region will highlight the region in yellow. <br/>");
                sbList.Append("* Each region has a number. Hover the mouse over the number to see path/data of the content. <br/>");
                sbList.Append("</div>");
                
            }*/
            UserSession.StaticContentList = null;
            return MvcHtmlString.Create(sb.ToString());
        }

        /// <summary>
        /// Used to prepare the lists of static paths to be used
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static string PrepList(List<StaticItem> list)
        {
            string res = string.Empty;

            foreach (StaticItem s in list)
            {
                string e = s.StaticKey + ";" + s.StaticValue.Replace("\\", "\\\\");
                res += e + ",";
            }

            return res.TrimEnd(',');
        }

        /// <summary>
        /// Loads static content using supplied static HTML file located inside BHIContent 
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="contentFilePath">The content file path.</param>
        /// <param name="pathMapper">The path mapper.</param>
        /// <param name="partnerSiteUrl">The partner site URL.</param>
        /// <returns></returns>
        public static MvcHtmlString StaticContent(this HtmlHelper helper, string contentFilePath, IPathMapper pathMapper, string partnerSiteUrl)
        {
            return StaticContent(helper, contentFilePath, new List<ContentTag>(), pathMapper, partnerSiteUrl);
        }
        #endregion

        public static MvcHtmlString ToMvcHtmlString(this string text)
        {
            return MvcHtmlString.Create(text);
        }

        public static MvcHtmlString ToMvcHtmlString(this StringBuilder text)
        {
            return text.ToString().ToMvcHtmlString();
        }

        #region Private
        private static string ProcessHTML(string html, IList<ContentTag> replacementTags, string divKey, out string title)
        {
            title = string.Empty;
            try
            {
                html = ParseHtml(html, out title);
                if (html.Contains("["))
                    html = ParseReplacementTags(html, replacementTags);
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }
            if (showAreas)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<div class=\"nhsStaticContent\">");
                sb.Append(string.Format("<a class=\"stmark\"><b class=\"stNumber\" title=\"{0}\" ></b></a>", divKey));
                sb.Append(html);
                sb.Append("</div>");
                return sb.ToString();
            }
            return html;
        }

        private static string ParseHtml(string html, out string title)
        {
            IElement body = new BodyElement();
            var titleElement = new TitleElement();
            //Parse HTML
            var parseHTML = new ParseHTML(html);

            //Get Body
            body.InnerMarkup = parseHTML.GetInnerHTML(body);
            title = parseHTML.GetInnerHTML(titleElement);
            if (body.InnerMarkup.Length > 0)
                return body.InnerMarkup;

            return html;
        }

        private static string ParseReplacementTags(string html, IList<ContentTag> contentTags)
        {
            if (contentTags == null) //nothing to replace
                return html;

            try
            {
                foreach (ContentTag contentTag in contentTags)
                {
                    string tagKey = contentTag.TagKey.ToType<string>();
                    tagKey = string.Format("[{0}]", tagKey);
                    html = html.Replace(tagKey, contentTag.TagValue);
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }
            return html;
        }

        private static string MapFilePath(string relPath, IPathMapper pathMapper, string partnerSiteUrl)
        {
            var scPath = @"~/" + Configuration.StaticContentFolder; //~/BHIContent

            if (!string.IsNullOrEmpty(partnerSiteUrl))
                scPath += "/" + partnerSiteUrl; //~BHIContent/ChronicleHomes

            scPath += "/" + relPath; //~BHIContent/ChronicleHomes/TrackingScripts/trackingscripts.xml

            return pathMapper.MapPath(scPath);
        }

        private static string GetStreamData(string path)
        {
            try
            {
                var streamReader = new StreamReader(path, Encoding.GetEncoding("iso-8859-1"));
                string streamData = streamReader.ReadToEnd();
                streamReader.Close();

                return streamData;
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }
            return string.Empty;
        }
        #endregion
    }
}
