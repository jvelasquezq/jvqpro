﻿using System.Collections.Generic;
using System.Web.Mvc;
using Nhs.Library.Constants;
using Nhs.Library.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class PartnerNavigationHelper
    {
        public static MvcHtmlString GetSignInLink(this HtmlHelper helper, PartialViewModels.PartnerNavigation model, object linkAttributes = null)
        {
            var showModal = model.Globals.PartnerLayoutConfig.PartnerAllowsModals;
            var linkLabel = "Sign In";

            return showModal
                       ? helper.NhsModalWindowLink( linkLabel, 
                                                    Pages.LoginModal,
                                                    ModalWindowsConst.SignInModalWidth,
                                                    ModalWindowsConst.SignInModalHeight, new List<RouteParam>(), linkAttributes)
                       : helper.NhsLink(linkLabel, Pages.SignIn, new List<RouteParam>(), linkAttributes);
        }

        public static MvcHtmlString GetRegisterLink(this HtmlHelper helper, PartialViewModels.PartnerNavigation model, object linkAttributes = null)
        {
            var showModal = model.Globals.PartnerLayoutConfig.PartnerAllowsModals;
            var linkLabel = "Create Account";

            return showModal
                       ? helper.NhsModalWindowLink(linkLabel,
                                                    Pages.RegisterModal,
                                                    ModalWindowsConst.RegisterModalWidth,
                                                    ModalWindowsConst.RegisterModalHeight, new List<RouteParam>(), linkAttributes)
                       : helper.NhsLink(linkLabel, Pages.Register, new List<RouteParam>(), linkAttributes);
        }
    }
}