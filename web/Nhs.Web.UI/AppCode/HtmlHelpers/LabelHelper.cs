﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Routing;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class LabelHelper
    {
        public static MvcHtmlString LabelForWithSpan<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string labelText, string spanText, string spanClass, object htmlAttributes)
        {
            return LabelForWithSpan(html, expression, labelText, spanText, spanClass, new RouteValueDictionary(htmlAttributes));
        }

        public static MvcHtmlString LabelForWithSpan<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string labelText, string spanText, string spanClass, IDictionary<string, object> htmlAttributes)
        {
            var htmlFieldName = ExpressionHelper.GetExpressionText(expression);
            if (String.IsNullOrEmpty(labelText))
            {
                return MvcHtmlString.Empty;
            }

            var tag = new TagBuilder("label");
            tag.MergeAttributes(htmlAttributes);
            tag.Attributes.Add("for", html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(htmlFieldName));

            var span = new TagBuilder("span");
            span.Attributes.Add("class",spanClass);
            span.SetInnerText(spanText);

            // assign <span> to <label> inner html
            tag.InnerHtml = span.ToString(TagRenderMode.Normal) + labelText;

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }
    }
}