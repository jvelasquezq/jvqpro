﻿using System;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using Nhs.Library.Common;
using Nhs.Library.Web;
using Nhs.Mvc.Routing.Interface;
using Nhs.Mvc.Routing.Utils;
using Nhs.Web.UI.AppCode.Extensions;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class PagingHelper
    {
        public static string Pager(this HtmlHelper htmlHelper, int pageCount, int currentPage)
        {
            return Pager(htmlHelper, pageCount, currentPage, null, null, null);
        }

        public static string Pager(this HtmlHelper htmlHelper, int pageCount, int currentPage, string actionName)
        {
            return Pager(htmlHelper, pageCount, currentPage, actionName, null, null);
        }

        public static string Pager(this HtmlHelper htmlHelper, int pageCount, int currentPage, string actionName, string controllerName)
        {
            return Pager(htmlHelper, pageCount, currentPage, actionName, controllerName, null);
        }

        public static string Pager(this HtmlHelper htmlHelper, int pageCount, int currentPage, object values)
        {
            return Pager(htmlHelper, pageCount, currentPage, null, null, new RouteValueDictionary(values));
        }

        public static string Pager(this HtmlHelper htmlHelper, int pageCount, int currentPage, string actionName, object values)
        {
            return Pager(htmlHelper, pageCount, currentPage, actionName, null, new RouteValueDictionary(values));
        }

        public static string Pager(this HtmlHelper htmlHelper, int pageCount, int currentPage, string actionName, string controllerName, object values)
        {
            return Pager(htmlHelper, pageCount, currentPage, actionName, controllerName, new RouteValueDictionary(values));
        }

        public static string Pager(this HtmlHelper htmlHelper, int pageCount, int currentPage, RouteValueDictionary valuesDictionary)
        {
            return Pager(htmlHelper, pageCount, currentPage, null, null, valuesDictionary);
        }

        public static string Pager(this HtmlHelper htmlHelper, int pageCount, int currentPage, string actionName, string controllerName, RouteValueDictionary valuesDictionary)
        {
            if (valuesDictionary == null)
            {
                valuesDictionary = new RouteValueDictionary();
            }
            if (actionName != null)
            {
                if (valuesDictionary.ContainsKey("action"))
                {
                    throw new ArgumentException(@"The valuesDictionary already contains an action.", "actionName");
                }
                valuesDictionary.Add("action", actionName);
            }
            if (!string.IsNullOrEmpty(controllerName))
            {
                if (valuesDictionary.ContainsKey("Controller"))
                {
                    throw new ArgumentException(@"The valuesDictionary already contains a controller.", "controllerName");
                }
                valuesDictionary.Add("Controller", controllerName);
            }
            var pager = new PagerLinks(htmlHelper.ViewContext, pageCount, currentPage, valuesDictionary);
            return pager.RenderHtml();
        }
        
    }

    public class PagerLinks 
    {
        private ViewContext viewContext;
        private readonly int _pageCount;
        private readonly int _currentPage;
        private readonly RouteValueDictionary values;

        public PagerLinks(ViewContext viewContext, int pageCount, int currentPage, RouteValueDictionary valuesDictionary)
        {
            this.viewContext = viewContext;
            this._pageCount = pageCount;
            this._currentPage = currentPage;
            this.values = valuesDictionary;
        }

        public string RenderHtml()
        {
            int nrOfPagesToDisplay = UserSession.PageSize;

            var sb = new StringBuilder();   
            sb.Append("<p class=\"nhs_PagingLinks\">");

            // Previous)
            sb.Append(this._currentPage > 1
                          ? GeneratePageLink("&lt; Prev", this._currentPage - 1)
                          : "<span class=\"nhs_DisabledPage\">&lt; Prev</span>");

            int start = 1;
            int end = _pageCount;

            if (_pageCount > nrOfPagesToDisplay)
            {
                int middle = (int)Math.Ceiling(nrOfPagesToDisplay / 2d) - 1;
                int below = (this._currentPage - middle);
                int above = (this._currentPage + middle);

                if (below < 4)
                {
                    above = nrOfPagesToDisplay;
                    below = 1;
                }
                else if (above > (_pageCount - 4))
                {
                    above = _pageCount;
                    below = (_pageCount - nrOfPagesToDisplay);
                }

                start = below;
                end = above;
            }

            if (start > 3)
            {
                sb.Append(GeneratePageLink("1", 1));
                sb.Append(GeneratePageLink("2", 2));
                sb.Append("...");
            }
            for (int i = start; i <= end ; i++)
            {
                if (i == this._currentPage)
                {
                    sb.AppendFormat("<span class=\"nhs_CurrentPage\">{0}</span>", i);
                }
                else
                {
                    sb.Append(GeneratePageLink(i.ToString(), i));
                }
            }
            if (end < (_pageCount - 3))
            {
                sb.Append("...");
                sb.Append(GeneratePageLink((_pageCount - 1).ToString(), _pageCount - 1));
                sb.Append(GeneratePageLink(_pageCount.ToString(), _pageCount));
            }

            // Next
            sb.Append(this._currentPage < _pageCount
                          ? GeneratePageLink("Next &gt;", (this._currentPage + 1))
                          : "<span class=\"nhs_DisabledPage\">Next &gt;</span>");

            //for (int i = 1; i <= _pageCount; i++)
            //{
            //    sb.Append(GeneratePageLink(i.ToString(), i));
            //}
            sb.Append("</p>");
            return sb.ToString();
        }

        private string GeneratePageLink(string linkText, int pageNumber)
        {
            string linkUrl = NhsRoute.CurrentRoute.HostUrl;

            if (linkUrl.ToLower().IndexOf("search") != -1) // case of search by facet (the url changes to the form commresults/search/params....)
                linkUrl = UserSession.GetItem("SearchUrl").ToString().TrimEnd('/');

            // when the url is not friendly, example it comes from advsearch the CurrentRoute.HostUrl doesnt returns it ok, so use CurrentRoute.Params.ToUrl()
            if (linkUrl.IndexOf("-") != -1)
            {
                if (linkUrl.IndexOf("page") != -1)
                    linkUrl = linkUrl.Substring(0, linkUrl.IndexOf("page-") - 1);

                linkUrl += "/page-" + pageNumber;
            }
            else
            {
                if(linkUrl.IndexOf("=") == -1)
                    linkUrl = linkUrl + "?" + NhsRoute.CurrentRoute.Params.ToUrl();

                if (linkUrl.IndexOf("page") != -1)
                    linkUrl = linkUrl.Substring(0, linkUrl.IndexOf("page") - 1);

                linkUrl += "&page=" + pageNumber;
                
            }

            string linkFormat = "<a href=\"{0}\">{1}</a>"; 
            
            return String.Format(linkFormat, linkUrl, linkText);
        }
    }
}
