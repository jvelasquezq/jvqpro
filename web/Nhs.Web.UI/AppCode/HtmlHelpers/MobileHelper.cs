﻿using System;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Mvc.Domain.Model.Web.Partials;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResultMobile;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class MobileHelper
    {
        public static MvcHtmlString GetPhoneNumber(this QuickViewViewModel model)
        {
            return model.PhoneNumber.CreateMobilePhone("");
        }

        public static MvcHtmlString GetPhoneNumber(this CommunityMapCard model)
        {
            var data = string.Format("data-commid=\"{0}\" data-builderid=\"{1}\"", model.Id, model.BId);
            return model.PhoneNumber.CreateMobilePhone(attr: data);
        }

        public static MvcHtmlString GetPhoneNumber(this CommunityItemMobileViewModel model)
        {
            var data = string.Format("data-commid=\"{0}\" data-builderid=\"{1}\"", model.Id, model.BuilderId);
            return model.Phone.CreateMobilePhone(attr: data);
        }

        public static MvcHtmlString GetPhoneNumber(this BoylResult model)
        {
            var data = string.Format("data-commid=\"{0}\" data-builderid=\"{1}\"", model.CommunityId, model.BuilderId);
            return model.Phone.CreateMobilePhone(attr: data);
        }

        public static MvcHtmlString CreateMobilePhone(this string phone, string @class = "ir", string text = "", string attr = "")
        {
            if (string.IsNullOrEmpty(phone))
                return "".ToMvcHtmlString();

            if (string.IsNullOrEmpty(text))
                text = LanguageHelper.Call;

            phone = phone.GetPhoneNumber();
            
            if (phone.Length == 10)
                return String.Format("<a class=\"nhs_CallBtn btn {0}\" href=\"tel:{1}\" {3}>{2}</a>", @class, phone, text, attr).ToMvcHtmlString();

            return "".ToMvcHtmlString();
        }

        public static MvcHtmlString CreateDisplayPhone(this string phone)
        {
            return phone.CreateMobilePhone("", phone.GetPhoneNumber().FormatPhone());
        }
    }
}