﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Mvc.Routing.Configuration;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class NhsPro7Helper
    {
        /// <summary>
        /// Renders Home Page Footer Box Content (including builder and home buyer text, or facebook and twitter texts depending on Active User or 30 day cookie
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="isProPL"></param>
        /// <returns></returns>
        public static MvcHtmlString RenderHomePageFooterBoxContent(this HtmlHelper helper, bool isProPL)
        {
            StringBuilder sb = new StringBuilder();
            var activeUserOrCookie = (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser) ||
                       (HttpContext.Current.Request.Cookies["NHSPro_Cookie"] != null &&
                        DateTime.Compare(
                            System.Convert.ToDateTime(HttpContext.Current.Request.Cookies["NHSPro_Cookie"].Value),
                            DateTime.Now) <= 30);
            if (!isProPL)
            {
                sb.Append(String.Format("<div class='{0}'>", activeUserOrCookie ? "pro_Facebook" : "pro_FooterBuyer"));
                sb.Append(helper.RenderSeoContent(SeoTemplateType.Pro_Home,
                                                  activeUserOrCookie ? "social-facebook" : "footerhomebuyertext"));
            }
            else
            {
                sb.Append(String.Format("<div class='{0}'>", "pro_Facebook"));
                sb.Append(helper.RenderSeoContent(SeoTemplateType.Pro_Home,
                                                  "social-facebook"));
            }
            sb.Append("</div>");
            if (!isProPL)
            {
                sb.Append(String.Format("<div class='{0}'>", activeUserOrCookie ? "pro_Twitter" : "pro_FooterBuilder"));
                sb.Append(helper.RenderSeoContent(SeoTemplateType.Pro_Home,
                                                  activeUserOrCookie ? "social-twitter" : "footerbuildertext"));
            }
            else
            {
                sb.Append(String.Format("<div class='{0}'>", "pro_Twitter"));
                sb.Append(helper.RenderSeoContent(SeoTemplateType.Pro_Home,
                                                  "social-twitter"));
            }
            sb.Append("</div>");

            return MvcHtmlString.Create(sb.ToString());

        }
        /// <summary>
        /// Renders home page content.  Checks if user is active.  In case it is not active it checks for 30 day cookie
        /// </summary>
        /// <param name="helper"></param>
        /// <returns></returns>
        public static MvcHtmlString RenderHomePageContent(this HtmlHelper helper)
        {
            StringBuilder sb = new StringBuilder();
            if (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser)
            {
                sb.Append(helper.RenderSeoContent(SeoTemplateType.Pro_Home, "promotextleftloggedin"));
            }
            else
            {
                if (HttpContext.Current.Request.Cookies["NHSPro_Cookie"] != null &&
                    DateTime.Compare(System.Convert.ToDateTime(HttpContext.Current.Request.Cookies["NHSPro_Cookie"].Value), DateTime.Now) <= 30)
                {
                    sb.Append(helper.RenderSeoContent(SeoTemplateType.Pro_Home, "signuptext"));
                    sb.Append(helper.NhsModalWindowLink("Sign Up", Pages.RegisterModal, ModalWindowsConst.RegisterModalWidth,
                                                        ModalWindowsConst.RegisterModalHeight, new List<RouteParam>(),
                                                        new
                                                            {
                                                                @class = "btn btn_SignIn",
                                                                onclick =
                                                            "$jq.googlepush('Account Events','Create Account','Open Form - Header Link')"
                                                            }));
                }
                else
                    sb.Append(helper.RenderSeoContent(SeoTemplateType.Pro_Home, "promotextleft"));
            }
            return MvcHtmlString.Create(sb.ToString());
        }
    }
}