﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Helpers.Content;
using Nhs.Library.Web;
using Nhs.Library.Web.ImageResizer;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Mvc.Routing.Utils;
using Resources;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System.Web.Routing;

    public static class ImageHtmlHelper
    {
        // images path that handle the helper so urls are transformed to the new url format for resizing
        private static List<string> includePaths = new List<string>
        {
            "images/homes", 
            "images/basiclisting",
            "images/brandshowcase",
            "images/ownerstories"
        };
        // images path that should exclude the helper to be re formated and shouldnt be resized
        private static List<string> excludePaths = new List<string> { };


        #region Image Links

        public static MvcHtmlString NhsImageLink(this HtmlHelper helper, string imgSrc, string linkSrc, List<RouteParam> paramList, ResizeCommands resizerCommands, bool preserveCase = false)
        {
            return NhsImageLink(helper, imgSrc, linkSrc, paramList, resizerCommands, null, null, preserveCase);
        }

        public static MvcHtmlString NhsImageLink(this HtmlHelper helper, string imgSrc, string linkSrc, List<RouteParam> paramList, ResizeCommands resizerCommands, object linkAttributes, object imgAttributes, bool preserveCase = false)
        {
            return NhsImageLink(helper, imgSrc, linkSrc, paramList, resizerCommands, linkAttributes, imgAttributes, false, false, preserveCase);
        }

        public static MvcHtmlString NhsImageLink(this HtmlHelper helper, string imgSrc, string linkSrc, List<RouteParam> paramList, ResizeCommands resizerCommands, object linkAttributes, object imgAttributes, bool makeModal, bool isLinkSrcNonNhsUri, bool preserveCase = false, bool isAbsoluteUrl = false)
        {
            const string http = "http://";

            imgSrc = FormatImageUrlForIrs(imgSrc);

            //Construct <img> 
            var imageTag = BuildImageTag(helper, imgSrc, resizerCommands, new RouteValueDictionary(imgAttributes));

            //Construct <a href>
            string linkUrl = "";
            if (!string.IsNullOrEmpty(linkSrc))
            {
                string linkSrcWithHttp = (!linkSrc.Contains(http) && isLinkSrcNonNhsUri)
                                             ? string.Concat(http, linkSrc)
                                             : linkSrc;

                linkUrl = isLinkSrcNonNhsUri || paramList == null ? linkSrcWithHttp : paramList.ToUrl(linkSrc, !preserveCase);
                //Use original page name if non-NHS url ELSE use paramList
            }

            if (isAbsoluteUrl)
                linkUrl = NhsUrlHelper.ResolveServerUrl(linkUrl);

            //Push <img/> inside <a href></a> => <a href><img/></a>
            return BuildHrefTag(linkUrl, imageTag.ToString(), linkAttributes, makeModal);
        }


        public static MvcHtmlString NhsAsyncImageLink(this HtmlHelper helper, string imgSrc, string linkSrc, List<RouteParam> paramList, ResizeCommands resizerCommands, bool preserveCase = false)
        {
            return NhsAsyncImageLink(helper, imgSrc, linkSrc, paramList, resizerCommands, null, null, preserveCase);
        }

        public static MvcHtmlString NhsAsyncImageLink(this HtmlHelper helper, string imgSrc, string linkSrc, List<RouteParam> paramList, ResizeCommands resizerCommands, object linkAttributes, object imgAttributes, bool preserveCase = false)
        {
            return NhsAsyncImageLink(helper, imgSrc, linkSrc, paramList, resizerCommands, linkAttributes, imgAttributes, false, false, preserveCase);
        }

        public static MvcHtmlString NhsAsyncImageLink(this HtmlHelper helper, string imgSrc, string linkSrc, List<RouteParam> paramList, ResizeCommands resizerCommands, object linkAttributes, object imgAttributes, bool makeModal, bool isLinkSrcNonNhsUri, bool preserveCase = false, bool isAbsoluteUrl = false)
        {
            const string http = "http://";

            imgSrc = FormatImageUrlForIrs(imgSrc);

            //Construct <img> 
            var imageTag = BuildAsyncImageTag(helper, imgSrc, resizerCommands, new RouteValueDictionary(imgAttributes));

            //Construct <a href>
            var linkUrl = "";
            if (!string.IsNullOrEmpty(linkSrc))
            {
                var linkSrcWithHttp = (!linkSrc.Contains(http) && isLinkSrcNonNhsUri)
                                             ? string.Concat(http, linkSrc)
                                             : linkSrc;

                linkUrl = isLinkSrcNonNhsUri || paramList == null ? linkSrcWithHttp : paramList.ToUrl(linkSrc, !preserveCase);
                //Use original page name if non-NHS url ELSE use paramList
            }

            if (isAbsoluteUrl)
                linkUrl = NhsUrlHelper.ResolveServerUrl(linkUrl);

            //Push <img/> inside <a href></a> => <a href><img/></a>
            return BuildHrefTag(linkUrl, imageTag.ToString(), linkAttributes, makeModal);
        }

        public static MvcHtmlString NhsActionImageLink(this HtmlHelper helper, string imgSrc, ActionResult actionResult, ResizeCommands resizerCommands)
        {
            return NhsActionImageLink(helper, imgSrc, resizerCommands, actionResult, string.Empty);
        }

        public static MvcHtmlString NhsActionImageLink(this HtmlHelper helper, string imgSrc, ResizeCommands resizerCommands, ActionResult actionResult, string altText)
        {
            return NhsActionImageLink(helper, imgSrc, actionResult, resizerCommands, null, new { alt = altText });
        }

        public static MvcHtmlString NhsActionImageLink(this HtmlHelper helper, string imgSrc, ActionResult actionResult, ResizeCommands resizerCommands, object linkAttributes, object imgAttributes)
        {
            string plh = "[replaceme]";
            var imageTag = BuildImageTag(helper, imgSrc, resizerCommands, new RouteValueDictionary(imgAttributes)).ToString();
            var linkSrc = NhsActionLink(helper, plh, actionResult, linkAttributes).ToString();
            return MvcHtmlString.Create(linkSrc.Replace(plh, imageTag));
        }
        #endregion


        #region Image
        public static MvcHtmlString NhsImage(this HtmlHelper helper, string imgSrc, ResizeCommands resizerCommands,
            string altText, object imgHtmlAttributes, bool prefixResourceDomain, bool removeUnderScore = true)
        {
            imgSrc = FormatImageUrlForIrs(imgSrc, removeUnderScore);

            var attributes = new RouteValueDictionary(imgHtmlAttributes);
            if (!string.IsNullOrEmpty(altText) && !attributes.ContainsKey("alt"))
                attributes.Add("alt", altText);

            var imageTag = BuildImageTag(helper, imgSrc, resizerCommands, attributes);
            return imageTag;
        }

        public static MvcHtmlString NhsImage(this HtmlHelper helper, string imgSrc, ResizeCommands resizerCommands, string altText, bool prefixResourceDomain, bool removeUnderscore = true)
        {
            return helper.NhsImage(imgSrc, resizerCommands, altText, null, prefixResourceDomain, removeUnderscore);
        }

        public static MvcHtmlString AsyncNhsImage(this HtmlHelper helper, string imgSrc, ResizeCommands resizerCommands, string altText, bool prefixResourceDomain, bool removeUnderScore = true, bool useSchemaOrg = false)
        {
            var attributes = new RouteValueDictionary();

            if (!string.IsNullOrEmpty(altText) && !attributes.ContainsKey("alt"))
                attributes.Add("alt", altText);

            if (!attributes.ContainsKey("class"))
                attributes.Add("class", "async");

            if (useSchemaOrg)
                attributes.Add("itemprop", "contentUrl");

            imgSrc = FormatImageUrlForIrs(imgSrc, removeUnderScore);
            var imageTag = BuildAsyncImageTag(helper, imgSrc, resizerCommands, attributes);
            return imageTag;
        }
        #endregion

        #region Helper Methods

        private static MvcHtmlString NhsActionLink(this HtmlHelper helper, string linkText, ActionResult actionResult, object linkAttributes)
        {
            var routeValues = actionResult.GetRouteValueDictionary();
            string routeName = RouteHelper.GetRouteNameFromAction(routeValues);

            if (NhsRoute.PartnerSiteUrl.Length > 0 && !string.IsNullOrEmpty(routeName))
                routeName = "p" + routeName;

            return MvcHtmlString.Create(HtmlHelper.GenerateLink(helper.ViewContext.RequestContext, helper.RouteCollection, linkText, routeName, null, null, routeValues, new RouteValueDictionary(linkAttributes)));
        }


        public static MvcHtmlString BuildImageTag(this HtmlHelper helper, string imageUrl, ResizeCommands resizerCommands, IDictionary<string, object> htmlAttributes)
        {
            if (string.IsNullOrWhiteSpace(imageUrl))
                return MvcHtmlString.Create(string.Empty);

            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            var tag = new TagBuilder("img");

            if (IndexOf(imageUrl, includePaths) && !string.IsNullOrEmpty(Configuration.IRSDomain))
                tag.Attributes["src"] = urlHelper.Image(imageUrl, resizerCommands);
            else
                tag.Attributes["src"] = imageUrl;

            tag.MergeAttributes(htmlAttributes);
            return MvcHtmlString.Create(tag.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString BuildAsyncImageTag(this HtmlHelper helper, string imageUrl, ResizeCommands resizerCommands, IDictionary<string, object> htmlAttributes)
        {
            if (string.IsNullOrWhiteSpace(imageUrl))
                return MvcHtmlString.Create(string.Empty);

            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            var tag = new TagBuilder("img");

            if (htmlAttributes != null && !htmlAttributes.ContainsKey("class"))
                htmlAttributes.Add("class", "async");

            if (IndexOf(imageUrl, includePaths) && !string.IsNullOrEmpty(Configuration.IRSDomain))
                tag.Attributes["data-src"] = urlHelper.Image(imageUrl, resizerCommands);
            else
                tag.Attributes["data-src"] = imageUrl;

            tag.Attributes["src"] = GlobalResources14.Default.images._1x1_gif;
            tag.MergeAttributes(htmlAttributes);
            return MvcHtmlString.Create(tag.ToString(TagRenderMode.SelfClosing));
        }

        private static MvcHtmlString BuildHrefTag(string linkUrl, string innerHtmlOrLinkText, object htmlAttributes, bool makeModal)
        {
            TagBuilder link = new TagBuilder("a");
            link.MergeAttribute("href", linkUrl);
            link.InnerHtml = innerHtmlOrLinkText;

            RouteValueDictionary attributes;
            if (htmlAttributes != null && htmlAttributes.GetType() == typeof(RouteValueDictionary))
                attributes = htmlAttributes as RouteValueDictionary;
            else
                attributes = new RouteValueDictionary(htmlAttributes);

            if (makeModal)
            {
                // Class attribute
                var @class = "thickbox";
                if (attributes.ContainsKey("class"))
                    @class += " " + attributes["class"];
                attributes["class"] = @class;
            }

            link.MergeAttributes(attributes, true);
            return MvcHtmlString.Create(link.ToString());

        }

        private static string FormatImageUrlForIrs(string imageUrl, bool removeUnderscore = true)
        {
            return ImageResizerUrlHelpers.FormatImageUrlForIrs(imageUrl, removeUnderscore);
        }

        private static bool IndexOf(string text, IEnumerable<string> itemsToSearch)
        {
            return ImageResizerUrlHelpers.IndexOf(text, itemsToSearch);
        }

        #endregion
    }
}
