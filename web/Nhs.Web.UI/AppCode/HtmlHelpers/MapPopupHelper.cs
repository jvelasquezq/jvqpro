using System.Collections.Generic;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResultMobile;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class MapPopupHelper

    {
        public static MvcHtmlString GetMapPopup(this HtmlHelper helper, CommunityItemMobileViewModel model)
        {
            return helper.GetMapPopup(model.Lat.ToType<decimal>(), model.Lng.ToType<decimal>(), model.Id, model.BuilderId, model.MarketId, model.PrLo, model.PrHi, model.MarketName, "ir btn nhs_MapBtn nhs_ModalPopUp");
        }

        public static MvcHtmlString GetMapPopup(this HtmlHelper helper, BoylResult model)
        {
            return helper.GetMapPopup(model.Latitude.ToType<decimal>(), model.Longitude.ToType<decimal>(), model.CommunityId, model.BuilderId, model.MarketId, model.PriceLow.ToType<string>(), model.PriceHigh.ToType<string>(), model.MarketName, "ir btn nhs_MapBtn nhs_ModalPopUp");
        }

        public static MvcHtmlString GetMapPopup(this HtmlHelper helper, IDetailViewModel model)
        {
            return helper.GetMapPopup(model.Latitude.ToType<decimal>(), model.Longitude.ToType<decimal>(), model.CommunityId, model.BuilderId, model.MarketId, model.PriceHigh, model.PriceHigh, model.MarketName, "ir btn nhs_MapBtn nhs_ModalPopUp");
        }

        public static MvcHtmlString GetMapPopup(this HtmlHelper helper, HomeItemViewModel model)
        {
            var isModelHouse = model.Status.ToLower() == "m";
            //model.IsBl
            return model.IsBl == 0
                ? helper.GetMapPopup(model.Lat.ToType<decimal>(), model.Lng.ToType<decimal>(),
                    model.CommId, model.BuilderId, model.MarketId,
                    model.PrHi, model.PrLo, model.MarketName, "nhs_MapLink thickbox", model.IsBasic != 0? 0 : model.HomeId, isModelHouse)
                : helper.GetBasicListingMapPopup(model.HomeId, model.Addr, model.Lat, model.Lng, model.PrHi, model.PrLo);
        }

        public static MvcHtmlString GetMapPopup(this HtmlHelper helper, CommunityItemViewModel model)
        {
            return helper.GetMapPopup(model.Lat.ToType<decimal>(), model.Lng.ToType<decimal>(),model.Id, model.BuilderId, model.MarketId, model.PrHi, model.PrLo, model.MarketName);
        }

        public static MvcHtmlString GetMapPopup(this HtmlHelper helper, string communityName, string builderName, string city, int marketId, string marketName, string state,
                                                int communityId, int builderId, string latitude, string longitude, string priceHigh,
                                                string priceLow)
        {
            const int width = ModalWindowsConst.MapPopupWidth;
            const int height = ModalWindowsConst.MapPopupHieght;
            var routeParams = new List<RouteParam>
                {
                    new RouteParam(RouteParams.CommunityId, communityId.ToType<string>(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.CommunityName, communityName, RouteParamType.QueryString),
                    new RouteParam(RouteParams.BuilderName, builderName, RouteParamType.QueryString),
                    new RouteParam(RouteParams.City, city, RouteParamType.QueryString),
                    //76890: MarketId has been removed from the signature of this method
                    //new RouteParam(RouteParams.MarketID, marketId, RouteParamType.QueryString),
                    new RouteParam(RouteParams.MarketName, marketName, RouteParamType.QueryString),
                    new RouteParam(RouteParams.State, state, RouteParamType.QueryString),
                    new RouteParam(RouteParams.BuilderId, builderId.ToType<string>(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.Latitude, latitude.ToType<string>(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.Longitude, longitude.ToType<string>(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.PriceHigh, priceHigh.ToType<string>(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.PriceLow, priceLow.ToType<string>(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.Width, width.ToType<string>(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.Height, height.ToType<string>(), RouteParamType.QueryString)

                };
            return helper.NhsLink(LanguageHelper.ShowMap, Pages.CommunityResultsMapPopup, routeParams, new { @class = "nhs_MapLink thickbox", @id = communityId + "-" + builderId, @title = LanguageHelper.ViewOnMap });
        }

        public static MvcHtmlString GetMapPopup(this HtmlHelper helper, decimal lat, decimal lng, int communityId, int builderId, int martketId, 
            string priceHigh, string priceLow, string marketName, string @class = "nhs_MapLink thickbox", int homeId = 0, bool isModelHouse = false)
        {
            if (lat == 0 && lng == 0) return null;
            
            const int width = ModalWindowsConst.MapPopupWidth;
            const int height = ModalWindowsConst.MapPopupHieght;
            var routeParams = new List<RouteParam>
            {
                new RouteParam(RouteParams.CommunityId, communityId, RouteParamType.QueryString),
                new RouteParam(RouteParams.MarketName, marketName, RouteParamType.QueryString),
                new RouteParam(RouteParams.BuilderId, builderId, RouteParamType.QueryString),                
                new RouteParam(RouteParams.PropertyId, homeId == 0? communityId : homeId, RouteParamType.QueryString),
                new RouteParam(RouteParams.Width, width, RouteParamType.QueryString),
                new RouteParam(RouteParams.Height, height, RouteParamType.QueryString)
            };

            if (isModelHouse == false)
            {
                routeParams.Add(new RouteParam(RouteParams.PriceHigh, priceHigh, RouteParamType.QueryString));
                routeParams.Add(new RouteParam(RouteParams.PriceLow, priceLow, RouteParamType.QueryString));
            }

            return helper.NhsLink(LanguageHelper.ShowMap, Pages.CommunityResultsMapPopup, routeParams, new { @class, @id = communityId + "-" + builderId, @title = LanguageHelper.ViewOnMap });
        }

        public static MvcHtmlString GetBasicListingMapPopup(this HtmlHelper helper, int homeId, string address, string lat, string lng, string priceHigh, string priceLow, string @class = "nhs_MapLink thickbox")
        {
            if (string.IsNullOrEmpty(lat) && string.IsNullOrEmpty(lng)) return null;
            
            const int width = ModalWindowsConst.MapPopupWidth;
            const int height = ModalWindowsConst.MapPopupHieght;
            var @params = new List<RouteParam>
            {
                new RouteParam(RouteParams.BasicListingId, homeId, RouteParamType.QueryString),
                new RouteParam(RouteParams.Address, address, RouteParamType.QueryString),
                new RouteParam(RouteParams.Latitude, lat, RouteParamType.QueryString),
                new RouteParam(RouteParams.Longitude, lng, RouteParamType.QueryString),
                new RouteParam(RouteParams.PriceHigh, priceHigh, RouteParamType.QueryString),
                new RouteParam(RouteParams.PriceLow, priceLow, RouteParamType.QueryString),
                new RouteParam(RouteParams.Width, width, RouteParamType.QueryString),
                new RouteParam(RouteParams.Height, height, RouteParamType.QueryString)
            };
            return helper.NhsLink(LanguageHelper.ShowMap, Pages.CommunityResultsBasicListingMapPopup, @params, new { @class, @id = homeId, @title = LanguageHelper.ViewOnMap });
        }
    }
}