﻿using System.Collections.Generic;
using System.Linq;
using Nhs.Library.Enums;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class LocationHandlerHelper
    {
        public static IEnumerable<IGrouping<LocationType, Location>> GetLocationGroups(this LocationHandlerViewModel model)
        {
            var group= model.Locations.GroupBy(p => ((LocationType)p.Type));
            return group;
        }

        public static string GetLocationGroupsName(this LocationHandlerViewModel model, LocationType groupName, int count)
        {
            if (model.Globals.PartnerLayoutConfig.IsBrandPartnerCna)
            {
                switch (groupName)
                {
                    case LocationType.Market:
                        if (count == 1)
                            return "1 área metropolitana";
                        return count + " áreas metropolitanas";
                    case LocationType.City:
                        if (count == 1)
                            return "1 ciudad";
                        return count + " ciudades";
                    case LocationType.County:
                        if (count == 1)
                            return "1 condado";
                        return count + " condados";
                    case LocationType.Zip:
                        if (count == 1)
                            return "1 código postal";
                        return count + " códigos postales";
                    case LocationType.Community:
                        if (count == 1)
                            return "1 comunidad";
                        return count + " comunidades";
                    case LocationType.Developer:
                        if (count == 1)
                            return "1 construtor";
                        return count + " construtores";

                }
            }
            return ((count > 1 ? "" : "a ") + groupName.Stringify() + (count > 1 ? "s" : "")).Replace("ys", "ies");
        }
    }
}