﻿using System;
using System.IO;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Xml;
using System.Xml.XPath;
using Bdx.Web.Api.Objects.Constants;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Traffic;
using Nhs.Library.Web;
using Nhs.Utility.Constants;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Mvc.Routing.Interface;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResultMobile;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class HtmlGoogleHelper
    {
        private const string XPATH_PAGE_SEARCH_STRING = @"/variables/variable[@name='{0}']";
        public static MvcHtmlString TrackingCommunityCount(this CommunityResultsViewModel model)
        {
            var typeSearch = model.TrigerByFacets ? "Refinement" : "Intial Search";
            if (NhsRoute.IsBrandPartnerNhsPro)
            {
                return
                    string.Format(
                        "<!-- Google Analytics Traffic Community Count -->" + Environment.NewLine +
                        "<script type='text/javascript'>jQuery.googlepush('CommunityResultsCount', '{0}', '{1}');</script>",
                        typeSearch, model.Results.TotalCommunities).ToMvcHtmlString();
            }
            return
                string.Format(
                    "<!-- Google Analytics Traffic Community Count -->" + Environment.NewLine +
                    "<script type='text/javascript'>setTimerToTrackCommunityCount('{0}', '{1}', 100);</script>",
                    typeSearch, model.Results.TotalCommunities).ToMvcHtmlString();
        }

        //Google Analytics 
        //_gaq.push(['_set', 'campaignParams','utm_campaign=CAMPAIGN&utm_source=SOURCE&utm_medium=MEDIUM']);        
        public static string GetSetTrafficCategorization()
        {
            //var trafficParam = TrafficHelper.Validate(HttpContext.Current.Request,
            //                                        HttpContext.Current.Request.UrlReferrer);

            var trafficParam = GetSetTrafficCategorizationString();

            return string.IsNullOrEmpty(trafficParam)
                       ? string.Empty
                       : string.Format(
                           "<!-- Google Analytics Traffic Categorization -->" + Environment.NewLine +
                           "<script type='text/javascript'>{0}</script>",
                           trafficParam);
        }

        //Google Analytics 
        //_gaq.push(['_setCustomVar', 1,'Market', 'Birmingham, AL:3',3]);
        //_gaq.push(['_trackEvent', 'Market', 'Marketname, State', 'MarketID']);
        public static MvcHtmlString GetSetCustomVar(string marketName, string stateName, int marketId)
        {
            return (NhsRoute.IsBrandPartnerNhsPro
                ? String.Format(
                    "<!-- Google Analytics -->{3}<script type='text/javascript'>{3}" +
                    "_gaq.push(['_setCustomVar', 1,'Market', '{0}, {1}:{2}',3]);{3}" +
                    "jQuery.googlepush('Market', '{0}', '{1}');{3}</script>",
                    marketName, stateName, marketId, Environment.NewLine)
                : string.Empty).ToMvcHtmlString();
        }

        public static string GetSetTrafficCategorizationString()
        {
            //var trafficParam = TrafficHelper.Validate(HttpContext.Current.Request,
            //                                        HttpContext.Current.Request.UrlReferrer);

            var trafficParam = TrafficHelperNew.Validate(HttpContext.Current.Request,
                                                      string.IsNullOrEmpty(UserSession.DynamicReferUrl)
                                                          ? null
                                                          : new Uri(UserSession.DynamicReferUrl));

            return string.IsNullOrEmpty(trafficParam)
                       ? string.Empty
                       : string.Format("_gaq.push(['_set', 'campaignParams', '{0}']);", trafficParam);
        }

        //TDV Gogle Data
        //dataLayer.push({'key': 'value'});        
        public static string SetDataLayerPair(string key, int optValue)
        {                            
            var value = string.Empty;
            
            var xmlPath = HttpContext.Current.Server.MapPath(@"~/" + Configuration.StaticContentFolder + @"/" + "tdvGtmEvents.xml");
                        
            if (File.Exists(xmlPath))
            {
                var doc = new XmlDocument();
                try
                {
                    doc.Load(xmlPath);
                    XmlNode root = doc.DocumentElement;
                    if (root != null)
                    {                                                
                        if (root.SelectSingleNode(string.Format(XPATH_PAGE_SEARCH_STRING, key)) != null &&
                            !string.IsNullOrEmpty(root.SelectSingleNode(string.Format(XPATH_PAGE_SEARCH_STRING, key)).Attributes["value"]
                                    .Value.Trim()))
                        {
                            value =
                                root.SelectSingleNode(string.Format(XPATH_PAGE_SEARCH_STRING, key)).Attributes["value"]
                                    .Value;
                            
                            if (key.ToLower().Equals("recommendedlead"))
                            {
                                value = (Convert.ToInt32(value)*optValue).ToString();
                            }
                        }                        
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogger.LogError("TDV Metric Read Error!", string.Format("Error reading XML File: {0}", xmlPath), "Check XML file. Detailed stack trace in the following entry.");
                    ErrorLogger.LogError(ex);
                    return string.Empty;
                }                
            }
            else
            {
                return string.Empty;
            }
            
            return ((!string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(value)) ? string.Format("dataLayer.push({{'{0}': '{1}'}});", key, value) : string.Empty);            
        }

        #region CreateDataLayer
        public static MvcHtmlString GetGtmDataLayerScript()
        {
            var id = "GTM-NCCM8F";

            var code = new StringBuilder();
            code.AppendLine("<!-- Google Tag Manager -->");
            code.AppendLine("<noscript>");
            code.AppendLine("   <iframe src=\"//www.googletagmanager.com/ns.html?id=" + id +
                            "\" height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe>");
            code.AppendLine("</noscript>");
            code.AppendLine("<script>   " +
                            "     (function (w, d, s, l, i) {" +
                            "        w[l] = w[l] || []; w[l].push({ 'gtm.start':" +
                            "new Date().getTime(), event: 'gtm.js'" +
                            "        }); var f = d.getElementsByTagName(s)[0]," +
                            "j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =" +
                            "'//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);" +
                            "    })(window, document, 'script', 'dataLayer', '" + id + "');" +
                            "</script>");
            code.AppendLine("<!-- Google Tag Manager -->");
            return code.ToMvcHtmlString();
        }

        public static MvcHtmlString GetGtmDataLayerForHomePage(this BaseViewModel model)
        {
            return GetGtmDataLayerGlobal("Home", model.Globals.PartnerInfo.PartnerName, model.Globals.PartnerInfo.BrandPartnerName, model.Globals.PartnerInfo.PartnerId);
        }

        public static MvcHtmlString GetGtmDataLayerGlobal(this BaseViewModel model)
        {
            var pageHaveCustomGtm = model.Globals.PartnerLayoutConfig.AmIOnDetailPage ||
                                    model.Globals.PartnerLayoutConfig.AmIOnCommunityResultsPage ||
                                    model.Globals.PartnerLayoutConfig.AmIOnHomeResultsPage ||
                                    model.Globals.PartnerLayoutConfig.AmIOnHomePage;

            if (pageHaveCustomGtm )
                return MvcHtmlString.Empty;

            var pageType = NhsRoute.CurrentRoute.Function;
            return GetGtmDataLayerGlobal(pageType, model.Globals.PartnerInfo.PartnerName,
                model.Globals.PartnerInfo.BrandPartnerName, model.Globals.PartnerInfo.PartnerId);
        }

        private static MvcHtmlString GetGtmDataLayerGlobal(string pageType, string partnerName, string brandPartnerName, int partnerId)
        {
            var data = GetBaseGtmDataLayer(pageType, partnerName, brandPartnerName, partnerId);
            return CreateDataLayer(data);
        }

        public static MvcHtmlString GetGtmDataLayerForCommResults(this CommunityResultsViewModel model)
        {
            var searchParams = UserSession.PropertySearchParameters;

            var data = GetBaseGtmDataLayer("Community_Results",
               model.Globals.PartnerInfo.PartnerName,
               model.Globals.PartnerInfo.BrandPartnerName, model.Globals.PartnerInfo.PartnerId);
            data.Add("'marketId': '" + model.Market.MarketId + "'");
            data.Add("'marketName': '" + model.Market.MarketName + ", " + model.Market.StateAbbr + "'");
            data.Add("'matchingResults': '" + (model.Results.TotalCommunities - model.Results.TotalNearbyCommunities) + "'");
            if (!string.IsNullOrEmpty(searchParams.City))
                data.Add("'city': '" + searchParams.City + "'");
            return CreateDataLayer(data);
        }

        public static MvcHtmlString GetGtmDataLayerForDetailPage(this IDetailViewModel model, bool isBasic = false)
        {
            if (model.IsInactive)
                return MvcHtmlString.Empty;

            var data = GetBaseGtmDataLayer((model.IsPageCommDetail ? "Community" : "Home") + "_Detail" + (isBasic?"_Basic":""),
                model.Globals.PartnerInfo.PartnerName,
                model.Globals.PartnerInfo.BrandPartnerName, model.Globals.PartnerInfo.PartnerId);

            data.Add("'marketId': '" + model.MarketId + "'");
            data.Add("'marketName': '" + model.MarketName + ", " + model.StateAbbr + "'");
            data.Add("'builderId': '" + model.BuilderId + "'");
            if (model.CommunityId > 0)
                data.Add("'communityId': '" + model.CommunityId + "'");
            if (!string.IsNullOrEmpty(model.CommunityCity))
                data.Add("'city': '" + model.CommunityCity.Replace(" ", "_") + "'");
            data.Add("'priceRange': " +
                     GetPrice(model.PriceLow.ToType<decimal>(), model.PriceHigh.ToType<decimal>(),
                         model.IsPageCommDetail));
            if (!string.IsNullOrEmpty(model.CorporationName))
                data.Add("'builderCorp': '" + HttpUtility.JavaScriptStringEncode(model.CorporationName) + "'");
            if (!string.IsNullOrEmpty(model.BuilderName))
                data.Add("'builderDivision': '" + HttpUtility.JavaScriptStringEncode(model.BuilderName) + "'");
            if (!string.IsNullOrEmpty(model.CommunityName))
                data.Add("'communityName': '" + HttpUtility.JavaScriptStringEncode(model.CommunityName) + "'");

            if (model.Amenities!=null && model.Amenities.Any() || model.IsGreenProgram || model.IsAgeRestricted || model.IsAdult)
                data.Add(BuildAmenities(model));
            return CreateDataLayer(data);
        }

        public static MvcHtmlString GetGtmDataLayerForCommResults(this CommunityMobileViewModel model)
        {
            var searchParams = UserSession.SearchParametersV2;
            var nearbyCount = (searchParams.SortBy == SortBy.Random ? model.ResultCounts.NbyCommCount : 0);
            var data = GetBaseGtmDataLayer("Community_Results", model.Globals.PartnerInfo.PartnerName,
                model.Globals.PartnerInfo.BrandPartnerName, model.Globals.PartnerInfo.PartnerId);
            data.Add("'matchingResults': '" + (model.ResultCounts.CommCount - nearbyCount) + "'");
            if (model.Market != null)
            {
                data.Add("'marketId': '" + model.Market.MarketId + "'");
                data.Add("'marketName': '" + model.Market.MarketName + ", " + model.Market.StateAbbr + "'");
            }

            if (!string.IsNullOrEmpty(searchParams.City))
                data.Add("'city': '" + searchParams.City + "'");
            return CreateDataLayer(data);
        }

        //CommResults v2
        public static MvcHtmlString GetGtmDataLayerForCommResults(this BaseCommunityHomeResultsViewModel model)
        {
            var searchParams = UserSession.SearchParametersV2;
            var searchType = model.SrpPageType == SearchResultsPageType.CommunityResults ? "Community_Results" : "Home_Results"; 

            var nearbyCount = (searchParams.SortBy == SortBy.Random ? model.ResultCounts.NbyCommCount : 0);
            var data = GetBaseGtmDataLayer(searchType, model.Globals.PartnerInfo.PartnerName,
                model.Globals.PartnerInfo.BrandPartnerName, model.Globals.PartnerInfo.PartnerId);
            if (model.Market != null)
            {
                data.Add("'marketId': '" + model.Market.MarketId + "'");
                data.Add("'marketName': '" + model.Market.MarketName + ", " + model.Market.StateAbbr + "'");
            }
            data.Add("'matchingResults': '" + (model.ResultCounts.CommCount - nearbyCount) + "'");
            if (!string.IsNullOrEmpty(searchParams.City))
                data.Add("'city': '" + searchParams.City + "'");
            if (searchParams.IsMultiLocationSearch && searchParams.SyntheticInfo != null)
            {
                data.Add("'synthGeo': '" + searchParams.SyntheticInfo.Name + "'");
            }

            return CreateDataLayer(data);
        }

        private static List<string> GetBaseGtmDataLayer(string pageType, string partnerName, string brandPartnerName, int partnerId)
        {
            var data = new List<string>
            {
                "'pageType': '" + pageType + "'",
                "'partnerName': '" + partnerName + "'",
                "'brandPartnerName': '" + brandPartnerName + "'",
                "'partnerId': '" + partnerId + "'",
                "'adaptiveTemplate': '" + (NhsRoute.ShowMobileSite ? "M" : "D") + "'"
            };
            return data;
        }


        private static MvcHtmlString CreateDataLayer(IEnumerable<string> data)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("<!--GTM Data Layer-->");
            stringBuilder.AppendLine("<script type='text/javascript'>" + Environment.NewLine + "dataLayer =[{");
            stringBuilder.AppendLine(string.Join("," + Environment.NewLine, data.Where(p => !string.IsNullOrEmpty(p))));
            stringBuilder.AppendLine("}];" + Environment.NewLine + "</script>");
            return stringBuilder.ToMvcHtmlString();
        }
        
        #endregion

        private static string BuildAmenities(IDetailViewModel model)
        {
            var amenities = (from amenity in model.Amenities from amen in amenity.Value select amen.Description).ToList();
            var amenitiesList = new List<string>();
            if (amenities.Any(p => p == "Pool"))
                amenitiesList.Add("'Pool'");

            if (amenities.Any(p => p == "Golf Course"))
                amenitiesList.Add("'Golf'");

            if (amenities.Any(p => p == "Green Belt" || p == "Pond" || p == "Marina" || p == "Lake" || p == "Beach"))
                amenitiesList.Add("'Nature'");

            if (amenities.Any(p => p == "Park"))
                amenitiesList.Add("'Park'");

            if (amenities.Any(p => p == "Views"))
                amenitiesList.Add("'Views'");

            if (amenities.Any(p => p == "Water Front Lots"))
                amenitiesList.Add("'Waterfront'");

            if (amenities.Any(p => p == "Tennis" || p == "Soccer" || p == "Volleyball" || p == "Basketball" || p == "Baseball" || p == "Trails"))
                amenitiesList.Add("'Sports'");

            if (model.IsGreenProgram)
                amenitiesList.Add("'Green'");

            if (model.IsAdult || model.IsAgeRestricted)
                amenitiesList.Add("'Adult'");

            return amenitiesList.Any()
                       ? (amenitiesList.Count > 1
                              ? "'amenities': [" + string.Join(",", amenitiesList) + "]"
                              : "'amenities': " + string.Join(",", amenitiesList) + "")
                       : "";
        }

        private static MvcHtmlString CreateDataLayer(List<string> data)
        {
            // If cookie or value(s) are not available at runtime, skip.
            if (!string.IsNullOrEmpty(UserSession.GoogleUtmaUId) && !string.IsNullOrEmpty(UserSession.GoogleUtmaSId))
            {
                data.Add("'uid' : '" + UserSession.GoogleUtmaUId + "'");
                data.Add("'sid' : '" + UserSession.GoogleUtmaSId + "'");
            }


            if (!string.IsNullOrEmpty(UserSession.UserProfile.GenericGUID))
            {
                data.Add("'i_uid' : '" + UserSession.UserProfile.GenericGUID + "'");
            }

            
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("<!--GTM Data Layer-->");
            stringBuilder.AppendLine("<script type='text/javascript'>" + Environment.NewLine + "dataLayer =[{");
            stringBuilder.AppendLine(string.Join("," + Environment.NewLine, data.Where(p => !string.IsNullOrEmpty(p)).Select(s => s)));
            stringBuilder.AppendLine("}];" + Environment.NewLine + "</script>");

            return stringBuilder.ToMvcHtmlString();
        }

        private static string GetPrice(decimal priceLow, decimal priceHigh, bool isPageCommDetail)
        {
            if (isPageCommDetail)
            {
                if (priceLow > priceHigh) //60379
                {
                    var tmpPrice = priceHigh;
                    priceHigh = priceLow;
                    priceLow = tmpPrice;
                }

                var low = PriceRange(priceLow);
                if (priceLow == priceHigh)
                    return "'p" + (low < 10 ? low.ToType<string>() : "0" + low) + "'";

                var high = PriceRange(priceHigh);
                var prices = new List<string>();
                for (var i = low; i <= high; i++)
                {
                    if (i < 10)
                        prices.Add("'p0" + i + "'");
                    else
                        prices.Add("'p" + i + "'");
                }

                return prices.Count > 1 ? "[" + string.Join(", ", prices) + "]" : prices[0];
            }
            else
            {
                var low = PriceRange(priceLow);
                    return "'p" + (low < 10 ? low.ToType<string>() : "0" + low) + "'";
            }
        }

        private static int PriceRange(decimal price)
        {
            if (price < 100000)
                return 0;
            if (price >= 100000 && price <= 199999)
                return 1;
            if (price >= 200000 && price <= 299999)
                return 2;
            if (price >= 300000 && price <= 399999)
                return 3;
            if (price >= 400000 && price <= 499999)
                return 4;
            if (price >= 500000 && price <= 599999)
                return 5;
            if (price >= 600000 && price <= 699999)
                return 6;
            if (price >= 700000 && price <= 799999)
                return 7;
            if (price >= 800000 && price <= 899999)
                return 8;
            if (price >= 900000 && price <= 999999)
                return 9;
            return 10;
        }

        public static MvcHtmlString GetGoogleMapScript(this HtmlHelper helper)
        {
            var script = new StringBuilder();
            script.AppendFormat(
                "<script src=\"{0}\"", GetGoogleMapScript());
            script.Append("  type=\"text/javascript\">");
            script.Append("</script>");
            return script.ToString().ToMvcHtmlString();
        }

        public static string GetGoogleMapScript()
        {
            var chanel = "webapp_" + NhsRoute.BrandPartnerId + "_" + (NhsRoute.IsMobileDevice ? "Mobile" : "Desktop");
            return string.Format("https://maps.googleapis.com/maps/api/js?v=3.18&client={0}&sensor=true&language={1}&libraries=geometry,places&channel={2}", Configuration.GoogleMapsApiClientId, "en", chanel);
        }

    }
}
