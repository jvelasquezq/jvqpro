﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class ProCrmHelper
    {

        public static string GetClientColumn(this HtmlHelper htmlHelper)
        {
            var stringBuilder = new StringBuilder();
            var link = "<h2>" + htmlHelper.NhsLink("#=FullName #",
                                          new List<RouteParam> { new RouteParam(RouteParams.ClientId, "#=data.ClientId #", RouteParamType.Friendly, true, false) }, Pages.ClientDetail, true) + "</h2>";
            stringBuilder.Append(link);
            stringBuilder.Append("#=GetRelated(RelatedFirstName,RelatedLastName)#");
            return stringBuilder.ToString();
        }

        public static string GetActivitiesColumn(this HtmlHelper htmlHelper)
        {
            return htmlHelper.NhsModalWindowLink("#=data.Description #", "procrm/taskmodal/clientid-#=data.ClientId #/taskid-#=data.ClientTaskId #", ModalWindowsConst.ProCrmTaskWidth, ModalWindowsConst.ProCrmTaskHeight, new List<RouteParam>(), new { title = "Edit Activity" }, false, false).ToString();
        }

        public static MvcHtmlString GetFinalSourceOfClients(this AddNewClientsViewModel model)
        {
            return
                string.Format("[{0}]",
                              string.Join(",",
                                          model.ClientSourceList.Select(
                                              p =>
                                              string.Format("{{\"Text\":\"{0}\",\"Value\":\"{1}\"}}", p.Text, p.Value))))
                      .ToMvcHtmlString();
        }

        public static MvcHtmlString GetFinalSourceOfClients(this AddEmailToNewClients model)
        {
            return
                string.Format("[{0}]",
                              string.Join(",",
                                          model.ClientSourceList.Select(
                                              p =>
                                              string.Format("{{\"Text\":\"{0}\",\"Value\":\"{1}\"}}", p.Text, p.Value))))
                      .ToMvcHtmlString();
        }

        public static MvcHtmlString GetFavoritesLink(this HtmlHelper htmlHelper,string text, ListingType listingType, int listingId, int comId, int builderId, object linkAttributes, bool isNextSteps, bool isComResults)
        {
            var @params = new List<RouteParam>
                {
                    new RouteParam(RouteParams.ListingType, listingType.Stringify(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.CommunityId, comId, RouteParamType.QueryString),
                    new RouteParam(RouteParams.BuilderId, builderId.ToString(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.PropertyId, listingId.ToString(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.IsNextSteps, isNextSteps.ToString(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.IsCommunityResultsPage, isComResults.ToString(), RouteParamType.QueryString)
                };
            return htmlHelper.NhsModalWindowLink(text, Pages.GetSavetoFavorites,
                                                 ModalWindowsConst.ProCrmFavoriteWidth, ModalWindowsConst.ProCrmFavoriteHeight,
                                                 @params, linkAttributes);
        }

        public static MvcHtmlString GetSaveFavoritesLink(this HtmlHelper htmlHelper, AddToFavoritesClient model, string text)
        {
            var savedToPlanner = false;
            var url = htmlHelper.GetActionUrl(NhsMvc.ProCrm.Name, NhsMvc.ProCrm.ActionNames.SaveToFavoritesAgent);
            int planId = 0, specId = 0;
            switch (model.ListingType)
            {
                case "C":
                    {
                        var pl = new PlannerListing(model.PropertyId, ListingType.Community);
                        if (UserSession.UserProfile.Planner.SavedCommunities.Contains(pl))
                            savedToPlanner = true;
                    }
                    break;
                case "P":
                    {
                        planId = model.PropertyId;
                        var pl = new PlannerListing(model.PropertyId, ListingType.Plan);
                        if (UserSession.UserProfile.Planner.SavedHomes.Contains(pl))
                            savedToPlanner = true;
                    }
                    break;
                default:
                    {
                        specId = model.PropertyId;
                        var pl = new PlannerListing(model.PropertyId, ListingType.Spec);
                        if (UserSession.UserProfile.Planner.SavedHomes.Contains(pl))
                            savedToPlanner = true;
                    }
                    break;
            }


            // Sets link properties
            if (savedToPlanner)
            {
                return MvcHtmlString.Create("<span class=\"pro_Saved\">This listing has been saved to your personal list of favorites</span>");
            }
            var gp = model.IsCommunityResultsPage
                     ? "'Community Results Events', 'Community Item'"
                     : string.Format("'{0} Detail Events','{0} - {1}'",
                                     model.ListingType == "C" ? "Community" : "Home",
                                     model.IsNextSteps ? "Next Steps" : "Gallery");
            object parameters = new
                {
                    onclick = string.Format(
                        "CrmSF.SaveToFavorites({0},{1},{2},{3},'{4}'); $jq.googlepush({5}, 'Save to favorites'); return false;",
                        model.CommunityId, model.BuilderId, planId, specId, url, gp),
                    tittle = "Save this listing as a client favorite?",
                    id = "pro_savetofavoritesagent"
                };
            return htmlHelper.NhsLink(text, Pages.RecentItems, new List<RouteParam>(), parameters);
        }
    }
}
