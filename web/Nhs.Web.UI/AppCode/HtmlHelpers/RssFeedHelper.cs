﻿using System.Collections.Generic;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Helpers.Content;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class RssFeedHelper
    {
        public static List<RssFeed> GetRssFeeds(this HtmlHelper helperRssFeed, string rssUrl)
        {
            var feeds = new List<RssFeed>(RssReader.GetRssFeed(rssUrl));
            return feeds;
        }
    }
}