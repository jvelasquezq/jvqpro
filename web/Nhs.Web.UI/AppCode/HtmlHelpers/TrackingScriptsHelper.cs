﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers.Tracking;
using Nhs.Library.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class TrackingScriptsHelper
    {
        public static MvcHtmlString TrackingScript(this HtmlHelper helper, string file, IPathMapper pathMapper, string partnerSiteUrl)
        {
            return helper.StaticContent(String.Format("TrackingScripts/{0}", file), new List<ContentTag>(), pathMapper, partnerSiteUrl);
        }

        public static IList<string> GetTrackingScriptList(int brandPartnerId, int partnerId, string pageFunction, string hostUrl, IPathMapper pathMapper, bool showsInHead)
        {
            var scriptFiles = new List<string>();

            var scriptsHelper = new TrackingScriptsReader(pathMapper);
            var trackingScripts = scriptsHelper.ParseScripts();

            foreach (var script in trackingScripts)
            {
                var brand = script.ApplyAllBrandPartners
                            ? script.BrandPartners[0]
                            : script.BrandPartners.SingleOrDefault(s => s.BrandPartnerId == brandPartnerId);

                if (brand != null)
                {
                    var partner = brand.ApplyAllPartners
                                  ? brand.Partners[0]
                                  : brand.Partners.SingleOrDefault(p => p.PartnerId == partnerId |
                                                                        (!string.IsNullOrEmpty(p.SiteUrl) && NhsRoute.PartnerSiteUrl.ToLower().Trim() == p.SiteUrl));


                    if (partner != null)
                    {
                        if (((partner.ApplyAllFunctions || partner.Functions.Contains(pageFunction.ToLower())) && script.AddToHead == showsInHead) || pageFunction.Equals("iphoneinterstitial", StringComparison.CurrentCultureIgnoreCase))
                            if (pageFunction.Equals("iphoneinterstitial", StringComparison.CurrentCultureIgnoreCase))
                            {
                                if(showsInHead && (script.FileName.Equals("GATagNhs.html", StringComparison.CurrentCultureIgnoreCase) ||
                                    script.FileName.Equals("GATagMove.html", StringComparison.CurrentCultureIgnoreCase))
                                    )                                    
                                    scriptFiles.Add(script.FileName);
                                else if(!showsInHead && script.FileName.Equals("GATagBottom.html",StringComparison.CurrentCultureIgnoreCase))
                                {
                                    scriptFiles.Add(script.FileName);
                                }
                            }
                            else                                                                                        
                                scriptFiles.Add(script.FileName);

                        //Wild card maps e.g. *LeadConfirmation* => this can occur anywhere in the URL
                        scriptFiles.AddRange(from func in partner.Functions
                                             where hostUrl.ToLower().Contains(func.TrimStart('*').TrimEnd('*')) && func.StartsWith("*") && func.EndsWith("*")
                                             select script.FileName);
                    }
                }
            }

            return scriptFiles;
        }
     

        public static string GetConversionTrackerUrl(IDetailViewModel viewModel, string requestUniqueKey, string communityIds)
        {
            var @params = new List<RouteParam>
            {
                new RouteParam(RouteParams.RequestUniqueKey, requestUniqueKey),
                new RouteParam(RouteParams.LeadType, viewModel.IsPageCommDetail ? LeadType.Community : LeadType.Home),
                new RouteParam(RouteParams.AlertSelected, viewModel.SendAlerts.ToType<string>()),
                new RouteParam(RouteParams.PostalCode, viewModel.UserPostalCode),
                new RouteParam(RouteParams.CommunityListGoogle, communityIds),
                new RouteParam(RouteParams.IsModal, "false")
            };
            //@params.Add(new RouteParam(UrlConst.UserNameAndLastName, viewModel.Name, RouteParamType.Friendly));
            //@params.Add(new RouteParam(UrlConst.Email, viewModel.MailAddress, RouteParamType.Friendly));

            if (viewModel.CommunityId > 0)
                @params.Add(new RouteParam(RouteParams.CommunityId, viewModel.CommunityId.ToString()));
            if (viewModel.BuilderId > 0)
                @params.Add(new RouteParam(RouteParams.BuilderId, viewModel.BuilderId.ToString()));
            if (viewModel.PlanId > 0)
                @params.Add(new RouteParam(RouteParams.PlanId, viewModel.PlanId.ToString()));
            if (viewModel.SpecId > 0)
                @params.Add(new RouteParam(RouteParams.SpecId, viewModel.SpecId.ToString()));

            // Add the from param only if the CTAName Cookie were set and it is not a on page load
            if (HttpContext.Current.Request.Cookies["CTAName"] != null && HttpContext.Current.Request.HttpMethod == "POST")
            {
                var ctaName = HttpContext.Current.Request.Cookies["CTAName"].Value.Replace(" ", "_");
                @params.Add(new RouteParam(RouteParams.FromPage, ctaName));
            }
            
            return @params.ToQueryString("ConversionTracker");
        }

        public static string GetConversionTrackerUrlForModals(BaseViewModel viewModel, string alertSelected)
        {
            var @params = new List<RouteParam>
                {
                    new RouteParam(RouteParams.RequestUniqueKey, RouteParams.RequestUniqueKey.Value<string>()),
                    new RouteParam(RouteParams.IsModal, "true")
                    //new RouteParam(UrlConst.Email, RouteParams.Email.Value<string>(), RouteParamType.Friendly)
                };

            // Add the from param only if the CTAName Cookie were set and it is not a on page load
            if (HttpContext.Current.Request.Cookies["CTAName"] != null && HttpContext.Current.Request.HttpMethod == "POST")
            {
                var ctaName = HttpContext.Current.Request.Cookies["CTAName"].Value.Replace(" ", "_");
                @params.Add(new RouteParam(RouteParams.FromPage, ctaName));
            }

            if (!string.IsNullOrEmpty(RouteParams.CommunityId.Value<string>()))
                @params.Add(new RouteParam(RouteParams.CommunityId, RouteParams.CommunityId.Value<string>()));

            if (!string.IsNullOrEmpty(RouteParams.Builder.Value<string>()))
                @params.Add(new RouteParam(RouteParams.BuilderId, RouteParams.Builder.Value<string>()));
           
            if (!string.IsNullOrEmpty(RouteParams.PlanList.Value<string>()))
                @params.Add(new RouteParam(RouteParams.PlanId, RouteParams.PlanList.Value<string>()));
            
            if (!string.IsNullOrEmpty(RouteParams.SpecList.Value<string>()))
                @params.Add(new RouteParam(RouteParams.SpecId, RouteParams.SpecList.Value<string>()));

            if (!string.IsNullOrEmpty(alertSelected))
                @params.Add(new RouteParam(RouteParams.AlertSelected, alertSelected));

            if (!string.IsNullOrEmpty(RouteParams.ApptModal.Value<string>()))
                @params.Add(new RouteParam(RouteParams.ApptModal, RouteParams.ApptModal.Value<string>()));

            @params.Add(new RouteParam(RouteParams.GoogleAnalytics, "true"));
            return @params.ToQueryString("ConversionTracker");
        }

        public static string GetConversionTrackerUrlSendCustomBrochure(IDetailViewModel viewModel, LeadInfo leadInfo)
        {
            var @params = new List<RouteParam>
                {
                    new RouteParam(RouteParams.RequestUniqueKey, leadInfo.RequestUniqueKey),
                    new RouteParam(RouteParams.IsModal, "false"),
                    new RouteParam(RouteParams.FromPage, Pages.SendCustomBrochure)
                };

            if (viewModel.CommunityId > 0)
                @params.Add(new RouteParam(RouteParams.CommunityId, viewModel.CommunityId.ToType<string>()));

            if (viewModel.BuilderId > 0)
                @params.Add(new RouteParam(RouteParams.BuilderId, viewModel.BuilderId.ToType<string>()));

            if (viewModel.PlanId > 0)
                @params.Add(new RouteParam(RouteParams.PlanId, viewModel.PlanId.ToType<string>()));

            if (viewModel.SpecId > 0)
                @params.Add(new RouteParam(RouteParams.SpecId, viewModel.SpecId.ToType<string>()));

            return @params.ToQueryString("ConversionTracker");
        }
       
        public static string GetConversionTrackerDirectSuggested(int total, int select)
        {
            var @params = new List<RouteParam>();
            var ur= @params.ToQueryString("conversiontrackerdirectsuggested");
            ur += "?total=" + total + "&select=" + select;

            // Add the from param only if the CTAName Cookie were set and it is not a on page load
            if (HttpContext.Current.Request.Cookies["CTAName"] != null && HttpContext.Current.Request.HttpMethod == "POST")
            {
                var ctaName = HttpContext.Current.Request.Cookies["CTAName"].Value.Replace(" ", "_");
                ur += string.Format("&{0}={1}", UrlConst.FromPage, ctaName);
            }
            
            return ur;
        }

        public static string GetConversionTrackerOnPageRecoCommunity(int total, int select)
        {
            var @params = new List<RouteParam>();
            var ur = @params.ToQueryString("conversiontrackeronpagerecommendentcommunity");
            ur += "?csource=matchemail&total=" + total + "&select=" + select;

            // Add the from param only if the CTAName Cookie were set and it is not a on page load
            if (HttpContext.Current.Request.Cookies["CTAName"] != null && HttpContext.Current.Request.HttpMethod == "POST")
            {
                var ctaName = HttpContext.Current.Request.Cookies["CTAName"].Value.Replace(" ", "_");
                ur += string.Format("&{0}={1}", UrlConst.FromPage, ctaName);
            }

            return ur;
        }


        public static string GetConversionTrackerUrlForModals(int totalRecoComms)
        {
            var @params = new List<RouteParam>();
            var ur = @params.ToQueryString("conversiontracker");
            ur += "?totalRecoComms=" + totalRecoComms ;

            // Add the from param only if the CTAName Cookie were set and it is not a on page load
            if (HttpContext.Current.Request.Cookies["CTAName"] != null && HttpContext.Current.Request.HttpMethod == "POST")
            {
                var ctaName = HttpContext.Current.Request.Cookies["CTAName"].Value.Replace(" ", "_");
                ur += string.Format("&{0}={1}", UrlConst.FromPage, ctaName);
            } 
            return ur;
        }

        public static string GetConversionTrackerUrlForModalsMobile(int totalRecoComms, string fromPage)
        {
            var @params = new List<RouteParam>();
            var ur = @params.ToQueryString("conversiontracker/mobile");
            ur += "?totalRecoComms=" + totalRecoComms + "&frompage=" + fromPage;
            return ur;
        }

        public static string GetConversionReturningUsers(int total, int select, int referenceCommunityWasSent)
        {
            var @params = new List<RouteParam>();
            var ur = @params.ToQueryString("conversiontracker/returningusers");
            ur += "?shown=" + total + "&select=" + select + "&referencecomm=" + referenceCommunityWasSent;
            return ur;
        }

        public static string GetConversionTrackerReturnedUsers(int total, int select)
        {
            var @params = new List<RouteParam>();
            var ur = @params.ToQueryString("conversiontracker/SendBrosureReturnedUser");
            ur += "?total=" + total + "&select=" + select;
            return ur;
        }


        public static string GetConversionTrackerForThanksBrochure(int total, int select)
        {
            var @params = new List<RouteParam>();
            var ur = @params.ToQueryString("thankyoutracker");
            ur += "?total=" + total + "&select=" + select;

            // Add the from param only if the CTAName Cookie were set and it is not a on page load
            if (HttpContext.Current.Request.Cookies["CTAName"] != null && HttpContext.Current.Request.HttpMethod == "POST")
            {
                var ctaName = HttpContext.Current.Request.Cookies["CTAName"].Value.Replace(" ", "_");
                ur += string.Format("&{0}={1}", UrlConst.FromPage, ctaName);
            } 
            return ur;
        } 

        public static string GetConversionTrackerForThanksBrochure(BaseViewModel viewModel, string specId, string planId, string communityId, string communityIds, string builderIds, string specList, string planList, string requestUniqueKey, int count)
        {
            var @params = new List<RouteParam>
            {
                new RouteParam(RouteParams.FromPage, Pages.LeadsRequestBrochureThanksModalConversion)
            };

            if (!string.IsNullOrEmpty(communityId))
                @params.Add(new RouteParam(RouteParams.CommunityId, communityId));
            if (!string.IsNullOrEmpty(specId))
                @params.Add(new RouteParam(RouteParams.SpecId, specId));
            if (!string.IsNullOrEmpty(planId))
                @params.Add(new RouteParam(RouteParams.PlanId, planId));

            @params.Add(new RouteParam(RouteParams.CommunityList, communityIds));
            @params.Add(new RouteParam(RouteParams.BuilderList, builderIds));
            @params.Add(new RouteParam(RouteParams.SpecList, specList));
            @params.Add(new RouteParam(RouteParams.PlanList, planList));
            @params.Add(new RouteParam(RouteParams.RequestUniqueKey, requestUniqueKey));
            @params.Add(new RouteParam(RouteParams.Count, count.ToString()));
            @params.Add(new RouteParam(RouteParams.IsModal, "true"));

            // Add the from param only if the CTAName Cookie were set and it is not a on page load
            if (HttpContext.Current.Request.Cookies["CTAName"] != null && HttpContext.Current.Request.HttpMethod == "POST")
            {
                var ctaName = HttpContext.Current.Request.Cookies["CTAName"].Value.Replace(" ", "_");
                @params.Add(new RouteParam(RouteParams.FromPage, ctaName));
            } 
            
            return @params.ToQueryString("thankyoutracker");
        }
    }
}
