﻿using System;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Constants.Route;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers.Tracking;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class LiveChatHelper
    {
        private const int BizHourStart = 9;
        private const int BizHourEndWeekday = 21;
        private const int BizHourEndWeekend = 18;
        private const string UrlLocationScript = "jQuery.GetLocationHrefWithUtmzAndRefer('{0}')";

        /// <summary>
        /// This return the Move button data, NHS do not have this one, only works for MOVE
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="headContent"></param>
        /// <returns></returns>
        public static MvcHtmlString LiveChatScript(this HtmlHelper htmlHelper, string headContent)
        {
            if (ValidBizHour() == false) return MvcHtmlString.Empty;


            var placementid = Configuration.ChatPlacementMoveButton;
            
            int providerId = GetProviderId(); 

            return
                MvcHtmlString.Create(!string.IsNullOrEmpty(headContent)
                                         ? string.Format(
                                             @"<div id=""nhs_LiveChat"">
                                               <a onclick=""javascript:jQuery.googlepush('Chat Links','Navigation','Live Chat', 1); window.open('http://bdx.contactatonce.com/caoclientcontainer.aspx?ProviderId={0}&MerchantId={1}&PlacementId={2}&JumpUrl='+{3},'','resizable=yes,toolbar=no,menubar=no,location=no,scrollbars=no,status=no,height=400,width=600');return false;""
                                                   href=""#""><img src=""http://BDX.contactatonce.com/getagentstatusimage.aspx?ProviderId={0}&MerchantId={1}&PlacementId={2}"" border=""0"" alt=""Click to Chat"" onerror=""this.height=0;this.width=0;""/>                                             
                                                </a>
                                               </div>",
                                             providerId,NhsRoute.BrandPartnerId, placementid,GetOriginationLocationScript())
                                         : string.Empty); // */
        }

        public static MvcHtmlString LiveChatLink(string linkText, string googlePush,ChatPlacement placement)
        {
            if (ValidBizHour() == false) return MvcHtmlString.Create("");

            var merchantId = NhsRoute.BrandPartnerId;
            int providerId = GetProviderId(); 

            //<img src=""http://BDX.contactatonce.com/getagentstatusimage.aspx?ProviderId={0}&MerchantId={1}&PlacementId={2}"" border=""0"" onerror=""this.height=0;this.width=0;""/>
            var link = string.Format(@"<a onclick=""javascript:{4}; window.open('http://bdx.contactatonce.com/caoclientcontainer.aspx?ProviderId={0}&MerchantId={1}&PlacementId={2}&JumpUrl='+{5},'','resizable=yes,toolbar=no,menubar=no,location=no,scrollbars=no,status=no,height=400,width=600');return false;""
                                href=""#"">{3}
                            </a>", providerId, merchantId, placement,linkText, googlePush, GetOriginationLocationScript());

            return MvcHtmlString.Create(link);
        }

        public static MvcHtmlString GetLiveChatTab(ChatPlacement placement, BaseViewModel viewModel)
        {
            if (!viewModel.Globals.PartnerLayoutConfig.AmIOnHomeDetailPage &&
                !viewModel.Globals.PartnerLayoutConfig.AmIOnCommunityDetailPage &&
                !viewModel.Globals.PartnerLayoutConfig.AmIOnCommunityResultsPage) return MvcHtmlString.Empty;

            var merchantId = NhsRoute.BrandPartnerId;
            int providerId = GetProviderId();
            //merchant, provider, placement
            const string html =
                @"<div id=""floating_chat_button"" style=""height: auto; position:fixed; bottom:275px; right:0px; z-index: 250"">
	                       <a onclick=""javascript:jQuery.googlepush('Chat Links','Tab','Chat Now', 1); window.open('http://dm5.contactatonce.com/CaoClientContainer.aspx?MerchantId={0}&amp;Providerid={1}&amp;PlacementId={2}&amp;JumpUrl='+{3},'','resizable=yes,toolbar=no,menubar=no,location=no,scrollbars=no,status=no,height=400,width=600');return false;"" href=""#"">
                             <img onerror=""this.height=0;this.width=0"" src=""http://dm5.contactatonce.com/getagentstatusimage.aspx?MerchantId={0}&amp;ProviderId={1}&amp;PlacementId={2}"" border=""0"" />
                           </a>
                         </div>";
            return string.Format(html, merchantId, providerId, placement, GetOriginationLocationScript()).ToMvcHtmlString();
        }

        public static string GetLiveChatScript(BaseViewModel viewModel, IMarketService marketService, IPathMapper pathMapper)
        {
            if (ValidBizHour() == false) return string.Empty;

            if (!viewModel.Globals.PartnerLayoutConfig.AmIOnHomeDetailPage &&
                !viewModel.Globals.PartnerLayoutConfig.AmIOnCommunityDetailPage &&
                !viewModel.Globals.PartnerLayoutConfig.AmIOnCommunityResultsPage) return string.Empty;

            var partnerId = NhsRoute.PartnerId;
            var brandPartnerId = NhsRoute.BrandPartnerId;
            var marketId = 0;

            var stateName = RouteParams.StateName.Value();
            var marketName = RouteParams.Area.Value();
            if (stateName.Length > 0 && marketName.Length > 0)//For DFU markets
            {
                if (marketService != null)
                {
                    var market = marketService.GetMarket(partnerId, stateName, marketName, false);
                    if (market != null)
                        marketId = market.MarketId;
                }
            }
            else
            {
                marketId = RouteParams.Market.Value().ToType<Int32>() > 0 ? RouteParams.Market.Value().ToType<Int32>() : viewModel.Globals.SdcMarketId;
            }

            if (marketId < 1)
                marketId = UserSession.PersonalCookie.MarketId;

            if (marketId > 0)
            {
                var brandPartnerConfig = LiveChatConfigReader.GetBrandPartnerConfig(brandPartnerId, pathMapper);
                var addLiveChatScript = false;

                if (brandPartnerConfig != null)
                {
                    //Normal partner - if show on all partners = true
                    if (brandPartnerId != partnerId && brandPartnerConfig.AllPartners)
                        addLiveChatScript = true;

                    //If current partner is a brand partner (NHS/Move)
                    if (brandPartnerId == partnerId)
                        addLiveChatScript = true;

                    //If live chat for current market is NOT enabled - do not apply
                    if (brandPartnerConfig.Markets.Find(m => m.MarketId == marketId) == null)
                        addLiveChatScript = false;
                }
                if (addLiveChatScript)
                {                   
                    int providerId = GetProviderId(); 

                    var scripts = string.Format("<script src=\"http://bdx.contactatonce.com/scripts/PopIn.js\" type=\"text/javascript\"></script>" +
                           "<script id=\"nhs_chatDataSrc\" data-src=\"http://bdx.contactatonce.com/PopInGenerator.aspx?MerchantId={0}&ProviderId={1}&PlacementId={2}&JumpUrl=\" type=\"text/javascript\"></script>",
                           brandPartnerId, providerId,ChatPlacement.MoveNhsDropInWindow);

                    return scripts + GetLiveChatTab(ChatPlacement.NhsChatTab, viewModel);
                }
            }
            return string.Empty;
        }

        private static string GetOriginationLocationScript()
        {            
            return string.Format(UrlLocationScript, GetRefer());
        }

        private static string GetRefer()
        {
            return UserSession.Refer;
        }

        private static int GetProviderId()
        {
            /*
             ProviderID:
                13392 for NHS
                13684 for Move
             */
            return 13684;
        }

        private static bool ValidBizHour()
        {
           // return true;
            TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
            DateTime now = TimeZoneInfo.ConvertTime(DateTime.Now, timeZone);
            bool flagInterval = SelectWeekInterval(now.DayOfWeek);

            var leftRange = new DateTime(now.Year, now.Month, now.Day, BizHourStart, 0, 0);
            var rightRange = flagInterval ?
                                new DateTime(now.Year, now.Month, now.Day, BizHourEndWeekday, 0, 0)
                              : new DateTime(now.Year, now.Month, now.Day, BizHourEndWeekend, 0, 0);

            return leftRange.Hour <= now.Hour && now.Hour <= rightRange.Hour;
        }

        private static bool SelectWeekInterval(DayOfWeek today)
        {
            return today >= DayOfWeek.Monday && today <= DayOfWeek.Friday;
        }

    }
}
