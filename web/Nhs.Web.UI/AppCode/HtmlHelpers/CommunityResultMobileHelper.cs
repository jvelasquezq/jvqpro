using System.Collections.Generic;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Library.Web.ImageResizer;
using Nhs.Mvc.Domain.Model.Web.Partials;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResultMobile;
using Nhs.Mvc.Routing.Configuration;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class CommunityResultMobileHelper
    {
        public static string GetCommunityItemMobileImage(this CommunityItemMobileViewModel model)
        {
            if (!model.Thumb1.IsValidImage())
                return  Resources.GlobalResources14.Default.images.no_photo.no_photos_180x120_png;

            var imagePath =
                ImageResizerUrlHelpers.BuildIrsImageUrl(ImageResizerUrlHelpers.FormatImageUrlForIrs(model.Thumb1), ImageSizes.HomeThumb);
            return imagePath;
        }

        public static string GetBathsBeds(this CommunityItemMobileViewModel model)
        {
            return GetBathsBeds(model.BrLo, model.BrHi, model.BaLo, model.BaHi);
        }

        public static string GetBathsBeds(this CommunityMapCard model)
        {
            return GetBathsBeds(model.MinBedroom, model.MaxBedroom, model.MinBath, model.MaxBath);
        }

        private static string GetBathsBeds(int brLo, int brHi, int baLo, int baHi)
        {
            var beds = "";
            var baths = "";

            if (brLo > 0 || brHi > 0)
            {
                if (brLo > 1)
                    beds = brLo.ToType<string>();

                if (brHi > 1)
                {
                    if (string.IsNullOrEmpty(beds))
                        beds = brHi.ToType<string>();
                    else if (brLo != brHi)
                        beds += "-" + brHi.ToType<string>();
                }
            }

            if (baLo > 0 || baHi > 0)
            {
                if (baLo > 1)
                    baths = baLo.ToType<string>();

                if (baHi > 1)
                {
                    if (string.IsNullOrEmpty(baths))
                        baths = baHi.ToType<string>();
                    else if (baLo != baHi)
                        baths += "-" + baHi.ToType<string>();
                }
            }

            if (string.IsNullOrEmpty(beds) && string.IsNullOrEmpty(baths))
                return "";

            var info = "";

            if (!string.IsNullOrEmpty(beds))
                info = beds + " " + LanguageHelper.Bedrooms;

            if (string.IsNullOrEmpty(baths))
                return info;

            if (!string.IsNullOrEmpty(info))
                info += " | " + baths + " " + LanguageHelper.Bathrooms;
            else
                info = baths + " " + LanguageHelper.Bathrooms;

            return info;
        }

        public static string GetPriceAndHomes(this BoylResult result)
        {
            return GetPriceAndHomes(result.MatchingHomes, result.PriceHigh.ToType<int>(), result.PriceLow.ToType<int>());
        }

        public static string GetPriceAndHomes(this CommunityItemMobileViewModel model)
        {
            return GetPriceAndHomes(model.NumHomes, model.PrHi.ToType<int>(), model.PrLo.ToType<int>());
        }

        public static string GetPriceAndHomes(this CommunityMapCard model)
        {
            return GetPriceAndHomes(model.NunHom, model.PrHi.ToType<int>(), model.PrLo.ToType<int>());
        }

        private static string GetPriceAndHomes(int numHomes, int prHi, int prLo)
        {
            var info = "";
            if (numHomes > 0)
                info = numHomes + " " + LanguageHelper.Home + (numHomes > 1 ? "s" : "");

            var price = "";

            if (prLo > 0)
                price = LanguageHelper.From+" " + prLo.ToString("C0");
            else if (prHi > 0)
                price = LanguageHelper.From + " " + prHi.ToString("C0");

            if (!string.IsNullOrEmpty(price))
                info += " " + price;
            return info;
        }
        
        public static string GetCommunityDetailUrl(this CommunityItemMobileViewModel result)
        {
            var paramz = result.ToCommunityDetail();
            return paramz.ToUrl(RedirectionHelper.GetCommunityDetailPage());
        }

        public static MvcHtmlString GetLocation(this CommunityMobileViewModel model)
        {
            var text = model.Market.MarketName + ", " + model.Market.State.StateAbbr + " Area";

            if (model.SearchParams.IsMultiLocationSearch)
            {
                text = model.SearchParams.SyntheticInfo.Name;
            }
            else if (!model.IsCurrentLocation)
            {
                if (model.SearchParams.WebApiSearchType == WebApiSearchType.Radius)
                {
                    if (!string.IsNullOrEmpty(model.SearchParams.City))
                        text = model.SearchParams.City + ", " + model.State;

                    if (!string.IsNullOrEmpty(model.SearchParams.County))
                        text = model.County + " " + LanguageHelper.County+", " + model.State;

                    if (!string.IsNullOrEmpty(model.SearchParams.PostalCode))
                        text = model.SearchParams.PostalCode + ", " + model.State;
                }
            }

            return text.ToMvcHtmlString();
        }

        public static MvcHtmlString QuickView(this HtmlHelper helper, CommunityMapCard model)
        {
            return helper.QuickView(model.Id.ToType<string>(), model.MId.ToType<string>(), model.PhoneNumber);
        }

        public static MvcHtmlString QuickView(this HtmlHelper helper, CommunityItemMobileViewModel model)
        {
            return helper.QuickView(model.Id.ToType<string>(), model.MarketId.ToType<string>(), model.Phone);
        }

        public static MvcHtmlString QuickView(this HtmlHelper helper, BoylResult model)
        {
            return helper.QuickView(model.CommunityId.ToType<string>(), model.MarketId.ToType<string>(), model.Phone, true);
        }


        public static MvcHtmlString QuickView(this HtmlHelper helper, string communityId, string marketId, string phoneNumber, bool isBoylResult = false)
        {
            var urlParams = new List<RouteParam>
            {
                new RouteParam
                {
                    Name = RouteParams.CommunityId,
                    Value = communityId,
                    ParamType = RouteParamType.QueryString,
                    LowerCaseParamName = true
                },

                new RouteParam
                {
                    Name = RouteParams.MarketId,
                    Value = marketId,
                    ParamType = RouteParamType.QueryString,
                    LowerCaseParamName = true
                },

                new RouteParam
                {
                    Name = RouteParams.PhoneNumber,
                    Value = phoneNumber,
                    ParamType = RouteParamType.QueryString,
                    LowerCaseParamName = true
                }
                ,new RouteParam
                {
                    Name = RouteParams.IsBoylResult,
                    Value = isBoylResult.ToType<string>(),
                    ParamType = RouteParamType.QueryString,
                    LowerCaseParamName = true
                }
            };
            return helper.NhsLink(LanguageHelper.QuickView, Pages.QuickView, urlParams,
                new {@class = "btn nhs_QuickBtn nhs_ModalPopUp"});
        }
    }
}
