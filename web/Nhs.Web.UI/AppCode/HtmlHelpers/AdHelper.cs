﻿/*
 * NewHomeSource - http://www.NewHomeSource.com
 * Copyright © 2001-2010 Builders Digital Experience, LLC.
 * ========================================================
 * Author: Luis Ruiz
 * Date: 11/22/2010
 * Description:  HtmlHelper to render the html code to show ads
 * Edit History (please mention edits succinctly):
 * =========================================================
 */

using System;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Extensions;
using Nhs.Library.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class AdHelper
    {
        public static MvcHtmlString AdControl(this HtmlHelper helper, string currentAdPosition, bool useIFrame, bool javaScriptParams, AdController adController, BaseViewModel model = null)
        {
            var sb = GetAdControl(currentAdPosition, useIFrame, javaScriptParams, adController, model);
            return MvcHtmlString.Create(sb.ToString());
        }

        public static StringBuilder GetAdControl(string currentAdPosition, bool useIFrame, bool javaScriptParams,
                                                 AdController adController, BaseViewModel model = null, int countRef = 1, bool getCommResultsSkin = false)
        {
            if (javaScriptParams == false)
            {
                if (!adController.AllPositions.Any())
                    adController.AllPositions = NhsRoute.CurrentRoute.AdPositions;
            }

            var sb = new StringBuilder();

            var areAdsEnabled = AreAdsEnabled();

            if (areAdsEnabled)
            {
                if (getCommResultsSkin == false)
                {
                    if (!javaScriptParams)
                    {
                        sb.Append("<div id=\"nhsAdContainer" + currentAdPosition + "_" + countRef 
                            + "\" class=\"nhsAdContainer" + currentAdPosition +"\">");
                        sb.Append(useIFrame
                                      ? adController.RenderIFrame(currentAdPosition, countRef)
                                      : adController.Render(currentAdPosition, countRef));
                        sb.Append("</div>");

                    }
                    else
                    {
                        sb.Append(adController.RenderJavaScriptParams());
                    } 
                }
                else
                {                    
                    var crunchedResourceName = string.Empty;

                    if (string.IsNullOrWhiteSpace(crunchedResourceName)==false)
                    {                        
                        sb.Append(ResourceCombinerEnum.GetByKey(crunchedResourceName).GetCrunchedResource());
                    }

                    sb.Append(adController.RenderAdSkin(currentAdPosition, countRef));
                }
            }
            return sb;
        }

        private static bool AreAdsEnabled()
        {
            //ads can be turned off partner site wide through config or per url using showads=false OR showads=0
            bool areAdsEnabled = Convert.ToBoolean(Configuration.AdControlEnabled);

            if (!string.IsNullOrEmpty(RouteParams.ShowAds.Value()))
                areAdsEnabled = areAdsEnabled & Convert.ToBoolean(RouteParams.ShowAds.Value());
            return areAdsEnabled;
        }
    }
}

