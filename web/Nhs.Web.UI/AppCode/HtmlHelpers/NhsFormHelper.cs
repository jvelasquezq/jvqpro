﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class NhsFormHelper
    {
        public static MvcForm BeginNhsForm(this AjaxHelper helper, AjaxOptions ajaxOptions)
        {
            return helper.BeginNhsForm("action", ajaxOptions);
        }

        /// <summary>
        /// Ajax Form - NOTE: This will discard any params from the current URL. If you would like to persist current params, use another overload.
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="actionName">Name of the action.</param>
        /// <param name="ajaxOptions">The ajax options.</param>
        /// <returns></returns>
        public static MvcForm BeginNhsForm(this AjaxHelper helper, string actionName, AjaxOptions ajaxOptions)
        {
            return helper.BeginNhsForm(actionName, "controller", ajaxOptions, false);
        }

        public static MvcForm BeginNhsForm(this AjaxHelper helper, string actionName, AjaxOptions ajaxOptions, bool useCurrentParams)
        {
            return helper.BeginNhsForm(actionName, "controller", ajaxOptions, useCurrentParams);
        }

        public static MvcForm BeginNhsForm(this AjaxHelper helper, string actionName, string controllerName, AjaxOptions ajaxOptions, bool useCurrentParams, bool useExplicitNames=false)
        {
            var routeParams = new List<RouteParam>();

            if (useCurrentParams) //use current route params
                routeParams = NhsRoute.CurrentRoute.Params as List<RouteParam>;

            string actionUrl = GetActionUrl(helper.ViewContext, controllerName, actionName, routeParams, useExplicitNames);
            return helper.BeginForm(actionName, controllerName, helper.ViewContext.RouteData, ajaxOptions,

                                    new
                                        {
                                            @action = actionUrl,
                                            @enctype = "multipart/form-data"
                                        }
                                    );

        }

        public static MvcForm BeginNhsForm(this AjaxHelper helper, string actionName, string controllerName, AjaxOptions ajaxOptions, 
            string formId, string cssClass,bool useCurrentParams, bool useExplicitNames = false)
        {
            var routeParams = new List<RouteParam>();

            if (useCurrentParams) //use current route params
                routeParams = NhsRoute.CurrentRoute.Params as List<RouteParam>;

            string actionUrl = GetActionUrl(helper.ViewContext, controllerName, actionName, routeParams, useExplicitNames);
            return helper.BeginForm(actionName, controllerName, helper.ViewContext.RouteData, ajaxOptions,

                                    new
                                    {
                                        action = actionUrl,
                                        enctype = "multipart/form-data",
                                        id = formId,
                                        @class = cssClass
                                    }
                                    );
        }

        //TODO: Clean these overloads
        public static MvcForm BeginNhsForm(this HtmlHelper helper)
        {
            // ReSharper disable Mvc.ActionNotResolved
            return helper.BeginForm();
            // ReSharper restore Mvc.ActionNotResolved
        }

        public static MvcForm BeginNhsForm(this HtmlHelper helper, string actionName)
        {
            return BeginNhsForm(helper, actionName, FormMethod.Post, string.Empty);
        }

        public static MvcForm BeginNhsForm(this HtmlHelper helper, string actionName, string formName)
        {
            return BeginNhsForm(helper, actionName, FormMethod.Post, formName);
        }

        public static MvcForm BeginNhsForm(this HtmlHelper helper, string actionName, FormMethod formMethod, string formName)
        {
            return BeginNhsForm(helper, actionName, "controller", formMethod, formName);
        }
        public static MvcForm BeginNhsForm(this HtmlHelper helper, string actionName, string controllerName, bool useExplicitNames)
        {
            return BeginNhsForm(helper, actionName, controllerName, FormMethod.Post, "", false, useExplicitNames, null);
        }

        public static MvcForm BeginNhsForm(this HtmlHelper helper, string actionName, string controllerName, bool useExplicitNames, Dictionary<string, object> htmlAttributes)
        {
            return BeginNhsForm(helper, actionName, controllerName, FormMethod.Post, "",false, useExplicitNames, htmlAttributes);
        }

        public static MvcForm BeginNhsForm(this HtmlHelper helper, string actionName, string controllerName, bool useExplicitNames, string formName ="", Dictionary<string, object> htmlAttributes = null)
        {
            return BeginNhsForm(helper, actionName, controllerName, FormMethod.Post, formName, false, useExplicitNames, htmlAttributes);
        }

        public static MvcForm BeginNhsForm(this HtmlHelper helper, string actionName, string controllerName, FormMethod formMethod, string formName)
        {
            return BeginNhsForm(helper, actionName, controllerName, formMethod, formName, true);
        }

        public static MvcForm BeginNhsForm(this HtmlHelper helper, string actionName, string controllerName, FormMethod formMethod, string formName, bool useCurrentParams, bool useExplicitNames, Dictionary<string, object> htmlAttributes = null)
        {
            var routeParams = new List<RouteParam>();

            if (useCurrentParams) //use current route params
                routeParams = NhsRoute.CurrentRoute.Params as List<RouteParam>;
            var actionUrl = GetActionUrl(helper.ViewContext, controllerName, actionName, routeParams, useExplicitNames);

            var formhtmlAttributes = new Dictionary<string, object>
                {
                    {"action", actionUrl},
                    {"id", formName},
                    {"name", formName},
                    {"enctype", "multipart/form-data"}
                }.MergeDictionaries(htmlAttributes);

            return helper.BeginForm("action", controllerName, FormMethod.Post, formhtmlAttributes);
        }

        public static MvcForm BeginNhsForm(this HtmlHelper helper, string actionName, string controllerName, FormMethod formMethod, string formName, bool useCurrentParams)
        {
            var routeParams = new List<RouteParam>();

            if (useCurrentParams) //use current route params
                routeParams = NhsRoute.CurrentRoute.Params as List<RouteParam>;

            string actionUrl = GetActionUrl(helper.ViewContext, controllerName, actionName, routeParams);        

            return helper.BeginForm(
                "action",
                controllerName,
                FormMethod.Post,
                new
                {
                    @action = actionUrl,
                    @id = formName,
                    @name = formName
                }
                );
        }

        public static MvcForm BeginNhsForm(this HtmlHelper helper, string actionName, string controllerName, FormMethod formMethod, string formName, bool useCurrentParams, bool useExplicitNames)
        {
            var routeParams = new List<RouteParam>();

            if (useCurrentParams) //use current route params
                routeParams = NhsRoute.CurrentRoute.Params as List<RouteParam>;

            string actionUrl = GetActionUrl(helper.ViewContext, controllerName, actionName, routeParams, useExplicitNames);

            if (string.IsNullOrWhiteSpace(actionUrl))
            {
                actionUrl = "/";
            }

            return helper.BeginForm(
                "action",
                controllerName,
                FormMethod.Post,
                new
                {
                    @action = actionUrl,
                    @id = formName,
                    @name = formName
                }
                );
        }

        public static MvcForm BeginNhsForm(this HtmlHelper helper, string actionName, string controllerName, FormMethod formMethod, string formName, bool useCurrentParams, string cssClass = "")
        {
            var routeParams = new List<RouteParam>();

            if (useCurrentParams) //use current route params
                routeParams = NhsRoute.CurrentRoute.Params as List<RouteParam>;

            string actionUrl = GetActionUrl(helper.ViewContext, controllerName, actionName, routeParams);
            return helper.BeginForm(
                "action",
                controllerName,
                FormMethod.Post,
                new
                {
                    @action = actionUrl,
                    @id = formName,
                    @name = formName,
                    @class = cssClass
                }
                );
        }

        public static string GetActionUrl(this HtmlHelper helper, string controllerName, string actionName, List<RouteParam> paramz=null, bool useExplicitNames = true)
        {
            if (paramz == null)
                paramz = new List<RouteParam>();
            return GetActionUrl(helper.ViewContext, controllerName, actionName, paramz, useExplicitNames);
        }

        public static string GetActionUrl(this ViewContext context, List<RouteParam> paramz, string controllerName, string actionName,
             bool useExplicitNames = false)
        {
            if (paramz == null)
                paramz = new List<RouteParam>();
            return GetActionUrl(context, controllerName, actionName, paramz, useExplicitNames);
        }

        private static string GetActionUrl(ViewContext context, string controllerName, string actionName, List<RouteParam> paramz, bool useExplicitNames = false)
        {
            string routeControllerName;
            string routeActionName;

            if (!useExplicitNames)
            {
                routeControllerName =
                   (from routeDataValue in context.Controller.ControllerContext.RouteData.Values
                    where routeDataValue.Key == "controller"
                    select routeDataValue).FirstOrDefault().Value.ToString();

                routeActionName =
                    (from routeDataValue in context.Controller.ControllerContext.RouteData.Values
                     where routeDataValue.Key == "action"
                     select routeDataValue).FirstOrDefault().Value.ToString();
            }
            else
            {
                routeControllerName = controllerName;
                routeActionName = actionName;
            }

            //Find all routes that match up on controller              
            var matchingRoutes = (from rt in MvcRoutes.Routes
                                  where
                                      rt.Controller.Name.Equals(routeControllerName, StringComparison.InvariantCultureIgnoreCase) 
                                      && rt.Controller.Action.Equals(routeActionName, StringComparison.InvariantCultureIgnoreCase)
                                  select rt);

            if (matchingRoutes.ToList().Count == 0)
                matchingRoutes = (from rt in MvcRoutes.Routes
                                  where
                                      rt.Controller.Name.Equals(routeControllerName, StringComparison.InvariantCultureIgnoreCase)
                                  select rt);
            paramz.RemoveAll(p => p.Name == RouteParams.Action || p.ParamType == RouteParamType.QueryString || p.ParamType == RouteParamType.Hash);
            paramz.RemoveAll(p => p.Name == RouteParams.InvalidPropertySearch|| p.ParamType == RouteParamType.QueryString || p.ParamType == RouteParamType.Hash);
            
            string actionUrl = string.Empty;
            bool routeFound = false;

            foreach (var route in matchingRoutes)
            {
                actionUrl = route.RouteUrl.Replace("{*optionals}", string.Empty); //Replace any querystrings etc.

                routeFound = true;
                foreach (var param in paramz)
                {
                    string placeHolder = "{" + param.PlaceHolder + "}";
                    string nameValue = string.Format("{0}-{{{1}}}", param.ReturnRouteParamName(param.LowerCaseParamValue), param.PlaceHolder); //TODO: Need a workaround when we remove dashes from urls
                    if (actionUrl.Contains(nameValue))
                    {
                        actionUrl = actionUrl.Replace(placeHolder, param.Value);
                    }
                    else //no match on parameter, skip to next route
                    {
                        routeFound = false;
                        break;
                    }
                }
                if (routeFound)
                    break;
            }

            if (!routeFound || paramz.Count == 0) //if you still don't find a route, try using the controller and action names
            {
                var route = (from rt in MvcRoutes.Routes
                             where rt.Controller.Name.Equals(routeControllerName, StringComparison.InvariantCultureIgnoreCase) &&
                                     rt.Controller.Action.Equals(actionName, StringComparison.InvariantCultureIgnoreCase)
                             select rt).FirstOrDefault();

                if (route != null)
                    actionUrl = route.RouteUrl.Replace("{*optionals}", string.Empty); //Replace any querystrings etc.
            }

            actionUrl = "/" + actionUrl.Replace("{partnersiteurl}", NhsRoute.PartnerSiteUrl).Replace("{action}", actionName) + "/";

            if (!routeFound)
            {
                foreach (var param in paramz)
                    actionUrl += param.Name + "-" + param.Value + "/";

                actionUrl += "action-" + actionName;
            }

            return actionUrl.TrimEnd('/');
        }
    }
}
