﻿using System.Collections.Generic;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class BuilderShowCaseHelper
    {
        public static string GetBuilderShowCaseGalleryUrl(this BuilderShowCaseViewModel model)
        {
            var @paramas = new List<RouteParam>();
            @paramas.Add(new RouteParam(RouteParams.BrandId, model.BrandId, RouteParamType.QueryString));
            @paramas.Add(new RouteParam(RouteParams.IncludeVideo, "true", RouteParamType.QueryString));
            return @paramas.ToUrl(Pages.GetBuilderShowCaseGallery);
        }

        public static string BuilderShowCaseMoreLocations(this BuilderShowCaseViewModel model)
        {
            var @paramas = new List<RouteParam>();
            @paramas.Add(new RouteParam(RouteParams.BrandId, model.BrandId, RouteParamType.QueryString));
            return @paramas.ToUrl(Pages.BuilderShowCaseMoreLocations);
        }

        public static string AskQuestionToBuilder(this BuilderShowCaseViewModel model)
        {
            var @paramas = new List<RouteParam>();
            @paramas.Add(new RouteParam(RouteParams.BrandId, model.BrandId));
            return @paramas.ToUrl(Pages.BuilderShowCaseAskQuestion);
        }
    }
}