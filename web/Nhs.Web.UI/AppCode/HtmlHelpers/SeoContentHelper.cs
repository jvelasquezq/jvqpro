﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Web;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Seo;
using System.Web.Mvc;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Library.Common;
using System.Web.Caching;
using Nhs.Library.Helpers.Content;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class SeoContentHelper
    {
        /// <summary>
        /// Generic overload
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="market">The market.</param>
        /// <param name="city">The city.</param>
        /// <param name="state">The state.</param>
        /// <param name="county">The county.</param>
        /// <param name="zip">The zip.</param>
        /// <param name="communityName">Name of the community.</param>
        /// <param name="contentTags">The content tags.</param>
        /// <param name="templateType">Type of the template.</param>
        /// <param name="htmlBlockName">Name of the HTML block.</param>
        /// <param name="srpType"></param>
        /// <returns></returns>
        public static MvcHtmlString RenderSeoContent(this HtmlHelper helper, string market, string city, string state, string county, 
            string zip, string communityName, string brandName, string brandId, IList<ContentTag> contentTags, 
            SeoTemplateType templateType, string htmlBlockName, SrpTypeEnum srpType = SrpTypeEnum.CommunityResults)
        {
            var seoContentManager = new SeoContentManager();
            
            if (contentTags == null)
                contentTags = new List<ContentTag>();

            var partnerSiteUrl = NhsRoute.PartnerSiteUrl;

            if (!string.IsNullOrEmpty(partnerSiteUrl))
                partnerSiteUrl = partnerSiteUrl + "/";

            contentTags.Add(new ContentTag() { TagKey = ContentTagKey.PartnerSiteUrl, TagValue = partnerSiteUrl });
            seoContentManager.ContentTags = contentTags;
            seoContentManager.Market = market;
            seoContentManager.City = city;
            seoContentManager.State = state;
            seoContentManager.Zip = zip;
            seoContentManager.County = county;
            seoContentManager.PartnerSiteUrl = NhsRoute.PartnerSiteUrl;
            seoContentManager.CommunityName = communityName;
            seoContentManager.BrandName = brandName;            
            seoContentManager.BrandID = brandId;
            //BEGIN TICKET 78565
            //The School District ID is added only for getting the school district SEO files as they are needed in some places (as for heading1 section).
            //Oonce the community results.seo gets migrated to use the SeoContentService this won't needed, as the viewModel should contain an H1 tag to be replaced/shown 
            //directly without needing to call the file a second time (as currently is).  The same applies for ticket 78563
            var contentTag = contentTags.FirstOrDefault(ct => ct.TagKey == ContentTagKey.SchoolDistrict);
            if (contentTag != null) seoContentManager.SchoolDistrict = contentTag.TagValue;
            //END TICKET 78565

            contentTag = contentTags.FirstOrDefault(ct => ct.TagKey == ContentTagKey.SyntheticName);
            if (contentTag != null) seoContentManager.SyntheticName = contentTag.TagValue;
            
            string seoInfo = string.Empty;
            string output = string.Empty;
            try
            {
                seoInfo = string.Format("SEO Content: {0} -> {1}", templateType, htmlBlockName);
                StaticContentHelper.ProcessStaticEntry(seoInfo);
                output = seoContentManager.GetHtmlBlock(templateType, htmlBlockName, strType: srpType);
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }

            string staticContent = System.Web.HttpContext.Current.Request.QueryString["showcontentareas"];
            bool showstatic;
            if (staticContent != null)
            {
                bool.TryParse(staticContent, out showstatic);
                UserSession.ShowStaticOverlay = showstatic;
            }
            else
                showstatic = UserSession.ShowStaticOverlay;
            
            if (showstatic)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<div class=\"nhs_StaticContent\">");
                sb.Append(string.Format("<a class=\"stmark\"><b class=\"stNumber\" title=\"{0}\" ></b></a>", seoInfo));
                sb.Append(output);
                sb.Append("</div>");
                output = sb.ToString();
            }

            return MvcHtmlString.Create(output);
        }

        public static MvcHtmlString RenderAgentQuoteContent(this HtmlHelper helper, IPathMapper pathMapper)
        {
            var proContentHelper = new ProContentHelper(pathMapper);
            List<AgentQuote> agentQuotes = proContentHelper.GetAgentQuotes();
            string agentQuoteHtml = DisplayRandomAgentQuotes(agentQuotes);
            return MvcHtmlString.Create(agentQuoteHtml);
        }

        public static MvcHtmlString RenderBuyNewSectionContent(this HtmlHelper helper, SeoTemplateType seoTemplateType)
        {
            List<BuyNewContentSection> sections = ContentHelper.GetBuyNewSections(seoTemplateType);
            if (sections == null || !sections.Any()) return MvcHtmlString.Empty;
            var randomNumber = new Random();
            const string htmlString = "<figure><img src='{0}' alt='{1}'; /><figcaption>{2}</figcaption></figure>";
            var section = sections[randomNumber.Next(0, sections.Count)];

            return MvcHtmlString.Create(string.Format(htmlString,section.ImageUrl, section.ImageText, section.Description));
        }

        private static string DisplayRandomAgentQuotes(List<AgentQuote> agentQuotes)
        {
            var sb = new StringBuilder();
            var url = string.Empty;

            if (agentQuotes != null && agentQuotes.Count != 0)
            {
                var randomNumber = new Random();
                AgentQuote agentQuote = agentQuotes[randomNumber.Next(0, agentQuotes.Count)];
                if (!string.IsNullOrEmpty(agentQuote.AgentPhotoUrl))
                {
                    sb.Append("<div class='pro_AgentThumb'>");
                    sb.Append(string.Format("<img src={0} alt=''/>", agentQuote.AgentPhotoUrl));
                    sb.Append("</div>");
                }
                sb.Append("<ul>");
                sb.Append("<li>");
                sb.Append(agentQuote.QuoteText);
                sb.Append("</li>");
                sb.Append("</ul>");
                sb.Append("<p>");
                sb.Append("<strong>");
                sb.Append(agentQuote.AgentName);
                sb.Append("</strong>");
                sb.Append("<br>");
                sb.Append(agentQuote.AgentLocation);
                sb.Append("</p>");
            }

            return sb.ToString();
        }
        public static MvcHtmlString RenderZipCodeByMarketId(this HtmlHelper helper,CommunityResultsViewModel model)
        {
         
            int marketId = model.Market.MarketId;
            IList<int> col1 = model.ZipCodesCol1;
            IList<int> col2 = model.ZipCodesCol2;
            IList<int> col3 = model.ZipCodesCol3;
            var sb = new StringBuilder();
            sb.Append("<div class=\"nhs_CommResListBuilders\"><h4>Find new homes by zip code</h4>");
            sb.Append("<ul>");
            var title = "";
            foreach (int zipCode in col1)
            {
                IList<RouteParam> commResultsLink = new List<RouteParam>
                 { new RouteParam { Name = RouteParams.Market, Value = marketId.ToString(), ParamType =  RouteParamType.Friendly},
                    new RouteParam { Name = RouteParams.PostalCode, Value = zipCode.ToString(), ParamType =  RouteParamType.Friendly} };
                title =string.Format("{0} new homes for sale in {1}, {2}", zipCode, model.City, model.State);
                sb.Append(string.Format("<li><a href='{0}' title='{2}'>{1}</a></li>", commResultsLink.ToUrl(Pages.CommunityResults).ToLower(), zipCode, title));
            }
            sb.Append("</ul>");
            sb.Append("<ul>"); 
             
            foreach (int zipCode in col2)
            {
                IList<RouteParam> commResultsLink = new List<RouteParam>
                 { new RouteParam { Name = RouteParams.Market, Value = marketId.ToString(), ParamType =  RouteParamType.Friendly},
                    new RouteParam { Name = RouteParams.PostalCode, Value = zipCode.ToString(), ParamType =  RouteParamType.Friendly} };

                title = string.Format("{0} new homes for sale in {1}, {2}", zipCode, model.City, model.State);
                sb.Append(string.Format("<li><a href='{0}' title='{2}'>{1}</a></li>", commResultsLink.ToUrl(Pages.CommunityResults).ToLower(), zipCode, title));
            }
            sb.Append("</ul>");
            sb.Append("<ul>");
            foreach (int zipCode in col3)
            {
                IList<RouteParam> commResultsLink = new List<RouteParam>
                 { new RouteParam { Name = RouteParams.Market, Value = marketId.ToString(), ParamType =  RouteParamType.Friendly},
                    new RouteParam { Name = RouteParams.PostalCode, Value = zipCode.ToString(), ParamType =  RouteParamType.Friendly} };

                title = string.Format("{0} new homes for sale in {1}, {2}", zipCode, model.City, model.State);
                sb.Append(string.Format("<li><a href='{0}' title='{2}'>{1}</a></li>", commResultsLink.ToUrl(Pages.CommunityResults).ToLower(), zipCode, title));
            }
            sb.Append("</ul>");
            sb.Append("</div>");
            return MvcHtmlString.Create(sb.ToString());

        }

        public static MvcHtmlString RenderSeoCommunitiesContent(this HtmlHelper helper, List<Market> markets,
            string stateAbbr)
        {
            var seoContentManager = new SeoContentManager();
            var commName = string.Empty;
            var marketName = string.Empty;
            var title = string.Empty;
            var sb = new StringBuilder();
            int pageSize = 0;


            var marketId = NhsUrl.GetMarketID;

            var communitiesNames =
                (string[])
                    WebCacheHelper.GetObjectFromCache(WebCacheConst.WebCacheKeyPrefix + WebCacheConst.PartnerMarkets,
                        true);
            if (communitiesNames == null)
            {
                communitiesNames = seoContentManager.GetSeoPathFiles(SeoTemplateType.Nhs_CommunityName_Srp);
                WebCacheHelper.AddObjectToCache(communitiesNames,
                    WebCacheConst.WebCacheKeyPrefix + WebCacheConst.PartnerMarkets,
                    new TimeSpan(0, WebCacheConst.DefaultMins, 0), true);
            }

            var seoCommunities = new List<SEOCommunity>();

            if (marketId != 0 || !string.IsNullOrEmpty(stateAbbr))
                foreach (var comm in communitiesNames)
                {
                    var fileName = comm.Substring(comm.LastIndexOf(@"\") + 1);

                    if (fileName.IndexOf("Default") == -1)
                    {
                        marketName = string.Empty;
                        var tempstate = string.Empty;
                        try
                        {
                            commName = fileName.Substring(0, fileName.IndexOf("-"));
                            marketName = fileName.Substring(fileName.IndexOf("-") + 1,
                                (fileName.IndexOf("_") - fileName.IndexOf("-") - 4));
                            tempstate = fileName.Substring(fileName.IndexOf("_") - 2, 2);
                        }
                        catch
                        {

                            //Case 76890: Please don't uncomment following line see # Number if you need further details
                            //ErrorLogger.LogError("SEO Error", "RenderSeoCommunitiesContent are not working", ex.StackTrace);
                        }

                        if (!string.IsNullOrEmpty(marketName) || !string.IsNullOrEmpty(tempstate))
                        {
                            var market =
                                markets.Where(m => m.MarketName.ToLower() == marketName.ToLower()).FirstOrDefault();
                            if (market != null)
                                seoCommunities.Add(new SEOCommunity()
                                {
                                    Name = commName,
                                    MarketName = marketName,
                                    State = tempstate,
                                    MarketId = market.MarketId
                                });
                        }
                    }
                }

            var currMarket = markets.Where(m => m.MarketId == marketId).FirstOrDefault();

            if (currMarket != null)
            {
                title = string.Format("<h4>Find new {0} homes by subdivision</h4>", currMarket.MarketName);
                seoCommunities = seoCommunities.Where(c => c.MarketId == marketId).OrderBy(c => c.Name).ToList();

                if (seoCommunities.Count > 0)
                {
                    // Create a 3 pages list for Market Brands
                    pageSize = (seoCommunities.Count/3) + (seoCommunities.Count%3 > 0 ? 1 : 0);

                    var cols = new List<SEOCommunity>[]
                    {
                        seoCommunities.Take(pageSize).ToList(),
                        seoCommunities.Skip(pageSize).Take(pageSize).ToList(),
                        seoCommunities.Skip(pageSize*2).Take(pageSize).ToList()
                    };

                    sb.Append("<div class=\"nhs_CommResListBuilders\">" + title);

                    foreach (var commsSubSet in cols)
                    {
                        sb.Append("<ul>");
                        foreach (var comm in commsSubSet)
                        {
                            IList<RouteParam> commResultsLink = new List<RouteParam>()
                            {
                                new RouteParam()
                                {
                                    Name = RouteParams.Market,
                                    Value = comm.MarketId.ToString(),
                                    ParamType = RouteParamType.Friendly
                                },
                                new RouteParam()
                                {
                                    Name = RouteParams.CommunityName,
                                    Value = HttpUtility.UrlEncode(comm.Name),
                                    ParamType = RouteParamType.QueryString
                                }
                            };

                            sb.Append(string.Format("<li><a href='{0}'>{1}</a></li>",
                                commResultsLink.ToUrl(Pages.CommunityResults).ToLower(), comm.Name));
                        }

                        sb.Append("</ul>");
                    }

                    sb.Append("</div>");
                }
            }
            else if (!string.IsNullOrEmpty(stateAbbr))
            {
                title = "<h2>Browse new home communities by subdivision</h2>";
                seoCommunities =
                    seoCommunities.Where(c => c.State == stateAbbr)
                        .OrderBy(c => c.MarketName)
                        .ThenBy(c => c.Name)
                        .ToList();

                if (seoCommunities.Count > 0)
                {
                    var commsGroup = seoCommunities.GroupBy(c => c.MarketName).OrderBy(g => g.Key);

                    sb.Append(title);

                    foreach (var marketGroup in commsGroup)
                    {
                        //sb.Append("<div class='nhs_IndexBox'>");
                        sb.Append("<h4>" + marketGroup.Key + "</h4>");

                        // Create a 3 pages list for Market Brands
                        pageSize = (marketGroup.Count()/3) + (marketGroup.Count()%3 > 0 ? 1 : 0);

                        var cols = new List<SEOCommunity>[]
                        {
                            marketGroup.Take(pageSize).ToList(),
                            marketGroup.Skip(pageSize).Take(pageSize).ToList(),
                            marketGroup.Skip(pageSize*2).Take(pageSize).ToList()
                        };

                        foreach (var column in cols)
                        {
                            sb.Append("<ul class='nhs_IndexList'>");

                            foreach (var comm in column)
                            {

                                IList<RouteParam> commResultsLink = new List<RouteParam>()
                                {
                                    new RouteParam()
                                    {
                                        Name = RouteParams.Market,
                                        Value = comm.MarketId.ToString(),
                                        ParamType = RouteParamType.Friendly
                                    },
                                    new RouteParam()
                                    {
                                        Name = RouteParams.CommunityName,
                                        Value = HttpUtility.UrlEncode(comm.Name),
                                        ParamType = RouteParamType.QueryString
                                    }
                                };

                                sb.Append(string.Format("<li><a href='{0}'>{1}</a></li>",
                                    commResultsLink.ToUrl(Pages.CommunityResults).ToLower(), comm.Name));
                            }

                            sb.Append("</ul>");
                        }

                        //sb.Append("</div>");

                    }
                }
            }

            return MvcHtmlString.Create(sb.ToString());
        }

        /// <summary>
        /// Market only
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="market"></param>
        /// <param name="contentTags"></param>
        /// <param name="templateType"></param>
        /// <param name="htmlBlockName"></param>
        /// <param name="srpType"></param>
        /// <returns></returns>
        public static MvcHtmlString RenderSeoContent(this HtmlHelper helper, string market, IList<ContentTag> contentTags,
            SeoTemplateType templateType, string htmlBlockName, SrpTypeEnum srpType = SrpTypeEnum.CommunityResults)
        {
            return RenderSeoContent(helper, market, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, contentTags, templateType,
                                    htmlBlockName, srpType);
        }

        /// <summary>
        /// City and State only
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="contentTags"></param>
        /// <param name="templateType"></param>
        /// <param name="htmlBlockName"></param>
        /// <param name="srpType"></param>
        /// <returns></returns>
        public static MvcHtmlString RenderSeoContent(this HtmlHelper helper, string city, string state,
            IList<ContentTag> contentTags, SeoTemplateType templateType, string htmlBlockName, SrpTypeEnum srpType = SrpTypeEnum.CommunityResults)
        {
            return RenderSeoContent(helper, string.Empty, city, state, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, contentTags, templateType,
                                    htmlBlockName, srpType);
        }

        /// <summary>
        /// Just template type
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="templateType"></param>
        /// <param name="htmlBlockName"></param>
        /// <param name="srpType"></param>
        /// <returns></returns>
        public static MvcHtmlString RenderSeoContent(this HtmlHelper helper, SeoTemplateType templateType, string htmlBlockName, SrpTypeEnum srpType = SrpTypeEnum.CommunityResults)
        {
            return RenderSeoContent(helper, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, null, templateType,
                                    htmlBlockName, srpType);
        }

        /// <summary>
        /// Content Tags
        /// </summary>
        /// <param name="helper">The helper.</param>
        /// <param name="contentTags">The content tags.</param>
        /// <param name="templateType">Type of the template.</param>
        /// <param name="htmlBlockName">Name of the HTML block.</param>
        /// <param name="srpType"></param>
        /// <returns></returns>
        public static MvcHtmlString RenderSeoContent(this HtmlHelper helper, IList<ContentTag> contentTags,
            SeoTemplateType templateType, string htmlBlockName, SrpTypeEnum srpType = SrpTypeEnum.CommunityResults)
        {
            return RenderSeoContent(helper, 
                GetSeoContentTag(ContentTagKey.MarketName, contentTags), 
                GetSeoContentTag(ContentTagKey.City, contentTags), 
                GetSeoContentTag(ContentTagKey.StateID, contentTags), 
                GetSeoContentTag(ContentTagKey.CountyName, contentTags), 
                GetSeoContentTag(ContentTagKey.Zip, contentTags), 
                GetSeoContentTag(ContentTagKey.CommunityName, contentTags), 
                GetSeoContentTag(ContentTagKey.BrandName, contentTags), 
                GetSeoContentTag(ContentTagKey.BrandID, contentTags), 
                contentTags, templateType, htmlBlockName,srpType);
        }

        public static string GetSeoContentTag(ContentTagKey key, IList<ContentTag> contentTags)
        {
            var contentTag = (from ct in contentTags
                              where ct.TagKey == key
                              select ct).FirstOrDefault();

            return contentTag != null ? contentTag.TagValue : string.Empty;
        }

        public static string GetSeoTDVMetrics(this HtmlHelper helper)
        {
            // return a custom json format like a dictionary for fast access in js {'event1' : {name: 'Clickthroughs',  value: 30}, 'event2' : {name: 'Driving Directions', value: 4}}; 
            var metrics =  TdvMetricsReader.GetTdvMetrics();
            var json = "{" + string.Join(",", metrics.Select(m => string.Format("'{0}': {{name : '{1}', value : '{2}'}}", m.Event, m.Name, m.Value)).ToArray()) + "}";
            return json;
        }

    }
}
