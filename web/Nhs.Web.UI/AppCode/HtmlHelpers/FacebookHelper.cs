﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants.Enums;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Utility.Constants;
using Nhs.Web.UI.AppCode.Helper;
using Resources;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class FacebookHelper
    {
        public static List<MetaTagFacebook> GetFacebookMeta(string communityName, string brandName, string image, string planName = "")
        {
            var text = !string.IsNullOrEmpty(planName) ? String.Format("{0} {3} {1} {4} {2}", planName, communityName, brandName, LanguageHelper.At, LanguageHelper.By) : String.Format("{0} {2} {1}", communityName, brandName, LanguageHelper.By);
            var url = HttpContext.Current.Request.Url.Authority;
            image = !String.IsNullOrEmpty(image) ? Configuration.IRSDomain + image.Replace("//", "/") : GlobalResourcesMvc.Default.images.logo_nhs_png;           

            var metas = new List<MetaTagFacebook>{
                                                    new MetaTagFacebook { Name = FacebookMetaType.Type, Content = "website" },
                                                    GetFacebookMetaAppIdFacebook(),
                                                    new MetaTagFacebook { Name = FacebookMetaType.Url, Content = HttpContext.Current.Request.Url.ToString() },
                                                    new MetaTagFacebook { Name = FacebookMetaType.Image, Content = image },
                                                    new MetaTagFacebook { Name = FacebookMetaType.Title, Content = text },
                                                    new MetaTagFacebook { Name = FacebookMetaType.Description, Content = String.Format("{0} on {1}", text, url.Trim('/')) },
                                                    new MetaTagFacebook { Name = FacebookMetaType.Site_Name, Content = "NewHomeSource" }
                                                  };
            return metas;
        }

        public static MetaTagFacebook GetFacebookMetaAppIdFacebook()
        {
            return new MetaTagFacebook { Name = FacebookMetaType.App_Id, Abbr = "fb", Content = Configuration.AppIdFacebook };
        }

        public static MvcHtmlString GetFacebookWidget(this HtmlHelper htmlHelper, int width = 290, int height = 395)
        {
            var site = "pages/New-Home-Source-Professional/126778360685109";
            
            return
                MvcHtmlString.Create(
                    string.Format(
                        "<div class=\"fb-like-box\" data-href=\"https://www.facebook.com/{2}\" data-width=\"{0}\" data-height=\"{1}\" " +
                        "data-show-faces=\"false\" data-border-color=\"#CCCCCC\" data-stream=\"true\" data-header=\"true\"></div>",
                        width, height,site));
        }

        public static MvcHtmlString GetBspFacebookWidget(this HtmlHelper htmlHelper, string fbWidgetHtml)
        {
            return fbWidgetHtml.Contains("[fbsep]") ? fbWidgetHtml.Replace("[fbsep]", Environment.NewLine).ToMvcHtmlString() : MvcHtmlString.Empty;
        }


        public static MvcHtmlString FbLike(this HtmlHelper htmlHelper)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append("<fb:like ");
            stringBuilder.Append("send=\"false\" ");
            stringBuilder.Append("layout=\"button_count\" ");
            stringBuilder.Append("width=\"90\" ");
            stringBuilder.Append("show_faces=\"false\" ");
            stringBuilder.AppendFormat("href=\"{0}\">", HttpContext.Current.Request.Url);
            stringBuilder.Append("</fb:like>");
            return stringBuilder.ToMvcHtmlString();
        }
        
        public static MvcHtmlString FbLogin(this HtmlHelper htmlHelper)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append("<div ");
            stringBuilder.Append("class=\"fb-login-button fbLoginButton ft_btn\" ");
            stringBuilder.Append("data-scope=\"email\" ");
            if (NhsRoute.IsMobileDevice)
            {
                stringBuilder.Append("data-size=\"large\" ");
            }
            else {
                stringBuilder.Append("data-size=\"xlarge\" ");
            }
            stringBuilder.Append("onlogin=\"jQuery.CheckLoginState()\" ");
            stringBuilder.Append(">");
            stringBuilder.Append(LanguageHelper.SignInWithFacebook);
            stringBuilder.Append("</div>");
            stringBuilder.Append("<script type=\"text/javascript\">");
            stringBuilder.Append("jQuery.InitFacebookControls(" + Configuration.AppIdFacebook + ");");
            stringBuilder.Append("</script>");
            return stringBuilder.ToMvcHtmlString();
        }
    }
}
