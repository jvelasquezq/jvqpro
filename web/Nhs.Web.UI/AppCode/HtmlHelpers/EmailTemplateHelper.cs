﻿using System.Collections.Generic;
using System.Web.Mvc;
using Nhs.Library.Web;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class EmailTemplateHelper
    {
        private const string FolderPath = @"EmailTemplates\{0}";

        public static MvcHtmlString EmailStaticContent(this HtmlHelper helper, List<ContentTag> replacementTags, string htmlFileName)
        {
            var pahtFile = string.Format(FolderPath, htmlFileName);
            return helper.StaticContent(pahtFile, replacementTags, new ContextPathMapper(), "");
        }

        public static MvcHtmlString EmailStaticContentWithTitle(this HtmlHelper helper, List<ContentTag> replacementTags, string htmlFileName, out string title)
        {
            var pahtFile = string.Format(FolderPath, htmlFileName);
            return helper.StaticContent(pahtFile, replacementTags, new ContextPathMapper(), "", out title);
        }

        public static string GetMatchingCommunities(this EmailTemplateViewModel model)
        {
            var html = "";
            if (model.ShowMatchingComm)
            {
                var pahtFile2 = string.Format(FolderPath, "MatchingCommunities.html");
                html = StaticContentHelper.StaticStringContent(null, pahtFile2, new List<ContentTag>(),
                    new ContextPathMapper(), "");
            }

            return html;
        }


        public static string GetRecommendComms(this EmailTemplateViewModel model)
        {
            var pahtFile2 = string.Format(FolderPath, "RecommendComms.html");
            var html = StaticContentHelper.StaticStringContent(null, pahtFile2,new List<ContentTag>(), new ContextPathMapper(), "");

            return html;
        }
    }
}
