using System.Collections.Generic;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Helpers;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class HomeDetailMobileHelper
    {
        public static MvcHtmlString GetCommunityNameLink(this HtmlHelper htmlHelper, HomeDetailViewModel model)
        {
            var paramz = new List<RouteParam>
            {
                new RouteParam(RouteParams.Builder, model.BuilderId),
                new RouteParam(RouteParams.Community, model.CommunityId)
            };
            return htmlHelper.NhsLink(model.CommunityName, paramz, Pages.CommunityDetail);
        }

        public static string HomeStatusTypeToString(this HomeDetailViewModel model)
        {
            return EnumConversionUtil.HomeStatusTypeToString(model.HomeStatus, LanguageHelper.AvailableNow, LanguageHelper.HomeModel, LanguageHelper.ReadyToBuild, LanguageHelper.UnderConstruction, LanguageHelper.QuickMoveIn);
        }

        public static bool IsModelHome(this HomeDetailViewModel model)
        {
            return model.HomeStatus.ToType<int>().HomeStatusTypeToHomeStatus() == "M";
        }

        public static string GetHomeGalleryUrl(this HomeDetailViewModel model)
        {
            var @paramas = new List<RouteParam>();
            if (!model.IsPlan)
            {
                @paramas.Add(new RouteParam(RouteParams.SpecId, model.SpecId, RouteParamType.QueryString));
                @paramas.Add(new RouteParam(RouteParams.PlanId, 0, RouteParamType.QueryString));
            }
            else
            {
                @paramas.Add(new RouteParam(RouteParams.SpecId, 0, RouteParamType.QueryString));
                @paramas.Add(new RouteParam(RouteParams.PlanId, model.PlanId, RouteParamType.QueryString));
            }

            @paramas.Add(new RouteParam(RouteParams.IncludeVideo, "true", RouteParamType.QueryString));
            return @paramas.ToUrl(Pages.GetHomeImages);
        }

        public static string GetFloorPlanGalleryUrl(this HomeDetailViewModel model)
        {
            var @paramas = new List<RouteParam>();
            if (!model.IsPlan)
            {
                @paramas.Add(new RouteParam(RouteParams.SpecId, model.SpecId, RouteParamType.QueryString));
                @paramas.Add(new RouteParam(RouteParams.PlanId, 0, RouteParamType.QueryString));
            }
            else
            {
                @paramas.Add(new RouteParam(RouteParams.SpecId, 0, RouteParamType.QueryString));
                @paramas.Add(new RouteParam(RouteParams.PlanId, model.PlanId, RouteParamType.QueryString));
            }
            @paramas.Add(new RouteParam(RouteParams.UseHub, model.PreviewMode.ToType<string>(), RouteParamType.QueryString));
            return @paramas.ToUrl(Pages.GetFloorPlanGallery);
        }

    }
}