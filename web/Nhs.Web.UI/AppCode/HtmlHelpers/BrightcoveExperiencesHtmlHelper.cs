﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class BrightcoveExperiencesHtmlHelper
    {
        public static MvcHtmlString GetBrightcoveExperiencesScript(this HtmlHelper helper)
        {
            var script = new StringBuilder();
            script.AppendFormat("<script src=\"{0}\"", "http://admin.brightcove.com/js/BrightcoveExperiences.js");
            script.Append("  type=\"text/javascript\">");
            script.Append("</script>");
            return script.ToString().ToMvcHtmlString();
        }
    }
}