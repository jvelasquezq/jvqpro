﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.ViewModels;
using Resources;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class PdfBrochureHelper
    {
        public static List<AmenitiesViewModel> GetAmenitiesCommunity(this HtmlHelper helper, PdfBrochureViewModel model)
        {
            if (model.HaveAmenities)
            {
                var amenityGroupType = Enum.GetValues(typeof (AmenityGroupType)).Cast<AmenityGroupType>();
                return amenityGroupType.Select(groupType => new AmenitiesViewModel(model.Amenities, groupType)).ToList();
            }
            return new List<AmenitiesViewModel>();
        }

        public static MvcHtmlString GetImage(this HtmlHelper helper, MediaPlayerObject image)
        {
            string thumbnail = image.Thumbnail.Replace("//", "/");
            if (!thumbnail.IsValidImage())
            {
                thumbnail =  GlobalResources14.Default.images.no_photo.no_photos_120x80_png;
                return helper.NhsImage(thumbnail, image.Caption, false);
            }

            return helper.NhsImage(thumbnail, image.Caption, true);
        }

        public static MvcHtmlString BuilderLink(this HtmlHelper helper, PdfBrochureViewModel model)
        {
            var builderUrl = model.BuilderUrl.StartsWith("http")
                                 ? model.BuilderUrl
                                 : string.Format("http://{0}", model.BuilderUrl);
            var host = new Uri(builderUrl);
            return helper.NhsLink(host.Host, model.GetParamBuilderLink, Pages.LogRedirect, true);
        }

        public static MvcHtmlString GetFormattedHotHomeTitleAndDescription(this HtmlHelper helper, string title,
            string description)
        {
            return string.IsNullOrEmpty(description)
                ? MvcHtmlString.Create(title)
                : MvcHtmlString.Create(string.Format("{0} : {1}", title, description));
        }

        public static MvcHtmlString GetFormattedPromoTitleAndDescription(this HtmlHelper helper, string title,
            string description)
        {
            return string.IsNullOrEmpty(description)
                ? MvcHtmlString.Create(title)
                : MvcHtmlString.Create(string.Format("{0} : {1}", title, description));
        }
    }
}
