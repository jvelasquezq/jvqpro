﻿using System.Web.Mvc.Html;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class CommunityResultHelper
    {
        public static void GetItemToShow(this HtmlHelper htmlHelper, ExtendedCommunityResult com)
        {
            if (com.IsBasicListing)
            {
                htmlHelper.RenderPartial(NhsMvc.Default.Views.CommunityResults.BasicListingItem, com);
            }
            else if (com.IsBasicCommunity && NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
            {
                htmlHelper.RenderPartial(NhsMvc.Default.Views.CommunityResults.BasicCommunityItem, com);
            }
            else
            {
                htmlHelper.RenderPartial(NhsMvc.Default.Views.CommunityResults.CommunityItem, com);
            }
        }
    }
}