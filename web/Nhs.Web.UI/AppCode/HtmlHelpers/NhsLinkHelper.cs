﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Linq;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Mvc.Routing.Interface;
using Nhs.Mvc.Routing.Utils;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using System.Text.RegularExpressions;
using System.Web.UI.MobileControls;
using Nhs.Library.Helpers.Utility;
using Nhs.Mvc.Routing.Configuration;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    /// <summary>
    /// HTMLHelper for all sorts of links
    /// </summary>
    public static class NhsLinkHelper
    {
        #region Image Links
        public static MvcHtmlString NhsImageLink(this HtmlHelper helper, string imgSrc, string linkSrc, bool preserveCase = false)
        {
            return NhsImageLink(helper, imgSrc, linkSrc, new List<RouteParam>(), preserveCase);
        }


        public static MvcHtmlString NhsImageLink(this HtmlHelper helper, string imgSrc, string linkSrc, List<RouteParam> paramList, bool preserveCase = false)
        {
            return NhsImageLink(helper, imgSrc, linkSrc, paramList, null, null, preserveCase);
        }

        public static MvcHtmlString NhsImageLink(this HtmlHelper helper, string imgSrc, string linkSrc, List<RouteParam> paramList, object linkAttributes, object imgAttributes, bool preserveCase = false)
        {
            return NhsImageLink(helper, imgSrc, linkSrc, paramList, linkAttributes, imgAttributes, false, false, preserveCase);
        }

        public static MvcHtmlString NhsImageLink(this HtmlHelper helper, string imgSrc, string linkSrc, List<RouteParam> paramList, object linkAttributes, object imgAttributes, bool makeModal, bool isLinkSrcNonNhsUri, bool preserveCase = false, bool isAbsoluteUrl = false)
        {
            const string http = "http://";

            //Construct <img> 
            var imageTag = BuildImageTag(imgSrc, string.Empty, imgAttributes);

            //Construct <a href>
            string linkUrl = "";
            if (!string.IsNullOrEmpty(linkSrc))
            {
                string linkSrcWithHttp = (!linkSrc.Contains(http) && isLinkSrcNonNhsUri)
                                             ? string.Concat(http, linkSrc)
                                             : linkSrc;

                linkUrl = isLinkSrcNonNhsUri || paramList == null ? linkSrcWithHttp : paramList.ToUrl(linkSrc, !preserveCase);
                //Use original page name if non-NHS url ELSE use paramList
            }

            if (isAbsoluteUrl)
                linkUrl = NhsUrlHelper.ResolveServerUrl(linkUrl);

            //Push <img/> inside <a href></a> => <a href><img/></a>
            return BuildHrefTag(linkUrl, imageTag.ToString(), linkAttributes, makeModal);
        }


        public static MvcHtmlString NhsAsyncImageLink(this HtmlHelper helper, string imgSrc, string linkSrc, List<RouteParam> paramList,
            object linkAttributes, IDictionary<string, object> imgAttributes, bool makeModal)
        {
            const string http = "http://";

            //Construct <img> 
            if(imgAttributes == null)
                imgAttributes = new Dictionary<string, object>();

            if (!imgAttributes.ContainsKey("class"))
                imgAttributes.Add("class", "async");
            
            var imageTag = helper.BuildAsyncImageTag(imgSrc, null, imgAttributes);

            //Construct <a href>
            var linkUrl = string.Empty;
            if (string.IsNullOrEmpty(linkSrc))
                return BuildHrefTag(linkUrl, imageTag.ToString(), linkAttributes, makeModal);

            var linkSrcWithHttp = (!linkSrc.Contains(http)) ? string.Concat(http, linkSrc) : linkSrc;
            linkUrl = paramList == null ? linkSrcWithHttp : paramList.ToUrl(linkSrc, false);
            //Use original page name if non-NHS url ELSE use paramList

            //Push <img/> inside <a href></a> => <a href><img/></a>
            return BuildHrefTag(linkUrl, imageTag.ToString(), linkAttributes, makeModal);
        }


        public static MvcHtmlString NhsAsyncImageLink(this HtmlHelper helper, string imgSrc, string linkSrc, List<RouteParam> paramList, object linkAttributes, IDictionary<string, object> imgAttributes, bool makeModal, bool isLinkSrcNonNhsUri, bool preserveCase = false, bool isAbsoluteUrl = false)
        {
            const string http = "http://";

            //Construct <img> 
            if (imgAttributes == null)
                imgAttributes = new Dictionary<string, object>();
            else if (!imgAttributes.ContainsKey("class"))
                imgAttributes.Add("class", "async");
            
            var imageTag = helper.BuildAsyncImageTag(imgSrc, null, imgAttributes);

            //Construct <a href>
            var linkUrl = string.Empty;
            if (!string.IsNullOrEmpty(linkSrc))
            {
                var linkSrcWithHttp = (!linkSrc.Contains(http) && isLinkSrcNonNhsUri)
                                             ? string.Concat(http, linkSrc)
                                             : linkSrc;

                linkUrl = isLinkSrcNonNhsUri || paramList == null ? linkSrcWithHttp : paramList.ToUrl(linkSrc, !preserveCase);
                //Use original page name if non-NHS url ELSE use paramList
            }

            if (isAbsoluteUrl)
                linkUrl = NhsUrlHelper.ResolveServerUrl(linkUrl);

            //Push <img/> inside <a href></a> => <a href><img/></a>
            return BuildHrefTag(linkUrl, imageTag.ToString(), linkAttributes, makeModal);
        }

        public static MvcHtmlString NhsActionImageLink(this HtmlHelper helper, string imgSrc, ActionResult actionResult)
        {
            return NhsActionImageLink(helper, imgSrc, string.Empty, actionResult);
        }

        public static MvcHtmlString NhsActionImageLink(this HtmlHelper helper, string imgSrc, string altText, ActionResult actionResult)
        {
            return NhsActionImageLink(helper, imgSrc, actionResult, null, new { alt = altText });
        }

        public static MvcHtmlString NhsActionImageLink(this HtmlHelper helper, string imgSrc, ActionResult actionResult, object linkAttributes, object imgAttributes)
        {
            string plh = "[replaceme]";
            var imageTag = BuildImageTag(imgSrc, string.Empty, imgAttributes).ToString();
            var linkSrc = NhsActionLink(helper, plh, actionResult, linkAttributes).ToString();
            return MvcHtmlString.Create(linkSrc.Replace(plh, imageTag));
        }
        #endregion

        #region Image
        public static MvcHtmlString NhsImage(this HtmlHelper helper, string imgSrc, string altText, object imgHtmlAttributes, bool prefixResourceDomain)
        {
            //Construct <img> 
            if (!string.IsNullOrEmpty(imgSrc) && prefixResourceDomain && !imgSrc.StartsWith("http"))
                imgSrc = string.Concat(Configuration.IRSDomain, imgSrc);

            var imageTag = BuildImageTag(imgSrc, altText, imgHtmlAttributes);
            return imageTag;
        }

        public static MvcHtmlString NhsAsyncImage(this HtmlHelper helper, string imgSrc, string altText, object imgHtmlAttributes, bool prefixResourceDomain)
        {
            if (!string.IsNullOrEmpty(imgSrc) && prefixResourceDomain && !imgSrc.StartsWith("http"))
                imgSrc = string.Concat(Configuration.IRSDomain, imgSrc);

            var imageTag = BuildAsyncImageTag(imgSrc, altText, imgHtmlAttributes);
            return imageTag;
        }

        public static MvcHtmlString NhsImage(this HtmlHelper helper, string imgSrc, string altText, bool prefixResourceDomain)
        {
            return helper.NhsImage(imgSrc, altText, null, prefixResourceDomain);
        }


        #endregion

        #region Links
        public static MvcHtmlString ResolveUrl(this HtmlHelper helper, string pageName)
        {
            var @params = new List<RouteParam>();
            return MvcHtmlString.Create(@params.ToUrl(pageName));
        }

        public static MvcHtmlString NhsLinkLogin(this HtmlHelper helper)
        {

            var @params = new List<RouteParam>();
            if (NhsRoute.CurrentRoute.Function.ToLower() != Pages.Login &&
                NhsRoute.CurrentRoute.Function.ToLower() != Pages.SignIn &&
                NhsRoute.CurrentRoute.Function.ToLower() != Pages.Register &&
                NhsRoute.CurrentRoute.Function.ToLower() != Pages.RecoverPassword &&
                NhsRoute.CurrentRoute.Function.ToLower() != Pages.Home &&
                !string.IsNullOrEmpty(NhsRoute.CurrentRoute.Function))
            {
                @params.Add(new RouteParam(RouteParams.NextPage, HttpUtility.UrlEncode(NhsRoute.CurrentRoute.HostUrl),
                    RouteParamType.QueryString, true, true));

            }
            return helper.NhsLink(LanguageHelper.SignInCreateAccount, Pages.Login, @params, null);
        }

        public static MvcHtmlString NhsFullSite(this HtmlHelper helper)
        {

            var @params = new List<RouteParam>
            {
                new RouteParam(RouteParams.NextPage, HttpUtility.UrlEncode(NhsRoute.CurrentRoute.HostUrl),
                    RouteParamType.QueryString, true, true)
            };
            return helper.NhsLink(LanguageHelper.FullSite, Pages.ShowFullSite, @params, null);
        }

        public static MvcHtmlString NhsMobileSite(this HtmlHelper helper)
        {

            var @params = new List<RouteParam>
            {
                new RouteParam(RouteParams.NextPage, HttpUtility.UrlEncode(NhsRoute.CurrentRoute.HostUrl), RouteParamType.QueryString, true, true)
            };
            return helper.NhsLink(LanguageHelper.MobileSite, Pages.ShowMobileSite, @params, null);
        }
        
        public static MvcHtmlString NhsLink(this HtmlHelper helper, string linkText, string pageName, bool preserveCase = false)
        {
            return NhsLink(helper, linkText, new List<RouteParam>(), pageName, preserveCase);
        }

        public static MvcHtmlString NhsLink(this HtmlHelper helper, string linkText, List<RouteParam> paramList, string pageName, bool preserveCase = false)
        {
            return NhsLink(helper, linkText, pageName, paramList, null, preserveCase);
        }

        public static MvcHtmlString NhsImageLinkLogAndRedirect(this HtmlHelper helper, string text, string imgSrc, string linkUrl, string eventCode, object logInfo, string onclickExtraCode, object linkAttributes = null, bool isBasicListing = false, bool isFeaturedListing = false, object imgAttributes = null)
        {
            string onclickJs = onclickExtraCode;
            var linkAttrs = new RouteValueDictionary(linkAttributes);
            var inANewWindow = linkAttrs.Any(i => i.Key == "target" && i.Value.ToString() == "_blank");

            linkUrl = linkUrl.Replace("'", "");

            onclickJs += string.Format(@" if(typeof logger != 'undefined') {{ logger.logWithParametersAndRedirect(this, '{0}', '{1}', {2}, {3}); return false; }}", linkUrl, eventCode, logInfo.ToJson(), inANewWindow.ToString().ToLower());
   
            linkAttrs.Add("onclick", onclickJs);
            linkAttrs.Add("title", text);

            var attributes = new RouteValueDictionary(imgAttributes);
            if(attributes.ContainsKey("class"))
                attributes["class"] = attributes["class"] + " async";
            else
                attributes.Add("class" , "async");

            return NhsAsyncImageLink(helper, imgSrc, NhsRoute.CurrentRoute.HostUrl, null, linkAttrs, attributes, false, false);
        }


        public static MvcHtmlString NhsLinkLogAndRedirect(this HtmlHelper helper, string linkText, string linkUrl, string eventCode, object logInfo, string onclickExtraCode, object linkAttributes = null, bool isBasicListing = false, bool isFeaturedListing = false, bool isAbsoluteUrl = false)
        {
            string onclickJs = onclickExtraCode;
            var linkAttrs = new RouteValueDictionary(linkAttributes);
            var inANewWindow = linkAttrs.Any(i => i.Key == "target" && i.Value.ToString() == "_blank");

            linkUrl = linkUrl.Replace("'", string.Empty);
            if (isAbsoluteUrl)
                linkUrl = NhsUrlHelper.ResolveServerUrl("/" + linkUrl);

            onclickJs += string.Format(@" if(typeof logger != 'undefined') {{ logger.logWithParametersAndRedirect(this, '{0}', '{1}', {2}, {3}); return false; }}", linkUrl, eventCode, logInfo.ToJson(), inANewWindow.ToString().ToLower());

            linkAttrs.Add("onclick", onclickJs);


            return BuildHrefTag(NhsRoute.CurrentRoute.HostUrl, linkText, linkAttrs, false);
        }

        /// <summary>
        /// This method creates a link using the redirect page, and add the link passed by parameter as a query string parameter. This is useful for html that has to be sent by email, where we can't use js
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="linkText"></param>
        /// <param name="linkUrl"></param>
        /// <param name="eventCode"></param>
        /// <param name="commId"></param>
        /// <param name="builderId"></param>
        /// <param name="planId"></param>
        /// <param name="specId"></param>
        /// <param name="marketId"></param>
        /// <param name="linkAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString NhsLinkLogAndRedirectWithoutJs(this HtmlHelper helper, string linkText, string linkUrl, string eventCode, string commId, string builderId, string planId, string specId, string marketId, object linkAttributes = null, string utmParameters = "", string utmContent = "")
        {
            var linkAttrs = new RouteValueDictionary(linkAttributes);
            var url = GetUrlNhsLinkLogAndRedirectWithoutJs(linkUrl, eventCode, commId, builderId, planId, specId, utmParameters, utmContent);
            // TODO improve this by using a regex
            return BuildHrefTag(url.Replace(".com//", ".com/"), linkText, linkAttrs, false);
        }

        public static string GetUrlNhsLinkLogAndRedirectWithoutJs(string linkUrl, string eventCode, string commId = "", string builderId = "", string planId = "", string specId = "", string utmParameters = "", string utmContent = "")
        {

            linkUrl = linkUrl.Replace("'", "");
            if (linkUrl == "communitydetail")
                linkUrl += string.Format("/community-{0}/builder-{1}", commId, builderId);
            if (linkUrl == "homedetail")
            {
                if (specId.ToType<int>() > 0)
                    linkUrl += string.Format("/specid-{0}", specId);
                if (planId.ToType<int>() > 0)
                    linkUrl += string.Format("/planid-{0}", planId);
            }

            var loggerUrl = new NhsUrl(Pages.LogRedirect);
            loggerUrl.AddParameter(UrlConst.LogEvent, eventCode, RouteParamType.Friendly);

            var nUrl = NhsUrlHelper.GetFriendlyUrl();
            var domainPartnerSiteUrl = nUrl.DomainInfo;
            var domainParttnerSiteUrlWithPrivateLabel = nUrl.SiteRootWithDomainInfo.Replace(".com//", ".com/");

            var url = domainPartnerSiteUrl + loggerUrl + "/?url=" + HttpUtility.UrlEncode(domainParttnerSiteUrlWithPrivateLabel + linkUrl + String.Format(utmParameters, utmContent));

            return url.Replace(".com//", ".com/");
        }

        public static MvcHtmlString NhsLinkLogAndRedirectWithoutJsMove(this HtmlHelper helper, string linkText,
            string linkUrl, string eventCode, string commId, string marketId, string city, string builderName,
            object linkAttributes = null, string utmParameters = "", string utmContent = "")
        {
            var linkAttrs = new RouteValueDictionary(linkAttributes);

            var market = XMarketFactory.CreateMarket(marketId.ToType<int>());

            string friendlyUrl = string.Format("{0}/{1}/{2}/{3}/{4}/{5}-by-{6}",
                Pages.CommunityDetailMove,
                commId,
                market.State,
                market.MarketName.RemoveSpecialCharacters(),
                city.RemoveSpecialCharacters(),
                linkText.RemoveSpecialCharacters(),
                builderName.RemoveSpecialCharacters());

            friendlyUrl = Regex.Replace(friendlyUrl.ToLower().Replace(" ", "-"), @"\-+", "-");

            var loggerUrl = new NhsUrl(Pages.LogRedirect);
            loggerUrl.AddParameter(UrlConst.LogEvent, eventCode, RouteParamType.Friendly);

            var nUrl = NhsUrlHelper.GetFriendlyUrl();
            var domainPartnerSiteUrl = nUrl.DomainInfo;
            var domainParttnerSiteUrlWithPrivateLabel = nUrl.SiteRootWithDomainInfo.Replace(".com//", ".com/");

            var url = domainPartnerSiteUrl + loggerUrl + "/?url=" +
                      HttpUtility.UrlEncode(domainParttnerSiteUrlWithPrivateLabel + friendlyUrl +
                                            String.Format(utmParameters, utmContent));

            // TODO improve this by using a regex
            return BuildHrefTag(url.Replace(".com//", ".com/"), linkText, linkAttrs, false);
        }

        public static MvcHtmlString NhsLinkLogAndRedirectWithoutJsAndDfu(this HtmlHelper helper, string linkText, string linkUrl, string eventCode, int marketId, string queryStringParameter, object linkAttributes = null, string utmParameters = "", string utmContent = "")
        {
            var linkAttrs = new RouteValueDictionary(linkAttributes);
            linkUrl = linkUrl.Replace("'", string.Empty);
            var resultsUrl = string.Format(@"/{0}/{1}/", RedirectionHelper.ToResultsParams(marketId).ToUrl(linkUrl), queryStringParameter);
            var dfuUrl = resultsUrl.Contains("?")
                ? resultsUrl + string.Format(utmParameters.Replace("?", "&"), utmContent)
                : resultsUrl + string.Format(utmParameters, utmContent);

            var loggerUrl = new NhsUrl(Pages.LogRedirect);
            loggerUrl.AddParameter(UrlConst.LogEvent, eventCode, RouteParamType.Friendly);

            var nUrl = NhsUrlHelper.GetFriendlyUrl();
            var domainPartnerSiteUrl = nUrl.DomainInfo;
            var domainParttnerSiteUrlWithPrivateLabel = nUrl.SiteRootWithDomainInfo.Replace(".com//", ".com/");
            string url= domainPartnerSiteUrl + loggerUrl + "/?url=" + domainParttnerSiteUrlWithPrivateLabel + dfuUrl;

            // TODO improve this by using a regex
            return BuildHrefTag(url.Replace(".com//", ".com/"), linkText, linkAttrs, false);
        }

        public static MvcHtmlString NhsGetBrochureForSearchAlert(this HtmlHelper helper, string linkText, string linkUrl, string eventCode, string commId, string builderId, string planId, string specId, string marketId, UserProfile userProfile, object linkAttributes = null, string utmParameters = "", string utmContent = "")
        {
            var linkAttrs = new RouteValueDictionary(linkAttributes);

            var loggerUrl = new NhsUrl(Pages.LogRedirect);
            loggerUrl.AddParameter(UrlConst.LogEvent, eventCode, RouteParamType.Friendly);

            var nUrl = NhsUrlHelper.GetFriendlyUrl();
            var domain = nUrl.DomainInfo;


            var url = domain + loggerUrl + "/?url=" +
                      HttpUtility.UrlEncode(GetBrochureUrl(string.Empty, string.Empty, commId, builderId, userProfile) +
                                            String.Format(utmParameters, utmContent));

            return BuildHrefTag(url.Replace(".com//", ".com/"), linkText, linkAttrs, false);
        }

        private static string GetBrochureUrl(string planId, string specId, string communityId, string builderId, UserProfile userProfile)
        {
            const string utmParameters = "utm_source=alert&utm_medium=email&utm_campaign=alert&utm_content=viewbrochure";
            var @params = new List<RouteParam>();
            @params.Add(new RouteParam(RouteParams.Email, userProfile.LogonName));
            @params.Add(new RouteParam(RouteParams.Refer, userProfile.ReferrerName));
            
            if (communityId.Length > 0 && builderId.Length > 0)
            {
                @params.Add(new RouteParam(RouteParams.CommunityId, communityId));
                @params.Add(new RouteParam(RouteParams.BuilderId, builderId));
            }
            if (planId.Length > 0)
                @params.Add(new RouteParam(RouteParams.PlanId, planId));
            if (specId.Length > 0)
                @params.Add(new RouteParam(RouteParams.SpecId, specId));
            
                @params.Add(new RouteParam(RouteParams.LeadType,  LeadType.Community));
                return @params.ToUrl(Pages.BrochureGen, true, true, utmParameters);
        }

        public static MvcHtmlString CreateOnClickLogEvent(this HtmlHelper helper, string linkUrl, string eventCode, string commId,
            string builderId, string planOrFLId, string specId, string marketId, string onclickExtraCode, object linkAttributes = null,
            bool isBasicListing = false, bool isFeaturedListing = false, bool isLoggingActive = true)
        {
            var onclick = "onclick=\"{0}\"";

            string onclickJs = onclickExtraCode;
            var linkAttrs = new RouteValueDictionary(linkAttributes);
            var inANewWindow = linkAttrs.Any(i => i.Key == "target" && i.Value.ToString() == "_blank");

            linkUrl = linkUrl.Replace("'", string.Empty);
            if (isLoggingActive)
            {
                if (isBasicListing)
                    onclickJs += string.Format(" if(typeof logger != 'undefined') {{ logger.logBasicListingAndRedirect(this, '{0}', '{1}', {2}, {3}); return false; }}", linkUrl, eventCode, commId, marketId);
                else if (isFeaturedListing)
                    onclickJs += string.Format(" if(typeof logger != 'undefined') {{ logger.logFeaturedListingAndRedirect(this, '{0}', '{1}', {2}, {3}, {4}, {5}, {6}); return false; }}", linkUrl, eventCode, planOrFLId, commId, builderId, marketId, inANewWindow.ToString().ToLower());
                else
                    onclickJs += string.Format(" if(typeof logger != 'undefined') {{ logger.logAndRedirect(this, '{0}', '{1}', {2}, {3}, {4}, {5}, {6}, {7} ); return false; }}", linkUrl, eventCode, commId, builderId, planOrFLId, specId, marketId, inANewWindow.ToString().ToLower());
            }
            else
            {
                onclickJs += string.Format(" if(typeof logger != 'undefined') {{logger._redirect(this, '{0}', {1});  return false; }}", linkUrl, inANewWindow.ToString().ToLower());
            }

            onclick = string.Format(onclick, onclickJs);

            return onclick.ToMvcHtmlString();
        }

        public static MvcHtmlString NhsLinkRichBreadcrumb(this HtmlHelper helper, string linkText, string pageName,
            List<RouteParam> paramList, dynamic linkAttributes, bool preserveCase = false, bool isAbsoluteUrl = false)
        {
            var linkUrl = paramList.ToUrl(pageName, !preserveCase);

            if (isAbsoluteUrl)
                linkUrl = NhsUrlHelper.ResolveServerUrl(linkUrl);

            linkAttributes.itemprop = "url";

            return BuildHrefTag(linkUrl, linkText, (object)linkAttributes, false, true);
        }

        public static MvcHtmlString NhsLink(this HtmlHelper helper, string linkText, string pageName, List<RouteParam> paramList, object linkAttributes, bool preserveCase = false, bool isAbsoluteUrl = false)
        {
            var linkUrl = paramList.ToUrl(pageName, !preserveCase);

            if (isAbsoluteUrl)
                linkUrl = NhsUrlHelper.ResolveServerUrl(linkUrl);

            return BuildHrefTag(linkUrl, linkText, linkAttributes, false);
        }


        public static string BuildLink(string pageName, List<RouteParam> paramList, bool preserveCase = true, bool isAbsoluteUrl = false)
        {
            var linkUrl = paramList.ToUrl(pageName, preserveCase);

            if (isAbsoluteUrl)
                linkUrl = NhsUrlHelper.ResolveServerUrl(linkUrl);

            return linkUrl;
        }

        public static MvcHtmlString NhsLinkWithUtm(this HtmlHelper helper, string linkText, string pageName, List<RouteParam> paramList, object linkAttributes, string utmParameters = "", bool isAbsoluteUrl = true)
        {
            var linkUrl = paramList.ToUrl(pageName, utmParameters).Replace("??", "?");
            if (isAbsoluteUrl)
                linkUrl = NhsUrlHelper.ResolveServerUrl(linkUrl);
            return BuildHrefTag(linkUrl, linkText, linkAttributes, false);
        }

        public static MvcHtmlString NhsLinkCustom(this HtmlHelper helper, string linkText, string url, object parameters = null)
        {
            return BuildHrefTag(url, linkText, parameters, false);
        }

        public static MvcHtmlString NhsLink(this HtmlHelper helper, string linkText, string pageName, object parameters, bool preserveCase = false)
        {
            return BuildNhsLinkUrl(linkText, pageName, new RouteValueDictionary(parameters).ToParams(RouteParamType.Friendly), false, preserveCase);
        }

        public static MvcHtmlString ChatLink(this HtmlHelper helper, string linkText, string googlePushScript, ChatPlacement placement)
        {
            return LiveChatHelper.LiveChatLink(linkText, googlePushScript, placement);
        }

        public static MvcHtmlString NhsLink(this HtmlHelper helper, string linkText, string pageName, object paramList, bool persistParams, bool preserveCase = false)
        {
            var finalList = new RouteValueDictionary();
            // Joins lists
            if (persistParams)
            {
                foreach (var param in NhsRoute.CurrentRoute.Params)
                    finalList.Add(param.Name.ToString(), param.Value);
            }
            foreach (var param in new RouteValueDictionary(paramList))
                if (!finalList.ContainsKey(param.Key))
                    finalList.Add(param.Key, param.Value);

            return BuildNhsLinkUrl(linkText, pageName, finalList.ToParams(RouteParamType.Friendly), false, preserveCase);
        }

        public static MvcHtmlString NhsActionLink(this HtmlHelper helper, string linkText, ActionResult actionResult)
        {
            return NhsActionLink(helper, linkText, actionResult, null);
        }

        public static MvcHtmlString NhsActionLink(this HtmlHelper helper, string linkText, ActionResult actionResult, object linkAttributes)
        {
            var routeValues = actionResult.GetRouteValueDictionary();
            string routeName = RouteHelper.GetRouteNameFromAction(routeValues);

            if (NhsRoute.PartnerSiteUrl.Length > 0 && !string.IsNullOrEmpty(routeName))
                routeName = "p" + routeName;

            return MvcHtmlString.Create(HtmlHelper.GenerateLink(helper.ViewContext.RequestContext, helper.RouteCollection, linkText, routeName, null, null, routeValues, new RouteValueDictionary(linkAttributes)));
        }

        #endregion

        #region Back to Search Link
        public static string BackToSearchLink(int totalCommunities, int marketId, string marketName)
        {
            const string containerText = "<p>View {0} new home communities in the greater {1} Area</p>";
            const string backToSearchLink = "<p><a href='{0}'>Return to your search</a></p>";
            if (totalCommunities > 0)
            {
                string hyperLink = string.Format(backToSearchLink, string.Format(@"/{0}/{1}-{2}", Pages.CommunityResults, "market", marketId));
                string textToDisplay = string.Format(containerText, totalCommunities, marketName);

                return string.Concat(hyperLink, textToDisplay);
            }
            return string.Format(backToSearchLink, string.Concat("/", Pages.Home));
        }
        #endregion

        #region Submit Links
        public static string NhsSubmitLink(this HtmlHelper htmlHelper, string linkText)
        {
            return htmlHelper.NhsSubmitLink(linkText, null);
        }

        public static string NhsSubmitLink(this HtmlHelper htmlHelper, string linkText, object htmlAttributes)
        {
            TagBuilder tagBuilder = new TagBuilder("a");
            tagBuilder.MergeAttribute("href", @"javascript:void(0)");
            tagBuilder.MergeAttribute("onclick", @"$(this).parents('form:first').submit()");
            tagBuilder.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            tagBuilder.InnerHtml = linkText;
            return tagBuilder.ToString();
        }
        #endregion

        #region Modal Window Links
        public static MvcHtmlString NhsModalWindowLink(this HtmlHelper htmlHelper, string linkText, string pageName, int width, int height)
        {
            return NhsModalWindowLink(htmlHelper, linkText, pageName, width, height, new List<RouteParam>(), null);
        }


        public static MvcHtmlString NhsModalWindowLink(this HtmlHelper htmlHelper, string linkText, string pageName, int width, int height, bool lowerCaseUrl)
        {
            return NhsModalWindowLink(htmlHelper, linkText, pageName, width, height, new List<RouteParam>(), null, false, lowerCaseUrl);
        }

        public static MvcHtmlString NhsModalWindowLink(this HtmlHelper htmlHelper, string linkText, string pageName, int width, int height, List<RouteParam> paramList)
        {
            return NhsModalWindowLink(htmlHelper, linkText, pageName, width, height, paramList, null);
        }

        public static MvcHtmlString NhsModalWindowLink(this HtmlHelper htmlHelper, string linkText, string pageName, int width, int height, List<RouteParam> paramList, Dictionary<string, object> linkAttributes)
        {
            return NhsModalWindowLink(htmlHelper, linkText, pageName, width,  height,  paramList, linkAttributes, false, false);
        }

        public static MvcHtmlString NhsModalWindowLink(this HtmlHelper htmlHelper, string linkText, string pageName, int width, int height, List<RouteParam> paramList, object linkAttributes, bool showInIFrame = false, bool lowerCaseUrl = true)
        {
            if (paramList == null)
                paramList = new List<RouteParam>();

            if (showInIFrame)
            {
                paramList.Add(new RouteParam(RouteParams.KeepThis, "true", RouteParamType.QueryString));
                paramList.Add(new RouteParam(RouteParams.TB_iframe, "true", RouteParamType.QueryString));
                lowerCaseUrl = false;
            }

            if (width > 0)
                paramList.Add(new RouteParam(RouteParams.Width, width.ToString(), RouteParamType.QueryString));
            if (height > 0)
                paramList.Add(new RouteParam(RouteParams.Height, height.ToString(), RouteParamType.QueryString));
            //paramList.Add(new Param(NhsLinkParams.modal, "true", UrlParamType.QueryString));

            var linkUrl = paramList.ToUrl(pageName, lowerCaseUrl);
            return BuildHrefTag(linkUrl, linkText, linkAttributes, true);
        }

        public static MvcHtmlString NhsModalWindowLink(this HtmlHelper htmlHelper, string linkText, string pageName, int width, int height, object parameters)
        {
            return NhsModalWindowLink(htmlHelper, linkText, pageName, width, height, parameters, null, RouteParamType.QueryString);
        }

        public static MvcHtmlString NhsModalWindowLink(this HtmlHelper htmlHelper, string linkText, string pageName, int width, int height, object parameters, object linkAttributes)
        {
            return NhsModalWindowLink(htmlHelper, linkText, pageName, width, height, parameters, linkAttributes, RouteParamType.QueryString);
        }

        public static MvcHtmlString NhsModalWindowLink(this HtmlHelper htmlHelper, string linkText, string pageName, int width, int height, object parameters, object linkAttributes, RouteParamType urlParamType, bool showInIFrame = false)
        {
            var @params = new RouteValueDictionary(parameters).ToParams(urlParamType);
            return NhsModalWindowLink(htmlHelper, linkText, pageName, width, height, @params, linkAttributes, showInIFrame);
        }

        public static MvcHtmlString NhsModalWindowLink(this HtmlHelper htmlHelper, string linkText, string linkUrl, object htmlAttributes = null)
        {
            return BuildHrefTag(linkUrl, linkText, htmlAttributes, true);
        }
        #endregion

        #region AjaxLink

        public static MvcHtmlString NhsAjaxLink(this HtmlHelper helper, string linkText, string pageName)
        {
            return NhsAjaxLink(helper, linkText, pageName, new List<RouteParam>());
        }

        public static MvcHtmlString NhsAjaxLink(this HtmlHelper helper, string linkText, string pageName, List<RouteParam> paramList)
        {
            string url = paramList.ToUrl(pageName);
            TagBuilder tagBuilder = new TagBuilder("a");
            tagBuilder.MergeAttribute("href", @"javascript:void(0)");
            tagBuilder.MergeAttribute("onclick", string.Format("tb_ChangeUrl('{0}')", url));  // TODO: Container shouldn't be hardcoded
            tagBuilder.InnerHtml = linkText;
            return MvcHtmlString.Create(tagBuilder.ToString());
        }
        #endregion

        #region Private Helper Methods
        private static MvcHtmlString BuildImageTag(string imgSrc, string alt, object imgHtmlAttributes)
        {
            var imgTag = new TagBuilder("img");
            imgTag.MergeAttribute("src", imgSrc);
            imgTag.MergeAttribute("alt", alt);
            imgTag.MergeAttribute("title", alt);
            imgTag.MergeAttributes(new RouteValueDictionary(imgHtmlAttributes), true);
            return MvcHtmlString.Create(imgTag.ToString(TagRenderMode.SelfClosing));
        }

        private static MvcHtmlString BuildAsyncImageTag(string imgSrc, string alt, object imgHtmlAttributes)
        {
            var imgTag = new TagBuilder("img");
            imgTag.MergeAttribute("class", "async");
            imgTag.MergeAttribute("data-src", imgSrc);
            imgTag.MergeAttribute("src", Resources.GlobalResources14.Default.images._1x1_gif);
            imgTag.MergeAttribute("alt", alt);
            imgTag.MergeAttribute("title", alt);
            imgTag.MergeAttributes(new RouteValueDictionary(imgHtmlAttributes), true);
            return MvcHtmlString.Create(imgTag.ToString(TagRenderMode.SelfClosing));
        }


        //TODO: Add support for params with no dashes in them
        private static MvcHtmlString BuildNhsLinkUrl(string linkText, string pageName, IEnumerable<RouteParam> paramList, bool makeModal, bool preserveCase = false)
        {
            return BuildHrefTag(paramList.ToUrl(pageName, !preserveCase), linkText, null, makeModal);
        }

        private static MvcHtmlString BuildHrefTag(string linkUrl, string innerHtmlOrLinkText, object htmlAttributes,
            bool makeModal, bool useRichBreadcrum = false)
        {
            var link = new TagBuilder("a");
            link.MergeAttribute("href", linkUrl);
            
            link.InnerHtml = useRichBreadcrum
                ? string.Format("<span>{0}</span>", innerHtmlOrLinkText)
                : innerHtmlOrLinkText;

            IDictionary<string, object> attributes;

            if (htmlAttributes == null)
                attributes = new Dictionary<string, object>();
            else if (htmlAttributes.GetType() == typeof(RouteValueDictionary))
                attributes = htmlAttributes as RouteValueDictionary;
            else if (htmlAttributes.GetType() == typeof(Dictionary<string, object>))
                attributes = htmlAttributes as Dictionary<string, object>;
            else if (htmlAttributes is ExpandoObject)
                attributes = (IDictionary<string, object>)htmlAttributes;
            else attributes = new RouteValueDictionary(htmlAttributes);

            if (makeModal)
            {
                // Class attribute
                var @class = "thickbox";
               
                if (NhsRoute.ShowMobileSite)
                    @class = "nhs_ModalPopUp";

                if (attributes != null)
                {
                    if (attributes.ContainsKey("class"))
                        @class += " " + attributes["class"];
                    attributes["class"] = @class;
                }
            }

            link.MergeAttributes(attributes, true);
            return MvcHtmlString.Create(link.ToString());

        }

        #endregion
    }
}
