﻿using System.Web.Mvc;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class TvHelper
    {
        public static MvcHtmlString NhsYoutubeEmbed(this HtmlHelper helper, string videoUrl, string originUrl, string id)
        {
            const string youtubeConst = "<div id='{0}' class='nhs_EmbedYTPlayer' data='{1}&origin={2}'></div>";
            var embedCode = string.Format(youtubeConst, id, videoUrl, originUrl);
            return new MvcHtmlString(embedCode);
        }

    }
}