﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Bdx.Web.Api.Objects;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Content;
using Nhs.Library.Web;
using Nhs.Library.Web.ImageResizer;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Partials;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Extensions;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult;

namespace Nhs.Web.UI.AppCode.HtmlHelpers
{
    public static class CommunityHomeResultHelper
    {
        public static MvcHtmlString GetZipCodeLink(this HtmlHelper helper, CommunityHomeResultsViewModel model, int zipCode)
        {
            var marketId = model.Market.MarketId;
            var @params = new List<RouteParam>
            {
                new RouteParam(RouteParams.Market, marketId),
                new RouteParam(RouteParams.PostalCode, zipCode, RouteParamType.QueryString)
            }.ToResultsParams();

            var page = RedirectionHelper.GetCommunityResultsPage();

            string title;
            if (model.Globals.PartnerLayoutConfig.IsBrandPartnerNhs)
                title = string.Format("{0} new homes - {1}, {2} New Homes for Sale", zipCode, model.City, model.Market.State.StateName);
            else if (model.Globals.PartnerLayoutConfig.IsBrandPartnerCna)
                title = string.Format("{0} nuevas casas - {1}, {2} Nuevas casa a la venta", zipCode, model.City, model.Market.State.StateName);
            else
                title= string.Format("{0} new homes for sale in {1}, {2}", zipCode, model.City, model.State);

            return helper.NhsLink(zipCode.ToType<string>(), page, @params, new {title});
        }

        public static MvcHtmlString GetSeoCommunityLink(this HtmlHelper helper, SEOCommunity comm)
        {
            var @params = new List<RouteParam>
                {
                    new RouteParam(RouteParams.Market,comm.MarketId),
                    new RouteParam(RouteParams.CommunityName, HttpUtility.UrlEncode(comm.Name), RouteParamType.QueryString)
                }.ToResultsParams();

            var page = RedirectionHelper.GetCommunityResultsPage();
            return helper.NhsLink(comm.Name, page, @params, null);
        }

        public static MvcHtmlString CityFooterLinks(this HtmlHelper helper, string cityName, List<RouteParam> parameters, int marketId)
        {
            var page = RedirectionHelper.GetCommunityResultsPage();
            var home = " homes";
            return helper.NhsLink(string.Concat(cityName, home), page, parameters, null, true);
        }

        public static MvcHtmlString CommingSonFooterLink(this HtmlHelper helper, string marketName, int marketId)
        {
            var @params = new List<RouteParam>
            {
                new RouteParam(RouteParams.Market, marketId),
                new RouteParam(RouteParams.ComingSoon, "true", RouteParamType.QueryString)
            }.ToResultsParams();

            var page = RedirectionHelper.GetCommunityResultsPage();
            return helper.NhsLink(LanguageHelper.SeeCommunitiesThatAreComingSoon + " " + marketName, page, @params,new {@id = "ComingSoonFacet"});
        }

        public static MvcHtmlString ActiveAdultFooterLink(this HtmlHelper helper, string linkText, int marketId)
        {
            var @params = new List<RouteParam>
            {
                new RouteParam(RouteParams.Market, marketId),
                new RouteParam(RouteParams.Adult, "true", RouteParamType.QueryString)
            }.ToResultsParams();

            var page = RedirectionHelper.GetCommunityResultsPage();

            return helper.NhsLink(linkText,  page, @params, new { @id = "ActiveAdultFacet" });
        }


        public static MvcHtmlString CreateCountsFacetsMobile(this ApiResultCounts resultCounts)
        {
            if (resultCounts.CommCount == 0)
                return LanguageHelper.NoResultsFound.ToMvcHtmlString();
            return
                string.Format("{0} Communit{1} <span>|</span> {2} Home{3}", resultCounts.CommCount,
                              (resultCounts.CommCount > 1 ? "ies" : "y"), resultCounts.HomeCount,
                              (resultCounts.HomeCount > 1 ? "s" : "")).ToMvcHtmlString();
        }

        public static MvcHtmlString CreateCounts(this BaseCommunityHomeResultsViewModel model)
        {
            if ((model.ResultCounts.CommCount == 0 && model.SrpPageType == SearchResultsPageType.CommunityResults) ||
                (model.ResultCounts.HomeCount == 0 && model.ResultCounts.BlCount == 00 && model.SrpPageType == SearchResultsPageType.HomeResults))
                return ("<strong>" + LanguageHelper.OopsNoResults + "</strong>").ToMvcHtmlString();

            if (model.Globals.PartnerLayoutConfig.IsBrandPartnerCna)
            {
                var replace1 = model.ResultCounts.CommCount > 1 ? "es" : "";
                var replace2 = model.ResultCounts.HomeCount + model.ResultCounts.BlCount;
                var replace3 = model.ResultCounts.HomeCount + model.ResultCounts.BlCount > 1 ? "s" : "";
                var replace4 =  (model.SrpPageType == SearchResultsPageType.CommunityResults
                            ? model.ResultCounts.CommCount
                            : model.ResultCounts.HomeCount + model.ResultCounts.BlCount) > 1
                            ? "s"
                            : "";
                return
                    string.Format(
                        model.SrpPageType == SearchResultsPageType.CommunityResults
                            ? LanguageHelper.MachingCommunityResultTitle
                            : LanguageHelper.MachingHomeResultTitle, model.ResultCounts.CommCount, replace1, replace2,
                        replace3, replace4).ToMvcHtmlString();
            }
            return
               string.Format(
                   model.SrpPageType == SearchResultsPageType.CommunityResults
                       ? LanguageHelper.MachingCommunityResultTitle
                       : LanguageHelper.MachingHomeResultTitle, model.ResultCounts.CommCount,
                   (model.ResultCounts.CommCount > 1 ? "ies" : "y"),
                   model.ResultCounts.HomeCount + model.ResultCounts.BlCount,
                   (model.ResultCounts.HomeCount + model.ResultCounts.BlCount > 1 ? "s" : "")).ToMvcHtmlString();
        }
        
        public static MvcHtmlString Pagination(this CommunityHomeResultsViewModel model)
        {
            var totalResults =
                model.SrpPageType == SearchResultsPageType.CommunityResults
                    ? model.ResultCounts.CommCount
                    : model.ResultCounts.HomeCount + model.ResultCounts.BlCount;
            var url = model.PagingUrl.Replace("[function]", model.SrpPageType == SearchResultsPageType.CommunityResults
                                                                ? Pages.CommunityResults
                                                                : Pages.HomeResults);

            const string dots = "<a href=\"javascript:void(0)\" class=\"nhs_PagerDots {0}\" {1}>...</a>";            

            var numberOfPages = Math.Ceiling(totalResults.ToType<decimal>() / model.PageSize).ToType<int>();

            if (numberOfPages == 0)
                numberOfPages = 1;

            var html = new StringBuilder();
            html.AppendLine("<div class=\"nhs_PagingLinks\" " + (numberOfPages == 1 ? "style=\"display:none\"" : "") + ">");
            html.AppendLine(string.Format("<a href=\"javascript:void(0)\" class=\"nhs_Previous btnCss {0}\">" + LanguageHelper.Previous + "</a>", 
                model.CurrentPage > 1 ? string.Empty : "nhs_Disabled"));
            html.AppendLine("<span id=\"nhs_PagingLinks\">");
            html.AppendLine("<span>");
            html.AppendLine(GeneratePageLink(1, url, model.CurrentPage, false));
            html.Append(string.Format(dots, "nhs_Previous", 
                numberOfPages-2 < 14 || model.CurrentPage <= 9 
                ? "style='display:none;'" 
                : string.Empty));

            html.AppendLine("</span>");

            html.AppendLine("<span class=\"nhs_PagingOut\">");
            html.AppendLine("<ul id=\"nhs_PagingLinksConteiner\" class=\"nhs_PagingIn\">");
            if (numberOfPages > 2)
            {
                for (var i = 2; i < numberOfPages; i++)
                    html.AppendLine("<li>" + GeneratePageLink(i, url, model.CurrentPage, false) + "</li>");
            }
            html.AppendLine("</ul>");
            html.AppendLine("</span>");
            
            html.AppendLine("<span>");

            var lastDots = string.Format(dots, "nhs_Next", 
                numberOfPages - 2 > 14 && model.CurrentPage + 7 < numberOfPages - 1 
                ? string.Empty 
                : "style='display:none;'");

            html.AppendLine(lastDots);

            html.AppendLine(numberOfPages >= 2
                                ?  GeneratePageLink(numberOfPages, url, model.CurrentPage, true)
                                : "<a class=\"nhs_Page nhs_lastPage\" data-page=\"\" href=\"\"></a>");
           
            html.AppendLine("</span>");
            html.AppendLine("</span>");
            html.AppendLine(string.Format("<a href=\"javascript:void(0)\" class=\"nhs_Next btnCss {0}\">" + LanguageHelper.Next + "</a>", model.CurrentPage < numberOfPages ? string.Empty : "nhs_Disabled"));            
            
            html.AppendLine("</div>");
            return html.ToString().ToMvcHtmlString();
        }

        private static string GeneratePageLink(int pageNumber, string linkUrl, int currentPage, bool lastPage)
        {
            if (pageNumber == currentPage)
            {
                return
                    String.Format(linkUrl.Contains("?")
                                      ? "<a class=\"nhs_Page nhs_Active " + (lastPage ? "nhs_lastPage" : "") +
                                        "\" data-page=\"{1}\" href=\"{0}&page={1}\">{1}</a>"
                                      : "<a class=\"nhs_Page nhs_Active " + (lastPage ? "nhs_lastPage" : "") +
                                        "\" data-page=\"{1}\" href=\"{0}/page-{1}\">{1}</a>", linkUrl, pageNumber);
            }
            return
                String.Format(
                    linkUrl.Contains("?")
                        ? "<a class=\"nhs_Page " + (lastPage ? "nhs_lastPage" : "") +
                          "\" data-page=\"{1}\" href=\"{0}&page={1}\">{1}</a>"
                        : "<a class=\"nhs_Page " + (lastPage ? "nhs_lastPage" : "") +
                          "\" data-page=\"{1}\" href=\"{0}/page-{1}\">{1}</a>", linkUrl, pageNumber);
        }

        public static void StaticMapUrl(this CommunityHomeResultsViewModel model, SearchParams searchParams, string size, string icon = "", string color = "")
        {
            var usedZoom = false;
            var mapPoints = model.Map.CenterLat.ToString(new NumberFormatInfo()) + "," +
                            model.Map.CenterLng.ToString(new NumberFormatInfo());
            if (!string.IsNullOrEmpty(model.CommunityName) && (model.CommunityResults.Any() || model.HomeResults.Any()))
            {
                var arrayCoord = model.CommunityResults.Any()
                    ? model.CommunityResults.DistinctBy(item => string.Format("{0},{1}", item.Lat, item.Lng))
                        .OrderBy(k => k.Id)
                        .Select(
                            item => string.Format("{0},{1}", item.Lat, item.Lng))
                    : model.HomeResults.DistinctBy(item => string.Format("{0},{1}", item.Lat, item.Lng))
                        .OrderBy(k => k.CommId)
                        .Select(item => string.Format("{0},{1}", item.Lat, item.Lng));
                mapPoints = string.Join("|", arrayCoord);
            }
            else
                usedZoom = true;

            model.GoogleStaticMapUlr = GoogleHelperMaps.StaticMapUrl(mapPoints, size, usedZoom, model.Map.ZoomLevel, icon, color);
        }


        public static MvcHtmlString CommunitCardThumbnail(this HtmlHelper helper, CommunityMapCard model)
        {
            var thumbnail = model.Image;
            if (model.IsValidImage())
            {
                thumbnail = thumbnail.Replace(ImageSizes.Small, ImageSizes.HomeThumb)
                                     .Replace(ImageSizes.Results, ImageSizes.CommDetailThumb);
            }
            else
            {
                thumbnail = Resources.GlobalResources14.Default.images.no_photo.no_photos_180x120_png;
            }

            return helper.NhsImage(thumbnail, new ResizeCommands { MaxWidth = 240, MaxHeight = 180 }, "", false);
        }

        public static MvcHtmlString CommunitCardLink(this HtmlHelper helper, CommunityMapCard model, string name,
                                                     object parameters)
        {
            if (model.IsBasic || model.IsBasicListing)
            {
                var urlHelper =
                    StringHelper.RemoveSpecialCharacters(
                        String.Format("{0}-{1}-{2}-{3}", model.Name, model.City, model.St, model.Zip).Trim())
                                .Replace(" ", "-")
                                .Replace("--", "-");

                var actionResult = model.IsBasic
                                       ? NhsMvc.BasicCommunityDetail.Show(model.Id.ToType<int>(), urlHelper)
                                       : NhsMvc.BasicHomeDetail.Show(model.Id.ToType<int>(), urlHelper);


                return helper.NhsActionLink(name, actionResult, parameters);
            }

            

            return helper.NhsLink(name, RedirectionHelper.GetCommunityDetailPage(), model.ToCommunityDetail(), parameters);
        }

        public static bool IsValidImage(this CommunityMapCard model)
        {
            return model.Image.IsValidImage();

        }

        public static MvcHtmlString CommunityImage(this HtmlHelper helper, CommunityItemViewModel model)
        {
            string altText1;
            if (PartnerLayoutHelper.GetPartnerLayoutConfig().IsBrandPartnerMove)
            {
                altText1 = "house for sale in " + model.Name.Trim() + " by " + model.Brand.Name.Trim();
            }
            else
            {                
                altText1 = model.Name.Trim() + " " + LanguageHelper.By + " " + model.Brand.Name.Trim() + " " + LanguageHelper.In + " " + model.MarketName.Trim() + " " + model.StateName.Trim();
            }

            return helper.NhsImage(model.ValidateCommResThumb1(), model.GetCommResThumb1Size(), altText1, false);
        }

        public static MvcHtmlString AsyncCommunityImage(this HtmlHelper helper, CommunityItemViewModel model)
        {
            string altText1;
        
            if (PartnerLayoutHelper.GetPartnerLayoutConfig().IsBrandPartnerMove)
                altText1 = "house for sale in " + model.Name.Trim() + " by " + (model.Brand != null ? model.Brand.Name.Trim(): string.Empty);
            else
                altText1 = model.Name.Trim() + " by " + model.Brand.Name.Trim() + " in " + model.MarketName.Trim() + " " + model.StateName.Trim();

            return helper.AsyncNhsImage(model.ValidateCommResThumb1(), model.GetCommResThumb1Size(), altText1, false, true, true);
        }

        public static MvcHtmlString AsyncCommunityImageMove(this HtmlHelper helper, CommunityItemViewModel model)
        {
            var altText2 = "homes in " + model.Name.Trim() + " by " + model.Brand.Name.Trim();
            return helper.AsyncNhsImage(model.ValidateCommResThumb2(), model.GetCommResThumb2Size(), altText2, false, true, true);
        }

        public static MvcHtmlString CommunityImageMove(this HtmlHelper helper, CommunityItemViewModel model)
        {
            var altText2 = "homes in " + model.Name.Trim() + " by " + model.Brand.Name.Trim();
            return helper.NhsImage(model.ValidateCommResThumb2(), model.GetCommResThumb2Size(), altText2, false);
        }

        public static MvcHtmlString LogEventWithParameters(this HtmlHelper helper, string eventCode,
            string parameterName, string parameters, int marketId)
        {
            var jsCode = "";
            if (!string.IsNullOrEmpty(parameters))
            {
                jsCode =
                    string.Format(
                        "setTimeout(function() {{ logger.logEventWithParameters('{0}', {{{1}: '{2}', marketId: {3}, refer: '{4}', partnerId: {5} , async: false }}); }}, 2000);",
                        eventCode, parameterName, parameters, marketId, UserSession.Refer, NhsRoute.PartnerId);
            }
            return jsCode.ToMvcHtmlString();
        }
    }
}
