﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Bdx.Web.Api.Objects;
using Bdx.Web.Api.Objects.Constants;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Utility.Html;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;
using Resources;
using HomeStatusType = Nhs.Search.Objects.Constants.HomeStatusType;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class PdfBrochureController : ApplicationController
    {
        private readonly ICommunityService _communityService;
        private readonly IListingService _listingService;
        private readonly IUserProfileService _userProfileService;
        private readonly IApiService _apiService;
        private readonly IPartnerService _partnerService;

        public PdfBrochureController(IPartnerService partnerService,ICommunityService communityService, IListingService listingService, IUserProfileService userProfileService, IApiService apiService)
        {
            _communityService = communityService;
            _listingService = listingService;
            _userProfileService = userProfileService;
            _apiService = apiService;
            _partnerService = partnerService;
        }

        #region PdfBrochure

        // GET: /PdfBrochure/
        public virtual ActionResult Index()
        {
            var model = GetPdfBrochureViewModel(RouteParams.Email.Value<string>());
            if (Request.QueryString["logevent"] == null)
            {
                var planId = RouteParams.PlanId.Value<Int32>();
                var specId = RouteParams.SpecId.Value<Int32>();
                var communityId = RouteParams.CommunityId.Value<Int32>();

                Community community = null;

                if (!specId.Equals(0) || !communityId.Equals(0) || !planId.Equals(0))
                {
                    if (!communityId.Equals(0))
                    {
                        community = _communityService.GetCommunity(communityId);
                        GetCommunityInformation(model, community);
                    }
                    else if (!specId.Equals(0))
                    {
                        var spec = _listingService.GetSpec(specId);
                        community = _communityService.GetCommunity(spec.CommunityId);
                        GetHomeInformation(model, spec.Plan, spec);
                    }
                    else if (!planId.Equals(0))
                    {
                        var plan = _listingService.GetPlan(planId);
                        community = _communityService.GetCommunity(plan.CommunityId);
                        GetHomeInformation(model, plan, null);
                    }

                    GetCommunityGlobalInformation(model, community);

                    var primaryImage = model.IsPageCommDetail
                        ? model.ImagesCommunityGallery.FirstOrDefault()
                        : model.ImagesHomeGallery.FirstOrDefault();

                    model.ImagePath = primaryImage != null
                        ? Configuration.IRSDomain +
                          primaryImage.Url.Replace("//", "/").Replace(ImageSizes.CommDetailThumb, ImageSizes.CommDetailMain).Replace(ImageSizes.HomeThumb, ImageSizes.HomeMain)
                        : GlobalResources14.Default.images.no_photo.no_photos_460x307_png;
                }
            }
            return View(NhsMvc.Default.Views.PdfBrochure.Index, model);
        }
        //
        // GET: /brochure/preview
        public virtual ActionResult Preview()
        {
            var model = GetPdfBrochureViewModel(UserSession.UserProfile.Email);
            model.CommunityName = "Lost Creek at Falcon Pointe";
            model.BuilderLogo = GlobalResourcesMvc.Pro.images.preview._1314605_MED_gif;
            model.ImagePath = GlobalResourcesMvc.Pro.images.preview.cdmain_8445530_jpg;
            return View(NhsMvc.Default.Views.PdfBrochure.Preview.Index, model);
        }

        private PdfBrochureViewModel GetPdfBrochureViewModel(string agentEmailAddress)
        {
            var model = new PdfBrochureViewModel();
            model.Logo = PartnerLogo();
            if (Request.QueryString["logevent"] == null)
            {
                model.PreviewMode = false;
                if (NhsRoute.IsBrandPartnerNhsPro)
                {
                    var agent = _userProfileService.GetUserByLogonName(agentEmailAddress, NhsRoute.PartnerId);

                    if (agent != null)
                    {
                        var agentBrochureTemplate = agent.AgentBrochureTemplates.FirstOrDefault();

                        if (agentBrochureTemplate != null)
                        {
                            model.AgentEmailAddress = agentEmailAddress;

                            model.AgentName = string.Format("{0} {1}", agentBrochureTemplate.FirstName, agentBrochureTemplate.LastName);
                            model.AgencyLogo = agentBrochureTemplate.AgencyLogo;
                            model.AgentPhoto = agentBrochureTemplate.ImageUrl;
                            model.AgentCity = agentBrochureTemplate.City;
                            model.AgentState = agentBrochureTemplate.State;
                            model.AgencyName = agentBrochureTemplate.AgencyName;
                            model.AgentPhoneNumber = (!string.IsNullOrEmpty(agentBrochureTemplate.OfficePhone))
                                                         ? agentBrochureTemplate.OfficePhone
                                                         : ((!string.IsNullOrEmpty(agentBrochureTemplate.MobilePhone))
                                                                ? agentBrochureTemplate.MobilePhone
                                                                : string.Empty);
                            model.AgentLicenseNumber = agent.RealStateLicense;
                            model.StateAbbr = agentBrochureTemplate.State;

                        }
                    }

                }
            }
            return model;
        }

        private string PartnerLogo()
        {
            string logo = string.Empty;
            var partnerInfo = _partnerService.GetPartner(NhsRoute.PartnerId);
            logo = partnerInfo.PartnerLogo.Replace("[resource:]", Configuration.ResourceDomain);
            return logo;
        }

        private void GetCommunityInformation(PdfBrochureViewModel model, Community community)
        {
            model.IsPageCommDetail = true;
            model.PriceDisplay = FormattedPrice(community.PriceHigh, community.PriceLow, community.BCType);
            model.DisplaySqFeet = FormattedSqFeet(community.SqFtLow, community.SqFtHigh);
            model.BedroomRange = community.MinBedroom + "-" + community.MaxBedroom;

            model.Description = community.CommunityDescription;

            var resultsView = GetCommunityHomeResults(community);
            resultsView = resultsView.Where(p => p.Price.ToType<decimal>() > 0);

            #region Move In Ready Homes

            var extendedHomeResults = resultsView.Where(p => p.Status == "A");
            if (extendedHomeResults.Any())
            {
                model.HaveQuickMoveIn = true;
                model.HomesAvailable.Add(new HomesAvailable
                {
                    HomeResultPages = extendedHomeResults.ToRows(5),
                    Tittle = LanguageHelper.MoveInReadyHomes,
                    MarketName = community.Market.MarketName
                });

            }

            #endregion

            #region Under Construction Homes

            extendedHomeResults = resultsView.Where(p => p.Status == "UC");
            if (extendedHomeResults.Any())
            {
                model.HaveUnderConstruction = true;
                model.HomesAvailable.Add(new HomesAvailable
                {
                    HomeResultPages = extendedHomeResults.ToRows(5),
                    Tittle = LanguageHelper.UnderConstructionHomes,
                    IsUnderConstruction = true,
                    MarketName = community.Market.MarketName
                });
            }

            #endregion

            #region Ready To Build Homes
            extendedHomeResults = resultsView.Where(p => p.Status == "R");
            if (extendedHomeResults.Any())
            {
                model.HaveReadyToBuild = true;
                model.HomesAvailable.Add(new HomesAvailable
                {
                    HomeResultPages = extendedHomeResults.ToRows(5),
                    Tittle = LanguageHelper.ReadyToBuildHomes,
                    IsReadyToBuild = true,
                    MarketName = community.Market.MarketName
                });
            }

            #endregion

            var useTrackingNumber = !string.IsNullOrEmpty(community.TrackingPhoneNumber);

            model.PhoneNumber = useTrackingNumber ? community.TrackingPhoneNumber : community.PhoneNumber;
            model.DrivingDirections = FormattedText(community.SalesOffice.DrivingDirections) ?? string.Empty;
        }

        private void GetHomeInformation(PdfBrochureViewModel model, IPlan plan, ISpec spec)
        {
            var isPlan = spec == null;
            model.IsPlan = isPlan;

            model.PriceDisplay = (isPlan ? plan.Price : spec.Price).ToString("$###,####");
            model.DisplaySqFeet = string.Format("{0:###,###}", (isPlan ? plan.SqFt : spec.SqFt) ?? 0);

            if ((isPlan ? plan.Bedrooms : spec.Bedrooms) > 0)
                model.BedroomRange = (isPlan ? plan.Bedrooms : spec.Bedrooms).ToString();

            if ((isPlan ? plan.Bathrooms : spec.Bathrooms) > 0)
                model.BathroomRange = ListingHelper.ComputeBathrooms((isPlan ? plan.Bathrooms : spec.Bathrooms) ?? 0, (isPlan ? plan.HalfBaths : spec.HalfBaths) ?? 0);

            if ((isPlan ? plan.Garages : spec.Garages) > 0.0m)
                model.GarageRange = (isPlan ? plan.Garages : spec.Garages).ToString();

            model.ImagesHomeGallery = _listingService.GetMediaPlayerObjects(plan, spec, false, true).Where(p => p.Type == MediaPlayerObjectTypes.Image).Take(6);

            var listingServiceHelper = new ListingServiceHelper { UseHub = _communityService.UseHub };
            model.Description = isPlan
                                    ? string.IsNullOrEmpty(plan.Description)
                                          ? listingServiceHelper.GetAutoDescriptionForHome(plan, null)
                                          : ParseHTML.StripHTML(plan.Description).Replace("\r", string.Empty).Replace("\n", string.Empty)
                                    : string.IsNullOrEmpty(spec.Description)
                                          ? listingServiceHelper.GetAutoDescriptionForHome(spec.Plan, spec)
                                          : ParseHTML.StripHTML(spec.Description).Replace("\r", string.Empty).Replace("\n", string.Empty);
            model.HomeStatus = isPlan ? (int)HomeStatusType.ReadyToBuild : spec.HomeStatus;
            model.Stories = isPlan ? plan.Stories.ToType<int>() : spec.Stories.ToType<int>();

            model.FloorPlanImages = isPlan ? _listingService.GetFloorPlanImagesForPlan(plan.PlanId) : _listingService.GetFloorPlanImagesForSpec(plan, spec);
            model.DrivingDirections = FormattedText(plan.Community.SalesOffice.DrivingDirections) ?? string.Empty;
            model.PhoneNumber = !string.IsNullOrEmpty(plan.Community.TrackingPhoneNumber) ? plan.Community.TrackingPhoneNumber : _communityService.GetTollFreeNumber(NhsRoute.PartnerId, plan.Community.CommunityId,
                                                                          plan.Community.BuilderId);
            if (string.IsNullOrEmpty(model.PhoneNumber))
                model.PhoneNumber = plan.Community.PhoneNumber;
            model.HomeTitle = isPlan ? plan.PlanName : spec.PlanName;
        }

        private void GetCommunityGlobalInformation(PdfBrochureViewModel model, Community community)
        {
            model.AllNewHomesCount = community.HomeCount.ToType<int>();
            model.CommunityName = community.CommunityName;
            model.CommunityId = community.CommunityId;
            model.MarketId = community.MarketId;
            model.BuilderId = community.BuilderId;
            //BEGIN-FIX: 74994
            model.Address1 = community.SalesOffice.Address1;
            model.Address2 = community.SalesOffice.Address2;
            model.ZipCode = community.SalesOffice.ZipCode;
            model.CommunityCity = community.SalesOffice.City;
            model.CommunityState = community.State.StateName;
            model.StateAbbr = community.State.StateAbbr;
            //END-FIX: 74994            
            model.CityInformation = GetCityInformation(community.City, community.State.StateAbbr, community.Market.MarketName);
            model.Schools = _communityService.GetSchools(NhsRoute.PartnerId, community.CommunityId);
            model.BuilderLogo = community.Brand.LogoMedium;
            model.BuilderLogoSmall = community.Brand.LogoSmall;
            model.BuilderUrl = !string.IsNullOrEmpty(community.Builder.Url)
                                   ? community.Builder.Url
                                   : (!string.IsNullOrEmpty(community.Builder.ParentBuilder.Url)
                                          ? community.Builder.ParentBuilder.Url
                                          : string.Empty);
            model.BuilderName = !string.IsNullOrEmpty(community.Builder.BuilderName)
                                    ? community.Builder.BuilderName
                                    : (!string.IsNullOrEmpty(community.Builder.ParentBuilder.BuilderName)
                                           ? community.Builder.ParentBuilder.BuilderName
                                           : string.Empty);

            model.Amenities = _communityService.GetAmenities(community.IAmenities, community.IOpenAmenities);
            model.GreenPrograms = _communityService.GetCommunityGreenPrograms(community.CommunityId).Take(2).ToList();
            model.Promotions = _communityService.GetCommunityPromotions(community.CommunityId, community.BuilderId).Take(3).ToList();
            model.Utilities = _communityService.GetCommunityUtilities(community.CommunityId, NhsRoute.PartnerId);
            model.Latitude = (double)community.Latitude;
            model.Longitude = (double)community.Longitude;
            if (NhsRoute.IsBrandPartnerNhsPro)
            {
                model.HoursOfOperation = community.SalesOffice.HoursOfOperation != null
                                         ? community.SalesOffice.HoursOfOperation.ToType<string>() : "";
            }
            else
                model.HoursOfOperation = community.SalesOffice.HoursOfOperation != null
                                             ? community.SalesOffice.HoursOfOperation.ToType<string>().Replace("<br />", "   ").Replace("<br/>", "   ") : "";

            model.SalesAgentName = !string.IsNullOrEmpty(community.SalesOffice.SalesAgent)
               ? community.SalesOffice.SalesAgent
               : string.Empty;

            model.SalesEmailAddress = !string.IsNullOrEmpty(community.SalesOffice.EmailAddress)
                ? community.SalesOffice.EmailAddress
                : string.Empty;

            model.ImagesCommunityGallery = _communityService.GetMediaPlayerObjects(community.CommunityId, false, true).Take(9);
            model.CommunityDescription = community.CommunityDescription;
            var imgMap = _communityService.GetBuilderMap(community.AllImages);
            if (imgMap != null)
                model.BuilderMapUrl = imgMap.ImagePath + imgMap.ImageTypeCode + "_" + imgMap.ImageName;


            var icon = Configuration.ResourceDomainPublic + "/globalresources14/default/images/icons/map_poi_blue.png";
            model.UrlMap = GoogleHelperMaps.StaticMapUrl(string.Format("{0},{1}", model.Latitude, model.Longitude), "300x430", true, 15, icon, "");

        }

        public virtual JsonResult GetBuilderMapUrl(string size, string mapPoints)
        {
            var icon = Configuration.ResourceDomainPublic + "/globalresources14/default/images/icons/map_poi_blue.png";
            var zoom = NhsRoute.IsBrandPartnerNhsPro ? 8 : 13;
            var url = GoogleHelperMaps.StaticMapUrl(mapPoints, "630x" + size, true, zoom, icon);
            return Json(url, JsonRequestBehavior.AllowGet);
        }

        private IEnumerable<ApiHomeResultV2> GetCommunityHomeResults(Community community)
        {
            //var parameters = new PropertySearchParams
            //{
            //    CommunityId = community.CommunityId,
            //    BuilderId = community.BuilderId,
            //    PartnerId = NhsRoute.PartnerId,
            //    SortOrder = SortOrder.Status,
            //    HomeStatus = homeStatus,
            //    ListingTypeFlag = community.ListingTypeFlag,
            //    MarketId = community.MarketId
            //};

            var parameters = new SearchParams
            {
                CommId = community.CommunityId,
                PartnerId = NhsRoute.PartnerId,
                SortBy = SortBy.Comm,
                BuilderId = community.BuilderId,
                MarketId = community.MarketId,
                WebApiSearchType = WebApiSearchType.Exact,
                PageSize = 10000,
                PageNumber = 1
            };

            var results = _apiService.GetResultsWebApi<ApiHomeResultV2>(parameters, SearchResultsPageType.HomeResults);
            return results.Result ?? new List<ApiHomeResultV2>();
        }

        public MvcHtmlString GetCityInformation(string city, string state, string market)
        {
            // Ticket 69571, the city information has to be taken from Search Result Page content store
            var seoContentManager = new SeoContentManager
            {
                City = city.Replace(" ", String.Empty),
                State = state,
                Market = market,
                ContentTags = new List<ContentTag>
                {
                    new ContentTag {TagKey = ContentTagKey.MarketName, TagValue = market},
                    new ContentTag {TagKey = ContentTagKey.CityName, TagValue = city},
                    new ContentTag {TagKey = ContentTagKey.City, TagValue = city},
                    new ContentTag {TagKey = ContentTagKey.StateName, TagValue = state}
                }
            };

            // Set the content tags, in the xml they are meta tags as [CityName] that should be replace by its value

            var htmlText = seoContentManager.GetHtmlBlock(SeoTemplateType.Nhs_Market_City_Srp, "footer", false);

            return MvcHtmlString.Create(htmlText);
        }

        private string FormattedPrice(decimal priceHigh, decimal priceLow, string bcType)
        {
            if (!ShowCommunityPriceDetailsInfo(bcType)) return string.Empty;
            return StringHelper.PrettyPrintRange(priceLow, priceHigh, "C0", LanguageHelper.From.ToTitleCase());
        }

        private static bool ShowCommunityPriceDetailsInfo(string bcType)
        {
            return bcType != "X" && bcType != "C";
        }

        private string FormattedSqFeet(int sqFtLow, int sqFtHigh)
        {
            if (sqFtLow == 0 && sqFtHigh == 0) return string.Empty;
            return sqFtLow == sqFtHigh ? string.Format("{0}", sqFtHigh) : string.Format("{0} - {1}", sqFtLow, sqFtHigh);
        }

        private string FormattedText(string text)
        {
            if (string.IsNullOrEmpty(text)) return string.Empty;
            var descriptionText = ParseHTML.StripHTML(text);
            return descriptionText.Replace("\r", string.Empty).Replace("\n", string.Empty);
        }
        #endregion

        #region Community/Home Results

        public virtual ActionResult CommunityResultsHtml()
        {
            var model = new BaseViewModel();
            return View(NhsMvc.Default.Views.PdfBrochure.CommunityResultsPdf.Show, model);
        }

        public virtual ActionResult HomeResultsHtml()
        {
            var model = new BaseViewModel();
            return View(NhsMvc.Default.Views.PdfBrochure.HomeResultsPdf.Show, model);
        }

        #endregion
    }
}
