﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Content;
using Nhs.Library.Helpers.Property;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects.Constants;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Filters;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class AdvancedSearchController: ApplicationController
    {
        #region Member variables
        private readonly IPathMapper _pathMapper;
        private readonly IMarketService _marketService;
        private readonly IStateService _stateService;
        private readonly ICommunityService _communityService;
        private readonly ILookupService _lookupService;
        private readonly IPartnerService _partnerService;
        #endregion

        public AdvancedSearchController(IPartnerService partnerService, IPathMapper pathMapper, IMarketService marketService, ICommunityService commnunityService, ILookupService lookupService, IStateService stateService)
            : base(pathMapper)
        {
            _partnerService = partnerService;
            _pathMapper = pathMapper;
            _marketService = marketService;
            _communityService = commnunityService;
            _lookupService = lookupService;
            _stateService = stateService;
        }

        //
        // GET: /AdvancedSearch/

        public virtual ActionResult Show() 
        {           
            AdvancedSearchViewModel model = GetAdvancedSearchModel();
            if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
                AddCanonicalTag(model);
// ReSharper disable Asp.NotResolved
            return View(model);
// ReSharper restore Asp.NotResolved
        }        
        
        public virtual ActionResult ValidateRedirect(AdvancedSearchViewModel model)
        {            
            var paramz = new List<RouteParam>();
            var uselocation = false;
            var commSearch = model.CommSearch;
            

            if (commSearch)
            {
                UserSession.PropertySearchParameters.MinLatitude = 0;
                UserSession.PropertySearchParameters.MinLongitude = 0;
                UserSession.PropertySearchParameters.MaxLatitude = 0;
                UserSession.PropertySearchParameters.MaxLongitude = 0;

                BuildSearchParameters(true, "", model);

                if (CommonUtils.IsZip(model.SearchText.Trim()))
                {
                    uselocation = true;
                    var routeParameters = UserSession.PropertySearchParameters.GetRouteParams(RouteParamType.QueryString);
                    paramz.AddRange(routeParameters.Where(param => !param.Name.ToString().ToLower().Equals(UrlConst.State)));
                    paramz.Add(new RouteParam(RouteParams.SearchText, model.SearchText, RouteParamType.QueryString, true, true));
                }
                else if( (UserSession.PropertySearchParameters.County == string.Empty && UserSession.PropertySearchParameters.City == string.Empty && UserSession.PropertySearchParameters.MarketId != 0) ||
                    (UserSession.PropertySearchParameters.City != string.Empty && UserSession.PropertySearchParameters.MarketId != 0) ||
                    (UserSession.PropertySearchParameters.County != string.Empty && UserSession.PropertySearchParameters.MarketId != 0)
                    )
                {
                    var routeParameters = UserSession.PropertySearchParameters.GetRouteParams(RouteParamType.QueryString);
                    paramz.AddRange(routeParameters.Where(param => !param.Name.ToString().ToLower().Equals(UrlConst.State)));
                    paramz.Add(new RouteParam(RouteParams.SearchType, SearchTypeSource.AdvancedSearchCommunityResults, RouteParamType.QueryString, true, true));
                }                
                else
                {
                    var searchText = model.SearchText.Trim();
                    uselocation = true;
                    if (model.SearchText.Trim().Contains(","))
                    {
                        searchText = model.SearchText.Trim().Substring(0, model.SearchText.Trim().IndexOf(",",StringComparison.CurrentCultureIgnoreCase)).Trim();
                    }
                    var routeParameters = UserSession.PropertySearchParameters.GetRouteParams(RouteParamType.QueryString);
                    paramz.AddRange(routeParameters);

                    if (!string.IsNullOrEmpty(model.SearchType) && searchText.IndexOf(",") != -1 )
                        paramz.Add(new RouteParam(RouteParams.SearchType, model.SearchType));

                    paramz.Add(new RouteParam(RouteParams.SearchText, searchText, RouteParamType.QueryString, true, true));
                }

                UserSession.SearchType = SearchTypeSource.AdvancedSearchCommunityResults;
                
            }
            else
            {
                BuildSearchParameters(false, "", model);

                var routeParameters = UserSession.PropertySearchParameters.GetRouteParams(RouteParamType.QueryString);
                paramz.AddRange(routeParameters.Where(param => !param.Name.ToString().ToLower().Equals(UrlConst.State)));

                if (CommonUtils.IsZip(model.SearchText.Trim()))
                {
                    uselocation = true;
                    paramz.Add(new RouteParam(RouteParams.ToPage, Pages.HomeResults, RouteParamType.QueryString, true, true));
                    paramz.Add(new RouteParam(RouteParams.SearchText, model.SearchText, RouteParamType.QueryString, true, true));
                }
                else
                {                    
                    if(!string.IsNullOrEmpty(UserSession.PersonalCookie.City))
                        paramz.Add(new RouteParam(RouteParams.City, UserSession.PersonalCookie.City, RouteParamType.QueryString, true, true));
                }                

                UserSession.SearchType = SearchTypeSource.AdvancedSearchHomeResults;
            }

            if (model.Promo == PromotionType.Consumer || 
                (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>() && model.RblOfferAll == false))
            {
                paramz.Add(new RouteParam(RouteParams.HotDeals, "true", RouteParamType.QueryString, true, true));
            }

            /*if (model.RblOfferAll == false)
            {
                paramz.Add(new Param(NhsLinkParams.HotDeals, "true", UrlParamType.QueryString, true, true));
            }*/

            if (uselocation)
                return Redirect(paramz.ToUrl(Pages.LocationHandler).ToLower());
            
            if(commSearch)
                return Redirect(paramz.ToUrl(Pages.CommunityResults).ToLower());
            
            return Redirect(paramz.ToUrl(Pages.HomeResults).ToLower());
        }

        private AdvancedSearchViewModel GetAdvancedSearchModel()
        {
            const int dropBed = -1;
            const int dropBath = -1;
            const int dropGarage = -1;
            const int sqFtMin = -1;
            const int dropLiving = -1;
            const int dropHome = -1;
            const int dropStories = -1;
            const int dropMaster = -1;
            var schoolList = BindSchoolsDropDown(UserSession.PersonalCookie.MarketId);
            schoolList.Insert(0, new SchoolDistrict { DistrictId = -1, DistrictName = LanguageHelper.AllSchoolDistricts } );

            var commList = BindCommunityDropDown(UserSession.PersonalCookie.MarketId);
            commList.Insert(0, new Community { CommunityId = -1, CommunityName = LanguageHelper.AllCommunities });
            
            var builderList = BindBuilderDropDown(UserSession.PersonalCookie.MarketId);
            builderList.Insert(0, new Brand { BrandId = -1, BrandName = LanguageHelper.AllBuilders });
            
            var priceLow = UserSession.PersonalCookie.PriceLow;
            var priceHigh = UserSession.PersonalCookie.PriceHigh;
            var priceRange = _lookupService.GetCommonListItems(CommonListItem.PriceRange, LanguageHelper.Any);
            var bedList = _lookupService.GetCommonListItems(CommonListItem.Bedrooms, LanguageHelper.Any);
            var bathList = _lookupService.GetCommonListItems(CommonListItem.Bathrooms, LanguageHelper.Any);
            var garageList = _lookupService.GetCommonListItems(CommonListItem.Garage, LanguageHelper.Any);
            var livingList = _lookupService.GetCommonListItems(CommonListItem.LivingArea, LanguageHelper.Any);
            var homeList = _lookupService.GetCommonListItems(CommonListItem.HomeType, LanguageHelper.Any);
            var storiesList = _lookupService.GetCommonListItems(CommonListItem.Stories, LanguageHelper.Any);
            var masterList = _lookupService.GetCommonListItems(CommonListItem.MasterBed, LanguageHelper.Any);
            
            

            var marketId = UserSession.PersonalCookie.MarketId;
            var searchText = UserSession.PersonalCookie.SearchText ?? string.Empty;
            var state = UserSession.PersonalCookie.State;
            var marketName = _marketService.GetMarketName(NhsRoute.PartnerId, marketId);

            // Sets location field
            var txtLoc = (string.IsNullOrWhiteSpace(searchText) == false ? searchText : marketName);
            if ((searchText.ToLower() != marketName.ToLower()) & (!CommonUtils.IsZip(searchText)))
            {
                var cityMatch = false;
                var cities = _marketService.GetCitiesForMarket(NhsRoute.PartnerId, marketId);
                
                foreach (var ct in cities)
                {
                    var city = ct.CityName;
                    if (city.ToLower() == searchText.ToLower())
                    {
                        txtLoc = city;
                        cityMatch = true;
                        break;
                    }
                }

                if (cityMatch != true)
                    txtLoc = marketName;
            }
            if (!CommonUtils.IsZip(txtLoc) && txtLoc != string.Empty)   // Sets state
                txtLoc += @", " + state;
                                    

            var viewModel = new AdvancedSearchViewModel
            {
                SearchText = txtLoc,
                PriceLow = UserSession.PersonalCookie.PriceLow,
                PriceHigh = UserSession.PersonalCookie.PriceHigh,
                SchoolId = Convert.ToString(UserSession.PropertySearchParameters.SchoolDistrictId),
                CommunityId = Convert.ToString(UserSession.PropertySearchParameters.CommunityId),
                BrandId = Convert.ToString(UserSession.PropertySearchParameters.BrandId),
                CblGolf = UserSession.PropertySearchParameters.GolfCourse,
                CblPool = UserSession.PropertySearchParameters.Pool,
                CblGreen = UserSession.PropertySearchParameters.GreenProgram,
                CblNature = UserSession.PropertySearchParameters.GreenNatureAreas,
                CblViews = UserSession.PropertySearchParameters.Views,
                CblWater = UserSession.PropertySearchParameters.Waterfront,
                CblSports = UserSession.PropertySearchParameters.SportsFacilities,
                CblParks = UserSession.PropertySearchParameters.Parks,
                CblAdultSenior = UserSession.PropertySearchParameters.Adult,
                TypeAheadUrl = (string.IsNullOrEmpty(NhsRoute.PartnerSiteUrl) ? "" : (NhsRoute.PartnerSiteUrl + "/")) +  NhsMvc.PartialViews.ActionNames.PartnerLocationsIndex,
                SchoolList = schoolList,
                CommunityList = commList,
                BuilderList = builderList,
                MasterBedDrop = -1,
                LowPriceRange = new SelectList(priceRange, "LookupValue", "LookupText", priceLow),
                HighPriceRange = new SelectList(priceRange, "LookupValue", "LookupText", priceHigh),
                BedList = new SelectList(bedList, "LookupValue", "LookupText", dropBed),
                BathList = new SelectList(bathList, "LookupValue", "LookupText", dropBath),
                GarageList = new SelectList(garageList, "LookupValue", "LookupText", dropGarage),
                LivingList = new SelectList(livingList, "LookupValue", "LookupText", dropLiving),
                HomeList = new SelectList(homeList, "LookupValue", "LookupText", dropHome),
                StoriesList = new SelectList(storiesList, "LookupValue", "LookupText", dropStories),
                MasterBedList = new SelectList(masterList, "LookupValue", "LookupText", dropMaster)
            };

            //Square Feet Facet
            if (viewModel.Globals.PartnerLayoutConfig.DoesPartnerHaveSquareFeetFacet)
            {
                viewModel.SqFtList = new SelectList(_lookupService.GetSquareFeetListItems(LanguageHelper.Any), "LookupValue", "LookupText", sqFtMin);
            }

            //// Register Meta Tags information
            var paramz = new List<ContentTag>();
            var metaReader = new MetaReader(_pathMapper);
            var metas = metaReader.GetMetaTagInformation(Pages.AdvancedSearch, paramz.ToDictionary(), PageHeaderSectionName.SeoMetaPages);
            OverrideDefaultMeta(viewModel, metas);
            
            base.SetupAdParameters(viewModel);
            return viewModel;
        }


        private IList<SchoolDistrict> BindSchoolsDropDown(int marketId)
        {
            var schools = new List<SchoolDistrict>();
            if (marketId != 0)
            {
                var market = _marketService.GetMarket(NhsRoute.PartnerId, marketId, true);
                schools.AddRange(market.SchoolDistricts);
            }

            return schools;
        }

        private IList<Community> BindCommunityDropDown(int marketId)
        {
            var comms = new List<Community>();
            if (marketId != 0)
            {
                var list = _communityService.GetCommunitiesByMarketId(marketId);
                comms.AddRange(list);
            }

            return comms;
        }
        

        private IList<Brand> BindBuilderDropDown(int marketId)
        {
            var builders = new List<Brand>();
            if (marketId != 0)
            {
                var market = _marketService.GetMarket(NhsRoute.PartnerId, marketId, true);
                builders.AddRange(market.Brands);
            }

            return builders;
        }

        private void BuildSearchParameters(bool searchCommunities, string locationFilter, AdvancedSearchViewModel model)
        {
            var marketId = 0;
            var cityOutput = string.Empty;            
            var stateName = string.Empty;
            var postalCode = string.Empty;
            var location = !string.IsNullOrEmpty(locationFilter) ? locationFilter : model.SearchText;

            if (UserSession.SearchParametersV2 != null)
                UserSession.SearchParametersV2.Init();
            
            UserSession.PropertySearchParameters.NhsClear();
            if (location.EndsWith("area", StringComparison.InvariantCultureIgnoreCase))
            {
                location = location.Substring(0, location.LastIndexOf("area", StringComparison.InvariantCultureIgnoreCase)).Trim();
            }

            // By ZIP
            if (CommonUtils.IsZip(location))
            {
                postalCode = location.Trim();
            }
            // By City, ST or County, ST
            else
            {                
                var commaPostion = location.IndexOf(",",StringComparison.CurrentCultureIgnoreCase);
                var locationName = location.Substring(0, commaPostion).Trim();
                stateName = location.Substring(commaPostion + 1).Trim();
                if (stateName.Length > 2)
                {                    
                    var states = _stateService.GetPartnerStates(NhsRoute.PartnerId);
                    foreach (var s in states)
                    {
                        if (s.StateName.ToLower() == stateName.ToLower())
                        {
                            stateName = s.StateAbbr;
                            break;
                        }
                    }
                }
                
                DataProvider.Current.GetMarketIdFromCity(locationName, stateName, false, ref marketId, ref cityOutput);
                
                if (marketId == 0) //try basic listing markets                    
                    DataProvider.Current.GetMarketIdFromCity(locationName, stateName, true, ref marketId, ref cityOutput);

                if (marketId == 0)
                {
                    // we couldn't find a valid market for the text entered, which means we're either searching for county
                    // or dummy text. Populate CountyNameFilter, Market and we're good to go!
                    UserSession.PropertySearchParameters.City = string.Empty;
                    UserSession.PropertySearchParameters.County = string.Empty;
                    if (locationName.ToLower().Contains("county"))
                    {
                        var countyOutput = locationName.Substring(0, locationName.ToLower().IndexOf("county",StringComparison.InvariantCultureIgnoreCase) - 1);
                        marketId = DataProvider.Current.GetMarketIdFromCounty(countyOutput, stateName);                        
                        if (marketId != 0)
                            UserSession.PropertySearchParameters.County = countyOutput;
                    }
                }
                else
                {
                    UserSession.PropertySearchParameters.City = cityOutput;
                    UserSession.PropertySearchParameters.County = string.Empty;
                }
            }

            UserSession.PropertySearchParameters.MarketName = string.Empty;
            UserSession.PropertySearchParameters.StateName = string.Empty;
            UserSession.PropertySearchParameters.State = stateName;
            UserSession.PropertySearchParameters.CityNameFilter = string.Empty;
            UserSession.PropertySearchParameters.State = stateName;
            UserSession.PropertySearchParameters.MarketId = marketId;
            UserSession.PropertySearchParameters.SchoolDistrictId = int.Parse(model.SchoolId == "-1" ? "0" : model.SchoolId);
            UserSession.PropertySearchParameters.CommunityId = int.Parse(model.CommunityId == "-1" ? "0" : model.CommunityId);
            UserSession.PropertySearchParameters.PostalCode = postalCode;
            //check when we dont need these params settings
            if (string.IsNullOrEmpty(locationFilter))
            {
                UserSession.PropertySearchParameters.Pool = model.CblPool;
                UserSession.PropertySearchParameters.GolfCourse = model.CblGolf;
                UserSession.PropertySearchParameters.GreenProgram = model.CblGreen;
                UserSession.PropertySearchParameters.GreenNatureAreas = model.CblNature;
                UserSession.PropertySearchParameters.Parks = model.CblParks;
                UserSession.PropertySearchParameters.Views = model.CblViews;
                UserSession.PropertySearchParameters.Waterfront = model.CblWater;
                UserSession.PropertySearchParameters.SportsFacilities = model.CblSports;
                UserSession.PropertySearchParameters.Adult = model.CblAdultSenior;

                UserSession.PropertySearchParameters.PriceLow = string.IsNullOrEmpty(Convert.ToString(model.PriceLow)) ? 0 : model.PriceLow;
                UserSession.PropertySearchParameters.PriceHigh = string.IsNullOrEmpty(Convert.ToString(model.PriceHigh)) ? 0 : model.PriceHigh;

                // Ensure price low is < price high.  
                if (UserSession.PropertySearchParameters.PriceLow > UserSession.PropertySearchParameters.PriceHigh && UserSession.PropertySearchParameters.PriceHigh != 0)
                {
                    var tmp = UserSession.PropertySearchParameters.PriceLow;
                    UserSession.PropertySearchParameters.PriceLow = UserSession.PropertySearchParameters.PriceHigh;
                    UserSession.PropertySearchParameters.PriceHigh = tmp;
                }

                UserSession.PropertySearchParameters.NumOfBeds = model.BedDrop == -1 ? 0 : model.BedDrop;
                UserSession.PropertySearchParameters.NumOfBaths = model.BathDrop == -1 ? 0 : model.BathDrop;
                UserSession.PropertySearchParameters.NumOfGarages = model.GarageDrop == -1 ? 0 : model.GarageDrop.ToType<decimal>();
                UserSession.PropertySearchParameters.NumOfLiving = model.LivingDrop == -1 ? 0 : model.LivingDrop;
                UserSession.PropertySearchParameters.HomeType = model.HomeDrop == "-1" ? string.Empty : model.HomeDrop;
                UserSession.PropertySearchParameters.Stories = model.StoriesDrop == -1 ? 0 : model.StoriesDrop;
                UserSession.PropertySearchParameters.MasterBedLocation = model.MasterBedDrop == -1 ? 0 : model.MasterBedDrop;
                UserSession.PropertySearchParameters.BrandId = model.BrandId == "-1" ? 0 : int.Parse(model.BrandId);

                
                UserSession.PropertySearchParameters.HomeStatus = 0;//(model.RblHomeAll ? 0 : (int)HomeStatusType.QuickMoveIn);

                if (NhsRoute.IsBrandPartnerNhsPro)
                {
                    UserSession.PropertySearchParameters.HomeStatus = (model.RblHomeAll ? 0 : (int)HomeStatusType.QuickMoveIn);
                }

                UserSession.PropertySearchParameters.QuickMoveIn = (model.RblHomeAll ? -1 : (int)HomeStatusType.QuickMoveIn);                

                UserSession.PropertySearchParameters.SqFtLow = (model.SqFtMin == int.Parse("-1") ? 0 : model.SqFtMin);
                UserSession.PropertySearchParameters.PromotionType = model.Promo;

                if (PartnerLayoutHelper.GetPartnerLayoutConfig().DoesPartnerHaveHotDealsLink)
                {
                    if (searchCommunities)
                    {
                        UserSession.PropertySearchParameters.SpecialOfferComm = model.RblOfferAll ? 0 : 1;
                    }
                    else
                    {
                        UserSession.PropertySearchParameters.SpecialOfferListing = model.RblOfferAll ? 0 : 1;
                    }
                }
                else
                {
                    if (UserSession.PropertySearchParameters.SpecialOfferComm == 1)
                    {
                        UserSession.PropertySearchParameters.SpecialOfferComm = 0;
                    }
                }
            }

            // Set personal cookie values.
            UserSession.PersonalCookie.State = stateName;
            UserSession.PersonalCookie.MarketId = marketId;
            UserSession.PersonalCookie.City = cityOutput;
            UserSession.PersonalCookie.PriceLow = string.IsNullOrEmpty(Convert.ToString(model.PriceLow)) ? 0 : model.PriceLow;
            UserSession.PersonalCookie.PriceHigh = string.IsNullOrEmpty(Convert.ToString(model.PriceHigh)) ? 0 : model.PriceHigh;
        }
    }
}
