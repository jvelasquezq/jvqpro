﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json;
using Nhs.Library.Business.Sso;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Sso;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using System.Collections.Generic;
using Nhs.Library.Web;
using Nhs.Library.Constants.Route;
using Nhs.Library.Extensions;
using System.Web.Helpers;
using System.Globalization;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Common;
using System.Text;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Web.UI.AppCode.Helper;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class SsoController : ApplicationController
    {
        private readonly ISsoService _ssoService;
        private readonly IPathMapper _pathMapper;
        private readonly IPartnerService _partnerService;

        public SsoController(IPathMapper pathMapper, ISsoService ssoService, IPartnerService partnerService)
            : base(pathMapper)
        {
            _pathMapper = pathMapper;
            _ssoService = ssoService;
            _partnerService = partnerService;
        }

        public virtual ActionResult StartSso()
        {
            //Creates the authn request
            var partner = _partnerService.GetPartner(NhsRoute.PartnerId);
            var successRequest = _ssoService.RequestLoginAtIdentityProvider(System.Web.HttpContext.Current.Response, NhsRoute.PartnerSiteUrl, partner.SsoDestionationUrl);
            //if the request was successful then it returns an EmptyResult so the user gets to the SP (service provider) login page, if something went wrong it goes to the homepage with signed out state
            if (successRequest)
                return new EmptyResult();
            return Redirect(Pages.EditAccountModal, new List<RouteParam>());
        }

        public virtual ActionResult AssertionConsumerService()
        {
            Trace.Write("Assertion Started" + Environment.NewLine);

            if (string.IsNullOrEmpty(NhsRoute.PartnerSiteUrl))
                Trace.Write("Site URL is empty for partner!" + Environment.NewLine);

            PartnerSamlAttributes attributes = SsoConfigReader.GetPartnerSamlAttributes(NhsRoute.PartnerSiteUrl, _pathMapper);

            Trace.Write("Received Partner SAML Attribs Successfully" + Environment.NewLine);

            if (System.Web.HttpContext.Current == null)
                Trace.Write("HttpContext.Current is NULL!" + Environment.NewLine);

            if (_ssoService == null)
                Trace.Write("_ssoService is NULL!" + Environment.NewLine);

            //Receives the SAML response
            NhsSamlReponse nhsSamlResponse = _ssoService.ReceiveSamlResponse(System.Web.HttpContext.Current.Request, NhsRoute.PartnerSiteUrl);

            if (nhsSamlResponse == null)
                Trace.Write("SAML Response is NULL!" + Environment.NewLine);

            if (nhsSamlResponse.SamlResponse == null)
                Trace.Write("SAML Response 2 is NULL!" + Environment.NewLine);

            // Check whether the SAML response indicates success or an error and process accordingly.
            if (nhsSamlResponse.SamlResponse.IsSuccess())
            {
                _ssoService.ProcessSuccessSamlResponse(nhsSamlResponse.SamlResponse, nhsSamlResponse.RelayState,
                                                       attributes,
                                                       NhsRoute.PartnerId, NhsRoute.PartnerSiteUrl,
                                                       NhsRoute.BrandPartnerId,
                                                       LanguageHelper.WrongPassword,
                                                       LanguageHelper.EmailDoNotExist,
                                                       LanguageHelper.UserAlreadyExist);
            }
            else
            {
                _ssoService.ProcessErrorSamlResponse(nhsSamlResponse.SamlResponse);
            }

            return (!string.IsNullOrEmpty(nhsSamlResponse.RelayState) &&
                    nhsSamlResponse.RelayState.Contains(Configuration.NewHomeSourceProfessionalSSODomain))
                       ? Redirect(nhsSamlResponse.RelayState)
                       : Redirect(Pages.Home, new List<RouteParam>());
        }

        public virtual ActionResult CreateAccountFromSsoCredentials(bool? subcribeNewsLetter)
        {
            var jsonData = UserSession.TempUserProfile;
            if (string.IsNullOrWhiteSpace(jsonData) == false)
            {
                var userProfile = JsonConvert.DeserializeObject<UserProfile>(jsonData);
                if (userProfile != null)
                {
                    var subscribeToNewsletter = _ssoService.VerifyPartnerAllowsWeeklyOptIn(NhsRoute.PartnerSiteUrl);
                    _ssoService.CreateUserProfile(userProfile, NhsRoute.BrandPartnerId, LanguageHelper.WrongPassword, LanguageHelper.EmailDoNotExist, LanguageHelper.UserAlreadyExist, subscribeToNewsletter);
                    UserSession.TempUserProfile = null;
                }
            }

            return Redirect(Pages.Home, new List<RouteParam>());
        }

        public virtual ActionResult ShowMredTermsOfService()
        {
            var partialUrl = NhsMvc.PartnerBrandGroupViews_88.Views.Sso.Mred.PartialTermsOfService;

            return Request.IsAjaxRequest()
                       ? PartialView(partialUrl) as ActionResult
                       : View(partialUrl);
        }

        public virtual ActionResult ShowEastBayTermsOfService()
        {
            var partialUrl = NhsMvc.PartnerBrandGroupViews_88.Views.Sso.EastBay.PartialTermsOfService;

            return Request.IsAjaxRequest()
                       ? PartialView(partialUrl) as ActionResult
                       : View(partialUrl);
        }

        /// <summary>
        /// Decrypts user information
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult Decrypt()
        {
            string tokenValue = RouteParams.Token.Value<string>();

            if (string.IsNullOrEmpty(tokenValue)) //REMAX Support (REMAX sends us token=)
                tokenValue = RouteParams.DataToken.Value<string>();

            try
            {
                var userProfile = this.ParseUrlAndGetUserProfile(tokenValue);
                var subscribeToNewsletter = _ssoService.VerifyPartnerAllowsWeeklyOptIn(NhsRoute.PartnerSiteUrl);
                _ssoService.CreateUserProfile(userProfile, NhsRoute.BrandPartnerId, LanguageHelper.WrongPassword, LanguageHelper.EmailDoNotExist, LanguageHelper.UserAlreadyExist, subscribeToNewsletter);

                return Redirect(Pages.Home, new List<RouteParam>());
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
                return new JsonResult { Data = ex.Message };
            }
        }

        /// <summary>
        /// Posts user info via querystring parameters and creates Profile 
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult PostUser(FormCollection form)
        {
            //PROCESS QUERYSTRING PARAMS HERE AND POPULATE userProfile
            try
            {
                var firstName = form[UrlConst.FirstName];
                var lastName = form[UrlConst.LastName];
                var license = form[UrlConst.License];
                var phone = form[UrlConst.Phone];
                var mobile = form[UrlConst.Mobile];
                var email = form[UrlConst.Email];
                var zip = form[UrlConst.Zip];
                var agencyName = form[UrlConst.AgencyName];
                var ssoId = form[UrlConst.AgentId];

                //Validate mandatory fields
                if (string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName) || string.IsNullOrEmpty(email) ||
                    string.IsNullOrEmpty(zip) || string.IsNullOrEmpty(ssoId) || string.IsNullOrEmpty(agencyName))
                {
                    throw new Exception("ERROR:  Required parameters missing");
                }

                var userProfile = new UserProfile()
                {
                    FirstName = firstName,
                    LastName = lastName,
                    LogonName = email,
                    RealStateLicense = license,
                    DayPhone = phone,
                    EveningPhone = mobile,
                    PostalCode = zip,
                    PartnerId = NhsRoute.PartnerId,
                    SsoUserId = ssoId,
                    AgencyName = agencyName
                };
                var subscribeToNewsletter = _ssoService.VerifyPartnerAllowsWeeklyOptIn(NhsRoute.PartnerSiteUrl);
                _ssoService.CreateUserProfile(userProfile, NhsRoute.BrandPartnerId, LanguageHelper.WrongPassword, LanguageHelper.EmailDoNotExist, LanguageHelper.UserAlreadyExist, subscribeToNewsletter);
                return Redirect(Pages.Home, new List<RouteParam>());
            }

            catch (Exception exception)
            {
                ErrorLogger.LogError(exception);
                return new JsonResult { Data = exception.Message };
            }
        }

        /// <summary>
        /// Partner uses this method to POST data along with hashed credentials
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public virtual ActionResult Authenticate()
        {
            try
            {
                var sessionToken = RouteParams.SessionToken.Value<string>(); //Hashed (partnerSitePassword + ":" + DateTime.UtcNow.ToString("yyyyMMddHH") + ":" + AssociateId) using hashtype e.g. hash("abc1234!:2013020516:99942931")
                var dataToken = RouteParams.DataToken.Value<string>(); //Base64-encoded User info
                var hashType = RouteParams.HashType.Value<string>(); //md5, sha1, sha256, sha384, sha512
                var dateTime = RouteParams.Dt.Value<string>();

                return ValidatePostData(sessionToken, dataToken, hashType, dateTime);
            }
            catch (Exception exception)
            {
                ErrorLogger.LogError(exception);
                return new JsonResult { Data = exception.Message };
            }
        }


        [HttpPost]
        public virtual ActionResult AuthenticateClientForm(FormCollection form)
        {
            try
            {
                var sessionToken = form[UrlConst.SessionToken];//Hashed (partnerSitePassword + ":" + DateTime.UtcNow.ToString("yyyyMMddHH") + ":" + AssociateId) using hashtype e.g. hash("abc1234!:2013020516:99942931")
                var dataToken = form[UrlConst.DataToken]; //Base64-encoded User info
                var hashType = form[UrlConst.HashType]; //md5, sha1, sha256, sha384, sha512
                var dateTime = form[UrlConst.DateTime];

                return ValidatePostData(sessionToken, dataToken, hashType, dateTime);
            }
            catch (Exception exception)
            {
                ErrorLogger.LogError(exception);
                return new JsonResult { Data = exception.Message };
            }
        }

        private ActionResult ValidatePostData(string sessionToken, string dataToken, string hashType, string dateTime)
        {
            #region VALIDATE
            if (string.IsNullOrEmpty(sessionToken))
                throw new Exception("ERROR: SessionToken Missing!");

            if (string.IsNullOrEmpty(dataToken))
                throw new Exception("ERROR: DataToken Missing!");

            if (string.IsNullOrEmpty(dateTime))
                throw new Exception("ERROR: DateTime Missing!");

            if (string.IsNullOrEmpty(hashType))
                throw new Exception("ERROR: hashType Missing!");

            var date = DateTime.ParseExact(dateTime, "yyyyMMddHH",
                                           CultureInfo.InvariantCulture); //UTC Datetime ("yyyyMMddHH")

            #endregion

            var partnerSitePassword = _partnerService.GetPartner(NhsRoute.PartnerId).LeadPostingPassword;

            var userProfile = this.ParseUrlAndGetUserProfile(dataToken);
            if (_ssoService.Authenticate(sessionToken, date, hashType, partnerSitePassword, userProfile.SsoUserId))
            {

                switch (NhsRoute.PartnerSiteUrl.ToLower())
                {
                    case "mred":
                        var createCookieMred = _ssoService.ValidateMredTermsOfService(userProfile, NhsRoute.BrandPartnerId, NhsRoute.PartnerSiteUrl, LanguageHelper.WrongPassword, LanguageHelper.EmailDoNotExist, LanguageHelper.UserAlreadyExist);
                        if (createCookieMred)
                        {
                            CookieManager.MredTempUserProfile = "true";
                            UserSession.TempUserProfile = JsonConvert.SerializeObject(userProfile);
                        }
                        break;
                    case "eastbay":
                        var createCookieEb = _ssoService.ValidateMredTermsOfService(userProfile, NhsRoute.BrandPartnerId, NhsRoute.PartnerSiteUrl, LanguageHelper.WrongPassword, LanguageHelper.EmailDoNotExist, LanguageHelper.UserAlreadyExist);
                        if (createCookieEb)
                        {
                            CookieManager.EastBayTempUserProfile = "true";
                            UserSession.TempUserProfile = JsonConvert.SerializeObject(userProfile);
                        }
                        break;
                    default:
                        var subscribeToNewsletter = _ssoService.VerifyPartnerAllowsWeeklyOptIn(NhsRoute.PartnerSiteUrl);
                        _ssoService.CreateUserProfile(userProfile, NhsRoute.BrandPartnerId, LanguageHelper.WrongPassword, LanguageHelper.EmailDoNotExist, LanguageHelper.UserAlreadyExist, subscribeToNewsletter);
                        break;
                }

                return Redirect(Pages.Home, new List<RouteParam>());
            }
            throw new Exception("ERROR: Invalid parameters passed. Access denied.");
        }

        #region SSO TESTER METHODS

        [HttpGet]
        public virtual ActionResult ShowPro7SsoTester()
        {

            return View(GetPro7TesterViewModel());
        }

        [HttpPost]
        public virtual ActionResult ShowPro7SsoTester(Pro7SsoTesterViewModel viewModel)
        {
            var postUrl = viewModel.PostUrl;

            if (!string.IsNullOrEmpty(postUrl))
            {
                viewModel = GetPro7TesterViewModel();
                viewModel.PostUrl = postUrl;
                return View(viewModel);
            }

            if (!string.IsNullOrEmpty(viewModel.UserInfo))
                viewModel.UserProfile = this.ParseUrlAndGetUserProfile(viewModel.UserInfo.EncodeToBase64String());

            var hashedPasscode = CryptoHelper.HashUsingAlgo(viewModel.Passcode + ":" + viewModel.UtcDatetime + ":" + viewModel.UserProfile.SsoUserId, viewModel.SelectedAlgorithm.ToLowerInvariant());
            var sessionToken = hashedPasscode;
            var dataToken = string.Empty;
            if (!string.IsNullOrEmpty(viewModel.UserInfo))
                dataToken = viewModel.UserInfo.EncodeToBase64String();

            var date = viewModel.UtcDatetime;
            var hashType = viewModel.SelectedAlgorithm;
            viewModel = GetPro7TesterViewModel();
            viewModel.PostUrl =
                    string.Format("{0}/{1}/sso/authenticate?sessiontoken={2}&datatoken={3}&dt={4}&hashtype={5}",
                                  Configuration.NewHomeSourceDomain.StripLeadingEndingChar('/'), NhsRoute.PartnerSiteUrl,
                                  sessionToken, dataToken, date, hashType);

            // "http://mvc.newhomesource.com/armls/sso/authenticate?sessiontoken=3577c23d8b67ac9d383eae418783677d&datatoken=Zmlyc3RuYW1lPUFteXxsYXN0bmFtZT1TbWl0aHxlbWFpbD1hbXkuc21pdGhAcmVtYXgubmV0fHBob25lPTUxMi05MTMtNDkxNnxhc3NvY2lhdGVpZD0xMDY3ODY5NnxvZmZpY2VuYW1lPVJFL01BWCAxfG9mZmljZXppcD03ODYxMw==&dt=2013020523&hashtype=md5";

            viewModel.HashedPasscode = "Generated Hashcode: " + hashedPasscode;
            viewModel.UserInfo = dataToken;
            return View(viewModel);
        }

        private Pro7SsoTesterViewModel GetPro7TesterViewModel()
        {
            var vm = new Pro7SsoTesterViewModel();

            vm.Algorithms = new SelectList(new[] { "MD5", "SHA1", "SHA256", "SHA384", "SHA512" }, vm.SelectedAlgorithm);
            return vm;
        }

        private UserProfile ParseUrlAndGetUserProfile(string tokenValue)
        {
            //DECODE and VALIDATE
            string[] dataArray = tokenValue.DecodeFromBase64String().Split('|');
            string firstName = string.Empty,
                   lastname = string.Empty,
                   email = string.Empty,
                   phone = string.Empty,
                   ssoId = string.Empty,
                   officename = string.Empty,
                   officezip = string.Empty;
            foreach (string uData in dataArray)
            {
                if (uData.Contains("firstname"))
                    firstName = uData.Split('=')[1];
                else if (uData.Contains("lastname"))
                    lastname = uData.Split('=')[1];
                else if (uData.Contains("email"))
                    email = uData.Split('=')[1];
                else if (uData.Contains("phone"))
                    phone = uData.Split('=')[1];
                else if (uData.Contains("associateid"))
                    ssoId = uData.Split('=')[1];
                else if (uData.Contains("officename"))
                    officename = uData.Split('=')[1];
                else if (uData.Contains("officezip"))
                    officezip = uData.Split('=')[1];
            }

            if (!String.IsNullOrEmpty(email))
                email = email.Split(';').First();

            //Validates if the required data was sent in the Encoded token
            if (!String.IsNullOrEmpty(firstName) && !String.IsNullOrEmpty((lastname)) &&
                !String.IsNullOrEmpty(email) &&
                !String.IsNullOrEmpty(ssoId) && !String.IsNullOrEmpty(officezip))
            {
                var userProfile = new UserProfile
                {
                    LogonName = email,
                    FirstName = firstName,
                    LastName = lastname,
                    PostalCode = officezip,
                    DayPhone = phone,
                    SsoUserId = ssoId,
                    PartnerId = NhsRoute.PartnerId,
                    AgencyName = officename
                };

                return userProfile;
            }
            else
            {
                throw new Exception("One or more parameters was missing");
            }
        }
        #endregion
    }
}
