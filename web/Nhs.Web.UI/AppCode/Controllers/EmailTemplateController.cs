﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Helpers.Lead;
using Nhs.Library.Helpers.Utility;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Search.Objects.Constants;
using Nhs.Web.UI.AppCode.Extensions;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Mvc.Routing.Interface;
using Nhs.Library.Web;
using Nhs.Library.Enums;
using Nhs.Library.Common;
using Nhs.Library.Extensions;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Route;
using Nhs.Library.Proxy;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Filters;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Partials;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Search.Objects;
using Nhs.Utility.Common;
using Nhs.Utility.Constants;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Resources;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class EmailTemplateController : ApplicationController
    {

        #region Members
        private readonly IPartnerService _partnerService;
        private readonly ICommunityService _communityService;
        private readonly IBuilderService _builderService;
        private readonly ILeadService _leadService;
        private readonly IListingService _listingService;
        private readonly IUserProfileService _userProfileService;
        private readonly IMarketService _marketService;
        private readonly IPathMapper _pathMapper; 
        private readonly IUserProfileService _userService;
        private readonly ISearchAlertService _searchAlertService;
        private const string UtmParameters = "?utm_source={0}&utm_medium=email&utm_campaign={1}&utm_content={2}";
        private readonly IPlannerService _plannerService;
        #endregion

        #region Constructor
        public EmailTemplateController(IUserProfileService userService,IPartnerService partnerService, ICommunityService communityService, IBuilderService builderService, ILeadService leadService, IListingService listingService, IMarketService marketService,
            IUserProfileService userProfileService, IPathMapper pathMapper, ISearchAlertService searchAlertService, IPlannerService plannerService)
        {
            _partnerService = partnerService;
            _marketService = marketService;
            _communityService = communityService;
            _builderService = builderService;
            _leadService = leadService;
            _listingService = listingService;
            _userProfileService = userProfileService;
            _pathMapper = pathMapper;
            _userService = userService;
            _searchAlertService = searchAlertService;
            _plannerService = plannerService;
        }
        #endregion

        #region Actions

        public virtual ActionResult ShowLeadEmail()
        {
            if (NhsRoute.IsBrandPartnerNhsPro)
                return ShowLeadEmailOld();

            var viewModel = GetEmailTemplateViewModel(false);
            // ReSharper disable Asp.NotResolved
            return View(viewModel);
            // ReSharper restore Asp.NotResolved

        }

        public virtual ActionResult ShowRecoEmail()
        {
            if (NhsRoute.IsBrandPartnerNhsPro )
                return ShowRecoEmailOld();

            var viewModel = GetEmailTemplateViewModel(true);
            // ReSharper disable Asp.NotResolved
            return View(viewModel);
            // ReSharper restore Asp.NotResolved
        }


        public virtual ActionResult ShowLeadEmailOld()
        {
            var viewModel = GetEmailViewModel();
            viewModel.BrochureHeadingText = GetBrochureHeadingText(viewModel);
            // ReSharper disable Asp.NotResolved
            return View(NhsMvc.Default.Views.EmailTemplate.OldVertion.ShowLeadEmail, viewModel);
            // ReSharper restore Asp.NotResolved
        }

        public virtual ActionResult ShowRecoEmailOld()
        {
            var viewModel = GetEmailViewModel();

            // ReSharper disable Asp.NotResolved
            return View(NhsMvc.Default.Views.EmailTemplate.OldVertion.ShowRecoEmail, viewModel);
            // ReSharper restore Asp.NotResolved
        }
        public virtual ActionResult ShowPresentedEmail()
        {
            var viewModel = new PresentedEmailViewModel();
            viewModel.Email = RouteParams.Email.Value();
            var sourceCommunityId = RouteParams.SourceCommunityId.Value<int>();
            var maxReco = RouteParams.RecoMax.Value<int>();
            var recoCommunityList = _communityService.GetRecommendedCommunities(viewModel.Email, NhsRoute.PartnerId, sourceCommunityId, 0, 0, 0, maxReco, true);
            var sourceCommunity = _communityService.GetCommunity(sourceCommunityId);
         
            if (recoCommunityList.Any())
            {
                viewModel.CommunityList = recoCommunityList;
                viewModel.RecoCommunityList = string.Join(",", recoCommunityList.Select(p => p.CommunityId));
                viewModel.CommunityName = sourceCommunity.CommunityName;
                viewModel.SourceCommunityId = sourceCommunityId.ToString();
                viewModel.MarketName = sourceCommunity.Market.MarketName;
                viewModel.FirstName = RouteParams.FirstName.Value();
                viewModel.LastName = RouteParams.LastName.Value();
                viewModel.PostalCode = RouteParams.PostalCode.Value();
                viewModel.RecoCommunityBuilderList = string.Join(",",
                                                                 recoCommunityList.Select(
                                                                     p => p.BuilderId + "|" + p.CommunityId));

               

                Response.Write("<SUBJECT>Other communities we think you will enjoy</SUBJECT>");

            }
            else
            {
                Response.Write("<NODATA>No Data for Recommended Communities</NODATA>");
            }

            var logger = new ImpressionLogger
            {
                CommunityId = sourceCommunity.CommunityId,
                BuilderId = sourceCommunity.BuilderId,
                PartnerId = NhsRoute.PartnerId.ToType<string>(),
                Refer = UserSession.Refer
            };
            logger.LogView(LogImpressionConst.MatchEmail);

            // ReSharper disable Asp.NotResolved
            return View(viewModel);
            // ReSharper restore Asp.NotResolved
        }

        private LeadEmailViewModel GetEmailViewModel()
        {
            var viewModel = new LeadEmailViewModel();
            AssembleViewModel(viewModel);
            

            if (viewModel.IsRecommendedEmail)
                PrintRecoSubject(viewModel.PropertyName);
            else
                PrintLeadSubject(viewModel.PropertyName, viewModel.BrandName, viewModel.IsComLead, viewModel.IsMultiBrochue);

            return viewModel;
        }

        public virtual ActionResult AccountCreationEmail(string userid)
        {
            var model = new AccountEmailTemplateViewModel();
            model.UsedPartnerName = true;
            FillBaseEmailTemplateViewModel(model);
            var partnerInfo = _partnerService.GetPartner(NhsRoute.PartnerId);

            // Creates and load a local profile object for mail. profile object 
            // from session is not available in mail.
            var tmpProfile = _userService.GetUserByGuid(userid);

            model.UserEmail = tmpProfile.LogonName;
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.Email, tmpProfile.LogonName));
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.Password, tmpProfile.Password));
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.FirstName, tmpProfile.FirstName));
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.Arrow, GlobalResources14.Default.images.buttons.btn_63_arrow_gif));

            var newhomechannel = NhsRoute.IsBrandPartnerNhsPro && NhsRoute.PartnerId != NhsRoute.BrandPartnerId
                ? " new home channel"
                : string.Empty;

            var beaconParams = new List<RouteParam>
            {
                new RouteParam(RouteParams.LogEvent, LogImpressionConst.OpenEmailCreateAccount),
                new RouteParam(RouteParams.Email, model.UserEmail, RouteParamType.QueryString)
            };

            var beaconUrl = PathHelpers.GetAbsoluteURL(beaconParams.ToUrl(Pages.EventLogger));
            model.BeaconUrl = beaconUrl;

            var searchNewHomes = NhsLinkHelper.GetUrlNhsLinkLogAndRedirectWithoutJs(Pages.Home,LogImpressionConst.EmailAccountCreationClickThrough);
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.SearchNewHomes, searchNewHomes));

            var startSearchAlert = NhsLinkHelper.GetUrlNhsLinkLogAndRedirectWithoutJs(Pages.CreateAlert,LogImpressionConst.EmailAccountCreationClickThrough);
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.StartSearchAlert, startSearchAlert));

            var getOurNewsletter = NhsLinkHelper.GetUrlNhsLinkLogAndRedirectWithoutJs(Pages.EditAccount + "/" + RouteParams.EventCode + "-" +LogImpressionConst.EmailAccountCreationClickThrough, LogImpressionConst.EmailAccountCreationClickThrough);
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.GetOurNewsletter, getOurNewsletter));
            string savedHomesLink;
            string savedHomesText;
            if (NhsRoute.IsBrandPartnerNhsPro)
            {
                savedHomesText = "Open my Client's Favorites";
                savedHomesLink = NhsLinkHelper.GetUrlNhsLinkLogAndRedirectWithoutJs(Pages.SavedHomes,LogImpressionConst.EmailAccountCreationClickThrough);
            }
            else
            {
                savedHomesText = LanguageHelper.SavedHomesText;
                savedHomesLink = NhsLinkHelper.GetUrlNhsLinkLogAndRedirectWithoutJs(Pages.SavedHomes + "/?tab=favorite", LogImpressionConst.EmailAccountCreationClickThrough);
            }

            model.ReplacementTags.Add(new ContentTag(ContentTagKey.SavedHomesLink, savedHomesLink));
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.SavedHomesText, savedHomesText));

            if (model.UsedPartnerName)
                Response.Write("<SUBJECT>Welcome to " + partnerInfo.PartnerName + newhomechannel + "</SUBJECT>");
            else
                Response.Write("<SUBJECT>Welcome to " + partnerInfo.SiteName + newhomechannel + "</SUBJECT>");

            return View(NhsMvc.Default.Views.EmailTemplate.AccountCreation.AccountCreation, model);
        }


        public virtual ActionResult RecoverPasswordEmail(string userid)
        {
            var model = new RecoverPasswordTemplateViewModel();
            model.UsedPartnerName = true;
            FillBaseEmailTemplateViewModel(model);

            var tmpProfile = _userService.GetUserByGuid(userid);
            model.UserEmail = tmpProfile.LogonName;
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.Email, tmpProfile.LogonName));
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.Password, tmpProfile.Password));
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.FirstName, !string.IsNullOrEmpty(tmpProfile.FirstName) ? tmpProfile.FirstName : tmpProfile.LogonName));
            Response.Write("<SUBJECT>Password Request</SUBJECT>");
            return View(NhsMvc.Default.Views.EmailTemplate.AccountCreation.RecoverPasswordEmail, model);
        }

        public virtual ActionResult SearchAlertEmail()
        {
            //Get querystring Params
            var userId = RouteParams.UserId.Value<string>();
            var alertId = RouteParams.AlertId.Value<int>();
            
            //Setup ViewModel
            var model = new SearchAlertTemplateViewModel
            {
                UtmParameters = "?utm_source=alert&utm_medium=email&utm_campaign=alert&utm_content={0}"
            };

            FillBaseEmailTemplateViewModel(model);
            IResultsView resultsFromService;

            //If user guid passed, then it is the weekly email (multiple search alerts)
            if (!string.IsNullOrEmpty(userId))
                model.IsWeeklyEmail = true;

            //Populate Alert List along with user id  if weekly
            if (model.IsWeeklyEmail)
            {
                model.AlertList = _searchAlertService.GetSearchAlertsByUserGuid(userId);
            }
            else
            {
                //If only AlertId is passed, process just that alert (this is the alert email that is sent after an alert is created)
                var alert = _searchAlertService.GetSearchAlert(alertId);
                userId = alert.UserGuid;
                model.AlertName = alert.Title;
                model.AlertList.Add(alert);
            }

            var userProfile = !StringHelper.IsValidEmail(userId) ? _userProfileService.GetUserByGuid(userId) : new UserProfile { LogonName = userId };

            model.FirstName = userProfile.FirstName;
            model.UserEmail = userProfile.LogonName;
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.Email, userProfile.LogonName));
            model.UserProfile = userProfile;
            //Setup Market Context  - use first alert if weekly
            int? marketId = model.AlertList.First().MarketId;

            model.MarketId = marketId.ToType<int>();
            model.MarketName = _marketService.GetMarketName(NhsRoute.PartnerId, marketId.ToType<int>());
            var market = _marketService.GetMarket(marketId.ToType<int>());
            model.MarketName = market.MarketName;

            foreach (var alert in model.AlertList)
            {
                IEnumerable<Community> communities;

                if (model.IsWeeklyEmail) //Generate new results
                {
                    //CALL ALERT SEARCH FROM LEGACY WS
                    var searchService = new SearchService();
                    resultsFromService = searchService.AlertsSearch(alert.ToSearchParams(model.UserProfile.PartnerId));
                    communities = (resultsFromService.CommunityResults).AsQueryable().ToCommunity().ToList();
                }
                else //Use saved results
                {
                    var alertResults = _searchAlertService.GetAlertResults(alertId).ToList();
                    communities = _communityService.GetCommunities(alertResults.Select(r => r.CommunityId).ToList()); //Map to community entity
                }

                //Dedupe and build final list
                foreach (var alertResult in communities)
                {
                    var tr = model.FinalResults.FindAll(cr => alertResult.CommunityId == cr.CommunityId && alertResult.BuilderId == cr.BuilderId);

                    if (tr.Count == 0)
                        model.FinalResults.Add(alertResult);
                }

                if (model.IsWeeklyEmail) //Generate new results
                {
                    //Save to DB
                    _searchAlertService.SaveResults(model.FinalResults, alertId);
                }
            }

            if (model.IsWeeklyEmail)
                _userProfileService.UpdateLastMatchDate(userId);

            //No alerts set by user - do not send email
            if (model.AlertList.Count == 0)
            {
                Response.Write("5¬0¬");
                return new EmptyResult();
            }

            //If still no results, upsell using generic community search
            if (model.FinalResults.Count <= 0)
            {
                var searchService = new SearchService();
                var psp = new PropertySearchParams(marketId.ToType<int>())
                {
                    PartnerId = model.UserProfile.PartnerId
                };

                //CALL COMMUNITY SEARCH FROM LEGACY WS
                resultsFromService = searchService.CommunitySearch(psp); //TODO: Call API

                if (resultsFromService.CommunityResults.Count > 10)//Pick top 10
                    resultsFromService.CommunityResults.RemoveRange(9, resultsFromService.CommunityResults.Count - 10);

                model.FinalResults = (resultsFromService.CommunityResults).AsQueryable().ToCommunity().ToList();

                if (model.IsWeeklyEmail) //Don't keep sending upsell communities every week
                {
                    _searchAlertService.IncrementNoMatchCount(userId);

                    int noMatchCount = _searchAlertService.GetNoMatchCount(userId);
                    if (noMatchCount > ProfileConst.MAX_NO_MATCH_EMAIL_TRY) //This is an indicator for the weekly notifier to not send email 
                    {
                        Response.Write("4¬" + noMatchCount + "¬");
                        return new EmptyResult();
                    }
                    Response.Write("2¬" + noMatchCount + "¬");
                }
            }
            else
            {
                int noMatchCount = _searchAlertService.GetNoMatchCount(userId);
                if (noMatchCount <= ProfileConst.MAX_NO_MATCH_EMAIL_TRY)
                    Response.Write("1¬" + noMatchCount + "¬");
                else
                    Response.Write("3¬" + noMatchCount + "¬");

                if (model.IsWeeklyEmail)
                    _searchAlertService.ResetNoMatchCount(userId); //Reset count when results found
            }

            // FILL COUNTERS FOR COMMUNITIES AND LISTINGS
            model.TotalNumberOfHomes = DataProvider.Current.GetQuickMoveInHomesCountForMarket(model.MarketId.ToType<int>(), NhsRoute.PartnerId);
            model.TotalNumberOfCommunities = DataProvider.Current.GetCommunitiesCountForMarket(model.MarketId.ToType<int>(), NhsRoute.PartnerId);

            //Write subject - this is parsed on the ackmanager side
            if (!model.IsWeeklyEmail)
            {
                var alert = model.AlertList.First();
                var subject = "<SUBJECT>"+LanguageHelper.SearchAlert+", " + DateTime.Now.ToString("MMMM d") + ", " + alert.Title + "</SUBJECT>";
                Response.Write(subject);
            }

            return View(NhsMvc.Default.Views.EmailTemplate.AlertResults.AlertResult, model);
        }

        public virtual ActionResult SendToFriendEmail(SendFriendTemplateViewModel model)
        {
            var partnerInfo = _partnerService.GetPartner(NhsRoute.PartnerId);

            // Set the links
            model.FromEmail = partnerInfo.FromEmail;
            model.SiteName = partnerInfo.SiteName;
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.Email, partnerInfo.FromEmail));
            FillBaseEmailTemplateViewModel(model);
            // Get the communities and homes
            if (model.CommunityId > 0)
            {
                model.Community = _communityService.GetSavedCommunity(NhsRoute.PartnerId, model.CommunityId);
                model.Promotions = _communityService.GetCommunityPromotions(model.CommunityId, model.BuilderId);

                // Set the community Link
                var commDetailUrl = new NhsUrl(Pages.CommunityDetail);
                commDetailUrl.AddParameter(UrlConst.CommunityID, model.CommunityId.ToType<string>());
                commDetailUrl.AddParameter(UrlConst.BuilderID, model.BuilderId.ToType<string>());
                var searchParams = new NhsPropertySearchParams
                {
                    MarketId = model.Community.MarketId,
                    BuilderId = model.BuilderId,
                    CommunityId = model.CommunityId
                };
                var searchResults = _communityService.GetCommunityResults(searchParams, false);
                model.TotalNumberOfCommunities = searchResults.TotalCommunities;
                model.TotalNumberOfHomes = searchResults.TotalHomes;

                // Get the hot home community, there could be more than one, so I use the first
                if (model.Community.HasHotHome == 1)
                {
                    var firstHome = _communityService.GetMatchingHomesForCommunity(model.CommunityId, UserSession.PropertySearchParameters).FirstOrDefault(home => home.IsHotHome);
                    if (firstHome != null)
                        model.HotHomeSpec = firstHome;
                }

                Response.Write("<SUBJECT>" + string.Format(LanguageHelper.NewHomesInOn, model.Community.City, model.SiteName) + "</SUBJECT>");
            }
            else
            {
                model.Spec = _plannerService.GetSavedHome(model.PlanId, model.SpecId, NhsRoute.PartnerId);
                model.Promotions = _communityService.GetCommunityPromotions(model.Spec.CommunityId, model.BuilderId);


                model.Community = _communityService.GetSavedCommunity(NhsRoute.PartnerId, model.Spec.CommunityId);
                var searchParams = new NhsPropertySearchParams
                {
                    MarketId = model.Community.MarketId,
                    BuilderId = model.BuilderId,
                    CommunityId = model.Community.CommunityId
                };
                var searchResults = _communityService.GetCommunityResults(searchParams, false);
                model.TotalNumberOfCommunities = searchResults.TotalCommunities;
                model.TotalNumberOfHomes = searchResults.TotalHomes;
                Response.Write("<SUBJECT>" + string.Format(LanguageHelper.NewHomesInOn, model.Spec.Community.City, model.Spec.Community.StateAbbr) + "</SUBJECT>");
            }

            return View(NhsMvc.Default.Views.EmailTemplate.Planner.SendFriendEmail, model);
        }
        #endregion

        private void AssembleViewModel(LeadEmailViewModel model)
        {
            string externalUrl;
            var sourceProperty = new LeadEmailProperty();

            //Basic Attribs
            model.FirstName = RouteParams.FirstName.Value<string>().ToTitleCase();
            model.RecoCount = RouteParams.RecoCount.Value<int>();
            model.SiteName = _partnerService.GetPartner(NhsRoute.PartnerId).SiteName;

            model.Email = RouteParams.Email.Value<string>();

            model.ShowMatchingCommunities = RouteParams.Showmc.Value<bool>();
            model.OnDemandPdf = RouteParams.OnDemandPdf.Value<bool>();
            var brochureutmParameters = !model.OnDemandPdf ? UtmParameters : UtmParameters.Replace("?", "&");
            var requestId = model.RequestId =  RouteParams.RequestID.Value<int>();
            var requestInfo = _leadService.GetCommsHomesForRequest(requestId);

            int marketId;
            string requestTypeCode;
            _leadService.GetRequestAttribs(requestId, out requestTypeCode, out marketId);

            Community sourceCommunity = null;
            IPlan sourcePlan = null;
            ISpec sourceSpec = null;

            if (RouteParams.SourceCommunityId.Value<int>() != 0)
                sourceCommunity = _communityService.GetCommunity(RouteParams.SourceCommunityId.Value<int>());
            else if (RouteParams.SourcePlanId.Value<int>() != 0)
            {
                sourcePlan = _listingService.GetPlan(RouteParams.SourcePlanId.Value<int>());
                sourceCommunity = sourcePlan.Community;
            }
            else if (RouteParams.SourceSpecId.Value<int>() != 0)
            {
                sourceSpec = _listingService.GetSpec(RouteParams.SourceSpecId.Value<int>());
                sourceCommunity = sourceSpec.Community;
            }

            if (requestTypeCode == LeadType.Market)
            {
                model.IsRecommendedEmail = true;
                requestTypeCode = sourceSpec != null || sourcePlan != null ? LeadType.Home : LeadType.Community;
            }

            var utmCampainType = model.IsRecommendedEmail ? "recommended" : (model.RecoCount > 0? "brochurerec" : "brochurenorec");
            var utmSource = model.IsRecommendedEmail ? "recommended" : "brochure";

            //Build all static urls
            BuildStaticLinks(model, utmSource, utmCampainType);

            if (requestInfo.Count > 0)
            {
                model.IsMultiBrochue = NhsRoute.IsBrandPartnerNhsPro && requestInfo.Count > 1;

                model.Email = requestInfo[0].EmailAddress;
                BuildBeaconLink(model);
                model.RequestType = requestTypeCode;

                string communityIdList;
                string builderIdList;
                switch (requestTypeCode)
                {
                    #region Home
                    case LeadType.Home:
                        IList<int> planIds = new List<int>();
                        IList<int> specIds = new List<int>();

                        if (model.IsRecommendedEmail)
                        {
                            communityIdList = string.Join(",", requestInfo.Select(r => r.CommunityId).ToList());
                            builderIdList = string.Join(",", requestInfo.Select(r => r.BuilderId).ToList());

                            model.RecommendedCommunities = _communityService.GetCommunitiesForRecoEmail(NhsRoute.PartnerId, communityIdList, builderIdList, sourceCommunity.Latitude, sourceCommunity.Longitude);
                            var community = new LatLong(sourceCommunity.Latitude.ToType<double>(), sourceCommunity.Longitude.ToType<double>());
                            model.RecommendedCommunities = model.RecommendedCommunities.OrderBy(p => new LatLong(p.Latitude.ToType<double>(), p.Longitude.ToType<double>()).ArcDistance(community)).ToList();

                            foreach (var comm in model.RecommendedCommunities)
                            {
                                if (!string.IsNullOrEmpty(comm.Builder.Url))
                                    comm.Builder.Url = GetBuilderUrl(comm.Builder.Url, comm.CommunityId, comm.BuilderId,
                                                                     0, 0, utmSource, utmCampainType);

                                comm.NonPdfBrochureUrl = _communityService.GetNonPdfBrochureUrl(comm.CommunityId, 0, 0,
                                                                                                comm.BuilderId);
                                comm.RequestItemId =
                                    requestInfo.FirstOrDefault(p => p.CommunityId == comm.CommunityId).RequestItemId;
                            }
                       
                            if(sourcePlan != null)
                                planIds = new List<int> { sourcePlan.PlanId };
                            else if(sourceSpec != null)
                                specIds = new List<int> { sourceSpec.SpecId };
                        }
                        else
                        {
                            specIds = requestInfo.Where(s => s.SpecId != 0).Select(s => s.SpecId).ToList();
                            planIds = requestInfo.Where(s => s.PlanId != 0).Select(s => s.PlanId).ToList();
                        }

                        var homes = _leadService.GetLeadEmailHomes(planIds, specIds);

                        //Get info from first home and assign basic model properties
                        sourceProperty = homes.FirstOrDefault();
                        model.PropertyName = sourceProperty.CommunityName;
                        model.CommunityId = sourceProperty.CommunityId;
                        model.BrandName = sourceProperty.BrandName;
                        model.BrochureTextFor = GetBrochureTextFor(model, sourceProperty);
                        model.AdditionalText = GetAdditionalInformationText(model, sourceProperty);                        
                        if (requestTypeCode.Equals("hm"))
                        {
                            model.PropertyHeaderText = GetPropertyHeaderText(sourceProperty);
                            if (sourceProperty.SpecId != 0)
                                model.IsSpecHome = true;
                        }

                        foreach (var home in homes)
                        {
                            home.MarketId = marketId;
                            home.ImageThumbnail = home.ImageThumbnail.Length > 0 && home.ImageThumbnail != "N"
                                ? Configuration.IRSDomain +
                                  home.ImageThumbnail.Replace(ImageSizes.Small, ImageSizes.HomeThumb)
                                      .Replace(".gif", ".jpg")
                                : GlobalResources14.Default.images.no_photo.no_photos_180x120_png;

                            home.FormatHomeAttrib = ListingUtility.FormatBedrooms(home.NumBedrooms, false).Replace(" bedrooms", LanguageHelper.BedroomAbbreviation) +
                                                    string.Format(" {0}{1}", ListingUtility.ComputeBathrooms(home.NumBaths, home.NumHalfBaths), LanguageHelper.BathroomAbbreviation) +
                                                    string.Format(" {0:0.##}{1}", home.NumGarages, LanguageHelper.GarageAbbreviation);

                            if (Convert.ToBoolean(home.IsHotHome))
                                home.ShowHotArea = true;

                            //Get community info (for promos et al)
                            var comm = _communityService.GetCommunity(home.CommunityId);

                            
                            if (comm != null)
                            {
                                var promos = _communityService.GetCommunityPromotions(comm.CommunityId, comm.BuilderId);
                                home.Promos = promos;
                                if (promos.Any())
                                    home.ShowPromoArea = true;

                                home.CommunityId = comm.CommunityId;
                                home.BuilderId = comm.BuilderId;
                                home.BuilderUrl = GetBuilderUrl(comm.Builder.Url, comm.CommunityId, comm.BuilderId, home.SpecId, home.PlanId, utmSource, utmCampainType);
                            }

                            home.FormatPrice = FormatCurrency(home.HomeStatus, home.Price);

                            //Home Detail Link
                            externalUrl = home.SpecId != 0 ? SetHomeDetailLink(model, home.SpecId, home.PlanName, true) : SetHomeDetailLink(model, home.PlanId, home.PlanName, false);
                            externalUrl = HttpUtility.UrlEncode(externalUrl + string.Format(UtmParameters, utmSource, utmCampainType, "homename"));
                            home.DetailPageUrl = GetLogRedirectUrl(home.BuilderId, home.CommunityId, home.SpecId, home.PlanId, GetEventCode(model.IsRecommendedEmail), externalUrl, string.Empty);
                            home.DetailPageText = home.PlanName;

                            //Home Brochure Link
                            var nonPdfBrochureUrl = _communityService.GetNonPdfBrochureUrl(home.CommunityId, home.PlanId,
                                                                                           home.SpecId, home.BuilderId);
                            if (!model.IsRecommendedEmail && model.OnDemandPdf)
                            {
                                var requestid = home.SpecId != 0 ? requestInfo.FirstOrDefault(p => p.SpecId == home.SpecId).RequestItemId : requestInfo.FirstOrDefault(p => p.PlanId == home.PlanId).RequestItemId;
                                externalUrl = ViewBrochureHelper.ViewBrochureLink(model.FirstName, requestid,
                                                                                  model.Email, "");
                            }
                            else
                                externalUrl = GetBrochureUrl(home.PlanId.ToType<string>(),
                                                                     home.SpecId.ToType<string>(), string.Empty,
                                                                     string.Empty, requestTypeCode, model.Email);

                            if (!string.IsNullOrEmpty(nonPdfBrochureUrl) && NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
                                externalUrl = nonPdfBrochureUrl;

                            home.BrochureUrl = GetLogRedirectUrl(home.BuilderId, home.CommunityId, home.SpecId, home.PlanId, GetEventCode(model.IsRecommendedEmail), HttpUtility.UrlEncode(externalUrl + (!string.IsNullOrEmpty(nonPdfBrochureUrl) && NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>() ? string.Empty : string.Format(brochureutmParameters, utmSource, utmCampainType, "freebrochure2"))), string.Empty);
                            model.BrochureUrl = GetLogRedirectUrl(home.BuilderId, home.CommunityId, home.SpecId, home.PlanId, GetEventCode(model.IsRecommendedEmail), HttpUtility.UrlEncode(externalUrl + (!string.IsNullOrEmpty(nonPdfBrochureUrl) && NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>() ? string.Empty : string.Format(brochureutmParameters, utmSource, utmCampainType, "freebrochure1"))), string.Empty);

                            model.SourceLead = home;
                            home.PropertyHeaderText = GetPropertyHeaderText(home);
                        }
                        if (homes.Any())
                        model.Properties = homes;
                        break;
                    #endregion

                    #region Community
                    case LeadType.Community:
                        
                        model.IsComLead = true;

                        communityIdList = string.Join(",", requestInfo.Select(r => r.CommunityId).ToList());
                        builderIdList = string.Join(",", requestInfo.Select(r => r.BuilderId).ToList());
                        var commIds = requestInfo.Where(r => r.CommunityId != 0).Select(r => r.CommunityId).ToList();

                        if (model.IsRecommendedEmail)
                        {
                            model.RecommendedCommunities =
                                _communityService.GetCommunitiesForRecoEmail(NhsRoute.PartnerId, communityIdList,
                                                                             builderIdList, sourceCommunity.Latitude,
                                                                             sourceCommunity.Longitude);
                            var community = new LatLong(sourceCommunity.Latitude.ToType<double>(), sourceCommunity.Longitude.ToType<double>());
                            model.RecommendedCommunities = model.RecommendedCommunities.OrderBy(p => new LatLong(p.Latitude.ToType<double>(), p.Longitude.ToType<double>()).ArcDistance(community)).ToList();

                            foreach (var comm in model.RecommendedCommunities)
                            {
                                if (!string.IsNullOrEmpty(comm.Builder.Url))
                                    comm.Builder.Url = GetBuilderUrl(comm.Builder.Url, comm.CommunityId, comm.BuilderId,
                                                                     0, 0, utmSource, utmCampainType);

                                comm.NonPdfBrochureUrl = _communityService.GetNonPdfBrochureUrl(comm.CommunityId, 0, 0,
                                                                                                comm.BuilderId);
                                comm.RequestItemId = requestInfo.FirstOrDefault(p => p.CommunityId == comm.CommunityId).RequestItemId;
                            }
                            commIds = new List<int>() { sourceCommunity.CommunityId };
                        }

                        //Get all lead comms
                        var communities = _leadService.GetLeadEmailCommunities(commIds);
                        sourceProperty = communities.FirstOrDefault();
                        sourceProperty.MarketId = marketId;
                        model.CommunityId = sourceProperty.CommunityId;
                        model.BrandName = sourceProperty.BrandName;
                        model.PropertyName = sourceProperty.CommunityName;
                        model.BrochureTextFor = GetBrochureTextFor(model, sourceProperty);
                        model.AdditionalText = GetAdditionalInformationText(model, sourceProperty);
                        model.PropertyHeaderText = sourceProperty.CommunityName;

                        foreach (var comm in communities)
                        {
                            comm.MarketId = marketId;
                            externalUrl = SetCommDetailLink(model, comm.CommunityId.ToType<string>(), comm.BuilderId.ToType<string>(), sourceCommunity);
                            externalUrl = HttpUtility.UrlEncode(externalUrl + string.Format(UtmParameters, utmSource, utmCampainType, "commname"));
                            comm.DetailPageUrl = GetLogRedirectUrl(comm.BuilderId, comm.CommunityId, comm.SpecId, comm.PlanId, GetEventCode(model.IsRecommendedEmail), externalUrl, string.Empty);

                            var nonPdfBrochureUrl = _communityService.GetNonPdfBrochureUrl(comm.CommunityId, comm.PlanId,
                                                                                           comm.SpecId, comm.BuilderId);
                            if (!model.IsRecommendedEmail && model.OnDemandPdf)
                            {
                                var requestid =
                                    requestInfo.FirstOrDefault(p => p.CommunityId == comm.CommunityId).RequestItemId;
                                externalUrl = ViewBrochureHelper.ViewBrochureLink(model.FirstName, requestid,
                                                                                        model.Email, "");
                            }
                            else
                                externalUrl = GetBrochureUrl(string.Empty, string.Empty,
                                                                   comm.CommunityId.ToType<string>(),
                                                                   comm.BuilderId.ToType<string>(), requestTypeCode,
                                                                   model.Email);

                            if (!string.IsNullOrEmpty(nonPdfBrochureUrl) &&
                                NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
                                externalUrl = nonPdfBrochureUrl;

                            comm.BrochureUrl = GetLogRedirectUrl(comm.BuilderId, comm.CommunityId, comm.SpecId, comm.PlanId, GetEventCode(model.IsRecommendedEmail), HttpUtility.UrlEncode(externalUrl + (!string.IsNullOrEmpty(nonPdfBrochureUrl) && NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>() ? string.Empty : string.Format(brochureutmParameters, utmSource, utmCampainType, "freebrochure2"))), string.Empty);
                            model.BrochureUrl = GetLogRedirectUrl(comm.BuilderId, comm.CommunityId, comm.SpecId, comm.PlanId, GetEventCode(model.IsRecommendedEmail), HttpUtility.UrlEncode(externalUrl + (!string.IsNullOrEmpty(nonPdfBrochureUrl) && NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>() ? string.Empty : string.Format(brochureutmParameters, utmSource, utmCampainType, "freebrochure1"))), string.Empty);
                            //comm.BrochureUrl = string.IsNullOrEmpty(nonPdfBrochureUrl) ? GetLogRedirectUrl(comm.BuilderId, comm.CommunityId, comm.SpecId, comm.PlanId, GetEventCode(model.IsRecommendedEmail), HttpUtility.UrlEncode(externalUrl + string.Format(UtmParameters, utmSource, utmCampainType, "freebrochure2")), string.Empty) : nonPdfBrochureUrl;
                            //model.BrochureUrl = string.IsNullOrEmpty(nonPdfBrochureUrl) ? GetLogRedirectUrl(comm.BuilderId, comm.CommunityId, comm.SpecId, comm.PlanId, GetEventCode(model.IsRecommendedEmail), HttpUtility.UrlEncode(externalUrl + string.Format(UtmParameters, utmSource, utmCampainType, "freebrochure1")), string.Empty) : nonPdfBrochureUrl;

                            //TODO: Check into IP Profiles and finalize this
                            comm.ImageThumbnail = comm.ImageThumbnail.Length > 0 && comm.ImageThumbnail != "N"
                                                     ? Configuration.IRSDomain +
                                                       comm.ImageThumbnail.Replace(ImageSizes.Small, ImageSizes.HomeThumb).Replace(ImageSizes.Results, ImageSizes.CommDetailThumb).Replace(".gif", ".jpg").Replace(".png", ".jpg")
                                                     : GlobalResources14.Default.images.no_photo.no_photos_180x120_png;

                            comm.Promos = _communityService.GetCommunityPromotions(comm.CommunityId, comm.BuilderId);
                            comm.ShowPromoArea = comm.Promos.Any();

                            if (sourceCommunity == null)
                                sourceCommunity = _communityService.GetCommunity(comm.CommunityId);

                            comm.BuilderUrl = GetBuilderUrl(comm.BuilderUrl, comm.CommunityId, comm.BuilderId, 0, 0, utmSource, utmCampainType) ;

                            var listings = new List<LeadEmailProperty>();

                            if (sourceCommunity.SpecHotHomeId > 0)
                            {
                                listings = _leadService.GetLeadEmailHomes(new List<int>(), new List<int>{ sourceCommunity.SpecHotHomeId.ToType<int>() }).ToList();
                                comm.ShowHotArea = true;
                                comm.HotHomeTitle = sourceCommunity.SpecHotHomeTitle;
                            }
                            else if (sourceCommunity.PlanHotHomeId > 0)
                                listings = _leadService.GetLeadEmailHomes(new List<int> { sourceCommunity.PlanHotHomeId.ToType<int>() }, new List<int>()).ToList();

                            if (listings.Count > 0)
                            {
                                comm.NumBedrooms = listings[0].NumBedrooms;
                                comm.NumBaths = listings[0].NumBaths;
                                comm.NumHalfBaths = listings[0].NumHalfBaths;
                                comm.NumGarages = listings[0].NumGarages;
                            }

                            if (comm.PriceLow > 0 && comm.PriceHigh > 0)
                            {
                                var priceLabel = StringHelper.PrettyPrintRange(Convert.ToDouble(comm.PriceLow), Convert.ToDouble(comm.PriceHigh), "c0", LanguageHelper.From.ToTitleCase());

                                if (priceLabel.Split('m').Length > 0)
                                    comm.FormatPrice = priceLabel.Split('m')[1];
                            }
                            model.SourceLead = comm;
                        }

                        if (communities.Any())
                            model.Properties = communities;
                        break;

                        #endregion
                }
            }
             
            //Build Community Results Url
            var commResParams = new List<RouteParam>
                {
                    new RouteParam(RouteParams.Market, sourceProperty.MarketId.ToType<string>(), RouteParamType.Friendly, true,
                              true)
                };
            if (sourceProperty.PriceLow > 0)
                commResParams.Add(new RouteParam(RouteParams.PriceLow, sourceProperty.PriceLow.ToType<int>().ToType<string>(), RouteParamType.Friendly, true, true));
            if (sourceProperty.PriceHigh > 0)
                commResParams.Add(new RouteParam(RouteParams.PriceHigh, sourceProperty.PriceHigh.ToType<int>().ToType<string>(), RouteParamType.Friendly, true, true));
            externalUrl = PathHelpers.GetAbsoluteURL(commResParams.ToUrl(Pages.CommunityResults));
            model.CommResultsUrl = GetLogRedirectUrl(sourceProperty.BuilderId, sourceProperty.CommunityId, sourceProperty.SpecId, sourceProperty.PlanId, GetEventCode(model.IsRecommendedEmail), HttpUtility.UrlEncode(externalUrl + string.Format(UtmParameters, utmSource, utmCampainType, "findsimilar")), string.Empty);

            //Build Mortgage Rates Url
            var mortgageParams = new List<RouteParam>
                {
                    new RouteParam(RouteParams.Market, sourceProperty.MarketId.ToType<string>(), RouteParamType.Friendly, true,
                              true),
                    new RouteParam(RouteParams.Community, sourceProperty.CommunityId.ToType<string>(), RouteParamType.Friendly,
                              true, true),
                    new RouteParam(RouteParams.Builder, sourceProperty.BuilderId.ToType<string>(), RouteParamType.Friendly, true,
                              true)
                };
            externalUrl = HttpUtility.UrlEncode(PathHelpers.GetAbsoluteURL(mortgageParams.ToUrl(Pages.MortgageRates)) + string.Format(UtmParameters, utmSource, utmCampainType, "mortgage"));
            model.MortgageRatesUrl = GetLogRedirectUrl(sourceProperty.BuilderId, sourceProperty.CommunityId, sourceProperty.SpecId, sourceProperty.PlanId, GetEventCode(model.IsRecommendedEmail), externalUrl, string.Empty);
            if (NhsRoute.IsBrandPartnerNhsPro)
                ProSettings(model);
        }

        private void ProSettings(LeadEmailViewModel model)
        {
            var agent = _userProfileService.GetUserByLogonName(model.Email, NhsRoute.PartnerId);
            if (agent != null)
            {
                var agentBrochureTemplate = agent.AgentBrochureTemplates.FirstOrDefault();
                if (agentBrochureTemplate != null)
                {
                    model.SiteLogo = !string.IsNullOrEmpty(agentBrochureTemplate.ImageUrl)
                                         ? string.Format("{0}/{1}", Configuration.IRSDomain,
                                                         agentBrochureTemplate.ImageUrl)
                                         : !string.IsNullOrEmpty(agentBrochureTemplate.AgencyLogo)
                                               ? string.Format("{0}/{1}", Configuration.IRSDomain,
                                                               agentBrochureTemplate.AgencyLogo)
                                               : string.Empty;

                    model.AgentBrochureTemplate = agentBrochureTemplate;
                }
            }
        }

        private void BuildBeaconLink(LeadEmailViewModel model)
        {
            //Beacon Url
            var beaconParams = new List<RouteParam>();
            beaconParams.Add(new RouteParam(RouteParams.LogEvent, LogImpressionConst.OpenEmailBrochure));
            beaconParams.Add(new RouteParam(RouteParams.Email, model.Email, RouteParamType.QueryString));
            model.BeaconUrl = PathHelpers.GetAbsoluteURL(beaconParams.ToUrl(Pages.EventLogger));
        }

        private string GetAdditionalInformationText(LeadEmailViewModel model, LeadEmailProperty property)
        {
            return string.Format(LanguageHelper.WillLikelyFollowUpWithMoreDetail, property.BrandName);                        
        }

        private string GetBrochureHeadingText(LeadEmailViewModel model)
        {
            string text = LanguageHelper.GetYourFreeBrochure;

            if (model.IsComLead)
            {
                text += string.Format(" {0} {1}", LanguageHelper.For, model.PropertyName); 
            }            

            return text;
        }

        private string GetBrochureTextFor(LeadEmailViewModel model, LeadEmailProperty property)
        {
            string text = string.Empty;

            if (model.IsComLead)
            {
                text = string.Format(LanguageHelper.For + " {0} " + LanguageHelper.By+ " {1}", model.PropertyName, model.BrandName);
            }
            else
            {
                if (property.SpecId != 0)
                {
                    if (!string.IsNullOrEmpty(property.Address))
                        text = string.Format(LanguageHelper.For + " {0}", property.Address);
                    else
                    {
                        var spec = _listingService.GetSpec(property.SpecId);
                        if(!string.IsNullOrEmpty(spec.Address1))
                            text = string.Format(LanguageHelper.For + " {0}", spec.Address1);
                        else if (!string.IsNullOrEmpty(spec.Address2))
                            text = string.Format(LanguageHelper.For + " {0}", spec.Address2);
                        else
                        {
                            var comm = _communityService.GetCommunity(property.CommunityId);
                            if(!string.IsNullOrEmpty(comm.Address1))
                                text = string.Format(LanguageHelper.For + " {0}", comm.Address1);
                            else if (!string.IsNullOrEmpty(comm.Address2))
                                text = string.Format(LanguageHelper.For + " {0}", comm.Address2);
                        }
                    }
                    
                }
                else
                {
                    string initWord = (property.PlanName.ToLower().StartsWith("the")) ? "for" : "for the";
                    text = string.Format("{0} {1}"+LanguageHelper.At+" {2}", initWord, property.PlanName, property.CommunityName);
                }
            }

            return text;
        }

        private string GetPropertyHeaderText(LeadEmailProperty property)
        {
            string text = string.Empty;

            if (property.SpecId != 0)
            {

                if (!string.IsNullOrEmpty(property.Address))
                    text = property.Address;
                else
                {
                    var spec = _listingService.GetSpec(property.SpecId);
                    if (!string.IsNullOrEmpty(spec.Address1))
                        text = spec.Address1;
                    else if (!string.IsNullOrEmpty(spec.Address2))
                        text = spec.Address2;
                    else
                    {
                        var comm = _communityService.GetCommunity(property.CommunityId);
                        if (!string.IsNullOrEmpty(comm.Address1))
                            text = comm.Address1;
                        else if (!string.IsNullOrEmpty(comm.Address2))
                            text = comm.Address2;
                    }
                }

            }
            else
            {
                string initWord = (property.PlanName.ToLower().StartsWith("the")) ? "" : "The";
                text = string.Format("{0} {1}" + LanguageHelper.At + " {2}", initWord, property.PlanName, property.CommunityName).Trim();
            }

            return text;
        }

        private void BuildStaticLinks(LeadEmailViewModel model, string utmSource, string utmCampainType)
        {
            model.SiteLogo = PartnerLogo();
            model.SiteLogoBackground = "#0091da";        
            model.SiteHomePageUrl = PathHelpers.GetAbsoluteURL("/") + string.Format(UtmParameters, utmSource, utmCampainType, "HomePage_Top");            

            model.PolicyUrl = PathHelpers.GetAbsoluteURL(new List<RouteParam>().ToUrl(Pages.PrivacyPolicy)) + string.Format(UtmParameters, utmSource, utmCampainType, "privacy");
            
            model.UnsubscribeUrl = PathHelpers.GetAbsoluteURL(new List<RouteParam>()    
            {
                //new RouteParam(RouteParams.UserId, model.Email, RouteParamType.Friendly,true)
            }.ToUrl(Pages.Unsubscribe)) + string.Format(UtmParameters, utmSource, utmCampainType, "unsubscribe");
            

            var learnParams = new List<RouteParam>();
            var qParams = new List<RouteParam>();
            //Learn More
            learnParams.Add(new RouteParam(RouteParams.Category, "Why-Buy-a-New-Home", RouteParamType.Friendly));
            learnParams.Add(new RouteParam(RouteParams.CategoryId, "45", RouteParamType.Friendly));
            model.LearnMoreUrl = PathHelpers.GetAbsoluteURL(learnParams.ToUrl(Pages.HomeGuideCategory)) + string.Format(UtmParameters, utmSource, utmCampainType, "whybuynew");

            //Ask Questions
            qParams.Add(new RouteParam(RouteParams.Article, "Questions-To-Ask-Your-Builder", RouteParamType.Friendly));
            model.QuestionsUrl = PathHelpers.GetAbsoluteURL(qParams.ToUrl(Pages.HomeGuideArticle)) + string.Format(UtmParameters, utmSource, utmCampainType, "15builderquest"); ;

            //Get Prequalified Today
            var prequalUrl = "https://www.prequalplus.com/landing?gate=BDX&iid=bdx:[pcode]:email";
            model.GetQualifiedUrl = prequalUrl.Replace("[pcode]", "nhl") + string.Format(UtmParameters.Replace("?", "&"), utmSource, utmCampainType, "prequalified");
        }

        private string GetBuilderUrl(string url, int communityId, int builderId, int specId, int planId, string utmSource, string utmCampainType)
        {
            var externalUrl = !string.IsNullOrEmpty(url) ? url : string.Empty;
            return string.IsNullOrEmpty(externalUrl) ? "#" : GetLogRedirectUrl(builderId, communityId, specId, planId, LogImpressionConst.EmailBrandClickThrough, HttpUtility.UrlEncode(externalUrl), "?" + HttpUtility.UrlEncode(string.Format(UtmParameters.Replace("?", string.Empty), utmSource, utmCampainType, "buildersite")));
        }

        private string GetLogRedirectUrl(int builderId, int communityId, int specId, int planId, string eventCode, string externalUrl, string extraParams)
        {
            var paramz = new List<RouteParam>();
            paramz.Add(new RouteParam(RouteParams.LogEvent, eventCode));
            paramz.Add(new RouteParam(RouteParams.Builder, builderId));
            paramz.Add(new RouteParam(RouteParams.Community, communityId));
            paramz.Add(new RouteParam(RouteParams.SpecId, specId));
            paramz.Add(new RouteParam(RouteParams.PlanId, planId));
            paramz.Add(new RouteParam(RouteParams.HUrl, CryptoHelper.HashUsingAlgo(HttpUtility.UrlDecode(HttpUtility.UrlDecode(externalUrl)) + ":" + eventCode, "md5")));

            if (!string.IsNullOrEmpty(extraParams))
                return PathHelpers.GetAbsoluteURL(paramz.ToUrl(Pages.LogRedirect, false)) + extraParams + "&" + RouteParams.ExternalUrl + "=" + externalUrl;
            
            paramz.Add(new RouteParam(RouteParams.ExternalUrl, externalUrl, RouteParamType.QueryString));
            
            return PathHelpers.GetAbsoluteURL(paramz.ToUrl(Pages.LogRedirect, false));
        }

        private string GetEventCode(bool isRcm)
        {
            return isRcm ? LogImpressionConst.EmailRecommendedClickThrough : LogImpressionConst.EmailBrochureClickThrough;
        }
        

        private string GetBrochureUrl(string planId, string specId, string communityId, string builderId, string requestType, string email)
        {
            var brochureUrl = new List<RouteParam>();
            brochureUrl.Add(new RouteParam(RouteParams.Email, email, RouteParamType.Friendly, true, true));

            if (communityId.Length > 0 && builderId.Length > 0)
            {
                brochureUrl.Add(new RouteParam(RouteParams.Community, communityId, RouteParamType.Friendly, true, true));
                brochureUrl.Add(new RouteParam(RouteParams.Builder, builderId, RouteParamType.Friendly, true, true));
            }

            if (planId.Length > 0)
                brochureUrl.Add(new RouteParam(RouteParams.PlanId, planId, RouteParamType.Friendly, true, true));

            if (specId.Length > 0)
                brochureUrl.Add(new RouteParam(RouteParams.SpecId, specId, RouteParamType.Friendly, true, true));

            brochureUrl.Add(new RouteParam(RouteParams.LeadType, requestType, RouteParamType.Friendly, true, true));
            return PathHelpers.GetAbsoluteURL(brochureUrl.ToUrl(Pages.BrochureGen));
        }

        private string FormatCurrency(string homeStatus, object price)
        {
            if (homeStatus.ToType<int>() != HomeStatusType.ModelHome.ToType<int>())
                return StringHelper.FormatCurrency(price);

            return string.Empty;
        }

        private string SetHomeDetailLink(LeadEmailViewModel model, int specOrPlanId, string planName, bool isSpec)
        {
            List<RouteParam> homeDetailUrl = new List<RouteParam>();

            homeDetailUrl.Add(isSpec
                                  ? new RouteParam(RouteParams.SpecId, specOrPlanId.ToType<string>(), RouteParamType.Friendly, true, true)
                                  : new RouteParam(RouteParams.PlanId, specOrPlanId.ToType<string>(), RouteParamType.Friendly, true, true));

            //model.DetailLinkText = planName;

            return PathHelpers.GetAbsoluteURL(homeDetailUrl.ToUrl(Pages.HomeDetail));
        }

        private string SetCommDetailLink(LeadEmailViewModel model, string communityId, string builderId, Community comm)
        {
            string res = string.Empty;
            if (comm == null)
            {
                var commDetailUrl = new List<RouteParam>
                {
                    new RouteParam(RouteParams.Builder, builderId, RouteParamType.Friendly, true, true),
                    new RouteParam(RouteParams.Community, communityId, RouteParamType.Friendly, true, true)
                };
                res = commDetailUrl.ToUrl(Pages.CommunityDetail);
            }
            else
                res = comm.ToCommunityDetail().ToUrl(RedirectionHelper.GetCommunityDetailPage());

            return PathHelpers.GetAbsoluteURL(res);
        }

        private void PrintLeadSubject(string propertyName, string brandName, bool isComLead,bool isMultiBrochure)
        {
            var subjectTitle = (NhsRoute.IsBrandPartnerNhsPro)
                                   ? string.Format(LanguageHelper.MayIntersestYou,
                                   (isComLead ? (isMultiBrochure ? LanguageHelper.Communities : LanguageHelper.Community) : LanguageHelper.Home).ToLower(), isMultiBrochure ? LanguageHelper.These : LanguageHelper.This.ToTitleCase())
                                   : LanguageHelper.YourFreeBrochureFor;

            if (!isComLead && NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<Int32>())
                Response.Write("<SUBJECT>" + subjectTitle + " " + propertyName + "</SUBJECT>");
            else
                Response.Write("<SUBJECT>" + subjectTitle + " " + propertyName + " " + LanguageHelper.By + " " + brandName + "</SUBJECT>");
        }

        private void PrintRecoSubject(string propertyName)
        {
            var subjectTitle =LanguageHelper.YourRecommendedCommunitiesSimilarTo + " ";
            Response.Write("<SUBJECT>" + subjectTitle + propertyName + "</SUBJECT>");

        }

    }
}
