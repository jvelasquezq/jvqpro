﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Business.Seo.SiteIndex;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Constants.Route;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Extensions;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class SiteIndexController : ApplicationController
    {
        #region private attributes

        private readonly IPathMapper _pathMapper;
        private readonly IPartnerService _partnerService;
        private readonly IStateService _stateService;
        private readonly IMarketService _marketService;
        private readonly ILookupService _lookupService;
        private readonly IBrandService _brandService;
        private readonly ICommunityService _communityService;
        private readonly IApiService _apiService;
        private readonly ISeoContentService _seoContentService;
        private readonly IMarketDfuService _marketDFUService;


        #endregion

        #region constructors
        public SiteIndexController(IPathMapper pathMapper, IPartnerService partnerService, IStateService stateService,
            IMarketService marketService, ILookupService lookupService, IBrandService brandService,
            ICommunityService communityService, IApiService apiService, ISeoContentService seoContentService, IMarketDfuService marketDFUService)
            : base(pathMapper)
        {
            _pathMapper = pathMapper;
            _partnerService = partnerService;
            _stateService = stateService;
            _marketService = marketService;
            _lookupService = lookupService;
            _brandService = brandService;
            _communityService = communityService;
            _apiService = apiService;
            _seoContentService = seoContentService;
            _marketDFUService = marketDFUService;
        }
        #endregion

        #region Actions
        public virtual ActionResult SiteIndex()
        {
            var viewModel = new SiteIndexViewModel
            {
                Globals = { PartnerInfo = _partnerService.GetPartner(NhsRoute.PartnerId) }
            };

            viewModel.PartnerName = viewModel.Globals.PartnerInfo.PartnerName;
            viewModel.States = this._stateService.GetPartnerStates(NhsRoute.PartnerId);
            viewModel.SeoContentMarkets = "newhomesbymarketlist";
            viewModel.SeoContentCustomLinksHeader = "customlinkslistheader";
            viewModel.SeoContentCustomLinksList = "customlinkslist";
            viewModel.SeoContentFooterTextHeader = "footertextheader";
            viewModel.SeoContentFooterText = "footertext";

            var contentTags = new List<ContentTag>
            {
                new ContentTag {TagKey = ContentTagKey.PartnerName, TagValue = viewModel.PartnerName}
            };
            viewModel.SeoContentTags = contentTags;
            
            var seoContent = _seoContentService.GetSeoContent<SiteIndexContent>(new SeoContentOptions
                (
                SiteIndexConst.SiteIndexFolder,
                "SiteIndexSeo.xml",
                replaceTags: contentTags
                ));

            viewModel.H1 = seoContent.H1;
            viewModel.OnPageText = seoContent.OnPageText;

            OverwriteMetaFromContent(viewModel, seoContent);
            base.SetupAdParameters(viewModel);

            // ReSharper disable once Mvc.ViewNotResolved
            return View(viewModel);
        }

        public virtual ActionResult SearchRedirect(int selectedMarketId, int? priceLow, int? priceHigh)
        {
            var param = new List<RouteParam>();

            UserSession.PersonalCookie.MarketId = selectedMarketId;
            param.Add(new RouteParam(RouteParams.Market, selectedMarketId));

            if (priceLow != null)
            {
                param.Add(new RouteParam(RouteParams.PriceLow, priceLow));
                UserSession.PersonalCookie.PriceLow = (int)priceLow;
            }

            if (priceHigh != null)
            {
                param.Add(new RouteParam(RouteParams.PriceHigh, priceHigh));
                UserSession.PersonalCookie.PriceHigh = priceLow.ToType<int>();
            }

            //redirect to location handler page
            return this.Redirect(Pages.CommunityResults, param);
        }

        public virtual ActionResult StateIndex(string statename)
        {
            statename = statename.ReturnSpaceAndDash();
            var viewModel = new StateIndexViewModel();
            int partnerId = NhsRoute.PartnerId;
            var partner = this._partnerService.GetPartner(partnerId);
            UserSession.PersonalCookie.State = statename;
            var userSessionMarket = UserSession.PersonalCookie.MarketId;

            viewModel.PartnerId = partnerId;
            viewModel.BrandPartnerId = NhsRoute.BrandPartnerId;
            viewModel.SiteRoot = (string.IsNullOrEmpty(NhsRoute.PartnerSiteUrl) ? "" : (NhsRoute.PartnerSiteUrl + "/"));
            viewModel.ResourceRoot = Configuration.ResourceDomain + "/";
            
            string stateAbbreviation = _stateService.GetStateAbbreviation(statename);                      
            State state = _stateService.GetStateCoodinates(stateAbbreviation);

            if (state == null)
            {
                return RedirectTo404();
            }

            viewModel.StateName = state.StateName;
            viewModel.StateAbbreviation = stateAbbreviation;

            viewModel.CenterLat = state.Latitude.ToType<decimal>();
            viewModel.CenterLng = state.Longitude.ToType<decimal>();
            viewModel.ZoomLevel = state.Zoom;
            SetupStateCountsFromWebApi(viewModel, viewModel.StateAbbreviation);
            var contentTags = new List<ContentTag>
            {
                new ContentTag {TagKey = ContentTagKey.StateName, TagValue = viewModel.StateName},
                new ContentTag() {TagKey = ContentTagKey.StateID, TagValue = viewModel.StateAbbreviation}
            };

            var seoContent = _seoContentService.GetSeoContent<SiteIndexStateContent>(new SeoContentOptions
          (
              SiteIndexConst.SiteIndexStateFolder,
              stateAbbreviation + ".xml",
              replaceTags: SetupResultCountsContentTags(viewModel, contentTags, viewModel.StateAbbreviation)
          ));

            //SEO CONTENT FOR STATIC DIVS AT THE BOTTOM OF THE PAGE JUST BELOW OF THE TAB SECTION
            viewModel.SEOContent_NHS_Tab3 = seoContent.Tab3;
            viewModel.SEOContent_NHS_Tab2 = seoContent.Tab2;
            viewModel.SEOContent_NHS_Tab1 = seoContent.Tab1;
            viewModel.SEOContent_NHS_Articles = seoContent.Article;
            viewModel.SEOContent_NHS_Footer = seoContent.Footer;
            viewModel.SEOContent_NHS_Header = seoContent.Header;
            viewModel.SEOContent_NHS_H1 = seoContent.H1;          

            OverwriteMetaFromContent(viewModel, seoContent);

            viewModel.SelectedMarketId = userSessionMarket == 0 ? partner.SelectMetroId.ToType<int>() : userSessionMarket;

            if (!NhsRoute.IsBrandPartnerNhsPro)
            {
                viewModel.NewHomeBuilders = this._brandService.GetBrandsByState(partnerId, viewModel.StateAbbreviation).ToColumns(2);    
            }
            
            
            var priceLow = UserSession.PersonalCookie.PriceLow;
            var priceHigh = UserSession.PersonalCookie.PriceHigh;
            var priceRange = this._lookupService.GetCommonListItems(CommonListItem.PriceRange);

            string customFilter = string.Format("{0}='{1}'", CommunityDBFields.State, viewModel.StateAbbreviation);
            var comms = _communityService.GetSpotlightCommunities(NhsRoute.PartnerId, UserSession.PropertySearchParameters.MarketId, customFilter);
            viewModel.SpotLightCommunities = ConvertToExtendedCommResults(comms);

            viewModel.LowPriceRange = new SelectList(priceRange, "LookupValue", "LookupText", priceLow);
            viewModel.HighPriceRange = new SelectList(priceRange, "LookupValue", "LookupText", priceHigh);

            var markets = _marketService.GetMarkets(NhsRoute.PartnerId).Where(mkt => mkt.StateAbbr == viewModel.StateAbbreviation).ToList();
            SetupMarketCountsFromWebApi(markets);

            viewModel.MarketPoints = GetMarketPoints(markets);

            viewModel.MarketList = markets.ToColumns(3);

            //Add canonical tag
            base.AddCanonicalWithIncludeParams(viewModel, new List<string>() { "state" });
            base.SetupAdParameters(viewModel);
            viewModel.Globals.PartnerInfo = partner;

            // ReSharper disable once Mvc.ViewNotResolved
            return View(viewModel);
        }

        private List<ContentTag> SetupResultCountsContentTags(BaseStateIndexViewModel viewModel, List<ContentTag> contentTags, string stateAbbr)
        {
            contentTags.Add(new ContentTag
            {
                TagKey = ContentTagKey.BedroomRangeLow,
                TagValue = !String.IsNullOrEmpty(viewModel.ResultCounts.Facets.BrRange)
                    ? viewModel.ResultCounts.Facets.BrRange.Split('-')[0].ToFormattedThousands()
                    : "0"
            });
            contentTags.Add(new ContentTag
            {
                TagKey = ContentTagKey.BedroomRangeHigh,
                TagValue = !String.IsNullOrEmpty(viewModel.ResultCounts.Facets.BrRange)
                    ? viewModel.ResultCounts.Facets.BrRange.Split('-')[1].ToFormattedThousands()
                    : "0"
            });
            contentTags.Add(new ContentTag
            {
                TagKey = ContentTagKey.BathroomRangeLow,
                TagValue = !String.IsNullOrEmpty(viewModel.ResultCounts.Facets.BaRange)
                    ? viewModel.ResultCounts.Facets.BaRange.Split('-')[0].ToFormattedThousands()
                    : "0"
            });
            contentTags.Add(new ContentTag
            {
                TagKey = ContentTagKey.BathroomRangeHigh,
                TagValue = !String.IsNullOrEmpty(viewModel.ResultCounts.Facets.BaRange)
                    ? viewModel.ResultCounts.Facets.BaRange.Split('-')[1].ToFormattedThousands()
                    : "0"
            });
            contentTags.Add(new ContentTag
            {
                TagKey = ContentTagKey.SquareFtRangeLow,
                TagValue = !String.IsNullOrEmpty(viewModel.ResultCounts.Facets.SftRange)
                    ? viewModel.ResultCounts.Facets.SftRange.Split('-')[0].ToFormattedThousands()
                    : "0"
            });
            contentTags.Add(new ContentTag
            {
                TagKey = ContentTagKey.SquareFtRangeHigh,
                TagValue = !String.IsNullOrEmpty(viewModel.ResultCounts.Facets.SftRange)
                    ? viewModel.ResultCounts.Facets.SftRange.Split('-')[1].ToFormattedThousands()
                    : "0"
            });
            contentTags.Add(new ContentTag
            {
                TagKey = ContentTagKey.PriceRangeLow,
                TagValue = !String.IsNullOrEmpty(viewModel.ResultCounts.Facets.PrRange)
                    ? viewModel.ResultCounts.Facets.PrRange.Split('-')[0].ToFormattedThousands()
                    : "0"
            });
            contentTags.Add(new ContentTag
            {
                TagKey = ContentTagKey.PriceRangeHigh,
                TagValue = !String.IsNullOrEmpty(viewModel.ResultCounts.Facets.PrRange)
                    ? viewModel.ResultCounts.Facets.PrRange.Split('-')[1].ToFormattedThousands()
                    : "0"
            });
            contentTags.Add(new ContentTag
            {
                TagKey = ContentTagKey.CommunityCount,
                TagValue = viewModel.ResultCounts.CommCount.ToType<String>().ToFormattedThousands()
            });

            //BL are counted too as they are counted on the results count in the search reults page page, ResultList.cshtml 
            contentTags.Add(new ContentTag
            {
                TagKey = ContentTagKey.HomeCount,
                TagValue = (viewModel.ResultCounts.CommAllHomeCount + viewModel.ResultCounts.BlCount).ToType<String>().ToFormattedThousands()
            });
            contentTags.Add(new ContentTag
            {
                TagKey = ContentTagKey.SpecCount,
                TagValue = viewModel.ResultCounts.QmiCount.ToType<String>().ToFormattedThousands()
            });
            contentTags.Add(new ContentTag
            {
                TagKey = ContentTagKey.PlanCount,
                TagValue = (viewModel.ResultCounts.CommAllHomeCount - viewModel.ResultCounts.QmiCount).ToType<String>().ToFormattedThousands()
            });
            contentTags.Add(new ContentTag
            {
                TagKey = ContentTagKey.BuilderCount,
                TagValue = viewModel.ResultCounts.Facets.Brands != null ? viewModel.ResultCounts.Facets.Brands.Count.ToType<String>().ToFormattedThousands() : "0"
            });

            return contentTags;
        }

        [HttpPost]
        public virtual ActionResult ShowMarketBuilders(CommunityDetailViewModel model)
        {
            return NoResultsFound(model);
        }

        public virtual ActionResult ShowMarketBuilders(string area, string statename)
        {
            statename = statename.ReturnSpaceAndDash();
            var viewModel = new SiteIndexViewModel
            {
                PartnerName = _partnerService.GetPartner(NhsRoute.PartnerId).PartnerName
            };

            var market = _marketService.GetMarket(NhsRoute.PartnerId, statename, area, true);

            
            if (market == null)
            {
                IDetailViewModel newModel = GetInvalidPageMode();

                return View(NhsMvc.Default.Views.SiteIndex.SiteIndexNoResults, newModel);
            }
            else
            {
                base.SetupAdParameters(viewModel);


                viewModel.Market = market;
                viewModel.StateName = market.State.StateName;


                var contentTags = new List<ContentTag>
                {
                    new ContentTag {TagKey = ContentTagKey.PartnerName, TagValue = viewModel.PartnerName},
                    new ContentTag {TagKey = ContentTagKey.MarketName, TagValue = viewModel.Market.MarketName},
                    new ContentTag {TagKey = ContentTagKey.MarketId, TagValue = viewModel.Market.MarketId.ToString()},
                    new ContentTag {TagKey = ContentTagKey.StateName, TagValue = viewModel.Market.State.StateName},
                    new ContentTag {TagKey = ContentTagKey.StateID, TagValue = viewModel.Market.State.StateAbbr}
                };
                viewModel.SeoContentTags = contentTags;

                var seoContent = _seoContentService.GetSeoContent<SiteIndexContent>(new SeoContentOptions
                    (
                    SiteIndexConst.SiteIndexFolder,
                    "DefaultMarketNewHomeBuildersIndexHeader.xml",
                    replaceTags: contentTags
                    ));

                viewModel.H1 = seoContent.H1;
                viewModel.OnPageText = seoContent.OnPageText;

                OverwriteMetaFromContent(viewModel, seoContent);

                var searchParams = new SearchParams()
                {
                    NoBoyl = true,
                    LastCached = true,
                    WebApiSearchType = WebApiSearchType.Exact,
                    PartnerId = NhsRoute.PartnerId,
                    MarketId = market.MarketId,
                    PageSize = int.MaxValue
                };

                var data = _apiService.GetResultsWebApi<CommunityItemViewModel>(searchParams, SearchResultsPageType.CommunityResults);

                var searchResults = data.Result; searchResults.ForEach(r => r.MarketName = market.MarketName); 

                // Group Communities by City and for each group create a 3 pages list
                viewModel.MarketCommunities = searchResults.GroupBy(c => c.City).OrderBy(c => c.Key).ToDictionary(a => a.Key,
                                                                                                                     a => new List<CommunityItemViewModel>[] { 
                                                                                                                             a.Take((a.ToList().Count / 3) + 1 ).ToList(), 
                                                                                                                             a.ToList().Skip((a.ToList().Count / 3) + 1).Take((a.ToList().Count / 3) + 1).ToList(),
                                                                                                                             a.ToList().Skip(((a.ToList().Count / 3) + 1) * 2).Take((a.ToList().Count / 3) + 1).ToList()
                                                                                                                           }
                                                                                                                  );

                // Create a 3 pages list for Market Brands
                var brandsPageSize = (market.Brands.Count / 3) + 1;
                viewModel.MarketBrands = new List<Brand>[] { market.Brands.Take(brandsPageSize).ToList(), 
                                                         market.Brands.Skip(brandsPageSize).Take(brandsPageSize).ToList(),
                                                         market.Brands.Skip(brandsPageSize * 2).Take(brandsPageSize).ToList()
                                                       };

                // Create a 3 pages list for Market Brands
                var citiesPageSize = (market.Cities.Count / 3) + 1;
                viewModel.MarketCities = new List<City>[] { market.Cities.Take(citiesPageSize).ToList(), 
                                                         market.Cities.Skip(citiesPageSize).Take(citiesPageSize).ToList(),
                                                         market.Cities.Skip(citiesPageSize * 2).Take(citiesPageSize).ToList()
                                                       };
            }

            return View(viewModel);         
        }

        public virtual ActionResult ShowStateBrand(string stateName, int brandId)
        {
            stateName = stateName.ReturnSpaceAndDash();
            var viewModel = new StateBuilderViewModel
                {
                    PartnerName = _partnerService.GetPartner(NhsRoute.PartnerId).PartnerName,
                    Brand = _brandService.GetBrandById(brandId, NhsRoute.PartnerId)
                };

            //Related with Case 79887 - /home-builders/state-district-of-columbia/brandid-6569 
            stateName = stateName.Replace('-', ' ');
            
            //Invalid or dead brand
            if (viewModel.Brand == null)
            {
                return RedirectPermanent(new List<RouteParam>().ToUrl(Pages.NoResultsFound));
            }            

            string stateAbbreviation = _stateService.GetStateAbbreviation(stateName);
            var stateInfo = _stateService.GetStateInfo(NhsRoute.PartnerId, stateName);

            //Invalid state
            if (stateInfo == null)
            {
                return RedirectPermanent(new List<RouteParam>().ToUrl(Pages.NoResultsFound));
            }

            var stateCoodinates = _stateService.GetStateCoodinates(stateAbbreviation);
            viewModel.StateAbbreviation = stateAbbreviation;
            viewModel.StateName = StringHelper.ToTitleCase(stateInfo.StateName);
            base.AddCanonicalWithIncludeParams(viewModel, new List<string> { "state", "brandid" });

            SetupStateCountsFromWebApi(viewModel, viewModel.StateAbbreviation, brandId);
            var contentTags = new List<ContentTag>
                {
                    new ContentTag {TagKey = ContentTagKey.StateName, TagValue = viewModel.StateName},
                    new ContentTag {TagKey = ContentTagKey.StateID, TagValue = viewModel.StateAbbreviation},
                    new ContentTag {TagKey = ContentTagKey.BrandName, TagValue = viewModel.Brand.BrandName},
                    new ContentTag { TagKey = ContentTagKey.BuilderName, TagValue = viewModel.Brand.BrandName},
                    new ContentTag {TagKey = ContentTagKey.BrandID, TagValue = viewModel.Brand.BrandId.ToString(CultureInfo.InvariantCulture)},
                };
           
            var file = string.Format("{0}_{1}_{2}.xml", stateAbbreviation, viewModel.Brand.BrandId, viewModel.Brand.BrandName);
            var failBackFiles = new List<string>
            {
                string.Format("{0}.xml", stateAbbreviation),
                "Default.xml"
            };
            var seoContent = _seoContentService.GetSeoContent<StateBrandContent>(new SeoContentOptions
                (
                    SiteIndexConst.StateBrandFolder,
                    file,
                    failBackFiles,
                    replaceTags:SetupResultCountsContentTags(viewModel, contentTags, viewModel.StateAbbreviation)
                ));

            viewModel.SEOContent_NHS_Header = seoContent.Header;
            viewModel.SEOContent_NHS_Footer = seoContent.Footer;
            viewModel.SEOContent_NHS_H1 = seoContent.H1;

            OverwriteMetaFromContent(viewModel, seoContent);
           
            var userSessionMarket = UserSession.PersonalCookie.MarketId;
            var partner = _partnerService.GetPartner(NhsRoute.PartnerId);

            viewModel.SelectedMarketId = userSessionMarket == 0 ? partner.SelectMetroId.ToType<int>() : userSessionMarket;
            var priceLow = UserSession.PersonalCookie.PriceLow;
            var priceHigh = UserSession.PersonalCookie.PriceHigh;
            var priceRange = this._lookupService.GetCommonListItems(CommonListItem.PriceRange);

            viewModel.LowPriceRange = new SelectList(priceRange, "LookupValue", "LookupText", priceLow);
            viewModel.HighPriceRange = new SelectList(priceRange, "LookupValue", "LookupText", priceHigh);

            viewModel.MarketList = this._marketService.GetMarketsByState(NhsRoute.PartnerId, viewModel.StateAbbreviation).ToList();
            viewModel.MarketList.Insert(0, new Market { MarketId = 0, MarketName = DropDownDefaults.Area });

            var markets = _marketService.GetMarketsByBrand(NhsRoute.PartnerId, viewModel.StateAbbreviation, brandId);
            SetupMarketCountsFromWebApi(markets, brandId);

            if (markets.Count == 0) //Brand/builder does not have any communities in this state, do a 404
            {
                var @params = new List<RouteParam>();
                return RedirectPermanent(@params.ToUrl(Pages.NoResultsFound));
            }

            viewModel.BrandMarkets = markets.ToColumns(3);

            viewModel.CenterLat = stateCoodinates.Latitude.ToType<decimal>();
            viewModel.CenterLng = stateCoodinates.Longitude.ToType<decimal>();
            viewModel.ZoomLevel = stateCoodinates.Zoom;
            viewModel.MarketPoints = GetMarketPoints(markets);


            string customFilter = string.Format("{0}='{1}' AND brand_id= {2}", CommunityDBFields.State, viewModel.StateAbbreviation, brandId);
            var comms = _communityService.GetSpotlightCommunities(NhsRoute.PartnerId, UserSession.PropertySearchParameters.MarketId, customFilter);
            viewModel.SpotLightCommunities = ConvertToExtendedCommResults(comms);
            viewModel.ContainsCondoTownCommunities = stateInfo.ContainsCondoTownCommunities;
            viewModel.ContainsGolfCommunities = stateInfo.ContainsGolfCommunities;
            viewModel.ContainsWaterFrontCommunities = stateInfo.ContainsWaterFrontCommunities;

            base.SetupAdParameters(viewModel);

            // ReSharper disable once Mvc.ViewNotResolved
            return View(viewModel);
        }

        public virtual ActionResult StateAmenity(string statename, string amenityname)
        {
            statename = statename.ReturnSpaceAndDash();
            var viewModel = new StateAmenityViewModel();
            viewModel.AmenityName = amenityname;
            string stateAbbreviation = _stateService.GetStateAbbreviation(statename);
            string amenityType = string.Empty;
            var amenityPath = string.Empty;

            if (string.IsNullOrWhiteSpace(stateAbbreviation))
            {
                return RedirectTo404();
            }

            if (amenityname == SEOAmenityType.StateCondoTown)
            {
                amenityType = CommunityAmenity.CondoTownHome;
                viewModel.AmenityDesc = LanguageHelper.NewCondoAndTownhomeCommunities;
                viewModel.AmenityDesc2 = LanguageHelper.CondosAndTownhomes;
                
                viewModel.AmenityMarket = Pages.AmenityMarketCondo;
                viewModel.ShowCondoTownLink = false;
                amenityPath = SiteIndexConst.AmenitiesCondoFolder;
            }
            else if (amenityname == SEOAmenityType.StateGolf)
            {
                amenityType = CommunityAmenity.GolfCource;
                viewModel.AmenityDesc = LanguageHelper.NewGolfCourseCommunities;
                viewModel.AmenityDesc2 = LanguageHelper.GolfHomes;
               
                viewModel.AmenityMarket = Pages.AmenityMarketGolf;
                viewModel.ShowGolfCourseLink = false;
                amenityPath = SiteIndexConst.AmenitiesGolfFolder;
            }
            else if (amenityname == SEOAmenityType.StateWaterfront)
            {
                amenityType = CommunityAmenity.Waterfront;
                viewModel.AmenityDesc = LanguageHelper.WaterfrontNewHomeCommunities;
                viewModel.AmenityDesc2 = LanguageHelper.WaterfrontHomes;
                
                viewModel.AmenityMarket = Pages.AmenityMarketWaterfront;
                viewModel.ShowWaterFrontLink = false;
                amenityPath = SiteIndexConst.AmenitiesWaterfrontFolder;
            }
            else
            {
                return RedirectTo404();
            }

            var stateCoodinates = _stateService.GetStateCoodinates(stateAbbreviation);
            int partnerId = NhsRoute.PartnerId;
            var priceLow = UserSession.PersonalCookie.PriceLow;
            var priceHigh = UserSession.PersonalCookie.PriceHigh;
            var priceRange = this._lookupService.GetCommonListItems(CommonListItem.PriceRange);
            viewModel.StateAbbreviation = stateAbbreviation;
            viewModel.LowPriceRange = new SelectList(priceRange, "LookupValue", "LookupText", priceLow);
            viewModel.HighPriceRange = new SelectList(priceRange, "LookupValue", "LookupText", priceHigh);
            var markets = _marketService.GetMarketsByAmenity(viewModel.StateAbbreviation, amenityType, partnerId).ToList();
            SetupMarketCountsFromWebApi(markets, 0, amenityType);

            viewModel.LearnMoreMarket = markets.ToColumns(3);
            viewModel.AreaList = markets;
            viewModel.AreaList.Insert(0, new Market { MarketId = 0, MarketName = DropDownDefaults.Area });

            viewModel.Globals.PartnerInfo = _partnerService.GetPartner(NhsRoute.PartnerId);
            viewModel.PartnerName = viewModel.Globals.PartnerInfo.PartnerName;
            viewModel.StateName = StringHelper.ToTitleCase(statename);                      

            base.AddCanonicalWithIncludeParams(viewModel, new List<string>() { "state", "amenity" });
            base.SetupAdParameters(viewModel);
            viewModel.CenterLat = stateCoodinates.Latitude.ToType<decimal>();
            viewModel.CenterLng = stateCoodinates.Longitude.ToType<decimal>();
            viewModel.ZoomLevel = stateCoodinates.Zoom;
            viewModel.MarketPoints = GetMarketPoints(markets);

            SetupStateCountsFromWebApi(viewModel, viewModel.StateAbbreviation, 0, amenityname);
            var contentTags = new List<ContentTag>
            {
                new ContentTag {TagKey = ContentTagKey.StateName, TagValue = viewModel.StateName},
                new ContentTag() {TagKey = ContentTagKey.StateID, TagValue = viewModel.StateAbbreviation}
            };

            var seoContent = _seoContentService.GetSeoContent<AmenitiesContent>(new SeoContentOptions
            (
                amenityPath, 
                stateAbbreviation + ".xml", 
                replaceTags: SetupResultCountsContentTags(viewModel, contentTags, viewModel.StateAbbreviation)
            ));

            viewModel.SeoHeaderContent = seoContent.Header;
            viewModel.SeoFootherContent = seoContent.Footer;
            viewModel.SeoH1Content = seoContent.H1;

            OverwriteMetaFromContent(viewModel, seoContent);

            // ReSharper disable once Mvc.ViewNotResolved
            return View(viewModel);
        }

        public virtual ActionResult StateAmenityRedirect(int selectedMarketId, string amenityName, int? priceLow, int? priceHigh)
        {
            IList<RouteParam> param = new List<RouteParam>();

            UserSession.PersonalCookie.MarketId = selectedMarketId;
            param.Add(new RouteParam(RouteParams.Market, selectedMarketId.ToString()));

            if (priceLow != null)
            {
                param.Add(new RouteParam(RouteParams.PriceLow, priceLow.ToString()));
                UserSession.PersonalCookie.PriceLow = priceLow.ToType<int>();
            }

            if (priceHigh != null)
            {
                param.Add(new RouteParam(RouteParams.PriceHigh, priceHigh.ToString()));
                UserSession.PersonalCookie.PriceHigh = priceLow.ToType<int>();
            }

            if (amenityName == SEOAmenityType.StateCondoTown)
                param.Add(new RouteParam(RouteParams.HomeType, "MF"));
            else if (amenityName == SEOAmenityType.StateGolf)
                param.Add(new RouteParam(RouteParams.GolfCourse, true));
            else
                param.Add(new RouteParam(RouteParams.WaterFront, true));


            return Redirect(Pages.CommunityResults, param);
        }

        public virtual ActionResult NoResultsFound()
        {
            IDetailViewModel model = GetInvalidPageMode();
            var metaRegistrar = new MetaRegistrar(_pathMapper);
            var metas = metaRegistrar.GetRobotsMeta("noodp, noydir, noindex");
            base.AddNoIndex(metaRegistrar, metas.ToList());

            OverrideDefaultMeta(model as BaseViewModel, metas);
            base.SetupAdParameters((CommunityDetailViewModel)model);
            return View(NhsMvc.Default.Views.SiteIndex.SiteIndexNoResults, model);
        }

        IDetailViewModel GetInvalidPageMode()
        {
            IDetailViewModel model = new CommunityDetailViewModel();
            ((CommunityDetailViewModel)model).ExternalMediaLinks = new List<MediaPlayerObject>();
            model.GreenPrograms = new List<GreenProgram>();
            model.Promotions = new List<Promotion>();
            model.States = (from s in _stateService.GetPartnerStates(NhsRoute.PartnerId) select new SelectListItem { Text = s.StateName, Value = s.StateAbbr });
            model.PriceLowRange = (from l in _lookupService.GetCommonListItems(CommonListItem.MinPrice) select new SelectListItem { Text = l.LookupText, Value = l.LookupValue });
            model.PriceHighRange = (from l in _lookupService.GetCommonListItems(CommonListItem.MaxPrice) select new SelectListItem { Text = l.LookupText, Value = l.LookupValue });
            model.SearchText = string.Empty;
            Response.StatusCode = 404;
            return model;
        }


        [HttpPost]
        public virtual ActionResult NoResultsFound(CommunityDetailViewModel model)
        {
            if (string.IsNullOrEmpty(model.SearchText))
            {
                ModelState.AddModelError("SearchText", @"location required.");
                IDetailViewModel newModel = GetInvalidPageMode();
                base.SetupAdParameters((CommunityDetailViewModel)newModel);
                return View(NhsMvc.Default.Views.SiteIndex.SiteIndexNoResults, newModel);
            }

            IList<RouteParam> param = new List<RouteParam>();

            // Searching by Zip
            if (CommonUtils.IsZip(model.SearchText))
            {
                param.Add(new RouteParam(RouteParams.SearchText, model.SearchText, RouteParamType.QueryString));
            }
            // Searching by city or zip
            else if (!string.IsNullOrEmpty(model.SearchText))
            {
                param.Add(new RouteParam(RouteParams.SearchText, model.SearchText, RouteParamType.QueryString));
                param.Add(new RouteParam(RouteParams.State, model.SelectedStateAbbr));
                UserSession.PersonalCookie.State = model.SelectedStateAbbr;
            }
            // Input error handler
            else
            {
                ModelState.AddModelError("SearchText", @"invalid data, city or zip required.");
                CommunityDetailBaseViewModel vm = CommunityDetailExtensions.GetInactivePropertyViewModel(true, _stateService, _lookupService) as CommunityDetailViewModel;

                // ReSharper disable Asp.NotResolved
                return View(vm);
                // ReSharper restore Asp.NotResolved
            }

            if (!string.IsNullOrEmpty(model.PriceLow))
            {
                param.Add(new RouteParam(RouteParams.PriceLow, model.PriceLow));
                UserSession.PersonalCookie.PriceLow = int.Parse(model.PriceLow);
            }

            if (!string.IsNullOrEmpty(model.PriceHigh))
            {
                param.Add(new RouteParam(RouteParams.PriceHigh, model.PriceHigh));
                UserSession.PersonalCookie.PriceHigh = int.Parse(model.PriceHigh);
            }

            //redirect to location handler page
            return this.Redirect(Pages.LocationHandler, param);
        }

        private string GetMarketPoints(IEnumerable<Market> markets)
        {
            var marketsPoints =
                markets.Where(m => m.TotalCommunities > 0).Select(
                    p =>
                        new
                        {
                            p.Latitude,
                            p.Longitude,
                            p.MarketName,
                            p.TotalCommunities,
                            p.TotalCustomBuilders,
                            p.MarketId,
                            p.State.StateName,
                            p.State.StateAbbr,
                            UserFriendlyUrls = (NhsRoute.PartnerId == NhsRoute.BrandPartnerId && 
                                                _marketDFUService.IsMarketIdValidDFU(NhsRoute.BrandPartnerId, RouteParams.MarketId.ToType<int>()))
                        }).ToJson();
            return marketsPoints;
        }

        private List<ExtendedCommunityResult> ConvertToExtendedCommResults(List<CommunityResult> communities)
        {
            var extCommResults = new List<ExtendedCommunityResult>();

            foreach (var result in communities)
            {
                extCommResults.Add(new ExtendedCommunityResult
                                       {
                                           BrandId = result.BrandId,
                                           BrandName = result.BrandName,
                                           BuilderId = result.BuilderId,
                                           BuilderUrl = result.BuilderUrl,
                                           BuildOnYourLot = result.AlertDeletedFlag,
                                           City = result.City,
                                           CommunityId = result.CommunityId,
                                           CommunityImageThumbnail = result.CommunityImageThumbnail,
                                           CommunityName = result.CommunityName,
                                           CommunityType = result.CommunityType,
                                           County = result.County,
                                           FeaturedListingId = result.FeaturedListingId,
                                           Green = result.Green,
                                           HasHotHome = result.HasHotHome,
                                           Latitude = result.Latitude,
                                           Longitude = result.Longitude,
                                           MarketId = result.MarketId,
                                           MatchingHomes = result.MatchingHomes,
                                           PostalCode = result.PostalCode,
                                           PriceHigh = result.PriceHigh,
                                           PriceLow = result.PriceLow,
                                           PromoId = result.PromoId,
                                           State = result.State,
                                           SubVideoFlag = result.SubVideoFlag,
                                           Video_url = result.Video_url,
                                           HasVideo = result.SubVideoFlag.ToUpper() == "Y",
                                           MarketName = _marketService.GetMarketName(NhsRoute.PartnerId, result.MarketId),
                                           StateName = _stateService.GetStateName(result.State)
                                       });
            }

            return extCommResults;

        }

        

        private void SetupMarketCountsFromWebApi(List<Market> marketList, int brandId = 0, string communityAmenityType = null)
        {
            // Get community counts by market from cache from Web API
            var searchParams = new SearchParams()
            {
                CountsOnly = true,
                NoBoyl = true,
                LastCached = true,
                WebApiSearchType = WebApiSearchType.Exact,
                PartnerId = NhsRoute.PartnerId
            };

            // override community total counts by market from the web Api
            foreach (var market in marketList)
            {
                searchParams.MarketId = market.MarketId;

                if (brandId != 0)
                    searchParams.BrandId = brandId;
                if (!string.IsNullOrEmpty(communityAmenityType))
                {
                    switch (communityAmenityType)
                    {
                        case CommunityAmenity.GolfCource:
                            searchParams.GolfCourse = true; break;
                        case CommunityAmenity.Waterfront:
                            searchParams.Waterfront = true; break;
                        case CommunityAmenity.CondoTownHome:
                            searchParams.MultiFamily = true; break;
                    }
                }
                
                var data = _apiService.GetResultsWebApi<CommunityItemViewModel>(searchParams, SearchResultsPageType.CommunityResults);
                if (data.ResultCounts != null)
                    market.TotalCommunities = data.ResultCounts.CommCount;

            }

            marketList = marketList.Where(m => m.TotalCommunities > 0).ToList();
        }


        private List<ExtendedCommunityResult> ConvertCommunityResult(List<NhsCommunityResult> communities)
        {

            var comms = communities.Select(c => new ExtendedCommunityResult()
            {
                SearchKey = c.SearchKey,
                IsBasicListing = c.IsBasicListing,
                AlertDeletedFlag = c.AlertDeletedFlag,
                BrandId = c.BrandId,
                BrandName = c.BrandName,
                BuilderId = c.BuilderId,
                BuilderUrl = c.BuilderUrl,
                BuildOnYourLot = c.AlertDeletedFlag,
                City = c.City,
                CommunityId = c.CommunityId,
                CommunityImageThumbnail = c.CommunityImageThumbnail,
                CommunityName = c.CommunityName,
                CommunityType = c.CommunityType,
                County = c.County,
                FeaturedListingId = c.FeaturedListingId,
                Green = c.Green,
                HasHotHome = c.HasHotHome,
                Latitude = c.Latitude,
                Longitude = c.Longitude,
                MarketId = c.MarketId,
                MatchingHomes = c.MatchingHomes,
                PostalCode = c.PostalCode,
                PriceHigh = c.PriceHigh,
                PriceLow = c.PriceLow,
                PromoId = c.PromoId,
                State = c.State,
                SubVideoFlag = c.SubVideoFlag,
                Video_url = c.Video_url,
                HasVideo = c.SubVideoFlag.ToUpper() == "Y",
                HasEvents = c.HasEvents,
                QuickMoveinHomesCount = c.QuickMoveinHomesCount
            }).ToList();

            comms.ForEach(c => { var mkr = _marketService.GetMarket(NhsRoute.PartnerId, c.MarketId, false); if (mkr != null) c.MarketName = mkr.MarketName; });

            return comms;
        }

        private void SetupStateCountsFromWebApi(BaseStateIndexViewModel viewModel, string stateAbbr = "", int brandId = 0, string communityAmenityType = null)
        {
            // Get community counts by market from cache from Web API
            var searchParams = new SearchParams()
            {
                CountsOnly = true,
                LastCached = true,
                State = stateAbbr,
                WebApiSearchType = WebApiSearchType.Exact,
                PartnerId = NhsRoute.PartnerId
            };

            if (brandId > 0)
                searchParams.BrandId = brandId;

            if (!string.IsNullOrEmpty(communityAmenityType))
            {
                switch (communityAmenityType)
                {
                    case SEOAmenityType.StateCondoTown:
                        searchParams.MultiFamily = true;
                        break;
                    case SEOAmenityType.StateGolf:
                        searchParams.GolfCourse = true;
                        break;
                    case SEOAmenityType.StateWaterfront:
                        searchParams.Waterfront = true;
                        break;
                }
            }

            var data = _apiService.GetResultsWebApi<CommunityItemViewModel>(searchParams, SearchResultsPageType.CommunityResults);
            if (data.ResultCounts != null)
                viewModel.ResultCounts = data.ResultCounts;
        }

        #endregion
    }

}
