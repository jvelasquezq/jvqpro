﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Interface;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using SearchAlert = Nhs.Mvc.Domain.Model.Profiles.SearchAlert;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class ChatLeadGenController : ApplicationController
    {
        private readonly IStateService _stateService;
        private readonly IMarketService _marketService;
        private readonly ILookupService _lookupService;
        private readonly ISearchAlertService _searchAlertService;
        private readonly IMapService _mapService;

        public ChatLeadGenController(IStateService stateService, IMarketService marketService,
            ILookupService lookupService, ISearchAlertService searchAlertService, IMapService mapService)
        {
            _stateService = stateService;
            _marketService = marketService;
            _lookupService = lookupService;
            _searchAlertService = searchAlertService;
            _mapService = mapService;
        }

        #region actions

        public virtual ActionResult Show()
        {
            var viewModel = new ChatLeadGenViewModel();
            BindChatLeadGenViewModel(viewModel);
            
            // Set up the ads
            base.SetupAdParameters(viewModel);
            viewModel.Globals.PartnerLayoutConfig.ShowRightAdColumn = true;
            return View(NhsMvc.Default.Views.ChatLeadGen.ChatLeadGen, viewModel);
        }

        public virtual ActionResult Save(ChatLeadGenViewModel model)
        {
            // Set up the ads
            base.SetupAdParameters(model);
            model.Globals.PartnerLayoutConfig.ShowRightAdColumn = true;
            ValidateInformation(model);
            if (ModelState.IsValid)
            {
                var searchAlert = new SearchAlert();
                var postalCode = string.IsNullOrEmpty(model.ZipCodeUserInfo) ? String.Empty : model.ZipCodeUserInfo.Trim();

                // UserId
                //Create dummy profile
                var profile = new Profile(Configuration.PartnerId);
                profile.Email = model.EmailAddress;
                profile.FirstName = model.FirstName;
                profile.LastName = model.LastName;
                profile.PostalCode = postalCode;

                if (!String.IsNullOrEmpty(model.Phone))
                {
                    profile.DayPhone = CommonUtils.ConvertPhoneToNumber(model.Phone);
                    profile.DayPhone = StringHelper.FormatPhone(string.Empty, profile.DayPhone, string.Empty);
                }
                    
                profile.UserID = searchAlert.UserGuid = profile.Email; //put email in guid field

                // Search by state & area
                if (model.State != null)
                {
                    searchAlert.State = model.State;
                    searchAlert.MarketId = int.Parse(model.Area);
                    if (model.City != null)
                    {
                        searchAlert.City = model.City;
                    }
                }
                // Search by zip
                else if (model.Zip != String.Empty)
                {
                    searchAlert.PostalCode = model.Zip;
                    searchAlert.Radius = model.Radius;
                    searchAlert.MarketId = _marketService.GetMarketIdFromPostalCode(model.Zip, Configuration.PartnerId);
                    searchAlert.State = _marketService.GetStateNameForMarket(searchAlert.MarketId.ToType<int>());
                }

                //Market
                var market = _marketService.GetMarket(NhsRoute.PartnerId, searchAlert.MarketId.ToType<int>(), true);
                searchAlert.MarketName = market.MarketName;

                searchAlert.Lat = market.LatLong.Latitude;
                searchAlert.Lng = market.LatLong.Longitude;
                _searchAlertService.SetGeoLocation(market, model.State, model.City, model.Zip, model.Radius, searchAlert, _mapService);

                // Min/Max price
                searchAlert.PriceMin = int.Parse(model.PriceFrom);
                searchAlert.PriceMax = int.Parse(model.PriceTo);

                //Builder
                var brand = market.Brands.FirstOrDefault(b => b.BrandId == model.Builder.ToType<int>());

                if (brand != null)
                    searchAlert.BuilderName = brand.BrandName;

                // Options
                if (model.Bedrooms != -1)
                    searchAlert.Bedrooms = model.Bedrooms;
                searchAlert.IsSingleFamily = model.TypeSingle;
                searchAlert.IsCondo = model.Condo;
                searchAlert.HasPool = model.AmenitiyPool;
                searchAlert.HasGolfCourse = model.AmenitiyGolf;
                searchAlert.IsGatedCommunity = model.AmenitiyGated;
                if (model.School != null)
                {
                    searchAlert.SchoolDistrictId = int.Parse(model.School);
                }
                if (model.Builder != null)
                {
                    searchAlert.BuilderId = int.Parse(model.Builder);
                }

                // Search name
                searchAlert.Title = LanguageHelper.ChatAgentRecommendedCommunities;
                
                // Creates alert
                _searchAlertService.CreateAlert(NhsRoute.PartnerId, searchAlert, profile, 0, model.Comments);

                // Hide the form and show the success message
                model.DoneAddingInfo = true;
            }
            BindChatLeadGenViewModel(model);
            return View(NhsMvc.Default.Views.ChatLeadGen.ChatLeadGen, model);
        }
        
        #endregion

        #region methods
        
        private void ValidateInformation(ChatLeadGenViewModel model)
        {
            // User info zip code
            if (!model.IsInternational)
            {
                if (string.IsNullOrEmpty(model.ZipCodeUserInfo))
                {
                    ModelState.AddModelError("ZipCodeUserInfo", LanguageHelper.MSG_ZipCodeRequired);
                }
                else if (_marketService.GetMarketIdFromPostalCode(model.ZipCodeUserInfo, NhsRoute.PartnerId) == 0)
                {
                    ModelState.AddModelError("ZipCodeUserInfo", LanguageHelper.MSG_ACCOUNT_INVALID_ZIP);
                }
            }

            // No location selected
            if (string.IsNullOrEmpty(model.State) && string.IsNullOrEmpty(model.Area) && string.IsNullOrEmpty(model.Zip))
                ModelState.AddModelError("Area", LanguageHelper.MSG_SEARCH_ALERT_NO_LOCATION);

            // Search by zip
            else if (string.IsNullOrEmpty(model.State) && string.IsNullOrEmpty(model.Area))
            {
                var marketId = _marketService.GetMarketIdFromPostalCode(model.Zip, NhsRoute.PartnerId);
                if (!StringHelper.IsValidZip(model.Zip))
                    ModelState.AddModelError("Zip", LanguageHelper.MSG_REGISTER_INVALID_ZIP);
                else if (marketId == 0)
                    ModelState.AddModelError("Zip", LanguageHelper.MSG_REGISTER_INVALID_ZIP);
            }

            // Search by state & area
            else if (string.IsNullOrEmpty(model.Zip) && string.IsNullOrEmpty(model.Area))
                ModelState.AddModelError("Area", LanguageHelper.MSG_ADV_SEARCH_AREA_P15);

            // Price range
            if (string.IsNullOrEmpty(model.PriceFrom) || string.IsNullOrEmpty(model.PriceTo))
                ModelState.AddModelError("PriceFrom", LanguageHelper.MSG_SEARCH_ALERT_NO_PRICE);
            else if (int.Parse(model.PriceFrom) > int.Parse(model.PriceTo))
                ModelState.AddModelError("PriceTo", LanguageHelper.MSG_ADV_SEARCH_PRICE_RANGE);
        }

        private void BindChatLeadGenViewModel(ChatLeadGenViewModel model)
        {
            // Dynamic drop down lists.             
            var states = _stateService.GetSearchAlertStates(NhsRoute.PartnerId);
            model.StateList = new SelectList(states, "StateAbbr", "StateName");
            model.AreaList = GetAreaListItems(model.State);
            model.CityList = GetCityListItems(model.Area);
            model.SchoolList = GetSchoolListItems(model.Area);
            model.BuilderList = GetBuilderListItems(model.Area);

            // Fixed drop down lists. 
            var radius = _lookupService.GetCommonListItems(CommonListItem.Radius);
            var minPrices = _lookupService.GetCommonListItems(CommonListItem.MinPrice);
            var maxPrices = _lookupService.GetCommonListItems(CommonListItem.MaxPrice);
            var bedrooms = _lookupService.GetCommonListItems(CommonListItem.Bedrooms);
            model.RadiusList = new SelectList(radius, "LookupValue", "LookupText");
            model.MinPriceList = new SelectList(minPrices, "LookupValue", "LookupText");
            model.MaxPriceList = new SelectList(maxPrices, "LookupValue", "LookupText");
            model.BedroomsList = new SelectList(bedrooms, "LookupValue", "LookupText");
            
        }

        private IList<SelectListItem> GetAreaListItems(string state)
        {
            var list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = DropDownDefaults.Area, Value = string.Empty });   // Option item

            // Gets areas
            if (!string.IsNullOrEmpty(state))
            {
                var markets = _marketService.GetMarketsByState(NhsRoute.PartnerId, state);
                list.AddRange(markets.Select(m => new SelectListItem { Text = m.MarketName, Value = m.MarketId.ToString() }));
            }

            return list;
        }
        private IList<SelectListItem> GetCityListItems(string marketVal)
        {
            int marketId = string.IsNullOrEmpty(marketVal) ? 0 : int.Parse(marketVal);

            var list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = DropDownDefaults.City, Value = string.Empty });   // Option item

            // Gets cities
            if (marketId != 0)
            {
                var market = _marketService.GetMarket(NhsRoute.PartnerId, marketId, false);
                list.AddRange(market.Cities.Select(c => new SelectListItem { Text = c.CityName, Value = c.CityName }));
            }

            return list;
        }
        private IList<SelectListItem> GetSchoolListItems(string marketVal)
        {
            int marketId = string.IsNullOrEmpty(marketVal) ? 0 : int.Parse(marketVal);

            var list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = DropDownDefaults.AllSchoolDistricts, Value = string.Empty });   // Option item

            // Gets schools
            if (marketId != 0)
            {
                var market = _marketService.GetMarket(NhsRoute.PartnerId, marketId, true);
                list.AddRange(market.SchoolDistricts.Select(s => new SelectListItem { Text = s.DistrictName, Value = s.DistrictId.ToString() }));
            }

            return list;
        }
        private IList<SelectListItem> GetBuilderListItems(string marketVal)
        {
            int marketId = string.IsNullOrEmpty(marketVal) ? 0 : int.Parse(marketVal);

            var list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = DropDownDefaults.AllBuilders, Value = string.Empty });   // Option item

            // Gets brands
            if (marketId != 0)
            {
                var market = _marketService.GetMarket(NhsRoute.PartnerId, marketId, true);
                list.AddRange(market.Brands.Select(b => new SelectListItem { Text = b.BrandName, Value = b.BrandId.ToString() }));
            }

            return list;
        }
        #endregion
    }
}