﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.CMS;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Mvc.Routing.Configuration;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class ResourceCenterController
    {
        #region Members
        private const string _noiseWords = "a the and i or is";
        #endregion

        #region Actions
        public virtual ActionResult ShowArticle(string articleName)
        {
            var friendlyArticleName = articleName.Replace("-", " ");
            long articleId;
            var xml = _cmsService.GetArticleByTitle(friendlyArticleName, out articleId);
            ActionResult actionResult;

            if (string.IsNullOrWhiteSpace(xml) || articleId < 0)
            {
                var partnerUrl = NhsRoute.PartnerSiteUrl;
                string url = string.IsNullOrWhiteSpace(partnerUrl)
                    ? string.Format("~/{0}", Pages.GetResourceCenterLink(NhsRoute.BrandPartnerId))
                    : string.Format("~/{0}/{1}", NhsRoute.PartnerSiteUrl, Pages.GetResourceCenterLink(NhsRoute.BrandPartnerId));

                actionResult = RedirectPermanent(url);
            }
            else
            {
                var articleSmartFormType = _cmsService.GetArticleSmartFormType(articleId);

                switch (articleSmartFormType)
                {
                    case EktronConstants.MPAContainer:
                        actionResult = ShowMultiPartArticle(articleName);
                        break;
                    case EktronConstants.MPASection:
                        actionResult = ShowMultiPartArticleSection(articleName);
                        break;
                    case EktronConstants.FhSlideshow:
                        actionResult = ShowFeaturedHomesSlideshow(friendlyArticleName, xml, articleId);
                        break;
                    default:
                        actionResult = ShowArticlePage(articleName, xml, articleId, friendlyArticleName);
                        break;
                }
            }

            return actionResult;
        }

        public virtual ActionResult ShowSearchResults(string searchtext)
        {
            searchtext = string.IsNullOrEmpty(searchtext) ? string.Empty : searchtext.Trim().Replace("'", " ");
            var linkUrl = NhsRoute.CurrentRoute.HostUrl;

            if (linkUrl.ToLower().Contains("{articlesearch}") || linkUrl.ToLower().Contains("articlesearch"))
                return ShowResults(searchtext);

            if (searchtext.Equals("Search by topic (kitchens, mortgage approval, building process, etc.)", StringComparison.CurrentCultureIgnoreCase))
                searchtext = string.Empty;

            var model = new ResourceCenterViewModels.ArticleResultsViewModel
            {
                ArticlesList = new List<RcArticle>()
            };

            if (!NhsRoute.ShowMobileSite)
            {
                model.TrendingNow =
                    _cmsService.GetTrendingArticles(Configuration.EktronResourceCenterTaxonomyId,
                        Configuration.EktronLanguageId).ToList();
            }
            //model.Globals.ResourceCenterCategories = _cmsService.GetCategoriesHierarchy().ToList();

            if (!string.IsNullOrEmpty(searchtext))
            {
                var filerNoise = FilterNoise(searchtext);
                ShowKeywords(model, filerNoise[0], filerNoise[1], filerNoise[2], filerNoise[3]);
                var articleList = _cmsService.GetArticleResultsByKeywords(filerNoise[0], filerNoise[1], filerNoise[2],
                    filerNoise[3], Configuration.EktronResourceCenterTaxonomyId, Configuration.EktronLanguageId);
                var rcArticles = articleList as List<RcArticle> ?? articleList.ToList();

                if (rcArticles.Any())
                {
                    model.ArticlesList = rcArticles;
                }
                else
                    model.NoResultsMsg = LanguageHelper.NoResultsFound;
            }
            else
            {
                model.NoResultsMsg = LanguageHelper.PleaseSpecifyASearchPhrase;
                model.KeyWords = string.Empty;
            }

            //canonical
            if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
                AddCanonicalTag(model);
            else
                AddProResourceCenterCanonicalTag(model);

            //Carry through ads from other pages into Resource Center
            SetupAdParameters(model);

            return View("ShowSearchResults", model);
        }

        public virtual ActionResult ShowResults(string articleSearch)
        {
            if (articleSearch.Equals("Search by topic (kitchens, mortgage approval, building process, etc.)", StringComparison.CurrentCultureIgnoreCase))
                articleSearch = string.Empty;

            var paramz = new List<RouteParam>
            {
                new RouteParam(RouteParams.SearchText, articleSearch, RouteParamType.Friendly, true, true)                        
            };

            var targetPage = Pages.AgentResourcesSearch;
            var url = paramz.ToUrl(targetPage);

            return Redirect(url);
        }

        public virtual ActionResult ShowArticleSlideShow(string articleName)
        {
            return ShowArticle(articleName);
        }
        #endregion

        #region Private Methods

        private ActionResult ShowArticlePage(string articleName, string xml, long articleId, string friendlyArticleName)
        {
            //if (NhsRoute.IsMobile)
            //    return View(new ResourceCenterViewModels.ArticleViewModel());

            ActionResult actionResult;
            var article = CmsReader.ParseArticle(xml, articleName);
            string lastCategory = null;
            bool isSlideShow = false;

            if (UserSession.GetItem("LastCMSCategory") != null)
                lastCategory = UserSession.GetItem("LastCMSCategory").ToString();

            if (article == null)
            {
                actionResult = RedirectPermanent("~/" + Pages.GetResourceCenterLink(NhsRoute.BrandPartnerId));
            }
            else
            {
                IRCArticleViewModel model;

                if (NhsRoute.CurrentRoute.Controller.Action == this.ActionNames.ShowArticleSlideShow)
                {
                    model = new ResourceCenterViewModels.ArticleSlideShowViewModel
                    {
                        SlideShow = _cmsService.ParseSlideShowXml(xml)
                    };
                    isSlideShow = true;
                }
                else
                {
                    model = new ResourceCenterViewModels.ArticleViewModel
                    {
                        TrendingNow =
                            _cmsService.GetTrendingArticles(Configuration.EktronResourceCenterTaxonomyId,
                                                            Configuration.EktronLanguageId).ToList()
                    };
                }

                model.IsArticlesPage = true;
                model.Article = article;

                //model.Autor = CmsReader.ParseAuthor(_cmsService.GetArticleById(article.Author.ToType<long>()).Html);
                var authorId = article.Author.ToType<long>();
                model.Autor = _cmsService.GetAllAuthorsInfo(Configuration.EktronResourceCenterTaxonomyId,
                                                            Configuration.EktronLanguageId)
                                         .FirstOrDefault(author => author.AuthorId == authorId) ?? new RcAuthor();

                //model.RelatedArticles = article.RelatedArticles.Select(articleid => _cmsService.GetRcArticleById(articleid, Configuration.EktronResourceCenterTaxonomyId, Configuration.EktronLanguageId)).Where(p => p != null);

                model.RelatedArticles =
                    article.RelatedArticles.Select(articleid => _cmsService.GetArticleById(articleid).ParseArticle())
                           .Where(p => p != null);

                model.ParentCategories =
                    _cmsService.GetCategoriesByArticleId(articleId, lastCategory).OrderBy(c => c.CategoryId).ToList();

                if (model.ParentCategories.Count == 0)
                    model.ParentCategories =
                        _cmsService.GetCategoriesByArticleId(articleId, null).OrderBy(c => c.CategoryId).ToList();

                //Metas
                var metas = _cmsService.GetMetasForArticle(friendlyArticleName, Configuration.EktronResourceCenterTaxonomyId,
                                                           Configuration.EktronLanguageId);
                base.OverrideDefaultMeta(model as BaseViewModel, metas);

                //Canonicals
                if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
                    AddCanonicalWithExcludeParams(model as BaseViewModel, new List<string>());
                else
                    AddProResourceCenterCanonicalTag(model as BaseViewModel);
                

                //Carry through ads from other pages into Resource Center
                SetupAdParameters(model as BaseViewModel);

                (model as BaseViewModel).Globals.AdController.AddArticleParameter(friendlyArticleName);

                actionResult = View(isSlideShow?  NhsMvc.Default.Views.ResourceCenter.ShowArticleSlideShow:"ShowArticle", model);
            }

            return actionResult;
        }

        private string[] FilterNoise(string search)
        {
            string[] results = { string.Empty, string.Empty, string.Empty, string.Empty };
            var words = search.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            var currentKeyword = 1;
            foreach (var word in words.Where(word => !IsNoiseWord(word)))
            {
                switch (currentKeyword)
                {
                    case 1:
                        results[0] = word;
                        break;
                    case 2:
                        results[1] = word;
                        break;
                    case 3:
                        results[2] = word;
                        break;
                    case 4:
                        results[3] = word;
                        break;
                }
                currentKeyword += 1;
            }

            return results;
        }

        private void ShowKeywords(ResourceCenterViewModels.ArticleResultsViewModel model, string keyWord1, string keyWord2, string keyWord3, string keyWord4)
        {
            if (!string.IsNullOrWhiteSpace(keyWord1) ||
                !string.IsNullOrWhiteSpace(keyWord2) ||
                !string.IsNullOrWhiteSpace(keyWord3) ||
                !string.IsNullOrWhiteSpace(keyWord4)
                )
            {
                model.KeyWords = @"Keyword(s): " + keyWord1 + @" " + keyWord2 + @" " + keyWord3 + @" " + keyWord4;
            }
            else
                model.KeyWords = string.Empty;
        }

        private bool IsNoiseWord(string word)
        {
            return (_noiseWords.IndexOf(word.ToLower(), StringComparison.Ordinal) + 1) > 0;
        }

        #endregion
    }
}
