﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Nhs.Library.Business;
using Nhs.Library.Business.Seo;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects.Constants;
using Nhs.Utility.Common;
using Nhs.Utility.Data.Paging;
using Nhs.Web.UI.AppCode.Extensions;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.Lib;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class BasicCommunityDetailController : BaseDetailController
    {
        private readonly ICommunityService _communityService;
        private readonly IPartnerService _partnerService;
        private readonly IMarketService _marketService;
        private readonly IStateService _stateService;
        private readonly ILookupService _lookupService;
        private readonly IBuilderService _builderService;
        private readonly IApiService _apiService;
        private readonly IAffiliateLinkService _affiliateLinkService;
        private readonly ISeoContentService _seoContentService;

        public BasicCommunityDetailController(ICommunityService communityService, IPartnerService partnerService, IMarketService marketService, IStateService stateService, ILookupService lookupService, ApiService apiService, IBuilderService builderService, IAffiliateLinkService affiliateLinkService, ISeoContentService seoContentService)
        {
            _communityService = communityService;
            _partnerService = partnerService;
            _marketService = marketService;
            _stateService = stateService;
            _lookupService = lookupService;
            _apiService = apiService;
            _builderService = builderService;
            _affiliateLinkService = affiliateLinkService;
            _seoContentService = seoContentService;
        }

        private bool AddCanonicalLocal(int communityId)
        {
            //add a canonical if community is active on brand partner 
            //add a canonical if we are on a partner private-label site
            return _communityService.IsCommunityActiveOnParentBrandPartner(communityId, NhsRoute.BrandPartnerId) ||
                   NhsRoute.PartnerId != NhsRoute.BrandPartnerId;
        }

        public virtual ActionResult Show(int communityid, string optionals)
        {
            var model = new BasicCommunityDetailViewModel();
            var community = _communityService.GetCommunity(communityid);
            var partnerMaskIndex = _partnerService.GetPartnerMaskIndex(NhsRoute.PartnerId);

            // Add an extra parameter to identify from SEO inactive data
            if (community == null && string.IsNullOrEmpty(RouteParams.Msg.Value<string>()))
                return RedirectPermanent(GetInactiveUrl());

            //Community not valid
            if (community == null) //Check if community exists.
                model = CommunityDetailExtensions.GetInactiveBasicCommunityPropertyViewModel(true, _stateService, _lookupService) as BasicCommunityDetailViewModel;
            else
            {
                var partnerSortValue = community.PartnerMask.Substring(partnerMaskIndex - 1, 1);
                model.IsBasicCommunity = "F" == partnerSortValue;
                model.CommunityId = community.CommunityId;
                model.BuilderId = community.BuilderId;
                var addCanonical = AddCanonicalLocal(communityid);
                var addNoIndexTag = !addCanonical;

                if (!model.IsBasicCommunity)
                    return RedirectPermanent(RedirectionHelper.GetCommunityDetailPage(), community.ToCommunityDetail());

                if (NhsRoute.ShowMobileSite)
                    GetBasicCommunityDetailInfoMobile(model, community);
                else
                    GetBasicCommunityDetailInfo(model, community);

                try
                {
                    var contentTags = new List<ContentTag>
                    {
                        new ContentTag { TagKey = ContentTagKey.MarketName, TagValue = community.Market.MarketName },
                        new ContentTag { TagKey = ContentTagKey.MarketId, TagValue = community.Market.MarketId.ToString(CultureInfo.InvariantCulture) },
                        new ContentTag { TagKey = ContentTagKey.MarketState, TagValue = community.State.StateAbbr },
                        new ContentTag { TagKey = ContentTagKey.MarketStateName, TagValue = community.State.StateName},
                        new ContentTag { TagKey = ContentTagKey.StateName, TagValue = community.State.StateName },
                        new ContentTag {TagKey = ContentTagKey.CityName, TagValue = community.City},
                        new ContentTag { TagKey = ContentTagKey.CommunityName, TagValue = community.CommunityName},
                        new ContentTag { TagKey = ContentTagKey.BrandName, TagValue = community.Builder.Brand.BrandName},
                        new ContentTag {TagKey = ContentTagKey.Zip, TagValue = community.PostalCode}
                    };

                    model.SeoContentTags = contentTags;

                    var seoContent = _seoContentService.GetSeoContent<SeoBaseContent>(new SeoContentOptions(@"/BasicCommunityDetail/",
                                            "BasicCommunityDetailSeo.xml", replaceTags: contentTags));
                    model.H1 = seoContent.H1;

                    //TODO: To add this
                    if (addNoIndexTag)
                        //AddNoIndex(metaRegistrar, metas);
                        AddNoIndex(seoContent);


                    var resizeCommands = ImageSizes.GetResizeCommands(ImageSizes.HomeMain);
                    var parameters = (resizeCommands.HasValues) ? "?" + resizeCommands : String.Empty;
                    var image = community.SpotlightThumbnail;
                    if (!string.IsNullOrEmpty(image))
                        image = image.RemoveAllSizes() + parameters;
                    OverwriteMetaFromContent(model, seoContent, FacebookHelper.GetFacebookMeta(model.CommunityName, model.BrandName, image ?? ""));
                }
                catch (Exception ex)
                {
                    ErrorLogger.LogError(ex);
                }

            }
            // ReSharper disable once Mvc.ViewNotResolved
            return View(model);
        }

        [HttpPost]
        public virtual ActionResult Sort(int commId, int page, string sortColumn)
        {
            var sortOrder = sortColumn.FromString<SortOrder>();
            Community community = null;
            var model = new BasicCommunityDetailViewModel();
            if (_communityService.IsCommunityActiveOnParentBrandPartner(commId, NhsRoute.PartnerId))
                community = _communityService.GetCommunity(commId);
            model = GetExtendedHomeResult(model, community, page, sortOrder);
            return PartialView(NhsMvc.Default.Views.BasicCommunityDetail.BasicCommunityResultData, model);
        }

        private void GetBasicCommunityDetailInfo(BasicCommunityDetailViewModel model, Community community)
        {
            BindCommunityInformation(model, community);

            var isBilled = _communityService.GetCommunityBilledStatus(NhsRoute.PartnerId, community.BuilderId, community.CommunityId, community.MarketId);
            model.IsBilled = isBilled;
            model.ShowSocialIcons = ShowSocialIcons(isBilled);

            GetNearByCommunities(model, community.CommunityId);
            GetAffiliateLinks(model);
            LogHomeResults(model);

            var img = _communityService.GetBuilderMap(community.AllImages);
            if (img != null)
                model.BuilderMapUrl = img.ImagePath + img.ImageTypeCode + "_" + img.ImageName;

            BindAds(model);
        }

        private void GetNearByCommunities(BasicCommunityDetailViewModel model, int communityId)
        {
            model.ShowCta = false;
            model.NearByComms = _communityService.GetNearbyCommunities(NhsRoute.PartnerId, communityId, 0, 2, false, true).ToList();
            model.NearByCommsObject = model.NearByComms.GroupBy(n => new { n.Latitude, n.Longitude })
                                                       .Select(g => new { g.Key.Latitude, g.Key.Longitude, Name = g.Count(), MarketPoints = g })
                                                       .ToJson();
            model.NearByCommsSmall = model.NearByComms.Any() ? model.NearByComms.Take(4).ToList() : model.NearByComms;

            if (model.NearByComms.Count >= 2) return;

            var partnerMaskIndex = _partnerService.GetPartnerMaskIndex(NhsRoute.PartnerId);
            var commIds = _communityService.GetCommunitiesByMarketId(model.MarketId)
                                           .Where(c => !c.ListingTypeFlag.Equals("B", StringComparison.InvariantCultureIgnoreCase) &&
                                                        c.PartnerMask.Substring(partnerMaskIndex - 1, 1) != "0" &&
                                                        c.PartnerMask.Substring(partnerMaskIndex - 1, 1) != "F" &&
                                                        c.Status.Equals("Active", StringComparison.CurrentCultureIgnoreCase)).ToList();
            model.ShowCta = commIds.Count > 0;
        }

        private void GetAffiliateLinks(BasicCommunityDetailViewModel model)
        {
            model.AffiliateLinksData = new AffiliateLinksData();

            // Affiliate Links Logic is only for NHS and MOVE
            if (NhsRoute.IsBrandPartnerNhsPro) return;

            var links = _affiliateLinkService.GetAffiliateLinks(Pages.BasicCommunity, NhsRoute.PartnerId);
            var freeCreditScore = links.LinksList.Find(x => x.Keyword.IsCaseInsensitiveEqual(AffiliateLinksTypes.FreeCreditScore));

            if (freeCreditScore != null)
                model.AffiliateLinksData.FreeCreditScoreFormArea = freeCreditScore.ToLink();

            var mortgageRates = links.LinksList.Find(x => x.Keyword.IsCaseInsensitiveEqual(AffiliateLinksTypes.MortgageRates));
            if (mortgageRates != null)
                model.AffiliateLinksData.MortageRatesFormArea = mortgageRates.ToLink();
        }

        private void BindCommunityInformation(BasicCommunityDetailViewModel model, Community community)
        {
            model.IsPageCommDetail = true;
            model.CommunityCity = community.SalesOffice.City;
            model.ZipCode = community.SalesOffice.ZipCode;
            model.SalesOfficeAddress1 = community.SalesOffice.Address1;
            model.SalesOfficeAddress2 = community.SalesOffice.Address2;
            model.State = community.State.StateName;
            model.CommunityName = community.CommunityName;
            model.BuilderName = community.Builder.BuilderName;
            model.MarketName = community.Market.MarketName;
            model.BrandId = community.BrandId;
            model.BrandName = community.Brand.BrandName ?? string.Empty;
            model.HomesAvailable = community.HomeCountNoSpec > 0 ? HomesAvailableLabel(community.HomeCountNoSpec) : LanguageHelper.NewHomesComingSoon;
            model.PriceLow = community.PriceLow.ToType<string>();
            model.PriceHigh = community.PriceHigh.ToType<string>();
            model.BuilderName = community.Builder.BuilderName ?? string.Empty;
            model.ShowMortgageLink = community.ShowMortgageLink == "Y";
            model.MarketId = community.MarketId;
            model.MarketName = community.Market.MarketName;
            model.StateAbbr = community.Market.StateAbbr;
            model.CommunityId = community.CommunityId;
            model.BuilderId = community.BuilderId;
            model.Latitude = (double)community.Latitude;
            model.Longitude = (double)community.Longitude;
            model.DrivingDirections = string.Empty;
            model.IsCommunityMultiFamily = community.IsCondo.ToType<bool>() || community.IsTownHome.ToType<bool>();
            model.UnderneathMapCommName = community.CommunityName;
            model.EnableRequestAppointment = false;
            model.IsBasicCommunity = true;
            model.SelectedTab = CommunityDetailTabs.AllCommunities;
            model.CorporationName = community.Builder.ParentBuilder == null ? model.BuilderName : community.Builder.ParentBuilder.BuilderName ?? string.Empty;
            model.DisplaySqFeet = FormattedSqFeet(community.SqFtLowNoSpec, community.SqFtHighNoSpec);
            model = GetExtendedHomeResult(model, community, 0, SortOrder.Status);
            model.PageUrl = model.Globals.CurrentUrl;
            model.UnderneathMapAddress = community.SalesOffice.Address1 + " <br >" + community.SalesOffice.City + ", " + community.State.StateName +
                                         " " + community.SalesOffice.ZipCode;
        }

        private static void LogHomeResults(BasicCommunityDetailViewModel model)
        {
            if (model.HomeResultsApi == null)
                model.HomesAvailable = "New homes coming soon";
            else
                foreach (var home in model.HomeResultsApi)
                {
                    var logger = new ImpressionLogger
                    {
                        CommunityId = model.CommunityId,
                        BuilderId = model.BuilderId,
                        PartnerId = NhsRoute.PartnerId.ToString(CultureInfo.InvariantCulture),
                        Refer = UserSession.Refer
                    };

                    if (home.IsSpec == 0)
                        logger.AddPlan(home.HomeId);
                    else
                        logger.AddSpec(home.HomeId);

                    logger.LogView(LogImpressionConst.BasicHomeImpression);
                }
        }

        private BasicCommunityDetailViewModel GetExtendedHomeResult(BasicCommunityDetailViewModel model, Community community, int page, SortOrder sortOrder)
        {

            var resultsView = GetCommunityHomeResults(community);

            var sortActions = new SortHomeActions();
            var shortedHomes = sortActions.SortOptions[sortOrder](resultsView);
            var extendedHomeResults = shortedHomes.Where(p => p.IsSpec == 0).ToList();
            var pageSize = (extendedHomeResults.Count <= 40) ? extendedHomeResults.Count : 12;
            var showNewHomesExtraTab = extendedHomeResults.Count < community.HomeCount.ToType<int>();
            var pagedList = pageSize > 0 ? extendedHomeResults.ToPagedList(page, pageSize) : null;
            model.ShowNewHomesExtraTab = showNewHomesExtraTab;
            model.HomeResultsApi = pagedList;
            model.AllNewHomesCount = extendedHomeResults.Count();
            model.FilteredHomesCount = extendedHomeResults.Count();
            model.BcType = community.BCType;
            return model;
        }

        private List<HomeItem> GetCommunityHomeResults(Community community)
        {
            var parameters = new SearchParams
            {
                CommId = community.CommunityId,
                PartnerId = NhsRoute.PartnerId,
                BuilderId = community.BuilderId,
                MarketId = community.MarketId,
                WebApiSearchType = WebApiSearchType.Exact,
                PageSize = 1000,
                PageNumber = 1
            };

            var results = _apiService.GetResultsWebApi<HomeItem>(parameters, SearchResultsPageType.HomeResults);
            return results.Result ?? new List<HomeItem>();
        }

        private string FormattedSqFeet(int sqFtLow, int sqFtHigh)
        {
            if (sqFtLow == 0 && sqFtHigh == 0) return string.Empty;

            if (sqFtLow == sqFtHigh) return string.Format("({0} " + LanguageHelper.MSG_SQ_FT + ")", sqFtHigh);

            return string.Format("({0} - {1} " + LanguageHelper.MSG_SQ_FT + ")", sqFtLow, sqFtHigh);

        }
        private string HomesAvailableLabel(int? homeCount)
        {
            if (homeCount == null || homeCount == 0) return string.Empty;
            return homeCount > 1 ? string.Format("{0} " + LanguageHelper.NewHomes, homeCount) : string.Format("{0} " + LanguageHelper.NewHome, homeCount);
        }

        private void BindAds(CommunityDetailViewModel model)
        {
            UserSession.AdBuilderIds.Clear();
            model.Globals.AdController.AddMarketParameter(model.MarketId);
            model.Globals.AdController.AddCommunityParameter(model.CommunityId);
            model.Globals.AdController.AddPriceParameter(model.PriceLow.ToType<int>(), model.PriceHigh.ToType<int>());
            model.Globals.AdController.AddStateParameter(model.StateAbbr);
            model.Globals.AdController.AddBuilderParameter(model.BuilderId);
            UserSession.AdBuilderIds.Add(model.BuilderId);
            model.Globals.AdController.AddCityParameter(model.CommunityCity);
            model.Globals.AdController.AddZipParameter(model.ZipCode);

            var builderIds = _builderService.GetMarketBuilders(NhsRoute.PartnerId, model.MarketId);
            foreach (var builder in builderIds)
            {
                model.Globals.AdController.AddBuilderParameter(builder.BuilderId);
                UserSession.AdBuilderIds.Add(builder.BuilderId);
            }

            if (model.HomeResults == null || !model.HomeResults.Any()) return;

            foreach (var home in model.HomeResults)
            {
                model.Globals.AdController.AddCityParameter(home.City);
                model.Globals.AdController.AddZipParameter(home.PostalCode);
                model.Globals.AdController.AddBuilderParameter(home.BuilderId);
                UserSession.AdBuilderIds.Add(home.BuilderId);
            }
        }
    }
}
