﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Bdx.Web.Api.Objects.Constants;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Helpers.Tv;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Utility.Constants;
using Nhs.Web.UI.AppCode.Filters;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResultMobile;
using Nhs.Web.UI.AppCode.ViewModels.Tv;
using Resources;
using StructureMap;
using UrlHelper = Nhs.Utility.Web.UrlHelper;

namespace Nhs.Web.UI.AppCode.Controllers
{
    [PartnersAuthorize(NhsRoutePartnerTypeEnum.UseNhsRouteBrandPartnerId,  PartnersConst.Pro)]
    public partial class TvController : ApplicationController
    {
        private readonly ITvService _tvService;
        private readonly IMarketService _marketService;
        private readonly IApiService _apiService;

        public TvController(ITvService tvService, IMarketService marketService, IApiService apiService)
        {
            _tvService = tvService;
            _marketService = marketService;
            _apiService = apiService;
        }


        public virtual ActionResult RedirectToDefault()
        {
            return RedirectPermanent(Pages.NewHomeSourceTv + "?utm_source=nhsslashtv&utm_medium=media&utm_campaign=tvurls");
        }

        public virtual ActionResult ShowTvHome()
        {
            var marketId = _tvService.GetPartnerMarketIdIfOnlyHaveOne(NhsRoute.PartnerId, NhsRoute.BrandPartnerId);
            var tvMarkets = _tvService.GetTvMarkets(NhsRoute.PartnerId).ToList();

            if (marketId > 0)
            {
                ActionResult redirectPermanent;
                if (ActionResult(tvMarkets, marketId, out redirectPermanent)) return redirectPermanent;
            }

            marketId = UserSession.PersonalCookie.MarketId;

            if (marketId > 0)
            {
                ActionResult redirectPermanent;
                if (ActionResult(tvMarkets, marketId, out redirectPermanent)) return redirectPermanent;
            }

            if (NhsRoute.IsMobileDevice)
                foreach (var market in tvMarkets)
                    market.Image = GlobalResources14.Default.images.icons.map_poi_blue_png;

            var model = new NhsTvViewModel { TvMarketPoints = tvMarkets.ToJson() };
            var page = NhsRoute.IsBrandPartnerNhsPro
                ? NhsMvc.PartnerBrandGroupViews_88.Views.Tv.ShowTvHome
                : "ShowTvHome";
            
            SetupAdParameters(model);
            return View(page, model);
        }

        private bool ActionResult(List<TvMarket> tvMarkets, int marketId, out ActionResult redirectPermanent)
        {
            var market = tvMarkets.FirstOrDefault(p => p.MarketId == marketId);
            if (market != null)
            {
                {
                    redirectPermanent = RedirectPermanent(Pages.NewHomeSourceTv + "/" + market.Keyword.ToLowerCase());
                    return true;
                }
            }
            redirectPermanent = null;
            return false;
        }

        public virtual ActionResult ShowTvMarket(string market)
        {
            var tvMarkets = _tvService.GetTvMarkets(NhsRoute.PartnerId);
            var marketTv = tvMarkets.FirstOrDefault(p => p.Keyword.Equals(market, StringComparison.CurrentCultureIgnoreCase));
            if (marketTv == null)
            {
                return RedirectPermanent(Pages.NewHomeSourceTv);
            }

            var marketContent = _tvService.GetTvMarketContent(marketTv.Keyword, marketTv.MarketId, NhsRoute.PartnerId);

            var searchParams = new SearchParams
            {
                MarketId = marketTv.MarketId,
                WebApiSearchType = WebApiSearchType.Exact,
                PageNumber = 1,
                PageSize = 99999,
                BuilderId = 0,
                SortBy = SortBy.HomeStatus,
                NoBoyl = false,
                PartnerId =  NhsRoute.PartnerId
            };

            //Get the results from the web api
            var resultsWepApi = _apiService.GetResultsWebApi<CommunityItemMobileViewModel>(searchParams, SearchResultsPageType.CommunityResults);
            var results = resultsWepApi.Result ?? new List<CommunityItemMobileViewModel>();

            if (marketContent.Latitude.Equals(0) && marketContent.Longitude.Equals(0))
            {
                marketContent.Latitude = marketTv.Latitude;
                marketContent.Longitude = marketTv.Longitude;
            }

            if (marketContent.Videos != null && marketContent.Videos.Any())
            {
                marketContent.Videos = marketContent.Videos.OrderBy(x => x.Order).ToList();
                marketContent.Videos.ForEach(video =>
                {
                    var youtubeVideoId = UrlHelper.GetYoutubeVideoId(video.url);
                    video.VideoId = youtubeVideoId;
                    video.Thumbnail = string.Format("http://img.youtube.com/vi/{0}/hqdefault.jpg", youtubeVideoId);
                });    
            }            

            var model = new TvMarketMobileViewModel
            {
                Market = marketTv,
                MarketContent = marketContent,
                TvMarketPoints = marketContent.Communities.ToJson(),
                CommunitiesForTvBuilderPoints = marketContent.CommunitiesForTvBuilder.ToJson(),
                KeyWord = marketTv.Keyword,
                CommunitiesForMobile = results.Where(x => marketContent.Communities.Exists( comm => comm.Id == x.Id && comm.IsFeature)).ToList(),
                Promos = marketContent.Promotions.AsQueryable().ToExactColumns(3)
            };

            FillPersonalCookie(model.Market.MarketId);

            var page = NhsRoute.IsBrandPartnerNhsPro
                ? NhsMvc.PartnerBrandGroupViews_88.Views.Tv.ShowTvMarket
                : "ShowTvMarket";

            base.SetupAdParameters(model);            

            return View(page, model);
        }

        private void FillPersonalCookie(int marketId)
        {
            var market = _marketService.GetMarket(marketId);
            UserSession.PersonalCookie.MarketId = marketId;
            UserSession.PersonalCookie.State = market.StateAbbr;
            UserSession.PersonalCookie.Save();
        }

        #region Contest Modals

        [HttpGet]
        public virtual ActionResult ShowModal(int marketId, bool showSignUp = true)
        {
            var model = new TvContestModalViewModel();
            FillMarketDataForModal(model, marketId, showSignUp);
            return PartialView(NhsMvc.Default.Views.Tv.PartialViews.PartialTvContestModal, model);
        }

        private void FillMarketDataForModal(TvContestModalViewModel model, int marketId, bool showSignUp)
        {
            var tvMarkets = _tvService.GetTvMarkets(NhsRoute.PartnerId);

            if (marketId <= 0) return;

            var market = tvMarkets.FirstOrDefault(p => p.MarketId == marketId);

            if (market == null) return;

            var marketContent = _tvService.GetTvMarketContent(market.Keyword, market.MarketId, NhsRoute.PartnerId);
            model.MarketName = market.Keyword.ToTitleCase();
            model.PdfLinkUrl = marketContent.ContestUrl;
            model.MarketId = marketId;
            model.SignMeUpSpetialOffers = true;
            model.ShowSignUp = showSignUp;
            model.Email = UserSession.UserProfile.Email;
            model.FullName = (UserSession.UserProfile.FirstName + " " + UserSession.UserProfile.LastName).Trim();
        }

        [HttpGet]
        public virtual ActionResult ShowModalThanks(TvContestModalViewModel model)
        {
            ModelState.Clear();
            return PartialView(NhsMvc.Default.Views.Tv.PartialViews.PartialTvContestModalThanks, model);
        }

        [HttpPost]
        public virtual ActionResult SaveContest(TvContestModalViewModel model)
        {
            var actionResult = ShowModal(model.MarketId, model.ShowSignUp);
            try
            {
                if (ModelState.IsValid)
                {

                    bool spetialOffersSuccess;
                    var success = SendToResposys(model, out spetialOffersSuccess);

                    if (success && spetialOffersSuccess)
                    {
                        if (string.IsNullOrWhiteSpace(UserSession.UserProfile.Email))
                        {
                            UserSession.UserProfile.Email = model.Email;
                        }
                        actionResult = ShowModalThanks(model);
                    }
                    else
                    {
                        ModelState.AddModelError("form", @"There is a problem in your request please try again.");
                    }

                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("form", ex.Message);
            }

            return actionResult;
        }

        private bool SendToResposys(TvContestModalViewModel model, out bool spetialOffersSuccess)
        {
            var nlService = ObjectFactory.GetInstance<INewsletterService>();
            var tvMarkets = _tvService.GetTvMarkets(NhsRoute.PartnerId);
            var market = tvMarkets.FirstOrDefault(p => p.MarketId == model.MarketId);

            var success = nlService.SendContestRequest(model.Email, model.FullName ?? string.Empty,
                                                       model.MarketId, NhsRoute.BrandPartnerId);

            spetialOffersSuccess = true;

            if (model.SignMeUpSpetialOffers)
            {
                spetialOffersSuccess = nlService.SendRequest(model.Email,
                                                             model.MarketId,
                                                             market.Keyword.ToTitleCase(),
                                                             NhsRoute.PartnerId,
                                                             model.PartnerName,
                                                             NhsRoute.PartnerSiteUrl,
                                                             NhsRoute.BrandPartnerId);
            }
            return success;
        }

        #endregion
    }
}
