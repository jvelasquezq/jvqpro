﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Constants.Route;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers.MarketMaps;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Library.Helpers.Seo;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Web.UI.AppCode.Extensions;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class CommunityResultsController
    {       

        private void GetCitiesAndZipCodeViewModel(ref CommunityHomeResultsViewModel model, SearchParams searchParams)
        {
            // split zipcodes into 3 columns so we can sort columns vertically
            var itemsCount = int.Parse(Configuration.ZipCodesResultsByMarket);
            var zipCodes = _marketService.GetZipCodesByMarket(searchParams.MarketId, searchParams.City, itemsCount);
            model.ZipCodesCols = zipCodes.ToColumns(4);
            model.CitiesCols = model.Market.Cities.ToColumns(4);
        }

        private void SeoCommunitiesContentViewModel(ref CommunityHomeResultsViewModel model)
        {
            var seoContentManager = new SeoContentManager();
            var markets = _marketService.GetMarkets(NhsRoute.PartnerId);
            var marketId = model.Market.MarketId;

            var communitiesNames =
                (string[])
                    WebCacheHelper.GetObjectFromCache(WebCacheConst.WebCacheKeyPrefix + WebCacheConst.PartnerMarkets,
                        true);
            if (communitiesNames == null)
            {
                communitiesNames = seoContentManager.GetSeoPathFiles(SeoTemplateType.Nhs_CommunityName_Srp);
                WebCacheHelper.AddObjectToCache(communitiesNames,
                    WebCacheConst.WebCacheKeyPrefix + WebCacheConst.PartnerMarkets,
                    new TimeSpan(0, WebCacheConst.DefaultMins, 0), true);
            }

            var seoCommunities = new List<SEOCommunity>();

            foreach (
                var fileName in
                    communitiesNames.Select(
                        comm => comm.Substring(comm.LastIndexOf(@"\", StringComparison.Ordinal) + 1))
                        .Where(fileName => fileName.IndexOf("Default", StringComparison.Ordinal) == -1))
            {
                var commName = string.Empty;
                var marketName = string.Empty;
                var tempstate = string.Empty;
                try
                {
                    commName = fileName.Substring(0, fileName.IndexOf("-", StringComparison.Ordinal));
                    marketName = fileName.Substring(fileName.IndexOf("-", StringComparison.Ordinal) + 1,
                        (fileName.IndexOf("_", StringComparison.Ordinal) -
                         fileName.IndexOf("-", StringComparison.Ordinal) - 4));
                    tempstate = fileName.Substring(fileName.IndexOf("_", StringComparison.Ordinal) - 2, 2);
                }
                catch (Exception ex)
                {
                }

                if (string.IsNullOrEmpty(marketName) && string.IsNullOrEmpty(tempstate)) continue;
                var market = markets.FirstOrDefault(m => m.MarketName.ToLower() == marketName.ToLower());
                if (market != null)
                    seoCommunities.Add(new SEOCommunity()
                    {
                        Name = commName,
                        MarketName = marketName,
                        State = tempstate,
                        MarketId = market.MarketId
                    });
            }

            seoCommunities = seoCommunities.Where(c => c.MarketId == marketId).OrderBy(c => c.Name).ToList();
            model.SeoCommunityCols = seoCommunities.ToColumns(4);
        }

        private void SetContentManagerViewModel(ICommunityHomeResultsViewModel communityHomeResultsViewModel,
                                                      SearchParams searchParams, Market market)
        {
            var contentTags = new List<ContentTag>();
            
            var searchType = communityHomeResultsViewModel.SrpPageType == SearchResultsPageType.CommunityResults
                ? SrpTypeEnum.CommunityResults
                : SrpTypeEnum.HomeResults;

            communityHomeResultsViewModel.SeoType = searchType;

            if (RouteParams.Inventory.Value<bool>() )
            {
                if (searchParams.IsMultiLocationSearch)
                    communityHomeResultsViewModel.SeoTemplateType = SeoTemplateType.Synthetic_Search_QMI;
                else
                    communityHomeResultsViewModel.SeoTemplateType = !string.IsNullOrEmpty(searchParams.City) ? SeoTemplateType.Nhs_Market_City_QMI_Srp : SeoTemplateType.Nhs_Market_QMI_Srp;

                if (!string.IsNullOrEmpty(searchParams.City))
                    communityHomeResultsViewModel.City = searchParams.City;
            }
            else if (RouteParams.HotDeals.Value<bool>() )
            {
                if (searchParams.IsMultiLocationSearch)
                    communityHomeResultsViewModel.SeoTemplateType = SeoTemplateType.Synthetic_Search_HotDeals;
                else
                    communityHomeResultsViewModel.SeoTemplateType = SeoTemplateType.Nhs_Market_HotDeals_Srp;
            }
            else if (RouteParams.ComingSoon.Value<bool>())
            {
                if (searchParams.IsMultiLocationSearch)
                    communityHomeResultsViewModel.SeoTemplateType = SeoTemplateType.Synthetic_Search_ComingSoon;
                else
                    communityHomeResultsViewModel.SeoTemplateType = SeoTemplateType.Nhs_Market_Coming_Srp;
            }
            else if (RouteParams.BrandId.Value<int>() > 0)
            {
                if (searchParams.IsMultiLocationSearch)
                    communityHomeResultsViewModel.SeoTemplateType = SeoTemplateType.Synthetic_Search_Builder;
                else if (!String.IsNullOrEmpty(searchParams.City))
                {
                    communityHomeResultsViewModel.SeoTemplateType = (searchParams.SrpType ==
                                                                     SearchResultsPageType.CommunityResults)
                        ? SeoTemplateType.Community_City
                        : SeoTemplateType.Home_City;
                    communityHomeResultsViewModel.City = searchParams.City;
                }
                else if (!String.IsNullOrEmpty(searchParams.PostalCode))
                {
                    communityHomeResultsViewModel.SeoTemplateType = (searchParams.SrpType ==
                                                                     SearchResultsPageType.CommunityResults)
                        ? SeoTemplateType.Community_PostalCode
                        : SeoTemplateType.Home_PostalCode;
                    communityHomeResultsViewModel.Zip = searchParams.PostalCode;
                }
                else if (!String.IsNullOrEmpty(searchParams.County))
                {
                    communityHomeResultsViewModel.SeoTemplateType = (searchParams.SrpType ==
                                                                     SearchResultsPageType.CommunityResults)
                        ? SeoTemplateType.Community_County
                        : SeoTemplateType.Home_County;
                    communityHomeResultsViewModel.County = searchParams.County;
                }
                else
                {
                    communityHomeResultsViewModel.SeoTemplateType = (searchParams.SrpType ==
                                                                     SearchResultsPageType.CommunityResults)
                        ? SeoTemplateType.Community_Market
                        : SeoTemplateType.Home_Market;
                }
                communityHomeResultsViewModel.BrandId = searchParams.BrandId;
                communityHomeResultsViewModel.BrandName =
                    _brandService.GetBrandById(communityHomeResultsViewModel.BrandId, NhsRoute.BrandPartnerId).BrandName;
            }
            else if (searchParams.Waterfront)
            {
                if (searchParams.IsMultiLocationSearch)
                    communityHomeResultsViewModel.SeoTemplateType = SeoTemplateType.Synthetic_Search_WaterFront;
                else if (!String.IsNullOrEmpty(searchParams.City))
                {
                    communityHomeResultsViewModel.SeoTemplateType = (searchParams.SrpType ==
                                                                     SearchResultsPageType.CommunityResults)
                        ? SeoTemplateType.Waterfront_Community_City
                        : SeoTemplateType.Waterfront_Home_City;
                    communityHomeResultsViewModel.City = searchParams.City;
                }
                else //Uses market template in case there was no city specified
                {
                    communityHomeResultsViewModel.SeoTemplateType = (searchParams.SrpType ==
                                                                     SearchResultsPageType.CommunityResults)
                        ? SeoTemplateType.Waterfront_Community_Market
                        : SeoTemplateType.Waterfront_Home_Market;
                }
            }
            else if (searchParams.GolfCourse)
            {
                if (searchParams.IsMultiLocationSearch)
                    communityHomeResultsViewModel.SeoTemplateType = SeoTemplateType.Synthetic_Search_GolfCourse;
                else if (!String.IsNullOrEmpty(searchParams.City))
                {
                    communityHomeResultsViewModel.SeoTemplateType = (searchParams.SrpType ==
                                                                     SearchResultsPageType.CommunityResults)
                        ? SeoTemplateType.Golf_Community_City
                        : SeoTemplateType.Golf_Home_City;

                    communityHomeResultsViewModel.City = searchParams.City;
                }
                else //Uses market template in case there was no city specified
                {
                    communityHomeResultsViewModel.SeoTemplateType = (searchParams.SrpType ==
                                                                     SearchResultsPageType.CommunityResults)
                        ? SeoTemplateType.Golf_Community_Market
                        : SeoTemplateType.Golf_Home_Market;
                }
            }
            else if (searchParams.MultiFamily)
            {
                if (searchParams.IsMultiLocationSearch)
                    communityHomeResultsViewModel.SeoTemplateType = SeoTemplateType.Synthetic_Search_MultiFamily;
                else if (!String.IsNullOrEmpty(searchParams.City))
                {
                    communityHomeResultsViewModel.SeoTemplateType = (searchParams.SrpType ==
                                                                     SearchResultsPageType.CommunityResults)
                        ? SeoTemplateType.MF_Community_City
                        : SeoTemplateType.MF_Home_City;

                    communityHomeResultsViewModel.City = searchParams.City;
                }
                else //Uses market template in case there was no city specified
                {
                    communityHomeResultsViewModel.SeoTemplateType = (searchParams.SrpType ==
                                                                     SearchResultsPageType.CommunityResults)
                        ? SeoTemplateType.MF_Community_Market
                        : SeoTemplateType.MF_Home_Market;
                }
            }
            else if (searchParams.Adult)
            {
                if (searchParams.IsMultiLocationSearch)
                    communityHomeResultsViewModel.SeoTemplateType = SeoTemplateType.Synthetic_Search_Adult;
                else if (!String.IsNullOrEmpty(searchParams.City))
                {
                    communityHomeResultsViewModel.SeoTemplateType = (searchParams.SrpType ==
                                                                     SearchResultsPageType.CommunityResults)
                        ? SeoTemplateType.Adult_Community_City
                        : SeoTemplateType.Adult_Home_City;

                    communityHomeResultsViewModel.City = searchParams.City;
                }
                else //Uses market template in case there was no city specified
                {
                    communityHomeResultsViewModel.SeoTemplateType = (searchParams.SrpType ==
                                                                     SearchResultsPageType.CommunityResults)
                        ? SeoTemplateType.Adult_Community_Market
                        : SeoTemplateType.Adult_Home_Market;
                }
            }
            else if (!string.IsNullOrEmpty(searchParams.SchoolDistrictIds))
            {
                contentTags.Add(new ContentTag
                {
                    TagKey = ContentTagKey.SchoolDistrict,
                    TagValue = communityHomeResultsViewModel.ResultCounts.Facets.SchoolDistricts != null &&
                    communityHomeResultsViewModel.ResultCounts.Facets.SchoolDistricts.Count > 0
                        ? communityHomeResultsViewModel.ResultCounts.Facets.SchoolDistricts.First(
                            schoolDistrict => schoolDistrict.Key.ToString() == searchParams.SchoolDistrictIds).Value
                        : string.Empty
                });
                contentTags.Add(new ContentTag
                {
                    TagKey = ContentTagKey.SchoolDistrictId,
                    TagValue = !string.IsNullOrEmpty(searchParams.SchoolDistrictIds)
                        ? searchParams.SchoolDistrictIds
                        : string.Empty
                });

                if (searchParams.IsMultiLocationSearch)
                    communityHomeResultsViewModel.SeoTemplateType = SeoTemplateType.Synthetic_Search_SchoolDistrict;
                else
                    communityHomeResultsViewModel.SeoTemplateType = (searchParams.SrpType == SearchResultsPageType.CommunityResults) ? SeoTemplateType.SchoolDistrict_Community_Market : SeoTemplateType.SchoolDistrict_Home_Market;

            }
            else if (!string.IsNullOrEmpty(searchParams.CommName))
            {
                contentTags.Add(new ContentTag {TagKey = ContentTagKey.CommunityName, TagValue = searchParams.CommName});

                communityHomeResultsViewModel.SeoTemplateType = SeoTemplateType.Nhs_CommunityName_Srp;
                communityHomeResultsViewModel.CommunityName = searchParams.CommName;
            }
            else if (searchParams.IsMultiLocationSearch)
            {
                communityHomeResultsViewModel.SeoTemplateType = SeoTemplateType.Synthetic_Search;
            }
            else if (!string.IsNullOrEmpty(searchParams.City) && searchParams.IsBuilderTabSearch == false)
            {
                // this is a market/city search
                communityHomeResultsViewModel.SeoTemplateType = SeoTemplateType.Nhs_Market_City_Srp;
                communityHomeResultsViewModel.City = searchParams.City;
            }
            else if (!string.IsNullOrEmpty(searchParams.PostalCode))
            {
                // this is a zipcode search
                // get Zip and ZipPrimaryCity
                contentTags.Add(new ContentTag {TagKey = ContentTagKey.ZipPrimaryCity, TagValue = market.MarketName});
                //contentTags.Add(new ContentTag {TagKey = ContentTagKey.Zip, TagValue = searchParams.PostalCode});
                communityHomeResultsViewModel.SeoTemplateType = SeoTemplateType.Nhs_Zip_Srp;
                communityHomeResultsViewModel.Zip = searchParams.PostalCode;
            }
            else if (!string.IsNullOrEmpty(searchParams.County))
            {
                // county search
                // get CountyName
                communityHomeResultsViewModel.SeoTemplateType = SeoTemplateType.Nhs_County_Srp;
                communityHomeResultsViewModel.County = searchParams.County;
                communityHomeResultsViewModel.State = searchParams.State;
            }
            else if (searchParams.IsBuilderTabSearch) 
            {
                if (!string.IsNullOrEmpty(searchParams.City))
                {
                    // this is a market/city search
                    communityHomeResultsViewModel.SeoTemplateType = SeoTemplateType.Nhs_Builders_Market_City_Srp;
                    communityHomeResultsViewModel.City = searchParams.City;
                }
                else
                {
                    communityHomeResultsViewModel.SeoTemplateType = SeoTemplateType.Nhs_Builders_Market_Srp;
                }
                
            }
            else
            {
                communityHomeResultsViewModel.SeoTemplateType = SeoTemplateType.Nhs_Market_Srp;
            }

            var currentStateId = market.State.StateAbbr;
            var currentStateName = market.State.StateName;

            if(!string.IsNullOrEmpty(searchParams.State) && searchParams.State != market.StateAbbr)
            {
                currentStateId = searchParams.State;
                currentStateName = _stateService.GetStateName(currentStateId);
            }

            // Replace tags
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.MarketId, TagValue = market.MarketId.ToString(CultureInfo.InvariantCulture) });
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.MarketName, TagValue = market.MarketName });

            contentTags.Add(new ContentTag { TagKey = ContentTagKey.MarketState, TagValue = currentStateId });
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.MarketStateName, TagValue = currentStateName });

            if(searchParams.SyntheticInfo != null)
                contentTags.Add(new ContentTag { TagKey = ContentTagKey.SyntheticName, TagValue = searchParams.SyntheticInfo.Name});

            contentTags.Add(new ContentTag { TagKey = ContentTagKey.StateID, TagValue = currentStateId });
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.StateName, TagValue = currentStateName });

            contentTags.Add(new ContentTag { TagKey = ContentTagKey.CountyName, TagValue = searchParams.County });
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.BrandID, TagValue = communityHomeResultsViewModel.BrandId.ToString(CultureInfo.InvariantCulture) });
            contentTags.Add(new ContentTag
            {
                TagKey = ContentTagKey.BrandName,
                TagValue = communityHomeResultsViewModel.BrandName
            });
            contentTags.Add(new ContentTag
            {
                TagKey = ContentTagKey.CityName,
                TagValue = communityHomeResultsViewModel.City
            });
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.Zip, TagValue = searchParams.PostalCode });
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.PageNumber, TagValue = RouteParams.Page.Value() });

            contentTags.Add(new ContentTag
            {
                TagKey = ContentTagKey.BedroomRangeLow,
                TagValue = !String.IsNullOrEmpty(communityHomeResultsViewModel.ResultCounts.Facets.BrRange)
                    ? communityHomeResultsViewModel.ResultCounts.Facets.BrRange.Split('-')[0]
                    : "0"
            });
            contentTags.Add(new ContentTag
            {
                TagKey = ContentTagKey.BedroomRangeHigh,
                TagValue = !String.IsNullOrEmpty(communityHomeResultsViewModel.ResultCounts.Facets.BrRange)
                    ? communityHomeResultsViewModel.ResultCounts.Facets.BrRange.Split('-')[1]
                    : "0"
            });
            contentTags.Add(new ContentTag
            {
                TagKey = ContentTagKey.BathroomRangeLow,
                TagValue = !String.IsNullOrEmpty(communityHomeResultsViewModel.ResultCounts.Facets.BaRange)
                    ? communityHomeResultsViewModel.ResultCounts.Facets.BaRange.Split('-')[0]
                    : "0"
            });
            contentTags.Add(new ContentTag
            {
                TagKey = ContentTagKey.BathroomRangeHigh,
                TagValue = !String.IsNullOrEmpty(communityHomeResultsViewModel.ResultCounts.Facets.BaRange)
                    ? communityHomeResultsViewModel.ResultCounts.Facets.BaRange.Split('-')[1]
                    : "0"
            });
            contentTags.Add(new ContentTag
            {
                TagKey = ContentTagKey.SquareFtRangeLow,
                TagValue = !String.IsNullOrEmpty(communityHomeResultsViewModel.ResultCounts.Facets.SftRange)
                    ? communityHomeResultsViewModel.ResultCounts.Facets.SftRange.Split('-')[0]
                    : "0"
            });
            contentTags.Add(new ContentTag
            {
                TagKey = ContentTagKey.SquareFtRangeHigh,
                TagValue = !String.IsNullOrEmpty(communityHomeResultsViewModel.ResultCounts.Facets.SftRange)
                    ? communityHomeResultsViewModel.ResultCounts.Facets.SftRange.Split('-')[1]
                    : "0"
            });
            contentTags.Add(new ContentTag
            {
                TagKey = ContentTagKey.PriceRangeLow,
                TagValue = !string.IsNullOrEmpty(communityHomeResultsViewModel.ResultCounts.Facets.PrRange)
                    ? communityHomeResultsViewModel.ResultCounts.Facets.PrRange.Split('-')[0]
                    : "0"
            });
            contentTags.Add(new ContentTag
            {
                TagKey = ContentTagKey.PriceRangeHigh,
                TagValue = !string.IsNullOrEmpty(communityHomeResultsViewModel.ResultCounts.Facets.PrRange)
                    ? communityHomeResultsViewModel.ResultCounts.Facets.PrRange.Split('-')[1]
                    : "0"
            });
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.CommunityCount, TagValue = communityHomeResultsViewModel.ResultCounts.CommCount.ToType<string>() });
            //BL are counted too as they are counted on the results count in the search reults page page, ResultList.cshtml 
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.HomeCount, TagValue = (communityHomeResultsViewModel.ResultCounts.CommAllHomeCount + communityHomeResultsViewModel.ResultCounts.BlCount).ToType<string>().ToFormattedThousands() });
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.SpecCount, TagValue = communityHomeResultsViewModel.ResultCounts.QmiCount.ToType<string>().ToFormattedThousands() });
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.PlanCount, TagValue = (communityHomeResultsViewModel.ResultCounts.CommAllHomeCount - communityHomeResultsViewModel.ResultCounts.QmiCount).ToType<string>().ToFormattedThousands() });
            contentTags.Add(new ContentTag
            {
                TagKey = ContentTagKey.BuilderCount,
                TagValue =
                    communityHomeResultsViewModel.ResultCounts.Facets.Brands != null
                        ? communityHomeResultsViewModel.ResultCounts.Facets.Brands.Count.ToType<string>().ToFormattedThousands()
                        : "0"
            });            

            communityHomeResultsViewModel.SeoContentTags = contentTags;
        }

        private void SetMetaTagsForSeo(ICommunityHomeResultsViewModel model)
        {
            var metaRegistrar = new MetaRegistrar(_pathMapper);
            var metas = new List<MetaTag>();
            var type = model.SrpPageType == SearchResultsPageType.HomeResults
                        ? SrpTypeEnum.HomeResults
                        : SrpTypeEnum.CommunityResults;

            try 
            {
                /*if (model.SrpPageType == SearchResultsPageType.HomeResults && !model.ShowBrandSeo 
                    && !model.ShowWaterfrontSeo && !model.ShowGolfSeo && !model.ShowMultiFamilySeo)
                {
                    if ((RouteParams.Inventory.Value<bool>() &&NhsRoute.IsPartnerNhs) ||
                        (RouteParams.SpecHomes.Value<bool>() && NhsRoute.IsPartnerMove))
                    {
                        OverrideDefaultMeta(model as BaseViewModel, model.SeoContentTags, model.SeoTemplateType, strType: type);
                    }
                    else
                    {
                        metas = metaRegistrar.NhsGetMetaTagsForSeoHomeResults(model.City, model.Market.MarketName,
                                                                              model.CommunityName, string.Empty,
                                                                              model.State);
                        OverrideDefaultMeta(model as BaseViewModel, metas);
                    }
                    return;
                }*/

                OverrideDefaultMeta(model as BaseViewModel, model.SeoContentTags, model.SeoTemplateType, strType: type);
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }
        }

        private static string GetBackgroundImagePath(SearchParams searchParams, IPathMapper pathMapper)
        {
            var marketsList = MarketBackgroundsConfigReader.GetMarketBackgrounds(pathMapper);
            if (marketsList == null || !marketsList.Any()) return string.Empty;
            var imageForMarket =marketsList.FirstOrDefault(m => m.MarketId == searchParams.MarketId.ToType<String>());
            if (imageForMarket== null)
            {
                imageForMarket = marketsList.FirstOrDefault(m => m.MarketId == "default");
                return imageForMarket != null ? imageForMarket.ImageFileName : string.Empty;
            }
            return imageForMarket.ImageFileName;
        }
    }
}
