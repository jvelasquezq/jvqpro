﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class BasicSearchController : ApplicationController
    {
        //
        // GET: /BasicSearch/

        public virtual ActionResult Show()
        {
            return View(new BasicSearchViewModel());
        }

    }
}
