﻿using System;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Bdx.Web.Api.Objects;
using Nhs.Library.Business;
using Nhs.Library.Business.Seo;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Filters;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult;
using System.Web;
using Nhs.Library.Helpers.Seo;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class CommunityResultsController : ApplicationController
    {
        private readonly IApiService _apiService;
        private readonly IMapService _mapService;
        private readonly IPathMapper _pathMapper;
        private readonly IBoylService _boylService;
        private readonly IStateService _stateService;
        private readonly IBrandService _brandService;
        private readonly IMarketService _marketService;
        private readonly IBuilderService _builderService;
        private readonly IListingService _listingService;
        private readonly IPartnerService _partnerService;
        private readonly ICommunityService _communityService;
        private readonly IBasicListingService _basicListingService;
        private readonly IMarketDfuService _marketDfuService;


        public CommunityResultsController(ICommunityService communityService, IMapService mapService, IMarketService marketService,
            IPathMapper pathMapper, IBoylService boylService, IBrandService brandService, IBuilderService builderService,
            IPartnerService partnerService, IStateService stateService, IApiService apiService, IBasicListingService basicListingService, IListingService listingService, IMarketDfuService marketDfuService)
            : base(pathMapper)
        {
            _apiService = apiService;
            _mapService = mapService;
            _pathMapper = pathMapper;
            _boylService = boylService;
            _stateService = stateService;
            _brandService = brandService;
            _marketService = marketService;
            _builderService = builderService;
            _partnerService = partnerService;
            _listingService = listingService;
            _communityService = communityService;
            _basicListingService = basicListingService;
            _marketDfuService = marketDfuService;
        }

        // GET: /CommunityResultsV2/

        [NoCache]
        public virtual ActionResult Show()
        {
            if (NhsRoute.ShowMobileSite)
                return ShowMobile();

            //TODO: Remove this once we have old commresults deprecated.
            if (NhsRoute.IsBrandPartnerNhsPro)
            {
                return ShowOldCommResultsResults();
            }
            
            Market market = null;
            Synthetic syntheticGeoName = null;

            //Set Result Page Type
            UserSession.RightAdToDisplayIsPosition = !UserSession.RightAdToDisplayIsPosition;
            // When DFU market name is provided
            int marketId = 0;
            if (RouteParams.Market.Value<int>() == 0)
            {
                if (!string.IsNullOrEmpty(RouteParams.Area.Value()) || !string.IsNullOrEmpty(RouteParams.MarketName.Value()))
                {
                    var state = RouteParams.StateName.Value();
                    var marketName = RouteParams.MarketName.Value();
                    market = _marketService.GetMarket(NhsRoute.PartnerId, state.Replace("-", " "), marketName.Replace("-", " ").Trim(), false);
                    marketId = market != null ? market.MarketId : 0;
                }
                else
                {
                    if (!string.IsNullOrEmpty(RouteParams.SyntheticName.Value<string>()))
                    {
                        var name = RouteParams.SyntheticName.Value<string>().Replace("-", " ");
                        syntheticGeoName = SyntheticGeoReader.GetSynthetics().FirstOrDefault(s => s.Name.ToLower() == name.ToLower());

                        if (syntheticGeoName != null)
                        {
                            market = _marketService.GetMarket(syntheticGeoName.MarketId);
                            marketId = market != null ? market.MarketId : 0;
                        }
                    }
                }
            }
            else
            {
                marketId = RouteParams.Market.Value<int>();
                market = GetMarket(marketId);
            }

            if (market == null)
            {
                return RedirectTo404();
            }

            var communityResultsPages = new List<string>
            {
                Pages.CommunityResultsv2.ToLower(),
                Pages.CommunityResults.ToLower(),
                Pages.Comunidades.ToLower()
            };
            var isCommunityResults = communityResultsPages.Contains(NhsRoute.CurrentRoute.Function.ToLower());

            //301ing in the base/application controller constructor will still allow this action to be executed, so we 301 here.
            var refer301Url = GetReferRedirectUrl(market, isCommunityResults);

            if (!string.IsNullOrEmpty(refer301Url))
            {
                return RedirectPermanent(refer301Url);
            }

            //Case 77642: IF removed below is simplified with the new DFU Service. 
            //if (RouteParams.Market.Value<int>() != 0 && Configuration.MarketsListforDFU.Contains(RouteParams.Market.Value<int>()) && NhsRoute.PartnerId == NhsRoute.BrandPartnerId) // not a private label site   
            var resultPage = RedirectionHelper.RedirecToNewFormat(RouteParams.Market.Value<int>(), _marketDfuService);
            if (!string.IsNullOrEmpty(resultPage))
            {
                return RedirectPermanent(resultPage, market.ToResultsParams(NhsRoute.CurrentRoute.Params));
            }


            UserSession.PersonalCookie.MarketId = marketId;
            UserSession.PersonalCookie.State = market.StateAbbr;

            var searchParams = GetDefaultSearchParams(marketId);
            searchParams.Cities = searchParams.Counties = searchParams.PostalCodes = null;
            searchParams.Markets = null;

            if (syntheticGeoName != null)
            {
                syntheticGeoName.Name = new CultureInfo("en").TextInfo.ToTitleCase(syntheticGeoName.Name.ToLower());
                searchParams.IsMultiLocationSearch = true;
                searchParams.SyntheticInfo = syntheticGeoName;

                switch (syntheticGeoName.LocationType)
                {
                    case LocationType.City:
                        searchParams.Cities = syntheticGeoName.ToLocationsList(); break;
                    case LocationType.County:
                        searchParams.Counties = syntheticGeoName.ToLocationsList(); break;
                    case LocationType.Zip:
                        searchParams.PostalCodes = syntheticGeoName.ToLocationsList(); break;
                    default :
                        searchParams.Markets = syntheticGeoName.ToLocationsList().Select(s => s.ToType<int>()).ToList();
                        break;
                }
            }
            else
            {
                searchParams.IsMultiLocationSearch = false;
                searchParams.SyntheticInfo = null;
            }   


            var vm = GetBaseCommunityResultsViewModel(searchParams, market);
            vm.IsBuilderTabSearch = searchParams.IsBuilderTabSearch;

            if (string.IsNullOrEmpty(vm.Globals.CanonicalLink))
                AddCanonicalWithExcludeParamsAndReplace(vm, new List<string> { "state", "pricelow", "pricehigh" },
                                                            new Dictionary<string, string> { { "city", "citynamefilter" } });

            UserSession.PersonalCookie.SearchText = GetSearchText(searchParams);

            var view = NhsMvc.Default.Views.CommunityResultsV2.Show;
           

            // ReSharper disable Mvc.ViewNotResolved
            return View(view, vm);
            // ReSharper restore Mvc.ViewNotResolved
        }

        [NoCache]
        public virtual ActionResult ShowDFU(string statename, string marketname)
        {
            if (NhsRoute.IsBrandPartnerNhsPro)
                return ShowDFUWithCity(statename, marketname, string.Empty);

            return Show();
        }

        [HttpGet]
        public virtual ActionResult GetCommunityMapCards(string commIds, string basicIds, bool forceHeader = false)
        {
            var data = string.IsNullOrEmpty(commIds)
                           ? new Dictionary<string, int>()
                           : commIds.Split(',')
                                    .Select(t => t.Split('|'))
                                    .ToDictionary(id => id[0], id => Convert.ToInt32(id[1]));
            var commCards = _communityService.GetCommunityMapCards(NhsRoute.PartnerId, string.Join(",", data.Keys), basicIds);



            var savedListings = UserSession.UserProfile.IsLoggedIn()
              ? UserSession.UserProfile.Planner.SavedCommunities.Select(p => p.ListingId).ToList()
              : new List<int>();
            foreach (var d in data)
            {
                var card = commCards.FirstOrDefault(p => p.Id == d.Key);
                if (card == null) continue;
                card.IsFavoriteListing = savedListings.Contains(card.Id.ToType<int>());
                card.NunHom = d.Value;
                card.ForceHeader = forceHeader;

                //assign market and state name
                var market = GetMarket(card.MId);
                card.MName = market.MarketName;
                card.StName = market.State.StateName;
            }

            if (NhsRoute.ShowMobileSite)
                return PartialView(NhsMvc.Default.ViewsMobile.CommunityResults.Community.CommunityMapCard, commCards);
            return PartialView(NhsMvc.Default.Views.CommunityResultsV2.Community.CommunityMapCard, commCards);
        }

        [HttpPost]
        public virtual ActionResult GetResults(SearchParams searchParameters)
        {
            var model = FillViewModelFromWepApi(searchParameters);
            model.IsTriggerFacets = true;
            UserSession.PersonalCookie.SearchText = GetSearchText(searchParameters);
            return PartialView(NhsMvc.Default.Views.CommunityResultsV2.ResultsData, model);
        }

        [HttpPost]
        public virtual ActionResult UpdateSeoContent(SearchParams searchParams)
        {
            var market = _marketService.GetMarket(searchParams.MarketId);
            var model = new CommunityHomeResultsViewModel
            {
                Market = market,
                State = market.StateAbbr,
                BrandPartnerName = _partnerService.GetPartner(NhsRoute.BrandPartnerId).PartnerName
            };

            // Fill the number of communities and homes required by some seo tags
            FillViewModelFromWepApi(searchParams, model);

            // Set the map information
            UpdateMapData(searchParams, ref model);
            var icon = Configuration.ResourceDomainPublic + "/globalresources14/default/images/icons/map_poi_green.png";

            model.StaticMapUrl(searchParams, "275x160", icon);

            SetContentManagerViewModel(model, searchParams, market);

            return PartialView(NhsMvc.Default.Views.CommunityResultsV2.HeaderInformation, model);
        }

        [HttpPost]
        public virtual JsonResult GetCounts(SearchParams searchParameters, string searchText, string searchType, int marketId)
        {
            searchParameters = GetSearchParamsForCounts(searchParameters, searchText, searchType);
            var facetsCounts = GetFacetCounts(searchParameters);
            searchParameters.CountsOnly = false;
            facetsCounts.Facets.Cities.Insert(0, new ApiFacetOption { Key = marketId, Value = LanguageHelper.AllAreas, State = searchParameters.State });
            var data = new { facetsCounts, searchParameters };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult SaveListingToPlanner(int communityId, int builderId, int homeId = 0, int isSpec = 0)
        {

            var logger = new ImpressionLogger
            {
                CommunityId = communityId,
                BuilderId = builderId,
                PartnerId = NhsRoute.PartnerId.ToType<string>(),
                Refer = UserSession.Refer
            };

            bool isSaved;
            //if home
            if (homeId > 0)
            {
                var pl = new PlannerListing(homeId, builderId, isSpec > 0 ? ListingType.Spec : ListingType.Plan);
                isSaved = UserSession.UserProfile.Planner.SavedHomes.Contains(pl);
                if (!isSaved)
                    UserSession.UserProfile.Planner.AddSavedHome(homeId, isSpec > 0);
                else
                    UserSession.UserProfile.Planner.RemoveSavedHome(homeId, isSpec > 0);

                if (isSpec > 0)
                    logger.AddSpec(homeId);
                else
                    logger.AddPlan(homeId);


                logger.LogView(LogImpressionConst.SaveToFavoriteHomeResults);
            }
            else
            {
                var pl = new PlannerListing(communityId, ListingType.Community);
                isSaved = UserSession.UserProfile.Planner.SavedCommunities.Contains(pl);
                if (!isSaved)
                    UserSession.UserProfile.Planner.AddSavedCommunity(communityId, builderId);
                else
                    UserSession.UserProfile.Planner.RemoveSavedCommunity(communityId, builderId);

                logger.LogView(LogImpressionConst.SaveToFavoriteCommunityResults);
            }
            return Json(!isSaved, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult GetMapPoints(SearchParams searchParameters)
        {
            searchParameters.ExcludeBasicListings = searchParameters.SrpType == SearchResultsPageType.CommunityResults;
            GetLocationCoordinates(ref searchParameters, searchParameters.GetLocationCoordinates ? GetMarket(searchParameters.MarketId) : null);
            var result = _communityService.GetCommunityHomeMapPoints(searchParameters, SearchResultsPageType.CommunityResults, NhsRoute.PartnerId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult SearchMapAreaConfirmed()
        {
            UserSession.SearchMapAreaConfirmed = true;
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult ChangeMapStatus(bool isMapVisible)
        {
            var sp = UserSession.SearchParametersV2 ?? GetDefaultSearchParams(RouteParams.MarketId.ToType<int>());
            sp.IsMapVisible = isMapVisible;
            UserSession.SearchParametersV2 = sp;
            return Json(isMapVisible, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult ChangeRadiusAndSearchType(SearchParams searchParams)
        {
            searchParams.GetRadius = false;
            UserSession.SearchParametersV2 = searchParams;

            return Json(searchParams, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult ShowPopupMap(int communityId, string marketName, int builderId, string priceHigh, string priceLow, int? propertyId)
        {
            //76890: MarketId has been removed from the signature of this method
            var comm = _communityService.GetCommunity(communityId);

            var model = new MapPopupViewModel();
            model.PriceRange = StringHelper.PrettyPrintRange(priceLow.ToType<double>(), priceHigh.ToType<double>(), "c0", LanguageHelper.From.ToTitleCase());
            model.CommunityId = communityId;
            model.BuilderId = comm.BuilderId;
            model.BuilderName = comm.Brand.BrandName;
            model.City = comm.City;
            model.CommunityName = comm.CommunityName;
            model.Latitude = comm.Latitude;
            model.Longitude = comm.Longitude;
            model.MarketName = marketName;
            model.State = comm.State.StateAbbr;
            model.StateName = comm.State.StateName;
            model.ZipCode = comm.PostalCode;
            var partnerMaskIndex = _partnerService.GetPartnerMaskIndex(NhsRoute.PartnerId);
            model.IsBasic = "F" == comm.PartnerMask.Substring(partnerMaskIndex - 1, 1);
            model.NumHom = comm.HomeCount.ToType<int>();
            model.PropertyId = (propertyId.GetValueOrDefault() != 0) ? propertyId.ToType<int>() : communityId;

            var address1 = (!string.IsNullOrEmpty(comm.Address1) && comm.OutOfCommunityFlag.ToType<bool>())
                         ? comm.Address1
                         : comm.SalesOffice.Address1;
            var address2 = (!string.IsNullOrEmpty(comm.Address2) && comm.OutOfCommunityFlag.ToType<bool>())
                         ? comm.Address2
                         : comm.SalesOffice.Address2;
            model.Addr = address1 + " " + address2;

            if (NhsRoute.ShowMobileSite)
            {
                return PartialView(NhsMvc.Default.ViewsMobile.CommunityResults.PopupMap, model);
            }
            return PartialView(NhsMvc.Default.Views.CommunityResultsV2.PopupMap, model);
        }

        public virtual ActionResult ShowBasicListingPopupMap(int basicListingId, string address, string latitude, string longitude, string priceHigh, string priceLow)
        {
            //Method create based on issue in case 76882 and conflicts with 76890
            var model = new MapPopupViewModel
            {
                PriceRange = StringHelper.PrettyPrintRange(priceHigh.ToType<double>(), priceLow.ToType<double>(), "c0"),
                Latitude = latitude.ToType<decimal>(),
                Longitude = longitude.ToType<decimal>(),
                CommunityName = address,
                PropertyId = basicListingId
            };

            return PartialView(NhsRoute.ShowMobileSite ? NhsMvc.Default.ViewsMobile.CommunityResults.PopupMap : NhsMvc.Default.Views.CommunityResultsV2.PopupMap, model);
        }

        public virtual ActionResult GetBuildersArea(IEnumerable<ApiFacetOption> brands, int marketId)
        {
            var apiFacetOptions = brands as IList<ApiFacetOption> ?? brands.ToList();
            var model = new BaseCommunityHomeResultsViewModel { Market = new Market { MarketId = marketId } };
            var brandsInformation = GetBrandsInformation(marketId, apiFacetOptions).ToList();
            
            model.BrandsList = brandsInformation.Where(b => apiFacetOptions.Any(c => c.Key == b.BrandId) && !b.IsBasic).ToColumns(2);
            model.BasicBrandsList = brandsInformation.Where(b => apiFacetOptions.Any(c => c.Key == b.BrandId) && b.IsBasic).ToColumns(2);

            return PartialView(NhsMvc.Default.Views.CommunityResultsV2.BuilderArea, model);
        }
    }
}
