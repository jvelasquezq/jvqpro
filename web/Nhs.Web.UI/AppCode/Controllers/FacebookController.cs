﻿using System;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Route;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Routing.Interface;
using Nhs.Mvc.Routing.Utils;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Web.UI.AppCode.Helper;

namespace Nhs.Web.UI.AppCode.Controllers
{

    public partial class FacebookController : ApplicationController
    {
        private IUserProfileService _userProfileService;

        public FacebookController(IUserProfileService userProfileService)
        {
            _userProfileService = userProfileService;
        }

        [HttpPost]
        public virtual JsonResult Login(FacebookData data)
        {
            if (UserSession.UserProfile.ActorStatus != WebActors.ActiveUser)
            {
                var routeParams = RouteHelper.GetRouteByUrl(HttpContext.Request.UrlReferrer).DeepCloneObject<RouteElement>().Params;                
                var partnerId = NhsRoute.PartnerId;

                var logger = new ImpressionLogger
                {
                    MarketId = routeParams.Value<int>(RouteParams.Market),
                    PartnerId = partnerId.ToString(),
                    CommunityId = routeParams.Value<int>(RouteParams.Community),
                    BuilderId = routeParams.Value<int>(RouteParams.Builder)
                };

                logger.AddSpec(routeParams.Value<int>(RouteParams.SpecId));
                logger.AddPlan(routeParams.Value<int>(RouteParams.PlanId));

                var profile = new Profile(partnerId, data.Email)
                                  {
                                      FirstName = data.FirstName,
                                      LastName = data.LastName,
                                      Password = data.Id
                                  };

                if (!_userProfileService.LogonNameExists(data.Email, partnerId))
                {
                    CreateProfile(profile);
                    logger.LogView(LogImpressionConst.FacebookCreateAccount);
                }
                else
                {
                    _userProfileService.SignIn(data.Email, NhsRoute.PartnerId, LanguageHelper.EmailDoNotExist);
                    logger.LogView(LogImpressionConst.FacebookLogin);
                }

                return Json(true, JsonRequestBehavior.AllowGet);
            }

            return Json(false, JsonRequestBehavior.AllowGet);
        }        


        private void CreateProfile(Profile legacyProfile)
        {
            var profile = _userProfileService.GetProfileFromLegacyProfile(legacyProfile);

            profile.DateRegistered = profile.DateLastChanged = DateTime.Now;
            profile.MailingList = profile.MarketOptin = "0";

            // Default dropdown values
            profile.MoveInDate = -1;
            profile.FinancePreference = profile.MovingReason = null;

            // Setting those values ... required in stored proc.
            profile.BoxRequestedDate = DateTime.Now;
            profile.InitialMatchDate = DateTime.Now;
            profile.LastMatchesSentDate = DateTime.Now;

            // Store communites & homes to save to planner after user creation
            var savedCommunities = UserSession.UserProfile.Planner.SavedCommunities;
            var savedHomes = UserSession.UserProfile.Planner.SavedHomes;

            // Saving user info...also sets profile cookie
            UserSession.UserProfile.ActorStatus = WebActors.ActiveUser;
            _userProfileService.CreateProfile(profile, NhsRoute.BrandPartnerId, LanguageHelper.WrongPassword, LanguageHelper.EmailDoNotExist, LanguageHelper.UserAlreadyExist);
            _userProfileService.CreateRegistrationLead(profile);

            // Add communties and homes to planner
            foreach (var listing in savedCommunities)
            {
                UserSession.UserProfile.Planner.AddSavedCommunity(listing.ListingId, listing.BuilderId);
                UserSession.UserProfile.Planner.UpdateCommunityRequestDate(listing.ListingId, listing.BuilderId, listing.SourceRequestKey);
            }
            foreach (var listing in savedHomes)
            {
                UserSession.UserProfile.Planner.AddSavedHome(listing.ListingId, listing.ListingType == ListingType.Spec);
                UserSession.UserProfile.Planner.UpdateHomeRequestDate(listing.ListingId, listing.ListingType == ListingType.Spec, listing.SourceRequestKey);
            }
        }
    }

    public class FacebookData
    {
        public string Email { get; set; }
        public string Gender { get; set; }
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public string Locale { get; set; }
        public string Location { get; set; }
    }
}
