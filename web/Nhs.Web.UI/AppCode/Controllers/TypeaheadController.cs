﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Nhs.Library.Business.Kendo;
using Nhs.Library.Enums;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Entities;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Binders;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class TypeaheadController : ApplicationController
    {
        private readonly ITypeaheadService _service;
        private readonly IPartnerService _partnerService;
        private readonly IBrandService _brandService;
        
        public TypeaheadController(ITypeaheadService service, IPartnerService partnerService, IBrandService brandService)
        {
            _service = service;
            _partnerService = partnerService;
            _brandService = brandService;
        }

        public virtual JsonResult GetCommunities([ModelBinder(typeof(KendoParametersModelBinder))]KendoFilterContainer filter)
        {            
            var comms = _service.GetAllCommunities(filter);
            return Json(comms, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult GetBuilders([ModelBinder(typeof(KendoParametersModelBinder))] KendoFilterContainer filter)
        {
            var builders = _service.GetAllBuilders(filter);
            return Json(builders, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult GetBrands([ModelBinder(typeof (KendoParametersModelBinder))] KendoFilterContainer filter)
        {
            var brands = _brandService.GetAllBrands(NhsRoute.PartnerId);
            switch (filter.Operator)
            {
                case KendoFilterOperator.Contains:
                    var stringComparison = filter.IgnoreCase
                        ? StringComparison.CurrentCultureIgnoreCase
                        : StringComparison.CurrentCulture;
                    brands = brands.Where(m => m.BrandName.Contains(filter.Value, stringComparison)).Select(c => c).ToList();
                    break;
                case KendoFilterOperator.StartsWith:
                    brands = brands.Where(m => m.BrandName.StartsWith(filter.Value, filter.IgnoreCase, CultureInfo.CurrentCulture))
                        .Select(c => c).ToList();
                    break;
            }

            var typeaheadBrands = brands.Select(brand => new TypeaheadBrand { BrandName = brand.BrandName, BrandId = brand.BrandId.ToString() }).ToList();

            return Json(typeaheadBrands, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult GetCities([ModelBinder(typeof (KendoParametersModelBinder))] KendoFilterContainer filter)
        {
            var locations = _partnerService.GetTypeAheadSuggestions(NhsRoute.PartnerId, filter.Value, false, false).Where((m => m.Type == LocationType.City.ToType<int>()));
            switch (filter.Operator)
            {
                case KendoFilterOperator.Contains:
                    var stringComparison = filter.IgnoreCase
                        ? StringComparison.CurrentCultureIgnoreCase
                        : StringComparison.CurrentCulture;
                    locations = locations.Where(m => m.Name.Contains(filter.Value, stringComparison)).Select(c => c).ToList();
                    break;
                case KendoFilterOperator.StartsWith:
                    locations = locations.Where(m => m.Name.StartsWith(filter.Value, filter.IgnoreCase, CultureInfo.CurrentCulture))
                        .Select(c => c).ToList();
                    break;
            }

            var typeaheadCities =
                locations.Select(
                    location =>
                        new TypeaheadCity
                        {
                            CityId = location.Name,
                            CityName = string.Format("{0}, {1}", location.Name, location.State)
                        }).ToList();

            return Json(typeaheadCities, JsonRequestBehavior.AllowGet);
            
        }

        public virtual JsonResult GetZipCodesByPartner(string term)
        {
            var builders = _service.GetZipCodesByParner(term, NhsRoute.PartnerId);
            return Json(builders.Select(loc => loc.Name).ToArray(), JsonRequestBehavior.AllowGet);
        }

        
    }
}