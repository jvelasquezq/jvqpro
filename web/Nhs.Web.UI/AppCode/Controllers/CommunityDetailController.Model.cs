﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using Kendo.Mvc.Extensions;
using Nhs.Library.Business;
using Nhs.Library.Business.Enums;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Proxy;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects;
using Nhs.Search.Objects.Constants;
using Nhs.Utility.Common;
using Nhs.Utility.Constants;
using Nhs.Utility.Data.Paging;
using Nhs.Utility.Html;
using Nhs.Web.UI.AppCode.Extensions;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.Lib;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class CommunityDetailController
    {
        private NhsPropertySearchParams _searchParameters;

        private CommunityDetailViewModel GetCommunityDetailModel(Community community, NhsPropertySearchParams searchParams, int page,
                                                                 bool addNoIndexTag)
        {
            _searchParameters = searchParams;
            var filteredHomesCount = 0;

            ModelState.Remove("CreditScoreText");
            ModelState.Remove("ShowCreditScoreSection");
            if (community == null) //Check if community exists.
                return CommunityDetailExtensions.GetInactivePropertyViewModel(true, _stateService, _lookupService) as CommunityDetailViewModel;

            _searchParameters.MarketId = community.MarketId;
            _searchParameters.ListingTypeFlag = community.ListingTypeFlag;
            _searchParameters.CommunityStatus = community.BCType;
            var searchResults = new CommunityResultsView();

            if (RouteParams.AddToProfile.Value<bool>())
                SaveCommunityToPlanner(community.CommunityId, community.BuilderId, true);

            WebApiCommonResultModel<List<HomeItem>> resultsView = null;

            if (UserSession.PropertySearchParameters.MarketId != 0)
            {
                var @params = _searchParameters.Clone() as NhsPropertySearchParams;
                if (@params != null)
                {
                    @params.EndRecord = @params.HomeStatus = 0; //Get all home types using filter
                    //@params.CityNameFilter = string.Empty;
                    var filteredHomes = _listingService.GetHomesResultsFromApi<HomeItem>(@params, NhsRoute.PartnerId);
                    filteredHomesCount = filteredHomes.ResultCounts.HomeCount;

                    //Get filtered home count
                    if (filteredHomesCount == community.HomeCount) //no need to get count if same as total count
                        filteredHomesCount = 0;
                    else
                        resultsView = filteredHomes;
                }
            }

            if (filteredHomesCount == 0)
            {
                resultsView = GetCommunityHomeResultsFromApi(searchParams, searchParams.HomeStatus == 5);
                if (resultsView.ResultCounts.HomeCount <= 0)
                    resultsView = GetCommunityHomeResultsFromApi(searchParams, true);
            }

            var sortActions = new SortHomeActions();
           
            resultsView.Result = resultsView.Result.Select(home =>
            {
                var market = home.MarketId != community.MarketId
                    ? _marketService.GetMarket(home.MarketId)
                    : community.Market;

                home.MarketName = market.MarketName;
                home.StateName = market.State.StateName;
                return home;
            }).ToList();

            var shortedHomes = sortActions.SortOptions[_searchParameters.SortOrder](resultsView.Result);

            var hotHomes = new List<HomeItem>();
            if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
            {
                hotHomes = resultsView.Result.Where(r => r.IsHotHome > 0).Take(2).Select(r =>
                {
                    if (r.IsSpec == 1)
                    {
                        var spec = _listingService.GetSpec(r.HomeId);
                        r.HotHomeDescription = spec.HotHomeDescription;
                    }
                    else
                    {
                        var plan = _listingService.GetPlan(r.HomeId);
                        r.HotHomeDescription = plan.HotHomeDescription;
                    }
                    return r;
                }).ToList();
            }

            var pageSize = (resultsView.ResultCounts.HomeCount <= 40) ? resultsView.ResultCounts.HomeCount : 12;
            var showNewHomesExtraTab = resultsView.ResultCounts.HomeCount < community.HomeCount.ToType<int>();
            var pagedList = pageSize > 0 ? shortedHomes.ToPagedList(page, pageSize) : null;
           
            var isBilled = false;
            var brand = community.Brand ?? _brandService.GetBrandById(community.BrandId, NhsRoute.BrandPartnerId);
            var isShowCaseActive = brand.BrandShowCase != null && brand.BrandShowCase.StatusId == 1;
            var hasTheBrandActiveComms = _communityService.GetCommunitiesByBrandId(NhsRoute.PartnerId, brand.BrandId).Any();
            var partnerMask = GetCommunityPartnerMaskValue(community.PartnerMask);

            if ("F" != partnerMask)
                isBilled = _communityService.GetCommunityBilledStatus(searchParams.PartnerId, community.BuilderId,
                                                                      community.CommunityId, community.MarketId);

            var userLoggedIn = (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser);

            // Get credit score links
            string creditScoreLink1 = string.Empty, creditScoreLink2 = string.Empty;
            _communityService.GetCreditScoreLinks(ref creditScoreLink1, ref creditScoreLink2);

            var viewModel = new CommunityDetailViewModel
            {
                NonPdfBrochureUrl = _communityService.GetNonPdfBrochureUrl(community.CommunityId, 0, 0, community.BuilderId),
                NewUser = NhsRoute.CurrentRoute.Params.Any(p => p.Name == RouteParams.NewUser),
                IsBilled = isBilled,
                ShowSocialIcons = ShowSocialIcons(isBilled),
                IsPhoneNumberVisible = false,
                ShowLeadForm = true,
                LeadSectionState = SetLeadSectionState(isBilled, userLoggedIn).ToType<int>(),
                HasHotHomes = community.HasHotHome,
                BuilderLogo = brand.LogoMedium,
                BuilderLogoSmall = brand.LogoSmall,
                HasShowCaseInformation = brand.HasShowCase && isShowCaseActive && hasTheBrandActiveComms,
                FilteredHomesCount = filteredHomesCount,
                ShowExpiredRequestBrochureLink = false,
                //Results info
                CurrentSortOrder = searchParams.SortOrder,
                ShowNewHomesExtraTab = showNewHomesExtraTab,
                ShowHotHomeSection = community.HasHotHome == 1,
                TotalComunities = searchResults.TotalCommunities,
                AllNewHomesCount = community.HomeCount.ToType<int>(),
                QuickMoveInCount = community.QuickMoveInCount.ToType<int>(),
                HomeResultsApi = pagedList,
                HotHomesApi = hotHomes,
                SelectedTab = TabSelected(searchParams.HomeStatus, showNewHomesExtraTab),
                SelectedStateAbbr = community.StateAbbr,
                //Info to display
                PageLinkAction = (searchParams.HomeStatus == 0) ? "Show" : "QuickMoveIn",
                MarketName = community.Market.MarketName,
                StateName = community.Market.State.StateName,
                MarketId = community.MarketId,
                SaveCommunitySuccessMessage = LanguageHelper.MSG_COMMDETAIL_ADDED_TO_PLANNER,
                AddCommunityToPlannerUrl = GetAddCommunityToPlannerUrl(community.CommunityId, community.BuilderId),
                //BEGIN-FIX: 74994
                SalesOfficeAddress1 = community.SalesOffice.Address1,
                SalesOfficeAddress2 = community.SalesOffice.Address2,
                //END-FIX: 74994
                LogActionMethodUrl = GetMethodUrl(community.CommunityId, community.BuilderId),
                HomeSearchViewMetricDisplayed = (pagedList != null) ? HomeSearchViewMetricDisplayed(pagedList, community.HomeCount.ToType<int>(), community.MarketId)
                                                                    : string.Empty,
                HomeSearchViewMetricHidden = (pagedList != null) ? HomeSearchViewMetricHidden(pagedList, community.HomeCount.ToType<int>(), community.MarketId)
                                                                 : string.Empty,
                CreditScoreLink1 = creditScoreLink1,
                CreditScoreLink2 = creditScoreLink2
            };

            if (NhsRoute.IsBrandPartnerNhsPro)
                viewModel.SavedPropertysProCrm = UserSession.UserProfile.Planner.GetSavedProperties();
            
            viewModel.ShowPlaceHolderInputText = false;
            viewModel.CommunityStyles = new List<string>();
            if (community.IsAgeRestricted.ToType<bool>()) viewModel.CommunityStyles.Add("Age Restricted");
            if (community.IsAdult.ToType<bool>()) viewModel.CommunityStyles.Add("Adult");
            if (community.IsCondo.ToType<bool>()) viewModel.CommunityStyles.Add("Condo/TownHome");
            if (community.IsGreen.ToType<bool>()) viewModel.CommunityStyles.Add("Green");
            if (community.IsGated.ToType<bool>()) viewModel.CommunityStyles.Add("Gated");
            if (community.IsMasterPlanned.ToType<bool>()) viewModel.CommunityStyles.Add("Master-Planned");

            viewModel.Testimonials = _communityService.GetCustomerTestimonials(5, community.CommunityId, community.BuilderId);
            viewModel.AwardImages = _communityService.GetAwardImages(community.AllImages);

            AssembleCommunityDetailCommunityStyles(viewModel, community);
            AssembleCommunityDetailBaseModel(viewModel, community);
            AssembleDetailViewModel(viewModel, community);
            AssemblePropertyMapViewModel(viewModel, community);

            //Set ad vars
            BindAds(ref viewModel, community);

            //SEO meta
            try
            {
                OverrideDefaultMetaCommDetail(community, addNoIndexTag, viewModel, isBilled);
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }

            //Set SDC Market Id
            viewModel.Globals.SdcMarketId = community.MarketId;
            var boylList = _boylService.GetBoylResultsCnh(viewModel.MarketName, viewModel.StateAbbr);
            viewModel.CnhBoyl = boylList.FirstOrDefault(p => p.NhsBuilderId == viewModel.BuilderId && p.IsCnh);
            var tags = new List<ContentTag> { new ContentTag(ContentTagKey.CommunityId, viewModel.CommunityId.ToString()) };
            viewModel.AffiliateLinksData = new AffiliateLinksData();
            UserSession.PersonalCookie.MarketId = viewModel.MarketId;
            UserSession.PersonalCookie.State = viewModel.StateAbbr;
            UserSession.PersonalCookie.ZipCode = viewModel.ZipCode;
            UserSession.PersonalCookie.LastPrice = viewModel.PriceLow.ToType<decimal>();

            if (NhsRoute.IsBrandPartnerNhsPro)
                return viewModel;

            //BEGIN: Affiliate Links Logic is only for NHS and MOVE
            var links = _affiliateLinkService.GetAffiliateLinks(Pages.CommunityDetail, NhsRoute.PartnerId);
            var freeCreditScore = links.LinksList.Find(x => x.Keyword.IsCaseInsensitiveEqual(AffiliateLinksTypes.FreeCreditScore));
            if (freeCreditScore != null)
                viewModel.AffiliateLinksData.FreeCreditScoreFormArea = freeCreditScore.ToLink(tags);

            var mortgageRates = links.LinksList.Find(x => x.Keyword.IsCaseInsensitiveEqual(AffiliateLinksTypes.MortgageRates));
            if (mortgageRates != null)
                viewModel.AffiliateLinksData.MortageRatesFormArea = mortgageRates.ToLink(tags);

            var calcMortgageRates = links.LinksList.Find(x => x.Keyword.IsCaseInsensitiveEqual(AffiliateLinksTypes.CalculateMortgagePayments));
            if (calcMortgageRates != null)
                viewModel.AffiliateLinksData.CalculateMortagePaymentsNexStepsArea = calcMortgageRates.ToLink(tags);
            //END: Affiliate Links Logic is only for NHS and MOVE

            return viewModel;
        }

        private void AssembleCommunityDetailCommunityStyles(CommunityDetailViewModel model, Community community)
        {
            model.IsAdult = community.IsAdult.ToType<bool>();
            model.IsTownHome = community.IsTownHome.ToType<bool>();
            model.IsAgeRestricted = community.IsAgeRestricted.ToType<bool>();
            model.IsCondo = community.IsCondo.ToType<bool>();
            model.IsGreenProgram = community.IsGreenProgram.ToType<bool>();
            model.IsGreen = community.IsGreen.ToType<bool>();
            model.IsGated = community.IsGated.ToType<bool>();
            model.IsMasterPlanned = community.IsMasterPlanned.ToType<bool>();
            model.IsCommunityMultiFamily = model.IsTownHome || model.IsCondo;
        }

        private string GetCommunityPartnerMaskValue(string partnerMaskString)
        {
            var partnerMaskIndex = _partnerService.GetPartnerMaskIndex(NhsRoute.PartnerId);
            return partnerMaskString.Substring(partnerMaskIndex - 1, 1);
        }

        private void OverrideDefaultMetaCommDetail(Community community, bool addNoIndexTag, CommunityDetailViewModel viewModel, bool isBilled)
        {
            var schoolDistrictName = viewModel.Schools.Count > 0
                                             ? viewModel.Schools[0].SchoolDistrict.DistrictName
                                             : string.Empty;

            var metaRegistrar = new MetaRegistrar(_pathMapper);
            var metas =
                metaRegistrar.NhsGetMetaTagsForSEOCommunityDetail(community.CommunityName, community.City,
                    community.Market.MarketName, schoolDistrictName,
                    community.Builder.Brand.BrandName,
                    community.StateAbbr, Pages.CommunityDetail).ToList();

            if (addNoIndexTag)
                AddNoIndex(metaRegistrar, metas);

              var dpSeo = new SeoContentManager
                {
                    BrandID = viewModel.BrandId.ToString(CultureInfo.InvariantCulture),
                    BrandName = viewModel.BrandName,
                    City = viewModel.CommunityCity,
                    CommunityName = viewModel.CommunityName,
                    State = viewModel.State,
                    Market = community.Market.MarketName,
                    Zip = community.PostalCode
                };
                var contentTags = new List<ContentTag>
                {
                    new ContentTag(ContentTagKey.CommunityName, viewModel.CommunityName),
                    new ContentTag(ContentTagKey.BrandID, viewModel.BrandId.ToString(CultureInfo.InvariantCulture)),
                    new ContentTag(ContentTagKey.BrandName, viewModel.BrandName),
                    new ContentTag(ContentTagKey.City, viewModel.CommunityCity),
                    new ContentTag(ContentTagKey.StateName, viewModel.StateAbbr),
                    new ContentTag(ContentTagKey.MarketName, community.Market.MarketName),
                    new ContentTag(ContentTagKey.Zip, community.PostalCode)
                };
                var dataSeo = dpSeo.GetSeoDetailPages(viewModel.CommunityId, contentTags);
                metas = dataSeo.MetaTags;
                viewModel.Globals.H1 = dataSeo.H1;
                viewModel.Globals.Seo = dataSeo.Seo;
            OverrideDefaultMeta(viewModel, metas, FacebookHelper.GetFacebookMeta(viewModel.CommunityName, viewModel.BrandName, community.SpotlightThumbnail));
        }

        private void SetSeo(bool addNoIndexTag, CommunityDetailViewModel viewModel, Community community)
        {
            try
            {
                var schoolDistrictName = viewModel.Schools.Count > 0
                                             ? viewModel.Schools[0].SchoolDistrict.DistrictName
                                             : string.Empty;

                var metaRegistrar = new MetaRegistrar(_pathMapper);
                var metas =
                    metaRegistrar.NhsGetMetaTagsForSEOCommunityDetail(community.CommunityName, community.City,
                                                                      community.Market.MarketName, schoolDistrictName,
                                                                      community.Builder.Brand.BrandName,
                                                                      community.StateAbbr, Pages.CommunityDetail).ToList();

                if (addNoIndexTag)
                    AddNoIndex(metaRegistrar, metas);

                var resizeCommands = ImageSizes.GetResizeCommands(ImageSizes.HomeMain);
                var parameters = (resizeCommands.HasValues) ? "?" + resizeCommands : String.Empty;
                var image = community.SpotlightThumbnail;
                if (!string.IsNullOrEmpty(image))
                {
                    image = image.RemoveAllSizes() + parameters;
                }
                OverrideDefaultMeta(viewModel, metas, FacebookHelper.GetFacebookMeta(viewModel.CommunityName, viewModel.BrandName, image ?? ""));

            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }
        }



        private string HomeSearchViewMetricHidden(IEnumerable<ExtendedHomeResult> homes, int allNewHomesCount, int marketId)
        {
            if (allNewHomesCount == 0) return string.Empty;

            int index = homes.Count() - 12;
            if (index < 1) return string.Empty;
            //TODO: Avoid reverse (performance issue possibility)
            IEnumerable<ExtendedHomeResult> homesMetric = homes.Reverse().Take(index);

            var metricBuilder = new StringBuilder();

            foreach (string chain in homesMetric.Select(home => string.Concat(NhsRoute.PartnerId, ",", marketId, ",", home.BuilderId, ",", home.CommunityId, ",", home.SpecId, ",", home.PlanId, ",", "|")))
                metricBuilder.Append(chain);

            metricBuilder.Remove(metricBuilder.Length - 2, 2);
            return metricBuilder.ToString();
        }

        private string GetMethodUrl(int communityId, int builderId)
        {
            string partnerSite = string.IsNullOrEmpty(NhsRoute.PartnerSiteUrl)
                                     ? string.Empty
                                     : string.Concat("/", NhsRoute.PartnerSiteUrl);
            return string.Format("{0}/{1}/{2}-{3}/{4}-{5}/", partnerSite, Pages.CommunityDetail, NhsLinkParams.Community,
                                     communityId, NhsLinkParams.Builder, builderId);
        }


        /// <summary>
        /// this method fills out the basic data in the Model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="community"></param>
        private void AssembleCommunityDetailBaseModel(CommunityDetailBaseViewModel model, Community community)
        {
            model.BcType = community.BCType;
            model.IsPageCommDetail = true;
            model.BrandId = community.BrandId;
            model.MarketId = community.MarketId;

            //BEGIN-FIX: 74994
            model.CommunityCity = community.SalesOffice.City;
            //END-FIX: 74994

            model.Utilities = _communityService.GetCommunityUtilities(community.CommunityId, NhsRoute.PartnerId);
            model.BuilderId = community.BuilderId;
            model.CommunityId = community.CommunityId;
            model.CommunityName = community.CommunityName;
            model.BuilderName = community.Builder.BuilderName;
            model.MarketName = community.Market.MarketName;
            model.PopUpViewerName = community.CommunityName;
            model.ShowBrochure = (string.IsNullOrEmpty(model.BcType) || model.BcType.ToLower() != BuilderCommunityType.ComingSoon);
            model.BuilderUrl = !string.IsNullOrEmpty(community.Builder.Url) ? community.Builder.Url : (!string.IsNullOrEmpty(community.Builder.ParentBuilder.Url)
                                          ? community.Builder.ParentBuilder.Url
                                          : string.Empty);
            model.CommunityUrl = community.CommunityUrl ?? string.Empty;
            model.Amenities = _communityService.GetAmenities(community.IAmenities, community.IOpenAmenities);
            if (!NhsRoute.ShowMobileSite)
                model.SearchParametersJSon = new JavaScriptSerializer().Serialize(_searchParameters);
            model.Schools = _communityService.GetSchools(NhsRoute.PartnerId, community.CommunityId);
            model.CommunityDescription = FormattedText(community.CommunityDescription);
            //Green Programs
            model.GreenPrograms = _communityService.GetCommunityGreenPrograms(community.CommunityId).Take(2).ToList();
        }

        private void FormatCommunityAgentCommissionSection(IDetailViewModel model, Community community)
        {
            model.AgentPolicyLink = AgentPolicyUrl(community.CommunityId, community.BuilderId);
            bool isNotCommissionFlat = string.IsNullOrEmpty(community.CommissionRateType) || community.CommissionRateType.ToLower() != "flat";
            model.AgentCompensation = community.AgentCommission.ToType<double>() <= 0
                ? string.Empty
                : (isNotCommissionFlat
                    ? FormatedPercentage(community.AgentCommission.ToType<string>())
                    : StringHelper.FormatCurrency(community.AgentCommission));

            model.AgentCompensationAdditionalComments = community.AdditionalCommissionComments;

            if (community.PayoutTiming != null && Enum.IsDefined(typeof (AgentCommissionPayoutTiming), community.PayoutTiming))
                model.AgentCommissionPayoutTiming = Enum.Parse(typeof (AgentCommissionPayoutTiming), community.PayoutTiming, true).ToString();

            if (community.CommissionBasis != null && Enum.IsDefined(typeof(AgentCommissionBasis), community.CommissionBasis))
            {
                var agentCommissionBasis =
                    (AgentCommissionBasis) Enum.Parse(typeof (AgentCommissionBasis), community.CommissionBasis, true);
                model.AgentCommissionBasis =
                    agentCommissionBasis.GetAttributeValue<DescriptionAttribute, string>(x => x.Description);
            }
        }

        /// <summary>
        /// this method fills out the detail data in the Model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="community"></param>
        private void AssembleDetailViewModel(IDetailViewModel model, Community community)
        {
            if (NhsRoute.IsBrandPartnerNhsPro)
                FormatCommunityAgentCommissionSection(model, community);
            
            model.BcType = community.BCType;

            var partnerUsesMatchmaker = _partnerService.GetPartner(NhsRoute.PartnerId).UsesMatchMaker.ToBool();
            model.PartnerUsesMatchmaker = partnerUsesMatchmaker;
            model.ShowUserPhoneNumber = NhsUrl.ShowUserPhoneNumber;

            //Prepop User Info
            if (partnerUsesMatchmaker && !UserSession.HasUserOptedIntoMatchMaker)
                model.SendAlerts = true;

            model.UserPostalCode = UserSession.UserProfile.PostalCode;
            model.MailAddress = UserSession.UserProfile.LogonName;
            if (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser)
                model.LiveOutside = String.IsNullOrEmpty(UserSession.UserProfile.PostalCode);

            model.Name = string.Concat(UserSession.UserProfile.FirstName, " ", UserSession.UserProfile.LastName).Trim();
            model.UserPhoneNumber = UserSession.UserProfile.DayPhone;

            //Brand Info
            model.BrandThumbnail = string.Concat(Configuration.ResourceDomain, community.Brand.LogoMedium);
            model.BrandName = community.Brand.BrandName ?? string.Empty;

            //Builder Info
            model.BuilderName = community.Builder.BuilderName ?? string.Empty;
            model.CorporationName = community.Builder.ParentBuilder == null
                                         ? model.BuilderName
                                         : community.Builder.ParentBuilder.BuilderName ?? string.Empty;

            //builder Coop                   
            var coopInfo = _builderService.GetBuilderCoOpInfo(model.BuilderId, NhsRoute.BrandPartnerId);
            model.HasCoop = coopInfo.IsCoo;
            model.HasBuilderPactLink = coopInfo.IsPac;            
            

            //Info to display
            model.BcType = community.BCType;

            //BEGIN FIX: 74994
            model.ZipCode = community.SalesOffice.ZipCode;
            model.State = community.SalesOffice.State;
            //END-FIX: 74994

            model.StateAbbr = community.State.StateAbbr;
            model.PhoneNumber = community.PhoneNumber;
            model.TrackingPhoneNumber = community.TrackingPhoneNumber;
            model.HomesAvailable = community.HomeCount > 0
                                       ? HomesAvailableLabel(community.HomeCount)
                                       : LanguageHelper.NewHomesComingSoon;
            model.ShowMortgageLink = (community.ShowMortgageLink == "Y");
            model.PriceDisplay = FormattedPrice(community.PriceHigh, community.PriceLow, community.BCType);
            model.HoursOfOperation = community.SalesOffice.HoursOfOperation.ToType<string>();
            model.DisplaySqFeet = FormattedSqFeet(community.SqFtLow, community.SqFtHigh);
            model.SaleAgents = _communityService.GetSalesAgents(community.SalesOfficeId);
            model.EnvisionUrl = community.EnvisionLink;
            model.PriceLow = community.PriceLow.ToString();
            model.PriceHigh = community.PriceHigh.ToString();

            //Flags
            model.ShowSaveToAccount = SetSaveToAccountFlag();

            //Multimedia data
            model.Videos = community.Videos;
            model.Promotions = new Collection<Promotion>();
            model.Events = new Collection<Event>();
            if (NhsRoute.IsBrandPartnerNhsPro)
            {
                if (model.IsBilled) //Changes because 58867
                {
                    var events = _communityService.GetCommunityEvents(community.CommunityId, community.BuilderId).ToList();
                    model.Events = events.Select(e => new Event
                    {
                        EventId = e.EventID,
                        Title = e.Title,
                        Description = e.Description,
                        EventFlyerUrl = e.EventFlyerURL,
                        EventType = e.EventTypeCode,
                        EventEndDate = e.EventEndTime,
                        EventStartDate = e.EventStartTime
                    }).ToList();

                }

                var promotions = _communityService.GetCommunityDetailPromotions(community.CommunityId, community.BuilderId).ToList();
                if (!model.IsBilled) //Changes because 58867
                    promotions.RemoveAll(promo => promo.PromoTypeCode != "COM");

                model.Promotions = promotions.Select(p => new Promotion
                {
                    PromoId = p.PromoID,
                    PromoTextShort = p.PromoTextShort,
                    PromoTextLong = p.PromoTextLong,
                    PromoFlyerUrl = p.PromoFlyerURL,
                    PromoType = p.PromoTypeCode,
                    PromoEndDate = p.PromoEndDate,
                    PromoStartDate = p.PromoStartDate,
                    PromoUrl = p.PromoURL
                }).ToList();
            }
            else
            {
                var promotions = _communityService.GetCommunityDetailPromotions(community.CommunityId, community.BuilderId);
                model.Promotions = promotions.Where(w => w.PromoTypeCode != "AGT").Take(3).Select(p => new Promotion
                {
                    PromoId = p.PromoID,
                    PromoTextShort = p.PromoTextShort,
                    PromoTextLong = p.PromoTextLong,
                    PromoFlyerUrl = p.PromoFlyerURL,
                    PromoType = p.PromoTypeCode,
                    PromoEndDate = p.PromoEndDate,
                    PromoStartDate = p.PromoStartDate,
                    PromoUrl = p.PromoURL
                }).ToList();

            }

            // TODO: Remove this logic for image viewer from here, gallery is now loading async so should do the image viewer
            if (RouteParams.ShowPopupPlayer.Value<bool>())
            {
                model.PlayerMediaObjects = _communityService.GetMediaPlayerObjects(community.CommunityId, true, false, false, ImageSizes.Large).ToList();

                foreach (var ext in model.ExternalMediaLinks)
                    model.PlayerMediaObjects.Add(ext);

                FillExtendedMediaObjectsInfo(model);
            }
            model.Size = new KeyValuePair<int, int>(460, 307);
            model.PageUrl = model.Globals.CurrentUrl;
        }

        /// <summary>
        /// this method fills out the map data in the Model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="community"></param>
        private void AssemblePropertyMapViewModel(IPropertyMapViewModel model, Community community)
        {
            model.MarketId = community.MarketId;
            model.CommunityId = community.CommunityId;
            model.BuilderId = community.BuilderId;
            model.Latitude = (double)community.Latitude;
            model.Longitude = (double)community.Longitude;
            model.DrivingDirections = FormattedText(community.SalesOffice.DrivingDirections) ?? string.Empty;
            int count = NhsRoute.CurrentRoute.Function == Pages.CommunityDetailv1 ? 6 : 5;

            var img = _communityService.GetBuilderMap(community.AllImages);

            if (img != null)
                model.BuilderMapUrl = img.ImagePath + img.ImageTypeCode + "_" + img.ImageName;

            model.UnderneathMapCommName = community.CommunityName;
            model.UnderneathMapAddress = community.SalesOffice.Address1 + " " + community.SalesOffice.Address2 + " <br />" + community.SalesOffice.City + ", " + community.State.StateName + " " +
                                         community.SalesOffice.ZipCode;

            model.EnableRequestAppointment = true;

        }

        #region Helper Methods to build the Model

        //private bool IsBrochureActive(string email, int communityId, int builderId)
        //{
        //    var brochureStatus = _profileService.GetBrochureStatus(email, communityId, builderId, 0, 0, NhsRoute.PartnerId);
        //    return brochureStatus != BrochureStatus.Expired;
        //}

        private HomeResultsView GetCommunityHomeResults(Community community, bool resetSearchParams)
        {
            var parameters = GetSearchParams(community.CommunityId, SortOrder.Status, 0, community.ListingTypeFlag);
            parameters.BuilderId = community.BuilderId;

            parameters.MarketId = community.MarketId;

            //All homes tab (reset search params)
            if (_searchParameters.HomeStatus == -1 || resetSearchParams)
            {
                parameters.HomeStatus = 0;
                return _listingService.GetHomeResults(parameters, true);
            }

            //Quick move-in tab (reset search params but pass quick move-in filter)
            if (_searchParameters.HomeStatus == (int)HomeStatusType.QuickMoveIn)
            {
                parameters.HomeStatus = 5;
                return _listingService.GetHomeResults(parameters, true);
            }

            //Matching homes tab (use session search params)
            parameters = _searchParameters.Clone() as NhsPropertySearchParams;
            parameters.EndRecord = 0;
            return _listingService.GetHomeResults(parameters, true);
        }

        private bool SetSaveToAccountFlag()
        {
            if (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser) return false;
            return true;
        }

        /// <summary>
        /// Display homeCount with format
        /// </summary>
        /// <param name="homeCount"></param>
        /// <returns></returns>
        private string HomesAvailableLabel(int? homeCount)
        {
            if (homeCount == null || homeCount == 0) return string.Empty;
            return homeCount > 1 ? string.Format("{0} " + LanguageHelper.NewHomes, homeCount) : string.Format("{0} " + LanguageHelper.NewHome, homeCount);
        }

        private string FormattedPrice(decimal priceHigh, decimal priceLow, string bcType)
        {
            if (!ShowCommunityPriceDetailsInfo(bcType)) return string.Empty;
            return StringHelper.PrettyPrintRange(priceLow, priceHigh, "c0", LanguageHelper.From.ToTitleCase());
        }

        private static bool ShowCommunityPriceDetailsInfo(string bcType)
        {
            if (bcType == "X" || bcType == "C") return false;

            return true;
        }

        private string FormattedSqFeet(int sqFtLow, int sqFtHigh)
        {
            if (sqFtLow == 0 && sqFtHigh == 0) return string.Empty;

            if (sqFtLow == sqFtHigh) return string.Format("({0} " + LanguageHelper.MSG_SQ_FT + ")", sqFtHigh);

            if (sqFtLow == 0) return string.Format("({0} " + LanguageHelper.MSG_SQ_FT + ")", sqFtHigh);

            if (sqFtHigh == 0) return string.Format("({0} " + LanguageHelper.MSG_SQ_FT + ")", sqFtLow);

            return string.Format("({0} - {1} " + LanguageHelper.MSG_SQ_FT + ")", sqFtLow, sqFtHigh);

        }


        private string FormattedText(string text)
        {
            if (string.IsNullOrEmpty(text)) return string.Empty;

            string descriptionText = ParseHTML.StripHTML(text);

            return descriptionText.Replace("\r", string.Empty).Replace("\n", string.Empty);
        }
        #endregion
    }
}

