﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers.Lead;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Domain.Model.Web.Partials;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;
using Resources;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class EmailTemplateController
    {
        #region New Template Emails

        private EmailTemplateViewModel GetEmailTemplateViewModel(bool isRecoEmail)
        {
            var model = new EmailTemplateViewModel();
            var recoCount = RouteParams.RecoCount.Value<int>();
            model.ShowMatchingComm = RouteParams.Showmc.Value<bool>();
            var requestId = RouteParams.RequestID.Value<int>();
            model.RequestId = requestId;
            var requesInfo = _leadService.GetLeadEmailInfo(requestId);
            var requestItemInfo = _leadService.GetRequestItemInformation(requestId);

            var utmCampainType = isRecoEmail ? "recommended" : (recoCount > 0 ? "brochurerec" : "brochurenorec");
            var utmSource = isRecoEmail ? "recommended" : "brochure";

            if (requestItemInfo.Rows.Count > 0)
            {
                BuildBeaconLink(model, DBValue.GetString(requestItemInfo.Rows[0]["email_address"]));
                model.ReplacementTags.Add(new ContentTag(ContentTagKey.Email, DBValue.GetString(requestItemInfo.Rows[0]["email_address"])));
                if (isRecoEmail)
                {
                    FillLeadRecomInformation(ref model, requesInfo, requestItemInfo, utmSource, utmCampainType);
                }
                else
                {
                    FillLeadEmailInfomation(ref model, requesInfo, requestItemInfo, utmSource, utmCampainType);
                    model.ReplacementTags.Add(new ContentTag(ContentTagKey.MatchingCommunities, model.GetMatchingCommunities()));
                    var recommendCommsHtml = recoCount == 0 ? "" : model.GetRecommendComms();
                    model.ReplacementTags.Add(new ContentTag(ContentTagKey.RecommendComms, recommendCommsHtml));
                }
            }  

            var profileInfo = model.SourceProperties.FirstOrDefault();

            if (profileInfo != null)
            {
                model.UserEmail = profileInfo.EmailAddress;
            }

            
            BuildStaticLinks(model);

            model.ReplacementTags.Add(new ContentTag(ContentTagKey.BrochureUtmEmailSource, utmSource));
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.BrochureUtmEmailCampaign, utmCampainType));
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.FirstName, RouteParams.FirstName.Value<string>()));
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.SiteName, _partnerService.GetPartner(NhsRoute.PartnerId).SiteName));
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.RecoCount, recoCount.ToType<string>()));

            var source = model.SourceProperties.FirstOrDefault();

            if (source != null)
            {
                model.ReplacementTags.Add(new ContentTag(ContentTagKey.CommunityId, source.CommunityId.ToType<string>()));
                model.ReplacementTags.Add(new ContentTag(ContentTagKey.RequestId, requestId.ToType<string>()));
                model.ReplacementTags.Add(new ContentTag(ContentTagKey.PropertyName, source.PropertyName));
                model.ReplacementTags.Add(new ContentTag(ContentTagKey.BrandName, source.BrandName));
                model.ReplacementTags.Add(new ContentTag(ContentTagKey.CommunityName, source.CommunityName));
                model.ReplacementTags.Add(new ContentTag(ContentTagKey.PlanName, source.PlanName));
                model.ReplacementTags.Add(new ContentTag(ContentTagKey.ResourceDomain, Configuration.ResourceDomain));
                model.ReplacementTags.Add(new ContentTag(ContentTagKey.Year, DateTime.Today.Year.ToType<string>()));
                
                if (!isRecoEmail)
                {
                    PrintLeadSubject(source.PropertyName, source.BrandName, source.IsComm, false);
                    model.ReplacementTags.Add(new ContentTag(ContentTagKey.BrochureUrl, source.BrochureUrl.Replace("freebrochure2", "freebrochure1")));
                }
                else
                    PrintRecoSubject(source.PropertyName);
            }

            return model;
        }

        private void FillLeadRecomInformation(ref EmailTemplateViewModel model, LeadEmail requesInfo, DataTable requestItemInfo, string utmSource, string utmCampainType)
        {
            var sourceCommunityId = RouteParams.SourceCommunityId.Value<int>();
            var sourcePlanId = RouteParams.SourcePlanId.Value<int>();
            var sourceSpecId = RouteParams.SourceSpecId.Value<int>();
            var sourceLead = new LeadEmailItem
            {
                FirstName = DBValue.GetString(requestItemInfo.Rows[0]["first_name"]),
                EmailAddress = DBValue.GetString(requestItemInfo.Rows[0]["email_address"])
            };

            if (sourceCommunityId != 0)
            {
                var sourceCommunity = _communityService.GetCommunity(sourceCommunityId);
                sourceLead.CommunityId = sourceCommunity.CommunityId;
                sourceLead.BuilderId = sourceCommunity.BuilderId;
                sourceLead = FillLeadInfoComm(sourceLead, utmSource, utmCampainType, false, false, sourceCommunity);
                sourceLead.IsComm = true;
            }
            else if (sourcePlanId != 0)
            {
                var sourcePlan = _listingService.GetPlan(sourcePlanId);
                sourceLead.PlanId = sourcePlanId;
                sourceLead = FillLeadInfoHome(sourceLead, requesInfo, utmSource, utmCampainType, false, null, sourcePlan);
            }
            else if (sourceSpecId != 0)
            {
                var sourceSpec = _listingService.GetSpec(sourceSpecId);
                sourceLead.SpecId = sourceSpecId;
                sourceLead = FillLeadInfoHome(sourceLead, requesInfo, utmSource, utmCampainType, false, sourceSpec);
                sourceLead.IsSpec = true;
            }

            if (sourceLead != null)
            {
                model.SourceProperties.Add(sourceLead);

                var community = new LatLong(sourceLead.Latitude, sourceLead.Longitude);

                var recoProperties = new List<LeadEmailItem>();
                foreach (DataRow row in requestItemInfo.Rows)
                {
                    var leadItem = new LeadEmailItem
                    {
                        RequestTypeCode = DBValue.GetString(row["request_type_code"]),
                        EmailAddress = DBValue.GetString(row["email_address"]),
                        CommunityId = row["community_id"].ToType<int>(),
                        BuilderId = row["builder_id"].ToType<int>(),
                        PlanId = row["plan_id"].ToType<int>(),
                        SpecId = row["specification_id"].ToType<int>(),
                        UniqueKey = row["source_request_key"].ToType<string>(),
                        FirstName = row["first_name"].ToType<string>(),
                        LastName = row["last_name"].ToType<string>(),
                        RequestItemId = row["request_item_id"].ToType<string>()
                    };

                    leadItem = FillLeadInfoComm(leadItem, utmSource, utmCampainType, true);
                    if (leadItem != null)
                    {
                        leadItem.SourcePropertyName = sourceLead.PropertyName;
                        leadItem.Distance =
                            Math.Round(new LatLong(leadItem.Latitude, leadItem.Longitude).ArcDistance(community), 2);
                        recoProperties.Add(leadItem);
                    }
                }
                model.RecoProperties = recoProperties.OrderBy(p => p.Distance);
            }
        }

        private void FillLeadEmailInfomation(ref EmailTemplateViewModel model, LeadEmail requesInfo, DataTable requestItemInfo, string utmSource, string utmCampainType)
        {
            foreach (DataRow row in requestItemInfo.Rows)
            {
                var leadItem = new LeadEmailItem
                {
                    RequestTypeCode = DBValue.GetString(row["request_type_code"]),
                    EmailAddress = DBValue.GetString(row["email_address"]),
                    CommunityId = row["community_id"].ToType<int>(),
                    BuilderId = row["builder_id"].ToType<int>(),
                    PlanId = row["plan_id"].ToType<int>(),
                    SpecId = row["specification_id"].ToType<int>(),
                    UniqueKey = row["source_request_key"].ToType<string>(),
                    FirstName = row["first_name"].ToType<string>(),
                    LastName = row["last_name"].ToType<string>(),
                    RequestItemId = row["request_item_id"].ToType<string>()
                };


                if (requesInfo.RequestTypeCode == LeadType.Home)
                {
                    leadItem = FillLeadInfoHome(leadItem, requesInfo, utmSource, utmCampainType);
                    if (leadItem != null)
                        model.SourceProperties.Add(leadItem);
                }
                else
                {
                    leadItem = FillLeadInfoComm(leadItem, utmSource, utmCampainType, false);
                    if (leadItem != null)
                        model.SourceProperties.Add(leadItem);
                }
            }
        }

        private LeadEmailItem FillLeadInfoHome(LeadEmailItem leadItem, LeadEmail requesInfo, string utmSource, string utmCampainType, bool includeBrochure = true, ISpec spec = null, IPlan plan = null)
        {
            Community community;
            leadItem.MarketId = requesInfo.MarketId;
            string imageThumbnail = string.Empty, homeStatus = string.Empty;
            int bedrooms, bathRooms, halfBaths, sqFt;
            decimal price, garages;
            bool isHotHome;
            if (leadItem.SpecId > 0)
            {
                spec = spec ?? _listingService.GetSpec(leadItem.SpecId);
                if (spec == null) return null;
                var image = _listingService.GetHomeImagesForSpec(spec.Plan, spec).FirstOrDefault();
                if (image != null)
                    imageThumbnail = string.Format("{0}/{1}_{2}", image.ImagePath, ImageSizes.HomeThumb,
                        image.ImageName);
                bedrooms = spec.Bedrooms.ToType<int>();
                bathRooms = spec.Bathrooms.ToType<int>();
                halfBaths = spec.HalfBaths.ToType<int>();
                garages = spec.Garages.ToType<decimal>();
                sqFt = spec.SqFt.ToType<int>();
                isHotHome = spec.IsHotHome.ToType<bool>();
                community = spec.Community ?? _communityService.GetCommunity(spec.CommunityId);
                homeStatus = spec.HomeStatus.ToType<string>();
                price = spec.Price;
                leadItem.PlanName = spec.PlanName;
                leadItem.IsSpec = true;
                leadItem.Address = !string.IsNullOrEmpty(spec.Address1) ? spec.Address1 : spec.Address2;
            }
            else
            {
                plan = plan ?? _listingService.GetPlan(leadItem.PlanId);
                if (plan == null) return null;
                var image = _listingService.GetHomeImagesForSpec(plan, spec).FirstOrDefault();
                if (image != null)
                    imageThumbnail = string.Format("{0}/{1}_{2}", image.ImagePath, ImageSizes.HomeThumb,
                        image.ImageName);
                bedrooms = plan.Bedrooms.ToType<int>();
                bathRooms = plan.Bathrooms.ToType<int>();
                halfBaths = plan.HalfBaths.ToType<int>();
                garages = plan.Garages.ToType<decimal>();
                sqFt = plan.SqFt.ToType<int>();
                isHotHome = plan.IsHotHome.ToType<bool>();
                community = plan.Community ?? _communityService.GetCommunity(plan.CommunityId);
                price = plan.Price;
                leadItem.PlanName = plan.PlanName;
            }

            leadItem.MarketId = community.CommunityId;
            leadItem.BrandName = community.Brand.BrandName;
            leadItem.CommunityName = community.CommunityName;
            //BEGIN-FIX: 74994            
            if (string.IsNullOrEmpty(leadItem.Address))
                leadItem.Address = string.Concat(community.SalesOffice.Address1, " ", community.SalesOffice.Address2);
            leadItem.City = community.City;
            leadItem.State = community.StateAbbr;
            leadItem.PostalCode = community.SalesOffice.ZipCode.Trim();
            //END-FIX: 74994
            
            leadItem.BrandImage = string.Concat(Configuration.IRSDomain, community.Brand.LogoSmall);

            leadItem.ImageThumbnail = imageThumbnail.IsValidImage()
                ? Configuration.IRSDomain +
                  imageThumbnail.Replace(ImageSizes.Small, ImageSizes.HomeThumb).Replace(".gif", ".jpg")
                :  GlobalResources14.Default.images.no_photo.no_photos_195x130_png;

            var homeAtrtributes = new List<string>();
            if(bedrooms>0)
                homeAtrtributes.Add(ListingUtility.FormatBedrooms(bedrooms, false, bedroomWord: LanguageHelper.Bedroom, bedroomsWord: LanguageHelper.Bedrooms));

            if (bathRooms > 0 || halfBaths > 0)
                homeAtrtributes.Add(string.Format("{0} {1}", ListingUtility.ComputeBathrooms(bathRooms, halfBaths),
                    (bathRooms + halfBaths) > 1 ? LanguageHelper.Bathrooms : LanguageHelper.Bath));

            if (garages > 0.0m)
                homeAtrtributes.Add(string.Format("{0:0.##} {1}", garages, garages > 1 ? LanguageHelper.Garages : LanguageHelper.Garage));

            if (sqFt > 0)
                homeAtrtributes.Add(string.Format("{0} {1}", sqFt, "sq.ft."));

            leadItem.FormatHomeAttrib = string.Join(" | ", homeAtrtributes);
            leadItem.ShowHotArea = isHotHome;


            if (community != null)
            {
                var promos = _communityService.GetCommunityDetailPromotions(community.CommunityId, community.BuilderId).Where(w => w.PromoTypeCode != "AGT");
                leadItem.Promos = promos;
                if (promos.Any())
                    leadItem.ShowPromoArea = true;

                leadItem.CommunityId = community.CommunityId;
                leadItem.BuilderId = community.BuilderId;
                leadItem.BuilderUrl = GetBuilderUrl(community.Builder.Url, community.CommunityId,
                    community.BuilderId, leadItem.SpecId, leadItem.PlanId, utmSource, utmCampainType);
                leadItem.Latitude = community.Latitude;
                leadItem.Longitude = community.Longitude;
            }

            leadItem.FormatPrice = FormatCurrency(homeStatus, price);

            var homeDetailUrl = new List<RouteParam>
            {
                leadItem.SpecId > 0
                    ? new RouteParam(RouteParams.SpecId, leadItem.SpecId)
                    : new RouteParam(RouteParams.PlanId, leadItem.PlanId)
            };
            //Home Detail Link
            var externalUrl = PathHelpers.GetAbsoluteURL(homeDetailUrl.ToUrl(Pages.HomeDetail));    
            externalUrl = HttpUtility.UrlEncode(externalUrl + String.Format(UtmParameters, utmSource, utmCampainType, "homename"));
            leadItem.DetailPageUrl = GetLogRedirectUrl(leadItem.BuilderId, leadItem.CommunityId, leadItem.SpecId, leadItem.PlanId, GetEventCode(false), externalUrl, string.Empty);
            leadItem.BuilderUrl = GetBuilderUrl(community.Builder.Url, leadItem.CommunityId, leadItem.BuilderId, 0, 0, utmSource, utmCampainType);
            if (includeBrochure)
            {
                var pdfBrochureUrl = _communityService.GetNonPdfBrochureUrl(leadItem.CommunityId, leadItem.PlanId, leadItem.SpecId, leadItem.BuilderId);

                if (string.IsNullOrEmpty(pdfBrochureUrl))
                    pdfBrochureUrl =
                        ViewBrochureHelper.ViewBrochureLink(leadItem.FirstName, leadItem.RequestItemId, leadItem.EmailAddress, "") +
                        String.Format(UtmParameters.Replace("?", "&"), utmSource, utmCampainType, "freebrochure2");

                pdfBrochureUrl = HttpUtility.UrlEncode(pdfBrochureUrl);

                leadItem.BrochureUrl = GetLogRedirectUrl(leadItem.BuilderId, leadItem.CommunityId, leadItem.SpecId, leadItem.PlanId, GetEventCode(false), pdfBrochureUrl, string.Empty);
            }
            leadItem.PropertyName = GetPropertyName(leadItem);
            return leadItem;
        }

        private LeadEmailItem FillLeadInfoComm(LeadEmailItem leadItem, string utmSource, string utmCampainType, bool isRecoComm, bool includeBrochure = true, Community comm = null)
        {
            comm = comm ?? _communityService.GetCommunity(leadItem.CommunityId);
            if (comm != null)
            {
                leadItem.IsComm = true;
                leadItem.Latitude = comm.Latitude;
                leadItem.Longitude = comm.Longitude;
                leadItem.BuilderId = comm.BuilderId;
                leadItem.BrandName = comm.Brand.BrandName;
                leadItem.CommunityName = comm.CommunityName;
                //BEGIN-FIX: 74994
                leadItem.Address = string.Concat(comm.SalesOffice.Address1, " ", comm.SalesOffice.Address2) ;
                leadItem.City = comm.SalesOffice.City;
                leadItem.State = comm.StateAbbr;
                leadItem.PostalCode = comm.SalesOffice.ZipCode.Trim();
                //END-FIX: 74994
                leadItem.BrandImage = string.Concat(Configuration.IRSDomain, comm.Brand.LogoSmall);


                var res = comm.ToCommunityDetail().ToUrl(RedirectionHelper.GetCommunityDetailPage());
                var externalUrl = PathHelpers.GetAbsoluteURL(res);
                externalUrl = HttpUtility.UrlEncode(externalUrl + String.Format(UtmParameters, utmSource, utmCampainType, "commname"));
                leadItem.DetailPageUrl = GetLogRedirectUrl(leadItem.BuilderId, leadItem.CommunityId, leadItem.SpecId, leadItem.PlanId, GetEventCode(isRecoComm), externalUrl, string.Empty);

                if (includeBrochure)
                {
                    var pdfBrochureUrl = _communityService.GetNonPdfBrochureUrl(leadItem.CommunityId, leadItem.PlanId, leadItem.SpecId, leadItem.BuilderId);
                    if (string.IsNullOrEmpty(pdfBrochureUrl))
                        pdfBrochureUrl = ViewBrochureHelper.ViewBrochureLink(leadItem.FirstName, leadItem.RequestItemId, leadItem.EmailAddress, "") +
                                         String.Format(UtmParameters.Replace("?", "&"), utmSource, utmCampainType, "freebrochure2");

                    pdfBrochureUrl = HttpUtility.UrlEncode(pdfBrochureUrl);
                    leadItem.BrochureUrl = GetLogRedirectUrl(leadItem.BuilderId, leadItem.CommunityId, leadItem.SpecId, leadItem.PlanId, GetEventCode(isRecoComm), pdfBrochureUrl, string.Empty);

                }

                leadItem.ImageThumbnail = comm.SpotlightThumbnail.IsValidImage()
                    ? Configuration.IRSDomain +
                      comm.SpotlightThumbnail.Replace(ImageSizes.Small, ImageSizes.HomeThumb)
                          .Replace(ImageSizes.Results, ImageSizes.CommDetailThumb)
                          .Replace(".gif", ".jpg")
                          .Replace(".png", ".jpg")
                    :  GlobalResources14.Default.images.no_photo.no_photos_195x130_png;

                leadItem.Promos = _communityService.GetCommunityDetailPromotions(leadItem.CommunityId, leadItem.BuilderId).Where(w => w.PromoTypeCode != "AGT");
                leadItem.ShowPromoArea = leadItem.Promos.Any();

                leadItem.BuilderUrl = GetBuilderUrl(comm.Builder.Url, leadItem.CommunityId, leadItem.BuilderId, 0, 0, utmSource, utmCampainType);

                if (comm.SpecHotHomeId > 0)
                {
                    leadItem.ShowHotArea = true;
                    leadItem.HotHomeTitle = comm.SpecHotHomeTitle;
                    leadItem.HotHomeDescription = comm.SpecHotHomeDescription;
                }
                else if (comm.PlanHotHomeId > 0)
                {
                    leadItem.ShowHotArea = true;
                    leadItem.HotHomeTitle = comm.PlanHotHomeTitle;
                    leadItem.HotHomeDescription = comm.PlanHotHomeDescription;
                }

                decimal priceLow, priceHigh;
                if (comm.HomeCount.ToType<int>() > 1)
                {
                    priceLow = comm.MinPrice.ToType<decimal>();
                    priceHigh = comm.MaxPrice.ToType<decimal>();
                }
                else
                {
                    priceLow = comm.PriceLow;
                    priceHigh = comm.PriceHigh;
                }

                if (priceLow > 0 || priceHigh > 0)
                {
                    var priceLabel = string.Empty;
                    if (priceLow > 0)
                        priceLabel = StringHelper.FormatCurrency(priceLow);

                    if (priceHigh > 0)
                    {
                        if (string.IsNullOrEmpty(priceLabel))
                            priceLabel = StringHelper.FormatCurrency(priceHigh);
                        else if (priceLow != priceHigh)
                            priceLabel += " - " + StringHelper.FormatCurrency(priceHigh);
                    }

                    leadItem.FormatPrice = priceLabel;
                }

                leadItem.PropertyName = GetPropertyName(leadItem);
                return leadItem; 
            }
            return null;
        }

        private string GetPropertyName(LeadEmailItem property)
        {
            string text;
            if (property.IsComm)
                text = property.CommunityName;
            else if (property.IsSpec)
            {
                text = string.Format("{0} {1} {2}", string.IsNullOrEmpty(property.Address) ? property.PlanName : property.Address, LanguageHelper.At, property.CommunityName).Trim();
            }
            else
            {
                text = string.Format("{0} {1} {2}", property.PlanName, LanguageHelper.At, property.CommunityName).Trim();
            }

            return text;
        }

        private string PartnerLogo()
        {
            var partnerInfo = _partnerService.GetPartner(NhsRoute.PartnerId);
            var logo = partnerInfo.PartnerLogo.Replace("[resource:]", Configuration.ResourceDomain);
            return logo;
        }

        private void FillBaseEmailTemplateViewModel(BaseEmailTemplateViewModel model)
        {
            var partnerInfo = _partnerService.GetPartner(NhsRoute.PartnerId);
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.RootDomain, PathHelpers.GetAbsoluteURL("/")));
            //This may need to be changed to is brand partner pro, to check with Mario and Praveen
            model.ReplacementTags.Add(!NhsRoute.IsBrandPartnerNhsPro
                ? new ContentTag(ContentTagKey.SiteLogo, PartnerLogo())
                : new ContentTag(ContentTagKey.SiteLogo, model.BrandPartnerLogo));

            if(model.UsedPartnerName)
                model.ReplacementTags.Add(new ContentTag(ContentTagKey.SiteName, partnerInfo.PartnerName));
            else
                model.ReplacementTags.Add(new ContentTag(ContentTagKey.SiteName, partnerInfo.SiteName));
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.SupportEmail, partnerInfo.FromEmail));

            
            if (!NhsRoute.IsBrandPartnerNhsPro)
            {
                //UnsubscribeUrl Parameters
                var listParam = new List<RouteParam>();

                if (string.IsNullOrWhiteSpace(model.UserEmail) == false)
                {
                    listParam.Add(new RouteParam(RouteParams.UserId, model.UserEmail, RouteParamType.QueryString));
                }
                //*****************************
                var url = PathHelpers.GetAbsoluteURL(listParam.ToUrl(Pages.Unsubscribe));
                model.ReplacementTags.Add(new ContentTag(ContentTagKey.UnsubscribeUrl, url));
            }
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.PolicyUrl, PathHelpers.GetAbsoluteURL(new List<RouteParam>().ToUrl(Pages.PrivacyPolicy))));
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.Year, DateTime.Today.Year.ToType<string>()));
        }

        private void BuildStaticLinks(EmailTemplateViewModel model)
        {
            FillBaseEmailTemplateViewModel(model);
            
            var learnParams = new List<RouteParam>();
            var qParams = new List<RouteParam>();
           
                //Learn More
                learnParams.Add(new RouteParam(RouteParams.Category, "Why-Buy-a-New-Home"));
                learnParams.Add(new RouteParam(RouteParams.CategoryId, "45"));
                model.ReplacementTags.Add(new ContentTag(ContentTagKey.LearnMoreUrl, PathHelpers.GetAbsoluteURL(learnParams.ToUrl(Pages.HomeGuideCategory))));
                qParams.Add(new RouteParam(RouteParams.Article, "Questions-To-Ask-Your-Builder"));
                model.ReplacementTags.Add(new ContentTag(ContentTagKey.QuestionsUrl, PathHelpers.GetAbsoluteURL(qParams.ToUrl(Pages.HomeGuideArticle))));
        
            //Get Prequalified Today
            var prequalUrl = "https://www.prequalplus.com/landing?gate=BDX&iid=bdx:[pcode]:email";
            prequalUrl = prequalUrl.Replace("[pcode]", "pro");
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.GetQualifiedUrl, prequalUrl));
        }

        private void BuildBeaconLink(EmailTemplateViewModel model, string email)
        {
            //Beacon Url
            var beaconParams = new List<RouteParam>
            {
                new RouteParam(RouteParams.LogEvent, LogImpressionConst.OpenEmailBrochure),
                new RouteParam(RouteParams.Email, email,RouteParamType.QueryString)
            };
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.BeaconUrl, PathHelpers.GetAbsoluteURL(beaconParams.ToUrl(Pages.EventLogger))));
        }

        #endregion
    }
}
