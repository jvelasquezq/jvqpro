﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Kendo.Mvc.Extensions;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Constants.Route;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects;
using Nhs.Search.Objects.Constants;
using Nhs.Utility.Common;
using Nhs.Utility.Html;
using Nhs.Web.UI.AppCode.Extensions;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;
using StructureMap;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class HomeDetailController
    {
        #region Assemble ViewModel
        private HomeDetailViewModel GetViewModel(IPlan plan, ISpec spec, bool addNoIndexTag, Community currentCommunity)
        {
            var brand = _brandService.GetBrandById(currentCommunity.BrandId, NhsRoute.BrandPartnerId);
            var market = _marketService.GetMarket(currentCommunity.MarketId);

            string homeTitle;
            string stateName, stateAbbr;
            var sqFtDisplay = string.Empty;
            var storyDisplay = string.Empty;
            var isPlan = spec == null;
            int bedRooms;
            int bathRooms;
            decimal garage;
            int sqFt;
            decimal price;
            var showSaveToAccount = true;
            var homeBasicSpecs = new StringBuilder();
            const bool showExpiredRequestBrochureLink = false;

            ListingType listingType;
            int listingId; // This is either a plan id or a spec id 
            string virtualTourUrl;
            string floorPlanViewerUrl;
            string imageThumbnail;
            int halfBaths;

            string homeMarketingDescription;
            string homeDescription;
            int homeStatus;
            int stories;
            var listingServiceHelper = new ListingServiceHelper { UseHub = _listingService.UseHub };

            if (isPlan)
            {
                listingType = ListingType.Plan;
                listingId = plan.PlanId;
                homeTitle = plan.PlanName;
                bedRooms = plan.Bedrooms.ToType<int>();
                bathRooms = plan.Bathrooms.ToType<int>();
                garage = plan.Garages.ToType<decimal>();
                price = plan.Price;
                virtualTourUrl = plan.VirtualTourUrl;
                floorPlanViewerUrl = _listingService.GetFloorPlanViewerUrl(plan, null);
                homeMarketingDescription = plan.HomeMarketingDescription;
                homeDescription = string.IsNullOrEmpty(plan.Description)
                                      ? listingServiceHelper.GetAutoDescriptionForHome(plan, null)
                                      : ParseHTML.StripHTML(plan.Description).Replace("\r", string.Empty).Replace("\n", string.Empty);
                imageThumbnail = plan.ImageThumbnail;
                homeStatus = (int)HomeStatusType.ReadyToBuild;
                halfBaths = plan.HalfBaths ?? 0;
                sqFt = plan.SqFt ?? 0;
                stories = plan.Stories.ToType<int>();
                stateName = currentCommunity.State.StateName;
                stateAbbr = currentCommunity.StateAbbr;

            }
            else
            {
                listingType = ListingType.Spec;
                listingId = spec.SpecId;
                homeTitle = string.Format("{0} ({1})", (spec).Address1, spec.PlanName);
                bedRooms = spec.Bedrooms.ToType<int>();
                bathRooms = spec.Bathrooms.ToType<int>();
                garage = spec.Garages.ToType<int>();
                price = spec.Price;
                virtualTourUrl = spec.VirtualTourUrl;
                floorPlanViewerUrl = _listingService.GetFloorPlanViewerUrl(null, spec);
                homeMarketingDescription = spec.HomeMarketingDescription;
                homeDescription = string.IsNullOrEmpty(spec.Description)
                                      ? listingServiceHelper.GetAutoDescriptionForHome(spec.Plan, spec)
                                      : ParseHTML.StripHTML(spec.Description).Replace("\r", string.Empty).Replace("\n", string.Empty);
                homeStatus = spec.HomeStatus;
                imageThumbnail = spec.ImageThumbnail;
                halfBaths = spec.HalfBaths ?? 0;
                sqFt = spec.SqFt ?? 0;
                stories = spec.Stories.ToType<int>();
                stateName = currentCommunity.State.StateName;
                stateAbbr = currentCommunity.StateAbbr;
            }


            if (bedRooms > 0)
                homeBasicSpecs.AppendFormat("<li>{0} " + (bedRooms == 1 ? LanguageHelper.Bedroom : LanguageHelper.Bedrooms) + "</li>", bedRooms);
            //homeBasicSpecs.Append(string.Concat("<li>", bedRooms, "bedroom", "</li>"));

            if (bathRooms > 0)
                homeBasicSpecs.AppendFormat("<li>{0} " + (bathRooms == 1 ? LanguageHelper.MSG_BATHROOM : LanguageHelper.Bathrooms) +"</li>",
                                            ListingHelper.ComputeBathrooms(bathRooms, halfBaths));
            //homeBasicSpecs.Append(string.Concat("<li>", bathRooms, "bathroom", "</li>"));

            if (garage > 0.0m)
                homeBasicSpecs.AppendFormat("<li>{0:0.##} " + (garage == 1 ? LanguageHelper.Garage : LanguageHelper.Garages) + "</li>", garage);
            //homeBasicSpecs.Append(string.Concat("<li>", garage, " garage", "</li>"));

            //Phone number - Get builder's toll free number
            string phoneNumber = _communityService.GetTollFreeNumber(NhsRoute.PartnerId, currentCommunity.CommunityId,
                                                                     currentCommunity.BuilderId);

            if (string.IsNullOrEmpty(phoneNumber)) //else use community SO number
                phoneNumber = currentCommunity.PhoneNumber;

            if (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser)
                showSaveToAccount = false;

            AddHomeToUserPlanner(plan.CommunityId, listingId, listingType, currentCommunity.BuilderId, isPlan);
            // Check for addtoprofile flag and add to planner

            //Sqft
            if (sqFt > 0)
            {
                sqFtDisplay = string.Format("{0} <abbr title=\"square feet\">sq.ft.</abbr>",
                                            string.Format("{0:###,###}", sqFt));
                homeBasicSpecs.Append(string.Concat("<li>", sqFtDisplay, "</li>"));
            }

            //Stories
            if (stories > 0)
            {
                storyDisplay = string.Format("{0} " + (stories == 1 ? LanguageHelper.Story : LanguageHelper.Stories), stories);
                homeBasicSpecs.Append(string.Concat("<li>", storyDisplay, "</li>"));
            }

            bool sendAlerts = false;
            bool partnerUsesMatchmaker = _partnerService.GetPartner(NhsRoute.PartnerId).UsesMatchMaker.ToBool();

            bool isShowCaseActive = brand.BrandShowCase != null && brand.BrandShowCase.StatusId == 1;
            bool hasTheBrandActiveComms = _communityService.GetCommunitiesByBrandId(NhsRoute.PartnerId, brand.BrandId).Any();

            ////Prepop User Info
            if (partnerUsesMatchmaker && !UserSession.HasUserOptedIntoMatchMaker)
                sendAlerts = true;

            //Init view model
            var viewModel = new HomeDetailViewModel();
            if (NhsRoute.IsBrandPartnerNhsPro)
            {
                viewModel.SavedPropertysProCrm = UserSession.UserProfile.Planner.GetSavedProperties();
            }
            viewModel.Plan = plan;
            viewModel.PlanId = plan.PlanId;
            viewModel.SpecId = isPlan ? 0 : spec.SpecId;
            viewModel.PropertyId = listingId;
            viewModel.ImageThumbnail = imageThumbnail;
            //Community Info
            viewModel.CommunityId = plan.CommunityId;
            //BEGIN-FIX: 74994            
            viewModel.CommunityCity = currentCommunity.SalesOffice.City;
            viewModel.ZipCode = currentCommunity.SalesOffice.ZipCode;
            //END-FIX: 74994            

            //BEGIN-FIX 70914
            if (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser)
                viewModel.LiveOutside = string.IsNullOrEmpty(UserSession.UserProfile.PostalCode);
            //END FIX 70914

            viewModel.CommunityName = currentCommunity.CommunityName;
            viewModel.BuilderName = currentCommunity.Builder.BuilderName;
            viewModel.MarketName = currentCommunity.Market.MarketName;
            viewModel.CommunityDescription = FormattedText(currentCommunity.CommunityDescription);
            viewModel.BuilderId = currentCommunity.BuilderId;
            viewModel.MarketId = currentCommunity.MarketId;

            viewModel.State = stateName;
            viewModel.StateAbbr = stateAbbr;

            viewModel.SelectedStateAbbr = stateAbbr;
            viewModel.MarketName = market.MarketName;
            viewModel.IsAdult = currentCommunity.IsAdult.ToType<bool>();
            viewModel.IsAgeRestricted = currentCommunity.IsAgeRestricted.ToType<bool>();
            viewModel.IsCondo = currentCommunity.IsCondo.ToType<bool>();
            viewModel.IsGreenProgram = currentCommunity.IsGreenProgram.ToType<bool>();
            viewModel.IsGreen = currentCommunity.IsGreen.ToType<bool>();
            viewModel.IsGated = currentCommunity.IsGated.ToType<bool>();
            viewModel.IsMasterPlanned = currentCommunity.IsMasterPlanned.ToType<bool>();
            viewModel.IsCommunityMultiFamily = viewModel.IsCondo || currentCommunity.IsTownHome.ToType<bool>();

            //Location Info
            //BEGIN-FIX: 74994
            viewModel.SalesOfficeAddress1 = currentCommunity.SalesOffice.Address1;
            viewModel.SalesOfficeAddress2 = currentCommunity.SalesOffice.Address2;
            //END-FIX: 74994

            //BEGIN-FIX: 79672
            var useSalesOfficeAddress = isPlan || (spec != null && string.IsNullOrEmpty(spec.Address1) && string.IsNullOrEmpty(spec.Address2));
            viewModel.HomeAddress1 = useSalesOfficeAddress ? viewModel.SalesOfficeAddress1 : spec.Address1;
            viewModel.HomeAddress2 = useSalesOfficeAddress ? viewModel.SalesOfficeAddress2 : spec.Address2;
            //END-FIX: 79672

            viewModel.PopUpViewerName = isPlan ? plan.PlanName : string.Format("{0} ({1} Plan)", (spec).Address1, spec.PlanName);
            viewModel.ShowBrochure = ((string.IsNullOrEmpty(plan.Community.BCType) || plan.Community.BCType.ToLower() != BuilderCommunityType.ComingSoon));
            viewModel.Latitude = (double)currentCommunity.Latitude;
            viewModel.Longitude = (double)currentCommunity.Longitude;
            viewModel.IsPageCommDetail = false;
            viewModel.DrivingDirections = FormattedText(currentCommunity.SalesOffice.DrivingDirections) ?? string.Empty;
            viewModel.EnableRequestAppointment = true;
            viewModel.NewUser = NhsRoute.CurrentRoute.Params.Any(p => p.Name == RouteParams.NewUser);
            //Home Info
            viewModel.HomeTitle = homeTitle;
            viewModel.PriceDisplay = string.Format(LanguageHelper.From.ToTitleCase() + ": <span>{0:C0}</span>", isPlan ? plan.Price : spec.Price);
            viewModel.PriceLow = isPlan ? plan.Price.ToType<string>() : spec.Price.ToType<string>();
            viewModel.ExcludesLand = Convert.ToBoolean(isPlan ? plan.LandExcluded : spec.LandExcluded);
            viewModel.StoryDisplay = storyDisplay;
            viewModel.DisplaySqFeet = string.Format("{0}", sqFtDisplay);
            viewModel.SqFtDisplay = sqFtDisplay;
            viewModel.BedroomsAbbr = string.Format(@"<br /><strong>{0}</strong> / ", plan.FormatBedrooms(false));
            viewModel.BathroomsAbbr = string.Format(@"<strong>{0}</strong> ", plan.FormatBathrooms(false));
            viewModel.GaragesAbbr = plan.Garages > 0.0m
                                        ? string.Format(@" / <strong>{0}</strong> ", plan.FormatGarages())
                                        : string.Empty;
            viewModel.HomeSpecsText = this.ComputeHomeSpecsText(plan);
            viewModel.HomeMarketingDescription = homeMarketingDescription;
            viewModel.HomeDescription = homeDescription;
            viewModel.ShowHotHomeSection = plan.IsHotHome == 1;

            viewModel.HomeStatus = homeStatus;
            viewModel.BasicHomeSpecs = homeBasicSpecs.ToString();

            //Contact Info
            viewModel.PhoneNumber = phoneNumber;
            viewModel.TrackingPhoneNumber = currentCommunity.TrackingPhoneNumber;
            viewModel.HoursOfOperation = currentCommunity.SalesOffice.HoursOfOperation != null ?
                currentCommunity.SalesOffice.HoursOfOperation.ToType<string>().Replace("<br />", "   ").Replace("<br/>", "   ") : "";
            viewModel.SaleAgents = _communityService.GetSalesAgents(currentCommunity.SalesOfficeId);

            //Builder Info
            viewModel.BuilderName = currentCommunity.Builder.BuilderName ?? string.Empty;
            viewModel.CorporationName = currentCommunity.Builder.ParentBuilder == null
                                            ? viewModel.BuilderName
                                            : currentCommunity.Builder.ParentBuilder.BuilderName ?? string.Empty;

            viewModel.BrandName = brand.BrandName;
            viewModel.HasShowCaseInformation = brand.HasShowCase && isShowCaseActive && hasTheBrandActiveComms;
            viewModel.BrandId = currentCommunity.BrandId;
            viewModel.BrandThumbnail = string.Concat(Configuration.ResourceDomain, brand.LogoMedium);
            viewModel.BuilderLogo = brand.LogoMedium;
            viewModel.BuilderLogoSmall = brand.LogoSmall;
            viewModel.BuilderUrl = !string.IsNullOrEmpty(currentCommunity.Builder.Url)
                                       ? currentCommunity.Builder.Url
                                       : (!string.IsNullOrEmpty(currentCommunity.Builder.ParentBuilder.Url)
                                              ? currentCommunity.Builder.ParentBuilder.Url
                                              : string.Empty);
            viewModel.CommunityUrl = currentCommunity.CommunityUrl ?? string.Empty;
            viewModel.EnvisionUrl = plan.EnvisionUrl;
            viewModel.PlanName = isPlan ? plan.PlanName : spec.PlanName;
            //Prepop User Info
            viewModel.Name = (UserSession.UserProfile.FirstName + " " + UserSession.UserProfile.LastName).Trim();
            viewModel.UserPhoneNumber = UserSession.UserProfile.DayPhone;
            viewModel.UserPostalCode = UserSession.UserProfile.PostalCode;
            viewModel.MailAddress = UserSession.UserProfile.LogonName;
            viewModel.SendAlerts = sendAlerts;
            viewModel.PartnerUsesMatchmaker = partnerUsesMatchmaker;
            viewModel.ShowUserPhoneNumber = NhsUrl.ShowUserPhoneNumber;

            //Home Ancillary Info
            viewModel.Amenities = _communityService.GetAmenities(currentCommunity.IAmenities, plan.Community.IOpenAmenities);
            viewModel.Utilities = _communityService.GetCommunityUtilities(plan.CommunityId, NhsRoute.PartnerId);
            viewModel.Schools = _communityService.GetSchools(NhsRoute.PartnerId, currentCommunity.CommunityId);

            // Get credit score links
            string creditScoreLink1 = "", creditScoreLink2 = "";
            _communityService.GetCreditScoreLinks(ref creditScoreLink1, ref creditScoreLink2);
            viewModel.CreditScoreLink1 = creditScoreLink1;
            viewModel.CreditScoreLink2 = creditScoreLink2;

            string tagValue = isPlan ? string.Format("{0}-{1}", "planid", viewModel.PlanId) : string.Format("{0}-{1}", "specid", viewModel.SpecId);
            var tags = new List<ContentTag> { new ContentTag(ContentTagKey.SpecOrPlanPlusId, tagValue) };
            var isBilled = false;
            var partnerMaskIndex = _partnerService.GetPartnerMaskIndex(NhsRoute.PartnerId);
            var partnerSortValue = plan.Community.PartnerMask.Substring(partnerMaskIndex - 1, 1);

            if ("F" != partnerSortValue)
                isBilled = _communityService.GetCommunityBilledStatus(NhsRoute.PartnerId, currentCommunity.BuilderId,
                                                                      currentCommunity.CommunityId, viewModel.MarketId);



            var userLoggedIn = (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser);

            viewModel.LeadSectionState = SetLeadSectionState(isBilled, userLoggedIn).ToType<int>();
            viewModel.ShowSocialIcons = ShowSocialIcons(isBilled);
            bool isNotCommissionFlat = string.IsNullOrEmpty(currentCommunity.CommissionRateType) || currentCommunity.CommissionRateType.ToLower() != "flat";
            viewModel.AgentCompensation = isNotCommissionFlat ? FormatedPercentage(currentCommunity.AgentCommission.ToType<string>()) : StringHelper.FormatCurrency(currentCommunity.AgentCommission);
            viewModel.AgentCommissionPayoutTiming = currentCommunity.PayoutTiming;
            viewModel.AgentCompensationAdditionalComments = currentCommunity.AdditionalCommissionComments;
            viewModel.IsBilled = isBilled;
            viewModel.Promotions = new Collection<Promotion>();
            viewModel.Events = new Collection<Event>();
            if (NhsRoute.IsBrandPartnerNhsPro)
            {
                if (viewModel.IsBilled) //Changes because 58867
                {
                    var events = _communityService.GetCommunityEvents(currentCommunity.CommunityId,
                                                                      currentCommunity.BuilderId).ToList();
                    viewModel.Events = events.Select(e => new Event
                    {
                        EventId = e.EventID,
                        Title = e.Title,
                        Description = e.Description,
                        EventFlyerUrl = e.EventFlyerURL,
                        EventType = e.EventTypeCode,
                        EventEndDate = e.EventEndTime,
                        EventStartDate = e.EventStartTime
                    })
                                             .ToList();
                }

                var builderService = ObjectFactory.GetInstance<IBuilderService>();
                var coopInfo = builderService.GetBuilderCoOpInfo(viewModel.BuilderId, NhsRoute.BrandPartnerId);
                viewModel.HasCoop = coopInfo.IsCoo;
                viewModel.HasBuilderPactLink = coopInfo.IsPac;

                var promotions = _communityService.GetCommunityDetailPromotions(currentCommunity.CommunityId, currentCommunity.BuilderId).ToList();
                if (!viewModel.IsBilled) //Changes because 58867
                    promotions.RemoveAll(promo => promo.PromoTypeCode != "COM");

                viewModel.Promotions = promotions.Select(p => new Promotion
                {
                    PromoId = p.PromoID,
                    PromoTextShort = p.PromoTextShort,
                    PromoTextLong = p.PromoTextLong,
                    PromoFlyerUrl = p.PromoFlyerURL,
                    PromoType = p.PromoTypeCode,
                    PromoEndDate = p.PromoEndDate,
                    PromoStartDate = p.PromoStartDate,
                    PromoUrl = p.PromoURL
                }).ToList();


                viewModel.AgentPolicyLink = AgentPolicyUrl(currentCommunity.CommunityId, currentCommunity.BuilderId);
            }
            else
            {
                var promotions = _communityService.GetCommunityDetailPromotions(currentCommunity.CommunityId,
                        currentCommunity.BuilderId).ToList();

                viewModel.Promotions = promotions.Where(w => w.PromoTypeCode != "AGT").Select(p => new Promotion
                {
                    PromoId = p.PromoID,
                    PromoTextShort = p.PromoTextShort,
                    PromoTextLong = p.PromoTextLong,
                    PromoFlyerUrl = p.PromoFlyerURL,
                    PromoType = p.PromoTypeCode,
                    PromoEndDate = p.PromoEndDate,
                    PromoStartDate = p.PromoStartDate,
                    PromoUrl = p.PromoURL
                }).ToList();
            }
            var communityImages = currentCommunity.AllImages;
            viewModel.GreenPrograms = _communityService.GetCommunityGreenPrograms(currentCommunity.CommunityId).Take(2).ToList();
            viewModel.AwardImages = _communityService.GetAwardImages(communityImages);
            viewModel.Testimonials = _communityService.GetCustomerTestimonials(5, currentCommunity.CommunityId, currentCommunity.BuilderId);
            viewModel.HomeOptions = _listingService.GetHomeOptionsForPlan(plan, NhsRoute.PartnerId);
            var img = _communityService.GetBuilderMap(communityImages);

            if (img != null)
                viewModel.BuilderMapUrl = img.ImagePath + img.ImageName;

            //Get Similar homes from search service - Rule is to use same # of beds, baths
            var searchParams = new SearchParams
            {
                CommId = currentCommunity.CommunityId,
                BuilderId = currentCommunity.BuilderId,
                PartnerId = NhsRoute.PartnerId,
                MarketId = currentCommunity.MarketId,
                HomeStatus = homeStatus.HomeStatusTypeToHomeStatus(),
                WebApiSearchType = WebApiSearchType.Exact
            };

            //All homes for comm

            var allHomes =
                _listingService.GetHomesResultsFromApi<HomeItem>(searchParams, NhsRoute.PartnerId).Result.Select(home =>
                {
                    var _market = home.MarketId == market.MarketId ? market : _marketService.GetMarket(home.MarketId);
                    home.MarketName = _market.MarketName;
                    home.StateName = _market.State.StateName;
                    return home;
                }).ToList();

            var crossMarketingSpecs = new List<HomeItem>();
            if (spec != null)
                crossMarketingSpecs =
                    allHomes.Where(s => s.IsSpec > 0 && s.HomeId != spec.SpecId && s.HomeId == plan.PlanId)
                        .OrderBy(list => list.Addr)
                        .ToList();

            //Similar homes
            var similarHomes = GetSimilarHomes(allHomes, plan, spec, isPlan, bedRooms, bathRooms, price, viewModel.MarketName, viewModel.MarketId);

            var hotHomes = new List<HomeItem>();
            if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
            {
                var homeId = isPlan ? plan.PlanId : spec.SpecId;
                hotHomes = allHomes.Where(r => r.IsHotHome == 1 && r.HomeId != homeId).Take(2).Select(r =>
                {
                    if (r.IsSpec == 1)
                    {
                        var _spec = _listingService.GetSpec(r.HomeId);
                        r.HotHomeDescription = _spec.HotHomeDescription;
                    }
                    else
                    {
                        var _plan = _listingService.GetPlan(r.HomeId);
                        r.HotHomeDescription = _plan.HotHomeDescription;
                    }
                    return r;
                }).ToList();
            }

            viewModel.CrossMarketingHomes = crossMarketingSpecs;
            viewModel.SimilarHomesApi = similarHomes;
            viewModel.HotHomesApi = hotHomes;
            viewModel.VirtualTourUrl = viewModel.SpecId > 0 ? spec.VirtualTourUrl : plan.VirtualTourUrl;

            //Helper flags
            viewModel.IsPageCommDetail = false;
            viewModel.IsPlan = isPlan;
            viewModel.ShowLeadForm = true;
            viewModel.ShowSaveToAccount = showSaveToAccount;
            viewModel.ShowExpiredRequestBrochureLink = showExpiredRequestBrochureLink;
            viewModel.ShowMortgageLink = (plan.Community.ShowMortgageLink == "Y");
            viewModel.IsPhoneNumberVisible = false;
            viewModel.SaveHomeSuccessMessage = LanguageHelper.MSG_COMMDETAIL_ADDED_TO_PLANNER;
            viewModel.BcType = currentCommunity.BCType;
            viewModel.LogActionMethodUrl = GetMethodUrl(isPlan, isPlan ? plan.PlanId : spec.SpecId);
            viewModel.UnderneathMapCommName = currentCommunity.CommunityName;
            viewModel.UnderneathMapAddress = ((!string.IsNullOrEmpty(currentCommunity.Address1) &&
                                               currentCommunity.OutOfCommunityFlag.ToType<bool>())
                                                  ? currentCommunity.Address1
                                                  : currentCommunity.SalesOffice.Address1) +
                                             " <br >" + currentCommunity.City + ", " + currentCommunity.State.StateName +
                                             " " + currentCommunity.PostalCode;



            viewModel.ShowPlaceHolderInputText = NhsRoute.CurrentRoute.Function.IndexOf("v3") != -1 ||
                                                 NhsRoute.CurrentRoute.Function.IndexOf("v4") != -1;

            // TODO: Remove this logic for image viewer from here, gallery is now loading async so should do the image viewer
            if (RouteParams.ShowPopupPlayer.Value<bool>())
            {
                viewModel.ExternalMediaLinks = new List<MediaPlayerObject>();
                if (!string.IsNullOrEmpty(virtualTourUrl) &&
                    !virtualTourUrl.Equals("n", StringComparison.InvariantCultureIgnoreCase))
                {

                    viewModel.ExternalMediaLinks.Add(new MediaPlayerObject
                    {
                        Type = MediaPlayerObjectTypes.Link,
                        SubType = MediaPlayerObjectTypes.SubTypes.VirtualTour,
                        Url = virtualTourUrl
                    });
                }

                if (!string.IsNullOrEmpty(floorPlanViewerUrl) &&
                    !floorPlanViewerUrl.Equals("n", StringComparison.InvariantCultureIgnoreCase))
                {

                    viewModel.ExternalMediaLinks.Add(new MediaPlayerObject
                    {
                        Type = MediaPlayerObjectTypes.Link,
                        SubType = MediaPlayerObjectTypes.SubTypes.FloorPlanImages,
                        Url = floorPlanViewerUrl
                    });
                }

                viewModel.PlayerMediaObjects = _listingService.GetMediaPlayerObjects(plan, spec);
                foreach (var ext in viewModel.ExternalMediaLinks)
                    viewModel.PlayerMediaObjects.Add(ext);
                FillExtendedMediaObjectsInfo(viewModel);
            }

            //Add params to Ad controller
            this.BindAds(ref viewModel, price, currentCommunity.StateAbbr);
            //SEO
            OverrideHomeDetailMeta(spec, plan, addNoIndexTag, viewModel,currentCommunity);

            //Set SDC Market Id
            viewModel.Globals.SdcMarketId = currentCommunity.MarketId;
            viewModel.Size = new KeyValuePair<int, int>(460, 307);
            viewModel.PageUrl = viewModel.Globals.CurrentUrl;
            var boylList = _boylService.GetBoylResultsCnh(viewModel.MarketName, viewModel.StateAbbr);
            viewModel.CnhBoyl = boylList.FirstOrDefault(p => p.NhsBuilderId == viewModel.BuilderId && p.IsCnh);

            UserSession.PersonalCookie.MarketId = viewModel.MarketId;
            UserSession.PersonalCookie.State = viewModel.StateAbbr;
            UserSession.PersonalCookie.ZipCode = viewModel.ZipCode ;
            UserSession.PersonalCookie.LastPrice = viewModel.PriceLow.ToType<decimal>();

            viewModel.AffiliateLinksData = new AffiliateLinksData();
            if (NhsRoute.IsBrandPartnerNhsPro)
                return viewModel;

            //BEGIN: Affiliate Links Logic is only for NHS and MOVE
            var links = _affiliateLinkService.GetAffiliateLinks(Pages.HomeDetail, NhsRoute.PartnerId);
            var freeCreditScore = links.LinksList.Find(x => x.Keyword.IsCaseInsensitiveEqual(AffiliateLinksTypes.FreeCreditScore));
            if (freeCreditScore != null)
                viewModel.AffiliateLinksData.FreeCreditScoreFormArea = freeCreditScore.ToLink(tags);

            var mortgageRates = links.LinksList.Find(x => x.Keyword.IsCaseInsensitiveEqual(AffiliateLinksTypes.MortgageRates));
            if (mortgageRates != null)
                viewModel.AffiliateLinksData.MortageRatesFormArea = mortgageRates.ToLink(tags);

            var calcMortgageRates = links.LinksList.Find(x => x.Keyword.IsCaseInsensitiveEqual(AffiliateLinksTypes.CalculateMortgagePayments));
            if (calcMortgageRates != null)
                viewModel.AffiliateLinksData.CalculateMortagePaymentsNexStepsArea = calcMortgageRates.ToLink(tags);
            //END: Affiliate Links Logic is only for NHS and MOVE


            return viewModel;
        }

        private List<HomeItem> GetSimilarHomes(List<HomeItem> homeResults, IPlan plan, ISpec spec, bool isPlan, int numBeds, int numBaths, decimal price, string marketName, int marketId)
        {
            if (isPlan)
                homeResults.RemoveAll(list => list.HomeId == plan.PlanId);
            else
                homeResults.RemoveAll(list => list.HomeId == spec.SpecId);

            homeResults = homeResults.Take(8).OrderByDescending(list => ListingsSimilarityScore(list, numBeds, numBaths, price)).ToList();

            foreach (var home in homeResults)
                home.MarketName = marketId == home.MarketId ? marketName : _marketService.GetMarketName(NhsRoute.PartnerId, home.MarketId);
            return homeResults;
        }

        private int ListingsSimilarityScore(HomeItem list, int numBeds, int numBaths, decimal price)
        {
            if (list.Br == numBeds && list.Ba == numBaths && list.Price.ToType<decimal>() == price)
                return 10;

            if (list.Br == numBeds && list.Ba == numBaths)
                return 9;

            if (list.Br == numBeds || list.Ba == numBaths || list.Price.ToType<decimal>() == price)
                return 8;

            if (Math.Abs(list.Br - numBeds) == 1)
                return 7;

            if (Math.Abs(list.Price.ToType<decimal>() - price) < 50000)
                return 6;

            if (Math.Abs(list.Br - numBeds) == 2)
                return 5;

            if (Math.Abs(list.Price.ToType<decimal>() - price) < 100000)
                return 4;

            return 1;
        }
        private string FormattedText(string text)
        {
            if (string.IsNullOrEmpty(text)) return string.Empty;

            string descriptionText = ParseHTML.StripHTML(text);

            return descriptionText.Replace("\r", string.Empty).Replace("\n", string.Empty);
        }
        private string GetMethodUrl(bool isPlan, int id)
        {
            string partnerSite = string.IsNullOrEmpty(NhsRoute.PartnerSiteUrl)
                                     ? string.Empty
                                     : string.Concat("/", NhsRoute.PartnerSiteUrl);

            return isPlan ? string.Format("{0}/{1}/planid-{2}/", partnerSite, Pages.HomeDetail, id) : string.Format("{0}/{1}/specid-{2}/", partnerSite, Pages.HomeDetail, id);
        }

        private IEnumerable<ExtendedHomeResult> GetExtendedHomeResults(IEnumerable<HomeResult> sortedResults)
        {
            var homeResults = new List<ExtendedHomeResult>();

            //get all specs only
            var specIds = (from s in sortedResults
                           where s.SpecId > 0
                           select s.SpecId).ToList();

            //get all plans for specs
            var specPlanIds = (from p in sortedResults
                               where p.SpecId > 0
                               select p.PlanId).ToList();

            //get all plans only
            var planIds = (from p in sortedResults
                           where p.SpecId == 0
                           select p.PlanId).ToList();

            IList<ISpec> specs = new List<ISpec>();
            IList<IPlan> plans = new List<IPlan>();
            IList<IPlan> specPlans = new List<IPlan>();

            if (specIds.Count > 0) //get all specs only
            {
                specs = _listingService.GetSpecsForSpecIds(specIds, true);
                specPlans = _listingService.GetPlansForPlanIds(specPlanIds, true);
            }

            if (planIds.Count > 0) // get all plans that are not specs
                plans = _listingService.GetPlansForPlanIds(planIds, true);

            foreach (var sr in sortedResults)
            {
                var hr = new ExtendedHomeResult();
                hr.Address1 = sr.Address1;
                hr.Address2 = sr.Address2;
                hr.BrandImageThumbnail = sr.BrandImageThumbnail;
                hr.BrandName = sr.BrandName;
                hr.BuilderId = sr.BuilderId;
                hr.City = sr.City;
                hr.CommunityId = sr.CommunityId;
                hr.CommunityName = sr.CommunityName;
                hr.County = sr.County;
                hr.HomeStatus = sr.HomeStatus;
                hr.ImageCount = sr.ImageCount;
                hr.IsHotHome = sr.IsHotHome;
                hr.ListingId = sr.ListingId;
                hr.MarketingDescription = sr.MarketingDescription;
                hr.NumBathrooms = sr.NumBathrooms;
                hr.NumBedrooms = sr.NumBedrooms;
                hr.NumGarages = sr.NumGarages;
                hr.NumHalfBathrooms = sr.NumHalfBathrooms;
                hr.PlanId = sr.PlanId;
                hr.PlanImageThumbnail = sr.PlanImageThumbnail;
                hr.PlanName = sr.PlanName;
                hr.PlanTypeCode = sr.PlanTypeCode;
                hr.PostalCode = sr.PostalCode;
                hr.Price = sr.Price;
                hr.PromoId = sr.PromoId;
                hr.SpecCity = sr.SpecCity;
                hr.SpecId = sr.SpecId;
                hr.SpecPostalCode = sr.SpecPostalCode;
                hr.SpecState = sr.SpecState;
                hr.SquareFeet = sr.SquareFeet;
                hr.State = sr.State;
                hr.Stories = sr.Stories;

                if (sr.SpecId > 0)
                {
                    hr.Spec = (from s in specs where s.SpecId == sr.SpecId select s).FirstOrDefault();
                    if (hr.Spec != null)
                        hr.Plan = (from p in specPlans where p.PlanId == hr.Spec.PlanId select p).FirstOrDefault();
                }
                else
                {
                    hr.Plan = (from p in plans where p.PlanId == sr.PlanId select p).FirstOrDefault();
                }

                hr.StateName = _stateService.GetStateName(sr.State);

                var marketId = _marketService.GetMarketIdFromStateCity(sr.State, sr.City);
                if (marketId > 0)
                    hr.MarketName = _marketService.GetMarketName(NhsRoute.PartnerId, marketId);

                homeResults.Add(hr);
            }

            return homeResults;
        }
        private void AddHomeToUserPlanner(int commId, int listingId, ListingType listingType, int builderId, bool isPlan)
        {
            if (UserSession.UserProfile.IsLoggedIn())
            {
                var pl = new PlannerListing(listingId, listingType)
                {
                    BuilderId = builderId,
                    UserId = UserSession.UserProfile.UserID
                };

                // user is active user and wanted to add listing to profile
                var addToProfile = RouteParams.AddToProfile.Value<bool>();

                //if addtoprofile param present then user was redirected here from login page
                if (addToProfile)
                    SaveHomeToUserPlanner(commId, pl, isPlan);
            }
        }

        private string ComputeHomeSpecsText(IPlan plan)
        {
            var label = string.Format(@"{0}", plan.FormatBedrooms(false));

            if (plan.Bathrooms > 0)
                label += string.Format(@" / {0}", plan.FormatBathrooms(false));

            if (plan.Garages > 0.0m)
                label += string.Format(@" / {0:0.##} car garage", plan.Garages);

            return label;
        }

        private void BindAds(ref HomeDetailViewModel model, decimal price, string state)
        {
            model.Globals.AdController.AddMarketParameter(model.MarketId);
            model.Globals.AdController.AddCommunityParameter(model.CommunityId);
            model.Globals.AdController.AddPriceParameter(price.ToType<int>(), price.ToType<int>());
            model.Globals.AdController.AddStateParameter(state);
            model.Globals.AdController.AddBuilderParameter(model.BuilderId);
            model.Globals.AdController.AddCityParameter(model.CommunityCity);
            model.Globals.AdController.AddZipParameter(model.UserPostalCode);
        }

        #endregion

    }
}
