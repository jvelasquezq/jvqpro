﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Business.Config;
using Nhs.Library.Business.Seo;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers.Property;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Utility.Constants;
using Nhs.Web.UI.AppCode.Extensions;
using Nhs.Web.UI.AppCode.Filters;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Web.UI.AppCode.ViewModels.Coop;
using Partner = Nhs.Mvc.Domain.Model.Web.Partner;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class PartialViewsController : BaseController
    {
        #region Members
        private readonly IMarketService _marketService;
        private readonly ICommunityService _communityService;
        private readonly IListingService _listingService;
        private readonly IBasicListingService _basicListingService;
        private readonly IBuilderService _builderService;
        private readonly IMapService _mapService;
        private readonly IPartnerService _partnerService;
        private readonly IUserProfileService _userProfileService;
        private readonly IPathMapper _pathMapper;
        #endregion

        #region Constructor
        public PartialViewsController(IPartnerService partnerService, IMarketService marketService, 
            ICommunityService communityService, IListingService listingService, IMapService mapService,
            IPathMapper pathMapper, IBasicListingService basicListingService, IBuilderService builderService, IUserProfileService userProfileService)
            : base(pathMapper)
        {
            _partnerService = partnerService;
            _marketService = marketService;
            _communityService = communityService;
            _listingService = listingService;
            _mapService = mapService;
            _pathMapper = pathMapper;
            _basicListingService = basicListingService;
            _builderService = builderService;
            _userProfileService = userProfileService;
        }
        #endregion

        public virtual ActionResult DrivingDirections(int communityId, string lat, string lng, bool check, bool isBasicListing, string startingPoint)
        {
            var model = new PropertyMapViewModel();

            if (isBasicListing)
            {
                var basicListing = _basicListingService.GetBasicListing(communityId);
                model.Latitude = basicListing.Latitude.ToType<double>();
                model.Longitude = basicListing.Longitude.ToType<double>();
            }
            else
            {
                var comm = _communityService.GetCommunity(communityId);
                model.BrandName = comm.Brand.BrandName;
                model.Latitude = (double)comm.Latitude;
                model.Longitude = (double)comm.Longitude;
                model.PropertyName = comm.CommunityName;
                model.DrivingDirections = comm.SalesOffice.DrivingDirections;
                model.Globals.AdController.AddMarketParameter(comm.MarketId);
                model.Globals.AdController.AddCommunityParameter(comm.CommunityId);
                model.Globals.AdController.AddPriceParameter(comm.PriceLow.ToType<int>(), comm.PriceHigh.ToType<int>());
                model.Globals.AdController.AddStateParameter(comm.StateAbbr);
                model.Globals.AdController.AddBuilderParameter(comm.BuilderId);
                model.Globals.AdController.AddCityParameter(comm.City);
                model.Globals.AdController.AddZipParameter(comm.PostalCode);
            }

            model.StartingPoint = startingPoint;
            model.RouteLat = lat.ToType<double>();
            model.RouteLng = lng.ToType<double>();

            // If start point found, get the driving directions
            if (model.RouteLat != 0 && model.RouteLng != 0 && !check)
            {
                // ReSharper disable once Mvc.ViewNotResolved
                return View(model);
            }

            if (model.RouteLat != 0 && model.RouteLng != 0 && check)
                return Content("Address Ok");

            return Content("Unable to get driving directions");
        }

        /// <summary>
        /// BreadCrumb global navigation
        /// </summary>
        /// <returns></returns>
        [NoCache]
        public virtual ActionResult BreadCrumb(SearchParams sParams, bool refresh = false, int marketId = 0)
        {            
            var searchParams = sParams == null 
                ? UserSession.PropertySearchParameters 
                : sParams.ToPropertySearchParams();

            if (marketId == 0)
            {
                marketId = RouteParams.Market.Value<int>();
            }
            
            Community community = null;
            Market market = null;
            var communityId = 0;
            var builderId = 0;

            var viewModel = new PartialViewModels.BreadcrumbViewModel();

            if (marketId == 0 && !string.IsNullOrEmpty(RouteParams.StateName.Value()) && !string.IsNullOrEmpty(RouteParams.Area.Value()))
            {
                // check to see if market is defined by statename, market namd
                market = _marketService.GetMarket(NhsRoute.PartnerId, RouteParams.StateName.Value(), RouteParams.Area.Value(), false);
                if (market != null)
                    marketId = market.MarketId;
            }

            var postal = RouteParams.PostalCode.Value();

            if (marketId == 0 && !string.IsNullOrWhiteSpace(postal))
            {
                marketId = _marketService.GetMarketIdFromPostalCode(postal, NhsRoute.PartnerId);
            }

            //If plan available, use plan and infer market id
            int planId = RouteParams.PlanId.Value<int>();
            if (planId != 0)
            {
                var p = _listingService.GetPlan(planId);
                var c = p.Community;
                marketId = c.MarketId;
                communityId = c.CommunityId;
                builderId = c.BuilderId;
                viewModel.ListingName = p.PlanName;
                viewModel.CommunityCity = c.City;
                viewModel.CommunityState = c.StateAbbr;
            }

            //If spec available, use spec and infer market id
            int specId = RouteParams.SpecId.Value<int>();
            if (specId != 0)
            {
                var s = _listingService.GetSpec(specId);
                var c = s.Community;
                marketId = c.MarketId;
                communityId = c.CommunityId;
                builderId = c.BuilderId;
                viewModel.ListingName = string.Format("{0} ({1})", s.Address1, s.PlanName);
                viewModel.CommunityCity = c.City;
                viewModel.CommunityState = c.StateAbbr;
            }

            //If comm available, use comm and infer market id
            if (communityId == 0)
                communityId = RouteParams.Community.Value<int>() == 0 ? RouteParams.CommunityId.Value<int>() : RouteParams.Community.Value<int>();

            if (communityId != 0)
            {
                community = _communityService.GetCommunity(communityId);
                if (community != null)
                {
                    builderId = community.BuilderId;
                    viewModel.CommunityCity = community.City;
                    viewModel.CommunityState = community.StateAbbr;
                }
                if (community != null && market == null)
                    marketId = community.MarketId;                
            }

            //Basic Listing Detail page
            var basicListingId = RouteParams.ListingId.Value<int>();
            if (basicListingId > 0)
            {
                var basicListing = _basicListingService.GetBasicListing(basicListingId);
                marketId = basicListing.MarketId;
                viewModel.ListingName = basicListing.DisplayAddress.ToType<bool>() ? basicListing.StreetAddress : string.Empty;
                viewModel.CityName = basicListing.City;
                viewModel.CommunityCity = basicListing.City;
                viewModel.CommunityState = basicListing.State;
            }

            if (refresh && searchParams.MarketId > 0)
            {
                marketId = searchParams.MarketId;
            }

            if (marketId == 0)
            {
                viewModel.IsVisible = false;
            }
            else
            {
                searchParams = UserSession.PropertySearchParameters;

                viewModel.IsVisible = true;
                if (market == null)
                    market = _marketService.GetMarket(NhsRoute.PartnerId, marketId, false) ?? _marketService.GetMarket(marketId);

                viewModel.State = market.State.StateAbbr;
                viewModel.StateName = market.State.StateName;

                viewModel.MarketId = marketId;
                viewModel.MarketName = market.MarketName;
                if (string.IsNullOrEmpty(viewModel.CityName))
                    viewModel.CityName = string.IsNullOrEmpty(searchParams.City) ? searchParams.CityNameFilter.ToTitleCase() : searchParams.City.ToTitleCase();

                viewModel.ZipCode = searchParams.PostalCode;
                viewModel.CountyName = searchParams.County.ToTitleCase();
                viewModel.CommunityId = communityId;
                viewModel.BuilderId = builderId;
                viewModel.IsInDetailsPage = (communityId > 0 || basicListingId > 0);

                if (communityId != 0)
                {
                    if (community == null) community = _communityService.GetCommunity(communityId);
                    viewModel.CommunityName = community.CommunityName;
                    viewModel.BrandName = community.Brand.BrandName;
                    viewModel.CityName = community.City;
                }
                else if(!string.IsNullOrEmpty(searchParams.State))
                {
                    viewModel.State = searchParams.State;
                }

                viewModel.IsCityVisible = !string.IsNullOrEmpty(viewModel.CityName); //Comment this due a ticket 68547 && (viewModel.CityName != viewModel.MarketName || !string.IsNullOrEmpty(viewModel.CommunityName) || !string.IsNullOrEmpty(viewModel.ListingName));
                viewModel.IsCountyVisible = !string.IsNullOrEmpty(viewModel.CountyName);
                viewModel.IsZipVisible = !string.IsNullOrEmpty(viewModel.ZipCode);
                viewModel.IsCommunityVisible = (communityId > 0 || planId > 0 || specId > 0);

                var routeName = NhsRoute.CurrentRoute.Name;

                if (routeName == "CommunityRecommendations")
                {                    
                    viewModel.IsRecommVisible = true;                   
                }


                viewModel.IsListingVisible = !string.IsNullOrEmpty(viewModel.ListingName);
                viewModel.BackToSearchParams = viewModel.GetParamsBackToSearch();
                viewModel.HasValidMarketIdInSession = UserSession.PropertySearchParameters.MarketId > 0;
            }

            // ReSharper disable Asp.NotResolved
            return View(viewModel);
            // ReSharper restore Asp.NotResolved
        }

        /// <summary>
        /// BreadCrumb global navigation
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult BreadCrumbV2(bool refresh = false)
        {
            var marketId = RouteParams.Market.Value<int>();
            
            Community community = null;
            Market market = null;
            var communityId = 0;
            var builderId = 0;
            Synthetic syntheticGeoName = null; 
            var viewModel = new PartialViewModels.BreadcrumbViewModel();

            var state = RouteParams.StateName.Value();
            var marketName = RouteParams.MarketName.Value();

            if (marketId == 0 && !string.IsNullOrEmpty(state) && !string.IsNullOrEmpty(marketName))
            {
                market = _marketService.GetMarket(NhsRoute.PartnerId, state.Replace("-", " "), marketName.Replace("-", " "), false);
                marketId = market != null ? market.MarketId : 0;
            }

            if (!string.IsNullOrEmpty(RouteParams.SyntheticName.Value<string>()))
            {
                var name = RouteParams.SyntheticName.Value<string>().Replace("-", " ");
                syntheticGeoName = SyntheticGeoReader.GetSynthetics().Where(s => s.Name.ToLower() == name.ToLower()).FirstOrDefault();
                if (syntheticGeoName != null)
                {
                    market = _marketService.GetMarket(syntheticGeoName.MarketId);
                    marketId = market != null ? market.MarketId : 0;
                    viewModel.IsSyntheticSearch = true;
                }
            }

            //If plan available, use plan and infer market id
            int planId = RouteParams.PlanId.Value<int>();
            if (planId != 0)
            {
                var p = _listingService.GetPlan(planId);
                var c = p.Community;
                marketId = c.MarketId;
                communityId = c.CommunityId;
                builderId = c.BuilderId;
                viewModel.ListingName = p.PlanName;
                viewModel.CommunityCity = c.City;
                viewModel.CommunityState = c.StateAbbr;
            }

            //If spec available, use spec and infer market id
            int specId = RouteParams.SpecId.Value<int>();
            if (specId != 0)
            {
                var s = _listingService.GetSpec(specId);
                var c = s.Community;
                marketId = c.MarketId;
                communityId = c.CommunityId;
                builderId = c.BuilderId;
                viewModel.ListingName = string.Format("{0} ({1})", s.Address1, s.PlanName);
                viewModel.CommunityCity = c.City;
                viewModel.CommunityState = c.StateAbbr;
            }

            //If comm available, use comm and infer market id
            if (communityId == 0)
                communityId = RouteParams.Community.Value<int>();

            if (communityId != 0)
            {
                community = _communityService.GetCommunity(communityId);
                if (community != null)
                {
                    builderId = community.BuilderId;
                    viewModel.CommunityCity = community.City;
                    viewModel.CommunityState = community.StateAbbr;
                }
                if (community != null && market == null)
                    marketId = community.MarketId;
            }

            //Basic Listing Detail page
            var basicListingId = RouteParams.ListingId.Value<int>();
            if (basicListingId > 0)
            {
                var basicListing = _basicListingService.GetBasicListing(basicListingId);
                marketId = basicListing.MarketId;
                viewModel.ListingName = basicListing.DisplayAddress.ToType<bool>() ? basicListing.StreetAddress : string.Empty;
                viewModel.CityName = basicListing.City;
                viewModel.CommunityCity = basicListing.City;
                viewModel.CommunityState = basicListing.State;
            }

            if (refresh)
            {
                marketId = UserSession.SearchParametersV2.MarketId;
            }

            if (marketId == 0)
                viewModel.IsVisible = false;
            else
            {
                var searchParams = UserSession.SearchParametersV2;

                viewModel.IsVisible = true;
                if (market == null)
                    market = _marketService.GetMarket(NhsRoute.PartnerId, marketId, false) ?? _marketService.GetMarket(marketId);

                viewModel.State = market.State.StateAbbr;
                viewModel.StateName = market.State.StateName;

                viewModel.MarketId = marketId;
                viewModel.MarketName = syntheticGeoName != null? syntheticGeoName.Name : market.MarketName;
                if (string.IsNullOrEmpty(viewModel.CityName))
                    viewModel.CityName = searchParams.City.ToTitleCase();

                viewModel.ZipCode = searchParams.PostalCode;
                viewModel.CountyName = searchParams.County.ToTitleCase();
                viewModel.CommunityId = communityId;
                viewModel.BuilderId = builderId;
                viewModel.IsInDetailsPage = (communityId > 0 || basicListingId > 0);

                if (communityId != 0)
                {
                    if (community == null) community = _communityService.GetCommunity(communityId);
                    viewModel.CommunityName = community.CommunityName;
                    viewModel.BrandName = community.Brand.BrandName;
                    viewModel.CityName = community.City;
                }
                else if (!string.IsNullOrEmpty(searchParams.State))
                {
                    viewModel.State = searchParams.State;
                }

                viewModel.IsCityVisible = !string.IsNullOrEmpty(viewModel.CityName);
                viewModel.IsCountyVisible = !string.IsNullOrEmpty(viewModel.CountyName);
                viewModel.IsZipVisible = !string.IsNullOrEmpty(viewModel.ZipCode);
                viewModel.IsCommunityVisible = (communityId > 0 || planId > 0 || specId > 0);

                var routeName = NhsRoute.CurrentRoute.Name;

                if (routeName == "CommunityRecommendations")
                {
                    viewModel.IsRecommVisible = true;
                }


                viewModel.IsListingVisible = !string.IsNullOrEmpty(viewModel.ListingName);
                viewModel.BackToSearchParams = viewModel.GetParamsBackToSearch();
                viewModel.HasValidMarketIdInSession = UserSession.SearchParametersV2 != null && UserSession.SearchParametersV2.MarketId > 0;
            }

            // ReSharper disable Asp.NotResolved
            return View(viewModel);
            // ReSharper restore Asp.NotResolved
        }

        /// <summary>
        /// Partner Navigation
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult PartnerNavigation()
        {
            var viewModel = new PartialViewModels.PartnerNavigation();
            var partner = _partnerService.GetPartner(NhsRoute.PartnerId);
            // Set visibility
            switch (UserSession.UserProfile.ActorStatus)
            {
                case WebActors.ActiveUser:
                    viewModel.SignOutLink = true;
                    break;
                case WebActors.GuestUser:
                case WebActors.PassiveUser:
                    viewModel.SignInLink = true;
                    break;
            }
            viewModel.PartnerAllowsRegistration = partner.AllowRegistration != "N";
            viewModel.NewHomeGuideLink = partner.UsesHomeGuide != "N";
            viewModel.PartnerUsesSso = partner.UsesSso == "Y";
            // ReSharper disable Asp.NotResolved
            return View(viewModel);
            // ReSharper restore Asp.NotResolved
        }

        /// <summary>
        /// Personal Bar.
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult PersonalBar()
        {
            // Model creation and default values
            var model = new PartialViewModels.PersonalBar
            {
                SpotlightHomesVisible = false,
                NoOfHomesVisible = false,
                NoHomesText = "No",
                RecentItemsLinkVisible = true,
                RecentItemsLinkActiveUserVisible = true,
                SavedHomeVisible = false,
                SavedHomeLnkText = String.Empty,
                SuccessMessageVisible = false,
                SuccessMessageText = String.Empty
            };

            // Show Home Count
            var homecount = UserSession.UserProfile.Planner.RecentlyViewedHomes.Count;
            var pageFunction = NhsRoute.CurrentRoute.Function;
            if ((NhsRoute.PartnerId == 1 | NhsRoute.PartnerId == 333) & (pageFunction == string.Empty | pageFunction == Pages.Home))
            {
                model.SpotlightHomesVisible = true;
            }

            if (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser)
            {
                model.RecentItemsLinkVisible = false;
                model.SavedHomeVisible = true;
                int savedCommunityCount = UserSession.UserProfile.Planner.SavedCommunities.Count;
                int savedHomeCount = UserSession.UserProfile.Planner.SavedHomes.Count;

                model.NoHomesText = string.Format("{0} home{1} viewed.", homecount, (homecount == 1) ? string.Empty : "s");               

                if (homecount == 0)
                {
                    model.RecentItemsLinkActiveUserVisible = false;
                }
                if (savedHomeCount > 0 & savedCommunityCount > 0)
                {
                    model.SavedHomeLnkText = string.Format("{0} saved homes and communities.", savedHomeCount + savedCommunityCount);
                }
                else if (savedHomeCount > 0)
                {
                    model.SavedHomeLnkText = string.Format("{0} saved home{1}.", savedHomeCount, (savedHomeCount == 1) ? string.Empty : "s");
                }
                else if (savedCommunityCount > 0)
                {
                    model.SavedHomeLnkText = string.Format("{0} saved communit{1}.", savedCommunityCount, (savedCommunityCount == 1) ? "y" : "ies");
                }
                else
                {
                    model.SavedHomeVisible = false;
                    model.RecentItemsLinkActiveUserVisible = false;
                    model.RecentItemsLinkVisible = (homecount != 0);
                }
            }
            else //not active user
            {
                model.NoHomesText = homecount < 1 
                         ? "No homes viewed." 
                         : string.Format("{0} home{1} viewed.", homecount, (homecount == 1) ? string.Empty : "s");

                

                model.SavedHomeVisible = false;
                model.RecentItemsLinkActiveUserVisible = false;
                model.RecentItemsLinkVisible = (homecount != 0);
            }

            Response.Cookies.Add(new HttpCookie("showedRecentItems", homecount.ToString()));

            // Show success message
            if (UserSession.HasSentToFriend)
            {
                model.SuccessMessageText = ModalSuccessMessages.SendToFriend;
                model.SuccessMessageVisible = true;
                UserSession.HasSentToFriend = false;
            }

            // ReSharper disable Asp.NotResolved
            return View(model);
            // ReSharper restore Asp.NotResolved
        }


        public virtual ActionResult Advertisement(string position, bool frame, bool jsParameters)
        {
            var model = new PartialViewModels.AdViewModel
            {
                Position = position,
                UseFrame = frame,
                JavaScriptParams = jsParameters
            };

            // ReSharper disable Asp.NotResolved
            return View(model);
            // ReSharper restore Asp.NotResolved
        }

        /// <summary>
        /// BDX Footer
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult BDXFooter()
        {
            // Default values
            var model = new PartialViewModels.BDXFooter
            {
                NhsFooterAdBoxVisible = true,
                NhsFooterVisible = true,
                MoveFooterVisible = true,
                ShowAboutFooter = _partnerService.GetPartner(NhsRoute.PartnerId).FooterShowAboutUs.ToBool(),
                ShowRegistrationLinks = _partnerService.GetPartner(NhsRoute.PartnerId).AllowRegistration.ToBool()
            };

            model.NhsFooterVisible = false;

            string pageFunction = NhsRoute.CurrentRoute.Function;
            if (pageFunction == string.Empty | pageFunction == Pages.Home)
            {
                model.NhsFooterAdBoxVisible = false;
            }
            

            // ReSharper disable Asp.NotResolved
            return View(model);
            // ReSharper restore Asp.NotResolved
        }

        public virtual ActionResult PartnerFooter()
        {
            Partner partner = _partnerService.GetPartner(NhsRoute.PartnerId);
            var model = new PartialViewModels.PartnerFooter
            {
                Partner = partner,
                IsPassiveUser = (WebActors.ActiveUser != UserSession.UserProfile.ActorStatus)
            };

            // ReSharper disable Asp.NotResolved
            return View(model);
            // ReSharper restore Asp.NotResolved
        }

        public virtual ActionResult HwNhlFooter()
        {
            Partner partner = _partnerService.GetPartner(NhsRoute.PartnerId);
            var model = new PartialViewModels.HwNhlFooter
            {
                Partner = partner
            };

            // ReSharper disable Asp.NotResolved
            return View(model);
            // ReSharper restore Asp.NotResolved
        }

        /// <summary>
        /// Tracking Scripts
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult TrackingScripts(bool showsInHead = false)
        {
            var brandPartnerId = NhsRoute.BrandPartnerId;
            var partnerId = NhsRoute.PartnerId;
            var pageFunction = NhsRoute.CurrentRoute.Function;
            var hostUrl = NhsRoute.CurrentRoute.HostUrl;

            var scriptFiles = TrackingScriptsHelper.GetTrackingScriptList(brandPartnerId, partnerId, pageFunction, hostUrl, _pathMapper, showsInHead);
            var model = new PartialViewModels.TrackingScripts { ScriptFiles = scriptFiles };

            // ReSharper disable Asp.NotResolved
            return View(model);
            // ReSharper restore Asp.NotResolved
        }


        /// <summary>
        /// Seo links promotion
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult SeoLinkPromotion()
        {
            // get the url values for the different nhs elements
            var marketId = NhsRoute.GetValue(RouteParams.MarketId).ToType<int>();
            var city = NhsRoute.GetValue(RouteParams.City);
            var communityId = NhsRoute.GetValue(RouteParams.CommunityId).ToType<int>();
            var planId = NhsRoute.GetValue(RouteParams.PlanId).ToType<int>();
            var specId = NhsRoute.GetValue(RouteParams.SpecId).ToType<int>();
            string currentName;
            var marketName = NhsRoute.GetValue(RouteParams.Area).ToType<string>();
            var stateName = NhsRoute.GetValue(RouteParams.StateName).ToType<string>();
            var state = RouteParams.State.Value<string>();

            if (!string.IsNullOrEmpty(marketName) && !string.IsNullOrEmpty(stateName)) //DFUs
            {
                var mkt = _marketService.GetMarket(NhsRoute.PartnerId, stateName, marketName, false);
                marketId = mkt.MarketId;
            }

            var currentLocation = GetLocation(marketId, city, state, communityId, planId, specId, out currentName);

            var configHelper = new SeoLinkPromotionReader(_pathMapper);
            var seoData = configHelper.ParseScripts();
            SeoLinkPromotionPage pageData = null;

            if (seoData != null && seoData.ContainsKey(NhsRoute.CurrentRoute.Function))
            {
                pageData = seoData[NhsRoute.CurrentRoute.Function];

                // foreach link set the text using the page anchor text replacing the specified tags
                foreach (var link in pageData.Links)
                {
                    string name;

                    // compute the distance between the current location and the url location
                    var newLocation = GetLocation(link.MarketId, link.City, string.Empty, link.CommunityId, link.PlanId, link.SpecId, out name);
                    if (newLocation != null && newLocation.Latitude != 0 && currentLocation != null && currentLocation.Latitude != 0)
                    {
                        var distance = newLocation.ArcDistance(currentLocation);
                        link.Distance = distance > 0 ? distance : -1;
                    }

                    //  set the variable names to replace
                    var replaceTags = new Dictionary<string, string>();
                    if (link.MarketId != 0 || !string.IsNullOrEmpty(link.City))
                        replaceTags.Add("[location]", name);
                    if (link.MarketId != 0)
                        replaceTags.Add("[market]", name);
                    if (!string.IsNullOrEmpty(link.City))
                        replaceTags.Add("[city]", name);
                    if (link.CommunityId != 0)
                        replaceTags.Add("[community name]", name);
                    if (link.PlanId != 0 || link.SpecId != 0)
                        replaceTags.Add("[plan name]", name);

                    var linkText = (string.IsNullOrEmpty(link.Name)) ? pageData.LinksText : link.Name;
                    linkText = replaceTags.Aggregate(linkText, (current, tag) => current.Replace(tag.Key, tag.Value));

                    link.Name = linkText;
                }
            }

            // Logic for sort the links and only show the config count for this page 
            var model = new PartialViewModels.SeoLinkPromotion();

            if (pageData != null)
            {
                model.Title = pageData.Title;
                model.Links = pageData.Links.Where(l => l.Priority > 0).OrderByDescending(l => l.Priority / l.Distance).Take(pageData.Count).ToList();
            }

            // ReSharper disable Asp.NotResolved
            return View(model);
            // ReSharper restore Asp.NotResolved
        }

        public virtual JsonResult PartnerLocationsIndex(string term )
        {
            IList<string> lst = _partnerService.GetTypeAheadSuggestions(NhsRoute.PartnerId, term, true).
                Select(l => l.Name + (l.Type >= (int)LocationType.Community ? " in " + l.MarketName + ", " + l.State : (l.Type != (int)LocationType.Zip ? ", " + l.State : string.Empty) + (l.Type == (int)LocationType.Market ? " Area" : string.Empty)) + "|" + l.Type).Distinct().ToList();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult ListingAutoComplete(string term)
        {
            var lst = _communityService.GetAllCommunitiesForPartner(NhsRoute.PartnerId.ToType<int>())
                                       .Where(
                                           w =>
                                           w.CommunityName.ToLower().Contains(term))
                                       .Select(
                                           s =>
                                           new
                                               {
                                                   label =
                                               string.Format("{0}, {1} {2}", s.CommunityName, s.Market.MarketName,
                                                             s.State.StateName),
                                                   value = s.CommunityId
                                               }).Take(20).ToArray();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult SendFriendModal()
        {
            var model = new PartialViewModels.SendFriend();
            var marketId = RouteParams.MarketId.Value<int>();

            model.SpecId = RouteParams.SpecId.Value<int>();
            model.PlanId = RouteParams.PlanId.Value<int>();
            model.CommunityId = RouteParams.Community.Value<int>();
            model.BuilderId = RouteParams.Builder.Value<int>();

            var metaRegistrar = new MetaRegistrar(_pathMapper);
            var metas = metaRegistrar.GetRobotsMeta("noodp, noydir, noindex");
            OverrideDefaultMeta(model, metas);

            // Ad Variables added.
            model.Globals.AdController.AddBuilderParameter(model.BuilderId);
            model.Globals.AdController.AddMarketParameter(marketId);

            if (!string.IsNullOrEmpty(UserSession.PropertySearchParameters.PriceLow.ToString()) && !string.IsNullOrEmpty(UserSession.PropertySearchParameters.PriceLow.ToString()))
            {
                model.Globals.AdController.AddPriceParameter(UserSession.PropertySearchParameters.PriceLow, UserSession.PropertySearchParameters.PriceHigh);
            }

            //  Two conditions to add the state: First one is UserSession.PropertySearchParameters.State; 
            //  second one is MarketID. If both are empty, no state param is added to the adController.
            if (!string.IsNullOrEmpty(UserSession.PropertySearchParameters.State))
            {
                model.Globals.AdController.AddStateParameter(UserSession.PropertySearchParameters.State);
            }
            else
            {
                if (marketId != 0)
                {
                    var market = _marketService.GetMarket(NhsRoute.PartnerId, marketId, false);
                    if (market != null)
                    {
                        model.Globals.AdController.AddStateParameter(market.StateAbbr);
                    }
                }
            }

            var _userProfile = UserSession.UserProfile;

            if (_userProfile.ActorStatus != WebActors.GuestUser)
            {
                model.SupportEmail = _userProfile.Email;
            }
            // ReSharper disable Asp.NotResolved
            return View(NhsMvc.PartialViews.Views.Actionable.SendFriendModal, model);
            // ReSharper restore Asp.NotResolved
        }

        [HttpPost]
        public virtual ActionResult SendFriendModal(PartialViewModels.SendFriend model)
        {
            ValidateSendFriend(model);

            if (ModelState.IsValid)
            {
                const string delimiter = ",;";
                var delimiterChar = delimiter.ToCharArray();
                var array = model.FriendEmail.Split(delimiterChar);
                var savedEmails = new ArrayList();
                var comments = model.LeadComments ?? string.Empty;

                foreach (var friendEmail in array.Select(t => t.Trim()))
                {
                    if (friendEmail != string.Empty)
                    {
                        _userProfileService.SendToFriend(NhsRoute.BrandPartnerId, NhsRoute.PartnerId, UserSession.UserProfile.UserID, UserSession.UserProfile.LogonName, UserSession.UserProfile.FirstName, UserSession.UserProfile.LastName, model.SupportEmail, friendEmail, friendEmail, model.SpecId, model.PlanId,
                                                             model.CommunityId, model.BuilderId, comments);
                    }
                    savedEmails.Add(friendEmail + "|" + friendEmail);
                }

                var senderEmail = model.SupportEmail.Trim();

                if (model.SendMe)
                {
                    _userProfileService.SendToFriend(NhsRoute.BrandPartnerId, NhsRoute.PartnerId, UserSession.UserProfile.UserID, UserSession.UserProfile.LogonName, UserSession.UserProfile.FirstName, UserSession.UserProfile.LastName, senderEmail, senderEmail, senderEmail, model.SpecId, model.PlanId, model.CommunityId, model.BuilderId, comments);
                    savedEmails.Add(senderEmail + "|" + senderEmail);
                }

                UserSession.SetItem(SessionConst.SendFriendData, savedEmails);
                model.ShowSuccessMessage = true;
                return JavaScript("tb_remove();");
            }

            // ReSharper disable Asp.NotResolved
            return JavaScript(string.Format("ShowSendToFriendErrors({0});", ModelState
                        .SelectMany(ms => ms.Value.Errors)
                        .Select(ms => ms.ErrorMessage)));
            // ReSharper restore Asp.NotResolved
        }



        private void ValidateSendFriend(PartialViewModels.SendFriend model)
        {

            if (string.IsNullOrEmpty(model.SupportEmail))
            {
                ModelState.AddModelError("Email", "From email address is required.");
                ;
            }
            else if (!string.IsNullOrEmpty(model.SupportEmail) && !StringHelper.IsValidEmail(model.SupportEmail))
            {
                ModelState.AddModelError("Email", "From email address is not valid.");
            }

            if (string.IsNullOrEmpty(model.FriendEmail))
            {
                ModelState.AddModelError("FriendEmails", "Friend's email address is required.");
            }
            else
            {
                const string delimiter = ",;";
                var delimiterChar = delimiter.ToCharArray();

                var array = model.FriendEmail.Split(delimiterChar);

                if (array.Count() > 3)
                {
                    ModelState.AddModelError("FriendEmails", @"You can only send to 3 friend's emails as maximum.");
                }
                else
                {
                    foreach (var t in array.Where(t => t.Trim() != string.Empty).Where(t => !StringHelper.IsValidEmail(t.Trim())))
                    {
                        ModelState.AddModelError("FriendEmails", "Friend's email address is not valid.");
                    }
                }

            }
        }

        public virtual ActionResult ShowFlashPlayer()
        {
            // ReSharper disable Asp.NotResolved
            return View();
            // ReSharper restore Asp.NotResolved
        }

        private LatLong GetLocation(int marketId, string city, string state, int communityId, int planId, int specId, out string name)
        {
            LatLong currentLocation = null;
            name = string.Empty;
            var cacheKey = WebCacheConst.WebCacheKeyPrefix + "mkt-" + marketId + "ct-" + city + "cm-" + communityId + "pl-" + planId + "sp-" + specId;
            var value = (WebCacheHelper.GetObjectFromCache(cacheKey, false) as object[]);

            if (value == null)
            {
                if (!string.IsNullOrEmpty(city))
                {
                    var cityLoc = _mapService.GeoCodeCityByMarket(city, marketId, state);
                    if (cityLoc != null)
                    {
                        name = cityLoc.Name;
                        currentLocation = new LatLong(cityLoc.Latitude, cityLoc.Longitude);
                        return currentLocation;
                    }
                }

                if (marketId != 0)
                {
                    var market = _marketService.GetMarket(marketId);
                    name = market.MarketName;
                    currentLocation = new LatLong(market.Latitude.ToType<double>(), market.Longitude.ToType<double>());
                }
                else
                {
                    if (communityId != 0)
                    {
                        var comm = _communityService.GetCommunity(communityId);
                        name = comm.CommunityName;
                        currentLocation = new LatLong(comm.Latitude.ToType<double>(), comm.Longitude.ToType<double>());
                    }
                    else if (planId != 0)
                    {
                        var spec = _listingService.GetPlan(planId);
                        name = spec.PlanName;
                        currentLocation = new LatLong(spec.Community.Latitude.ToType<double>(), spec.Community.Longitude.ToType<double>());
                    }
                    else if (specId != 0)
                    {
                        var plan = _listingService.GetSpec(specId);
                        name = plan.PlanName;
                        currentLocation = new LatLong(plan.Community.Latitude.ToType<double>(), plan.Community.Longitude.ToType<double>());
                    }
                }

                WebCacheHelper.AddObjectToCache(new object[] { name, currentLocation }, cacheKey, new TimeSpan(WebCacheConst.DefaultHours, 0, 0), false);
            }
            else
            {
                currentLocation = (LatLong)value[1];
                name = value[0].ToString();
            }

            return currentLocation;
        }

        public virtual ActionResult ShowIntersitialMris()
        {
            ActionResult returnAction = View(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.Intersitial.PartialMrisShow);
                
            if (Request.IsAjaxRequest())
            {
                returnAction = PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.Intersitial.PartialMrisShow);
            }

            return returnAction;
        }

        public virtual ActionResult ShowBuilderAgentPartnershipPact(int builderId)
        {
            var model = new PartnershipPactViewModel
            {
                Info = _builderService.GetBuilderCoOpInfo(builderId, NhsRoute.BrandPartnerId, true),
                Globals = { PartnerInfo = _partnerService.GetPartner(NhsRoute.PartnerId) }
            };

            if (model.Info.IsPac)
            {
                model.Info.LogoUrlMed = string.Concat(Configuration.ResourceDomain, model.Info.LogoUrlMed);
                return View(NhsMvc.PartnerBrandGroupViews_88.Views.CoOpBuilderPact.Show, model);
            }      
            return Content("<h2>Invalid Request</h2>");
        }
            

    }
}
