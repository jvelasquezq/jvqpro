﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class DownLoadFilesController : Controller
    {
        private readonly IPathMapper _pathMapper;

        public DownLoadFilesController(IPathMapper pathMapper)
        {
            _pathMapper = pathMapper;
        }

        public virtual FileResult SiteMapIndex()
        {
            Stream stream = null;
            var siteMapsIndex = _pathMapper.MapPath(Configuration.SiteMapFolder + "/sitemapindex.xml");
            if (System.IO.File.Exists(siteMapsIndex))
            {
              stream =  System.IO.File.OpenRead(siteMapsIndex);
            }
            else
            {
                var stringBuilder = new StringBuilder();
                stringBuilder.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                stringBuilder.AppendLine("<sitemapindex xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">");
                stringBuilder.AppendLine("</sitemapindex>");
                var ms = new MemoryStream(Encoding.ASCII.GetBytes(stringBuilder.ToString()));
                stream = ms;
            }
            
            Response.AppendHeader("Content-Disposition", "inline;filename=sitemapindex.xml");
            return new FileStreamResult(stream, "text/xml");
        }
    }
}