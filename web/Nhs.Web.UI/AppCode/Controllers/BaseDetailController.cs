﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Constants.Route;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Brightcove;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.Hub;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Extensions;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Library.Constants.Route;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;

namespace Nhs.Web.UI.AppCode.Controllers
{
    /// <summary>
    /// Contains common methods to both CDC and HDC
    /// </summary>
    public partial class BaseDetailController : ApplicationController
    {
        private ICommunityService _communityService;
        private IListingService _listingService;

        public BaseDetailController(ICommunityService communityService, IListingService listingService,
                                    IPathMapper pathMapper)
            : base(pathMapper)
        {
            _communityService = communityService;
            _listingService = listingService;
        }

        public BaseDetailController()
        {

        }

        public virtual ActionResult GetGalleryObjects(int communityId, int planId, int specId, bool isPreview)
        {
            Community community = null;
            HubCommunity hubCommunity = null;
            IDetailViewModel model = null;

            if (planId > 0 || specId > 0)
            {
                _listingService.UseHub = isPreview;
                var spec = _listingService.GetSpec(specId);
                var plan = specId > 0 && spec.Plan != null ? spec.Plan : _listingService.GetPlan(planId);
                var virtualTourUrl = specId > 0 ? spec.VirtualTourUrl : plan.VirtualTourUrl;
                var floorPlanViewerUrl = _listingService.GetFloorPlanViewerUrl(plan, spec);

                if (isPreview)
                    hubCommunity = _communityService.GetHubCommunity(plan.CommunityId);
                else
                    community = _communityService.GetCommunity(plan.CommunityId);

                model = new HomeDetailViewModel
                {
                    CommunityId = isPreview ? hubCommunity.CommunityId : community.CommunityId,
                    CommunityName = isPreview ? hubCommunity.CommunityName : community.CommunityName,
                    BuilderId = isPreview ? hubCommunity.BuilderId : community.BuilderId,
                    BuilderName = isPreview ? hubCommunity.Builder.BuilderName : community.Builder.BuilderName,
                    MarketId = isPreview ? hubCommunity.MarketId : community.MarketId,
                    MarketName = isPreview ? hubCommunity.Market.MarketName : community.Market.MarketName,
                    PropertyMediaLinks = _listingService.GetMediaPlayerObjects(plan, spec),
                    ExternalMediaLinks = new List<MediaPlayerObject>()
                };

                if (!string.IsNullOrEmpty(virtualTourUrl) && !virtualTourUrl.Equals("n", StringComparison.InvariantCultureIgnoreCase))
                    model.ExternalMediaLinks.Add(new MediaPlayerObject
                    {
                        Type = MediaPlayerObjectTypes.Link,
                        SubType = MediaPlayerObjectTypes.SubTypes.VirtualTour,
                        Url = virtualTourUrl
                    });

                if (!string.IsNullOrEmpty(floorPlanViewerUrl) && !floorPlanViewerUrl.Equals("n", StringComparison.InvariantCultureIgnoreCase))
                    model.ExternalMediaLinks.Add(new MediaPlayerObject
                    {
                        Type = MediaPlayerObjectTypes.Link,
                        SubType = MediaPlayerObjectTypes.SubTypes.FloorPlanImages,
                        Url = floorPlanViewerUrl
                    });
                if (community != null || (hubCommunity != null))
                    model.PinterestDescription = isPreview ? string.Format("{0} by {1} at {2}", plan.PlanName, hubCommunity.Brand.BrandName, hubCommunity.CommunityName).Replace("'", "\\'")
                                                           : string.Format("{0} by {1} at {2}", plan.PlanName, community.Brand.BrandName, community.CommunityName).Replace("'", "\\'");
            }
            else if (communityId > 0)
            {
                _communityService.UseHub = isPreview;

                if (isPreview)
                    hubCommunity = _communityService.GetHubCommunity(communityId);
                else
                    community = _communityService.GetCommunity(communityId);

                if (community != null || hubCommunity != null)
                    model = new CommunityDetailViewModel
                    {
                        CommunityId = isPreview ? hubCommunity.CommunityId : community.CommunityId,
                        CommunityName = isPreview ? hubCommunity.CommunityName : community.CommunityName,
                        BuilderId = isPreview ? hubCommunity.BuilderId : community.BuilderId,
                        BuilderName = isPreview ? hubCommunity.Builder.BuilderName : community.Builder.BuilderName,
                        MarketId = isPreview ? hubCommunity.MarketId : community.MarketId,
                        MarketName = isPreview ? hubCommunity.Market.MarketName : community.Market.MarketName,
                        ExternalMediaLinks = _communityService.GetVideoTourImages(communityId).Select(media => new MediaPlayerObject
                            {
                                Type = MediaPlayerObjectTypes.Link,
                                SubType = MediaPlayerObjectTypes.SubTypes.ExternalVideo,
                                Title = media.ImageTitle.EscapeSingleQuote(),
                                Url = media.OriginalPath,
                                Sort = media.ImageSequence.ToType<int>()
                            }).OrderBy(m => m.Type).ThenBy(m => m.Sort).ToList(),
                        PropertyMediaLinks = _communityService.GetMediaPlayerObjects(communityId, true, false, NhsRoute.ShowMobileSite).ToList(),
                        PinterestDescription = isPreview ? string.Format("{0} by {1} in {2}, {3}", hubCommunity.CommunityName, hubCommunity.Brand.BrandName, hubCommunity.City, hubCommunity.State.StateName).Replace("'", "\\'")
                                                         : string.Format("{0} by {1} in {2}, {3}", community.CommunityName, community.Brand.BrandName, community.City, community.State.StateName).Replace("'", "\\'")
                    };
            }

            if (model == null) return Json(new { });
            if (model.ExternalMediaLinks != null && model.ExternalMediaLinks.Any())
                model.ExternalMediaLinks.ForEach(m => m.Url = (m.IsYouTubeVideo ? m.Url : new HtmlHelper(new ViewContext(), new ViewPage()).GetExternalVideoLink(model, m).ToString()));

            model.FirstImage = model.PropertyMediaLinks.Any() ? Configuration.ResourceDomain + model.PropertyMediaLinks.First().Url : string.Empty;
            return Json(new { model.PropertyMediaLinks, model.ExternalMediaLinks, model.PinterestDescription, model.FirstImage });
        }

        protected ActionResult GetInactivePropertyViewModel(bool isCommunity, IStateService stateService,
                                                            ILookupService lookupService)
        {
            IDetailViewModel model;

            if (isCommunity)
            {
                model = new CommunityDetailViewModel();
                ((CommunityDetailViewModel)model).ExternalMediaLinks = new List<MediaPlayerObject>();
            }
            else
            {
                model = new HomeDetailViewModel();
                ((HomeDetailViewModel)model).ExternalMediaLinks = new List<MediaPlayerObject>();
            }
            model.GreenPrograms = new List<GreenProgram>();
            model.Promotions = new List<Promotion>();
            model.States = (from s in stateService.GetPartnerStates(NhsRoute.PartnerId)
                            select new SelectListItem { Text = s.StateName, Value = s.StateAbbr });
            model.PriceLowRange = (from l in lookupService.GetCommonListItems(CommonListItem.MinPrice)
                                   select new SelectListItem { Text = l.LookupText, Value = l.LookupValue });
            model.PriceHighRange = (from l in lookupService.GetCommonListItems(CommonListItem.MaxPrice)
                                    select new SelectListItem { Text = l.LookupText, Value = l.LookupValue });
            model.SearchText = string.Empty;
            model.IsInactive = true;
            if (Request.IsAjaxRequest())
            {
                var view = NhsRoute.ShowMobileSite
                    ? NhsMvc.Default.ViewsMobile.Common.PropertyDetail.InactiveProperty
                    : NhsMvc.Default.Views.Common.PropertyDetail.InactivePropertyForm;
                return PartialView(view, model);
            }
            var baseModel = (BaseViewModel)model;
            baseModel.IsitInactiveData = true;

            AddCanonicalTag(baseModel, new List<string> { "planid", "specid" }, false);

            return View(model);
        }

        protected bool ShowSocialIcons(bool isBilled)
        {
            return (((NhsRoute.IsBrandPartnerNhsPro) && isBilled) ||
                    (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>()));
        }

        protected LeadSectionStateConst SetLeadSectionState(bool isBilled, bool isLoggedIn)
        {
            var sectionState = LeadSectionStateConst.LoggedInPaid;
            if (isBilled && isLoggedIn)
                sectionState = LeadSectionStateConst.LoggedInPaid;
            else if (!isBilled && isLoggedIn)
                sectionState = LeadSectionStateConst.LoggedInUnpaid;
            else if (isBilled && !isLoggedIn)
                sectionState = LeadSectionStateConst.LoggedOutPaid;
            else if (!isBilled && !isLoggedIn)
                sectionState = LeadSectionStateConst.LoggedOutUnpaid;
            return sectionState;
        }

        protected string FormatedPercentage(string expr)
        {
            var res = string.Empty;

            try
            {
                double num;
                bool isNum = double.TryParse(expr, out num);
                if (isNum)
                {
                    res = string.Format("{0}%", num);
                }

            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
                res = string.Empty;
            }

            return res;
        }

        protected bool AddCanonical(int communityId)
        {
            //add a canonical if community is active on brand partner 
            //add a canonical if we are on a partner private-label site
            return _communityService.IsCommunityActiveOnParentBrandPartner(communityId, NhsRoute.BrandPartnerId) ||
                   NhsRoute.PartnerId != NhsRoute.BrandPartnerId;
        }

        protected void SendMessageAgCompensation(IDetailViewModel model)
        {
            var logger = new ImpressionLogger
            {
                CommunityId = model.CommunityId,
                BuilderId = model.BuilderId,
                PartnerId = NhsRoute.PartnerId.ToType<String>(),
                Refer = UserSession.Refer
            };

            if (!model.IsPageCommDetail)
            {
                logger.AddSpec(model.PlanId);
                if (!model.IsPlan)
                {
                    logger.AddSpec(model.SpecId);
                }
            }

            logger.LogView(LogImpressionConst.RequestCommision);

            var lead = BuildLeadForBuilderMessage(model, null, LeadAction.AgentCompensation, false);
            model.Globals.Google = TrackingScriptsHelper.GetConversionTrackerUrlSendCustomBrochure(model, lead);
        }

        protected void SendMessageToBuilder(IDetailViewModel model)
        {
            var logger = new ImpressionLogger
                {
                    CommunityId = model.CommunityId,
                    BuilderId = model.BuilderId,
                    PartnerId = NhsRoute.PartnerId.ToType<String>(),
                    Refer = UserSession.Refer
                };

            var leadAction = model.RequestAnAppointment || model.ScheduleAppointment
                                 ? LeadAction.RequestApointment
                                 : LeadAction.ContactBuilder;
            var lead = BuildLeadForBuilderMessage(model, logger, leadAction);

            logger.LogView(model.RequestAnAppointment || model.ScheduleAppointment
                               ? LogImpressionConst.RequestAppointment
                               : LogImpressionConst.GeneralInquiry);

            model.Globals.Google = TrackingScriptsHelper.GetConversionTrackerUrlSendCustomBrochure(model, lead);
        }

        private LeadInfo BuildLeadForBuilderMessage(IDetailViewModel model, ImpressionLogger logger, string leadAction, bool useImpressionLogger = true)
        {
            var lead = new LeadInfo
                {
                    LeadUserInfo =
                        {
                            Email = UserSession.UserProfile.LogonName,
                            FirstName = UserSession.UserProfile.FirstName,
                            LastName = UserSession.UserProfile.LastName,
                            SessionId = UserSession.SessionId
                        },
                    RequestUniqueKey = string.Format("{0}-c{1}-", Guid.NewGuid(), model.CommunityId),
                    MarketId = model.MarketId,
                    CommunityList = model.CommunityId.ToType<String>(),
                    ClientsInfo =
                        string.Format("<ClientsInfo><Client Name=\"{0}\" Email=\"{1}\" /></ClientsInfo>",
                                      UserSession.UserProfile.FirstName.ToTitleCase() + " " +
                                      UserSession.UserProfile.LastName.ToTitleCase(), UserSession.UserProfile.Email),
                    BuilderId = model.BuilderId,
                    LeadType = model.PlanId == 0 && model.SpecId == 0 ? LeadType.Community : LeadType.Home,
                    LeadAction = leadAction,
                    Referer = UserSession.Refer,
                    SuppressEmail = true,
                    LeadComments = model.Message
                };

            if (model.PlanId > 0 || model.SpecId > 0)
            {
                if (model.IsPlan)
                {
                    if (useImpressionLogger && logger != null)
                    {
                        logger.AddPlan(model.PlanId);
                    }
                    lead.PlanList = model.PlanId.ToType<String>();
                    lead.RequestUniqueKey += "p" + lead.PlanList;
                }
                else
                {
                    if (useImpressionLogger && logger != null)
                    {
                        logger.AddSpec(model.SpecId);
                    }
                    lead.SpecList = model.SpecId.ToType<String>();
                    lead.RequestUniqueKey += "s" + lead.SpecList;
                }
            }
            LeadUtil.GenerateLead(lead);
            return lead;
        }

        protected ClientTask GetSendCustomBrochure(LeadViewModels.SendCustomBrochurePost model)
        {
            var listingIds = new List<int>();
            var proccessTime = DateTime.Now;
            var description = "Sent brochure multiple communities/homes";
            var notes = "";
            if (!model.IsMultiBrochure)
            {
                string nameListing;
                string nameBuilder;
                string link;
                if (model.IsCommunity)
                {
                    var community = _communityService.GetCommunity(model.CommunityId);
                    listingIds.Add(model.CommunityId);
                    nameListing = community.CommunityName;
                    nameBuilder = community.Builder.BuilderName;
                    var @params = new List<RouteParam>
                        {
                            new RouteParam(RouteParams.Builder, community.BuilderId.ToType<string>()),
                            new RouteParam(RouteParams.Community, community.CommunityId.ToType<string>())
                        };
                    link = @params.ToUrl(Pages.CommunityDetail, false, true);
                }
                else if (model.IsPlan)
                {
                    var plan = _listingService.GetPlan(model.PlanId);
                    listingIds.Add(model.PlanId);
                    nameListing = plan.PlanName;
                    nameBuilder = plan.Community.Builder.BuilderName;
                    var @params = new List<RouteParam>
                        {
                            new RouteParam(RouteParams.PlanId, model.PlanId.ToType<string>())
                        };
                    link = @params.ToUrl(Pages.HomeDetail, false, true);
                }
                else
                {
                    var spec = _listingService.GetSpec(model.SpecId);
                    listingIds.Add(model.SpecId);
                    nameListing = spec.PlanName;
                    nameBuilder = spec.Community.Builder.BuilderName;
                    var @params = new List<RouteParam>
                        {
                            new RouteParam(RouteParams.PlanId, model.SpecId.ToType<string>())
                        };
                    link = @params.ToUrl(Pages.HomeDetail, false, true);
                }
                description = string.Format("Sent brochure for {0}", nameListing);
                notes = string.Format("By {0}. {1}", nameBuilder, link);
            }
            else
                listingIds.AddRange(UserSession.MultiBrochureList.BrochureList.Select(p => p.CommunityId));
            var task = new ClientTask
            {
                Description = description,
                Notes = notes,
                TaskType = ProCrmTaskTypes.Email.ToString(),
                TaskDate = proccessTime,
                DateLastChanged = proccessTime,
                TaskCompleteDate = proccessTime
            };
            foreach (var listingId in listingIds)
            {
                task.ClientTaskListings.Add(new ClientTaskListing
                {
                    ListingType =
                        model.IsCommunity || model.IsMultiBrochure ? "C" : model.IsPlan ? "P" : "S",
                    PropertyId = listingId
                });
            }
            return task;
        }

        protected string AgentPolicyUrl(int communityId, int builderId)
        {
            var externalUrl = string.Empty;

            var agentPolicies =
                    _communityService.GetCommunityAgentPolicies(communityId, builderId)
                                     .ToList();

            var policyLink = agentPolicies.Select(p => new CommunityAgentPolicy
            {
                PromoURL = p.PromoURL,
                PolicyFlyerURL = p.PolicyFlyerURL
            }).ToList();

            if (policyLink.Any())
            {
                var communityAgentPolicy = policyLink.FirstOrDefault();
                if (communityAgentPolicy != null)
                {
                    externalUrl = !string.IsNullOrEmpty(communityAgentPolicy.PolicyFlyerURL)
                                      ? communityAgentPolicy.PolicyFlyerURL
                                      : (!string.IsNullOrEmpty(communityAgentPolicy.PromoURL)
                                             ? communityAgentPolicy.PromoURL
                                             : string.Empty);


                    if (!string.IsNullOrEmpty(communityAgentPolicy.PolicyFlyerURL))
                        externalUrl = Configuration.AgentPolicyUrl + externalUrl;
                }
            }
            return externalUrl;
        }

        protected string GetInactiveUrl()
        {
            return string.Format(Request.RawUrl + "{0}{1}={2}", (Request.RawUrl.IndexOf("?") == -1 ? "?" : "&"), RouteParams.Msg.ToString().ToLower(), "no-longer-available");
        }

        protected void FillExtendedMediaObjectsInfo(IDetailViewModel model)
        {
            model.PlayerMediaObjects = FillExtendedMediaObjectsInfo(model.PlayerMediaObjects, model.IsPageCommDetail);
        }

        protected List<MediaPlayerObject> FillExtendedMediaObjectsInfo(IList<MediaPlayerObject> playerMediaObjects, bool isCommunity)
        {
            var bcVideos = BCAPI.FindVideosByTags(string.Empty, playerMediaObjects.Where(v => !string.IsNullOrEmpty(v.VideoID)).ToList().Select(v => v.VideoID).ToArray().Join(","));

            playerMediaObjects.ForEach(v =>
            {
                if (v.Type == "i" && !string.IsNullOrEmpty(v.Url) && v.Url.IndexOf("?", StringComparison.Ordinal) != -1)
                {
                    v.Url = v.Url.Substring(0, v.Url.IndexOf("?", StringComparison.Ordinal)); // show images in the original size 
                }
                // Videos Type
                if (!string.IsNullOrEmpty(v.VideoID))
                {
                    v.OnlineVideoID = v.VideoID;

                    if (v.SubType == MediaPlayerObjectTypes.SubTypes.BrightcoveVideo && bcVideos != null)
                    {
                        var video = bcVideos.FirstOrDefault(bcV => bcV.referenceId == v.VideoID);
                        if (video != null)
                        {
                            v.Url = video.videoStillURL;
                            v.Thumbnail = video.thumbnailURL;
                            v.OnlineVideoID = video.id.ToType<string>();
                        }
                    }
                }
                // External Links
                else if (v.Type == MediaPlayerObjectTypes.Link)
                {
                    v.TypeDescription = MediaPlayerObjectTypes.MediaDescriptions[string.Format("{0}-{1}", v.Type, v.SubType)];

                    if (v.SubType == MediaPlayerObjectTypes.SubTypes.VirtualTour)
                        v.Event = (isCommunity ? LogImpressionConst.CommunityDetailVirtualTour : LogImpressionConst.HomeDetailVirtualTour);
                    else if (v.SubType == MediaPlayerObjectTypes.SubTypes.FloorPlanImages)
                        v.Event = LogImpressionConst.HomeDetailInteractiveFloorPlan;
                    else if (v.SubType == MediaPlayerObjectTypes.SubTypes.PlanVideo)
                        v.Event = LogImpressionConst.CommunityDetailInteractiveFloorPlan;
                    else if (v.SubType == MediaPlayerObjectTypes.SubTypes.ExternalVideo)
                        v.Event = LogImpressionConst.CommunityDetailExternalVideo;
                }
            });

            return playerMediaObjects.Where(v => v.Type != MediaPlayerObjectTypes.Video || !string.IsNullOrEmpty(v.Thumbnail)).OrderBy(m => (m.Type == "v" ? 1 : (m.Type == "i" ? 2 : 3))).ThenBy(m => m.Sort).ToList();

        }
    }
}
