using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Bdx.Web.Api.Objects;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Maps;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Proxy;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    /// <summary>
    /// Map controller, class that returns any data requested by the mapping funtionality
    /// </summary>
    public partial class MapController : ApplicationController
    {
        private readonly IApiService _apiService;
        private readonly ICommunityService _communityService;
        private readonly IBasicListingService _basicListingService;
        private readonly IMarketService _marketService;
        private readonly IBrandService _brandService;

        public MapController(IApiService apiService, ICommunityService communityService, IBasicListingService basicListingService, IPathMapper pathMapper, IMarketService marketService, IBrandService brandService)
            : base(pathMapper)
        {
            _apiService = apiService;
            _brandService = brandService;
            _communityService = communityService;
            _basicListingService = basicListingService;
            _marketService = marketService;
        }
        
        //public virtual ActionResult GetCommMapPoints(string searchParameters, double minLat, double minLng, double maxLat, double maxLng, int maxResults)
        //{
        //    int totalResultsCount = 0;
        //    string[] result = MapHelper.GetCommMapPointsExt(searchParameters, minLat, minLng, maxLat, maxLng, maxResults, ref totalResultsCount);
        //    var temp = new string[] { totalResultsCount.ToString() };

        //    return Json(temp.Concat(result).ToArray().ToJson(), JsonRequestBehavior.AllowGet);
        //}

        //This wil be remove when Pro is migrated
        public virtual ActionResult GetCommMapPointsV2(string searchParameters, double minLat, double minLng, double maxLat, double maxLng)
        {
           
            var total = 0;
            string[] result = MapHelper.GetCommMapPointsExt(searchParameters, minLat, minLng, maxLat, maxLng, 0, ref total);

            var resultsConverted = result != null && result.Length > 0 ? result.Select(
                r => new ApiMapPoint()
                         {
                             Id = r.Split(';')[0].ToType<int>(),
                             IsBl = (r.Split(';')[16] == "True" ? 1 : 0),
                             IsBasic = (r.Split(';')[17] == "True" ? 1 : 0),
                             Lat = r.Split(';')[13],
                             Lng = r.Split(';')[14],
                             Name = r.Split(';')[2],
                             PrLo = (r.Split(';')[8] == "$ - $" || r.Split(';')[8] == "$2,147,483,647 - $") ? "0" : r.Split(';')[8].Split('-')[0].Replace("$", string.Empty).Trim(),
                             PrHi = (r.Split(';')[8] == "$ - $" || r.Split(';')[8] == "$2,147,483,647 - $") ? "0" : r.Split(';')[8].Split('-')[1].Replace("$", string.Empty).Trim()
                         }).ToList() : new List<ApiMapPoint>();

            // Group comms with same lat long 
            var gruop = resultsConverted.GroupBy(n => new { n.Lat, n.Lng }).Select(g =>
                                                                              new
                                                                              {
                                                                                  g.Key.Lat,
                                                                                  g.Key.Lng,
                                                                                  isBasic = g.All(p => p.IsBl == 1),
                                                                                  Name = g.Count(),
                                                                                  HomesCount = g.Sum(p => p.NumHomes),
                                                                                  Price = StringHelper.PrettyPrintRange(g.Min(p => p.PrLo).ToType<double>(), g.Max(p => p.PrHi).ToType<double>(), "c0", LanguageHelper.From.ToTitleCase()),
                                                                                  MarketPoints = g
                                                                              });

            return Json(new { Total = resultsConverted.Count, Results = gruop }.ToJson(), JsonRequestBehavior.AllowGet);
        }

        //This wil be remove when Pro is migrated
        public virtual ActionResult GetMarketMapPoints(int partnerId, double minLat, double minLng, double maxLat, double maxLng)
        {
            return Json(MapHelper.GetMarketMapPoints(partnerId, minLat, minLng, maxLat, maxLng).ToJson(), JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult GetBrandMarketMapPoints(int partnerId, int brandId, double minLat, double minLng, double maxLat, double maxLng)
        {
            var searchParams = new SearchParams
            {
                MinLat = minLat,
                MinLng = minLng,
                MaxLat = maxLat,
                MaxLng = maxLng,
                BrandId = brandId,
                PageSize =  999999,
                PageNumber = 1,
                PartnerId = partnerId,
                NoBoyl = false,
                WebApiSearchType = WebApiSearchType.Map
            };

            var pointsApi = _apiService.GetResultsWebApi<ApiCommunityResultV2>(searchParams, WebApiMethods.Communities).Result;
            var groupPoints = pointsApi.Where(p => p.MarketId > 0).GroupBy(p => p.MarketId);
            var market = _marketService.GetMarkets(partnerId);
             
            var points = new List<object>();
            foreach (var point in groupPoints)
            {
                var m = market.FirstOrDefault(p => p.MarketId == point.Key);
                var actionUrl = NhsLinkHelper.BuildLink(point.Any(f => f.CommunityType == "N") ? Pages.CommunityResults : Pages.BOYLResults, new List<RouteParam>
                    {
                        new RouteParam(RouteParams.Market, m.MarketId),
                        new RouteParam(RouteParams.BrandId, brandId)
                    });

                points.Add(new
                {
                    Lat = m.Latitude,
                    Lng = m.Longitude,
                    Title = m.MarketName + ", " + m.State.StateAbbr + " (" + point.Count() + (point.Count() > 1 ? " " + LanguageHelper.Communities : " " + LanguageHelper.Community) + ")",
                    ActionUrl = actionUrl
                });
            }
            return Json(points, JsonRequestBehavior.AllowGet);
        }

        //This wil be remove when Pro is migrated
        public virtual ActionResult GetStateMapPoints(int partnerId, double minLat, double minLng, double maxLat, double maxLng)
        {
            return Json(MapHelper.GetStateMapPoints(partnerId, minLat, minLng, maxLat, maxLng).ToJson(), JsonRequestBehavior.AllowGet);
        }
        
        //public virtual ActionResult GetStateMarketMapPoints(int partnerId, string state)
        //{
        //    return this.Content(MapHelper.GetStateMarketMapPoints(partnerId, state).OuterXml, "text/xml");
        //}

        public virtual ActionResult DrivingDirections(int communityId, string lat, string lng, bool check, bool isBasicListing, string startingPoint)
        {
            var model = new PropertyMapViewModel();

            if (isBasicListing)
            {
                var basicListing = _basicListingService.GetBasicListing(communityId);
                model.Latitude = basicListing.Latitude.ToType<double>();
                model.Longitude = basicListing.Longitude.ToType<double>();
            }
            else
            {
                var comm = _communityService.GetCommunity(communityId);
                model.BrandName = comm.Brand.BrandName;
                model.Latitude = (double)comm.Latitude;
                model.Longitude = (double)comm.Longitude;
                model.PropertyName = comm.CommunityName;
                model.DrivingDirections = comm.SalesOffice.DrivingDirections;
                model.Globals.AdController.AddMarketParameter(comm.MarketId);
                model.Globals.AdController.AddCommunityParameter(comm.CommunityId);
                model.Globals.AdController.AddPriceParameter(comm.PriceLow.ToType<int>(), comm.PriceHigh.ToType<int>());
                model.Globals.AdController.AddStateParameter(comm.StateAbbr);
                model.Globals.AdController.AddBuilderParameter(comm.BuilderId);
                model.Globals.AdController.AddCityParameter(comm.City);
                model.Globals.AdController.AddZipParameter(comm.PostalCode);
            }

            model.StartingPoint = startingPoint;
            model.RouteLat = lat.ToType<double>();
            model.RouteLng = lng.ToType<double>();

            // If start point found, get the driving directions
            if (model.RouteLat != 0 && model.RouteLng != 0 && !check)
            {
                // ReSharper disable once Mvc.ViewNotResolved
                return View(model);
            }

            if (model.RouteLat != 0 && model.RouteLng != 0 && check)
                return Content("Address Ok");

            return Content("Unable to get driving directions");
        }

        public virtual ActionResult GetMapImageUrl(string mapPoints, string size, bool usedZoom = false, int zoom = 13, string icon = "", string color = "")
        {
            return Json(new { url = GoogleHelperMaps.StaticMapUrl(mapPoints, size, usedZoom, zoom, icon, color) }, JsonRequestBehavior.AllowGet);
        }
    }
}
