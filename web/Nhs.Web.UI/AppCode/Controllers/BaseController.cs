﻿/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright © 2001-2010 Builders Digital Experience, LLC.
 * ========================================================
 * Author: Praveen Guggarigoudar
 * Date: 12/15/2010
 * Edit History (please mention edits succinctly):
 * =========================================================
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.SessionState;
using Nhs.Library.Business;
using Nhs.Library.Business.Seo;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Constants.Route;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Mvc.Routing.Utils;
using Nhs.Utility.Common;
using Nhs.Utility.Web;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;
using StructureMap;

namespace Nhs.Web.UI.AppCode.Controllers
{
    /// <summary>
    /// Base class for ApplicationController and PartialViewsController
    /// </summary>
    public partial class BaseController : Controller
    {
        private readonly IPathMapper _pathMapper;

        private readonly IBuilderService _builderService;

        public IPathMapper PathMapper
        {
            get
            {
                return _pathMapper;
            }
        }

        #region Constructors
        public BaseController()
        {
            _builderService = GetInstanceOf<IBuilderService>();
        }

        public BaseController(IPathMapper pathMapper)
            : this()
        {
            _pathMapper = pathMapper;
        }
        #endregion

        /// <summary>
        /// GEt the instance of an interface defined in the IoC 
        /// </summary>
        /// <typeparam name="T">Interface</typeparam>
        /// <returns></returns>
        public T GetInstanceOf<T>()
        {
            return ObjectFactory.GetInstance<T>();
        }

        #region Override Meta

        protected void OverrideDefaultMeta(BaseViewModel viewModel, IList<ContentTag> contentTags,
            SeoTemplateType seoTemplateType, IList<MetaTagFacebook> facebookMetas = null, SrpTypeEnum strType = SrpTypeEnum.CommunityResults)
        {
            var metaReader = new MetaReader(_pathMapper);
            List<MetaTag> metas;

            if (seoTemplateType != SeoTemplateType.None)
            {
                var seoContentManager = new SeoContentManager();
                seoContentManager.Market = SeoContentHelper.GetSeoContentTag(ContentTagKey.MarketName, contentTags);
                seoContentManager.City = SeoContentHelper.GetSeoContentTag(ContentTagKey.CityName, contentTags);
                seoContentManager.County = SeoContentHelper.GetSeoContentTag(ContentTagKey.CountyName, contentTags);
                seoContentManager.State = SeoContentHelper.GetSeoContentTag(ContentTagKey.StateID, contentTags);
                seoContentManager.Zip = SeoContentHelper.GetSeoContentTag(ContentTagKey.Zip, contentTags);
                seoContentManager.BrandName = SeoContentHelper.GetSeoContentTag(ContentTagKey.BrandName, contentTags);
                seoContentManager.BrandID = SeoContentHelper.GetSeoContentTag(ContentTagKey.BrandID, contentTags);
                seoContentManager.SchoolDistrict = SeoContentHelper.GetSeoContentTag(ContentTagKey.SchoolDistrict, contentTags);
                seoContentManager.SyntheticName = SeoContentHelper.GetSeoContentTag(ContentTagKey.SyntheticName, contentTags);
                seoContentManager.PageName = NhsRoute.CurrentRoute.Function;

                seoContentManager.CommunityName = SeoContentHelper.GetSeoContentTag(ContentTagKey.CommunityName, contentTags);

                if (contentTags == null)
                    contentTags = new List<ContentTag>();

                var partnerSiteUrl = NhsRoute.PartnerSiteUrl;

                if (!string.IsNullOrEmpty(partnerSiteUrl))
                    partnerSiteUrl += "/";

                contentTags.ToList().Add(new ContentTag() { TagKey = ContentTagKey.PartnerSiteUrl, TagValue = partnerSiteUrl });
                seoContentManager.PartnerSiteUrl = NhsRoute.PartnerSiteUrl;
                metas = seoContentManager.GetMetaTags(seoTemplateType, contentTags, strType);
            }
            else
            {
                metas = metaReader.GetMetaTagInformation(NhsRoute.CurrentRoute.Function, contentTags.ToDictionary(),
                                                         PageHeaderSectionName.SeoMetaPages);
            }
            OverrideDefaultMeta(viewModel, metas);

            if (facebookMetas != null)
                if (facebookMetas.Count > 0)
                    viewModel.Globals.Facebook = string.Format("<!--Start Graph Tags for the Like -->{1}{0}{1}<!--End Graph Tags for the Like --> ", MetaReader.ConstructTag(facebookMetas), Environment.NewLine);
        }

        protected void OverrideDefaultMeta(BaseViewModel viewModel, IList<MetaTag> metas)
        {
            if (metas.Count > 0)
            {
                viewModel.Globals.Title = MetaReader.ConstructTag(metas, MetaName.Title); //Initialize base title tag (bad pattern I know!)
                viewModel.Globals.Title = viewModel.Globals.PartnerLayoutConfig.PartnerIsABrand ? MetaReader.ConstructTag(metas, MetaName.Title) : viewModel.Globals.Shell.Title.Value; //Override if a partner
                viewModel.Globals.Keywords = MetaReader.ConstructTag(metas, MetaName.Keywords);
                viewModel.Globals.Description = MetaReader.ConstructTag(metas, MetaName.Description);
                viewModel.Globals.Robots = MetaReader.ConstructTag(metas, MetaName.Robots);
                viewModel.Globals.CanonicalLink = MetaReader.ConstructTag(metas, MetaName.Canonical);
            }
        }

        protected void OverwriteMetaFromContent(BaseViewModel viewModel, SeoBaseContent seoContent, List<MetaTagFacebook> facebookMetaTags = null)
        {
            OverrideDefaultMeta(viewModel, new List<MetaTag>()
            {
                new MetaTag
                {
                    Name = MetaName.Keywords,
                    Content = seoContent.MetaKeywords
                },
                new MetaTag
                {
                    Name = MetaName.Title,
                    Content = seoContent.Title
                },
                new MetaTag
                {
                    Name = MetaName.Description,
                    Content = seoContent.MetaDescription
                },
                new MetaTag
                {
                    Name = MetaName.Robots,
                    Content = seoContent.MetaRobots
                },
                new MetaTag
                {
                    Name = MetaName.Canonical,
                    Content = seoContent.Canonical
                }
            });
            if (facebookMetaTags != null && facebookMetaTags.Count > 0)
                viewModel.Globals.Facebook = string.Format("<!--Start Graph Tags for the Like -->{1}{0}{1}<!--End Graph Tags for the Like --> ", MetaReader.ConstructTag(facebookMetaTags), Environment.NewLine);
        }


        protected void OverrideDefaultMeta(BaseViewModel viewModel, IList<MetaTag> metas, IList<MetaTagFacebook> facebookMetas)
        {
            OverrideDefaultMeta(viewModel, metas);
            if (facebookMetas.Count > 0)
                viewModel.Globals.Facebook = string.Format("<!--Start Graph Tags for the Like -->{1}{0}{1}<!--End Graph Tags for the Like --> ", MetaReader.ConstructTag(facebookMetas), Environment.NewLine);
        }

        protected void OverrideDefaultMeta(BaseViewModel viewModel, IList<ContentTag> contentTags)
        {
            OverrideDefaultMeta(viewModel, contentTags, SeoTemplateType.None);
        }

        #endregion

        #region Redirect Helpers
        /// <summary>
        /// Redirect action, allow to redirect to another page change in the url in the browser
        /// </summary>
        /// <param name="url">Page to redirect</param>
        /// <param name="usedjavascript"></param>
        /// <returns></returns>
        public virtual ActionResult RedirectAjaxForm(string url, bool usedjavascript = false)
        {
            if (usedjavascript)
                return JavaScript("window.location = '" + url + "'");
            return Redirect(url);
        }

        public virtual ActionResult RedirectAjaxForm(string function, IList<RouteParam> urlParams)
        {
            return JavaScript("window.location = '" + urlParams.ToUrl(function) + "'");
        }

        public virtual RedirectResult RedirectPermanent(string function, IList<RouteParam> urlParams)
        {
            return base.RedirectPermanent(urlParams.ToUrl(function, false));
        }

        public virtual RedirectResult RedirectPermanent(string function, IList<RouteParam> urlParams, bool completeUrl)
        {
            return base.RedirectPermanent(urlParams.ToUrl(function, false, completeUrl));
        }

        public virtual RedirectResult Redirect(string function, IList<RouteParam> urlParams)
        {
            return base.Redirect(urlParams.ToUrl(function));
        }

        public virtual RedirectResult Redirect(string function, IList<RouteParam> urlParams, string extraParameters)
        {
            var partnerInUrl = NhsRoute.PartnerSiteUrl.ToLower();
            var redirectUrl = urlParams.ToUrl(function);
            if (!string.IsNullOrEmpty(partnerInUrl) && redirectUrl.Contains(partnerInUrl)
                && string.IsNullOrWhiteSpace(extraParameters) == false && extraParameters.ToLower().Contains(partnerInUrl))
            {
                var strRemove = partnerInUrl + "/";
                extraParameters = extraParameters.Replace(strRemove, "").Trim();
            }

            if (string.IsNullOrWhiteSpace(extraParameters) == false)
            {
                if (redirectUrl.Contains("?"))
                {
                    extraParameters = extraParameters.Replace("?", "&");
                    extraParameters = extraParameters.TrimStart('/');
                }
            }

            var finalUrl = redirectUrl + extraParameters;

            return base.Redirect(finalUrl.Replace("//", "/"));
        }

        #region RouteParam


        protected RedirectResult RedirectTo404()
        {
            var urlParams = new List<RouteParam>();
            urlParams.Add(new RouteParam(RouteParams.Statuscode, "404", RouteParamType.QueryString));
            return Redirect(urlParams.ToUrl(Pages.MvcError.ToTitleCase()));
        }

        public virtual ActionResult RedirectAjaxForm(string function, List<RouteParam> urlParams)
        {
            return JavaScript("window.location = '" + urlParams.ToUrl(function) + "'");
        }

        public virtual RedirectResult RedirectPermanent(string function, List<RouteParam> urlParams)
        {
            return base.RedirectPermanent(urlParams.ToUrl(function, false));
        }

        public virtual RedirectResult RedirectPermanent(string function, List<RouteParam> urlParams, bool completeUrl)
        {
            return base.RedirectPermanent(urlParams.ToUrl(function, false, completeUrl));
        }

        public virtual RedirectResult Redirect(string function, List<RouteParam> urlParams)
        {
            return base.Redirect(urlParams.ToUrl(function));
        }

        public virtual RedirectResult Redirect(string function, List<RouteParam> urlParams, string extraParameters)
        {
            var partnerInUrl = NhsRoute.PartnerSiteUrl.ToLower();
            var redirectUrl = urlParams.ToUrl(function);
            if (!string.IsNullOrEmpty(partnerInUrl) && redirectUrl.Contains(partnerInUrl)
                && string.IsNullOrWhiteSpace(extraParameters) == false && extraParameters.ToLower().Contains(partnerInUrl))
            {
                var strRemove = partnerInUrl + "/";
                extraParameters = extraParameters.Replace(strRemove, "").Trim();
            }

            if (string.IsNullOrWhiteSpace(extraParameters) == false)
            {
                if (redirectUrl.Contains("?"))
                {
                    extraParameters = extraParameters.Replace("?", "&");
                    extraParameters = extraParameters.TrimStart('/');
                }
            }

            var finalUrl = redirectUrl + extraParameters;

            return base.Redirect(finalUrl.Replace("//", "/"));
        }
        #endregion

        protected RedirectToRouteResult RedirectToNhsRoute(ActionResult actionResult)
        {
            string routeName = RouteHelper.GetRouteNameFromAction(actionResult.GetRouteValueDictionary());
            if (NhsRoute.PartnerSiteUrl.Length > 0)
                routeName = "p" + routeName;

            return base.RedirectToRoute(routeName, actionResult.GetRouteValueDictionary());
        }

        protected RedirectToRouteResult RedirectToNhsAction(ActionResult actionResult)
        {
            return RedirectToNhsRoute(actionResult);
        }
        #endregion

        #region Error Handling
        protected override void OnException(ExceptionContext filterContext)
        {
            ErrorLogger.LogError(filterContext.Exception);
            if (filterContext.HttpContext.IsCustomErrorEnabled)
            {
                filterContext.ExceptionHandled = true;
                filterContext.Result = this.RedirectToRoute(NhsMvc.MVCError.Name);
            }
        }
        #endregion

        #region Canonical Tag

        protected void AddProResourceCenterCanonicalTag(BaseViewModel viewModel)
        {
            var href = CanonicalHelper.GetProResourceCenterCanonicalString(new List<string>(), false, _pathMapper, null, false);

            if (viewModel.IsitInactiveData && !string.IsNullOrEmpty(href))
                href = href.Split('?')[0]; // remove parameters for inactive detail pages.

            ////Related with hotfix ticket: 70244 Add canonical and noindex to v2 pages
            //if (href.Contains("communityresultsv2")) href = href.Replace("communityresultsv2", "communityresults");

            viewModel.Globals.CanonicalLink = string.Format("<link rel=\"canonical\" href=\"{0}\" />", href);
        }

        protected void AddCanonicalTag(BaseViewModel viewModel)
        {
            AddCanonicalWithIncludeParams(viewModel, new List<string>());
        }

        protected void AddCanonicalTag(BaseViewModel viewModel, string href)
        {
            viewModel.Globals.CanonicalLink = string.Format("<link rel=\"canonical\" href=\"{0}\" />", href);
        }

        protected void AddCanonicalWithExcludeParams(BaseViewModel viewModel, List<string> paramz)
        {
            AddCanonicalTag(viewModel, paramz, true);
        }

        protected void AddCanonicalWithExcludeParamsAndReplace(BaseViewModel viewModel, List<string> paramz, Dictionary<string, string> replaceparamz)
        {
            AddCanonicalTag(viewModel, paramz, true, replaceparamz);
        }

        protected void AddCanonicalWithIncludeParams(BaseViewModel viewModel, List<string> paramz, bool usePartnerUrl = false)
        {
            AddCanonicalTag(viewModel, paramz, false, null, usePartnerUrl);
        }

        protected void AddCanonicalTag(BaseViewModel viewModel, List<string> paramz, bool excludeParams, Dictionary<string, string> replaceparamz = null, bool usePartnerUrl = false)
        {
            var href = CanonicalHelper.GetCanonicalString(paramz, excludeParams, _pathMapper, replaceparamz, usePartnerUrl);

            if (viewModel.IsitInactiveData && !string.IsNullOrEmpty(href))
                href = href.Split('?')[0]; // remove parameters for inactive detail pages.

            //Related with hotfix ticket: 70244 Add canonical and noindex to v2 pages
            if (href.Contains("communityresultsv2")) href = href.Replace("communityresultsv2", "communityresults");

            viewModel.Globals.CanonicalLink = string.Format("<link rel=\"canonical\" href=\"{0}\" />", href);
        }

        #endregion

        protected void AddNoIndex(MetaRegistrar metaRegistrar, List<MetaTag> metas)
        {
            var robotMeta = metaRegistrar.GetRobotsMeta("noindex").First();
            var robot = metas.FirstOrDefault(m => m.Name == MetaName.Robots);
            if (robot.Name != MetaName.Robots) //UNKNOWN REASON, COMES BACK AS TITLE, SO EXTRA CHECK
            {
                robot = robotMeta;
            }
            else
            {
                if (!string.IsNullOrEmpty(robot.Content))
                    robot.Content += ", " + robotMeta.Content; //append to current
                else
                    robot.Content = robotMeta.Content;
            }

            metas.RemoveAll(m => m.Name == MetaName.Robots);
            metas.Add(robot);
        }

        protected void AddNoIndex(SeoBaseContent seoContent)
        {
            seoContent.MetaRobots = string.IsNullOrEmpty(seoContent.MetaRobots)
                ? "NOINDEX"
                : seoContent.MetaRobots += ", NOINDEX";
        }

        protected void SetNoIndexRobotsToModel(BaseViewModel model)
        {
            var metaRegistrar = new MetaRegistrar(_pathMapper);
            var metas = metaRegistrar.GetRobotsMeta("noindex");
            AddNoIndex(metaRegistrar, metas.ToList());
            OverrideDefaultMeta(model, metas);
        }

        protected void SetupAdParameters(BaseViewModel model)
        {
            var state = string.Empty;
            var marketId = 0;
            var marketService = GetInstanceOf<IMarketService>();
            bool useSessionBuilders = false;

            if (UserSession.PersonalCookie.MarketId > 0 && model.Globals.AdController.MarketParam == 0) //First precedence - Market Cookie
            {
                marketId = UserSession.PersonalCookie.MarketId;
                var currMarket = marketService.GetMarket(NhsRoute.PartnerId, marketId, false);

                if (currMarket != null)
                    state = currMarket.StateAbbr ?? string.Empty;

                useSessionBuilders = UserSession.AdBuilderIds.Count > 0;
            }
            else if (model.Globals.AdController.MarketParam > 0) //Second Precedence - Force Market via Param
            {
                marketId = model.Globals.AdController.MarketParam > 0
                    ? model.Globals.AdController.MarketParam
                    : UserSession.PersonalCookie.MarketId > 0
                        ? UserSession.PersonalCookie.MarketId
                        : -1;

                state = marketId > 0
                    ? marketService.GetMarket(NhsRoute.PartnerId, marketId, false).StateAbbr
                    : string.Empty;
            }
            else //Get market from IP
            {
                var ip = RouteParams.Ip.Value<uint>() <= 0 ? HTTP.GetClientIpAddress(Request).IpToInt32() : RouteParams.Ip.Value<uint>(); //test ip by passing in query param
                var ipLocation = marketService.GetMarketFromIp(ip);
                if (ipLocation.MarketId > 0)
                {
                    marketId = ipLocation.MarketId;
                    state = ipLocation.StateAbbr;
                }
            }

            model.Globals.AdController.AddMarketParameter(marketId);
            model.Globals.AdController.AddStateParameter(state);
            this.AddMarketBuildersToAdController(model, marketId, useSessionBuilders);
        }

        private void AddMarketBuildersToAdController(BaseViewModel model, int marketId, bool useSessionBuilders)
        {
            if (useSessionBuilders)
            {
                if (UserSession.AdBuilderIds.Count > 0)
                {
                    foreach (var bid in UserSession.AdBuilderIds)
                        model.Globals.AdController.AddBuilderParameter(bid);
                }
                return;
            }

            if (marketId <= 0)
                return;

            //else figure builders from market
            UserSession.AdBuilderIds.Clear();
            var builderIds = _builderService.GetMarketBuilders(NhsRoute.PartnerId, marketId);
            foreach (var builder in builderIds)
            {
                if (builder.IsBilled)
                {
                    model.Globals.AdController.AddBuilderParameter(builder.BuilderId);
                    UserSession.AdBuilderIds.Add(builder.BuilderId);
                }
            }
        }

        internal void GenerateNewSession()
        {
            var manager = new SessionIDManager();
            var newId = manager.CreateSessionID(System.Web.HttpContext.Current);
            bool redirected;
            bool isAdded;
            manager.SaveSessionID(System.Web.HttpContext.Current, newId, out redirected, out isAdded);
        }

        internal int GetCommunityIdFromCoockie(out int marketId)
        {
            var cCommId = CookieManager.LastCommunityView;
            var cMarket = CookieManager.LastMarketView;
            marketId = string.IsNullOrWhiteSpace(cMarket) ? -1 : Convert.ToInt32(cMarket);
            var commId = string.IsNullOrWhiteSpace(cCommId) ? -1 : Convert.ToInt32(cCommId);

            return commId;
        }
    }
}
