﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Bdx.Web.Api.Objects;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Route;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers;
using Nhs.Library.Helpers.Lead;
using Nhs.Library.Proxy;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Domain.Model.Web.Partials;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Extensions;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class FreeBrochureController : BaseDetailController
    {
        #region Members
        private readonly IListingService _listingService;
        private readonly ICommunityService _communityService;
        private readonly IMarketService _marketService;
        private readonly IStateService _stateService;
        private readonly IPartnerService _partnerService;
        private readonly IUserProfileService _userService;
        private readonly IPathMapper _pathMapper;
        private readonly IUserProfileService _userProfileService;
        private readonly IBrandService _brandService;
        private readonly ILeadService _leadService;
        private readonly IProCrmService _proCrmService;
        private readonly IApiService _apiService;
        #endregion

        #region Constructor

        public FreeBrochureController(ILeadService leadService, IListingService listingService,
            ICommunityService communityService, IMarketService marketService, IStateService stateService,
            IPartnerService partnerService, IUserProfileService userService, IPathMapper pathMapper,
            IUserProfileService userProfileService, IBrandService brandService, IProCrmService proCrmService, IApiService apiService)
            : base(communityService, listingService, pathMapper)
        {
            _proCrmService = proCrmService;
            _listingService = listingService;
            _communityService = communityService;
            _marketService = marketService;
            _stateService = stateService;
            _partnerService = partnerService;
            _userService = userService;
            _pathMapper = pathMapper;
            _userProfileService = userProfileService;
            _brandService = brandService;
            _leadService = leadService;
            _apiService = apiService;
        }

        #endregion

        #region Actions

        #region FreeBrochure on Page

        public virtual ActionResult MyBrochure(RecommendedCommunitiesViewModal model)
        {
            string creditScoreLink1 = "", creditScoreLink2 = "";
            _communityService.GetCreditScoreLinks(ref creditScoreLink1, ref creditScoreLink2);
            model.CreditScoreLink1 = creditScoreLink1;
            model.CreditScoreLink2 = creditScoreLink2;

            var f = _partnerService.GetPartner(NhsRoute.PartnerId).AllowRegistration.ToBool();
            var leadInfo = WrapLeadGeneration(model.IsPageCommDetail ? LeadType.Community : LeadType.Home, model, f, model.MatchEmail);
            model.RequestUniqueKey = leadInfo.RequestUniqueKey;

            var comList = leadInfo.CommunityList.Contains("|")
                ? leadInfo.CommunityList.Split(',').Select(p => p.Split('|')[1].ToType<int>()).ToList()
                : new List<int>();

            var googleHelper = new GoogleAnalyticEcommerce(_communityService.GetCommunity(model.CommunityId),
                _communityService.GetCommunities(comList).ToList(), leadInfo.RequestUniqueKey,
                _partnerService.GetPartner(NhsRoute.PartnerId).PartnerName);
            var google = googleHelper.GetScript();

            model.UserAlreadyExists = _userService.LogonNameExists(model.MailAddress, NhsRoute.BrandPartnerId);
            model.Google = google;
            var view = model.IsComunityDetailV2 ? NhsMvc.Default.Views.Common.LeadFormV2.LeadConfirmation : NhsMvc.Default.Views.Common.LeadForm.LeadConfirmation;
            return PartialView(view, model);
        }

        protected void ValidateForm(IDetailViewModel model)
        {
            var regex = new Regex(StringHelper.NameLastnameRegex);

            if (string.IsNullOrEmpty(model.Name) || !regex.IsMatch(model.Name))
                ModelState.AddModelError("Name", LanguageHelper.PleaseProvideFirstAndLastName);

            regex = new Regex(StringHelper.EmailRegexfull);

            if (string.IsNullOrEmpty(model.MailAddress) ||
                !regex.IsMatch(model.MailAddress))
                ModelState.AddModelError("MailAddress", LanguageHelper.EmailIsBlankOrInvalid);

            regex = new Regex(StringHelper.ZipRegex);

            if (!string.IsNullOrEmpty(model.UserPhoneNumber) && !CommonUtils.IsPhone(model.UserPhoneNumber))
                ModelState.AddModelError("Phone", LanguageHelper.InvalidPhone);

            if (!model.LiveOutside)
            {
                if (string.IsNullOrEmpty(model.UserPostalCode))
                    ModelState.AddModelError("Zip", LanguageHelper.MSG_ZipCodeRequired);
                else if (!string.IsNullOrEmpty(model.UserPostalCode) &&
                         !regex.IsMatch(model.UserPostalCode))
                    ModelState.AddModelError("ZipCode", LanguageHelper.MSG_ACCOUNT_INVALID_ZIP);
            }
        }

        protected LeadInfo WrapLeadGeneration(string leadType, IDetailViewModel model, bool showRegInfo, bool matchEmail = false)
        {
            //Setup vm properties
            model.ShowRegInfoDetail = showRegInfo;

            var community = _communityService.GetCommunity(model.CommunityId);

            //Populate Basic Lead Information
            var leadInfo = GetBasicLeadInfo(leadType, model, community.MarketId);
            UserSession.HasUserOptedIntoMatchMaker = true;

            if (string.IsNullOrEmpty(model.BcType))
            {
                model.BcType = community.BCType;
            }
            if (model.BcType.ToLower() == BuilderCommunityType.ComingSoon.ToLower() && NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
            {
                model.Comments = "COMING SOON LEAD! This consumer would like to be added to your Coming Soon community interest list.";
                model.SendAlerts = false;
                leadInfo.SuppressEmail = true;
                leadInfo.LeadComments = model.Comments;
            }

            //Reco Leads
            var recoComms = new List<ApiRecoCommunityResult>();

            if (model.SendAlerts && !model.NewSearchRCC)
            {
                recoComms = GetRecoComms<List<ApiRecoCommunityResult>>(model.CommunityId, model.MarketId,
                    leadInfo.LeadType, model.PlanId, model.SpecId, model.MailAddress);
                //This sets RecoCount

                //Assign reco comms count before sending source lead
                leadInfo.RecoCount = recoComms.Count;
            }

            if (!model.SendAlerts)
                leadInfo.ShowMatchingCommunities = true;

            if (matchEmail)
                leadInfo.ShowMatchingCommunities = false;
            //Generate Lead, add to planner and get comment back - CANNOT CHANGE THIS SEQUENCE OF GENERATING LEAD FIRST AND THEN RCMs
            var comments = GenerateLeadAndAddToPlanner(leadInfo);

            //Generate RCM leads
            if (model.SendAlerts && !model.NewSearchRCC)
                GenerateRcmLeads(recoComms, community, leadInfo, model, comments);

            SetupViewModel(model, recoComms, leadInfo);
            return leadInfo;
        }

        protected LeadInfo GetBasicLeadInfo(string leadType, IDetailViewModel model, int marketId)
        {
            var leadInfo = new LeadInfo();

            if (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser)
            {
                leadInfo = LeadUtil.GetLeadInfoFromSession();
            }
            else
            {
                leadInfo.LeadUserInfo.Email = UserSession.UserProfile.LogonName = model.MailAddress;
                leadInfo.LeadUserInfo.FirstName = UserSession.UserProfile.FirstName = model.Name.FirstPart(" ");
                leadInfo.LeadUserInfo.LastName = UserSession.UserProfile.LastName = model.Name.LastPart(" ");
                leadInfo.LeadUserInfo.PostalCode = UserSession.UserProfile.PostalCode = model.UserPostalCode;
                UserSession.UserProfile.GenericGUID = _userProfileService.GetUserIdByEmail(model.MailAddress);

                if (String.IsNullOrEmpty(UserSession.UserProfile.GenericGUID))
                    UserSession.UserProfile.GenericGUID = Guid.NewGuid().ToString();
            }

            leadInfo.LeadUserInfo.Phone = UserSession.UserProfile.DayPhone = model.UserPhoneNumber;
            leadInfo.LeadType = leadType;
            leadInfo.MarketId = marketId;
            leadInfo.LeadComments = (model.LiveOutside)
                                        ? string.Concat("International lead - ", model.Comments)
                                        : model.Comments;
            leadInfo.BuilderId = model.BuilderId;
            leadInfo.CommunityList = model.CommunityId.ToType<string>();
            leadInfo.LeadUserInfo.SessionId = UserSession.SessionId;
            leadInfo.RequestUniqueKey = string.Format("{0}-c{1}-", Guid.NewGuid(), leadInfo.CommunityList);
            leadInfo.Referer = UserSession.Refer;
            leadInfo.LeadAction = LeadAction.FreeBrochure;
            leadInfo.FromPage = "onpagecd";

            if (leadType == LeadType.Home)
            {
                leadInfo.FromPage = "onpagehd"; //Overwrite with HD
                if (model.IsPlan)
                {
                    leadInfo.PlanList = model.PlanId.ToType<string>();
                    leadInfo.RequestUniqueKey += "p" + leadInfo.PlanList;
                }
                else
                {
                    leadInfo.SpecList = model.SpecId.ToType<string>();
                    leadInfo.RequestUniqueKey += "s" + leadInfo.SpecList;
                }
            }
            return leadInfo;
        }

        protected T GetRecoComms<T>(int communityId, int marketId, string leadType, int planId, int specId, string mailAddress, int maxRecs = 0) where T : new()
        {
            var recoComms = new T();
            switch (leadType)
            {
                case LeadType.Community:
                    recoComms = _communityService.GetRecommendedCommunities<T>(mailAddress, NhsRoute.PartnerId, communityId, 0, 0, maxRecs);
                    break;

                case LeadType.Home:
                    recoComms = _communityService.GetRecommendedCommunities<T>(mailAddress, NhsRoute.PartnerId, 0, specId, planId, maxRecs);
                    break;
            }

            return recoComms;
        }

        protected string GenerateLeadAndAddToPlanner(LeadInfo leadInfo)
        {
            //Generate Lead
            var commentsAppendedText = LeadUtil.GenerateLead(leadInfo);

            // Save homes or communities to planner
            if (leadInfo.LeadType == LeadType.Community || leadInfo.LeadType == LeadType.Home)
                LeadUtil.AddToPlannerFromLeads((Profile)UserSession.UserProfile, leadInfo.LeadType,
                                               leadInfo.CommunityList, leadInfo.BuilderId, leadInfo.PlanList,
                                               leadInfo.SpecList, leadInfo.RequestUniqueKey);

            return commentsAppendedText;
        }

        protected void SetupViewModel(IDetailViewModel model, List<ApiRecoCommunityResult> recoComms, LeadInfo leadInfo)
        {
            model.ShowSaveToAccount = UserSession.UserProfile.ActorStatus != WebActors.ActiveUser;
            model.MailAddress = UserSession.UserProfile.ActorStatus == WebActors.ActiveUser
                                    ? UserSession.UserProfile.Email
                                    : model.MailAddress;
            model.ConversionIFrameSource = TrackingScriptsHelper.GetConversionTrackerUrl(model,
                                                                                         leadInfo.RequestUniqueKey,
                                                                                         String.Join(",",
                                                                                                     recoComms.Select(
                                                                                                         p =>
                                                                                                         p.CommunityId)));
        }

        protected void GenerateRcmLeads(IList<ApiRecoCommunityResult> recoComms, Community community, LeadInfo leadInfo,
                                        IDetailViewModel model, string comments)
        {
            var leadComments = "";
            //Now, generate RCMs if any
            if (recoComms.Count > 0)
            {
                if (leadInfo.LeadType == LeadType.Home)
                //Lead type is still COM or HM till this point. In the SendRecommendedProperties method below, is when we change the leadtype to RCM.
                {
                    IPlan plan;
                    ISpec spec = null;

                    if (model.IsPlan)
                    {
                        plan = _listingService.GetPlan(model.PlanId);
                        leadInfo.SourcePlanId = model.PlanId;
                    }
                    else
                    {
                        spec = _listingService.GetSpec(model.SpecId);
                        plan = spec.Plan;
                        leadInfo.SourceSpecId = model.SpecId;
                    }

                    leadComments = TemplateWordingHelper.GetMessageHome(community, plan, spec);
                }
                else if (leadInfo.LeadType == LeadType.Community)
                {
                    leadInfo.SourceCommunityId = model.CommunityId;
                    leadComments = TemplateWordingHelper.GetMessageCommunity(community);
                }

                //Removed User Comments, beacuse the issue 48254 --> User Comments Showing up in RCM Leads
                leadInfo.LeadComments = (comments + (model.LiveOutside ? "International lead - " : ""));
                leadInfo.LeadComments += leadComments;
                _communityService.SendRecommendedProperties(leadInfo, recoComms, community);
            }
        }
        #endregion


        #region Recommended Communities
        public virtual ActionResult RecommendedCommunities(bool isCommunityPage, string requestUniqueKey, int communityId, string email,
                                   int planid, int specid)
        {
            var model = GetRecommendedCommunitiesModel(isCommunityPage, requestUniqueKey, communityId, email, planid, specid, true);
            return PartialView(NhsMvc.Default.Views.FreeBrochure.RecommendedCommunities, model);
        }

        public virtual ActionResult ShowRecomendations(int requestid, int community)
        {
            var leadProperty = _leadService.GetCommsHomesForRequest(requestid).FirstOrDefault();
            var model = GetRecommendedCommunitiesModel(true, leadProperty.UniqueKey, community, leadProperty.EmailAddress, leadProperty.PlanId, leadProperty.SpecId, false, leadProperty.FirstName, leadProperty.LastName, leadProperty.PostalCode);
            return View(model);
        }

        public virtual ActionResult ShowRecomendationsPage(int community, string communityList, string email, string firstName, string lastName, string postalCode)
        {
            var model = GetRecommendedCommunitiesModel(true, "", community, email, 0, 0, false, firstName, lastName, postalCode);
            model.RecoCommunityList = communityList;
            model.FromMatchEmail = true;
            return View(NhsMvc.Default.Views.FreeBrochure.ShowRecomendations, model);
        }

        [HttpPost]
        public virtual JsonResult RecommendedCommunities(LeadInfoViewModel model, string requestUniqueKey, bool isModal, bool fromMatchEmail)
        {
            var commList = model.RecoComms.GetSelectCommsAndBuilders();
            var commToReco = model.RecoComms.GetSelectCommunities();

            var conversionIFrameSource = TrackingScriptsHelper.GetConversionTrackerDirectSuggested(model.RecoComms.Count(), commToReco.Count());
            var lead = LeadUtil.GetLeadInfoFromSession();
            lead.RequestUniqueKey = string.Format("{0}-c{1}-", Guid.NewGuid(), model.CommunityId);
            lead.MarketId = model.MarketId;
            lead.CommunityList = commList;
            lead.BuilderId = model.BuilderId;
            lead.LeadType = LeadType.Market;
            lead.LeadAction = isModal ? LeadAction.RecommendedModal : LeadAction.RecommendedOnPage;
            lead.LeadUserInfo.SessionId = UserSession.SessionId;
            lead.LeadUserInfo.Email = model.Email;
            lead.LeadUserInfo.PostalCode = model.Zip;
            lead.LeadUserInfo.Phone = model.Phone;
            lead.LeadUserInfo.LastName = model.LastName;
            lead.LeadUserInfo.FirstName = model.FirstName;
            lead.Referer = UserSession.Refer;
            lead.SourceCommunityId = model.CommunityId;

            var community = _communityService.GetCommunity(model.CommunityId);

            string leadComments;
            var isComm = true;
            IPlan plan = null;
            ISpec spec = null;
            if (model.PlanId > 0 || model.SpecId > 0)
            {
                if (model.SpecId == 0)
                    plan = _listingService.GetPlan(model.PlanId);
                else
                    spec = _listingService.GetSpec(model.SpecId);
                isComm = false;
                leadComments = TemplateWordingHelper.GetMessageHome(community, plan, spec);
            }
            else
                leadComments = TemplateWordingHelper.GetMessageCommunity(community);


            lead.LeadComments += leadComments;
            LeadUtil.GenerateLead(lead);
            var leadType = isModal ? GoogleGaConst.RcmRecComModal : (fromMatchEmail ? GoogleGaConst.RcmMatchEmail : GoogleGaConst.RcmEmail);

            //var googleHelper = new GoogleHelper(_communityService, _listingService);
            //googleHelper.CreateGoogleAnalyticsRecommendedCommunityLeads(community.CommunityId, requestUniqueKey, commToReco, isComm, isModal, leadType);
            var googleHelper = new GoogleAnalyticEcommerce(community, _communityService.GetCommunities(commToReco).ToList(), requestUniqueKey, _partnerService.GetPartner(NhsRoute.PartnerId).PartnerName);
            var google = googleHelper.GetScript();
            
            var data = new
                {
                    google,
                    total = model.RecoComms.Count(),
                    select = commToReco.Count(),
                    iframe =
                        "<iframe src='" + conversionIFrameSource +
                        "' width='0' height='0' frameborder='0' scrolling='no' rel='nofollow'></iframe>"
                };

            return Json(data, JsonRequestBehavior.AllowGet);
        }


        private RecommendedCommunitiesViewModal GetRecommendedCommunitiesModel(bool isCommunityPage, string requestUniqueKey, int communityId, string email,
                                   int planid, int specid, bool isModal, string firstName = null, string lastName = null, string postalCode = null)
        {
            var community = _communityService.GetCommunity(communityId);
            var model = new RecommendedCommunitiesViewModal
            {
                IsPageCommDetail = isCommunityPage,
                IsCommunityPage = isCommunityPage,
                IsModal = isModal,
                HeadlineText = RouteParams.HeadlineText.Value<string>(),
                SubheadText = RouteParams.SubheadText.Value<string>(),
                SessionId =
                    !string.IsNullOrEmpty(RouteParams.SessionId.Value<string>())
                        ? RouteParams.SessionId.Value<string>()
                        : UserSession.SessionId,
                Refer = RouteParams.Refer.Value<string>(),
                UserPostalCode = string.IsNullOrWhiteSpace(postalCode) ? RouteParams.PostalCode.Value<string>() : postalCode,
                RequestUniqueKey = requestUniqueKey,
                MarketId = community.MarketId,
                MarketName = community.Market.MarketName,
                StateAbbr = community.StateAbbr,
                CommunityId = community.CommunityId,
                PlanId = planid,
                SpecId = specid,
                BuilderId = community.BuilderId,
                CommunityName = community.CommunityName,
                UserPhoneNumber = RouteParams.PhoneNumber.Value<string>(),
                Email = string.IsNullOrWhiteSpace(email) ? RouteParams.Email.Value<string>() : email,
                FirstName = string.IsNullOrWhiteSpace(firstName) ? RouteParams.FirstName.Value<string>() : firstName,
                LastName = string.IsNullOrWhiteSpace(lastName) ? RouteParams.LastName.Value<string>() : lastName,
                DefaultChecked = RouteParams.DefaultChecked.Value(true),
                MaxRecs = string.IsNullOrWhiteSpace(RouteParams.MaxRecs.Value<string>()) ? "0" : RouteParams.MaxRecs.Value<string>()
            };
            if (isModal)
            {
                var maxRecs = RouteParams.MaxRecs.Value<int>();
                model = GetRecommendedCommunitiesItemsForModel(model, isCommunityPage, communityId, planid, specid,
                                                               email,
                                                               model.DefaultChecked, maxRecs,
                                                               RouteParams.CommunityList.Value<string>());

                var matchEmail = RouteParams.MatchEmail.Value<bool>();
                if (matchEmail && model.RecoComms.Any())
                {
                    _leadService.InsertPresenteComunitys(model.CommunityId, string.Join(",", model.RecoComms.Select(p => p.CommunityId)), model.Email, model.FirstName, model.LastName, model.UserPostalCode, NhsRoute.PartnerId, model.MarketId);
                }
            }

            return model;
        }

        [HttpGet]
        public virtual ActionResult GetRecommendedCommunitiesItems(bool isCommunityPage, int communityId, int planid,
                                                                   int specid, string email, bool defaultChecked,
                                                                   int maxRecs, string recoCommunityList)
        {
            var model = GetRecommendedCommunitiesItemsForModel(new RecommendedCommunitiesViewModal(), isCommunityPage,
                                                               communityId, planid, specid, email, defaultChecked,
                                                               maxRecs, recoCommunityList);
            return PartialView(NhsMvc.Default.Views.CommunityRecommendations.PartialItem, model);
        }

        private RecommendedCommunitiesViewModal GetRecommendedCommunitiesItemsForModel(RecommendedCommunitiesViewModal model, bool isCommunityPage, int communityId, int planid,
                                                                   int specid, string email, bool defaultChecked,
                                                                   int maxRecs, string recoCommunityList)
        {

            model.DefaultChecked = defaultChecked;
            if (!string.IsNullOrWhiteSpace(recoCommunityList))
            {
                var comList = recoCommunityList.Split(',').Select(p => p.ToType<int>()).ToList();
                model.RecoComms = new List<ApiRecoCommunityResultEx>();
                foreach (var commid in comList)
                {
                    var currentComm = _communityService.GetCommunity(commid);
                    var communityResult = new ApiRecoCommunityResultEx
                    {
                        BuilderId = currentComm.BuilderId,
                        CommunityId = currentComm.CommunityId,
                        CommunityName = currentComm.CommunityName,
                        City = currentComm.City,
                        State = currentComm.Market.StateAbbr,
                        PostalCode = currentComm.PostalCode,
                        PriceHigh = currentComm.PriceHigh.ToType<int>(),
                        PriceLow = currentComm.PriceLow.ToType<int>()
                    };

                    var propertyMediaLinks = _communityService.GetMediaPlayerObjects(currentComm.CommunityId).ToList();
                    communityResult.CommunityImageThumbnail = propertyMediaLinks.Any()
                                            ? Configuration.ResourceDomain + propertyMediaLinks.First().Url
                                            : string.Empty;

                    model.RecoComms.Add(communityResult);
                }
            }
            else
            {
                var leadType = isCommunityPage ? LeadType.Community : LeadType.Home;
                var community = _communityService.GetCommunity(communityId);
                var recoComms = GetRecoComms<List<ApiRecoCommunityResultEx>>(community.CommunityId, community.MarketId, leadType, planid, specid, email,
                                             maxRecs);
                model.RecoComms = recoComms;
            }

            var brandIds = model.RecoComms.Select(x => x.BrandId).ToList();
            var brands = _brandService.GetBrandsByIds(brandIds, NhsRoute.BrandPartnerId);

            foreach (var result in model.RecoComms)
            {
                var brand = brands.FirstOrDefault(b => b.BrandId == result.BrandId);
                if (brand != null)
                    result.BrandImageThumbnail = result.GetBrandThumbnail(brand, true);
            }
            return model;
        }
        /// <summary>
        /// This Actions is call from the Reco Email\
        /// 63891 Follow-Up Recommendations Email
        /// </summary>
        /// <param name="community"></param>
        /// <param name="communityList"></param>
        /// <param name="email"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="postalCode"></param>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult GetAllBrochuresPage(int community, string communityList, string email,
                                                        string firstName, string lastName, string postalCode)
        {
            var sourceCommunity = _communityService.GetCommunity(community);
            var lead = LeadUtil.GetLeadInfoFromSession();
            lead.RequestUniqueKey = string.Format("{0}-c{1}-", Guid.NewGuid(), community);
            lead.MarketId = sourceCommunity.MarketId;
            lead.BuilderId = sourceCommunity.BuilderId;
            lead.CommunityList = communityList;
            lead.LeadType = LeadType.Market;
            lead.LeadAction = LeadAction.RecommendedThankYouPage;
            lead.LeadUserInfo.SessionId = UserSession.SessionId;
            lead.LeadUserInfo.Email = email;
            lead.LeadUserInfo.LastName = lastName;
            lead.LeadUserInfo.PostalCode = postalCode;
            lead.LeadUserInfo.FirstName = firstName;
            lead.Referer = UserSession.Refer;
            lead.SourceCommunityId = community;
            lead.LeadComments = TemplateWordingHelper.GetMessageCommunity(sourceCommunity);
            LeadUtil.GenerateLead(lead);

            var reccommList = communityList.Split(',').Select(p => p.Split('|')[1].ToType<int>()).ToList();

            //var googleHelper = new GoogleHelper(_communityService, _listingService);
            //googleHelper.CreateGoogleAnalyticsRecommendedCommunityLeads(sourceCommunity.CommunityId,  lead.RequestUniqueKey, reccommList, true, false, GoogleGaConst.RcmMatchEmail);
            
            var googleHelper = new GoogleAnalyticEcommerce(sourceCommunity, _communityService.GetCommunities(reccommList).ToList(), lead.RequestUniqueKey, _partnerService.GetPartner(NhsRoute.PartnerId).PartnerName);
            
            var conversionIFrameSource =
                TrackingScriptsHelper.GetConversionTrackerOnPageRecoCommunity(reccommList.Count,
                                                                                      reccommList.Count);
            var model = new RecommendedCommunitiesViewModal
                {
                    MarketId = sourceCommunity.MarketId,
                    MarketName = sourceCommunity.Market.MarketName,
                    UrlIframe = conversionIFrameSource,
                    Google = googleHelper.GetScript()
                };

            return View(NhsMvc.Default.Views.CommunityRecommendations.ShowConfirmation, model);
        }

        #endregion

        /// <summary>
        /// Shows the brochure interstitial page and redirects to getbrochure.
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult ShowInterstitial()
        {
            var requestUniqueKey = RouteParams.RequestUniqueKey.Value();
            var nextPageUrl = "";
            var numberOfSearch = RouteParams.NumberOfSearch.Value<int>();
            var communityId = RouteParams.CommunityId.Value<int>() | RouteParams.Community.Value<int>();
            var planId = RouteParams.PlanId.Value<int>();
            var specId = RouteParams.SpecId.Value<int>();
            var viewModel = new LeadViewModels.BrochureInterstitialViewModel
           {
               RefreshDelay = 5,
               Globals = { PartnerInfo = _partnerService.GetPartner(NhsRoute.PartnerId) }
           };

            if (Configuration.OnDemandBrochureGeneration)
            {
                var requestInfo = _leadService.GetCommsHomesForRequestBySourceRequestKey(requestUniqueKey);
                if (!requestInfo.Any())
                {
                    numberOfSearch++;
                    viewModel.NumberOfSearch = numberOfSearch;
                    var @params = new List<RouteParam>
                        {
                            new RouteParam(RouteParams.RequestUniqueKey, requestUniqueKey, RouteParamType.QueryString),
                            new RouteParam(RouteParams.NumberOfSearch, numberOfSearch, RouteParamType.QueryString)
                        };

                    if (communityId > 0)
                        @params.Add(new RouteParam(RouteParams.CommunityId, communityId, RouteParamType.QueryString));


                    if (planId > 0)
                        @params.Add(new RouteParam(RouteParams.PlanId, planId, RouteParamType.QueryString));


                    if (specId > 0)
                        @params.Add(new RouteParam(RouteParams.SpecId, specId, RouteParamType.QueryString));

                    nextPageUrl = @params.ToUrl(Pages.BrochureInterstitial);
                }
                else
                {
                    LeadEmailProperty rInfo = null;
                    if (communityId > 0)
                        rInfo = requestInfo.FirstOrDefault(p => p.CommunityId == communityId);

                    if (planId > 0)
                        rInfo = requestInfo.FirstOrDefault(p => p.PlanId == planId);

                    if (specId > 0)
                        rInfo = requestInfo.FirstOrDefault(p => p.SpecId == specId);

                    if (rInfo != null)
                    {
                        nextPageUrl = ViewBrochureHelper.ViewBrochureLink(rInfo.FirstName, rInfo.RequestItemId,
                                                                          rInfo.EmailAddress, "");

                        return RedirectPermanent(nextPageUrl);
                    }
                    viewModel.NumberOfSearch = 5;
                }
            }
            else
            {
                nextPageUrl = RouteParams.NextPage.Value();
            }
            viewModel.NextPageUrl = nextPageUrl;
            return View(NhsMvc.Default.Views.FreeBrochure.ShowInterstitial, viewModel);
        }

        #region Tracker

        public virtual ActionResult TrackConversion()
        {
            var viewModel = new LeadViewModels.ConversionTrackerViewModel();
            var metaRegistrar = new MetaRegistrar(_pathMapper);
            var metas = metaRegistrar.GetRobotsMeta("noodp, noydir, noindex");
            OverrideDefaultMeta(viewModel, metas);


            if (RouteParams.GoogleAnalytics.Value<bool>())
            {
                //var googleHelper = new GoogleHelper(_communityService, _listingService);
                //googleHelper.CreateGoogleAnalyticsDirectLead(RouteParams.RequestUniqueKey.Value(), RouteParams.Community.Value<Int32>(), RouteParams.SpecId.Value<Int32>(), RouteParams.PlanId.Value<Int32>(), RouteParams.IsModal.Value<bool>());
                var googleHelper = new GoogleAnalyticEcommerce(_communityService.GetCommunity(RouteParams.Community.Value<Int32>()), null, RouteParams.RequestUniqueKey.Value(), _partnerService.GetPartner(NhsRoute.PartnerId).PartnerName);
                viewModel.Globals.Google = googleHelper.GetScript();
            }
            return View(NhsMvc.Default.Views.FreeBrochure.TrackConversion, viewModel);
        }

        public virtual ActionResult ThankYouTracker()
        {
            var viewModel = new LeadViewModels.ConversionTrackerViewModel();
            var metaRegistrar = new MetaRegistrar(_pathMapper);
            var metas = metaRegistrar.GetRobotsMeta("noodp, noydir, noindex");
            OverrideDefaultMeta(viewModel, metas);
            return View(NhsMvc.Default.Views.FreeBrochure.ThankYouTracker, viewModel);
        }
        #endregion

        #region Send Custom Brochure

        public virtual ActionResult SendCustomBrochure()
        {
            var viewModel = new LeadViewModels.SendCustomBrochure
            {
                PlanId = RouteParams.PlanId.Value<Int32>(),
                SpecId = RouteParams.SpecId.Value<Int32>(),
                CommunityId =
                    RouteParams.Community.Value<Int32>() | RouteParams.CommunityList.Value<Int32>() |
                    RouteParams.CommunityId.Value<Int32>(),
                BuilderId = RouteParams.Builder.Value<Int32>() | RouteParams.BuilderList.Value<Int32>(),
                MarketId = RouteParams.MarketId.Value<Int32>() | RouteParams.Market.Value<Int32>(),
                MailAddress = RouteParams.Email.Value<String>(),
                LeadType = RouteParams.LeadType.Value<String>(),
                IsMvc = RouteParams.IsMvc.Value<Boolean>(),
                IsBilled = RouteParams.IsBilled.Value<Boolean>(),
                IsMultiBrochure = RouteParams.IsMultiBrochure.Value<Boolean>(),
                Email = UserSession.UserProfile.Email,
                FirstName = UserSession.UserProfile.FirstName,
                LastName = UserSession.UserProfile.LastName,
                SessionId = UserSession.SessionId
            };

            if (viewModel.IsMultiBrochure)
            {
                var multiBrochureItem = UserSession.MultiBrochureList.BrochureList.FirstOrDefault();
                if (multiBrochureItem != null)
                    viewModel.CommunityId = multiBrochureItem.CommunityId;
            }

            var agent = _userProfileService.GetUserByLogonName(UserSession.UserProfile.Email, NhsRoute.PartnerId);
            if (agent != null)
            {
                var agentBrochureTemplate = agent.AgentBrochureTemplates.FirstOrDefault();
                if (agentBrochureTemplate != null)
                    viewModel.HavAgentPhotoOrAgencyLogo = !string.IsNullOrEmpty(agentBrochureTemplate.AgencyLogo) ||
                                                          !string.IsNullOrEmpty(agentBrochureTemplate.ImageUrl);
            }

            var masterClientList = _proCrmService.GetRecentUsedClients(UserSession.UserProfile.UserID);

            for (var i = 0; i < masterClientList.Count(); i++)
            {
                viewModel.ShowClearNames = true;
                viewModel.ClientsEmails.Add(new Client
                {
                    Index = i,
                    ClientId = masterClientList[i].ClientId,
                    Name = string.Format("{0} {1}", masterClientList[i].FirstName, masterClientList[i].LastName),
                    Email = string.Join("; ", masterClientList[i].ClientEmails.Select(p => p.Email))
                });
            }

            for (var i = viewModel.ClientsEmails.Count(); i < 3; i++)
                viewModel.ClientsEmails.Add(new Client { Index = i });


            if (viewModel.IsMvc)
                return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.FreeBrochure.SendCustomBrochure, viewModel);

            return View(viewModel);
        }

        [HttpPost]
        public virtual ActionResult SendCustomBrochure(LeadViewModels.SendCustomBrochurePost model)
        {
            var inputClients = model.ClientsEmails.Where(p => !string.IsNullOrWhiteSpace(p.Email) || !string.IsNullOrWhiteSpace(p.Name)).AsQueryable();
            var xml = new StringBuilder();
            xml.Append("<ClientsInfo>");
            foreach (var client in inputClients)
            {
                foreach (var emails in client.Email.Trim(';').Split(';'))
                {
                    xml.AppendFormat("<Client Name=\"{0}\" Email=\"{1}\"/>", client.Name, emails.Trim());
                }
            }

            xml.Append("</ClientsInfo>");

            bool isPlan = model.SpecId == 0;

            var logger = new ImpressionLogger
            {
                CommunityId = model.CommunityId,
                BuilderId = model.BuilderId,
                PartnerId = NhsRoute.PartnerId.ToType<String>(),
                Refer = UserSession.Refer
            };

            var lead = new LeadInfo
            {
                LeadUserInfo =
                    {
                        Email = model.Email,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        SessionId = model.SessionId
                    }
            };

            var script = new StringBuilder();
            lead.LeadType = model.LeadType == LeadType.Community ? LeadType.Community : LeadType.Home;

            var addNewClientsViewModel = new AddNewClientsViewModel
            {
                IsMvc = model.IsMvc,
                PlanId = model.PlanId,
                SpecId = model.SpecId,
                IsMultiBrochure = model.IsMultiBrochure
            };

            //var googleHelper = new GoogleHelper(_communityService, _listingService);

            if (!model.IsMultiBrochure)
            {
                lead.RequestUniqueKey = string.Format("{0}-c{1}-", Guid.NewGuid(), model.CommunityId);
                lead.MarketId = model.MarketId;
                lead.CommunityList = model.CommunityId.ToType<String>();
                addNewClientsViewModel.CommunityId = model.CommunityId;
                lead.BuilderId = model.BuilderId;
                if (lead.LeadType == LeadType.Home)
                {
                    if (isPlan)
                    {
                        logger.AddPlan(model.PlanId);
                        lead.PlanList = model.PlanId.ToType<String>();
                        lead.RequestUniqueKey += "p" + lead.PlanList;
                    }
                    else
                    {
                        logger.AddSpec(model.SpecId);
                        lead.SpecList = model.SpecId.ToType<String>();
                        lead.RequestUniqueKey += "s" + lead.SpecList;
                    }
                }
                Mvc.Domain.Model.Web.Partner partner = _partnerService.GetPartner(NhsRoute.PartnerId);
                string partnerName = (partner != null) ? partner.PartnerName : string.Empty;
                //googleHelper.CreateGoogleAnalyticsDirectLeadPro(lead.RequestUniqueKey, model.CommunityId, model.SpecId, model.PlanId);
                var googleHelper = new GoogleAnalyticEcommerce(_communityService.GetCommunity(model.CommunityId), null, lead.RequestUniqueKey, partnerName);
                script.AppendLine(googleHelper.GetScript());
            }
            lead.LeadAction = LeadAction.FreeBrochure;
            lead.Referer = UserSession.Refer;
            lead.LeadUserInfo.SessionId = UserSession.SessionId;
            lead.ClientsInfo = xml.ToString();
            lead.IsBilled = model.IsBilled;
            lead.AgentMail = UserSession.UserProfile.Email;
            lead.AgentName = UserSession.UserProfile.FirstName + " " + UserSession.UserProfile.LastName;

            if (model.IsMultiBrochure)
            {
                lead.RequestUniqueKey = string.Format("{0}-mb", Guid.NewGuid());
                var multiBrochureList = UserSession.MultiBrochureList;
                addNewClientsViewModel.ListingIds = string.Join(",",
                                                                UserSession.MultiBrochureList.BrochureList.Select(
                                                                    p => p.CommunityId));

                var googleHelper = new GoogleHelper(_communityService, _listingService);
                googleHelper.CreateGoogleAnalyticsRecommendedCommunityLeadsMultiSelect(lead.RequestUniqueKey, UserSession.MultiBrochureList.BrochureList.Select(p => p.CommunityId).ToList(), true);
                
                script.AppendLine(googleHelper.ToString());

                lead.MarketId = multiBrochureList.Market;
                var multiBrochureListGroup = multiBrochureList.BrochureList.GroupBy(p => p.IsBilled);

                foreach (var item in multiBrochureListGroup)
                {
                    var multiBrochureItem = item.FirstOrDefault();
                    if (multiBrochureItem != null)
                    {
                        var communityId = multiBrochureItem.CommunityId;
                        var commList = string.Join(",", item.Select(p => p.CommunityId));
                        lead.RequestUniqueKey = string.Format("{0}-c{1}-", Guid.NewGuid(), communityId);
                        lead.CommunityList = commList;
                    }
                    lead.IsBilled = item.Key;
                    LeadUtil.GenerateLead(lead);
                }
                //lead.BuilderId = builderId;
            }
            else
                LeadUtil.GenerateLead(lead);

            logger.LogView(LogImpressionConst.SendBrochure);

            var masterClients = _proCrmService.GetMyClientList(UserSession.UserProfile.UserID);

            if (masterClients == null || !masterClients.Any())
            {
                foreach (var client in inputClients)
                {
                    client.Email = client.Email.Trim(';');
                }

                addNewClientsViewModel.NewClients = inputClients.Where(p => !string.IsNullOrWhiteSpace(p.Email)).ToList();
                addNewClientsViewModel.ClientSourceList.AddRange(inputClients.Select(p => new SelectListItem { Value = p.Name, Text = p.Name }));
            }
            else
            {

                #region Task
                var clientIds = inputClients.Where(p => p.ClientId > 0).Select(p => p.ClientId).Distinct();
                var task = GetSendCustomBrochure(model);

                //************************************************************************
                //Update the TASK and add the clients
                _proCrmService.UpdateClients(clientIds, task);
                //************************************************************************


                #endregion

                var masterClientEmails = from client in masterClients from email in client.ClientEmails select email.Email.ToLowerCase();

                // List of client emails for current agent
                foreach (var client in inputClients)
                {
                    client.Email = client.Email.Trim(';');
                    foreach (var email in client.Email.Split(';').Distinct().Where(email => masterClientEmails.Any(p => p.Trim() == email.Trim())))//Exclude emails that are aleady in the client DB --masterClients
                    {
                        client.Email = client.Email.Replace(email, "").Replace("; ;", "; ").Trim(';');
                    }

                    if (!string.IsNullOrWhiteSpace(client.Email))
                    {
                        if (client.ClientId == 0)
                        {
                            var clientEmail = addNewClientsViewModel.NewClients.FirstOrDefault(p => p.Name.ToLowerCase() == client.Name.ToLowerCase());
                            if (clientEmail == null)
                            {
                                addNewClientsViewModel.ClientSourceList.Add(new SelectListItem
                                {
                                    Value = client.Name,
                                    Text = client.Name
                                });

                                addNewClientsViewModel.NewClients.Add(client);
                            }
                            else
                                clientEmail.Email += "; " + client.Email;
                        }
                        else
                        {
                            var clientEmail = addNewClientsViewModel.NewEmails.FirstOrDefault(p => p.ClientId == client.ClientId);

                            var newEmailClient = new List<ClientEmail> { new ClientEmail { ClientId = client.ClientId, Email = client.Email } };
                            _proCrmService.InsertEmailsToClient(client.ClientId, newEmailClient);
                            if (clientEmail != null)
                                clientEmail.Email += "; " + client.Email;
                        }
                    }
                }

                addNewClientsViewModel.ClientSourceList.AddRange(
                    masterClients.Where(
                    p => string.IsNullOrWhiteSpace(p.RelatedFirstName) && string.IsNullOrWhiteSpace(p.RelatedLastName)).Select(
                        p =>
                        new SelectListItem
                            {
                                Value = p.ClientId.ToString(),
                                Text = string.Format("{0} {1}", p.FirstName, p.LastName)
                            }));
            }


            UserSession.MultiBrochureList = new MultiBrochureList();
            if (!addNewClientsViewModel.NewClients.Any())
            {
                script.AppendLine(model.IsMvc
                    ? "setTimeout('tb_remove();',500); "
                    : "setTimeout('window.parent.tb_remove();',500); ");
                script.AppendLine("try{window.parent.commResults.UnCheckAll();}catch(ex){}");

                return Json(new { google = script.ToString() }, JsonRequestBehavior.AllowGet);

            }
            addNewClientsViewModel.GoogleAnalytics = script;
            return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.MyClients.AddNewClientsToYourList,
                               addNewClientsViewModel);
        }

        public virtual ActionResult AutoCompleteHtml(int index)
        {
            return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.FreeBrochure.AutoCompleteClientEmailName, new Client { Index = index });
        }

        #endregion

        #region Returned User
        public virtual JsonResult SendBrosureReturnedUser(ReturnedUserRecoCommViewModel model)
        {
            var createGAScript = false;
            var gaScript = string.Empty;

            CookieManager.EncEmail = CryptoHelper.Encrypt(model.Email, CookieConst.EncEmail);

            var commToReco = model.RecoComms.GetSelectCommunities();
            var conversionIFrameSource = TrackingScriptsHelper.GetConversionReturningUsers(model.RecoComms.Count,
                                                                                           commToReco.Count(),
                                                                                           model.IsCheckMainCommBase
                                                                                                .ToType<int>());
            var leadInfo = new LeadInfo();
            if (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser)
                leadInfo = LeadUtil.GetLeadInfoFromSession();

            leadInfo.LeadUserInfo.Email = UserSession.UserProfile.LogonName = model.Email;

            var fullName = (model.FullName ?? string.Empty).Trim();
            leadInfo.LeadUserInfo.FirstName = UserSession.UserProfile.FirstName = fullName.FirstPart(" ");
            leadInfo.LeadUserInfo.LastName = UserSession.UserProfile.LastName = fullName.LastPart(" ");
            leadInfo.LeadUserInfo.PostalCode = UserSession.UserProfile.PostalCode = model.Zip;

            UserSession.UserProfile.WriteProfileCookie();

            //var googleHelper = new GoogleHelper(_communityService, _listingService);
            if (model.IsCheckMainCommBase)
            {
                leadInfo.LeadType = LeadType.Community;
                leadInfo.MarketId = model.MarketId;
                leadInfo.BuilderId = model.BuilderId;
                leadInfo.CommunityList = model.CommunityId.ToString(CultureInfo.InvariantCulture);
                leadInfo.LeadUserInfo.SessionId = UserSession.SessionId;
                leadInfo.RequestUniqueKey = string.Format("{0}-c{1}-", Guid.NewGuid(), model.CommunityId);
                leadInfo.LeadAction = LeadAction.FreeBrochureReturningUser;
                LeadUtil.GenerateLead(leadInfo);
                createGAScript = true;
                //googleHelper.CreateGoogleAnalyticsDirectLead(leadInfo.RequestUniqueKey, model.CommunityId, 0, 0, true, GoogleGaConst.DirectReturn);
            }
            else
            {
                _leadService.InsertPresenteComunitys(model.CommunityId, model.CommunityId.ToString(), model.Email, leadInfo.LeadUserInfo.FirstName, leadInfo.LeadUserInfo.LastName, leadInfo.LeadUserInfo.PostalCode, NhsRoute.PartnerId, model.MarketId);
            }

            var commList = model.RecoComms.GetSelectCommsAndBuilders();
            if (!string.IsNullOrWhiteSpace(commList))
            {
                leadInfo.SourceCommunityId = model.CommunityId;
                var comm = _communityService.GetCommunity(model.CommunityId);
                leadInfo.LeadComments = TemplateWordingHelper.GetMessageCommunity(comm);
                leadInfo.CommunityList = commList.TrimEnd(',');
                leadInfo.LeadType = LeadType.Market;
                leadInfo.LeadAction = LeadAction.RecommendedReturningUser;
                LeadUtil.GenerateLead(leadInfo);
                //googleHelper.CreateGoogleAnalyticsRecommendedCommunityLeads(model.CommunityId, leadInfo.RequestUniqueKey, commToReco, true, true, GoogleGaConst.RcmReturnUser);
            }

            if (createGAScript) {
                var googleHelper = new GoogleAnalyticEcommerce(_communityService.GetCommunity(model.CommunityId), _communityService.GetCommunities(commToReco).ToList(), leadInfo.RequestUniqueKey, _partnerService.GetPartner(NhsRoute.PartnerId).PartnerName);
                gaScript = googleHelper.GetScript();
            }

            if (model.MatchEmail && model.RecoComms.GetUnSelectCommunities().Any())
            {
                _leadService.InsertPresenteComunitys(model.CommunityId, string.Join(",", model.RecoComms.GetUnSelectCommunities()), model.Email, leadInfo.LeadUserInfo.FirstName, leadInfo.LeadUserInfo.LastName, leadInfo.LeadUserInfo.PostalCode, NhsRoute.PartnerId, model.MarketId);
            }

            var data = new
            {
                google = gaScript,
                Iframe = "<iframe src='" + conversionIFrameSource +
                         "' width='0' height='0' frameborder='0' scrolling='no' rel='nofollow'></iframe>"
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion
    }

}
