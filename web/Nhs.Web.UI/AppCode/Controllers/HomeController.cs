﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Business.Map;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Constants.Route;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Content;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Filters;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Search.Objects.Constants;
using Nhs.Web.UI.AppCode.Filters;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Utility.Common;
using System.Linq;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Library.Business;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers.Utility;
using Nhs.Utility.Constants;
using Nhs.Search.Objects;
using Nhs.Web.UI.AppCode.Helper;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class HomeController : ApplicationController
    {
        private readonly ICmsService _cmsService;
        private readonly IBrandService _brandService;
        private readonly ILookupService _lookupService;
        private readonly IStateService _stateService;
        private readonly IMarketService _marketService;
        private readonly IPartnerService _partnerService;
        private readonly IPathMapper _pathMapper;
        private readonly IApiService _apiService;

        public HomeController(ICmsService cmsService, IBrandService brandService, ILookupService lookupService, IStateService stateService, IMarketService marketService, IPartnerService partnerService, IPathMapper pathMapper, IApiService apiService)
            : base(pathMapper)
        {
            _cmsService = cmsService;
            _brandService = brandService;
            _lookupService = lookupService;
            _stateService = stateService;
            _marketService = marketService;
            _partnerService = partnerService;
            _pathMapper = pathMapper;
            _apiService = apiService;
        }

        //GET: /Home/
        [NoCache]
        public virtual ActionResult Show()
        {
            HomeViewModel model;
            if (NhsRoute.ShowMobileSite)
            {
                model = GetHomeViewModelMobile();
            }
            else
            {
                int marketId = UserSession.PersonalCookie.MarketId;
                string searchText = UserSession.PersonalCookie.SearchText;

                string state = UserSession.PersonalCookie.State;
                string marketName = _marketService.GetMarketName(NhsRoute.PartnerId, marketId);

                if (string.IsNullOrEmpty(searchText))
                    searchText = marketName;

                var bvm = new BaseViewModel();
                if ((searchText.ToLower() != marketName.ToLower()) & (!CommonUtils.IsZip(searchText)))
                {
                    bool cityMatch = false;
                    IList<City> cities = _marketService.GetCitiesForMarket(NhsRoute.PartnerId, marketId);
                    foreach (City cty in cities)
                    {
                        if (cty.CityName.ToLower() == searchText.ToLower())
                        {
                            cityMatch = true;
                            break;
                        }

                        if (cty.County.ToLower() == searchText.ToLower())
                        {
                            searchText = cty.County + " County";
                            cityMatch = true;
                            break;
                        }
                    }

                    if (!cityMatch)
                        searchText = marketName;
                }

                // special case for Washington DC ...
                if (searchText.ToUpper().EndsWith(" DC") && state.ToUpper() == "DC")
                    searchText = searchText.Remove(searchText.Length - 3);

                searchText = searchText.ToTitleCase();

                if (!CommonUtils.IsZip(searchText) && !string.IsNullOrEmpty(searchText) && !string.IsNullOrEmpty(state))
                    // Sets state
                    searchText += ", " + state.ToUpper();

                UserSession.SearchType = SearchTypeSource.SearchUnknown;
                UserSession.PropertySearchParameters.NhsClear();

                if (UserSession.SearchParametersV2 != null)
                    UserSession.SearchParametersV2.Init();

                //End Changes Case 76432
                UserSession.PropertySearchParameters.PromotionType = PromotionType.Any;

                model = GetViewModel(searchText, UserSession.PersonalCookie.PriceLow.ToString(),
                    UserSession.PersonalCookie.PriceHigh.ToString(),
                    UserSession.PersonalCookie.BathRooms, UserSession.PersonalCookie.BedRooms);



            }

            base.AddCanonicalTag(model);
            base.SetupAdParameters(model);
            return View(model);

        }

        [HttpPost]
        public virtual ActionResult Show(string searchText, string searchType, string priceLow, string priceHigh,
            string numOfBeds = "", string numOfBaths = "", string toPage = "", string sqFtMin = "", string amenityName = "")
        {
            IList<RouteParam> param = new List<RouteParam>();
            bool hadAreaWord = false;

            if (string.IsNullOrEmpty(searchType) && !string.IsNullOrEmpty(Request.Form["SearchType"]))
                searchType = Request.Form["SearchType"];

            //if (resetSearchParameters && toPage != Pages.CommunityResultsv2)
            //    UserSession.SearchParametersV2 = null;

            if (searchText.EndsWith("area", StringComparison.InvariantCultureIgnoreCase))
            {
                searchText = searchText.Substring(0, searchText.LastIndexOf("area", StringComparison.InvariantCultureIgnoreCase)).Trim();
                searchType = ((int)LocationType.Market).ToType<string>();
                hadAreaWord = true;
            }
            else if (searchType.ToType<int>() == (int)LocationType.Market)
                searchType = string.Empty;


            if (searchText.IndexOf(",") == -1 && !hadAreaWord)// if not of the form Location, State it wast selected to from the type ahead list, so reset search type                
                searchType = string.Empty;

            var locations = _partnerService.GetTypeAheadSuggestions(NhsRoute.PartnerId, searchText, true);

            if (locations.Count == 1)
                searchText = locations[0].Type >= (int)LocationType.Community ?
                                locations[0].Name + " in " + locations[0].MarketName + ", " + locations[0].State :
                                locations[0].Name + (locations[0].Type != (int)LocationType.Zip ? ", " : string.Empty) + locations[0].State;

            var state = string.Empty;

            if (searchText.LastIndexOf(" ") != -1)
                state = searchText.Substring(searchText.LastIndexOf(" ")).Trim();

            var stateAbbr = string.Empty;

            if (!string.IsNullOrEmpty(state))
            {

                if (state.Length == 2)
                    stateAbbr = string.IsNullOrEmpty(_stateService.GetStateName(state)) ? null : state;
                else
                    stateAbbr = _stateService.GetStateAbbreviation(state);
            }

            // Searching by Zip
            if (CommonUtils.IsZip(searchText))
            {
                param.Add(new RouteParam(RouteParams.SearchText, searchText, RouteParamType.QueryString));
            }
            // Searching by City, ST or Market, ST
            else if (searchText.IndexOf(",") != -1 || !string.IsNullOrEmpty(stateAbbr))
            {
                int commaPostion = searchText.IndexOf(",");
                string locationText, stateText;

                if (commaPostion != -1)
                {
                    locationText = searchText.Substring(0, commaPostion).Trim();
                    stateText = searchText.Substring(commaPostion + 1).Trim();
                }
                else
                {
                    locationText = searchText.Substring(0, searchText.LastIndexOf(" "));
                    stateText = stateAbbr;
                }

                if (!string.IsNullOrEmpty(locationText) && !string.IsNullOrEmpty(stateText))
                {
                    // Full State Name
                    if (stateText.Length > 2)
                    {
                        stateText = _stateService.GetStateAbbreviation(stateText);
                    }



                    if (CommonUtils.IsZip(locationText) && string.IsNullOrWhiteSpace(stateText) == false)
                    {
                        var marketFromZip = _marketService.GetMarketIdFromPostalCode(locationText, NhsRoute.BrandPartnerId);
                        var marketfromId = _marketService.GetMarket(NhsRoute.PartnerId, marketFromZip, false);
                        if (marketfromId != null && stateText.ToLower() != marketfromId.StateAbbr.ToLower())
                        {
                            locationText = locationText + " " + stateText;
                            stateText = string.Empty;
                        }
                    }

                    param.Add(new RouteParam(RouteParams.SearchText, HttpUtility.UrlEncode(locationText), RouteParamType.QueryString));

                    if (string.IsNullOrWhiteSpace(stateText) == false)
                    {
                        param.Add(new RouteParam(RouteParams.State, stateText));
                        UserSession.PersonalCookie.State = stateText;
                    }

                    UserSession.PersonalCookie.SearchText = locationText;
                    UserSession.PersonalCookie.SearchText = searchText;

                }
                else
                {
                    ModelState.AddModelError("SearchText", @"invalid data, location and state required.");
                }
            }
            else
            {
                param.Add(new RouteParam(RouteParams.SearchText, searchText, RouteParamType.QueryString));
                UserSession.PersonalCookie.SearchText = searchText;
            }


            if (!ModelState.IsValid && !NhsRoute.ShowMobileSite)
            {
                HomeViewModel vm = GetViewModel(searchText, priceLow, priceHigh);
                // ReSharper disable Asp.NotResolved
                return View("Show", vm);
                // ReSharper restore Asp.NotResolved
            }

            // Rules for prices 
            if (!string.IsNullOrEmpty(priceLow) || !string.IsNullOrEmpty(priceHigh))
            {
                if (!string.IsNullOrEmpty(priceLow) && priceLow.IndexOf("$") != -1)
                    priceLow = priceLow.Substring(1);

                if (!string.IsNullOrEmpty(priceHigh) && priceHigh.IndexOf("$") != -1)
                    priceHigh = priceHigh.Substring(1);

                if (!string.IsNullOrEmpty(priceLow) && priceLow.IndexOf(",") != -1)
                    priceLow = priceLow.Replace(",", string.Empty);

                if (!string.IsNullOrEmpty(priceHigh) && priceHigh.IndexOf(",") != -1)
                    priceHigh = priceHigh.Replace(",", string.Empty);

                if (!string.IsNullOrEmpty(priceLow) && priceLow != "0" && priceLow.Length < 4)
                    priceLow = priceLow + "000";

                if (!string.IsNullOrEmpty(priceHigh) && priceHigh != "0" && priceHigh.Length < 4)
                    priceHigh = priceHigh + "000";

                if (priceLow == "0") priceLow = null;
                if (priceHigh == "0") priceHigh = null;

                if (!string.IsNullOrEmpty(priceHigh) && !string.IsNullOrEmpty(priceLow) && priceLow.ToType<int>() > priceHigh.ToType<int>())
                {
                    var temp = priceLow;
                    priceLow = priceHigh;
                    priceHigh = temp;
                }

            }

            if (!string.IsNullOrEmpty(priceLow))
            {
                param.Add(new RouteParam(RouteParams.PriceLow, priceLow));
                UserSession.PersonalCookie.PriceLow = int.Parse(priceLow);
            }

            if (!string.IsNullOrEmpty(priceHigh))
            {
                param.Add(new RouteParam(RouteParams.PriceHigh, priceHigh));
                UserSession.PersonalCookie.PriceHigh = int.Parse(priceHigh);
            }

            if (!string.IsNullOrEmpty(numOfBaths))
                param.Add(new RouteParam(RouteParams.BathRooms, numOfBaths));

            if (!string.IsNullOrEmpty(numOfBeds))
                param.Add(new RouteParam(RouteParams.BedRooms, numOfBeds));

            // Fix for 70271, use the Sqt filter
            if (!string.IsNullOrEmpty(sqFtMin))
                param.Add(new RouteParam(RouteParams.SquareFeet, sqFtMin));

            if (!string.IsNullOrEmpty(searchType))
                param.Add(new RouteParam(RouteParams.SearchType, searchType));

            if (!string.IsNullOrEmpty(toPage))
                param.Add(new RouteParam(RouteParams.ToPage, toPage));

            if (!string.IsNullOrEmpty(amenityName))
                param.Add(new RouteParam(RouteParams.Amenity, amenityName));

            //redirect to location handler page
            return this.Redirect(Pages.LocationHandler, param);
        }


        // Home for Partners
        public virtual ViewResult ShowBasicSearch()
        {
            return View(NhsMvc.Default.Views.Home.BasicSearch, GetBasicSearchModel());
        }

        // Home for Partners
        [HttpPost]
        public virtual ActionResult ShowBasicSearch(string state, int? area, string city, string zip, int? PriceLow, int? PriceHigh)
        {
            IList<RouteParam> param = new List<RouteParam>();
            var isValid = false;

            if (string.IsNullOrEmpty(zip))
            {
                var partner = _partnerService.GetPartner(NhsRoute.PartnerId);

                if (PartnerLayoutHelper.GetPartnerLayoutConfig().DoesPartnerHaveStateDropDown && string.IsNullOrEmpty(state))
                {
                    ModelState.AddModelError("State", LanguageHelper.StateIsRequired);
                }
                else
                {
                    UserSession.PersonalCookie.State = string.IsNullOrEmpty(state) ? partner.SelectState : state;
                    param.Add(new RouteParam(RouteParams.State, UserSession.PersonalCookie.State));
                    isValid = true;
                }

                if (PartnerLayoutHelper.GetPartnerLayoutConfig().DoesPartnerHaveMarketDropDown && (!area.HasValue || area.Value == 0) && isValid)
                {
                    ModelState.AddModelError("Area", LanguageHelper.AreaIsRequired);
                    isValid = false;
                }
                else
                {
                    UserSession.PersonalCookie.MarketId = area.HasValue ? area.Value : partner.SelectMetroId.ToType<int>();
                    param.Add(new RouteParam(RouteParams.Market, UserSession.PersonalCookie.MarketId.ToString()));
                    isValid = true;
                }
            }

            if (!isValid)
            {
                if (string.IsNullOrEmpty(zip))
                    ModelState.AddModelError("Zip", LanguageHelper.MSG_REGISTER_INVALID_ZIP);
                else if (!CommonUtils.IsZip(zip))
                    ModelState.AddModelError("Zip", LanguageHelper.MSG_REGISTER_INVALID_ZIP);
            }

            if (!ModelState.IsValid)
            {
                return View(NhsMvc.Default.Views.Home.BasicSearch, GetBasicSearchModel());
            }


            if (!string.IsNullOrEmpty(city))
            {
                param.Add(new RouteParam(RouteParams.City, city));
                UserSession.PersonalCookie.City = city;
            }

            var page = string.Empty;

            if (!string.IsNullOrEmpty(zip))
            {
                if (CommonUtils.IsZip(zip))
                {
                    param.Clear();
                    param.Add(new RouteParam(RouteParams.SearchText, zip));
                    UserSession.PersonalCookie.SearchText = zip;
                    page = Pages.LocationHandler;
                }
            }
            else
                page = Pages.CommunityResults;


            if (PriceLow.HasValue & PriceHigh.HasValue)
            {
                if (PriceLow.Value > PriceHigh.Value)
                {
                    var tmp = PriceLow;
                    PriceLow = PriceHigh;
                    PriceHigh = tmp;
                }

            }

            if (PriceLow.HasValue)
            {
                param.Add(new RouteParam(RouteParams.PriceLow, PriceLow.Value.ToString(), RouteParamType.QueryString));
                UserSession.PersonalCookie.PriceLow = PriceLow.Value;
            }

            if (PriceHigh.HasValue)
            {
                param.Add(new RouteParam(RouteParams.PriceHigh, PriceHigh.Value.ToString(), RouteParamType.QueryString));
                UserSession.PersonalCookie.PriceHigh = PriceHigh.Value;
            }

            param.Add(new RouteParam(RouteParams.SearchType, SearchTypeSource.QuickSearchCommunityResults, RouteParamType.QueryString));
            //redirect to location handler page
            return this.Redirect(page, param);



        }

        // Populate Markets Dropdownlist for partner sites on ajax requests
        public virtual JsonResult GetMarkets(string state)
        {
            var markets = _marketService.GetMarketsByState(NhsRoute.PartnerId, state).Select(m => new { Id = m.MarketId, Name = m.MarketName });

            return this.Json(markets);
        }

        // Populate Markets Dropdownlist for partner sites on ajax requests
        public virtual JsonResult GetCities(int area)
        {
            var cities = _marketService.GetCitiesForMarket(NhsRoute.PartnerId, area).Select(c => new { Id = c.CityName, Name = c.CityName });

            return this.Json(cities);
        }

        public virtual ActionResult Search(string searchText, string toPage = "")
        {
            return Show(searchText, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, toPage);
        }

        #region LocationHandler existing Functionality

        public virtual ActionResult LocationHandler()
        {
            string searchText = RouteParams.SearchText.Value<string>();
            string stateAbbr = RouteParams.State.Value<string>();
            decimal priceHigh = RouteParams.PriceHigh.Value<decimal>();
            decimal priceLow = RouteParams.PriceLow.Value<decimal>();
            LocationType type = (LocationType)RouteParams.SearchType.Value<int>();

            if (string.IsNullOrEmpty(stateAbbr) && searchText.Contains(","))
            {
                var textArray = searchText.Split(',');
                searchText = textArray.Length == 2
                                 ? string.Format("{0},{1}", textArray[0].Trim(), textArray[1].Trim())
                                 : searchText.Replace(", ", ",");
            }

            UserSession.PropertySearchParameters.NhsClear();
            UserSession.PersonalCookie.SearchText = string.Empty;

            if (UserSession.SearchParametersV2 != null)
                UserSession.SearchParametersV2.Init();

            var abbrsHelper = new AbbreviationsReader(_pathMapper);
            var abbreviations = abbrsHelper.ParseScripts();

            LocationHandlerViewModel model = GetLocationHandlerModel(searchText, stateAbbr, type, priceLow, priceHigh, abbreviations);

            var extraParams = NhsRoute.CurrentRoute.Params.Where(p => p.ParamType == RouteParamType.QueryString && p != RouteParams.SearchText).ToList();

            //Add noindex
            var metaRegistrar = new MetaRegistrar(_pathMapper);
            var metas = metaRegistrar.GetRobotsMeta("noodp, noydir, noindex");
            base.AddNoIndex(metaRegistrar, metas.ToList());
            OverrideDefaultMeta(model, metas);

            if (string.IsNullOrEmpty(model.SearchText))
            {
                return this.Redirect(Pages.Home);
            }

            if (extraParams != null && extraParams.Count > 0)
            {
                model.ExtraParameters = extraParams.ToUrl(useDummyFunction: true);
            }

            // Rule: if only one location rather than community type matches the searched text redirect to that one
            if (model.Locations != null && (model.Locations.Count == 1 || model.Locations.Where(l => l.Type < (int)LocationType.Community).ToList().Count == 1))
            {
                var loc = model.Locations.FirstOrDefault(l => l.Type < (int)LocationType.Community) ??
                          model.Locations[0];

                return LocationHanlderRedirect(loc, model.PriceLow, model.PriceHigh, model.ExtraParameters);
            }
            // Rule: if only one location for the selected type matches the searched text redirect to that one
            if (model.Locations != null && type >= LocationType.Market && model.Locations.Count(l => l.Type == (int)type) == 1)
            {
                var loc = model.Locations.FirstOrDefault(l => l.Type == (int)type);

                return LocationHanlderRedirect(loc, model.PriceLow, model.PriceHigh, model.ExtraParameters);
            }
            // Rule: If there is only 1 location that exactly match the searched location, redirect to that location
            if (model.Locations != null && model.Locations.Count(l => (l.Name.ToLower() == model.SearchText.ToLower() || TypeAheadAbbreviations.ChangeAbbreviations(model.SearchText, abbreviations).ToLower() == l.Name.ToLower()) && !string.IsNullOrEmpty(l.State) && !string.IsNullOrEmpty(model.StateAbbr) && l.State.ToLower() == model.StateAbbr.ToLower()) == 1)
            {
                var loc = model.Locations.FirstOrDefault(l => (l.Name.ToLower() == model.SearchText.ToLower() || TypeAheadAbbreviations.ChangeAbbreviations(model.SearchText, abbreviations).ToLower() == l.Name.ToLower()) && l.State.ToLower() == model.StateAbbr.ToLower());

                if (loc != null)
                    return LocationHanlderRedirect(loc, model.PriceLow, model.PriceHigh, model.ExtraParameters);
            }
            // Rule: If there is a market and and city that have the same location name, redirect to the market if was sected (searchtype defined) if not to the city
            else if (model.Locations != null && model.Locations.Count(l => l.Type < (int)LocationType.Community && l.Name.ToLower() == model.SearchText.ToLower() && !string.IsNullOrEmpty(l.State) && !string.IsNullOrEmpty(model.StateAbbr) && l.State.ToLower() == model.StateAbbr.ToLower()) == 2 &&
                model.Locations.Count(l => l.Type == (int)LocationType.Market && l.Name.ToLower() == model.SearchText.ToLower() && !string.IsNullOrEmpty(l.State) && !string.IsNullOrEmpty(model.StateAbbr) && l.State.ToLower() == model.StateAbbr.ToLower()) == 1)
            {

                var loc = model.Locations.FirstOrDefault(l => l.Type == (int)LocationType.Market && l.Name.ToLower() == model.SearchText.ToLower() && l.State.ToLower() == model.StateAbbr.ToLower());

                if (type != LocationType.Market)
                    loc = model.Locations.FirstOrDefault(l => l.Type == (int)LocationType.City && l.Name.ToLower() == model.SearchText.ToLower() && l.State.ToLower() == model.StateAbbr.ToLower()); ;

                if (loc != null)
                    return LocationHanlderRedirect(loc, model.PriceLow, model.PriceHigh, model.ExtraParameters);
            }
            // Rule: If there is only 1 location that exactly match the searched location and type is Community or Developer, check for city
            // because it could be same name and same state but different city
            // redirect to that location
            else if (model.Locations != null && model.Locations.Count(l => (l.Name.ToLower() == model.SearchText.ToLower() || TypeAheadAbbreviations.ChangeAbbreviations(model.SearchText, abbreviations).ToLower() == l.Name.ToLower()) && !string.IsNullOrEmpty(l.State) && !string.IsNullOrEmpty(model.StateAbbr) && l.State.ToLower() == model.StateAbbr.ToLower() && l.Type >= 5 && l.MarketName == model.City) == 1)
            {
                var loc = model.Locations.FirstOrDefault(l => (l.Name.ToLower() == model.SearchText.ToLower() || TypeAheadAbbreviations.ChangeAbbreviations(model.SearchText, abbreviations).ToLower() == l.Name.ToLower()) && l.State.ToLower() == model.StateAbbr.ToLower() && l.MarketName == model.City);

                if (loc != null)
                    return LocationHanlderRedirect(loc, model.PriceLow, model.PriceHigh, model.ExtraParameters);
            }

            base.SetupAdParameters(model);

            // ReSharper disable once Mvc.ViewNotResolved
            return View(model);
        }

        public virtual ActionResult LocationHanlderRedirect(Location location, decimal priceLow, decimal priceHigh, string extraParams)
        {
            var urlParams = new List<RouteParam>();
            var  page = Pages.CommunityResults;

            if (!string.IsNullOrEmpty(RouteParams.ToPage.Value<string>()))
            {
                page = RouteParams.ToPage.Value<string>();
            }

            urlParams.Add(new RouteParam ( RouteParams.Market, location.MarketId.ToString()));

            if (location.Type == (int)LocationType.City)
                urlParams.Add(new RouteParam ( RouteParams.CityNameFilter, location.Name));
            else if (location.Type == (int)LocationType.Zip)
                urlParams.Add(new RouteParam ( RouteParams.PostalCode, location.Name));
            else if (location.Type == (int)LocationType.County)
            {
                var countyName = location.Name.Replace("County", string.Empty).Trim();
                urlParams.Add(new RouteParam ( RouteParams.County, countyName));
            }

            if (priceLow > 0)
                urlParams.Add(new RouteParam ( RouteParams.PriceLow, priceLow));

            if (priceHigh > 0)
                urlParams.Add(new RouteParam ( RouteParams.PriceHigh, priceHigh));

            if (location.Type >= (int)LocationType.Community)
                urlParams.Add(new RouteParam ( RouteParams.CommunityName, HttpUtility.UrlEncode(location.Name), RouteParamType.QueryString ));

            if (!string.IsNullOrEmpty(RouteParams.Green.Value<string>()))
                urlParams.Add(new RouteParam ( RouteParams.GreenProgram, RouteParams.Green.Value<string>()));

            if (RouteParams.BathRooms.Value<int>() != 0)
                urlParams.Add(new RouteParam ( RouteParams.BathRooms, RouteParams.BathRooms.Value<string>(), RouteParamType.QueryString ));

            if (RouteParams.BedRooms.Value<int>() != 0)
                urlParams.Add(new RouteParam ( RouteParams.BedRooms, RouteParams.BedRooms.Value<string>(), RouteParamType.QueryString ));

            if (RouteParams.SquareFeet.Value<int>() != 0)
                urlParams.Add(new RouteParam ( RouteParams.SquareFeet, RouteParams.SquareFeet.Value<string>(), RouteParamType.QueryString ));

            if (!string.IsNullOrEmpty(RouteParams.Email.Value<string>()))
                urlParams.Add(new RouteParam ( RouteParams.Email, RouteParams.Email.Value<string>()));

            if (RouteParams.Range.Value<int>() != 0)
                urlParams.Add(new RouteParam ( RouteParams.Range, RouteParams.Range.Value<string>()));

            if (!string.IsNullOrEmpty(RouteParams.Phone.Value<string>()))
                urlParams.Add(new RouteParam ( RouteParams.PhoneNumber, RouteParams.Phone.Value<string>()));

            if (!string.IsNullOrEmpty(RouteParams.Name.Value<string>()))
                urlParams.Add(new RouteParam ( RouteParams.Name, RouteParams.Name.Value<string>()));

            if (!string.IsNullOrEmpty(RouteParams.UserZip.Value<string>()))
                urlParams.Add(new RouteParam ( RouteParams.UserZip, RouteParams.UserZip.Value<string>()));

            if (!string.IsNullOrEmpty(RouteParams.BrandId.Value<string>()))
                urlParams.Add(new RouteParam ( RouteParams.BrandId, RouteParams.BrandId.Value<string>()));

            if (!string.IsNullOrEmpty(RouteParams.Refer.Value<string>()))
                urlParams.Add(new RouteParam ( RouteParams.Refer, RouteParams.Refer.Value<string>()));

            if (!string.IsNullOrEmpty(RouteParams.Amenity.Value()))
            {
                RouteParams name;
                switch (RouteParams.Amenity.Value())
                {
                    case SEOAmenityType.StateWaterfront:
                        name = RouteParams.WaterFront;
                        break;
                    case SEOAmenityType.StateGolfCourse:
                        name = RouteParams.GolfCourse;
                        break;
                    default:
                        name = RouteParams.TownHomes;
                        break;

                }
                urlParams.Add(new RouteParam(name, "true", RouteParamType.QueryString, true, true));
            }

            return Redirect(page, urlParams.ToResultsParams(), extraParams);
        }
        #endregion

        // Get Model for move, nhs and pro
        private HomeViewModel GetViewModel(string searchText, string selectedPriceLo, string selectedPriceHi, string selectedBathRoom = "", string selectedBedRoom = "")
        {
            var viewModel = new HomeViewModel();
            var marketId = UserSession.PersonalCookie.MarketId;
            //All partners
            viewModel.SearchText = searchText;

            viewModel.PriceLoRange = new SelectList(_lookupService.GetCommonListItems(CommonListItem.MinPrice),
                "LookupValue", "LookupText", selectedPriceLo);
            viewModel.PriceHiRange = new SelectList(_lookupService.GetCommonListItems(CommonListItem.MaxPrice),
                "LookupValue", "LookupText", selectedPriceHi);
            //NHS Pro only
            if (NhsRoute.IsBrandPartnerNhsPro)
            {
                viewModel.BathList = new SelectList(_lookupService.GetCommonListItems(CommonListItem.Bathrooms),
                    "LookupValue", "LookupText", selectedBathRoom);
                viewModel.BedRoomsList = new SelectList(_lookupService.GetCommonListItems(CommonListItem.Bedrooms),
                    "LookupValue", "LookupText", selectedBedRoom);
                viewModel.SqFtList = new SelectList(_lookupService.GetSquareFeetListItems(), "LookupValue",
                    "LookupText", "");
            }

            //NHL-only
            if (viewModel.Globals.PartnerLayoutConfig.IsPartnerNhl)
            {
                viewModel.SpotLightCommunities = _marketService.GetSpotlightCommunitiesForMarket(NhsRoute.PartnerId, UserSession.PersonalCookie.MarketId)
                    .Select(c => new SpotLightCommunity(
                        c.CommunityName,
                        c.CommunityId,
                        c.MarketId,
                        _marketService.GetMarketName(NhsRoute.PartnerId, c.MarketId),
                        c.BrandName,
                        c.BuilderId.ToString(),
                        c.City,
                        c.State,
                        _stateService.GetStateName(c.State),
                        c.PriceLow,
                        c.PriceHigh,
                        c.CommunityImageThumbnail,
                        c.HasHotHome,
                        c.SubVideoFlag.ToLowerInvariant() == "y",
                        c.PromoId,
                        c.Green,
                        c.CommunityType));
            }
            else
            {
                var trustedBrandsCount = 20;

                var partnerBrands = _brandService.GetBrandsByMarket(NhsRoute.PartnerId, marketId).GetOnesWithLogos().ToList();
                partnerBrands.Shuffle(); //wiggle wiggle wiggle - snoop dogg
                partnerBrands = partnerBrands.Take(trustedBrandsCount).ToList();

                //likely a market-based brand filter. Back fill with national brands
                if (partnerBrands.Count < trustedBrandsCount && marketId > 0)
                {
                    var additionalBrands = _brandService.GetBrandsByMarket(NhsRoute.PartnerId, 0).GetOnesWithLogos().ToList(); //national brands
                    additionalBrands.Shuffle(); //wiggle these too

                    var brandsNeeded = trustedBrandsCount - partnerBrands.Count;

                    additionalBrands = additionalBrands.Take(brandsNeeded).ToList();
                    partnerBrands = partnerBrands.Union(additionalBrands, new BrandComparer()).ToList(); //merge market brands with national
                }
                viewModel.Brands = partnerBrands;
            }
            
            viewModel.SearchText = searchText;

            base.OverrideDefaultMeta(viewModel, new List<ContentTag>(), this.GetSeoTemplate(), new List<MetaTagFacebook> { FacebookHelper.GetFacebookMetaAppIdFacebook() });
            return viewModel;
        }

        // Get Model for Partner Sites
        private BasicSearchViewModel GetBasicSearchModel()
        {
            var basicSearchModel = new BasicSearchViewModel();

            var partner = _partnerService.GetPartner(NhsRoute.PartnerId);

            var userSessionState = UserSession.PersonalCookie.State;
            var userSessionMarket = UserSession.PersonalCookie.MarketId;

            var selectedState = string.IsNullOrEmpty(userSessionState) ? partner.SelectState : userSessionState;
            var selectedArea = userSessionMarket == 0 ? partner.SelectMetroId.ToType<int>() : userSessionMarket;
            var selectedCity = string.Empty;

            //This is only short term workaround for now.   55782
            const string partnerSiteUrlCustom = "fabava";

            //This is only short term workaround for now.   55782
            if (partner.PartnerSiteUrl.Equals(partnerSiteUrlCustom, StringComparison.CurrentCultureIgnoreCase))
            {
                selectedState = "DC";
            }
            ////////////////////////////////////////////

            IList<City> cities = null;
            IList<State> states = _stateService.GetPartnerStates(NhsRoute.PartnerId);

            //This is only short term workaround for now.   55782
            if (partner.PartnerSiteUrl.Equals(partnerSiteUrlCustom, StringComparison.CurrentCultureIgnoreCase))
            {
                states = _stateService.GetPartnerStates(NhsRoute.PartnerId).Where(st => st.StateAbbr.Equals("DC", StringComparison.CurrentCultureIgnoreCase)).ToList();
                if (states.Count == 0)
                {
                    states.Add(new State
                    {
                        StateAbbr = "DC",
                        StateName = "District of Columbia"
                    });
                }
            }
            ////////////////////////////////////////////

            if (states != null && states.Count >= 1 && string.IsNullOrEmpty(selectedState)) selectedState = states.ElementAt(0).StateAbbr;
            IEnumerable<Market> areas = _marketService.GetMarketsByState(NhsRoute.PartnerId, selectedState).ToList();

            //This is only short term workaround for now.   55782
            if (partner.PartnerSiteUrl.Equals(partnerSiteUrlCustom, StringComparison.CurrentCultureIgnoreCase))
            {
                areas = areas.Where(area => area.MarketName.Equals("Washington DC", StringComparison.CurrentCultureIgnoreCase));
                if (!areas.Any())
                {
                    var mk = new Market
                    {
                        MarketName = "Washington DC",
                        MarketId = partner.SelectMetroId.ToType<int>()
                    };
                    IList<Market> mkList = new List<Market>();
                    mkList.Add(mk);
                    areas = mkList;
                }
            }

            if (selectedArea != 0)
                cities = _marketService.GetCitiesForMarket(NhsRoute.PartnerId, selectedArea);

            //This is only short term workaround for now.   55782
            if (partner.PartnerSiteUrl.Equals(partnerSiteUrlCustom, StringComparison.CurrentCultureIgnoreCase))
            {
                selectedCity = "Fredericksburg";
            }

            var priceLow = UserSession.PersonalCookie.PriceLow;
            var priceHigh = UserSession.PersonalCookie.PriceHigh;
            var priceRange = _lookupService.GetCommonListItems(CommonListItem.PriceRange);
            
            //TODO
            var spotlightCommunities =  _marketService.GetSpotlightCommunitiesForMarket(NhsRoute.PartnerId, basicSearchModel.Area);

            basicSearchModel.States = new SelectList(states, "StateAbbr", "StateName", selectedState);
            if (areas != null)
                basicSearchModel.Areas = new SelectList(areas, "MarketId", "MarketName", selectedArea);
            //This is only short term workaround for now.   55782
            if (partner.PartnerSiteUrl.Equals(partnerSiteUrlCustom, StringComparison.CurrentCultureIgnoreCase))
            {
                if (cities != null)
                    basicSearchModel.Cities = new SelectList(cities, "CityName", "CityName", selectedCity);
            }
            else
            {
                if (cities != null)
                    basicSearchModel.Cities = new SelectList(cities, "CityName", "CityName");
            }
            ///////////////////////////////////////////
            basicSearchModel.LowPriceRange = new SelectList(priceRange, "LookupValue", "LookupText", priceLow);
            basicSearchModel.HighPriceRange = new SelectList(priceRange, "LookupValue", "LookupText", priceHigh);
            basicSearchModel.SpotLightCommunities = spotlightCommunities.Select(c => new SpotLightCommunity(c.CommunityName, c.CommunityId, c.MarketId, _marketService.GetMarketName(NhsRoute.PartnerId, c.MarketId), c.BrandName, c.BuilderId.ToString(), c.City, c.State, _stateService.GetStateName(c.State), c.PriceLow, c.PriceHigh, c.CommunityImageThumbnail, c.HasHotHome, c.SubVideoFlag.ToLowerInvariant() == "y", c.PromoId, c.Green, c.CommunityType));

            base.AddCanonicalTag(basicSearchModel);
            return basicSearchModel;
        }


        private SeoTemplateType GetSeoTemplate()
        {
            if (NhsRoute.IsBrandPartnerNhsPro)
                return SeoTemplateType.Pro_Home;

            return SeoTemplateType.Nhs_HomeV2;
        }

        private LocationHandlerViewModel GetLocationHandlerModel(string searchText, string state, LocationType type, decimal priceLow, decimal priceHight, Dictionary<string, string> abbreviations)
        {
            LocationHandlerViewModel model = GetBasicLocationHandlerModel();

            model.SearchText = searchText;
            model.StateAbbr = state;
            model.PriceHigh = priceHight;
            model.PriceLow = priceLow;


            //case: 36435 adding a business exception for this specific combination.
            if ((model.SearchText.ToLower().Equals("saint louis") || model.SearchText.ToLower().Equals("st louis")) && model.StateAbbr.ToLower().Equals("mo"))
            {
                model.SearchText = "St. Louis";
            }

            //case 37284 new requirement added
            if (model.SearchText.ToLower().Equals("st louis") && model.StateAbbr.ToLower().Equals("mo"))
            {
                model.SearchText = "St. Louis";
            }

            string fullText = searchText + (string.IsNullOrEmpty(model.StateAbbr) ? string.Empty : ", " + model.StateAbbr);


            if (CommonUtils.IsZip(fullText))
            {
                model.MarketId = _marketService.GetMarketIdFromPostalCode(fullText, NhsRoute.PartnerId);

                if (model.MarketId == 0)
                {
                    var similarZipCodes = _marketService.GetSimilarPostalCodes(fullText, NhsRoute.PartnerId);
                    if (similarZipCodes.Count > 0)
                    {
                        int similarZipMarketId = similarZipCodes[0].MarketId;
                        Market mrkt = _marketService.GetMarket(similarZipMarketId);

                        List<Market> list = _marketService.GetMarketsByState(NhsRoute.PartnerId, mrkt.StateAbbr).ToList();

                        var marketsPageSize = (list.Count / 2) + 1;
                        model.SimilarMarkets = new List<Market>[] { list.Take(marketsPageSize).ToList(), 
                                                                    list.Skip(marketsPageSize).Take(marketsPageSize).ToList()
                                                                  };
                        model.StateAbbr = mrkt.StateAbbr;
                        State stateData = _stateService.GetStateCoodinates(mrkt.StateAbbr);
                        model.StateMapData = new MapData() { ZoomLevel = Convert.ToInt32(StateMapZoom.GetZoomLevel(model.StateAbbr)), CenterLat = (double)stateData.Latitude, CenterLng = (double)stateData.Longitude };
                    }
                }
                else
                {
                    model.Locations = new List<Location>();
                    model.Locations.Add(new Location() { MarketId = model.MarketId, Name = fullText, Type = (int)LocationType.Zip });
                }
            }
            else
            {
                List<Location> locations = _partnerService.GetTypeAheadSuggestions(NhsRoute.PartnerId, fullText, false, true);

                if (type == LocationType.Community)
                {
                    var city = GetCityFromSearchText(model.SearchText);
                    locations = locations.Where(l => l.MarketName.EqualNullSaveLowercase(city)).ToList();
                }

                if (type >= LocationType.Market)
                    locations = locations.Where(l => l.Type == (int)type).ToList();

                if (type >= LocationType.Community)
                {
                   model.City = GetCityFromSearchText(model.SearchText);
                }

                if (model.SearchText.IndexOf(" in ") != -1)
                    model.SearchText = model.SearchText.Substring(0, model.SearchText.LastIndexOf(" in "));


                if (locations.Count(l => l.Type < (int)LocationType.Community &&
                    ((l.Name.ToLower() + ", " + l.State.ToLower()) == TypeAheadAbbreviations.ReplaceStateName(TypeAheadAbbreviations.ChangeAbbreviations(fullText, abbreviations).ToLower()).ToLower() ||
                     (l.Name.ToLower() + ", " + l.State.ToLower()) == TypeAheadAbbreviations.ReplaceStateName(fullText.ToLower()).ToLower())) == 1)
                {
                    var tmpLocs = new List<Location>();
                    tmpLocs = locations.Where(l => l.Type < (int)LocationType.Community).ToList();
                    if (tmpLocs.Any())
                        locations = tmpLocs;

                    tmpLocs = locations.Where(l => l.MarketName.Equals(searchText, StringComparison.CurrentCultureIgnoreCase)).ToList();

                    if (!tmpLocs.Any())
                    {
                        tmpLocs = locations.Where(l => l.Name.StartsWith(searchText, StringComparison.CurrentCultureIgnoreCase) && l.Type == (int)LocationType.Market).ToList();
                    }
                    if (tmpLocs.Any())
                        locations = tmpLocs;

                    if (locations.Count > 1)
                    {
                        tmpLocs = locations.Where(l => l.Name.Equals(l.MarketName, StringComparison.CurrentCultureIgnoreCase)).ToList();

                        if (tmpLocs.Any())
                            locations = tmpLocs;
                    }
                }

                if (locations.Count > 0)
                {
                    var newLocs = new List<Location>();

                    // start from the end, so that community locations are checked first
                    foreach (var t in locations)
                    {
                        t.Market = _marketService.GetMarket(t.MarketId);
                        if (string.IsNullOrEmpty(t.MarketName)
                            || t.Name.All(Char.IsNumber)
                            || !newLocs.Exists(l => l.MarketId == t.MarketId && l.Type == t.Type && l.State == t.State
                                                    && (t.Type == (int)LocationType.Community
                                                            ? l.MarketName == t.MarketName
                                                            : (l.Name == t.Name &&
                                                               t.Type == (int)LocationType.City ||
                                                               t.Type == (int)LocationType.County))
                                                    && l.MarketName == t.MarketName
                                 ))
                        {

                            if (t.Type >= (int)LocationType.Community) t.Name = model.SearchText;
                            newLocs.Add(t);
                        }
                    }

                    model.Locations = newLocs.OrderBy(l => (l.Type >= (int)LocationType.Community) ? l.MarketName : l.Name).ToList();
                }
                else
                {
                    if (!string.IsNullOrEmpty(model.StateAbbr))
                    {
                        List<Market> list = _marketService.GetMarketsByState(NhsRoute.PartnerId, model.StateAbbr).ToList();

                        var marketsPageSize = (list.Count / 2) + 1;
                        model.SimilarMarkets = new List<Market>[] { list.Take(marketsPageSize).ToList(), 
                                                                    list.Skip(marketsPageSize).Take(marketsPageSize).ToList()
                                                                  };

                        State stateData = _stateService.GetStateCoodinates(model.StateAbbr);
                        if (stateData != null)
                        {
                            model.StateMapData = new MapData()
                            {
                                ZoomLevel = Convert.ToInt32(StateMapZoom.GetZoomLevel(model.StateAbbr)),
                                CenterLat = (double)stateData.Latitude,
                                CenterLng = (double)stateData.Longitude
                            };
                        }
                    }
                }
            }

            model.SearchText = StringHelper.ToTitleCase(model.SearchText);
            model.StateAbbr = StringHelper.ToTitleCase(model.StateAbbr);

            if (model.StateMapData != null)
            {
                // var mm = _marketService.GetMarkets(NhsRoute.PartnerId);
                var mk = _marketService.GetMarkets(NhsRoute.PartnerId).Where(mkt => (mkt.StateAbbr ?? string.Empty).ToUpper() == (model.StateAbbr ?? string.Empty).ToUpper()).ToList();
                model.MarketPoints = mk
                    .Select(p => new { p.Latitude, p.Longitude, p.MarketName, p.TotalCommunities, p.MarketId, p.StateAbbr, p.State.StateName }).ToJson();
            }

            model.SearchText = searchText;

            return model;
        }

        private string GetCityFromSearchText(string searchText)
        {
            var city = string.Empty;
            if (searchText.IndexOf(" in ", StringComparison.Ordinal) != -1)
                city = searchText.Substring(searchText.LastIndexOf(" in ", StringComparison.Ordinal) + 3).Trim();
            else if (searchText.IndexOf(" at ", StringComparison.Ordinal) != -1)
                city = searchText.Substring(searchText.LastIndexOf(" at ", StringComparison.Ordinal) + 3).Trim();

            return city;
        }

        private LocationHandlerViewModel GetBasicLocationHandlerModel()
        {
            var model = new LocationHandlerViewModel();

            model.SiteRoot = (string.IsNullOrEmpty(NhsRoute.PartnerSiteUrl) ? "" : (NhsRoute.PartnerSiteUrl + "/"));
            model.ResourceRoot = Configuration.ResourceDomain + "/";
            model.BrandPartnerId = NhsRoute.BrandPartnerId;

            model.PriceLoRange = new SelectList(_lookupService.GetCommonListItems(CommonListItem.MinPrice), "LookupValue", "LookupText", model.PriceLow);
            model.PriceHiRange = new SelectList(_lookupService.GetCommonListItems(CommonListItem.MaxPrice), "LookupValue", "LookupText", model.PriceHigh);
            model.TypeAheadUrl = (string.IsNullOrEmpty(NhsRoute.PartnerSiteUrl) ? "" : (NhsRoute.PartnerSiteUrl + "/")) + NhsMvc.PartialViews.ActionNames.PartnerLocationsIndex;
            model.PartnerId = NhsRoute.PartnerId;

            return model;
        }
    }
}
