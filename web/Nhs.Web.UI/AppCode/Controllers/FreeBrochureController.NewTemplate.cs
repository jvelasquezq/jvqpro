﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Bdx.Web.Api.Objects;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Constants.Route;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers;
using Nhs.Library.Helpers.Lead;
using Nhs.Library.Proxy;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult;
using Nhs.Web.UI.AppCode.ViewModels.LeadView;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class FreeBrochureController
    {
        [HttpGet]
        public virtual ActionResult RequestBrochure()
        {
            var mobileView = NhsMvc.Default.ViewsMobile.FreeBrochure.RequestBrochure;
            var model = new LeadViewModalModel();
            var partnerUsesMatchmaker = _partnerService.GetPartner(NhsRoute.PartnerId).UsesMatchMaker.ToBool();
            model.PartnerUsesMatchmaker = partnerUsesMatchmaker;

            model.PlanId = RouteParams.PlanId.Value<int>();
            model.SpecId = RouteParams.SpecId.Value<int>();
            model.CommunityId = RouteParams.CommunityList.Value<int>() | RouteParams.Community.Value<int>();
            model.LeadType = RouteParams.LeadType.Value();
            model.LeadAction = RouteParams.LeadAction.Value();
            model.FromPage = RouteParams.FromPage.Value();
            model.BuilderList = RouteParams.BuilderList.Value();
            model.CommunityList = RouteParams.CommunityList.Value();
            model.PlanList = RouteParams.PlanList.Value();
            model.SpecList = RouteParams.SpecList.Value();
            model.MarketId = RouteParams.Market.Value<int>() != 0 ? RouteParams.Market.Value<int>() : RouteParams.MarketId.Value<int>(); //Tickets #79014 & #82868 Added validation for two values.
            model.BuilderId = RouteParams.Builder.Value<int>();
            model.Name = (UserSession.UserProfile.FirstName + " " + UserSession.UserProfile.LastName).Trim();
            model.Email = UserSession.UserProfile.Email;
            model.Phone = UserSession.UserProfile.DayPhone;
            model.Zip = UserSession.UserProfile.PostalCode;
            model.LeadFormType = RouteParams.LeadFormType.Value();

            if (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser)
                model.International = string.IsNullOrEmpty(UserSession.UserProfile.PostalCode);

            if (partnerUsesMatchmaker && !UserSession.HasUserOptedIntoMatchMaker)
                model.IncludeRecoComm = true;

            var searchParams = new SearchParams
            {
                MarketId = model.MarketId,
                WebApiSearchType = WebApiSearchType.Exact,
                PartnerId = NhsRoute.PartnerId
            };

            if (model.PlanId == 0 && model.SpecId == 0)
            {
                searchParams.CommId = model.CommunityId;
                var data = GetCommunitiesFromApi(searchParams);
                var comm = data.Result.FirstOrDefault();
                if (comm != null)
                {
                    model.PropertyName = comm.Name;
                    model.BuilderName = comm.Brand.Name;
                    model.PropertyImage = comm.Thumb1;
                    model.Latitude = comm.Lat;
                    model.Longitude = comm.Lng;
                    
                    if (string.Equals(comm.Status, BuilderCommunityType.ComingSoon, StringComparison.InvariantCultureIgnoreCase))
                    {
                        mobileView = NhsMvc.Default.ViewsMobile.FreeBrochure.AddToInterestList;
                        model.IsAddMeToInterestList = true;
                    }
                    else
                    {
                        mobileView = NhsMvc.Default.ViewsMobile.FreeBrochure.RequestBrochure;
                        model.IsAddMeToInterestList = false;
                    }
                }

            }
            else
            {
                searchParams.CommId = model.CommunityId;
                searchParams.PlanId = model.PlanId;
                searchParams.SpecId = model.SpecId;
                var data = GetHomesFromApi(searchParams);
                var home = data.Result.FirstOrDefault();
                if (home != null)
                {
                    model.BuilderName = home.Brand.Name;
                    model.PropertyImage = home.Thumb1;
                    model.Latitude = home.Lat;
                    model.Longitude = home.Lng;
                    if (model.SpecId != 0)
                    {
                        model.PropertyName =
                            string.Format("{0} at {1}", string.IsNullOrEmpty(home.Addr) ? home.PlanName : home.Addr,
                                home.CommName).Trim();
                    }
                    else
                    {
                        var initWord = (home.PlanName.ToLower().StartsWith("the")) ? "" : "The";
                        model.PropertyName =
                            string.Format("{0} {1} at {2}", initWord, home.PlanName, home.CommName).Trim();
                    }
                }
            }

            switch (model.LeadAction)
            {
                case LeadAction.HomeRequestApointment:
                case LeadAction.RequestApointment:
                case LeadAction.CaRequestApointment:
                case LeadAction.HovRequestApointment:
                case LeadAction.CommRequestApointment:
                    model.IsAppointment = true;
                    var comment = RouteParams.LeadComments.Value().Replace("***", "");
                    if (!string.IsNullOrEmpty(comment))
                    {
                        model.AppointmentDateTime = comment.ToType<DateTime>();
                    }
                    model.ShowCalendar = true;
                    break;
                case LeadAction.ContactBuilder:
                case LeadAction.RequestPromotion:
                case LeadAction.CaRequestPromotion:
                case LeadAction.HovRequestPromotion:
                    model.IsSpecialOffer = true;
                    model.Comments = NhsRoute.ShowMobileSite
                        ? ""
                        : "Please tell me about any special offers you are currently running.";
                    break;
                default:

                    model.IsFreeBrochure = true;
                    break;
            }

            if (Request.IsAjaxRequest() == false)
                return View(model);
            return PartialView(NhsRoute.ShowMobileSite ? mobileView : NhsMvc.Default.Views.FreeBrochure.RequestBrochure, model);
        }

        ////freebrochure/requestrecocomms         Don't Delete
        //public virtual ActionResult RequestRecoComms()
        //{
        //    var recoComms = new List<ApiRecoCommunityResult>();
        //    recoComms = _communityService.GetRecommendedCommunities("", NhsRoute.PartnerId, 43976, 0, 0, 0);

        //    var viewModel = new LeadRecoModalViewModel
        //    {
        //        RequestUniqueKey = "",
        //        Google = "",
        //        RecoCommunities = recoComms.Take(3).ToList(),
        //        BuilderName = "BuilderName"
        //    };
        //    return View(viewModel);
        //}

        ////freebrochure/thankyoumodal          Don't Delete
        //public virtual ActionResult ThankYouModal()
        //{
        //    var model = new ThankYouModalViewModel
        //    {
        //        IsSignIn = false,
        //        IsExistingUser = false
        //    };

        //    return View(model);
        //}

        [HttpPost]
        public virtual ActionResult RequestBrochure(LeadViewModalModel model)
        {
            string requestUniqueKey;
            int recoCount;
            var google = SendLead(model, out requestUniqueKey, out recoCount);
            var conversionIFrameSource = TrackingScriptsHelper.GetConversionTrackerUrlForModals(recoCount);
            var recoComms = new List<ApiRecoCommunityResult>();
            switch (model.LeadType)
            {
                case LeadType.Community:
                    recoComms = _communityService.GetRecommendedCommunities(model.Email, NhsRoute.PartnerId,
                              model.CommunityId, 0, 0, 0, 0, true, true);

                    break;
                case LeadType.Home:
                    recoComms = _communityService.GetRecommendedCommunities(model.Email, NhsRoute.PartnerId, 0,
                             model.SpecId, model.PlanId, 0, 0, true, true);

                    break;
            }

            if (!recoComms.Any())
                return RedirectThankYouModal(google, conversionIFrameSource, model.Email);

            _leadService.InsertPresenteComunitys(model.CommunityId, string.Join(",", recoComms.Select(p => p.CommunityId)), model.Email, model.Name.FirstPart(" "), model.Name.LastPart(" "), model.Zip, NhsRoute.PartnerId, model.MarketId);

            var viewModel = new LeadRecoModalViewModel
            {
                RequestUniqueKey = requestUniqueKey,
                Google = google,
                RecoCommunities = recoComms.Take(3).ToList(),
                FirstName = model.Name.FirstPart(" "),
                LastName = model.Name.LastPart(" "),
                Email = model.Email,
                Zip = model.Zip,
                International = model.International,
                Phone = model.Phone,
                ConversionIFrameSource = conversionIFrameSource,
                CommunityId = model.CommunityId,
                BuilderName = model.BuilderName,
                BuilderId = model.BuilderId,
                MarketId = model.MarketId
            };
            return PartialView(NhsMvc.Default.Views.FreeBrochure.RequestRecoComms, viewModel);
        }

        [HttpPost]
        public virtual ActionResult RequestRecoComms(LeadInfoViewModel model)
        {
            //var googleHelper = new GoogleHelper(_communityService);
            var gaScript = string.Empty;

            var commToReco = model.RecoComms.Where(p => p.Selected).ToList();
            if (commToReco.Any())
            {
                var lead = new LeadInfo();
                var commList = model.RecoComms.GetSelectCommunitiesString();
                lead.LeadUserInfo.Email = model.Email;
                lead.LeadUserInfo.PostalCode = model.Zip;
                lead.LeadUserInfo.Phone = model.Phone;
                lead.LeadUserInfo.LastName = model.LastName;
                lead.LeadUserInfo.FirstName = model.FirstName;

                lead.RequestUniqueKey = string.Format("{0}-c{1}-", Guid.NewGuid(), model.CommunityId);
                lead.MarketId = model.MarketId;
                lead.CommunityList = commList;
                lead.BuilderId = model.BuilderId;
                lead.LeadAction = LeadAction.RecommendedModal;
                lead.LeadUserInfo.SessionId = UserSession.SessionId;

                lead.LeadType = LeadType.Community;
                lead.Referer = UserSession.Refer;
                //Send the Lead
                LeadUtil.GenerateLead(lead);
                foreach (var r in commToReco)
                {
                    var pl = new PlannerListing(r.CommunityId, ListingType.Community);
                    if (!UserSession.UserProfile.Planner.SavedCommunities.Contains(pl))
                    {
                        UserSession.UserProfile.Planner.AddSavedCommunity(r.CommunityId, r.BuilderId);
                    }

                    var googleHelper = new GoogleAnalyticEcommerce(_communityService.GetCommunity(r.CommunityId), null, string.Format("{0}-c{1}-", Guid.NewGuid(), model.CommunityId), _partnerService.GetPartner(NhsRoute.PartnerId).PartnerName);
                    gaScript += googleHelper.GetScript();

                }
            }

            var conversionIFrameSource = TrackingScriptsHelper.GetConversionTrackerForThanksBrochure(model.RecoComms.Count(), commToReco.Count());
            return RedirectThankYouModal(gaScript, conversionIFrameSource, model.Email);
        }

        [HttpPost]
        public virtual JsonResult ThankYouModal(string email, string password, bool isExistingUser)
        {
            var savedCommunities = UserSession.UserProfile.Planner.SavedCommunities;
            var savedHomes = UserSession.UserProfile.Planner.SavedHomes;

            if (isExistingUser)
            {
                try
                {

                    _userService.SignIn(email, password, NhsRoute.PartnerId, LanguageHelper.WrongPassword, LanguageHelper.EmailDoNotExist);
                }
                catch
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {

                var profile = new UserProfile();
                var sessionProfile = UserSession.UserProfile;

                profile.LogonName = email;
                profile.FirstName = sessionProfile.FirstName;
                profile.LastName = sessionProfile.LastName;
                profile.Password = password;
                profile.PartnerId = NhsRoute.PartnerId;

                // Default dropdown values
                profile.MoveInDate = -1;
                profile.FinancePreference = profile.MovingReason = null;
                profile.PostalCode = sessionProfile.PostalCode;
                profile.MailingList = profile.MarketOptin = "0";

                // Setting these values ... required in stored proc.
                profile.DateRegistered =
                    profile.DateLastChanged =
                        profile.BoxRequestedDate = profile.InitialMatchDate = profile.LastMatchesSentDate = DateTime.Now;

                _userService.CreateProfile(profile, NhsRoute.BrandPartnerId, LanguageHelper.WrongPassword, LanguageHelper.EmailDoNotExist, LanguageHelper.UserAlreadyExist);
                _userService.CreateRegistrationLead(profile);

                // Saving user info...also sets profile cookie
                UserSession.UserProfile.ActorStatus = WebActors.ActiveUser;
            }

            foreach (var listing in savedCommunities)
            {
                //_userService.AddCommunityToUserPlanner(profile, listing.ListingId, listing.BuilderId, listing.ListingType);
                UserSession.UserProfile.Planner.AddSavedCommunity(listing.ListingId, listing.BuilderId);
                UserSession.UserProfile.Planner.UpdateCommunityRequestDate(listing.ListingId, listing.BuilderId, listing.SourceRequestKey);
            }

            foreach (var listing in savedHomes)
            {
                UserSession.UserProfile.Planner.AddSavedHome(listing.ListingId, listing.ListingType == ListingType.Spec);
                UserSession.UserProfile.Planner.UpdateHomeRequestDate(listing.ListingId, listing.ListingType == ListingType.Spec, listing.SourceRequestKey);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        private ActionResult RedirectThankYouModal(string google, string conversionIFrameSource, string email, string name = "", bool isFreeBrochure = false, bool isAppointment = false, bool isSpecialOffer = false, bool isAddToInterestList = false)
        {
            var model = new ThankYouModalViewModel
            {
                Name = name,
                Google = google,
                ConversionIFrameSource = conversionIFrameSource,
                Email = email,
                IsSignIn = UserSession.UserProfile.ActorStatus == WebActors.ActiveUser,
                IsFreeBrochure = isFreeBrochure,
                IsAppointment = isAppointment,
                IsSpecialOffer = isSpecialOffer,
                IsAddMeToInterestList = isAddToInterestList
            };
            model.IsExistingUser = _userService.LogonNameExists(model.Email, NhsRoute.PartnerId);

            return PartialView(NhsRoute.ShowMobileSite ? NhsMvc.Default.ViewsMobile.FreeBrochure.ThankYouModal : NhsMvc.Default.Views.FreeBrochure.ThankYouModal, model);
        }

        private string SendLead(LeadViewModalModel model, out string requestUniqueKey, out int recoCount)
        {
            var isComunity = false;
            var isSpec = model.SpecId != 0;
            Community community = null;
            ISpec spec = null;
            IPlan plan = null;
            var leadInfo = new LeadInfo();
            leadInfo.LeadComments = "";
            var isMobile = NhsRoute.ShowMobileSite;
            if (isMobile)
                leadInfo.LeadComments = "From mobile site. ";

            if (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser)
            {
                leadInfo.LeadUserInfo.Email = model.Email;
                leadInfo.LeadUserInfo.FirstName = model.Name.FirstPart(" ");
                leadInfo.LeadUserInfo.LastName = model.Name.LastPart(" ");
                leadInfo.LeadUserInfo.PostalCode = model.Zip;
            }
            else
            {
                leadInfo.LeadUserInfo.Email = UserSession.UserProfile.LogonName = model.Email;
                leadInfo.LeadUserInfo.FirstName = UserSession.UserProfile.FirstName = model.Name.FirstPart(" ");
                leadInfo.LeadUserInfo.LastName = UserSession.UserProfile.LastName = model.Name.LastPart(" ");
                leadInfo.LeadUserInfo.PostalCode = UserSession.UserProfile.PostalCode = model.Zip;
            }

            if (model.IsAddMeToInterestList && NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
            {
                model.Comments = "COMING SOON LEAD! This consumer would like to be added to your Coming Soon community interest list.";
                leadInfo.SuppressEmail = true;
                leadInfo.LeadComments = model.Comments;
            }

            leadInfo.LeadUserInfo.Phone = UserSession.UserProfile.DayPhone = model.Phone;

            if (model.International)
                leadInfo.LeadComments += "International lead - ";

            if (model.ShowCalendar)
            {
                leadInfo.LeadComments += "Requested appointment date: ";
                if (model.AppointmentDateTime != DateTime.MinValue)
                    leadInfo.LeadComments += model.AppointmentDateTime.ToShortDateString();
            }
            else
            {
                leadInfo.LeadComments += model.Comments ?? "";
            }
            leadInfo.ShowMatchingCommunities = !model.IncludeRecoComm;


            leadInfo.LeadType = model.LeadType;
            leadInfo.MarketId = model.MarketId;
            leadInfo.BuilderId = model.BuilderId;
            leadInfo.LeadUserInfo.SessionId = UserSession.SessionId;
            leadInfo.Referer = UserSession.Refer;
            leadInfo.LeadAction = model.LeadAction;
            leadInfo.FromPage = model.FromPage;
            requestUniqueKey = leadInfo.RequestUniqueKey = string.Format("{0}-c{1}-", Guid.NewGuid(), model.CommunityId);
            var recoComms = new List<ApiRecoCommunityResult>();
            switch (model.LeadType)
            {
                case LeadType.Community:
                    leadInfo.CommunityList = model.CommunityList;
                    isComunity = true;
                    var communityId = model.CommunityId;
                    community = _communityService.GetCommunity(communityId);
                    if (model.IncludeRecoComm)
                        recoComms = _communityService.GetRecommendedCommunities(model.Email, NhsRoute.PartnerId, communityId, 0, 0, 0);

                    break;
                case LeadType.Home:
                    if (isSpec)
                    {
                        spec = _listingService.GetSpec(model.SpecId);
                        community = spec.Community ?? _communityService.GetCommunity(spec.CommunityId);
                        leadInfo.SpecList = model.SpecId.ToType<string>();
                        leadInfo.RequestUniqueKey += "s" + leadInfo.SpecList;
                    }
                    else
                    {
                        plan = _listingService.GetPlan(model.PlanId);
                        community = plan.Community ?? _communityService.GetCommunity(plan.CommunityId);
                        leadInfo.PlanList = model.PlanId.ToType<string>();
                        leadInfo.RequestUniqueKey += "p" + leadInfo.PlanList;

                    }
                    if (model.IncludeRecoComm)
                        recoComms = _communityService.GetRecommendedCommunities(model.Email, NhsRoute.PartnerId, 0, model.SpecId, model.PlanId, 0);
                    break;
            }

            recoCount = leadInfo.RecoCount = recoComms.Count;
            //Send the Lead
            var comments = GenerateLeadAndAddToPlanner(leadInfo);

            if (isMobile)
                leadInfo.LeadComments = "From mobile site. " + comments;

            //var googleHelper = new GoogleHelper();
            //googleHelper.CreateGoogleAnalyticsDirectLead(leadInfo.RequestUniqueKey, community, spec, plan, true, leadFormType: model.LeadFormType);

            if (model.IncludeRecoComm)
            {
                var leadComments = "";
                if (recoComms.Count > 0)
                {
                    if (leadInfo.LeadType == LeadType.Home)
                    {

                        if (isSpec)
                        {
                            leadInfo.SourceSpecId = model.SpecId;
                            leadInfo.SourcePlanId = model.PlanId;
                        }
                        else
                        {
                            leadInfo.SourcePlanId = model.PlanId;
                        }

                        leadComments = TemplateWordingHelper.GetMessageHome(community, plan, spec);
                    }
                    else if (leadInfo.LeadType == LeadType.Community)
                    {
                        leadInfo.SourceCommunityId = model.CommunityId;
                        leadComments = TemplateWordingHelper.GetMessageCommunity(community);
                    }

                    //Removed User Comments, beacuse the issue 48254 --> User Comments Showing up in RCM Leads
                    if (model.International)
                        leadInfo.LeadComments += "International lead - ";
                    leadInfo.LeadComments += leadComments;
                    {
                        var bcIds = string.Empty;
                        foreach (var r in recoComms)
                        {
                            bcIds += string.Format("{0}|{1},", r.BuilderId, r.CommunityId);

                            var pl = new PlannerListing(r.CommunityId, ListingType.Community);
                            if (!UserSession.UserProfile.Planner.SavedCommunities.Contains(pl))
                            {
                                UserSession.UserProfile.Planner.AddSavedCommunity(r.CommunityId, r.BuilderId);
                            }
                        }

                        leadInfo.RequestUniqueKey = "";
                        leadInfo.CommunityList = bcIds.TrimEnd(',');
                        leadInfo.LeadType = LeadType.Market;
                        leadInfo.LeadAction = LeadAction.Recommended;
                        leadInfo.Referer = UserSession.Refer;
                        LeadUtil.GenerateLead(leadInfo);
                    }

                    UserSession.HasUserOptedIntoMatchMaker = true;
                }

                //googleHelper.CreateGoogleAnalyticsRecommendedCommunityLeads(community, requestUniqueKey, recoComms, isComunity, true,GoogleGaConst.RecoModal);
            }

            var googleHelper = new GoogleAnalyticEcommerce(community, _communityService.GetCommunities(recoComms.Select(c => c.CommunityId).ToList()).ToList(), leadInfo.RequestUniqueKey, _partnerService.GetPartner(NhsRoute.PartnerId).PartnerName);
            var google = googleHelper.GetScript();

            return google;
        }

        private WebApiCommonResultModel<List<CommunityItemViewModel>> GetCommunitiesFromApi(SearchParams searchParams)
        {
            var data = _apiService.GetResultsWebApi<CommunityItemViewModel>(searchParams, SearchResultsPageType.CommunityResults);
            return data;
        }

        private WebApiCommonResultModel<List<HomeItemViewModel>> GetHomesFromApi(SearchParams searchParams)
        {
            var data = _apiService.GetResultsWebApi<HomeItemViewModel>(searchParams, SearchResultsPageType.HomeResults);
            return data;
        }
    }
}
