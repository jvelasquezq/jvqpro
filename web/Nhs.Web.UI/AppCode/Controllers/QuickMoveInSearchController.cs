﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class QuickMoveInSearchController : ApplicationController
    {
        #region Member variables
        private readonly IQuickMoveInSearchService _quickService;
        private readonly IStateService _stateService;
        private readonly IPathMapper _pathMapper;
        private readonly IMarketService _marketService;
        private readonly IPartnerService _partnerService;
        private ILookupService _lookupService;

        #endregion

        #region Constructor
        public QuickMoveInSearchController(IQuickMoveInSearchService quickService, IPathMapper pathMapper, IMarketService marketService, IStateService stateService, ILookupService lookupService, IPartnerService partnerService)
            : base(pathMapper)
        {
            _quickService = quickService;
            _pathMapper = pathMapper;
            _marketService = marketService;
            _stateService = stateService;
            _lookupService = lookupService;
            _partnerService = partnerService;
        }
        #endregion


        #region Actions

        //
        // GET: /QuickMoveInSearch/

        public virtual ActionResult Show()
        {
            var viewModel = GetModel("");
            AddCanonicalTag(viewModel);
            return View(viewModel);
        }

        [HttpPost]
        public virtual ActionResult Show(QuickMoveInSearchViewModel model)
        {
            return Redirect(GetUrlForRedirect(model));
        }

        [HttpPost]
        public virtual ActionResult ChangeState(QuickMoveInSearchViewModel model)
        {
            var viewModel = GetModel(model.StateAbbr);
            return PartialView(NhsMvc.Default.Views.QuickMoveInSearch.MarketSelect, viewModel);
        }

        #endregion


        #region Private Methods

        private string GetUrlForRedirect(QuickMoveInSearchViewModel model)
        {
            string redirectUrl = string.Empty;
            List<RouteParam> paramz = new List<RouteParam>();
            bool uselocation = false;

            var searchParams = new SearchParams();
            searchParams.Init(true);

            UserSession.SearchParametersV2 = searchParams;

            if (!string.IsNullOrEmpty(model.PostalCode))
            {
                uselocation = true;
                paramz.Add(new RouteParam(RouteParams.SearchText, model.PostalCode, RouteParamType.QueryString, true, true));
                if (!string.IsNullOrEmpty(model.StateAbbr))
                {
                    paramz.Add(new RouteParam(RouteParams.State, string.IsNullOrEmpty(model.StateAbbr) ? "" : model.StateAbbr, RouteParamType.Friendly, true, true));
                }
                paramz.Add(new RouteParam(RouteParams.ToPage, model.Globals.PartnerLayoutConfig.IsBrandPartnerCna ? Pages.Casas : Pages.HomeResults, RouteParamType.QueryString, true, true));
                UserSession.PersonalCookie.SearchText = model.PostalCode;
                UserSession.PersonalCookie.State = string.IsNullOrEmpty(model.StateAbbr) ? "" : model.StateAbbr;

                // Used by the new WebApi service that community results consumes
                UserSession.SearchParametersV2.State = string.IsNullOrEmpty(model.StateAbbr) ? String.Empty : model.StateAbbr;
                UserSession.SearchParametersV2.SearchLocation = model.PostalCode;

                UserSession.SearchParametersV2.MarketId = _marketService.GetMarketIdFromPostalCode(model.PostalCode, NhsRoute.PartnerId);
            }
            else
            {
                paramz.Add(new RouteParam(RouteParams.Market, model.MarketId.ToString(), RouteParamType.Friendly, true, true));
                UserSession.PropertySearchParameters.MarketId = model.MarketId;

                // Used by the new WebApi service that community results consumes
                UserSession.SearchParametersV2.MarketId = model.MarketId;
            }

            //Price From
            if (!string.IsNullOrEmpty(model.PriceLow.ToString()) && model.PriceLow.ToString() != "0")
            {
                paramz.Add(new RouteParam(RouteParams.PriceLow, model.PriceLow.ToString(), RouteParamType.Friendly, true, true));
                UserSession.PersonalCookie.PriceLow = int.Parse(model.PriceLow.ToString());

                // Used by the new WebApi service that community results consumes
                UserSession.SearchParametersV2.PriceLow = int.Parse(model.PriceLow.ToString(CultureInfo.InvariantCulture));
            }

            //Price To
            if (!string.IsNullOrEmpty(model.PriceHigh.ToString()) && model.PriceHigh.ToString() != "0")
            {
                paramz.Add(new RouteParam(RouteParams.PriceHigh, model.PriceHigh.ToString(), RouteParamType.Friendly, true, true));
                UserSession.PersonalCookie.PriceHigh = int.Parse(model.PriceHigh.ToString());

                // Used by the new WebApi service that community results consumes
                UserSession.SearchParametersV2.PriceHigh = int.Parse(model.PriceHigh.ToString(CultureInfo.InvariantCulture));
            }

            if (model.BedDrop.ToString() != "-1")
            {
                // Used by the new WebApi service that community results consumes
                UserSession.SearchParametersV2.Bedrooms = Convert.ToInt32(model.BedDrop.ToString(CultureInfo.InvariantCulture));

                paramz.Add(new RouteParam(RouteParams.BedRooms, model.BedDrop.ToString(), RouteParamType.Friendly, true, true));
            }


            if (model.BathDrop.ToString() != "-1")
            {
                // Used by the new WebApi service that community results consumes
                UserSession.SearchParametersV2.Bathrooms = Convert.ToInt32(model.BathDrop.ToString(CultureInfo.InvariantCulture));

                paramz.Add(new RouteParam(RouteParams.BathRooms, model.BathDrop.ToString(), RouteParamType.Friendly, true, true));
            }

            //always search for quick move-in homes
            UserSession.SearchParametersV2.Qmi = true;

            UserSession.SearchType = SearchTypeSource.AdvancedSearchHomeResults;

            if (uselocation)
                redirectUrl = paramz.ToUrl(Pages.LocationHandler).ToLower();
            else
                redirectUrl = paramz.ToUrl(Pages.HomeResults).ToLower();


            return redirectUrl;
        }

        private QuickMoveInSearchViewModel GetModel(string state)
        {
            int partnerId = Configuration.PartnerId;
            bool isValid = string.IsNullOrEmpty(RouteParams.DisplayMessage.Value());
            string st = string.Empty;
            int mk = 0;

            var stateList = _quickService.GetQuickMoveStates(NhsRoute.PartnerId);

            stateList.Insert(0, new State { StateAbbr = "", StateName = LanguageHelper.ChooseState });

            if (!string.IsNullOrEmpty(UserSession.PersonalCookie.State) && state == "")
            {
                foreach (State listItem in stateList)
                {
                    if (listItem.StateAbbr.Trim().ToLower() == UserSession.PersonalCookie.State.ToLower())
                    {
                        st = UserSession.PersonalCookie.State;
                    }
                }
            }
            else
            {
                st = string.IsNullOrEmpty(state) ? stateList[0].StateAbbr : state;
            }


            var marketList = GetAreasMarket(st);

            marketList.Insert(0, new Market { MarketId = 0, MarketName = LanguageHelper.ChooseAnArea });

            if (!string.IsNullOrEmpty(UserSession.PersonalCookie.MarketId.ToString()) && state == "")
            {
                foreach (Market listItem in marketList)
                {
                    if (listItem.MarketId == UserSession.PersonalCookie.MarketId)
                    {
                        mk = UserSession.PersonalCookie.MarketId;
                    }
                }
            }

            var priceLow = UserSession.PersonalCookie.PriceLow;
            var priceHigh = UserSession.PersonalCookie.PriceHigh;
            var priceRange = _lookupService.GetCommonListItems(CommonListItem.PriceRange);
            var dropBed = -1;
            var dropBath = -1;
            var bedList = _lookupService.GetCommonListItems(CommonListItem.Bedrooms);
            var bathList = _lookupService.GetCommonListItems(CommonListItem.Bathrooms);

            var viewModel = new QuickMoveInSearchViewModel
            {
                StateAbbr = st,
                StateList = stateList,
                MarketId = mk,
                MarketList = marketList,
                ShowValidationMessage = !isValid,
                ShowCustomAdd = true,
                ShowStaticContentCol = true,
                LowPriceRange = new SelectList(priceRange, "LookupValue", "LookupText", priceLow),
                HighPriceRange = new SelectList(priceRange, "LookupValue", "LookupText", priceHigh),
                BedList = new SelectList(bedList, "LookupValue", "LookupText", dropBed),
                BathList = new SelectList(bathList, "LookupValue", "LookupText", dropBath)
            };

            //Add ad params
            if (UserSession.PersonalCookie.MarketId != 0)
                viewModel.Globals.AdController.AddMarketParameter(UserSession.PersonalCookie.MarketId);

            //// Register Meta Tags information
            List<ContentTag> paramz = new List<ContentTag>();
            paramz.Add(new ContentTag
            {
                TagKey = ContentTagKey.MarketName,
                TagValue = "Austin"
            });

            MetaReader metaReader = new MetaReader(_pathMapper);
            var metas = metaReader.GetMetaTagInformation(Pages.QuickMoveInSearch, paramz.ToDictionary(), PageHeaderSectionName.SeoMetaPages);
            OverrideDefaultMeta(viewModel, metas);
            
            return viewModel;
        }



        private List<Market> GetAreasMarket(string state)
        {
            var list = new List<Market>();
            if (!string.IsNullOrEmpty(state))
            {
                var markets = _marketService.GetMarketsByState(NhsRoute.PartnerId, state);
                list.AddRange(markets.Where(m => m.TotalQuickMoveInHomes > 0));
            }
            return list;
        }

        #endregion                

        

    }
}
