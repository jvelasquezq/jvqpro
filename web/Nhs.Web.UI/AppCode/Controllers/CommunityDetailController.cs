﻿using System.Collections.Generic;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects;
using Nhs.Search.Objects.Constants;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class CommunityDetailController : BaseDetailController
    {
        #region Member Variables

        private readonly IPartnerService _partnerService;
        private readonly IListingService _listingService;
        private readonly ICommunityService _communityService;
        private readonly IStateService _stateService;
        private readonly ILookupService _lookupService;
        private readonly IPathMapper _pathMapper;
        private readonly IBoylService _boylService;
        private readonly IApiService _apiService;
        private readonly IBrandService _brandService;
        private readonly IAffiliateLinkService _affiliateLinkService;
        private readonly IBuilderService _builderService;
        private readonly IMarketService _marketService;
        #endregion

        #region Constructor
        public CommunityDetailController(IPartnerService partnerService, IListingService listingService, ICommunityService communityService,
             IStateService stateService, ILookupService lookupService, IPathMapper pathMapper, IBoylService boylService,
            IBuilderService builderService, IMarketService marketService, IApiService apiService, IBrandService brandService, IAffiliateLinkService affiliateLinkService)
            : base(communityService, listingService, pathMapper)
        {
            _marketService = marketService;
            _partnerService = partnerService;
            _listingService = listingService;
            _communityService = communityService;
            _stateService = stateService;
            _lookupService = lookupService;
            _pathMapper = pathMapper;
            _boylService = boylService;
            _apiService = apiService;
            _brandService = brandService;
            _affiliateLinkService = affiliateLinkService;
            _builderService = builderService;
        }
        #endregion

        #region Private Methods

        private CommunityDetailTabs TabSelected(int homeStatus, bool showNewHomesExtraTab)
        {
            if (homeStatus == (int)HomeStatusType.QuickMoveIn) return CommunityDetailTabs.QuickMoveIn;

            if (homeStatus == -1) return CommunityDetailTabs.AllCommunities;

            return (showNewHomesExtraTab) ? CommunityDetailTabs.FilterCommunities : CommunityDetailTabs.AllCommunities;
        }

        private void BindAds(ref CommunityDetailViewModel model, Community community)
        {
            model.Globals.AdController.AddMarketParameter(model.MarketId);
            model.Globals.AdController.AddCommunityParameter(model.CommunityId);
            model.Globals.AdController.AddPriceParameter(community.PriceLow.ToType<int>(), community.PriceHigh.ToType<int>());
            model.Globals.AdController.AddStateParameter(community.StateAbbr);
            model.Globals.AdController.AddBuilderParameter(model.BuilderId);
            model.Globals.AdController.AddCityParameter(community.City);
            model.Globals.AdController.AddZipParameter(community.PostalCode);
        }

        private NhsPropertySearchParams GetSearchParams(int communityId, SortOrder sortOrder, int homeStatus, string listingTypeFlag)
        {
            return new NhsPropertySearchParams
            {
                CommunityId = communityId,
                PartnerId = NhsRoute.PartnerId,
                SortOrder = sortOrder,
                HomeStatus = homeStatus,
                ListingTypeFlag = listingTypeFlag,
                WebApiSearchType = WebApiSearchType.Exact.ToType<int>()
            };
        }

        private string GetAddCommunityToPlannerUrl(int communityId, int builderId)
        {
            var paramz = new List<RouteParam> {
                new RouteParam(RouteParams.Community, communityId),
                new RouteParam(RouteParams.Builder, builderId)};
            return string.Format(@"""{0}/{1}""", paramz.ToUrl("community"), NhsMvc.CommunityDetail.ActionNames.AddCommunityToUserPlanner);
            //TODO: Change from community to Pages.CommunityDetail
        }

        private static void LogCommunityHeaderClickAction(int communityId, int builderId, string eventCode)
        {
            var logger = new ImpressionLogger
            {
                CommunityId = communityId,
                BuilderId = builderId,
                PartnerId = NhsRoute.PartnerId.ToString(),
                Refer = UserSession.Refer
            };
            logger.LogView(eventCode);
        }

        private void SaveCommunityToPlanner(int communityId, int builderId, bool logAction)
        {
            var pl = new PlannerListing(communityId, ListingType.Community);
            if (!UserSession.UserProfile.Planner.SavedCommunities.Contains(pl))
                UserSession.UserProfile.Planner.AddSavedCommunity(communityId, builderId);

            if (logAction)
                LogCommunityHeaderClickAction(communityId, builderId, LogImpressionConst.AddCommunityToUserPlanner);
        }

        private static void LogCommunityNextStepsClickAction(int communityId, int builderId, string eventCode)
        {
            var logger = new ImpressionLogger
            {
                CommunityId = communityId,
                BuilderId = builderId,
                PartnerId = NhsRoute.PartnerId.ToString(),
                Refer = UserSession.Refer
            };
            logger.LogView(eventCode);
        }
        #endregion
    }
}
