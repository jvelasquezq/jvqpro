﻿using System.Collections.Generic;
using System.Web.Mvc;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Interface;
using Nhs.Web.UI.AppCode.Helper;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class PartialViewsController
    {
        /// <summary>
        /// Get the data for the type ahead 
        /// </summary>
        /// <param name="term"></param>
        /// <returns>List of Location</returns>
        public virtual JsonResult PartnerLocationsIndexMobile(string term)
        {
            var lst = new List<Location> {new Location {Name = LanguageHelper.CurrentLocation}};
            lst.AddRange(_partnerService.GetTypeAheadSuggestions(NhsRoute.PartnerId, term, true));
            
            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult ShareModal()
        {
            return PartialView(NhsMvc.Default.ViewsMobile.Common.PropertyDetail.Share);
        }
    }
}
