﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Bdx.Web.Api.Objects;
using Bdx.Web.Api.Objects.Constants;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Proxy;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
   public partial class SearchController : ApplicationController
    {
        //
        // GET: /Search/
        private readonly IPartnerService _partnerService;
        private readonly ILookupService _lookupService;
        private readonly IStateService _stateService;

        public SearchController(IPartnerService partnerService, ILookupService lookupService, IStateService stateService)
        {
            _stateService = stateService;
            _lookupService = lookupService;
            _partnerService = partnerService;
        }

        public virtual ActionResult Map()
        {
            return View(new BaseViewModel());
        }

        public virtual JsonResult GetPointsMap(string minLat, string minLng, string maxLat, string maxLng, string g)
        {
            var searchParams = new SearchParams();
            searchParams.WebApiSearchType= WebApiSearchType.Map;
            var partnerSitePassword = _partnerService.GetPartner(NhsRoute.PartnerId).LeadPostingPassword;
            var was = new WebApiServices
            {
                PartnerSitePassword = partnerSitePassword
            };
            
            was.AddParameter(ApiUrlConstV2.PartnerId, NhsRoute.PartnerId);
            was.AddParameter(ApiUrlConstV2.MinLat, minLat);
            was.AddParameter(ApiUrlConstV2.MinLng, minLng);
            was.AddParameter(ApiUrlConstV2.MaxLat, maxLat);
            was.AddParameter(ApiUrlConstV2.MaxLng, maxLng);
            var data = was.GetData<ApiCommonResultModel<List<ApiMapPoint>>>(WebApiMethods.CommunityLocations);
            var result = data.Result ?? new List<ApiMapPoint>();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        protected ActionResult GetInactivePropertyViewModel()
        {
            IDetailViewModel model = new CommunityDetailViewModel();
            model.States = (from s in _stateService.GetPartnerStates(NhsRoute.PartnerId)
                            select new SelectListItem { Text = s.StateName, Value = s.StateAbbr });
            model.PriceLowRange = (from l in _lookupService.GetCommonListItems(CommonListItem.MinPrice)
                                   select new SelectListItem { Text = l.LookupText, Value = l.LookupValue });
            model.PriceHighRange = (from l in _lookupService.GetCommonListItems(CommonListItem.MaxPrice)
                                    select new SelectListItem { Text = l.LookupText, Value = l.LookupValue });
            model.SearchText = string.Empty;
            model.IsInactive = true;
            if (Request.IsAjaxRequest())
                return PartialView(NhsMvc.Default.Views.Common.PropertyDetail.InactivePropertyForm, model);
            return View(model);
        }

        [HttpPost]
        public virtual ActionResult InvalidPropertySearch(CommunityDetailViewModel model)
        {
            if (string.IsNullOrEmpty(model.SearchText))
            {
                ModelState.AddModelError("SearchText", @"location required.");
                return GetInactivePropertyViewModel();
            }

            IList<RouteParam> @params = new List<RouteParam>();

            // Searching by Zip
            if (CommonUtils.IsZip(model.SearchText))
            {
                @params.Add(new RouteParam(RouteParams.SearchText, model.SearchText, RouteParamType.QueryString));
            }
            // Searching by city or zip
            else if (!string.IsNullOrEmpty(model.SearchText))
            {
                @params.Add(new RouteParam(RouteParams.SearchText, model.SearchText, RouteParamType.QueryString));
                @params.Add(new RouteParam(RouteParams.State, model.SelectedStateAbbr));
                UserSession.PersonalCookie.State = model.SelectedStateAbbr;
            }
            // Input error handler
            else
            {
                ModelState.AddModelError("SearchText", @"invalid data, city or zip required.");
                return GetInactivePropertyViewModel();
            }

            if (!string.IsNullOrEmpty(model.PriceLow))
            {
                @params.Add(new RouteParam(RouteParams.PriceLow, model.PriceLow));
                UserSession.PersonalCookie.PriceLow = int.Parse(model.PriceLow);
            }

            if (!string.IsNullOrEmpty(model.PriceHigh))
            {
                @params.Add(new RouteParam(RouteParams.PriceHigh, model.PriceHigh));
                UserSession.PersonalCookie.PriceHigh = int.Parse(model.PriceHigh);
            }

            //redirect to location handler page
            string redirect = @params.ToUrl(Pages.LocationHandler);

            return Json(new {redirect = true, redirecUrl = redirect});

        }
       
    }
}
