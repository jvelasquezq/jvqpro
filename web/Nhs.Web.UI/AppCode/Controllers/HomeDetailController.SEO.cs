﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class HomeDetailController
    {
        private void OverrideHomeDetailMeta(ISpec spec, IPlan plan, bool addNoIndexTag, HomeDetailViewModel viewModel, Community currentCommunity)
        {
            try
            {
                var metaRegistrar = new MetaRegistrar(_pathMapper);
                var metas = new List<MetaTag>();

                if (currentCommunity != null)
                    metas = metaRegistrar.NhsGetMetaTagsForSEOHomeDetail(currentCommunity.City,
                                                                         viewModel.MarketName,
                                                                         currentCommunity.CommunityName,
                                                                         currentCommunity.StateAbbr,
                                                                         viewModel.BrandName,
                                                                         plan.Bedrooms.ToString(),
                                                                         plan.Bathrooms.ToString(),
                                                                         string.IsNullOrEmpty(plan.PlanName)
                                                                                ? (spec != null
                                                                                     ? spec.PlanName
                                                                                     : string.Empty)
                                                                                : plan.PlanName,
                                                                         currentCommunity.PostalCode.Trim(),
                                                                         viewModel.IsPlan);

                if (metas != null && metas.Count > 0)
                {
                    if (!viewModel.IsPlan && spec != null)
                    {
                        var ind = metas.TakeWhile(meta => meta.Name != MetaName.Title).Count();
                        var nt = new MetaTag
                        {
                            Content = string.Format("{0} {1}", !string.IsNullOrEmpty((spec).Address1) 
                                ? (spec).Address1 + "," 
                                : string.Empty, metas[ind].Content).Trim(),
                            Name = MetaName.Title
                        };
                        metas[ind] = nt;
                    }
                }

                if (addNoIndexTag)
                    base.AddNoIndex(metaRegistrar, metas);

                var dpSeo = new SeoContentManager
                {
                    BrandID = viewModel.BrandId.ToString(CultureInfo.InvariantCulture),
                    BrandName = viewModel.BrandName,
                    City = viewModel.CommunityCity,
                    CommunityName = viewModel.CommunityName,
                    State = viewModel.State,
                    Market = viewModel.MarketName,
                    Zip = viewModel.ZipCode
                };
                var contentTags = new List<ContentTag>
                    {
                        new ContentTag(ContentTagKey.HomeTitle, viewModel.HomeTitle, false),
                        new ContentTag(ContentTagKey.BrandID, viewModel.BrandId.ToString(CultureInfo.InvariantCulture)),
                        new ContentTag(ContentTagKey.BrandName, viewModel.BrandName),
                        new ContentTag(ContentTagKey.CommunityName, viewModel.CommunityName),
                        new ContentTag(ContentTagKey.City, viewModel.CommunityCity),
                        new ContentTag(ContentTagKey.StateName, viewModel.State),
                        new ContentTag(ContentTagKey.MarketName, viewModel.MarketName),
                        new ContentTag(ContentTagKey.Zip, viewModel.ZipCode),
                        new ContentTag(ContentTagKey.PlanName,
                            string.IsNullOrEmpty(plan.PlanName)
                                ? (spec != null ? spec.PlanName : string.Empty)
                                : plan.PlanName)
                    };
                var dataSeo = dpSeo.GetSeoDetailPages(viewModel.IsPlan ? plan.PlanId : spec.SpecId, contentTags,
                    (viewModel.IsPlan ? SeoTemplateType.Plan : SeoTemplateType.Spec));
                metas = dataSeo.MetaTags;
                viewModel.Globals.H1 = dataSeo.H1;
                viewModel.Globals.Seo = dataSeo.Seo;

                OverrideDefaultMeta(viewModel, metas,
                                    FacebookHelper.GetFacebookMeta(viewModel.CommunityName, viewModel.BrandName, viewModel.ImageThumbnail, viewModel.Plan.PlanName));
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }
        }
    }
}
