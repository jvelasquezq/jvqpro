﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Bdx.Web.Api.Objects.Constants;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Proxy;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects.Constants;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Lib;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class CommunityDetailController
    {
        #region Action

        public virtual ActionResult SearchMobile(SearchParams searchParams, CommunityDetailTabs selectedTab, bool moreResults)
        {
            var model = new CommunityDetailViewModel();
            searchParams.SortBy = SortBy.HomeStatus;
            if (!moreResults)
            {
                searchParams.PageSize = 20;
                searchParams.PageNumber = 1;
                model.IsTriggerByTab = true;
                UserSession.PreviewsGroupingBarValueComDetail = "";
            }

            WebApiCommonResultModel<List<HomeItem>> resultsView;
            switch (selectedTab)
            {
                case CommunityDetailTabs.AllCommunities:
                    resultsView = GetCommunityHomeResultsFromApi(searchParams);
                    break;
                case CommunityDetailTabs.QuickMoveIn:
                    resultsView = GetCommunityHomeResultsFromApi(searchParams, true);
                    break;
                default:
                    resultsView = _listingService.GetHomesResultsFromApi<HomeItem>(searchParams, NhsRoute.PartnerId);
                    break;
            }
            //Get the number of pages that the current search
            if (!moreResults)
            {
                var numberOfPages = Math.Ceiling(resultsView.ResultCounts.HomeCount.ToType<decimal>() / searchParams.PageSize).ToType<int>();

                if (numberOfPages == 0)
                    numberOfPages = 1;
                model.TotalPages = numberOfPages;
            }



            var logger = new ImpressionLogger
            {
                MarketId = searchParams.MarketId,
                BuilderId = searchParams.BuilderId,
                CommunityId = searchParams.CommId,
                PartnerId = NhsRoute.PartnerId.ToType<string>()
            };

            foreach (var home in resultsView.Result)
            {
                if (home.IsSpec == 1)
                    logger.AddSpec(home.HomeId);
                else
                    logger.AddPlan(home.HomeId);
            }

            logger.LogView(LogImpressionConst.CommunityDetailHomeSearchViews);


            model.HomeResultsApiV2 = resultsView.Result.GroupBy(p => p.Status);


            return PartialView(NhsMvc.Default.ViewsMobile.CommunityDetail.CommunityDetailTabsHomes, model);
        }

        public virtual ActionResult GetMediaPlayerObjects(int communityId)
        {
            var model = GetMediaPlayerObjectsList(communityId);
            return PartialView(NhsMvc.Default.ViewsMobile.CommunityDetail.Gallery, model);
        }

        [HttpPost]
        public virtual JsonResult ShowRoute(int communityId, int builderId, int planId, int specId)
        {
            LogCommunityHeaderClickAction(communityId, builderId, LogImpressionConst.MobileCommunityDetailShowRoute);
            return new JsonResult { Data = "Click has been logged!" };
        }

        [HttpPost]
        public virtual JsonResult ShareByEmail(int communityId, int builderId, int planId, int specId)
        {
            LogCommunityHeaderClickAction(communityId, builderId, LogImpressionConst.MobileCommunityDetailShareByEmail);
            return new JsonResult { Data = "Click has been logged!" };
        }

        [HttpPost]
        public virtual JsonResult SocialMediaShare(int communityId, int builderId, int planId, int specId)
        {
            LogCommunityHeaderClickAction(communityId, builderId, LogImpressionConst.MobileCommunityDetailSocialMediaShare);
            return new JsonResult { Data = "Click has been logged!" };
        }

        
        [HttpPost]
        public virtual JsonResult LogBuilderShowCase(int communityId, int builderId, int planId, int specId)
        {
            LogCommunityHeaderClickAction(communityId, builderId, LogImpressionConst.BuilderWebsiteLink);
            return new JsonResult { Data = "Click has been logged!" };
        }
        
        #endregion

        #region Private Methods

        private List<MediaPlayerObject> GetMediaPlayerObjectsList(int communityId)
        {
            var videoTours =
            _communityService.GetVideoTourImages(communityId).Select(media => new MediaPlayerObject
            {
                Type = MediaPlayerObjectTypes.Link,
                SubType = MediaPlayerObjectTypes.SubTypes.ExternalVideo,
                Title = media.ImageTitle.EscapeSingleQuote(),
                Url = media.OriginalPath,
                Sort = media.ImageSequence.ToType<int>()
            }).OrderBy(m => m.Type).ThenBy(m => m.Sort);


            var objects = _communityService.GetMediaPlayerObjects(communityId, true, false, NhsRoute.ShowMobileSite).ToList();
            objects.AddRange(videoTours);
            var model = FillExtendedMediaObjectsInfo(objects, true);
            return model;
        }

        private CommunityDetailViewModel GetCommunityDetailModelMobile(Community community, bool addNoIndexTag)
        {
            UserSession.PreviewsGroupingBarValueComDetail = string.Empty;
            var vm = new CommunityDetailViewModel();

            string partnerMask = GetCommunityPartnerMaskValue(community.PartnerMask);
            var isBilled = false;

            if ("F" != partnerMask)
                isBilled = _communityService.GetCommunityBilledStatus(NhsRoute.PartnerId, community.BuilderId, community.CommunityId, community.MarketId);

            vm.IsBilled = isBilled;

            AssembleCommunityDetailBaseModel(vm, community);
            AssembleCommunityDetailModelMobile(vm, community);
            AssembleCommunityDetailCommunityStyles(vm, community);
            SetSeo(addNoIndexTag, vm, community);
            return vm;
        }

        private void AssembleCommunityDetailModelMobile(CommunityDetailViewModel model, Community community)
        {
            var brand = community.Brand ?? _brandService.GetBrandById(community.BrandId, NhsRoute.BrandPartnerId);
            var isShowCaseActive = brand.BrandShowCase != null && brand.BrandShowCase.StatusId == 1;
            var hasTheBrandActiveComms =
                _communityService.GetCommunitiesByBrandId(NhsRoute.PartnerId, brand.BrandId).Any();

            //Location Info
            model.ZipCode = community.PostalCode;
            model.State = community.State.StateName;
            model.StateAbbr = community.State.StateAbbr;
            model.Latitude = community.Latitude.ToType<double>();
            model.Longitude = community.Longitude.ToType<double>();

            //Phone
            model.PhoneNumber = community.PhoneNumber;

            //Brand Info
            model.BuilderName = community.Builder.BuilderName ?? string.Empty;
            model.BrandThumbnail = string.Concat(Configuration.ResourceDomain, brand.LogoMedium);
            model.BrandName = brand.BrandName ?? string.Empty;
            model.HasShowCaseInformation = brand.HasShowCase && isShowCaseActive && hasTheBrandActiveComms;

            //Green Programs
            model.GreenPrograms = _communityService.GetCommunityGreenPrograms(community.CommunityId).ToList();

            //Promos
            var promotions = _communityService.GetCommunityDetailPromotions(community.CommunityId, community.BuilderId);

            model.Promotions = promotions.Where(w => w.PromoTypeCode != "AGT").Select(p => new Promotion
            {
                PromoId = p.PromoID,
                PromoTextShort = p.PromoTextShort,
                PromoTextLong = p.PromoTextLong,
                PromoFlyerUrl = p.PromoFlyerURL,
                PromoType = p.PromoTypeCode,
                PromoEndDate = p.PromoEndDate,
                PromoStartDate = p.PromoStartDate,
                PromoUrl = p.PromoURL
            }).ToList();

            //Create a copy of the Search Parameters, in that way the originals don't get affected
            //If the SearchParametersV2 is NULL will create a new SearchParams object
            var searchParams = UserSession.SearchParametersV2.DeepCloneObject<SearchParams>() ?? new SearchParams();
            searchParams.WebApiSearchType = WebApiSearchType.Exact;
            searchParams.CommId = model.CommunityId;
            searchParams.PartnerId = NhsRoute.PartnerId;
            searchParams.BuilderId = model.BuilderId;
            searchParams.MarketId = model.MarketId;
            searchParams.PageSize = 20;
            searchParams.PageNumber = 1;
            searchParams.SortBy = SortBy.HomeStatus;

            var seletedTab = CommunityDetailTabs.FilterCommunities;
            var resultsView = _listingService.GetHomesResultsFromApi<HomeItem>(searchParams, NhsRoute.PartnerId);
            var filteredHomesCount = resultsView.ResultCounts.HomeCount;

            //if is 0 result the system searh with out the filters
            if (filteredHomesCount == 0 || filteredHomesCount == community.HomeCount)
            {
                filteredHomesCount = 0;
                seletedTab = CommunityDetailTabs.AllCommunities;
                resultsView = GetCommunityHomeResultsFromApi(searchParams);
                //resultsView = GetCommunityHomeResultsFromApi(searchParams, true);
                //if (resultsView.ResultCounts.HomeCount == 0)
                //{
                //    seletedTab=CommunityDetailTabs.AllCommunities;
                //    resultsView = GetCommunityHomeResultsFromApi(searchParams);
                //}
            }

            var hotHomes = new List<HomeItem>();
            if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
            {
                hotHomes = resultsView.Result.Where(r => r.IsHotHome > 0).Take(2).Select(r =>
                {
                    if (r.IsSpec == 1)
                    {
                        var spec = _listingService.GetSpec(r.HomeId);
                        r.HotHomeDescription = spec.HotHomeDescription;
                    }
                    else
                    {
                        var plan = _listingService.GetPlan(r.HomeId);
                        r.HotHomeDescription = plan.HotHomeDescription;
                    }
                    return r;
                }).ToList();
            }


            model.HotHomesApi = hotHomes;

            model.AllNewHomesCount = community.HomeCount.ToType<int>();
            model.QuickMoveInCount = community.QuickMoveInCount.ToType<int>();
            model.FilteredHomesCount = filteredHomesCount;
            model.SelectedTab = seletedTab;
            model.PriceLow = community.PriceLow.ToType<string>();
            model.PriceHigh = community.PriceHigh.ToType<string>();
            model.MarketName = community.Market.MarketName;
            model.CorporationName = community.Builder.ParentBuilder == null
                                         ? model.BuilderName
                                         : community.Builder.ParentBuilder.BuilderName ?? string.Empty;

            //Get the number of pages that the current search
            var numberOfPages = Math.Ceiling(resultsView.ResultCounts.HomeCount.ToType<decimal>() / searchParams.PageSize).ToType<int>();

            if (numberOfPages == 0)
                numberOfPages = 1;
            model.TotalPages = numberOfPages;

            var sortedHomes = new SortHomeActions().SortOptions[SortOrder.Status](resultsView.Result);
            var logger = new ImpressionLogger
            {
                MarketId = model.MarketId,
                BuilderId = model.BuilderId,
                CommunityId = model.CommunityId,
                PartnerId = NhsRoute.PartnerId.ToType<string>()
            };

            foreach (var home in sortedHomes)
            {
                if (home.IsSpec == 1)
                    logger.AddSpec(home.HomeId);
                else
                    logger.AddPlan(home.HomeId);
            }

            logger.LogView(LogImpressionConst.CommunityDetailHomeSearchViews);

            var results = sortedHomes.GroupBy(p => p.Status);


           
            model.HomeResultsApiV2 = results;

            model.SearchParametersJSon = searchParams.ToJson();
            model.HoursOfOperation = community.SalesOffice.HoursOfOperation.ToType<string>();
            if (UserSession.UserProfile.IsLoggedIn())
            {
                var pl = new PlannerListing(model.CommunityId, model.BuilderId, ListingType.Community);
                if (UserSession.UserProfile.Planner.SavedCommunities.Contains(pl))
                    model.SavedToPlanner = true;
            }

            model.LogActionMethodUrl = GetMethodUrl(community.CommunityId, community.BuilderId);
        }

        #endregion
    }
}