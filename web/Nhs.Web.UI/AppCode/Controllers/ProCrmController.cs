﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Utility.Constants;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class ProCrmController : MyAccountController
    {
        private readonly IProCrmService _proCrmService;
        private readonly IPartnerService _partnerService;
        private readonly IStateService _stateService;
        private readonly IMarketService _marketService;
        private readonly IUserProfileService _userProfileService;
        private readonly ICommunityService _communityService;
        private readonly IListingService _listingService;
        private readonly ISearchAlertService _searchAlertService;

        public ProCrmController(IProCrmService proCrmService, IUserProfileService userService,
            IPartnerService partnerService,
            IStateService stateService, IMarketService marketService,
            IUserProfileService userProfileService, ICommunityService communityService, IListingService listingService,
            IPlannerService plannerService, ISearchAlertService searchAlertService, IPathMapper pathMapper)
            : base(userService, plannerService, marketService, communityService, listingService, searchAlertService, pathMapper)
        {
            _proCrmService = proCrmService;
            _partnerService = partnerService;
            _stateService = stateService;
            _marketService = marketService;
            _userProfileService = userProfileService;
            _communityService = communityService;
            _listingService = listingService;
            _searchAlertService = searchAlertService;
        }

        #region Crm
        #region My Clients Page/ Client List
        /// <summary>
        /// Show the list of active clients
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult ShowMyClients()
        {
            var returnInvalid = RedirectInvalidRequest();
            if (returnInvalid != null)
            {
                return returnInvalid;
            }
            return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.ShowMyClients, new ProCrmViewModel());
        }

        /// <summary>
        /// Get the information for the list of clients
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult GetClients([DataSourceRequest] DataSourceRequest request,
                                               ProCrmClientsStatusType clientsStatus = ProCrmClientsStatusType.Deleted)
        {
            var userId = GetUserGuid();
            var changePage = false;
            if (clientsStatus == ProCrmClientsStatusType.Deleted && UserSession.ProCrmClientsStatus == ProCrmClientsStatusType.Deleted)
                clientsStatus = ProCrmClientsStatusType.Active;
            else if (UserSession.ProCrmClientsStatus != ProCrmClientsStatusType.Deleted && clientsStatus == ProCrmClientsStatusType.Deleted)
                clientsStatus = UserSession.ProCrmClientsStatus;

            if (UserSession.ProCrmClientsStatus != clientsStatus)
                changePage = true;

            UserSession.ProCrmClientsStatus = clientsStatus;

            var masterClientList = clientsStatus == ProCrmClientsStatusType.Active
                                       ? _proCrmService.GetMyClientList(userId, ProCrmClientsStatusType.Active)
                                       : _proCrmService.GetMyClientList(userId);

            var clients = GetClientsInfoModel(masterClientList);
            if (changePage && clientsStatus == ProCrmClientsStatusType.Active)
                request.Page = 1;


            #region Hack Sorting to use Last Name
            if (request.Sorts != null && request.Sorts.Any())
            {
                var sort = request.Sorts.FirstOrDefault();

                if (sort != null && sort.Member == "FullName")
                {
                    //hack the Member to use the Last Name when is sorting
                    sort.Member = "LastName";
                }
            }
            #endregion

            return Json(clients.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Add/Edit Client Modal
        /// <summary>
        /// Action for the add client modal
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult AddClientModal()
        {
            var model = new DetailsClientInfo();
            var stateList = _stateService.GetAllStates().ToList();
            stateList.Insert(0, new State { StateAbbr = string.Empty, StateName = "Choose" });

            model.StateList = new SelectList(stateList, "StateAbbr", "StateAbbr");
            return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.MyClients.PartialAddEditClientModal, model);
        }

        /// <summary>
        /// Action for the edit client modal
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult EditClientModal(long clientId)
        {
            var client = _proCrmService.GetClientDetails(clientId, GetUserGuid());
            var model = new DetailsClientInfo { IsEditMode = true };

            if (client == null) return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.MyClients.PartialAddEditClientModal, model);

            model.ClientId = clientId;
            model.Status = client.AccountStatus;
            model.City = client.City;
            model.Address = client.Address;
            model.FirstName = client.FirstName;
            model.LastName = client.LastName;
            model.Notes = client.Notes;
            model.RelatedFirstName = client.RelatedFirstName;
            model.RelatedLastName = client.RelatedLastName;
            model.State = client.State;
            model.ZipCode = client.PostalCode;

            var stateList = _stateService.GetAllStates().ToList();
            stateList.Insert(0, new State { StateAbbr = string.Empty, StateName = "Choose" });
            model.StateList = new SelectList(stateList, "StateAbbr", "StateAbbr");
            model.EmailList = client.ClientEmails.Select(p => new EmailList { Email = p.Email, EmailType = p.EmailType }).ToList();

            if (!client.ClientPhones.Any()) return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.MyClients.PartialAddEditClientModal, model);

            var phone = client.ClientPhones.FirstOrDefault();
            if (phone != null)
            {
                model.Phone1 = phone.Phone;
                model.Phone1Type = phone.PhoneType;
            }

            if (client.ClientPhones.Count <= 1) return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.MyClients.PartialAddEditClientModal, model);

            phone = client.ClientPhones.ToArray()[1];
            if (phone == null) return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.MyClients.PartialAddEditClientModal, model);

            model.Phone2 = phone.Phone;
            model.Phone2Type = phone.PhoneType;
            return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.MyClients.PartialAddEditClientModal, model);
        }

        /// <summary>
        /// Action for the post of add client modal
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual ActionResult AddClientModal(DetailsClientInfo model)
        {
            return AddEditClientModal(model);
        }

        /// <summary>
        /// Action for the post of edit client modal
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual ActionResult EditClientModal(DetailsClientInfo model)
        {
            return AddEditClientModal(model);
        }

        #region Private Methos Add/ Edit Client
        private ActionResult AddEditClientModal(DetailsClientInfo model)
        {
            Dictionary<string, string> emailDuplicate;
            if (!_proCrmService.EmailExists(GetUserGuid(), model.ClientId, model.EmailList.Select(p => p.Email).ToList(), out emailDuplicate) ||
                model.EmailList == null || !model.EmailList.Any())
                return InsertEditClient(model);

            foreach (var email in emailDuplicate)
                ModelState.AddModelError(Guid.NewGuid().ToString(), string.Format("Raise warning: {0} already exists for {1} client", email.Value, email.Key));

            var stateList = _stateService.GetAllStates().ToList();
            stateList.Insert(0, new State { StateAbbr = string.Empty, StateName = "Choose" });
            model.StateList = new SelectList(stateList, "StateAbbr", "StateAbbr");

            return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.MyClients.AddEditClientModal, model);
        }

        private JavaScriptResult InsertEditClient(DetailsClientInfo model)
        {
            var client = new AgentClient
            {
                AccountStatus = (byte)model.Status,
                City = model.City,
                Address = model.Address,
                DateCreated = DateTime.Now,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Notes = model.Notes,
                RelatedFirstName = model.RelatedFirstName,
                RelatedLastName = model.RelatedLastName,
                State = model.State,
                UserId = UserSession.UserProfile.UserID,
                PostalCode = model.ZipCode,
                ClientId = (int)model.ClientId
            };

            var emails = model.EmailList.Where(p => !string.IsNullOrWhiteSpace(p.Email))
                              .Select(p =>
                                  new ClientEmail
                                  {
                                      Email = p.Email,
                                      EmailType = p.EmailType == "Choose" ? "" : p.EmailType
                                  });

            var phones = new List<ClientPhone>();
            if (!string.IsNullOrWhiteSpace(model.Phone1))
                phones.Add(new ClientPhone { Phone = model.Phone1, PhoneType = model.Phone1Type });

            if (!string.IsNullOrWhiteSpace(model.Phone2))
                phones.Add(new ClientPhone { Phone = model.Phone2, PhoneType = model.Phone2Type });

            var valid = model.IsEditMode
                            ? _proCrmService.UpdateClient(client, emails, phones)
                            : _proCrmService.InsertClient(client, emails, phones);
            if (valid)
            {

                var url =
                    new List<RouteParam>
                        {
                            new RouteParam(RouteParams.ClientId, client.ClientId, RouteParamType.QueryString, true, true)
                        }
                        .ToUrl(Pages.GetClientsDetailInfo);

                return !model.IsEditMode
                           ? JavaScript("crm.UpClients()")
                           : JavaScript("crm.UpClientsInfo('" + url + "')");
            }
            return JavaScript("window.parent.tb_remove();alert('Is a problem try to update/insert the information');");
        }
        #endregion

        #endregion

        #region Client Detail Page
        /// <summary>
        /// Show the page of Client Detail
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public virtual ActionResult ShowClientDetail(int clientId)
        {
            var returnInvalid = RedirectInvalidRequest();
            if (returnInvalid != null)
            {
                return returnInvalid;
            }

            var clientDetails = _proCrmService.GetClientDetails(clientId, GetUserGuid());
            if (clientDetails == null)
            {
                return RedirectTo404();
            }

            var model = GetFavoritesViewModel();
            model.ShowMyClients = true;
            model.SelectClientsTab = true;
            model.IsClientDetailPage = true;
            model.SelectMyAccount = false;
            model.ClientId = clientId;

            model.ProCrmViewModel = new ProCrmViewModel
            {
                ClientInfo = clientDetails,
                ClientFavorites = clientDetails.AccountStatus == (int)ProCrmClientsStatusType.Deleted
                            ? null : GetFavoritesClient(clientDetails)
            };
            model.ProCrmViewModel.ClientFavorites.IsClientDetailPage = true;
            model.ProCrmViewModel.ClientFavorites.ClientId = clientId;
            return View(model);
        }


        public virtual JsonResult ShowAllActivities([DataSourceRequest] DataSourceRequest request, int clientId)
        {
            var clientDetails = _proCrmService.GetClientDetails(clientId, GetUserGuid()) ?? new AgentClient();
            var activities = clientDetails.ClientTasks.OrderByDescending(p => p.DateLastChanged)
                                          .Select(clientTask => new ActivityInformation
                                          {
                                              TaskType = clientTask.TaskType,
                                              Description = clientTask.Description.Length > 84
                                                         ? clientTask.Description.CutLenghtByWords(85, "...")
                                                         : clientTask.Description,
                                              ClientTaskId = clientTask.ClientTaskId,
                                              TaskDate = clientTask.TaskDate.ToType<DateTime>(),
                                              ClientId = clientDetails.ClientId,
                                              PropertyList =
                                                  clientTask.ClientTaskListingNames.Where(p => p.PropertyId > 0)
                                                            .Select(
                                                                p =>
                                                                new ActivityListingInformation
                                                                {
                                                                    Id = p.PropertyId,
                                                                    Type = p.ListingType,
                                                                    Name = p.PropertyName,
                                                                    CommunityId = p.CommunityId.ToType<int>(),
                                                                    CommunityName = p.CommunityName
                                                                })
                                          });

            return Json(activities.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        #region Client Detail Header

        /// <summary>
        /// Changes the status of a client, to inactive, active or delete
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public virtual JsonResult ChangeStatusClient(long clientId, int status)
        {
            var clientStatus = status.FromInt<ProCrmClientsStatusType>();
            return
                Json(
                    _proCrmService.UpdateStatusClient(clientId, clientStatus),
                    JsonRequestBehavior.AllowGet);
        }

        #region Private Methods
        /// <summary>
        /// Get the information of the client, for the client detail page 
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public virtual ActionResult GetClientsDetailInfo(int clientId)
        {
            var userId = GetUserGuid();
            var clients = _proCrmService.GetClientDetails(clientId, userId);
            return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.ClientDetail.PartialCrmClientInfoDetail, clients);
        }
        #endregion

        #endregion

        #region Client Detail Tasks
        public virtual ActionResult ShowAddEditModal(int clientId, int taskId = 0)
        {
            var task = _proCrmService.GetTask(clientId, GetUserGuid(), taskId);

            var model = new ClientTaskViewModel
            {
                ClientId = clientId,
                DueAmPm = DateTime.Now.ToString("tt").ToUpper(),
                DueDate = DateTime.Now.ToString("MM/dd/yyyy"),
                DueTimeHour = DateTime.Now.ToString("hh"),
                DueTimeMinutes = DateTime.Now.ToString("mm"),
                DueTimeSpan = DateTime.Now.TimeOfDay,
                //this is for add a default 1 listing null in the view
                ListingList = new List<ListingTaskViewModel>
                        {
                            new ListingTaskViewModel {Id = -1, Name = ""}
                        }
            };

            if (task != null)
            {
                model.TaskType = task.TaskType;
                model.ProCrmTaskTypes = task.TaskType.FromString<ProCrmTaskTypes>();
                model.ClientTaskId = task.ClientTaskId;
                model.ClientId = task.ClientId;
                model.DateLastChanged = task.DateLastChanged;
                model.TaskCompleteDate = task.TaskCompleteDate;
                model.Description = task.Description;
                model.TaskDate = task.TaskDate;
                model.Notes = task.Notes;
                model.DueAmPm = task.TaskDate.HasValue ? task.TaskDate.Value.ToString("tt").ToUpper() : model.DueAmPm;
                model.DueDate = task.TaskDate.HasValue ? task.TaskDate.Value.ToString("MM/dd/yyyy") : model.DueDate;
                model.DueTimeHour = task.TaskDate.HasValue ? task.TaskDate.Value.ToString("hh") : model.DueTimeHour;
                model.DueTimeMinutes = task.TaskDate.HasValue ? task.TaskDate.Value.ToString("mm") : model.DueTimeMinutes;
                model.DueTimeSpan = task.TaskDate.HasValue ? task.TaskDate.Value.TimeOfDay : DateTime.Now.TimeOfDay;
                model.ClientTaskListings = task.ClientTaskListings;

                if (task.ClientTaskListings.Any())
                {
                    //get the community name for the PropertyId
                    model.ListingList = new List<ListingTaskViewModel>();

                    foreach (var clientTaskListing in task.ClientTaskListings)
                    {
                        var comm = _communityService.GetCommunity(clientTaskListing.PropertyId);
                        if (comm != null)
                        {
                            model.ListingList.Add(new ListingTaskViewModel
                            {
                                Name = string.Format("{0}, {1} {2}", comm.CommunityName, comm.Market.MarketName, comm.State.StateAbbr),
                                Id = clientTaskListing.PropertyId,
                                TaskListingId = clientTaskListing.TaskListingId
                            });
                        }
                    }

                    if (!model.ListingList.Any())
                        model.ListingList = new List<ListingTaskViewModel>
                            {
                                new ListingTaskViewModel {Id = -1, Name = ""}
                            };
                }
            }

            return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.ClientDetail.PartialEditAddTasks, model);
        }

        [HttpPost]
        public virtual JavaScriptResult SaveTask(ClientTaskViewModel model)
        {
            try
            {
                model.Description = _proCrmService.FillDefaultDescription(model.Description, model.ClientId, GetUserGuid(), model.ProCrmTaskTypes);

                var task = new ClientTask
                {
                    ClientId = model.ClientId,
                    Description = model.Description,
                    Notes = model.Notes,
                    ClientTaskId = model.ClientTaskId,
                    TaskType = model.ProCrmTaskTypes.ToString(),
                    TaskDate = Convert.ToDateTime(string.Concat(model.DueDate, " ", model.DueTimeHour + ":" + model.DueTimeMinutes + " " + model.DueAmPm)),
                    DateLastChanged = DateTime.Now,
                    TaskCompleteDate = model.TaskCompleteDate
                };

                var clientTaskListingList = model.ListingList.Where(p => p.Id > 0 && !p.Delete).Select(listing => new ClientTaskListing
                {
                    ClientTaskId = model.ClientTaskId,
                    ListingType = "C",
                    PropertyId = listing.Id,
                    TaskListingId = listing.TaskListingId
                });

                var clientTaskListingListDelete = model.ListingList.Where(p => p.Delete).Select(listing => listing.TaskListingId);
                string error;
                var success = model.ClientTaskId > 0 ? _proCrmService.UpdateTask(task, clientTaskListingList, clientTaskListingListDelete, out error)
                                                     : _proCrmService.InsertTask(task, clientTaskListingList, task.ClientId, out error);

                if (success) return JavaScript("crm.InsertUpdateTask()");
                ModelState.AddModelError("form", error);
                model.DueAmPm = task.TaskDate != null ? task.TaskDate.Value.ToString("tt").ToUpper() : string.Empty;
                model.DueDate = task.TaskDate != null ? task.TaskDate.Value.ToString("MM/dd/yyyy") : string.Empty;
                model.DueTimeHour = task.TaskDate != null ? task.TaskDate.Value.ToString("hh") : string.Empty;
                model.DueTimeMinutes = task.TaskDate != null ? task.TaskDate.Value.ToString("mm") : string.Empty;
                return JavaScript("alert('" + error + "')");
            }
            catch (Exception)
            {
                return JavaScript("alert('There is an error in your information, Description and Date/Time are required.');");
            }

        }

        public virtual JsonResult DeleteTask(int clientId, int taskId)
        {
            var success = true;
            if (taskId > 0)
            {
                success = _proCrmService.DeleteTask(clientId, taskId, GetUserGuid());
            }
            return Json(success, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Client Detail Favorites

        #region Private Methods
        private FavoritesViewModel GetFavoritesClient(AgentClient client)
        {
            var planIds = new List<string>();
            var specIds = new List<string>();
            var communityIds = new List<string>();
            foreach (var clientPlanner in client.ClientPlanners)
            {
                switch (EnumConversionUtil.StringToListingType(clientPlanner.ListingType))
                {
                    case ListingType.Plan:
                        planIds.Add(clientPlanner.PropertyId.ToType<string>());
                        break;
                    case ListingType.Spec:
                        specIds.Add(clientPlanner.PropertyId.ToType<string>());
                        break;
                    case ListingType.Community:
                        communityIds.Add(clientPlanner.PropertyId.ToType<string>());
                        break;

                }
            }
            var model = new FavoritesViewModel();
            var saveProp = GetFavorites(communityIds, planIds, specIds);
            model.FavoritesSaveComm = saveProp.FavoritesSaveComm;
            model.FavoritesSaveHome = saveProp.FavoritesSaveHome;
            return model;
        }
        #endregion

        #endregion
        #endregion

        #region Add New Clients and Emails Modal

        [HttpGet]
        public virtual JsonResult ClientsAutoCompleteEmail(string text, int clientId)
        {
            IEnumerable<ClientEmail> masterClientEmails = _proCrmService.GetClientEmails(GetUserGuid()).OrderBy(p => p.Email);
            if (text == " ")
            {
                masterClientEmails = masterClientEmails.Take(5);
                text = "";
            }
            if (clientId > 0)
                masterClientEmails = masterClientEmails.Where(p => p.ClientId == clientId);

            var tempdata = masterClientEmails.Where(p => p.Email.ToLowerCase().Contains(text.ToLowerCase()))
                                  .Select(
                                      p =>
                                      new
                                      {
                                          label = p.Email,
                                          value = p.ClientId,
                                          name = p.AgentClient.FirstName + " " + p.AgentClient.LastName,
                                          emails = string.Join("; ", p.AgentClient.ClientEmails.Select(f => f.Email))
                                      }).Take(20);
            var dataReturn = tempdata.ToArray();
            return Json(dataReturn, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual JsonResult ClientsAutoComplete(string text, bool isSendBrochure)
        {
            IEnumerable<AgentClient> clients = _proCrmService.GetMyClientList(GetUserGuid())
                .OrderByDescending(od => od.DateLastChanged);

            if (text == " ")
            {
                clients = clients.Take(5);
                text = "";
            }
            var tempdata = clients.Where(
                p => (p.FirstName + " " + p.LastName).ToLowerCase().Contains(text.ToLowerCase())
                     || (p.RelatedFirstName + " " + p.RelatedLastName).ToLowerCase().Contains(text.ToLowerCase()))
                                  .Select(
                                      p =>
                                      new
                                      {
                                          label = p.FirstName + " " + p.LastName,
                                          value = p.ClientId,
                                          relatedName = p.RelatedFirstName + " " + p.RelatedLastName,
                                          emails = isSendBrochure ? string.Join("; ", p.ClientEmails.Select(f => f.Email)) : ""
                                      }).Take(20);

            var dataReturn = tempdata.ToArray();
            return Json(dataReturn, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public virtual ActionResult AddNewClientsToYourList(AddNewClientsViewModel model)
        {
            var clientsAlreadyProcess = new List<string>();
            var proccessTime = DateTime.Now;
            var baseTask = GetSendCustomBrochure(model);

            #region New Clients
            foreach (var inputClient in model.NewClients.Where(p => p.SelectClient || !string.IsNullOrWhiteSpace(p.PrimaryClient))
                                                        .OrderByDescending(p => !string.IsNullOrWhiteSpace(p.PrimaryClient))
                                                        .Where(inputClient => !clientsAlreadyProcess.Contains(inputClient.Name)))
            {
                var task = new ClientTask
                {
                    Description = baseTask.Description,
                    Notes = baseTask.Notes,
                    TaskType = baseTask.TaskType,
                    TaskDate = baseTask.TaskDate,
                    DateLastChanged = baseTask.DateLastChanged,
                    TaskCompleteDate = baseTask.TaskCompleteDate
                };

                foreach (var clientTaskListing in task.ClientTaskListings)
                    task.ClientTaskListings.Add(clientTaskListing);

                if (StringHelper.IsNumeric(inputClient.PrimaryClient))
                {
                    task.ClientId = inputClient.PrimaryClient.ToType<int>();
                    var emails = inputClient.Email.Split(';').Select(p => new ClientEmail { Email = p });
                    _proCrmService.RelateClient(inputClient.PrimaryClient.ToType<int>(), inputClient.Name.FirstPart(" "),
                                                inputClient.Name.LastPart(" "), emails, task);
                }
                else
                {
                    var client = new AgentClient
                    {
                        AccountStatus = 1,
                        DateCreated = proccessTime,
                        FirstName = inputClient.Name.FirstPart(" "),
                        LastName = inputClient.Name.LastPart(" "),
                        UserId = UserSession.UserProfile.UserID
                    };

                    IEnumerable<string> emails = inputClient.Email.Split(';');
                    if (!string.IsNullOrWhiteSpace(inputClient.PrimaryClient))
                    {
                        client.RelatedFirstName = inputClient.PrimaryClient.FirstPart(" ");
                        client.RelatedLastName = inputClient.PrimaryClient.LastPart(" ");
                        var relateClient = model.NewClients.FirstOrDefault(p => p.Name == inputClient.PrimaryClient);
                        if (relateClient != null)
                            emails = emails.Union(relateClient.Email.Split(';'));

                        clientsAlreadyProcess.Add(inputClient.PrimaryClient);
                    }

                    var emailsList = emails.Select(p => new ClientEmail { Email = p });
                    _proCrmService.InsertClient(client, emailsList, null, task);
                }
                clientsAlreadyProcess.Add(inputClient.Name);
            }
            #endregion

            foreach (var inputClient in model.NewEmails.Where(p => p.SelectClient))
            {
                var task = new ClientTask
                {
                    Description = baseTask.Description,
                    Notes = baseTask.Notes,
                    TaskType = baseTask.TaskType,
                    TaskDate = baseTask.TaskDate,
                    DateLastChanged = baseTask.DateLastChanged,
                    TaskCompleteDate = baseTask.TaskCompleteDate
                };

                foreach (var clientTaskListing in task.ClientTaskListings)
                    task.ClientTaskListings.Add(clientTaskListing);

                var emailsList = inputClient.Email.Split(';').Select(p => new ClientEmail { Email = p });
                _proCrmService.InsertEmailsToClient(inputClient.ClientId, emailsList, task);
            }

            var script = new StringBuilder();
            script.AppendLine(model.IsMvc ? "setTimeout('tb_remove();',500);" : ModalWindowHelper.GetScriptCloseWindow());
            script.AppendLine("try{window.parent.commResults.UnCheckAll();}catch(ex){}");
            return Json(new { JavaScriptCode = script.ToString() });
            //return JavaScript(script.ToString());
        }
        #endregion

        #region Contac to Builder
        public virtual ActionResult ContactBuilderAndRequestAppointmentClientModal(CommunityDetailViewModel viewmodel)
        {
            var model = new ContactBuilderAndRequestAppointmentClient
            {
                BuilderName = viewmodel.BuilderName,
                HomeTitle = viewmodel.HomeTitle,
                CommunityName = viewmodel.CommunityName,
                Message = viewmodel.Message,
                PlanId = viewmodel.PlanId,
                SpecId = viewmodel.SpecId,
                BuilderId = viewmodel.BuilderId,
                IsRequestAppointment = viewmodel.ScheduleAppointment || viewmodel.RequestAnAppointment,
                IsContactBuilder = viewmodel.GeneralInquiry,
                CommunityId = viewmodel.CommunityId,
                PropertyId =
                    viewmodel.PlanId == 0 && viewmodel.SpecId == 0
                        ? viewmodel.CommunityId
                        : viewmodel.SpecId > 0 ? viewmodel.SpecId : viewmodel.PlanId
            };

            GetLastClients(model);
            if (Request.IsAjaxRequest())
            {
                return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.ContactBuilderAndRequestAppointmentClient.PartialContactBuilderAndRequestAppointmentClient, model);
            }
            return View(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.ContactBuilderAndRequestAppointmentClient.PartialContactBuilderAndRequestAppointmentClient, model);
        }

        [HttpPost]
        public virtual ActionResult ContactBuilderAndRequestAppointmentClientModal(ContactBuilderAndRequestAppointmentClient model)
        {
            var clients = model.Clients.Where(p => p.ClientId > 0 && !string.IsNullOrWhiteSpace(p.Name)).Select(p => p.ClientId).AsQueryable();

            if (clients.Any())
            {
                var task = GetClientTaskFromModel(model);
                _proCrmService.UpdateClients(clients, task);
            }

            model.Clients.RemoveAll(p => p.ClientId > 0 || string.IsNullOrWhiteSpace(p.Name));

            if (model.Clients.Any())
            {
                var viewModel = new AddEmailToNewClients().CopyFrom(model);

                viewModel.Clients = model.Clients.Select(p => new AddClientToList
                {
                    Name = p.Name,
                    Email = p.Email,
                    ClientId = p.ClientId
                }).ToList();

                BuildClientSourceList(viewModel);
                ModelState.Clear();
                return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.MyClients.AddEmailToNewClientsModalWithGrouping,
                                   viewModel);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        private void BuildClientSourceList(AddEmailToNewClients viewModel)
        {
            var masterClients = _proCrmService.GetMyClientList(UserSession.UserProfile.UserID, ProCrmClientsStatusType.Active);

            viewModel.ClientSourceList.AddRange(
                masterClients.Where(
                    p => string.IsNullOrWhiteSpace(p.RelatedFirstName) && string.IsNullOrWhiteSpace(p.RelatedLastName)).Select(
                        p =>
                        new SelectListItem
                        {
                            Value = p.ClientId.ToString(CultureInfo.InvariantCulture),
                            Text = string.Format("{0} {1}", p.FirstName, p.LastName)
                        }).ToList());
        }

        [HttpPost]
        public virtual ActionResult SendMessageToBuilder(CommunityDetailViewModel model)
        {
            base.SendMessageToBuilder(model);
            return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.LeadForm.LeadSectionConfirmationPro, model);
        }

        [HttpPost]
        public virtual JsonResult SendMessageToBuilderAgCommision(CommunityDetailViewModel model)
        {
            var message = "OK";
            var foundError = false;
            try
            {
                if (ModelState.IsValid)
                {
                    SendMessageAgCompensation(model);
                }
            }
            catch (Exception error)
            {
                message = error.Message;
                foundError = true;
            }
            return Json(new { ok = message, hasError = foundError }, JsonRequestBehavior.DenyGet);
        }

        public virtual ActionResult ReturnToBuilderMessage(CommunityDetailViewModel model)
        {
            return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.LeadForm.LeadSectionFieldsPro, model);
        }
        #endregion

        #region Client Favorites

        public virtual ActionResult SaveToFavoritesClientModal(string listingType, int propertyId, int communityId, int builderId, bool isNextSteps, bool isCommunityResultsPage)
        {
            var model = new AddToFavoritesClient
            {
                ListingType = listingType.ToUpper(),
                PropertyId = propertyId,
                BuilderId = builderId,
                CommunityId = communityId,
                IsCommunityResultsPage = isCommunityResultsPage,
                IsNextSteps = isNextSteps
            };
            if (listingType == "P")
                model.PlanId = propertyId;
            if (listingType == "S")
                model.SpecId = propertyId;

            GetLastClients(model);

            return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.SaveToFavoritesClientModal, model);
        }

        [HttpPost]
        public virtual ActionResult SaveToFavoritesClientModal(AddToFavoritesClient model)
        {
            var clientPlanner = model.Clients.Where(p => p.ClientId > 0 && !string.IsNullOrWhiteSpace(p.Name))
                                     .Select(p => new ClientPlanner
                                     {
                                         ClientId = p.ClientId,
                                         ListingType = model.ListingType,
                                         PropertyId = model.PropertyId
                                     }).AsQueryable();

            if (clientPlanner.Any())
            {
                _proCrmService.InsertToFavoritesClient(clientPlanner);
            }

            model.Clients.RemoveAll(p => p.ClientId > 0 || string.IsNullOrWhiteSpace(p.Name));

            if (model.Clients.Any())
            {
                var viewModel = new AddEmailToNewClients().CopyFrom(model);
                viewModel.Clients = model.Clients.Select(p => new AddClientToList { Name = p.Name }).ToList();
                viewModel.IsFromSaveToFavorites = true;

                var masterClients = _proCrmService.GetMyClientList(UserSession.UserProfile.UserID, ProCrmClientsStatusType.Active);

                viewModel.ClientSourceList.AddRange(
                    masterClients.Where(
                    p => string.IsNullOrWhiteSpace(p.RelatedFirstName) && string.IsNullOrWhiteSpace(p.RelatedLastName)).Select(
                        p =>
                        new SelectListItem
                        {
                            Value = p.ClientId.ToString(CultureInfo.InvariantCulture),
                            Text = string.Format("{0} {1}", p.FirstName, p.LastName)
                        }).ToList());

                return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.MyClients.AddEmailToNewClientsModalWithGrouping, viewModel);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult SaveToFavoritesAgent(int communityId, int builderId, int planId, int specId)
        {
            if (planId == 0 && specId == 0)
            {
                var pl = new PlannerListing(communityId, ListingType.Community);
                if (!UserSession.UserProfile.Planner.SavedCommunities.Contains(pl))
                    UserSession.UserProfile.Planner.AddSavedCommunity(communityId, builderId);
            }
            else
            {
                var pl = new PlannerListing(planId > 0 ? planId : specId,
                                            planId > 0 ? ListingType.Plan : ListingType.Spec);

                if (!UserSession.UserProfile.Planner.SavedHomes.Contains(pl))
                    UserSession.UserProfile.Planner.AddSavedHome(planId > 0 ? planId : specId, planId == 0);
            }
            var logger = new ImpressionLogger
            {
                CommunityId = communityId,
                BuilderId = builderId,
                PartnerId = NhsRoute.PartnerId.ToType<string>(),
                Refer = UserSession.Refer
            };

            if (planId > 0 || specId > 0)
            {
                if (planId > 0)
                    logger.AddPlan(planId);
                else if (specId > 0)
                    logger.AddSpec(specId);
            }

            logger.LogView(planId == 0 && specId == 0
                               ? LogImpressionConst.AddCommunityToUserPlanner
                               : LogImpressionConst.AddHomeToUserPlanner);

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult AutoCompleteHtml(int index)
        {
            return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.AutoCompleteClientName, new Client { Index = index });
        }

        public virtual ActionResult ToolTipActivities(int id, string type)
        {
            if (type == "C")
            {
                var savedCommsTable = _proCrmService.GetSavedCommunities(id.ToType<string>(), NhsRoute.PartnerId);
                if (savedCommsTable.Rows.Count > 0)
                {
                    var model = BindCommunityInformation(savedCommsTable.Rows[0], false);
                    model.ShowFreebrochure = false;
                    return PartialView(NhsMvc.Default.Views.MyAccount.FavoritesCommunity, model);
                }
            }
            else
            {
                var planIds = type == "P" ? id.ToType<string>() : "";
                var specIds = type == "S" ? id.ToType<string>() : "";
                var savedHomesTable = _proCrmService.GetSavedHomes(planIds, specIds, NhsRoute.PartnerId);
                if (savedHomesTable.Rows.Count > 0)
                {
                    var model = BindHomeInformation(savedHomesTable.Rows[0], false);
                    model.ShowFreebrochure = false;
                    return PartialView(NhsMvc.Default.Views.MyAccount.FavoritesHome, model);
                }
            }
            return PartialView();
        }

        [HttpPost]
        public virtual JsonResult DeleteClientFavorite(int propertyId, int clientId)
        {
            try
            {
                _proCrmService.DeleteToFavoritesClient(propertyId, clientId);
                return Json(new { valid = true, id = propertyId }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Save Email To New Clients
        [HttpPost]
        public virtual ActionResult SaveEmailToNewClients(AddEmailToNewClients model)
        {
            var clientsMaster = model.Clients.Where(p => p.SelectClient).ToArray();

            if (clientsMaster.Any() == false && clientsMaster.Any(w => string.IsNullOrWhiteSpace(w.Email) == false))
            {
                return JavaScript("tb_remove()");
            }

            Dictionary<string, string> emailDuplicate;

            var nullEmails = clientsMaster.Where(e => string.IsNullOrWhiteSpace(e.Email)).Select(e => true);

            if (nullEmails.Any())
            {
                ModelState.AddModelError("form",
                                             @"Email is required");
                model.HasErrors = true;
                BuildClientSourceList(model);
                return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.MyClients.AddEmailToNewClientsModalWithGrouping,
                                   model);
            }

            if (clientsMaster.Any() &&
                _proCrmService.EmailExists(GetUserGuid(), 0, clientsMaster.Select(p => p.Email).ToList(),
                                           out emailDuplicate))
            {
                foreach (var email in emailDuplicate)
                {
                    ModelState.AddModelError(Guid.NewGuid().ToString(),
                                             string.Format("{0} already exists for {1}, please use another email address.",
                                                           email.Value, email.Key));
                }
                model.HasErrors = true;
                BuildClientSourceList(model);
                return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.MyClients.AddEmailToNewClientsModalWithGrouping,
                                   model);
            }
            var basicTask = GetClientTaskFromModel(model);

            var clients = new List<AgentClient>();
            var listingIds = new List<int> { model.IsCommunity ? model.CommunityId : model.IsPlan ? model.PlanId : model.SpecId };

            foreach (var client in clientsMaster)
            {
                var clientTaskListingList = listingIds.Select(listing => new ClientTaskListing
                {
                    ListingType = model.IsCommunity ? "C" : model.IsPlan ? "P" : "S",
                    PropertyId = listing
                }).ToList();

                var extraDescription = new StringBuilder(basicTask.Description);
                extraDescription.AppendLine(" ");

                var newTask = new ClientTask
                {
                    Description = string.Format("Contacted the builder for {0}, by {1}", model.CommunityName, model.BuilderName),
                    Notes = extraDescription.ToString(),
                    TaskType = basicTask.TaskType,
                    TaskDate = basicTask.TaskDate,
                    DateLastChanged = basicTask.DateLastChanged,
                    TaskCompleteDate = basicTask.TaskCompleteDate
                };

                newTask.ClientTaskListings.AddRange(clientTaskListingList);

                foreach (var clientTaskListings in basicTask.ClientTaskListings)
                {
                    newTask.ClientTaskListings.Add(new ClientTaskListing
                    {
                        ListingType = clientTaskListings.ListingType,
                        PropertyId = clientTaskListings.PropertyId
                    });
                }

                if (StringHelper.IsNumeric(client.PrimaryClient))
                {
                    newTask.ClientId = client.PrimaryClient.ToType<int>();
                    var emails = client.Email.Split(';').Select(p => new ClientEmail { Email = p });
                    _proCrmService.RelateClient(client.PrimaryClient.ToType<int>(),
                                                client.Name.FirstPart(" "), client.Name.LastPart(" "),
                                                emails, newTask);

                }
                else
                {
                    var agentClient = new AgentClient
                    {
                        FirstName = client.Name.FirstPart(" "),
                        LastName = client.Name.LastPart(" "),
                        DateCreated = DateTime.Now,
                        AccountStatus = (byte)ProCrmClientsStatusType.Active,
                        UserId = UserSession.UserProfile.UserID
                    };

                    agentClient.ClientEmails.Add(new ClientEmail { Email = client.Email });
                    if (model.IsFromSaveToFavorites)
                    {
                        agentClient.ClientPlanners.Add(new ClientPlanner
                        {
                            ListingType = model.ListingType,
                            PropertyId = model.PropertyId,
                            DateCreated = DateTime.Now
                        });
                    }
                    agentClient.ClientTasks.Add(newTask);
                    clients.Add(agentClient);
                }
            }

            if (clients.Any())
                _proCrmService.InsertClients(clients);
            return JavaScript("tb_remove()");
        }
        #endregion

        #region Private Methods Crm

        private void GetLastClients(AddToFavoritesClient model)
        {
            var masterClientList = _proCrmService.GetRecentUsedClients(GetUserGuid());

            for (var i = 0; i < masterClientList.Count; i++)
            {
                model.ShowClearNames = true;
                model.Clients.Add(new Client
                {
                    Index = i,
                    ClientId = masterClientList[i].ClientId,
                    Name = string.Format("{0} {1}", masterClientList[i].FirstName, masterClientList[i].LastName)
                });
            }

            for (var i = model.Clients.Count(); i < 3; i++)
                model.Clients.Add(new Client { Index = i });
        }

        private ClientTask GetClientTaskFromModel(BasicAddTo model)
        {
            if (model.IsContactBuilder || model.IsRequestAppointment || model.IsFromSaveToFavorites)
            {
                #region Task

                string nameListing, nameBuilder, link, type = "";

                if (model.IsContactBuilder)
                    type = "Contacted the builder for";
                if (model.IsRequestAppointment)
                    type = "Requested appointment for";
                if (model.IsCommunity)
                {
                    if (string.IsNullOrWhiteSpace(model.CommunityName) || string.IsNullOrWhiteSpace(model.BuilderName))
                    {
                        var community = _communityService.GetCommunity(model.CommunityId);
                        nameListing = community.CommunityName;
                        nameBuilder = community.Builder.BuilderName;
                        model.BuilderId = community.BuilderId;
                    }
                    else
                    {
                        nameListing = model.CommunityName;
                        nameBuilder = model.BuilderName;
                    }

                    var comm = _communityService.GetCommunity(model.CommunityId);
                    link = comm.ToCommunityDetail().ToUrl(RedirectionHelper.GetCommunityDetailPage(), false, true);
                }
                else if (model.IsPlan)
                {
                    if (string.IsNullOrWhiteSpace(model.HomeTitle) || string.IsNullOrWhiteSpace(model.BuilderName))
                    {
                        var plan = _listingService.GetPlan(model.PlanId);
                        nameListing = plan.PlanName + " at " + model.CommunityName;
                        nameBuilder = plan.Community.Builder.BuilderName;
                    }
                    else
                    {
                        nameListing = model.HomeTitle + " at " + model.CommunityName;
                        nameBuilder = model.BuilderName;
                    }

                    var @params = new List<RouteParam>
                    {
                        new RouteParam(RouteParams.PlanId, model.PlanId.ToType<string>(), RouteParamType.Friendly, true, true)
                    };
                    link = @params.ToUrl(Pages.HomeDetail, false, true);
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(model.HomeTitle) || string.IsNullOrWhiteSpace(model.BuilderName))
                    {
                        var spec = _listingService.GetSpec(model.SpecId);
                        nameListing = spec.PlanName + " at " + model.CommunityName;
                        nameBuilder = spec.Community.Builder.BuilderName;
                    }
                    else
                    {
                        nameListing = model.HomeTitle + " at " + model.CommunityName;
                        nameBuilder = model.BuilderName;
                    }


                    var @params = new List<RouteParam>
                    {
                        new RouteParam(RouteParams.SpecId, model.SpecId.ToType<string>(), RouteParamType.Friendly, true, true)
                    };
                    link = @params.ToUrl(Pages.HomeDetail, false, true);
                }
                #endregion

                var proccessTime = DateTime.Now;
                return new ClientTask
                {
                    Description = string.Format("{0} {1}, by {2}", type, nameListing, nameBuilder),
                    Notes = string.Format("\"{0}\". {1}", model.Message, link),
                    TaskType = ProCrmTaskTypes.Email.ToString(),
                    TaskDate = proccessTime,
                    DateLastChanged = proccessTime,
                    TaskCompleteDate = proccessTime,
                    ClientTaskListings = new List<ClientTaskListing>
                            {
                                new ClientTaskListing
                                    {
                                        ListingType = model.IsCommunity ? "C" : model.IsPlan ? "P" : "S",
                                        PropertyId = model.PropertyId
                                    }
                            }
                };
            }
            return null;
        }

        private IEnumerable<ClientInformation> GetClientsInfoModel(IEnumerable<AgentClient> masterClientList)
        {
            var clientList = new List<ClientInformation>();
            foreach (var agentClient in masterClientList)
            {
                var clientsInfo = new ClientInformation
                {
                    ClientId = agentClient.ClientId,
                    FirstName = agentClient.FirstName,
                    LastName = agentClient.LastName,
                    RelatedFirstName = agentClient.RelatedFirstName,
                    RelatedLastName = agentClient.RelatedLastName,
                    DateLastChanged = agentClient.DateLastChanged.ToType<DateTime>(),
                    AccountStatus = agentClient.AccountStatus
                };

                var clientTasks = agentClient.ClientTasks.OrderByDescending(p => p.DateLastChanged).Take(2).AsQueryable();
                if (clientTasks.Any())
                {
                    var clientTask = clientTasks.FirstOrDefault();
                    if (clientTask != null)
                    {
                        clientsInfo.ClientTaskId1 = clientTask.ClientTaskId;
                        clientsInfo.ClientTaskDescription1 = clientTask.Description.Length > 84
                                                                 ? clientTask.Description.CutLenghtByWords(85, "...")
                                                                 : clientTask.Description;
                    }
                }
                if (clientTasks.Count() > 1)
                {
                    var clientTask = clientTasks.Skip(1).FirstOrDefault();
                    if (clientTask != null)
                    {
                        clientsInfo.ClientTaskId2 = clientTask.ClientTaskId;
                        clientsInfo.ClientTaskDescription2 = clientTask.Description.Length > 84
                                                                 ? clientTask.Description.CutLenghtByWords(85, "...")
                                                                 : clientTask.Description;
                    }
                }
                clientList.Add(clientsInfo);
            }
            return clientList;
        }

        private ActionResult RedirectInvalidRequest()
        {
            if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
            {
                return RedirectTo404();
            }

            if (UserSession.UserProfile.ActorStatus != WebActors.ActiveUser)
            {
                if (Request.Url != null)
                {
                    var next = HttpUtility.UrlEncode(Request.Url.PathAndQuery);
                    return Redirect(Pages.SignIn, new List<RouteParam> { new RouteParam(RouteParams.NextPage, next, RouteParamType.QueryString) });
                }
            }

            return null;
        }

        private string GetUserGuid()
        {
            var userGuid = UserSession.UserProfile.UserID;
            return userGuid;
        }
        #endregion
        #endregion

        #region Agent Account

        public virtual ActionResult ShowMyAccount()
        {
            var returnInvalid = RedirectInvalidRequest();
            if (returnInvalid != null)
            {
                return returnInvalid;
            }

            var model = GetFavoritesViewModel();
            return View(model);
        }

        public virtual ActionResult MyFavoritesTab()
        {
            var model = GetFavoritesViewModel();

            var favorites = GetSavedPropertiesViewModelModel();
            model.FavoritesSaveHome = favorites.FavoritesSaveHome;
            model.FavoritesSaveComm = favorites.FavoritesSaveComm;

            return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.Agent.FavoritesTab, model);
        }

        public virtual ActionResult BrochureTab()
        {
            var agent = _userProfileService.GetUserByLogonName(UserSession.UserProfile.Email, NhsRoute.PartnerId);
            var stateList = _stateService.GetAllStates().ToList();
            stateList.Insert(0, new State { StateAbbr = string.Empty, StateName = DropDownDefaults.State });

            var model = new MyAccountBrochure();

            if (agent != null && NhsRoute.IsBrandPartnerNhsPro)
            {
                var agentBrochureTemplate = agent.AgentBrochureTemplates.FirstOrDefault();

                if (agentBrochureTemplate != null)
                    model = new MyAccountBrochure
                    {
                        Email = agentBrochureTemplate.Email,
                        FirstName = agentBrochureTemplate.FirstName,
                        LastName = agentBrochureTemplate.LastName,
                        AgencyLogoPath = agentBrochureTemplate.AgencyLogo,
                        AgentPhotoPath = agentBrochureTemplate.ImageUrl,
                        City = agentBrochureTemplate.City,
                        StateAbbr = agentBrochureTemplate.State,
                        AgencyName = agentBrochureTemplate.AgencyName,
                        AgentLicenseNumber = agent.RealStateLicense,
                        BusinessPhone = agentBrochureTemplate.OfficePhone,
                        MobilePhone = agentBrochureTemplate.MobilePhone,
                        StateList = new SelectList(stateList, "StateAbbr", "StateName")
                    };

            }
            else
            {
                return Content("<div><h1>Invalid Request</h1><br><h3>Please login first in order to see this content.</h3></div>");
            }
            return View(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.Agent.BrochureTab, model);
        }

        [HttpPost]
        public virtual ActionResult BrochureTab(MyAccountBrochure model)
        {
            ValidateBrochureInformation(model);

            if (ModelState.IsValid)
            {
                var server = Configuration.ImageSharePath;
                var path =
                    Path.Combine(@"Pro7\agent\", UserSession.UserProfile.UserID)
                        .Replace("\\\\", "\\")
                        .TrimStart('\\'); //
                var folder = Path.Combine(server, path);

                // Create a new DirectoryInfo object corresponding to the remote folder.
                var dirInfo = new DirectoryInfo(folder);
                if (!dirInfo.Exists)
                    dirInfo.Create();
                var index = server.IndexOf("\\", 2);
                var folder2 = server.Substring(index + 1);
                if (model.AgencyLogo != null)
                {
                    var extension = Path.GetExtension(model.AgencyLogo.FileName);
                    model.AgencyLogoPath = Path.Combine(path, "AgencyLogo" + Guid.NewGuid() + extension);
                    model.AgencyLogo.SaveAs(Path.Combine(server, model.AgencyLogoPath));
                    model.AgencyLogoPath = Path.Combine(folder2, model.AgencyLogoPath).Replace("\\", "/");
                }

                if (model.AgentPhoto != null)
                {
                    var extension = Path.GetExtension(model.AgentPhoto.FileName);
                    model.AgentPhotoPath = Path.Combine(path, "AgentPhoto" + Guid.NewGuid() + extension);
                    model.AgentPhoto.SaveAs(Path.Combine(server, model.AgentPhotoPath));
                    model.AgentPhotoPath = Path.Combine(folder2, model.AgentPhotoPath).Replace("\\", "/");
                }

                var agentBrochureInfo = new AgentBrochureTemplate
                {
                    UserGuid = UserSession.UserProfile.UserID,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    AgencyLogo = model.AgencyLogoPath,
                    ImageUrl = model.AgentPhotoPath,
                    City = model.City,
                    State = model.StateAbbr,
                    AgencyName = model.AgencyName,
                    OfficePhone = model.BusinessPhone,
                    MobilePhone = model.MobilePhone,
                    DateCreated = DateTime.Now,
                    DateLastChanged = UserSession.UserProfile.DateRegistered
                };
                _userProfileService.SetAgentRecord(agentBrochureInfo);

                model.Saved = true;
            }

            var stateList = _stateService.GetAllStates().ToList();
            stateList.Insert(0, new State { StateAbbr = string.Empty, StateName = DropDownDefaults.State });

            model.StateList = new SelectList(stateList, "StateAbbr", "StateName");
            return View(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.Agent.BrochureTab, model);
        }

        public virtual ActionResult EditAccount()
        {
            UserSession.PersistUrlParams(this);
            var returnInvalid = RedirectInvalidRequest();
            if (returnInvalid != null)
            {
                return returnInvalid;
            }
            var model = GetManageAccountViewModel();
            model.Redirect = true;
            return View(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.Agent.EditAccount, model);
        }

        public virtual ActionResult ManageAccount()
        {
            return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.Agent.ManageAccountForm,
                               GetManageAccountViewModel());
        }

        [HttpPost]
        public virtual ActionResult ManageAccount(ManageAccountViewModel model)
        {
            ValidateManageAccount(model);

            if (!ModelState.IsValid)
                return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.Agent.ManageAccountForm, model);

            var originalUser = CopyUser();
            var agent = _userProfileService.GetUserByLogonName(UserSession.UserProfile.Email, NhsRoute.PartnerId);

            UserSession.UserProfile.LogonName = model.Email;
            UserSession.UserProfile.FirstName = model.FirstName;
            UserSession.UserProfile.LastName = model.LastName;
            UserSession.UserProfile.Address1 = model.Address1;
            UserSession.UserProfile.Address2 = "";
            UserSession.UserProfile.DayPhone = model.Phone; //Format Phone
            UserSession.UserProfile.DayPhoneExt = "";
            UserSession.UserProfile.City = model.City;
            UserSession.UserProfile.PostalCode = model.ZipCode;
            UserSession.UserProfile.State = model.StateAbbr;
            UserSession.UserProfile.MailList = model.Newsletter ? "1" : "0";

            if (NhsRoute.IsBrandPartnerNhsPro)
            {
                UserSession.UserProfile.WeeklyNotifierOptIn = model.Promos;
                UserSession.UserProfile.MarketOptIn = "0";
            }
            else
                UserSession.UserProfile.MarketOptIn = model.Promos ? "1" : "0";

            UserSession.UserProfile.Password = model.Password;
            // These are not used anymore but just given support as used in stored procs.
            UserSession.UserProfile.BoxRequestedDate = DateTime.Now;
            UserSession.UserProfile.InitialMatchDate = DateTime.Now;

            UserSession.UserProfile.MoveInDate = Convert.ToInt16(model.MoveInDate);
            UserSession.UserProfile.FinancePreference = Convert.ToInt16(model.FinancePref);
            UserSession.UserProfile.Reason = Convert.ToInt16(model.Reason);

            if (NhsRoute.IsBrandPartnerNhsPro)
            {
                UserSession.UserProfile.DayPhone = model.RequestPhone;
                UserSession.UserProfile.EvePhone = model.MobilePhone;
                UserSession.UserProfile.RegMetro = model.MarketId;
                UserSession.UserProfile.AgencyName = model.AgencyName;
                UserSession.UserProfile.RealEstateLicense = model.RealEstateLicense;
                UserSession.UserProfile.City = model.CityId;
                UserSession.UserProfile.State = model.StateId;
                if (agent != null)
                {
                    var agentBrochureTemplate = agent.AgentBrochureTemplates.FirstOrDefault();
                    var finalProfile = UserSession.UserProfile.SetValuesForBrochureTable(originalUser, agentBrochureTemplate);
                    _userProfileService.SaveBrochureTemplate(finalProfile);
                }
                else
                {
                    var finalProfile = UserSession.UserProfile.SetValuesForBrochureTable(originalUser, null);
                    _userProfileService.SaveBrochureTemplate(finalProfile);
                }

            }

            UserSession.UserProfile.SaveProfile();
            return PartialView(NhsMvc.PartnerBrandGroupViews_88.Views.ProCrm.Agent.ManageAccountForm, model);
        }

        #region Private Methods

        private FavoritesViewModel GetFavoritesViewModel()
        {
            var model = new FavoritesViewModel();
            switch (RouteParams.Tab.Value<string>())
            {
                case "favorite":
                    model.SelectFavoriteTab = true;
                    break;
                case "brochure":
                    model.SelectBrochureTab = true;
                    break;
                case "myclient":
                    model.SelectClientsTab = true;
                    break;
                default:
                    model.SelectMyAccount = true;
                    break;
            }

            model.ShowMyClients = RouteParams.ShowMyClients.Value<bool>();
            return model;
        }

        private ManageAccountViewModel GetManageAccountViewModel()
        {
            var partner = _partnerService.GetPartner(NhsRoute.PartnerId);
            var phone = UserSession.UserProfile.DayPhone;

            var model = new ManageAccountViewModel
            {
                Email = UserSession.UserProfile.LogonName,
                FirstName = UserSession.UserProfile.FirstName,
                LastName = UserSession.UserProfile.LastName,
                Address1 = String.IsNullOrEmpty(UserSession.UserProfile.Address2)
                               ? UserSession.UserProfile.Address1
                               : UserSession.UserProfile.Address1 + ", " + UserSession.UserProfile.Address2,
                ZipCode = UserSession.UserProfile.PostalCode,
                City = UserSession.UserProfile.City,
                Password = UserSession.UserProfile.Password,
                ConfirmPassword = UserSession.UserProfile.Password,
                ConfirmEmail = UserSession.UserProfile.Email,
                StateAbbr = UserSession.UserProfile.State,
                Newsletter = UserSession.UserProfile.MailList == "1",
                ShowNewsletter = (partner.UsesMailList == "Y"),
                ShowPromos = (partner.UsesThirdPartyOptin == "Y"),
                AgencyName = UserSession.UserProfile.AgencyName,
                MobilePhone = UserSession.UserProfile.EvePhone,
                RequestPhone = UserSession.UserProfile.DayPhone,
                RealEstateLicense = UserSession.UserProfile.RealEstateLicense,
                Promos = UserSession.UserProfile.WeeklyNotifierOptIn
            };

            if (string.IsNullOrEmpty(UserSession.UserProfile.PostalCode) &&
                !string.IsNullOrEmpty(UserSession.UserProfile.RegMetro))
            {
                if (_marketService != null)
                {
                    var marketName =
                        _marketService.GetMarket(NhsRoute.PartnerId, Convert.ToInt32(UserSession.UserProfile.RegMetro),
                                                 false) != null
                            ? _marketService.GetMarket(NhsRoute.PartnerId,
                                                       Convert.ToInt32(UserSession.UserProfile.RegMetro), false).
                                  MarketName
                            : null;
                    if (marketName != null)
                        model.MarketName = marketName;
                }
            }

            if (UserSession.UserProfile.MoveInDate != -1)
                model.MoveInDate = UserSession.UserProfile.MoveInDate;
            if (UserSession.UserProfile.FinancePreference != 0)
                model.FinancePref = UserSession.UserProfile.FinancePreference;
            if (UserSession.UserProfile.Reason != 0)
                model.Reason = UserSession.UserProfile.Reason.ToType<string>();

            if (phone != null)
            {
                model.Phone = phone.Length == 10
                                  ? "(" + phone.Substring(0, 3) + ") " + phone.Substring(3, 3) + "-" +
                                    phone.Substring(6, 4)
                                  : phone;
            }

            return model;
        }

        private void ValidateManageAccount(ManageAccountViewModel model)
        {
            // Checks password only if it's not comming from lead form
            if (NhsUrl.GetFromPage != Pages.LeadsRequestInfoLogged &&
                NhsUrl.GetFromPage != Pages.LeadsRequestInfoLoggedModal)
            {
                //Cross check this old way of checking password
                if (model.Password != "**********" && !string.IsNullOrEmpty(model.Password))
                {
                    if (model.Password.Length < 5 || model.Password.Length > 20)
                    {
                        ModelState.AddModelError("Password",
                                                 string.IsNullOrEmpty(model.Password)
                                                     ? LanguageHelper.MSG_ACCOUNT_NO_PASSWORD
                                                     : LanguageHelper.MSG_ACCOUNT_INVALID_PASSWORD);
                    }
                    else
                    {
                        if (model.Password != model.ConfirmPassword)
                        {
                            // MSG_ACCOUNT_DIFF_PASSWORD should be used instead. Update when this message 
                            // is available in StringResource
                            ModelState.AddModelError("ConfirmPassword",
                                                     LanguageHelper.MSG_ACCOUNT_INVALID_CONFIRPASSWORD);
                        }
                    }
                }
                else
                    ModelState.AddModelError("Password",
                                             string.IsNullOrEmpty(model.Password)
                                                 ? LanguageHelper.MSG_ACCOUNT_NO_PASSWORD
                                                 : LanguageHelper.MSG_ACCOUNT_INVALID_PASSWORD);
                // End of Password check
            }

            if (string.IsNullOrEmpty(model.Email))
            {
                ModelState.AddModelError("Email", LanguageHelper.MSG_ACCOUNT_NO_EMAIL);
            }
            else
            {
                var fValid = StringHelper.IsValidEmail(model.Email);

                if (!fValid)
                {
                    ModelState.AddModelError("Email", LanguageHelper.MSG_ACCOUNT_INVALID_EMAIL);
                }
                else
                {
                    if (UserSession.UserProfile.LogonName != model.Email)
                    {
                        if (_userProfileService.LogonNameExists(model.Email, NhsRoute.PartnerId))
                        {
                            ModelState.AddModelError("Email", LanguageHelper.MSG_ACCOUNT_SAME_EMAIL);
                        }
                    }
                }
            } // End login check

            // Checks for NON-INTERNATIONAL users
            if (!model.LiveOutside)
            {
                //ZIP is not considered to be a mandatory field...but validates its correctness if entered by user.
                if (!string.IsNullOrEmpty(model.ZipCode))
                {
                    if (model.ZipCode.Trim() == "00000" || !StringHelper.IsValidZip(model.ZipCode.Trim()))
                        ModelState.AddModelError("ZipCode", LanguageHelper.MSG_ACCOUNT_INVALID_ZIP);
                }
                else ModelState.AddModelError("ZipCode", LanguageHelper.MSG_ACCOUNT_INVALID_ZIP);
            }

            if (NhsRoute.IsBrandPartnerNhsPro && string.IsNullOrEmpty(model.MarketId))
            {
                ModelState.Remove("ZipCode");
                ModelState.AddModelError("ZipCode", LanguageHelper.MSG_ACCOUNT_INVALID_ZIP);
            }

            if (string.IsNullOrEmpty(model.FirstName))
            {
                ModelState.AddModelError("FirstName", LanguageHelper.MSG_ACCOUNT_NO_FIRST);
            }

            if (string.IsNullOrEmpty(model.LastName))
            {
                ModelState.AddModelError("LastName", LanguageHelper.MSG_ACCOUNT_NO_LAST);
            }

            if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
            {
                ModelState.Remove("AgencyName");
            }
        }

        private void ValidateBrochureInformation(MyAccountBrochure model)
        {
            if (UserSession.UserProfile.ActorStatus != WebActors.ActiveUser)
            {
                ModelState.AddModelError("NotLogin", "Login first in order to save this data");
            }

            if (model.AgencyLogo != null)
            {
                var s = Path.GetExtension(model.AgencyLogo.FileName);
                if (s != null)
                {
                    var extension = s.Replace(".", "").ToLower();
                    if (extension != "jpg" && extension != "jpeg" && extension != "gif" && extension != "png")
                        ModelState.AddModelError("AgencyLogo", LanguageHelper.MSG_INVALID_IMAGE_EXTENSION);
                    else
                    {

                        var img = new Bitmap(model.AgencyLogo.InputStream);
                        if (img.Width > 180 || img.Height > 100)
                            ModelState.AddModelError("AgencyLogo", LanguageHelper.MSG_AGENCY_LOGO_INCORRECT_SIZE);
                    }
                }
            }

            if (model.AgentPhoto != null)
            {
                var s = Path.GetExtension(model.AgentPhoto.FileName);
                if (s != null)
                {
                    var extension = s.Replace(".", "").ToLower();
                    if (extension != "jpg" && extension != "jpeg" && extension != "gif" && extension != "png")
                        ModelState.AddModelError("AgentPhoto", LanguageHelper.MSG_INVALID_IMAGE_EXTENSION);
                    else
                    {

                        var img = new Bitmap(model.AgentPhoto.InputStream);
                        if (img.Width > 103 || img.Height > 131)
                            ModelState.AddModelError("AgentPhoto", LanguageHelper.MSG_AGENCY_PHOTO_INCORRECT_SIZE);
                    }
                }
            }

            if (string.IsNullOrEmpty(model.Email))
            {
                ModelState.AddModelError("Email", LanguageHelper.MSG_ACCOUNT_NO_EMAIL);
            }
            else
            {
                var fValid = StringHelper.IsValidEmail(model.Email);

                if (!fValid)
                {
                    ModelState.AddModelError("Email", LanguageHelper.MSG_ACCOUNT_INVALID_EMAIL);
                }
                else
                {
                    if (UserSession.UserProfile.LogonName != model.Email)
                    {
                        if (_userProfileService.LogonNameExists(model.Email, NhsRoute.PartnerId))
                        {
                            ModelState.AddModelError("Email", LanguageHelper.MSG_ACCOUNT_SAME_EMAIL);
                        }
                    }
                }
            } // End login check


            if (string.IsNullOrEmpty(model.FirstName))
            {
                ModelState.AddModelError("FirstName", LanguageHelper.MSG_ACCOUNT_NO_FIRST);
            }

            if (string.IsNullOrEmpty(model.LastName))
            {
                ModelState.AddModelError("LastName", LanguageHelper.MSG_ACCOUNT_NO_LAST);
            }
        }

        private Profile CopyUser()
        {
            var originalUser = new Profile
            {
                UserID = UserSession.UserProfile.UserID,
                AgencyName = UserSession.UserProfile.AgencyName,
                FirstName = UserSession.UserProfile.FirstName,
                LastName = UserSession.UserProfile.LastName,
                Email = UserSession.UserProfile.Email,
                LogonName = UserSession.UserProfile.LogonName,
                PostalCode = UserSession.UserProfile.PostalCode,
                City = UserSession.UserProfile.City,
                State = UserSession.UserProfile.State,
                RegMetro = UserSession.UserProfile.RegMetro,
                DayPhone = UserSession.UserProfile.DayPhone,
                EvePhone = UserSession.UserProfile.EvePhone,
                Password = UserSession.UserProfile.Password,
                RealEstateLicense = UserSession.UserProfile.RealEstateLicense
            };

            return originalUser;

        }
        #endregion
        #endregion
    }
}
