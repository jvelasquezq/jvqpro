﻿using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Extensions;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Utility.Web;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class SendToPhoneController : ApplicationController
    {
        private readonly IListingService _listingService;
        private readonly ICommunityService _communityService;

        public SendToPhoneController(IListingService listingService, ICommunityService communityService)
        {
            _communityService = communityService;
            _listingService = listingService;
        }

        [HttpGet]
        public virtual ActionResult SendToPhone()
        {
            var model = new SendToPhoneModel
            {
                CommunityId = RouteParams.CommunityId.Value<int>(),
                SpecId = RouteParams.SpecId.Value<int>(),
                PlanId = RouteParams.PlanId.Value<int>()
            };

            return PartialView(
                NhsRoute.ShowMobileSite
                    ? NhsMvc.Default.ViewsMobile.SendToPhone.SendToPhone
                    : NhsMvc.Default.Views.SendToPhone.SendToPhone, model);
        }

        [HttpPost]
        public virtual JsonResult SendToPhone(SendToPhoneModel model)
        {
            model.Phone = string.Format("{0}@{1}", CommonUtils.ConvertPhoneToNumber(model.Phone), model.Provider);
            if (model.Provider == "tmomail.net")
                model.Phone = "1" + model.Phone;


            foreach (var body in GetMessage(model))
                MailHelper.Send(model.Phone, Configuration.FromEmail, "", body, false);

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        private IEnumerable<string> GetMessage(SendToPhoneModel model)
        {
            var message = new StringBuilder();
            Community community;
            var address = "";
            if (model.PlanId > 0)
            {
                var plan = _listingService.GetPlan(model.PlanId);
                community = plan.Community ?? _communityService.GetCommunity(plan.CommunityId);
                message.AppendFormat("{0} {1} ", plan.PlanName, LanguageHelper.At);
            }
            else if (model.SpecId > 0)
            {
                var spec = _listingService.GetSpec(model.SpecId);
                community = spec.Community ?? _communityService.GetCommunity(spec.CommunityId);
                message.AppendFormat("{0} {1} ", spec.PlanName,LanguageHelper.At);
                address = spec.Address1;
            }
            else
                community = _communityService.GetCommunity(model.CommunityId);

            model.BuilderId = community.Builder == null ? 0 : community.Builder.BuilderId;
            message.AppendFormat("{0} {2} {1}|", community.CommunityName,
                community.Builder == null ? "" : community.Builder.BuilderName, LanguageHelper.By);
            if (!string.IsNullOrEmpty(community.Address1) || !string.IsNullOrEmpty(address))
                message.Append((!string.IsNullOrEmpty(address) ? address : community.Address1) + "|");

            message.AppendFormat("{0}, {1} {2}|", community.City, community.StateAbbr, community.PostalCode.TrimEnd());
            message.AppendFormat("Ph: {0}|", community.PhoneNumber);
            message.Append(GetShortenedDetailPageURL(model));
            return StringHelper.SplitBodyForMessage(message.ToString());
        }

        private string GetShortenedDetailPageURL(SendToPhoneModel model)
        {
            var logPage = (model.SpecId > 0 || model.PlanId > 0) ? Pages.HomeDetail : Pages.CommunityDetail;
            var @params = new List<RouteParam>();


            if (model.SpecId > 0)
                @params.Add(new RouteParam(RouteParams.SpecId, model.SpecId, RouteParamType.Friendly, true, true));
            else if (model.PlanId > 0)
                @params.Add(new RouteParam(RouteParams.PlanId, model.PlanId, RouteParamType.Friendly, true, true));
            else
            {
                @params.Add(new RouteParam(RouteParams.Builder, model.BuilderId, RouteParamType.Friendly, true, true));
                @params.Add(new RouteParam(RouteParams.Community, model.CommunityId, RouteParamType.Friendly, true, true));
            }
            var url = PathHelpers.GetAbsoluteURL(@params.ToUrl(logPage));
            var bitlyData = BitlyHelper.GetData(Configuration.BitlyUser, Configuration.BitlyKey, url,
                BitlyMethod.Shorten);

            return !string.IsNullOrEmpty(bitlyData) ? bitlyData : url;
        }
    }
}