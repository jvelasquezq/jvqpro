﻿using System;
using System.Web.Mvc;
using Nhs.Web.UI.AppCode.Filters;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
     [Instrumentation(Order = 1)]
     [PartnerInfo(Order = 2)]
     public partial class MVCErrorController : Controller
    {
        //
        // GET: /MVCError/

        public virtual ActionResult MVCError()
        {
            int errorCode = 500;
            if (!string.IsNullOrEmpty(Request.QueryString["statuscode"]))
                errorCode = Convert.ToInt32(Request.QueryString["statuscode"]);

            Response.TrySkipIisCustomErrors = true; 
            Response.StatusCode = errorCode;
            return View(new BaseViewModel());
        }

    }
}
