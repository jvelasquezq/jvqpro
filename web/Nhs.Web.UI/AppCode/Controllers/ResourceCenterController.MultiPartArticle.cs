﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Mvc.Domain.Model.CMS;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Library.Web;
using System.Xml;
using Nhs.Library.Helpers.Content;
using Nhs.Utility.Constants;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class ResourceCenterController
    {
        public virtual ActionResult ShowMultiPartArticle(string articleName)
        {
            var friendlyArticleName = articleName.Replace("-", " ");
            var mpArticle = _cmsService.GetMultiPartArticle(friendlyArticleName,
                Configuration.EktronResourceCenterTaxonomyId, Configuration.EktronLanguageId);

            if (mpArticle == null)
            {
                return RedirectPermanent("~/" + Pages.GetResourceCenterLink(NhsRoute.BrandPartnerId));
            }

            var model = new ResourceCenterViewModels.MultiPartArticleViewModel { MultiPartArticle = mpArticle };

            string lastCategory = null;

            if (UserSession.GetItem("LastCMSCategory") != null)
                lastCategory = UserSession.GetItem("LastCMSCategory").ToString();

            model.ParentCategories = _cmsService.GetCategoriesByArticleId(mpArticle.ArticleID, lastCategory)
                                   .OrderBy(c => c.CategoryId)
                                   .ToList();

            model.IsArticlesPage = true;

            //Metas
            var metas = _cmsService.GetMetasForArticle(friendlyArticleName, Configuration.EktronResourceCenterTaxonomyId,
                Configuration.EktronLanguageId);
            base.OverrideDefaultMeta(model, metas);

            //Canonicals
            if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
                AddCanonicalWithExcludeParams(model, new List<string>());
            else
                AddProResourceCenterCanonicalTag(model);

            //Carry through ads from other pages into Resource Center
            SetupAdParameters(model);

            model.Globals.AdController.AddArticleParameter(friendlyArticleName);

            return View(NhsMvc.Default.Views.ResourceCenter.ShowMultiPartArticle, model);
        }

        public virtual ActionResult ShowMultiPartArticleSection(string sectionName)
        {
            var friendlyArticleName = sectionName.Trim().ToLower().Replace("-", " ");
            var mpArticle = _cmsService.GetMultiPartArticleBySection(friendlyArticleName,
                Configuration.EktronResourceCenterTaxonomyId, Configuration.EktronLanguageId);

            if (mpArticle == null)
            {
                return RedirectPermanent("~/" + Pages.GetResourceCenterLink(NhsRoute.BrandPartnerId));
            }

            mpArticle.ArticleMainImage = mpArticle.ArticleMainImage.Replace("/uploadedImages/", string.Format("{0}uploadedImages/", Configuration.EktronDomain));

            var model = new ResourceCenterViewModels.MultiPartArticleSectionViewModel
                {
                    MultiPartArticle = mpArticle,
                    ArticleSection =
                        mpArticle.ArticleSections.First(
                            artsec => artsec.ContentTitle.Trim().ToLower() == friendlyArticleName),
                    SectionIndex =
                        mpArticle.ArticleSections.IndexOf(
                            mpArticle.ArticleSections.First(
                                artsec => artsec.ContentTitle.Trim().ToLower() == friendlyArticleName))
                };

            if (model.SectionIndex > 0)
                model.PreviousArticleSection = mpArticle.ArticleSections[model.SectionIndex - 1];
            if (model.SectionIndex < mpArticle.ArticleSections.Count - 1)
                model.NextArticleSection = mpArticle.ArticleSections[model.SectionIndex + 1];

            model.ArticleSection.SectionImage = model.ArticleSection.SectionImage.Replace("/uploadedImages/", string.Format("{0}uploadedImages/", Configuration.EktronDomain));

            string lastCategory = null;

            if (UserSession.GetItem("LastCMSCategory") != null)
                lastCategory = UserSession.GetItem("LastCMSCategory").ToString();

            model.ParentCategories = _cmsService.GetCategoriesByArticleId(mpArticle.ArticleID, lastCategory)
                                   .OrderBy(c => c.CategoryId)
                                   .ToList();
            model.IsArticlesPage = true;

            //Metas
            var metas = _cmsService.GetMetasForArticle(friendlyArticleName, Configuration.EktronResourceCenterTaxonomyId, Configuration.EktronLanguageId);
            base.OverrideDefaultMeta(model, metas);

            //Canonicals
            if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
                AddCanonicalWithExcludeParams(model, new List<string>());
            else
                AddProResourceCenterCanonicalTag(model);

            //There is a problem with this on production but not reproducible on sprint, test or stage. Remove try catch once issue identified
            try
            {
                //Carry through ads from other pages into Resource Center
                SetupAdParameters(model);
            }

            catch (Exception ex)
            {
                ErrorLogger.LogError("ISSUE WITH AD PARAMETERS", ex.Message, ex.Message);
            }

            model.Globals.AdController.AddArticleParameter(friendlyArticleName);

            return View(NhsMvc.Default.Views.ResourceCenter.ShowMultiPartArticleSection, model);
        }

        public virtual ActionResult ShowFeaturedHomesSlideshow(string friendlyArticleName, string xml, long articleId)
        {
            var model = new ResourceCenterViewModels.ResourceCenterFeaturedHomesSlideShowViewModel();
            var featuresHomeData = CmsReader.ParseFeaturedHomesSlideShow(xml);

            var authorId = featuresHomeData.IntroductoryAuthor.ToType<long>();
            featuresHomeData.IntroductoryAuthor = string.Empty;

            if (authorId > 0)
            {
                var authorData = GetAuthorFromId(authorId);

                if (_cmsService.HasAuthorPageAndIsActive(authorData.Name,
                    Configuration.EktronResourceCenterTaxonomyId, Configuration.EktronLanguageId))
                {
                    featuresHomeData.IntroductoryAuthor = BuildAnchorAuthor(authorData.Name, PartnerLayoutHelper.GetPartnerLayoutConfig().IsBrandPartnerCna);
                }
            }

            foreach (var slide in featuresHomeData.Slides)
            {
                var innerAuthorId = slide.AuthorId;
                slide.Author = string.Empty;
                if (innerAuthorId > 0)
                {
                    var authorData = GetAuthorFromId(authorId);
                    if (_cmsService.HasAuthorPageAndIsActive(authorData.Name,
                        Configuration.EktronResourceCenterTaxonomyId, Configuration.EktronLanguageId))
                    {
                        slide.Author = BuildAnchorAuthor(authorData.Name, PartnerLayoutHelper.GetPartnerLayoutConfig().IsBrandPartnerCna);
                    }
                }
            }

            string lastCategory = null;

            if (UserSession.GetItem("LastCMSCategory") != null)
                lastCategory = UserSession.GetItem("LastCMSCategory").ToString();

            var parentCategories = _cmsService.GetCategoriesByArticleId(articleId, lastCategory)
                                   .OrderBy(c => c.CategoryId)
                                   .ToList();

            parentCategories.Add(new RCCategory() { CategoryName = featuresHomeData.SlideshowMainTitle });

            model.FeaturedHomes = featuresHomeData;
            model.ParentCategories = parentCategories;

            foreach (var relatedArticle in model.FeaturedHomes.RelatedArticles)
            {
                var article = _cmsService.GetRcArticleById(relatedArticle,
                    Configuration.EktronResourceCenterTaxonomyId, Configuration.EktronLanguageId);

                if (article != null)
                {
                    model.TrendingNow.Add(article);
                }
            }

            //Metas
            var metas = _cmsService.GetMetasForArticle(friendlyArticleName, Configuration.EktronResourceCenterTaxonomyId, Configuration.EktronLanguageId);
            base.OverrideDefaultMeta(model, metas);

            //Canonicals
            if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
                AddCanonicalWithExcludeParams(model, new List<string>());
            else
                AddProResourceCenterCanonicalTag(model);

            //Carry through ads from other pages into Resource Center
            SetupAdParameters(model);

            model.Globals.AdController.AddArticleParameter(friendlyArticleName);

            return View("ShowFeaturedHomesSlideshow", model);
        }


        private RcAuthor GetAuthorFromId(long authorId)
        {
            var authorData = _cmsService.GetAllAuthorsInfo(Configuration.EktronResourceCenterTaxonomyId,
                                                        Configuration.EktronLanguageId)
                                     .FirstOrDefault(author => author.AuthorId == authorId) ?? new RcAuthor();

            return authorData;
        }

        private static string BuildAnchorAuthor(string author, bool isCna)
        {
            var url =
                NhsUrlHelper.BuildResourceCenterUrl(isCna ? "/autores/" : "/authors/" +
                                                    author.ToLower().Replace(" ", "-"));
            var anchorAuthor = "<a href=\""
                                + url
                                + "\" rel=\"author\">"
                                + author
                                + "</a>";

            return anchorAuthor;
        }
    }
}
