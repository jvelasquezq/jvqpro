﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Exceptions;
using Nhs.Library.Extensions;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class AccountController : ApplicationController
    {
        private readonly IUserProfileService _userService;
        private readonly IPartnerService _partnerService;
        private readonly IMarketService _marketService;
        private readonly ISsoService _ssoService;
        private readonly IPathMapper _pathMapper;
        private readonly IStateService _stateService;
        private readonly ILookupService _lookupService;

        public AccountController(IUserProfileService userService, IPartnerService partnerService, IMarketService marketService, ISsoService ssoService,
                                 IPathMapper pathMapper, IStateService stateService, ILookupService lookupService)
            : base(pathMapper)
        {
            _userService = userService;
            _partnerService = partnerService;
            _marketService = marketService;
            _ssoService = ssoService;
            _pathMapper = pathMapper;
            _stateService = stateService;
            _lookupService = lookupService;
        }

        #region Actions

        public virtual ActionResult Unsubscribe()
        {
            var viewModel = new UnsubscribeViewModel();


            var metaRegistrar = new MetaRegistrar(_pathMapper);
            var metas = metaRegistrar.GetRobotsMeta("noodp, noydir, noindex");
            base.OverrideDefaultMeta(viewModel, metas);
            base.AddCanonicalTag(viewModel);

            //This is from chat agent search alert
            var userId = RouteParams.UserId.Value();

            viewModel.FromEmail = NhsRoute.IsBrandPartnerNhsPro
                ? "support@newhomesourceprofessional.com"
                : "support@thebdx.com";
            if (StringHelper.IsValidEmail(userId))
            {
                viewModel.Email = userId;
            }
            else if (string.IsNullOrEmpty(UserSession.UserProfile.LogonName) && !string.IsNullOrEmpty(userId))
            {
                //This is a guid
                var profile = _userService.GetUserByGuid(userId);
                if (profile != null)
                    viewModel.Email = profile.LogonName;
            }
            else
            {
                viewModel.Email = UserSession.UserProfile.LogonName;
            }

            base.SetupAdParameters(viewModel);

            // ReSharper disable once Mvc.ViewNotResolved
            return View(viewModel);
        }

        [HttpPost]
        public virtual ActionResult Unsubscribe(UnsubscribeViewModel viewModel)
        {

            var unsubResults = _userService.Unsubscribe(viewModel.Email, NhsRoute.PartnerId, "Y", "N");

            switch (unsubResults)
            {
                case UnsubscribeResult.Successful:
                case UnsubscribeResult.InvalidEmail:
                    viewModel.ShowThankYou = true;
                    viewModel.ConfirmationMessage = "<strong>" + viewModel.Email + "</strong>" +
                                                    " " + LanguageHelper.HasBeenRemovedFromOurMailing;
                    break;
                //case UnsubscribeResult.InvalidEmail:
                //  ModelState.AddModelError("InvalidEmail", HttpUtility.HtmlDecode(LanguageHelper.MSG_UNSUBSCRIBE_INVALID_EMAIL));
                // break;
            }

            base.SetupAdParameters(viewModel);
            // ReSharper disable once Mvc.ViewNotResolved
            return View(viewModel);
        }

        public virtual ActionResult SignIn()
        {
            var model = new LoginViewModel
                {
                    FromPage = RouteParams.NextPage.Value(),
                    OtherContent = !Request.IsAjaxRequest(),
                    IsAjaxRequest = Request.IsAjaxRequest(),
                    Globals = { PartnerInfo = _partnerService.GetPartner(NhsRoute.PartnerId) }
                };

            if (NhsRoute.ShowMobileSite)
            {
                if (string.IsNullOrEmpty(model.FromPage))
                    model.FromPage = HttpUtility.UrlDecode(new List<RouteParam>().ToUrl(Pages.Home));
                return View("SignIn", model);
            }

            if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
                base.AddCanonicalTag(model);

            if (model.Globals.PartnerInfo.UsesSso.ToBool())
                return View(NhsMvc.Default.Views.Account.SsoLogin, model);

            if (string.IsNullOrEmpty(model.FromPage))
                model.FromPage = GetFBRedirect(Request.Url.ToString());

            if (Request.IsAjaxRequest() == false)
            {
                base.SetupAdParameters(model);
            }

            return View(NhsMvc.Default.Views.Account.Login, model);
        }


        public virtual ActionResult LogOut()
        {
            var model = new LoginViewModel
                {
                    FromPage = RouteParams.NextPage.Value(),
                    OtherContent = !Request.IsAjaxRequest(),
                    Globals = { PartnerInfo = _partnerService.GetPartner(NhsRoute.PartnerId) }
                };

            if (!string.IsNullOrEmpty(model.Globals.PartnerInfo.SLODestinationURL))
            {
                var successRequest = _ssoService.RequestLogOutAtIdentityProvider(System.Web.HttpContext.Current.Response,
                                                            NhsRoute.PartnerSiteUrl, model.Globals.PartnerInfo.SLODestinationURL);
                _userService.SignOut(NhsRoute.PartnerId);
                return successRequest ? (ActionResult)new EmptyResult() : Redirect(Request.UrlReferrer.ToString());
            }

            _userService.SignOut(NhsRoute.PartnerId);
            return Redirect(Request.UrlReferrer.ToString());
        }

        public virtual ActionResult Login()
        {
            var model = new LoginViewModel { FromPage = HttpUtility.UrlDecode(RouteParams.NextPage.Value()), IsAjaxRequest = true };

            if (NhsRoute.ShowMobileSite)
                return PartialView(NhsMvc.Default.ViewsMobile.Account.SignIn, model);

            var partner = _partnerService.GetPartner(NhsRoute.PartnerId);
            if (partner.UsesSso.ToBool())
                return PartialView(NhsMvc.Default.Views.Account.SsoLogin, model);

            // In Pro after the Login we have to execute the action that the user wanted to perform
            if (RouteParams.ForcePopupPlayer.Value<bool>())
            {
                TempData["ForcePopupPlayerAction"] = true;
            }

            
            if (RouteParams.ToPage.Value() == Pages.GetSavetoFavorites)
            {
                // Reconstruct the original parameter list and save it
                var paramsList = new List<RouteParam>
                {
                    new RouteParam(RouteParams.ListingType, RouteParams.ListingType.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.CommunityId, RouteParams.CommunityId.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.BuilderId, RouteParams.BuilderId.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.PropertyId, RouteParams.PropertyId.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.IsNextSteps, RouteParams.IsNextSteps.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.IsCommunityResultsPage, RouteParams.IsCommunityResultsPage.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.NextWidth, RouteParams.NextWidth.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.NextHeight, RouteParams.NextHeight.Value(), RouteParamType.QueryString)
                };

                TempData["PerformSaveToFavoritesAction"] = paramsList;
            }
            else if (RouteParams.ToPage.Value() == Pages.SendCustomBrochure)
            {
                // Reconstruct the original parameter list and save it
                var paramsList = new List<RouteParam>
                {
                    new RouteParam(RouteParams.Refer, RouteParams.Refer.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.MarketId, RouteParams.MarketId.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.LeadType, RouteParams.LeadType.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.Community, RouteParams.Community.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.PlanId, RouteParams.PlanId.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.SpecId, RouteParams.SpecId.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.Builder, RouteParams.Builder.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.IsBilled, RouteParams.IsBilled.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.LeadAction, RouteParams.LeadAction.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.FromPage, RouteParams.FromPage.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.Market, RouteParams.Market.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.IsMultiBrochure, RouteParams.IsMultiBrochure.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.CommunityList, RouteParams.CommunityList.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.NextWidth, RouteParams.NextWidth.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.NextHeight, RouteParams.NextHeight.Value(), RouteParamType.QueryString)
                    
                };

                TempData["PerformSendCustomBrochureAction"] = paramsList;
            }
            else if (RouteParams.ToPage.Value() == "ContactBuilder")
            {
                var paramsList = new List<RouteParam>()
                {
                    new RouteParam(RouteParams.Action, RouteParams.Action.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.Radio, RouteParams.Radio.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.GeneralInquiry, RouteParams.GeneralInquiry.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.ScheduleAppointment, RouteParams.ScheduleAppointment.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.Message, RouteParams.Message.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.BuilderName, RouteParams.BuilderName.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.HomeTitle, RouteParams.HomeTitle.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.CommunityName, RouteParams.CommunityName.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.IsPlan, RouteParams.IsPlan.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.CommunityId, RouteParams.CommunityId.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.BuilderId, RouteParams.BuilderId.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.PlanId, RouteParams.PlanId.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.SpecId, RouteParams.SpecId.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.MarketId, RouteParams.MarketId.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.RequestAnAppointment, RouteParams.RequestAnAppointment.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.NextWidth, RouteParams.NextWidth.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.NextHeight, RouteParams.NextHeight.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.UpdateTargetId, RouteParams.UpdateTargetId.Value(), RouteParamType.QueryString)
                };

                TempData["PerformContactBuilderAction"] = paramsList;
            }
            else if (RouteParams.ToPage.Value() == "CompensationLead")
            {
                var paramsList = new List<RouteParam>
                {
                    new RouteParam(RouteParams.Action, RouteParams.Action.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.NextWidth, RouteParams.NextWidth.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.NextHeight, RouteParams.NextHeight.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.Message, RouteParams.Message.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.BuilderName, RouteParams.BuilderName.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.HomeTitle, RouteParams.HomeTitle.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.CommunityName, RouteParams.CommunityName.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.IsPlan, RouteParams.IsPlan.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.CommunityId, RouteParams.CommunityId.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.BuilderId, RouteParams.BuilderId.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.PlanId, RouteParams.PlanId.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.SpecId, RouteParams.SpecId.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.MarketId, RouteParams.MarketId.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.RequestAnAppointment, RouteParams.RequestAnAppointment.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.ShowSaveActivityClient, RouteParams.ShowSaveActivityClient.Value(), RouteParamType.QueryString)
                };

                TempData["PerformAgentCompensationAction"] = paramsList;
            }
            else if (RouteParams.Action.Value() == "AgentPolicyRequest")
            {
                var paramsList = new List<RouteParam>
                {
                    new RouteParam(RouteParams.AgentPolicyLink, RouteParams.AgentPolicyLink.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.EventCode, RouteParams.EventCode.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.CommunityId, RouteParams.CommunityId.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.BuilderId, RouteParams.BuilderId.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.PlanId, RouteParams.PlanId.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.SpecId, RouteParams.SpecId.Value(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.MarketId, RouteParams.MarketId.Value(), RouteParamType.QueryString)
                };

                TempData["PerformAgentPolicyAction"] = paramsList;
            }
            return PartialView(NhsMvc.Default.Views.Account.Login, model);
        }

        public virtual Boolean GetLogin(string u, string p)
        {
            bool res = true;
            try
            {
                _userService.SignIn(u, p, NhsRoute.PartnerId, LanguageHelper.WrongPassword, LanguageHelper.EmailDoNotExist);
            }
            catch (InvalidPasswordException)
            {
                res = false;
            }
            catch (UnknownUserException)
            {
                res = false;
            }
            return res;
        }

        private string GetFBRedirect(string currPage)
        {
            string redirect = string.Empty;

            if (currPage.ToLower().Contains("/signin"))
            {
                redirect = new List<RouteParam>().ToUrl(Pages.Home);
            }

            return redirect;
        }

        [HttpPost]
        public virtual JavaScriptResult Login(LoginViewModel model)
        {
            // Checks login
            try
            {
                _userService.SignIn(model.Email, model.Password, NhsRoute.PartnerId, LanguageHelper.WrongPassword, LanguageHelper.EmailDoNotExist);

                //  If 'NextPage' isn't empty, redirects user
                string jsToExecute = string.Empty;
                string nextPage = model.FromPage;
                if (!string.IsNullOrWhiteSpace(nextPage))
                    nextPage = HttpUtility.UrlDecode(nextPage);
                NhsUrl nUrl = NhsUrlHelper.GetFriendlyUrl();
                string domainPartnerSiteUrl = nUrl.SiteRootWithDomainInfo;
                string url = domainPartnerSiteUrl.TrimEnd('/');

                // Case where I have to perfom the Save to Favorite Action
                if (TempData["PerformSaveToFavoritesAction"] != null || TempData["PerformSendCustomBrochureAction"] != null || TempData["PerformContactBuilderAction"] != null || TempData["PerformAgentCompensationAction"] != null || TempData["PerformAgentPolicyAction"] != null)
                {
                    // Get the original parameters
                    List<RouteParam> paramList;
                    if (TempData["PerformSaveToFavoritesAction"] != null)
                    {
                        paramList = (List<RouteParam>)TempData["PerformSaveToFavoritesAction"];
                    }
                    else if (TempData["PerformSendCustomBrochureAction"] != null)
                    {
                        paramList = (List<RouteParam>)TempData["PerformSendCustomBrochureAction"];
                    }
                    else if (TempData["PerformContactBuilderAction"] != null)
                    {
                        paramList = (List<RouteParam>)TempData["PerformContactBuilderAction"];
                    }
                    else if (TempData["PerformAgentCompensationAction"] != null)
                    {
                        paramList = (List<RouteParam>)TempData["PerformAgentCompensationAction"];
                    }
                    else
                    {
                        paramList = (List<RouteParam>)TempData["PerformAgentPolicyAction"];
                    }


                    // Get the Width and Height of the window
                    var widthParam = paramList.FirstOrDefault(RouteParam => RouteParam.Name == RouteParams.NextWidth);
                    var heightParam = paramList.FirstOrDefault(RouteParam => RouteParam.Name == RouteParams.NextHeight);

                    int heigth = 0, width = 0;

                    if (widthParam != null && heightParam != null)
                    {
                        width = Convert.ToInt32(widthParam.Value);
                        heigth = Convert.ToInt32(heightParam.Value);

                        // Remove the width and height wich are not required
                        paramList.Remove(widthParam);
                        paramList.Remove(heightParam);
                    }


                    String modalTitle = String.Empty, urlToExecute = String.Empty;
                    var displayModal = true;
                    if (TempData["PerformSaveToFavoritesAction"] != null)
                    {
                        modalTitle = "Save this listing as a client favorite?";
                        urlToExecute = paramList.ToUrl(Pages.GetSavetoFavorites, true);
                    }
                    else if (TempData["PerformSendCustomBrochureAction"] != null)
                    {
                        // Insert the Email which is required
                        paramList.Insert(0,
                            new RouteParam(RouteParams.Email, UserSession.UserProfile.Email, RouteParamType.Friendly));

                        // And the Is MVC that is always true in these cases
                        paramList.Insert(1, new RouteParam(RouteParams.IsMvc, Boolean.TrueString, RouteParamType.Friendly));

                        modalTitle = "Send Customized Brochure to Clients";
                        urlToExecute = paramList.ToUrl(Pages.SendCustomBrochure, true);
                    }
                    else if (TempData["PerformContactBuilderAction"] != null)
                    {
                        var actionParam = paramList.FirstOrDefault(RouteParam => RouteParam.Name == RouteParams.Action);
                        paramList.Remove(actionParam);

                        var updateTargetIdParam = paramList.FirstOrDefault(RouteParam => RouteParam.Name == RouteParams.UpdateTargetId);
                        paramList.Remove(updateTargetIdParam);
                        var updateTargetIdValue = updateTargetIdParam != null ? "#" + updateTargetIdParam.Value : "";

                        modalTitle = "Save this activity to a client record?";
                        urlToExecute = paramList.ToUrl(actionParam != null ? actionParam.Value : String.Empty, true);

                        // Add the action for displaying the Send another Message page
                        jsToExecute += "var data = $jq('" + updateTargetIdValue + " .pro_ValuesInputForm input," + updateTargetIdValue + " .pro_ValuesInputForm textarea').serialize();";
                        jsToExecute += "crm.sendMessageToTheBuilder('" + updateTargetIdValue + "', crm, data, false);";
                    }
                    else if (TempData["PerformAgentCompensationAction"] != null)
                    {
                        var actionParam = paramList.FirstOrDefault(RouteParam => RouteParam.Name == RouteParams.Action);
                        paramList.Remove(actionParam);

                        modalTitle = "Save this activity to a client record?";
                        urlToExecute = paramList.ToUrl(actionParam != null ? actionParam.Value : String.Empty, true);

                        // Add the action for displaying the Send another Message page
                        jsToExecute += "var data = jQuery('#pro_agCompensationLeadFormDivId textarea, #pro_divAgCompensationLead input[type=hidden]').serialize();";


                        // Display or not the modal
                        var showModalParam = paramList.FirstOrDefault(RouteParam => RouteParam.Name == RouteParams.ShowSaveActivityClient);
                        paramList.Remove(showModalParam);

                        if (!(showModalParam != null && Boolean.Parse(showModalParam.Value)))
                        {
                            displayModal = false;
                            jsToExecute += "var messageSent = crmAgCompensationLead.sendMessageCompensationLead(crmAgCompensationLead, data, false, true);";
                        }
                        else
                        {
                            jsToExecute += "var messageSent = crmAgCompensationLead.sendMessageCompensationLead(crmAgCompensationLead, data, false, false);";
                        }
                    }
                    else
                    {
                        // Get the required parameters
                        var linkPolicyParam = paramList.FirstOrDefault(RouteParam => RouteParam.Name == RouteParams.AgentPolicyLink);
                        var eventCodeParam = paramList.FirstOrDefault(RouteParam => RouteParam.Name == RouteParams.EventCode);
                        var communityIdParam = paramList.FirstOrDefault(RouteParam => RouteParam.Name == RouteParams.CommunityId);
                        var builderIdParam = paramList.FirstOrDefault(RouteParam => RouteParam.Name == RouteParams.BuilderId);
                        var specIdParam = paramList.FirstOrDefault(RouteParam => RouteParam.Name == RouteParams.SpecId);
                        var planIdParam = paramList.FirstOrDefault(RouteParam => RouteParam.Name == RouteParams.PlanId);
                        var marketIdParam = paramList.FirstOrDefault(RouteParam => RouteParam.Name == RouteParams.MarketId);

                        // The agent policy request action does not open any modal, it just open a link in a separate window
                        displayModal = false;
                        jsToExecute += string.Format(" if(typeof logger != 'undefined') {{ logger.logAndRedirect(this, '{0}', '{1}', {2}, {3}, {4}, {5}, {6}, {7} ); window.parent.tb_remove(); window.location.reload(); }}", linkPolicyParam.Value, eventCodeParam.Value, communityIdParam.Value, builderIdParam.Value, planIdParam.Value, specIdParam.Value, marketIdParam.Value, "true");
                        jsToExecute += "window.parent.tb_remove();";
                        jsToExecute += "window.location.reload();";
                    }

                    if (displayModal)
                    {
                        jsToExecute += "window.parent.tb_ChangeUrl('" + urlToExecute + "');";
                        jsToExecute += "window.parent.tb_ChangeTitle('');"; // Add the title.
                        jsToExecute += "window.parent.tb_ChangeTitle('" + modalTitle + "');";
                        jsToExecute += "window.parent.tb_resizeWindow(" + width + ", " + heigth + ");";
                        jsToExecute += "window.parent.tb_DisplayLoadingIcon();";
                    }

                    jsToExecute += "reloadPageAfterLogin = true;";
                }
                else if (!model.OtherContent)
                {
                    if (!string.IsNullOrEmpty(nextPage))
                        jsToExecute = "window.location = '/" + nextPage.TrimStart('/') + "';";
                    else if (TempData["ForcePopupPlayerAction"] != null && TempData["ForcePopupPlayerAction"].ToType<bool>())
                        jsToExecute = "window.location = window.location.href + ((location.href.indexOf('?') == -1? '?' : '&') + 'forcepopupplayer=true')";
                    else  
                        jsToExecute = "window.location.reload();";
                }
                else
                {
                    if (!string.IsNullOrEmpty(nextPage) && nextPage.StartsWith("http://") == false)
                    {
                        if (!nextPage.ToLower().Contains(url.ToLower()) &&
                            !nextPage.StartsWith(nUrl.SiteRoot.TrimEnd('/')))
                        {
                            if (nextPage.Trim().IndexOfAny(new char[] { '/' }) != 0)
                            {
                                nextPage = "/" + nextPage;
                            }

                            jsToExecute = "window.location = '" + url + nextPage + "';";
                        }
                        else
                        {
                            jsToExecute = "window.location = '" + nextPage + "';";
                        }
                    }
                    else
                    {
                        jsToExecute = "window.location = '" + url + "';";
                    }
                }
                return
                    JavaScript(" if(typeof _gaq != 'undefined') _gaq.push(['_setCustomVar', 1,'User', '{web:" +
                               UserSession.UserProfile.UserID.Replace("{", string.Empty).Replace("}", string.Empty) +
                               "}',2]); " + " setTimeout(function() {" + jsToExecute + "}, 100); ");
            }
            catch (InvalidPasswordException e)
            {
                return JavaScript("jQuery(\"#nhs_Errors\").append('<li>" + e.Message + "</li>')");
            }
            catch (UnknownUserException e)
            {
                return JavaScript("jQuery(\"#nhs_Errors\").append('<li>" + e.Message + "</li>')");
            }
        }

        public virtual ActionResult RegisterOnPage()
        {
            var viewModel = GetRegisterViewModel();
            base.AddCanonicalTag(viewModel);
            base.SetupAdParameters(viewModel);
            return View("Register", viewModel);
        }

        public virtual ActionResult Register()
        {
            UserSession.PersistUrlParams(this); // Persist params;
            return PartialView(NhsRoute.ShowMobileSite ? NhsMvc.Default.ViewsMobile.Account.Register : NhsMvc.Default.Views.Account.Register, GetRegisterViewModel());
        }

        //due an error 59608
        //[HttpGet]
        public virtual JsonResult GetHomeMarketInfo(string zip)
        {
            var marketInfo = _marketService.GetMarketInfoForCreateAccount(zip);
            return Json(marketInfo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public virtual ActionResult Register(RegisterViewModel model)
        {
            // Hack
            if (model.ComingFromLead)
                ModelState.Clear();

            if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
            {
                ModelState.Remove("AgencyName");
            }
            //model.LiveOutsideReg = string.IsNullOrEmpty(model.ZipCodeReg);
            //ValidateRegister(model);


            if (_userService.LogonNameExists(model.Email, NhsRoute.PartnerId))
                return Json(new { logonNameExists = true }, JsonRequestBehavior.AllowGet);

            var profile = new UserProfile();

            profile.LogonName = model.Email;
            profile.FirstName = model.Name.FirstPart(" ");
            profile.LastName = model.Name.LastPart(" ");
            profile.Password = model.Password;
            profile.DateRegistered = DateTime.Now;
            profile.DateLastChanged = DateTime.Now;
            profile.PartnerId = NhsRoute.PartnerId;
            profile.RegistrationMarket = model.MarketId;
            profile.AgencyName = model.AgencyName;
            profile.RealStateLicense = model.RealEstateLicense;

            //My home needs defaults
            profile.MoveInDate = -1;

            // NOT International users
            if (!model.LiveOutsideReg)
            {
                profile.PostalCode = model.ZipCodeReg;
            }

            profile.MailingList = model.Newsletter ? "1" : "0";
            profile.HomeWeeklyOptIn = model.Newsletter;
            if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
                profile.MarketOptin = model.Promos ? "1" : "0";
            else
            {
                profile.WeeklyOptin = model.Promos;
                profile.MarketOptin = "0";
            }

            // Setting those values ... required in stored proc.
            profile.BoxRequestedDate = DateTime.Now;
            profile.InitialMatchDate = DateTime.Now;
            profile.LastMatchesSentDate = DateTime.Now;

            // Create user
            _userService.CreateProfile(profile, NhsRoute.BrandPartnerId, LanguageHelper.WrongPassword, LanguageHelper.EmailDoNotExist, LanguageHelper.UserAlreadyExist);
            _userService.CreateRegistrationLead(profile);

            if (model.Globals.PartnerInfo == null)
                model.Globals.PartnerInfo = _partnerService.GetPartner(NhsRoute.PartnerId);

            // Set the Partner site name and the email
            model.FromEmail = model.Globals.PartnerInfo.FromEmail;
            model.PartnerSiteName = model.Globals.PartnerInfo.SiteName;

            // Redirects user
            var url = GetRegisterRedirectUrl(model);
            //Reloads the page after creating account for NHSPro as there are no Alerts created. 

            if (NhsRoute.IsBrandPartnerNhsPro)
                return Json(new { script = "window.location.reload()" });

            if (!model.IsModal)
                return Json(new { redirectUrl = url });

            var searchAlertModel = new SearchAlertViewModel.CreateAlertViewModel();
            BindCreateAlertData(searchAlertModel);
            return PartialView(NhsMvc.Default.Views.SearchAlert.CreateAlert, searchAlertModel);
        }

        private RegisterViewModel GetRegisterViewModel()
        {
            string requrl = Request.Url.ToString().ToLower();
            var useOtherContent = requrl.Contains("register") && !requrl.Contains("modal");
            var model = new RegisterViewModel
            {
                Login = new SmallLoginViewModel(),
                OtherContent = useOtherContent || NhsUrl.GetOtherContent,
                FromPage = RouteParams.NextPage.Value(),
                IsAjaxRequest = Request.IsAjaxRequest()
            };

            model.ComingFromLead = !string.IsNullOrEmpty(RouteParams.RegFromLeads.Value()) && NhsUrl.GetRegFromLeads == "true";
            model.Promos = true;

            // Load profile information
            if (model.ComingFromLead)
            {
                // Sets profile fields
                var profile = UserSession.UserProfile;
                model.Email = profile.LogonName;
                model.Name = profile.FirstName +" " +profile.LastName;
                model.ZipCodeReg = profile.PostalCode;

            }

            return model;
        }

        public virtual ActionResult RecoverPassword()
        {
            var model = new ForgotPasswordViewModel
            {
                OtherContent = true,
                ShowGoback = Request.UrlReferrer != null,
                IsAjaxRequest = false,
                FromPage = RouteParams.NextPage.Value()
            };

            base.AddCanonicalTag(model);

            return View("RecoverPassword", model);

        }

        [HttpPost]
        public virtual ActionResult RecoverPassword(ForgotPasswordViewModel model)
        {
            model = ValidateForgotPassword(model);
            return PartialView(NhsRoute.ShowMobileSite ? NhsMvc.Default.ViewsMobile.Account.RecoverPassword : NhsMvc.Default.Views.Account.RecoverPassword, model);
        }

        public virtual ActionResult ForgotPassword()
        {
            var model = new ForgotPasswordViewModel
            {
                ShowGoback = true,
                IsAjaxRequest = true,
                FromPage = RouteParams.NextPage.Value()
            };

            return PartialView(NhsRoute.ShowMobileSite ? NhsMvc.Default.ViewsMobile.Account.RecoverPassword : NhsMvc.Default.Views.Account.RecoverPassword, model);
        }

        [HttpPost]
        public virtual ActionResult ForgotPassword(ForgotPasswordViewModel model)
        {
            var message = LanguageHelper.YourPasswordHasBeenSent;
            model = ValidateForgotPassword(model);

            if (!model.PasswordChanged || !NhsRoute.ShowMobileSite)
                return PartialView(NhsRoute.ShowMobileSite ? NhsMvc.Default.ViewsMobile.Account.RecoverPassword : NhsMvc.Default.Views.Account.RecoverPassword, model);

            var js = "alert('" + message + "');";
            if (model.IsAjaxRequest)
                js += "jQuery('#nhs_SignIn').click();";
            else
            {
                var url =
                    (!string.IsNullOrEmpty(model.FromPage) ? new List<RouteParam> { new RouteParam(RouteParams.NextPage, HttpUtility.UrlEncode(model.FromPage), RouteParamType.QueryString) }
                                                           : new List<RouteParam>()).ToUrl(Pages.SignIn);
                js += "window.location = '" + url + "'";
            }
            return NhsRoute.ShowMobileSite ? Json(new { Message = message}) : Json(new { JavaScriptCode = js });
        }

        public virtual JsonResult SimpleRecoverPassword(string email)
        {
            // Get profile
            var profile = _userService.GetUserByLogonName(email, NhsRoute.PartnerId);
            if (profile != null)
            {
                _userService.CreateRecoverPasswordLead(profile);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ForgotPasswordViewModel ValidateForgotPassword(ForgotPasswordViewModel model)
        {
            // Validation
            if (model.Email != null && !StringHelper.IsValidEmail(model.Email))
                ModelState.AddModelError("Email", LanguageHelper.MSG_ACCOUNT_INVALID_EMAIL);

            if (ModelState.IsValid)
            {
                // Get profile
                var profile = _userService.GetUserByLogonName(model.Email, NhsRoute.PartnerId);
                if (profile != null)
                {
                    _userService.CreateRecoverPasswordLead(profile);
                    model.PasswordChanged = true;
                }
                else
                    ModelState.AddModelError("Email", LanguageHelper.MSG_LOGIN_WRONG_EMAIL);
            }


            return model;
        }

        public virtual ActionResult RegisterThanks()
        {
            return PartialView(NhsMvc.Default.Views.Account.RegisterThanks);
        }

        public virtual ActionResult ForgotPasswordAsString()
        {
            var message = LanguageHelper.MSG_ACCOUNT_INVALID_EMAIL;

            var lead = LeadUtil.GetLeadInfoFromSession();
            var email = lead.LeadUserInfo.Email;

            if (!_userService.EmailExists(email, NhsRoute.PartnerId)) 
                return Content(message);

            UserProfile profile = _userService.GetUserByLogonName(email, NhsRoute.PartnerId);
            if (profile == null) return Content(message);
            _userService.CreateRecoverPasswordLead(profile);
            message = "Please check your email account in order to get your password.";

            return Content(message);
        }

        public virtual ActionResult ManageAccount()
        {
            if (UserSession.UserProfile.ActorStatus != WebActors.ActiveUser)
            {
                if (Request.Url != null)
                {
                    var next = HttpUtility.UrlEncode(Request.Url.PathAndQuery);
                    return Redirect(Pages.SignIn, new List<RouteParam> { new RouteParam(RouteParams.NextPage, next, RouteParamType.QueryString) });
                }
            }

            var model = GetManageAccountModel();
            if (Request.IsAjaxRequest())
            {
                model.IsPopupWindow = true;
                return PartialView(NhsMvc.Default.Views.Account.ManageAccount, model);
            }
            // ReSharper disable once Mvc.ViewNotResolved
            return View(NhsMvc.Default.Views.Account.ManageAccount, model);
        }

        private PartialViewModels.ManageAccount GetManageAccountModel()
        {
            IProfile userProfile = UserSession.UserProfile;

            string phone = userProfile.DayPhone;
            if (phone != null)
            {
                if (phone.Length == 10)
                {
                    phone = "(" + phone.Substring(0, 3) + ") " + phone.Substring(3, 3) + "-" + phone.Substring(6, 4);
                }
            }

            var address = new StringBuilder(userProfile.Address1 ?? string.Empty);

            if (string.IsNullOrWhiteSpace(userProfile.Address2) == false)
            {
                address.AppendFormat(" ,{0}", userProfile.Address2);
            }

            var model = new PartialViewModels.ManageAccount
            {
                FirstName = userProfile.FirstName,
                LastName = userProfile.LastName,
                Email = userProfile.Email,
                EmailConfirm = userProfile.Email,
                Password = userProfile.Password,
                PasswordConfirm = userProfile.Password,
                LogonName = userProfile.LogonName,
                BusinessPhone = phone,
                MobilePhone = userProfile.EvePhone,
                AgencyName = userProfile.AgencyName,
                RealEstateLicense = userProfile.RealEstateLicense,
                City = userProfile.City,
                CityPro = userProfile.City,
                State = userProfile.State,
                StatePro = userProfile.State,
                ZipCode = userProfile.PostalCode,
                HomeMarket = (string.IsNullOrEmpty(userProfile.RegMetro)) ? string.Empty : _userService.GetMarketName(userProfile.RegMetro.ToType<int>(), NhsRoute.PartnerId),
                InternationalPhone = string.IsNullOrEmpty(userProfile.PostalCode),
                Newsletter = userProfile.MailList == "1",
                StreetAdress = address.ToString(),
                Promos = userProfile.MarketOptIn == "1",
                PromosPro = userProfile.WeeklyNotifierOptIn,
            };

            FillDropdowns(model, userProfile);

            if (model.StreetAdress == ", ")
            {
                model.StreetAdress = string.Empty;
            }

            base.SetupAdParameters(model);
            return model;
        }

        private void FillDropdowns(PartialViewModels.ManageAccount model, IProfile userProfile)
        {
            model.States = GetItem(_userService.GetStates(), userProfile.State, true, valueField: "Text");
            model.MoveInDates = GetItem(_userService.GetMoveInDateItems(),
                (userProfile.MoveInDate != -1 ? userProfile.MoveInDate.ToString(CultureInfo.InvariantCulture) : null));
            model.HomeFinancials = GetItem(_userService.GetFinancePrefItems(),
                (userProfile.FinancePreference != 0
                    ? userProfile.FinancePreference.ToString(CultureInfo.InvariantCulture)
                    : null));
            model.WhyLookingNewHomes = GetItem(_userService.GetMoveReasonItems(),
                (userProfile.Reason != 0 ? userProfile.Reason.ToString(CultureInfo.InvariantCulture) : null));
        }

        private SelectList GetItem(DataTable data, string selectedValue, bool sortByText = false, string textField = "Text", string valueField = "Value")
        {

            var items = (from dataRow in data.AsEnumerable()
                         let text = dataRow.Field<string>(textField)
                         let value = dataRow.Field<string>(valueField)
                         select new SelectListItem()
                         {
                             Text = text,
                             Value = value,
                             Selected = (string.IsNullOrWhiteSpace(selectedValue) == false && value == selectedValue)
                         });

            if (sortByText)
            {
                items = items.OrderBy(o => o.Text);
            }

            var list = new SelectList(items, "Value", "Text", selectedValue);
            return list;
        }

        [HttpPost]
        public virtual ActionResult ManageAccountUpdate(PartialViewModels.ManageAccount model)
        {
            if (UserSession.UserProfile.ActorStatus != WebActors.ActiveUser)
            {
                if (Request.Url != null)
                {
                    var next = HttpUtility.UrlEncode(Request.Url.PathAndQuery);
                    return Redirect(Pages.SignIn, new List<RouteParam> { new RouteParam(RouteParams.NextPage, next, RouteParamType.QueryString) });
                }
            }

            var userProfile = UserSession.UserProfile;
            if (ModelState.IsValid)
            {
                if (FormValid(model))
                {
                    ModelState.Clear();
                    var originalUser = CopyUser();
                    bool isPro = (NhsRoute.IsBrandPartnerNhsPro);
                    userProfile.LogonName = model.Email;
                    userProfile.FirstName = model.FirstName;
                    userProfile.LastName = model.LastName;
                    userProfile.Address1 = model.StreetAdress;
                    userProfile.Address2 = string.Empty;
                    userProfile.DayPhone = model.BusinessPhone;
                    userProfile.EvePhone = model.MobilePhone ?? string.Empty;
                    userProfile.DayPhoneExt = string.Empty;
                    userProfile.City = model.City ?? model.CityPro;
                    userProfile.PostalCode = model.ZipCode ?? string.Empty;
                    userProfile.State = model.State;
                    userProfile.MailList = model.Newsletter ? "1" : "0";
                    userProfile.HomeWeeklyOptIn = model.Newsletter;
                    userProfile.MarketOptIn = isPro ? model.Promos.ToType<int>().ToString() : model.Promos.ToType<int>().ToString();
                    if (model.InternationalPhone)   // international have no zip
                        userProfile.PostalCode = string.Empty;

                    if (UserSession.UserProfile.Password != model.Password)
                    {
                        userProfile.Password = model.Password;
                    }

                    // These are not used anymore but just given support as used in stored procs.
                    userProfile.BoxRequestedDate = DateTime.Now;
                    userProfile.InitialMatchDate = DateTime.Now;

                    userProfile.MoveInDate = Convert.ToInt16(model.MoveInDate);
                    if (model.HomeFinancial.ToType<int>() != -1)
                        userProfile.FinancePreference = model.HomeFinancial.ToType<int>();
                    if (model.WhyLookingNewHome.ToType<int>() != -1)
                        userProfile.Reason = model.WhyLookingNewHome.ToType<int>();

                    if (NhsRoute.IsBrandPartnerNhsPro)
                    {
                        userProfile.DayPhone = model.BusinessPhone;
                        userProfile.EvePhone = model.MobilePhone;
                        userProfile.RegMetro = model.HomeMarket;
                        userProfile.AgencyName = model.AgencyName;
                        userProfile.RealEstateLicense = model.RealEstateLicense;
                        userProfile.City = model.CityPro;
                        userProfile.State = model.StatePro;

                        var finalProfile = UserSession.UserProfile.SetValuesForBrochureTable(originalUser, null);
                        _userService.SaveBrochureTemplate(finalProfile);
                    }

                    userProfile.SaveProfile();
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }

            FillDropdowns(model, userProfile);
            SetupAdParameters(model);
            return PartialView(NhsMvc.Default.Views.Account.ManageAccount, model);
        }

        private bool FormValid(PartialViewModels.ManageAccount model)
        {
            bool valid = true;
            bool isPro = (NhsRoute.IsBrandPartnerNhsPro);

            //ModelState.AddModelError("", LanguageHelper.MSG_ACCOUNT_FORM_INVALID);          

            // Checks password only if it's not comming from lead form
            if (NhsUrl.GetFromPage != Pages.LeadsRequestInfoLogged && NhsUrl.GetFromPage != Pages.LeadsRequestInfoLoggedModal)
            {
                //Cross check this old way of checking password
                if (model.Password != "**********")
                {
                    if (string.IsNullOrEmpty(model.Password) || model.Password.Length < 5 || model.Password.Length > 20)
                    {
                        ModelState.AddModelError("Password",
                            string.IsNullOrWhiteSpace(model.Password)
                                ? LanguageHelper.MSG_ACCOUNT_NO_PASSWORD
                                : LanguageHelper.MSG_ACCOUNT_INVALID_PASSWORD);
                        valid = false;
                    }
                    else
                    {
                        if (model.Password != model.PasswordConfirm)
                        {
                            // MSG_ACCOUNT_DIFF_PASSWORD should be used instead. Update when this message 
                            // is available in StringResource

                            ModelState.AddModelError("PasswordConfirm", LanguageHelper.MSG_ACCOUNT_INVALID_PASSWORD);
                            valid = false;
                        }
                    }
                    if (!valid)
                    {
                        model.Password = string.Empty;
                        model.PasswordConfirm = string.Empty;
                    }
                }
                // End of Password check
            }

            if (string.IsNullOrWhiteSpace(model.Email))
            {
                ModelState.AddModelError("Email", LanguageHelper.MSG_ACCOUNT_NO_EMAIL);
                valid = false;
            }
            else if (isPro && string.IsNullOrWhiteSpace(model.PasswordConfirm))
            {
                ModelState.AddModelError("Email", LanguageHelper.MSG_ACCOUNT_NO_EMAIL);
                valid = false;
            }
            else if (isPro && (model.Email != model.EmailConfirm))
            {
                // MSG_ACCOUNT_DIFF_EMAIL should be used instead. Update when this message 
                // is available in StringResource               
                ModelState.AddModelError("EmailConfirm", LanguageHelper.MSG_ACCOUNT_INVALID_EMAIL);
                valid = false;
            }
            else
            {
                bool fValid = StringHelper.IsValidEmail(model.Email);

                if (!fValid)
                {
                    ModelState.AddModelError("Email", LanguageHelper.MSG_ACCOUNT_INVALID_EMAIL);
                    valid = false;
                }
                else
                {
                    if (UserSession.UserProfile.LogonName != model.Email)
                    {
                        if (_userService.LogonNameExists(model.Email, NhsRoute.PartnerId))
                        {
                            ModelState.AddModelError("Email", LanguageHelper.MSG_ACCOUNT_SAME_EMAIL);
                            valid = false;
                        }
                    }
                }
            }// End login check

            // Checks for NON-INTERNATIONAL users
            if (!model.InternationalPhone)
            {
                // Checks ZIP
                if (string.IsNullOrEmpty(model.ZipCode) || StringHelper.IsValidZip(model.ZipCode) == false)
                {
                    ModelState.AddModelError("ZipCode", LanguageHelper.MSG_ACCOUNT_INVALID_ZIP);
                    valid = false;
                }


                if (string.IsNullOrWhiteSpace(model.BusinessPhone) == false)
                {

                    if (StringHelper.StringContainsAnyLetter(model.BusinessPhone))
                    {
                        ModelState.AddModelError("BusinessPhone", HttpUtility.HtmlDecode(LanguageHelper.MSG_ACCOUNT_INVALID_PHONE));
                        valid = false;
                    }
                    var phone = StringHelper.FormatPhoneAsDigitsOnly<string>(model.BusinessPhone);

                    if (phone.Length == 10)
                    {
                        try
                        {
                            Int64 iPhone = Convert.ToInt64(phone);
                            model.BusinessPhone = iPhone.ToString(CultureInfo.InvariantCulture);
                        }
                        catch
                        {
                            ModelState.AddModelError("BusinessPhone", HttpUtility.HtmlDecode(LanguageHelper.MSG_ACCOUNT_INVALID_PHONE));
                            valid = false;
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("BusinessPhone", HttpUtility.HtmlDecode(LanguageHelper.MSG_ACCOUNT_INVALID_PHONE));
                        valid = false;
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(model.FirstName))
            {
                ModelState.AddModelError("FirstName", LanguageHelper.MSG_ACCOUNT_NO_FIRST);
                valid = false;
            }

            if (string.IsNullOrWhiteSpace(model.LastName))
            {
                ModelState.AddModelError("LastName", LanguageHelper.MSG_ACCOUNT_NO_LAST);
                valid = false;
            }

            return valid;
        }

        private Profile CopyUser()
        {
            return ((Profile)UserSession.UserProfile).CloneHimself();
        }

        #region Brochure

        public virtual ActionResult BrochureLogin()
        {
            var profile = UserSession.UserProfile;
            var model = new LoginViewModel
            {
                FromPage = Request.UrlReferrer + "?addtoprofile=true",
                Email = profile.LogonName
            };

            UserSession.PersistUrlParams(this); // Persist params;
            ModelState.Clear();
            return PartialView(NhsMvc.Default.Views.Account.BrochureLogin, model);
        }

        [HttpPost]
        public virtual ActionResult BrochureLogin(LoginViewModel model)
        {
            ValidateLogin(model);
            if (!ModelState.IsValid) return PartialView(NhsMvc.Default.Views.Account.BrochureLogin, model);

            try
            {
                // Checks login
                _userService.SignIn(model.Email, model.Password, NhsRoute.PartnerId, LanguageHelper.WrongPassword, LanguageHelper.EmailDoNotExist);
                return Json(new { redirectUrl = model.FromPage });
            }
            catch
            {
                ModelState.AddModelError("Email", @"Either the email address or the password appears to be incorrect. Please try again.");
            }

            return PartialView(NhsMvc.Default.Views.Account.BrochureLogin, model);
        }

        public virtual ActionResult BrochureRegister()
        {
            var profile = UserSession.UserProfile;
            var model = new RegisterViewModel
            {
                FromPage = Request.UrlReferrer + "?addtoprofile=true",
                Email = profile.LogonName,
                Name = profile.FirstName +" "+profile.LastName,
                ZipCodeReg = profile.PostalCode
            };
            UserSession.PersistUrlParams(this); // Persist params;
            ModelState.Clear();

            return PartialView(NhsMvc.Default.Views.Account.BrochureRegister, model);
        }

        [HttpPost]
        public virtual ActionResult BrochureRegister(RegisterViewModel model)
        {

            if (_userService.LogonNameExists(model.Email, NhsRoute.PartnerId))
                return Json(new { logonNameExists = true }, JsonRequestBehavior.AllowGet);


            if (string.IsNullOrEmpty(UserSession.UserProfile.LogonName))
                return Json(new { expiredSession = true }, JsonRequestBehavior.AllowGet);

            var profile = new UserProfile();
            var sessionProfile = UserSession.UserProfile;

            profile.LogonName = sessionProfile.Email;
            profile.FirstName = sessionProfile.FirstName;
            profile.LastName = sessionProfile.LastName;
            profile.Password = model.Password;
            profile.DateRegistered = DateTime.Now;
            profile.DateLastChanged = DateTime.Now;
            profile.PartnerId = NhsRoute.PartnerId;

            // Default dropdown values
            profile.MoveInDate = -1;
            profile.FinancePreference = null;
            profile.MovingReason = null;

            profile.PostalCode = sessionProfile.PostalCode;
            profile.MailingList = "0";
            profile.MarketOptin = "0";

            // Setting these values ... required in stored proc.
            profile.BoxRequestedDate = DateTime.Now;
            profile.InitialMatchDate = DateTime.Now;
            profile.LastMatchesSentDate = DateTime.Now;

            var savedCommunities = UserSession.UserProfile.Planner.SavedCommunities;
            var savedHomes = UserSession.UserProfile.Planner.SavedHomes;

            // Saving user info...also sets profile cookie
            UserSession.UserProfile.ActorStatus = WebActors.ActiveUser;

            try
            {
                if (!_userService.LogonNameExists(profile.LogonName, NhsRoute.BrandPartnerId))
                    _userService.CreateProfile(profile, NhsRoute.BrandPartnerId, LanguageHelper.WrongPassword, LanguageHelper.EmailDoNotExist, LanguageHelper.UserAlreadyExist);
                else
                {
                    return Json(new { logonNameExists = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (InvalidPasswordException)
            {
                return Json(new { logonNameExists = true }, JsonRequestBehavior.AllowGet);
            }

            _userService.CreateRegistrationLead(profile);

            foreach (var listing in savedCommunities)
            {
                UserSession.UserProfile.Planner.AddSavedCommunity(listing.ListingId, listing.BuilderId);
                UserSession.UserProfile.Planner.UpdateCommunityRequestDate(listing.ListingId, listing.BuilderId, listing.SourceRequestKey);
            }

            foreach (var listing in savedHomes)
            {
                UserSession.UserProfile.Planner.AddSavedHome(listing.ListingId, listing.ListingType == ListingType.Spec);
                UserSession.UserProfile.Planner.UpdateHomeRequestDate(listing.ListingId, listing.ListingType == ListingType.Spec, listing.SourceRequestKey);
            }

            return Json(new { redirectUrl = model.FromPage + "&newuser=true" });
        }

        #endregion


        #region BrochureV2

        public virtual ActionResult BrochureLoginV2()
        {
            var profile = UserSession.UserProfile;
            var model = new LoginViewModel
            {
                FromPage = Request.UrlReferrer + "?addtoprofile=true",
                Email = profile.LogonName
            };

            UserSession.PersistUrlParams(this); // Persist params;
            ModelState.Clear();
            return PartialView(NhsMvc.Default.Views.Common.LeadFormV2.BrochureLogin, model);
        }

        [HttpPost]
        public virtual ActionResult BrochureLoginV2(LoginViewModel model)
        {
            ValidateLogin(model);
            if (!ModelState.IsValid) return PartialView(NhsMvc.Default.Views.Common.LeadFormV2.BrochureLogin, model);

            try
            {
                // Checks login
                _userService.SignIn(model.Email, model.Password, NhsRoute.PartnerId, LanguageHelper.WrongPassword, LanguageHelper.EmailDoNotExist);
                return Json(new { redirectUrl = model.FromPage });
            }
            catch
            {
                ModelState.AddModelError("Email", @"Either the email address or the password appears to be incorrect. Please try again.");
            }

            return PartialView(NhsMvc.Default.Views.Common.LeadFormV2.BrochureLogin, model);
        }

        public virtual ActionResult BrochureRegisterV2()
        {
            var profile = UserSession.UserProfile;
            var model = new RegisterViewModel
            {
                FromPage = Request.UrlReferrer + "?addtoprofile=true",
                Email = profile.LogonName,
                Name = profile.FirstName + " " + profile.LastName,
                ZipCodeReg = profile.PostalCode
            };
            UserSession.PersistUrlParams(this); // Persist params;
            ModelState.Clear();

            return PartialView(NhsMvc.Default.Views.Common.LeadFormV2.BrochureRegister, model);
        }

        [HttpPost]
        public virtual ActionResult BrochureRegisterV2(RegisterViewModel model)
        {

            if (_userService.LogonNameExists(model.Email, NhsRoute.PartnerId))
                return Json(new { logonNameExists = true }, JsonRequestBehavior.AllowGet);


            if (string.IsNullOrEmpty(UserSession.UserProfile.LogonName))
                return Json(new { expiredSession = true }, JsonRequestBehavior.AllowGet);

            var profile = new UserProfile();
            var sessionProfile = UserSession.UserProfile;

            profile.LogonName = sessionProfile.Email;
            profile.FirstName = sessionProfile.FirstName;
            profile.LastName = sessionProfile.LastName;
            profile.Password = model.Password;
            profile.DateRegistered = DateTime.Now;
            profile.DateLastChanged = DateTime.Now;
            profile.PartnerId = NhsRoute.PartnerId;

            // Default dropdown values
            profile.MoveInDate = -1;
            profile.FinancePreference = null;
            profile.MovingReason = null;

            profile.PostalCode = sessionProfile.PostalCode;
            profile.MailingList = "0";
            profile.MarketOptin = "0";

            // Setting these values ... required in stored proc.
            profile.BoxRequestedDate = DateTime.Now;
            profile.InitialMatchDate = DateTime.Now;
            profile.LastMatchesSentDate = DateTime.Now;

            var savedCommunities = UserSession.UserProfile.Planner.SavedCommunities;
            var savedHomes = UserSession.UserProfile.Planner.SavedHomes;

            // Saving user info...also sets profile cookie
            UserSession.UserProfile.ActorStatus = WebActors.ActiveUser;

            try
            {
                if (!_userService.LogonNameExists(profile.LogonName, NhsRoute.BrandPartnerId))
                    _userService.CreateProfile(profile, NhsRoute.BrandPartnerId, LanguageHelper.WrongPassword, LanguageHelper.EmailDoNotExist, LanguageHelper.UserAlreadyExist);
                else
                {
                    return Json(new { logonNameExists = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (InvalidPasswordException)
            {
                return Json(new { logonNameExists = true }, JsonRequestBehavior.AllowGet);
            }

            _userService.CreateRegistrationLead(profile);

            foreach (var listing in savedCommunities)
            {
                UserSession.UserProfile.Planner.AddSavedCommunity(listing.ListingId, listing.BuilderId);
                UserSession.UserProfile.Planner.UpdateCommunityRequestDate(listing.ListingId, listing.BuilderId, listing.SourceRequestKey);
            }

            foreach (var listing in savedHomes)
            {
                UserSession.UserProfile.Planner.AddSavedHome(listing.ListingId, listing.ListingType == ListingType.Spec);
                UserSession.UserProfile.Planner.UpdateHomeRequestDate(listing.ListingId, listing.ListingType == ListingType.Spec, listing.SourceRequestKey);
            }

            return Json(new { redirectUrl = model.FromPage + "&newuser=true" });
        }

        #endregion

        #endregion

        #region Private Methods

        #region Create Alert
        private void BindCreateAlertData(SearchAlertViewModel.CreateAlertViewModel model)
        {
            // Dynamic drop down lists.             
            var states = _stateService.GetSearchAlertStates(NhsRoute.PartnerId);
            model.StateList = new SelectList(states, "StateAbbr", "StateName");
            model.AreaList = GetAreaListItems(model.State);
            model.CityList = GetCityListItems(model.Area);
            model.SchoolList = GetSchoolListItems(model.Area);
            model.BuilderList = GetBuilderListItems(model.Area);

            // Fixed drop down lists. 
            var radius = _lookupService.GetCommonListItems(CommonListItem.Radius);
            var minPrices = _lookupService.GetCommonListItems(CommonListItem.MinPrice);
            var maxPrices = _lookupService.GetCommonListItems(CommonListItem.MaxPrice);
            var bedrooms = _lookupService.GetCommonListItems(CommonListItem.Bedrooms);
            model.RadiusList = new SelectList(radius, "LookupValue", "LookupText");
            model.MinPriceList = new SelectList(minPrices, "LookupValue", "LookupText");
            model.MaxPriceList = new SelectList(maxPrices, "LookupValue", "LookupText");
            model.BedroomsList = new SelectList(bedrooms, "LookupValue", "LookupText");

            // Other properties
            model.FromRegistration = true;
            model.SkipLink = "window.location.reload()";
        }

        private IList<SelectListItem> GetAreaListItems(string state)
        {
            var list = new List<SelectListItem> { new SelectListItem { Text = DropDownDefaults.Area, Value = string.Empty } };

            if (string.IsNullOrEmpty(state)) return list;

            var markets = _marketService.GetMarketsByState(NhsRoute.PartnerId, state);
            list.AddRange(markets.Select(m => new SelectListItem { Text = m.MarketName, Value = m.MarketId.ToString() }));
            return list;
        }

        private IList<SelectListItem> GetCityListItems(string marketVal)
        {
            var marketId = string.IsNullOrEmpty(marketVal) ? 0 : int.Parse(marketVal);
            var list = new List<SelectListItem> { new SelectListItem { Text = DropDownDefaults.City, Value = string.Empty } };

            if (marketId == 0) return list;

            var market = _marketService.GetMarket(NhsRoute.PartnerId, marketId, false);
            list.AddRange(market.Cities.Select(c => new SelectListItem { Text = c.CityName, Value = c.CityName }));
            return list;
        }

        private IList<SelectListItem> GetSchoolListItems(string marketVal)
        {
            var marketId = string.IsNullOrEmpty(marketVal) ? 0 : int.Parse(marketVal);
            var list = new List<SelectListItem> { new SelectListItem { Text = DropDownDefaults.AllSchoolDistricts, Value = string.Empty } };

            if (marketId == 0) return list;

            var market = _marketService.GetMarket(NhsRoute.PartnerId, marketId, true);
            list.AddRange(market.SchoolDistricts.Select(s => new SelectListItem { Text = s.DistrictName, Value = s.DistrictId.ToString() }));
            return list;
        }

        private IList<SelectListItem> GetBuilderListItems(string marketVal)
        {
            var marketId = string.IsNullOrEmpty(marketVal) ? 0 : int.Parse(marketVal);
            var list = new List<SelectListItem> { new SelectListItem { Text = DropDownDefaults.AllBuilders, Value = string.Empty } };

            if (marketId == 0) return list;

            var market = _marketService.GetMarket(NhsRoute.PartnerId, marketId, true);
            list.AddRange(market.Brands.Select(b => new SelectListItem { Text = b.BrandName, Value = b.BrandId.ToString() }));
            return list;
        }
        #endregion

        public virtual ActionResult SubmitRegisterLogin(RegisterViewModel registerModel)
        {
            var model = registerModel.Login;
            ValidateLogin(model);

            if (ModelState.IsValid)
            {
                // Checks login
                try
                {
                    _userService.SignIn(model.Username, model.Password, NhsRoute.PartnerId, LanguageHelper.WrongPassword, LanguageHelper.EmailDoNotExist);

                    return
                        Json(new
                        {
                            Script = " if(typeof _gaq != 'undefined') _gaq.push(['_setCustomVar', 1,'User', '{web:" +
                                UserSession.UserProfile.UserID.Replace("{", string.Empty).Replace("}", string.Empty) +
                                "}',2]); " + " setTimeout(function() { location.reload(); }, 100); "
                        });

                }
                catch (InvalidPasswordException e)
                {
                    ModelState.AddModelError("Login.Password", e.Message);
                }
                catch (UnknownUserException e)
                {
                    ModelState.AddModelError("Login.Username", e.Message);
                }
            }


            return PartialView(NhsMvc.Default.Views.Account.Register, registerModel);
        }

        private void ValidateRegister(RegisterViewModel model)
        {
            if (string.IsNullOrEmpty(model.Email))
            {
                ModelState.AddModelError("Email", LanguageHelper.MSG_ACCOUNT_NO_EMAIL);
            }
            else if (!StringHelper.IsValidEmail(model.Email))
            {
                ModelState.AddModelError("Email", LanguageHelper.MSG_ACCOUNT_INVALID_EMAIL);
            }

            
            if (string.IsNullOrEmpty(model.Password))
            {
                ModelState.AddModelError("Password", LanguageHelper.MSG_ACCOUNT_NO_PASSWORD);
            }
            else
            {
                if (model.Password.Length < 5 || model.Password.Length > 20)
                    ModelState.AddModelError("Password", LanguageHelper.MSG_ACCOUNT_INVALID_PASSWORD);
            }
           
            if (!string.IsNullOrEmpty(model.Email) && _userService.LogonNameExists(model.Email, NhsRoute.PartnerId))
                ModelState.AddModelError("Email", LanguageHelper.MSG_ACCOUNT_SAME_EMAIL);

            if (!model.LiveOutsideReg)
            {
                if (string.IsNullOrEmpty(model.ZipCodeReg))
                    ModelState.AddModelError("ZipCodeReg", LanguageHelper.MSG_ZipCodeRequired);
                else if (!StringHelper.IsValidZip(model.ZipCodeReg.Trim()))
                    ModelState.AddModelError("ZipCodeReg", LanguageHelper.MSG_NotValidZipCode);
                else if (StringHelper.IsValidZip(model.ZipCodeReg.Trim()) && string.IsNullOrEmpty(model.MarketName) && PartnersConst.Pro.ToType<int>() == NhsRoute.BrandPartnerId)
                    ModelState.AddModelError("ZipCodeReg", LanguageHelper.MSG_ACCOUNT_INVALID_ZIP);
            }
        }

        private void ValidateLogin(SmallLoginViewModel model)
        {
            ModelState.Clear();
            // Validation
            if (string.IsNullOrEmpty(model.Username))
            {
                ModelState.AddModelError("Login.Username", @"Email required");
            }
            else
            {
                if (!StringHelper.IsValidEmail(model.Username))
                    ModelState.AddModelError("Login.Username", @"Invalid email format");
            }

            if (string.IsNullOrEmpty(model.Password))
                ModelState.AddModelError("Login.Password", @"Password required");
        }

        private void ValidateLogin(LoginViewModel model)
        {
            // Validation
            if (string.IsNullOrEmpty(model.Email))
            {
                ModelState.AddModelError("Email", @"Email required");
            }
            else
            {
                if (!StringHelper.IsValidEmail(model.Email))
                    ModelState.AddModelError("Email", @"Invalid email format");
            }

            if (string.IsNullOrEmpty(model.Password))
                ModelState.AddModelError("Password", @"Password required");
        }

        public string GetRegisterRedirectUrl(RegisterViewModel model)
        {
            // Get persisted params
            var paramList = UserSession.GetPersistedtUrlParams();
            paramList.Add(new RouteParam(RouteParams.Registration, "true", model.OtherContent ? RouteParamType.Friendly : RouteParamType.QueryString));// account created
            if (model.OtherContent)
            {
                paramList = new List<RouteParam>
                {
                    new RouteParam(RouteParams.NextPage, model.FromPage, RouteParamType.QueryString)
                };
            }
            // Uses Matchmaker, then go to Create Alert
            var partner = _partnerService.GetPartner(NhsRoute.PartnerId);
            if (partner.UsesMatchMaker == "Y")
                return paramList.ToUrl(model.OtherContent ? Pages.CreateAlert : Pages.CreateAlertModal);

            if (model.OtherContent)
                return model.FromPage;

            // No alert created
            paramList.Add(new RouteParam(RouteParams.AlertCreated, "false", RouteParamType.QueryString));

            // Coming from leads go to the leads thank you
            var regFromLeads = paramList.Value(RouteParams.RegFromLeads).ToType<bool>();
            if (regFromLeads)
                return paramList.ToUrl(Pages.LeadsRequestBrochureThanksModal);

            // Coming from register go directly to the register thank you
            return paramList.ToUrl(Pages.RegisterThankYouModal);
        }

        #endregion
    }
}
