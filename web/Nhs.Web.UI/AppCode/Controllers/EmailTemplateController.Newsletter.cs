﻿using System;
using System.Compat.Web;
using System.Data;
using System.Web.Mvc;
using System.Linq;
using System.Collections.Generic;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Helpers.MarketMaps;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects;
using Nhs.Search.Objects.Constants;
using Nhs.Utility.Common;
using Nhs.Utility.Web;
using Nhs.Web.UI.AppCode.Extensions;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;
using TweetSharp;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class EmailTemplateController
    {
        #region Actions

        public virtual ActionResult ShowNewsletterEmail(int marketId, bool? registered)
        {
            var model = new ProNewsletterViewModel(marketId, registered ?? true);
            NhsPropertySearchParams searchParams = new NhsPropertySearchParams();

            model.IsRegisteredUser = registered ?? true;
            model.FacebookUrl = _partnerService.GetPartnerMarketFacebook(NhsRoute.PartnerId, NhsRoute.BrandPartnerId, marketId);
            model.ShowEventsSection = NhsRoute.IsPartnerNhsPro;
 
            if (string.IsNullOrEmpty(model.FacebookUrl))
                model.FacebookUrl = Configuration.FacebookAccountUrl;

            model.Tweets = WebCacheHelper.GetObjectFromCache("TwitterNews", false).ToType<List<TwitterStatus>>();

            if (model.Tweets == null)
            {
                model.Tweets = TwitterHelper.GetTwitterUpdates(5);
                WebCacheHelper.AddObjectToCache(model.Tweets, "TwitterNews", new TimeSpan(WebCacheConst.DefaultHours, 0, 0), false);
            }

            var siteTems = _partnerService.GetPartnerSiteTerms(NhsRoute.PartnerId);

            // if no color schema for the private label, get the nhs pro colors
            if (!siteTems.ContainsKey("EmailTemplateLinkColor"))
                siteTems = _partnerService.GetPartnerSiteTerms(NhsRoute.BrandPartnerId);

            if (siteTems != null && siteTems.Count > 0)
            {
                model.EmailTemplateLinkColor = siteTems["EmailTemplateLinkColor"];
                model.EmailTemplateTopHeaderColor = siteTems["EmailTemplateTopHeaderColor"];
                model.EmailTemplateTopHeader2Color = siteTems["EmailTemplateTopHeader2Color"];
                model.EmailTemplateButtonBgColor = siteTems["EmailTemplateButtonBgColor"];
                model.EmailTemplateButtonTextColor = siteTems["EmailTemplateButtonTextColor"];
                model.EmailTemplateSubHeaderColor = siteTems["EmailTemplateSubHeaderColor"];
                model.EmailTemplateSubHeaderTextColor = siteTems["EmailTemplateSubHeaderTxtColor"];
            }
            else
            {
                // Default values for Nhs Pro
                model.EmailTemplateLinkColor = "#1a3554";
                model.EmailTemplateTopHeaderColor = "#004a7c";
                model.EmailTemplateTopHeader2Color = "#004a7c";
                model.EmailTemplateButtonBgColor = "#f38e29";
                model.EmailTemplateButtonTextColor = "#ffffff";
                model.EmailTemplateSubHeaderColor = "#e6ebef";
                model.EmailTemplateSubHeaderTextColor = "#444444";

            }

            model.Market = _marketService.GetMarket(marketId);

            searchParams.MarketId = model.Market.MarketId;
            searchParams.PartnerId = NhsRoute.PartnerId;

            var commsTable = _builderService.GetMarketBcs(NhsRoute.PartnerId, marketId).AsEnumerable();
            Dictionary<int, bool> commsStatus = commsTable.ToDictionary(r => r["community_id"].ToType<int>(), r => r["billed"].ToType<string>() == "Y");

            SetupCommunitiesForEmailLetter(searchParams, model, commsStatus);

            model.QuickMoveinHomes = GetQuickMoveInForEmailLetter(searchParams);

            model.Events = GetEventsForEmailLetter(searchParams, commsStatus);

            model.Promotions = GetPromosForEmailLetter(searchParams, commsStatus);

            if (model.Globals.PartnerLayoutConfig.IsBrandPartnerNhsPro)
                model.MarketMapPdfFilePath = GetMarketMapLink(_pathMapper, searchParams.MarketId);
            
            ViewBag.Title = "Weekly email template";
            return View(model);
        }

        /// <summary>
        /// The route parameter names had to be changed because of the 260 characters limit of the web browser
        /// </summary>
        /// <param name="u">UserName</param>
        /// <param name="gd">Generation Date</param>
        /// <param name="gdy">Generation Date Year</param>
        /// <param name="fn">File Name</param>
        /// <param name="t">type (registered or unregistered for pro; consumer for move and nhs)</param>
        /// <returns></returns>
        public virtual ActionResult ShowCustomNewsletter(string u, string gd, string gdy, string fn, string t)
        {
            string newsletterUrl = string.Format("{0}{1}/{2}{3}", Configuration.NewsletterVirtualDirectory, t, fn, ".htm");
            string htmlBody = HTTP.HttpGet(newsletterUrl);
            htmlBody = htmlBody.Replace("$lookup(Name)$", u);
            htmlBody = htmlBody.Replace("$lookup(GenerationDate)$", gd);
            htmlBody = htmlBody.Replace("$lookup(GenerationDateYear)$", gdy);
            return Content(htmlBody);
        }

        /// <summary>
        /// New Communities 
        /// 	Select Top 10 Grand Opening or Coming Soon communities for market, 
        ///     Sort by 
        ///         Date created
        ///  	    Perform additional sort placing all Billed above Unbilled
        /// 
        /// Featured Comms
        ///	    Select top 5 featured billed communities from market
        ///	    Sort by
        ///	        Sequence number within each brand, random sort 
        ///         Random sort 
        /// </summary>        
        private void SetupCommunitiesForEmailLetter(NhsPropertySearchParams searchParams, ProNewsletterViewModel model,
                                                    Dictionary<int, bool> commsStatus)
        {
            List<NhsCommunityResult> communities = null;
            CommunityResultsViewExt searchResults = null;

            //	New Communities 
            //	    Select Top 10 Grand Opening or Coming Soon communities for market, order by date created
            //  	Perform additional sort placing all Billed above Unbilled

            searchParams.CommunityStatus = CommunityStatusType.ComingSoonGrandOpening;
            searchParams.SortByDate = true;

            // Communities for the market
            searchResults = _communityService.GetCommunityResults(searchParams, true);
                
            SetBilledStatus(searchResults.NhsCommunityResults, commsStatus);
            communities =
                searchResults.NhsCommunityResults.Take(Configuration.EmailTemplateCommunitiesCount)
                             .Select((item, index) => new {Item = item, Index = index})
                             .OrderBy(c => c.Item.IsBilled ? 0 : 1).ThenBy(e => e.Item.IsBasicCommunity)
                             .ThenBy(c => c.Index)
                             .Select(c => c.Item)
                             .ToList();

            model.Communities = ConvertCommunityResult(communities);

            // 5.	Featured
            //  Select top 5 featured billed communities from market
            // 	Sort by
            //      1.	Sequence number within each brand, ordered randomly 
            //      2.	Random sort
            searchParams.CommunityStatus = CommunityStatusType.All; // clear comm status 
            searchResults = _communityService.GetCommunityResults(searchParams, true);
            communities = searchResults.FeaturedListings.Where(comm => !model.Communities.Select(c => c.CommunityId).ToList().Contains(comm.CommunityId)).ToList();
            model.FeaturedCommunities = ConvertCommunityResult(communities.Take(Configuration.EmailTemplateFeaturedCommunitiesCount).ToList());
        }

        /// <summary>
        /// Quick Move in
        ///	    Select top 3 active Quick Move In’s within the market
        ///	    Sort by
        ///	        Sequence number within each brand, ordered date created asc
        ///	        Date created asc 
        ///	        Perform additional sort placing all Billed above Unbilled
        /// </summary>        
        private List<ExtendedHomeResult> GetQuickMoveInForEmailLetter(NhsPropertySearchParams searchParams)
        {
            searchParams.HomeStatus = (int)HomeStatusType.QuickMoveIn;
            searchParams.EndRecord = 0;

            // Quick Move in Communities 
            var homesResult = _listingService.GetHomeResultsExt(searchParams, true);
            var homes = homesResult.HomeResults.OrderBy(c => c.CommunityId).ToList() ;

            var round = 1; // take one comm for each brand per rounds till complete the count to distribute results till brands
            List<HomeResult> tempHomes = new List<HomeResult>();

            while (tempHomes.Count < Configuration.EmailTemplateQuickMoveInCount && homes.Count > 0 && round <= homes.Count)
            {
                foreach (var home in homes)
                {
                    if (tempHomes.Count == Configuration.EmailTemplateQuickMoveInCount) break;

                    if (tempHomes.Count(c => c.BrandName == home.BrandName) < round && !tempHomes.Exists(c => c.SpecId == home.SpecId))
                        tempHomes.Add(home);
                }
                round++;
            }

            return _listingService.GetExtendedHomeResults(NhsRoute.PartnerId, tempHomes, true).OrderBy(l => l.IsBilled ? 0 : 1).ThenByDescending(h => h.CommunityId).ToList();
        }

        private static string GetMarketMapLink(IPathMapper pathMapper, int marketId)
        {
            var marketsList = MarketMapsConfigReader.GetMarketsMaps(pathMapper);

            if (marketsList == null || marketsList.Count <= 0) return string.Empty;
            var filteredListByMarket = marketsList.Where(m => m.MarketId == marketId).ToList();
            if (filteredListByMarket.Count <= 0) return string.Empty;
            var currentMarket = filteredListByMarket.First();
            return currentMarket.FileName;
        }


        /// <summary>
        /// Events 
        ///	Select top 5 active current/future events within the market
        ///	Sort by
        ///	    Billed then unbilled
        ///     Sequence number within each brand, ordered by billed then unbilled, the event start date asc.
        /// 	Event start date asc
        /// </summary>        
        private List<CommunityEventInfo> GetEventsForEmailLetter(NhsPropertySearchParams searchParams, Dictionary<int, bool> commsStatus)
        {
            // Communities with events for the market
            searchParams.HomeStatus = 0;
            searchParams.HasEvent = true;
            var eventCommunities = _communityService.GetCommunityResults(searchParams, true);

            SetBilledStatus(eventCommunities.NhsCommunityResults, commsStatus);
            var results = _communityService.GetCommunityEvents(eventCommunities.NhsCommunityResults.Where(c => c.IsBasicCommunity == false).Select(c => c.CommunityId).ToList()).ToList();  

            var events = results.Select(e => new CommunityEventInfo
            {
                EventId = e.EventID,
                Title = e.Title,
                Description = e.Description,
                EventFlyerUrl = e.EventFlyerURL,
                EventType = e.EventTypeCode,
                EventEndDate = e.EventEndTime,
                EventStartDate = e.EventStartTime,
                CommunityId = (int)e.CommunityID,
                BuilderId = e.BuilderID,
                CommunityName = eventCommunities.NhsCommunityResults.Where(c => c.CommunityId == e.CommunityID).FirstOrDefault().CommunityName,
                Address = eventCommunities.NhsCommunityResults.Where(c => c.CommunityId == e.CommunityID).Select(c => c.City + "," + c.State + " " + c.PostalCode).FirstOrDefault(),
                BuilderName = eventCommunities.NhsCommunityResults.Where(c => c.CommunityId == e.CommunityID).FirstOrDefault().BrandName,
                IsBilled = eventCommunities.NhsCommunityResults.Where(c => c.CommunityId == e.CommunityID).FirstOrDefault().IsBilled,
                BrandId = eventCommunities.NhsCommunityResults.Where(c => c.CommunityId == e.CommunityID).FirstOrDefault().BrandId
            }).OrderBy(e => e.IsBilled? 0 : 1).ThenBy(e => e.EventStartDate).DistinctBy(e=>e.EventId).ToList();

            var round = 1; // take one comm for each brand per rounds till complete the count to distribute results till brands
            List<CommunityEventInfo> eventsTemp = new List<CommunityEventInfo>();
            var eventCount = 5;

            while (eventsTemp.Count < eventCount && events.Count > 0 && round <= events.Count)
            {
                foreach (var e in events)
                {
                    if (eventsTemp.Count == eventCount) break;

                    if (eventsTemp.Count(c => c.BrandId == e.BrandId) < round && !eventsTemp.Exists(c => c.EventId == e.EventId))
                        eventsTemp.Add(e);
                }
                round++;
            }

            return eventsTemp.OrderBy(e => e.EventStartDate).ToList();
        }

        
        /// <summary>
        ///	Promotions
        ///	    Select top 10 active current/future promotions within the market
        ///	    Sort by
        ///	        Billed then unbilled
        ///	        Sequence number within each brand, ordered by billed then unbilled, the promo start date asc.
        ///	        Promo start date asc

        /// </summary>        
        private List<CommunityPromotionInfo> GetPromosForEmailLetter(NhsPropertySearchParams searchParams, Dictionary<int, bool> commsStatus)
        {
            // Communities with promos for the market
            searchParams.HasEvent = false; // clear has event filter
            //69062 Hot fix.
            //If brand is NHS Pro then the promotions to display will be both agent and consumer, if any other brand they will be only consumer.
            searchParams.PromotionType = (NhsRoute.IsBrandPartnerNhsPro)
                                             ? PromotionType.Both
                                             : PromotionType.Consumer;
            var promoCommunities = _communityService.GetCommunityResults(searchParams, true);

            SetBilledStatus(promoCommunities.NhsCommunityResults, commsStatus);
            var results = _communityService.GetCommunityPromotions(promoCommunities.NhsCommunityResults.Where(c => c.IsBasicCommunity == false).Select(c => c.CommunityId).ToList()).ToList();
            
            var promos = results.Select(p => new CommunityPromotionInfo
            {
                PromoId = p.PromoID,
                PromoTextShort = p.PromoTextShort,
                PromoTextLong = p.PromoTextLong,
                PromoFlyerUrl = p.PromoFlyerURL,
                PromoType = p.PromoTypeCode,
                PromoEndDate = p.PromoEndDate,
                PromoStartDate = p.PromoStartDate,
                PromoUrl = p.PromoURL,
                CommunityId = p.CommunityID,
                BuilderId = p.BuilderID,
                CommunityName = promoCommunities.NhsCommunityResults.Where(c => c.CommunityId == p.CommunityID).FirstOrDefault().CommunityName,
                BuilderName = promoCommunities.NhsCommunityResults.Where(c => c.CommunityId == p.CommunityID).FirstOrDefault().BrandName,
                State = promoCommunities.NhsCommunityResults.Where(c => c.CommunityId == p.CommunityID).FirstOrDefault().State,
                MarketName = _marketService.GetMarket(NhsRoute.PartnerId, promoCommunities.NhsCommunityResults.Where(c => c.CommunityId == p.CommunityID).FirstOrDefault().MarketId, false).MarketName,
                City = promoCommunities.NhsCommunityResults.Where(c => c.CommunityId == p.CommunityID).FirstOrDefault().City,
                BrandId = promoCommunities.NhsCommunityResults.Where(c => c.CommunityId == p.CommunityID).FirstOrDefault().BrandId,
                IsBilled = promoCommunities.NhsCommunityResults.Where(c => c.CommunityId == p.CommunityID).FirstOrDefault().IsBilled
            }).OrderBy(e => e.IsBilled ? 0 : 1).ThenBy(e => e.PromoStartDate).DistinctBy(e => e.PromoId).ToList();


            var round = 1; // take one comm for each brand per rounds till complete the count to distribute results till brands
            List<CommunityPromotionInfo> promosTemp = new List<CommunityPromotionInfo>();
            var promoCount = Configuration.EmailTemplatePromotionsCount;

            while (promosTemp.Count < promoCount && promos.Count > 0 && round <= promos.Count)
            {
                foreach (var e in promos.TakeWhile(e => promosTemp.Count != promoCount))
                {
                    if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
                    {
                        if (promosTemp.Count(c => c.BrandId == e.BrandId) < round &&
                            !promosTemp.Exists(c => c.PromoId == e.PromoId) && e.PromoType == "COM")
                            promosTemp.Add(e);
                    }
                    else
                    {
                        //Ticket 70156:  To hide agent promos if the community is unbilled or basic (basics are already excluded at this step)
                        promosTemp.RemoveAll(f => f.IsBilled == false && f.PromoType == "AGT");
                        if (promosTemp.Count(c => c.BrandId == e.BrandId) < round &&
                            !promosTemp.Exists(c => c.PromoId == e.PromoId))
                            promosTemp.Add(e);
                    }
                }
                round++;
            }

            return promosTemp.OrderBy(e => e.PromoStartDate).ToList();
        }

        /// <summary>
        /// Set the billed status from the results returned from the WS
        /// </summary>
        /// <param name="results"></param>
        /// <param name="commsStatus"></param>
        private void SetBilledStatus(List<NhsCommunityResult> results, Dictionary<int, bool> commsStatus)
        {
            if (commsStatus != null && commsStatus.Count > 0)
            {
                foreach (var comm in results)
                {
                    if (commsStatus.ContainsKey(comm.CommunityId))
                    {
                        comm.IsBilled = commsStatus[comm.CommunityId];
                    }
                }
            }
        }


        private List<ExtendedCommunityResult> ConvertCommunityResult(List<NhsCommunityResult> communities)
        {

            var comms = communities.Select(c => new ExtendedCommunityResult()
            {
                SearchKey = c.SearchKey,
                IsBasicListing = c.IsBasicListing,
                AlertDeletedFlag = c.AlertDeletedFlag,
                BrandId = c.BrandId,
                BrandName = c.BrandName,
                BuilderId = c.BuilderId,
                BuilderUrl = c.BuilderUrl,
                BuildOnYourLot = c.AlertDeletedFlag,
                City = c.City,
                CommunityId = c.CommunityId,
                CommunityImageThumbnail = c.CommunityImageThumbnail,
                CommunityName = c.CommunityName,
                CommunityType = c.CommunityType,
                County = c.County,
                FeaturedListingId = c.FeaturedListingId,
                Green = c.Green,
                HasHotHome = c.HasHotHome,
                Latitude = c.Latitude,
                Longitude = c.Longitude,
                MarketId = c.MarketId,
                MatchingHomes = c.MatchingHomes,
                PostalCode = c.PostalCode,
                PriceHigh = c.PriceHigh,
                PriceLow = c.PriceLow,
                PromoId = c.PromoId,
                State = c.State,
                SubVideoFlag = c.SubVideoFlag,
                Video_url = c.Video_url,
                HasVideo = c.SubVideoFlag.ToUpper() == "Y",
                HasEvents = c.HasEvents,
                QuickMoveinHomesCount = c.QuickMoveinHomesCount
            }).ToList();

            comms.ForEach(c => { var mkr = _marketService.GetMarket(NhsRoute.PartnerId, c.MarketId, false); if (mkr != null) c.MarketName = mkr.MarketName; });

            return comms;
        }

        #endregion
    }

}
