﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Proxy;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects;
using Nhs.Search.Objects.Constants;
using Nhs.Utility.Common;
using Nhs.Utility.Data.Paging;
using Nhs.Web.UI.AppCode.Extensions;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.Lib;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Web.UI.AppCode.ViewModels.NewMediaViewer;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class CommunityDetailController
    {
        public virtual ActionResult MediaViewer(int communityId, string nextPage)
        {
            var community = _communityService.GetCommunity(communityId);
            var model = new NewMediaViewerViewModel
            {
                PropertyName = community.CommunityName,
                PriceDisplay = FormattedPrice(community.PriceHigh, community.PriceLow, community.BCType),
                CommunityId = communityId,
                BuilderId = community.BuilderId,
                MarketId = community.Market.MarketId,
                City = community.SalesOffice.City,
                State = community.SalesOffice.State,
                ZipCode = community.SalesOffice.ZipCode,
                MediaPlayerList = GetMediaPlayerObjectsList(communityId),
                IsAjaxRequest = Request.IsAjaxRequest(),
                IsCommunityDetail = true,
                IsLoggedIn = UserSession.UserProfile.IsLoggedIn(),
                BcType = community.BCType,
                NextPage = nextPage
            };

            model.FirstImage = model.MediaPlayerList.Any()
                ? (model.MediaPlayerList.First().Url.StartsWith("http") ? "" : Configuration.IRSDomain) +
                  model.MediaPlayerList.First().Url
                : string.Empty;
            model.PageUrl = new List<RouteParam>
            {
                new RouteParam(RouteParams.Builder, community.BuilderId),
                new RouteParam(RouteParams.Community, community.CommunityId)
            }.ToUrl(Pages.CommunityDetail, true, true);

            model.PinterestDescription = string.Format("{0} by {1} in {2}, {3}", community.CommunityName, community.Brand.BrandName, community.City, community.State.StateName).Replace("'", "\\'");

            if (UserSession.UserProfile.IsLoggedIn())
            {
                var pl = new PlannerListing(communityId, community.BuilderId, ListingType.Community);
                if (UserSession.UserProfile.Planner.SavedCommunities.Contains(pl))
                    model.SavedToPlanner = true;
            }


            if (Request.IsAjaxRequest())
                return PartialView(NhsMvc.Default.Views.Common.PropertyDetail.NewMediaViewer, model);
            return View(NhsMvc.Default.Views.Common.PropertyDetail.NewMediaViewer, model);
        }

        public virtual ActionResult GetNearbyCommunities(int communityId, int builderId, string brandName)
        {
            var model = new NearbyCommunityModel();
            model.BrandName = brandName;
            model.NearByComms = _communityService.GetNearbyCommunities(NhsRoute.PartnerId, communityId, builderId, null).Take(5);
            model.NearByCommsObject =
                model.NearByComms.GroupBy(n => new { n.Latitude, n.Longitude })
                    .Select(
                        g =>
                            new
                            {
                                g.Key.Latitude,
                                g.Key.Longitude,
                                Name = g.Count(),
                                MarketPoints = g.Select(p => new { p.CommunityId })
                            })
                    .ToJson();
            return PartialView(NhsMvc.Default.Views.Common.PropertyDetail.NearByCommunities, model);
        }

        public virtual ActionResult RedirectToDefault(int communityId)
        {
            var community = _communityService.GetCommunity(communityId);

            if (community != null)
            {
                if (!(NhsRoute.CurrentRoute.Params.Any(p => p.Name == RouteParams.Builder)))
                    NhsRoute.CurrentRoute.Params.Add(new RouteParam(RouteParams.Builder, community.BuilderId.ToString(), RouteParamType.Friendly));

                return RedirectPermanent("~/" + NhsRoute.CurrentRoute.Params.ToUrl(Pages.CommunityDetail));
            }

            return RedirectTo404();
        }

        public virtual ActionResult ShowV1(int communityId, int builderId)
        {
            return Show(communityId, builderId);
        }

        public virtual ActionResult ShowPreview(int communityId, int builderId)
        {
            var searchParams = UserSession.PropertySearchParameters.Clone() as NhsPropertySearchParams;
            int page = 0;
            if (!string.IsNullOrEmpty(RouteParams.Page.Value())) page = RouteParams.Page.Value<int>() - 1;

            var cdvm = GetCommunityDetailPreviewModel(searchParams, page, communityId);

            var metaRegistrar = new MetaRegistrar(_pathMapper);
            var metas = metaRegistrar.GetRobotsMeta("noodp, noydir, noindex");
            base.AddNoIndex(metaRegistrar, metas.ToList());

            OverrideDefaultMeta(cdvm, metas);

            return View("Show", cdvm);
        }

        public virtual ActionResult Show(int? communityId, int? builderId)
        {
            
            if (communityId == null)
                communityId = RouteParams.Community.Value<int>() == 0 ? RouteParams.CommunityId.Value<int>() : RouteParams.Community.Value<int>();

            // Gets models
            var page = 1;
            if (!string.IsNullOrEmpty(RouteParams.Page.Value()) && RouteParams.Page.Value<int>() > 0)
                page = RouteParams.Page.Value<int>() - 1;

            Community community = null;
            var searchParams = UserSession.PropertySearchParameters.Clone() as NhsPropertySearchParams;
            var addCanonical = AddCanonical(communityId.ToType<int>());
            if (_communityService.IsCommunityActiveOnParentBrandPartner(communityId.ToType<int>(), NhsRoute.PartnerId))
                community = _communityService.GetCommunity(communityId.ToType<int>());

            // Add an extra parameter to identify from SEO inactive data
            if (community == null && string.IsNullOrEmpty(RouteParams.Msg.Value<string>()))
                return RedirectPermanent(GetInactiveUrl());

            var refer301Url = base.GetReferRedirectUrl(community);

            if (!string.IsNullOrEmpty(refer301Url))
                return RedirectPermanent(refer301Url);

            if (UserSession.PropertySearchParameters.MarketId != 0 & searchParams != null)
            {
                searchParams.CommunityId = communityId.ToType<int>();
                searchParams.SortOrder = SortOrder.Status;
                searchParams.BuilderId = builderId.GetValueOrDefault();
            }
            else
                searchParams = GetSearchParams(communityId.ToType<int>(), SortOrder.Status, 0, "N");

            var pageDetail = RedirectionHelper.GetDetailPageRedirectName(true);
            if (!string.IsNullOrEmpty(pageDetail) && community != null)
            {
                return RedirectPermanent(pageDetail, community.ToCommunityDetail());
            }

            if (community != null && !string.IsNullOrEmpty(community.PartnerMask))
            {
                var partnerMaskIndex = _partnerService.GetPartnerMaskIndex(NhsRoute.PartnerId);
                var partnerSortValue = community.PartnerMask.Substring(partnerMaskIndex - 1, 1);

                if ("F" == partnerSortValue && NhsRoute.IsBrandPartnerNhsPro)
                {
                    var @params = new List<RouteParam> { new RouteParam(RouteParams.Community, community.CommunityId) };
                    var url = @params.ToUrl(Pages.BasicCommunity) + "/" + string.Format("{0}-{1}-{2}-{3}", community.CommunityName, community.City,
                                            community.State.StateAbbr, community.PostalCode).Trim().Replace(" ", "-").Replace("--", "-").Replace("'", "");
                    return RedirectPermanent(url);
                }

                if ((RouteParams.ForcePopupPlayer.Value<bool>() || RouteParams.AddToProfile.Value<bool>()) && UserSession.UserProfile.IsLoggedIn())
                    SaveCommunityToPlanner(community.CommunityId, community.BuilderId, true);
            }

            var viewModel = (community == null) ? CommunityDetailExtensions.GetInactivePropertyViewModel(true, _stateService, _lookupService) as CommunityDetailViewModel
                                               : (NhsRoute.ShowMobileSite ? GetCommunityDetailModelMobile(community, !addCanonical)
                                                                         : GetCommunityDetailModel(community, searchParams, page, !addCanonical));

            viewModel.IsitInactiveData = community == null;

            if (community == null || (addCanonical && string.IsNullOrEmpty(viewModel.Globals.CanonicalLink))) // if apply the rules to addCanonical and it wasnt 
                AddCanonicalWithIncludeParams(viewModel, new List<string> { "community", "builder" }, (community != null && community.OwnerPartnerId == NhsRoute.PartnerId && NhsRoute.BrandPartnerId != NhsRoute.PartnerId));


            if (!string.IsNullOrWhiteSpace(viewModel.BcType) && viewModel.BcType.ToLower() != BuilderCommunityType.ComingSoon.ToLower())
            {
                CookieManager.LastCommunityView = communityId.Value.ToString(CultureInfo.InvariantCulture);
                CookieManager.LastMarketView = viewModel.MarketId.ToString(CultureInfo.InvariantCulture);
                CookieManager.PreviousSessionId = UserSession.SessionId;
            }

            UserSession.PreviousPage = Pages.CommunityDetail;
            UserSession.PersonalCookie.MarketId = viewModel.MarketId;
            UserSession.PersonalCookie.State = viewModel.State;

            // ReSharper disable Asp.NotResolved
            return View(viewModel);
            // ReSharper restore Asp.NotResolved
        }

        [HttpPost]
        public virtual ActionResult Search(NhsPropertySearchParams searchParams, int page, CommunityDetailTabs selectedTab, int allNewHomesCount, int quickMoveInCount, int filteredHomesCount, bool showAll, bool hideAll)
        {
            CommunityDetailViewModel model;

            if (NhsRoute.CurrentRoute.Function == Pages.CommunityDetailPreview)
                model = GetCommunityDetailPreviewModel(searchParams, page - 1, searchParams.CommunityId);
            else
            {
                model = new BasicCommunityDetailViewModel();
                WebApiCommonResultModel<List<HomeItem>> resultsView;
                if (CommunityDetailTabs.AllCommunities == selectedTab)
                {
                    resultsView = GetCommunityHomeResultsFromApi(searchParams, searchParams.HomeStatus == 5);

                    if (resultsView.ResultCounts.HomeCount <= 0)
                        resultsView = GetCommunityHomeResultsFromApi(searchParams, true);
                }
                else
                {
                    searchParams.QuickMoveIn = searchParams.HomeStatus;
                    searchParams.PageSize = int.MaxValue;
                    resultsView = _listingService.GetHomesResultsFromApi<HomeItem>(searchParams, NhsRoute.PartnerId);

                    resultsView.Result = resultsView.Result.Select(home =>
                    {
                        var market = _marketService.GetMarket(home.MarketId);
                        home.MarketName = market.MarketName;
                        home.StateName = market.State.StateName;
                        return home;
                    }).ToList();
                }

                var sortActions = new SortHomeActions();
                model.HomeResultsApi = sortActions.SortOptions[searchParams.SortOrder](resultsView.Result);
                if (!showAll && !hideAll)
                {
                    int pageSize = (resultsView.ResultCounts.HomeCount <= 40) ? resultsView.ResultCounts.HomeCount : 12;

                    var pagedList = pageSize > 0
                        ? model.HomeResultsApi.ToPagedList(page, pageSize)
                        : null;
                    model.HomeResultsApi = pagedList;
                }
                else if (hideAll)
                {
                    model.HomeResultsApi = model.HomeResultsApi.Take(12);
                }
                model.MarketName = searchParams.MarketName;
                model.State = searchParams.StateName;
                model.BcType = searchParams.CommunityStatus;
            }

            model.SelectedTab = selectedTab;
            model.AllNewHomesCount = allNewHomesCount;
            model.QuickMoveInCount = quickMoveInCount;
            model.FilteredHomesCount = filteredHomesCount;
            model.CurrentSortOrder = searchParams.SortOrder;

            var view = NhsRoute.CurrentRoute.Function == Pages.CommunityDetailv2
                ? NhsMvc.Default.Views.CommunityDetailv2.ResultData : NhsMvc.Default.Views.CommunityDetail.ResultData;
            return PartialView(view, model);
        }

        /// <summary>
        /// Action used for facets, paging and sort
        /// </summary>
        /// <param name="searchParameters">Serialized Json representation of the search parameter object</param>
        /// <param name="page">Page number</param>
        /// <returns></returns>
        [HttpPost]
        public virtual ActionResult SearchV1(string searchParameters, int page)
        {
            var searchParams = JsonHelper.FromJson<NhsPropertySearchParams>(searchParameters);
            Community community = null;

            if (_communityService.IsCommunityActiveOnParentBrandPartner(searchParams.CommunityId, NhsRoute.PartnerId))
            {
                community = _communityService.GetCommunity(searchParams.CommunityId);
            }

            CommunityDetailViewModel model = GetCommunityDetailModel(community, searchParams, page, false);
            return PartialView(NhsMvc.Default.Views.CommunityDetail.HomesGallery, model);
        }

        public virtual ActionResult LeadConfirmation(CommunityDetailViewModel model)
        {
            return PartialView(NhsMvc.Default.Views.Common.LeadForm.LeadConfirmation, model);
        }

        #region JSon Calls

        public virtual JsonResult AddCommunityToUserPlanner(int communityId, int builderId)
        {
            if (UserSession.UserProfile.ActorStatus != WebActors.ActiveUser) //Redirect to login
            {
                var thisPageParams = new List<RouteParam>
                {
                    new RouteParam(RouteParams.Community, communityId),
                    new RouteParam(RouteParams.Builder, builderId),
                    new RouteParam(RouteParams.AddToProfile, "true")
                };

                string nextPage = thisPageParams.ToUrl(Pages.CommunityDetail);

                var nextPageParams = new List<RouteParam> {
                    new RouteParam(RouteParams.NextPage, nextPage, RouteParamType.QueryString)};

                string url = nextPageParams.ToUrl(NhsRoute.ShowMobileSite ? Pages.LoginModal : Pages.Login);
                return this.Json(new { redirectUrl = url, data = false });
            }

            SaveCommunityToPlanner(communityId, builderId, true);
            return Json(new { message = LanguageHelper.MSG_COMMDETAIL_ADDED_TO_PLANNER, data = true });
        }

        [HttpPost]
        public virtual JsonResult LogPhoneNumber(int communityId, int builderId, int planId, int specId)
        {
            LogCommunityHeaderClickAction(communityId, builderId, NhsRoute.ShowMobileSite ? LogImpressionConst.MobileCommunityDetailCallSalesOffice : LogImpressionConst.CommunityBuilderPhone);
            return new JsonResult { Data = "Click has been logged!" };

        }

        [HttpPost]
        public virtual JsonResult LogIPhoneNumber(int communityId, int builderId, int planId, int specId)
        {
            LogCommunityHeaderClickAction(communityId, builderId, LogImpressionConst.CallCommunityDetail);
            return new JsonResult { Data = "Click has been logged!" };
        }

        [HttpPost]
        public virtual JsonResult LogExternalVideo(int communityId, int builderId)
        {
            LogCommunityHeaderClickAction(communityId, builderId, LogImpressionConst.OffsiteVideo);
            return new JsonResult { Data = "Click has been logged!" };

        }

        public virtual JsonResult LogMortgageRates(int communityId, int builderId, int planId, int specId)
        {
            LogCommunityHeaderClickAction(communityId, builderId, LogImpressionConst.GetMortgateRatesCommDetail);
            return new JsonResult { Data = "Click has been logged!" };
        }

        public virtual JsonResult LogMortgageMatch(int communityId, int builderId, int planId, int specId)
        {
            LogCommunityHeaderClickAction(communityId, builderId, LogImpressionConst.GetMortgateMatchCommDetail);
            return new JsonResult { Data = "Click has been logged!" };
        }

        #region Log Next Steps section

        public virtual JsonResult LogNextStepsSave(int communityId, int builderId, int planId, int specId)
        {
            LogCommunityNextStepsClickAction(communityId, builderId, LogImpressionConst.NextStepsComClickSave);
            return new JsonResult { Data = "Saved to your profile!" };
        }

        public virtual JsonResult LogNextStepsPrint(int communityId, int builderId, int planId, int specId)
        {
            LogCommunityNextStepsClickAction(communityId, builderId, LogImpressionConst.NextStepsComClickPrint);
            return new JsonResult { Data = "Click has been logged!" };
        }

        public virtual JsonResult LogNextStepsBrochure(int communityId, int builderId, int planId, int specId)
        {
            LogCommunityNextStepsClickAction(communityId, builderId, LogImpressionConst.NextStepsComClickBrochure);
            return new JsonResult { Data = "Click has been logged!" };
        }

        public virtual JsonResult LogNextStepsSeePhone(int communityId, int builderId, int planId, int specId)
        {
            LogCommunityNextStepsClickAction(communityId, builderId, LogImpressionConst.NextStepsComClickSeePhone);
            return new JsonResult { Data = "Click has been logged!" };
        }

        #endregion
        
        #endregion
    }
}