﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nhs.Library.Extensions;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Library.Web;
using Nhs.Library.Enums;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Route;
using State = Nhs.Mvc.Domain.Model.Web.State;
using Market = Nhs.Mvc.Domain.Model.Web.Market;

using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Web.UI.AppCode.Helper;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class BoylSearchController : ApplicationController
    {
        #region Member variables
        private readonly IBoylService _boylService;
        private readonly IStateService _stateService;
        private readonly IPathMapper _pathMapper;
        private readonly IMarketService _marketService;

        #endregion

        #region Constructor
        public BoylSearchController(IBoylService boylService, IPathMapper pathMapper, IMarketService marketService, IStateService stateService)
            : base(pathMapper)
        {
            _boylService = boylService;
            _pathMapper = pathMapper;
            _marketService = marketService;
            _stateService = stateService;
        }
        #endregion

        #region Actions
        public virtual ActionResult Show()
        {
            var viewModel = GetBoylSearchModel("");
            AddCanonicalTag(viewModel);
            base.SetupAdParameters(viewModel);
            // ReSharper disable once Mvc.ViewNotResolved
            return View(viewModel);
        }

        [HttpPost]
        public virtual ActionResult Show(BoylSearchViewModel model)
        {
            var paramz = new List<RouteParam>();

            if (string.IsNullOrEmpty(model.PostalCode))
            {
                paramz.Add(new RouteParam(RouteParams.Market, model.MarketId, RouteParamType.Friendly, true, true));
            }
            else
            {
                paramz.Add(new RouteParam(RouteParams.PostalCode, model.PostalCode, RouteParamType.Friendly, true, true));
            }

            return Redirect(paramz.ToUrl("BoylResults").ToLower());

        }

        [HttpPost]
        public virtual ActionResult ChageState(BoylSearchViewModel model)
        {
            var viewModel = GetBoylSearchModel(model.StateAbbr);
            return PartialView(NhsMvc.Default.Views.BoylSearch.MarketSelect, viewModel);
        }
        #endregion

        #region Helper Methods

        public virtual JsonResult GetAreas(string state)
        {
            var listItems = GetAreaListItems(_stateService.GetStateAbbreviation(state));
            return Json(listItems, JsonRequestBehavior.AllowGet);
        }

        private List<Market> GetAreasMarket(string state)
        {
            var list = new List<Market>();
            if (!string.IsNullOrEmpty(state))
            {
                var markets = _marketService.GetMarketsByState(NhsRoute.PartnerId, state);
                list.AddRange(markets.Where(m => m.TotalCustomBuilders > 0));
            }
            return list;
        }

        private IList<SelectListItem> GetAreaListItems(string state)
        {
            var list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = DropDownDefaults.Area, Value = string.Empty });   // Option item

            // Gets areas
            if (string.IsNullOrEmpty(state)) return list;

            var markets = _marketService.GetMarketsByState(NhsRoute.PartnerId, state);
            list.AddRange(markets.Where(m => m.TotalCustomBuilders > 0).Select(m => new SelectListItem { Text = m.MarketName, Value = m.MarketId.ToString() }));

            return list;
        }

        #endregion

        #region Private Methods
        private BoylSearchViewModel GetBoylSearchModel(string state)
        {

            int partnerId = Configuration.PartnerId; //NhsRoute.PartnerId?
            bool isValid = string.IsNullOrEmpty(RouteParams.DisplayMessage.Value());
            string st = string.Empty;
            int mk = 0;

            var stateList = _boylService.GetBoylStates(NhsRoute.PartnerId);

            stateList.Insert(0, new State { StateAbbr = string.Empty, StateName = LanguageHelper.ChooseState });

            if (!string.IsNullOrEmpty(UserSession.PersonalCookie.State) && string.IsNullOrEmpty(state))
            {
                foreach (State listItem in stateList)
                {
                    if (listItem.StateAbbr.Trim().ToLower() == UserSession.PersonalCookie.State.ToLower())
                    {
                        st = UserSession.PersonalCookie.State;
                    }
                }
            }
            else
            {
                st = string.IsNullOrEmpty(state) ? stateList[0].StateAbbr : state;
            }


            var marketList = GetAreasMarket(st);

            marketList.Insert(0, new Market { MarketId = 0, MarketName = LanguageHelper.ChooseAnArea });

            if (!string.IsNullOrEmpty(UserSession.PersonalCookie.MarketId.ToString()) && string.IsNullOrEmpty(state))
            {
                foreach (Market listItem in marketList)
                {
                    if (listItem.MarketId == UserSession.PersonalCookie.MarketId)
                    {
                        mk = UserSession.PersonalCookie.MarketId;
                    }
                }
            }

            var viewModel = new BoylSearchViewModel
            {
                StateAbbr = st,
                StateList = stateList,
                MarketId = mk,
                MarketList = marketList,
                ShowValidationMessage = !isValid,
                ShowFixedPartnerContent = true,
            };

            //Add ad params
            if (UserSession.PersonalCookie.MarketId != 0)
                viewModel.Globals.AdController.AddMarketParameter(UserSession.PersonalCookie.MarketId);

            //// Register Meta Tags information
            var paramz = new List<ContentTag>
            {
                new ContentTag
                {
                    TagKey = ContentTagKey.MarketName,
                    TagValue = "Austin"
                }
            };

            var metaReader = new MetaReader(_pathMapper);
            var metas = metaReader.GetMetaTagInformation(Pages.BOYLSearch, paramz.ToDictionary(), PageHeaderSectionName.SeoMetaPages);
            base.OverrideDefaultMeta(viewModel, metas);

            return viewModel;
        }
        #endregion
    }
}
