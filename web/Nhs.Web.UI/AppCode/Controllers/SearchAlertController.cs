﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Exceptions;
using Nhs.Library.Extensions;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.UrlRewriter.Configuration;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;
using SearchAlert = Nhs.Mvc.Domain.Model.Profiles.SearchAlert;
//using Nhs.Library.Business;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class SearchAlertController : ApplicationController
    {
        #region Constants

        private const decimal PREPOP_MIN_PRICE_ADJUST = (decimal)0.8;
        private const decimal PREPOP_MAX_PRICE_ADJUST = (decimal)1.2;
        private const string PREPOP_MIN_PRICE_DEFAULT = "200000";
        private const string PREPOP_MAX_PRICE_DEFAULT = "400000";

        #endregion

        private readonly IStateService _stateService;
        private readonly IMarketService _marketService;
        private readonly ILookupService _lookupService;
        private readonly ISearchAlertService _searchAlertService;
        private readonly IUserProfileService _userProfileService;
        private readonly ICommunityService _communityService;
        private readonly IListingService _listingService;
        private readonly IMapService _mapService;
        private readonly IPartnerService _partnerService;
        private readonly IMarketDfuService _marketDfuService;
        private readonly IBrandService _brandService;

        public SearchAlertController(IStateService stateService, IMarketService marketService, ILookupService lookupService,
            ISearchAlertService searchAlertService, IUserProfileService userProfileService, ICommunityService communityService,
            IListingService listingService, IMapService mapService, IPartnerService partnerService, IMarketDfuService marketDFUService,
            IBrandService brandService)
        {
            _stateService = stateService;
            _marketService = marketService;
            _lookupService = lookupService;
            _searchAlertService = searchAlertService;
            _userProfileService = userProfileService;
            _communityService = communityService;
            _listingService = listingService;
            _mapService = mapService;
            _partnerService = partnerService;
            _marketDfuService = marketDFUService;
            _brandService = brandService;
        }

        #region Actions

        public virtual Boolean GetLogin(string u, string p)
        {
            bool res = true;
            try
            {
                _userProfileService.SignIn(u, p, NhsRoute.PartnerId, LanguageHelper.WrongPassword, LanguageHelper.EmailDoNotExist);
            }
            catch (InvalidPasswordException)
            {
                res = false;
            }
            catch (UnknownUserException)
            {
                res = false;
            }
            return res;
        }


        #endregion

        public virtual ActionResult CreateAlert()
        {
            var model = new SearchAlertViewModel.CreateAlertViewModel();
            UserSession.PersistUrlParams(this); // Persist params

            // Return view
            BindCreateAlertData(model);

            if (UserSession.UserProfile.ActorStatus != WebActors.ActiveUser)
            {
                NhsUrl url = new NhsUrl();
                url.Function = Pages.LoginModal;
                NhsUrl nUrl = NhsUrlHelper.GetFriendlyUrl();

                string partnerSiteUrl = RewriterConfiguration.CurrentPartnerSiteUrl;
                if (string.IsNullOrEmpty(partnerSiteUrl))
                    partnerSiteUrl = " ";

                string nextPage = nUrl.ToString().Replace(partnerSiteUrl, string.Empty);
                url.AddParameter(UrlConst.NextPage, nextPage, RouteParamType.Friendly);
                url.Redirect();
            }
            return PartialView(NhsMvc.Default.Views.SearchAlert.CreateAlert, model);
        }

        public virtual ActionResult CreateAlertOnPage()
        {
            var model = new SearchAlertViewModel.CreateAlertViewModel();
            UserSession.PersistUrlParams(this); // Persist params

            // Return view
            BindCreateAlertData(model);
            model.FromPage = String.IsNullOrEmpty(RouteParams.NextPage.Value()) ? Pages.CreateAlert : RouteParams.NextPage.Value();

            if (UserSession.UserProfile.ActorStatus != WebActors.ActiveUser)
            {
                var paramList = new List<RouteParam>();
                var nextPage = model.FromPage;

                paramList.Add(new RouteParam(RouteParams.NextPage, nextPage, RouteParamType.QueryString, true, true));

                return Redirect(paramList.ToUrl(Pages.SignIn));
            }

            // Change the target id used by the ajax form to be mvc_container
            model.OtherContent = true;
            return View(NhsMvc.Default.Views.SearchAlert.CreateAlertOnPage, model);
        }

        [HttpPost]
        public virtual ActionResult CreateAlert(SearchAlertViewModel.CreateAlertViewModel model)
        {
            ValidateCreateAlert(model);

            if (ModelState.IsValid)
            {
                SaveAlert(model);
                model.AlertSaved = true;
                return PartialView(NhsMvc.Default.Views.SearchAlert.CreateAlert, model);
            }

            BindCreateAlertData(model);
            model.AlertSaved = false;
            return PartialView(NhsMvc.Default.Views.SearchAlert.CreateAlert, model);
        }

        [HttpGet]
        public virtual ActionResult ViewSavedAlerts()
        {

            var searchAlerts = _searchAlertService.GetSearchAlertsByUserGuid(UserSession.UserProfile.UserID);

            // If the user is not logged in, then I redirect him to the log in page
            if (UserSession.UserProfile.ActorStatus != WebActors.ActiveUser)
            {
                if (Request.Url != null)
                {
                    var next = HttpUtility.UrlEncode(Request.Url.PathAndQuery);
                    return Redirect(Pages.SignIn, new List<RouteParam> { new RouteParam(RouteParams.NextPage, next, RouteParamType.QueryString) });
                }
            }

            // If not id of the alert is in the parameters then select the first one
            SearchAlert selectedSearchAlert = null;
            var firstAlert = searchAlerts.FirstOrDefault();
            var marketName = String.Empty;
            var searchTerms = new StringBuilder();
            var selectedSearchAlertCommunities = new List<Community>();
            if (firstAlert != null)
            {
                selectedSearchAlert = NhsUrl.GetOptionID == 0 ? firstAlert : searchAlerts.FirstOrDefault(alert => alert.AlertId == NhsUrl.GetOptionID);
                if (selectedSearchAlert != null)
                {
                    if (selectedSearchAlert.MarketId != null)
                    {
                        var market = _marketService.GetMarket((int)selectedSearchAlert.MarketId);
                        marketName = market.MarketName;
                    }

                    // fill search terms
                    if (selectedSearchAlert.Bedrooms != null && selectedSearchAlert.Bedrooms != 0)
                        searchTerms.Append(", " + selectedSearchAlert.Bedrooms + " beds");
                    if (selectedSearchAlert.IsBoyl)
                        searchTerms.Append(", BOYL");
                    if (selectedSearchAlert.IsGatedCommunity)
                        searchTerms.Append(", gated");
                    if (selectedSearchAlert.IsCondo)
                        searchTerms.Append(", condo");
                    if (selectedSearchAlert.IsSingleFamily)
                        searchTerms.Append(", single family");
                    if (selectedSearchAlert.HasPool)
                        searchTerms.Append(", pool");
                    if (selectedSearchAlert.HasGolfCourse)
                        searchTerms.Append(", golf");
                    if (selectedSearchAlert.BuilderId != null && selectedSearchAlert.BuilderId.Value != 0)
                    {
                        searchTerms.Append(", built by " + _brandService.GetBrandById(selectedSearchAlert.BuilderId.Value, NhsRoute.BrandPartnerId));
                    }
                    if (!string.IsNullOrEmpty(selectedSearchAlert.SchoolDistrictName))
                    {
                        searchTerms.Append(", " + selectedSearchAlert.SchoolDistrictName);
                    }

                    // The communities in the SearchAlertResult does not have all the required information,
                    // so I am using the community service in order to get it
                    var communities = selectedSearchAlert.AlertResults.Select(alertResult => _communityService.GetCommunity(alertResult.CommunityId)).Where(comm => comm != null).ToList();
                    if (selectedSearchAlert.MarketId != null)
                        selectedSearchAlertCommunities = SortByLocation(communities, (int)selectedSearchAlert.MarketId,
                            selectedSearchAlert.State, selectedSearchAlert.City, selectedSearchAlert.PostalCode);
                    else
                    {
                        selectedSearchAlertCommunities = communities;
                    }

                    // I need to check for each of the communities if the brochure was already requested
                    // If the user already requested the brochure, then the View Brochure button is displayed instead of the free brochure button
                    foreach (var community in selectedSearchAlertCommunities)
                    {
                        var notBrochureUrl = _communityService.GetNonPdfBrochureUrl(community.CommunityId, 0, 0, community.BuilderId);

                        var comm = _communityService.GetVideoTourImages(community.CommunityId).Where(video => video.ImageTypeCode == ImageTypes.CommunityVideoTour)
                                                    .Select(media => new MediaPlayerObject { Url = media.OriginalPath }).ToList();

                        if (comm.Count > 0 && community.Videos.FirstOrDefault() != null)
                        {
                            community.Videos.FirstOrDefault().VideoURL = comm.First().Url;
                            community.HasVideo = 1;
                        }

                        if (!string.IsNullOrEmpty(notBrochureUrl)) continue;
                        var plannerListing = FindSavedCommunity(community.CommunityId, community.BuilderId);
                        var plannerRecommendedListing = FindRecoCommunity(community.CommunityId, community.BuilderId);
                        var sourceRequestKey = "";

                        if (plannerListing != null && !string.IsNullOrWhiteSpace(plannerListing.SourceRequestKey) && plannerListing.LeadRequestDate < DateTime.Now.AddDays(7))
                            sourceRequestKey = plannerListing.SourceRequestKey;
                        else if (plannerRecommendedListing != null &&
                                 !string.IsNullOrWhiteSpace(plannerRecommendedListing.SourceRequestKey) && plannerRecommendedListing.LeadRequestDate < DateTime.Now.AddDays(7))
                            sourceRequestKey = plannerRecommendedListing.SourceRequestKey;

                        if (string.IsNullOrEmpty(sourceRequestKey)) continue;
                        var @params = new List<RouteParam>
                        {
                            new RouteParam(RouteParams.RequestUniqueKey, sourceRequestKey, RouteParamType.QueryString),
                            new RouteParam(RouteParams.Community, community.CommunityId.ToString(), RouteParamType.QueryString)
                        };

                        community.ViewBrochureUrl = @params.ToUrl(Pages.BrochureInterstitial);
                    }
                }
            }

            var viewModel = new SearchAlertViewModel.SavedSearchAlert
            {
                SearchAlerts = searchAlerts.ToList(),
                FirstName = UserSession.UserProfile.FirstName,
                LastName = UserSession.UserProfile.LastName,
                SelectedSearchAlert = selectedSearchAlert,
                MarketName = marketName,
                SearchTerms = searchTerms.ToString(),
                SelectedSearchAlertCommunities = selectedSearchAlertCommunities
            };
            return View(NhsMvc.Default.Views.SearchAlert.SavedAlerts, viewModel);
        }

        public virtual ActionResult DeleteSavedSearchAlert(int searchAlertId)
        {
            // If the user is not logged in, then I redirect him to the log in page
            if (UserSession.UserProfile.ActorStatus != WebActors.ActiveUser)
            {
                if (Request.Url != null)
                {
                    var next = HttpUtility.UrlEncode(Request.Url.PathAndQuery);
                    return Redirect(Pages.SignIn, new List<RouteParam> { new RouteParam(RouteParams.NextPage, next, RouteParamType.QueryString) });
                }
            }

            var searchAlert = _searchAlertService.GetSearchAlert(searchAlertId);

            // Delete it
            _searchAlertService.RemoveSavedAlert(searchAlert);

            return RedirectToAction(NhsMvc.SearchAlert.ActionNames.ViewSavedAlerts);
        }

        [HttpPost]
        public virtual ActionResult SavedAlertRemoveCommunities(SearchAlertViewModel.SavedSearchAlert viewModel)
        {
            // If the user is not logged in, then I redirect him to the log in page
            if (UserSession.UserProfile.ActorStatus != WebActors.ActiveUser)
            {
                if (Request.Url != null)
                {
                    var next = HttpUtility.UrlEncode(Request.Url.PathAndQuery);
                    return Redirect(Pages.SignIn, new List<RouteParam> { new RouteParam(RouteParams.NextPage, next, RouteParamType.QueryString) });
                }
            }

            // Removed the selected communities.
            var selectedCommunities = viewModel.SelectedSearchAlertCommunities.Where(comm => comm.Selected).ToList();
            foreach (var community in selectedCommunities)
            {
                _searchAlertService.RemoveAlertResult(viewModel.SelectedSearchAlert.AlertId, community.CommunityId, community.BuilderId);
            }

            // I update the search alert, since I removed some of the communities
            viewModel.SelectedSearchAlert = _searchAlertService.GetSearchAlert(viewModel.SelectedSearchAlert.AlertId);

            // The communities in the SearchAlertResult does not have all the required information,
            // so I am using the community service in order to get it
            viewModel.SelectedSearchAlertCommunities = viewModel.SelectedSearchAlert.AlertResults.Select(alertResult => _communityService.GetCommunity(alertResult.CommunityId)).Where(comm => comm != null).ToList();
            return PartialView(NhsMvc.Default.Views.SearchAlert.SavedAlertResults, viewModel);
        }

        [HttpPost]
        public virtual JsonResult SavedAlertSaveCommunities(SearchAlertViewModel.SavedSearchAlert viewModel)
        {
            var selectedCommunities = viewModel.SelectedSearchAlertCommunities.Where(comm => comm.Selected).ToList();

            foreach (var community in selectedCommunities)
            {
                var pl = new PlannerListing(community.CommunityId, ListingType.Community);
                if (!UserSession.UserProfile.Planner.SavedCommunities.Contains(pl))
                {
                    UserSession.UserProfile.Planner.AddSavedCommunity(community.CommunityId, community.BuilderId);
                    var logger = new ImpressionLogger
                    {
                        CommunityId = community.CommunityId,
                        BuilderId = community.BuilderId,
                        PartnerId = NhsRoute.PartnerId.ToString(),
                        Refer = UserSession.Refer
                    };
                    logger.LogView(LogImpressionConst.AddCommunityToUserPlanner);
                }
            }

            return Json(selectedCommunities.Count + (selectedCommunities.Count == 1 ? " Community was" : " Communities were") + " saved.");
        }

        public virtual ActionResult SortAlertResults(int alertId, string sortOption)
        {
            var viewModel = new SearchAlertViewModel.SavedSearchAlert
            {
                SelectedSearchAlert = _searchAlertService.GetSearchAlert(alertId)
            };

            // The communities in the SearchAlertResult does not have all the required information,
            // so I am using the community service in order to get it
            var communities = viewModel.SelectedSearchAlert.AlertResults.Select(alertResult => _communityService.GetCommunity(alertResult.CommunityId)).Where(comm => comm != null).ToList();

            switch (sortOption.ToLower())
            {
                case "":
                    viewModel.SelectedSearchAlertCommunities = communities;
                    break;
                case "location":
                    if (viewModel.SelectedSearchAlert.MarketId != null)
                    {
                        viewModel.SelectedSearchAlertCommunities = SortByLocation(communities, (int)viewModel.SelectedSearchAlert.MarketId,
                            viewModel.SelectedSearchAlert.State, viewModel.SelectedSearchAlert.City, viewModel.SelectedSearchAlert.PostalCode);
                    }
                    else
                    {
                        viewModel.SelectedSearchAlertCommunities = communities;
                    }

                    break;
                case "price":
                    viewModel.SelectedSearchAlertCommunities = communities.OrderBy(comm => comm.PriceLow);
                    break;
                case "builder":
                    viewModel.SelectedSearchAlertCommunities = communities.OrderBy(comm => comm.Brand.BrandName);
                    break;
                case "name":
                    viewModel.SelectedSearchAlertCommunities = communities.OrderBy(comm => comm.CommunityName);
                    break;
                case "homematches":
                    viewModel.SelectedSearchAlertCommunities = communities.OrderByDescending(comm => comm.HomeCount);
                    break;
                case "specialoffer":
                    viewModel.SelectedSearchAlertCommunities = communities.OrderByDescending(comm => comm.HasConsumerPromo);
                    break;
                case "quickmovein": // this one should sort comms from most # of specs with quick move-in status
                    viewModel.SelectedSearchAlertCommunities = communities.OrderBy(comm => comm.QuickMoveInCount);
                    break;
            }

            return PartialView(NhsMvc.Default.Views.SearchAlert.SavedAlertResults, viewModel);
        }

        public virtual ActionResult GetAreas2(string state)
        {
            var listItems = GetAreaListItems(state);
            return Json(listItems, JsonRequestBehavior.AllowGet);
        }


        public virtual JsonResult GetAreas(string state)
        {
            var listItems = GetAreaListItems(_stateService.GetStateAbbreviation(state));
            return Json(listItems, JsonRequestBehavior.AllowGet);
        }
        public virtual JsonResult GetCities(string market)
        {
            var listItems = GetCityListItems(market);
            return Json(listItems, JsonRequestBehavior.AllowGet);
        }
        public virtual JsonResult GetBuilders(string market)
        {
            if (StringHelper.IsValidZip(market))    // supports zip
                market = _marketService.GetMarketIdFromPostalCode(market, NhsRoute.PartnerId).ToString();
            var listItems = GetBuilderListItems(market);
            return Json(listItems, JsonRequestBehavior.AllowGet);
        }
        public virtual JsonResult GetSchools(string market)
        {
            if (StringHelper.IsValidZip(market))    // supports zip
                market = _marketService.GetMarketIdFromPostalCode(market, NhsRoute.PartnerId).ToString();
            var listItems = GetSchoolListItems(market);
            return Json(listItems, JsonRequestBehavior.AllowGet);
        }
        public virtual JsonResult GetSuggestedName(string area, string areaText, string city, string zip, string priceFrom, string priceTo)
        {
            var suggestedName = GetAlertName(city, areaText, area, zip, priceFrom, priceTo);
            return Json(suggestedName, JsonRequestBehavior.AllowGet);
        }


        #region Private Methods

        #region Create Alert

        private void BindCreateAlertData(SearchAlertViewModel.CreateAlertViewModel model)
        {
            // Dynamic drop down lists.             
            var states = _stateService.GetSearchAlertStates(NhsRoute.PartnerId);
            model.StateList = new SelectList(states, "StateAbbr", "StateName");
            model.AreaList = GetAreaListItems(model.State);
            model.CityList = GetCityListItems(model.Area);
            model.SchoolList = GetSchoolListItems(model.Area);
            model.BuilderList = GetBuilderListItems(model.Area);

            // Fixed drop down lists. 
            var radius = _lookupService.GetCommonListItems(CommonListItem.Radius);
            var minPrices = _lookupService.GetCommonListItems(CommonListItem.MinPrice);
            var maxPrices = _lookupService.GetCommonListItems(CommonListItem.MaxPrice);
            var bedrooms = _lookupService.GetCommonListItems(CommonListItem.Bedrooms);
            model.RadiusList = new SelectList(radius, "LookupValue", "LookupText");
            model.MinPriceList = new SelectList(minPrices, "LookupValue", "LookupText");
            model.MaxPriceList = new SelectList(maxPrices, "LookupValue", "LookupText");
            model.BedroomsList = new SelectList(bedrooms, "LookupValue", "LookupText");

            // Other properties
            model.FromRegistration = RouteParams.Registration.Value().ToType<bool>() || RouteParams.RegFromLeads.Value().ToType<bool>();


            model.SkipLink = GetSkipCreateAlertLOnClick();
        }

        private void ValidateCreateAlert(SearchAlertViewModel.CreateAlertViewModel model)
        {
            // Alerts limit
            var alerts = _searchAlertService.GetSearchAlertsByUserGuid(UserSession.UserProfile.UserID);
            var alertNames = alerts.Select(alert => alert.Title).ToList();
            if (alertNames.Count >= 5)
                ModelState.AddModelError("SuggestedName", LanguageHelper.MSG_SEARCH_ALERT_LIMIT);

            // Alert name
            if (string.IsNullOrEmpty(model.SuggestedName))
                ModelState.AddModelError("SuggestedName", LanguageHelper.MSG_SEARCH_ALERT_NO_NAME);
            else if (alertNames.Contains(model.SuggestedName))
                ModelState.AddModelError("SuggestedName", LanguageHelper.MSG_SEARCH_ALERT_NAME_EXISTS);

            // No location selected
            if (string.IsNullOrEmpty(model.State) && string.IsNullOrEmpty(model.Area) && string.IsNullOrEmpty(model.Zip))
                ModelState.AddModelError("Area", LanguageHelper.MSG_SEARCH_ALERT_NO_LOCATION);

            // Search by zip
            else if (string.IsNullOrEmpty(model.State) && string.IsNullOrEmpty(model.Area))
            {
                var marketId = _marketService.GetMarketIdFromPostalCode(model.Zip, NhsRoute.PartnerId);
                if (!StringHelper.IsValidZip(model.Zip))
                    ModelState.AddModelError("Zip", LanguageHelper.MSG_REGISTER_INVALID_ZIP);
                else if (marketId == 0)
                    ModelState.AddModelError("Zip", LanguageHelper.MSG_REGISTER_INVALID_ZIP);
            }

            // Search by state & area
            else if (string.IsNullOrEmpty(model.Zip) && string.IsNullOrEmpty(model.Area))
                ModelState.AddModelError("Area", LanguageHelper.MSG_ADV_SEARCH_AREA_P15);

            // Price range
            if (string.IsNullOrEmpty(model.PriceFrom) || string.IsNullOrEmpty(model.PriceTo))
                ModelState.AddModelError("PriceFrom", LanguageHelper.MSG_SEARCH_ALERT_NO_PRICE);
            else if (int.Parse(model.PriceFrom) > int.Parse(model.PriceTo))
                ModelState.AddModelError("PriceTo", LanguageHelper.MSG_ADV_SEARCH_PRICE_RANGE);
        }

        private void SaveAlert(SearchAlertViewModel.CreateAlertViewModel model)
        {
            var searchAlert = new SearchAlert { UserGuid = UserSession.UserProfile.UserID };

            // Search by state & area
            if (!string.IsNullOrEmpty(model.State))
            {
                searchAlert.State = model.State;
                searchAlert.MarketId = int.Parse(model.Area);
                if (!string.IsNullOrEmpty(model.City))
                {
                    searchAlert.City = model.City;
                }
            }

            // Search by zip
            else if (!string.IsNullOrEmpty(model.Zip))
            {
                searchAlert.PostalCode = model.Zip;
                searchAlert.Radius = model.Radius;
                searchAlert.MarketId = _marketService.GetMarketIdFromPostalCode(model.Zip, NhsRoute.PartnerId);
                searchAlert.State = _marketService.GetStateNameForMarket(searchAlert.MarketId.ToType<int>());
            }

            //Market
            var market = _marketService.GetMarket(NhsRoute.PartnerId, searchAlert.MarketId.ToType<int>(), true);
            searchAlert.MarketName = market.MarketName;

            searchAlert.Lat = market.LatLong.Latitude;
            searchAlert.Lng = market.LatLong.Longitude;
            _searchAlertService.SetGeoLocation(market, model.State, model.City, model.Zip, model.Radius, searchAlert, _mapService);

            //School
            var schoolDistrict = market.SchoolDistricts.FirstOrDefault(s => s.DistrictId == model.School.ToType<int>());
            if (schoolDistrict != null)
                searchAlert.SchoolDistrictName = schoolDistrict.DistrictName;

            //Builder
            var brand = market.Brands.FirstOrDefault(b => b.BrandId == model.Builder.ToType<int>());

            if (brand != null)
                searchAlert.BuilderName = brand.BrandName;

            // Min/Max price
            searchAlert.PriceMin = int.Parse(model.PriceFrom);
            searchAlert.PriceMax = int.Parse(model.PriceTo);

            // Options
            if (model.Bedrooms != -1)
                searchAlert.Bedrooms = model.Bedrooms;
            searchAlert.IsSingleFamily = model.TypeSingle;
            searchAlert.IsCondo = model.Condo;
            searchAlert.HasPool = model.AmenitiyPool;
            searchAlert.HasGolfCourse = model.AmenitiyGolf;
            searchAlert.IsGatedCommunity = model.AmenitiyGated;
            if (!string.IsNullOrEmpty(model.School))
                searchAlert.SchoolDistrictId = int.Parse(model.School);
            if (!string.IsNullOrEmpty(model.Builder))
                searchAlert.BuilderId = int.Parse(model.Builder);

            // Search name
            searchAlert.Title = model.SuggestedName;

            // Creates alert
            _searchAlertService.CreateAlert(NhsRoute.PartnerId, searchAlert, UserSession.UserProfile, 0); //No source community available since it's a stand-alone search alert
        }

        private string GetSkipCreateAlertLOnClick()
        {
            var paramList = UserSession.GetPersistedtUrlParams();
            var regFromLeads = paramList.Value(RouteParams.RegFromLeads).ToType<bool>();

            // Coming from registration after request_info
            if (regFromLeads)
            {
                paramList.Add(new RouteParam(RouteParams.AlertCreated, "false", RouteParamType.QueryString));
                var url = paramList.ToUrl(Pages.LeadsRequestBrochureThanksModal);
                return string.Format("tb_ChangeUrl('{0}')", url);
            }

            // Directly from registration
            return "window.location.reload()";
        }

        #endregion

        private string GetAlertName(string city, string areaText, string area, string zip, string priceFrom, string priceTo)
        {
            var alerts = _searchAlertService.GetSearchAlertsByUserGuid(UserSession.UserProfile.UserID);
            var alertNames = alerts.Select(alert => alert.Title).ToList();
            return GetSuggestedName(city, areaText, area, zip, priceFrom, priceTo, alertNames);
        }

        private string GetSuggestedName(string city, string areaText, string areaValue, string zip, string priceFrom, string priceTo, IList<string> userAlertsNames)
        {
            // Checks fields aren't empty
            if ((!string.IsNullOrEmpty(areaValue) || !string.IsNullOrEmpty(zip)) && (!string.IsNullOrEmpty(priceFrom) && !string.IsNullOrEmpty(priceTo)))
            {
                // Sets geography name (city, area and zip in that order)
                string geography;
                if (!string.IsNullOrEmpty(city))
                    geography = city;
                else if (!string.IsNullOrEmpty(areaValue))
                    geography = areaText;
                else
                    geography = zip;

                // Generates suggested name
                string suggestedName = string.Format(LanguageHelper.Alert + ": [{0}], ${1}-${2}", geography, priceFrom, priceTo);

                // Verifies that it's unique
                string uniqueName = suggestedName;
                if (userAlertsNames.Contains(uniqueName))
                {
                    for (int i = 1; userAlertsNames.Contains(uniqueName); i++)
                        uniqueName = string.Format("{0}_{1}", suggestedName, i);
                }
                return uniqueName;
            }
            return string.Empty;
        }

        private IList<SelectListItem> GetAreaListItems(string state)
        {
            var list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = LanguageHelper.NoPreference, Value = string.Empty });   // Option item

            // Gets areas
            if (!string.IsNullOrEmpty(state))
            {
                var markets = _marketService.GetMarketsByState(NhsRoute.PartnerId, state);
                list.AddRange(markets.Select(m => new SelectListItem { Text = m.MarketName, Value = m.MarketId.ToString() }));
            }

            return list;
        }

        private IList<SelectListItem> GetCityListItems(string marketVal)
        {
            int marketId = string.IsNullOrEmpty(marketVal) ? 0 : int.Parse(marketVal);

            var list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = LanguageHelper.NoPreference, Value = string.Empty });   // Option item

            // Gets areas
            if (marketId != 0)
            {
                var market = _marketService.GetMarket(NhsRoute.PartnerId, marketId, false);
                list.AddRange(market.Cities.Select(c => new SelectListItem { Text = c.CityName, Value = c.CityName }));
            }

            return list;
        }

        private IList<SelectListItem> GetSchoolListItems(string marketVal)
        {
            int marketId = string.IsNullOrEmpty(marketVal) ? 0 : int.Parse(marketVal);

            var list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = LanguageHelper.AllSchoolDistricts, Value = string.Empty });   // Option item

            // Gets areas
            if (marketId != 0)
            {
                var market = _marketService.GetMarket(NhsRoute.PartnerId, marketId, true);
                list.AddRange(market.SchoolDistricts.Select(s => new SelectListItem { Text = s.DistrictName, Value = s.DistrictId.ToString() }));
            }

            return list;
        }

        private IList<SelectListItem> GetBuilderListItems(string marketVal)
        {
            int marketId = string.IsNullOrEmpty(marketVal) ? 0 : int.Parse(marketVal);

            var list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = LanguageHelper.AllBuilders, Value = string.Empty });   // Option item

            // Gets areas
            if (marketId != 0)
            {
                var market = _marketService.GetMarket(NhsRoute.PartnerId, marketId, true);
                list.AddRange(market.Brands.Select(b => new SelectListItem { Text = b.BrandName, Value = b.BrandId.ToString() }));
            }

            return list;
        }

        #endregion

        #region ViewSearchAlert
        private GeoCoordinate GetAlertGeoLocation(int marketId, string stateName, string cityName, string zipCode, out int radius)
        {
            var market = _marketService.GetMarket(NhsRoute.PartnerId, marketId, true);
            var location = new GeoCoordinate();

            if (string.IsNullOrWhiteSpace(stateName) == false && string.IsNullOrWhiteSpace(cityName) == false)
            {
                var city = _mapService.GeoCodeCityByMarket(cityName, market.MarketId.ToType<int>(),
                                market.StateAbbr);
                location.Latitude = city.Latitude;
                location.Longitude = city.Longitude;
                radius = city.Radius;
                return location;
            }

            if (string.IsNullOrWhiteSpace(zipCode) == false)
            {
                var zip = _mapService.GeoCodePostalCode(zipCode);
                location.Latitude = zip.Latitude;
                location.Longitude = zip.Longitude;
                radius = zip.Radius;
                return location;
            }

            if (market.LatLong != null)
            {
                location.Latitude = market.LatLong.Latitude;
                location.Longitude = market.LatLong.Longitude;
            }
            else
            {
                location.Latitude = market.Latitude.ToType<double>();
                location.Longitude = market.Longitude.ToType<double>();
            }
            radius = market.Radius;

            return location;
        }

        private List<Community> SortByLocation(List<Community> communities, int marketId, string state, string city, string postalCode)
        {
            var radius = 0;
            var alertLocation = GetAlertGeoLocation(marketId, state, city, postalCode, out radius);

            foreach (var community in communities)
            {
                var commLocation = new GeoCoordinate((double)community.Latitude,
                    (double)community.Longitude);
                community.DistanceFromCenter = alertLocation.GetDistanceTo(commLocation);
            }

            return communities.OrderBy(comm => comm.DistanceFromCenter).ToList();
        }

        private PlannerListing FindSavedCommunity(int communityId, int builderId)
        {
            return UserSession.UserProfile.Planner.SavedCommunities.FirstOrDefault(pl => pl.ListingId == communityId && pl.BuilderId == builderId);
        }

        private PlannerListing FindRecoCommunity(int communityId, int builderId)
        {
            return UserSession.UserProfile.Planner.RecommendedListings.FirstOrDefault(pl => pl.ListingId == communityId && pl.BuilderId == builderId);
        }

        #endregion

    }
}
