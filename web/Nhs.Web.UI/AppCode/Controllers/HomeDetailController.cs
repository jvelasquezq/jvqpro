﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;
using Bdx.Web.Api.Objects;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Route;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers.Content;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.ViewModels;
using System.Linq;
using Nhs.Library.Helpers.Utility;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels.NewMediaViewer;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class HomeDetailController : BaseDetailController
    {
        #region Biz Services

        private readonly IPartnerService _partnerService;
        private readonly IListingService _listingService;
        private readonly ICommunityService _communityService;
        private readonly ILookupService _lookupService;
        private readonly IStateService _stateService;
        private readonly IMarketService _marketService;
        private readonly IPathMapper _pathMapper;
        private readonly IBoylService _boylService;
        private readonly IBrandService _brandService;
        private readonly IAffiliateLinkService _affiliateLinkService;
        #endregion

        #region Constructor
        public HomeDetailController(IPartnerService partnerService, IListingService listingService, ICommunityService communityService,
            ILookupService lookupService, IStateService stateService, IMarketService marketService, IPathMapper pathMapper, 
            IBoylService boylService, IBrandService brandService, 
            IAffiliateLinkService affiliateLinkService)
            : base(communityService, listingService, pathMapper)
        {
            _brandService = brandService;
            _partnerService = partnerService;
            _listingService = listingService;
            _communityService = communityService;
            _lookupService = lookupService;
            _stateService = stateService;
            _pathMapper = pathMapper;
            _boylService = boylService;
            _marketService = marketService;
            _affiliateLinkService = affiliateLinkService;
        }
        #endregion

        #region Actions

        #region Default Show Get

        public virtual ActionResult MediaViewer(int specid, int planid, string nextPage )
        {
            IPlan plan = null;
            ISpec spec = null;
            var virtualTourUrl = "";
            var floorPlanViewerUrl = "";
            var homeTitle = "";
            decimal price= 0;
            bool isModelHome = false;

            if (planid > 0)
            {
                plan = _listingService.GetPlan(planid);
                homeTitle = plan.PlanName;
                price = plan.Price;
                virtualTourUrl = plan.VirtualTourUrl;
                floorPlanViewerUrl = _listingService.GetFloorPlanViewerUrl(plan, null);
            }
            else if (specid > 0)
            {
                spec = _listingService.GetSpec(specid);
                plan = spec.Plan;
                homeTitle = string.Format("{0} ({1})", (spec).Address1, spec.PlanName);
                price = spec.Price;
                virtualTourUrl = spec.VirtualTourUrl;
                floorPlanViewerUrl = _listingService.GetFloorPlanViewerUrl(null, spec);
                isModelHome = spec.HomeStatus == (int) HomeStatusType.ModelHome;
            }

            if (plan == null)
            {
                throw new ArgumentException("plan or spec need it");
            }

            var currentCommunity = _communityService.GetCommunity(plan.CommunityId);

            var model = new NewMediaViewerViewModel
            {
                PropertyName = homeTitle,
                CommunityId = plan.CommunityId,
                PlanId = planid,
                SpecId = specid,
                BuilderId = currentCommunity.BuilderId,
                MarketId = currentCommunity.Market.MarketId,
                City = currentCommunity.SalesOffice.City,
                State = currentCommunity.SalesOffice.State,
                ZipCode = currentCommunity.SalesOffice.ZipCode,
                MediaPlayerList = GetMediaPlayerObjectsList(plan, spec, virtualTourUrl, floorPlanViewerUrl),
                IsAjaxRequest = Request.IsAjaxRequest(),
                IsLoggedIn = UserSession.UserProfile.IsLoggedIn(),
                BcType = currentCommunity.BCType,
                NextPage = nextPage,
                PriceDisplay = (isModelHome? string.Empty : string.Format(LanguageHelper.From.ToTitleCase() + ": {0:C0}", price))
            };

            model.FirstImage = model.MediaPlayerList.Any()
                                     ? Configuration.IRSDomain + model.MediaPlayerList.First().Url
                                     : string.Empty;
            model.PageUrl = new List<RouteParam>
            {
                planid > 0
                    ? new RouteParam(RouteParams.PlanId, planid)
                    : new RouteParam(RouteParams.SpecId, specid)
            }.ToUrl(Pages.HomeDetail, true, true);

            model.PinterestDescription =
                string.Format("{0} by {1} at {2}", plan.PlanName, currentCommunity.Brand.BrandName,
                    currentCommunity.CommunityName).Replace("'", "\\'");

            if (UserSession.UserProfile.IsLoggedIn())
            {
                var pl = planid == 0
                     ? new PlannerListing(specid, ListingType.Spec) { BuilderId = model.BuilderId }
                     : new PlannerListing(planid, ListingType.Plan) { BuilderId = model.BuilderId };
                if (UserSession.UserProfile.Planner.SavedHomes.Contains(pl))
                    model.SavedToPlanner = true;
            }

            if (Request.IsAjaxRequest())
                return PartialView(NhsMvc.Default.Views.Common.PropertyDetail.NewMediaViewer, model);
            return View(NhsMvc.Default.Views.Common.PropertyDetail.NewMediaViewer, model);
        }

        public virtual ActionResult ShowV2()
        {
            return Show();
        }

        public virtual ActionResult ShowPreview(int? specId, int? planId)
        {
            var hdvm = GetHomeDetailPreviewModel(planId.ToType<int>(), specId.ToType<int>());
          
            var metaRegistrar = new MetaRegistrar(_pathMapper);
            var metas = metaRegistrar.GetRobotsMeta("noodp, noydir, noindex");
            AddNoIndex(metaRegistrar, metas.ToList());

            OverrideDefaultMeta(hdvm, metas);

            return View("ShowV2", hdvm);
        }

        public virtual ActionResult Show()
        {
            var planId = RouteParams.PlanId.Value<int>();
            var specId = RouteParams.SpecId.Value<int>();

            if (UserSession.PreviousPage != Pages.CommunityDetail)
                UserSession.PreviousPage = Pages.HomeDetail;

            IPlan plan = null;
            ISpec spec = null;

            if (planId > 0)
            {
                plan = _listingService.GetPlan(planId);
                if (plan != null && _communityService.IsCommunityActiveOnParentBrandPartner(plan.CommunityId, NhsRoute.PartnerId) == false)
                {
                    plan = null;
                }
            }

            if (specId > 0)
            {
                spec = _listingService.GetSpec(specId);
                if (spec != null)
                {
                    plan = spec.Plan;
                    if (_communityService.IsCommunityActiveOnParentBrandPartner(spec.Plan.CommunityId, NhsRoute.PartnerId) == false)
                    {
                        plan = null;
                        spec = null;
                    }
                }
            }

            if (plan == null && spec == null)
            {
                // Add an extra parameter to identify from SEO inactive data
                if (string.IsNullOrEmpty(RouteParams.Msg.Value<string>()))
                {
                    return RedirectPermanent(GetInactiveUrl());
                }
                return GetInactivePropertyViewModel(false, _stateService, _lookupService);
            }

            //301ing in the base/application controller constructor will still allow this action to be executed, so we 301 here.
            var refer301Url = GetReferRedirectUrl(spec, plan, specId > 0);

            if (!string.IsNullOrEmpty(refer301Url))
                return RedirectPermanent(refer301Url);

            var currentCommunity = _communityService.GetCommunity(plan.CommunityId);

            //Redirect to a 404 if the parent community is Basic. Case 78407
            if (!string.IsNullOrEmpty(currentCommunity.PartnerMask))
            {
                var partnerMaskIndex = _partnerService.GetPartnerMaskIndex(NhsRoute.PartnerId);
                var partnerSortValue = currentCommunity.PartnerMask.Substring(partnerMaskIndex - 1, 1);

                if ("F" == partnerSortValue && NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
                {
                    return RedirectTo404();
                }
            }


            var pageDetail = RedirectionHelper.GetDetailPageRedirectName(false, specId > 0);
            if (!string.IsNullOrEmpty(pageDetail))
            {
                if (spec != null)
                    return RedirectPermanent(pageDetail, spec.ToHomeDetail());

                if (plan != null)
                    return RedirectPermanent(pageDetail, plan.ToHomeDetail());
            }

            var addCanonical = AddCanonical(plan.CommunityId);
            var model = NhsRoute.ShowMobileSite ? GetViewModelMobile(plan, spec, !addCanonical, currentCommunity) : GetViewModel(plan, spec, !addCanonical, currentCommunity);

            if (addCanonical && string.IsNullOrEmpty(model.Globals.CanonicalLink)) // if apply the rules to addCanonical and it wasnt defined in the Seo Content File
                base.AddCanonicalWithIncludeParams(model, 
                    new List<string> { "planid", "specid" }, 
                    (currentCommunity != null && currentCommunity.OwnerPartnerId == NhsRoute.PartnerId && NhsRoute.BrandPartnerId != NhsRoute.PartnerId));

            if (planId > 0)
                UserSession.UserProfile.Planner.AddRecentlyViewedHome(planId, false);
            else
                UserSession.UserProfile.Planner.AddRecentlyViewedHome(specId, true);

            // ReSharper disable once Mvc.ViewNotResolved
            return View(model);
        }

        public virtual ActionResult GaleryFloorPlan(int selectImage)
        {
            var planId = RouteParams.PlanId.Value<int>();
            var specId = RouteParams.SpecId.Value<int>();
            var useHub = RouteParams.PreviewMode.Value<bool>();
            var isPlan = specId == 0;
            IList<IImage> floorPlanImages;

            _listingService.UseHub = useHub;
            if (isPlan)
            {
                var plan = _listingService.GetPlan(planId);
                floorPlanImages = _listingService.GetFloorPlanImagesForPlan(plan.PlanId);
            }
            else
            {
                var spec = _listingService.GetSpec(specId);
                var plan = _listingService.GetPlan(spec.PlanId);
                floorPlanImages = _listingService.GetFloorPlanImagesForSpec(plan, spec);
            }

            var model = new HomeGaleryFloorPlanViewModel
                {
                    FloorPlanImages = floorPlanImages,
                    SelectImage = selectImage,
                    Height = RouteParams.Height.Value<int>()
                };
            return PartialView(NhsMvc.Default.Views.HomeDetail.GaleryFloorPlan, model);
        }

        #endregion
        #region Lead Confirmation
        public virtual ActionResult LeadConfirmation(HomeDetailViewModel model)
        {            
            
            return PartialView(NhsMvc.Default.Views.Common.LeadForm.LeadConfirmation, model);
        }
        #endregion

        #region Add Home to User Account
        public virtual JsonResult AddHomeToUserPlanner()
        {
            IPlan plan = null;
            ISpec spec = null;
            var isPlan = false;
            var planId = RouteParams.PlanId.Value<int>();
            var specId = RouteParams.SpecId.Value<int>();
            int listingId = 0;
            int communityId = 0;

            if (planId > 0)
            {
                isPlan = true;
                plan = _listingService.GetPlan(planId);
                listingId = planId;
                communityId = plan.CommunityId;
            }

            if (specId > 0)
            {
                spec = _listingService.GetSpec(specId);
                listingId = specId;
                communityId = spec.CommunityId;
            }

            if (UserSession.UserProfile.ActorStatus != WebActors.ActiveUser) //Redirect to login
            {
                var thisPageParams = new List<RouteParam>
                {
                    isPlan
                        ? new RouteParam(RouteParams.PlanId, plan.PlanId.ToString())
                        : new RouteParam(RouteParams.SpecId, spec.SpecId.ToString()),
                    new RouteParam(RouteParams.AddToProfile, "true", RouteParamType.QueryString, true, true)
                };

                string nextPage = thisPageParams.ToUrl(Pages.HomeDetail);

                var nextPageParams = new List<RouteParam>
                {
                    new RouteParam(RouteParams.NextPage, nextPage, RouteParamType.QueryString)
                };

                string url = nextPageParams.ToUrl(NhsRoute.IsMobileDevice ? Pages.LoginModal : Pages.Login);
                return Json(new { redirectUrl = url, data = false });
            }

            PlannerListing pl;
            var currentCommunity = _communityService.GetCommunity(isPlan ? plan.CommunityId : spec.Plan.CommunityId);
            
            pl = isPlan
                ? new PlannerListing(listingId, ListingType.Plan) {BuilderId = currentCommunity.BuilderId}
                : new PlannerListing(listingId, ListingType.Spec) {BuilderId = currentCommunity.BuilderId};  

            pl.UserId = UserSession.UserProfile.UserID;

            SaveHomeToUserPlanner(communityId, pl, isPlan);
            return Json(new { message = LanguageHelper.MSG_HOMEDETAIL_ADDED_TO_PLANNER, data = true });
        }
        #endregion

        #region Log Phone Number and Mortgage Link Click
        [HttpPost]
        public virtual JsonResult LogPhoneNumber(int communityId, int builderId, int planId, int specId)
        {
            var isPlan = specId == 0;
            LogHomeHeaderClickAction(communityId, builderId, planId, specId, isPlan, NhsRoute.IsMobileDevice ? LogImpressionConst.MobileHomeDetailCallSalesOffice : LogImpressionConst.HomeBuilderPhone);
            return new JsonResult { Data = "Click has been logged!" };
        }

        [HttpPost]
        public virtual JsonResult LogIPhoneNumber(int communityId, int builderId, int planId, int specId)
        {
            var isPlan = specId == 0;
            LogHomeHeaderClickAction(communityId, builderId, planId, specId, isPlan, LogImpressionConst.CallHomeDetail);
            return new JsonResult { Data = "Click has been logged!" };
        }


        [HttpPost]
        public virtual JsonResult LogMortgageRates(int communityId, int builderId, int planId, int specId)
        {
            var isPlan = specId == 0;
            LogHomeHeaderClickAction(communityId, builderId, planId, specId, isPlan, LogImpressionConst.GetMortgateRatesHomeDetail);
            return new JsonResult { Data = "Click has been logged!" };
        }

        [HttpPost]
        public virtual JsonResult LogMortgageMatch(int communityId, int builderId, int planId, int specId)
        {
            var isPlan = specId == 0;
            LogHomeHeaderClickAction(communityId, builderId, planId, specId, isPlan, LogImpressionConst.GetMortgateMatchHomeDetail);
            return new JsonResult { Data = "Click has been logged!" };
        }

        #endregion

        #region Log Next Steps section

        public virtual JsonResult LogNextStepsSave(int communityId, int builderId, int planId, int specId)
        {
            LogHomeNextStepsClickAction(communityId, builderId, planId, specId, LogImpressionConst.NextStepsClickSave);
            return new JsonResult { Data = "Saved to your profile!" };
        }

        public virtual JsonResult LogNextStepsPrint(int communityId, int builderId, int planId, int specId)
        {
            LogHomeNextStepsClickAction(communityId, builderId, planId, specId, LogImpressionConst.NextStepsClickPrint);
            return new JsonResult { Data = "Click has been logged!" };
        }

        public virtual JsonResult LogNextStepsBrochure(int communityId, int builderId, int planId, int specId)
        {
            LogHomeNextStepsClickAction(communityId, builderId, planId, specId, LogImpressionConst.NextStepsClickBrochure);
            return new JsonResult { Data = "Click has been logged!" };
        }

        public virtual JsonResult LogNextStepsSeePhone(int communityId, int builderId, int planId, int specId)
        {
            LogHomeNextStepsClickAction(communityId, builderId, planId, specId, LogImpressionConst.NextStepsClickSeePhone);
            return new JsonResult { Data = "Click has been logged!" };
        }

        #endregion
   
        #endregion

        #region Private Methods

        private void SaveHomeToUserPlanner(int commId, PlannerListing pl, bool isPlan)
        {
            if (!UserSession.UserProfile.Planner.SavedHomes.Contains(pl))
            {
                UserSession.UserProfile.Planner.AddSavedHome(pl.ListingId, !isPlan);                //if listing was not saved then add same to planner
                LogHomeHeaderClickAction(commId, pl.BuilderId, pl.ListingId, pl.ListingId, isPlan, LogImpressionConst.AddHomeToUserPlanner);
            }
        }

        private static void LogHomeHeaderClickAction(int communityId, int builderId, int planId, int specId, bool isPlan, string eventCode)
        {
            var logger = new ImpressionLogger
            {
                CommunityId = communityId,
                BuilderId = builderId,
                PartnerId = NhsRoute.PartnerId.ToString(CultureInfo.InvariantCulture),
                Refer = UserSession.Refer
            };
            if (isPlan)
                logger.AddPlan(planId);
            else if (specId > 0)
                logger.AddSpec(specId);

            logger.LogView(eventCode);
        }

        private void LogHomeNextStepsClickAction(int communityId, int builderId, int planId, int specId, string eventCode)
        {
            var logger = new ImpressionLogger
            {
                CommunityId = communityId,
                BuilderId = builderId,
                PartnerId = NhsRoute.PartnerId.ToString(CultureInfo.InvariantCulture),
                Refer = UserSession.Refer
            };
            if (specId > 0)
                logger.AddSpec(specId);
            else if (planId > 0)
                logger.AddPlan(planId);

            logger.LogView(eventCode);
        }
        #endregion
    }
}
