﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Bdx.Web.Api.Objects;
using Bdx.Web.Api.Objects.Constants;
using Nhs.Library.Business;
using Nhs.Library.Business.Map;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Helpers.WebApiServices;
using Nhs.Library.Proxy;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Entities;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Utility.Constants;
using Nhs.Web.UI.AppCode.Extensions;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class CommunityResultsController
    {
        #region Private Methods


        private IEnumerable<Brand> GetBrandsInformation(int marketId, IList<ApiFacetOption> brandsToFill)
        {
            var brands = _brandService.GetBrandsByMarket(NhsRoute.PartnerId, marketId);
            // this is already cached so no impact in CR performance, problem is brandService.GerBrandsByMarket returns isBasic that is not returned by marketService.GetMarket().Brands (when brand and school info is loaded)
            var extraBrandsInfo = _marketService.GetMarket(NhsRoute.PartnerId, marketId, true).Brands;
            brands = brands.Where(brand => brandsToFill.Any(x => x.Key == brand.BrandId)).ToList();

            foreach (var brand in brands)
            {
                var tempBrand = extraBrandsInfo.FirstOrDefault(eb => eb.BrandId == brand.BrandId);

                if (tempBrand == null) continue;

                brand.HasBilledCommununities = tempBrand.HasBilledCommununities;
                brand.IsBoyl = tempBrand.IsBoyl;
                brand.IsCustomBuilder = tempBrand.IsCustomBuilder;
            }

            return brands;
        }

        private ApiResultCounts GetFacetCounts(SearchParams searchParams)
        {
            GetLocationCoordinates(ref searchParams, searchParams.GetLocationCoordinates ? GetMarket(searchParams.MarketId) : null);
            if (searchParams.SrpType == SearchResultsPageType.CommunityResults)
            {
                var resultsWepApi = GetCommunitiesFromApi<CommunityItemViewModel>(searchParams);
                var resultCounts = resultsWepApi.ResultCounts ?? new ApiResultCounts();
                return resultCounts;
            }
            else
            {
                var resultsWepApi = GetHomesFromApi<HomeItemViewModel>(searchParams);
                var resultCounts = resultsWepApi.ResultCounts ?? new ApiResultCounts();
                return resultCounts;
            }
        }

        private CommunityHomeResultsViewModel GetBaseCommunityResultsViewModel(SearchParams searchParams, Market market)
        {
            if (market == null)
                market = _marketService.GetMarket(searchParams.MarketId); //market not active for partner

            var minZoom = RouteParams.MinZoom.Value<int>();
            var model = new CommunityHomeResultsViewModel
            {
                Market = market,
                State = market.StateAbbr,
                RightAdToDisplayIsPosition = UserSession.RightAdToDisplayIsPosition,
                ShowComingSoonLink = (market.TotalComingSoon > 0),
                BrandPartnerName = _partnerService.GetPartner(NhsRoute.BrandPartnerId).PartnerName,
                SortBy = searchParams.SortBy,
                SortOrder = searchParams.SortOrder,
                PlotRadius = RouteParams.PlotRadius.Value<bool>(),
                ShowBasics = RouteParams.ShowBasics.Value<bool>(),
                MinZoom = minZoom > 0 ? minZoom : 2,
                SearchMapArea =
                    searchParams.MinLng > 0 || searchParams.MinLat > 0 || searchParams.MaxLat > 0 ||
                    searchParams.MaxLng > 0,
                SearchMapAreaConfirmed = UserSession.SearchMapAreaConfirmed
            };

            FillViewModelFromWepApi(searchParams, model);
            GetFooterInformation(model, searchParams);
            SetContentManagerViewModel(model, searchParams, market);

            if (string.IsNullOrEmpty(searchParams.City) && string.IsNullOrEmpty(searchParams.PostalCode) && string.IsNullOrEmpty(searchParams.County))
                model.ShowCitySearch = model.Market.Cities.Any(p => p.CityName == searchParams.MarketName);
            else
                model.ShowCitySearch = false;

            if (!string.IsNullOrEmpty(searchParams.CommName) && model.ResultCounts.CommCount == 1)
            {
                if (searchParams.SrpType == SearchResultsPageType.CommunityResults)
                {
                    var comm = model.CommunityResults.FirstOrDefault();
                    if (comm != null)
                    {
                        searchParams.CommId = comm.Id;
                        model.SearchParametersJSon = searchParams.ToJson();
                    }
                }
                else
                {
                    var home = model.HomeResults.FirstOrDefault();
                    if (home != null)
                    {
                        searchParams.CommId = home.CommId;
                        model.SearchParametersJSon = searchParams.ToJson();
                    }
                }
            }
        

            model.CommunityName = searchParams.CommName;
            model.MarketBackgroundImagePath =  string.Empty;
            CreateStaticUrls(model);
            SetMetaTagsForSeo(model);
            UpdateMapData(searchParams, ref model);

            var icon = Configuration.ResourceDomainPublic + "/globalresources14/default/images/icons/map_poi_blue.png";

            model.StaticMapUrl(searchParams, "275x160", icon);
            UserSession.AdBuilderIds.Clear();
            foreach (var builder in _builderService.GetMarketBuilders(NhsRoute.PartnerId, searchParams.MarketId).Where(builder => !UserSession.AdBuilderIds.Contains(builder.BuilderId)))
            {
                model.Globals.AdController.AddBuilderParameter(builder.BuilderId);
                UserSession.AdBuilderIds.Add(builder.BuilderId);
            }

            model.Globals.AdController.AddMarketParameter(searchParams.MarketId);
            model.Globals.AdController.AddPriceParameter(searchParams.PriceLow, searchParams.PriceHigh);
            model.Globals.AdController.AddStateParameter(model.Market.StateAbbr);

            GetQmiOrHotDealsUrl(model);
            FillReturnModalValidation(model);

            if (searchParams.MarketId > 0)
                CookieManager.LastMarketView = searchParams.MarketId.ToString(CultureInfo.InvariantCulture);

            // Set the list hub keys in order to log the events in their server
            model.ListHubProviderId = Configuration.ListHubProviderId;
            model.ListHubTestLogging = Configuration.ListHubTestLogging;
            model.IsMapVisible = searchParams.IsMapVisible;

            return model;
        }

        private BaseCommunityHomeResultsViewModel FillViewModelFromWepApi(SearchParams searchParams, BaseCommunityHomeResultsViewModel model = null)
        {
            if (model == null)
                model = new BaseCommunityHomeResultsViewModel { Market = GetMarket(searchParams.MarketId) };
            
            if (searchParams.PageNumber == 0)
                searchParams.PageNumber = 1;

            model.City = GetLocationCoordinates(ref searchParams, model.Market);
            searchParams.GetLocationCoordinates = false;
            model.PageSize = searchParams.PageSize;
            model.CurrentPage = searchParams.PageNumber;
            model.SrpPageType = searchParams.SrpType;
            List<int> communityIds;
            if (searchParams.SrpType == SearchResultsPageType.CommunityResults)
            {
                var resultsWepApi = GetCommunitiesFromApi<CommunityItemViewModel>(searchParams);
                var results = resultsWepApi.Result ?? new List<CommunityItemViewModel>();
                model.CommunityResults = results;
                model.ResultCounts = resultsWepApi.ResultCounts;
                communityIds = results.Select(c => c.Id).ToList();
            }
            else
            {
                var resultsWepApi = GetHomesFromApi<HomeItemViewModel>(searchParams);
                var results = resultsWepApi.Result ?? new List<HomeItemViewModel>();
                model.HomeResults = results;
                model.ResultCounts = resultsWepApi.ResultCounts;
                communityIds = results.Select(p => p.CommId).ToList();
            }
            
            // Set hot homes and promotions
            BindResults(model, searchParams.SrpType, communityIds, searchParams);
            model.BrandsJSon = model.ResultCounts.Facets.Brands.ToJson();
            model.CommunitiesJSon = model.ResultCounts.Facets.Communities.ToJson();
            model.SchoolDistrictsJSon = model.ResultCounts.Facets.SchoolDistricts.ToJson();
            model.ResultCounts.Facets.Cities.Insert(0, new ApiFacetOption { Key = model.Market.MarketId, Value = @"All areas", State = model.Market.StateAbbr });
            model.CitysJSon = model.ResultCounts.Facets.Cities.ToColumns(3).ToJson();
            model.FindCustomBuilders =  new FindCustomBuilders();
            // Begin Case 81164: Adding Feature Listing to NHS Platform
            _communityService.GetFeaturedListings(model.Market.MarketId, model.FeaturedListingsTop, model.FeaturedListingsBottom, NhsRoute.PartnerId);
            
            // these market and state functions uses cache so dont worry about performance about getting the names for these collections and impact the page times
            model.FeaturedListingsTop.ForEach(c => { c.MarketName = _marketService.GetMarketName(NhsRoute.PartnerId, c.MarketId); c.StateName = _stateService.GetStateName(c.State); });
            model.FeaturedListingsBottom.ForEach(c => { c.MarketName = _marketService.GetMarketName(NhsRoute.PartnerId, c.MarketId); c.StateName = _marketService.GetStateNameForMarket(c.MarketId); });
            var featuredListingsIds = model.FeaturedListingsTop.Select(f => f.BuilderId + " " + f.CommunityId + " " + f.FeaturedListingId).ToList();
            featuredListingsIds.AddRange(model.FeaturedListingsBottom.Select(f => f.BuilderId + " " + f.CommunityId + " " + f.FeaturedListingId).ToList());
            model.FeaturedListingsIdList = featuredListingsIds.ToArray().Join(",");
            
            // End Case 81164: Adding Feature Listing to NHS Platform

            if (searchParams.SrpType == SearchResultsPageType.CommunityResults)
            {
                model.CommunitiesIdList = model.CommunityResults.Where(c => !c.IsBasic.ToType<bool>()).Select(c => c.BuilderId + " " + c.Id).ToArray().Join(",");
                model.BasicCommunitiesIdList = model.CommunityResults.Where(c => c.IsBasic.ToType<bool>()).Select(c => c.BuilderId + " " + c.Id).ToArray().Join(",");
            }
            else
            {
                model.HomesIdList = model.HomeResults.Where(h => !h.IsBl.ToType<bool>() && !h.IsBasic.ToType<bool>()).Select(h => h.HomeId + " " + (h.IsSpec.ToType<bool>() ? "s" : "p")).ToArray().Join(",");
                model.BasicListingsIdList = model.HomeResults.Where(h => h.IsBl.ToType<bool>()).Select(h => h.BuilderId + " " + h.HomeId).ToArray().Join(",");
            }

            model.Globals.SdcMarketId = model.Market.MarketId;
            model.SearchParametersJSon = searchParams.ToJson();
            searchParams.CustomRadiusSelected = false; //Set it to false,  72221 NHS Community Results: wrong  location value
            UserSession.SearchParametersV2 = searchParams;
            return model;
        }

        private void CreateStaticUrls(CommunityHomeResultsViewModel model)
        {
            var @params = NhsRoute.CurrentRoute.Params.Clone();
            var @paramsTabs = NhsRoute.CurrentRoute.Params.Clone();
            var filterParameters = new List<string> { "plotradius", "showbasics", "minzoom", "page" };
            var filterNames = new List<string> { "bedrooms", "pricelow", "pricehigh", "bathrooms", "comingsoon", "hotdeals", "FromPage", "ToPage", "Green", 
            "inventory", "spechomes", "communitystatus", "status"};
            var page = Pages.CommunityResults;
            foreach (var name in filterParameters.Where(x => @params.Any(p => p.Name.ToString().ToLower() == x)))
            {
                @params.Remove(@params.First(p => p.Name.ToString().ToLower() == name));
                @paramsTabs.Remove(@paramsTabs.First(p => p.Name.ToString().ToLower() == name));
            }

            model.PagingUrl = @params.ToUrl(page);

            foreach (var name in filterNames.Where(x => @paramsTabs.Any(p => p.Name.ToString().ToLower() == x || p.Value.ToLower() == x)))
                @paramsTabs.Remove(@paramsTabs.First(p => p.Name.ToString().ToLower() == name || p.Value.ToLower() == name));

            model.TabsUrl = @paramsTabs.ToUrl(page);

            //Creates the Url for the CityNameFilters in the facets, if the current url have a citynamefilter or city the system will removed
            if (@params.Any(p => p.Name.ToString().ToLower() == "citynamefilter" || p.Name.ToString().ToLower() == "city"))
                @params.Remove(@params.First(p => p.Name.ToString().ToLower() == "citynamefilter" || p.Name.ToString().ToLower() == "city"));

            model.CityNameFilterUrl = @params.ToUrl(page);
        }

        private FindCustomBuilders GetFindCustomBuilders(BaseCommunityHomeResultsViewModel model)
        {
            var boylResults = _boylService.GetBoylResults(NhsRoute.PartnerId, model.Market.MarketId,
                                                        model.Market.MarketName, model.Market.StateAbbr).ToList();

            boylResults = boylResults.Where(c => !c.IsBasicCommunity).ToList();

            boylResults.Shuffle();

            var data = new FindCustomBuilders
            {
                Show = true,
                BuilderCount = boylResults.GroupBy(p => p.BrandName).Count(),
                MarketName = model.Market.MarketName,
                MarketId = model.Market.MarketId
            };

            foreach (var boylResult in boylResults.Take(2))
            {
                var image = boylResult.BrandImageThumbnailMedium ?? boylResult.BrandImageThumbnail;
                if (string.IsNullOrEmpty(image))
                    image = null;
                data.FindCustomBuildersBuilders.Add(new FindCustomBuildersBuilders
                {
                    BuildeName = boylResult.BrandName,
                    CommunityImage = boylResult.CommThumbnail(),
                    BuildeImage = image == null ? string.Empty : (boylResult.IsCnh ? Configuration.CNHResourceDomain : Configuration.ResourceDomain) + image
                });
            }

            return data;
        }

        private void BindResults(BaseCommunityHomeResultsViewModel model, SearchResultsPageType srpType, List<int> communityIds, SearchParams searchParams)
        {
            var promotions = searchParams.HotDeals ? _communityService.GetCommunityPromotions(communityIds)
                                                    .Where(w => w.PromoTypeCode != "AGT")
                                                    .ToList()
                                                    : new List<CommunityPromotion>();

            var hotHomes = searchParams.HotDeals ? _communityService.GetCommunityHotDeals(communityIds).ToList() : new List<HotHome>();

            var prevGroupValue = string.Empty;
            var isBasicListingGroup = false;
            var isBasicCommunityGroup = false;
            var isNearbyBasicListingGroup = false;
            var isNearbyBasicCommunityGroup = false;
            var savedListings = new List<int>();

            if (UserSession.UserProfile.IsLoggedIn())
            {
                if (srpType == SearchResultsPageType.CommunityResults)
                    savedListings = UserSession.UserProfile.Planner.SavedCommunities.Select(p => p.ListingId).ToList();
                else
                    savedListings = UserSession.UserProfile.Planner.SavedHomes.Select(p => p.ListingId).ToList();
            }

            if (srpType == SearchResultsPageType.CommunityResults)
            {
                foreach (var communityItem in model.CommunityResults)
                {
                    model.Globals.AdController.AddCityParameter(communityItem.City);
                    model.Globals.AdController.AddZipParameter(communityItem.Zip);

                    communityItem.HotHomes = hotHomes.Where(p => p.CommunityId == communityItem.Id).ToList();
                    communityItem.Promotions = promotions.Where(p => p.CommunityID == communityItem.Id).ToList();
                    communityItem.MarketName = model.Market.MarketName;
                    communityItem.StateName = model.Market.State.StateName;
                    communityItem.IsFavoriteListing = savedListings.Contains(communityItem.Id);
                    if (searchParams.SortBy != SortBy.Price)
                        SetGroupingInfo(srpType, communityItem, searchParams, ref prevGroupValue, ref isBasicListingGroup,ref isBasicCommunityGroup);
                }
            }
            else
            {
                foreach (var homeItem in model.HomeResults)
                {
                    model.Globals.AdController.AddCityParameter(homeItem.City);
                    model.Globals.AdController.AddZipParameter(homeItem.Zip);

                    homeItem.MarketName = model.Market.MarketName;
                    homeItem.StateName = model.Market.State.StateName;
                    homeItem.IsFavoriteListing = savedListings.Contains(homeItem.HomeId);
                    homeItem.HotHomes = hotHomes.Where(p => p.SpecId != null && p.SpecId.ToType<int>() == homeItem.HomeId).ToList();
                    homeItem.Promotions = promotions.Where(p => p.CommunityID == homeItem.CommId).ToList();
                    SetGroupingInfo(srpType, homeItem, searchParams, ref prevGroupValue, ref isBasicListingGroup,ref isBasicCommunityGroup);
                }
            }
        }

        
        private void SetGroupingInfo(SearchResultsPageType srpType, IBaseApiItem result, SearchParams searchParams,
            ref string prevGroupValue, ref bool isBasicListingGroup,
            ref bool isBasicCommunityGroup)
        {

            if (searchParams.SortBy != SortBy.Random)
            {
                switch (searchParams.SortBy)
                {
                    case SortBy.City:
                        if (prevGroupValue != result.City + ", " + result.State)
                        {
                            result.GroupingBarTitle = result.City + ", " + result.State;
                            prevGroupValue = result.City + ", " + result.State;
                        }
                        break;
                    case SortBy.Brand:
                        if (prevGroupValue != result.Brand.Name && !string.IsNullOrEmpty(result.Brand.Name))
                        {
                            result.GroupingBarTitle = result.Brand.Name;
                            prevGroupValue = result.Brand.Name;
                        }
                        break;

                    case SortBy.NumHomes:
                        var matchesGroupValue = result.NumHomes / 50;

                        if (matchesGroupValue * 50 == result.NumHomes)
                        {
                            --matchesGroupValue;
                        }

                        if (prevGroupValue != matchesGroupValue.ToType<string>())
                        {
                            var groupValueLow = matchesGroupValue * 50;
                            var groupValueHigh = groupValueLow + 50;

                            if (result.IsBasic.ToType<bool>())
                                result.GroupingBarTitle = string.Empty;
                            else if (groupValueLow < 0)
                                result.GroupingBarTitle = "0 " + LanguageHelper.Homes;
                            else
                                result.GroupingBarTitle = groupValueLow + 1 + " - " + groupValueHigh + " " + LanguageHelper.Homes;

                            prevGroupValue = matchesGroupValue.ToType<string>();
                        }
                        break;
                    case SortBy.Bed:
                        if (prevGroupValue != result.Br.ToType<string>())
                        {
                            result.GroupingBarTitle = result.Br.ToType<string>() + " " +
                                                      (result.Br == 1 ? LanguageHelper.Bedroom : LanguageHelper.Bedrooms);
                            prevGroupValue = result.Br.ToType<string>();
                        }
                        break;

                }
            }
            else
            {
                if (!string.IsNullOrEmpty(searchParams.City))
                {
                    string cityRadiusGroupValue;
                    if (string.IsNullOrEmpty(searchParams.City) ||
                        result.City.ToLower() == searchParams.City.ToLowerCase())
                    {
                        cityRadiusGroupValue = result.City + ", " + result.State;
                    }
                    else
                    {
                        cityRadiusGroupValue = string.Format(LanguageHelper.Nearby,
                            srpType == SearchResultsPageType.CommunityResults
                                ? LanguageHelper.Communities
                                : LanguageHelper.Homes);
                    }


                    if (prevGroupValue != cityRadiusGroupValue && result.IsBasic == 0)
                    {
                        result.GroupingBarTitle = cityRadiusGroupValue;
                        prevGroupValue = cityRadiusGroupValue;
                    }
                }
                else if (!string.IsNullOrEmpty(searchParams.County))
                {
                    string countyRadiusGroupValue;
                    if (result.County.ToLower() == searchParams.County.ToLowerCase())
                    {
                        countyRadiusGroupValue = result.County + " " + LanguageHelper.County;
                    }
                    else
                    {
                        countyRadiusGroupValue = string.Format(LanguageHelper.Nearby,
                            srpType == SearchResultsPageType.CommunityResults
                                ? LanguageHelper.Communities
                                : LanguageHelper.Homes);
                    }

                    if (prevGroupValue != countyRadiusGroupValue && result.IsBasic == 0)
                    {
                        result.GroupingBarTitle = countyRadiusGroupValue;
                        prevGroupValue = countyRadiusGroupValue;
                    }
                }
                else if (!string.IsNullOrEmpty(searchParams.PostalCode))
                {
                    string zipRadiusGroupValue;

                    if (result.Zip.Trim().ToLower() == searchParams.PostalCode.ToLowerCase().Trim())
                    {
                        zipRadiusGroupValue = LanguageHelper.Zip + " - " + result.Zip.Trim();
                    }
                    else
                    {
                        zipRadiusGroupValue = string.Format(LanguageHelper.Nearby,
                            srpType == SearchResultsPageType.CommunityResults
                                ? LanguageHelper.Communities
                                : LanguageHelper.Homes);
                    }

                    if (prevGroupValue != zipRadiusGroupValue && result.IsBasic == 0)
                    {
                        result.GroupingBarTitle = zipRadiusGroupValue;
                        prevGroupValue = zipRadiusGroupValue;
                    }
                }


                if ((result.IsBasic.ToType<bool>() || result.IsBl.ToType<bool>()) && !isBasicListingGroup)
                {
                    UserSession.IsBasicListingGroup = result.ShowBasicListingIndicator = isBasicListingGroup = true;
                }

                if (result.IsBasic.ToType<bool>() && !isBasicCommunityGroup)
                {
                    UserSession.IsBasicCommunityGroup = result.ShowBasicListingIndicator = isBasicCommunityGroup = true;
                }
            }
        }

        private Market GetMarket(int marketId)
        {
            return _marketService.GetMarket(marketId);
        }

        private SearchParams GetDefaultSearchParams(int marketId)
        {
            var searchParams = UserSession.SearchParametersV2;

            if (searchParams != null)
            {
                searchParams.IsMapVisible = searchParams.IsMapVisible || NhsLinkParams.MapSearch.GetValue<bool>();

                if (searchParams.CustomRadiusSelected)
                {
                    searchParams.CustomRadiusSelected = false;
                    searchParams.GetRadius = false;
                    FillBuildersResultsParam(searchParams);
                    return searchParams;
                }
            }

            if (searchParams == null || marketId != searchParams.MarketId)
            {
                searchParams = new SearchParams();
                searchParams.Init(true);
                searchParams.MarketId = marketId;

                FillBuildersResultsParam(searchParams);
                searchParams.IsMapVisible = searchParams.IsMapVisible || NhsLinkParams.MapSearch.GetValue<bool>();
                return searchParams;
            }

            //If the user came back from a detail page, we have to persist all the facets
            //if (isReturn)
            //    return searchParams;

            if (marketId == searchParams.MarketId)
            {
                var city = searchParams.City ?? string.Empty;
                var postalCode = searchParams.PostalCode ?? string.Empty;
                var county = searchParams.County ?? string.Empty;
                var commName = searchParams.CommName ?? string.Empty;

                searchParams.PostalCode = string.Empty;
                searchParams.City = string.Empty;
                searchParams.County = string.Empty;
                searchParams.CommName = string.Empty;
                searchParams.SetFromRouteParam(NhsRoute.CurrentRoute.Params);

                if (city != searchParams.City || postalCode != searchParams.PostalCode ||
                    county != searchParams.County || commName != searchParams.CommName)
                {
                    searchParams.SortFirstBy = string.Empty;
                    searchParams.SortBy = SortBy.Random;
                    searchParams.SortOrder = false;
                    searchParams.PageNumber = 1;
                    searchParams.PageSize = 10;
                    searchParams.CommId = 0;
                    searchParams.GetLocationCoordinates = true;
                    searchParams.GetRadius = true;
                    searchParams.MinLng = 0;
                    searchParams.MinLat = 0;
                    searchParams.MaxLat = 0;
                    searchParams.MaxLng = 0;
                }
            }

            if (string.IsNullOrEmpty(UserSession.PreviousPage))
            {
                var communityResultsPages = new List<string>
                {
                    Pages.CommunityResultsv2.ToLower(),
                    Pages.CommunityResults.ToLower(),
                    Pages.Comunidades.ToLower()
                };
                searchParams.SrpType = communityResultsPages.Contains(NhsRoute.CurrentRoute.Function.ToLower())
                    ? SearchResultsPageType.CommunityResults
                    : SearchResultsPageType.HomeResults;
            }
            else
            {
                switch (UserSession.PreviousPage)
                {
                    case Pages.HomeDetail:
                        searchParams.SrpType = SearchResultsPageType.HomeResults;
                        break;
                    case Pages.CommunityDetail:
                        searchParams.SrpType = SearchResultsPageType.CommunityResults;
                        break;
                    default:
                        searchParams.SrpType = SearchResultsPageType.CommunityResults;
                        break;
                }

                UserSession.PreviousPage = string.Empty;
            }



            FillBuildersResultsParam(searchParams);

            return searchParams;
        }

        private void FillBuildersResultsParam(SearchParams searchParams)
        {
            searchParams.IsBuilderTabSearch = RouteParams.HomeBuilders.Value<bool>();
        }

        private SearchParams GetSearchParamsForCounts(SearchParams searchParameters, string searchText, string searchType)
        {
            if (!string.IsNullOrEmpty(searchText) && !string.IsNullOrEmpty(searchType))
            {
                var type = searchType.ToType<int>();
                if (searchText.EndsWith("area", StringComparison.InvariantCultureIgnoreCase))
                {
                    searchParameters.WebApiSearchType = WebApiSearchType.Exact;
                    searchText =
                        searchText.Substring(0,
                            searchText.LastIndexOf("area", StringComparison.InvariantCultureIgnoreCase)).Trim();
                }
                var locations = _partnerService.GetTypeAheadSuggestions(NhsRoute.PartnerId, searchText, false, true);
                var location = locations.FirstOrDefault(l => l.Type == type);
                if (location != null)
                {
                    return GetSearchParamsForCounts(searchParameters, location);
                }
            }

            searchParameters.CountsOnly = true;
            return searchParameters;
        }

        private static string GetSearchText(SearchParams searchParameters)
        {
            var text = string.Empty;

            if (searchParameters.MarketId > 0 && !String.IsNullOrEmpty(searchParameters.MarketName))
            {
                text = searchParameters.MarketName + ", " + searchParameters.State + " Area";
            }

            if (!String.IsNullOrEmpty(searchParameters.City))
            {
                text = searchParameters.City + ", " + searchParameters.State;
            }

            if (!String.IsNullOrEmpty(searchParameters.County))
            {
                text = searchParameters.County + " County" + ", " + searchParameters.State;
            }

            if (!String.IsNullOrEmpty(searchParameters.PostalCode) && searchParameters.PostalCode.ToType<int>() > 0)
            {
                text = searchParameters.PostalCode + ", " + searchParameters.State;
            }

            return text;
        }

        /// <summary>
        /// City/County/Zip Get GeoLocation Coordinates
        /// </summary>
        /// <param name="searchParams"></param>
        /// <param name="market"></param>
        /// <returns></returns>
        private string GetLocationCoordinates(ref SearchParams searchParams, Market market)
        {
            var cityName = market != null ? market.MarketName : string.Empty;
            // CITY
            if (!string.IsNullOrEmpty(searchParams.City))
            {
                if (searchParams.GetLocationCoordinates)
                {
                    var city = _mapService.GeoCodeCityByMarket(searchParams.City, market.MarketId,
                        market.StateAbbr);
                    if (city != null)
                        SetGetLocationCoordinates(ref searchParams, city.Radius, city.Latitude, city.Longitude,
                            city.State, searchParams.City, SortBy.Random);
                }

                cityName = searchParams.City;
            }
            // ZIP
            else if (!string.IsNullOrEmpty(searchParams.PostalCode))
            {
                var zipCode = _mapService.GeoCodePostalCode(searchParams.PostalCode);
                if (zipCode != null && searchParams.GetLocationCoordinates)
                {
                    SetGetLocationCoordinates(ref searchParams, zipCode.Radius, zipCode.Latitude, zipCode.Longitude, zipCode.State, searchParams.PostalCode, SortBy.Random);

                    if (!string.IsNullOrEmpty(zipCode.City))
                        cityName = zipCode.City;
                }
            }
            // COUNTY
            else if (!string.IsNullOrEmpty(searchParams.County) && searchParams.GetLocationCoordinates)
            {
                var county = _mapService.GeoCodeCountyByMarket(searchParams.County, searchParams.MarketId);
                if (county != null)
                    SetGetLocationCoordinates(ref searchParams, county.Radius, county.Latitude, county.Longitude, county.State, searchParams.County, SortBy.Random);

            } // MARKET
            else if (searchParams.MarketId != 0 && searchParams.GetLocationCoordinates)
            {
                searchParams.Radius = 0;
                searchParams.WebApiSearchType = WebApiSearchType.Exact;
                //Tickets 46973 & 49416: Set the last radius used in the radius facet, if is null set 0
                if (market != null)
                {
                    searchParams.State = market.State.StateAbbr;
                    if (market.LatLong != null)
                    {
                        searchParams.OriginLat = market.LatLong.Latitude;
                        searchParams.OriginLng = market.LatLong.Longitude;
                    }
                    else
                    {
                        searchParams.OriginLat = Convert.ToDouble(market.Latitude);
                        searchParams.OriginLng = Convert.ToDouble(market.Longitude);
                    }
                }
            }
            searchParams.MarketName = market != null ? market.MarketName : string.Empty;

            return cityName;
        }

        private void SetGetLocationCoordinates(ref SearchParams searchParams, int radius, double latitude, double longitude, string state, string sortFirstBy, SortBy sortBy)
        {
            if (searchParams.GetRadius)
            {
                searchParams.WebApiSearchType = WebApiSearchType.Radius;
                searchParams.Radius = radius;
            }

            searchParams.OriginLat = latitude;
            searchParams.OriginLng = longitude;
            searchParams.State = state;
            searchParams.SortFirstBy = sortFirstBy;
            searchParams.SortBy = sortBy;
            searchParams.SortOrder = false;
        }

        private WebApiCommonResultModel<List<T>> GetCommunitiesFromApi<T>(SearchParams searchParams) where T : new()
        {
            if (searchParams.PartnerId == 0)
                searchParams.PartnerId = NhsRoute.PartnerId;

            var data = _apiService.GetResultsWebApi<T>(searchParams, SearchResultsPageType.CommunityResults);
            return data;
        }

        private WebApiCommonResultModel<List<T>> GetHomesFromApi<T>(SearchParams searchParams) where T : new()
        {
            if (searchParams.PartnerId == 0)
                searchParams.PartnerId = NhsRoute.PartnerId;

            var data = _apiService.GetResultsWebApi<T>(searchParams, SearchResultsPageType.HomeResults);
            return data;
        }

        private void UpdateMapData(SearchParams searchParams, ref CommunityHomeResultsViewModel model)
        {
            if (model.Map == null)
                model.Map = new MapData();

            model.Map.ZoomLevel = 8;

            if (!string.IsNullOrEmpty(searchParams.CommName) &&
                (model.CommunityResults.Any() || model.HomeResults.Any()))
            {
                if (model.CommunityResults.Count() == 1)
                {
                    var comm = model.CommunityResults.First();
                    model.Map.CenterLat = comm.Lat.ToType<double>();
                    model.Map.CenterLng = comm.Lng.ToType<double>();
                    model.Map.ZoomLevel = 17;
                }
                if (!model.CommunityResults.Any() && model.ResultCounts.CommCount == 1)
                {
                    var home = model.HomeResults.First();
                    model.Map.CenterLat = home.Lat.ToType<double>();
                    model.Map.CenterLng = home.Lng.ToType<double>();
                    model.Map.ZoomLevel = 17;
                }

            }
            else
            {
                model.Map.CenterLat = searchParams.OriginLat;
                model.Map.CenterLng = searchParams.OriginLng;

                if (!string.IsNullOrEmpty(searchParams.County))
                {
                    model.Map.ZoomLevel = 9;
                }
                else if (!string.IsNullOrEmpty(searchParams.City) || !string.IsNullOrEmpty(searchParams.PostalCode))
                {
                    model.Map.ZoomLevel = 10;
                }
                else
                {
                    switch (searchParams.Radius)
                    {
                        case 25:
                            model.Map.ZoomLevel = 9;
                            break;
                        case 15:
                        case 10:
                            model.Map.ZoomLevel = 10;
                            break;
                        case 5:
                            model.Map.ZoomLevel = 11;
                            break;
                        case 3:
                            model.Map.ZoomLevel = 12;
                            break;
                    }
                }
            }
        }

        private void GetQmiOrHotDealsUrl(CommunityHomeResultsViewModel model)
        {
            model.HotDealsUrl = model.QuickMoveInUrl = model.TabsUrl + (model.TabsUrl.Contains("?") ? "&" : "?");
            if (!model.TabsUrl.Contains(UrlConst.HotDeals))
                model.HotDealsUrl += UrlConst.HotDeals + "=true";

            if (!model.TabsUrl.Contains(UrlConst.SpecHomes) && !model.TabsUrl.Contains(UrlConst.Inventory))
                model.QuickMoveInUrl += UrlConst.Inventory + "=true";
        }

        private void GetFooterInformation(CommunityHomeResultsViewModel model, SearchParams searchParams)
        {
            //Filed the Information in to the model
            GetCitiesAndZipCodeViewModel(ref model, searchParams);
            SeoCommunitiesContentViewModel(ref model);
        }

        private void FillReturnModalValidation(CommunityHomeResultsViewModel model)
        {
            if (NhsLinkParams.NewSession.GetValue<bool>())
                GenerateNewSession();

            var isNewSescion = (string.IsNullOrWhiteSpace(CookieManager.PreviousSessionId) == false && CookieManager.PreviousSessionId != UserSession.SessionId);
            var showReturnUserModal = NhsLinkParams.ShowReturnUserModal.GetValue<bool>();
            model.ShowReturnUserModal = showReturnUserModal;

            if (showReturnUserModal || (isNewSescion == false)) return;

            int cMarketId;
            var currentMarket = model.Market.MarketId > 0 ? model.Market.MarketId : RouteParams.Market.Value<int>();
            var cookieCommId = GetCommunityIdFromCoockie(out cMarketId);
            var hasSameMarket = currentMarket == cMarketId;

            if (cookieCommId <= 0 || cMarketId <= 0 || hasSameMarket == false) return;

            var lead = LeadUtil.GetLeadInfoFromSession();
            var rcoms = _communityService.GetRecommendedCommunities(lead.LeadUserInfo.Email, NhsRoute.PartnerId,
                cookieCommId, 0, 0, 0);
            var hasRecomented = rcoms.Any();

            var alreadyRequestBrosurePreviosComm = _communityService.AlreadyRequestBrosure
                (
                    NhsRoute.PartnerId,
                    currentMarket,
                    cookieCommId,
                    lead.LeadUserInfo.Email
                );

            var wasAlreadyShow = UserSession.PopUpRetUserBroshure.ToType<bool>();

            var isValidCommId = CookieManager.LastCommunityView.ToType<int>() > 0;

            model.ShowReturnUserModal = (isValidCommId && (wasAlreadyShow == false)
                                           && (alreadyRequestBrosurePreviosComm == false) && hasRecomented);

            if (NhsLinkParams.ShowRumTraceCookie.GetValue<bool>())
            {
                WriteRumTraceDebugData(isNewSescion, hasSameMarket, lead, hasRecomented, alreadyRequestBrosurePreviosComm, currentMarket, wasAlreadyShow, isValidCommId);
            }
        }
        /// <summary>
        /// add this parameter in the url to trace the values for the RUM ?ShowRumTraceCookie=true
        /// </summary>
        /// <param name="isNewSescion"></param>
        /// <param name="hasSameMarket"></param>
        /// <param name="lead"></param>
        /// <param name="hasRecomented"></param>
        /// <param name="alreadyRequestBrosurePreviosComm"></param>
        /// <param name="currentMarket"></param>
        /// <param name="wasAlreadyShow"></param>
        /// <param name="isValidCommId"></param>
        private void WriteRumTraceDebugData(bool isNewSescion, bool hasSameMarket, LeadInfo lead, bool hasRecomented,
            bool alreadyRequestBrosurePreviosComm, int currentMarket, bool wasAlreadyShow, bool isValidCommId)
        {
            //*******************************************************
            var strDebug = new StringBuilder();
            strDebug.AppendFormat("isNewSescion : {0} :: ", isNewSescion);
            strDebug.AppendFormat("hasSameMarket : {0} :: ", hasSameMarket);
            strDebug.AppendFormat("lead.LeadUserInfo.Email : {0} :: ", lead.LeadUserInfo.Email);
            strDebug.AppendFormat("hasRecomented : {0} :: ", hasRecomented);
            strDebug.AppendFormat("alreadyRequestBrosurePreviosComm : {0} :: ", alreadyRequestBrosurePreviosComm);
            strDebug.AppendFormat("currentMarket : {0} :: ", currentMarket);
            strDebug.AppendFormat("wasAlreadyShow : {0} :: ", wasAlreadyShow);
            strDebug.AppendFormat("isValidCommId : {0} :: ", isValidCommId);
            strDebug.AppendFormat("ShowReturnUserModal : {0} :: ", (isValidCommId && (wasAlreadyShow == false)
                                                                    && (alreadyRequestBrosurePreviosComm == false) &&
                                                                    hasRecomented));
            strDebug.AppendFormat("SERVER NAME : {0} :: ", HttpContext.Server.MachineName);
            CookieManager.TraceDebugCookie = strDebug.ToString();
            //*******************************************************
        }

        #endregion
    }
}
