﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Enums;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Library.Constants;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Web.UI.AppCode.Helper;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class SiteHelpController : ApplicationController
    {
        private readonly IPathMapper _pathMapper;
        private readonly IPartnerService _partnerService;

        public SiteHelpController(IPathMapper pathMapper, IPartnerService partnerService)
            : base(pathMapper)
        {
            _pathMapper = pathMapper;
            _partnerService = partnerService;
        }

        public virtual ActionResult ListYourHomes()
        {
            var model = GetContactBdxViewModel(new SiteHelpViewModels.ContactBdxViewModel());
            base.AddCanonicalTag(model);
            base.SetupAdParameters(model);
          
// ReSharper disable once Mvc.ViewNotResolved
            return View(model);          
        }

        public virtual ActionResult AboutUs()
        {
            return GetView(Pages.AboutUs, false);
        }

        public virtual ActionResult PrivacyPolicy()
        {
            return GetView(Pages.PrivacyPolicy);
        }

        public virtual ActionResult SupportedBrowsers()
        {
            return GetView(Pages.SupportedBrowsers);
        }

        public virtual ActionResult TermsOfUse()
        {
            return GetView(Pages.TermsOfUse);
        }

        public virtual ActionResult Partners()
        {
            return GetView(Pages.Partners, false);
        }

        public virtual ActionResult SiteHelp()
        {
            return GetView(Pages.SiteHelp);
        }

        public virtual ActionResult ContactUs()
        {
            var model = new SiteHelpViewModels.ContactUsViewModel();
            this.AddMetaTags(Pages.ContactUs, "noindex", true, model);
            model.ContactUsEmail = _partnerService.GetPartner(NhsRoute.PartnerId).ContactUsEmail;
            if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
                base.AddCanonicalTag(model);
            base.SetupAdParameters(model);
            
// ReSharper disable once Mvc.ViewNotResolved
            return View(model);            
        }

        [HttpPost]
        public virtual ActionResult ContactBdx(SiteHelpViewModels.ContactBdxViewModel model)
        {
            if (string.IsNullOrEmpty(model.Email) && !StringHelper.IsValidEmail(model.Email))
                ModelState.AddModelError("Email", LanguageHelper.MSG_ACCOUNT_NO_EMAIL);

            if (string.IsNullOrEmpty(model.FirstName))
                ModelState.AddModelError("FirstName", LanguageHelper.MSG_ACCOUNT_NO_FIRST);

            if (string.IsNullOrEmpty(model.LastName))
                ModelState.AddModelError("LastName", LanguageHelper.MSG_ACCOUNT_NO_LAST);


            model = GetContactBdxViewModel(model);

            if (ModelState.IsValid)
            {
                model.ShowSuccessMessage = true;
                this.SendEmailToCampaignManager(model);
            }
            
            return PartialView(NhsMvc.Default.Views.SiteHelp.ContactBdxForm, model);
        }

        #region Private Methods

        private void SendEmailToCampaignManager(SiteHelpViewModels.ContactBdxViewModel model)
        {
            var emailMessage = new StringBuilder();
            emailMessage.Append("<html><body><br/>");
            emailMessage.Append("First Name: " + model.FirstName);
            emailMessage.Append("<br/>");
            emailMessage.Append("Last Name: " + model.LastName);
            emailMessage.Append("<br/>");
            emailMessage.Append("Company: " + model.Company);
            emailMessage.Append("<br/>");
            emailMessage.Append("Title: " + model.Title);
            emailMessage.Append("<br/>");
            emailMessage.Append("Email: " + model.Email);
            emailMessage.Append("<br/>");
            emailMessage.Append("Phone: " + model.Phone);
            emailMessage.Append("<br/>");
            emailMessage.Append("City: " + model.City);
            emailMessage.Append("<br/>");
            emailMessage.Append("State: " + model.State);
            emailMessage.Append("<br/>");
            emailMessage.Append("I am interested in: " + model.Product);
            emailMessage.Append("<br/>");
            emailMessage.Append("Comments: " + model.Comments);
            emailMessage.Append("<br/>");
            emailMessage.Append("</body></html>");
            MailHelper.Send(Configuration.BdxContactFormEmail, "support@newhomesource.com", "Contact BDX request submitted via 'List Your Homes'", emailMessage.ToString());
        }

        private SiteHelpViewModels.ContactBdxViewModel GetContactBdxViewModel(SiteHelpViewModels.ContactBdxViewModel model)
        {
            model.Products = new SelectList(LanguageHelper.Productos.Split(','));
            return model;
        }

        private void AddMetaTags(string pageName, string robotDescription, bool addRobotMeta, BaseViewModel viewModel)
        {
            var metaRegistrar = new MetaRegistrar(_pathMapper);

            var metas = new List<MetaTag>();
            if (!string.IsNullOrEmpty(pageName))
                metas = metaRegistrar.GetMetaTagsForSeoByPageName(pageName);

            if (addRobotMeta && !string.IsNullOrEmpty(robotDescription))
            {
                var robotMeta = metaRegistrar.GetRobotsMeta(robotDescription);
                metas.RemoveAll(m => m.Name == MetaName.Robots);
                metas.Add((from r in robotMeta select r).FirstOrDefault());
            }

            OverrideDefaultMeta(viewModel, metas);
        }

        private ActionResult GetView(string pages, bool addRobotMeta = true)
        {
            var model = GetViewModel();
            
            AddMetaTags(pages, "noindex", addRobotMeta, model);
            base.SetupAdParameters(model);
            // ReSharper disable Asp.NotResolved
            return View(model);
            // ReSharper restore Asp.NotResolved
        }

        private BaseViewModel GetViewModel()
        {
            var model = new BaseViewModel();
            base.AddCanonicalTag(model);
            return model;
        }

        #endregion
    }
}
