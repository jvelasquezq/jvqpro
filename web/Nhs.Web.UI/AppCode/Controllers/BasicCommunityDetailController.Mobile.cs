﻿using System.Linq;
using System.Web.Mvc;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects.Constants;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Lib;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
	public partial class BasicCommunityDetailController
	{
        public virtual ActionResult GetNearbyCommunities(int communityId, int count)
        {
            var model = _communityService.GetNearbyCommunities(NhsRoute.PartnerId, communityId, count);
            return PartialView(NhsMvc.Default.ViewsMobile.BasicCommunityDetail.NearByCommunities, model);
        }

        public void GetBasicCommunityDetailInfoMobile(BasicCommunityDetailViewModel model, Community community)
	    {
            model.CommunityName = community.CommunityName;
            model.BuilderName = community.Builder.BuilderName;
            model.MarketName = community.Market.MarketName;
            model.BrandId = community.BrandId;
            model.BrandName = community.Brand.BrandName ?? string.Empty;
            model.MarketId = community.MarketId;
            model.CommunityId = community.CommunityId;
            model.BuilderId = community.BuilderId;
            model.Latitude = (double)community.Latitude;
            model.Longitude = (double)community.Longitude;
            model.CommunityCity = community.City;
            model.ZipCode = community.PostalCode;
            model.State = community.State.StateName;
            model.StateAbbr = community.State.StateAbbr;
            model.PhoneNumber = community.PhoneNumber;
            model.IsCommunityMultiFamily = community.IsCondo.ToType<bool>() || community.IsTownHome.ToType<bool>();
            //Get Results from API
            var results = GetCommunityHomeResults(community);
            var sortedHomes = new SortHomeActions().SortOptions[SortOrder.Status](results);
            var resultsView = sortedHomes.Where(p => p.IsSpec == 0).ToList();

            model.AllNewHomesCount = resultsView.Count();
            model.BcType = community.BCType;
            model.HomeResultsApiV2 = resultsView.GroupBy(p => p.Status);
	    }
	}
}