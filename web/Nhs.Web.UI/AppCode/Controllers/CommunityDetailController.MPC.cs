﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Proxy;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects;
using Nhs.Search.Objects.Constants;
using Nhs.Utility.Common;
using Nhs.Utility.Data.Paging;
using Nhs.Web.UI.AppCode.Extensions;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.Lib;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Web.UI.AppCode.ViewModels.NewMediaViewer;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class CommunityDetailController
    {
        public virtual ActionResult ShowMPC(int? communityId)
        {
            
            if (communityId == null)
                communityId = RouteParams.Community.Value<int>() == 0 ? RouteParams.CommunityId.Value<int>() : RouteParams.Community.Value<int>();

            // Gets models
            var page = 1;
            if (!string.IsNullOrEmpty(RouteParams.Page.Value()) && RouteParams.Page.Value<int>() > 0)
                page = RouteParams.Page.Value<int>() - 1;

            Community community = null;
            var searchParams = UserSession.PropertySearchParameters.Clone() as NhsPropertySearchParams;
            var addCanonical = AddCanonical(communityId.ToType<int>());
            if (_communityService.IsCommunityActiveOnParentBrandPartner(communityId.ToType<int>(), NhsRoute.PartnerId))
                community = _communityService.GetCommunity(communityId.ToType<int>());

            // Add an extra parameter to identify from SEO inactive data
            if (community == null && string.IsNullOrEmpty(RouteParams.Msg.Value<string>()))
                return RedirectPermanent(GetInactiveUrl());

            var refer301Url = base.GetReferRedirectUrl(community);

            if (!string.IsNullOrEmpty(refer301Url))
                return RedirectPermanent(refer301Url);

            if (UserSession.PropertySearchParameters.MarketId != 0 & searchParams != null)
            {
                searchParams.CommunityId = communityId.ToType<int>();
                searchParams.SortOrder = SortOrder.Status;
            }
            else
                searchParams = GetSearchParams(communityId.ToType<int>(), SortOrder.Status, 0, "N");

            var pageDetail = RedirectionHelper.GetDetailPageRedirectName(true);
            if (!string.IsNullOrEmpty(pageDetail) && community != null)
            {
                return RedirectPermanent(pageDetail, community.ToCommunityDetail());
            }

            if (community != null && !string.IsNullOrEmpty(community.PartnerMask))
            {
                var partnerMaskIndex = _partnerService.GetPartnerMaskIndex(NhsRoute.PartnerId);
                var partnerSortValue = community.PartnerMask.Substring(partnerMaskIndex - 1, 1);

                if ("F" == partnerSortValue && NhsRoute.IsBrandPartnerNhsPro)
                {
                    var @params = new List<RouteParam> { new RouteParam(RouteParams.Community, community.CommunityId) };
                    var url = @params.ToUrl(Pages.BasicCommunity) + "/" + string.Format("{0}-{1}-{2}-{3}", community.CommunityName, community.City,
                                            community.State.StateAbbr, community.PostalCode).Trim().Replace(" ", "-").Replace("--", "-").Replace("'", "");
                    return RedirectPermanent(url);
                }

                if ((RouteParams.ForcePopupPlayer.Value<bool>() || RouteParams.AddToProfile.Value<bool>()) && UserSession.UserProfile.IsLoggedIn())
                    SaveCommunityToPlanner(community.CommunityId, community.BuilderId, true);
            }

            var viewModel = (community == null) ? CommunityDetailExtensions.GetInactivePropertyViewModel(true, _stateService, _lookupService) as CommunityDetailViewModel
                                               : (NhsRoute.ShowMobileSite ? GetCommunityDetailModelMobile(community, !addCanonical)
                                                                         : GetCommunityDetailModel(community, searchParams, page, !addCanonical));

            viewModel.IsitInactiveData = community == null;

            if (community == null || (addCanonical && string.IsNullOrEmpty(viewModel.Globals.CanonicalLink))) // if apply the rules to addCanonical and it wasnt 
                AddCanonicalWithIncludeParams(viewModel, new List<string> { "community", "builder" }, (community != null && community.OwnerPartnerId == NhsRoute.PartnerId && NhsRoute.BrandPartnerId != NhsRoute.PartnerId));


            if (!string.IsNullOrWhiteSpace(viewModel.BcType) && viewModel.BcType.ToLower() != BuilderCommunityType.ComingSoon.ToLower())
            {
                CookieManager.LastCommunityView = communityId.Value.ToString(CultureInfo.InvariantCulture);
                CookieManager.LastMarketView = viewModel.MarketId.ToString(CultureInfo.InvariantCulture);
                CookieManager.PreviousSessionId = UserSession.SessionId;
            }

            UserSession.PreviousPage = Pages.CommunityDetail;
            UserSession.PersonalCookie.MarketId = viewModel.MarketId;
            UserSession.PersonalCookie.State = viewModel.State;

            // ReSharper disable Asp.NotResolved
            return View(viewModel);
            // ReSharper restore Asp.NotResolved
        }

    }
}   