﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Helpers;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects.Constants;
using Nhs.Utility.Common;
using Nhs.Utility.Html;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Web.UI.AppCode.ViewModels.CommunityDetailMobile;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class HomeDetailController
    {
        public virtual ActionResult GetHomeImages(int planid, int specid, bool includeVideo)
        {
            IPlan plan = null;
            ISpec spec = null;
            var virtualTourUrl = "";
            var floorPlanViewerUrl = "";
            if (planid > 0)
            {
                plan = _listingService.GetPlan(planid);
                virtualTourUrl = plan.VirtualTourUrl;
                floorPlanViewerUrl = _listingService.GetFloorPlanViewerUrl(plan, null);
            }
            else if (specid > 0)
            {
                spec = _listingService.GetSpec(specid);
                virtualTourUrl = spec.VirtualTourUrl;
                plan = spec.Plan;
                floorPlanViewerUrl = _listingService.GetFloorPlanViewerUrl(null, spec);
            }

            var model = GetMediaPlayerObjectsList(plan, spec, virtualTourUrl, floorPlanViewerUrl);
            return PartialView(NhsMvc.Default.ViewsMobile.CommunityDetail.Gallery, model);
        }

        public virtual ActionResult GetFloorPlanGallery(int planid, int specid, bool useHub)
        {
            _listingService.UseHub = useHub;
            IPlan plan = null;
            ISpec spec = null;
            var model = new FloorPlanViewModel();
            if (planid > 0)
            {
                model.PlanId = planid;
                plan = _listingService.GetPlan(planid);
                model.PlanName = plan.PlanName;
            }
            else if (specid > 0)
            {
                model.SpecId = specid;
                model.IsSpec = true;
                spec = _listingService.GetSpec(specid);
                if (!useHub)
                    plan = spec.Plan;
                else
                    plan = spec.HubPlan ?? _listingService.GetPlan(spec.PlanId);
                model.PlanName = plan.PlanName;
            }
          
            if (planid > 0 || specid > 0)
                model.FloorPlans = _listingService.GetFloorPlanImagesForSpec(plan, spec);

            model.IsPreviewMode = useHub;
            var view = NhsRoute.ShowMobileSite ? NhsMvc.Default.ViewsMobile.HomeDetail.GaleryFloorPlan : NhsMvc.Default.Views.HomeDetail.FloorPlans;
            return PartialView(view, model);
        }

        [HttpPost]
        public virtual JsonResult ShowRoute(int communityId, int builderId, int planId, int specId)
        {
            var isPlan = specId == 0;
            LogHomeHeaderClickAction(communityId, builderId, planId, specId, isPlan, LogImpressionConst.MobileHomeDetailShowRoute);
            return new JsonResult { Data = "Click has been logged!" };
        }

        [HttpPost]
        public virtual JsonResult ShareByEmail(int communityId, int builderId, int planId, int specId)
        {
            var isPlan = specId == 0;
            LogHomeHeaderClickAction(communityId, builderId, planId, specId, isPlan, LogImpressionConst.MobileHomedatilDetailShareByEmail);
            return new JsonResult { Data = "Click has been logged!" };
        }

        [HttpPost]
        public virtual JsonResult SocialMediaShare(int communityId, int builderId, int planId, int specId)
        {
            var isPlan = specId == 0;
            LogHomeHeaderClickAction(communityId, builderId, planId, specId, isPlan, LogImpressionConst.MobileHomedatilDetailSocialMediaShare);
            return new JsonResult { Data = "Click has been logged!" };
        }

        [HttpPost]
        public virtual JsonResult LogBuilderShowCase(int communityId, int builderId, int planId, int specId)
        {
            var isPlan = specId == 0;
            LogHomeHeaderClickAction(communityId, builderId, planId, specId, isPlan, LogImpressionConst.BuilderWebsiteLink);
            return new JsonResult { Data = "Click has been logged!" };
        }

        private HomeDetailViewModel GetViewModelMobile(IPlan plan, ISpec spec, bool addNoIndexTag,
            Community currentCommunity)
        {
            var viewModel = new HomeDetailViewModel();
            viewModel.Plan = plan;
            viewModel.IsPlan = spec == null;
            int bedRooms;
            int bathRooms;
            decimal garage;
            int sqFt;
            int halfBaths;
            int stories;
            ListingType listingType;
            //We need this for SEO
            viewModel.PropertyMediaLinks = new List<MediaPlayerObject>();
            var listingServiceHelper = new ListingServiceHelper {UseHub = false};
            if (viewModel.IsPlan)
            {
                listingType = ListingType.Plan;
                viewModel.PropertyId = plan.PlanId;
                viewModel.PlanId = plan.PlanId;
                viewModel.HomeTitle = plan.PlanName;
                bedRooms = plan.Bedrooms.ToType<int>();
                bathRooms = plan.Bathrooms.ToType<int>();
                garage = plan.Garages.ToType<decimal>();
                viewModel.HomeDescription = string.IsNullOrEmpty(plan.Description)
                    ? listingServiceHelper.GetAutoDescriptionForHome(plan, null)
                    : ParseHTML.StripHTML(plan.Description).Replace("\r", string.Empty).Replace("\n", string.Empty);
                viewModel.HomeStatus = (int) HomeStatusType.ReadyToBuild;
                halfBaths = plan.HalfBaths ?? 0;
                sqFt = plan.SqFt ?? 0;
                stories = plan.Stories.ToType<int>();
            }
            else
            {
                listingType = ListingType.Spec;
                viewModel.PropertyId = spec.SpecId;
                viewModel.SpecId = spec.SpecId;
                viewModel.HomeTitle = string.Format("{0} ({1})", (spec).Address1, spec.PlanName);
                bedRooms = spec.Bedrooms.ToType<int>();
                bathRooms = spec.Bathrooms.ToType<int>();
                garage = spec.Garages.ToType<decimal>();
                viewModel.HomeDescription = string.IsNullOrEmpty(spec.Description)
                    ? listingServiceHelper.GetAutoDescriptionForHome(spec.Plan, spec)
                    : ParseHTML.StripHTML(spec.Description).Replace("\r", string.Empty).Replace("\n", string.Empty);
                viewModel.HomeStatus = spec.HomeStatus;
                halfBaths = spec.HalfBaths ?? 0;
                sqFt = spec.SqFt ?? 0;
                stories = spec.Stories.ToType<int>();
            }

            AddHomeToUserPlanner(plan.CommunityId, viewModel.PropertyId, listingType, currentCommunity.BuilderId,
                viewModel.IsPlan);

            var homeBasicSpecs = new StringBuilder();
            if (bedRooms > 0)
            {
                homeBasicSpecs.AppendFormat("<li>{0} " + (bedRooms == 1 ? LanguageHelper.Bedroom : LanguageHelper.Bedrooms) + "</li>", bedRooms);
            }

            if (bathRooms > 0)
                homeBasicSpecs.AppendFormat("<li>{0} " + (bathRooms == 1 ? LanguageHelper.MSG_BATHROOM : LanguageHelper.Bathrooms) + "</li>",
                    ListingHelper.ComputeBathrooms(bathRooms, halfBaths));

            if (garage > 0.0m)
                homeBasicSpecs.AppendFormat("<li>{0:0.##} " + (garage == 1 ? LanguageHelper.Garage : LanguageHelper.Garages) + "</li>", garage);

            //Sqft
            if (sqFt > 0)
            {
                var sqFtDisplay = string.Format("{0:###,###} <abbr title=\"square feet\">" + LanguageHelper.MSG_SQ_FT + "</abbr>", sqFt);
                viewModel.SqFtDisplay = sqFtDisplay;
                homeBasicSpecs.Append("<li>" + sqFtDisplay + "</li>");
            }

            //Stories
            if (stories > 0)
            {
                homeBasicSpecs.Append("<li>" + string.Format("{0} " + (stories == 1 ? LanguageHelper.Story : LanguageHelper.Stories), stories) + "</li>");
            }
            var partnerMaskIndex = _partnerService.GetPartnerMaskIndex(NhsRoute.PartnerId);
            var partnerSortValue = currentCommunity.PartnerMask.Substring(partnerMaskIndex - 1, 1);
            if ("F" != partnerSortValue)
                viewModel.IsBilled = _communityService.GetCommunityBilledStatus(NhsRoute.PartnerId,
                    currentCommunity.BuilderId,
                    currentCommunity.CommunityId, viewModel.MarketId);

            viewModel.BasicHomeSpecs = homeBasicSpecs.ToString();
            viewModel.CorporationName = currentCommunity.Builder.ParentBuilder == null
                ? viewModel.BuilderName
                : currentCommunity.Builder.ParentBuilder.BuilderName ?? string.Empty;

            //Community Info
            viewModel.CommunityId = plan.CommunityId;
            viewModel.CommunityCity = currentCommunity.City;
            viewModel.CommunityName = currentCommunity.CommunityName;
            viewModel.BuilderName = currentCommunity.Builder.BuilderName;
            viewModel.MarketName = currentCommunity.Market.MarketName;
            viewModel.CommunityDescription = FormattedText(currentCommunity.CommunityDescription);
            viewModel.BuilderId = currentCommunity.BuilderId;
            viewModel.MarketId = currentCommunity.MarketId;
            viewModel.State = currentCommunity.State.StateName;
            viewModel.StateAbbr = currentCommunity.State.StateAbbr;
            viewModel.ZipCode = currentCommunity.PostalCode;
            viewModel.IsCommunityMultiFamily = currentCommunity.IsCondo.ToType<bool>() ||
                                               currentCommunity.IsTownHome.ToType<bool>();

            viewModel.Latitude = (double) currentCommunity.Latitude;
            viewModel.Longitude = (double) currentCommunity.Longitude;

            var isShowCaseActive = currentCommunity.Brand.BrandShowCase != null &&
                                   currentCommunity.Brand.BrandShowCase.StatusId == 1;
            var hasTheBrandActiveComms =
                _communityService.GetCommunitiesByBrandId(NhsRoute.PartnerId, currentCommunity.Brand.BrandId).Any();

            //Builder Info
            viewModel.BrandName = currentCommunity.Builder.Brand.BrandName;
            viewModel.BuilderName = currentCommunity.Builder.BuilderName ?? string.Empty;
            viewModel.HasShowCaseInformation = currentCommunity.Brand.HasShowCase && isShowCaseActive &&
                                               hasTheBrandActiveComms;
            viewModel.BrandId = currentCommunity.BrandId;

            //Contact Info

            //Phone number - Get builder's toll free number
            var phoneNumber = _communityService.GetTollFreeNumber(NhsRoute.PartnerId, currentCommunity.CommunityId,
                currentCommunity.BuilderId);

            if (string.IsNullOrEmpty(phoneNumber)) //else use community SO number
                phoneNumber = currentCommunity.PhoneNumber;

            viewModel.PhoneNumber = phoneNumber;
            viewModel.HoursOfOperation = currentCommunity.SalesOffice.HoursOfOperation != null
                ? currentCommunity.SalesOffice.HoursOfOperation.ToType<string>()
                    .Replace("<br />", "   ")
                    .Replace("<br/>", "   ")
                : "";

            viewModel.PriceDisplay = string.Format(LanguageHelper.From.ToTitleCase() + ": {0:C0}", viewModel.IsPlan ? plan.Price : spec.Price);

            //Home Ancillary Info
            viewModel.Amenities = _communityService.GetAmenities(currentCommunity.IAmenities,
                currentCommunity.IOpenAmenities);
            viewModel.Utilities = _communityService.GetCommunityUtilities(plan.CommunityId, NhsRoute.PartnerId);
            viewModel.Schools = _communityService.GetSchools(NhsRoute.PartnerId, currentCommunity.CommunityId);
            viewModel.HotHomesApi = new List<HomeItem>();

            viewModel.PriceLow = viewModel.IsPlan ? plan.Price.ToType<string>() : spec.Price.ToType<string>();
            viewModel.MarketName = currentCommunity.Market.MarketName;

            var promotions =
                _communityService.GetCommunityDetailPromotions(currentCommunity.CommunityId,
                    currentCommunity.BuilderId).Take(2).ToList();


            viewModel.Promotions = promotions.Where(w => w.PromoTypeCode != "AGT").Select(p => new Promotion
            {
                PromoId = p.PromoID,
                PromoTextShort = p.PromoTextShort,
                PromoTextLong = p.PromoTextLong,
                PromoFlyerUrl = p.PromoFlyerURL,
                PromoType = p.PromoTypeCode,
                PromoEndDate = p.PromoEndDate,
                PromoStartDate = p.PromoStartDate,
                PromoUrl = p.PromoURL
            }).ToList();

            viewModel.GreenPrograms =
                _communityService.GetCommunityGreenPrograms(currentCommunity.CommunityId).Take(2).ToList();

            var searchParams = new SearchParams
            {
                CommId = currentCommunity.CommunityId,
                BuilderId = currentCommunity.BuilderId,
                PartnerId = NhsRoute.PartnerId,
                MarketId = currentCommunity.MarketId,
                HomeStatus = viewModel.HomeStatus.HomeStatusTypeToHomeStatus(),
                WebApiSearchType = WebApiSearchType.Exact
            };

            var allHomes = _listingService.GetHomesResultsFromApi<HomeItem>(searchParams, NhsRoute.PartnerId).Result.Select(home =>
            {
                var _market = _marketService.GetMarket(home.MarketId);
                home.MarketName = _market.MarketName;
                home.StateName = _market.State.StateName;
                return home;
            }).ToList();

            var similarHomes = GetSimilarHomes(allHomes, plan, spec, viewModel.IsPlan, bedRooms, bathRooms, viewModel.IsPlan ? plan.Price : spec.Price, 
                                               viewModel.MarketName, viewModel.MarketId);
            viewModel.SimilarHomesApi = similarHomes;
            if (UserSession.UserProfile.IsLoggedIn())
            {
                var pl = !viewModel.IsPlan
                    ? new PlannerListing(viewModel.SpecId, ListingType.Spec) {BuilderId = currentCommunity.BuilderId}
                    : new PlannerListing(viewModel.PlanId, ListingType.Plan) {BuilderId = currentCommunity.BuilderId};
                if (UserSession.UserProfile.Planner.SavedHomes.Contains(pl))
                    viewModel.SavedToPlanner = true;
            }

            viewModel.LogActionMethodUrl = GetMethodUrl(viewModel.IsPlan, viewModel.IsPlan ? plan.PlanId : spec.SpecId);

            //SEO
            OverrideHomeDetailMeta(spec, plan, addNoIndexTag, viewModel, currentCommunity);

            return viewModel;
        }

        private List<MediaPlayerObject> GetMediaPlayerObjectsList(IPlan plan, ISpec spec, string virtualTourUrl, string floorPlanViewerUrl)
        {
            var objects = _listingService.GetMediaPlayerObjects(plan, spec);

            if (!string.IsNullOrEmpty(virtualTourUrl) && !virtualTourUrl.Equals("n", StringComparison.InvariantCultureIgnoreCase))
            {
                objects.Add(new MediaPlayerObject
                {
                    Type = MediaPlayerObjectTypes.Link,
                    SubType = MediaPlayerObjectTypes.SubTypes.VirtualTour,
                    Url = virtualTourUrl
                });
            }

            if (!string.IsNullOrEmpty(floorPlanViewerUrl) && !floorPlanViewerUrl.Equals("n", StringComparison.InvariantCultureIgnoreCase))
            {

                objects.Add(
                    new MediaPlayerObject
                    {
                        Type = MediaPlayerObjectTypes.Link,
                        SubType = MediaPlayerObjectTypes.SubTypes.FloorPlanImages,
                        Url = floorPlanViewerUrl
                    }
                    );
            }


            var model = FillExtendedMediaObjectsInfo(objects, false);
            return model;
        }
    }
}
