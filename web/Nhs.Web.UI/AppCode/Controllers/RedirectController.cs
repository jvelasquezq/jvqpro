﻿using System.Linq;
using System.Web.Mvc;
using Nhs.Library.Constants;
using Nhs.Library.Extensions;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Web.UI.AppCode.Helper;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class RedirectController : BaseController
    {
        private readonly IListingService _listingService;
        private readonly ICommunityService _communityService;

        public RedirectController(IListingService listingService, ICommunityService communityService)
        {
            _listingService = listingService;
            _communityService = communityService;
        }

        public virtual RedirectResult CommunityDetail()
        {
            var pageDetail = RedirectionHelper.GetCommunityDetailPage();
            var communityId = RouteParams.CommunityId.Value<int>();
            var community = _communityService.GetCommunity(communityId);
            if (community == null )
                return RedirectTo404();
            return RedirectPermanent(pageDetail, community.ToCommunityDetail());
        }

        public virtual RedirectResult HomeDetail()
        {
            var planId = RouteParams.PlanId.Value<int>();
            var specId = RouteParams.SpecId.Value<int>();

            IPlan plan = null;
            ISpec spec = null;

            if (planId > 0)
            {
                plan = _listingService.GetPlan(planId);
            }

            if (specId > 0)
            {
                spec = _listingService.GetSpec(specId);
            }

            if (plan == null && spec == null)
            {
                return RedirectTo404();
            }

            var pageDetail = RedirectionHelper.GetHomeDetailPage(specId > 0);
           
            if (planId > 0)
                return RedirectPermanent(pageDetail, plan.ToHomeDetail());
            return RedirectPermanent(pageDetail, spec.ToHomeDetail());
        }

        public virtual RedirectResult CommunityResults()
        {
           var page = RedirectionHelper.GetCommunityResultsPage();
           var parameters = NhsRoute.CurrentRoute.Params;
           if (parameters.Exists(p => p == RouteParams.Condado || p == RouteParams.Ciudad))
           {
               var parameter = parameters.FirstOrDefault(p => p == RouteParams.Condado || p == RouteParams.Ciudad);
               if (parameter != null)
               {
                   parameter.Value = parameter.Value.RemoveSpaceAndDash();
               }
           }
           parameters = parameters.ToResultsParams();
           return RedirectPermanent(page, parameters);
        }

        public virtual RedirectResult HomeResults()
        {
            var page = RedirectionHelper.GetHomeResultsPage();
            var parameters = NhsRoute.CurrentRoute.Params;
            if (parameters.Exists(p => p == RouteParams.Condado || p == RouteParams.Ciudad))
            {
                var parameter = parameters.FirstOrDefault(p => p == RouteParams.Condado || p == RouteParams.Ciudad);
                if (parameter != null)
                {
                    parameter.Value = parameter.Value.RemoveSpaceAndDash();
                }
            }
            parameters = parameters.ToResultsParams();
            return RedirectPermanent(page, parameters);
        }

        public virtual RedirectResult StateIndex()
        {
            const string page = Pages.StateIndex;
            var parameters = NhsRoute.CurrentRoute.Params;
            return RedirectPermanent(page, parameters);
        }

        public virtual RedirectResult NewHomeBuilders()
        {
            var page = RedirectionHelper.GetCommunityResultsPage();
            var parameters = NhsRoute.CurrentRoute.Params;
            parameters = parameters.ToResultsParams();
            parameters.Add(new RouteParam(RouteParams.HomeBuilders, true,RouteParamType.QueryString));
            return RedirectPermanent(page, parameters);
        }

        public virtual RedirectResult NewHomeBuildersBrand()
        {
            var page = RedirectionHelper.GetCommunityResultsPage();
            var parameters = NhsRoute.CurrentRoute.Params;
            parameters = parameters.ToResultsParams();
            return RedirectPermanent(page, parameters);
        }
    }
}