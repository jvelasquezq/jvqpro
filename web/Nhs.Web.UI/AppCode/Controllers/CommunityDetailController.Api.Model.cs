﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bdx.Web.Api.Objects.Constants;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Proxy;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects;
using Nhs.Search.Objects.Constants;
using Nhs.Utility.Data.Paging;
using Nhs.Web.UI.AppCode.Lib;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class CommunityDetailController
    {

        private WebApiCommonResultModel<List<HomeItem>> GetCommunityHomeResultsFromApi(PropertySearchParams searchParams, bool useQmi = false)
        {

            var parameters = new SearchParams
            {
                CommId = searchParams.CommunityId,
                PartnerId = NhsRoute.PartnerId,
                SortBy = ToSortBy(SortOrder.Status),
                BuilderId = searchParams.BuilderId,
                MarketId = searchParams.MarketId,
                WebApiSearchType = WebApiSearchType.Exact,
                PageSize = 1000,
                PageNumber = 1,
                Qmi = useQmi               
            };

            var results = _apiService.GetResultsWebApi<HomeItem>(parameters, SearchResultsPageType.HomeResults); 
            results.Result = results.Result.Select(home =>
            {
                var market = _marketService.GetMarket(home.MarketId);
                home.MarketName = market.MarketName;
                home.StateName = market.State.StateName;
                return home;
            }).ToList();
            return results;
        }

        private WebApiCommonResultModel<List<HomeItem>> GetCommunityHomeResultsFromApi(SearchParams searchParams, bool useQmi = false)
        {
            var parameters = new SearchParams
            {
                CommId = searchParams.CommId,
                PartnerId = NhsRoute.PartnerId,
                SortBy = searchParams.SortBy,
                BuilderId = searchParams.BuilderId,
                MarketId = searchParams.MarketId,
                WebApiSearchType = WebApiSearchType.Exact,
                PageSize = searchParams.PageSize,
                PageNumber = searchParams.PageNumber,
                Qmi = useQmi
            };

            var results = _apiService.GetResultsWebApi<HomeItem>(parameters, SearchResultsPageType.HomeResults);
            results.Result = results.Result.Select(home =>
            {
                var market = _marketService.GetMarket(home.MarketId);
                home.MarketName = market.MarketName;
                home.StateName = market.State.StateName;
                return home;
            }).ToList();
            return results;
        }

        private static SortBy ToSortBy(SortOrder sortOrder)
        {
            SortBy returnValue;// = SortBy.Random;

            switch (sortOrder)
            {
                case SortOrder.Random:
                case SortOrder.Size:
                case SortOrder.SpecialOffer:
                case SortOrder.QuickMoveIn:
                case SortOrder.HomeMatches:
                case SortOrder.Openhouse:
                case SortOrder.Featuredtour:
                    returnValue = SortBy.Random;
                    break;
                case SortOrder.Location:
                case SortOrder.CityRadius:
                case SortOrder.CountyRadius:
                case SortOrder.ZipRadius:
                    returnValue = SortBy.Distance;
                    break;
                case SortOrder.Price:
                    returnValue = SortBy.Price;
                    break;
                case SortOrder.Builder:
                    returnValue = SortBy.Brand;
                    break;
                case SortOrder.Status:
                    returnValue = SortBy.HomeStatus;
                    break;
                case SortOrder.Name:
                    returnValue = SortBy.Plan;
                    break;
                case SortOrder.Numberofphotos:
                    returnValue = SortBy.NumPhotos;
                    break;
                default:
                    returnValue = SortBy.Random;
                    break;
            }

            return returnValue;
        }

        private string HomeSearchViewMetricDisplayed(IEnumerable<HomeItem> homes, int allNewHomesCount, int marketId)
        {
            if (allNewHomesCount == 0) return string.Empty;
            var homesMetric = homes.Take(12);

            var metricBuilder = new StringBuilder();

            foreach (string chain in homesMetric.Select(home => string.Concat(NhsRoute.PartnerId, ",", marketId, ",", home.BuilderId, ",", home.CommId, ",", home.IsSpec > 0 ? home.HomeId : 0, ",", home.HomeId, ",", "|")))
                metricBuilder.Append(chain);

            metricBuilder.Remove(metricBuilder.Length - 2, 2);
            return metricBuilder.ToString();
        }

        private string HomeSearchViewMetricHidden(IEnumerable<HomeItem> homes, int allNewHomesCount, int marketId)
        {
            if (allNewHomesCount == 0) return string.Empty;

            int index = homes.Count() - 12;
            if (index < 1) return string.Empty;
            //TODO: Avoid reverse (performance issue possibility)
            var homesMetric = homes.Reverse().Take(index);

            var metricBuilder = new StringBuilder();

            foreach (string chain in homesMetric.Select(home => string.Concat(NhsRoute.PartnerId, ",", marketId, ",", home.BuilderId, ",", home.CommId, ",", home.IsSpec > 0 ? home.HomeId : 0, ",", home.HomeId, ",", "|")))
                metricBuilder.Append(chain);

            metricBuilder.Remove(metricBuilder.Length - 2, 2);
            return metricBuilder.ToString();
        }
    }
}
