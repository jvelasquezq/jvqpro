﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using Nhs.Library.Business;
using Nhs.Library.Business.Map;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Constants.Route;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers.Content;
using Nhs.Library.Helpers.MarketMaps;
using Nhs.Library.Helpers.Property;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects;
using Nhs.Search.Objects.Constants;
using Nhs.Utility.Common;
using Nhs.Utility.Data.Paging;
using Nhs.Web.UI.AppCode.Extensions;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class CommunityResultsController
    {
        private string _prevGroupValue = "xxx";
        private int _numPostbacks;
        private bool _isBasicListingGroup;
        private bool _isBasicCommunityGroup;
        private Market _market;

        /// <summary>
        /// Action used when loaded the first time
        /// </summary>
        [HttpGet]
        public virtual ActionResult ShowOldCommResultsResults()
        {
            PartnerLayoutHelper.GetPartnerLayoutConfig();
            UserSession.PersonalCookie.MarketId = RouteParams.Market.Value<int>();

            Market market = null;
            if (RouteParams.Market.Value<int>() == 0)
            {
                var state = RouteParams.StateName.Value();
                var marketName = RouteParams.MarketName.Value();
                market = _marketService.GetMarket(NhsRoute.PartnerId, state.Replace("-", " "), marketName.Replace("-", " "), false);
            }
            else
            {
                var marketId = RouteParams.Market.Value<int>();
                market = GetMarket(marketId);
            }
            var communityResultsPages = new List<string>
            {
                Pages.CommunityResultsv2.ToLower(),
                Pages.CommunityResults.ToLower(),
                Pages.Comunidades.ToLower()
            };

            //301ing in the base/application controller constructor will still allow this action to be executed, so we 301 here.
            var isCommunityResults = communityResultsPages.Contains(NhsRoute.CurrentRoute.Function.ToLower());

            var refer301Url = GetReferRedirectUrl(market, isCommunityResults);

            if (!string.IsNullOrEmpty(refer301Url))
                return RedirectPermanent(refer301Url);


            //Case 77642: IF removed below is simplified with the new DFU Service. 
            //if (RouteParams.Market.Value<int>() != 0 && Configuration.MarketsListforDFU.Contains(RouteParams.Market.Value<int>()) && NhsRoute.PartnerId == NhsRoute.BrandPartnerId) // not a private label site   
            var resultPage = RedirectionHelper.RedirecToNewFormat(RouteParams.Market.Value<int>(), _marketDfuService);
            if (!string.IsNullOrEmpty(resultPage))
            {
                return RedirectPermanent(resultPage, market.ToResultsParams(NhsRoute.CurrentRoute.Params));
            }

            var searchParams = GetDefaultSearchParams();
            if (UserSession.PageSize <= 1)
            {
                UserSession.PageSize = Configuration.CommunityResultsPageSize;
            }

            // Gets models
            var page = 1;

            if (!string.IsNullOrEmpty(RouteParams.Page.Value()))
                page = RouteParams.Page.Value().ToType<int>();

            var model = GetCommunityResultsModel(searchParams, true, page);

            AddCanonicalWithExcludeParamsAndReplace(model, new List<string> { "state", "pricelow", "pricehigh" },
                                                         new Dictionary<string, string> { { "city", "citynamefilter" } });
            UserSession.PropertySearchParameters = searchParams;

            //Prevent caching
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetNoStore();
            Response.AppendHeader("Pragma", "no-cache");


            if (NhsLinkParams.NewSession.GetValue<bool>())
                GenerateNewSession();

            var isNewSescion = CookieManager.PreviousSessionId != UserSession.SessionId;
            model.ShowReturnUserModal = NhsLinkParams.ShowReturnUserModal.GetValue<bool>();

            if (isNewSescion || model.ShowReturnUserModal)
            {
                int cMarketId;
                var currentMarket = model.CurrentMarketId > 0 ? model.CurrentMarketId : NhsUrl.GetMarketID;
                var cookieCommId = GetCommunityIdFromCoockie(out cMarketId);
                var hasSameMarket = currentMarket == cMarketId;
                if (cookieCommId > 0 && cMarketId > 0 && hasSameMarket)
                {
                    var lead = LeadUtil.GetLeadInfoFromSession();
                    var rcoms = _communityService.GetRecommendedCommunities(lead.LeadUserInfo.Email, NhsRoute.PartnerId,
                                                                            cookieCommId, 0, 0, 0);
                    var hasRecomented = rcoms.Any();
                    model.AlreadyRequestBrosurePreviosComm = _communityService.AlreadyRequestBrosure
                        (
                            NhsRoute.PartnerId,
                            currentMarket,
                            cookieCommId,
                            lead.LeadUserInfo.Email
                        );
                    model.HasCommToRecommend = hasRecomented;
                    model.HasTheSameMarcketId = true;
                    model.IsNewSession = isNewSescion;
                }
            }

            //UserSession.PersonalCookie.SearchText = GetSearchText(searchParams.ToSearchParams());
                       
            // ReSharper disable once Mvc.ViewNotResolved
            return View(model);
            
        }


        /// <summary>
        /// This action is needed for the SEO page links on Quick Move in tab
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult QuickMoveIn()
        {
            var searchParams = GetDefaultSearchParams();
            searchParams.SpecialOfferComm = 0;
            searchParams.HomeStatus = (int)HomeStatusType.QuickMoveIn;

            // Gets models
            int page = 1;
            if (!string.IsNullOrEmpty(RouteParams.Page.Value()))
                page = Convert.ToInt32(RouteParams.Page.Value());

            var model = GetCommunityResultsModel(searchParams, true, page);
            UserSession.PropertySearchParameters = searchParams;

            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            // ReSharper disable Asp.NotResolved
            return View("Show", model);
            // ReSharper restore Asp.NotResolved
        }

        /// <summary>
        /// This action is needed for SEO page links on Hot Deals tab
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult HotDeals()
        {
            var searchParams = GetDefaultSearchParams();
            searchParams.HomeStatus = 0;
            searchParams.SpecialOfferComm = 1;

            // Gets models
            int page = 1;
            if (!string.IsNullOrEmpty(RouteParams.Page.Value()))
                page = Convert.ToInt32(RouteParams.Page.Value()) ;

            var model = GetCommunityResultsModel(searchParams, true, page);
            UserSession.PropertySearchParameters = searchParams;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            // ReSharper disable Asp.NotResolved
            return View("Show", model);
            // ReSharper restore Asp.NotResolved
        }

        /// <summary>
        /// Action used for facets, paging and sort
        /// </summary>
        /// <param name="searchParameters">Serialized Json representation of the search parameter object</param>
        /// <param name="searchLocation">The search location.</param>
        /// <param name="page">Page number</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="sortColumn">Sort column</param>
        /// <param name="pageCommand">The page command.</param>
        /// <param name="numPostBacks">The num post backs.</param>
        /// <param name="trigerByFacets"></param>
        /// <param name="showCode"></param>
        /// <param name="isLocationSearchBox"></param>
        /// <param name="searchUrl"></param>
        /// <returns></returns>
        public virtual ActionResult Search(string searchParameters, string searchLocation, int page, int pageSize, string sortColumn, string pageCommand, int? numPostBacks, bool trigerByFacets = false, bool showCode = false, bool isLocationSearchBox = false, string searchUrl = "", string locationType = "")
        {

            if (trigerByFacets)
                UserSession.MultiBrochureList = new MultiBrochureList();

            if (UserSession.GetItem("SearchUrl") == null)
                UserSession.SetItem("SearchUrl", searchUrl);

            var searchParams = JsonHelper.FromJson<NhsPropertySearchParams>(searchParameters);
            if (!isLocationSearchBox && !string.IsNullOrEmpty(searchParams.CityNameFilter))
            {
                searchParams.PostalCode = "";
                searchParams.PostalCodeFilter = "";
                searchParams.County = "";
                searchParams.CountyNameFilter = "";
            }
            UserSession.SetItem("LastUsedRadius", 0);

            //if Green property is removed on Home type/Amenities: clean UserSession
            if (searchParams.GreenProgram == false && UserSession.SearchType == SearchTypeSource.GreenCommunityResults)
                UserSession.SearchType = SearchTypeSource.SearchUnknown;

            UserSession.IsLocationSearchBox = isLocationSearchBox;
            //62208
            searchParams.ComingSoon = searchParams.CommunityStatus == CommunityStatusType.ComingSoon;

            if (searchParams.CommunityStatus != CommunityStatusType.ComingSoon && searchParams.ComingSoon)
                searchParams.ComingSoon = false;

            const bool retrieveSearchResultsFromDb = true;
            CommunityResultsViewModel model;
            if (numPostBacks.HasValue)
                _numPostbacks = numPostBacks.Value + 1;

            UserSession.PageSize = pageSize;

            bool isAreaSearch = false;

            if (!string.IsNullOrEmpty(searchLocation))
            {
                searchLocation = searchLocation.Trim();
                if (searchLocation.EndsWith("area", StringComparison.InvariantCultureIgnoreCase))
                {
                    searchLocation = searchLocation.Substring(0,
                        searchLocation.LastIndexOf("area", StringComparison.InvariantCultureIgnoreCase)).Trim();
                    isAreaSearch = true;
                }

                if (string.IsNullOrEmpty(searchLocation))
                {
                    ModelState.AddModelError("LocationName", @"location required.");
                }

                IList<RouteParam> param = new List<RouteParam>();

                var locations = _partnerService.GetTypeAheadSuggestions(NhsRoute.PartnerId, searchLocation, true);

                if (locations.Count == 0)
                {
                    ModelState.AddModelError("LocationName", @"Invalid data, either zip code or city, state");
                }
                else
                {
                    if (locations.Count == 1)
                        searchLocation = locations[0].Type >= (int)LocationType.Community ?
                                                                locations[0].Name + " in " + locations[0].MarketName + ", " + locations[0].State :
                                                                locations[0].Name + (locations[0].Type != (int)LocationType.Zip ? ", " : string.Empty) + locations[0].State;

                    // Searching by City, ST or Market, ST
                    if (searchLocation.IndexOf(",", StringComparison.Ordinal) != -1)
                    {
                        int commaPostion = searchLocation.IndexOf(",", StringComparison.Ordinal);
                        string locationText = searchLocation.Substring(0, commaPostion).Trim();
                        string stateText = searchLocation.Substring(commaPostion + 1).Trim();

                        if (!string.IsNullOrEmpty(locationText) && !string.IsNullOrEmpty(stateText))
                        {
                            // Full State Name
                            if (stateText.Length > 2)
                            {
                                stateText = _stateService.GetStateAbbreviation(stateText);
                            }

                            param.Add(new RouteParam(RouteParams.State, stateText, RouteParamType.Friendly));

                            if (!string.IsNullOrEmpty(locationType) && searchLocation.IndexOf(",") != -1)
                                param.Add(new RouteParam(RouteParams.SearchType, locationType, RouteParamType.Friendly));

                            param.Add(new RouteParam(RouteParams.SearchText, locationText, RouteParamType.QueryString));

                            UserSession.PersonalCookie.SearchText = searchLocation;
                            UserSession.PersonalCookie.State = stateText;
                        }
                        else
                        {
                            ModelState.AddModelError("LocationName", @"Invalid data, location and state required.");
                        }
                    }
                    else
                    {
                        param.Add(new RouteParam(RouteParams.SearchText, searchLocation, RouteParamType.QueryString));
                        UserSession.PersonalCookie.SearchText = searchLocation;
                    }
                }

                if (!ModelState.IsValid)
                {
                    model = GetCommunityResultsModel(searchParams, false, page);
                    model.LocationName = "Invalid Location";
                    return PartialView(NhsMvc.Default.Views.CommunityResults.Facets, model);
                }

                if (isAreaSearch)
                {
                    param.Add(new RouteParam(RouteParams.SearchType, 1));
                }

                return Content(param.ToUrl(Pages.LocationHandler));
            }

            if (sortColumn != null)
            {
                switch (sortColumn.ToLower())
                {
                    case "":
                        searchParams.SortOrder = SortOrder.Random;
                        break;
                    case "location":
                        searchParams.SortOrder = SortOrder.Location;
                        LogComunityResultClickAction(searchParams.MarketId, LogImpressionConst.SortByLocation);
                        break;
                    case "price":
                        searchParams.SortOrder = SortOrder.Price;
                        LogComunityResultClickAction(searchParams.MarketId, LogImpressionConst.SortByPrice);
                        break;
                    case "builder":
                        searchParams.SortOrder = SortOrder.Builder;
                        LogComunityResultClickAction(searchParams.MarketId, LogImpressionConst.SortByBuilder);
                        break;
                    case "name":
                        searchParams.SortOrder = SortOrder.Name;
                        LogComunityResultClickAction(searchParams.MarketId, LogImpressionConst.SortByCommunityName);
                        break;
                    case "homematches":
                        searchParams.SortOrder = SortOrder.HomeMatches;
                        LogComunityResultClickAction(searchParams.MarketId, LogImpressionConst.SortByHomeMatches);
                        break;
                }
            }

            // log page event
            if (!string.IsNullOrEmpty(pageCommand))
            {
                LogComunityResultClickAction(searchParams.MarketId, pageCommand);
            }

            model = GetCommunityResultsModel(searchParams, retrieveSearchResultsFromDb, page);
            model.TrigerByFacets = trigerByFacets;
            model.ShowCode = showCode;
            UserSession.PropertySearchParameters = searchParams;

            return PartialView(NhsMvc.Default.Views.CommunityResults.ResultData, model);
        }

        public virtual ActionResult QuickView(int communityId, string searchParameters)
        {
            var searchParams = JsonHelper.FromJson<PropertySearchParams>(searchParameters);
            var viewModel = new CommunityQuickViewViewModel();
            var sortActions = new SortActions();
            searchParams.CommunityId = communityId;
            searchParams.StartRecord = 0;
            searchParams.EndRecord = 0;
            var resultsView = _listingService.GetHomeResults(searchParams, true);

            viewModel.HomeResults = _listingService.GetExtendedHomeResults(NhsRoute.PartnerId, sortActions.SortOptions[SortOrder.Status](resultsView.HomeResults), false);

            return PartialView(NhsMvc.Default.Views.CommunityResults.QuickView, viewModel);
        }

        ///Add logic to handle this part in CRC.Action ShowDFU
        //[HttpGet]
        //public virtual ActionResult ShowDFU(string statename, string marketname)
        //{
        //    return ShowDFUWithCity(statename, marketname, string.Empty);
        //}

        [HttpGet]
        public virtual ActionResult ShowDFUWithBrand(string statename, string marketname, string brandid)
        {
            return ShowDFUWithCity(statename, marketname, string.Empty);
        }

        [HttpGet]
        public virtual ActionResult ShowDFUWithPage(string statename, string marketname, string pagenumber)
        {
            return ShowDFUWithCity(statename, marketname, string.Empty);
        }

        [HttpGet]
        public virtual ActionResult ShowDFUWithCity(string statename, string marketname, string city)
        {
            var searchParams = GetDefaultSearchParams();

            Market mkt = _marketService.GetMarket(NhsRoute.PartnerId, statename.Replace("-", " "), marketname.Replace("-", " "), false);
            searchParams.MarketId = mkt.MarketId;
            UserSession.PersonalCookie.MarketId = mkt.MarketId;

            var marketRedirectionUrl = RedirectOnInvalidMarket(mkt.MarketId);
            if (!string.IsNullOrEmpty(marketRedirectionUrl))
                return Redirect(marketRedirectionUrl);

            var communityResultsPages = new List<string>
            {
                Pages.CommunityResultsv2.ToLower(),
                Pages.CommunityResults.ToLower(),
                Pages.Comunidades.ToLower()
            };
            var isCommunityResults = communityResultsPages.Contains(NhsRoute.CurrentRoute.Function.ToLower());

            //301ing in the base/application controller constructor will still allow this action to be executed, so we 301 here.
            var refer301Url = GetReferRedirectUrl(mkt, isCommunityResults);

            if (!string.IsNullOrEmpty(refer301Url))
            {
                return RedirectPermanent(refer301Url);
            }

            // Gets model
            int page =1;
            if (!string.IsNullOrEmpty(RouteParams.Page.Value()))
                page = Convert.ToInt32(RouteParams.Page.Value());
            var model = GetCommunityResultsModel(searchParams, true, page);

            AddCanonicalWithExcludeParams(model, new List<string> { "state", "pricelow", "pricehigh" });

            var view = NhsMvc.Default.Views.CommunityResultsV2.Show;
            if (NhsRoute.IsBrandPartnerNhsPro)
            {
                view = NhsMvc.Default.Views.CommunityResults.Show;
            }

            // ReSharper disable Asp.NotResolved
            return View(view, model);
            // ReSharper restore Asp.NotResolved
        }

        public virtual JsonResult SaveCommunityToPlanner(int communityId, int builderId)
        {
            var pl = new PlannerListing(communityId, ListingType.Community);
            if (!UserSession.UserProfile.Planner.SavedCommunities.Contains(pl))
                UserSession.UserProfile.Planner.AddSavedCommunity(communityId, builderId);

            var logger = new ImpressionLogger
                {
                    CommunityId = communityId,
                    BuilderId = builderId,
                    PartnerId = NhsRoute.PartnerId.ToType<string>(),
                    Refer = UserSession.Refer
                };
            logger.LogView(LogImpressionConst.AddCommunityToUserPlanner);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult AddRemoveMultiBrochure(int marketId, int communityId, int builderId, bool isBilled, bool add, bool isFeatured, int page, string position)
        {
            UserSession.MultiBrochureList.Market = marketId;
            if (add)
                UserSession.MultiBrochureList.Add(communityId, builderId, isBilled, isFeatured, page, position);
            else
                UserSession.MultiBrochureList.Remove(communityId, builderId);
            return Json(UserSession.MultiBrochureList.BrochureList.Count(), JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult ShowModalRecoRetunUserBroshure()
        {
            var lead = LeadUtil.GetLeadInfoFromSession();
            UserSession.PopUpRetUserBroshure = true;
            int cmarketId;
            var commId = GetCommunityIdFromCoockie(out cmarketId);
            var model = new ReturnedUserRecoComm();
            var comm = _communityService.GetCommunity(commId);
            if (comm != null)
            {
                var address = string.Format("{0} {1}, {2}",
                                            comm.City, comm.State.StateAbbr, comm.PostalCode);
                var price = string.Format("From {0:C0} - {1:C0}",
                                          comm.PriceLow, comm.PriceHigh);

                model.Address = address;
                model.CommunityId = comm.CommunityId;
                model.BuilderId = comm.BuilderId;
                model.CommThumbnail = comm.SpotlightThumbnail;
                model.CommunityName = comm.CommunityName;
                model.CommPrice = price;
                model.MarketId = comm.MarketId;
            }
            model.HeadlineText = NhsLinkParams.PopUpHeaderRetUser.GetValue<string>();
            model.SubheadText = NhsLinkParams.PopUpExtraInfoRetUser.GetValue<string>();
            model.DefaultChecked = NhsLinkParams.DefaultChecked.GetValue(true);
            model.MatchEmail = NhsLinkParams.MatchEmail.GetValue<bool>();
            model.FullName = string.Format("{0} {1}", lead.LeadUserInfo.FirstName, lead.LeadUserInfo.LastName).Trim();
            model.Email = lead.LeadUserInfo.Email;
            model.Zip = lead.LeadUserInfo.PostalCode;
            model.RecoCommunitys = _communityService.GetRecommendedCommunities(lead.LeadUserInfo.Email, NhsRoute.PartnerId, commId, 0, 0, NhsLinkParams.MaxRecs.GetValue<int>());

            if (Request.IsAjaxRequest())
                return PartialView(NhsMvc.Default.Views.CommunityResults.PartialReturningUsersRecommendedComm, model);
            return View(NhsMvc.Default.Views.CommunityResults.PartialReturningUsersRecommendedComm, model);
        }

        #region Private Methods

        private string RedirectOnInvalidMarket(int marketId)
        {
            //if partner market is null or market bc's is null then redirect to nomatchhandler
            bool isMarketActiveForPartner = (!_marketService.IsMarketValidForPartner(NhsRoute.PartnerId, marketId) | _communityService.GetMarketBCs(NhsRoute.PartnerId, marketId, null).Count == 0) ? false : true;

            if (!isMarketActiveForPartner && marketId > 0)
            {
                var urlParams = new List<RouteParam>
                    {
                        new RouteParam(RouteParams.State, _marketService.GetStateNameForMarket(marketId)),
                        new RouteParam(RouteParams.Refer, ReferTags.CommunityResultsNoMatchRedirect)
                    };

                return urlParams.ToUrl(Pages.StateIndex);
            }
            return string.Empty;
        }

        private NhsPropertySearchParams GetDefaultSearchParams()
        {
            bool isReturn = false;
            //Prev search params
            NhsPropertySearchParams prevParams = UserSession.PreviousSearchParameters;

            // Search Params
            NhsPropertySearchParams searchParams = UserSession.PropertySearchParameters;


            if (Request.QueryString["frompage"] != null)
                if (Request.QueryString["frompage"] == "bcc")
                    isReturn = true;


            if (prevParams != null)
                if (!string.IsNullOrEmpty(prevParams.CommunityName) && isReturn)
                    return prevParams;


            if (prevParams != null && !string.IsNullOrEmpty(RouteParams.Page.Value())) //If search already performed and a page number passed, that means we don't reset facets.
                return searchParams;

            if ((searchParams.PromotionType == PromotionType.Agent || searchParams.PromotionType == PromotionType.Both) && UserSession.UserProfile.ActorStatus == WebActors.GuestUser)
                searchParams.PromotionType = PromotionType.Any;


            searchParams.City = string.Empty;
            searchParams.CityNameFilter = string.Empty;
            searchParams.County = string.Empty;
            searchParams.CountyNameFilter = string.Empty;
            searchParams.OriginLat = 0;
            searchParams.OriginLong = 0;
            searchParams.PostalCode = string.Empty;
            searchParams.PostalCodeFilter = string.Empty;
            searchParams.MarketName = string.Empty;
            searchParams.StateName = string.Empty;
            searchParams.CommunityName = string.Empty;
            searchParams.State = string.Empty;
            searchParams.GreenProgram = searchParams.GreenProgram || RouteParams.Green.Value().ToLower().Equals("true");
            searchParams.MinLatitude = 0;
            searchParams.MaxLatitude = 0;
            searchParams.MinLongitude = 0;
            searchParams.MaxLongitude = 0;
            searchParams.CommunityStatus = searchParams.CommunityStatus ?? string.Empty;
            searchParams.MaxLongitude = 0;
            searchParams.SqFtHigh = 0;
            //searchParams.HasEvent = false;

            searchParams.SetFromRouteParam(NhsRoute.CurrentRoute.Params);
            searchParams.FacetPriceLow = searchParams.PriceLow;
            searchParams.FacetPriceHigh = searchParams.PriceHigh;
            searchParams.ComingSoon = false;
            searchParams.SortOrder = SortOrder.Random;
            searchParams.CommunityStatus = CommunityStatusType.All;

            if (RouteParams.ComingSoon.Value().Equals("true"))
            {
                searchParams.CommunityStatus = CommunityStatusType.ComingSoon;
                searchParams.ComingSoon = true;
            }
            if (!string.IsNullOrEmpty(RouteParams.CommunityStatus.Value()))
            {
                searchParams.CommunityStatus = RouteParams.CommunityStatus.Value();
            }

            if (!string.IsNullOrEmpty(searchParams.MarketName) && !string.IsNullOrEmpty(searchParams.StateName)) //DFUs
            {
                var mkt = _marketService.GetMarket(NhsRoute.PartnerId, searchParams.StateName.Replace("-", " "), searchParams.MarketName.Replace("-", " "), false);
                searchParams.MarketId = mkt.MarketId; //Market Id is mandatory for a search
            }

            var searchType = UserSession.SearchType == SearchTypeSource.SearchUnknown ? RouteParams.SearchType.Value() : UserSession.SearchType;
            searchParams.PartnerId = NhsRoute.PartnerId;
            searchParams.HomeStatus = 0;
            if (RouteParams.SearchType.Value() != SearchTypeSource.HotDealsSearchCommunityResults)
            {
                if (RouteParams.HotDeals.Value().Equals("true"))
                {
                    searchParams.HomeStatus = 0;
                    searchParams.SpecialOfferComm = 1;
                }
                else
                    searchParams.SpecialOfferComm = 0;
            }

            //Tickets 46973 & 49416: Set the last used radius on default value 0 
            UserSession.SetItem("LastUsedRadius", 0);
            UserSession.SetItem("LastRadius", 0);

            string linkUrl = NhsRoute.CurrentRoute.HostUrl;

            if (linkUrl.IndexOf("-") == -1)
                linkUrl = linkUrl + "?" + NhsRoute.CurrentRoute.Params.ToUrl();

            UserSession.SetItem("SearchUrl", linkUrl);

            if (!string.IsNullOrEmpty(RouteParams.Sort.Value()))
                searchParams.SortOrder = (SortOrder)Enum.Parse(typeof(SortOrder), RouteParams.Sort.Value(), true);


            switch (searchType)
            {
                case SearchTypeSource.SearchUnknown: //do nothing
                    break;
                case SearchTypeSource.HotDealsSearchCommunityResults:
                    searchParams.HomeStatus = 0;
                    searchParams.SpecialOfferComm = 1;
                    break;
                case SearchTypeSource.GreenCommunityResults:
                    searchParams.GreenProgram = true;
                    break;
            }

            UserSession.PreviousSearchParameters = searchParams;
            if (UserSession.IsLocationSearchBox && NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
            {
                ResetFacetsUsingLocationSearchBox(searchParams);
                UserSession.IsLocationSearchBox = false;
            }

            return searchParams;
        }

        private static void ResetFacetsUsingLocationSearchBox(NhsPropertySearchParams searchParams)
        {
            searchParams.Radius = 0;
            searchParams.PriceLow = 100000;
            searchParams.PriceHigh = 1000000;
            searchParams.CommunityStatus = "";
            searchParams.NumOfBeds = 0;
            searchParams.NumOfBaths = 0;
            searchParams.HasVideo = false;
            searchParams.SchoolDistrictId = 0;
            searchParams.HomeType = "";
            searchParams.Pool = false;
            searchParams.GreenNatureAreas = false;
            searchParams.GreenProgram = false;
            searchParams.GolfCourse = false;
            searchParams.Gated = false;
            searchParams.Parks = false;
            searchParams.Adult = false;
            searchParams.BuilderId = 0;
            searchParams.BrandId = 0;
        }

        private static void LogComunityResultClickAction(int marketId, string eventCode)
        {
            var logger = new ImpressionLogger
            {
                MarketId = marketId,
                PartnerId = NhsRoute.PartnerId.ToString(),
                Refer = UserSession.Refer
            };
            logger.LogView(eventCode);
        }

        private CommunityResultsViewModel GetCommunityResultsModel(NhsPropertySearchParams searchParams, bool refreshCache, int pageIdx)
        {
            if (searchParams.MarketId != UserSession.MultiBrochureList.Market)
                UserSession.MultiBrochureList = new MultiBrochureList();

            int partnerId = NhsRoute.PartnerId;
            var model = new CommunityResultsViewModel
                {
                    IsMarketSearch = true,
                    ShowCode = Request.QueryString["ShowCode"] != null,
                    PostalCode = searchParams.PostalCode,
                    CurrentPage = pageIdx
                };


            // set to first page if less than 0
            if (pageIdx < 0)
                pageIdx = 0;

            if (model.Globals.PartnerLayoutConfig.IsBrandPartnerNhsPro)
                model.MarketMapPdfFilePath = GetMarketMapLink(_pathMapper, searchParams.MarketId);

            //Change related with ticket 72251
            _market = _marketService.GetMarket(NhsRoute.PartnerId, searchParams.MarketId, true) ?? _marketService.GetMarket(searchParams.MarketId);
            searchParams.State = _market.StateAbbr;


            GetLocationCoordinates(ref searchParams);

            if (!string.IsNullOrEmpty(searchParams.StateName) && !string.IsNullOrEmpty(searchParams.MarketName))
                //Dont apply extra state filter when market belongs to multiple states or Dfus
                searchParams.StateName = string.Empty;

            searchParams.StartRecord = pageIdx * UserSession.PageSize;
            searchParams.EndRecord = searchParams.StartRecord + UserSession.PageSize - 1;

            searchParams.TypeSearch = searchParams.Radius == 0 ? TypeSearch.ExactLocation : TypeSearch.None;
            //var stopwatch = new Stopwatch();
            //stopwatch.Start();
            model.Results = _communityService.GetCommunityResults(searchParams, refreshCache);

            //stopwatch.Stop();
            //var ts = stopwatch.Elapsed;
            //var performance =string.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);

            // Get the billable status for the all the communities in the current market, true = billed, false = unbilled 
            var commsStatus = new Dictionary<int, bool>();

            if (NhsRoute.IsBrandPartnerNhsPro)
            {
                var commsTable = _builderService.GetMarketBcs(NhsRoute.PartnerId, searchParams.MarketId).AsEnumerable();
                commsStatus = commsTable.ToDictionary(r => r["community_id"].ToType<int>(), r => r["billed"].ToType<string>() == "Y");
            }

            var commResults = GetExtendedCommResults(model.Results.NhsCommunityResults, searchParams, commsStatus);

            model.Communities = commResults.ToPagedList(pageIdx, UserSession.PageSize, model.Results.TotalResults);

            model.IsAOnlyBasicListingsMarket = model.Results.IsAOnlyBasicListingsMarket;
            model.IsCommunityResults = true;

            SetAdditionalExtendedCommunityProperties(model.Communities, searchParams); //Loads aditional properties for paged items only

            model.ListHubBasicListinIds = model.Communities.Where(p => p.BasicListingInfo.FeedProviderId == (int)BasicListingProvide.ListHub).Select(p => p.BasicListingInfo.ListingNumber).ToList().ToJson();
            model.ListHubBasicListinIdsList = model.Communities.Where(p => p.BasicListingInfo.FeedProviderId == (int)BasicListingProvide.ListHub).Select(p => p.BasicListingInfo.ListingId).ToList().ToJson();

            model.BasicListingsIdList = model.Communities.Where(c => c.IsBasicListing).Select(c => c.BuilderId + " " + c.CommunityId).ToArray().Join(",");
            model.BasicCommunitiesIdList = model.Communities.Where(c => c.IsBasicCommunity).Select(c => c.BuilderId + " " + c.CommunityId).ToArray().Join(",");
            model.CommunitiesIdList = model.Communities.Where(c => !c.IsBasicListing && !c.IsBasicCommunity).Select(c => c.BuilderId + " " + c.CommunityId).ToArray().Join(",");

            model.ListHubProviderId = Configuration.ListHubProviderId;
            model.ListHubTestLogging = Configuration.ListHubTestLogging;
            model.Market = _market;
            model.MarketsList = _marketService.GetMarkets(NhsRoute.PartnerId).ToList();
            model.State = _market.StateAbbr;
            if (!string.IsNullOrEmpty(searchParams.PostalCode))
            {
                var cityByPostalCode = _marketService.GetGeoLocationByPostalCode(searchParams.PostalCode);
                model.City = cityByPostalCode == null || string.IsNullOrEmpty(cityByPostalCode.City) ? _market.MarketName : cityByPostalCode.City;
            }
            else
                model.City = _market.MarketName;

            var boylResults = _communityService.GetBoylResults(searchParams.MarketId, partnerId);

            var brands = model.Market.Brands;
            IList<Brand> filteredBrands = null;

            model.TotalBrandsCount = brands.Count;

            if (NhsRoute.IsBrandPartnerNhsPro)
                filteredBrands = brands.Where(b => b.HasBilledCommununities).ToList();


            model.BrandsPromoted = _brandService.GetRandomizedBrands(filteredBrands ?? brands, brands.Count, true, NhsRoute.BrandPartnerId);

            // split brands into 2 colums so we can sort columns vertically
            var displyColumnCount = brands.Count / 2;

            var n = 0;
            foreach (Brand brd in brands)
            {
                if (n == 0 || n < displyColumnCount)
                    model.BrandsCol1.Add(brd);
                //else if (n >= displyColumnCount && n < displyColumnCount * 2)
                else
                    model.BrandsCol2.Add(brd);
                //else
                //    model.BrandsCol3.Add(brd);
                n++;
            }

            _market = model.Market;
            // split zipcodes into 3 columns so we can sort columns vertically
            int itemsCount = int.Parse(Configuration.ZipCodesResultsByMarket);
            IList<int> zipCodes = _marketService.GetZipCodesByMarket(searchParams.MarketId, searchParams.City, itemsCount);
            displyColumnCount = zipCodes.Count / 3;
            n = 0;
            foreach (var zip in zipCodes)
            {
                if (n == 0 || n < displyColumnCount)
                    model.ZipCodesCol1.Add(zip);
                else if (n >= displyColumnCount && n < displyColumnCount * 2)
                    model.ZipCodesCol2.Add(zip);
                else
                    model.ZipCodesCol3.Add(zip);
                n++;
            }


            // split cities into 3 columns so we can sort columns vertically
            displyColumnCount = _market.Cities.Count / 3;
            n = 0;
            foreach (var city in _market.Cities)
            {
                if (n == 0 || n < displyColumnCount)
                    model.CitiesCol1.Add(city);
                else if (n >= displyColumnCount && n < displyColumnCount * 2)
                    model.CitiesCol2.Add(city);
                else
                    model.CitiesCol3.Add(city);
                n++;
            }

            model.CurrentSortOrder = searchParams.SortOrder;

            // Counts 
            model.AllCommunitiesResults = model.Results.AllCommunities;
            model.HotHomesResults = model.Results.HotDealCommunities;
            model.QuickMoveInResults = model.Results.QuickMoveInCommunities;

            // Serialize parametersok
            model.SearchParametersJSon = new JavaScriptSerializer().Serialize(searchParams);

            // Map data
            UpdateMapData(searchParams, ref model);
            // Facets 
            GetLocationFacetPlace(searchParams, ref model);

            if (string.IsNullOrEmpty(searchParams.State))
                searchParams.State = model.Market.StateAbbr;

            SetPersonalCookie(searchParams);

            if (searchParams.SpecialOfferComm == 0 && searchParams.HomeStatus == 0)
            {
                model.SelectedTab = CommunityResultsTabs.AllCommunities;
                model.PageLinkAction = "Show";
            }
            else if (searchParams.SpecialOfferComm == 0 && searchParams.HomeStatus == 5)
            {
                model.SelectedTab = CommunityResultsTabs.QuickMoveIn;
                model.PageLinkAction = "QuickMoveIn";
            }
            else if (searchParams.SpecialOfferComm == 1 && searchParams.HomeStatus == 0)
            {
                model.SelectedTab = CommunityResultsTabs.HotDeals;
                model.PageLinkAction = "HotDeals";
            }
            else
            {
                model.SelectedTab = CommunityResultsTabs.AllCommunities;
                model.PageLinkAction = "Show";
            }

            var pagingRouteData = new RouteValueDictionary { { "market", searchParams.MarketId } };

            if (model.CurrentSortOrder != SortOrder.Random)
            {
                pagingRouteData.Add("sort", model.CurrentSortOrder);
            }
            if (searchParams.BrandId > 0)
            {
                pagingRouteData.Add("brandid", searchParams.BrandId);
            }
            model.PagingRouteData = pagingRouteData;

            //Bind Ads
            foreach (var comm in model.Communities)
            {
                model.Globals.AdController.AddCityParameter(comm.City);
                model.Globals.AdController.AddZipParameter(comm.PostalCode);
            }

            //48044
            UserSession.AdBuilderIds.Clear();
            foreach (var builder in _builderService.GetMarketBuilders(NhsRoute.PartnerId, searchParams.MarketId))
            {
                if (!UserSession.AdBuilderIds.Contains(builder.BuilderId))
                {
                    model.Globals.AdController.AddBuilderParameter(builder.BuilderId);
                    UserSession.AdBuilderIds.Add(builder.BuilderId);
                }
            }

            model.Globals.AdController.AddMarketParameter(searchParams.MarketId);
            model.Globals.AdController.AddPriceParameter(searchParams.PriceLow, searchParams.PriceHigh);
            model.Globals.AdController.AddStateParameter(model.Market.StateAbbr);
            model.IsAjaxRequest = Request.IsAjaxRequest();
            if (_numPostbacks >= Configuration.ActionsPerAdRefresh)
            {
                Random rnd = new Random();
                model.Globals.AdController.RandomNumber = rnd.Next(100000000, 999999999);
                model.Globals.AdController.AllPositions = NhsRoute.CurrentRoute.AdPositions;
                model.AdUpdateTop = model.Globals.AdController.GetIFrameSrc("Top");
                model.AdUpdateMiddle = model.Globals.AdController.GetIFrameSrc("Middle");
                model.AdUpdateMiddle3 = model.Globals.AdController.GetIFrameSrc("Middle3");
                model.AdUpdateRight2 = model.Globals.AdController.GetIFrameSrc("Right2");

                // Reset postback count.  
                _numPostbacks = 0;
                model.NumPostBacks = 0;
            }
            else
            {
                model.NumPostBacks = _numPostbacks + 1;
            }

            if (NhsRoute.IsBrandPartnerNhsPro)
            {
                var commFeaturedResults = this.GetExtendedCommResults(model.Results.FeaturedListings, searchParams, commsStatus);
                SetAdditionalExtendedCommunityProperties(commFeaturedResults, searchParams); //Loads aditional properties for featured listings
                _communityService.GetFeaturedListingsFromCurrentSearch(commFeaturedResults, model.FeaturedListingsTop, model.FeaturedListingsBottom, pageIdx, NhsRoute.PartnerId);                
            }
            else
            {
                // bind featured listings
                _communityService.GetFeaturedListings(_market.MarketId, model.FeaturedListingsTop, model.FeaturedListingsBottom, NhsRoute.PartnerId);
            }

            var featuredListingsIds = model.FeaturedListingsTop.Select(f => f.BuilderId + " " + f.CommunityId + " " + f.FeaturedListingId).ToList();
            featuredListingsIds.AddRange(model.FeaturedListingsBottom.Select(f => f.BuilderId + " " + f.CommunityId + " " + f.FeaturedListingId).ToList());
            model.FeaturedListingsIdList = featuredListingsIds.ToArray().Join(",");

            if (model.Results.TotalCommunities <= 0)
            {
                var clearSearchParams = (PropertySearchParams)searchParams.Clone();
                clearSearchParams.Clear();
                clearSearchParams.MarketId = _market.MarketId;
                model.ClearSearchParametersJSon = clearSearchParams.ToJson();

                var priceLow = _market.PriceLow == 0 ? "0" : String.Format("{0:$###,###}", _market.PriceLow);
                var priceHigh = _market.PriceHigh == 0 ? "0" : String.Format("{0:$###,###}", _market.PriceHigh);
                model.ClearFacetLinkText = _market.TotalCommunities.ToString() + " new communities from " + priceLow + " - " + priceHigh;

                clearSearchParams.SpecialOfferComm = 1;
                model.FindDealsSearchParametersJSon = clearSearchParams.ToJson();
            }

            SetContentManager(ref model, searchParams, pageIdx);

            OverrideDefaultMeta(model, model.SeoContentTags, model.SeoTemplateType);
            //Ticket 44671:Add BOYL builder videos to Community Video Gallery
            foreach (var boylItem in boylResults)
            {
                var commItem = new CommunityResult { BuilderId = boylItem.BuilderId, CommunityId = boylItem.CommunityId };
                model.Results.CommunityResults.Add(commItem);
            }
            model.ShowCaseMediaObjects = model.Results.AllCommunityIds.Any()
                                             ? _communityService.GetCommunityVideoIDs(model.Results.AllCommunityIds,
                                                                                      commsStatus).Distinct().ToList()
                                             : new List<MediaPlayerObject>();

            model.FirstImage = model.ShowCaseMediaObjects.Any() && !string.IsNullOrEmpty(model.ShowCaseMediaObjects.First().Url) ? Configuration.ResourceDomain + model.ShowCaseMediaObjects.First().Url : string.Empty;
            model.Size = new KeyValuePair<int, int>(300, 200);
            model.PageUrl = model.Globals.CurrentUrl;

            model.IsMove = model.Globals.PartnerLayoutConfig.IsBrandPartnerMove;

            model.FindCustomBuilders = new FindCustomBuilders();

            model.Globals.SdcMarketId = model.Market.MarketId;
            return model;
        }

        private static string GetMarketMapLink(IPathMapper pathMapper, int marketId)
        {
            var marketsList = MarketMapsConfigReader.GetMarketsMaps(pathMapper);

            if (marketsList == null || marketsList.Count <= 0) return string.Empty;
            var filteredListByMarket = marketsList.Where(m => m.MarketId == marketId).ToList();
            if (filteredListByMarket.Count <= 0) return string.Empty;
            var currentMarket = filteredListByMarket.First();
            return currentMarket.FileName;
        }

        private FindCustomBuilders GetFindCustomBuilders(CommunityResultsViewModel model)
        {
            var boylResults = _boylService.GetBoylResults(NhsRoute.PartnerId, model.Market.MarketId,
                                                        model.Market.MarketName, model.Market.StateAbbr);
            boylResults = boylResults.OrderBy(row => Guid.NewGuid());

            var data = new FindCustomBuilders
                {
                    Show = true,
                    BuilderCount = boylResults.GroupBy(p => p.BrandName).Count(),
                    MarketName = model.Market.MarketName,
                    MarketId = model.Market.MarketId
                };


            foreach (var boylResult in boylResults.Take(2))
            {
                data.FindCustomBuildersBuilders.Add(new FindCustomBuildersBuilders
                    {
                        BuildeName = boylResult.BrandName,
                        CommunityImage = boylResult.CommThumbnail(),
                        BuildeImage = (boylResult.IsCnh ? Configuration.CNHResourceDomain : Configuration.ResourceDomain) + boylResult.BrandImageThumbnail
                    });
            }
            return data;
        }

        //CommunityResultsViewExt currentResultView
        private IEnumerable<ExtendedCommunityResult> GetExtendedCommResults(IEnumerable<NhsCommunityResult> communityResults, NhsPropertySearchParams searchParams, Dictionary<int, bool> marketCommunitiesBillableStatus)
        {
            var extCommResults = new List<ExtendedCommunityResult>();
            if (!communityResults.Any())
                return extCommResults;

            var videos = _communityService.GetCommunityVideos(communityResults);

            var commIds = (from r in communityResults
                           select (int?)r.CommunityId).ToList();

            var allCommTourVideos = _communityService.GetAllVideoTourImages(commIds).ToList();

            foreach (var result in communityResults)
            {
                var extComm = new ExtendedCommunityResult { CommunityId = result.CommunityId };
                if (result.IsBasicListing && result.BasicListingInfo != null)
                {
                    extComm.BasicListingInfo = new BasicListingInfo
                    {
                        Bathrooms = result.BasicListingInfo.Bathrooms,
                        Bedrooms = result.BasicListingInfo.Bedrooms,
                        ListingId = result.BasicListingInfo.ListingId,
                        BasicListingText = result.BasicListingInfo.BasicListingText,
                        DisplayAddress = result.BasicListingInfo.DisplayAddress,
                        FeedProviderId = result.BasicListingInfo.FeedProviderId,
                        ListingNumber = result.BasicListingInfo.ListingNumber,
                    };
                }

                if (NhsRoute.IsBrandPartnerNhsPro)
                {
                    IEnumerable<PlannerListing> savedPropertysProCrm = UserSession.UserProfile.Planner.GetSavedProperties();
                    extComm.SavedPropertysProCrm = savedPropertysProCrm;

                    #region Coop info

                    var coopInfo = _builderService.GetBuilderCoOpInfo(result.BuilderId, NhsRoute.BrandPartnerId);
                    extComm.HasCoop = coopInfo.IsCoo;

                    #endregion
                }
                extComm.SearchKey = result.SearchKey;
                extComm.IsBasicListing = result.IsBasicListing;
                extComm.IsBasicCommunity = result.IsBasicCommunity;
                extComm.IsBilled = true; // default value when for partners rather than nhspro

                if (extComm.IsBasicCommunity && NhsRoute.IsBrandPartnerNhsPro)
                    extComm.IsBilled = false;
                else
                {
                    if (marketCommunitiesBillableStatus.Keys.Contains(extComm.CommunityId))
                        extComm.IsBilled = marketCommunitiesBillableStatus[extComm.CommunityId];
                }



                extComm.AlertDeletedFlag = result.AlertDeletedFlag;
                extComm.BrandId = result.BrandId;
                extComm.BrandName = result.BrandName;
                extComm.BuilderId = result.BuilderId;
                extComm.BuilderUrl = result.BuilderUrl;
                extComm.BuildOnYourLot = result.AlertDeletedFlag;
                extComm.City = result.City;
                extComm.CommunityId = result.CommunityId;
                extComm.CommunityImageThumbnail = result.CommunityImageThumbnail;
                extComm.AlernativeCommunityImageThumbnail = result.AlernativeCommunityImageThumbnail ?? result.CommunityImageThumbnail;
                extComm.CommunityName = result.CommunityName;
                extComm.CommunityType = result.CommunityType;
                extComm.County = result.County;
                extComm.FeaturedListingId = result.FeaturedListingId > 0 ? result.FeaturedListingId : result.CommunityId;
                extComm.Green = result.Green;
                extComm.HasHotHome = result.HasHotHome;
                extComm.Latitude = result.Latitude;
                extComm.Longitude = result.Longitude;
                extComm.MarketId = result.MarketId;
                extComm.MatchingHomes = result.MatchingHomes;
                extComm.PostalCode = result.PostalCode;
                extComm.PriceHigh = result.PriceHigh;
                extComm.PriceLow = result.PriceLow;
                extComm.PromoId = result.PromoId;
                extComm.HasConsumerPromos = result.HasConsumerPromos;
                extComm.HasAgentPromos = result.HasAgentPromos;
                extComm.State = result.State;
                extComm.StateName = _stateService.GetStateName(result.State);
                extComm.MarketName = _marketService.GetMarketName(NhsRoute.PartnerId, result.MarketId);
                extComm.SubVideoFlag = result.SubVideoFlag;
                extComm.Video_url = result.Video_url;
                extComm.HasVideo = result.SubVideoFlag.ToUpper() == "Y"; //TODO: Change this when we have a way to get video info from WS or DB.
                extComm.HasEvents = result.HasEvents;
                extComm.HasPromos = result.HasPromos;
                extComm.HasConsumerPromos = result.HasConsumerPromos;
                extComm.HasAgentPromos = result.HasAgentPromos;

                if (extComm.HasVideo)
                {
                    extComm.Video = videos.Find(v => v.CommunityId == extComm.CommunityId);

                    if (extComm.Video == null)
                        extComm.CommunityVideoTour = allCommTourVideos.Find(v => v.CommunityId == extComm.CommunityId);
                }

                if (searchParams.PromotionType != PromotionType.Any || searchParams.HasEvent)
                {
                    var promotions = _communityService.GetCommunityDetailPromotions(extComm.CommunityId, extComm.BuilderId).ToList();
                    if (!extComm.IsBilled) //Changes because 58867
                        promotions.RemoveAll(promo => promo.PromoTypeCode != "COM");
                    extComm.Promotions = promotions.Select(p => new Promotion
                    {
                        PromoId = p.PromoID,
                        PromoTextShort = p.PromoTextShort,
                        PromoTextLong = p.PromoTextLong,
                        PromoFlyerUrl = p.PromoFlyerURL,
                        PromoType = p.PromoTypeCode,
                        PromoEndDate = p.PromoEndDate,
                        PromoStartDate = p.PromoStartDate
                    }).ToList();
                    if (extComm.IsBilled)
                    {
                        var events = _communityService.GetCommunityEvents(extComm.CommunityId, extComm.BuilderId).ToList();
                        extComm.Events = events.Select(e => new Event
                        {
                            EventId = e.EventID,
                            Title = e.Title,
                            Description = e.Description,
                            EventFlyerUrl = e.EventFlyerURL,
                            EventType = e.EventTypeCode,
                            EventEndDate = e.EventEndTime,
                            EventStartDate = e.EventStartTime
                        })
                                               .ToList();
                    }

                }
                extCommResults.Add(extComm);
            }
            return extCommResults;
        }


        private void SetAdditionalExtendedCommunityProperties(IEnumerable<ExtendedCommunityResult> communityResults, PropertySearchParams searchParams)
        {
            var thereArePaidCommsInThePage = false;
            _isBasicListingGroup = false;
            _isBasicCommunityGroup = false;
            var basicListingIds = new List<int>();

            var extendedCommunityResults = communityResults as IList<ExtendedCommunityResult> ?? communityResults.ToList();
            foreach (var result in extendedCommunityResults)
            {
                thereArePaidCommsInThePage |= !result.IsBasicListing;
                if (!result.IsBasicListing)
                    result.BrandImageThumbnail = result.GetBrandThumbnail(_brandService.GetBrandById(result.BrandId, NhsRoute.BrandPartnerId));
                else
                    basicListingIds.Add(result.BasicListingInfo.ListingId);

                SetGroupingInfo(result, searchParams);
                result.ShowBasicListingIndicator &= thereArePaidCommsInThePage;
            }

            if (basicListingIds.Count > 0)
                SetBasicListingThumbnails(extendedCommunityResults, basicListingIds);
        }

        private void SetBasicListingThumbnails(IEnumerable<ExtendedCommunityResult> communityResults, IEnumerable<int> basicListingIds)
        {
            var basicListingImages = _basicListingService.GetImagesForBasicListings(basicListingIds);

            foreach (var basicListingId in basicListingIds)
            {
                var image = (from i in basicListingImages
                             where i.ListingId == basicListingId
                             select i).FirstOrDefault();

                var commResult = (from c in communityResults
                                  where c.BasicListingInfo.ListingId == basicListingId
                                  select c).FirstOrDefault();

                if (image != null)
                    commResult.CommunityImageThumbnail = image.LocalPath + ImageSizes.BasicListingHomeThumb + "_" + image.ImageName;
            }
        }

        private void SetGroupingInfo(ExtendedCommunityResult result, PropertySearchParams searchParams)
        {
            if (searchParams.SortOrder != SortOrder.Random)
            {
                switch (searchParams.SortOrder)
                {
                    case SortOrder.Builder:
                        if (_prevGroupValue != result.BrandName && !string.IsNullOrEmpty(result.BrandName))
                        {
                            result.GroupingBarTitle = result.BrandName;
                            _prevGroupValue = result.BrandName;
                        }
                        break;
                    case SortOrder.SpecialOffer:
                        string specialOfferGroupValue;

                        if ((result.PromoId != 0 || result.HasHotHome))
                        {
                            specialOfferGroupValue = "Special Offer";
                        }
                        else
                        {
                            specialOfferGroupValue = "Other Communities";
                        }

                        if (_prevGroupValue != specialOfferGroupValue)
                        {
                            result.GroupingBarTitle = specialOfferGroupValue;
                            _prevGroupValue = specialOfferGroupValue;
                        }

                        break;

                    case SortOrder.HomeMatches:
                        int matchesGroupValue = result.MatchingHomes / 50;

                        if (matchesGroupValue * 50 == result.MatchingHomes)
                        {
                            --matchesGroupValue;
                        }

                        if (_prevGroupValue != matchesGroupValue.ToString())
                        {
                            var groupValueLow = matchesGroupValue * 50;
                            var groupValueHigh = groupValueLow + 50;

                            if (result.IsBasicListing)
                                result.GroupingBarTitle = string.Empty;
                            else if (groupValueLow < 0)
                                result.GroupingBarTitle = "0 Homes";
                            else
                                result.GroupingBarTitle = groupValueLow + 1 + " - " + groupValueHigh + " Homes";

                            _prevGroupValue = matchesGroupValue.ToString();
                        }
                        break;

                    case SortOrder.Location:
                        if (_prevGroupValue != (result.City + ", " + result.State))
                        {
                            result.GroupingBarTitle = result.City + ", " + result.State;
                            _prevGroupValue = result.City + ", " + result.State;
                        }
                        break;
                    case SortOrder.CityRadius:
                        if (!string.IsNullOrWhiteSpace(searchParams.City) ||
                            !string.IsNullOrWhiteSpace(searchParams.CityNameFilter))
                        {
                            string cityRadiusGroupValue;
                            if ((result.City.ToLower() == searchParams.City.ToLower()) ||
                                (result.City.ToLower() == searchParams.CityNameFilter.ToLower()))
                            {
                                cityRadiusGroupValue = result.City + ", " + result.State;
                            }
                            else
                            {
                                cityRadiusGroupValue = "Nearby Communities";
                                result.IsNearbyCommunities = true;
                            }


                            if (_prevGroupValue != cityRadiusGroupValue)
                            {
                                result.GroupingBarTitle = cityRadiusGroupValue;
                                _prevGroupValue = cityRadiusGroupValue;
                            }
                        }
                        break;
                    case SortOrder.CountyRadius:
                        if (!string.IsNullOrWhiteSpace(searchParams.County) ||
                            !string.IsNullOrWhiteSpace(searchParams.CountyNameFilter))
                        {
                            string countyRadiusGroupValue;

                            if ((result.County.ToLower() == searchParams.County.ToLower()) ||
                                (result.County.ToLower() == searchParams.CountyNameFilter.ToLower()))
                            {
                                countyRadiusGroupValue = result.County + " County";
                            }
                            else
                            {
                                countyRadiusGroupValue = "Nearby Communities";
                                result.IsNearbyCommunities = true;
                            }

                            if (_prevGroupValue != countyRadiusGroupValue)
                            {
                                result.GroupingBarTitle = countyRadiusGroupValue;
                                _prevGroupValue = countyRadiusGroupValue;
                            }
                        }
                        break;
                    case SortOrder.ZipRadius:
                        if (!string.IsNullOrWhiteSpace(searchParams.PostalCode.Trim()) ||
                            !string.IsNullOrWhiteSpace(searchParams.PostalCodeFilter.Trim()))
                        {
                            string zipRadiusGroupValue;

                            if ((result.PostalCode.Trim().ToLower() == searchParams.PostalCode.Trim().ToLower()) ||
                                (result.PostalCode.Trim().ToLower() == searchParams.PostalCodeFilter.Trim().ToLower()))
                            {
                                zipRadiusGroupValue = "Zip - " + result.PostalCode.Trim();
                            }
                            else
                            {
                                zipRadiusGroupValue = "Nearby Communities";
                                result.IsNearbyCommunities = true;
                            }

                            if (_prevGroupValue != zipRadiusGroupValue)
                            {
                                result.GroupingBarTitle = zipRadiusGroupValue;
                                _prevGroupValue = zipRadiusGroupValue;
                            }
                        }



                        break;
                    case SortOrder.Price:
                        string priceGroupValue = string.Empty;

                        //#18178 - Show coming soons at the end of the result set
                        if (result.PriceLow == int.MaxValue)
                        {
                            priceGroupValue += "Coming Soon";
                        }
                        else
                            if (result.PriceLow < 60000)
                            {
                                priceGroupValue += "Less than $60,000";
                            }
                            else
                            {
                                if (result.PriceLow >= 1000000)
                                {
                                    priceGroupValue += "From $1,000,000 and up";
                                }
                                else
                                {
                                    int priceGroup = ((result.PriceLow / 20000) * 20000);
                                    priceGroupValue += "Priced from " + String.Format("{0:$###,###}", priceGroup);
                                }
                            }

                        if (_prevGroupValue != priceGroupValue)
                        {
                            result.GroupingBarTitle = priceGroupValue;
                            _prevGroupValue = priceGroupValue;
                        }
                        break;
                }

            }

            if (result.IsBasicListing && !_isBasicListingGroup)
                result.ShowBasicListingIndicator = _isBasicListingGroup = true;

            if (result.IsBasicCommunity && !_isBasicCommunityGroup)
                result.ShowBasicListingIndicator = _isBasicCommunityGroup = true;
        }

        private void SetContentManager(ref CommunityResultsViewModel communityResultsViewModel, NhsPropertySearchParams searchParams, int PageNumber)
        {
            var market = _market;
            var contentTags = new List<ContentTag>();

            if (RouteParams.HotDeals.Value().Equals("true"))
            {
                communityResultsViewModel.SeoTemplateType = SeoTemplateType.Nhs_Market_HotDeals_Srp;
            }
            else if (RouteParams.ComingSoon.Value().Equals("true"))
            {
                communityResultsViewModel.SeoTemplateType = SeoTemplateType.Nhs_Market_Coming_Srp;
            }
            else if (!string.IsNullOrEmpty(searchParams.CommunityName))
            {
                contentTags.Add(new ContentTag { TagKey = ContentTagKey.CommunityName, TagValue = searchParams.CommunityName });

                communityResultsViewModel.SeoTemplateType = SeoTemplateType.Nhs_CommunityName_Srp;
                communityResultsViewModel.CommunityName = searchParams.CommunityName;
            }

            else if (!string.IsNullOrEmpty(searchParams.City) || !string.IsNullOrEmpty(searchParams.CityNameFilter))
            {
                // this is a market/city search
                var city = !string.IsNullOrEmpty(searchParams.City) ? searchParams.City : searchParams.CityNameFilter;
                contentTags.Add(new ContentTag { TagKey = ContentTagKey.CityName, TagValue = city });

                communityResultsViewModel.SeoTemplateType = SeoTemplateType.Nhs_Market_City_Srp;
                communityResultsViewModel.City = city.Replace(" ", "");
            }

            else if (!string.IsNullOrEmpty(searchParams.PostalCode) || !string.IsNullOrEmpty(searchParams.PostalCodeFilter))
            {
                // this is a zipcode search
                // get Zip and ZipPrimaryCity
                var zip = !string.IsNullOrEmpty(searchParams.PostalCode) ? searchParams.PostalCode : searchParams.PostalCodeFilter;
                contentTags.Add(new ContentTag { TagKey = ContentTagKey.ZipPrimaryCity, TagValue = market.MarketName });
                contentTags.Add(new ContentTag { TagKey = ContentTagKey.Zip, TagValue = zip });
                communityResultsViewModel.SeoTemplateType = SeoTemplateType.Nhs_Zip_Srp;
                communityResultsViewModel.Zip = zip;
            }

            else if (!string.IsNullOrEmpty(searchParams.County) || !string.IsNullOrEmpty(searchParams.CountyNameFilter))
            {
                // county search
                // get CountyName
                var county = !string.IsNullOrEmpty(searchParams.County) ? searchParams.County : searchParams.CountyNameFilter;
                communityResultsViewModel.SeoTemplateType = SeoTemplateType.Nhs_County_Srp;
                communityResultsViewModel.County = county.Replace(" ", "");
                communityResultsViewModel.State = searchParams.State;
            }
            else
            {
                communityResultsViewModel.SeoTemplateType = SeoTemplateType.Nhs_Market_Srp;
            }

            //if (string.IsNullOrEmpty(searchParams.City))
            //    searchParams.City = communityResultsViewModel.City;


            // Replace tags
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.MarketId, TagValue = market.MarketId.ToString() });
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.MarketName, TagValue = market.MarketName });
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.MarketState, TagValue = string.IsNullOrEmpty(searchParams.State) ? communityResultsViewModel.State : searchParams.State });
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.MarketStateName, TagValue = market.State.StateName });
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.StateID, TagValue = string.IsNullOrEmpty(searchParams.State) ? communityResultsViewModel.State : searchParams.State });
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.StateName, TagValue = market.State.StateName });
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.CountyName, TagValue = searchParams.County });
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.CityName, TagValue = communityResultsViewModel.City });
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.CityName, TagValue = communityResultsViewModel.City });
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.PageNumber, TagValue = RouteParams.Page.Value() });

            //BEGIN CASE 72478
            //contentTags.Add(new ContentTag { TagKey = ContentTagKey.BedroomCount, TagValue = searchParams.NumOfBeds.ToType<String>() });
            //contentTags.Add(new ContentTag { TagKey = ContentTagKey.BathroomCount, TagValue = searchParams.NumOfBaths.ToType<String>() });
            //contentTags.Add(new ContentTag { TagKey = ContentTagKey.SquareFtRangeLow, TagValue = searchParams.SqFtLow.ToType<String>() });
            //contentTags.Add(new ContentTag { TagKey = ContentTagKey.SquareFtRangeHigh, TagValue = searchParams.SqFtHigh.ToType<String>() });
            //contentTags.Add(new ContentTag { TagKey = ContentTagKey.PriceRangeHigh, TagValue = searchParams.PriceHigh.ToType<String>() });
            //contentTags.Add(new ContentTag { TagKey = ContentTagKey.PriceRangeLow, TagValue = searchParams.PriceLow.ToType<String>() });
            //contentTags.Add(new ContentTag { TagKey = ContentTagKey.CommunityCount, TagValue = communityResultsViewModel.Results.TotalCommunities.ToType<String>() });
            //contentTags.Add(new ContentTag { TagKey = ContentTagKey.HomeCount, TagValue = communityResultsViewModel.Results.TotalHomes.ToType<String>() });
            //contentTags.Add(new ContentTag { TagKey = ContentTagKey.BuilderCount, TagValue = communityResultsViewModel.TotalBrandsCount.ToType<String>() });
            //contentTags.Add(new ContentTag { TagKey = ContentTagKey.SpecCount, TagValue = communityResultsViewModel.QuickMoveInResults.ToType<String>() });
            //contentTags.Add(new ContentTag { TagKey = ContentTagKey.PlanCount, TagValue = (communityResultsViewModel.Results.TotalHomes - communityResultsViewModel.QuickMoveInResults).ToType<string>() });
            //END CASE 72478

            communityResultsViewModel.SeoContentTags = contentTags;
        }

        private void GetLocationFacetPlace(PropertySearchParams searchParams, ref CommunityResultsViewModel model)
        {
            model.LocationHeading = string.Empty;

            // Map Search. 
            model.IsMapSearch = false;
            if (UserSession.GetItem("LastLocation") == null)
                UserSession.SetItem("LastLocation", string.Empty);

            // Zip search. 
            if (!string.IsNullOrEmpty(searchParams.PostalCodeFilter) || !string.IsNullOrEmpty(searchParams.PostalCode))
            {
                var zip = (!string.IsNullOrEmpty(searchParams.PostalCodeFilter)) ?
                                    searchParams.PostalCodeFilter : searchParams.PostalCode;

                model.LocationName = zip + ", " + searchParams.State;
                model.IsMarketSearch = false;
            }
            // County search
            else if (!string.IsNullOrEmpty(searchParams.CountyNameFilter) || !string.IsNullOrEmpty(searchParams.County))
            {

                var county = (!string.IsNullOrEmpty(searchParams.CountyNameFilter))
                                ? searchParams.CountyNameFilter
                                : searchParams.County;

                model.LocationName = county.ToTitleCase() + " County, " + (!string.IsNullOrEmpty(searchParams.State) ? searchParams.State : model.Market.StateAbbr);
                model.IsMarketSearch = false;
            }
            // City search
            else if (!string.IsNullOrEmpty(searchParams.CityNameFilter) || !string.IsNullOrEmpty(searchParams.City))
            {
                var city = (!string.IsNullOrEmpty(searchParams.CityNameFilter))
                                ? searchParams.CityNameFilter
                                : searchParams.City;

                model.LocationName = city.ToTitleCase() + ", " + (!string.IsNullOrEmpty(searchParams.State) ? searchParams.State : model.Market.StateAbbr);
                model.IsMarketSearch = false;
            }
            // Default market search.                     
            else
            {
                // special case for Washington DC....
                if (model.Market.MarketName.ToUpper().EndsWith(" DC") && model.Market.StateAbbr.ToUpper() == "DC")
                {
                    model.LocationName = model.Market.MarketName.Remove(model.Market.MarketName.Length - 3);
                    model.LocationName += ", " + model.Market.StateAbbr + " Area";
                }
                else
                {
                    model.LocationName = model.Market.MarketName + ", " + model.Market.StateAbbr + " Area";
                }
                model.IsMarketSearch = true;
            }

            if (searchParams.ComingSoon)
                model.LocationDescripcion = "COMING SOON COMMUNITIES";

            if (searchParams.MinLatitude != 0 || searchParams.MinLongitude != 0 ||
                searchParams.MaxLatitude != 0 || searchParams.MaxLongitude != 0)
            {
                model.IsMarketSearch = false;
                model.IsMapSearch = true;
                model.LocationName = string.Empty;
                model.LocationHeading = "Your selected map area ";
            }

            model.LocationChanged = UserSession.GetItem("LastLocation").ToString() != model.LocationName;
            UserSession.SetItem("LastLocation", model.LocationName);

            if (!string.IsNullOrEmpty(model.LocationName))
                UserSession.SetItem("Location", model.LocationName);

        }
        //-------------------------------------------------------------------------------------------------------------------------------------
        ///<summary>Get the cordinates for the search, and set it on the searchParams method parameter</summary>
        ///<param name="ref NhsPropertySearchParams searchParams">Collection of parameters for the search</param>
        ///<param name="searchParams"></param>
        ///<returns></returns>
        //-------------------------------------------------------------------------------------------------------------------------------------
        private void GetLocationCoordinates(ref NhsPropertySearchParams searchParams)
        {
            var lastUsedBrandId = Convert.ToInt32(UserSession.GetItem("LastUsedBrandId"));
            var removingBrandIdFilter = (searchParams.BrandId == 0 && lastUsedBrandId != 0);
            var currentCity = string.IsNullOrEmpty(searchParams.City) ? searchParams.CityNameFilter : searchParams.City;

            // If it is radius search dont reset the radios or lat-long
            //Tickets 46973 & 49416: LastUsedCity is used to determinate if the radius Facet was changed
            if (Request.IsAjaxRequest() && UserSession.GetItem("LastUsedRadius") != null &&
                searchParams.Radius != Convert.ToInt32(UserSession.GetItem("LastUsedRadius")) &&
                 currentCity == Convert.ToString(UserSession.GetItem("LastUsedCity")))
            {
                //Ticket 44093: when the radius is 0 we need to pass in the postalcodefilter to the search service.
                if (searchParams.Radius == 0)
                    searchParams.PostalCodeFilter = searchParams.PostalCode;

                if (!string.IsNullOrEmpty(searchParams.PostalCode) && searchParams.BrandId != 0 || removingBrandIdFilter)
                {
                    searchParams.PostalCodeFilter = string.Empty;
                    var pGeo = _mapService.GeoCodePostalCode(searchParams.PostalCode);
                    if (pGeo != null)
                    {
                        searchParams.Radius = pGeo.Radius;
                    }
                }

                //Tickets 57344: BrandId filtering on zip search
                UserSession.SetItem("LastUsedBrandId", searchParams.BrandId);

                UserSession.SetItem("LastUsedCity", currentCity);

                //Tickets 46973 & 49416: Set the last used radius on the radius Facet
                UserSession.SetItem("LastUsedRadius", searchParams.Radius);
                searchParams.Radius = (int)UserSession.GetItem("LastUsedRadius");
                return;
            }

            // Reset the radius when there is a city change via ajax so the page doesnt keep showing the last changed radius
            if (currentCity != UserSession.GetItem("LastUsedCity").ToType<string>())
                searchParams.Radius = 0;

            // CITY
            if (!string.IsNullOrEmpty(searchParams.City))
            {
                var c = _mapService.GeoCodeCityByMarket(searchParams.City, searchParams.MarketId, searchParams.State);

                if (c != null)
                {
                    if (searchParams.Radius == 0)
                        searchParams.Radius = c.Radius;

                    searchParams.State = c.State;
                    searchParams.OriginLat = c.Latitude;
                    searchParams.OriginLong = c.Longitude;
                    searchParams.SortOrder = searchParams.SortOrder == SortOrder.Random
                                            ? SortOrder.CityRadius : searchParams.SortOrder;
                    if (c.Radius == 0)
                        searchParams.CityNameFilter = searchParams.City;
                }
            }
            else
            {
                // CITY NAME FILTER
                if (!string.IsNullOrEmpty(searchParams.CityNameFilter))
                {
                    var c = _mapService.GeoCodeCityByMarket(searchParams.CityNameFilter, searchParams.MarketId, searchParams.State);
                    if (c != null)
                    {
                        if (searchParams.Radius == 0 && searchParams.SortOrder == SortOrder.Random) //67647 - Losing location when Sorting for Specific City
                            searchParams.Radius = c.Radius; //47100 - Set the radius to pre-defined city radius. WS will apply city-name filter as usual.

                        searchParams.State = c.State;
                        searchParams.OriginLat = c.Latitude;
                        searchParams.OriginLong = c.Longitude;
                        searchParams.SortOrder = searchParams.SortOrder == SortOrder.Random
                                                ? SortOrder.CityRadius : searchParams.SortOrder;
                    }
                }
                else
                {
                    // ZIP
                    if (!string.IsNullOrEmpty(searchParams.PostalCode))
                    {
                        var p = _mapService.GeoCodePostalCode(searchParams.PostalCode);
                        if (p != null)
                        {
                            if (searchParams.Radius == 0)
                                searchParams.Radius = p.Radius;

                            searchParams.State = p.State;
                            searchParams.OriginLat = p.Latitude;
                            searchParams.OriginLong = p.Longitude;
                            searchParams.SortOrder = searchParams.SortOrder == SortOrder.Random
                                                    ? SortOrder.ZipRadius : searchParams.SortOrder;
                            if (p.Radius == 0)
                                searchParams.PostalCodeFilter = searchParams.PostalCode;
                        }
                    }
                    else
                    {
                        // COUNTY
                        if (!string.IsNullOrEmpty(searchParams.County))
                        {
                            var county = _mapService.GeoCodeCountyByMarket(searchParams.County,
                                                                                    searchParams.MarketId);
                            if (county != null)
                            {
                                if (searchParams.Radius == 0)
                                    searchParams.Radius = county.Radius;

                                searchParams.State = county.State;
                                searchParams.OriginLat = county.Latitude;
                                searchParams.OriginLong = county.Longitude;
                                searchParams.SortOrder = searchParams.SortOrder == SortOrder.Random
                                                        ? SortOrder.CountyRadius : searchParams.SortOrder;
                                if (county.Radius == 0)
                                    searchParams.CountyNameFilter = searchParams.County;
                            }
                        }
                        else
                        {

                            // MARKET
                            if (searchParams.MarketId != 0)
                            {
                                var market = _mapService.GetMarketInfo(searchParams.MarketId, NhsRoute.PartnerId) ?? _marketService.GetMarket(NhsRoute.PartnerId, searchParams.MarketId, false);

                                //Tickets 46973 & 49416: Set the last radius used in the radius facet, if is null set 0
                                if (market != null)
                                {
                                    searchParams.State = market.State.StateAbbr;
                                    if (market.LatLong != null)
                                    {
                                        searchParams.OriginLat = market.LatLong.Latitude;
                                        searchParams.OriginLong = market.LatLong.Longitude;
                                    }
                                    else
                                    {
                                        searchParams.OriginLat = Convert.ToDouble(market.Latitude);
                                        searchParams.OriginLong = Convert.ToDouble(market.Longitude);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //Tickets 46973 & 49416: Set the last ciity used to search
            UserSession.SetItem("LastUsedCity", currentCity);
            UserSession.SetItem("LastUsedBrandId", searchParams.BrandId);
            UserSession.SetItem("LastUsedRadius", searchParams.Radius);
        }

        private void UpdateMapData(PropertySearchParams searchParams, ref CommunityResultsViewModel model)
        {
            if (model.Map == null)
                model.Map = new MapData();

            if (searchParams.MinLatitude != 0)
            {
                string zoom = RouteParams.Zoom.Value();

                if (!string.IsNullOrEmpty(zoom))
                {
                    model.Map.ZoomLevel = Convert.ToInt32(zoom);
                }

                model.Map.MinLat = searchParams.MinLatitude;
                model.Map.MinLng = searchParams.MinLongitude;
                model.Map.MaxLat = searchParams.MaxLatitude;
                model.Map.MaxLng = searchParams.MaxLongitude;

                var upperLeft = new LatLong(model.Map.MinLat, model.Map.MinLng);
                var bottomRight = new LatLong(model.Map.MaxLat, model.Map.MaxLng);
                var center = upperLeft.GetCenter(bottomRight);

                model.Map.CenterLat = center.Latitude;
                model.Map.CenterLng = center.Longitude;
            }
            else
            {
                model.Map.CenterLat = searchParams.OriginLat;
                model.Map.CenterLng = searchParams.OriginLong;
                switch (searchParams.Radius)
                {
                    case 25:
                        model.Map.ZoomLevel = 9;
                        break;
                    case 15:
                    case 10:
                        model.Map.ZoomLevel = 10;
                        break;
                    case 5:
                        model.Map.ZoomLevel = 11;
                        break;
                    case 3:
                        model.Map.ZoomLevel = 12;
                        break;
                }

            }

            if (model.Map.ZoomLevel == 0)
                model.Map.ZoomLevel = 10;
        }

        private void SetPersonalCookie(PropertySearchParams searchParams)
        {
            var text = string.Empty;
            if (searchParams.MarketId > 0 && !String.IsNullOrEmpty(searchParams.MarketName))
            {
                text = searchParams.MarketName + ", " + searchParams.State + " Area";
            }

            if (!String.IsNullOrEmpty(searchParams.City))
            {
                text = searchParams.City + ", " + searchParams.State;
            }

            if (!String.IsNullOrEmpty(searchParams.CityNameFilter))
            {
                text = searchParams.CityNameFilter + ", " + searchParams.State;
            }

            if (!String.IsNullOrEmpty(searchParams.County))
            {
                text = searchParams.County + " County" + ", " + searchParams.State;
            }

            if (!String.IsNullOrEmpty(searchParams.PostalCode) && searchParams.PostalCode.ToType<int>() > 0)
            {
                text = searchParams.PostalCode + ", " + searchParams.State;
            }
            UserSession.PersonalCookie.SearchText = text;

            var market = _marketService.GetMarket(NhsRoute.PartnerId, searchParams.MarketId, false);
            if (market != null)
            {
                UserSession.PersonalCookie.MarketId = searchParams.MarketId;
                UserSession.PersonalCookie.State = searchParams.State;
                if (String.IsNullOrEmpty(UserSession.PersonalCookie.SearchText))
                {
                    text = market.MarketName + ", " + market.State.StateAbbr + " Area";
                    UserSession.PersonalCookie.SearchText = text;
                }
            }


            UserSession.PersonalCookie.PriceLow = searchParams.PriceLow;
            UserSession.PersonalCookie.PriceHigh = searchParams.PriceHigh;
            UserSession.PersonalCookie.City = searchParams.CityNameFilter;
        }


        #endregion
    }
}
