﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Routing.Interface;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class NhlRedirectsController : ApplicationController
    {
        private IStateService _stateService;
        private IMarketService _marketService;

        public NhlRedirectsController(IStateService stateService, IMarketService marketService)
        {
            _stateService = stateService;
            _marketService = marketService;
        }

        public virtual ActionResult RedirectStateNewHomes(string state)
        {
            return RedirectPermanent(string.Format("~/newhomelistings/StateIndex/state-{0}", state));
        }

        public virtual ActionResult RedirectStateCountyNewHomes(string state, string county)
        {
            var stateName = _stateService.GetStateAbbreviation(state);
            var marketId = _marketService.GetMarketIdFromStateCounty(stateName, county);

            return
                RedirectPermanent(string.Format("~/newhomelistings/communityresults/market-{0}/county-{1}", marketId,
                                                county));
        }

        public virtual ActionResult RedirectStateCityNewHomes(string state, string city)
        {
            var stateName = _stateService.GetStateAbbreviation(state);
            var marketId = _marketService.GetMarketIdFromStateCity(stateName, city);

            return RedirectPermanent(string.Format("~/newhomelistings/communityresults/market-{0}", marketId));
        }

        public virtual ActionResult RedirectGeneric()
        {
            switch (NhsRoute.CurrentRoute.Function)
            {
                case "contact_us":
                    return RedirectPermanent("~/newhomelistings/ContactUs");

                case "about":
                    return RedirectPermanent("~/newhomelistings/AboutUs");

                case "quick-move-in":
                    return RedirectPermanent("~/newhomelistings/QuickMoveInSearch");

                case "privacy":
                    return RedirectPermanent("~/newhomelistings/PrivacyPolicy");

                case "terms":
                    return RedirectPermanent("~/newhomelistings/TermsOfUse");

                case "united_states-new-homes":
                    return RedirectPermanent("~/newhomelistings/SiteIndex");

                case "participate":
                    return RedirectPermanent("~/newhomelistings/ListYourHomes");

                case "who-we-are":
                    return RedirectPermanent("~/newhomelistings/AboutUs");

                default:
                    return RedirectPermanent("~/");
            }
        }
    }
}