﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Route;
using Nhs.Library.Extensions;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Mvc.Routing.Utils;
using Nhs.UrlRewriter.Configuration;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class MobileController : ApplicationController
    {
        private readonly ICommunityService _communityService;
        private readonly IListingService _listingService;
        private readonly IMarketService _marketService;

        public MobileController(ICommunityService communityService, IListingService listingService, IMarketService marketService)
        {
            _communityService = communityService;
            _listingService = listingService;
            _marketService = marketService;
        }

        public virtual ActionResult ShowIPhoneInterstitial()
        {
            // ReSharper disable Asp.NotResolved
            return View(new BaseViewModel());
            // ReSharper restore Asp.NotResolved
        }

        public virtual RedirectResult MobileRedirect(bool isIPhone)
        {
            var nextPage = NhsRoute.GetValue(RouteParams.NextPage);

            //iPhone - Came from interstitial. Set cookie and take to full site
            if (isIPhone)
            {
                var fullSiteCookie = new HttpCookie("ShowFullSiteToUser", "TRUE") { Expires = DateTime.Now.AddMonths(1) };
                Response.Cookies.Add(fullSiteCookie);

                return base.Redirect(string.IsNullOrEmpty(nextPage) ? Pages.Home : nextPage);
            }

            //Not an iPhone - compute target mobile URL
            return base.Redirect(GetMobileUrl(nextPage,RewriterConfiguration.MnhMobileSite));
        }

        private string GetMobileUrl(string nextPage, string mobileSite)
        {
            var function = nextPage.GetFunction();
            var mobileUrl = string.Empty;

            var nextPageParams = RouteHelper.GetRouteByUrl(
                    new Uri(Configuration.NewHomeSourceDomain + RouteParams.NextPage.Value<string>())).Params;

            switch (function)
            {
                case Pages.HomeResults:
                case Pages.HomeResultsv2:
                    var searchParams = UserSession.PropertySearchParameters;

                    var marketIdR = searchParams.MarketId > 0
                        ? searchParams.MarketId
                        : nextPageParams.Value<int>(RouteParams.Market);

                    var state = string.Empty;
                    var area = string.Empty;
                    if (marketIdR == 0)
                    {
                        var rg = nextPage.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

                        foreach (var s in rg)
                        {
                            if (s.Contains(RouteParams.StateName.ToString().ToLower()))
                            {
                                state = s.Replace(RouteParams.StateName.ToString().ToLower() + "-", string.Empty);
                            }

                            if (s.Contains(RouteParams.Area.ToString().ToLower()))
                            {
                                area = s.Replace(RouteParams.Area.ToString().ToLower() + "-", string.Empty);
                            }

                            if (s.Contains(RouteParams.Market.ToString().ToLower()))
                            {
                                marketIdR = s.Replace(RouteParams.Market.ToString().ToLower() + "-", string.Empty).ToType<int>();
                                break;
                            }
                        }
                    }

                    if (marketIdR == 0) //see if urls are DFUs
                    {
                        var stateName = nextPageParams.Value<string>(RouteParams.StateName);
                        var marketName = nextPageParams.Value<string>(RouteParams.Area);

                        if (string.IsNullOrWhiteSpace(stateName))
                        {
                            stateName = state;
                        }

                        if (string.IsNullOrWhiteSpace(marketName))
                        {
                            marketName = area;
                        }

                        if (!string.IsNullOrEmpty(stateName) && !string.IsNullOrEmpty(marketName))
                        {
                            var market = _marketService.GetMarket(NhsRoute.PartnerId, stateName, marketName, false);
                            if (market != null)
                                marketIdR = market.MarketId;
                        }
                    }

                    if (FillCityParam(mobileSite, nextPageParams, marketIdR, ref mobileUrl)) break;

                    mobileUrl = string.Format("{0}/Market.aspx?SiteMarketId={1}", mobileSite, marketIdR);
                    break;
                case Pages.CommunityResults:
                case Pages.CommunityResultsv2:
                    var marketId = nextPageParams.Value<int>(RouteParams.Market);

                    if (marketId == 0) //see if urls are DFUs
                    {
                        var stateName = nextPageParams.Value<string>(RouteParams.StateName);
                        var marketName = nextPageParams.Value<string>(RouteParams.Area);

                        if (!string.IsNullOrEmpty(stateName) && !string.IsNullOrEmpty(marketName))
                        {
                            var market = _marketService.GetMarket(NhsRoute.PartnerId, stateName, marketName, false);
                            if (market != null)
                                marketId = market.MarketId;
                        }
                    }

                    if (FillCityParam(mobileSite, nextPageParams, marketId, ref mobileUrl)) break;

                    mobileUrl = string.Format("{0}/Market.aspx?SiteMarketId={1}", mobileSite, marketId);

                    break;

                case Pages.CommunityDetailMove:
                case Pages.CommunityDetail:
                case Pages.CommunityDetailv1:
                    var communityId = nextPageParams.Value<int>(RouteParams.Community);

                    var community = _communityService.GetCommunity(communityId);
                    if (community != null)
                        mobileUrl = string.Format("{0}/Community.aspx?SiteMarketId={1}&BuilderId={2}&MarketId={1}&CommunityId={3}", mobileSite, community.MarketId, community.BuilderId, community.CommunityId);

                    break;

                case Pages.SpecDetailMove:
                case Pages.PlanDetailMove:
                case Pages.HomeDetail:
                case Pages.HomeDetailv1:
                case Pages.HomeDetailv2:
                case Pages.HomeDetailv3:
                case Pages.HomeDetailv4:
                    var planId = nextPageParams.Value<int>(RouteParams.PlanId);
                    var specId = nextPageParams.Value<int>(RouteParams.SpecId);

                    if (planId > 0) //Plan
                    {
                        var plan = _listingService.GetPlan(planId);
                        if (plan != null)
                            mobileUrl = string.Format("{0}/Home.aspx?SiteMarketId={1}&BuilderId={2}&MarketId={1}&Communityid={3}&PlanId={4}", mobileSite, plan.Community.MarketId, plan.Community.BuilderId, plan.CommunityId, planId);

                    }
                    else if (specId > 0) //Spec
                    {
                        var spec = _listingService.GetSpec(specId);
                        if (spec != null)
                            mobileUrl = string.Format("{0}/Home.aspx?SiteMarketId={1}&BuilderId={2}&MarketId={1}&Communityid={3}&PlanId={4}&SpecId={5}", mobileSite, spec.Community.MarketId, spec.Community.BuilderId, spec.CommunityId, spec.PlanId, specId);

                    }

                    break;
                default:
                    mobileUrl = mobileSite;
                    break;
            }

            return BuildMobileUrlWithQueryString(string.IsNullOrEmpty(mobileUrl) ? mobileSite : mobileUrl, nextPage);
        }

        private static bool FillCityParam(string mobileSite, IList<RouteParam> nextPageParams, int marketId, ref string mobileUrl)
        {
            var city = nextPageParams.Value<string>(RouteParams.CityNameFilter);

            if (string.IsNullOrWhiteSpace(city) == false)
            {
                mobileUrl = string.Format("{0}/Market.aspx?SiteMarketId={1}&city={2}", mobileSite, marketId, city);
                return true;
            }
            return false;
        }

        private string BuildMobileUrlWithQueryString(string mobileUrl, string nextPage)
        {
            var queryStringList = (from key in Request.QueryString.AllKeys
                                   where key != "nextpage" &&
                                   key != "isIphone"
                                   select (key + "=" + Request.QueryString[key])).ToList();

            GetNextPageParameters(nextPage, queryStringList);

            var queryString = string.Join("&", queryStringList);

            var retUrl = string.IsNullOrWhiteSpace(queryString)
                ? mobileUrl
                : mobileUrl.Contains("?")
                    ? string.Format("{0}&{1}", mobileUrl, queryString)
                    : string.Format("{0}?{1}", mobileUrl, queryString);

            return retUrl.TrimEnd(new[] { '&' });
        }

        private void GetNextPageParameters(string nextPage, List<string> queryStringList)
        {
            var nextPageValues = nextPage.Split(new[] { '?' }, StringSplitOptions.RemoveEmptyEntries);
            var nextPageList = new List<string>();

            if (nextPageValues != null && nextPageValues.Count() > 1)
            {
                nextPageList.AddRange(nextPageValues[1].Split(new[] { '&' }, StringSplitOptions.RemoveEmptyEntries));
            }

            if (nextPageList.Any())
            {
                queryStringList.AddRange(nextPageList);
            }
        }

        public virtual RedirectResult ShowFullSite()
        {
            var nextPage = NhsRoute.GetValue(RouteParams.NextPage);

            var fullSiteCookie = new HttpCookie("ShowFullSiteToUser", "TRUE") { Expires = DateTime.Now.AddMonths(1) };
            Response.Cookies.Add(fullSiteCookie);

            return base.Redirect(string.IsNullOrEmpty(nextPage) ? Pages.Home : nextPage);
        }

        public virtual RedirectResult ShowMobileSite()
        {
            var nextPage = NhsRoute.GetValue(RouteParams.NextPage);

            var fullSiteCookie = new HttpCookie("ShowFullSiteToUser", "FALSE") { Expires = DateTime.Now.AddMonths(1) };
            Response.Cookies.Add(fullSiteCookie);

            return base.Redirect(string.IsNullOrEmpty(nextPage) ? Pages.Home : nextPage);
        }
    }
}
