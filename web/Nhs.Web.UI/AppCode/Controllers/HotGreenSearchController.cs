﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{    

    public partial class HotGreenSearchController : ApplicationController
    {
        private const string Hotdeals = "hotdeals";
        #region Member variables
        private readonly IHotGreenSearchService _hgService;
        private readonly IStateService _stateService;
        private readonly IMarketService _marketService;
        private readonly IPathMapper _pathMapper;
        private readonly ILookupService _lookupService;        
        #endregion

        #region Constructor
        public HotGreenSearchController(IHotGreenSearchService hgService, IPathMapper pathMapper, IStateService stateService, ILookupService lookupService, IMarketService marketService)
            : base(pathMapper)
        {
            _hgService = hgService;
            _pathMapper = pathMapper;            
            _stateService = stateService;
            _lookupService = lookupService;
            _marketService = marketService;
        }
        #endregion

        #region Actions
        //
        // GET: /Search/
        public virtual ActionResult Show()
        {            
            var viewModel = GetModel("");
            AddCanonicalTag(viewModel);
            return View(viewModel);
        }


        [HttpPost]
        public virtual ActionResult Show(HotGreenSearchViewModel model)
        {            
            return Redirect(GetUrlForRedirect(model));            
        }

        #endregion

        #region Private Methods

        private string GetUrlForRedirect(HotGreenSearchViewModel model)
        {
            string redirectUrl;
            var paramz = new List<RouteParam>();
            var uselocation = false;
            var searchParams = new SearchParams();
            
            model.SearchType = GetSearchType(Request.Url.AbsolutePath);
            searchParams.Init(true);
            UserSession.SearchParametersV2 = searchParams;

            if (!string.IsNullOrEmpty(model.PostalCode))
            {
                uselocation = true;

                // Used by the new WebApi service that community results consumes
                UserSession.SearchParametersV2.SearchLocation = model.PostalCode;
                paramz.Add(new RouteParam(RouteParams.SearchText, model.PostalCode, RouteParamType.QueryString, true, true));
                
                if (!string.IsNullOrEmpty(model.StateAbbr))
                {
                    paramz.Add(new RouteParam(RouteParams.State, string.IsNullOrEmpty(model.StateAbbr) ? string.Empty : model.StateAbbr, RouteParamType.Friendly, true, true));
                    UserSession.SearchParametersV2.State = string.IsNullOrEmpty(model.StateAbbr) ? "" : model.StateAbbr;
                }
                
                //paramz.Add(new Param(NhsLinkParams.ToPage, Pages.CommunityResults, UrlParamType.QueryString, true, true));
                UserSession.PersonalCookie.SearchText = model.PostalCode;
                UserSession.PersonalCookie.State = string.IsNullOrEmpty(model.StateAbbr) ? "" : model.StateAbbr;

                int postalCode;
                UserSession.SearchParametersV2.MarketId = int.TryParse(model.PostalCode, out postalCode) 
                    ? _marketService.GetMarketIdFromPostalCode(model.PostalCode, NhsRoute.PartnerId) 
                    : _marketService.GetMarketIdFromStateCity(model.StateAbbr, model.PostalCode);
            }
            else
            {
                paramz.Add(new RouteParam(RouteParams.State, model.StateAbbr, RouteParamType.Friendly, true, true));
                UserSession.PersonalCookie.State = string.IsNullOrEmpty(model.StateAbbr) ? "" : model.StateAbbr;

                // Used by the new WebApi service that community results consumes
                UserSession.SearchParametersV2.State = model.StateAbbr;
            }

            //Price From
            if (!string.IsNullOrEmpty(model.PriceLow.ToString()) && model.PriceLow.ToString() != "0")
            {
                paramz.Add(new RouteParam(RouteParams.PriceLow, model.PriceLow.ToString(), RouteParamType.Friendly, true, true));
                UserSession.PersonalCookie.PriceLow = int.Parse(model.PriceLow.ToString());

                // Used by the new WebApi service that community results consumes
                UserSession.SearchParametersV2.PriceLow = Convert.ToInt32(model.PriceLow.ToString(CultureInfo.InvariantCulture));
            }

            //Price To
            if (!string.IsNullOrEmpty(model.PriceHigh.ToString()) && model.PriceHigh.ToString() != "0")
            {
                paramz.Add(new RouteParam(RouteParams.PriceHigh, model.PriceHigh.ToString(), RouteParamType.Friendly, true, true));
                UserSession.PersonalCookie.PriceHigh = int.Parse(model.PriceHigh.ToString());

                // Used by the new WebApi service that community results consumes
                UserSession.SearchParametersV2.PriceHigh = Convert.ToInt32(model.PriceHigh.ToString(CultureInfo.InvariantCulture));
            }

            if (model.SearchType == "green")
            {
                UserSession.SearchType = SearchTypeSource.GreenCommunityResults;
                UserSession.PropertySearchParameters.HomeStatus = 0;
                paramz.Add(new RouteParam(RouteParams.Green, "true"));

                // Used by the new WebApi service that community results consumes
                UserSession.SearchParametersV2.Green = true;
            }
            else if (model.SearchType == "hot")
            {
                UserSession.SearchType = SearchTypeSource.HotDealsSearchCommunityResults;
                UserSession.PropertySearchParameters.SpecialOfferComm = 1;
                UserSession.PropertySearchParameters.HomeStatus = 0;
                paramz.Add(new RouteParam(RouteParams.HotDeals, "true", RouteParamType.QueryString, true, true));

                // Used by the new WebApi service that community results consumes
                UserSession.SearchParametersV2.HotDeals = true;
            }

            redirectUrl = uselocation ? paramz.ToUrl(Pages.LocationHandler).ToLower() : paramz.ToUrl(Pages.CommunityResults).ToLower();
            return redirectUrl;
        }

        private string GetSearchType(string url)
        {
            string res = string.Empty;
            string param = url.ToLower();
            string referUrl = string.Empty;
            
            if (HttpContext.Request.UrlReferrer != null)
            {
                referUrl = HttpContext.Request.UrlReferrer.AbsoluteUri;
            }            

            if (referUrl.Contains(Hotdeals))
            {
                res = "hot";
            }
            else if (param.Contains("greensearch"))
            {
                res = "green";
            }
            else if (param.Contains(Hotdeals))
            {
                res = "hot";
            }

            return res;
        }

        private HotGreenSearchViewModel GetModel(string state)
        {
            string st = string.Empty;
            
            string schtype = GetSearchType(Request.Url.AbsolutePath);
            string localpage =  (schtype == "green") ? Pages.GreenSearch : ((schtype == "hot") ? Pages.HotDealsSearch : Pages.QuickMoveInSearch);

            var stateList = _hgService.GetHGStates(NhsRoute.PartnerId);
            
            stateList.Insert(0, new State { StateAbbr = "", StateName = LanguageHelper.ChooseState });

            if (!string.IsNullOrEmpty(UserSession.PersonalCookie.State) && state == "")
            {
                foreach (State listItem in stateList)
                {
                    if (listItem.StateAbbr.Trim().ToLower() == UserSession.PersonalCookie.State.ToLower())
                    {
                        st = UserSession.PersonalCookie.State;
                    }
                }
            }
            else
            {
                st = string.IsNullOrEmpty(state) ? stateList[0].StateAbbr : state;
            }            


            var priceLow = UserSession.PersonalCookie.PriceLow;
            var priceHigh = UserSession.PersonalCookie.PriceHigh;
            var priceRange = _lookupService.GetCommonListItems(CommonListItem.PriceRange);
            string flashsource = string.Empty;

            SpotlightCommunitiesViewModel spotmodel = new SpotlightCommunitiesViewModel();
            Spotlight spotElement = new Spotlight();
            

            if (schtype == "green")
            {
                flashsource = string.Format("{0}/GlobalResources/Default/swf/{1}", Configuration.ResourceDomain, "NHS-Green-Carousel");
                spotmodel.GreenHomes = true;
                spotElement.GreenHomes = true;
            }
            else if (schtype == "hot")
            {
                flashsource = string.Format("{0}/GlobalResources/Default/swf/{1}", Configuration.ResourceDomain, "NHS-HotDeal-Carousel");
                spotmodel.HotHomes = true;
                spotElement.HotHomes = true;
                spotElement.Filter = string.Format("({0}={1} OR {2}<>{3})", CommunityDBFields.HasHotHome, 1, CommunityDBFields.PromoID, 0);
            }
                        
            spotmodel.SpotList = ConvertToExtendedCommResults(spotElement.SetSpotlight());

            spotmodel.FlashSpot = flashsource;
            
            
            var viewModel = new HotGreenSearchViewModel
            {
                StateAbbr = st,
                StateList = stateList,
                SearchType = schtype,
                LowPriceRange = new SelectList(priceRange, "LookupValue", "LookupText", priceLow),
                HighPriceRange = new SelectList(priceRange, "LookupValue", "LookupText", priceHigh),
                FlashSpot = flashsource,
                SpotModel = spotmodel
            };

            
            //// Register Meta Tags information
            List<ContentTag> paramz = new List<ContentTag>();
            paramz.Add(new ContentTag
            {
                TagKey = ContentTagKey.MarketName,
                TagValue = "Austin"
            });

            MetaReader metaReader = new MetaReader(_pathMapper);
            var metas = metaReader.GetMetaTagInformation(localpage, paramz.ToDictionary(), PageHeaderSectionName.SeoMetaPages);
            OverrideDefaultMeta(viewModel, metas);
            
            return viewModel;
        }



        private List<ExtendedCommunityResult> ConvertToExtendedCommResults(List<CommunityResult> communities)
        {
            List<ExtendedCommunityResult> extCommResults = new List<ExtendedCommunityResult>();

            foreach (var result in communities)
            {
                extCommResults.Add(new ExtendedCommunityResult
                {
                    BrandId = result.BrandId,
                    BrandName = result.BrandName,
                    BuilderId = result.BuilderId,
                    BuilderUrl = result.BuilderUrl,
                    BuildOnYourLot = result.AlertDeletedFlag,
                    City = result.City,
                    CommunityId = result.CommunityId,
                    CommunityImageThumbnail = result.CommunityImageThumbnail,
                    CommunityName = result.CommunityName,
                    CommunityType = result.CommunityType,
                    County = result.County,
                    FeaturedListingId = result.FeaturedListingId,
                    Green = result.Green,
                    HasHotHome = result.HasHotHome,
                    Latitude = result.Latitude,
                    Longitude = result.Longitude,
                    MarketId = result.MarketId,
                    MatchingHomes = result.MatchingHomes,
                    PostalCode = result.PostalCode,
                    PriceHigh = result.PriceHigh,
                    PriceLow = result.PriceLow,
                    PromoId = result.PromoId,
                    State = result.State,
                    SubVideoFlag = result.SubVideoFlag,
                    Video_url = result.Video_url,
                    HasVideo = result.SubVideoFlag.ToUpper() == "Y",
                    MarketName = _marketService.GetMarketName(NhsRoute.PartnerId, result.MarketId),
                    StateName = _stateService.GetStateName(result.State)
                });
            }
            return extCommResults;
        }
        #endregion


    }
}
