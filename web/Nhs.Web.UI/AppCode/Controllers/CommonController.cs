﻿using System;
using System.Linq;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class CommonController : ApplicationController
    {
        private readonly IMarketService _marketService;

        public CommonController(IMarketService marketService, IPathMapper pathMapper)
            : base(pathMapper)
        {
            _marketService = marketService;
        }

        public virtual JsonResult SetRefer(string refVal, string refUrl)
        {
            if (!string.IsNullOrEmpty(refVal))
            {
                UserSession.DynamicRefer = refVal;
                UserSession.Refer = refVal;
            }

            if (!string.IsNullOrEmpty(refUrl))
            {
                UserSession.DynamicReferUrl = refUrl;
            }

            return Json(new { status = "ok" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult ShowSyntheticGeographiesReport()
        {
            var syntheticCollection = new SyntheticGeoCollectionViewModel();
            var synthetics = SyntheticGeoReader.GetSynthetics();

            foreach (var syntheticViewModel in synthetics.Select(syntheticGeo => new SyntheticGeoViewModel
            {
                SyntheticGeography = syntheticGeo,
                PostalCodes = _marketService.GetZipCodesForSynthetic(syntheticGeo)
            }))
            {
                syntheticCollection.SyntheticGeoCollection.Add(syntheticViewModel);
            }

            return ValidateUser() ? View(NhsMvc.Default.Views.SyntheticGeographies.ShowReportPage, syntheticCollection)
                                  : View(NhsMvc.Default.Views.SyntheticGeographies.ShowAccessDenied, syntheticCollection);
        }

        #region Private Methods

        private bool ValidateUser()
        {
            var isValidUser = UserSession.UserProfile.ActorStatus == WebActors.ActiveUser &&
                              string.Equals(UserSession.UserProfile.LogonName, Configuration.SyntheticGeographiesReportUser, StringComparison.InvariantCultureIgnoreCase) &&
                              UserSession.UserProfile.Password == Configuration.SyntheticGeographiesReportPassword;
            return isValidUser;
        }

        #endregion
    }
}
