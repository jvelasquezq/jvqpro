﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Content;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class EBookController : ApplicationController
    {
        private readonly IEBookService _iEBookService;
        private readonly INewsletterService _newsletterService;
        public EBookController(IEBookService iEBookService, INewsletterService newsletterService)
        {
            _iEBookService = iEBookService;
            _newsletterService = newsletterService;
        }

        //
        // GET: /eBook/
        [HttpGet]
        public virtual ActionResult Show()
        {
            // Ticket 81990 Do not display the ebook for CNA
            if (!Configuration.ShowEBook || PartnerLayoutHelper.GetPartnerLayoutConfig().IsBrandPartnerCna)
            {
                return RedirectPermanent(Pages.Home, new List<RouteParam>());
            }
            var model = new BaseViewModel();
            return View(model);
        }

        //
        // GET: /eBook/Email
        [HttpGet]
        public virtual ActionResult ShowEmail(string email)
        {
            var model = new EBookViewModel();
              model.ReplacementTags=new List<ContentTag>();
              model.ReplacementTags.Add(new ContentTag(ContentTagKey.SiteLogo, Resources.GlobalResourcesMvc.Pro.images.logo_png));
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.Email, email));
            model.ReplacementTags.Add(new ContentTag(ContentTagKey.ResourceDomain, Configuration.ResourceDomain));

            return View(model);
        }

        //
        // GET: /eBook/Register
        public virtual JsonResult EBookRegister(string email, string source)
        {
            var isAValidRegister = _iEBookService.CreateeBookLead(email, NhsRoute.PartnerId, source);
            CookieManager.ShowEbookSection = !isAValidRegister;
            return Json(isAValidRegister, JsonRequestBehavior.AllowGet);
        }

        // GET: /eBook/AdRegister
        public virtual JsonResult EBookAdRegister(string email, string source, int partnerId)
        {
            _iEBookService.CreateeBookLead(email, partnerId, source);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /eBook/modal
        public virtual ActionResult ShowEbookModal(string email, int marketId)
        {
            var marketList = _newsletterService.GetMarkets(NhsRoute.PartnerId);

            var model = new EBookViewModel
            {
                MarketList = marketList.OrderBy(p=>p.MarketName).Select(x => new SelectListItem
                {
                    Value = x.MarketId.ToString(),
                    Text = x.MarketName + @", " + x.State.StateName,
                    Selected = x.MarketId == marketId
                }),
                Email = email,
                MarketId = marketId
            };
            if (NhsRoute.IsMobileDevice)
                return PartialView(NhsMvc.Default.ViewsMobile.eBook.ShowEbookModal, model);
            return PartialView(NhsMvc.Default.Views.eBook.ShowEbookModal, model);
            //return View(NhsMvc.Default.Views.eBook.ShowEbookModal, model);
        }

        //
        // GET: /eBook/modal
        [HttpPost]
        public virtual JsonResult ShowEbookModal(bool homeOfTheWeek, bool weeklyMarketUpdate, int marketId, string email)
        {
            var isAValidRegister = _iEBookService.SignUpeBookModal(homeOfTheWeek, weeklyMarketUpdate, marketId, email,
                NhsRoute.PartnerId, NhsRoute.BrandPartnerId, NhsRoute.PartnerSiteUrl.ToLower());
            return Json(isAValidRegister, JsonRequestBehavior.AllowGet);
            
        }

        //
        // GET: /eBook/notshowmore
        public virtual JsonResult NotShowMore()
        {
            CookieManager.ShowEbookSection = false;
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /eBook/ChangeEBookState
        public virtual JsonResult ChangeEBookState()
        {
            UserSession.ShowEbookSection = false;
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}
