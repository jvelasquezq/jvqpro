﻿using System.Collections.Generic;
using System.Web.Mvc;
using Nhs.Library.Web;
using Nhs.Mvc.Routing.Interface;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Utility.Common;
using System.Linq;
using Bankrate;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Mvc.Routing.Configuration;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class HomeController
    {
        #region Actions
        public virtual ActionResult MobileSearch(string searchText, Location location = null)
        {
            //Remove the parameter name, form the list f parameter, because was creating issues in the ur for CR
            var param = NhsRoute.CurrentRoute.Params.FirstOrDefault(p => p.Name == RouteParams.Name);
            if (param != null)
                NhsRoute.CurrentRoute.Params.Remove(param);

            var priceLow = NhsLinkParams.PriceLow.GetValue<decimal>();
            var priceHigh = NhsLinkParams.PriceHigh.GetValue<decimal>();
            var bathrooms = NhsLinkParams.NumOfBaths.GetValue<string>();
            var bedrooms = NhsLinkParams.NumOfBeds.GetValue<string>();

            return !string.IsNullOrEmpty(searchText) ? Show(searchText, "", priceLow.ToType<string>(), priceHigh.ToType<string>(), bedrooms, bathrooms)
                                                     : LocationHanlderRedirect(location, priceLow, priceHigh, "");
        }
        #endregion

        #region Private Methods
        //Get the view model for the home page
        private HomeViewModel GetHomeViewModelMobile()
        {
            var viewModel = new HomeViewModel();
            viewModel.ShowWelcomeMessage = NhsLinkParams.Welcome.GetValue<bool>() && UserSession.UserProfile.IsLoggedIn();
            if (viewModel.ShowWelcomeMessage)
            {
                viewModel.UserName = UserSession.UserProfile.FirstName;
            }

            if (UserSession.PersonalCookie.Lat.ToType<decimal>() != 0 ||
                UserSession.PersonalCookie.Lng.ToType<decimal>() != 0)
            {
                viewModel.IsCurrentLocation = true;
                viewModel.SpotLightHomes = GetSpotLightHomes(UserSession.PersonalCookie.Lat,
                    UserSession.PersonalCookie.Lng);
            }
            else
            {
                viewModel.SpotLightHomes = _partnerService.GetSpotLightHomes(NhsRoute.PartnerId,UserSession.PersonalCookie.MarketId);
            }

            OverrideDefaultMeta(viewModel, new List<ContentTag>(), GetSeoTemplate(),
                new List<MetaTagFacebook> { FacebookHelper.GetFacebookMetaAppIdFacebook() });
            return viewModel;
        }

        public List<SpotLightHomes> GetSpotLightHomes(double originLat, double originLng)
        {
            var searchParams = new SearchParams
            {
                PageNumber = 1,
                PageSize = 25,
                OriginLat = originLat,
                OriginLng = originLng,
                Radius = 25
            };

            var data = _apiService.GetResultsWebApi<SpotLightHomes>(searchParams, SearchResultsPageType.HomeResults);
            var home = data == null || data.Result == null ? new List<SpotLightHomes>() : data.Result.ToRandom().ToList();
            return home;
        }
        #endregion
    }
}
