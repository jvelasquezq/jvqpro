﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Web;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class ResourceCenterController
    {
        public virtual ActionResult ShowCategory(string categoryName)
        {
            var friendlyCategoryName = categoryName.Replace("-", " ").Trim();

            //Validate if request category exist
            if (_cmsService.CatagoryExist(friendlyCategoryName, NhsRoute.BrandPartnerId) == false)
            {
                return RedirectTo404();
            }

            //Articles
            var articles = _cmsService.GetArticlesByCategory(friendlyCategoryName,
                Configuration.EktronResourceCenterTaxonomyId, Configuration.EktronLanguageId);

            //Sub Categories
            var subCats = _cmsService.GetSubCategories(friendlyCategoryName);

            //Get Global Config
            var globalConfig = _cmsService.GetBusinessConfiguration(NhsRoute.BrandPartnerId,
                                                                    EktronConstants.GlobalSettingsArticleName);

            var model = new ResourceCenterViewModels.CategoryViewModel
                {
                    GlobalBusinessConfig = globalConfig,
                    CategoryName = friendlyCategoryName.ToTitleCase(),
                    IsCategoryPage = true,
                    SubCategories = subCats != null
                                        ? subCats.ToList().Select(x => x.TaxonomyName)
                                        : new List<string>(),
                    FeaturedArticles = articles.OrderByDescending(p => p.Promoted).ThenBy(p => p.SequenceNumber).ThenBy(p => p.Title),
                    SlideShow = _cmsService.GetCategorySlideShow(friendlyCategoryName, NhsRoute.BrandPartnerId),
                     ParentCategories =
                        _cmsService.GetParentCategoriesByCategoryName(friendlyCategoryName)
                                   .OrderBy(c => c.CategoryId)
                                   .ToList()
                };

            if (!NhsRoute.ShowMobileSite)
            {
                model.TrendingNow =
                    _cmsService.GetTrendingArticles(Configuration.EktronResourceCenterTaxonomyId,
                        Configuration.EktronLanguageId).ToList();

            }
            //Metas
            var metas = _cmsService.GetMetasForCategory(friendlyCategoryName, Configuration.EktronResourceCenterTaxonomyId, Configuration.EktronLanguageId);
            base.OverrideDefaultMeta(model, metas);

            UserSession.SetItem("LastCMSCategory", friendlyCategoryName);

            //Canonical
            if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
                AddCanonicalWithExcludeParams(model, new List<string>());
            else
                AddProResourceCenterCanonicalTag(model);

            //Carry through ads from other pages into Resource Center
            SetupAdParameters(model);

            model.Globals.AdController.AddCategoryParameter(friendlyCategoryName);
            return View(model);
        }
    }
}
