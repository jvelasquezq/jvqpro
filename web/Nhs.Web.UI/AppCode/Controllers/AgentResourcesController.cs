﻿using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.Controllers
{
    /// <summary>
    /// A controller to support alternate urls using the same actions - Refer RouteConfig_Global.xml
    /// </summary>
    public partial class AgentResourcesController : ResourceCenterController
    {
        public AgentResourcesController(IPathMapper pathMapper, IPartnerService partnerService, IStateService stateService, ICmsService cmsService):base(pathMapper, partnerService, stateService, cmsService)
        {
            
        }        
    }
}