﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Bankrate;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Extensions;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class MortgageController : ApplicationController
    {
        private IListingService _listingService;
        private ICommunityService _communityService;
        private IPartnerService _partnerService;
        private IPathMapper _pathMapper;

        public MortgageController(IListingService listingService, ICommunityService communityService, IPartnerService partnerService, IPathMapper pathMapper)
        {
            _communityService = communityService;
            _listingService = listingService;
            _partnerService = partnerService;
            _pathMapper = pathMapper;
        }

        public virtual ActionResult RedirectMortgageTable()
        {
            var planId = RouteParams.PlanId.Value<Int32>();
            var specId = RouteParams.SpecId.Value<Int32>();

            var urlParams = new List<RouteParam>();
            urlParams.Add(specId > 0
                              ? new RouteParam(RouteParams.SpecId, specId, RouteParamType.Friendly, true, true)
                              : new RouteParam(RouteParams.PlanId, planId, RouteParamType.Friendly, true, true));

            return base.RedirectPermanent(Pages.MortgageTable, urlParams);
        }

        public virtual ActionResult RedirectMortgageRates()
        {
            var communityId = RouteParams.Community.Value<Int32>();

            var urlParams = new List<RouteParam>();
            urlParams.Add(new RouteParam(RouteParams.Community, communityId, RouteParamType.Friendly, true, true));

            return base.RedirectPermanent(Pages.MortgageRates, urlParams);
        }

        public virtual ActionResult ShowMortgagePayments()
        {            
            var model = GetModel();
            SetNoIndexNoFollowRobotsToModel(model);
            SetupAdParameters(model);
            return View(model);
        }

        public virtual ActionResult ShowMortgageTable()
        {         
            var model = GetModel();
            SetNoIndexNoFollowRobotsToModel(model);            

            return View(model);
        }

        private void BindAds(ref MortgageViewModel model, Community community)
        {
            model.Globals.AdController.AddMarketParameter(model.MarketID);
            model.Globals.AdController.AddCommunityParameter(community.CommunityId);
            model.Globals.AdController.AddPriceParameter(community.PriceLow.ToType<int>(), community.PriceHigh.ToType<int>());
            model.Globals.AdController.AddStateParameter(community.StateAbbr);
            model.Globals.AdController.AddBuilderParameter(model.BuilderID);
            model.Globals.AdController.AddCityParameter(community.City);
            model.Globals.AdController.AddZipParameter(community.PostalCode);
        }

        private void BindAds(ref MortgageViewModel model, decimal price, string state)
        {
            model.Globals.AdController.AddMarketParameter(model.MarketID);
            model.Globals.AdController.AddCommunityParameter(model.CommunityID);
            model.Globals.AdController.AddPriceParameter(price.ToType<int>(), price.ToType<int>());
            model.Globals.AdController.AddStateParameter(state);
            model.Globals.AdController.AddBuilderParameter(model.BuilderID);
            model.Globals.AdController.AddCityParameter(model.CommunityCity);
            model.Globals.AdController.AddZipParameter(model.UserPostalCode);
        }
        
        public virtual ActionResult GetRates(MortgageViewModel mmodel)
        {                                                            
            var model = new MortgageRatesViewModel(GetModel(false));
            model.CommunityID = mmodel.CommunityID;
            model.PointID = mmodel.PointID;
            model.ProductID = mmodel.ProductID;
            model.DownPay = mmodel.DownPay;
            model.PostalCode = mmodel.PostalCode;
            LoadBankrateTable(model);
            
            return PartialView(NhsMvc.Default.Views.Mortgage.GetRates, model);
        }

        public virtual ActionResult GetMatches(MortgageViewModel mmodel)
        {
            var model = new MortgageRatesViewModel(GetModel(false));
            model.CommunityID = mmodel.CommunityID;
            model.PointID = mmodel.PointID;
            model.ProductID = mmodel.ProductID;
            model.DownPay = mmodel.DownPay;
            model.PostalCode = mmodel.PostalCode;
            LoadMatches(model);
            
            return PartialView(NhsMvc.Default.Views.Mortgage.MatchesFound, model);
        }
        

        public virtual ActionResult ShowMortgageRates()
        {
            var model= new MortgageRatesViewModel(GetModel(false));
            var communityId = RouteParams.Community.Value<Int32>();
            if (communityId != 0)
                model.CommunityID = communityId;
            LoadBankrateTable(model);

            SetNoIndexRobotsToModel(model);

            return View(model);
        }
        
        public virtual ActionResult ShowMortgageCalculator()
        {
            var model = new MortgageCalculatorViewModel();
            var zip = "00000";
            decimal price = 0;

            if (UserSession.PersonalCookie != null &&!string.IsNullOrEmpty(UserSession.PersonalCookie.ZipCode))
                zip = UserSession.PersonalCookie.ZipCode;

            if (UserSession.PersonalCookie != null)
                price = UserSession.PersonalCookie.LastPrice;

            model.MortgageScriptUrl = SetURLIframeForCalculator(price, zip);
            SetupAdParameters(model);
            return View(model);
        }

        private string BuildLoadMatchesInfo(RateTableData mtg)
        {
            const string pattern = @"PhoneIcon.gif";
            Regex r = new Regex(pattern, RegexOptions.IgnoreCase);
            MatchCollection matches = r.Matches(mtg.FormattedData);

            return matches.Count + " matches found";
        }

        protected void SetNoIndexNoFollowRobotsToModel(BaseViewModel model)
        {
            var metaRegistrar = new MetaRegistrar(_pathMapper);
            var metas = metaRegistrar.GetRobotsMeta("noindex, nofollow");
            AddNoIndex(metaRegistrar, metas.ToList());
            OverrideDefaultMeta(model, metas);
        }

        private string BuildLoanHeaderInfo(double downPayment, int productId, string points)
        {            
            string res = string.Empty;

            string loaninfo = string.Format("{0:c0}", downPayment) + " down and " + points + " points";

            switch (productId)
            {
                case (1):
                    res = "for 30 Year Fixed with " + loaninfo;
                    break;
                case (2):
                    res = "for 15 Year Fixed with " + loaninfo;
                    break;
                case (6):
                    res = "for 5 Year ARM with " + loaninfo;
                    break;
                case (390):
                    res = "for Interest Only 5 Year ARM with " + loaninfo;
                    break;
                case (387):
                    res = "for Fixed - 20 year with " + loaninfo;
                    break;
                case (8):
                    res = "for ARM 3/1 with " + loaninfo;
                    break;
                case (9):
                    res = "for ARM 7/1 with " + loaninfo;
                    break;
                case (10):
                    res = "for ARM 10/1 with " + loaninfo;
                    break;
                case (389):
                    res = "for Interest Only ARM 3/1 with " + loaninfo;
                    break;
                case (447):
                    res = "for Interest Only ARM 7/1 with " + loaninfo;
                    break;
                case (565):
                    res = "for Interest Only Fixed 30-year with " + loaninfo;
                    break;
            }

            return res;
        }

        private double Parse(string input)
        {
            try
            {
                return Double.Parse(Regex.Replace(input, @"[^\d\.]", string.Empty));
            }
            catch
            {
                return 0.00;
            }
        }
        
      
        private MortgageViewModel GetModel(bool setiframe = true)
        {
            var mortgage = new MortgageViewModel();
            var planId = RouteParams.PlanId.Value<Int32>();
            var specId = RouteParams.SpecId.Value<Int32>();
            var communityId = RouteParams.Community.Value<Int32>();
            
                        
            Decimal price = 0;

            string zip = "00000";
            Boolean IsCondo = false;
            if (!specId.Equals(0) || !communityId.Equals(0) || !planId.Equals(0))
            {
                if (!specId.Equals(0))
                {
                    var spec = _listingService.GetSpec(specId);
                    if (spec == null)
                    {
                        mortgage.SpecID = specId;
                        return mortgage;
                    }
                    var community = spec.Community ?? _communityService.GetCommunity(spec.CommunityId);
                    mortgage = MortgagetableBindSpecData(spec, community);
                    IsCondo = spec.Plan.Condo == 1;
                    if (!string.IsNullOrEmpty(spec.PostalCode.Trim()))
                        zip = spec.PostalCode.Trim();
                    if (spec.Price > 0)
                        price = spec.Price;
                }
                else if (!communityId.Equals(0))
                {
                    var community = _communityService.GetCommunity(communityId);
                    if (community == null)
                    {
                        mortgage.CommunityID = communityId;
                        return mortgage;
                    } 

                    mortgage = MortgagetableBindCommunityData(community);
                    IsCondo = community.IsCondo.ToType<bool>();
                    if (community.PriceLow > 0)
                        price = community.PriceLow;
                    if (!string.IsNullOrEmpty(community.PostalCode))
                        zip = community.PostalCode.Trim();
                }
                else if (!planId.Equals(0))
                {
                    var Plan = _listingService.GetPlan(planId);
                    if (Plan == null)
                    {
                        mortgage.PlanID = planId;
                        return mortgage;
                    } 
                    var community = Plan.Community ?? _communityService.GetCommunity(Plan.CommunityId);
                    mortgage = MortgagetableBindPlanData(Plan, community);
                    IsCondo = Plan.Condo == 1;
                    if (!string.IsNullOrEmpty(community.PostalCode))
                        zip = community.PostalCode.Trim();
                    if (Plan.Price > 0)
                        price = Plan.Price;
                }
                mortgage.Function = NhsRoute.CurrentRoute.Function.ToLower();
                if (setiframe)
                    mortgage.IframeSRC = SetURLIframeForTable(price, zip, IsCondo);
                mortgage.HasData = true;                
            }

            return mortgage;
        }

        private MortgageViewModel MortgagetableBindPlanData(IPlan plan, Community community)
        {
            var mortgage = new MortgageViewModel();
            mortgage.IsPlan = true;
            mortgage.CommunityID = community.CommunityId;
            mortgage.PlanID = plan.Id;
            mortgage.MarketID = community.MarketId;
            mortgage.Name = plan.PlanName;
            mortgage.BuilderName = community.Builder == null ? "" : community.Builder.BuilderName;
            mortgage.MarketName = plan.Community.Market.MarketName;
            mortgage.Address = plan.Community.Address1;
            mortgage.City = plan.Community.City;
            mortgage.CommunityCity = community.City;
            mortgage.CommunityName = community.CommunityName;
            mortgage.State = plan.Community.State.StateAbbr;
            mortgage.StateName = plan.Community.State.StateName;
            mortgage.PostalCode = plan.Community.PostalCode;
            mortgage.ImageUrl = plan.PlanImages != null && plan.PlanImages.Count() > 0 ? plan.PlanImages.FirstOrDefault().ImagePath + ImageSizes.Small + "_" + plan.PlanImages.FirstOrDefault().ImageName : community.BannerImage;
            mortgage.Price = plan.Price;
            mortgage.Baths = plan.Bathrooms.ToType<int>();
            mortgage.Baths = plan.HalfBaths.ToType<int>();
            mortgage.Bedrooms = plan.Bedrooms.ToType<int>();
            mortgage.Garages = plan.Garages.ToType<string>();
            mortgage.BuilderID = community.BuilderId;

            BindAds(ref mortgage, plan.Price, community.StateAbbr);

            return mortgage;
        }

        private MortgageViewModel MortgagetableBindSpecData(ISpec spec, Community community)
        {
            var mortgage = new MortgageViewModel();
            mortgage.IsSpec = true;
            mortgage.CommunityID = community.CommunityId;
            mortgage.SpecID = spec.SpecId;
            mortgage.MarketID = community.MarketId;
            mortgage.Name = spec.PlanName;
            mortgage.BuilderName = community.Builder == null ? "" : community.Builder.BuilderName;
            mortgage.MarketName = community.Market.MarketName;
            mortgage.Address = community.Address1;
            mortgage.City = spec.City;
            mortgage.CommunityCity = community.City;
            mortgage.CommunityName = community.CommunityName;
            mortgage.State = spec.Community.StateAbbr;
            mortgage.PostalCode = spec.PostalCode;
            mortgage.ImageUrl = spec.SpecImages != null && spec.SpecImages.Count() > 0 ? spec.SpecImages.FirstOrDefault().ImagePath + ImageSizes.Small + "_" + spec.SpecImages.FirstOrDefault().ImageName : community.BannerImage;
            mortgage.Price = spec.Price;
            mortgage.Baths = spec.Bathrooms.ToType<int>();
            mortgage.Baths = spec.HalfBaths.ToType<int>();
            mortgage.Bedrooms = spec.Bedrooms.ToType<int>();
            mortgage.Garages = spec.Garages.ToType<string>();
            mortgage.BuilderID = community.BuilderId;

            BindAds(ref mortgage, spec.Price, community.StateAbbr);

            return mortgage;
        }
        private MortgageViewModel MortgagetableBindCommunityData(Community community)
        {
            var mortgage = new MortgageViewModel();
            mortgage.IsCommunity = true;
            mortgage.BuilderID = community.Builder == null ? 0 : community.Builder.BuilderId;
            mortgage.CommunityID = community.CommunityId;
            mortgage.Name = community.CommunityName;
            mortgage.BuilderName = community.Brand == null ? "" : community.Brand.BrandName;
            mortgage.MarketName = community.Market.MarketName;
            mortgage.Address = community.Address1;
            mortgage.CommunityName = community.CommunityName;
            mortgage.City = community.City;
            mortgage.ImageUrl = community.BannerImage;
            mortgage.State = community.State.StateAbbr;
            mortgage.PostalCode = community.PostalCode;
            mortgage.PriceLow = community.PriceLow;
            mortgage.PriceHigh = community.PriceHigh;
            mortgage.Price = community.PriceLow;
            mortgage.SqFtLow = community.SqFtLow;
            mortgage.SqFtHigh = community.SqFtHigh;
            mortgage.MarketID = community.MarketId;

            BindAds(ref mortgage, community);

            return mortgage;
        }

        private string SetURLIframeForTable(Decimal price, string zip, Boolean IsCondo)
        {
            var data = new Dictionary<string, object>{
                                                        {"siteid", Configuration.ICanBuyApiToken},
                                                        {"size", "840"},
                                                        {"ltype", "purc"},
                                                        {"transaction", "52"},
                                                        {"property_type", (IsCondo ? "39" : "34")}
                                                     };
            if (price > 0)
                data.Add("home_value", price);

            if (zip != "00000")
                data.Add("zip", zip);

            var listparams = data.Select(param => string.Format("{0}={1}", param.Key, param.Value).Trim()).ToArray();
            string scr = string.Format("{0}?{1}", string.Format("{0}{1}", Configuration.ICanBuyApiUrl.Trim(), (NhsRoute.CurrentRoute.Function.ToLower() == Pages.MortgagePayments) ? "mortgage-calculator" : ""), string.Join("&", listparams)).Trim(); ;//"http://mortgagerates.icanbuy.com/";

            return scr;
        }

        private string SetURLIframeForCalculator(Decimal price = 0, string zip = "00000")
        {
            var data = new Dictionary<string, object>{
                                                        {"siteid", Configuration.ICanBuyApiToken},
                                                        {"layout", "widget300"},
                                                        {"placeholder", "nhs_CalculatorBox"},
                                                        {"transaction", "52"}
                                                     };
            if (price > 0)
                data.Add("home_value", price);

            if (zip != "00000")
                data.Add("zip", zip);

            var listparams = data.Select(param => string.Format("{0}={1}", param.Key, param.Value).Trim()).ToArray();
            string scr = string.Format("{0}?{1}", string.Format("{0}{1}", Configuration.ICanBuyApiUrl.Trim(), (NhsRoute.CurrentRoute.Function.ToLower() == Pages.MortgagePayments) ? "mortgage-calculator" : ""), string.Join("&", listparams)).Trim(); ;//"http://mortgagerates.icanbuy.com/";

            return scr;
        }

        public void LoadBankrateTable(MortgageRatesViewModel model)
        {
            int bankrateApiPartnerId = Convert.ToInt16(Configuration.BankrateApiPartnerId);
            var bankrateApiWebCode = Configuration.BankrateApiWebCode;
            var divBankrateTable = string.Empty;
            
            //disable columns
            var mc = new MortgageColumnEnum[1];
            mc[0] = MortgageColumnEnum.Lock;
            RateTableData mtg;
            
            string prodIdStr = !string.IsNullOrEmpty(model.ProductID) ? model.ProductID : "1";
            string pointIdStr = !string.IsNullOrEmpty(model.PointID) ? model.PointID : "1";
                        

            if (!prodIdStr.Equals("other"))
            {
                int prodId = string.IsNullOrEmpty(model.ProductID) ? 1 : Convert.ToInt16(model.ProductID);
                int pointId = string.IsNullOrEmpty(model.PointID) ? 1 : Convert.ToInt16(model.PointID);

                double loan;
                double downPayment;
                if (string.IsNullOrEmpty(pointIdStr))
                {
                    loan = Convert.ToDouble(model.Price) * 0.8;
                    downPayment = Convert.ToDouble(model.Price) * 0.2;
                }
                else
                {
                    downPayment = Convert.ToDouble(model.DownPay);
                    loan = Convert.ToDouble(model.Price) - downPayment;
                }

            var rt = new RateRetrieval();
                var mpo = MortgagePointOptions.Zero;

                var points = string.Empty;
                switch (pointId)
                {
                    case (1):
                        mpo = MortgagePointOptions.Zero;
                        points = "0";
                        break;
                    case (2):
                        mpo = MortgagePointOptions.ZeroToOne;
                        points = "0.1 to 1.0";
                        break;
                    case (3):
                        mpo = MortgagePointOptions.OneToTwo;
                        points = "1.1 to 2.0";
                        break;
                    case (6):
                        mpo = MortgagePointOptions.All;
                        points = "All";
                        break;
                }
                try
                {
                    mtg = rt.MortgageRatesByZip(bankrateApiPartnerId, bankrateApiWebCode, prodId,
                                                              model.PostalCode, mpo,
                                                              7, loan, false, Format.HTML, mc);
                    if (mtg != null)
                    {                        
                        model.BankRateTableSrc = mtg.FormattedData;
                        model.PLoanMatches = BuildLoadMatchesInfo(mtg);
                        
                    }
                }
                catch
                {            
                    model.BankRateTableSrc = @"Sorry, the mortgage rate is not available at this moment, please try again later.";
                    model.PLoanMatches = "No matches found";
                }


            }
            else
            {                
                model.BankRateTableSrc = @"Please select a loan type.";
            }
        }


        public void LoadMatches(MortgageRatesViewModel model)
        {
            int bankrateApiPartnerId = Convert.ToInt16(Configuration.BankrateApiPartnerId);
            var bankrateApiWebCode = Configuration.BankrateApiWebCode;
            var divBankrateTable = string.Empty;

            //disable columns
            var mc = new MortgageColumnEnum[1];
            mc[0] = MortgageColumnEnum.Lock;
            RateTableData mtg;

            string prodIdStr = !string.IsNullOrEmpty(model.ProductID) ? model.ProductID : "1";
            string pointIdStr = !string.IsNullOrEmpty(model.PointID) ? model.PointID : "1";


            if (!prodIdStr.Equals("other"))
            {
                int prodId = string.IsNullOrEmpty(model.ProductID) ? 1 : Convert.ToInt16(model.ProductID);
                int pointId = string.IsNullOrEmpty(model.PointID) ? 1 : Convert.ToInt16(model.PointID);

                double loan;
                double downPayment;
                if (string.IsNullOrEmpty(pointIdStr))
                {
                    loan = Convert.ToDouble(model.Price) * 0.8;
                    downPayment = Convert.ToDouble(model.Price) * 0.2;
                }
                else
                {
                    downPayment = Convert.ToDouble(model.DownPay);
                    loan = Convert.ToDouble(model.Price) - downPayment;
                }

                var rt = new RateRetrieval();
                var mpo = MortgagePointOptions.Zero;

                string points = string.Empty;
                switch (pointId)
                {
                    case (1):
                        mpo = MortgagePointOptions.Zero;
                        points = "0";
                        break;
                    case (2):
                        mpo = MortgagePointOptions.ZeroToOne;
                        points = "0.1 to 1.0";
                        break;
                    case (3):
                        mpo = MortgagePointOptions.OneToTwo;
                        points = "1.1 to 2.0";
                        break;
                    case (6):
                        mpo = MortgagePointOptions.All;
                        points = "All";
                        break;
                }
                try
                {
                    mtg = rt.MortgageRatesByZip(bankrateApiPartnerId, bankrateApiWebCode, prodId,
                                                              model.PostalCode, mpo,
                                                              7, loan, false, Format.HTML, mc);
                    if (mtg != null)
                    {                        
                        model.PLoanMatches = BuildLoadMatchesInfo(mtg);                        
                    }
                }
                catch
                {                    
                    model.PLoanMatches = "No matches found";                    
                }


            }
            
        }

        
    }
}
