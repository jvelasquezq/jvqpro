﻿using System.Web.Mvc;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class CommunityRecommendationsController : BaseDetailController
    {
        private readonly ICommunityService _communityService;

        public CommunityRecommendationsController(ICommunityService communityService,  IPathMapper pathMapper, IListingService listingService)
            : base(communityService, listingService, pathMapper)
        {
            _communityService = communityService;
        }

        public virtual ActionResult ShowConfirmation(int community, int total,int select)
        {
            var communityData = _communityService.GetCommunity(community);
            var model = new RecommendedCommunitiesViewModal();
            var conversionIFrameSource = TrackingScriptsHelper.GetConversionTrackerOnPageRecoCommunity(total, select);
            if (communityData != null)
            {
                model = new RecommendedCommunitiesViewModal
                    {
                        MarketId = communityData.MarketId,
                        MarketName = communityData.Market.MarketName,
                        UrlIframe = conversionIFrameSource
                    };
            }

            return View(model);
        }


    }
}
