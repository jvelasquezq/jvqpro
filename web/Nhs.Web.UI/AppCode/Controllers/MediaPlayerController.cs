using System.Linq;
using System.Web.Mvc;
using Nhs.Library.Constants.Route;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers.Brightcove;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Mvc.Routing.Configuration;

namespace Nhs.Web.UI.AppCode.Controllers
{
    /// <summary>
    /// Map controller, class that returns any data requested by the mapping funtionality
    /// </summary>
    public partial class MediaPlayerController : ApplicationController
    {
        private ICommunityService _communityService;

        public MediaPlayerController(ICommunityService communityService, IPathMapper pathMapper)
            : base(pathMapper)
        {
            _communityService = communityService;
        }

        public virtual ActionResult Show()
        {
            var model = new MediaPlayerViewModel();
            var communityId = RouteParams.CommunityId.Value<int>();
            if (communityId != 0)
            {
                var comm = _communityService.GetCommunity(communityId);
                model.CommunityName = comm.CommunityName;
                model.PriceFormated = StringHelper.PrettyPrintRange(comm.PriceLow, comm.PriceHigh, "c0");
                model.Address = comm.City + ", " + comm.State.StateName + " " + comm.PostalCode;
                model.PinterestDescription = string.Format("{0} by {1} in {2}, {3}", comm.CommunityName, comm.Brand.BrandName, comm.City, comm.State.StateName).Replace("'", "\\'");
                model.CommunityId = RouteParams.CommunityId.Value<int>();
                
                var externalLinks = _communityService.GetVideoTourImages(communityId).Select(media => new MediaPlayerObject
                {
                    Type = MediaPlayerObjectTypes.Link,
                    SubType = MediaPlayerObjectTypes.SubTypes.ExternalVideo,
                    Title = media.ImageTitle.EscapeSingleQuote(),
                    Url = media.OriginalPath,
                    Sort = media.ImageSequence.ToType<int>()
                }).OrderBy(m => m.Type).ThenBy(m => m.Sort);

                model.MediaObjects = _communityService.GetMediaPlayerObjects(communityId).ToList();

                var bcVideos = BCAPI.FindVideosByReferenceIds(model.MediaObjects.Where(v=> string.IsNullOrEmpty(v.VideoID)).ToList().Select(v => v.VideoID).ToList());

                model.MediaObjects.ForEach(v =>
                                               {
                                                   if (v.Type == "i" && !string.IsNullOrEmpty(v.Url) && v.Url.IndexOf("?") != -1)
                                                   {
                                                       v.Url = v.Url.Substring(0, v.Url.IndexOf("?"));
                                                   }
                                                   var video = bcVideos.Where(bcV => bcV.referenceId == v.VideoID).FirstOrDefault();
                                                   if (video != null)
                                                   {
                                                       v.Url = video.videoStillURL;
                                                       v.Thumbnail = video.thumbnailURL;
                                                       v.VideoID = video.id.ToString();
                                                   }
                                               });

                model.MediaObjects.AddRange(externalLinks);
            }


            model.MediaObjects.ForEach(v => { if (v.Type == "l") v.TypeDescription = MediaPlayerObjectTypes.MediaDescriptions[string.Format("{0}-{1}", v.Type, v.SubType)]; });
            return View(model);
        }
    }
}
