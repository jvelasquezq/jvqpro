﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Filters;
using Nhs.Web.UI.AppCode.ViewModels;
using StructureMap;


namespace Nhs.Web.UI.AppCode.Controllers
{    
    [ResourceCenter()]
    public partial class ResourceCenterController : ApplicationController
    {
        #region Biz Services
        private readonly IStateService _stateService;
        private readonly IPartnerService _partnerService;
        private readonly ICmsService _cmsService;
        #endregion

        #region Constructor

        public ResourceCenterController(IPathMapper pathMapper, IPartnerService partnerService, IStateService stateService, ICmsService cmsService)
            : base(pathMapper)
        {
            _stateService = stateService;
            _partnerService = partnerService;
            _cmsService = cmsService;
        }

        protected ResourceCenterController():base(ObjectFactory.GetInstance<IPathMapper>())
        {            
            _stateService = ObjectFactory.GetInstance<IStateService>();
            _partnerService = ObjectFactory.GetInstance<IPartnerService>();
            _cmsService = ObjectFactory.GetInstance<ICmsService>();                        
        }

        #endregion

        #region Actions

        public virtual ActionResult ShowHome()
        {
            var model = GetHomeViewModel("");
            if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
                AddCanonicalTag(model);
            else
                AddProResourceCenterCanonicalTag(model);

            //Carry through ads from other pages into Resource Center
            SetupAdParameters(model);

            model.Globals.AdController.AddArticleParameter("Home");

            return View(model);
        }

        public virtual ActionResult ShowAuthorLandingPage()
        {
            var taxonomyId = Configuration.EktronResourceCenterTaxonomyId;
            int languageId = Configuration.EktronLanguageId;
            var model = new AuthorLandingPageViewModel()
            {
                Authors = _cmsService.GetAllAuthorsInfo(taxonomyId, languageId),
                LandingPageInfo = _cmsService.GetAuthorsLandingPage(taxonomyId, languageId),
              };
            if (!NhsRoute.ShowMobileSite)
            {
                model.TrendingNow =
                    _cmsService.GetTrendingArticles(Configuration.EktronResourceCenterTaxonomyId,
                        Configuration.EktronLanguageId).ToList();
            }
            //Metas
            var metas = _cmsService.GetMetasForCategory("authors", Configuration.EktronResourceCenterTaxonomyId, Configuration.EktronLanguageId);
            base.OverrideDefaultMeta(model, metas);

            UserSession.SetItem("LastCMSCategory", "authors");

            //Canonical
            if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
                AddCanonicalWithExcludeParams(model, new List<string>());
            else
                AddProResourceCenterCanonicalTag(model);

            //Carry through ads from other pages into Resource Center
            SetupAdParameters(model);

            model.Globals.AdController.AddCategoryParameter("authors");
            return View(model);
        }

        public virtual ActionResult ShowAuthorPage(string author)
        {
            if (string.IsNullOrWhiteSpace(author))
            {
                return RedirectToAction("ShowHome", "ResourceCenter");
            }

            var friendlyArticleName = author.Replace("-", " ");
            long articleId;
            var xml = _cmsService.GetArticleByTitle(friendlyArticleName, out articleId);

            var model = BuildAuthorModel(friendlyArticleName, xml, articleId);

            switch (model.ArticleSmartFormType)
            {
                case EktronConstants.Author:
                    return View(model);
                default:
                    return RedirectToAction("ShowHome", "ResourceCenter");
            }
        }

        #endregion

        #region Private Methods

        private ResourceCenterViewModels.AuthorPageViewModel BuildAuthorModel(string friendlyArticleName, string xml, long articleId)
        {
            var author = CmsReader.ParseAuthor(xml, articleId, friendlyArticleName);
            var authorId = articleId;
            ResourceCenterViewModels.AuthorPageViewModel model = null;

            if (author.Active)
            {
                model = new ResourceCenterViewModels.AuthorPageViewModel
                {
                    AuthorInfo = author,
                    Id = authorId,
                    RelatedArticles =
                        _cmsService.GetRelatedArticlesByAuthorName(author.Name, Configuration.EktronResourceCenterTaxonomyId, Configuration.EktronLanguageId),
                    ArticleSmartFormType = _cmsService.GetArticleSmartFormType(articleId)
                };

                if (!NhsRoute.ShowMobileSite)
                {
                    model.TrendingNow =
                        _cmsService.GetTrendingArticles(Configuration.EktronResourceCenterTaxonomyId,
                            Configuration.EktronLanguageId).ToList();
                }
            }
            else
            {
                model = new ResourceCenterViewModels.AuthorPageViewModel()
                {
                    ArticleSmartFormType = string.Empty
                };
            }

            return model;
        }

        private ResourceCenterViewModels.HomeViewModel GetHomeViewModel(string searchText)
        {
            var viewModel = new ResourceCenterViewModels.HomeViewModel
                {
                    SlideShow = _cmsService.GetHomePageSlideShow(NhsRoute.BrandPartnerId),
                    Categories = _cmsService.GetCategories(NhsRoute.BrandPartnerId),
                    Globals = { PartnerInfo = _partnerService.GetPartner(NhsRoute.PartnerId) },
                    MktSearch = searchText,
                    IsResourceGuidePage = true,
                };

            if (!NhsRoute.ShowMobileSite)
            {
                viewModel.TrendingNow = _cmsService.GetTrendingArticles(Configuration.EktronResourceCenterTaxonomyId,
                    Configuration.EktronLanguageId).ToList();
            }
            //Metas
            var metas = _cmsService.GetMetasForHomePage(Configuration.EktronResourceCenterTaxonomyId, Configuration.EktronLanguageId);
            base.OverrideDefaultMeta(viewModel, metas);
            return viewModel;
        }
        #endregion        
        
    }
}
