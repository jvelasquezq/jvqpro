﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nhs.Web.UI.AppCode.ViewModels;
using System.Web.Mvc;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class ScmController : ApplicationController
    {
        public virtual ActionResult ShowArticle(string articleTitle)
        {
            var vm = new ScmViewModels.ArticleViewModel();
            vm.ArticlePath = "Articles/" + articleTitle + ".html";
            return View(vm);
        }
    }
}
