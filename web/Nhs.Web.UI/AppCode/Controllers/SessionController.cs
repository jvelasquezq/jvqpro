﻿using System.Web.Mvc;
using Nhs.Library.Web;
using Nhs.Web.UI.AppCode.ViewModels.Session;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class SessionController : ApplicationController
    {
        public virtual ActionResult GetSession()
        {
            var model = new SessionViewModel
            {
                Email = UserSession.UserProfile.Email,
                FirstName = UserSession.UserProfile.FirstName,
                LastName = UserSession.UserProfile.LastName,
                SessionId = UserSession.SessionId
            };
            return View(model);
        }

        [HttpPost]
        public virtual ActionResult GetSessionPost()
        {

            var model = new SessionViewModel
            {
                Email = UserSession.UserProfile.Email,
                FirstName = UserSession.UserProfile.FirstName,
                LastName = UserSession.UserProfile.LastName,
                SessionId = UserSession.SessionId
            };
            return PartialView(NhsMvc.Default.Views.Session.Session,model);
        }
    }
}