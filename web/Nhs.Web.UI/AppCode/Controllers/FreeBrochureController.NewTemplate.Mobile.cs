﻿using System.Web.Mvc;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels.LeadView;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class FreeBrochureController
    {
        [HttpPost]
        public virtual ActionResult RequestBrochureMobile(LeadViewModalModel model)
        {
            string requestUniqueKey;
            int recoCount;
            var google = SendLead(model, out requestUniqueKey, out recoCount);
            var conversionIFrameSource = TrackingScriptsHelper.GetConversionTrackerUrlForModalsMobile(recoCount,model.FromPage);
            return RedirectThankYouModal(google, conversionIFrameSource, model.Email, model.Name.FirstPart(" "), model.IsFreeBrochure,
                model.IsAppointment, model.IsSpecialOffer, model.IsAddMeToInterestList);
        }

        public virtual ActionResult ContactBuilderModal(FreeBrochureLinkInformation model)
        {
            return PartialView(NhsMvc.Default.ViewsMobile.FreeBrochure.ContactBuilderModal, model);
        }
    }
}
