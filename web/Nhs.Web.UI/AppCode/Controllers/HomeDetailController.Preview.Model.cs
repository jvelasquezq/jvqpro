﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Kendo.Mvc.Extensions;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects;
using Nhs.Search.Objects.Constants;
using Nhs.Utility.Common;
using Nhs.Utility.Html;
using Nhs.Web.UI.AppCode.Extensions;
using Nhs.Web.UI.AppCode.ViewModels;
using System.Collections.Generic;
using Nhs.Library.Constants.Route;
using Nhs.Library.Helpers;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Web.UI.AppCode.Helper;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class HomeDetailController
    {
        public HomeDetailViewModel GetHomeDetailPreviewModel(int planId, int specId)
        {
            _listingService.UseHub = true;
            _communityService.UseHub = true;
            var listingServiceHelper = new ListingServiceHelper { UseHub = true };

            IPlan plan = null;
            ISpec spec = null;
            if (planId > 0)
                plan = _listingService.GetPlan(planId);
            else
            {
                spec = _listingService.GetSpec(specId);
                plan = spec.HubPlan ?? _listingService.GetPlan(spec.PlanId);
            }

            var sqFtDisplay = string.Empty;
            var storyDisplay = string.Empty;
            var showSaveToAccount = true;
            var homeBasicSpecs = new StringBuilder();


            var isPlan = spec == null;
            var infoAreaTitle = isPlan ? string.Format("{0} by {1}", plan.PlanName, plan.HubCommunity.Builder.BuilderName) : string.Format("{0} at {1}", plan.PlanName, spec.Address1);

            var bedRooms = isPlan ? plan.Bedrooms.ToType<int>() : spec.Bedrooms.ToType<int>();
            var bathRooms = isPlan ? plan.Bathrooms.ToType<int>() : spec.Bathrooms.ToType<int>();
            var garage = isPlan ? plan.Garages.ToType<decimal>() : spec.Garages.ToType<decimal>();
            var sqFt = (isPlan ? plan.SqFt : spec.SqFt) ?? 0;
            var price = isPlan ? plan.Price : spec.Price;
            var listingId = isPlan ? plan.PlanId : spec.SpecId;
            var virtualTourUrl = isPlan ? plan.VirtualTourUrl : spec.VirtualTourUrl;
            var floorPlanViewerUrl = isPlan ? _listingService.GetFloorPlanViewerUrl(plan, null) : _listingService.GetFloorPlanViewerUrl(null, spec);
            var halfBaths = (isPlan ? plan.HalfBaths : spec.HalfBaths) ?? 0;
            var homeMarketingDescription = isPlan ? plan.HomeMarketingDescription : spec.HomeMarketingDescription;
            var homeDescription = isPlan
                                         ? string.IsNullOrEmpty(plan.Description)
                                               ? listingServiceHelper.GetAutoDescriptionForHome(plan, null)
                                               : ParseHTML.StripHTML(plan.Description).Replace("\r", string.Empty).
                                                     Replace("\n", string.Empty)
                                         : string.IsNullOrEmpty(spec.Description)
                                               ? listingServiceHelper.GetAutoDescriptionForHome(spec.HubPlan, spec)
                                               : ParseHTML.StripHTML(spec.Description).Replace("\r", string.Empty).
                                                     Replace("\n", string.Empty);
            var homeStatus = isPlan ? (int)HomeStatusType.ReadyToBuild : spec.HomeStatus;
            var stories = isPlan ? plan.Stories.ToType<int>() : spec.Stories.ToType<int>();

            if (bedRooms > 0)
                homeBasicSpecs.AppendFormat("<li>{0} " + (bedRooms == 1? LanguageHelper.Bedroom : LanguageHelper.Bedrooms) + "</li>", bedRooms);

            if (bathRooms > 0)
                homeBasicSpecs.AppendFormat("<li>{0} " + (bathRooms == 1 ? LanguageHelper.Bath : LanguageHelper.Bathrooms) + "</li>",
                                            ListingHelper.ComputeBathrooms(bathRooms, halfBaths));

            if (garage > 0)
                homeBasicSpecs.AppendFormat("<li>{0:0.##} " + (garage == 1 ? LanguageHelper.Garage : LanguageHelper.Garages) + "</li>", garage);

            //Sqft
            if (sqFt > 0)
            {
                sqFtDisplay = string.Format("{0} <abbr title=\"square feet\">" + LanguageHelper.MSG_SQ_FT + "</abbr>",
                                            string.Format("{0:###,###}", sqFt));
                homeBasicSpecs.Append(string.Concat("<li>", sqFtDisplay, "</li>"));
            }

            //Stories
            if (stories > 0)
            {
                storyDisplay = string.Format("{0} " + (stories == 1 ? LanguageHelper.Story : LanguageHelper.Stories), stories);
                homeBasicSpecs.Append(string.Concat("<li>", storyDisplay, "</li>"));
            }

            //Phone number - Get builder's toll free number
            string phoneNumber = _communityService.GetTollFreeNumber(NhsRoute.PartnerId, plan.HubCommunity.CommunityId,
                                                                     plan.HubCommunity.BuilderId);

            if (string.IsNullOrEmpty(phoneNumber)) //else use community SO number
                phoneNumber = plan.HubCommunity.PhoneNumber;

            //Get Similar homes from search service - Rule is to use same # of beds, baths
            var searchParams = new PropertySearchParams
            {
                CommunityId = plan.HubCommunity.CommunityId,
                BuilderId = plan.HubCommunity.BuilderId,
                PartnerId = NhsRoute.PartnerId,
                MarketId = plan.HubCommunity.MarketId,
                HomeStatus = homeStatus,
                ListingTypeFlag = plan.HubCommunity.ListingTypeFlag
            };

            //All homes for comm
            var brand = _brandService.GetBrandById(plan.HubCommunity.BrandId, NhsRoute.BrandPartnerId);

            //Init view model
            var viewModel = new HomeDetailViewModel
            {
                PreviewMode = true,
                Plan = plan,
                PlanId = plan.PlanId,
                SpecId = isPlan ? 0 : spec.SpecId,
                PropertyId = listingId,
                CommunityId = plan.CommunityId,
                CommunityCity = plan.HubCommunity.City,
                CommunityName = plan.HubCommunity.CommunityName,
                CommunityDescription = FormattedText(plan.HubCommunity.CommunityDescription),
                BuilderId = plan.HubCommunity.BuilderId,
                MarketId = plan.HubCommunity.MarketId,
                State = plan.HubCommunity.State.StateName,
                StateAbbr = plan.HubCommunity.State.StateAbbr,
                ZipCode = plan.HubCommunity.PostalCode,
                SelectedStateAbbr = _stateService.GetStateAbbreviation(plan.HubCommunity.State.StateName),
                MarketName = plan.HubCommunity.Market.MarketName,
                SalesOfficeAddress1 = (!string.IsNullOrEmpty(plan.HubCommunity.Address1) &&
                                       plan.HubCommunity.OutOfCommunityFlag.ToType<bool>())
                    ? plan.HubCommunity.Address1
                    : plan.HubCommunity.SalesOffice.Address1,
                SalesOfficeAddress2 = !string.IsNullOrEmpty(plan.HubCommunity.Address2)
                    ? plan.HubCommunity.Address2
                    : plan.HubCommunity.SalesOffice.Address2,
                PartnerUsesMatchmaker = _partnerService.GetPartner(NhsRoute.PartnerId).UsesMatchMaker.ToBool(),
                IsBilled = _communityService.GetCommunityBilledStatus(NhsRoute.PartnerId, plan.HubCommunity.BuilderId, plan.HubCommunity.CommunityId, plan.HubCommunity.MarketId),
            };

            //BEGIN-FIX: 79672
            var useSalesOfficeAddress = isPlan || (string.IsNullOrEmpty(spec.Address1) && string.IsNullOrEmpty(spec.Address2));
            viewModel.HomeAddress1 = useSalesOfficeAddress ? viewModel.SalesOfficeAddress1 : spec.Address1;
            viewModel.HomeAddress2 = useSalesOfficeAddress ? viewModel.SalesOfficeAddress2 : spec.Address2;
            //END-FIX: 79672


            viewModel.Latitude = (double)plan.HubCommunity.Latitude;
            viewModel.Longitude = (double)plan.HubCommunity.Longitude;
            viewModel.IsPageCommDetail = false;
            viewModel.DrivingDirections = FormattedText(plan.HubCommunity.SalesOffice.DrivingDirections) ?? string.Empty;
            viewModel.EnableRequestAppointment = true;

            //Home Info
            viewModel.HomeTitle = isPlan ? plan.PlanName : string.Format("{0} ({1})", spec.Address1, spec.PlanName);
            viewModel.InfoAreaTitle = infoAreaTitle;
            viewModel.PriceDisplay = string.Format("{0:" + LanguageHelper.From + " $###;###}", isPlan ? plan.Price : spec.Price);
            viewModel.ExcludesLand = Convert.ToBoolean(isPlan ? plan.LandExcluded : spec.LandExcluded);
            viewModel.StoryDisplay = storyDisplay;
            viewModel.DisplaySqFeet = string.Format("{0}", sqFtDisplay);
            viewModel.SqFtDisplay = sqFtDisplay;
            viewModel.BedroomsAbbr = string.Format(@"<br /><strong>{0}</strong> / ", plan.FormatBedrooms(false));
            viewModel.BathroomsAbbr = string.Format(@"<strong>{0}</strong> ", plan.FormatBathrooms(false));
            viewModel.GaragesAbbr = plan.Garages > 0.0m
                                        ? string.Format(@" / <strong>{0}</strong> ", plan.FormatGarages())
                                        : string.Empty;
            viewModel.HomeSpecsText = ComputeHomeSpecsText(plan);
            viewModel.HomeMarketingDescription = homeMarketingDescription;
            viewModel.HomeDescription = homeDescription;
            viewModel.ShowHotHomeSection = plan.IsHotHome == 1;
            viewModel.CrossMarketingHomes = new List<HomeItem>();
            viewModel.HomeStatus = homeStatus;
            viewModel.BasicHomeSpecs = homeBasicSpecs.ToString();

            //Contact Info
            viewModel.PhoneNumber = phoneNumber;
            viewModel.HoursOfOperation = plan.HubCommunity.SalesOffice.HoursOfOperation != null ?
                plan.HubCommunity.SalesOffice.HoursOfOperation.ToType<string>().Replace("<br />", "   ").Replace("<br/>", "   ") : "";
            viewModel.SaleAgents = _communityService.GetSalesAgents(plan.HubCommunity.SalesOfficeId);

            //Builder Info
            viewModel.BuilderName = plan.HubCommunity.Builder.BuilderName ?? string.Empty;
            viewModel.BrandName = brand.BrandName;
            viewModel.BrandId = plan.HubCommunity.BrandId;
            viewModel.BrandThumbnail = string.Concat(Configuration.ResourceDomain, plan.HubCommunity.Brand.LogoMedium);
            viewModel.BuilderLogo = brand.LogoMedium;
            viewModel.BuilderLogoSmall = brand.LogoSmall;
            viewModel.BuilderUrl = !string.IsNullOrEmpty(plan.HubCommunity.Builder.Url)
                                       ? plan.HubCommunity.Builder.Url
                                       : (!string.IsNullOrEmpty(plan.HubCommunity.Builder.ParentBuilder.Url)
                                              ? plan.HubCommunity.Builder.ParentBuilder.Url
                                              : string.Empty);
            viewModel.CommunityUrl = plan.HubCommunity.CommunityUrl ?? string.Empty;
            viewModel.EnvisionUrl = plan.EnvisionUrl;


            //Home Ancillary Info
            viewModel.Amenities = _communityService.GetAmenities(plan.HubCommunity.IAmenities, plan.HubCommunity.IOpenAmenities);
            viewModel.Utilities = _communityService.GetCommunityUtilities(plan.CommunityId, NhsRoute.PartnerId);
            viewModel.Schools = _communityService.GetSchools(NhsRoute.PartnerId, plan.HubCommunity.CommunityId);
            viewModel.Promotions = new Collection<Promotion>();
            viewModel.Events = new Collection<Event>();
            if (NhsRoute.IsBrandPartnerNhsPro)
            {
                if (viewModel.IsBilled) //Changes because 58867
                {
                    var events = _communityService.GetCommunityEvents(plan.Community.CommunityId,
                                                                      plan.Community.BuilderId).ToList();
                    viewModel.Events = events.Select(e => new Event
                    {
                        EventId = e.EventID,
                        Title = e.Title,
                        Description = e.Description,
                        EventFlyerUrl = e.EventFlyerURL,
                        EventType = e.EventTypeCode,
                        EventEndDate = e.EventEndTime,
                        EventStartDate = e.EventStartTime
                    }).ToList();
                }

                var promotions = _communityService.GetCommunityDetailPromotions(plan.Community.CommunityId, plan.Community.BuilderId).ToList();
                if (!viewModel.IsBilled) //Changes because 58867
                    promotions.RemoveAll(promo => promo.PromoTypeCode != "COM");

                viewModel.Promotions = promotions.Select(p => new Promotion
                {
                    PromoId = p.PromoID,
                    PromoTextShort = p.PromoTextShort,
                    PromoTextLong = p.PromoTextLong,
                    PromoFlyerUrl = p.PromoFlyerURL,
                    PromoType = p.PromoTypeCode,
                    PromoEndDate = p.PromoEndDate,
                    PromoStartDate = p.PromoStartDate,
                    PromoUrl = p.PromoURL
                }).ToList();
            }
            else
                viewModel.Promotions = _communityService.GetCommunityPromotions(plan.HubCommunity.CommunityId, plan.HubCommunity.BuilderId).Take(2).ToList();
            viewModel.GreenPrograms = _communityService.GetCommunityGreenPrograms(plan.HubCommunity.CommunityId).Take(2).ToList();
            viewModel.HotHomesApi = new Collection<HomeItem>();
            var communityImages = plan.HubCommunity.AllHubImages;
            viewModel.AwardImages = _communityService.GetAwardImages(communityImages);
            viewModel.Testimonials = _communityService.GetCustomerTestimonials(5, plan.HubCommunity.CommunityId, plan.HubCommunity.BuilderId);
            viewModel.HomeOptions = _listingService.GetHomeOptionsForPlan(plan, NhsRoute.PartnerId);

            var img = _communityService.GetBuilderMap(communityImages);

            if (img != null)
                viewModel.BuilderMapUrl = img.ImagePath + img.ImageName;

            //Related Properties
            viewModel.SimilarHomesApi = new List<HomeItem>();

            //Helper flags
            viewModel.IsPageCommDetail = false;
            viewModel.IsPlan = isPlan;
            viewModel.ShowLeadForm = true;
            viewModel.ShowSaveToAccount = showSaveToAccount;
            viewModel.ShowExpiredRequestBrochureLink = false;
            viewModel.ShowMortgageLink = (plan.HubCommunity.ShowMortgageLink == "Y");
            viewModel.IsPhoneNumberVisible = false;
            viewModel.SaveHomeSuccessMessage = LanguageHelper.MSG_COMMDETAIL_ADDED_TO_PLANNER;
            viewModel.BcType = plan.HubCommunity.BCType;
            viewModel.LogActionMethodUrl = GetMethodUrl(isPlan, isPlan ? plan.PlanId : spec.SpecId);

            viewModel.UnderneathMapCommName = plan.HubCommunity.CommunityName;
            viewModel.UnderneathMapAddress = ((!string.IsNullOrEmpty(plan.HubCommunity.Address1) &&
                                               plan.HubCommunity.OutOfCommunityFlag.ToType<bool>())
                                                  ? plan.HubCommunity.Address1
                                                  : plan.HubCommunity.SalesOffice.Address1) +
                                             " <br >" + plan.HubCommunity.City + ", " + plan.HubCommunity.State.StateName +
                                             " " + plan.HubCommunity.PostalCode;


            viewModel.ShowPlaceHolderInputText = NhsRoute.CurrentRoute.Function.IndexOf("v3") != -1 ||
                                                 NhsRoute.CurrentRoute.Function.IndexOf("v4") != -1;

            if (RouteParams.ShowPopupPlayer.Value<bool>())
            {
                viewModel.PropertyMediaLinks = _listingService.GetMediaPlayerObjects(plan, spec);
                viewModel.ExternalMediaLinks = new List<MediaPlayerObject>();
                if (!string.IsNullOrEmpty(virtualTourUrl) && !virtualTourUrl.Equals("n", StringComparison.InvariantCultureIgnoreCase))
                {
                    viewModel.ExternalMediaLinks.Add(
                        new MediaPlayerObject
                        {
                            Type = MediaPlayerObjectTypes.Link,
                            SubType = MediaPlayerObjectTypes.SubTypes.VirtualTour,
                            Url = virtualTourUrl
                        }
                        );
                }

                if (!string.IsNullOrEmpty(floorPlanViewerUrl) && !floorPlanViewerUrl.Equals("n", StringComparison.InvariantCultureIgnoreCase))
                {
                    viewModel.ExternalMediaLinks.Add(
                        new MediaPlayerObject
                        {
                            Type = MediaPlayerObjectTypes.Link,
                            SubType = MediaPlayerObjectTypes.SubTypes.ExternalVideo,
                            Url = floorPlanViewerUrl
                        }
                        );
                }
            }

            //Add params to Ad controller
            BindAds(ref viewModel, price, plan.HubCommunity.StateAbbr);

            //Set SDC Market Id
            viewModel.Globals.SdcMarketId = plan.HubCommunity.MarketId;
            viewModel.Size = new KeyValuePair<int, int>(460, 307);
            viewModel.PageUrl = viewModel.Globals.CurrentUrl;
            viewModel.PinterestDescription = string.Format("{0} by {1} at {2}", plan.PlanName, plan.HubCommunity.Brand.BrandName, plan.HubCommunity.CommunityName).Replace("'", "\\'");
            viewModel.Globals.H1 = viewModel.HomeTitle;
            var tags = new List<ContentTag>
            {
                new ContentTag(ContentTagKey.CommunityId, viewModel.CommunityId.ToString())
            };

            viewModel.AffiliateLinksData = new AffiliateLinksData();

            if (NhsRoute.IsBrandPartnerNhsPro)
                return viewModel;

            //BEGIN: Affiliate Links Logic is only for NHS and MOVE
            var links = _affiliateLinkService.GetAffiliateLinks(Pages.CommunityDetail, NhsRoute.PartnerId);
            var freeCreditScore = links.LinksList.Find(x => x.Keyword.IsCaseInsensitiveEqual(AffiliateLinksTypes.FreeCreditScore));
            if (freeCreditScore != null)
                viewModel.AffiliateLinksData.FreeCreditScoreFormArea = freeCreditScore.ToLink(tags);

            var mortgageRates = links.LinksList.Find(x => x.Keyword.IsCaseInsensitiveEqual(AffiliateLinksTypes.MortgageRates));
            if (mortgageRates != null)
                viewModel.AffiliateLinksData.MortageRatesFormArea = mortgageRates.ToLink(tags);

            var calcMortgageRates = links.LinksList.Find(x => x.Keyword.IsCaseInsensitiveEqual(AffiliateLinksTypes.CalculateMortgagePayments));
            if (calcMortgageRates != null)
                viewModel.AffiliateLinksData.CalculateMortagePaymentsNexStepsArea = calcMortgageRates.ToLink(tags);
            //END: Affiliate Links Logic is only for NHS and MOVE

            return viewModel;
        }
    }
}
