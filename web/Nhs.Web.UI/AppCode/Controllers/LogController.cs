﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Route;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Filters;
using Nhs.UrlRewriter.Configuration;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class LogController : ApplicationController
    {
        private const string ErrorMessage =
          "Date:{0}  - Name:{1} - Message:'{2}' - Number: {3} - Href: {4} - AddInfo: {5} - BrowserInfo: '{6}'";

        private IPathMapper _pathMapper;

        public LogController(IPathMapper pathMapper)
            : base(pathMapper)
        {
            _pathMapper = pathMapper;
        }

        
        public virtual JsonResult EventLogger()
        {
            var response = "event logged!";
            string function = Pages.EventLogger;
            var logevent = RouteParams.LogEvent.Value<string>();
            string appRoot = VirtualPathUtility.ToAbsolute("~/");
            appRoot = (appRoot == "/") ? "NewHomeSource/" : appRoot;
            function = appRoot + function;
            if (function.Substring(0, 1) != "/")
                function = "/" + function;

            try
            {
                // Logs view
                var logger = new ImpressionLogger { MarketId = RouteParams.Market.Value<int>() };

                if (RouteParams.Builder.Value<int>() > 0)
                    logger.BuilderId = RouteParams.Builder.Value<int>();
                if (RouteParams.Community.Value<int>() > 0)
                    logger.CommunityId = RouteParams.Community.Value<int>();
                if (RouteParams.SpecId.Value<int>() > 0)
                    logger.AddSpec(RouteParams.SpecId.Value<int>());
                if (RouteParams.PlanId.Value<int>() > 0)
                    logger.AddPlan(RouteParams.PlanId.Value<int>());
                if (RouteParams.BasicListingId.Value<int>() > 0)
                    logger.BasicListingId = RouteParams.BasicListingId.Value<int>();
                if (RouteParams.FeaturedListingId.Value<int>() > 0)
                    logger.FeaturedListingId = RouteParams.FeaturedListingId.Value<int>();
                if (!string.IsNullOrEmpty(RouteParams.FromPage.Value<string>()))
                    logger.FromPage = RouteParams.FromPage.Value<string>();
                if (!string.IsNullOrEmpty(RouteParams.Refer.Value<string>()))
                    logger.Refer = RouteParams.Refer.Value<string>();
                if (RouteParams.TotalHomes.Value<int>() != 0)
                    logger.TotalHomes = RouteParams.TotalHomes.Value<int>();
                if (!string.IsNullOrEmpty(RouteParams.CommunityList.Value<string>()))
                    foreach (var comm in RouteParams.CommunityList.Value<string>().Split(','))
                    {
                        logger.AddBuilderCommunity(logger.BuilderId, comm.ToType<int>());
                    }
                if (!string.IsNullOrEmpty(RouteParams.BuilderCommunityList.Value<string>()))
                    foreach (var bc in RouteParams.BuilderCommunityList.Value<string>().Split(','))
                    {
                        logger.AddBuilderCommunity(bc.Split(' ')[0].ToType<int>(), bc.Split(' ')[1].ToType<int>());
                    }
                if (!string.IsNullOrEmpty(RouteParams.BasicListingList.Value<string>()))
                    foreach (var bl in RouteParams.BasicListingList.Value<string>().Split(','))
                    {
                        logger.AddBasicListing(bl.Split(' ')[0].ToType<int>(), bl.Split(' ')[1].ToType<int>());
                    }
                if (!string.IsNullOrEmpty(RouteParams.FeaturedListingList.Value<string>()))
                    foreach (var fl in RouteParams.FeaturedListingList.Value<string>().Split(','))
                    {
                        logger.AddFeaturedListing(fl.Split(' ')[0].ToType<int>(), fl.Split(' ')[1].ToType<int>(),
                                                  fl.Split(' ')[2].ToType<int>());
                    }
                if (!string.IsNullOrEmpty(RouteParams.HomeList.Value<string>()))
                    foreach (var h in RouteParams.HomeList.Value<string>().Split(','))
                    {
                        if (h.Split(' ')[1] == "s")
                            logger.AddSpec(h.Split(' ')[0].ToType<int>());
                        else  
                            logger.AddSpec(h.Split(' ')[0].ToType<int>());
                    } 

                var partnerIdParam = RouteParams.PartnerId.Value();
                logger.PartnerId = partnerIdParam != NhsRoute.PartnerId.ToString() &&
                                   !string.IsNullOrEmpty(partnerIdParam)
                                       ? partnerIdParam
                                       : NhsRoute.PartnerId.ToString(); //If partner id passed via url, use it

                //iPhone app logging
                if (RouteParams.IsMobile.Value<int>() > 0)
                {
                    logger.PartnerId = RewriterConfiguration.NhsIPhonePartnerId.ToString();
                }

                logger.LogView(logevent);
            }
            catch (Exception ex)
            {
                response = "Error logging the event: " + ex.Message;
            }


            return new JsonResult { Data = response, JsonRequestBehavior = JsonRequestBehavior.AllowGet};
        }

        [HttpHeader("Access-Control-Allow-Origin", "*")]
        public virtual JsonResult PartnerEventLogger(string partnerId, string eventCode, string propertyId, bool testMode)
        {
            string commId = null, planId = null, specId = null;
            string response = string.Empty;

            if (propertyId.Length > 4)
            {
                switch (propertyId.Substring(0, 4))
                {
                    case "BHIC":
                        commId = propertyId.Substring(4); break;
                    case "BHIP":
                        planId = propertyId.Substring(4); break;
                    case "BHIS":
                        specId = propertyId.Substring(4); break;
                }
            }

            var logger = new ImpressionLogger
            {
                PartnerId = partnerId,
                TestMode = testMode
            };

            if (!string.IsNullOrEmpty(commId))
                logger.CommunityId = Convert.ToInt32(commId);
            if (!string.IsNullOrEmpty(specId))
                logger.AddSpec(Convert.ToInt32(specId));
            else if (!string.IsNullOrEmpty(planId))
                logger.AddPlan(Convert.ToInt32(planId));

            try
            {
                logger.LogView(eventCode);
                response = "Partner event logged!";
            }
            catch (Exception ex)
            {
                response = "Error logging the event: " + ex.Message;
                ErrorLogger.LogError(ex);
            }

            return new JsonResult { Data = response, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        
        public virtual JsonResult EventLoggerBasicListing(string logevent, string basiclisting, string market)
        {
            var logger = new ImpressionLogger
            {
                MarketId = int.Parse(market),
                BasicListingId = int.Parse(basiclisting),
                PartnerId = NhsRoute.PartnerId.ToString()
            };

            logger.LogView(logevent);

            return new JsonResult { Data = "Basic Home Info has been logged", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        //
        /// <summary>
        /// Check Please: HomeSearchViewMetricHidden and HomeSearchViewMetricDisplayed in CDC.M
        /// </summary>
        /// <param name="logsInChains">
        ///     Pos[0]:PartnerId, Pos[1]: MarketId, Pos[2]: BuilderId, Pos[3]: CommunityId, Pos[4]: SpecId, Pos[5]: PlanId
        /// </param>
        /// <returns></returns>
        
        public virtual JsonResult EventHomeResultViewLogger(string logsInChains)
        {
            if (logsInChains == null) return new JsonResult { Data = "Error! client object is null" };
            string[] logArray = logsInChains.Split('|');

            foreach (string log in logArray)
            {
                if (!string.IsNullOrEmpty(log))
                {
                    string[] ids = log.Split(',');
                    EventHomeViewLogger(ids[0], ids[1], ids[2], ids[3], ids[4], ids[5]);
                }
            }
            return new JsonResult { Data = "HomeResult Metrics has been logged" };
        }


        [HttpPost]
        public virtual JsonResult EventHomeViewLogger(string partnerId, string marketId, string builderId, string communityId, string planId, string specId)
        {

            int spec = int.Parse(specId);
            var logger = new ImpressionLogger
            {
                MarketId = int.Parse(marketId),
                BuilderId = int.Parse(builderId),
                CommunityId = int.Parse(communityId),
                PartnerId = partnerId
            };
            if (spec > 0)
                logger.AddSpec(spec);
            else
                logger.AddPlan(int.Parse(planId));

            logger.LogView(LogImpressionConst.CommunityDetailHomeSearchViews);
            string response = string.Format("HomeSearchView has been logged! [Partner:{0} Market:{1} Builder:{2} Community:{3} Spec:{4} Plan:{5}]", partnerId, marketId, builderId, communityId, specId, planId);
            return new JsonResult { Data = response };
        }

        public virtual ActionResult LogAndRedirect()
        {
            ImpressionLogger logger = new ImpressionLogger();

            logger.PartnerId = NhsRoute.PartnerId.ToString();
            logger.MarketId = RouteParams.Market.Value<int>();
            logger.AddSpec(RouteParams.SpecId.Value<int>());
            logger.AddPlan(RouteParams.PlanId.Value<int>());
            logger.BuilderId = RouteParams.Builder.Value<int>();
            logger.CommunityId = RouteParams.Community.Value<int>();

            var logEvent = RouteParams.LogEvent.Value<string>();
            var url = RouteParams.Url.Value<string>();

            bool isAnNhsUrl = NhsUrlHelper.IsAnNhsUrl(url);

            if (!isAnNhsUrl && (string.IsNullOrEmpty(RouteParams.HUrl.Value<string>()) ||
               RouteParams.HUrl.Value<string>() == CryptoHelper.HashUsingAlgo(url + ":" + logEvent, "md5")))
                return RedirectTo404();
            
            logger.LogView(logEvent);
            if (!url.StartsWith("/"))
                url = "/" + url;

            return Redirect(url);
        }

        [HttpPost]
        public virtual JsonResult LogError(string nhsJsError)
        {
            if (string.IsNullOrEmpty(nhsJsError)) new JsonResult { Data = "Problems logging the error!" };

            var serializer = new JavaScriptSerializer();
            var errorMessage = serializer.Deserialize<NhsClientError>(nhsJsError);

            string errorDescription = string.Concat("ErrorInfo Name:", errorMessage.Name, " Message ", errorMessage.Message, " LineNumber: ", errorMessage.Number, " Href: ", errorMessage.Href, " ");
            string additionalInfo = string.Concat("AddInfo: ", errorMessage.AddInfo, " BrowserInfo: ", errorMessage.BrowserInfo);

            ErrorLogger.LogError("JavaScriptError", errorDescription, additionalInfo);
            return new JsonResult { Data = "Error has been logged!" };
        }


        //
        // GET: /LogRedirect/
        public virtual ActionResult LogRedirect()
        {
            var logger = new ImpressionLogger();
            var url = NhsUrl.GetExternalURL;

            var logEvent = NhsUrl.GetLogEvent;
            logEvent = logEvent.ToUpper();

            if (url.Length == 0)
                url = NhsUrl.GetRedirectUrl;

            url = HttpUtility.UrlDecode(url);

            var isAnNhsUrl = NhsUrlHelper.IsAnNhsUrl(url);
            var inValidUrl = (string.IsNullOrEmpty(NhsUrl.GetHUrl) ||
                              NhsUrl.GetHUrl != CryptoHelper.HashUsingAlgo(url + ":" + logEvent, "md5"));

            var isExternalUrl = string.IsNullOrWhiteSpace(NhsUrl.GetExternalURL) == false; //||
            //NhsUrl.GetExternalURL != CryptoHelper.HashUsingAlgo(url + ":" + logEvent, "md5");

            if (isExternalUrl == false)
            {
                if (!isAnNhsUrl && inValidUrl)
                {
                    var urlParams = new List<RouteParam>
                    {
                        new RouteParam(RouteParams.Statuscode, "404", RouteParamType.QueryString)
                    };
                    return RedirectPermanent(urlParams.ToUrl(Pages.MvcError.ToTitleCase()));
                }
            }

            switch (logEvent)
            {
                case LogImpressionConst.EmailAccountCreationClickThrough:
                    logger.PartnerId = Configuration.PartnerId.ToType<string>();
                    logger.LogView(logEvent);
                    break;

                case LogImpressionConst.EmailBrochureClickThrough:
                    if (NhsUrl.GetSpecID > 0) //If spec, do not add plan Id
                        logger.AddSpec(NhsUrl.GetSpecID);
                    else
                        logger.AddPlan(NhsUrl.GetPlanID);
                    logger.CommunityId = NhsUrl.GetCommunityID;
                    logger.BuilderId = NhsUrl.GetBuilderID;
                    logger.MarketId = NhsUrl.GetMarketID;
                    logger.PartnerId = Configuration.PartnerId.ToType<string>();
                    logger.LogView(logEvent);
                    break;

                case LogImpressionConst.EmailSearchAlertClickThrough:
                    logger.PartnerId = Configuration.PartnerId.ToType<string>();
                    logger.LogView(logEvent);
                    break;

                case LogImpressionConst.EmailSendFriendClickThrough:
                    logger.PartnerId = Configuration.PartnerId.ToType<string>();
                    logger.LogView(logEvent);
                    break;

                case LogImpressionConst.FeaturedListingClick:
                    logger.CommunityId = NhsUrl.GetCommunityID;
                    logger.BuilderId = NhsUrl.GetBuilderID;
                    logger.MarketId = NhsUrl.GetMarketID;
                    logger.FeaturedListingId = NhsUrl.GetFeaturedListingId;
                    logger.PartnerId = Configuration.PartnerId.ToType<string>();
                    logger.LogView(logEvent);
                    break;

                case LogImpressionConst.BasicListingBrokerSiteUrl:
                    logger.BasicListingId = NhsUrl.GetListingID;
                    logger.MarketId = NhsUrl.GetMarketID;
                    logger.PartnerId = Configuration.PartnerId.ToType<string>();
                    logger.Refer = UserSession.Refer;
                    logger.LogView(logEvent);
                    break;

                default:
                    if (NhsUrl.GetSpecID > 0) //If spec, do not add plan Id
                        logger.AddSpec(NhsUrl.GetSpecID);
                    else
                        logger.AddPlan(NhsUrl.GetPlanID);
                    logger.CommunityId = NhsUrl.GetCommunityID;
                    logger.BuilderId = NhsUrl.GetBuilderID;
                    logger.MarketId = NhsUrl.GetMarketID;
                    logger.PartnerId = Configuration.PartnerId.ToType<string>();
                    logger.Refer = UserSession.Refer;
                    logger.LogView(logEvent);
                    break;
                case LogImpressionConst.IdeaCenter:
                    logger.PartnerId = Configuration.PartnerId.ToType<string>();
                    logger.Refer = UserSession.Refer;
                    logger.AdvertiserId = NhsUrl.GetAdvertiserID.ToType<string>();
                    logger.AdvertiserUrl = NhsUrl.GetExternalURL;
                    logger.LogView(logEvent);
                    break;
            }

            //61639 - temp fix
            var malwareUrl = (from m in GetMalwareUrlList()
                              where url.Contains(m)
                              select m).FirstOrDefault();

            if (!string.IsNullOrEmpty(malwareUrl))
            {
                var urlParams = new List<RouteParam>
                {
                    new RouteParam(RouteParams.Statuscode, "404", RouteParamType.QueryString)
                };
                return RedirectPermanent(urlParams.ToUrl(Pages.MvcError.ToTitleCase()));
            }

            if (NhsUrl.GetClientRedirect)
                return Redirect(url);

            return Redirect(PathHelpers.CleanNavigateUrl(url));
        }


        private static IEnumerable<string> GetMalwareUrlList()
        {
            var urls = new List<string>
                {
                    "buvitiqa.ist-in-frankfurt.de/where-to-buy-unlocked-cell-phones.php", 
                    "igaqig.site50.net/foto-strane-da-inviare.php",
                    "ykoloda.hostoi.com/medical-insurance-brokers-in-pennsylvania.php",
                    "ydycygeci.net76.net/best-airless-paint-sprayer.php",
                    "ysukacevatif.comli.com/comfort-inn-lebanon-new-hampshire.php",
                    "ykoloda.hostoi.com",
                    "ibasozivewiw.host22.com",
                    "ulexij.zzl.org",
                    "enovam.host56.com/julianos-pizza.php",
                    "ysukacevatif.comli.com/the-boston-italians.php",
                    "fuvocovomin.netau.net/thurmont-md-events.php",
                    "crayoncolection.cold10.com/c-structure-of-structures.php",
                    "delfinohomes.com"
                };

            return urls;
        }
    }

    public class NhsClientError
    {
        public string Message { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public string Href { get; set; }
        public string AddInfo { get; set; }
        public string BrowserInfo { get; set; }
    }
}
