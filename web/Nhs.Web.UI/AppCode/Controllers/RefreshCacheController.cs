﻿using System;
using System.Collections.Specialized;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Nhs.Library.Common;
using Nhs.Library.Web;
using Nhs.Utility.Web;
using Nhs.Web.UI.AppCode.Filters;
using Nhs.Web.UI.AppCode.ViewModels.CacheRefresh;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class RefreshCacheController : ApplicationController
    {
        public virtual ActionResult RefreshCache(string code)
        {
            if (!string.IsNullOrEmpty(code))
            {
                if (string.Equals(code, Configuration.RefreshCachePassword))
                    CookieManager.RefreshCacheAuthorization = "true";
                else
                {
                    return !Request.IsAjaxRequest()
                        ? (ActionResult)View(NhsMvc.Default.Views.Util.RefreshCache, new ServerCacheList())
                        : Json(new { script = "jQuery('.error').show();" }, JsonRequestBehavior.AllowGet);
                }
            }
            if (!string.Equals(CookieManager.RefreshCacheAuthorization, "true"))
            {
                return !Request.IsAjaxRequest()
                       ? (ActionResult)View(NhsMvc.Default.Views.Util.RefreshCache, new ServerCacheList())
                       : Json(new { script = "jQuery('.error').show();" }, JsonRequestBehavior.AllowGet);
            }


            int webServerNum = Configuration.WebServerNum;
            string webServerPrefix = Configuration.WebServerPrefix;
            var remoteStatusList = new ServerCacheList
            {
                WebServerCount = webServerNum,
                WebServerPrefix = webServerPrefix,
                IsAuthorized = true
            };

            for (int i = 1; i <= webServerNum; i++)
            {
                string statusUrl = "http://" + webServerPrefix + i + "/Util/ShowStatus";
                string remoteResponse = HTTP.HttpGet(statusUrl);

                var serverStatus = new CacheRefreshViewModel
                {
                    SeverName = webServerPrefix + i,
                    RemoteUrl = statusUrl,
                    ServerResponse = remoteResponse
                };
                remoteStatusList.LastRefresh = remoteResponse;
                remoteStatusList.CacheList.Add(serverStatus);
            }
            return PartialView(NhsMvc.Default.Views.Util.RefreshCache, remoteStatusList);
        }

        public virtual MvcHtmlString ShowStatus()
        {
            string lastRefresh = (WebCacheManager.LastWebRefresh == DateTime.MinValue) ? "n/a" : WebCacheManager.LastWebRefresh.ToString();
            return MvcHtmlString.Create("Last Refresh:" + lastRefresh);
        }

        [HttpPost]
        public virtual void RefreshServerCache(string code)
        {
            if (!string.IsNullOrEmpty(code))
            {
                if (string.Equals(code, Configuration.RefreshCachePassword))
                    CookieManager.RefreshCacheAuthorization = "true";
                else
                    return;

            }
            if (!string.Equals(CookieManager.RefreshCacheAuthorization, "true"))
            {
                return;
            }
            var refreshParameter = Request.QueryString["refresh"];
            if (!string.IsNullOrWhiteSpace(refreshParameter) && refreshParameter.ToLower().Equals("yes"))
            {
                HttpRuntime.UnloadAppDomain();
            }
        }


        [NoCache]
        public virtual ActionResult RefreshAllServers(string code)
        {
            if (!string.Equals(CookieManager.RefreshCacheAuthorization, "true"))
            {
                return View(NhsMvc.Default.Views.Util.RefreshCache, new ServerCacheList());
            }

            int webServerNum = Configuration.WebServerNum;
            string webServerPrefix = Configuration.WebServerPrefix;
            string json = JsonConvert.SerializeObject(new { code = Configuration.RefreshCachePassword });
            var headers = new NameValueCollection { { "Accept-Charset", "utf-8" } };
            for (int i = 1; i <= webServerNum; i++)
            {
                string statusUrl = "http://" + webServerPrefix + i + "/Util/RefreshServerCache?refresh=yes";
                string remoteResponse = HTTP.HttpJsonPost(statusUrl, json);

            }

            var remoteStatusList = new ServerCacheList
            {
                WebServerCount = webServerNum,
                WebServerPrefix = webServerPrefix,
                IsAuthorized = true
            };

            for (int i = 1; i <= webServerNum; i++)
            {
                string statusUrl = "http://" + webServerPrefix + i + "/Util/ShowStatus";
                string remoteResponse = HTTP.HttpGet(statusUrl);

                var serverStatus = new CacheRefreshViewModel
                {
                    SeverName = webServerPrefix + i,
                    RemoteUrl = statusUrl,
                    ServerResponse = remoteResponse
                };
                remoteStatusList.LastRefresh = remoteResponse;
                remoteStatusList.CacheList.Add(serverStatus);
            }
            return PartialView(NhsMvc.Default.Views.Util.RefreshCache, remoteStatusList);
        }





    }
}
