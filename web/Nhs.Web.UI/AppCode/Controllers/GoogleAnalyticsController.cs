﻿using Nhs.Library.Web;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.HtmlHelpers;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class GoogleAnalyticsController : ApplicationController
    {


        public GoogleAnalyticsController()
        {
            
        }

        public virtual string GetReferWithHashTag(string refVal, string refUrl)
        {
            if (!string.IsNullOrEmpty(refVal))
            {                
                UserSession.DynamicRefer = refVal;
                UserSession.Refer = refVal;                
            }
            if (!string.IsNullOrEmpty(refUrl))
            {
                UserSession.DynamicReferUrl = refUrl;
            }
            var google = HtmlGoogleHelper.GetSetTrafficCategorizationString();
            return google;
        }

        public virtual string SetDataLayerPair(string key, int optValue)
        {            
            var google = HtmlGoogleHelper.SetDataLayerPair(key, optValue);
            return google;
        }
    }
}
