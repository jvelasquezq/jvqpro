﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Brightcove;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Domain.Model.BhiTransaction;
using Nhs.Utility.Constants;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Filters;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class OwnerStoriesController : ApplicationController
    {
        private readonly IOwnerStoriesService _ownerStoriesService;

        public OwnerStoriesController(IOwnerStoriesService ownerStoriesService)
        {
            
            _ownerStoriesService = ownerStoriesService;
        }

        [HttpGet]
        public virtual ActionResult Show()
        {                        
            var model = new OwnerStoriesViewModel();
            FillDefaulShowModel(model);
            model.Display = "none";
            // ReSharper disable once Mvc.ViewNotResolved
            return View(model);
        }

        [HttpPost]
        public virtual ActionResult Show(OwnerStoriesViewModel model)
        {
            Stream inputStream = null;
            var mediaType = string.Empty;
            ModelState.Clear();
            if (Request.Files != null && Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                if (file != null)
                {
                    inputStream = file.InputStream;
                    mediaType = file.ContentType;
                }
            }

            var hasError = false;
            IDictionary<string, string> errorsDic = new Dictionary<string, string>();
            if (ModelState.IsValid)
            {
                errorsDic = SendToService(model, mediaType, inputStream, errorsDic, ref hasError);
                model.SubmitOk = !hasError;
                if (hasError)
                {
                    foreach (var error in errorsDic)
                    {
                        ModelState.AddModelError(error.Key,error.Value);
                    }
                }
            }

            FillDefaulShowModel(model);
            model.Display = "block";
            return View(model);
        }

        private void FillDefaulShowModel(OwnerStoriesViewModel model)
        {
            var ownerStoriesContent = _ownerStoriesService.GetOwnerStoriesContent();
            model.ContestRulesContent = ownerStoriesContent.ContestRules;
            model.SubmissionContent = ownerStoriesContent.Submission;
            model.CategoryList = new SelectList(GetCategoryList(), "Value", "Text");
            model.OwnerStoryContentList = ownerStoriesContent.OwnerStoryContentList;
            SetupAdParameters(model);
        }

        /// <summary>
        /// IF you change this order can affect the auto select in jQuery for the (#nhs_OwnersLinkShareStory).click event
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<SelectListItem> GetCategoryList()
        {
            var categoryList = new List<SelectListItem>();
            categoryList.Insert(0,
                new SelectListItem {Text = @"Is Designed For The Way I Live", Value = "Is Designed For The Way I Live"});
            categoryList.Insert(1, new SelectListItem {Text = @"Saves Me Time And Money", Value = "Saves Me Time And Money"});
            categoryList.Insert(2, new SelectListItem {Text = @"Was Fun And Easy To Buy", Value = "Was Fun And Easy To Buy"});
            return categoryList;
        }

        public virtual ActionResult SendOwnerStory()
        {
            var model = new OwnerStoriesViewModel();

            return PartialView(NhsMvc.Default.Views.OwnerStories.PartialViews.OwnerStoriesForm, model);
        }

        [HttpPost]         
        public virtual JsonResult SaveOwnerStory(OwnerStoriesViewModel model)
        {        
            ModelState.Clear();          

            var hasError = false;
            IDictionary<string, string> errorsDic = new Dictionary<string, string>();
            if (ModelState.IsValid)
            {
                errorsDic = SendToService(model, string.Empty, null, errorsDic, ref hasError);
            }
            else
            {
                hasError = true;
                var errors = ModelState.Values.SelectMany(v => v.Errors.Select(s => new KeyValuePair<string, string>("Model",
                    string.IsNullOrWhiteSpace(s.ErrorMessage) ? s.Exception.Message : s.ErrorMessage )));
                foreach (var error in errors)
                {
                    errorsDic.Add(error);
                }
            }

            if (hasError)
            {
                model.OwnerStoryContentList = _ownerStoriesService.GetOwnerStoriesContent().OwnerStoryContentList;
                model.CategoryList = new SelectList(GetCategoryList(), "Value", "Text");
                return
                    Json(
                        new
                        {
                            html = RenderRazorViewToString(
                                    NhsMvc.Default.Views.OwnerStories.PartialViews.OwnerStoriesForm, model),
                            HasErrors = true,
                            Errors = errorsDic.Select(s=> s.Value)
                        }, JsonRequestBehavior.DenyGet);                
            }
                        
            return Json(new
            {
                html = RenderRazorViewToString(NhsMvc.Default.Views.OwnerStories.PartialViews.OwnerStoriesFormThankYou, model),
                HasErrors = false,
                Errors = errorsDic.Select(s => s.Value)
            }, JsonRequestBehavior.DenyGet);
        }

        private IDictionary<string, string> SendToService(OwnerStoriesViewModel model, string mediaType, Stream inputStream,
            IDictionary<string, string> errorsDic, ref bool hasError)
        {
            var story = new OwnerStory
            {
                BuilderName = model.BuilderName,
                BuilderId = model.BuilderId,
                City = model.City,
                CommunityName = model.CommunityName,
                CommunityId = model.CommunityId,
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Category = model.Category,
                StoryDescription = model.StoryDescription,
                MediaContentType = model.uploadFile != null ? model.uploadFile.ContentType : mediaType,
                FileStream = model.uploadFile != null ? model.uploadFile.InputStream : inputStream,
            };

            var serviceResult = _ownerStoriesService.InsertOwnerStory(story,
                Configuration.OwnerStoriesImageSharePath);

            if (serviceResult.HasErrors)
            {
                errorsDic = serviceResult.ModelErros;
                hasError = true;
            }
            return errorsDic;
        }

        [HttpPost]
        public virtual JsonResult ThankYou()
        {
            return new JsonResult();
        }

        #region Owner Stories Curation
        [HttpGet]
        public virtual ActionResult ShowCuration()
        {
            var model = new OwnerStoriesViewModel();
            if (!ValidateUser())
            {
                var accessDeniedView = NhsMvc.Default.Views.OwnerStories.ShowAccessDenied;
                return View(accessDeniedView, model);
            }
            return View(model);
        }

        public virtual JsonResult GetOwnerStoriesList()
        {
            var ownerStoriesList = _ownerStoriesService.GetOwnerStoriesList(null, true, null);
            return Json(ownerStoriesList, JsonRequestBehavior.AllowGet);
        }

        private bool ValidateUser()
        {
            bool isValidUser = UserSession.UserProfile.ActorStatus == WebActors.ActiveUser 
                               && UserSession.UserProfile.LogonName == "ownerstories@newhomesource.com"
                               && UserSession.UserProfile.Password == "owner";
            return isValidUser;
        }

        public virtual ActionResult ShowStoryDetail(int storyId)
        {
            var model = new OwnerStoriesViewModel();
            if (!ValidateUser())
            {
                var accessDeniedView = NhsMvc.Default.Views.OwnerStories.ShowAccessDenied;
                return View(accessDeniedView, model);
            }
            model.CategoryList = new SelectList(GetCategoryList(), "Value", "Text");
            model.ShowOwnerStoryDetail = true;
            SetupAdParameters(model);
            var ownerStory =
                _ownerStoriesService.GetOwnerStoriesList(null, true, null)
                    .FirstOrDefault(o => o.OwnerStoryId == storyId);
            if (ownerStory == null) return View(model);
            model.CommunityName = ownerStory.CommunityName;
            model.BuilderName = ownerStory.BuilderName;
            model.Category = ownerStory.Category;
            model.Email = ownerStory.Email;
            model.FirstName = ownerStory.FirstName;
            model.LastName = ownerStory.LastName;
            model.City = ownerStory.City;
            model.ValidCityName = ownerStory.City;
            model.ValidBuilderName = ownerStory.BuilderName;
            model.ValidCommunityName = ownerStory.CommunityName;
            model.StoryDescription = ownerStory.StoryDescription;
            model.NoNullOwnerStoryStatus = ownerStory.Status.ToType<bool>();
            model.OwnerStoryStatus = ownerStory.Status;
            model.OwnerStoryId = ownerStory.OwnerStoryId;
            model.BuilderId = ownerStory.BuilderId;
            model.CommunityId = ownerStory.CommunityId;
            model.HasImage = ownerStory.HasImage;
            //model.HasVideo = ownerStory.HasVideo;
            model.FilePath = ownerStory.FilePath;
            model.VideoThumbnailUrl = ownerStory.ProcessedVideoThumbnailUrl;
            model.VideoUrl = ownerStory.ProcessedVideoUrl;
            model.VideoId = ownerStory.ProcessedVideoId;
            var bcVideoList = BCAPI.FindVideosByTags(string.Empty,
                String.Format("htv-{0}-{1}-{2}", model.OwnerStoryId, model.VideoId, Configuration.BrightcoveEnvSuffix));
            BCVideo bcVideo = null;
            if (bcVideoList.Any())
                bcVideo = bcVideoList.First();
            if (bcVideo != null)
            {
                model.OnlineVideoId = bcVideo.id.ToString(CultureInfo.InvariantCulture);
                model.HasVideo = ownerStory.HasVideo;
            }
            return View(model);
        }

        [HttpPost]
        public virtual JsonResult CurateOwnerStory(OwnerStoriesViewModel model)
        {
            ModelState.Clear();

            var hasError = false;
            IDictionary<string, string> errorsDic = new Dictionary<string, string>();
            if (ModelState.IsValid)
            {
                errorsDic = SendCuratedOwnerStoryToService(model, errorsDic, ref hasError);
            }
            else
            {
                hasError = true;
                var errors = ModelState.Values.SelectMany(v => v.Errors.Select(s => new KeyValuePair<string, string>("Model",
                    string.IsNullOrWhiteSpace(s.ErrorMessage) ? s.Exception.Message : s.ErrorMessage)));
                foreach (var error in errors)
                {
                    errorsDic.Add(error);
                }
            }

            if (hasError)
            {
                model.CategoryList = new SelectList(GetCategoryList(), "Value", "Text");
                return
                    Json(
                        new
                        {
                            html = RenderRazorViewToString(
                                    NhsMvc.Default.Views.OwnerStories.ShowStoryDetail, model),
                            HasErrors = true,
                            Errors = errorsDic.Select(s => s.Value)
                        }, JsonRequestBehavior.DenyGet);
            }

            return Json(new
            {
                html = RenderRazorViewToString(NhsMvc.Default.Views.OwnerStories.PartialViews.OwnerStoriesFormThankYou, model),
                HasErrors = false,
                Errors = errorsDic.Select(s => s.Value)
            }, JsonRequestBehavior.DenyGet);
        }

        private IDictionary<string, string> SendCuratedOwnerStoryToService(OwnerStoriesViewModel model,
           IDictionary<string, string> errorsDic, ref bool hasError)
        {
            var story = new OwnerStory
            {
                OwnerStoryId = model.OwnerStoryId,
                BuilderName = model.BuilderName,
                BuilderId = model.BuilderId,
                City = model.City,
                CommunityName = model.CommunityName,
                CommunityId = model.CommunityId,
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Category = model.Category,
                StoryDescription = model.StoryDescription,
                Status = model.OwnerStoryStatus,
                MediaContentType = string.Empty
            };

            var serviceResult = _ownerStoriesService.CurateOnwerStory(story);

            if (serviceResult.HasErrors)
            {
                errorsDic = serviceResult.ModelErros;
                hasError = true;
            }
            return errorsDic;
        }
        #endregion
    }
}
