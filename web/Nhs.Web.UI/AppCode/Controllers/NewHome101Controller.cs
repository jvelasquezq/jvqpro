﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class NewHome101Controller : ResourceCenterController
    {
        public NewHome101Controller(IPathMapper pathMapper, IPartnerService partnerService, IStateService stateService,
                                    ICmsService cmsService) :
                                        base(pathMapper, partnerService, stateService, cmsService)
        {
            
        }
        
    }
}
