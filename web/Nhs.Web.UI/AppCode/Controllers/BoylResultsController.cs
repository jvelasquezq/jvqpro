﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Route;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.ViewModels;
using Market = Nhs.Mvc.Domain.Model.Web.Market;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class BoylResultsController : ApplicationController
    {
        private readonly IMarketService _marketService;
        private readonly IBrandService _brandService;
        private readonly IBoylService _boylService;
        private readonly IBuilderService _builderService;
        private readonly ICommunityService _communityService;
        

        public BoylResultsController(IBoylService boylService, IMarketService marketService, IPathMapper pathMapper, IBuilderService builderService, IBrandService brandService, ICommunityService communityService)
            : base(pathMapper)
        {
            _boylService = boylService;
            _marketService = marketService;
            _builderService = builderService;
            _brandService = brandService;
            _communityService = communityService;
        }

        public virtual ActionResult Show()
        {
            Market market;
            if (!ValidateMarket(out market))
            {
                return RedirectToRoute("boylsearch", new { DisplayMessage = "badzip" });
            }

            var viewModel = GetBoylResultsModel(market);

            AddCanonicalWithIncludeParams(viewModel, new List<string> { "market" });
            // ReSharper disable once Mvc.ViewNotResolved
            return View(viewModel);
        }

        private BoylResultsViewModel GetBoylResultsModel(Market market)
        {
            var boylResults = _boylService.GetBoylResults(NhsRoute.PartnerId, market.MarketId, market.MarketName, market.StateAbbr).ToList();

            foreach (var result in boylResults)
            {
                if (!result.SubVideoFlag.ToBool()) continue;

                var comm = _communityService.GetVideoTourImages(result.CommunityId).Where(video => video.ImageTypeCode == ImageTypes.CommunityVideoTour)
                                                                                 .Select(media => new MediaPlayerObject { Url = media.OriginalPath }).ToList();
                if (comm.Count > 0)
                    result.Video_url= comm.First().Url;
            }

            Brand currentBrand = null;

            if (RouteParams.BrandId.Value<int>() != 0)
                currentBrand = _brandService.GetBrandById(RouteParams.BrandId.Value<int>(), NhsRoute.BrandPartnerId);

            var viewModel = new BoylResultsViewModel
            {
                MarketId = market.MarketId,
                MarketName = market.MarketName,
                ShowComingSoonLink = (market.TotalComingSoon > 0),
                //TICKET 77202
                //Results = boylResults.OrderBy(p => p.BrandName).ThenBy(p => p.CommunityName).GroupBy(p => p.BrandName),
                Results = boylResults.Where(p => !p.IsBasicCommunity).OrderBy(p => p.BrandName).ThenBy(p => p.CommunityName).GroupBy(p => p.BrandName),
                BasicResults = boylResults.Where(p => p.IsBasicCommunity).OrderBy(p => p.BrandName).ThenBy(p => p.CommunityName).GroupBy(p => p.BrandName),
                TotalListings = boylResults.Count(),
                BuilderCommunityList = boylResults.Select(br => br.BuilderId.ToString() + " " + br.CommunityId.ToString()).ToArray().Join(",")
            };

            if (currentBrand != null)
                viewModel.CurrentBrand = currentBrand;

            var parameters = new List<ContentTag> { new ContentTag { TagKey = ContentTagKey.MarketName, TagValue = market.MarketName.ToTitleCase() } };

            //Assign spotlight homes only for desktop site
            if (!NhsRoute.ShowMobileSite)
                viewModel.SpotHomes = _boylService.GetSpotHomes(NhsRoute.PartnerId, market.MarketId).ToList();

            OverrideDefaultMeta(viewModel, parameters);
            FillAdsData(boylResults, viewModel, viewModel.SpotHomes, market);

            return viewModel;
        }

        private void FillAdsData(IList<BoylResult> boylResults, BoylResultsViewModel viewModel,
            IList<ExtendedHomeResult> spotResults, Market market)
        {
            if (boylResults.Any())
            {
                foreach (var boyl in boylResults.Select(bl => new { City = bl.City, Zip = bl.PostalCode, BuilderId = bl.BuilderId }))
                {
                    viewModel.Globals.AdController.AddCityParameter(boyl.City);
                    viewModel.Globals.AdController.AddZipParameter(boyl.Zip);
                }
            }

            UserSession.AdBuilderIds.Clear();
            var builderIds = _builderService.GetMarketBuilders(NhsRoute.PartnerId, viewModel.MarketId);
            foreach (var builder in builderIds)
            {
                viewModel.Globals.AdController.AddBuilderParameter(builder.BuilderId);
                UserSession.AdBuilderIds.Add(builder.BuilderId);
            }

            if (spotResults != null && spotResults.Any())
            {
                foreach (var spot in spotResults)
                {
                    viewModel.Globals.AdController.AddCityParameter(spot.City);
                    viewModel.Globals.AdController.AddZipParameter(spot.PostalCode);
                }
            }

            viewModel.Globals.AdController.AddMarketParameter(market.MarketId);
            viewModel.Globals.AdController.AddStateParameter(market.StateAbbr);
        }

        private bool ValidateMarket(out Market market)
        {

            market = null;
            string postalCode = RouteParams.PostalCode.Value();
            int marketId = postalCode != string.Empty
                ? _marketService.GetMarketIdFromPostalCode(postalCode, NhsRoute.PartnerId)
                : RouteParams.Market.Value().ToType<int>();

            //TODO:  Implement these messages
            if (marketId == 0)
            {
                return false;
            }

            market = _marketService.GetMarket(NhsRoute.PartnerId, marketId, false);

            if (market == null || market.TotalCustomBuilders == 0)
            {
                return false;
            }

            return true;
        }
    }
}
