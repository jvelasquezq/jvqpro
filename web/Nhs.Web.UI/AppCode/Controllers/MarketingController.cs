﻿using System.Collections.Generic;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Utility.Constants;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Filters;
using Nhs.Web.UI.AppCode.ViewModels;
using StructureMap;

namespace Nhs.Web.UI.AppCode.Controllers
{    
    [PartnersAuthorize(NhsRoutePartnerTypeEnum.UseNhsRoutePartnerId,PartnersConst.Pro)]
    public partial class MarketingController : ApplicationController
    {
        public MarketingController(IPathMapper pathMapper)
            : base(pathMapper)
        {
            
        }

        public virtual ActionResult ShowBrokerPartnership()
        {            
            var model = GetProMarketingViewModel();

            var metas = new List<MetaTag>()
                {
                    new MetaTag(){ Name = MetaName.Title, Content = "Broker Program"},
                    new MetaTag(){ Name = MetaName.Description, Content = "We keep your agents happy with a version of New Home Source Professional customized for your broker intranet."}
                };

            base.OverrideDefaultMeta(model, metas);

            return View(model);
        }

        public virtual ActionResult ShowMlsPartnership()
        {            
            var model = GetProMarketingViewModel();
            var metas = new List<MetaTag>()
                {
                    new MetaTag(){  Name = MetaName.Title, Content = "MLS Program"},
                    new MetaTag(){ Name = MetaName.Description, Content = "We keep your members happy with a version of New Home Source Professional customized for your MLS."},
                };

            base.OverrideDefaultMeta(model,metas);
       
            return View(model);
        }

        private ProMarketingViewModel GetProMarketingViewModel()
        {            
            return ObjectFactory.GetInstance<ProMarketingViewModel>();
        }        
    }
}