﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Library.Exceptions;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class AccountController
    {
        [HttpPost]
        public virtual JavaScriptResult RegisterMobile(RegisterViewModel model)
        {
            if (_userService.LogonNameExists(model.Email, NhsRoute.PartnerId))
            {
                return JavaScript("jQuery(\"#nhs_Errors\").append('<li>User already exists, try a different email</li>')");
            }
            try
            {
                var profile = new UserProfile
                {
                    LogonName = model.Email,
                    FirstName = model.Name.FirstPart(" "),
                    LastName = model.Name.LastPart(" "),
                    Password = model.Password,
                    PartnerId = NhsRoute.PartnerId,
                    RegistrationMarket = model.MarketId,
                    MoveInDate = -1
                };

                //My home needs defaults

                // NOT International users
                if (!model.LiveOutsideReg)
                {
                    profile.PostalCode = model.ZipCodeReg;
                }

                profile.MailingList = model.Newsletter ? "1" : "0";
                profile.HomeWeeklyOptIn = model.Newsletter;
                profile.MarketOptin = model.Promos ? "1" : "0";

                // Setting those values ... required in stored proc.
                profile.DateRegistered =
                    profile.DateLastChanged =
                        profile.BoxRequestedDate = profile.InitialMatchDate = profile.LastMatchesSentDate = DateTime.Now;

                // Create user
                _userService.CreateProfile(profile, NhsRoute.BrandPartnerId, LanguageHelper.WrongPassword, LanguageHelper.EmailDoNotExist, LanguageHelper.UserAlreadyExist);
                _userService.CreateRegistrationLead(profile);
                if (model.IsAjaxRequest)
                {
                    return JavaScript("window.location.reload()");
                }
               
                if(!string.IsNullOrEmpty(model.FromPage))
                    return JavaScript(string.Format("window.location='{0}'", model.FromPage));

                var url = new List<RouteParam> { new RouteParam(RouteParams.Welcome, "true", RouteParamType.QueryString, true, true) }.ToUrl(Pages.Home);
                return JavaScript(string.Format("window.location='{0}'", url));
            }
            catch (UnknownUserException e)
            {
                return JavaScript("jQuery(\"#nhs_Errors\").append('<li>" + e.Message + "</li>')");
            }
        }

      
        public virtual ActionResult UpdateAccount()
        {
            if (!UserSession.UserProfile.IsLoggedIn())
            {
                var urlRedirect = new List<RouteParam>().ToUrl(Pages.SignIn);
                return Redirect(urlRedirect);
            }

            var model = new UpdateAccountViewModel();
            model.FirstName = UserSession.UserProfile.FirstName + " " + UserSession.UserProfile.LastName;
            model.Email = UserSession.UserProfile.Email;
            model.ZipCodeReg = UserSession.UserProfile.PostalCode;
            model.Password = UserSession.UserProfile.Password;
            model.Newsletter = UserSession.UserProfile.HomeWeeklyOptIn.ToType<bool>();
            model.Promos = UserSession.UserProfile.MarketOptIn.ToType<int>().ToType<bool>();
            if (Request.IsAjaxRequest())
                return PartialView(NhsMvc.Default.ViewsMobile.Account.UpdateAccount, model);
                return View("UpdateAccount", model);
        }

        [HttpPost]
        public virtual JsonResult UpdateAccount(UpdateAccountViewModel model)
        {
            var userProfile = UserSession.UserProfile;
            userProfile.FirstName = model.FirstName.FirstPart(" ");
            userProfile.LastName = model.FirstName.LastPart(" ");
            userProfile.Email = model.Email;
            userProfile.PostalCode = model.ZipCodeReg;
            userProfile.Password = model.Password;
            userProfile.HomeWeeklyOptIn = model.Newsletter;
            userProfile.MarketOptIn = model.Promos ? "1" : "0";

            userProfile.SaveProfile();
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}
