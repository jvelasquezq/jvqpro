﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Helpers;
using Nhs.Library.Web;
using Nhs.Library.Web.ImageResizer;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects.Constants;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class MyAccountController : BaseDetailController
    {
        private readonly IUserProfileService _userService;
        private readonly IPlannerService _plannerService;
        private readonly IMarketService _marketService;
        private readonly ISearchAlertService _searchAlertService;
        private readonly IPathMapper _pathMapper;
        private readonly ICommunityService _communityService;

        public MyAccountController(IUserProfileService userService, IPlannerService plannerService, IMarketService marketService, ICommunityService communityService, IListingService listingService, ISearchAlertService searchAlertService, IPathMapper pathMapper)
            : base(communityService, listingService, null)
        {
            _userService = userService;
            _plannerService = plannerService;
            _marketService = marketService;
            _pathMapper = pathMapper;
            _communityService = communityService;
            _searchAlertService = searchAlertService;
        }

        public MyAccountController()
        {
        }

        public virtual ActionResult ShowSavedProperties()
        {
            if (UserSession.UserProfile.ActorStatus != WebActors.ActiveUser)
            {
                if (Request.Url != null)
                {
                    var next = HttpUtility.UrlEncode(Request.Url.PathAndQuery);
                    return Redirect(Pages.SignIn, new List<RouteParam> { new RouteParam(RouteParams.NextPage, next, RouteParamType.QueryString) });
                }
            }

            var model = GetSavedPropertiesViewModelModel();
            model.FirstName = UserSession.UserProfile.FirstName;
            model.LastName = UserSession.UserProfile.LastName;
            var searchAlerts = _searchAlertService.GetSearchAlertsByUserGuid(UserSession.UserProfile.UserID);
            model.SearchAlerts = searchAlerts.ToList();

            var metaRegistrar = new MetaRegistrar(_pathMapper);
            var metas = metaRegistrar.GetRobotsMeta("noodp, noydir, noindex");
            base.OverrideDefaultMeta(model, metas);
            base.SetupAdParameters(model);
            return View(model);
        }

        [HttpPost]
        public virtual JsonResult DeleteFavoriteHome(bool isSpec, int listingId)
        {
            var planner = UserSession.UserProfile.Planner;
            try
            {
                planner.RemoveSavedHome(listingId, isSpec);
                return Json(new { valid = true, id = listingId }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public virtual JsonResult DeleteFavoriteComm(int builderId, int communityId)
        {
            var planner = UserSession.UserProfile.Planner;
            try
            {
                planner.RemoveSavedCommunity(communityId, builderId);
                return Json(new { valid = true, id = communityId }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public SavePropertiesViewModel GetSavedPropertiesViewModelModel()
        {
            //This is in order to always have the latest user data when loading MyAccount
            _userService.SignIn(UserSession.UserProfile.LogonName, UserSession.UserProfile.Password, NhsRoute.PartnerId, LanguageHelper.WrongPassword, LanguageHelper.EmailDoNotExist);

            var planner = UserSession.UserProfile.Planner;
            var savedCommunities = planner.SavedCommunities;
            var savedHomes = planner.SavedHomes;

            var planIds = savedHomes.Where(p => ListingType.Plan == p.ListingType).Select(p => p.ListingId.ToType<string>()).ToList();
            var specIds = savedHomes.Where(p => ListingType.Spec == p.ListingType).Select(p => p.ListingId.ToType<string>()).ToList();
            var communityIds = savedCommunities.Select(p => p.ListingId.ToType<string>()).ToList();
            var model = GetFavorites(communityIds, planIds, specIds);
            return model;
        }

        public SavePropertiesViewModel GetFavorites(List<string> communityIds, List<string> planIds, List<string> specIds)
        {

            #region SavedHomes

            var savedHomesTable = _plannerService.GetSavedHomes(StringHelper.ListToString(planIds, ","), StringHelper.ListToString(specIds, ","), NhsRoute.PartnerId);

            var listPlanIds = new List<int>();
            var savedHomeRows = savedHomesTable.Rows;
            var newSavedHomesTable = savedHomesTable.Clone(); // get schema

            for (var rowIndex = 0; rowIndex < savedHomeRows.Count; rowIndex++)
            {
                var planId = DBValue.GetInt(savedHomeRows[rowIndex]["plan_id"]);
                var specId = DBValue.GetInt(savedHomeRows[rowIndex]["specification_id"]);

                if (specId == 0) //listing is not a spec
                {
                    if (!listPlanIds.Contains(planId))
                    {
                        newSavedHomesTable.ImportRow(savedHomeRows[rowIndex]); //import the row to new datatable
                        listPlanIds.Add(planId);
                    }

                }
                else //listing is a spec
                    newSavedHomesTable.ImportRow(savedHomeRows[rowIndex]); //import the row to new datatable
            }

            #endregion

            #region SavedCommunities

            var lstCommunities = new List<string>();

            var savedCommsTable = _plannerService.GetSavedCommunities(StringHelper.ListToString(communityIds, ","), NhsRoute.PartnerId);
            for (var i = 0; i < savedCommsTable.Rows.Count; i++)
            {
                var communityId = savedCommsTable.Rows[i]["community_id"].ToString();
                var builderId = savedCommsTable.Rows[i]["builder_id"].ToString();

                if (!lstCommunities.Contains(communityId + "," + builderId))
                    lstCommunities.Add(communityId + "," + builderId);
                else
                    savedCommsTable.Rows.RemoveAt(i);
            }

            #endregion

            var isMobile = NhsRoute.ShowMobileSite;
            var model = new SavePropertiesViewModel();
            model.FavoritesSaveHome = newSavedHomesTable.AsEnumerable().Select(p => BindHomeInformation(p, isMobile)).ToList();
            model.FavoritesSaveComm = savedCommsTable.AsEnumerable().Select(p => BindCommunityInformation(p, isMobile)).ToList();
            return model;
        }

        public FavoritesSaveComm BindCommunityInformation(DataRow row, bool isMobile)
        {
            var model = new FavoritesSaveComm();
            var address = string.IsNullOrEmpty(DBValue.GetString(row["city"])) ? "{0}{1} {2}" : "{0}, {1} {2}";

            if (row["spotlight_thumbnail"] != null)
            {
                var img = DBValue.GetString(row["spotlight_thumbnail"]);

                if (string.IsNullOrWhiteSpace(img) == false && img.ToLower() != "n")
                {
                    img = img.Replace("//", "/").ToLower();
                    img = ImageResizerUrlHelpers.FormatToIrsImageName(img);
                    model.CommThumbnail = ImageResizerUrlHelpers.BuildIrsImageUrl(img, isMobile ? ImageSizes.HomeThumb : ImageSizes.Small75X58);
                }
                else
                {
                    model.CommThumbnail = isMobile ? Resources.GlobalResources14.Default.images.no_photo.no_photos_180x120_png : Resources.GlobalResources14.Default.images.no_photo.no_photos_75x58_png;
                }
            }
            else
            {
                model.CommThumbnail = isMobile ? Resources.GlobalResources14.Default.images.no_photo.no_photos_180x120_png : Resources.GlobalResources14.Default.images.no_photo.no_photos_75x58_png;
            }

            model.CommunityName = DBValue.GetString(row["community_name"]);
            model.CommunityId = DBValue.GetInt(row["community_id"]);
            model.BuilderId = DBValue.GetInt(row["builder_id"]);
            model.BuilderName = DBValue.GetString(row["brand_name"]);
            model.MarketId = _marketService.GetMarketIdFromStateCity(DBValue.GetString(row["state"]), DBValue.GetString(row["city"]));
            model.MarketName = DBValue.GetString(row["market_name"]);
            model.Address = string.Format(address, DBValue.GetString(row["city"]), DBValue.GetString(row["state"]), DBValue.GetString(row["postal_code"]));
            model.StateName = DBValue.GetString(row["statename"]);
            model.StateAbbr = DBValue.GetString(row["state"]);
            model.CommunityCity = DBValue.GetString(row["city"]);
            model.CommPrice = StringHelper.PrettyPrintRange(DBValue.GetDouble(row["price_low"]), DBValue.GetDouble(row["price_high"]), "c0", LanguageHelper.From.ToTitleCase());
            model.PriceLow = DBValue.GetDecimal(row["price_low"]);
            model.PriceHigh = DBValue.GetDecimal(row["price_low"]);
            model.HomeCount = DBValue.GetString(row["no_home_total"]);
            model.HomeCount += (model.HomeCount != "" ? (" " + LanguageHelper.Home + (model.HomeCount == "1" ? "" : "s")) : LanguageHelper.NoHomesAvailable);
            model.InactiveSavedCommMessage = (int)row["status_id"] != 1;
            model.IsPageCommDetail = true;
            model.IsHotHome = DBValue.GetBool(row["has_hot_home"]).Equals(true);
            model.IsBilled = DBValue.GetBool(row["Billed"]);
            model.PhoneNumber = DBValue.GetString(row["phone_number"]);
            model.HasAgentPromos = DBValue.GetBool(row["agent_promo"]);
            model.HasConsumerPromos = DBValue.GetBool(row["consumer_promo"]);
            model.IsHotHome = DBValue.GetBool(row["consumer_promo"]);
            model.BcType = DBValue.GetString(row["bc_type"]);
            model.IsSpecialOffer = DBValue.GetInt(row["promo_id"]) > 0;
            model.IsComingSoon = model.BcType.ToLower().Equals(BuilderCommunityType.ComingSoon.ToLower()) &&
                                 NhsRoute.BrandPartnerId != Convert.ToInt32(PartnersConst.Pro);
            model.ShowFreebrochure = true;

            List<MediaPlayerObject> comm = null;
            if (!model.InactiveSavedCommMessage)
                comm =
                    _communityService.GetVideoTourImages(model.CommunityId)
                        .Where(video => video.ImageTypeCode == ImageTypes.CommunityVideoTour)
                        .Select(media => new MediaPlayerObject {Url = media.OriginalPath}).ToList();
            if (comm != null && comm.Count > 0)
            {
                model.CommVideo = comm.First().Url;
                model.HasVideo = true;
            }

            if (NhsRoute.IsBrandPartnerNhsPro) return model;

            var notBrochureUrl = _communityService.GetNonPdfBrochureUrl(model.CommunityId, 0, 0, model.BuilderId);
            if (string.IsNullOrEmpty(notBrochureUrl))
            {
                var plannerListing = FindSavedCommunity(model.CommunityId, model.BuilderId);
                var plannerRecommendedListing = FindRecoCommunity(model.CommunityId, model.BuilderId);
                var sourceRequestKey = "";

                if (plannerListing != null && !string.IsNullOrWhiteSpace(plannerListing.SourceRequestKey) && plannerListing.LeadRequestDate < DateTime.Now.AddDays(7))
                    sourceRequestKey = plannerListing.SourceRequestKey;
                else if (plannerRecommendedListing != null &&
                         !string.IsNullOrWhiteSpace(plannerRecommendedListing.SourceRequestKey) && plannerRecommendedListing.LeadRequestDate < DateTime.Now.AddDays(7))
                    sourceRequestKey = plannerRecommendedListing.SourceRequestKey;

                if (!string.IsNullOrEmpty(sourceRequestKey))
                {
                    model.ShowViewBrochure = true;
                    var @params = new List<RouteParam>
                    {
                        new RouteParam(RouteParams.RequestUniqueKey, sourceRequestKey, RouteParamType.QueryString),
                        new RouteParam(RouteParams.Community, model.CommunityId.ToString(), RouteParamType.QueryString)
                    };

                    model.ViewBrochureUrl = @params.ToUrl(Pages.BrochureInterstitial);
                }
            }
            else
            {
                model.ShowViewBrochure = true;
                model.ViewBrochureUrl = notBrochureUrl;
            }

            return model;
        }

        public FavoritesSaveHome BindHomeInformation(DataRow row, bool isMobile)
        {
            var planId = DBValue.GetInt(row["plan_id"]);
            var specId = DBValue.GetInt(row["specification_id"]);
            var marketid = DBValue.GetInt(row["market_id"]);
            var planName = DBValue.GetString(row["plan_name"]);
            string address = string.IsNullOrEmpty(DBValue.GetString(row["city"])) ? "{0}{1} {2}" : "{0}, {1} {2}";
            var model = new FavoritesSaveHome();
            model.ShowFreebrochure = true;
            model.IsPlan = specId == 0;
            model.PlanId = planId;
            model.SpecId = specId;
            model.MarketId = marketid;
            //70746: !string.IsNullOrEmpty(row["price"].ToString()): Relate with zero price ticket 
            model.PriceDisplay = !string.IsNullOrEmpty(row["price"].ToString()) &&
                                 row["home_status"].ToString() != ((int)HomeStatusType.ModelHome).ToType<string>()
                                ? Convert.ToDecimal(row["price"]).ToString("C0")
                                : string.Empty;
            model.DisplaySqFeet = (row["square_feet"] != DBNull.Value)
                ? DBValue.GetFormattedString(Convert.ToInt32(row["square_feet"]), "{0:#,###0}")
                : "N/A";
            model.BedroomsAbbr = "" + DBValue.GetInt(row["no_bedrooms"]);
            model.BathroomsAbbr = ListingHelper.ComputeBathrooms(DBValue.GetInt(row["no_baths"]),
                DBValue.GetInt(row["no_half_baths"]));
            model.GaragesAbbr = string.Format("{0:0.##}", DBValue.GetDecimal(row["no_car_garage"]));

            model.PhoneNumber = DBValue.GetString(row["phone_number"]);
            if (row["image_thumbnail"] != null)
            {
                var img = DBValue.GetString(row["image_thumbnail"]);

                if (string.IsNullOrWhiteSpace(img) == false && img.ToLower() != "n")
                {
                    img = img.Replace("//", "/").ToLower();
                    img = ImageResizerUrlHelpers.FormatToIrsImageName(img);
                    model.ImagePath = ImageResizerUrlHelpers.BuildIrsImageUrl(img, isMobile ? ImageSizes.HomeThumb : ImageSizes.Small75X58);
                }
                else
                {
                    model.ImagePath = isMobile ?  Resources.GlobalResources14.Default.images.no_photo.no_photos_180x120_png : Resources.GlobalResources14.Default.images.no_photo.no_photos_75x58_png;
                }
            }
            else
            {
                model.ImagePath = isMobile ? Resources.GlobalResources14.Default.images.no_photo.no_photos_180x120_png :  Resources.GlobalResources14.Default.images.no_photo.no_photos_75x58_png;
            }

            model.PlanName = planName;
            model.Name = planName;
            model.BrandName = DBValue.GetString(row["brand_name"]);
            model.CommunityName = DBValue.GetString(row["community_name"]);
            model.SalesOfficeAddress1 = string.Format(address, DBValue.GetString(row["city"]),
                DBValue.GetString(row["state"]),
                DBValue.GetString(row["postal_code"]));
            model.HotHome = DBValue.GetBool(row["hot_home"]).Equals(true);
            model.BuilderId = DBValue.GetInt(row["builder_id"]);
            model.CommunityId = DBValue.GetInt(row["community_id"]);
            model.IsBilled = DBValue.GetBool(row["Billed"]);
            model.HasAgentPromos = DBValue.GetBool(row["agent_promo"]);
            model.HasConsumerPromos = DBValue.GetBool(row["consumer_promo"]);
            model.State = DBValue.GetString(row["statename"]);
            model.StateAbbr = DBValue.GetString(row["state"]);
            model.CommunityCity = DBValue.GetString(row["city"]);
            model.ZipCode = DBValue.GetString(row["postal_code"]);
            model.HomeAddress = DBValue.GetString(row["address"]);
            model.MarketName = DBValue.GetString(row["market_name"]);
            if (row["home_status"] != DBNull.Value)
            {
                var statusIcon = GetStatusIcon(DBValue.GetInt(row["home_status"]));
                model.StatusIcon = statusIcon.FullPath;
                model.StatusIconAlt = statusIcon.Description;
            }

            if (NhsRoute.BrandPartnerId != Convert.ToInt32(PartnersConst.Pro))
            {
                var notBrochureUrl = _communityService.GetNonPdfBrochureUrl(model.CommunityId, model.PlanId, model.SpecId, model.BuilderId);

                if (string.IsNullOrEmpty(notBrochureUrl))
                {
                    var plannerListing = FindSavedHome(model.IsPlan ? model.PlanId : model.SpecId,
                        model.IsPlan ? ListingType.Plan : ListingType.Spec);
                    var sourceRequestKey = "";

                    if (plannerListing != null && !string.IsNullOrWhiteSpace(plannerListing.SourceRequestKey)
                        && plannerListing.LeadRequestDate < DateTime.Now.AddDays(7))
                        sourceRequestKey = plannerListing.SourceRequestKey;

                    if (!string.IsNullOrEmpty(sourceRequestKey))
                    {
                        model.ShowViewBrochure = true;
                        var @params = new List<RouteParam>
                        {
                            new RouteParam(RouteParams.RequestUniqueKey, sourceRequestKey, RouteParamType.QueryString),
                            model.IsPlan
                                ? new RouteParam(RouteParams.PlanId, model.PlanId.ToString(), RouteParamType.QueryString)
                                : new RouteParam(RouteParams.SpecId, model.SpecId.ToString(), RouteParamType.QueryString)
                        };

                        model.ViewBrochureUrl = @params.ToUrl(Pages.BrochureInterstitial);
                    }
                }
                else
                {
                    model.ShowViewBrochure = true;
                    model.ViewBrochureUrl = notBrochureUrl;
                }
            }

            return model;
        }

        private PlannerListing FindSavedHome(int listingId, ListingType listingType)
        {
            return UserSession.UserProfile.Planner.SavedHomes.FirstOrDefault(pl => pl.ListingId == listingId && pl.ListingType == listingType);
        }

        private PlannerListing FindSavedCommunity(int communityId, int builderId)
        {
            return UserSession.UserProfile.Planner.SavedCommunities.FirstOrDefault(pl => pl.ListingId == communityId && pl.BuilderId == builderId);
        }

        private PlannerListing FindRecoCommunity(int communityId, int builderId)
        {
            return UserSession.UserProfile.Planner.RecommendedListings.FirstOrDefault(pl => pl.ListingId == communityId && pl.BuilderId == builderId);
        }

        private MediaObject GetStatusIcon(int homeStatus)
        {

            var statusIcon = new MediaObject();
            var imagePath = Configuration.ResourceDomain + "/globalResources/default/images/icons/";
            var description = string.Empty;
            var name = string.Empty;

            switch (homeStatus)
            {
                case (int)HomeStatusType.AvailableNow:
                    name = "status_available.gif";
                    description = LanguageHelper.AvailableNow;
                    break;
                case (int)HomeStatusType.UnderConstruction:
                    name = "status_under_construction.gif";
                    description = LanguageHelper.UnderConstruction;
                    break;
                case (int)HomeStatusType.ReadyToBuild:
                    name = "status_ready.gif";
                    description = LanguageHelper.ReadyToBuild;
                    break;
                case (int)HomeStatusType.ModelHome:
                    name = "status_model_home.gif";
                    description = LanguageHelper.ModelHome;
                    break;
            }
            statusIcon.Path = imagePath;
            statusIcon.Name = name;
            statusIcon.Description = description;

            return statusIcon;
        }
    }
}
