﻿/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright © 2001-2010 Builders Digital Experience, LLC.
 * ========================================================
 * Author: Praveen Guggarigoudar
 * Date: 09/22/2010
 * Edit History (please mention edits succinctly):
 * 12/15/2010 - Praveen - Moved helper methods over to BaseController
 * =========================================================
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Route;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Filters;
using Nhs.Web.UI.AppCode.Helper;

namespace Nhs.Web.UI.AppCode.Controllers
{
    /// <summary>
    /// All global behavior is applied in the form of action filters on this base controller
    /// NOTE: All view-level controllers should inherit from ApplicationController.
    /// NOTE: All partial-view-level controllers should be implemented in PartialViewsController
    /// </summary>

    [Instrumentation(Order = 1)]
    [LiveChat(Order = 2)]
    //[ShellFilter(Order = 3)]
    [PageHeader(Order = 4)]
    [PartnerInfo(Order = 5)]
    [PersonalCookieFilter(Order = 7)]
    public partial class ApplicationController : BaseController
    {
        public ApplicationController(IPathMapper pathMapper)
            : base(pathMapper)
        {
            // Check for refer URL Parameter.  If exists, set session value for refer.
            // This check is done in page base so the refer tag is set in the session object
            // no matter which page was used as the entry page.  Refer tag is set in session and 
            // passed along during lead generation so leads can be tracked by refer tags. 

            if (NhsRoute.CurrentRoute == null)
            {
                NhsUrl url = new NhsUrl("MvcError");
                url.AddParameter(UrlConst.ErrorStatusCode, "404", RouteParamType.QueryString);
                url.Redirect();
            }

            var refer = RouteParams.Refer.Value<string>();
            if (!string.IsNullOrEmpty(refer))
            {
                UserSession.Refer = refer;
                UserSession.DynamicRefer = refer;
            }
        }

        /// <summary>
        /// Creates cookie for Home Page State
        /// This is for NHS Pro Only
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            //NHS Pro only
            if (NhsRoute.IsBrandPartnerNhsPro)
            {
                HttpCookie nhsProCookie = new HttpCookie("NHSPro_Cookie");
                DateTime expirationDate = DateTime.Now.AddMonths(1);
                nhsProCookie.Expires = expirationDate;
                nhsProCookie.Value = expirationDate.ToShortDateString();

                HttpContext.Request.Cookies.Remove("NHSPro_Cookie");
                HttpContext.Response.Cookies.Add(nhsProCookie);
            }

            base.Dispose(disposing);
        }

    public ApplicationController()
        {
            
        }



        protected string GetReferRedirectUrl(Market market, bool isCommunityResults)
        {
            var paramsAndHashParams = NhsRoute.CurrentRoute.Params.ToList();
            var listOfRedirectParams = GetReferRedirectListParams();
            var page = isCommunityResults ? RedirectionHelper.GetCommunityResultsPage() : RedirectionHelper.GetHomeResultsPage();
            
            if (listOfRedirectParams.Any())
            {
                // The list of parameters always have the State and the Market
                CookieManager.RedirectFromRefer = true;
                foreach (var param in listOfRedirectParams)
                {
                    var hashParam = paramsAndHashParams.FirstOrDefault(p => p.Name == param.Name);
                    if (hashParam != null)
                        hashParam.ParamType = RouteParamType.Hash;
                }

                return paramsAndHashParams.ToResultsParams().ToUrl(page);
            }

            if (paramsAndHashParams.Exists(p => p == RouteParams.CityNameFilter || p == RouteParams.City))
            {
                var parameter = paramsAndHashParams.FirstOrDefault(p => p == RouteParams.CityNameFilter || p == RouteParams.City);
               if(parameter!=null && parameter.Value.Contains(" "))
                   return paramsAndHashParams.ToResultsParams().ToUrl(page);
            }
            return string.Empty;
        }

        protected string GetReferRedirectUrl(Community community)
        {
            var listOfRedirectParams = GetReferRedirectListParams();

            if (listOfRedirectParams.Any())
            {
                CookieManager.RedirectFromRefer = true;
                var page = RedirectionHelper.GetCommunityDetailPage();
                var parameters = community.ToCommunityDetail();
                parameters.AddRange(listOfRedirectParams);
                return parameters.ToUrl(page);
            }
            
            return string.Empty;
        }

        protected string GetReferRedirectUrl(ISpec spec, IPlan plan, bool isSpec)
        {
            var listOfRedirectParams = GetReferRedirectListParams();

            if (listOfRedirectParams.Any())
            {
                CookieManager.RedirectFromRefer = true;
                var page = RedirectionHelper.GetHomeDetailPage(isSpec);

                var parameters = isSpec ? spec.ToHomeDetail() : plan.ToHomeDetail();
                parameters.AddRange(listOfRedirectParams);

                return parameters.ToUrl(page);
            }

            return string.Empty;
        }

        private List<RouteParam> GetReferRedirectListParams()
        {
            var paramsTo301 = new List<string>()
            {
                "utm_source",
                "utm_medium",
                "utm_term",
                "utm_content",
                "utm_campaign",
                "gclid",
                "refer",
                "mkwid",
                "pcrid"
            };
            var paramsSansHashParam = NhsRoute.CurrentRoute.Params.ToList();
            var hashParams = new List<RouteParam>();

            foreach (var hashParam in paramsSansHashParam.Where(p => paramsTo301.Contains(p.Name.ToString().ToLower())))
            {
                hashParam.ParamType = RouteParamType.Hash;
                hashParams.Add(hashParam);
            }

            return hashParams;
        }


        public string RenderRazorViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }
    }
}
