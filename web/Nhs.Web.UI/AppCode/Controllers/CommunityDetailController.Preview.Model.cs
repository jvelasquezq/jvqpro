﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.Script.Serialization;
using Kendo.Mvc.Extensions;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Route;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Hub;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects;
using Nhs.Search.Objects.Constants;
using Nhs.Utility.Common;
using Nhs.Utility.Data.Paging;
using Nhs.Web.UI.AppCode.Extensions;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Web.UI.AppCode.Helper;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class CommunityDetailController
    {
        private CommunityDetailViewModel GetCommunityDetailPreviewModel(NhsPropertySearchParams searchParams, int page, int communityId)
        {
            _communityService.UseHub = true;
            var community = _communityService.GetHubCommunity(communityId);

            if (community == null)
            {
                HttpContext.Response.Redirect(RedirectTo404().Url);
                return new CommunityDetailViewModel();
            }

            _searchParameters = searchParams;
            _searchParameters.CommunityId = communityId;
            _searchParameters.SortOrder = SortOrder.Status;
            _searchParameters.MarketId = community.MarketId;
            _searchParameters.ListingTypeFlag = community.ListingTypeFlag;

            var viewModel = BindCommunityInformation(community, page);

            AssembleDetailViewModel(viewModel, community);
            AssemblePropertyMapViewModel(viewModel, community);
            AssembleCommunityDetailBaseModel(viewModel, community);
            GetAffiliateLinks(viewModel);

            return viewModel;
        }

        private CommunityDetailViewModel BindCommunityInformation(HubCommunity community, int page)
        {
            var sortActions = new SortActions();
            var resultsView = GetHomesInPreviewMode(community).ToList();
            var extendedHomeResults = _listingService.GetExtendedHomeResults(NhsRoute.PartnerId, sortActions.SortOptions[_searchParameters.SortOrder](resultsView), false);
            var pageSize = resultsView.Count() <= 40 ? resultsView.Count() : 12;
            var showNewHomesExtraTab = extendedHomeResults.Count < community.HomeCount.ToType<int>();
            var pagedList = pageSize > 0 ? extendedHomeResults.ToPagedList(page, pageSize) : null;
            var brand = community.Brand ?? _brandService.HubBrands.FirstOrDefault(c => c.BrandId == community.BrandId);

            var viewModel = new CommunityDetailViewModel
            {
                PreviewMode = true,
                IsPhoneNumberVisible = false,
                ShowLeadForm = true,
                ShowExpiredRequestBrochureLink = false,
                HasHotHomes = community.HasHotHome.ToType<int>(),
                BuilderLogo = brand != null ? brand.LogoMedium : string.Empty,
                BuilderLogoSmall = brand != null ? brand.LogoSmall : string.Empty,
                ShowHotHomeSection = community.HasHotHome.ToType<int>() == 1,
                AllNewHomesCount = community.HomeCount.ToType<int>(),
                QuickMoveInCount = community.QuickMoveInCount.ToType<int>(),
                HomeResults = pagedList,
                HotHomes = NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>() ? (from r in extendedHomeResults where r.IsHotHome select r).Take(2).ToList()
                                                                                      : new List<ExtendedHomeResult>(),
                SelectedTab = TabSelected(_searchParameters.HomeStatus, showNewHomesExtraTab),
                SelectedStateAbbr = community.StateAbbr,
                PageLinkAction = "Show",
                MarketName = community.Market.MarketName,
                SaveCommunitySuccessMessage = LanguageHelper.MSG_COMMDETAIL_ADDED_TO_PLANNER,
                AddCommunityToPlannerUrl = GetAddCommunityToPlannerUrl(community.CommunityId, community.BuilderId),
                SalesOfficeAddress1 = (!string.IsNullOrEmpty(community.Address1) && community.OutOfCommunityFlag.ToType<bool>()) ? community.Address1
                                                                                                                                 : community.SalesOffice.Address1,
                SalesOfficeAddress2 = (!string.IsNullOrEmpty(community.Address2) && community.OutOfCommunityFlag.ToType<bool>()) ? community.Address2
                                                                                                                                 : community.SalesOffice.Address2,
                LogActionMethodUrl = GetMethodUrl(community.CommunityId, community.BuilderId),
                HomeSearchViewMetricDisplayed = string.Empty,
                HomeSearchViewMetricHidden = (pagedList != null) ? HomeSearchViewMetricHidden(pagedList, community.HomeCount.ToType<int>(), community.MarketId) 
                                                                 : string.Empty,
                IsBilled = _communityService.GetCommunityBilledStatus(NhsRoute.PartnerId, community.BuilderId, community.CommunityId, community.MarketId),
            };

            viewModel.ShowSocialIcons = ShowSocialIcons(viewModel.IsBilled);
            GetCommunityName(viewModel, community);
            return viewModel;
        }

        private void GetCommunityName(CommunityDetailViewModel viewModel, HubCommunity community)
        {
            var dpSeo = new SeoContentManager
            {
                BrandID = viewModel.BrandId.ToString(CultureInfo.InvariantCulture),
                BrandName = viewModel.BrandName,
                City = viewModel.CommunityCity,
                CommunityName = viewModel.CommunityName,
                State = viewModel.State,
                Market = community.Market.MarketName,
                Zip = community.PostalCode
            };

            var contentTags = new List<ContentTag>
                {
                    new ContentTag(ContentTagKey.CommunityName, community.CommunityName),
                    new ContentTag(ContentTagKey.BrandID, community.BrandId.ToString(CultureInfo.InvariantCulture)),
                    new ContentTag(ContentTagKey.BrandName, community.Brand.BrandName),
                    new ContentTag(ContentTagKey.City, community.City),
                    new ContentTag(ContentTagKey.StateName, community.StateAbbr),
                    new ContentTag(ContentTagKey.MarketName, community.Market.MarketName),
                    new ContentTag(ContentTagKey.Zip, community.PostalCode),
                };
            var dataSeo = dpSeo.GetSeoDetailPages(community.CommunityId, contentTags);
            viewModel.Globals.H1 = dataSeo.H1;
        }

        private void GetAffiliateLinks(CommunityDetailViewModel viewModel)
        {
            var tags = new List<ContentTag> { new ContentTag(ContentTagKey.CommunityId, viewModel.CommunityId.ToString()) };
            viewModel.AffiliateLinksData = new AffiliateLinksData();

            if (NhsRoute.IsBrandPartnerNhsPro)
                return;

            var links = _affiliateLinkService.GetAffiliateLinks(Pages.CommunityDetail, NhsRoute.PartnerId);
            var freeCreditScore = links.LinksList.Find(x => x.Keyword.IsCaseInsensitiveEqual(AffiliateLinksTypes.FreeCreditScore));
            if (freeCreditScore != null)
                viewModel.AffiliateLinksData.FreeCreditScoreFormArea = freeCreditScore.ToLink(tags);

            var mortgageRates = links.LinksList.Find(x => x.Keyword.IsCaseInsensitiveEqual(AffiliateLinksTypes.MortgageRates));
            if (mortgageRates != null)
                viewModel.AffiliateLinksData.MortageRatesFormArea = mortgageRates.ToLink(tags);

            var calcMortgageRates = links.LinksList.Find(x => x.Keyword.IsCaseInsensitiveEqual(AffiliateLinksTypes.CalculateMortgagePayments));
            if (calcMortgageRates != null)
                viewModel.AffiliateLinksData.CalculateMortagePaymentsNexStepsArea = calcMortgageRates.ToLink(tags);
        }

        private IEnumerable<HomeResult> GetHomesInPreviewMode(HubCommunity community)
        {
            return _communityService.GetMatchingHomesForHubCommunity(community.CommunityId);
        }

        private void AssembleDetailViewModel(IDetailViewModel model, HubCommunity community)
        {
            var partnerUsesMatchmaker = _partnerService.GetPartner(NhsRoute.PartnerId).UsesMatchMaker.ToBool();
            model.PartnerUsesMatchmaker = partnerUsesMatchmaker;

            //Prepop User Info
            if (partnerUsesMatchmaker && !UserSession.HasUserOptedIntoMatchMaker)
                model.SendAlerts = true;

            model.UserPostalCode = UserSession.UserProfile.PostalCode;
            model.MailAddress = UserSession.UserProfile.LogonName;
            model.Name = string.Concat(UserSession.UserProfile.FirstName, " ", UserSession.UserProfile.LastName).Trim();
            //Brand Info
            model.BrandThumbnail = string.Concat(Configuration.ResourceDomain, community.Brand.LogoMedium);
            model.BrandName = community.Brand.BrandName ?? string.Empty;

            //Builder Info
            model.BuilderName = community.Builder.BuilderName ?? string.Empty;

            //Info to display
            model.BcType = community.BCType;
            model.ZipCode = community.PostalCode;
            model.State = community.SalesOffice.State;
            model.StateAbbr = community.State.StateAbbr;
            model.PhoneNumber = community.PhoneNumber;
            model.HomesAvailable = community.HomeCount > 0 ? HomesAvailableLabel(community.HomeCount) : "New homes coming soon";
            model.ShowMortgageLink = community.ShowMortgageLink == "Y";
            model.PriceDisplay = FormattedPrice(community.PriceHigh, community.PriceLow, community.BCType);
            model.HoursOfOperation = community.SalesOffice.HoursOfOperation.ToType<string>();
            model.DisplaySqFeet = FormattedSqFeet(community.SqFtLow, community.SqFtHigh);
            model.SaleAgents = _communityService.GetSalesAgents(community.SalesOfficeId);
            //TODO model.EnvisionUrl = community.EnvisionLink;
            model.PriceLow = community.PriceLow.ToString();
            model.PriceHigh = community.PriceHigh.ToString();

            //Flags
            model.ShowSaveToAccount = SetSaveToAccountFlag();

            //Multimedia data
            //TODO model.Videos = community.Videos;
            model.GreenPrograms = _communityService.GetCommunityGreenPrograms(community.CommunityId).Take(2).ToList();
            model.Promotions = new Collection<Promotion>();
            model.Events = new Collection<Event>();
            if (NhsRoute.IsBrandPartnerNhsPro)
            {
                if (model.IsBilled) //Changes because 58867
                {
                    var events =
                         _communityService.GetCommunityEvents(community.CommunityId, community.BuilderId).ToList();
                    model.Events = events.Select(e => new Event
                    {
                        EventId = e.EventID,
                        Title = e.Title,
                        Description = e.Description,
                        EventFlyerUrl = e.EventFlyerURL,
                        EventType = e.EventTypeCode,
                        EventEndDate = e.EventEndTime,
                        EventStartDate = e.EventStartTime
                    })
                                         .ToList();

                }
                var promotions =
                   _communityService.GetCommunityDetailPromotions(community.CommunityId, community.BuilderId)
                                    .ToList();
                if (!model.IsBilled)//Changes because 58867
                    promotions.RemoveAll(promo => promo.PromoTypeCode != "COM");

                model.Promotions = promotions.Select(p => new Promotion
                {
                    PromoId = p.PromoID,
                    PromoTextShort = p.PromoTextShort,
                    PromoTextLong = p.PromoTextLong,
                    PromoFlyerUrl = p.PromoFlyerURL,
                    PromoType = p.PromoTypeCode,
                    PromoEndDate = p.PromoEndDate,
                    PromoStartDate = p.PromoStartDate,
                    PromoUrl = p.PromoURL
                }).ToList();

            }
            else
                model.Promotions = _communityService.GetCommunityPromotions(community.CommunityId, community.BuilderId).Take(3).ToList();
            model.AwardImages = _communityService.GetAwardImages(community.AllHubImages);
            model.Testimonials = _communityService.GetCustomerTestimonials(5, community.CommunityId, community.BuilderId);


            if (RouteParams.ShowPopupPlayer.Value<bool>())
            {
                model.PlayerMediaObjects = _communityService.GetMediaPlayerObjects(community.CommunityId, true, false, false, ImageSizes.Large).ToList();
                foreach (var ext in model.ExternalMediaLinks)
                    model.PlayerMediaObjects.Add(ext);

                model.PlayerMediaObjects = model.PlayerMediaObjects.OrderBy(m => (m.Type == "v" ? 1 : (m.Type == "i" ? 2 : 3))).ThenBy(m => m.Sort).ToList();
            }
            model.Size = new KeyValuePair<int, int>(460, 307);
            model.PageUrl = model.Globals.CurrentUrl;
        }

        /// <summary>
        /// this method fills out the basic data in the Model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="community"></param>
        private void AssembleCommunityDetailBaseModel(CommunityDetailBaseViewModel model, HubCommunity community)
        {
            model.IsPageCommDetail = true;
            model.BrandId = community.BrandId;
            model.MarketId = community.MarketId;
            model.CommunityCity = community.City;
            model.Utilities = _communityService.GetCommunityUtilities(community.CommunityId, NhsRoute.PartnerId);
            model.BuilderId = community.BuilderId;
            model.CommunityId = community.CommunityId;
            model.CommunityName = community.CommunityName;
            model.BuilderName = community.Builder.BuilderName;
            model.MarketName = community.Market.MarketName;
            model.BuilderUrl = !string.IsNullOrEmpty(community.Builder.Url) ? community.Builder.Url
                               : (!string.IsNullOrEmpty(community.Builder.ParentBuilder.Url) ? community.Builder.ParentBuilder.Url
                                                                                             : string.Empty);
            model.CommunityUrl = community.CommunityUrl ?? string.Empty;
            model.Amenities = _communityService.GetAmenities(community.IAmenities, community.IOpenAmenities);
            model.SearchParametersJSon = new JavaScriptSerializer().Serialize(_searchParameters);
            model.Schools = _communityService.GetSchools(NhsRoute.PartnerId, community.CommunityId);
            model.CommunityDescription = FormattedText(community.CommunityDescription);
        }

        private void AssemblePropertyMapViewModel(IPropertyMapViewModel model, HubCommunity community)
        {
            model.MarketId = community.MarketId;
            model.CommunityId = community.CommunityId;
            model.BuilderId = community.BuilderId;
            model.Latitude = (double)community.Latitude;
            model.Longitude = (double)community.Longitude;
            model.DrivingDirections = FormattedText(community.SalesOffice.DrivingDirections) ?? string.Empty;
            //model.NearByComms = _communityService.GetNearbyCommunities(NhsRoute.PartnerId, community.CommunityId, community.BuilderId, null);
            var img = _communityService.GetBuilderMap(community.AllHubImages);

            if (img != null)
                model.BuilderMapUrl = img.ImagePath + img.ImageTypeCode + "_" + img.ImageName;

            model.UnderneathMapCommName = community.CommunityName;
            model.UnderneathMapAddress = ((!string.IsNullOrEmpty(community.Address1) && community.OutOfCommunityFlag.ToType<bool>()) ? community.Address1 : community.SalesOffice.Address1) +
                                         " <br >" + community.City + ", " + community.State.StateName + " " + community.PostalCode;

            model.EnableRequestAppointment = true;

        }
    }
}
