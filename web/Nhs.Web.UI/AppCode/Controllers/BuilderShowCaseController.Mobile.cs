﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Helpers.Vimeo;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class BuilderShowCaseController
    {
        public virtual ActionResult GetMoreLocations(int brandid, int page)
        {
            var brand = _brandService.GetBrandById(brandid, NhsRoute.BrandPartnerId);
            List<Community> comms;

            if (!IsShowCasePageActive(brand, out comms))
                return RedirectPermanent(Pages.Home, new List<RouteParam>());

            var viewModel = new BuilderShowCaseViewModel
            {
                BrandId = brandid,
                BrandName = brand.BrandName,
                Communities = comms
            };

            viewModel.MetroAreaList = new BuilderShowCaseMetroAreaList();
            GetMetroAreaList(viewModel);
            
            if (page >= 1)
                viewModel.MetroAreaList.MetroAreas = viewModel.MetroAreaList.MetroAreas.Skip(page*8).Take(8);

            return PartialView(NhsMvc.Default.ViewsMobile.BuilderShowCase.PartialViews.ShowMetroAreas, viewModel);
        }

        public virtual ActionResult GetGallery(int brandId, bool includeVideo)
        {
            var viewModel = new BuilderShowCaseViewModel();
            GetImagesAndVideos(viewModel, brandId);

            var mediaPlayerObjects = new List<MediaPlayerObject>();

            if (includeVideo)
            {
                mediaPlayerObjects.AddRange(viewModel.ShowcaseVideos.OrderBy(video => video.Order).Select(video => new MediaPlayerObject
                {
                    Type = MediaPlayerObjectTypes.Video,
                    SubType = video.IsFromYoutube ? MediaPlayerObjectTypes.SubTypes.YoutubeVideo : (video.IsFromVimeo ? MediaPlayerObjectTypes.SubTypes.Vimeo : MediaPlayerObjectTypes.SubTypes.BrightcoveVideo),
                    IsVimeoVideo = video.IsFromVimeo,
                    IsYouTubeVideo = video.IsFromYoutube,
                    VideoID = string.Format("bid-{0}-{1}-{2}", video.BuilderId, video.VideoId, Configuration.BrightcoveEnvSuffix),
                    Thumbnail = video.ThumbnailUrl,
                    OnlineVideoID = video.VideoOnlineId,
                    Title = video.Title,
                    RefID = video.VideoId,
                    Url = video.VideoURL,
                    Sort = video.Order,
                }));
            }

            mediaPlayerObjects.AddRange(
                viewModel.AwardImages.Where(p => p.ImageName.IsValidImage()).Select(image => new MediaPlayerObject
                {
                    Type = MediaPlayerObjectTypes.Image,
                    Title = image.ImageTitle,
                    Thumbnail = string.Format("{0}/{1}", image.ImagePath, image.ImageName),
                    Caption = image.ImageDescription,
                    Url = string.Format("{0}/{1}", image.ImagePath, image.ImageName)
                }));

            return PartialView(NhsMvc.Default.ViewsMobile.CommunityDetail.Gallery, mediaPlayerObjects.ToList());
        }

        public virtual ActionResult GetAskQuestionModal(int brandId)
        {
            var brand = _brandService.GetBrandById(brandId, NhsRoute.BrandPartnerId);
            var viewModel = new BuilderShowCaseViewModel
            {
                BrandId = brand.BrandId,
                BrandName = brand.BrandName,
                BrandEmailAddress = brand.BrandShowCase != null ? brand.BrandShowCase.ContactEmail : string.Empty
            };
            return PartialView(NhsMvc.Default.ViewsMobile.BuilderShowCase.PartialViews.AskQuestionModal, viewModel);
        }

    }
}