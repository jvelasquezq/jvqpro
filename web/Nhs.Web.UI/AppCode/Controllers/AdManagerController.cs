﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Xml;
using Nhs.Library.Business;
using Nhs.Library.Business.Ad;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Helpers.Ad;
using Nhs.Library.Helpers.Content;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Mvc.Routing.Utils;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Extensions;
using Nhs.Web.UI.AppCode.Filters;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult;
using StructureMap;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class AdManagerController : ApplicationController
    {
        private readonly IApiService _apiService;
        private readonly IPathMapper _pathMapper;
        private readonly IBuilderService _builderService;

        public AdManagerController(IPathMapper pathMapper, IBuilderService builderService, IApiService apiService)
            : base(pathMapper)
        {
            _pathMapper = pathMapper;
            _builderService = builderService;
            _apiService = apiService;
        }

        [NoCache]
        public virtual JsonResult GetAdMarkup(string url, string adPosition, string adParams, bool useIFrame, bool javaScriptParams)
        {
            var ads = GetAdsData(url, adParams, useIFrame, javaScriptParams);
            return Json(ads, JsonRequestBehavior.AllowGet);
        }

        [NoCache]
        public virtual JsonResult GetSingleAdMarkup(string url, string adPosition, string adParams, bool useIFrame, bool javaScriptParams)
        {
            var ad = GetAdsData(url, adParams, useIFrame, javaScriptParams).FirstOrDefault(a => a.TargetPlaceHolder == adPosition);
            return Json(ad, JsonRequestBehavior.AllowGet);
        }

        [NoCache]
        public virtual JsonResult GetSkinAdMarkup(string url, string adPosition, string adParams, int marketId)
        {
            AdData ad;// = new AdData();
            var brandPartnerId = NhsRoute.BrandPartnerId;
            string cacheKey = string.Concat("AdsSkinConfig_brandPartner_", brandPartnerId);
            var pathMapper = ObjectFactory.GetInstance<IPathMapper>();
            var cache = ObjectFactory.GetInstance<IWebCacheService>();

            var brandPartners = cache.Get(cacheKey, false, () => GetValidMarketsSkin(pathMapper)) ?? new List<AdsBrandPartner>();

            var isValid = IsValidMarketSkin(brandPartners, brandPartnerId, NhsRoute.PartnerId, marketId);

            if (isValid == false)
            {
                ad = new AdData();
                return Json(ad, JsonRequestBehavior.AllowGet);
            }

            ad = GetAdsData(url, adParams, false, false, true).FirstOrDefault(a => a.TargetPlaceHolder == adPosition) ?? new AdData();

            return Json(ad, JsonRequestBehavior.AllowGet);
        }

        private List<AdsBrandPartner> GetValidMarketsSkin(IPathMapper pathMapper)
        {
            var brandPartners = new List<AdsBrandPartner>();
            string xmlFileName =
                pathMapper.MapPath(@"~/" + Configuration.StaticContentFolder + @"/Ads/CommResultsSkin/ActiveMarkets.xml");

            try
            {
                if (xmlFileName.Length <= 0 || !System.IO.File.Exists(xmlFileName))
                {
                    throw new Exception("Invalid XML file name or file does not exist");
                }

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(xmlFileName);

                foreach (XmlNode bp in xmlDoc.GetElementsByTagName("brandpartner"))
                {
                    if (bp.Attributes != null)
                    {
                        var abp = new AdsBrandPartner
                        {
                            BrandPartnerId = bp.Attributes["id"].Value.ToType<int>(),
                            AllPartners = bp.Attributes["allpartners"].Value.ToType<bool>()
                        };

                        var mkts = bp.ChildNodes.Cast<XmlNode>()
                            .Select(m => m.Attributes)
                            .Where(xmlAttributeCollection => xmlAttributeCollection != null)
                            .Select(xmlAttributeCollection => xmlAttributeCollection["id"].Value.ToType<int>()).ToList();
                        abp.Markets = mkts;
                        brandPartners.Add(abp);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }

            return brandPartners;
        }

        private bool IsValidMarketSkin(IEnumerable<AdsBrandPartner> brandPartners, int brandPartnerId, int partnerId, int marketId)
        {
            var isValid = false;

            if (marketId <= 0) return isValid;

            if (brandPartnerId == PartnersConst.Pro.ToType<int>()) return isValid;

            if (partnerId != brandPartnerId) return isValid;

            if (brandPartners == null) return isValid;

            var partner = brandPartners.FirstOrDefault(b => b.BrandPartnerId == brandPartnerId);

            if (partner != null)
            {
                isValid = partner.Markets.Any(m => m == marketId);
            }

            return isValid;
        }

        private void FilterPaidBuilderIds(List<string> allParams)
        {
            var listIds = new List<string>();
            var marketId = 0;

            if (allParams.Any(a => a.StartsWith("m=")))
            {
                var market = allParams.FirstOrDefault(m => m.StartsWith("m=")) ?? "0";
                marketId = market.Split('=')[1].ToType<int>();
            }

            if (marketId <= 0)
            {
                marketId = UserSession.PersonalCookie.MarketId;
            }

            if (marketId <= 0) return;

            var builderIds = _builderService.GetMarketBuilders(NhsRoute.PartnerId, marketId);
            var adBuilderIds = allParams.Where(w => w.StartsWith("b")).Select(c => c).ToList();

            foreach (var adBuilderId in adBuilderIds)
            {
                allParams.Remove(adBuilderId);
            }

            UserSession.AdBuilderIds.Clear();

            if (builderIds.Any(p => p.IsBilled) && adBuilderIds.Any())
            {
                var builderIdsBilled = builderIds.Where(w => w.IsBilled).Select(c => FormatBuilderId(c.BuilderId));

                listIds.AddRange(adBuilderIds.Where(adBuilderId => builderIdsBilled.Any(b => b == adBuilderId)));
                allParams.AddRange(listIds);
                UserSession.AdBuilderIds.AddRange(listIds.Select(c => c.Replace("b", string.Empty).ToType<int>()));
            }
        }

        private static string FormatBuilderId(int builderId)
        {
            var value = builderId.ToString(CultureInfo.InvariantCulture);
            var ss = new StringBuilder(value);

            while (ss.Length <= 3)
            {
                ss.Insert(0, "0");
            }

            ss.Insert(0, "b");
            return ss.ToString();
        }

        private List<AdData> GetAdsData(string url, string adParams, bool useIFrame, bool javaScriptParams, bool getSkin = false)
        {
            List<AdSlot> slots;
            AdController adController;
            var allPositions = FillAdsParameters(url, adParams, out slots, out adController);

            var ads = new List<AdData>();
            var count = 1;
            foreach (var position in allPositions)
            {
                var targetAdSlot = slots.Where(s => s.AdPositions.Contains(position)).ToList();
                if (targetAdSlot.Any())
                {
                    foreach (var adSlot in targetAdSlot)
                    {
                        ads.Add(new AdData
                        {
                            AdPosition = position,
                            AdHtml =
                                AdHelper.GetAdControl(position, useIFrame, javaScriptParams, adController, countRef: count, getCommResultsSkin: getSkin)
                                    .ToString(),
                            TargetPlaceHolder = adSlot != null ? adSlot.PlaceHolder : string.Empty,
                            Width = adSlot != null ? adSlot.Width : string.Empty,
                            Height = adSlot != null ? adSlot.Height : string.Empty,
                        });
                        count++;
                    }
                }
            }
            return ads;
        }

        private List<string> FillAdsParameters(string url, string adParams, out List<AdSlot> slots, out AdController adController)
        {
            var adPages = AdPageReader.GetAdPages(_pathMapper, NhsRoute.BrandPartnerId, NhsRoute.ShowMobileSite);
            url = url.AddLeadingSlash().AddTrailingSlash();

            var currentRoute = RouteHelper.GetRouteByUrl(new Uri(Configuration.NewHomeSourceDomain + url));

            if (url == "/")
            {
                url = "/home";
            }

            var allParams = JsonHelper.FromJson<List<string>>(adParams);

            FilterPaidBuilderIds(allParams);

            var adPageCode = string.Empty;
            var allPositions = new List<string>();

            var adPage =
                adPages.FirstOrDefault(
                    a => url.ToLower().Contains(a.UrlPattern.ToLower()) && !string.IsNullOrEmpty(a.UrlPattern));

            if (adPage != null)
            {
                adPageCode = adPage.AdPageCode;
                allPositions = adPage.Positions;
            }

            slots = AdSlotReader.GetAdSlots(_pathMapper, NhsRoute.ShowMobileSite);

            //if no adpositions defined in the route config, pick default positions for a slot
            if (!allPositions.Any())
                allPositions = slots.Select(s => s.AdPositions.FirstOrDefault()).ToList();

            //apply any overrides
            allPositions = this.ApplyOverrides(allPositions, currentRoute.Function);

            adController = new AdController
            {
                AdPageName = GetAdPageCode(adPageCode),
                AllPositions = allPositions,
                Parameters = allParams
            };
            return allPositions;
        }

        private string GetAdPageCode(string adPageCode)
        {
            var code = string.IsNullOrEmpty(adPageCode) ? "udf" : adPageCode;
            return code;
        }

        private List<string> ApplyOverrides(List<string> positions, string pageFunction)
        {
            var originalPosition = string.Empty;
            var overriddenPosition = string.Empty;

            if (PartnerLayoutHelper.AmIOnDetailPage(pageFunction))
            {
                originalPosition = "x04";
                overriddenPosition = "x01";
            }

            if (!string.IsNullOrEmpty(overriddenPosition))
                positions = positions.Select(p => p.Replace(originalPosition, overriddenPosition)).ToList();

            return positions;
        }

        public virtual ActionResult AdTestClient()
        {
            var model = new BaseViewModel();
            model.Globals.AdController.AddMarketParameter(269);
            model.Globals.AdController.AddCommunityParameter(44745);
            model.Globals.AdController.AddPriceParameter(100000, 500000);
            model.Globals.AdController.AddStateParameter("TX");
            model.Globals.AdController.AddBuilderParameter(1939);
            model.Globals.AdController.AddCityParameter("Cedar Park");
            model.Globals.AdController.AddZipParameter("78750");
            // ReSharper disable Mvc.ViewNotResolved
            return View(model);
            // ReSharper restore Mvc.ViewNotResolved
        }

        [HttpGet]
        public virtual JsonpResult FillDynamicAdBannerInformation(IList<int> marketIds, IList<int> builderIds)
        {
            if (marketIds == null || builderIds == null || !marketIds.Any() || !builderIds.Any())
                return null;

            var searchParams = new SearchParams { PartnerId = NhsRoute.PartnerId, Markets = marketIds, Builders = builderIds };
            var data = _apiService.GetResultsWebApi<CommunityItemViewModel>(searchParams, SearchResultsPageType.CommunityResults);

            var prices = data.ResultCounts.Facets.PrRange.Split('-');

            var lowPrice = prices[0].ToType<int>().ToFormattedThousands();
            var highPrice = prices[1].ToType<int>().ToFormattedThousands();
            var commName = data.ResultCounts.CommCount > 1 ? "Communities" : "Community";

            return new JsonpResult(new { priceLow = lowPrice, priceHigh = highPrice, commsCount = data.ResultCounts.CommCount, communityWord = commName });
        }
    }
}
