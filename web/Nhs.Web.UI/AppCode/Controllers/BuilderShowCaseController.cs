﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Business.Map;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Brightcove;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Filters;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Extensions;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Library.Helpers.Vimeo;
using Nhs.Mvc.Domain.Model.BhiTransaction;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Web.UI.AppCode.Filters;
using Nhs.Web.UI.AppCode.Helper;


namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class BuilderShowCaseController : ApplicationController
    {
        #region Members

        private readonly IPathMapper _pathMapper;
        private readonly IListingService _listingService;
        private readonly ICommunityService _communityService;
        private readonly IBrandService _brandService;
        private readonly IPartnerService _partnerService;
        private readonly IMapService _mapService;
        private readonly IVideoService _videoService;
        private readonly IOwnerStoriesService _ownerStoriesService;

        #endregion

        #region Constructor

        public BuilderShowCaseController(IPathMapper pathMapper, IListingService listingService,
                                         ICommunityService communityService, IBrandService brandService,
                                          IMapService mapService, IPartnerService partnerService, IVideoService videoService, IOwnerStoriesService ownerStoriesService)
            : base(pathMapper)
        {
            _pathMapper = pathMapper;
            _listingService = listingService;
            _communityService = communityService;
            _brandService = brandService;
            _partnerService = partnerService;
            _mapService = mapService;
            _videoService = videoService;
            _ownerStoriesService = ownerStoriesService;
        }

        #endregion

        #region Action

        [ValidateInput(false)]
        public virtual ActionResult GetShowcaseAboutGallery(int brandId, string brandName)
        {
            var viewModel = new BuilderShowCaseViewModel { BrandId = brandId, BrandName = Server.HtmlDecode(brandName) };
            var showcaseImages = _brandService.GetShowCaseImages(brandId).ToList();

            viewModel.AboutGalleryImages = showcaseImages.GetByTypes(ImageTypes.BrandShowCase).Take(10);
            return PartialView(NhsMvc.Default.Views.BuilderShowCase.Gallery, viewModel);
        }

        public virtual ActionResult GetShowcaseAboutAwards(int brandId)
        {
            var viewModel = new BuilderShowCaseViewModel { BrandId = brandId };
            var showcaseImages = _brandService.GetShowCaseImages(brandId).ToList();

            viewModel.AwardImages = showcaseImages.GetByTypes(ImageTypes.BrandAwards).Take(6);
            return PartialView(NhsMvc.Default.Views.BuilderShowCase.BuildershowcaseAwards, viewModel);
        }

        public virtual ActionResult GetShowcaseAboutTestimonials(int brandId)
        {
            var viewModel = new BuilderShowCaseViewModel { BrandId = brandId };
            var brand = _brandService.GetBrandById(viewModel.BrandId, NhsRoute.PartnerId);
            viewModel.Testimonials = brand.BrandTestimonials != null ? brand.BrandTestimonials.Take(6).ToList() : new List<BrandTestimonial>();
            return PartialView(NhsMvc.Default.Views.BuilderShowCase.BuildershowcaseTestimonials, viewModel);
        }

        public virtual ActionResult GetShowcaseAboutLatestNews(int brandId)
        {
            var viewModel = new BuilderShowCaseViewModel { BrandId = brandId };
            var brand = _brandService.GetBrandById(viewModel.BrandId, NhsRoute.PartnerId);

            viewModel.RssUrl = brand.BrandShowCase != null ? brand.BrandShowCase.RssFeed : string.Empty;
            viewModel.TwitterWidget = brand.BrandShowCase != null ? brand.BrandShowCase.TwitterWidget : string.Empty;
            viewModel.FacebookWidget = brand.BrandShowCase != null ? brand.BrandShowCase.FacebookWidget : string.Empty;

            return PartialView(NhsMvc.Default.Views.BuilderShowCase.ShowLatestNews, viewModel);
        }

        [ValidateInput(false)]
        public virtual ActionResult GetSpotlightCarousel(int brandId, string brandName)
        {
            var viewModel = new BuilderShowCaseViewModel { BrandId = brandId, BrandName = Server.HtmlDecode(brandName) };
            viewModel.SpotlightsCarousel = _brandService.GetBrandSpotlights(NhsRoute.PartnerId, viewModel.BrandId);
            return PartialView(NhsMvc.Default.Views.BuilderShowCase.ShowCarousel, viewModel);
        }

        [ValidateInput(false)]
        public virtual ActionResult GetShowcaseFullGallery(int brandId)
        {
            var viewModel = new BuilderShowCaseViewModel { BrandId = brandId };
            GetImagesAndVideos(viewModel, brandId);
            return PartialView(NhsMvc.Default.Views.BuilderShowCase.GalleryVideosAndImages, viewModel);
        }

        public virtual ActionResult ShowAbout(string brandname, int brandid)
        {
            var brand = _brandService.GetBrandById(brandid, NhsRoute.BrandPartnerId);
            var ajaxRequest = Request.QueryString["requestPartial"]; 
            List<Community> comms;

            if (!IsShowCasePageActive(brand, out comms))
                return RedirectPermanent(Pages.Home, new List<RouteParam>());

            var viewModel = GetModel(brand, comms);

            if (viewModel.Markets == null || viewModel.Markets.Count == 0)
                return RedirectPermanent(Pages.NoResultsFound, new List<RouteParam>());

            viewModel.IsAbout = true;

            if (NhsRoute.ShowMobileSite)
                GetImagesAndVideos(viewModel, brandid);

            if (!Request.IsAjaxRequest() && string.IsNullOrEmpty(ajaxRequest)) return View("Show", viewModel);
            var view = NhsRoute.ShowMobileSite ? NhsMvc.Default.ViewsMobile.BuilderShowCase.ShowAbout : NhsMvc.Default.Views.BuilderShowCase.ShowAbout;
            return PartialView(view, viewModel);
        }
    
        public virtual ActionResult ShowOwnerStories(string brandName, int brandId)
        {
            var brand = _brandService.GetBrandById(brandId, NhsRoute.PartnerId);
            var ajaxRequest = Request.QueryString["requestpartial"];
            List<Community> comms;

            if (!IsShowCasePageActive(brand, out comms))
                return RedirectPermanent(Pages.Home, new List<RouteParam>());

            var viewModel = GetModel(brand, comms, true);
            viewModel.OwnerStories = _ownerStoriesService.GetOwnerStoriesList(brand.BrandId, false, true);
            viewModel.IsOwnerStories = true;
            GetOwnerStoriesMediaObjects(viewModel);

            var view = NhsMvc.Default.Views.BuilderShowCase.ShowOwnerStories;
            if (Request.IsAjaxRequest() || !string.IsNullOrEmpty(ajaxRequest))
                return PartialView(view, viewModel);

            // ReSharper disable once Mvc.ViewNotResolved
            return View("Show", viewModel);
        }

        public virtual ActionResult ShowLocations(string brandname, int brandid)
        {
            var brand = _brandService.GetBrandById(brandid, NhsRoute.BrandPartnerId);
            var ajaxRequest = Request.QueryString["requestPartial"];
            List<Community> comms;

            if (!IsShowCasePageActive(brand, out comms))
                return RedirectPermanent(Pages.Home, new List<RouteParam>());

            var viewModel = GetModel(brand, comms);
            viewModel = GetMapForModel(viewModel);
            viewModel.IsLocations = true;

            GetMetroAreaList(viewModel);

            if (viewModel.Markets == null || viewModel.Markets.Count == 0)
                return RedirectPermanent(Pages.NoResultsFound, new List<RouteParam>());

            if (!Request.IsAjaxRequest() && string.IsNullOrEmpty(ajaxRequest)) return View("Show", viewModel);
            var view = NhsRoute.ShowMobileSite ? NhsMvc.Default.ViewsMobile.BuilderShowCase.ShowLocations : NhsMvc.Default.Views.BuilderShowCase.ShowLocations;
            return PartialView(view, viewModel);
        }

        public virtual ActionResult ShowGallery(string brandname, int brandid)
        {
            var brand = _brandService.GetBrandById(brandid, NhsRoute.BrandPartnerId);
            var ajaxRequest = Request.QueryString["requestPartial"];
            List<Community> comms;

            if (!IsShowCasePageActive(brand, out comms))
                return RedirectPermanent(Pages.Home, new List<RouteParam>());

            var viewModel = GetModel(brand, comms);

            if (viewModel.Markets == null || viewModel.Markets.Count == 0)
                return RedirectPermanent(Pages.NoResultsFound, new List<RouteParam>());

            viewModel.IsGallery = true;

            if (Request.IsAjaxRequest() || !string.IsNullOrEmpty(ajaxRequest))
                return PartialView(NhsMvc.Default.Views.BuilderShowCase.ShowGallery, viewModel);
            
            // ReSharper disable once Mvc.ViewNotResolved
            return View("Show", viewModel);
        }

        public virtual ActionResult ShowVideo(Video video)
        {
            // ReSharper disable once Mvc.ViewNotResolved
            return View("MediaPlayerVideo", new BuilderShowCaseViewModel { Video = video });
        }

        public virtual ActionResult ShowGalleryModal(int index, int brandid)
        {
            var model = new BuilderShowCaseViewModel
            {
                AboutGalleryImages = _brandService.GetShowCaseImages(brandid)
                                                  .GetByTypes(ImageTypes.BrandShowCase)
                                                  .OrderBy(p => p.ImageSequence).ToList(),
                ImagesGalleryIndex = index
            };
            // ReSharper disable once Mvc.ViewNotResolved
            return View("ShowGalleryModal", model);
        }

        [HttpPost]
        public virtual JsonResult AskQuestion(BuilderShowCaseViewModel model)
        {
            var hasError = false;
            var url = Request.UrlReferrer != null ? Request.UrlReferrer.AbsoluteUri : Request.RawUrl;
            IDictionary<string, string> errorsDic = new Dictionary<string, string>();
            string title;

            ModelState.Clear();
            ValidateLeadInfo(model);

            var tags = new List<ContentTag>
            {
                new ContentTag(ContentTagKey.BrandName, model.BrandName),
                new ContentTag(ContentTagKey.BrandID, model.BrandId.ToString()),
                new ContentTag(ContentTagKey.Question, model.Comments),
                new ContentTag(ContentTagKey.FirstName, model.Name),
                new ContentTag(ContentTagKey.Email, model.MailAddress),
                new ContentTag(ContentTagKey.Url, url )
            };

            var bodyMail = new HtmlHelper(new ViewContext(), new ViewPage()).EmailStaticContentWithTitle(tags, "BuilderShowcase.html", out title).ToHtmlString();

            if (ModelState.IsValid)
            {
                MailHelper.Send(model.BrandEmailAddress, Configuration.FromEmail, title, bodyMail);
                model.SendLedForm = true;
            }
            else
            {
                hasError = true;
                var errors = ModelState.Values.SelectMany(v => v.Errors.Select(s => new KeyValuePair<string, string>("Model",
                    string.IsNullOrWhiteSpace(s.ErrorMessage) ? s.Exception.Message : s.ErrorMessage)));
                foreach (var error in errors)
                {
                    errorsDic.Add(error);
                }
            }

            return Json(new
            {
                result = ModelState.IsValid,
                HasErrors = hasError,
                Errors = errorsDic.Select(s => s.Value)
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region protected Methods
        
        protected void ValidateLeadInfo(BuilderShowCaseViewModel model)
        {
            var regex = new Regex(StringHelper.NameLastnameRegex);

            if (string.IsNullOrEmpty(model.Name) || !regex.IsMatch(model.Name))
                ModelState.AddModelError("Name", @"Please enter first and last Name. Don't enter special characters.");

            regex = new Regex(StringHelper.EmailRegexfull);

            if (string.IsNullOrEmpty(model.MailAddress) ||
                !regex.IsMatch(model.MailAddress))
                ModelState.AddModelError("Email", @"Email appears invalid, please re-enter.");
        }

        protected BuilderShowCaseViewModel GetModel(Brand brand, List<Community> comms, bool loadTestimonials = false)
        {
            var marketList = _brandService.GetMarketandStateByBrand(brand.BrandId, NhsRoute.PartnerId).ToList();
            var marketIds = marketList.Select(b => b.MarketId).ToList();

            var model = new BuilderShowCaseViewModel
            {
                BrandName = brand.BrandName,
                BrandId = brand.BrandId,
                BrandLogo = (NhsRoute.IsMobileDevice ? string.Concat(Configuration.ResourceDomain, brand.BrandThumbnail()) : brand.LogoMedium),
                BrandUrl = brand.BrandShowCase != null ? brand.BrandShowCase.BrandUrl : string.Empty,
                BrandEmailAddress = brand.BrandShowCase != null ? brand.BrandShowCase.ContactEmail : string.Empty,
                OverviewText = brand.BrandShowCase != null ? brand.BrandShowCase.Overview : string.Empty,
                PromotionalText = brand.BrandShowCase != null ? brand.BrandShowCase.Description : string.Empty,
                Markets = _mapService.GetMarketsInfo(marketIds, NhsRoute.PartnerId),
                MetroAreaList = new BuilderShowCaseMetroAreaList(),
                Testimonials = loadTestimonials ? brand.BrandTestimonials : new List<BrandTestimonial>()
            };


            model.IsSingleMarketBrand = model.Markets.Count == 1;

            foreach (var builder in brand.Builders)
                model.Globals.AdController.AddBuilderParameter(builder.BuilderId);

            model.Communities = comms;
            model = GetMapForModel(model);
            OverrideDefaultMeta(model, brand, marketList);
            return model;
        }

        private static void GetOwnerStoriesMediaObjects(BuilderShowCaseViewModel model)
        {
            var mediaObjects = new List<OwnerStoryMediaObject>();

            if (model.Testimonials.Any())
            {
                mediaObjects.AddRange(model.Testimonials.Randomize().Select(testimonal => new OwnerStoryMediaObject
                {
                    Author = testimonal.Citation,
                    Description = testimonal.Description,
                    IsOwnerStory = false,
                    HasImage = false,
                    HasVideo = false,
                }));
            }
            if (model.OwnerStories.Any())
            {
                mediaObjects.AddRange(model.OwnerStories.Randomize().Select(ownerStory => new OwnerStoryMediaObject
                {
                    Author = string.Format("{0} {1}", ownerStory.FirstName, ownerStory.LastName),
                    Description = ownerStory.StoryDescription,
                    City = ownerStory.City,
                    MediaObjectPath = ownerStory.FilePath,
                    IsOwnerStory = true,
                    HasImage = ownerStory.HasImage,
                    HasVideo = ownerStory.HasVideo,
                    VideoUrl = ownerStory.ProcessedVideoUrl,
                    VideoThumbnailUrl = ownerStory.ProcessedVideoThumbnailUrl,
                    VideoTitle = ownerStory.ProcessedVideoTitle,
                    OwnerStoryId = ownerStory.OwnerStoryId.ToType<string>(),
                    VideoId = ownerStory.ProcessedVideoId.ToType<string>()
                }));
                var storiesWithVideos = mediaObjects.Where(o => o.HasVideo).ToList();
                foreach (var story in storiesWithVideos)
                {
                    var bcVideo = BCAPI.FindVideosByTags(string.Empty, String.Format("htv-{0}-{1}-{2}", story.OwnerStoryId, story.VideoId, Configuration.BrightcoveEnvSuffix)).First();
                    story.OnlineVideoId = bcVideo.id.ToString(CultureInfo.InvariantCulture);
                }
            }

            var totalRows = mediaObjects.Count() / 3 + (mediaObjects.Count() % 3 == 0 ? 0 : 1);
            var results = mediaObjects.Where(x => x.IsOwnerStory && (x.HasImage || x.HasVideo)).Take(totalRows).ToList();
            results.ForEach(x => mediaObjects.Remove(x));
            if (results.Count() < totalRows)
            {
                results.AddRange(mediaObjects.Where(x => x.IsOwnerStory).Take(totalRows - results.Count()).ToList());
            }
            results.ForEach(x => mediaObjects.Remove(x));

            mediaObjects.Randomize();
            results.AddRange(mediaObjects);
            results = results.Take(25).ToList();
            totalRows = results.Count() / 3 + (results.Count() % 3 == 0 ? 0 : 1);

            model.OwnerStoryMediaObjects = results;
            model.OwnerStoryMediaObjectsTotalRows = totalRows;
        }

        protected void OverrideDefaultMeta(BuilderShowCaseViewModel model, Brand brand, IEnumerable<Brand> marketList)
        {
            var metaRegistrar = new MetaRegistrar(_pathMapper);
            var pageName = "";

            var markets = marketList as IList<Brand> ?? marketList.ToList();
            var numberState = markets.GroupBy(p => p.State).Count();
            var multipleMarkets = markets.GroupBy(p => p.MarketId).Count() > 1;

            if (!multipleMarkets && numberState == 1)
            {
                var states = markets.FirstOrDefault() ?? new Brand();
                pageName = "buildershowcasesinglemarket";
                metaRegistrar.AddMetaParameter(MetaRegistarParam.StateName, states.State);
                metaRegistrar.AddMetaParameter(MetaRegistarParam.MarketName, states.MarketName);
            }
            else if (numberState == 1 && multipleMarkets)
            {
                var states = markets.FirstOrDefault() ?? new Brand();
                pageName = "buildershowcasemultiplemarkets";
                metaRegistrar.AddMetaParameter(MetaRegistarParam.StateName, states.State);
                metaRegistrar.AddMetaParameter(MetaRegistarParam.ParameterBuilderShowCaseState1, states.State);
            }
            else if (numberState == 2)
            {
                var states = markets.GroupBy(p => p.State).OrderBy(p => p.Key).Select(p => p.Key).ToList();
                pageName = "buildershowcaseupto2states";
                metaRegistrar.AddMetaParameter(MetaRegistarParam.ParameterBuilderShowCaseState1, states[0]);
                metaRegistrar.AddMetaParameter(MetaRegistarParam.ParameterBuilderShowCaseState2, states[1]);
            }
            else if (numberState > 2)
                pageName = "buildershowcasemorethan2states";

            var partner = _partnerService.GetPartner(NhsRoute.PartnerId);
            var commCount = model.Communities.Count().ToType<string>();
            metaRegistrar.AddMetaParameter(MetaRegistarParam.CommunitiesOnNhs, commCount);
            metaRegistrar.AddMetaParameter(MetaRegistarParam.Domain, partner.PartnerName);
            metaRegistrar.AddMetaParameter(MetaRegistarParam.BrandName, brand.BrandName);
            var metas = metaRegistrar.GetMetaTagsList(pageName);
            OverrideDefaultMeta(model, metas);
        }
        
        #endregion

        #region Private Methods

        private static bool FirstLeterIsVowel(string word)
        {
            var letter = word.ToLowerCase()[0];
            return letter == 'e' ||
            letter == 'a' ||
            letter == 'o' ||
            letter == 'i' ||
            letter == 'u';
        }

        private void GetImagesAndVideos(BuilderShowCaseViewModel viewModel, int brandId)
        {
            // The videos are stored in the data base using the brand_showcase_video table there each video is related to a branch
            var videos = _videoService.GetVideosForBrand(brandId).ToList();

            // I have to get the youtube videos
            viewModel.ShowcaseVideos = videos.Where(video => video.IsFromYoutube || video.IsFromVimeo).ToList();
            viewModel.ShowcaseVideos.ForEach(v =>
            {
                v.ThumbnailUrl = (v.IsFromVimeo ? VimeoAPI.GetVideobyId(v.VideoOnlineId).thumbnail_medium
                                                : string.Format("http://img.youtube.com/vi/{0}/hqdefault.jpg", v.VideoOnlineId));
            });

            // The videos from Brightcove have to be taken from there, using a special tag
            // I have to add a tag for each video
            var tags = videos.Where(v => !v.IsFromYoutube && !v.IsFromVimeo).ToList()
                             .Select(video => String.Format("bid-{0}-{1}-{2}", video.BuilderId, video.VideoId, Configuration.BrightcoveEnvSuffix)).ToList();

            //htv-<testimonial_id>-<video_id>-<dev or prod or test or stage>

            
            if (tags.Count > 0)
            {
                var bcVideos = BCAPI.FindVideosByTags(string.Empty, String.Join(",", tags));

                foreach (var bcVideo in bcVideos)
                    viewModel.ShowcaseVideos.Add(new Video
                    {
                        VideoOnlineId = bcVideo.id.ToString(CultureInfo.InvariantCulture),
                        VideoURL = bcVideo.videoStillURL,
                        ThumbnailUrl = bcVideo.thumbnailURL,
                        Title = bcVideo.name,
                    });
            }

            viewModel.AwardImages = _brandService.GetShowCaseImages(brandId).GetByTypes(ImageTypes.BrandShowCase).OrderBy(p => p.ImageSequence);
        }

        private static void GetMetroAreaList(BuilderShowCaseViewModel viewModel)
        {
            //Get all communities for brand
            var communities = viewModel.Communities.ToList();

            //Get distinct market list
            var markets = communities.DistinctBy(c => c.Market.MarketId).Select(c => c.Market).OrderBy(m => m.MarketName).ToList();

            var metroAreas = markets.Select(mkt => new BuilderShowCaseMetroArea
            {
                Market = mkt,
                Communities = (from c in communities where c.MarketId == mkt.MarketId select c),
                BrandId = viewModel.BrandId

            }).ToList();

            var metroCount = metroAreas.Count();
            viewModel.MetroAreaList.MetroAreas = metroAreas;
            viewModel.MetroAreaList.HasOne = metroCount == 1;
            viewModel.MetroAreaList.HeaderText = viewModel.MetroAreaList.HasOne ? viewModel.BrandName + " " + LanguageHelper.CommunitiesIn + " " + markets.First().MarketName
                                                                                : viewModel.BrandName + " " + LanguageHelper.BuildsIn + " " + metroCount + " " + LanguageHelper.MetroAreas + ":";
        }

        private static BuilderShowCaseViewModel GetMapForModel(BuilderShowCaseViewModel model)
        {
            if (model.Markets.Count > 0)
                if (model.IsSingleMarketBrand)
                {
                    model.Map = new MapData
                    {
                        CenterLat = model.Markets[0].LatLong.Latitude,
                        CenterLng = model.Markets[0].LatLong.Longitude,
                        ZoomLevel = 8,
                    };
                    model.MarketId = model.Markets[0].MarketId;
                }
                else if (model.Markets.GroupBy(m => m.StateAbbr).Count() == 1)
                    model.Map = new MapData
                    {
                        CenterLat = model.Markets[0].StateLatLong.Latitude,
                        CenterLng = model.Markets[0].StateLatLong.Longitude,
                        ZoomLevel = 7
                    };
                else
                    model.Map = new MapData
                    {
                        CenterLat = 38, // Coordinates for a state in the center of US
                        CenterLng = -98, // so map is centered when empty
                        ZoomLevel = 4
                    };
            else
                model.Map = new MapData
                {
                    CenterLat = 38, // Coordinates for a state in the center of US
                    CenterLng = -98, // so map is centered when empty
                    ZoomLevel = 4
                };

            return model;
        }

        private bool IsShowCasePageActive(Brand brand, out List<Community> comms)
        {
            var showCase = brand.BrandShowCase;
            var isShowCaseActive = showCase != null && showCase.StatusId == 1;
            comms = _communityService.GetCommunitiesByBrandId(NhsRoute.PartnerId, brand.BrandId, brand).ToList();

            return isShowCaseActive && comms.Any();
        }

        #endregion
    }
}
