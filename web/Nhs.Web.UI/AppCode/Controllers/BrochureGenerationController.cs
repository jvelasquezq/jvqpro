﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net.Mime;
using System.Text;
using System.Web.Mvc;
using EO.Pdf;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Route;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Domain.Model.Web.Partials;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Utility.Pdf;
using Nhs.Web.UI.AppCode.Extensions;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class BrochureGenerationController : ApplicationController
    {
        private readonly ILeadService _leadService;
         #region Constructor
        public BrochureGenerationController(ILeadService leadService)
        {
            _leadService = leadService;
        }
        #endregion

        public virtual FileResult ResultsPdf()
        {
             var log = new StringBuilder();
            Log(ref log, "--------------------------------------------------Start--------------------------------------------------", false);
            Log(ref log, "URL: " + Request.Url);
            var url = "http://mvc.newhomesource.com/pdfcrochure/communityresultshtml";
            //HtmlToPdf.Options.HeaderHtmlFormat = args.GetString("txtHeaderFormat");
            HtmlToPdf.Options.FooterHtmlFormat = "<div style=\"text-align:center;background-color: red;\">Page {page_number}/{total_pages}</div>";
            var ms = PdfGenerator.GetPdfForDownload(url, ref log);
            Log(ref log, "--------------------------------------------------End--------------------------------------------------", false);
            var impressionLogger = new ImpressionLogger();
            impressionLogger.LogPDFGeneration(log);
            return new FileStreamResult(ms, "application/pdf") { FileDownloadName = "download.pdf" };
        }

        public virtual FileResult GeneratePdfBrochure()
        {
            var log = new StringBuilder();
            return GetPdfBrochureObject(log, new LeadEmailProperty
            {
                CommunityId = RouteParams.CommunityId.Value<Int32>() ,
                BuilderId = RouteParams.Builder.Value<Int32>(),
                SpecId = RouteParams.SpecId.Value<Int32>(),
                PlanId = RouteParams.PlanId.Value<Int32>()
            });
        }

        public virtual ActionResult Show()
        {
            var impressionLogger = new ImpressionLogger();
            var log = new StringBuilder();
            Log(ref log, "--------------------------------------------------Start--------------------------------------------------", false);
            Log(ref log, "URL: " + Request.Url);

            var requestItemId = RouteParams.RequestItemId.Value();
            if (!string.IsNullOrWhiteSpace(requestItemId))
                requestItemId = CryptoHelper.Decrypt(RouteParams.RequestItemId.Value());

            Log(ref log, "RequestItemId: " + requestItemId);

            var info = _leadService.GetGetRequestItemDeliveryInfo(requestItemId.ToType<int>());
            if (info.BrochureExpiration > DateTime.Now || info.BrochureExpiration == DateTime.MinValue)
            {
                var brochureName = info.RequestItemDeliveryId + ".pdf";
                _leadService.UpdateBrochureName(info.RequestItemDeliveryId.ToType<int>(), brochureName);
                var pdf = GetPdfBrochureObject(log, info);
                Log(ref log, "--------------------------------------------------End--------------------------------------------------", false);
                impressionLogger.LogPDFGeneration(log);
                return pdf;
            }

            Log(ref log, "--------------------------------------------------End--------------------------------------------------", false);
            impressionLogger.LogPDFGeneration(log);
            var model = new BrochureGenerationViewModel();

            if (info.CommunityId != 0)
                model.ExpireUrl = SetCommunityLeadLink(info.CommunityId, info.BuilderId);
            else
            {
                model.ExpireUrl = info.SpecId != 0 ? SetHomeLeadLink(0, info.SpecId) : SetHomeLeadLink(info.PlanId, 0);
            }
            
// ReSharper disable once Mvc.ViewNotResolved
            return View(model);
        }

        private FileStreamResult GetPdfBrochureObject(StringBuilder log, LeadEmailProperty info)
        {
            var ms = new MemoryStream();

            try
            {
                var generateNhsBrochure = GetCustomBrochure(info, ref ms, ref log);
                //Generate system brochure
                if (generateNhsBrochure)
                {
                    var planId = info.PlanId;
                    var specId = info.SpecId;
                    var communityId = info.CommunityId;
                    var firstName = RouteParams.FirstName.Value<string>();
                    var email = RouteParams.Email.Value<string>();

                    if (info != null)
                    {
                        planId = info.PlanId;
                        specId = info.SpecId;
                        communityId = info.CommunityId;
                    }

                    //TODO: Need to check why the RouteParams is not returning the SpecId, PlanId and CommId from the URL
                    if (planId == 0 && specId == 0 && communityId == 0)
                    {
                        communityId = NhsUrl.GetCommunityID;
                        planId = NhsUrl.GetPlanID;
                        specId = NhsUrl.GetSpecID;
                    }

                    var pdfUrl = new List<RouteParam>();

                    if (communityId > 0)
                        pdfUrl.Add(new RouteParam(RouteParams.CommunityId, communityId.ToType<string>(), RouteParamType.Friendly, true, true));
                    else if (specId > 0)
                        pdfUrl.Add(new RouteParam(RouteParams.SpecId, specId.ToType<string>(), RouteParamType.Friendly, true, true));
                    else if (planId > 0)
                        pdfUrl.Add(new RouteParam(RouteParams.PlanId, planId.ToType<string>(), RouteParamType.Friendly, true, true));

                    pdfUrl.Add(new RouteParam(RouteParams.FirstName, firstName, RouteParamType.Friendly, true, false));
                    pdfUrl.Add(new RouteParam(RouteParams.Email, email, RouteParamType.QueryString, true, true));

                    var brochureurl = Configuration.NewHomeSourceDomain + pdfUrl.ToUrl(Pages.PdfBrochure);

                    ms = PdfGenerator.GetPdfForDownload(brochureurl, ref log);
                }
            }
            catch (Exception ex)
            {
                log.AppendLine(ex.Message);
            }


            //var cd = new ContentDisposition
            //{
            //    FileName = "test.pdf",
            //    Inline = false,
            //    Size = ms.Length,
            //    CreationDate = DateTime.Now
            //};

            //Response.AppendHeader("Content-Disposition", "inline");
            //return File(ms, MediaTypeNames.Application.Pdf);


            Response.AppendHeader("Content-Disposition", "inline");
            return new FileStreamResult(ms, "application/pdf");
        }

        protected string SetHomeLeadLink(int PlanId, int SpecId)
        {
            var link = new NhsUrl(Pages.HomeDetail); ;

            if (SpecId != 0)
                link.Parameters.Add(UrlConst.SpecID, SpecId.ToString(), RouteParamType.Friendly);
            if (PlanId != 0)
                link.Parameters.Add(UrlConst.PlanID, PlanId.ToString(), RouteParamType.Friendly);

            return link.ToString();
        }

        protected string SetCommunityLeadLink(int communityId, int builderId)
        {
            var link = new NhsUrl(Pages.CommunityDetail); ;

            link.AddParameter(UrlConst.BuilderID, builderId.ToString(), RouteParamType.Friendly);
            link.AddParameter(UrlConst.CommunityID, communityId.ToString(), RouteParamType.Friendly);

            return link.ToString();
        }

        private bool GetCustomBrochure(LeadEmailProperty info, ref MemoryStream ms, ref StringBuilder log)
        {
            try
            {
                DataTable commBrochures = null;
                DataTable homeBrochures = null;

                //Look for builder-provided community brochure first
                if (info.BuilderId > 0 && info.CommunityId > 0)
                    commBrochures = _leadService.GetCommunityBrochures(info.CommunityId, info.BuilderId);

                //Look for builder-provided spec brochure
                if (info.SpecId > 0)
                    homeBrochures = _leadService.GetHomeBrochures(0, info.SpecId);

                //Look for builder-provided plan brochure
                if (info.PlanId > 0)
                    homeBrochures = _leadService.GetHomeBrochures(info.PlanId, 0);

                if (info.SpecId > 0 || info.PlanId > 0)
                    homeBrochures = CombineCommAndHomeBrochures(homeBrochures);

                var brochures = commBrochures; //Comm brochures

                if (homeBrochures != null && homeBrochures.Rows.Count > 0) //Home brochures
                    brochures = homeBrochures;

                if (brochures == null || brochures.Rows.Count <= 0)
                {
                    return true;
                }

                return MergePdfs(brochures, ref ms, ref log); //merge pdfs
            }
            catch (Exception ex)
            {
                Log(ref log, "Error");
                Log(ref log, ex.Message, false);
                return true;
            }
        }

        private string GeneratePdf(LeadEmailProperty info, string strBrochureDir, string brochureName, ref StringBuilder log)
        {
            var firstName = RouteParams.FirstName.Value<string>();
            var email = RouteParams.Email.Value<string>();

            var pdfUrl = new List<RouteParam>();
            if (info.BuilderId > 0 && info.CommunityId > 0)
                pdfUrl.Add(new RouteParam(RouteParams.CommunityId, info.CommunityId.ToType<string>(), RouteParamType.Friendly, true, true));
            else if (info.SpecId > 0)
                pdfUrl.Add(new RouteParam(RouteParams.SpecId, info.SpecId.ToType<string>(), RouteParamType.Friendly, true, true));
            else if (info.PlanId > 0)
                pdfUrl.Add(new RouteParam(RouteParams.PlanId, info.PlanId.ToType<string>(), RouteParamType.Friendly, true, true));
            pdfUrl.Add(new RouteParam(RouteParams.FirstName, firstName, RouteParamType.Friendly, true, false));
            pdfUrl.Add(new RouteParam(RouteParams.Email, email, RouteParamType.QueryString, true, true));
             
            var brochureurl = Configuration.NewHomeSourceDomain + pdfUrl.ToUrl(Pages.PdfBrochure);

            try
            {
                brochureName = PdfGenerator.GetPdf(brochureurl, strBrochureDir, brochureName, ref log);
            }
            catch (Exception ex)
            {
                Log(ref log, "Error");
                Log(ref log, ex.Message, false);
            }

            return brochureName;
        }

        private DataTable CombineCommAndHomeBrochures(DataTable homeBrochures)
        {
            if (homeBrochures != null  && homeBrochures.Rows.Count > 0)
            {
                var includeCommBrochureWithHome =
                    Convert.ToBoolean(homeBrochures.Rows[0]["IncludeCommBrochureWithHome"]);

                var communityId = Convert.ToInt32(homeBrochures.Rows[0]["CommunityId"]);
                var builderId = Convert.ToInt32(homeBrochures.Rows[0]["BuilderId"]);
                var commBrochures = _leadService.GetCommunityBrochures(communityId, builderId);

                if (includeCommBrochureWithHome && commBrochures != null && commBrochures.Rows.Count > 0)
                    foreach (DataRow cdr in commBrochures.Rows)
                        homeBrochures.ImportRow(cdr);
            }

            return homeBrochures;
        }

        private bool MergePdfs(DataTable brochures, ref MemoryStream ms, ref StringBuilder log)
        {
            bool generateNhsBrochure;
            Log(ref log, "Getting Custom PDF: #" + brochures.Rows.Count);
            var pdfStagingAreaPath = Configuration.ImageProcessorFTPPath;

            try
            {
                var inputPaths = new List<string>();
                foreach (DataRow dr in brochures.Rows)
                {

                    var pdfLocation = dr["OriginalUrl"].ToString();
                    var pdfFolder = dr["FtpLocation"];

                    if (pdfLocation.StartsWith("http://") || pdfLocation.StartsWith("https://"))
                    {
                        inputPaths.Add(pdfLocation);
                    }
                    else
                    {
                        var pdfPath = string.Format(@"{0}\{1}\{2}", pdfStagingAreaPath, pdfFolder, pdfLocation);
                        if (System.IO.File.Exists(pdfPath))
                            inputPaths.Add(pdfPath);
                    }
                }

                if (inputPaths.Count > 0)
                {
                    generateNhsBrochure = PdfGenerator.MergePdfs(ref ms, inputPaths, ref log);
                }
                else
                {
                    Log(ref log,
                        "Even though DB said it had custom PDFs, I did not find any in the given locations. Will generate default NHS brochure instead.");
                    generateNhsBrochure = true;
                }
            }

            catch (Exception ex)
            {
                Log(ref log, "There was an error merging brochure pdfs.");
                Log(ref log, ex.Message, false);
                generateNhsBrochure = true;
            }
            return generateNhsBrochure;
        }

        private static void Log(ref StringBuilder log, string message, bool appendTime = true)
        {
            if (appendTime)
                log.AppendLine(DateTime.Now + ": " + message);
            else
                log.AppendLine(message);
        }
    }
}