﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;
using Nhs.Library.Constants;
using Nhs.Library.Helpers.Seo;
using Nhs.Mvc.Domain.Model.BasicListings;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using System.Linq;
using Kendo.Mvc.Extensions;
using Nhs.Library.Business;
using Nhs.Library.Business.Seo;
using Nhs.Library.Common;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Web.UI.AppCode.Extensions;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class BasicHomeDetailController : ApplicationController
    {
        private readonly IBasicListingService _basicListingService;
        private readonly ICommunityService _communityService;
        private readonly IPartnerService _partnerService;
        private readonly IBuilderService _builderService;
        private readonly IMarketService _marketService;
        private readonly IAffiliateLinkService _affiliateLinkService;
        private readonly ISeoContentService _seoContentService;

        public BasicHomeDetailController(IBasicListingService basicListingService, ICommunityService communityService,
            IPartnerService partnerService, IBuilderService builderService, IMarketService marketService, IAffiliateLinkService affiliateLinkService,
            ISeoContentService seoContentService)
        {
            _basicListingService = basicListingService;
            _communityService = communityService;
            _partnerService = partnerService;
            _builderService = builderService;
            _marketService = marketService;
            _affiliateLinkService = affiliateLinkService;
            _seoContentService = seoContentService;
        }

        public virtual ActionResult Show(int listingId, string optionals)
        {
            var basicListing = _basicListingService.GetBasicListing(listingId);
            
            // Set this property in order to let the breadcrumb knows where to go
            UserSession.PreviousPage = Pages.HomeDetail;

            if (basicListing == null)
                return RedirectTo404();

            var basicHomeDetailViewModel = new BasicHomeDetailViewModel
            {
                BasicListingId = basicListing.ListingId,
                IsCommunityMultiFamily = true,
                Description = basicListing.Description,
                Address = basicListing.StreetAddress,
                HomeAddress1 = basicListing.StreetAddress,
                Baths = basicListing.Bathrooms,
                Beds = basicListing.Bedrooms,
                Price = basicListing.FormattedListPrice,
                //Case 78296: We are only changing the domain. In another release we will clean up this part and we will use IRSHelper
                ImgUrl = basicListing.FirstListingImage == null ? string.Empty : Configuration.IRSDomain + basicListing.FirstListingImage.LocalPath + ImageSizes.BasicListingHomeMain + "_" + basicListing.FirstListingImage.ImageName,
                Sqft = basicListing.FormattedLivingArea,
                City = basicListing.City,
                CommunityCity = basicListing.City,
                State = basicListing.State,
                PostalCode = basicListing.PostalCode,
                ZipCode = basicListing.PostalCode,
                AgentName = basicListing.AgentName,
                AgentPhoneNumber = basicListing.AgentPhone,
                //Case 78296: We are only changing the domain. In another release we will clean up this part and we will use IRSHelper
                AgentPictureUrl = basicListing.FirstAgentPhoto == null ? string.Empty : Configuration.IRSDomain + basicListing.FirstAgentPhoto.LocalPath + ImageSizes.BasicListingAgentPhoto + "_" + basicListing.FirstAgentPhoto.ImageName,
                BrokerUrl = basicListing.ProviderUrl,
                //Case 78296: We are only changing the domain. In another release we will clean up this part and we will use IRSHelper                
                BrokerImageUrl = basicListing.FirstBrokerPhoto == null ? string.Empty : Configuration.IRSDomain + basicListing.FirstBrokerPhoto.LocalPath + ImageSizes.BasicListingBrokerLogo + "_" + basicListing.FirstBrokerPhoto.ImageName,
                LogActionMethodUrl = GetMethodUrl(basicListing.ListingId),
                EnableRequestAppointment = false,
                SchoolDistrict = basicListing.SchoolDistricts.Select(p => p.DistrictName).Distinct().ToList(),
                HideDrivingDirections = true,
                MlsNumber = basicListing.MlsNumber,
                BrokerName = basicListing.BrokerName,
                YearBuilt = basicListing.YearBuilt,
                MarketId = basicListing.MarketId,
                FeedProviderId = basicListing.FeedProviderId ?? 0,
                BasicListingNumber = basicListing.ListingNumber,
                ListHubProviderId = Configuration.ListHubProviderId,
                ListHubTestLogging = Configuration.ListHubTestLogging,
                WebBugUrl = basicListing.WebBugUrl
            };

       
            
            basicHomeDetailViewModel.AffiliateLinksData = new AffiliateLinksData();
            //BEGIN: Affiliate Links Logic is only for NHS and MOVE            
            if (!NhsRoute.IsBrandPartnerNhsPro)
            {
                var links = _affiliateLinkService.GetAffiliateLinks(Pages.BasicDetail, NhsRoute.PartnerId);

                var freeCreditScore = links.LinksList.Find(x => x.Keyword.IsCaseInsensitiveEqual(AffiliateLinksTypes.FreeCreditScore));
                if (freeCreditScore != null)
                    basicHomeDetailViewModel.AffiliateLinksData.FreeCreditScoreFormArea = freeCreditScore.ToLink();

                var mortgageRates = links.LinksList.Find(x => x.Keyword.IsCaseInsensitiveEqual(AffiliateLinksTypes.MortgageRates));
                if (mortgageRates != null)
                    basicHomeDetailViewModel.AffiliateLinksData.MortageRatesFormArea = mortgageRates.ToLink();
            }
            //END: Affiliate Links Logic is only for NHS and MOVE

            BindAds(basicHomeDetailViewModel);
            //End
            AssemblePropertyMapViewModel(basicHomeDetailViewModel, basicListing);

            var contentTags = new List<ContentTag>
            {
                new ContentTag {TagKey = ContentTagKey.MarketName, TagValue = basicHomeDetailViewModel.MarketName},
                new ContentTag {TagKey = ContentTagKey.MarketId, TagValue = basicHomeDetailViewModel.MarketId.ToString(CultureInfo.InvariantCulture)},
                new ContentTag {TagKey = ContentTagKey.MarketState, TagValue = basicHomeDetailViewModel.StateAbbr},
                new ContentTag {TagKey = ContentTagKey.MarketStateName, TagValue = basicHomeDetailViewModel.State},
                new ContentTag {TagKey = ContentTagKey.StateName, TagValue = basicHomeDetailViewModel.State},
                new ContentTag {TagKey = ContentTagKey.StateID, TagValue = basicHomeDetailViewModel.State},
                new ContentTag {TagKey = ContentTagKey.CityName, TagValue = basicHomeDetailViewModel.City},
                new ContentTag {TagKey = ContentTagKey.Zip, TagValue = basicHomeDetailViewModel.PostalCode},
                new ContentTag {TagKey = ContentTagKey.BasicHomeAddress, TagValue = basicHomeDetailViewModel.Address}
            };
            
            basicHomeDetailViewModel.SeoContentTags = contentTags;


            var seoContent = _seoContentService.GetSeoContent<SeoBaseContent>(new SeoContentOptions
                (
                @"/BasicHomeDetail/",
                "BasicHomeDetailSeo.xml",
                replaceTags: contentTags
                ));
            basicHomeDetailViewModel.H1 = seoContent.H1;

            OverwriteMetaFromContent(basicHomeDetailViewModel, seoContent);

            basicHomeDetailViewModel.PriceLow = basicListing.ListPrice.ToType<string>();
            basicHomeDetailViewModel.PriceHigh = "0";

            // ReSharper disable once Mvc.ViewNotResolved
            return View(basicHomeDetailViewModel);
        }

        private string GetMethodUrl(int listingId)
        {
            string partnerSite = string.IsNullOrEmpty(NhsRoute.PartnerSiteUrl)
                                     ? string.Empty
                                     : string.Concat("/", NhsRoute.PartnerSiteUrl);
            return string.Format("{0}/{1}/{2}/", partnerSite, Pages.BasicDetail, listingId);
        }

        /// <summary>
        /// this method fills out the map data in the Model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="basicListing"></param>
        private void AssemblePropertyMapViewModel(BasicHomeDetailViewModel model, BasicListing basicListing)
        {
            model.BuilderId = basicListing.ListingId;
            model.Latitude = basicListing.Latitude.ToType<double>();
            model.Longitude = basicListing.Longitude.ToType<double>();
            
            model.NearByComms = _communityService.GetNearbyCommunities(NhsRoute.BrandPartnerId, 0, basicListing.ListingId, 5, false, true).ToList();

            if (model.NearByComms.Count == 0)
            {
                var partnerMaskIndex = _partnerService.GetPartnerMaskIndex(NhsRoute.PartnerId);
                var commIds =
                    _communityService.GetCommunitiesByMarketId(model.MarketId)
                        .Where(
                            c =>
                                !c.ListingTypeFlag.Equals("B", StringComparison.InvariantCultureIgnoreCase) &&
                                c.PartnerMask.Substring(partnerMaskIndex - 1, 1) != "0" &&
                                c.PartnerMask.Substring(partnerMaskIndex - 1, 1) != "F" &&
                                c.Status.Equals("Active", StringComparison.CurrentCultureIgnoreCase))
                        .ToList();

                model.ShowCta = commIds.Count > 0;
                model.ShowEbook = !model.ShowCta;
            }
            else
                model.ShowCta = model.ShowEbook = false;

            var market = _marketService.GetMarket(model.MarketId);

            model.MarketName = market.MarketName;
            model.StateAbbr = market.StateAbbr;

            model.UnderneathMapAddress = (!string.IsNullOrEmpty(basicListing.StreetAddress) ? String.Format("{0} {1}, {2} {3}", basicListing.StreetAddress, basicListing.City, basicListing.State, basicListing.PostalCode)
                                        : String.Format("{0}, {1} {2}", basicListing.City, basicListing.State, basicListing.PostalCode));
        }
     
        private void BindAds(BasicHomeDetailViewModel model)
        {
            UserSession.AdBuilderIds.Clear();
            model.Globals.AdController.AddMarketParameter(model.MarketId);
            model.Globals.AdController.AddCommunityParameter(model.CommunityId);
            model.Globals.AdController.AddPriceParameter(model.Price.ToType<int>(), model.Price.ToType<int>());
            model.Globals.AdController.AddStateParameter(model.StateAbbr);
            model.Globals.AdController.AddBuilderParameter(model.BuilderId);
            UserSession.AdBuilderIds.Add(model.BuilderId);
            model.Globals.AdController.AddCityParameter(model.City);
            model.Globals.AdController.AddZipParameter(model.PostalCode);

            var builderIds = _builderService.GetMarketBuilders(NhsRoute.PartnerId, model.MarketId);
            foreach (var builder in builderIds)
            {
                model.Globals.AdController.AddBuilderParameter(builder.BuilderId);
                UserSession.AdBuilderIds.Add(builder.BuilderId);
            }
        }
    }
}
