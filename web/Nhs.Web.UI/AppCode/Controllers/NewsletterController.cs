﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Route;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Filters;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class NewsletterController : ApplicationController
    {
        private readonly INewsletterService _newsletterService;
        public NewsletterController(INewsletterService newsletterService)
        {
            _newsletterService = newsletterService;
        }

        public virtual ActionResult SignUp()
        {
            var model = new NewsletterViewModel()
                {
                    MarketList = _newsletterService.GetMarkets(NhsRoute.PartnerId),
                    Email = RouteParams.Email.Value()
                };

            var metas = new List<MetaTag>()
                {
                    new MetaTag(){  Name = MetaName.Title, Content = "Sign Up for weekly market updates"}                    
                };

            base.OverrideDefaultMeta(model, metas);
            base.SetupAdParameters(model);

            // ReSharper disable once Mvc.ViewNotResolved
            return View(model);
        }

        [HttpPost()]
        public virtual ActionResult SignUp(NewsletterViewModel model)
        {
            var hasError = false;
            Market market = null;
            if (ModelState.IsValid)
            {
                if (string.IsNullOrWhiteSpace(model.Email) || StringHelper.IsValidEmail(model.Email) == false)
                {
                    hasError = true;
                    ModelState.AddModelError("Email", @"Please provide a valid email");
                }

                market = _newsletterService.GetMarkets(NhsRoute.PartnerId).FirstOrDefault(m => m.MarketId == model.MarketId);

                if (market == null)
                {
                    hasError = true;
                    ModelState.AddModelError("MarketId", @"Invalid Market selected");
                }
            }
            else
            {
                hasError = true;
            }

            if (hasError)
            {
                model.MarketList = _newsletterService.GetMarkets(NhsRoute.PartnerId);
                if (market != null) model.MarketName = market.MarketName;
                return model.IsSmallSignUpRequest
                    ? PartialView(NhsMvc.Default.Views.Newsletter.PartialSmallSignUpInfo, model)
                    : PartialView(NhsMvc.Default.Views.Newsletter.PartialSignUp, model);
            }

            model.MarketName = market.MarketName;

            //send the info to Responsys V5
            _newsletterService.SendRequest(model.Email, model.MarketId, model.MarketName, NhsRoute.PartnerId, model.PartnerName, model.PartnerSiteUrl, NhsRoute.BrandPartnerId);

            return model.IsSmallSignUpRequest
                ? PartialView(NhsMvc.Default.Views.Newsletter.PartialSmallSignUpThanks, model)
                : PartialView(NhsMvc.Default.Views.Newsletter.PartialSignupThanks, model);
        }

        public virtual JsonResult SignUpJson(NewsletterViewModel model)
        {
            var hasError = false;
            var errorMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    if (string.IsNullOrWhiteSpace(model.Email) || StringHelper.IsValidEmail(model.Email) == false)
                    {
                        hasError = true;
                        errorMessage = "Please provide a valid email";
                    }
                }
                else
                {
                    hasError = true;
                }

                if (hasError == false)
                {
                    _newsletterService.SendRequest(model.Email, model.MarketId, model.MarketName, NhsRoute.PartnerId,
                        model.PartnerName, (model.PartnerSiteUrl ?? string.Empty), NhsRoute.BrandPartnerId);
                    UserSession.UserProfile.Email = model.Email;
                }
            }
            catch (System.Exception)
            {
                hasError = true;
                errorMessage = "There is an Error in your request, please try again.";
            }

            return Json(new { HasError = hasError, Message = errorMessage }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost()]
        public virtual ActionResult SignUpTvNewsletter(NewsletterViewModel model)
        {
            var hasError = false;
            //Market market = null;
            if (ModelState.IsValid)
            {
                if (string.IsNullOrWhiteSpace(model.Email) || StringHelper.IsValidEmail(model.Email) == false)
                {
                    hasError = true;
                    ModelState.AddModelError("Email", @"Please provide a valid email");
                }
            }
            else
            {
                hasError = true;
            }

            if (hasError)
            {
                return PartialView(model.IsSmallTvSignUpRequest ? NhsMvc.Default.Views.Tv.PartialViews.TvNewsletterSmallSignUp : NhsMvc.Default.Views.Tv.PartialViews.TvNewsletterSignUp, model);
            }
            //Adds the email address to the user session so it can be used on the contest modal
            UserSession.UserProfile.Email = model.Email;

            //send the info to Responsys V5
            _newsletterService.SendRequest(model.Email, model.MarketId, model.MarketName, NhsRoute.PartnerId, model.PartnerName, model.PartnerSiteUrl, NhsRoute.BrandPartnerId);

            //return model.IsSmallTvSignUpRequest
            //    ? PartialView(NhsMvc.Default.Views.Newsletter.PartialSmallSignUpThanks, model)
            //    : PartialView(NhsMvc.Default.Views.Newsletter.PartialSignupThanks, model);
            return PartialView(NhsMvc.Default.Views.Tv.PartialViews.TvNewsletterSignUpThanks, model);
        }
    }
}