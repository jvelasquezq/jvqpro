﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Bdx.Web.Api.Objects;
using Bdx.Web.Api.Objects.Constants;
using Nhs.Library.Business;
using Nhs.Library.Business.Seo;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Constants.Route;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers.Ad;
using Nhs.Library.Helpers.Maps;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Helpers.WebApiServices;
using Nhs.Library.Proxy;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Filters;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResultMobile;

namespace Nhs.Web.UI.AppCode.Controllers
{
    public partial class CommunityResultsController
    {
        #region Actions
        [NoCache]
        public virtual ActionResult ShowMobile()
        {
            Market market = null;
            Synthetic syntheticGeoName = null;
            int marketId = 0;

            if (RouteParams.Market.Value<int>() == 0)
            {
                if (!string.IsNullOrEmpty(RouteParams.Area.Value()) || !string.IsNullOrEmpty(RouteParams.MarketName.Value()))
                {
                    var state = RouteParams.StateName.Value();
                    var marketName = RouteParams.MarketName.Value();
                    market = _marketService.GetMarket(NhsRoute.PartnerId, state.Replace("-", " "), marketName.Replace("-", " "), false);
                    marketId = market != null ? market.MarketId : 0;
                }
                else
                {
                    if (!string.IsNullOrEmpty(RouteParams.SyntheticName.Value<string>()))
                    {
                        var name = RouteParams.SyntheticName.Value<string>().Replace("-", " ");
                        syntheticGeoName = SyntheticGeoReader.GetSynthetics().Where(s => s.Name.ToLower() == name.ToLower()).FirstOrDefault();

                        if (syntheticGeoName != null)
                        {
                            market = _marketService.GetMarket(syntheticGeoName.MarketId);
                            marketId = market != null ? market.MarketId : 0;
                        }
                    }
                }
            }
            else
            {
                marketId = RouteParams.Market.Value<int>();
                market = GetMarket(marketId);
            }



            //Case 77642: IF removed below is simplified with the new DFU Service. 
            //if (RouteParams.Market.Value<int>() != 0 && Configuration.MarketsListforDFU.Contains(RouteParams.Market.Value<int>()) && NhsRoute.PartnerId == NhsRoute.BrandPartnerId) // not a private label site   
            var resultPage = RedirectionHelper.RedirecToNewFormat(RouteParams.Market.Value<int>(), _marketDfuService);
            if (!string.IsNullOrEmpty(resultPage))
            {
                return RedirectPermanent(resultPage, NhsRoute.CurrentRoute.Params.ToResultsParams());
            }


            UserSession.PreviewsGroupingBarValue = string.Empty;
            var searchParams = GetDefaultSearchParams(marketId);
            var pageNumber = 1;
            searchParams.PageSize = 10;
            if (searchParams.PageNumber > 1)
            {
                pageNumber = searchParams.PageNumber;
                searchParams.PageSize = searchParams.PageNumber * 10;
                searchParams.PageNumber = 1;
            }

            if (syntheticGeoName != null)
            {
                syntheticGeoName.Name = new CultureInfo("en").TextInfo.ToTitleCase(syntheticGeoName.Name.ToLower());
                searchParams.IsMultiLocationSearch = true;
                searchParams.SyntheticInfo = syntheticGeoName;

                switch (syntheticGeoName.LocationType)
                {
                    case LocationType.City:
                        searchParams.Cities = syntheticGeoName.ToLocationsList(); break;
                    case LocationType.County:
                        searchParams.Counties = syntheticGeoName.ToLocationsList(); break;
                    case LocationType.Zip:
                        searchParams.PostalCodes = syntheticGeoName.ToLocationsList(); break;
                    default:
                        searchParams.Markets = syntheticGeoName.ToLocationsList().Select(s => s.ToType<int>()).ToList();
                        break;
                }
            }
            else
            {
                searchParams.IsMultiLocationSearch = false;
                searchParams.SyntheticInfo = null;
            }   

            var model = GetBaseCommunityResultsMobileViewModel(searchParams, market);
            searchParams.PageSize = 10;
            searchParams.PageNumber = pageNumber;

            UserSession.PersonalCookie.MarketId = model.Market.MarketId;
            UserSession.PersonalCookie.State = market.StateAbbr;
            UserSession.PersonalCookie.SearchText = GetSearchText(searchParams);

            UserSession.SearchParametersV2 = searchParams;
            // ReSharper disable once Mvc.ViewNotResolved
            return View("Show", model);
        }

        //This is used for the current location search
        [NoCache]
        public virtual ActionResult CurrentLocation()
        {
            //If is not mobile the user will be redirect to the home page
            if (!NhsRoute.ShowMobileSite)
                return RedirectPermanent(Pages.Home, new List<RouteParam>());

            //Create a new SearchParams object
            var searchParams = UserSession.SearchParametersV2 ?? new SearchParams();

            searchParams.MarketId = 0;
            searchParams.PostalCode = string.Empty;
            searchParams.City = string.Empty;
            searchParams.County = string.Empty;
            searchParams.CommName = string.Empty;
            searchParams.SetFromRouteParam(NhsRoute.CurrentRoute.Params);
            searchParams.MinLng = 0;
            searchParams.MinLat = 0;
            searchParams.MaxLat = 0;
            searchParams.MaxLng = 0;
            searchParams.PageNumber = 1;
            searchParams.PageSize = 10;

            var model = new CommunityMobileViewModel
            {
                Zoom = 3,
                IsCurrentLocation = true,
                CommunityResults = new List<CommunityItemMobileViewModel>(),
                ResultCounts = new ApiResultCounts { Facets = new ApiFacets() },

            };

            ConfigurateAds(searchParams, model);
            model.SearchParametersJSon = searchParams.ToJson();
            UserSession.SearchParametersV2 = searchParams;

            OverrideDefaultMeta(model, new[]
            {
                new MetaTag
                {
                    Content = "Now showing communities and homes near Current Location | NewHomeSource",
                    Name = MetaName.Title
                }
            });

            // ReSharper disable once Mvc.ViewNotResolved
            return View("Show", model);
        }

        public virtual ActionResult GetResultsMobile(SearchParams parameters, bool moreResults)
        {
            var model = new BaseCommunityMobileViewModel();
            //When the user click in get More Results link
            //Get the next page
            if (moreResults)
            {
                //Get the results from the web api
                var resultsWepApi = GetCommunitiesFromApi<CommunityItemMobileViewModel>(parameters);
                var results = resultsWepApi.Result ?? new List<CommunityItemMobileViewModel>();
                model.Market = parameters.MarketId == 0 ? null : GetMarket(parameters.MarketId);
                model.CommunityResults = SetInformationCommunities(results, parameters, model);

                //Configuration for the add in the results
                model.ShowResultsBanner = parameters.ShowResultsBanner;
                model.ResultsBannerPosition = parameters.ResultsBannerPosition;
                model.CurrentPage = parameters.PageNumber;
                //End
                model.MarketId = parameters.MarketId;
                model.CommunitiesIdList = model.CommunityResults.Where(c => !c.IsBasic.ToType<bool>()).Select(c => c.BuilderId + " " + c.Id).ToArray().Join(",");
                model.BasicCommunitiesIdList = model.CommunityResults.Where(c => c.IsBasic.ToType<bool>()).Select(c => c.BuilderId + " " + c.Id).ToArray().Join(",");
            }
            //When user use the filters
            else
            {
                UserSession.PreviewsGroupingBarValue = string.Empty;
                model = FillViewMobileModelFromWepApi(parameters);
                model.IsTriggerFacets = true;
            }

            return PartialView(NhsMvc.Default.ViewsMobile.CommunityResults.ResultsData, model);
        }

        public virtual ActionResult QuickViewMobile(int communityId, int marketId, string phoneNumber, bool isBoylResult)
        {
            //Create a copy of the Search Parameters, in that way the originals don't get affected
            var searchParams = isBoylResult
                ? new SearchParams()
                : UserSession.SearchParametersV2.DeepCloneObject<SearchParams>();

            if (searchParams == null)
            {
                UserSession.SearchParametersV2 = new SearchParams();
                UserSession.SearchParametersV2.Init();
                searchParams = UserSession.SearchParametersV2;
            }

            //Set the CommunityId and the marketId for the search
            searchParams.CommId = communityId;
            searchParams.MarketId = marketId;
            searchParams.WebApiSearchType = WebApiSearchType.Exact;
            searchParams.PageNumber = 1;
            searchParams.PageSize = 99999;
            searchParams.BuilderId = 0;
            searchParams.SortBy = SortBy.HomeStatus;
            searchParams.NoBoyl = false;

            //Begin Case 79892: Clean location Filters
            searchParams.City = string.Empty;
            searchParams.County = string.Empty;
            searchParams.PostalCode = string.Empty;
            //End Case 79892: Clean location Filters

            //Get the results from the web api
            var resultsWepApi = GetHomesFromApi<HomeItemMobileViewModel>(searchParams);
            var results = resultsWepApi.Result ?? new List<HomeItemMobileViewModel>();

            var comm = results.FirstOrDefault();
            var model = new QuickViewViewModel();
            model.Homes = results;
            var market =_marketService.GetMarket(marketId);
            foreach (var home in  model.Homes)
            {
                home.MarketName = market.MarketName;
            }
            if (comm != null)
            {
                model.CommunityName = comm.CommName;
                model.BuilderId = comm.BuilderId;
                model.CommunityId = communityId;
                model.PhoneNumber = phoneNumber;
                model.IsBoylResult = isBoylResult;
            }
            if (UserSession.UserProfile.IsLoggedIn())
            {
                var pl = new PlannerListing(model.CommunityId, model.BuilderId, ListingType.Community);
                if (UserSession.UserProfile.Planner.SavedCommunities.Contains(pl))
                    model.SavedToPlanner = true;
            }
            return PartialView(NhsMvc.Default.ViewsMobile.CommunityResults.QuickView, model);
        }

        //Get the counts for the facets
        [HttpPost]
        public virtual JsonResult GetCountsMobile(SearchParams searchParameters)
        {
            searchParameters = GetSearchParamsForCounts(searchParameters, null);
            var facetsCounts = GetFacetCounts(searchParameters);
            searchParameters.CountsOnly = false;
            var data = new { facetsCounts, facetsCounts.Facets.Communities };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual JsonResult GetNearbyCommunitiesMobile(double minLat, double minLng, double maxLat, double maxLng, string lat, string lng)
        {
            var searchParams = new SearchParams
            {
                MinLat = minLat,
                MinLng = minLng,
                MaxLat = maxLat,
                MaxLng = maxLng,
                PageSize = 25,
                PageNumber = 1,
                PartnerId = NhsRoute.PartnerId,
                NoBoyl = false,
                WebApiSearchType = WebApiSearchType.Map,
                SortBy = SortBy.Distance
            };

            var pointsApi = _apiService.GetResultsWebApi<ApiCommunityResultV2>(searchParams, WebApiMethods.Communities).Result;
            var json = pointsApi.GroupBy(n => new { n.Lat, n.Lng }).Select(
                      g =>
                          new
                          {
                              Latitude = g.Key.Lat,
                              Longitude = g.Key.Lng,
                              Name = g.Select(p => new { p.Name }),
                              Count = g.Count(),
                              MarketPoints = g.Select(p => new { CommunityId = p.Id, Count = g.Select( x=> x.NumHomes).FirstOrDefault(), p.IsBasic }),
                              CurrentPosition = (g.Key.Lat.ToType<double>() == lat.ToType<double>() && g.Key.Lng.ToType<double>() == lng.ToType<double>())
                          });
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        //Get the map point for the map and set the parameters on session
        public virtual JsonResult GetMapPointsMobile(SearchParams searchParameters)
        {
            var pageNumber = searchParameters.PageNumber;
            var pageSize = searchParameters.PageSize;
            searchParameters.ExcludeBasicListings = true;
            var result = _communityService.GetCommunityHomeMapPoints(searchParameters, SearchResultsPageType.CommunityResults, NhsRoute.PartnerId);
            searchParameters.PageNumber = pageNumber;
            searchParameters.PageSize = pageSize;
            UserSession.SearchParametersV2 = searchParameters;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //Get the new SearchParameters if the location change in the facets
        [HttpGet]
        public virtual JsonResult GetLocation(Location location)
        {
            var searchParameters = new SearchParams
            {
                GetLocationCoordinates = true,
                MarketId = location.MarketId,
                GetRadius = true
            };
            switch (location.Type)
            {
                case (int)LocationType.City:
                    searchParameters.City = location.Name;
                    break;
                case (int)LocationType.Zip:
                    searchParameters.PostalCode = location.Name;
                    break;
                case (int)LocationType.County:
                    {
                        var countyName = location.Name.Replace("County", string.Empty).Trim();
                        searchParameters.County = countyName;
                    }
                    break;
            }

            if (location.Type >= (int)LocationType.Community)
                searchParameters.CommName = location.Name;

            GetLocationCoordinates(ref searchParameters, GetMarket(location.MarketId));
            searchParameters.GetLocationCoordinates = false;
            return Json(searchParameters, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult GetBrands(SearchParams searchParameters)
        {
            var data = _communityService.GetBuilders(searchParameters, NhsRoute.PartnerId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult SetCurrentLocationPersonalCoockie(double lat, double lng)
        {
            var personalCookie = UserSession.PersonalCookie;
            personalCookie.Lat = lat;
            personalCookie.Lng = lng;
            UserSession.PersonalCookie = personalCookie;
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public virtual JsonResult ImpressionLogger(int communityId, int builderId, string eventName)
        {
            var logger = new ImpressionLogger
            {
                CommunityId = communityId,
                BuilderId = builderId,
                PartnerId = NhsRoute.PartnerId.ToString(),
                Refer = UserSession.Refer
            };
            logger.LogView(eventName);
            return new JsonResult { Data = "Click has been logged!" };
        }

        public virtual JsonResult AddCommunityToUserPlanner(int communityId, int builderId, string eventName)
        {
            var pl = new PlannerListing(communityId, ListingType.Community);
            if (!UserSession.UserProfile.Planner.SavedCommunities.Contains(pl))
                UserSession.UserProfile.Planner.AddSavedCommunity(communityId, builderId);

            return ImpressionLogger(communityId, builderId, eventName);
        }

        #endregion

        #region Private Metods
        private CommunityMobileViewModel GetBaseCommunityResultsMobileViewModel(SearchParams searchParams, Market market)
        {
            UserSession.IsBasicListingGroup = false;
            UserSession.IsBasicCommunityGroup = false;
            var model = new CommunityMobileViewModel
            {
                Market = market,
                State = market.StateAbbr,
                SortBy = searchParams.SortBy,
                SortOrder = searchParams.SortOrder,
                CenterLat = searchParams.OriginLat,
                CenterLng = searchParams.OriginLng,
                SearchMapArea =
                    searchParams.MinLng > 0 || searchParams.MinLat > 0 || searchParams.MaxLat > 0 ||
                    searchParams.MaxLng > 0,
                Zoom = searchParams.Zoom,
                IsMapView = searchParams.IsMapVisible
            };

            ConfigurateAds(searchParams, model);

            FillViewMobileModelFromWepApi(searchParams, model);
            model.SearchParams = searchParams;
            //set seo content
            SetContentManagerViewModel(model, searchParams, market);
            SetMetaTagsForSeo(model);

            UserSession.AdBuilderIds.Clear();
            foreach (var builder in _builderService.GetMarketBuilders(NhsRoute.PartnerId, searchParams.MarketId).Where(builder => !UserSession.AdBuilderIds.Contains(builder.BuilderId)))
            {
                model.Globals.AdController.AddBuilderParameter(builder.BuilderId);
                UserSession.AdBuilderIds.Add(builder.BuilderId);
            }

            model.Globals.AdController.AddMarketParameter(searchParams.MarketId);
            model.Globals.AdController.AddPriceParameter(searchParams.PriceLow, searchParams.PriceHigh);
            model.Globals.AdController.AddStateParameter(model.Market.StateAbbr);

            return model;
        }

        private void ConfigurateAds(SearchParams searchParams, CommunityMobileViewModel model)
        {
            //Get the AdDisplay from Community Results
            var adDisplay = AddDislayConfigReader.GetAdDisplay(_pathMapper, NhsRoute.CurrentRoute.HostUrl);
            if (adDisplay != null)
            {
                var showTopBanner = adDisplay.AdSlots.FirstOrDefault(p => p.PlaceHolder == AdSlotConst.TopX11);
                if (showTopBanner != null)
                    model.ShowTopBanner = showTopBanner.Show;

                var showBottomBanner = adDisplay.AdSlots.FirstOrDefault(p => p.PlaceHolder == AdSlotConst.Middle);
                if (showBottomBanner != null)
                    model.ShowBottomBanner = showBottomBanner.Show;

                var showResultsBanner = adDisplay.AdSlots.FirstOrDefault(p => p.PlaceHolder == AdSlotConst.MiddleCenter);
                if (showResultsBanner != null)
                {
                    //Save this configuration in the parameters. To make easy to get it in the ajaxs call of the filter or show more
                    searchParams.ShowResultsBanner = showResultsBanner.Show;
                    searchParams.ResultsBannerPosition = showResultsBanner.Position - 1;
                }
            }

        }

        private BaseCommunityMobileViewModel FillViewMobileModelFromWepApi(SearchParams searchParams, BaseCommunityMobileViewModel model = null)
        {
            if (model == null)
            {
                model = new BaseCommunityMobileViewModel { Market = GetMarket(searchParams.MarketId) };
            }

            if (searchParams.PageNumber == 0)
                searchParams.PageNumber = 1;

            //Configuration for the add in the results
            model.ShowResultsBanner = searchParams.ShowResultsBanner;
            model.ResultsBannerPosition = searchParams.ResultsBannerPosition;
            model.CurrentPage = searchParams.PageNumber;
            //End
            model.MarketId = searchParams.MarketId;

            model.City = GetLocationCoordinates(ref searchParams, model.Market);
            searchParams.GetLocationCoordinates = false;

            var resultsWepApi = GetCommunitiesFromApi<CommunityItemMobileViewModel>(searchParams);
            var results = resultsWepApi.Result ?? new List<CommunityItemMobileViewModel>();

            model.ResultCounts = resultsWepApi.ResultCounts;
            model.CommunityResults = SetInformationCommunities(results, searchParams, model);
            model.SearchParametersJSon = searchParams.ToJson();
            UserSession.SearchParametersV2 = searchParams;

            //Get the number of pages that the current search
            var numberOfPages = Math.Ceiling(resultsWepApi.ResultCounts.CommCount.ToType<decimal>() / 10).ToType<int>();

            if (numberOfPages == 0)
                numberOfPages = 1;
            model.Pages = numberOfPages;

            model.CommunitiesIdList = model.CommunityResults.Where(c => !c.IsBasic.ToType<bool>()).Select(c => c.BuilderId + " " + c.Id).ToArray().Join(",");
            model.BasicCommunitiesIdList = model.CommunityResults.Where(c => c.IsBasic.ToType<bool>()).Select(c => c.BuilderId + " " + c.Id).ToArray().Join(",");

            return model;
        }

        //Create the groups and  set if comm is Favorite Listing 
        private List<CommunityItemMobileViewModel> SetInformationCommunities(List<CommunityItemMobileViewModel> communityResults, SearchParams searchParams, BaseCommunityMobileViewModel model)
        {
            var savedListings = UserSession.UserProfile.IsLoggedIn()
               ? UserSession.UserProfile.Planner.SavedCommunities.Select(p => p.ListingId).ToList()
               : new List<int>();

            var prevGroupValue = UserSession.PreviewsGroupingBarValue;

            var isBasicListingGroup = UserSession.IsBasicListingGroup;
            var isBasicCommunityGroup = UserSession.IsBasicCommunityGroup;
            //var isNearbyBasicListingGroup = UserSession.IsNearbyBasicListingGroup;
            //var isNearbyBasicCommunityGroup = UserSession.IsNearbyBasicCommunityGroup;

            //var isBasicListingGroup = false;
            //var isBasicCommunityGroup = false;

            foreach (var communityItem in communityResults)
            {
                if (model.Market != null)
                {
                    communityItem.MarketName = model.Market.MarketName;
                    communityItem.StateName = model.Market.State.StateName;
                }
                else
                {
                    communityItem.StateName = communityItem.MarketName = "Current Location";
                }
                model.Globals.AdController.AddCityParameter(communityItem.City);
                model.Globals.AdController.AddZipParameter(communityItem.Zip);
                communityItem.IsFavoriteListing = savedListings.Contains(communityItem.Id);
                SetGroupingInfo(SearchResultsPageType.CommunityResults, communityItem, searchParams, ref prevGroupValue, ref isBasicListingGroup, ref isBasicCommunityGroup);
                UserSession.PreviewsGroupingBarValue = prevGroupValue;
            }

            return communityResults;
        }

        private SearchParams GetSearchParamsForCounts(SearchParams searchParameters, Location location)
        {
            if (location != null)
            {
                if (searchParameters.MarketId != location.MarketId)
                {
                    searchParameters = new SearchParams();
                    searchParameters.Init();
                }
                else
                {
                    searchParameters.PostalCode = string.Empty;
                    searchParameters.City = string.Empty;
                    searchParameters.County = string.Empty;
                    searchParameters.SortFirstBy = string.Empty;
                    searchParameters.SortBy = SortBy.Random;
                    searchParameters.SortOrder = false;
                    searchParameters.GetLocationCoordinates = true;
                }

                searchParameters.MarketId = location.MarketId;
                switch (location.Type)
                {
                    case (int)LocationType.City:
                        searchParameters.City = location.Name;
                        break;
                    case (int)LocationType.Zip:
                        searchParameters.PostalCode = location.Name;
                        break;
                    case (int)LocationType.County:
                        {
                            var countyName = location.Name.Replace("County", string.Empty).Trim();
                            searchParameters.County = countyName;
                        }
                        break;
                }

                if (location.Type >= (int)LocationType.Community)
                    searchParameters.CommName = location.Name;
            }
            searchParameters.CountsOnly = true;
            return searchParameters;
        }
        #endregion
    }
}
