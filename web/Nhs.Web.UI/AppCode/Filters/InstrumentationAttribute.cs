﻿using System;
using System.Text;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants.Route;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Utility.Web;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Filters
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class InstrumentationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var view = filterContext.Result;

            if (view != null && view is ViewResultBase)
            {
                BaseViewModel viewModel = ((ViewResultBase)view).ViewData.Model as BaseViewModel;
                if (viewModel != null && viewModel.Globals != null)
                {
                    var serverName = filterContext.RequestContext.HttpContext.Server.MachineName;
                    viewModel.Globals.ServerName = MetaReader.ConstructTag(serverName, MetaName.ServerName);

                    //var ipAddress = RouteParams.Ip.Value<uint>() <= 0 ? HTTP.GetClientIpAddress(filterContext.RequestContext.HttpContext.Request).IpToInt32() : RouteParams.Ip.Value<uint>(); //test ip by passing in query param

                    //viewModel.Globals.IpAddress = MetaReader.ConstructTag(ipAddress.Int32ToIp() + ":" + ipAddress, MetaName.IpAddress);
                    if (RouteParams.ShowRouteInfo.Value<bool>())
                    {
                        var routeInfo = new StringBuilder();
                        routeInfo.AppendLine("<!-----Route Info-------");
                        routeInfo.AppendLine("Matching Route: " + NhsRoute.CurrentRoute.RouteUrl);
                        routeInfo.AppendLine("Host Url: " + NhsRoute.CurrentRoute.HostUrl);
                        routeInfo.AppendLine("Route Name: " + NhsRoute.CurrentRoute.Name);
                        routeInfo.AppendLine("Ad Positions: " + NhsRoute.CurrentRoute.AdPositions.Flatten(';'));
                        routeInfo.AppendLine("Controller: " + NhsRoute.CurrentRoute.Controller.Name);
                        routeInfo.AppendLine("Action: " + NhsRoute.CurrentRoute.Controller.Action);
                        routeInfo.AppendLine("Params Url: " +
                                             NhsRoute.CurrentRoute.Params.ToUrl(NhsRoute.CurrentRoute.Function));
                        routeInfo.AppendLine("------------------------>");

                        //var scriptRouteInfo = new StringBuilder();

                        routeInfo.AppendFormat(@"<meta name=""MatchingRouteMeta"" content=""MatchingRoute:{0},HostUrl:{1},RouteName:{2},Controller:{3},Action:{4},ParamsUrl:{5}""/>",
                          NhsRoute.CurrentRoute.RouteUrl,
                          NhsRoute.CurrentRoute.HostUrl,
                          NhsRoute.CurrentRoute.Name,
                          NhsRoute.CurrentRoute.Controller.Name,
                          NhsRoute.CurrentRoute.Controller.Action,
                          NhsRoute.CurrentRoute.Params.ToUrl(NhsRoute.CurrentRoute.Function));

                        //routeInfo.AppendLine(scriptRouteInfo.ToString());

                        viewModel.Globals.RouteInfo = routeInfo.ToString();
                    }
                }
            }
            base.OnActionExecuted(filterContext);
        }
    }
}