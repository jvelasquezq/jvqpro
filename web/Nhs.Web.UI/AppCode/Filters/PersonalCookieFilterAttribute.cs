﻿using System;
using System.Web.Mvc;
using Nhs.Library.Web;

namespace Nhs.Web.UI.AppCode.Filters
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class PersonalCookieFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            UserSession.PersonalCookie.Save();
            base.OnActionExecuted(filterContext);
        }
    }
}