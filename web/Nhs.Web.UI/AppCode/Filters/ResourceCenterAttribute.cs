﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.UI.MobileControls;
using Nhs.Library.Constants;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.ViewModels;
using StructureMap;

namespace Nhs.Web.UI.AppCode.Filters
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class ResourceCenterAttribute : ActionFilterAttribute
    {
        private ICmsService _cmsService;

        /// <summary>
        /// Validate if the current Partner can see the Resource center or not if is NHS or PRO and his partners
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            var isValidRcPartnerCombo = (PartnersConst.Pro.ToType<int>() == NhsRoute.BrandPartnerId &&      //PRO brand partner and all partners
                                        NhsRoute.CurrentRoute.Function == Pages.AgentResources);
            
            if (isValidRcPartnerCombo) 
                return;

            string url;

            if (NhsRoute.CurrentRoute.Function == Pages.HomeGuide)
            {
                filterContext.HttpContext.Response.StatusCode = 301;
                url = string.Format("/{0}", GetCmsLink()).Replace("//", "/");
            }
            else if (PartnersConst.Pro.ToType<int>() == NhsRoute.BrandPartnerId)
            {              
                url = BuildNotFoundRequest(filterContext);
            }
            else
            {
                filterContext.HttpContext.Response.StatusCode = 301;
                url = string.Format("/{0}/{1}", NhsRoute.PartnerSiteUrl, GetCmsLink()).Replace("//", "/");
            }

            filterContext.Result = new RedirectResult(url, true);
        }

        /// <summary>
        /// Fill the data of the search box for PRO if is empty
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {

            //return if is not PRO site
            if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>()) return;

            var baseModel = filterContext.Result as System.Web.Mvc.ViewResultBase;

            if (baseModel == null) return;

            //get the Model
            var objectContent = baseModel.ViewData.Model;
            //get the base instance
            var model = objectContent as ResourceCenterViewModels.ResourceCenterBaseViewModel;

            if (model == null || model.CmsProSearchParams != null) return;

            if (_cmsService == null)
            {
                //inject the service if is null
                _cmsService = ObjectFactory.GetInstance<ICmsService>();
            }

            //fill the search pro data
            model.CmsProSearchParams = _cmsService.FillSearchViewModel("",
                                                                       UserSession.PersonalCookie.PriceLow.ToString(
                                                                           CultureInfo.InvariantCulture),
                                                                       UserSession.PersonalCookie.PriceHigh.ToString
                                                                           (CultureInfo.InvariantCulture),
                                                                           NhsRoute.BrandPartnerId,
                                                                       UserSession.PersonalCookie.BathRooms,
                                                                       UserSession.PersonalCookie.BedRooms);
        }


        private string GetCmsLink()
        {
            string cmsLink=Pages.AgentResources;
            return cmsLink;
        }

        private static string BuildNotFoundRequest(ControllerContext filterContext, bool addPartnerSiteUrl = true)
        {
            filterContext.HttpContext.Response.StatusCode = 404;
            var cmsLink = string.Format("/{0}/error?statuscode=404",  
                addPartnerSiteUrl ? NhsRoute.PartnerSiteUrl : string.Empty).Replace("//", "/");
            return cmsLink;
        }
    }

    public class MoveScriptFilter : MemoryStream
    {
        private readonly Stream _outputStream;
        public MoveScriptFilter(Stream outputStream)
        {
            _outputStream = outputStream;
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            _outputStream.Write(buffer, 0, buffer.Length);
        }

        public override void Close()
        {
            const string script = @"<script>
                               $jq(document).ready(function(){
                                   $jq('#mv_SearchNav ul li').each(function(){$jq(this).removeClass('active');});
                                   $jq('#mv_SearchNav ul li').eq(1).addClass('active');
                               });                               
                             </script>";
            var buffer = Encoding.UTF8.GetBytes(script);
            _outputStream.Write(buffer, 0, buffer.Length);
            base.Close();
        }
    }
}
