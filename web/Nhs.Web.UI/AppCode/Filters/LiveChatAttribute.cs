﻿using System;
using System.Web.Mvc;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.Filters
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class LiveChatAttribute : ActionFilterAttribute
    {
        //This guy gets injected in NhsServicesRegistry (SetAllProperties)
        public IMarketService MarketService { get; set; }

        public IPathMapper PathMapper { get; set; }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var view = filterContext.Result;

            if (view != null && view is ViewResultBase)
            {
                var viewModel = ((ViewResultBase) view).ViewData.Model as BaseViewModel;
                if (viewModel != null && viewModel.Globals != null)
                    viewModel.Globals.ActiveEngageHeader = LiveChatHelper.GetLiveChatScript(viewModel, MarketService,
                                                                                            PathMapper);
            }
            base.OnActionExecuted(filterContext);
        }
    }
}
