﻿using System;
using System.Web.Mvc;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Routing.Interface;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Filters
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class PartnerInfoAttribute : ActionFilterAttribute
    {
        public IPartnerService PartnerService
        { get; set; }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var view = filterContext.Result;

            if (view != null && view is ViewResultBase)
            {
                var viewModel = ((ViewResultBase)view).ViewData.Model as BaseViewModel;
                if (viewModel != null && viewModel.Globals != null)
                {
                    viewModel.Globals.PartnerInfo = PartnerService.GetPartner(NhsRoute.PartnerId);
                    if (NhsRoute.PartnerId == NhsRoute.BrandPartnerId)
                        viewModel.Globals.PartnerInfo.BrandPartnerName = viewModel.Globals.PartnerInfo.PartnerName;
                    else
                        viewModel.Globals.PartnerInfo.BrandPartnerName =
                            PartnerService.GetPartner(NhsRoute.BrandPartnerId).PartnerName;
                }
            }
            base.OnActionExecuted(filterContext);
        }

    }
}
