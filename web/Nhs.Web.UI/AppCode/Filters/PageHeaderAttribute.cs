﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Enums;
using Nhs.Mvc.Routing.Interface;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Filters
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class PageHeaderAttribute : ActionFilterAttribute
    {
        public MetaReader MetaReader
        { get; set; }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var view = filterContext.Result;

            if (view != null && view is ViewResultBase)
            {
                BaseViewModel viewModel = ((ViewResultBase)view).ViewData.Model as BaseViewModel;
                if (viewModel != null && viewModel.Globals != null)
                {
                    List<MetaTag> metas = MetaReader.GetMetaTagInformation(NhsRoute.CurrentRoute.Function, null);
                    if (string.IsNullOrEmpty(viewModel.Globals.Title))
                    {
                        viewModel.Globals.Title = MetaReader.ConstructTag(metas, MetaName.Title); //Initialize base tag (bad pattern I know!)
                        viewModel.Globals.Title = viewModel.Globals.PartnerLayoutConfig.PartnerIsABrand ? MetaReader.ConstructTag(metas, MetaName.Title) : viewModel.Globals.Shell.Title.Value;//Override if a partner
                    }
                    if (string.IsNullOrEmpty(viewModel.Globals.Keywords))
                        viewModel.Globals.Keywords = MetaReader.ConstructTag(metas, MetaName.Keywords);
                    if (string.IsNullOrEmpty(viewModel.Globals.Description))
                        viewModel.Globals.Description = MetaReader.ConstructTag(metas, MetaName.Description);
                    if (string.IsNullOrEmpty(viewModel.Globals.Description))
                        viewModel.Globals.Robots = MetaReader.ConstructTag(metas, MetaName.Robots);
                }
            }
            base.OnActionExecuted(filterContext);
        }
    }
}
