﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Nhs.Mvc.Routing.Interface;

namespace Nhs.Web.UI.AppCode.Filters
{
    /// <summary>
    /// Attribute to support Action Overloading - checks to make sure if all action parameters match
    /// Credit: http://blog.abodit.com/2010/02/asp-net-mvc-ambiguous-match/ 
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class MatchParametersAttribute : ActionMethodSelectorAttribute
    {
        public override bool IsValidForRequest(ControllerContext controllerContext, MethodInfo methodInfo)
        {
            // The Route values
            List<string> requestRouteValuesKeys = controllerContext.RouteData.Values.Where(v => !(v.Key == "controller" || v.Key == "action" || v.Key == "area")).Select(rv => rv.Key).ToList().ConvertAll(s=>s.ToLower());

            requestRouteValuesKeys.Remove("partnersiteurl");

            if (requestRouteValuesKeys.Contains("optionals"))
            {
                requestRouteValuesKeys.Remove("optionals");
                controllerContext.RouteData.Values.Remove("optionals");
                foreach (var param in NhsRoute.CurrentRoute.Params)
                {
                    if (!requestRouteValuesKeys.Contains(param.Name.ToString()))
                    {
                        requestRouteValuesKeys.Add(param.Name.ToString());
                    }
                    if (!controllerContext.RouteData.Values.ContainsKey((param.Name.ToString())))
                    {
                        controllerContext.RouteData.Values.Add(param.Name.ToString(), param.Value);
                    }
                }
            }

            // The Form values
            var form = controllerContext.HttpContext.Request.Form;
            List<string> requestFormValuesKeys = form.AllKeys.ToList().ConvertAll(s=>s.ToLower());

            // The parameters this method expects
            var parameters = methodInfo.GetParameters();

            // Parameters from the method that we haven’t matched up against yet
            var parametersNotMatched = parameters.ToList();

            // each parameter of the method can be marked as a [RouteValue] or [FormValue] or both or nothing
            foreach (var param in parameters)
            {
                string name = param.Name.ToLower();

                bool isRouteParam = param.GetCustomAttributes(true).Any(a => a is RouteValueAttribute);
                bool isFormParam = param.GetCustomAttributes(true).Any(a => a is FormValueAttribute);

                if (isRouteParam && requestRouteValuesKeys.Contains(name))
                {
                    // Route value matches parameter
                    requestRouteValuesKeys.Remove(name);
                    parametersNotMatched.Remove(param);
                }
                else if (isFormParam && requestFormValuesKeys.Contains(name))
                {
                    // Form value matches method parameter
                    requestFormValuesKeys.Remove(name);
                    parametersNotMatched.Remove(param);
                }
                else
                {
                    // methodInfo parameter does not match a route value or a form value
                    Debug.WriteLine(methodInfo + " failed to match " + param + " against either a RouteValue or a FormValue");
                    return false;
                }
            }

            // Having removed all the parameters of the method that are matched by either a route value or a form value
            // we are now left with all the parameters that do not match and all the route and form values that were not used

            if (parametersNotMatched.Count > 0)
            {
                Debug.WriteLine(methodInfo + " – FAIL: has parameters left over not matched by route or form values");
                return false;
            }

            if (requestRouteValuesKeys.Count > 0)
            {
                Debug.WriteLine(methodInfo + " – FAIL: Request has route values left that aren’t consumed");
                return false;
            }

            if (requestFormValuesKeys.Count > 1)
            {
                Debug.WriteLine(methodInfo + " – FAIL : unmatched form values " + string.Join(", ", requestFormValuesKeys.ToArray()));
                return false;
            }

            Debug.WriteLine(methodInfo + " – PASS – unmatched form values " + string.Join(", ", requestFormValuesKeys.ToArray()));
            return true;
        }
    }

    [AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false)]
    public sealed class RouteValueAttribute : Attribute
    {
    }

    /// <summary>
    /// This attribute can be placed on a parameter of an action method that should be present in FormData
    /// </summary>
    [AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false)]
    public sealed class FormValueAttribute : Attribute
    {
    }
}
