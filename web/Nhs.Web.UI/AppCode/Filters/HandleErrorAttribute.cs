﻿using System;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.Filters
{
    public class HandleErrorAttribute : System.Web.Mvc.HandleErrorAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            //if (context.HttpContext.IsCustomErrorEnabled)
            //{
            this.View = "MVCError";
            ErrorLogger.LogError(context.Exception);
            //context.ExceptionHandled = true;
            
            //}
            base.OnException(context);

        } 

    }
}
