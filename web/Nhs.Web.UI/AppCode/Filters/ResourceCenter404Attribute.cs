﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.Filters
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ResourceCenter404Attribute : ActionFilterAttribute
    {
        private string RedirectTo404()
        {
            var urlParams = new List<RouteParam>
                {
                    new RouteParam(RouteParams.Statuscode, "404", RouteParamType.QueryString)
                };
            return urlParams.ToUrl(Pages.MvcError.ToTitleCase());
        }
    }
}