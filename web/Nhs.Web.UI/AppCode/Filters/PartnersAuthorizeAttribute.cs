﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.Filters
{
    public enum NhsRoutePartnerTypeEnum : byte
    {
        UseNhsRoutePartnerId = 0,
        UseNhsRouteBrandPartnerId
    }

    /// <summary>
    /// Validate againts the NhsRoute.PartnerId if the the current partner request is in the list of partners allowed
    /// to get the data, if not Error 404 is return to the user
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class PartnersAuthorizeAttribute : ActionFilterAttribute
    {
        private readonly PartnersConst[] _partners;
        private readonly NhsRoutePartnerTypeEnum _partnerType;
        public PartnersAuthorizeAttribute(NhsRoutePartnerTypeEnum type, params PartnersConst[] partners)
        {
            _partners = partners;
            _partnerType = type;
        }

        /// <summary>
        /// Validate if the current Controller/Method can be see in the partner list
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {        
            if (ValidatePartnerRequest()) return;

            filterContext.HttpContext.Response.StatusCode = 404;
            var urlParams = new List<RouteParam> { new RouteParam(RouteParams.Statuscode, "404", RouteParamType.QueryString) };
            filterContext.Result = new RedirectResult(urlParams.ToUrl(Pages.MvcError.ToTitleCase()));
        }

        private bool ValidatePartnerRequest()
        {
            var isValid = false;

            if (_partners != null)
            {
                switch (_partnerType)
                {
                    case NhsRoutePartnerTypeEnum.UseNhsRoutePartnerId:
                        isValid =_partners.Any(a => a.ToType<int>() == NhsRoute.PartnerId);
                        break;
                    case NhsRoutePartnerTypeEnum.UseNhsRouteBrandPartnerId:
                        isValid = _partners.Any(a => a.ToType<int>() == NhsRoute.BrandPartnerId);
                        break;
                } 
            }

            return isValid;
        }
    }
}