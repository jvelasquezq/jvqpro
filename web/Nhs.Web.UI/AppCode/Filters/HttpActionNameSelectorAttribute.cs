﻿using System;
using System.Reflection;
using System.Web.Mvc;

namespace Nhs.Web.UI.AppCode.Filters
{
    /**
     * <summary>
     * This attribute allows you to specify several actions in the same form by differenciating them with the name of the submit button.
     * In order to use this you just need to add the attribute HttpActionNameSelectorAttribute to the Action in the controller, the action
     * must have the same name than the name of the submit button.
     * </summary>
     */
    public class HttpActionNameSelectorAttribute : ActionNameSelectorAttribute
    {
        public override bool IsValidName(ControllerContext controllerContext, string actionName, MethodInfo methodInfo)
        {
            if (actionName.Equals(methodInfo.Name, StringComparison.InvariantCultureIgnoreCase))
                return true;

            var request = controllerContext.RequestContext.HttpContext.Request;
            return request[methodInfo.Name] != null;
        }
    }
}