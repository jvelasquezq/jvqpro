﻿using System;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.Filters
{
    //NOTE: DO NOT USE Logic moved to BaseViewModel
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class ShellFilterAttribute : ActionFilterAttribute
    {
        public ShellFactory ShellFactory
        { get; set; }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var view = filterContext.Result as ViewResultBase;

            if (view != null )
            {
                BaseViewModel viewModel = view.ViewData.Model as BaseViewModel;
                if (viewModel != null)
                {
                    viewModel.Globals.Shell = ShellFactory.GetShell(NhsRoute.PartnerId, Configuration.PartnerShellType, MetaReader.StripTag(viewModel.Globals.Title, MetaName.Title));
                    viewModel.Globals.Title = MetaReader.ConstructTag(viewModel.Globals.Shell.Title.InnerMarkup,MetaName.Title);
                    viewModel.Globals.Shell.Head.InnerMarkup = MetaReader.RemoveTitle(viewModel.Globals.Shell.Head.InnerMarkup);
                }
            }

            base.OnActionExecuted(filterContext);
        }
    }
}
