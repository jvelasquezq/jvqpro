﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
    public class RedirectTo301Attribute : ActionFilterAttribute
    {
        private readonly PartnersConst[] _partners;
        private readonly NhsRoutePartnerTypeEnum _partnerType;
        private readonly string _redirectUrl;
        private readonly Regex _regex;

        public RedirectTo301Attribute(NhsRoutePartnerTypeEnum type, string redirectUrl,  params PartnersConst[] partnersToApply)
        {
            _partners = partnersToApply;
            _partnerType = type;
            _redirectUrl = redirectUrl;
        }

        public RedirectTo301Attribute(string regex, string redirectUrl, NhsRoutePartnerTypeEnum type, params PartnersConst[] partnersToApply)
        {
            _partners = partnersToApply;
            _partnerType = type;
            _redirectUrl = redirectUrl;
            _regex = new Regex(regex);
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (_regex != null && RedirectPartnerRequest())
            {
                if (_regex.IsMatch(filterContext.HttpContext.Request.RawUrl))
                {
                    PageRedirect(filterContext);
                    return;                    
                }
            }

            if (RedirectPartnerRequest())
            {
                PageRedirect(filterContext);
            }
        }

        private void PageRedirect(ActionExecutingContext filterContext)
        {
            filterContext.HttpContext.Response.StatusCode = 301;
            var urlParams = new List<RouteParam>();
            filterContext.Result = new RedirectResult(urlParams.ToUrl(_redirectUrl));
        }

        private bool RedirectPartnerRequest()
        {
            var isValid = false;

            if (_partners != null)
            {
                switch (_partnerType)
                {
                    case NhsRoutePartnerTypeEnum.UseNhsRoutePartnerId:
                        isValid = _partners.Any(a => a.ToType<int>() == NhsRoute.PartnerId);
                        break;
                    case NhsRoutePartnerTypeEnum.UseNhsRouteBrandPartnerId:
                        isValid = _partners.Any(a => a.ToType<int>() == NhsRoute.BrandPartnerId);
                        break;
                }
            }

            return isValid;
        }
    }
}