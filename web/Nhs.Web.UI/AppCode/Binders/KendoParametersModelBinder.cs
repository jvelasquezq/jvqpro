﻿using System;
using System.Web.Mvc;
using Nhs.Library.Business.Kendo;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.Binders
{
    public class KendoParametersModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var filter = new KendoFilterContainer
            {
                Logic = controllerContext.HttpContext.Request.Params["filter[logic]"],
                Value = controllerContext.HttpContext.Request.Params["filter[filters][0][value]"],
                Field = controllerContext.HttpContext.Request.Params["filter[filters][0][field]"],
                IgnoreCase = controllerContext.HttpContext.Request.Params["filter[filters][0][ignoreCase]"].ToType<bool>()
            };
            
            var op = controllerContext.HttpContext.Request.Params["filter[filters][0][operator]"];
            KendoFilterOperator fop;
            if (Enum.TryParse(op, true, out fop))
            {
                filter.Operator = fop;
            }
            
            return filter;
        }
    }       
}