﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers.Content;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Partials;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.UrlRewriter.Configuration;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;
using Resources;
using StructureMap;
using StructureMap.Query;

namespace Nhs.Web.UI.AppCode.Extensions
{
    public static class LinkGenerationExtensions
    {

        public static MvcHtmlString GetPolicyLink(this HtmlHelper htmlHelper, ISecondaryInfoViewModel model,
            string linkText, string cssClass, string logMethod)
        {
            var showModal = ((BaseViewModel) model).Globals.PartnerLayoutConfig.PartnerAllowsModals;
            int width, height;
            var paramz = new List<RouteParam>();

            if (model.IsPageCommDetail) //Comm detail
            {
                paramz.Add(new RouteParam(RouteParams.LeadType, LeadType.Community, RouteParamType.QueryString));
                paramz.Add(new RouteParam(RouteParams.CommunityList, model.CommunityId.ToString(),
                    RouteParamType.QueryString));
                paramz.Add(new RouteParam(RouteParams.Builder, model.BuilderId.ToString(), RouteParamType.QueryString));
                paramz.Add(new RouteParam(RouteParams.Market, model.MarketId.ToString(), RouteParamType.QueryString));
                paramz.Add(new RouteParam(RouteParams.FromPage, Pages.CommunityDetail, RouteParamType.QueryString));
            }
            else //Home detail
            {

                var hmModel = model as HomeDetailViewModel;
                paramz.Add(new RouteParam(RouteParams.LeadType, LeadType.Home, RouteParamType.QueryString));
                if (hmModel.IsPlan)
                {
                    paramz.Add(new RouteParam(RouteParams.PlanList, hmModel.PlanId.ToString(),
                        RouteParamType.QueryString));
                    paramz.Add(new RouteParam(RouteParams.PlanId, hmModel.PlanId.ToString(), RouteParamType.QueryString));
                }
                else
                {
                    paramz.Add(new RouteParam(RouteParams.SpecList, hmModel.SpecId.ToString(),
                        RouteParamType.QueryString));
                    paramz.Add(new RouteParam(RouteParams.SpecId, hmModel.SpecId.ToString(), RouteParamType.QueryString));
                }
                paramz.Add(new RouteParam(RouteParams.Builder, hmModel.BuilderId.ToString(), RouteParamType.QueryString));
                paramz.Add(new RouteParam(RouteParams.Community, hmModel.CommunityId.ToString(),
                    RouteParamType.QueryString));
                paramz.Add(new RouteParam(RouteParams.Market, model.MarketId.ToString(), RouteParamType.QueryString));
                paramz.Add(new RouteParam(RouteParams.FromPage, Pages.HomeDetail, RouteParamType.QueryString));
            }

            if (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser)
            {
                width = ModalWindowsConst.LeadsRequestInfoLoggedWidth;
                height = ModalWindowsConst.LeadsRequestInfoLoggedHeight;
            }
            else
            {
                width = ModalWindowsConst.LeadsRequestInfoShortWidth;
                height = ModalWindowsConst.LeadsRequestInfoShortHeight;
            }

            var pageName = showModal ? Pages.LeadsRequestBrochureModal : Pages.LeadsRequestInfo;
            if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
            {
                if (string.IsNullOrEmpty(logMethod))
                {
                    return showModal
                        ? htmlHelper.NhsModalWindowLink(linkText, pageName, width, height, paramz,
                            new {@class = cssClass})
                        : htmlHelper.NhsLink(linkText, pageName, paramz, new {@class = cssClass}, true);
                }

                return showModal
                    ? htmlHelper.NhsModalWindowLink(linkText, pageName, width, height, paramz,
                        new {@class = cssClass, onclick = logMethod}, true)
                    : htmlHelper.NhsLink(linkText, pageName, paramz, new {@class = cssClass, onclick = logMethod});
            }

            paramz.Add(new RouteParam(RouteParams.IsBilled, model.IsBilled.ToString(), RouteParamType.QueryString));
            return htmlHelper.SendCustomBrochureLink(paramz);
        }

        public static MvcHtmlString GetAgentPolicyLinkInfo(this HtmlHelper htmlHelper, ISecondaryInfoViewModel model,
            bool isBtn)
        {
            return GetPolicyLink(htmlHelper, model, "View agent policy", "btn btn_FreeBrochure", isBtn);
        }

        public static MvcHtmlString GetAgentPolicyImageLink(this HtmlHelper htmlHelper, ISecondaryInfoViewModel model,
            bool isBtn)
        {
            return GetAgentPolicyLinkInfo(htmlHelper, model, isBtn);
        }

        public static MvcHtmlString GetNextStepsFreeBrochureImageLinkForPro(this HtmlHelper htmlHelper,
            ISecondaryInfoViewModel model, int step,
            string top)
        {
            var linkText = step == 1
                ? "Send brochure to client"
                : "Contact the builder";
            var link = (top == "top")
                ? string.Format(@"<a class=""send"" href=""#ContactBuilder"">{0}</a>", linkText)
                : string.Format(@"<a id=""pro_contactBuilder"" href=""#ContactBuilder"">{0}</a>", linkText);

            return MvcHtmlString.Create(link);
        }

        public static MvcHtmlString GetBuilderCDLogUrl(this HtmlHelper htmlHelper, ISecondaryInfoViewModel model,
            bool isFooter)
        {
            return GetBuilderCDLogUrl(htmlHelper, model, isFooter, false);
        }


        public static MvcHtmlString GetBuilderCDLogUrl(this HtmlHelper htmlHelper, ISecondaryInfoViewModel model,
            bool isFooter, bool isNextSteps)
        {
            string communityUrl = model.CommunityUrl;
            if (string.IsNullOrEmpty(communityUrl))
            {
                if (string.IsNullOrEmpty(model.BuilderUrl))
                    return MvcHtmlString.Empty;

                communityUrl = model.BuilderUrl;
            }
            string linkLabel = LanguageHelper.VisitTheWebsite;

            var paramz = new List<RouteParam>();
            string eventCode;
            if (model.PlanId > 0 || model.SpecId > 0)
                eventCode = model.PreviewMode
                    ? LogImpressionConst.HomeDetailBuilderPreview
                    : LogImpressionConst.HomeDetailBuilder;
            else
                eventCode = model.PreviewMode
                    ? LogImpressionConst.CommunityDetailBuilderPreview
                    : LogImpressionConst.CommunityDetailBuilder;

            if (isNextSteps && (model.PlanId > 0 || model.SpecId > 0))
                eventCode = LogImpressionConst.NextStepsClickWebsite;
            if (isNextSteps && (model.PlanId == 0 && model.SpecId == 0))
                eventCode = LogImpressionConst.NextStepsComClickWebsite;

            string gaPushPage = (model.PlanId > 0 || model.SpecId > 0) ? "Home" : "Community";
            string onclickJsCode =
                string.Format(
                    "$jq.googlepush('Outbound Links','{0}','Builder Website');jQuery.SetDataLayerPair('siteClickthrough');",
                    isNextSteps
                        ? string.Format("{0} - Next Steps", gaPushPage)
                        : string.Format("{0} - Info", gaPushPage));

            var logInfo = new
            {
                communityId = model.CommunityId.ToString(),
                communityName = model.CommunityName.RemoveSpecialCharacters(),
                builderId = model.BuilderId.ToString(),
                builderName = model.BuilderName.RemoveSpecialCharacters(),
                planId = model.PlanId.ToString(),
                specId = model.SpecId.ToString(),
                marketId = model.MarketId.ToString(),
                marketName = model.MarketName.RemoveSpecialCharacters()
            };

            return htmlHelper.NhsLinkLogAndRedirect(linkLabel, PathHelpers.CleanNavigateUrl(communityUrl), eventCode,
                logInfo, onclickJsCode, new {target = "_blank", @class = "nhs_BuilderSiteLink"});
        }

        public static MvcHtmlString GetCommunityResultsUrl(this HtmlHelper htmlHelper, Location location,
            string extraParameters)
        {
            var urlParams = new List<RouteParam>();
            string name = location.Name;

            if (name.IndexOf(",", StringComparison.Ordinal) != -1)
                name = name.Substring(0, name.IndexOf(",", StringComparison.Ordinal));

            urlParams.Add(new RouteParam(RouteParams.Market, location.MarketId.ToString()));

            if (location.Type == (int) LocationType.City)
                urlParams.Add(new RouteParam(RouteParams.City, name));
            else if (location.Type == (int) LocationType.Zip)
                urlParams.Add(new RouteParam(RouteParams.PostalCode, name));
            else if (location.Type == (int) LocationType.County)
            {
                name = name.Replace("County", string.Empty).Trim();

                urlParams.Add(new RouteParam(RouteParams.County, name));
            }
            else if (location.Type == (int) LocationType.Community || location.Type == (int) LocationType.Developer)
                urlParams.Add(new RouteParam(RouteParams.CommunityName, name,RouteParamType.QueryString));

            var page = RedirectionHelper.GetCommunityResultsPage();
            var linkUrl = urlParams.ToResultsParams().ToUrl(page) + extraParameters;

            return
                htmlHelper.NhsLinkCustom(
                    (location.Type >= (int) LocationType.Community ? location.MarketName : location.Name) + ", " +
                    location.State, linkUrl);

        }


        public static MvcHtmlString GetBrandUrl(this HtmlHelper htmlHelper, Brand b, CommunityResultsViewModel Model)
        {
            if (b.IsBoyl)
            {
                var @params = new List<RouteParam>();
                @params.Add(new RouteParam
                {
                    Name = RouteParams.Market,
                    Value = Model.Market.MarketId.ToString(),
                    ParamType = RouteParamType.Friendly,
                    LowerCaseParamName = true
                });
                @params.Add(new RouteParam
                {
                    Name = RouteParams.BrandId,
                    Value = b.BrandId.ToString(),
                    ParamType = RouteParamType.Friendly,
                    LowerCaseParamName = true
                });

                return htmlHelper.NhsLink(b.BrandName, Pages.BOYLResults, @params,
                    new {@class = "nhs_BoylLink", title = b.BrandName, rel = b.BrandId.ToString()},
                    true);
            }

            return htmlHelper.NhsLink(b.BrandName, Pages.CommunityResults, b.CRParams(Model.Market.MarketId),
                new
                {
                    @class = "nhs_Facet",
                    title = b.BrandName,
                    rel = b.BrandId.ToString(),
                    onclick =
                        "commResults.get_log().logEvent('CRBR',0," + b.BrandId +
                        ");$jq.googlepush('Search Events', 'Search Refinement', 'All Buildersh');"
                },
                true);

        }

        public static MvcHtmlString GetProTvLink(this HtmlHelper htmlHelper,
            int marketId, string linkTitle = "New Home Source TV", string styleClassName = "")
        {
            var tvService = ObjectFactory.GetInstance<ITvService>();
            var marketService = ObjectFactory.GetInstance<IMarketService>();
            var markets = tvService.GetTvMarkets(NhsRoute.PartnerId).ToList();
            var partnerMarkets = marketService.GetMarkets(NhsRoute.PartnerId);
            var link = MvcHtmlString.Create(string.Empty);

            if (!markets.Any(a => partnerMarkets.Any(pm => pm.MarketId == a.MarketId))) return link;

            if (marketId == 0)
            {
                marketId = UserSession.PropertySearchParameters.MarketId;
            }

            //var marketId = model.CurrentMarketId > 0 ? model.CurrentMarketId : NhsUrl.GetMarketID;            

            var url = Pages.NewHomeSourceTv;

            if (markets.Any(m => m.MarketId == marketId))
            {
                url = string.Format(url + "/{0}",
                    markets.FirstOrDefault(m => m.MarketId == marketId).Keyword.ToLowerCase());
            }
            else if (markets.Count() == 1)
            {
                url = string.Format(url + "/{0}", markets.FirstOrDefault().Keyword.ToLowerCase());
            }
            dynamic attr = new System.Dynamic.ExpandoObject();
            attr.onclick = "jQuery.googlepush('Site Links','Navigation ','PRO New Home Source TV')";

            if (string.IsNullOrWhiteSpace(styleClassName) == false)
            {
                attr.@class = styleClassName;
            }

            link = htmlHelper.NhsLink(linkTitle, url, new List<RouteParam>(), (object) attr, true);

            return link;
        }

        public static MvcHtmlString GetAgentPolicyLink(this HtmlHelper htmlHelper, ISecondaryInfoViewModel model,
            bool isNextSteps)
        {
            string agentPolicyLink = model.AgentPolicyLink;

            if (string.IsNullOrEmpty(agentPolicyLink))
                return MvcHtmlString.Empty;

            const string linkLabel = "View agent policy";

            string eventCode;
            if (model.PlanId > 0 || model.SpecId > 0)
                eventCode = model.PreviewMode
                    ? LogImpressionConst.HomeDetailBuilderPreview
                    : LogImpressionConst.HomeDetailBuilder;
            else
                eventCode = model.PreviewMode
                    ? LogImpressionConst.CommunityDetailBuilderPreview
                    : LogImpressionConst.CommunityDetailBuilder;

            if (isNextSteps && (model.PlanId > 0 || model.SpecId > 0))
                eventCode = LogImpressionConst.NextStepsClickWebsite;
            if (isNextSteps && (model.PlanId == 0 && model.SpecId == 0))
                eventCode = LogImpressionConst.NextStepsComClickWebsite;


            var logInfo = new
            {
                communityId = model.CommunityId.ToString(),
                communityName = model.CommunityName.RemoveSpecialCharacters(),
                builderId = model.BuilderId.ToString(),
                planId = model.PlanId.ToString(),
                specId = model.SpecId.ToString(),
                marketId = model.MarketId.ToString()
            };

            return (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser)
                ? htmlHelper.NhsLinkLogAndRedirect(linkLabel, PathHelpers.CleanNavigateUrl(agentPolicyLink), eventCode,
                    logInfo, null, new {target = "_blank", @class = "nhs_BuilderSiteLink"})
                : htmlHelper.GetSignInLink(linkLabel, false, "", false, false, "", "");
        }

        public static MvcHtmlString EnvisionDesignLink(this HtmlHelper helper, IDetailViewModel model)
        {
            string linkLabel;
            string eventCode;

            if (model.PlanId > 0 || model.SpecId > 0)
            {
                linkLabel = LanguageHelper.SeeDesignOptions;
                eventCode = LogImpressionConst.HomeOpenEnvision;
            }
            else
            {
                linkLabel = LanguageHelper.EnvisionNewHome;
                eventCode = LogImpressionConst.CommOpenEnvision;
            }
            if (string.IsNullOrEmpty(model.EnvisionUrl))
                return MvcHtmlString.Create(string.Empty);


            var logInfo = new
            {
                communityId = model.CommunityId.ToString(),
                communityName = model.CommunityName.RemoveSpecialCharacters(),
                builderId = model.BuilderId.ToString(),
                planId = model.PlanId.ToString(),
                specId = model.SpecId.ToString(),
                marketId = model.MarketId.ToString(),
                builderName = model.BuilderName.RemoveSpecialCharacters(),
                marketName = model.MarketName.RemoveSpecialCharacters()
            };

            return helper.NhsLinkLogAndRedirect(linkLabel, PathHelpers.CleanNavigateUrl(model.EnvisionUrl), eventCode,
                logInfo, null, new {target = "_blank", @class = "nhs_EnvisionLink"});
        }

        //TODO: Fix these suckers - repeated all over the place
        public static MvcHtmlString GetMapRequestInfoLinkCd(this HtmlHelper helper)
        {
            return helper.GetMapRequestInfoLink(Pages.CommunityDetail,
                "commDetail.get_log().logEvent('MFBROCH',commDetail.get_commId(),commDetail.get_builderId());");
        }

        //TODO: Fix these suckers - repeated all over the place
        public static MvcHtmlString GetMapRequestInfoLinkHd(this HtmlHelper helper)
        {
            return helper.GetMapRequestInfoLink(Pages.HomeDetail,
                "homeCarousel.get_log().logEvent('MFBROCH',homeCarousel.get_commId(), homeCarousel.get_builderId(), homeCarousel.get_plan(),homeCarousel.get_spec());");

        }

        //TODO: Fix these suckers - repeated all over the place
        public static MvcHtmlString GetMapRequestInfoLinkCr(this HtmlHelper helper)
        {
            var parameters = new List<RouteParam>
            {
                new RouteParam(RouteParams.LogEvent, LogImpressionConst.HovFreeBrochure, RouteParamType.QueryString)
            };

            return helper.GetMapRequestInfoLink(Pages.CommunityResults,
                "commResults.get_log().logEvent('HOVFBROCH', 0, 0);", parameters);
        }

        private static MvcHtmlString GetMapRequestInfoLink(this HtmlHelper helper, string fromPage, string onclick,
            List<RouteParam> parameters = null)
        {
            var modalWidth = ModalWindowsConst.LeadsRequestInfoShortWidth;
            var modalHeight = ModalWindowsConst.LeadsRequestInfoShortHeight;

            if (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser)
            {
                modalWidth = ModalWindowsConst.LeadsRequestInfoLoggedWidth;
                modalHeight = ModalWindowsConst.LeadsRequestInfoLoggedHeight;
            }

            if (parameters == null)
                parameters = new List<RouteParam>();

            parameters.Add(new RouteParam(RouteParams.LeadType, LeadType.Community, RouteParamType.QueryString));
            parameters.Add(new RouteParam(RouteParams.LeadAction, LeadAction.FreeBrochure, RouteParamType.QueryString));
            parameters.Add(new RouteParam(RouteParams.CommunityList, "-commid-", RouteParamType.QueryString));
            parameters.Add(new RouteParam(RouteParams.Builder, "-builderid-", RouteParamType.QueryString));
            parameters.Add(new RouteParam(RouteParams.FromPage, fromPage, RouteParamType.QueryString));
            parameters.Add(new RouteParam(RouteParams.Market, "-marketid-", RouteParamType.QueryString));

            if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
            {
                return helper.NhsModalWindowLink("-text-", Pages.LeadsRequestBrochureModal, modalWidth, modalHeight,
                    parameters,
                    new
                    {
                        @class = "btnCss btn_FreeBrochureAlt",
                        onclick =
                            "$jq.googlepush('Lead Events','Search Results CTA Free Brochure','Open Form - Free Brochure');" +
                            onclick
                    }, true);
            }
            return helper.SendCustomBrochureLink(parameters, true, true,
                UserSession.UserProfile.ActorStatus == WebActors.GuestUser);
        }


        //TODO: Fix these suckers - repeated all over the place
        public static MvcHtmlString GetMapRequestPromoLinkCr(this HtmlHelper helper)
        {
            var modalWidth = ModalWindowsConst.LeadsRequestInfoShortWidth;
            var modalHeight = ModalWindowsConst.LeadsRequestInfoShortHeight;

            if (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser)
            {
                modalWidth = ModalWindowsConst.LeadsRequestInfoLoggedWidth;
                modalHeight = ModalWindowsConst.LeadsRequestInfoLoggedHeight;
            }
            return helper.NhsModalWindowLink("Request promotion and event information", Pages.LeadsRequestBrochureModal,
                modalWidth,
                modalHeight,
                new
                {
                    leadtype = LeadType.Community,
                    leadaction = LeadAction.HovRequestPromotion,
                    logevent = LogImpressionConst.HovRequestPromoInfo,
                    communitylist = "-commid-",
                    builder = "-builderid-",
                    frompage = Pages.CommunityResults,
                    market = "-marketid-"
                }, null, RouteParamType.QueryString, true);
        }

        //TODO: Fix these suckers - repeated all over the place
        public static MvcHtmlString GetMapRequestApptLink(this HtmlHelper helper)
        {
            var modalWidth = ModalWindowsConst.LeadsRequestInfoShortWidth;
            var modalHeight = ModalWindowsConst.LeadsRequestInfoShortHeight;

            if (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser)
            {
                modalWidth = ModalWindowsConst.LeadsRequestInfoLoggedWidth;
                modalHeight = ModalWindowsConst.LeadsRequestInfoLoggedHeight;
            }

            return helper.NhsModalWindowLink("Request an appointment", Pages.LeadsRequestBrochureModal,
                modalWidth,
                modalHeight,
                new
                {
                    leadtype = LeadType.Community,
                    leadaction = LeadAction.HovRequestApointment,
                    logevent = LogImpressionConst.HovRequestAppt,
                    communitylist = "-commid-",
                    builder = "-builderid-",
                    frompage = Pages.CommunityResults,
                    market = "-marketid-"
                }, null, RouteParamType.QueryString, true);
        }

        public static MvcHtmlString GetBasicListingProviderLink(this HtmlHelper htmlHelper,
            BasicHomeDetailViewModel model)
        {
            var logInfo = new
            {
                basicListingId = model.BasicListingId.ToString(),
                marketId = model.MarketId.ToString(),
                marketName = model.MarketName
            };

            return htmlHelper.NhsLinkLogAndRedirect("See Listing Website", PathHelpers.CleanNavigateUrl(model.BrokerUrl),
                LogImpressionConst.BasicListingBrokerSiteUrl, logInfo, null,
                new {title = "See Listing Website", target = "_blank", id = "nhs_BrokerUrl", rel = "nofollow"}, true);
        }

        public static MvcHtmlString GetExternalVirtualTourUrl(this HtmlHelper htmlHelper, HomeDetailViewModel model,
            string url)
        {
            var logInfo = new
            {
                communityId = model.CommunityId.ToString(),
                communityName = model.CommunityName.RemoveSpecialCharacters(),
                builderId = model.BuilderId.ToString(),
                planId = model.PlanId.ToString(),
                specId = model.SpecId.ToString(),
                marketId = model.MarketId.ToString(),
                builderName = model.BuilderName.RemoveSpecialCharacters(),
                marketName = model.MarketName.RemoveSpecialCharacters()
            };


            return htmlHelper.NhsLinkLogAndRedirect(LanguageHelper.HomeTour, PathHelpers.CleanNavigateUrl(url),
                LogImpressionConst.HomeDetailVirtualTour, logInfo,
                "$jq.googlepush('Outbound Links', 'Home - Gallery', 'Home Tour');",
                new {title = "Launch Virtual Tour", target = "_blank"});
        }

        public static MvcHtmlString GetExternalMediaUrl(this HtmlHelper htmlHelper, HomeDetailViewModel model,
            MediaPlayerObject mediaObject)
        {
            var virtualTourSrc = PartnerLayoutHelper.GetPartnerLayoutConfig().IsBrandPartnerCna
                ? GlobalResourcesMvc.CNA.images.mediaplayer.launch_vt_120x80_png
                : GlobalResourcesMvc.Default.images.mediaplayer.launch_vt_120x80_png;

            var floorPlanSrc = PartnerLayoutHelper.GetPartnerLayoutConfig().IsBrandPartnerCna
                ? GlobalResourcesMvc.CNA.images.mediaplayer.launch_ifp_120x80_png
                : GlobalResourcesMvc.Default.images.mediaplayer.launch_ifp_120x80_png;

            if (mediaObject.SubType == MediaPlayerObjectTypes.SubTypes.VirtualTour)
                return htmlHelper.NhsImageLinkLogAndRedirect(LanguageHelper.LaunchVirtualTour,
                    virtualTourSrc,
                    PathHelpers.CleanNavigateUrl(mediaObject.Url), LogImpressionConst.HomeDetailVirtualTour,
                    new
                    {
                        communityId = model.CommunityId.ToString(),
                        builderId = model.BuilderId.ToString(),
                        planId = model.PlanId.ToString(),
                        specId = model.SpecId.ToString(),
                        marketId = model.MarketId.ToString()
                    }, null, new {target = "_blank"});

            return htmlHelper.NhsImageLinkLogAndRedirect(LanguageHelper.LaunchInteractiveFloorPlanViewer,
                floorPlanSrc,
                PathHelpers.CleanNavigateUrl(mediaObject.Url), LogImpressionConst.HomeDetailInteractiveFloorPlan,
                new
                {
                    communityId = model.CommunityId.ToString(),
                    builderId = model.BuilderId.ToString(),
                    planId = model.PlanId.ToString(),
                    specId = model.SpecId.ToString(),
                    marketId = model.MarketId.ToString()
                }, null, new {target = "_blank"});
        }

        public static MvcHtmlString GetFeaturedListingImageLink(this HtmlHelper htmlHelper,
            ExtendedCommunityResult result)
        {

            if (string.IsNullOrEmpty(result.BuilderUrl))
                return htmlHelper.NhsImageLink(result.FeaturedListingThumbnail(),
                    RedirectionHelper.GetCommunityDetailPage(), result.ToCommunityDetail(),
                    new {title = result.GetAlternateTextForFeaturedListing(), target = "_self"},
                    new {title = result.GetAlternateTextForFeaturedListing()}, false);

            return htmlHelper.NhsImageLinkLogAndRedirect(result.GetAlternateTextForFeaturedListing(),
                result.FeaturedListingThumbnail(), PathHelpers.CleanNavigateUrl(result.BuilderUrl),
                LogImpressionConst.FeaturedListingClick,
                new
                {
                    communityId = result.CommunityId.ToString(),
                    builderId = result.BuilderId.ToString(),
                    featuredListingId = result.FeaturedListingId.ToString(),
                    marketId = result.MarketId.ToString()
                }, null, new {target = "_blank"}, false, true,
                new {itemprop = "contentUrl"});
        }

        public static MvcHtmlString GetFeaturedListingLink(this HtmlHelper htmlHelper, ExtendedCommunityResult result,
            string linkName)
        {
            if (string.IsNullOrEmpty(result.BuilderUrl))
                return htmlHelper.NhsLink(linkName, RedirectionHelper.GetCommunityDetailPage(),
                    result.ToCommunityDetail(),
                    new
                    {
                        title = result.GetAlternateTextForFeaturedListing(),
                        target = "_self",
                        @class = (linkName == result.CommunityName ? "" : "btnCss btn_GetMoreInfo"),
                        @id = "nhs_CommResultsItemLink" + result.CommunityId
                    }, false);

            var logInfo = new
            {
                communityId = result.CommunityId.ToString(),
                communityName = result.CommunityName.RemoveSpecialCharacters(),
                builderId = result.BuilderId.ToString(),
                featuredListingId = result.FeaturedListingId.ToString(),
                marketId = result.MarketId.ToString(),
                builderName = result.BrandName.RemoveSpecialCharacters(),
                marketName = result.MarketName.RemoveSpecialCharacters()
            };


            return htmlHelper.NhsLinkLogAndRedirect(linkName, PathHelpers.CleanNavigateUrl(result.BuilderUrl),
                LogImpressionConst.FeaturedListingClick, logInfo, null,
                new {target = "_blank", @class = (linkName == result.CommunityName ? "" : "btnCss btn_GetMoreInfo")},
                false, true);
        }

        public static MvcHtmlString GetFeaturedListingLink(this HtmlHelper htmlHelper, Community result, string linkName,
            int googleAnalyticsIndex)
        {

            if (result.FeaturedListingId != 0 || !string.IsNullOrEmpty(result.Builder.Url))
            {
                var logInfo = new
                {
                    communityId = result.CommunityId.ToString(),
                    communityName = result.CommunityName.RemoveSpecialCharacters(),
                    builderId = result.BuilderId.ToString(),
                    featuredListingId = result.FeaturedListingId.ToString(),
                    marketId = result.MarketId.ToString(),
                };

                return htmlHelper.NhsLinkLogAndRedirect(linkName, PathHelpers.CleanNavigateUrl(result.Builder.Url),
                    LogImpressionConst.FeaturedListingClick, logInfo,
                    string.Format("$jq.googlepush('Site Links', 'Featured', 'Place {0}');", googleAnalyticsIndex),
                    new {target = "_blank", @class = (linkName == result.CommunityName ? "" : "btnCss btn_GetMoreInfo")},
                    false, true);
            }
            return htmlHelper.NhsLink(linkName, RedirectionHelper.GetCommunityDetailPage(), result.ToCommunityDetail(),
                new
                {
                    target = "_self",
                    onclick =
                        string.Format("$jq.googlepush('Site Links', 'Featured', 'Place {0}');", googleAnalyticsIndex),
                    @class = (linkName == result.CommunityName ? "" : "btnCss btn_GetMoreInfo")
                },
                false, true);
        }

        public static MvcHtmlString GetFeaturedListingImageLogOnClick(this HtmlHelper htmlHelper, Community result,
            int googleAnalyticsIndex)
        {

            if (result.FeaturedListingId != 0 || !string.IsNullOrEmpty(result.Builder.Url))
            {
                return htmlHelper.CreateOnClickLogEvent(
                    PathHelpers.CleanNavigateUrl(result.Builder.Url),
                    LogImpressionConst.FeaturedListingClick,
                    result.CommunityId.ToString(CultureInfo.InvariantCulture),
                    result.BuilderId.ToString(CultureInfo.InvariantCulture),
                    result.FeaturedListingId.ToString(CultureInfo.InvariantCulture),
                    "0",
                    result.MarketId.ToString(CultureInfo.InvariantCulture),
                    string.Format("$jq.googlepush('Site Links', 'Featured', 'Place {0}');", googleAnalyticsIndex),
                    new
                    {
                        target = "_blank",
                    }, false, true);
            }
            var linkUrl =
                NhsUrlHelper.ResolveServerUrl(
                    result.ToCommunityDetail().ToUrl(RedirectionHelper.GetCommunityDetailPage(), false));
            return htmlHelper.CreateOnClickLogEvent(
                linkUrl, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                string.Format("$jq.googlepush('Site Links', 'Featured', 'Place {0}');", googleAnalyticsIndex),
                new
                {
                    target = "_self",
                }, false, true, false);
        }

        public static MvcHtmlString GetAwardImageLink(this HtmlHelper htmlHelper, IImage image)
        {
            var imgSrc = string.Format("{0}{1}{2}_{3}?maxwidth=90&maxheight=70&format=jpg", Configuration.IRSDomain,
                image.ImagePath,
                image.ImageTypeCode, image.ImageName);

            if (!string.IsNullOrEmpty(image.ClickThruUrl))
            {

                return htmlHelper.NhsImageLinkLogAndRedirect(image.ImageDescription, imgSrc,
                    PathHelpers.CleanNavigateUrl(image.ClickThruUrl), LogImpressionConst.AwardsClickThrough,
                    new {communityId = image.CommunityId.ToString(), builderId = image.BuilderId.ToString()}, null,
                    new {target = "_blank"});
            }

            return htmlHelper.NhsImage(imgSrc, image.ImageTitle, false);
        }

        public static MvcHtmlString GetSavePlannerLink(this HtmlHelper htmlHelper, IDetailViewModel viewModel,
            bool isFromNextSteps = false, string text = "Save to favorites")
        {
            return GetSavePlannerLink(htmlHelper, text, viewModel.CommunityId, viewModel.BuilderId,
                viewModel.SavedPropertysProCrm, isFromNextSteps);
        }

        public static MvcHtmlString GetSavePlannerLink(this HtmlHelper htmlHelper, string text, int commId = 0,
            int builderId = 0, IEnumerable<PlannerListing> savedPropertysProCrm = null, bool isFromNextSteps = false)
        {

            PlannerListing pl;

            var isCommDetail = NhsRoute.CurrentRoute.Function.ToLower() == Pages.CommunityDetail.ToLower() ||
                               NhsRoute.CurrentRoute.Function.ToLower() == Pages.CommunityDetailPreview.ToLower() ||
                               NhsRoute.CurrentRoute.Function.ToLower() == Pages.CommunityDetailMove.ToLower() ||
                               NhsRoute.CurrentRoute.Function.ToLower() == Pages.CommunityDetailv1.ToLower() ||
                               NhsRoute.CurrentRoute.Function.ToLower() == Pages.CommunityDetailv2.ToLower() ||
                               NhsRoute.CurrentRoute.Function.ToLower() == Pages.CommunityResults.ToLower() ||
                               NhsRoute.CurrentRoute.Function.ToLower() == Pages.Comunidades.ToLower() ||
                               NhsRoute.CurrentRoute.Function.ToLower() == Pages.Comunidad.ToLower();
            var savedToPlanner = false;

            if (isCommDetail)
            {
                if (commId == 0)
                    commId = RouteParams.Community.Value<int>();
                if (builderId == 0)
                    builderId = RouteParams.Builder.Value<int>();
                pl = new PlannerListing(commId, builderId, ListingType.Community);
                if (NhsRoute.IsBrandPartnerNhsPro && savedPropertysProCrm != null)
                    savedToPlanner =
                        savedPropertysProCrm.Any(p => p.ListingType == pl.ListingType && p.ListingId == pl.ListingId);
                else if (UserSession.UserProfile.Planner.SavedCommunities.Contains(pl))
                    savedToPlanner = true;
            }
            else
            {
                var specId = RouteParams.SpecId.Value<int>();
                if (specId == 0)
                {
                    var planId = RouteParams.PlanId.Value<int>();
                    pl = new PlannerListing(planId, ListingType.Plan);
                    if (NhsRoute.IsBrandPartnerNhsPro && savedPropertysProCrm != null)
                        savedToPlanner =
                            savedPropertysProCrm.Any(p => p.ListingType == pl.ListingType && p.ListingId == pl.ListingId);
                    else if (UserSession.UserProfile.Planner.SavedHomes.Contains(pl))
                        savedToPlanner = true;
                }
                else
                {
                    pl = new PlannerListing(specId, ListingType.Spec);
                    if (NhsRoute.IsBrandPartnerNhsPro && savedPropertysProCrm != null)
                        savedToPlanner =
                            savedPropertysProCrm.Any(p => p.ListingType == pl.ListingType && p.ListingId == pl.ListingId);
                    else if (UserSession.UserProfile.Planner.SavedHomes.Contains(pl))
                        savedToPlanner = true;
                }
            }


            // Sets link properties
            Dictionary<string, object> parameters;
            if (isFromNextSteps)
            {
                // Case when the user already added this to favorites
                if (savedToPlanner && UserSession.UserProfile.ActorStatus != WebActors.GuestUser &&
                    NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
                    return
                        MvcHtmlString.Create("<span id=\"nhs_NextStepsSaveThisItem\" class=\"nhs_ItemSaved\">" +
                                             LanguageHelper.SavedToYourProfile + "</span>");

                string onClickCalls = isCommDetail
                    ? "commDetail.NextStepsSaveCommunity(); $jq.googlepush('Community Detail Events','Community - Next Steps','Save to favorites'); return false;"
                    : "homeDetail.NextStepsSaveHome(); $jq.googlepush('Home Detail Events','Home - Next Steps','Save to favorites'); return false;";

                parameters = new Dictionary<string, object>
                {
                    {"id", "nhs_NextStepsSaveThisItem"},
                    {"onclick", NhsRoute.IsBrandPartnerNhsPro ? "" : onClickCalls},
                    {"class", (savedToPlanner ? "pro_SavedProperty" : "")},
                    {
                        "title",
                        NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>()
                            ? ""
                            : "Save this listing as a client favorite?"
                    }
                };
            }
            else
            {
                if (savedToPlanner && UserSession.UserProfile.ActorStatus != WebActors.GuestUser &&
                    NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
                {
                    return MvcHtmlString.Create("<span id=\"nhs_SaveThisItem\">" + LanguageHelper.Saved + "</span>");
                }

                string onClickCalls = isCommDetail
                    ? "commDetail.SaveCommunity(); $jq.googlepush('Community Detail Events', 'Community - Gallery', 'Save to favorites'); return false;"
                    : "homeDetail.SaveHome(); $jq.googlepush('Home Detail Events', 'Home - Gallery', 'Save to favorites'); return false;";
                
                parameters = new Dictionary<string, object>
                {
                    {"id", "nhs_SaveThisItem"},
                    {"onclick", NhsRoute.IsBrandPartnerNhsPro ? "" : onClickCalls},
                    {"class", (savedToPlanner ? "pro_SavedProperty" : "")},
                    {
                        "title",
                        NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>()
                            ? ""
                            : "Save this listing as a client favorite?"
                    }
                };
            }

            if (NhsRoute.CurrentRoute.Function.ToLower() == Pages.CommunityResults.ToLower() &&
                NhsRoute.IsBrandPartnerNhsPro)
            {

                parameters = new Dictionary<string, object>
                {
                    {"id", "nhs_SaveThisItem_" + commId},
                    {
                        "onclick", NhsRoute.IsBrandPartnerNhsPro
                            ? ""
                            : NhsRoute.IsBrandPartnerNhsPro
                                ? ""
                                : string.Format(
                                    "commResults.SaveCommunity({0},{1}); $jq.googlepush('Community Results Events', 'Community Item', 'Save to favorites');NHS.Scripts.Helper.stopBubble(event); return false;",
                                    commId, builderId)
                    },
                    {"class", "pro_SaveFavs " + (savedToPlanner ? "pro_SavedProperty" : "")},
                    {
                        "title",
                        NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>()
                            ? ""
                            : "Save this listing as a client favorite?"
                    }
                };
            }

            if (NhsRoute.CurrentRoute.Function.ToLower() != Pages.CommunityResults.ToLower() &&
                NhsRoute.IsBrandPartnerNhsPro && savedToPlanner)
            {
                text = "Save to favorites";
            }

            // Case where the site is Pro and the user is not logged in, in this case the link has to contain
            // the required information in order to perform the save to favorites action after the log in.
            if (NhsRoute.IsBrandPartnerNhsPro &&
                UserSession.UserProfile.ActorStatus != WebActors.ActiveUser)
            {
                // I have to create the sign in link and add the required parameters in order to execute the Save to Favorites action after the sign in
                var isCommunityResultsPage = NhsRoute.CurrentRoute.Function.ToLower() == Pages.CommunityResults.ToLower();
                var paramsList = new List<RouteParam>
                {
                    new RouteParam(RouteParams.ListingType, pl.ListingType.Stringify(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.CommunityId, commId,RouteParamType.QueryString),
                    new RouteParam(RouteParams.BuilderId, builderId,RouteParamType.QueryString),
                    new RouteParam(RouteParams.PropertyId, pl.ListingId,RouteParamType.QueryString),
                    new RouteParam(RouteParams.IsNextSteps, Boolean.FalseString, RouteParamType.QueryString),
                    new RouteParam(RouteParams.IsCommunityResultsPage, isCommunityResultsPage,RouteParamType.QueryString),
                    new RouteParam(RouteParams.ToPage, Pages.GetSavetoFavorites, RouteParamType.QueryString),
                    new RouteParam(RouteParams.NextWidth, ModalWindowsConst.ProCrmFavoriteWidth, RouteParamType.QueryString),
                    new RouteParam(RouteParams.NextHeight, ModalWindowsConst.ProCrmFavoriteHeight, RouteParamType.QueryString)
                };

                var link = htmlHelper.NhsModalWindowLink(text, Pages.LoginModal, ModalWindowsConst.SignInModalWidth,
                    ModalWindowsConst.SignInModalHeight, paramsList,
                    new Dictionary<string, object>()
                    {
                        {
                            "onclick",
                            "$jq.googlepush('Account Events','SignIn','Open Form - Header Link');jQuery.googlepush('Search Events', 'Search Results', 'Favorite Community');jQuery.SetDataLayerPair('favoriteSaved');"
                        },
                        {"data-hidetitle", "true"}
                    });
                return link;
            }


            return NhsRoute.IsBrandPartnerNhsPro
                ? htmlHelper.GetFavoritesLink(text, pl.ListingType, pl.ListingId, commId, builderId, parameters, false,
                    NhsRoute.CurrentRoute.Function.ToLower() ==
                    Pages.CommunityResults.ToLower())
                : htmlHelper.NhsLink(text, Pages.RecentItems, new List<RouteParam>(), parameters);
        }

        public static MvcHtmlString GetCrunchedResource(this HtmlHelper helper, ResourceCombinerEnum resourceSetName)
        {
            return resourceSetName.GetCrunchedResource();
        }

        public static MvcHtmlString RenderBuilderLogoLink(this HtmlHelper helper, IDetailViewModel model,
            bool isFooterLink)
        {
            return RenderBuilderLogoLink(helper, model, isFooterLink, false);
        }

        private static List<RouteParam> GetParamsMortgage(MortgageViewModel model)
        {
            var paramz = model.IsCommunity ? model.ToCommunityDetail() : model.ToHomeDetail();
            return paramz;
        }

        public static MvcHtmlString RenderImageMortgage(this HtmlHelper helper, MortgageViewModel model)
        {
            var @params = GetParamsMortgage(model);
            var logPage = (!model.IsCommunity)
                ? RedirectionHelper.GetHomeDetailPage(model.IsSpec)
                : RedirectionHelper.GetCommunityDetailPage();
            return helper.NhsImageLink(model.GetImage, logPage, @params, new {title = model.Name},
                new {alt = model.Name}, false, false, true);
        }

        public static MvcHtmlString LinkNameMortgage(this HtmlHelper helper, MortgageViewModel model)
        {
            var @params = GetParamsMortgage(model);
            var logPage = (!model.IsCommunity)
                ? RedirectionHelper.GetHomeDetailPage(model.IsSpec)
                : RedirectionHelper.GetCommunityDetailPage();
            return helper.NhsLink(model.Name, logPage, @params, new {title = model.Name}, true);
        }

        public static MvcHtmlString LinkReturnMortgage(this HtmlHelper helper, MortgageViewModel model)
        {
            var @params = GetParamsMortgage(model);
            var logPage = (!model.IsCommunity)
                ? RedirectionHelper.GetHomeDetailPage(model.IsSpec)
                : RedirectionHelper.GetCommunityDetailPage();
            return helper.NhsLink("Return to view homes", logPage, @params, new {title = "Return to view homes"}, true);
        }

        public static MvcHtmlString RenderBuilderLogoLink(this HtmlHelper helper, IDetailViewModel model,
            bool isFooterLink, bool isNextSteps)
        {
            if (string.IsNullOrEmpty(model.BuilderUrl) ||
                ((BaseViewModel) model).Globals.PartnerLayoutConfig.IsPartnerRealtor)
            {
                if (string.IsNullOrEmpty(model.BuilderLogo)) return MvcHtmlString.Empty;

                return helper.NhsImage(model.BuilderLogo, model.BuilderName, new {title = model.BrandName}, true);
            }

            var logoClickEvent = model.PreviewMode
                ? LogImpressionConst.BuilderLogoPreviewMode
                : LogImpressionConst.BuilderLogo;
            var logEvent = isFooterLink
                ? LogImpressionConst.FooterBuilderLogo
                : isNextSteps
                    ? (model.PlanId > 0 || model.SpecId > 0
                        ? LogImpressionConst.NextStepsClickLogo
                        : LogImpressionConst.NextStepsComClickWebsite)
                    : logoClickEvent;

            var tooltip = string.Format("View {0}'s site in new window", model.BuilderName);

            var @params = new List<RouteParam>();
            @params.Add(new RouteParam(RouteParams.ExternalUrl, HttpUtility.UrlEncode(model.BuilderUrl),
                RouteParamType.QueryString));
            @params.Add(new RouteParam(RouteParams.LogEvent, logEvent));

            string logo = string.Concat(Configuration.IRSDomain, model.BuilderLogo);

            var logInfo = new
            {
                communityId = model.CommunityId.ToString(),
                communityName = model.CommunityName.RemoveSpecialCharacters(),
                builderId = model.BuilderId.ToString(),
                builderName = model.BuilderName.RemoveSpecialCharacters(),
                planId = model.PlanId.ToString(),
                specId = model.SpecId.ToString(),
                marketId = model.MarketId.ToString(),
                marketName = model.MarketName.RemoveSpecialCharacters()
            };

            if (isNextSteps)
            {
                logo = string.Concat(Configuration.IRSDomain, model.BuilderLogoSmall);
            }

            if (string.IsNullOrEmpty(model.BuilderLogo) || model.BuilderLogo.IndexOf("1x1.gif") != -1)
            {


                return helper.NhsLinkLogAndRedirect(string.Concat("Visit ", model.BrandName),
                    PathHelpers.CleanNavigateUrl(model.BuilderUrl), logEvent, logInfo,
                    "jQuery.SetDataLayerPair('siteClickthrough');", new {target = "_blank", title = tooltip});
            }


            string gaPush =
                string.Format(
                    "$jq.googlepush('Outbound Links','{0} - {1}','Builder Logo');jQuery.SetDataLayerPair('siteClickthrough');",
                    (model.SpecId > 0 || model.PlanId > 0) ? "Home" : "Community",
                    isNextSteps ? "Next Steps" : "Detail");

            return helper.NhsImageLinkLogAndRedirect(tooltip, logo, PathHelpers.CleanNavigateUrl(model.BuilderUrl),
                logEvent, logInfo, gaPush,
                new
                {
                    target = "_blank",
                    @class =
                        ("nhs_DetailsFormLogoLink" + (model.IsPageCommDetail ? "Comm" : "Home") +
                         (isNextSteps ? "Bottom" : "Top"))
                }, false, false,
                new
                {
                    @class =
                        ("nhs_DetailsFormLogoImg" + (model.IsPageCommDetail ? "Comm" : "Home") +
                         (isNextSteps ? "Bottom" : "Top"))
                });
        }

        public static MvcHtmlString ViewBrochureLink(this HtmlHelper helper, IDetailViewModel model)
        {
            return ViewBrochureLink(helper, model.SpecId, model.PlanId, model.BrochureParams, model.NonPdfBrochureUrl);
        }

        public static MvcHtmlString SendCustomBrochureLink(this HtmlHelper helper, IDetailViewModel model)
        {
            var @params = model.BrochureParams;

            if (model.PlanId > 0)
                @params.Add(new RouteParam(RouteParams.PlanId, model.PlanId.ToString(CultureInfo.InvariantCulture)));

            if (model.SpecId > 0)
                @params.Add(new RouteParam(RouteParams.SpecId, model.SpecId.ToString(CultureInfo.InvariantCulture)));

            @params.Add(new RouteParam(RouteParams.IsBilled, model.IsBilled.ToType<String>()));
            return SendCustomBrochureLink(helper, @params);
        }

        public static MvcHtmlString ShowAgentPolicyLink(this HtmlHelper helper, ISecondaryInfoViewModel model,
            bool button = true, bool usedSmallOne = false, bool addLock = false, string linkText = "View agent policy",
            string @class = "", string style = "")
        {

            /**/
            string agentPolicyLink = model.AgentPolicyLink;
            if (string.IsNullOrEmpty(agentPolicyLink))
            {
                if (string.IsNullOrEmpty(agentPolicyLink))
                    return MvcHtmlString.Empty;

            }


            string eventCode;

            if (model.PlanId > 0 || model.SpecId > 0)
                eventCode = model.PreviewMode
                    ? LogImpressionConst.HomeDetailBuilderPreview
                    : LogImpressionConst.HomeDetailBuilder;
            else
                eventCode = model.PreviewMode
                    ? LogImpressionConst.CommunityDetailBuilderPreview
                    : LogImpressionConst.CommunityDetailBuilder;


            if (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser)
            {
                var logInfo = new
                {
                    communityId = model.CommunityId.ToString(),
                    communityName = model.CommunityName.RemoveSpecialCharacters(),
                    builderId = model.BuilderId.ToString(),
                    builderName = model.BuilderName.RemoveSpecialCharacters(),
                    planId = model.PlanId.ToString(),
                    specId = model.SpecId.ToString(),
                    marketId = model.MarketId.ToString(),
                    marketName = model.MarketName
                };

                return helper.NhsLinkLogAndRedirect(linkText, PathHelpers.CleanNavigateUrl(agentPolicyLink), eventCode,
                    logInfo, null,
                    new
                    {
                        target = "_blank",
                        @class =
                            button ? "btn btn_FreeBrochure" + (usedSmallOne ? "Alt " : " ") + " " + @class : "" + @class,
                        style
                    });
            }

            var paramsList = new List<RouteParam>
            {
                new RouteParam(RouteParams.AgentPolicyLink, PathHelpers.CleanNavigateUrl(agentPolicyLink),
                    RouteParamType.QueryString),
                new RouteParam(RouteParams.EventCode, eventCode, RouteParamType.QueryString),
                new RouteParam(RouteParams.CommunityId, model.CommunityId.ToString(CultureInfo.InvariantCulture),
                    RouteParamType.QueryString),
                new RouteParam(RouteParams.BuilderId, model.BuilderId.ToString(CultureInfo.InvariantCulture),
                    RouteParamType.QueryString),
                new RouteParam(RouteParams.PlanId, model.PlanId.ToString(CultureInfo.InvariantCulture),
                    RouteParamType.QueryString),
                new RouteParam(RouteParams.SpecId, model.SpecId.ToString(CultureInfo.InvariantCulture),
                    RouteParamType.QueryString),
                new RouteParam(RouteParams.MarketId, model.MarketId.ToString(CultureInfo.InvariantCulture),
                    RouteParamType.QueryString),
                new RouteParam(RouteParams.Action, "AgentPolicyRequest", RouteParamType.QueryString)
            };

            // Case where I have to display the login modal and then perform the requested action
            var link = helper.NhsModalWindowLink(linkText, Pages.LoginModal, ModalWindowsConst.SignInModalWidth,
                ModalWindowsConst.SignInModalHeight, paramsList,
                new Dictionary<string, object>()
                {
                    {
                        "class",
                        button
                            ? "btn btn_FreeBrochure" + (usedSmallOne ? "Alt " : " ") + " " + @class
                            : "" + @class
                    },
                    {"onclick", "$jq.googlepush('Account Events','SignIn','Open Form - Header Link')"},
                    {"style", style},
                    {"data-hideTitle", "true"}
                });
            return addLock ? (link + @"<span class=""nhs_LockIcon lock"">&nbsp;</span>").ToMvcHtmlString() : link;
        }

        public static MvcHtmlString SendCustomBrochureLink(this HtmlHelper helper, List<RouteParam> @params,
            bool button = true, bool usedSmallOne = false,
            bool addLock = false, string linkText = "Send Brochure", string @class = "", string style = "")
        {

            if (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser)
            {
                if (@params.All(p => p.Name != RouteParams.Email))
                    @params.Add(new RouteParam(RouteParams.Email, UserSession.UserProfile.Email));
                @params.Add(new RouteParam(RouteParams.IsMvc, "true"));

                return helper.NhsModalWindowLink(linkText, Pages.SendCustomBrochure,
                    ModalWindowsConst.SendCustomBrochureWidth,
                    ModalWindowsConst.SendCustomBrochureHeight, @params,
                    new
                    {
                        @class =
                            button
                                ? "" + (usedSmallOne ? "btnCss btn_FreeBrochureAlt " : "btn btn_FreeBrochure ") + " " +
                                  @class
                                : "" + @class,
                        title = "Send Customized Brochure to Clients",
                        style
                    });
            }
            // Case where I have to  create the Login link and add the required parameters in order to perform the
            // send brochure action after the login

            // Change all the parameters to be Query String
            @params = @params.Select(param =>
            {
                param.ParamType = RouteParamType.QueryString;
                return param;
            }).ToList();

            // Remove the email parameter since at this point is always empty
            @params.Remove(@params.FirstOrDefault(param => param.Name == RouteParams.Email));

            // Add the parameters required to perform the send brochure action
            @params.Add(new RouteParam(RouteParams.ToPage, Pages.SendCustomBrochure, RouteParamType.QueryString));
            @params.Add(new RouteParam(RouteParams.NextWidth, ModalWindowsConst.SendCustomBrochureWidth,
                RouteParamType.QueryString));
            @params.Add(new RouteParam(RouteParams.NextHeight, ModalWindowsConst.SendCustomBrochureHeight,
                RouteParamType.QueryString));

            var link = helper.NhsModalWindowLink(linkText, Pages.LoginModal, ModalWindowsConst.SignInModalWidth,
                ModalWindowsConst.SignInModalHeight, @params, new Dictionary<string, object>
               
                {
                    {"class" ,button? "btn btn_FreeBrochure" + (usedSmallOne ? "Alt " : " ") + " " + @class: "" + @class},
                    {"onclick", "$jq.googlepush('Account Events','SignIn','Open Form - Header Link')"},
                    {"style",style},
                    {"data-hideTitle", "true"}
                });
            return addLock ? (link + @"<span class=""nhs_LockIcon lock"">&nbsp;</span>").ToMvcHtmlString() : link;
        }

        private static MvcHtmlString ViewBrochureLink(this HtmlHelper helper, int SpecId, int PlanId,
            List<RouteParam> brochureParams,
            string nonPdfUrl,
            string linkText = "View brochure", bool addClass = true)
        {
            var gaPush = string.Format("$jq.googlepush('Lead Events','{0} - Gallery CTA Main Form','View Brochure');",
                (SpecId > 0 || PlanId > 0) ? "Home" : "Community");

            if (!string.IsNullOrEmpty(nonPdfUrl) && NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
            {
                var paramz = new List<RouteParam>
                {
                    new RouteParam(RouteParams.EventCode, LogImpressionConst.EmailBrochureClickThrough),
                    new RouteParam(RouteParams.HUrl,
                        CryptoHelper.HashUsingAlgo(
                            HttpUtility.UrlEncode(nonPdfUrl) + ":" + LogImpressionConst.EmailBrochureClickThrough, "md5")),
                    new RouteParam(RouteParams.ExternalUrl, HttpUtility.UrlEncode(nonPdfUrl), RouteParamType.QueryString)
                };

                return helper.NhsLink(linkText, Pages.LogRedirect, paramz,
                    new
                    {
                        target = "_blank",
                        @class = addClass ? "btnCss btn_ViewBrochure" : "",
                        title = linkText
                    }, true);
            }

            return helper.NhsLink(linkText, Pages.BrochureInterstitial,
                Configuration.OnDemandBrochureGeneration
                    ? brochureParams
                    : new List<RouteParam>
                    {
                        new RouteParam(RouteParams.NextPage, brochureParams.ToUrl(Pages.BrochureGen),
                            RouteParamType.QueryString)
                    },
                new
                {
                    @class = addClass ? " btnCss btn_ViewBrochure" : "",
                    target = "_blank",
                    onclick = gaPush
                });
        }

        public static MvcHtmlString SendToFriendLink(this HtmlHelper helper, IDetailViewModel model, string text)
        {
            var @params = new List<RouteParam>();

            var logInfo =
                new
                {
                    communityId = model.CommunityId,
                    communityName = model.CommunityName,
                    builderId = model.BuilderId,
                    builderName = model.BuilderName,
                    marketId = model.MarketId,
                    marketName = model.MarketName
                };
            string constLogEvent = model.Globals.PartnerLayoutConfig.AmIOnCommunityDetailPage
                ? "commDetail.get_log().logEventWithParameters('{0}', {1}); $jq.googlepush('Community Detail Events','Community - Gallery','Send to a Friend');"
                : "homeDetail.get_log().logEventWithParameters('{0}', {1}); $jq.googlepush('Home Detail Events','Home - " +
                  (text.ToLower().Trim().Equals("send") ? "Gallery" : "Next Steps") +
                  "','Send to a Friend');";

            string logEvent = model.Globals.PartnerLayoutConfig.AmIOnCommunityDetailPage
                ? string.Format(constLogEvent, "COMSTF", logInfo.ToJson())
                : string.Format(constLogEvent, "HOMSTF", logInfo.ToJson());

            if (model.Globals.PartnerLayoutConfig.AmIOnCommunityDetailPage)
            {
                @params.Add(new RouteParam { Name = RouteParams.Community, Value = model.CommunityId.ToString(), ParamType = RouteParamType.QueryString});
                @params.Add(new RouteParam { Name = RouteParams.Builder, Value = model.BuilderId.ToString(), ParamType = RouteParamType.QueryString });
            }
            else
            {
                var specId = RouteParams.SpecId.Value<int>();

                @params.Add(specId == 0
                    ? new RouteParam { Name = RouteParams.PlanId, Value = RouteParams.PlanId.Value<string>(), ParamType = RouteParamType.QueryString }
                    : new RouteParam { Name = RouteParams.SpecId, Value = specId.ToString(), ParamType = RouteParamType.QueryString });
            }

            @params.Add(new RouteParam { Name = RouteParams.LogEvent, Value = LogImpressionConst.OpenEmailSendToFriend, ParamType = RouteParamType.QueryString });

            object attributes = new {@class = "send", onclick = logEvent};

            return helper.NhsModalWindowLink(text,
                Pages.SendToFriendModal,
                ModalWindowsConst.SendToFriendWidth,
                ModalWindowsConst.SendToFriendHeight,
                @params, attributes);
        }

        public static MvcHtmlString SendToFriendLink(this HtmlHelper helper, IDetailViewModel model)
        {
            return SendToFriendLink(helper, model, "Send");
        }

        public static MvcHtmlString ContactUsPartnerName(this HtmlHelper helper)
        {
            string partnerName = "New Home Source Professional";
            return MvcHtmlString.Create(partnerName);
        }

        public static MvcHtmlString ContactUsEmail(this HtmlHelper helper, BaseViewModel model)
        {
            string email;
            try
            {
                email = model.Globals.PartnerInfo.ContactUsEmail;
            }
            catch
            {
                email = "support@newhomesource.com";
            }
            TagBuilder link = new TagBuilder("a");
            link.MergeAttribute("href", String.Format("mailto:{0}", email));
            link.InnerHtml = email;
            return MvcHtmlString.Create(link.ToString());
        }

        public static MvcHtmlString QuestionsEmail(this HtmlHelper helper,
            SiteHelpViewModels.ContactUsViewModel contactUsViewModel)
        {
            TagBuilder link = new TagBuilder("a");
            link.MergeAttribute("href", String.Format("mailto:{0}", contactUsViewModel.ContactUsEmail));
            link.InnerHtml = contactUsViewModel.ContactUsEmail;
            return MvcHtmlString.Create(link.ToString());
        }

        public static bool ShowBuilderLogo(this IDetailViewModel model)
        {
            if (string.IsNullOrEmpty(model.BuilderUrl) && string.IsNullOrEmpty(model.BuilderLogo)) return false;
            if (model.BuilderLogo.IndexOf("1x1.gif") != -1 && string.IsNullOrEmpty(model.BuilderUrl)) return false;
            return true;
        }

        public static MvcHtmlString MoreCommunitiesLikeThisLink(this HtmlHelper helper, IDetailViewModel model,
            string linkText)
        {
            var validPrices = new List<int>
            {
                100000,
                110000,
                120000,
                130000,
                140000,
                150000,
                160000,
                170000,
                180000,
                190000,
                200000,
                220000,
                240000,
                260000,
                280000,
                300000,
                325000,
                350000,
                375000,
                400000,
                425000,
                450000,
                475000,
                500000,
                600000,
                700000,
                800000,
                900000,
                1000000
            };

            var priceLow = Convert.ToInt32(decimal.Parse(model.PriceLow));
            var priceHigh = validPrices.Find(i => i > Convert.ToInt32(decimal.Parse(model.PriceHigh)));
            var tmpPriceLow = priceLow;

            foreach (var validPrice in validPrices)
            {
                if (validPrice > priceLow && priceLow > tmpPriceLow)
                    priceLow = tmpPriceLow;
                else
                    tmpPriceLow = validPrice;
            }
            // Creates link
            var @params = new List<RouteParam>
            {
                new RouteParam
                {
                    Name = RouteParams.Market,
                    Value = model.MarketId.ToString()
                }
            };
            if (priceLow > 0)
                @params.Add(new RouteParam
                {
                    Name = RouteParams.PriceLow,
                    Value = priceLow.ToString("#")
                });
            if (priceHigh > 0)
                @params.Add(new RouteParam
                {
                    Name = RouteParams.PriceHigh,
                    Value = priceHigh.ToString("#")
                });

            return helper.NhsLink(linkText, @params.ToResultsParams(), RedirectionHelper.GetCommunityResultsPage());
        }

        public static MvcHtmlString GetSavedHomesLink(this IDetailViewModel model, HtmlHelper helper)
        {
            return !model.NewUser
                ? MvcHtmlString.Empty
                : helper.NhsLink(LanguageHelper.GoToMySavedProperties, Pages.SavedHomes);
        }

        public static string GetMortgageMatchLink(this IDetailViewModel model)
        {
            var pcid = "";
            switch (NhsRoute.CurrentRoute.Function.ToLower())
            {
                case Pages.BasicCommunity:
                case Pages.CommunityDetail:
                case Pages.CommunityDetailv1:
                    pcid += @"bdx:mnh:cd";
                    break;
                case Pages.HomeDetail:
                case Pages.HomeDetailv1:
                case Pages.HomeDetailv2:
                case Pages.HomeDetailv3:
                case Pages.HomeDetailv4:
                    pcid += @"bdx:mnh:hd";
                    break;
            }

            return @"https://prequalplus.move.com/landing/prequalify_hassle_free/default.htm?iid=" + pcid;
        }

        public static MvcHtmlString MortgageCalculatorPage(this HtmlHelper htmlHelper, IDetailViewModel model)
        {
            if (string.IsNullOrWhiteSpace(model.AffiliateLinksData.CalculateMortagePaymentsNexStepsArea) == false)
            {
                return model.AffiliateLinksData.CalculateMortagePaymentsNexStepsArea.ToMvcHtmlString();
            }

            var paramz = new List<RouteParam>();
            if (model.Globals.PartnerLayoutConfig.AmIOnCommunityDetailPage)
                paramz.Add(new RouteParam(RouteParams.Community, model.CommunityId));
            else
                paramz.Add(model.IsPlan
                    ? new RouteParam(RouteParams.PlanId, model.PlanId)
                    : new RouteParam(RouteParams.SpecId, model.SpecId));
            return htmlHelper.NhsLink("Calculate mortgage payments", Pages.MortgagePayments, paramz, null);
        }

        public static string GoogleAnalyticsEventTracking(this IDetailViewModel model)
        {
            var category = "";
            var action = "";
            switch (NhsRoute.CurrentRoute.Function.ToLower())
            {
                case Pages.BasicCommunity:
                case Pages.CommunityDetail:
                case Pages.CommunityDetailPreview:
                case Pages.CommunityDetailMove:
                case Pages.CommunityDetailv1:
                    category = "Community Detail Events";
                    action = "Community - Gallery";
                    break;
                case Pages.HomeDetail:
                case Pages.HomeDetailPreview:
                case Pages.PlanDetailMove:
                case Pages.SpecDetailMove:
                case Pages.HomeDetailv1:
                case Pages.HomeDetailv2:
                case Pages.HomeDetailv3:
                case Pages.HomeDetailv4:
                    category = "Home Detail Events";
                    action = "Home - Gallery";
                    break;
            }

            var onClick = String.Format("$jq.googlepush('{0}', '{1}', 'Get Prequalified Today');", category, action);
            return onClick;
        }

        public static string GetLogAndRedirectUrl(this IEnumerable<RouteParam> @params, string pageName,
            string eventCode)
        {
            var redirectParams = new List<RouteParam>();
            var redirectUrl = @params.ToUrl(pageName);
            redirectUrl = ResolveAbsoluteUrl(redirectUrl);

            redirectParams.Add(new RouteParam(RouteParams.LogEvent, eventCode));
            redirectParams.Add(new RouteParam(RouteParams.ExternalUrl, HttpUtility.UrlEncode(redirectUrl),
                RouteParamType.QueryString));
            return redirectParams.ToUrl(Pages.LogRedirect);
        }

        public static bool GetHomesGalleryPager(this CommunityDetailViewModel model)
        {
            if (model.SelectedTab == CommunityDetailTabs.AllCommunities)
                return model.AllNewHomesCount > 40;
            if (model.SelectedTab == CommunityDetailTabs.QuickMoveIn)
                return model.QuickMoveInCount > 40;

            return model.FilteredHomesCount > 40;
        }

        public static bool GetHomesGalleryLink(this CommunityDetailViewModel model)
        {
            var homeCount = NhsRoute.CurrentRoute.Function.ToLower().Equals(Pages.CommunityDetailv1) ? 9 : 12;
            if (model.SelectedTab == CommunityDetailTabs.AllCommunities)
                return model.AllNewHomesCount > homeCount;
            if (model.SelectedTab == CommunityDetailTabs.QuickMoveIn)
                return model.QuickMoveInCount > homeCount;

            return model.FilteredHomesCount > homeCount;
        }

        //NOTE: This is a temp method for internal redirections and uses soon-to-be legacy NhsUrl functionality. Please refactor once we are 100pc MVC.
        private static string ResolveAbsoluteUrl(string relativePath)
        {
            var nUrl = NhsUrlHelper.GetFriendlyUrl();
            var siteurl = relativePath.StartsWith("/" + RewriterConfiguration.CurrentPartnerSiteUrl)
                ? nUrl.DomainInfo
                : nUrl.SiteRootWithDomainInfo;

            //cleanup shit. Usage for this method needs to be refactored.
            relativePath = relativePath
                .Replace("~/", string.Empty) //remove all ~/
                .Replace(siteurl, string.Empty); //remove domainname and partnersiteurl

            if (!string.IsNullOrEmpty(RewriterConfiguration.CurrentPartnerSiteUrl) &&
                relativePath.StartsWith(RewriterConfiguration.CurrentPartnerSiteUrl))
                relativePath = relativePath.Remove(0, RewriterConfiguration.CurrentPartnerSiteUrl.Length);
            //remove partnersiteurl if no domainsite

            return siteurl.TrimEnd('/') + "/" + new Regex("/+").Replace(relativePath, "/").TrimStart('/'); //cleanup /
        }

        public static MvcHtmlString GetMovePartnerLogoLink(this HtmlHelper htmlHelper)
        {
            return htmlHelper.NhsImageLink(GlobalResourcesMvc.Move.images.move_new_homes_png, string.Empty,
                new List<RouteParam>(),
                new
                {
                    title = "Move.com&reg; - The leader in online real estate",
                    accesskey = "1",
                    onclick = "$jq.googlepush('Site Links','Navigation ','NHS Logo');"
                },
                new
                {
                    @class = "nhsLogo",
                    border = "0",
                    alt = "Move.com&reg; - The leader in online real estate"
                });
        }

        public static MvcHtmlString GetProPartnerHeader(this HtmlHelper htmlHelper, BaseViewModel model)
        {
            var shell = model.Globals.Shell;
            return MvcHtmlString.Create(shell != null ? shell.Head.InnerMarkup : string.Empty);
        }

        public static MvcHtmlString GetProPartnerLogoLink(this HtmlHelper htmlHelper, string logoPath,
            BaseViewModel model)
        {
            var imagePath = model.Globals.PartnerLayoutConfig.PartnerIsABrand
                ? GlobalResourcesMvc.Pro.images.logo_png
                : logoPath.Replace("[resource:]", Configuration.ResourceDomain);

            return htmlHelper.NhsImageLink(imagePath, Pages.Home,
                new List<RouteParam>(),
                new
                {
                    title = "New Home Source Professional",
                    accesskey = "1",
                    onclick = "$jq.googlepush('Site Links','Navigation ','Pro7 Logo');",
                    @class = "pro_Logo"
                },
                new
                {
                    border = "0",
                    alt = "New Home Source Professional"
                });
        }

        public static MvcHtmlString LeadEmailTemplateDetailLink(this HtmlHelper helper, LeadEmailProperty propertyItem)
        {
            return new MvcHtmlString(string.Empty);
        }

        public static MvcHtmlString LeadEmailTemplateBrochureLink(this HtmlHelper helper, LeadEmailProperty propertyItem)
        {
            return new MvcHtmlString(string.Empty);
        }

        public static string PreviewBrochure(this HtmlHelper helper, LeadViewModels.SendCustomBrochure model)
        {
            var @params = new List<RouteParam>();
            var useBrochureVer6 = Configuration.UseBrochureVer6;
            if (useBrochureVer6)
                @params.Add(new RouteParam(RouteParams.Brochure, "true", RouteParamType.QueryString));

            if (model.IsCommunity)
            {
                if (useBrochureVer6)
                    @params.Add(new RouteParam(RouteParams.BuilderId, model.BuilderId.ToType<string>(),
                        RouteParamType.QueryString));

                @params.Add(new RouteParam(RouteParams.CommunityId, model.CommunityId.ToType<string>(),
                    useBrochureVer6 ? RouteParamType.QueryString : RouteParamType.Friendly));
            }
            else if (model.IsPlan)
                @params.Add(new RouteParam(RouteParams.PlanId, model.PlanId.ToType<string>(),
                    useBrochureVer6 ? RouteParamType.QueryString : RouteParamType.Friendly));
            else
                @params.Add(new RouteParam(RouteParams.SpecId, model.SpecId.ToType<string>(),
                    useBrochureVer6 ? RouteParamType.QueryString : RouteParamType.Friendly));
            @params.Add(new RouteParam(RouteParams.Email, model.MailAddress, RouteParamType.QueryString));

            return @params.ToUrl(useBrochureVer6 ? Pages.Brochure : Pages.PdfBrochure);



            //return helper.NhsLink("Preview custom brochure", useBrochureVer6 ? Pages.Brochure : Pages.PdfBrochure,
            //                      @params, new {target = "_blank", @class = "btn btn_PreviewBrochure", onclick="$jq.googlepush('Send Brochure', 'Modal', 'See a Preview');"});
        }




        public static MvcHtmlString GetPolicyLink(this HtmlHelper htmlHelper, ISecondaryInfoViewModel model,
            string linkText, string cssClass,
            bool isBTn)
        {
            return htmlHelper.ShowAgentPolicyLink(model, isBTn);
        }

        public static MvcHtmlString GetSignInLink(this HtmlHelper helper, string text, bool button = false,
            string idlink = "", bool usedSmallOne = false,
            bool addLock = false, string @class = "", string style = "", string @title = "")
        {
            var onclickCalls = (@title == "Save as Favorite"
                ? "$jq.googlepush('Account Events','SignIn','Open Form - Header Link');jQuery.googlepush('Search Events', 'Search Results', 'Favorite Community');jQuery.SetDataLayerPair('favoriteSaved');"
                : "$jq.googlepush('Account Events','SignIn','Open Form - Header Link')");

            var link = helper.NhsModalWindowLink(text, Pages.LoginModal, ModalWindowsConst.SignInModalWidth,
                ModalWindowsConst.SignInModalHeight, new List<RouteParam>(),
                new Dictionary<string, object>
                {
                    {
                        "class",
                        button ? "btn btn_FreeBrochure" + (usedSmallOne ? "Alt " : " ") + " " + @class : "" + @class
                    },
                    {"onclick", onclickCalls},
                    {"id", idlink},
                    {"title", @title},
                    {"style", style},
                    {"data-hidetitle", "true"}
                });
            return addLock ? (link + @"<span class=""nhs_LockIcon lock"">&nbsp;</span>").ToMvcHtmlString() : link;
        }

        public static MvcHtmlString BoylLinkResult(this HtmlHelper htmlHelper, FindCustomBuilders model)
        {
            var textToDisplay = model.BuilderCount == 1
                ? LanguageHelper.BoylResultLinkSingular
                : LanguageHelper.BoylResultLink;
            return htmlHelper.NhsLink(string.Format(textToDisplay, model.BuilderCount), Pages.BOYLResults,
                new List<RouteParam>
                {
                    new RouteParam(RouteParams.Market, model.MarketId.ToString())
                },
                new
                {
                    @class = "btnCss btn_SeeCustomListings",
                    onclick =
                        "if(logger) { logger.logEventWithParameters('CRBOYL', {async : false}); }$jq.googlepush('Lead Events', 'Search Results CTA Custom Builder Listings', 'Open Form – Custom Builder Listings')"
                });

        }

        public static List<RouteParam> IncludeUtmParams(this List<RouteParam> parameters, string source, string medium,
            string term, string content, string campain)
        {
            if (parameters == null)
                parameters = new List<RouteParam>();

            parameters.Add(new RouteParam(RouteParams.utm_source, source, RouteParamType.QueryString));
            parameters.Add(new RouteParam(RouteParams.utm_medium, medium, RouteParamType.QueryString));
            parameters.Add(new RouteParam(RouteParams.utm_term, term, RouteParamType.QueryString));
            parameters.Add(new RouteParam(RouteParams.utm_content, content, RouteParamType.QueryString));
            parameters.Add(new RouteParam(RouteParams.utm_campaign, campain, RouteParamType.QueryString));

            return parameters;
        }
    }
}
