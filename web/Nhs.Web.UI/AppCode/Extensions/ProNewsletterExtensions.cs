﻿using System.Collections.Generic;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Web.UI.AppCode.Helper;

namespace Nhs.Web.UI.AppCode.Extensions
{
    public static class ProNewsletterExtensions
    {
        public static List<RouteParam> ParameterNewLetterPromotions(this List<RouteParam> parameters)
        {
            parameters.Add(NhsRoute.IsPartnerNhsPro
                ? new RouteParam(RouteParams.PromotionType, "both", RouteParamType.QueryString)
                : new RouteParam(RouteParams.HotDeals, "true", RouteParamType.QueryString));

            parameters = parameters.IncludeUtmParams("brochure", "email", "view all promotions", "new builder promotions", "newhomesinsider");
            return parameters;
        }
        public static List<RouteParam> ParameterNewLetterQuickMoving(this List<RouteParam> parameters)
        {
                parameters.Add(new RouteParam(RouteParams.HomeStatus, "5", RouteParamType.QueryString));
            parameters = parameters.IncludeUtmParams("brochure", "email", "view all communities", "new quick move-in homes", "newhomesinsider");

            return parameters;
        }
        public static List<RouteParam> ParametersNewLetterEvents(this List<RouteParam> parameters)
        {
            parameters.Add(new RouteParam(RouteParams.HasEvents, "true"));
            parameters = parameters.IncludeUtmParams("brochure", "email", "view all events", "agent-only events", "newhomesinsider");
            return parameters;
        }


    }
}