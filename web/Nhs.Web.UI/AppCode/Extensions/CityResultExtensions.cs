﻿using System.Collections.Generic;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Web.UI.AppCode.Helper;

namespace Nhs.Web.UI.AppCode.Extensions
{
    public static class CityResultExtensions
    {
        public static List<RouteParam> CRParams(this City city, int marketId, bool fromPageParam = false)
        {
            var paramz = new List<RouteParam>
            {
                new RouteParam(RouteParams.Market, marketId),
                new RouteParam(RouteParams.CityNameFilter, city.CityName.ToLower())
            };
           
            return paramz.ToResultsParams();
        }
    }
}