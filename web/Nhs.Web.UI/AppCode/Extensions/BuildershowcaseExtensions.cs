﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Web.UI.AppCode.ViewModels;
using StructureMap.Query;

namespace Nhs.Web.UI.AppCode.Extensions
{
    public static class BuildershowcaseExtensions
    {
        public static string GetBuilderShowCaseUrl(this HtmlHelper helper, BuilderShowCaseViewModel model, string page)
        {
            var brandName = model.BrandName.Replace(" ", "-").Replace("'", string.Empty).Replace("/", string.Empty);
            return new List<RouteParam>().ToUrl(string.Format("builder/{0}/{1}/{2}", brandName, page, model.BrandId), true);
        }
    }
}