﻿using System;
using System.Collections.Specialized;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Nhs.Mvc.Routing.Interface;

namespace Nhs.Web.UI.AppCode.Extensions
{
    public static class NhsMvcRouteUrl
    {
        public static string NhsRouteUrl(this UrlHelper helper, string routeName, object routeValues, bool persist)
        {
            RouteValueDictionary rv;

            if (routeValues is RouteValueDictionary)
                rv = (RouteValueDictionary)routeValues;
            else
                rv = new RouteValueDictionary(routeValues);

            // Build uri
            string href = GetSiteRoot(helper);

            if (!string.IsNullOrEmpty(NhsRoute.PartnerSiteUrl))
                href += "/" + NhsRoute.PartnerSiteUrl;

            href += helper.RouteUrl(routeName, rv);

            Uri url = new Uri(href);
            if (!string.IsNullOrEmpty(url.Query))
            {
                href = url.AbsolutePath;
                NameValueCollection qscoll = HttpUtility.ParseQueryString(url.Query);
                for (int i = 0; i < qscoll.Count; i++)
                {
                    if (!href.Contains(qscoll.GetKey(i) + "-") && qscoll.GetKey(i).ToLower() != "partnersiteurl")
                        href += String.Format("/{0}-{1}", qscoll.GetKey(i), qscoll.Get(i));
                }
            }

            if (persist)
            {
                foreach (var param in NhsRoute.CurrentRoute.Params)
                    href += String.Format("/{0}-{1}", param.Name, param.Value);
            }

            return href;
        }

        private static string GetSiteRoot(UrlHelper helper)
        {
            var currentUri = helper.RequestContext.HttpContext.Request.Url;
            string href = currentUri.Scheme + Uri.SchemeDelimiter + currentUri.Host;
            if (currentUri.Port > 0)
                href += ":" + currentUri.Port;
            return href;
        }
    }
}