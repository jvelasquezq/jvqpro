﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Bdx.Web.Api.Objects;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Content;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Web;
using Nhs.Library.Web.ImageResizer;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult;
using Resources;

namespace Nhs.Web.UI.AppCode.Extensions
{
    public static class HomeResultExtensionsV2
    {

        public static MvcHtmlString GetFormattedPrice(this HomeItem result)
        {
            if (result.Status.ToLower() == "m")
            {
                return new MvcHtmlString("<p>&nbsp;</p>");
            }

            return result.Price.ToType<int>() > 0 ? MvcHtmlString.Create(string.Format(@"<p>" + LanguageHelper.From.ToTitleCase() + " {0}</p>", result.Price.ToType<int>().ToString("C0"))) : null;
        }

        public static MvcHtmlString GetFormattedPrice(this HomeItemViewModel result)
        {
            return MvcHtmlString.Create(string.Format(@"{0} ", result.Price.ToType<int>().FormatPrice()));
        }

        public static MvcHtmlString GetFormattedPriceLow(this SendFriendTemplateViewModel result)
        {
            return MvcHtmlString.Create(string.Format(@"{0} ", result.Community.PriceLow.ToType<decimal>().ToType<int>().FormatPrice()));
        }


        public static MvcHtmlString GetFormattedBed(this HomeItemViewModel result)
        {
            if (result.Br > 0)
                return MvcHtmlString.Create(string.Format(@"<li>{0} " + LanguageHelper.BedroomAbbreviation + "</li>", result.Br));

            return MvcHtmlString.Create(LanguageHelper.Studio);
        }

        public static MvcHtmlString GetFormattedBath(this HomeItemViewModel result)
        {
            if (result.Ba > 0)
            {
                var bathrooms = ListingUtility.ComputeBathrooms(result.Ba, result.HBa);
                return MvcHtmlString.Create(string.Format(@"<li>{0} " + LanguageHelper.BathroomAbbreviation+"</li>", bathrooms));
            }
            return MvcHtmlString.Empty;
        }

        public static MvcHtmlString GetFormattedGarage(this HomeItemViewModel result)
        {
            if (result.Gr > 0.0m)
                return MvcHtmlString.Create(string.Format(@"<li>{0:0.##} " + LanguageHelper.GarageAbbreviation + "</li>", result.Gr));

            return MvcHtmlString.Empty;
        }

        public static MvcHtmlString GetFormattedSqFt(this HomeItemViewModel result)
        {
            if (result.Sft > 0)
                return MvcHtmlString.Create(string.Format(@"<li>{0} <abbr title=""square feet"">sq.ft.</abbr></li>", result.Sft));

            return MvcHtmlString.Empty;
        }

        public static MvcHtmlString GetFormattedListingStatus(this HomeItemViewModel result)
        {
            string status;
            switch (result.Status)
            {
                case "A":
                    status = LanguageHelper.AvailableNow;
                    break;

                case "UC":
                    status = LanguageHelper.UnderConstruction;
                    break;

                case "M":
                    status = LanguageHelper.ModelHome;
                    break;

                case "R":
                    status = LanguageHelper.ReadyToBuild;
                    break;

                default:
                    return MvcHtmlString.Empty;

            }

            return MvcHtmlString.Create(string.Format(@" <p><strong>{0}</strong></p>", status));
        }

        public static MvcHtmlString GetFormattedBrandName(this HomeItemViewModel result)
        {
            return !string.IsNullOrEmpty(result.Brand.Name)
                ? MvcHtmlString.Create(string.Format("<p>" + LanguageHelper.By + " {0}</p>", result.Brand.Name))
                : MvcHtmlString.Empty;
        }
        
        public static string ValidateHomeResThumb1(this HomeItemViewModel result)
        {
            return result.Thumb1.IsValidImage() ? result.Thumb1 :  GlobalResources14.Default.images.no_photo.no_photos_325x216_png;
        }

        public static string ValidateHomeResThumb1(this HomeItem result)
        {
            return result.Thumb1.IsValidImage() ? result.Thumb1 : GlobalResources14.Default.images.no_photo.no_photos_325x216_png;
        }
        
        public static ResizeCommands GetHomeResThumb1Size(this HomeItemViewModel result)
        {
            return new ResizeCommands { MaxWidth = 432, MaxHeight = 292, Format = ImageFormat.Jpg, Scale = ScaleCommand.None };
        }

        public static string BlParamsBasicListingHref(this HomeItemViewModel result)
        {
            return StringHelper.RemoveSpecialCharacters(String.Format("{0}-{1}-{2}-{3}",  (result.IsBasic == 0? result.Addr : result.PlanName), result.City, result.State, result.Zip).Trim()).Replace(" ", "-").Replace("--", "-");
        } 

        public static string GetHomeLinkText(this ApiHomeResultV2 result)
        {
            return !string.IsNullOrEmpty(result.Addr) ? result.PlanName + " (" + result.Addr + ")" : result.PlanName;
        }

        public static List<RouteParam> HdParams(this HomeItem result, bool useFriendlyUrl = true)
        {

            List<RouteParam> paramz;
            var partnerInfo = PartnerLayoutHelper.GetPartnerLayoutConfig();

            if (partnerInfo.IsBrandPartnerMove && NhsRoute.PartnerId == NhsRoute.BrandPartnerId && useFriendlyUrl) // not a private label site
            {
                if (result.IsSpec.ToType<bool>())
                {
                    paramz = new List<RouteParam>  
                    {   
                        new RouteParam(RouteParams.Page, Pages.SpecDetailMove),
                        new RouteParam(RouteParams.SpecId, result.HomeId),
                        new RouteParam(RouteParams.MarketName, result.MarketName.RemoveSpecialCharacters()),
                        new RouteParam(RouteParams.Name, result.PlanName.RemoveSpecialCharacters()),
                        new RouteParam(RouteParams.Address, result.Addr.RemoveSpecialCharacters()),
                        new RouteParam(RouteParams.City, result.City),
                        new RouteParam(RouteParams.State, result.State.RemoveSpecialCharacters()),
                        new RouteParam(RouteParams.PostalCode, result.Zip)
                    };
                }
                else
                {
                    paramz = new List<RouteParam>  {   
                        new RouteParam(RouteParams.Page, Pages.PlanDetailMove),
                        new RouteParam(RouteParams.PlanId, result.HomeId.ToString()),
                        new RouteParam(RouteParams.State, result.State.RemoveSpecialCharacters()),
                        new RouteParam(RouteParams.MarketName, result.MarketName.RemoveSpecialCharacters()),
                        new RouteParam(RouteParams.City, result.City),
                        new RouteParam(RouteParams.Name, result.PlanName.RemoveSpecialCharacters()),
                        new RouteParam(RouteParams.CommunityName, result.CommName)
                    };
                }
            }
            else
            {
                var paramSpecOrPlan = result.IsSpec.ToType<bool>() ? new RouteParam(RouteParams.SpecId, result.HomeId) :
                                                                     new RouteParam(RouteParams.PlanId, result.HomeId);

                paramz = new List<RouteParam>() { paramSpecOrPlan };
            }


            return paramz;
        }
    }
}
