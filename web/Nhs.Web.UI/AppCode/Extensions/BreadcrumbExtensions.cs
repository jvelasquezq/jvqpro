﻿using System.Web.Mvc;
using Nhs.Library.Constants;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Library.Common;
using Nhs.Utility.Constants;
using Nhs.Web.UI.AppCode.Helper;

namespace Nhs.Web.UI.AppCode.Extensions
{
    public static class BreadcrumbExtensions
    {                        
        /// <summary>
        /// Add the state to the Breadcrumb
        /// </summary>
        public static MvcHtmlString BreadcrumbState(this HtmlHelper helper, PartialViewModels.BreadcrumbViewModel model)
        {

            var searchParams = UserSession.PropertySearchParameters;
            string htmlTag = string.Format("<li><span>State</span> <br />{0}</li>", helper.NhsLink(model.StateName, Pages.StateIndex, new { state = model.StateName }, true)).Replace("State-", "state-");//hack to work around SEO 
            return MvcHtmlString.Create(htmlTag);
        }

        /// <summary>
        /// Add the state to the Breadcrumb
        /// </summary>
       

        public static MvcHtmlString BreadcrumbArea(this HtmlHelper helper, PartialViewModels.BreadcrumbViewModel model)
        {
            var metroAreaText =  "Metro Area";
            var breadcrumbArea = string.Format("<li class=\"nhs_noBg\"><span>{1}</span> <br /><strong>{0}</strong></li>", model.MarketName, metroAreaText);

            if (model.IsInDetailsPage || model.IsZipVisible || model.IsCountyVisible || model.IsCityVisible)
            {
                var marketName = string.Concat(model.MarketName, (NhsRoute.CurrentRoute.Function == Pages.CommunityDetailv1 ? " Area Homes" : string.Empty));
                var page = (NhsRoute.CurrentRoute.Function == Pages.HomeDetail)? Pages.HomeResults: Pages.CommunityResults;
                var link = helper.NhsLink(marketName, page, new {Market = model.MarketId});
                breadcrumbArea = string.Format("<li><span>{1}</span> <br />{0}</li>", link, metroAreaText);
            }

            return MvcHtmlString.Create(breadcrumbArea);
        }

        public static MvcHtmlString RenderZipCode(this HtmlHelper helper, PartialViewModels.BreadcrumbViewModel model)
        {
            if (!model.IsZipVisible || model.IsInDetailsPage) return MvcHtmlString.Empty;

            string htmlTag = string.Format("<li class=\"nhs_noBg\"><span>Location</span> <br /><strong>{0}</strong></li>", model.ZipCode);
            return MvcHtmlString.Create(htmlTag);
        }

        public static MvcHtmlString RenderCountyAndCityCrumb(this HtmlHelper helper, PartialViewModels.BreadcrumbViewModel model)
        {
            string countyHtmlTag = string.Empty;
            string cityHtmlTag = string.Empty;
            string communityNameHtmlTag = string.Empty;
            string listingNameHtmlTag = string.Empty;
            //var searchParams = UserSession.PropertySearchParameters;
            string cityStName = string.Format("{0}, {1}", model.CityName, model.State);

            if (model.IsInDetailsPage)
                cityStName = string.Format("{0}, {1}", model.CommunityCity, model.CommunityState);

            var locationText = "Location";

            if (model.IsCountyVisible && !model.IsInDetailsPage)
            {
                string countyInfo = string.Concat(model.CountyName, (model.CountyName.ToLower().IndexOf("county") == -1 ? " County" : string.Empty));
                if (!string.IsNullOrEmpty(countyInfo) && !string.IsNullOrEmpty(model.State))
                    countyInfo += string.Format(", {0}", model.State);
                countyHtmlTag = string.Format("<li class=\"nhs_noBg\"><span>{1}</span> <br /><strong>{0}</strong></li>", countyInfo, locationText);
            }

            if (model.IsCityVisible && model.IsInDetailsPage)
            {
                var page = (NhsRoute.CurrentRoute.Function == Pages.HomeDetail) ? Pages.HomeResults : Pages.CommunityResults;
                var nhsLink = helper.NhsLink(cityStName, page, new { Market = model.MarketId, CityNameFilter = model.CityName });
                cityHtmlTag = string.Format("<li><span>{1}</span> <br />{0}</li>", nhsLink.ToHtmlString(), locationText);
            }
            else if(model.IsCityVisible)
                cityHtmlTag = string.Format("<li class=\"nhs_noBg\"><span>{1}</span> <br /><strong>{0}</strong></li>", cityStName, locationText);
            
            if(model.IsCommunityVisible)
            {
                if (model.IsListingVisible || model.IsRecommVisible)
                    communityNameHtmlTag = string.Format("<li><span>Community</span> <br />{0}</li>", helper.NhsLink(model.CommunityName,
                                                            RedirectionHelper.GetCommunityDetailPage(),
                                                            model.ToCommunityDetail())
                                                         );
                else
                    communityNameHtmlTag = string.Format("<li class=\"nhs_noBg\"><span>Community</span> <br /><strong>{0}</strong></li>", model.CommunityName);
            }


            if (model.IsListingVisible)
            {
                int Length = 25;
                string source =model.ListingName;
                source = (source.Length > Length) ? source.Substring(0, Length) + "..." : model.ListingName;
                var spanText = "Home";
                listingNameHtmlTag = string.Format("<li class=\"nhs_noBg\"><span>{1}</span> <br /><strong>{0}</strong></li>", source, spanText);
            }
            string htmlBreadcrumb = string.Concat(countyHtmlTag, cityHtmlTag, communityNameHtmlTag, listingNameHtmlTag);
            return MvcHtmlString.Create(htmlBreadcrumb);
        }

        public static MvcHtmlString RenderRecComm(this HtmlHelper helper, PartialViewModels.BreadcrumbViewModel model)
        {
            string htmlBreadcrumb = string.Empty;

            if (model.IsRecommVisible )
            {
                if (model.Globals.PartnerLayoutConfig.IsBrandPartnerCna)
                    htmlBreadcrumb = "<li class=\"nhs_noBg\"><span>Recomendaciones de Comunidades</span></li>";
                else
                    htmlBreadcrumb = "<li class=\"nhs_noBg\"><span>Community</span><br/><strong>Recommendations</strong></li>";
            }

            return MvcHtmlString.Create(htmlBreadcrumb);
        }
    }
}
