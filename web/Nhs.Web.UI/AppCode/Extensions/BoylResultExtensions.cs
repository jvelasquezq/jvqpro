﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Content;
using Nhs.Library.Web;
using Nhs.Library.Web.ImageResizer;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Extensions
{
    public static class BoylResultExtension
    {
        /// <summary>
        /// Retrieves the community price range
        /// </summary>
        /// <param name="boylResult">Community item</param>
        /// <returns>Price range string</returns>
        public static string CommunityItemPriceRange(this BoylResult boylResult)
        {
            string format = "$###,####";
            return string.Format("from {0}-{1}", boylResult.PriceLow.ToString(format), boylResult.PriceHigh.ToString(format));
        }

        public static string ParamsBasicCommunityHref(this BoylResult result)
        {
            return StringHelper.RemoveSpecialCharacters(String.Format("{0}-{1}-{2}-{3}", result.CommunityName, result.City, result.State, result.PostalCode).Trim()).Replace(" ", "-").Replace("--", "-");
        }

        /// <summary>
        /// Retrieves the picture or video thumbnail image to display in the community item
        /// </summary>
        /// <param name="boylResult">Community item</param>
        /// <returns>Thumbnail path</returns>
        public static string CommunityItemLocation(this BoylResult boylResult)
        {
            return string.Format("{0}, {1} {2}", boylResult.City, boylResult.State, boylResult.PostalCode);
        }


        /// <summary>
        /// Retrieves the picture or video thumbnail image to display in the community item
        /// </summary>
        /// <param name="boylResult">Community item</param>
        /// <returns>Thumbnail path</returns>
        public static string CommThumbnail(this BoylResult boylResult)
        {
            var thumbnail = boylResult.CommunityImageThumbnail;
            if (thumbnail.IsValidImage())
            {
                if (boylResult.IsCnh)
                {
                    var imageName = Path.GetFileName(thumbnail);
                    if (imageName != null)
                    {
                        var newName = ImageSizes.Small + "_" + imageName.Replace(ImageSizes.Medium + "_", ImageSizes.Small + "_").Replace(ImageSizes.Results + "_", ImageSizes.Small + "_");
                        thumbnail = Configuration.CNHResourceDomain + thumbnail.Replace(imageName, newName);
                    }
                    else
                    {
                        thumbnail =  Resources.GlobalResources14.Default.images.no_photo.no_photos_180x120_png;
                    }
                }
                else
                {
                    thumbnail = ImageResizerUrlHelpers.FormatImageUrlForIrs(thumbnail);
                    thumbnail = ImageResizerUrlHelpers.BuildIrsImageUrl(thumbnail, ImageSizes.CommDetailThumb);

                    if (thumbnail.IsValidImage() == false)
                    {
                        thumbnail = Resources.GlobalResources14.Default.images.no_photo.no_photos_180x120_png;
                    }

                    //thumbnail = Configuration.ResourceDomain +
                    //thumbnail.Replace(ImageSizes.Small + "_", ImageSizes.HomeThumb + "_").Replace(ImageSizes.Results + "_", ImageSizes.CommDetailThumb + "_");
                }

            }
            else
            {
                thumbnail =  Resources.GlobalResources14.Default.images.no_photo.no_photos_180x120_png;
            }
            return thumbnail;
        }

        public static string BrandImageThumbnailUrl(this BoylResult boylResult)
        {

            var thumbnail = boylResult.BrandImageThumbnail;

            thumbnail = thumbnail.IsValidImage()
                            ? (boylResult.IsCnh ? Configuration.CNHResourceDomain : Configuration.ResourceDomain) +
                              thumbnail : string.Empty;
            return thumbnail;
        }

        public static MvcHtmlString BrandImageThumbnail(this HtmlHelper htmlHelper, BoylResult boylResult)
        {
            string title = string.Format("View {0}'s site in new window", boylResult.BrandName);
            return !boylResult.IsCnh ?
                         htmlHelper.NhsImageLinkLogAndRedirect(title, boylResult.BrandImageThumbnailUrl(), PathHelpers.CleanNavigateUrl(boylResult.BuilderUrl), LogImpressionConst.BoylResultsBuilder, new { communityId = boylResult.CommunityId.ToString(), builderId = boylResult.BuilderId.ToString(), communityName = boylResult.CommunityName, builderName = boylResult.BrandName, marketId = boylResult.MarketId, marketName = boylResult.MarketName }, "event.stopPropagation();", new { id = "lnkBrandLogo", target = "_blank" })
                       : htmlHelper.NhsImage(boylResult.BrandImageThumbnailUrl(), title, false);
        }

        public static string BrandResultOnClick(this BoylResult boylResult)
        {
            string script;
            const string logCall = "if(typeof logger != 'undefined') {{ logger.logAndRedirect(this, '{0}', '{1}', {2}, {3}, 0, 0, {4}, true); return false; }} ";
            var page = RedirectionHelper.GetCommunityDetailPage();
            if (boylResult.IsCnh)
                script = "$jq.googlepush('Outbound Links', 'Search Results CTA Internal', 'Custom New Homes'); " +
                           string.Format(logCall, PathHelpers.CleanNavigateUrl(boylResult.BrandCNHLink()).Replace("'", ""),
                                                  LogImpressionConst.BoylResultsBuilder, boylResult.CommunityId, boylResult.BuilderId, boylResult.MarketId);
            else
                script = string.Format("window.location.href='{0}'", boylResult.ToCommunityDetail().ToUrl(page).ToLower());


            return script;
        }

        public static string BrandCNHLink(this BoylResult boylResult)
        {
            string brandNameEncoded = HttpUtility.UrlEncode(boylResult.BrandName.Replace(" ", "_"));
            return string.Format("{0}/Detail/Builder/{1}/{2}/{3}", Configuration.CNHDomain, boylResult.State, boylResult.MarketName.Replace(" ", ""), brandNameEncoded);
        }


        public static List<RouteParam> VtParams(this BoylResult boylResult)
        {
            var paramz = new List<RouteParam> {
                new RouteParam(RouteParams.Community, boylResult.CommunityId.ToString()),
                new RouteParam(RouteParams.Builder, boylResult.BuilderId.ToString()),
                new RouteParam(RouteParams.MarketId, NhsUrl.GetMarketID.ToString()),
                new RouteParam(RouteParams.LogEvent, LogImpressionConst.CommResVideoTour)
            };

            return paramz;
        }

        public static string CreateAlert(this BoylResult boylResult)
        {
            var paramz = new List<RouteParam> {
                new RouteParam(RouteParams.FromPage, Pages.BOYLResults),
                new RouteParam(RouteParams.PopupWindow, true.ToString().ToLower()),                                          
                new RouteParam(RouteParams.Width, ModalWindowsConst.CreateAlertModalWidth.ToString()),                                          
                new RouteParam(RouteParams.Height, ModalWindowsConst.CreateAlertModalHeight.ToString())};

            string res = paramz.ToUrl(Pages.CreateAlert);
            return res;
        }

        public static List<RouteParam> CreateAlert2()
        {

            var paramz = new List<RouteParam> {
                new RouteParam(RouteParams.FromPage, Pages.BOYLResults),
                new RouteParam(RouteParams.PopupWindow, true.ToString().ToLower())};
            return paramz;
        }

        public static List<RouteParam> AlertParams()
        {
            List<RouteParam> @params = new List<RouteParam> {
                new RouteParam(RouteParams.FromPage, Pages.BOYLResults),
                new RouteParam(RouteParams.PopupWindow, true.ToString().ToLower())};
            return @params;
        }

        /// <summary>
        /// Returns the javascript code for the row hover map functionality
        /// </summary>
        /// <param name="boylResult">Community item</param>
        /// <returns>Map javascript code</returns>
        public static string CommunityOnMapJsClickFunction(this BoylResult boylResult)
        {
            if (boylResult.Latitude != 0)
            {
                // javascript function: ViewCommunityOnMap(cid,bid,name,cty,st,zip,img,brandimg,price,brand,matches,promoid,type,lat,lng,market)
                StringBuilder sb = new StringBuilder();

                sb.Append("if(commResults) commResults.get_map().viewCommunityOnMap(event,");
                sb.Append("&quot;" + boylResult.CommunityId + "&quot;,");
                sb.Append("&quot;" + boylResult.BuilderId + "&quot;,");
                sb.Append("&quot;" + boylResult.CommunityName.Replace("'", @"\'") + "&quot;,"); //Escape apostrophes
                sb.Append("&quot;" + boylResult.City + "&quot;,");
                sb.Append("&quot;" + boylResult.State + "&quot;,");
                sb.Append("&quot;" + boylResult.PostalCode.Trim() + "&quot;,");
                sb.Append("&quot;" + boylResult.CommunityImageThumbnail + "&quot;,");
                sb.Append("&quot;" + boylResult.BrandImageThumbnail + "&quot;,");
                sb.Append("&quot;" + String.Format("{0:$###,###}", boylResult.PriceLow) + " - " + String.Format("{0:$###,###}", boylResult.PriceHigh) + "&quot;,");
                sb.Append("&quot;" + boylResult.BrandName.Replace("'", @"\'") + "&quot;,"); //Escape apostrophes
                sb.Append("&quot;" + boylResult.MatchingHomes + "&quot;,");
                sb.Append("&quot;" + boylResult.PromoId + "&quot;,");
                sb.Append("&quot;" + boylResult.CommunityType + "&quot;,");
                sb.Append(boylResult.Latitude + ",");
                sb.Append(boylResult.Longitude + ",");
                sb.Append(boylResult.MarketId + ");");
                sb.Append("return false;");

                return sb.ToString();
            }
            return "";
        }


        /// <summary>
        /// Returns the javascript code for the [Map this] button functionality
        /// </summary>
        /// <param name="boylResult">Community item</param>
        /// <param name="show">if set to <c>true</c> [show].</param>
        /// <returns>Map javascript code</returns>
        public static string CommunityOnMapJsHoverFunction(this BoylResult boylResult, bool show)
        {
            if (boylResult.Latitude != 0)
            {
                string functionName = show ? "viewCommunityOnMapOnHover" : "hideCommunityOnMapOnHover";
                StringBuilder sb1 = new StringBuilder();
                sb1.Append(string.Format("if(commResults) commResults.get_map().{0}(event,", functionName));
                sb1.Append("&quot;" + boylResult.CommunityId + "&quot;,");
                sb1.Append("&quot;" + boylResult.BuilderId + "&quot;);");
                sb1.Append("return false;");

                return sb1.ToString();
            }

            return "";
        }

        /// <summary>
        /// Returns the [Free brochure] button Url
        /// </summary>
        /// <param name="boylResult">Community item</param>
        /// <returns>Brochure Url</returns>
        public static string FreeBrochureUrl(this BoylResult boylResult)
        {
            //OLD APPROACH
            //RequestInfoHelper.SetRequestInfoLink(lnkFreeBrochure, boylResult.MarketId);
            //lnkFreeBrochure.Parameters.Add(new Param(NhsLinkParams.LeadType, LeadType.Community, UrlParamType.Friendly));
            //lnkFreeBrochure.Parameters.Add(new Param(NhsLinkParams.LeadAction, LeadAction.FreeBrochure, UrlParamType.Friendly));
            //lnkFreeBrochure.Parameters.Add(new Param(NhsLinkParams.CommunityList, boylResult.CommunityId.ToString(), UrlParamType.Friendly));
            //lnkFreeBrochure.Parameters.Add(new Param(NhsLinkParams.Builder, boylResult.BuilderId.ToString(), UrlParamType.Friendly));
            //lnkFreeBrochure.Parameters.Add(new Param(NhsLinkParams.FromPage, Pages.boylResults, UrlParamType.Friendly));
            //lnkFreeBrochure.Parameters.Add(new Param(NhsLinkParams.Market, boylResult.MarketId.ToString(), UrlParamType.Friendly)); 

            //CHANGE TO USE A RIGHT WAY TO BUILD URLS -> PENDING
            string url = string.Format("/requestinfoshort?leadtype=com&leadaction=fb&communitylist={0}&builder={1}&frompage=boylResults&market={2}&popupwindow=false",
                boylResult.CommunityId, boylResult.BuilderId, boylResult.MarketId);

            return url;
        }
    }
}
