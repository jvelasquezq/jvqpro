﻿using System.Collections.Generic;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Library.Web.ImageResizer;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Web.UI.AppCode.HtmlHelpers;

namespace Nhs.Web.UI.AppCode.Extensions
{
    public static class BrandResultExtensions
    {
        public static List<RouteParam> CRParams(this Brand brand, int marketId)
        {
            var paramz = new List<RouteParam> {
                new RouteParam(RouteParams.Market, marketId.ToString(), RouteParamType.Friendly, true, true),
                new RouteParam(RouteParams.BrandId, brand.BrandId.ToString(), RouteParamType.Friendly, true, true)
            };
            return paramz;
        }


        public static string BrandThumbnail(this Brand brand)
        {
            if (brand == null) return string.Empty;
            return !string.IsNullOrEmpty(brand.LogoSmall) && brand.LogoSmall != "N"
                                   ? brand.LogoSmall
                                   : string.Empty;
        }

        public static string BrandThumbnailMedium(this Brand brand)
        {
            if (brand == null) return string.Empty;
            return !string.IsNullOrEmpty(brand.LogoMedium) && brand.LogoMedium != "N"
                                   ? brand.LogoMedium
                                   : string.Empty;
        }

        public static MvcHtmlString RenderAwardImage(this HtmlHelper helper, IImage image)
        {
            var imgToLoad = string.Format("{0}{1}{2}", Configuration.IRSDomain, image.ImagePath, image.ImageName);
            var linkToLoad = string.IsNullOrEmpty(image.ClickThruUrl) ? "" : image.ClickThruUrl;
            var imageTitle = string.IsNullOrEmpty(image.ImageTitle) ? "Image" : image.ImageTitle;
            var linkAttributes = new {title = imageTitle, href = "javascript:void(0)"};
                                 
            return helper.NhsImageLink(imgToLoad, linkToLoad, new List<RouteParam>(), new ResizeCommands
            {
                Width = 90, 
                Height = 70            
            },linkAttributes,new {alt = imageTitle}, false, true);
        }

        public static MvcHtmlString RenderAwardUrl(this HtmlHelper helper, IImage awardImg)
        {
            const string http = "http://";
            string textToLoad = string.IsNullOrEmpty(awardImg.ClickThruUrl) ? string.Empty : awardImg.ClickThruUrl;
            string linkToLoad = string.IsNullOrEmpty(awardImg.ClickThruUrl) ? "#" : (!awardImg.ClickThruUrl.Contains(http))
                                             ? string.Concat(http, awardImg.ClickThruUrl)
                                             : awardImg.ClickThruUrl;
            
            return (linkToLoad == "#") ? MvcHtmlString.Create(string.Empty) :
                helper.NhsLink(textToLoad, string.Empty, new List<RouteParam>(), new { href = linkToLoad, target = "_blank" }, true);            

        }


    }
}