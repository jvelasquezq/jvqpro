﻿using System;
using System.Collections.Generic;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.BHIContent;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.Extensions
{
    public static class AffiliateLinkExtensions
    {    

        public static string ToLink(this AffiliateLink affiliateLink)
        {
            var link = string.IsNullOrEmpty(affiliateLink.IconPath) ? @"<a href=""{0}"" id=""{1}"" class=""{2}"" target=""{3}"" onclick=""{5}"">{4}</a>"
                                                      : @"<a href=""{0}"" id=""{1}"" class=""{2}"" style=""background: url({6}) no-repeat left 50%;"" 
                                                             target=""{3}"" onclick=""{5}"">{4}</a>";
            return string.Format(link, affiliateLink.Href, affiliateLink.Keyword, affiliateLink.CssClass, affiliateLink.Target, affiliateLink.LinkText, affiliateLink.OnClick, affiliateLink.IconPath);
        }

        public static string ToLink(this AffiliateLink affiliateLink, IList<ContentTag> contentTags)
        {
            var link = string.IsNullOrEmpty(affiliateLink.IconPath) ? @"<a href=""{0}"" id=""{1}"" class=""{2}"" target=""{3}"" onclick=""{5}"">{4}</a>"
                                                      : @"<a href=""{0}"" id=""{1}"" class=""{2}"" style=""background: url({6}) no-repeat left 50%;"" 
                                                             target=""{3}"" onclick=""{5}"">{4}</a>";
            var html = string.Format(link, affiliateLink.Href, affiliateLink.Keyword, affiliateLink.CssClass, affiliateLink.Target, affiliateLink.LinkText, affiliateLink.OnClick, affiliateLink.IconPath);
            
            try
            {
                foreach (var contentTag in contentTags)
                {
                    var tagKey = contentTag.TagKey.ToType<string>();
                    tagKey = string.Format("[{0}]", tagKey);
                    html = html.Replace(tagKey, contentTag.TagValue);
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);
            }
            
            return html;
        }
    }
}
