﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web.Mvc;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Content;
using Nhs.Library.Helpers.Property;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Web.UI.AppCode.HtmlHelpers;

namespace Nhs.Web.UI.AppCode.Extensions
{
    public static class BreadcrumbExtensionsV2
    {
        /* 
         *  Rich snippets - Breadcrumbs
         *  url : https://support.google.com/webmasters/answer/185417
         *  ticket : #72000
         */

        #region PRIVATE

        private static bool UseRichContent
        {
            get
            {
                return false;
            }
        }

        private static string Li
        {
            get
            {
                return "<li>{0}</li>";
            }
        }

        private static string Span
        {
            get
            {
                return UseRichContent
                    ? "<span>{0}</span>"
                    : "{0}";
            }
        }

        private static MvcHtmlString BuildAnchorUrl(HtmlHelper helper, string linkText, string pageName, List<RouteParam> param, dynamic attributes,
            bool preserveCase = false, bool isAbsoluteUrl = false)
        {
            return NhsRoute.IsBrandPartnerNhsPro
                ? helper.NhsLink(linkText, pageName, param, (object)attributes, preserveCase, isAbsoluteUrl)
                : helper.NhsLinkRichBreadcrumb(linkText, pageName, param, (object)attributes, preserveCase,
                    isAbsoluteUrl);
        }

        #endregion

        public static MvcHtmlString BackToMySearchButton(this HtmlHelper helper, PartialViewModels.BreadcrumbViewModel model)
        {
            dynamic parameters = new ExpandoObject();
            
            if (!model.IsInDetailsPage || !model.HasValidMarketIdInSession)
            {
                parameters.@class = "nhs_BreadcrumbBackHome";
                return string.Format(Li, BuildAnchorUrl(helper, LanguageHelper.NewHomes, Pages.Home, null, (object)parameters)).ToMvcHtmlString();
            }
            parameters.@class = "nhs_BreadcrumbBackSearch";
            
            switch (UserSession.SearchParametersV2.SrpType)
            {
                case SearchResultsPageType.CommunityResults:
                    return string.Format(Li, BuildAnchorUrl(helper, LanguageHelper.BackToSearch,  Pages.CommunityResults, model.BackToSearchParams, (object)parameters)).ToMvcHtmlString();
                case SearchResultsPageType.HomeResults:
                    return string.Format(Li, BuildAnchorUrl(helper, LanguageHelper.MySearchResults,  Pages.HomeResults, model.BackToSearchParams, (object)parameters)).ToMvcHtmlString();
                default:
                    return string.Format(Li, BuildAnchorUrl(helper, LanguageHelper.MySearchResults, Pages.CommunityResults, model.BackToSearchParams, (object)parameters)).ToMvcHtmlString();
            }
        }

        [ObsoleteAttribute("This method is obsolete. This works only for the old version of home/comm results in Pro (and Pro PLs) and should be removed once it gets migrated.", false)] 
        public static MvcHtmlString BackToMySearchButtonPro(this HtmlHelper helper, PartialViewModels.BreadcrumbViewModel model)
        {
            dynamic parameters = new ExpandoObject();

            if (!model.IsInDetailsPage || !model.HasValidMarketIdInSession)
            {
                parameters.@class = "nhs_BreadcrumbBackHome";
                return string.Format(Li, BuildAnchorUrl(helper, "New Homes", Pages.Home, null, (object)parameters)).ToMvcHtmlString();
            }
            parameters.@class = "nhs_BreadcrumbBackSearch";

            switch (UserSession.PreviousPage)
            {
                case Pages.CommunityDetail:
                    return string.Format(Li, BuildAnchorUrl(helper, "Back to Search",  Pages.CommunityResults, model.BackToSearchParams, (object)parameters)).ToMvcHtmlString();
                case Pages.HomeDetail:
                    return string.Format(Li, BuildAnchorUrl(helper, "My Search Results", Pages.HomeResults, model.BackToSearchParams, (object)parameters)).ToMvcHtmlString();
                default:
                    return string.Format(Li, BuildAnchorUrl(helper, "My Search Results", Pages.CommunityResults, model.BackToSearchParams, (object)parameters)).ToMvcHtmlString();
            }
        }

        public static MvcHtmlString BreadcrumbStateV2(this HtmlHelper helper,
                                                      PartialViewModels.BreadcrumbViewModel model)
        {
            dynamic parameters = new ExpandoObject();
            var htmlTag = string.Format(Li, BuildAnchorUrl(helper, model.StateName, Pages.StateIndex, new List<RouteParam> { new RouteParam(RouteParams.State, model.StateName.RemoveSpaceAndDash()) }, (object)parameters, true)); //hack to work around SEO 
            return MvcHtmlString.Create(htmlTag);
        }

        public static MvcHtmlString BreadcrumbAreaV2(this HtmlHelper helper, PartialViewModels.BreadcrumbViewModel model)
        {
            var metroAreaText = "Area";
            string textToDisplay;
            
            if (model.IsSyntheticSearch)
            {
                textToDisplay = model.MarketName;
            }
            else if (PartnerLayoutHelper.GetPartnerLayoutConfig().IsBrandPartnerCna)
            {
                textToDisplay = metroAreaText + " de " + model.MarketName;
            }
            else
            {
                textToDisplay = model.MarketName + " " + metroAreaText;
            }

            var breadcrumbArea = String.Format(Li, String.Format(Span, textToDisplay));
            dynamic parameters = new ExpandoObject();

            if (model.IsInDetailsPage || model.IsZipVisible || model.IsCountyVisible || model.IsCityVisible)
            {
                var marketName = String.Concat(textToDisplay, 
                    (NhsRoute.CurrentRoute.Function == Pages.CommunityDetailv1 
                       ? " Area Homes" 
                       : string.Empty));
                
                var page = (NhsRoute.CurrentRoute.Function == Pages.HomeDetail) ? Pages.HomeResults : Pages.CommunityResults;
                var parameter = new List<RouteParam>() {new RouteParam(RouteParams.Market, model.MarketId)};
                var link = BuildAnchorUrl(helper, marketName, page, parameter.ToResultsParams(), (object)parameters);

                breadcrumbArea = String.Format(Li, link);
            }

            return MvcHtmlString.Create(breadcrumbArea);
        }

        public static MvcHtmlString RenderZipCodeV2(this HtmlHelper helper, PartialViewModels.BreadcrumbViewModel model)
        {
            if (!model.IsZipVisible || model.IsInDetailsPage) return MvcHtmlString.Empty;
            var zip = String.Format(Span, model.ZipCode);
            return
                String.Format(Li, zip)
                      .ToMvcHtmlString();
        }

        public static MvcHtmlString RenderCountyAndCityCrumbV2(this HtmlHelper helper, PartialViewModels.BreadcrumbViewModel model)
        {
            var countyHtmlTag = string.Empty;
            var cityHtmlTag = string.Empty;
            var communityNameHtmlTag = string.Empty;
            var listingNameHtmlTag = string.Empty;
            var cityStName = string.Format("{0}, {1}", model.CityName, model.State);
            var parameters = new ExpandoObject();

            if (model.IsInDetailsPage)
            {
                cityStName = String.Format("{0}, {1}", model.CommunityCity, model.CommunityState);
            }        

            if (model.IsCountyVisible && !model.IsInDetailsPage)
            {
                var countyInfo = String.Concat(model.CountyName, (model.CountyName.ToLower().IndexOf("county", StringComparison.Ordinal) == -1 
                    ? " County" : String.Empty));

                if (!String.IsNullOrEmpty(countyInfo) && !String.IsNullOrEmpty(model.State))
                {
                    countyInfo += String.Format(", {0}", model.State);
                }

                countyHtmlTag = String.Format(Li, String.Format(Span, countyInfo));
            }

            if (model.IsCityVisible && model.IsInDetailsPage)
            {
                var page = (NhsRoute.CurrentRoute.Function == Pages.HomeDetail) ? Pages.HomeResults : Pages.CommunityResults;
                var parameter = new List<RouteParam>
                    {
                        new RouteParam(RouteParams.Market, model.MarketId),
                        new RouteParam(RouteParams.CityNameFilter, model.CityName)
                    };
                var nhsLink = BuildAnchorUrl(helper, cityStName, page, parameter.ToResultsParams(), parameters);
                cityHtmlTag = String.Format(Li, nhsLink.ToHtmlString());
            }

            else if (model.IsCityVisible)
                cityHtmlTag = String.Format(Li, String.Format(Span, cityStName));

            if (model.IsCommunityVisible)
            {
                if (model.IsListingVisible || model.IsRecommVisible)
                    communityNameHtmlTag = String.Format(Li, BuildAnchorUrl(helper,model.CommunityName,
                                                            RedirectionHelper.GetCommunityDetailPage(),
                                                            model.ToCommunityDetail(), parameters)
                                                         );
                else
                    communityNameHtmlTag = String.Format(Li, String.Format(Span,model.CommunityName));
            }

            if (model.IsListingVisible)
            {
                const int length = 25;
                string source = model.ListingName;
                source = (source.Length > length) ? source.Substring(0, length) + "..." : model.ListingName;              
                listingNameHtmlTag = String.Format(Li, String.Format(Span,source));
            }

            string htmlBreadcrumb = string.Concat(countyHtmlTag, cityHtmlTag, communityNameHtmlTag, listingNameHtmlTag);

            return MvcHtmlString.Create(htmlBreadcrumb);
        }

        public static MvcHtmlString RenderRecCommV2(this HtmlHelper helper, PartialViewModels.BreadcrumbViewModel model)
        {
            var htmlBreadcrumb = string.Empty;

            if (model.IsRecommVisible)
            {
                htmlBreadcrumb = String.Format(Li, String.Format(Span,LanguageHelper.CommunityRecommendations));
            }

            return MvcHtmlString.Create(htmlBreadcrumb);
        }
    }
}
