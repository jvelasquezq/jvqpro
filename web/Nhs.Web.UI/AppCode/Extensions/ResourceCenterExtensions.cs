﻿using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Extensions
{
    public static class ResourceCenterExtensions
    {
        public static ResourceCenterViewModels.ResourceCenterBaseViewModel AddWideHomesSearchParameter(this ResourceCenterViewModels.ResourceCenterBaseViewModel model,
            bool useWideHomeSearch, bool useSearchboxId2 = false)
        {
            var newModel = new ResourceCenterViewModels.ResourceCenterBaseViewModel
            {
                MktSearch = model.MktSearch,
                CmsProSearchParams = model.CmsProSearchParams,
                UseWideHomesSearchBox = useWideHomeSearch || model.UseWideHomesSearchBox,
                SearchBoxId = useSearchboxId2 ? "MktSearch2" : "MktSearch"
            };

           

            return newModel;
        }
    }
}