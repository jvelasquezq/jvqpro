﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Bdx.Web.Api.Objects;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Content;
using Nhs.Library.Web;
using Nhs.Library.Web.ImageResizer;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects;
using Nhs.Search.Objects.Constants;
using Nhs.Utility.Common;
using Nhs.Utility.Html;
using Nhs.Web.UI.AppCode.Controllers;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult;
using Resources;
using HomeStatusType = Nhs.Search.Objects.Constants.HomeStatusType;

namespace Nhs.Web.UI.AppCode.Extensions
{
    public static class CommunityDetailExtensions
    {

        public static string HomeThumbnail(this ApiHomeResultV2 homeResult)
        {
            string nophotopath = GlobalResources14.Default.images.no_photo.no_photos_180x120_png;
            if (NhsRoute.IsMobileDevice)
            {
                nophotopath = GlobalResources14.Default.images.no_photo.no_photos_alt_180x120_png;
            }
            return homeResult.Thumb1.IsValidImage()
              ? ImageResizerUrlHelpers.BuildIrsImageUrl(ImageResizerUrlHelpers.FormatImageUrlForIrs(homeResult.Thumb1), ImageSizes.HomeThumb)
              : nophotopath;
        }

        /// <summary>
        /// Retrieves the picture or video thumbnail image to display in the community item
        /// </summary>
        /// <param name="homeResult">Community item</param>
        /// <returns>Thumbnail path</returns>
        public static string HomeThumbnail(this HomeResult homeResult)
        {
            var thumbnail = ImageResizerUrlHelpers.FormatImageUrlForIrs(homeResult.PlanImageThumbnail);
            return thumbnail.IsValidImage()
                ? ImageResizerUrlHelpers.BuildIrsImageUrl(thumbnail, ImageSizes.HomeThumb)
                : GlobalResources14.Default.images.no_photo.no_photos_180x120_png;
        }

        public static string SmallHomeThumbnail(this HomeResult homeResult)
        {
            var thumbnail = ImageResizerUrlHelpers.FormatToIrsImageName(homeResult.PlanImageThumbnail);
            return thumbnail.IsValidImage()
                ? ImageResizerUrlHelpers.BuildIrsImageUrl(thumbnail, ImageSizes.CommDetailThumb)
                :GlobalResources14.Default.images.no_photo.no_photos_75x58_png;
        }

        public static string RecHomeThumbnail(this HomeResult homeResult)
        {
            var thumbnail = ImageResizerUrlHelpers.FormatToIrsImageName(homeResult.PlanImageThumbnail);
            return thumbnail.IsValidImage()
                ? ImageResizerUrlHelpers.BuildIrsImageUrl(thumbnail, ImageSizes.Spotlight)
                : GlobalResources14.Default.images.no_photo.no_photos_120x80_png;
        }

        public static MvcHtmlString GetExternalVideoLink(this HtmlHelper helper, IDetailViewModel model, MediaPlayerObject mpObject)
        {
            var logEvent = LogImpressionConst.VirtualTour;
            var image = PartnerLayoutHelper.GetPartnerLayoutConfig().IsBrandPartnerCna ? 
                GlobalResourcesMvc.CNA.images.mediaplayer.launch_ext_video_120x80_png:
                GlobalResourcesMvc.Default.images.mediaplayer.launch_ext_video_120x80_png;

            if (mpObject.SubType == MediaPlayerObjectTypes.SubTypes.PlanVideo)
            {
                logEvent = LogImpressionConst.CommunityDetailInteractiveFloorPlan;
            }
            else if (mpObject.SubType == MediaPlayerObjectTypes.SubTypes.ExternalVideo)
            {
                logEvent = LogImpressionConst.CommunityDetailExternalVideo;
                image = PartnerLayoutHelper.GetPartnerLayoutConfig().IsBrandPartnerCna ? 
                    GlobalResourcesMvc.CNA.images.mediaplayer.launch_ext_video_120x80_png:
                    GlobalResourcesMvc.Default.images.mediaplayer.launch_ext_video_120x80_png;
            }
            else if (mpObject.SubType == MediaPlayerObjectTypes.SubTypes.VirtualTour)
            {
                logEvent = LogImpressionConst.CommunityDetailVirtualTour;
                image = PartnerLayoutHelper.GetPartnerLayoutConfig().IsBrandPartnerCna ?
                    GlobalResourcesMvc.CNA.images.mediaplayer.launch_vt_120x80_png:
                    GlobalResourcesMvc.Default.images.mediaplayer.launch_vt_120x80_png;
            }
            else if (mpObject.SubType == MediaPlayerObjectTypes.SubTypes.FloorPlanImages)
                image = PartnerLayoutHelper.GetPartnerLayoutConfig().IsBrandPartnerCna ?
                    GlobalResourcesMvc.CNA.images.mediaplayer.launch_ifp_120x80_png :
                    GlobalResourcesMvc.Default.images.mediaplayer.launch_ifp_120x80_png;

            if (string.IsNullOrEmpty(mpObject.Url)) //NULL SVT
                mpObject.Url = string.Empty;

            var logInfo = new { communityId = model.CommunityId.ToString(), communityName = model.CommunityName.RemoveSpecialCharacters(), builderId = model.BuilderId.ToString(), builderName = model.BuilderName.RemoveSpecialCharacters(), planId = model.PlanId.ToString(), specId = model.SpecId.ToString(), marketId = model.MarketId.ToString(), marketName = model.MarketName.RemoveSpecialCharacters() };
            return helper.NhsImageLinkLogAndRedirect(LanguageHelper.ViewVideo, image, PathHelpers.CleanNavigateUrl(mpObject.Url), logEvent, logInfo, null, new { target = "_blank" });
        }

        public static MvcHtmlString RenderPromoLogo(this HtmlHelper helper)
        {
            if (NhsRoute.IsBrandPartnerNhsPro) {
                return helper.NhsImage(Resources.GlobalResourcesMvc.Default.images.icons.special_offers_png, LanguageHelper.SpecialOffer, false);
            }
            else {
                return helper.NhsImage(Resources.GlobalResources14.Default.images.icons.deals_png, LanguageHelper.SpecialOffer, false);
            }
        }

        public static MvcHtmlString RenderEventLogo(this HtmlHelper helper)
        {
            return helper.NhsImage(Resources.GlobalResourcesMvc.Default.images.icons.event_calendar_png, "Event", false);
        }

        public static MvcHtmlString RenderGreenLogo(this HtmlHelper helper)
        {
            if (NhsRoute.IsBrandPartnerNhsPro)
            {
                return helper.NhsImage(Resources.GlobalResourcesMvc.Default.images.icons.green_png, "Green Program", false);
            }
            else
            {
                return helper.NhsImage(Resources.GlobalResources14.Default.images.icons.green_png, "Green Program", false);
            } 
        }

        public static MvcHtmlString RenderHotHomeLogo(this HtmlHelper helper)
        {
            if (NhsRoute.IsBrandPartnerNhsPro)
            {
                return helper.NhsImage(Resources.GlobalResourcesMvc.Default.images.icons.hot_homes_png, "Hot Home", false);
            }
            else
            {
                return helper.NhsImage(Resources.GlobalResources14.Default.images.icons.hot_png, "Hot Home", false);
            }            
        }

        public static string HotHomeTitle(this HomeResult homeResult, IPlan plan, ISpec spec)
        {
            var hotHomeTitle = homeResult.SpecId > 0 ? spec.HotHomeTitle : plan.HotHomeTitle;
            return string.IsNullOrEmpty(hotHomeTitle) ? string.Empty : hotHomeTitle;
        }

        public static string HotHomeTitle(this ApiHomeResultV2 homeResult)
        {
            var hotHomeTitle = homeResult.GetHomeLinkText();
            return string.IsNullOrEmpty(hotHomeTitle) ? string.Empty : hotHomeTitle;
        }

        public static string HotHomeDescription(this HomeResult homeResult, IPlan plan, ISpec spec)
        {
            var hotHomeDescription = homeResult.SpecId > 0 ? spec.HotHomeDescription : plan.HotHomeDescription;
            return string.IsNullOrEmpty(hotHomeDescription) ? string.Empty : hotHomeDescription;
        }

        public static MvcHtmlString HomeStatusAsyncImage(this HtmlHelper helper, HomeResult homeResult)
        {
            var status = homeResult.HomeStatus();
            return helper.NhsAsyncImage(status["url"], status["desc"], null, false);
        }

        public static Dictionary<string, string> HomeStatus(this HomeResult homeResult)
        {
            var homeStatusInfo = new Dictionary<string, string>(2);

            switch (homeResult.HomeStatus)
            {
                case (int)HomeStatusType.AvailableNow:
                    homeStatusInfo.Add("url", Resources.GlobalResourcesMvc.Default.images.icons.status_available_png);
                    homeStatusInfo.Add("desc", "Available now");
                    break;
                case (int)HomeStatusType.UnderConstruction:
                    homeStatusInfo.Add("url", Resources.GlobalResourcesMvc.Default.images.icons.status_under_construction_png);
                    homeStatusInfo.Add("desc", "Under construction");
                    break;
                case (int)HomeStatusType.ModelHome:
                    homeStatusInfo.Add("url", Resources.GlobalResourcesMvc.Default.images.icons.status_model_home_png);
                    homeStatusInfo.Add("desc", "Model home");
                    break;
                default:
                    homeStatusInfo.Add("url", Resources.GlobalResourcesMvc.Default.images.icons.status_ready_png);
                    homeStatusInfo.Add("desc", "Ready to build");
                    break;
            }
            return homeStatusInfo;
        }

        public static MvcHtmlString HomeStatus(this ApiHomeResultV2 homeResult)
        {
            string @class;
            string text;
            switch (homeResult.Status.ToHomeStatusType())
            {
                case HomeStatusType.AvailableNow:
                    @class = "nhs_AvailableNow";
                    text = LanguageHelper.AvailableNow;
                    break;
                case HomeStatusType.UnderConstruction:
                    @class = "nhs_UnderConstruction";
                    text = LanguageHelper.UnderConstruction;
                    break;
                case HomeStatusType.ModelHome:
                    @class = "nhs_ModelHome";
                    text = LanguageHelper.ModelHome;
                    break;
                default:
                    @class = "nhs_ReadyToBuild";
                    text = LanguageHelper.ReadyToBuild;
                    break;
            }
            return string.Format("<p class=\"{0}\">{1}</p>", @class, text).ToMvcHtmlString();
        }

        public static string ImageCountLink(this HomeResult homeResult)
        {
            if (homeResult.ImageCount == 0)
                return "No photo Available";

            if (homeResult.ImageCount > 1)
                return string.Format("{0} photos", homeResult.ImageCount);

            if (homeResult.ImageCount == 1)
                return string.Format("{0} photo", homeResult.ImageCount);

            return string.Empty;
        }

        public static string FormatDistance(this decimal? distance)
        {
            return distance != null ? string.Format("{0}mi ", distance.ToType<decimal>().ToString("0.##")) : "0.0mi";
        }

        public static IList<IAmenity> GetAmenitiesByType(this IDictionary<AmenityGroup, IList<IAmenity>> amenities, AmenityGroup amenityGroup)
        {
            return (from a in amenities
                    where a.Key == amenityGroup
                    select a.Value).FirstOrDefault();
        }

        public static List<IGrouping<string, School>> GetSchoolsByDistrict(this IList<School> schools)
        {
            var list = schools.GroupBy(item => item.SchoolDistrict.DistrictName).ToList();
            return list;
        }

        public static string GetUtilities(this ICollection<Mvc.Domain.Model.Web.Utility> utilities)
        {
            if (utilities.Count == 0) return string.Empty;
            var utilitiesInfo = new StringBuilder();
            utilitiesInfo.Append("<ul>");
            foreach (Mvc.Domain.Model.Web.Utility utility in utilities)
            {
                if (utility.CommunityServiceTypeName == "HOA")
                {
                    if (!string.IsNullOrEmpty(utility.CommunityServiceDescription) || (utility.CommunityServiceMonthlyFee != 0 || utility.CommunityServiceYearlyFee != 0))
                    {
                        utilitiesInfo.Append("<li>");

                        utilitiesInfo.Append(string.Concat("<span>", utility.CommunityServiceTypeName, "</span>"));

                        if ((utility.CommunityServiceMonthlyFee != 0 && utility.CommunityServiceYearlyFee != 0) || (utility.CommunityServiceMonthlyFee != 0 && utility.CommunityServiceYearlyFee == 0))
                            utilitiesInfo.Append(string.Concat("<br />", LanguageHelper.Fee + ": " + utility.CommunityServiceMonthlyFee.FormatPrice(), " " + LanguageHelper.PerMonth));
                        else if (utility.CommunityServiceYearlyFee != 0)
                            utilitiesInfo.Append(string.Concat("<br />", LanguageHelper.Fee + ": " + utility.CommunityServiceYearlyFee.FormatPrice(), " " + LanguageHelper.PerYear));
                        else
                            utilitiesInfo.Append(@"<br /><span><a href=""#ContactBuilder"">" + LanguageHelper.Fee + ": " + LanguageHelper.ContactTheBuilder + "</a> </span>");

                        if (!string.IsNullOrEmpty(utility.CommunityServiceDescription))
                            utilitiesInfo.Append(string.Concat("<br />", utility.CommunityServiceDescription, "<br />"));

                        utilitiesInfo.Append("</li>");
                    }

                }
                else
                {
                    utilitiesInfo.Append("<li>");
                    utilitiesInfo.Append(string.Concat("<span>", utility.CommunityServiceTypeName, "</span><br />", utility.CommunityServiceDescription));

                    if (!string.IsNullOrEmpty(utility.CommunityServicePhone) && utility.CommunityServicePhone.Trim().Replace("0", string.Empty) != string.Empty)
                        utilitiesInfo.Append(string.Concat("<br />", StringHelper.FormatPhone(utility.CommunityServicePhone), "<br />"));

                    utilitiesInfo.Append("</li>");
                }

            }
            utilitiesInfo.Append("</ul>");
            return utilitiesInfo.ToString();
        }

        public static string CommThumbnail(this NearbyCommunity community)
        {
            var thumbnail = ImageResizerUrlHelpers.FormatToIrsImageName(community.SpotlightThumbnail);

            if (thumbnail.IsValidImage())
            {

                thumbnail = (thumbnail.Contains(Configuration.IRSDomain))
                          ? thumbnail
                          : ImageResizerUrlHelpers.BuildIrsImageUrl(thumbnail, ImageSizes.CommDetailThumb);

            }
            else
                thumbnail =GlobalResources14.Default.images.no_photo.no_photos_240x160_png;

            return thumbnail;
        }


        public static string CommThumbnailMobile(this NearbyCommunity community)
        {
            var thumbnail = ImageResizerUrlHelpers.FormatToIrsImageName(community.SpotlightThumbnail);

            if (thumbnail.IsValidImage())
            {

                thumbnail = (thumbnail.Contains(Configuration.IRSDomain))
                          ? thumbnail
                          : ImageResizerUrlHelpers.BuildIrsImageUrl(thumbnail, ImageSizes.CommDetailThumb);

            }
            else
                thumbnail = GlobalResources14.Default.images.no_photo.no_photos_alt_180x120_png;

            return thumbnail;
        }

        public static string CommSmallImage(this NearbyCommunity community)
        {
            var thumbnail = ImageResizerUrlHelpers.FormatToIrsImageName(community.SpotlightThumbnail);

            thumbnail = thumbnail.IsValidImage()
                ? ImageResizerUrlHelpers.BuildIrsImageUrl(thumbnail, ImageSizes.Small)
                : GlobalResources14.Default.images.no_photo.no_photos_120x80_png;

            return thumbnail;
        }

        public static string HomeThumbnail(this NearbyHome home)
        {
            var thumbnail = ImageResizerUrlHelpers.FormatToIrsImageName(home.ImageThumbnail);

            return thumbnail.IsValidImage()
                       ? ImageResizerUrlHelpers.BuildIrsImageUrl(thumbnail, ImageSizes.NearbyHome)
                       : GlobalResources14.Default.images.no_photo.no_photos_120x80_png;
        }


        public static MvcHtmlString HomeDetailImageLink(this HtmlHelper helper, ExtendedHomeResult result, bool returnSmallImage, bool previewMode = false)
        {
            var altText = string.Format("{0}{1}: {2}, {3} - {4}", ((string.IsNullOrEmpty(result.PlanName)) ? string.Empty : result.PlanName + " - "), result.CommunityName, result.City, result.State, result.BrandName).Trim();

            var titleText = string.Format("{0}{1}: {2}, {3} {4}", ((string.IsNullOrEmpty(result.PlanName)) ? string.Empty : result.PlanName + " - "), result.CommunityName, result.City, result.State, result.PostalCode).Trim();

            var thumbnail = returnSmallImage ? result.SmallHomeThumbnail() : result.HomeThumbnail();
            var pageName = previewMode ? Pages.HomeDetailPreview : RedirectionHelper.GetHomeDetailPage(result.SpecId > 0);
            return helper.NhsAsyncImageLink(thumbnail, pageName,
                                       result.ToHomeDetail(), null,
                                       new Dictionary<string, object>
                                           {
                                               {"title",  titleText},
                                               {"alt", altText},
                                               {"onclick", "return false;"},
                                               {"class", "async"}
                                           }, false);
        }

        public static MvcHtmlString HomeDetailImageLink(this HtmlHelper helper, HomeItem result, bool returnSmallImage, bool previewMode = false)
        {
            var pageName = previewMode ? Pages.HomeDetailPreview : RedirectionHelper.GetHomeDetailPage(result.IsSpec > 0);
            var link = helper.NhsAsyncImageLink(result.ValidateHomeResThumb1(), pageName, result.ToHomeDetail(), result.GetHomeResThumb1Size());

            return link;
        }

        public static MvcHtmlString HomeDetailLink(this HtmlHelper helper, ExtendedHomeResult result, bool previewMode = false)
        {
            var altText = result.PlanName + " - " + result.Address1 + " " + result.Address2 + " " +
                          result.City + ", " + result.State + " " +
                          result.PostalCode;

            var pageName = previewMode ? Pages.HomeDetailPreview : RedirectionHelper.GetHomeDetailPage(result.SpecId > 0);
            return helper.NhsLink(result.ListingTitle(), pageName,
                                  result.ToHomeDetail(), new { title = altText, onclick = "return false;" });
        }

        public static HomeStatusType ToHomeStatusType(this string status)
        {
            HomeStatusType retValue;

            if (string.IsNullOrWhiteSpace(status))
            {
                throw new ArgumentNullException("status");
            }

            switch (status.ToUpper())
            {
                case "A":
                    retValue = HomeStatusType.AvailableNow;
                    break;
                case "UC":
                    retValue = HomeStatusType.UnderConstruction;
                    break;
                case "M":
                    retValue = HomeStatusType.ModelHome;
                    break;
                case "R":
                    retValue = HomeStatusType.ReadyToBuild;
                    break;
                default:
                    retValue = HomeStatusType.ModelHome;
                    break;
            }

            return retValue;
        }

        public static MvcHtmlString BasiCommunityHomeName(this HomeItem homeResult)
        {
            if (homeResult.IsSpec != 0 & homeResult.Addr != string.Empty)
                return string.Concat(homeResult.Addr, " <span class=\"nhs_SpecPlanName\">(", homeResult.PlanName,
                                     ")</span>").ToMvcHtmlString();

            if (homeResult.IsSpec != 0)
                return string.Concat("<span class=\"nhs_SpecPlanNameOnly\">", homeResult.PlanName,
                                     "</span>").ToMvcHtmlString();

            if (homeResult.HomeId != 0)
                return homeResult.PlanName.ToMvcHtmlString();

            return string.Empty.ToMvcHtmlString();
        }

        public static string GetHomeDetailUrl(this HtmlHelper helper, ExtendedHomeResult result, bool previewMode = false)
        {
            var parameters = result.ToHomeDetail();
            return parameters.ToUrl(previewMode ? Pages.HomeDetailPreview : (RedirectionHelper.GetHomeDetailPage(result.SpecId > 0))).ToLower();
        }

        public static string GetHomeDetailUrl(this HtmlHelper helper, HomeItemViewModel result, bool previewMode = false)
        {
            var parameters = result.ToHomeDetail();
            return parameters.ToUrl(previewMode ? Pages.HomeDetailPreview : (RedirectionHelper.GetHomeDetailPage(result.IsSpec > 0))).ToLower();
        }

        public static string GetHomeDetailUrl(this HtmlHelper helper, HomeItem result, bool previewMode = false)
        {
            var pageName = previewMode ? Pages.HomeDetailPreview : RedirectionHelper.GetHomeDetailPage(result.IsSpec > 0);
            var parameters = result.ToHomeDetail();
            return parameters.ToUrl(pageName).ToLower();
        }

        public static string ShowPromoDescription(this Promotion promotion)
        {
            if (!string.IsNullOrEmpty(promotion.PromoUrl)) return promotion.PromoTextLong;

            string description = promotion.PromoTextFormatted;
            int descriptionLength = Configuration.PromoDescriptionLength;
            if (description.Length <= descriptionLength) return description;
            string chunk = description.Substring(0, descriptionLength);

            int lastWordCompleted = chunk.LastIndexOf(' ');
            return lastWordCompleted > 0 ? chunk.Substring(0, lastWordCompleted) : chunk;
        }

        public static string ShowHotHomeDescription(this HomeResult homeResult, IPlan plan, ISpec spec)
        {
            var hotHomeDescription = homeResult.SpecId > 0 ? spec.HotHomeDescription : plan.HotHomeDescription;

            if (hotHomeDescription.Contains("|"))
            {
                hotHomeDescription = hotHomeDescription.Substring(hotHomeDescription.IndexOf("|", System.StringComparison.Ordinal) + 1);
            }

            int descriptionLength = Configuration.PromoDescriptionLength;
            if (hotHomeDescription.Length <= descriptionLength)
            {
                return (string.IsNullOrEmpty(hotHomeDescription)
                    ? string.Empty
                    : string.Concat(": ", hotHomeDescription));
            }

            string chunk = hotHomeDescription.Substring(0, descriptionLength);

            int lastWordCompleted = chunk.LastIndexOf(' ');
            var shortText = lastWordCompleted > 0 ? chunk.Substring(0, lastWordCompleted) : chunk;
            return (string.IsNullOrEmpty(shortText) ? string.Empty : string.Concat(": ", shortText));
        }

        public static string ShowHotHomeDescription(this HomeItem homeResult)
        {
            var hotHomeDescription = homeResult.HotHomeDescription;

            if (hotHomeDescription.Contains("|"))
            {
                hotHomeDescription = hotHomeDescription.Substring(hotHomeDescription.IndexOf("|", System.StringComparison.Ordinal) + 1);
            }

            int descriptionLength = Configuration.PromoDescriptionLength;
            if (hotHomeDescription.Length <= descriptionLength)
            {
                return (string.IsNullOrEmpty(hotHomeDescription)
                    ? string.Empty
                    : string.Concat(": ", hotHomeDescription));
            }

            string chunk = hotHomeDescription.Substring(0, descriptionLength);

            int lastWordCompleted = chunk.LastIndexOf(' ');
            var shortText = lastWordCompleted > 0 ? chunk.Substring(0, lastWordCompleted) : chunk;
            return (string.IsNullOrEmpty(shortText) ? string.Empty : string.Concat(": ", shortText));
        }

        public static string HotHomeDescriptionLong(this HomeItem homeResult)
        {
            // throw new Exception("HOT LONG DESCRIPTION");
            var hotHomeDescription = homeResult.HotHomeDescription;

            if (hotHomeDescription.Contains("|"))
            {
                hotHomeDescription = hotHomeDescription.Substring(hotHomeDescription.IndexOf("|", System.StringComparison.Ordinal) + 1);
            }

            return (string.IsNullOrEmpty(hotHomeDescription) ? string.Empty : string.Concat(": ", hotHomeDescription));
        }

        public static string HotHomeTextFormatted(this HomeItem homeResult)
        {
            //throw new Exception("HotHomeTextFormatted");
            var hotHomeDescription = homeResult.HotHomeDescription;
            string hot = ParseHTML.StripHTML(hotHomeDescription);
            if (hot.Length <= 90) return hot;
            string chunk = hot.Substring(0, 90);

            int lastWordCompleted = chunk.LastIndexOf(' ');
            string sentenceTruncated = chunk.Substring(0, lastWordCompleted).TrimEnd();
            if (sentenceTruncated[sentenceTruncated.Length - 1] == '.')
                sentenceTruncated = sentenceTruncated.Remove(sentenceTruncated.Length - 1);

            return string.Concat(sentenceTruncated, "...");
        }

        public static int HotHomeDescriptionLength(this HomeItem hotHome)
        {
            return Configuration.PromoDescriptionLength;
        }

        public static string HotHomeDescriptionLong(this HomeResult homeResult, IPlan plan, ISpec spec)
        {
            var hotHomeDescription = homeResult.SpecId > 0 ? spec.HotHomeDescription : plan.HotHomeDescription;

            if (hotHomeDescription.Contains("|"))
            {
                hotHomeDescription = hotHomeDescription.Substring(hotHomeDescription.IndexOf("|", System.StringComparison.Ordinal) + 1);
            }

            return (string.IsNullOrEmpty(hotHomeDescription) ? string.Empty : string.Concat(": ", hotHomeDescription));
        }


        public static int HotHomeDescriptionLength(this HomeResult hotHome)
        {
            return Configuration.PromoDescriptionLength;
        }


        public static int PromoDescriptionLength(this Promotion promotion)
        {
            return Configuration.PromoDescriptionLength;
        }

        public static MvcHtmlString PromoLogLink(this HtmlHelper helper, IDetailViewModel model, string externalUrl, bool isAFlyer, bool isGreenPromo, bool isPreviewMode)
        {
            if (string.IsNullOrEmpty(externalUrl)) return MvcHtmlString.Empty;

            string logEvent = string.Empty;
            string logType;

            if (model.PlanId > 0 || model.SpecId > 0)
                logType = "home";
            else
                logType = "comm";

            switch (isGreenPromo)
            {
                case true:
                    switch (logType)
                    {
                        case "home":
                            logEvent = isPreviewMode ? LogImpressionConst.HomeResGreenLinkPreview : LogImpressionConst.HomeResGreenLink;
                            break;
                        case "comm":
                            logEvent = isPreviewMode ? LogImpressionConst.CommResGreenLinkPreview : LogImpressionConst.CommResGreenLink;
                            break;
                    }
                    break;
                case false:
                    switch (logType)
                    {
                        case "home":
                            logEvent = isPreviewMode ? LogImpressionConst.HomeResPromoLinkPreview : LogImpressionConst.HomeResPromoLink;
                            break;
                        case "comm":
                            logEvent = isPreviewMode ? LogImpressionConst.CommResPromoLinkPreview : LogImpressionConst.CommResPromoLink;
                            break;
                    }
                    break;
            }

            if (isAFlyer) externalUrl = (isGreenPromo ? Configuration.ProgramFlyerUrl : Configuration.PromoFlyerUrl) + externalUrl;


            var logInfo = new
            {
                communityId = model.CommunityId.ToString(),
                communityName = model.CommunityName.RemoveSpecialCharacters(),
                builderId = model.BuilderId.ToString(),
                builderName = model.BuilderName.RemoveSpecialCharacters(),
                planId = model.PlanId.ToString(),
                specId = model.SpecId.ToString(),
                marketId = model.MarketId.ToString(),
                marketName = model.MarketName.RemoveSpecialCharacters()
            };

            return !model.PreviewMode ? helper.NhsLinkLogAndRedirect(LanguageHelper.LearnMore + " »", PathHelpers.CleanNavigateUrl(externalUrl), logEvent, logInfo, null, new { target = "_blank" })
                                      : helper.NhsLinkLogAndRedirect(LanguageHelper.LearnMore + " »", string.Empty, string.Empty, string.Empty, null); 
        }

        public static MvcHtmlString PromoEventProLink(this HtmlHelper helper, string externalUrl, bool isAFlyer, bool isGreenPromo, bool isPreviewMode, bool isEvent = false)
        {
            if (string.IsNullOrEmpty(externalUrl)) return MvcHtmlString.Empty;
            if (isEvent)
            {
                externalUrl = externalUrl.EndsWith(".pdf")
                         ? Configuration.EventFlyerUrl + externalUrl
                         : externalUrl;
            }
            else if (isAFlyer)
            {
                externalUrl = externalUrl.EndsWith(".pdf")
                    ? (isGreenPromo ? Configuration.ProgramFlyerUrl : Configuration.PromoFlyerUrl) + externalUrl
                    : externalUrl;
            }

            return !isPreviewMode? helper.NhsLink("Click here to see additional details »", PathHelpers.CleanNavigateUrl(externalUrl), new List<RouteParam>(), new { target = "_blank", title = "" }, true)
                                 : helper.NhsLink("Click here to see additional details »", string.Empty, new List<RouteParam>(), null, true); 
        }

        public static MvcHtmlString AmenityLink(this HtmlHelper helper, IAmenity amenity)
        {
            if (string.IsNullOrEmpty(amenity.Url)) return MvcHtmlString.Create(amenity.Description);

            var htmlLink = new TagBuilder("a");

            if (!amenity.Url.StartsWith("http://") && !amenity.Url.StartsWith("https://"))
                amenity.Url = string.Format("http://{0}", amenity.Url);

            htmlLink.MergeAttribute("href", amenity.Url);
            htmlLink.InnerHtml = amenity.Description;
            htmlLink.Attributes.Add("target", "_blank");
            return MvcHtmlString.Create(htmlLink.ToString());
        }

        public static MvcHtmlString GetPromoCssClass(this IDetailPromotionsViewModel model, bool isPreview = false)
        {
            var homesCount = model.HotHomesApi.Count;

            if ((model.Promotions.Count > 0 || model.Events.Count > 0) && model.GreenPrograms.Count > 0 && homesCount > 0)
                return MvcHtmlString.Create("option3");

            if (((model.Promotions.Count > 0 || model.Events.Count > 0) && model.GreenPrograms.Count > 0) ||
               ((model.Promotions.Count > 0 || model.Events.Count > 0) && homesCount > 0) ||
               (homesCount > 0 && model.GreenPrograms.Count > 0)
               )
                return MvcHtmlString.Create("option2");

            return MvcHtmlString.Create("option1");
        }

        public static IDetailViewModel GetInactivePropertyViewModel(bool isCommunity, IStateService stateService, ILookupService lookupService)
        {
            IDetailViewModel model;

            if (isCommunity)
            {
                model = new CommunityDetailViewModel();
                ((CommunityDetailViewModel)model).ExternalMediaLinks = new List<MediaPlayerObject>();
            }
            else
            {
                model = new HomeDetailViewModel();
                ((HomeDetailViewModel)model).ExternalMediaLinks = new List<MediaPlayerObject>();
            }
            model.GreenPrograms = new List<GreenProgram>();
            model.Promotions = new List<Promotion>();
            model.States = (from s in stateService.GetPartnerStates(NhsRoute.PartnerId) select new SelectListItem { Text = s.StateName, Value = s.StateAbbr });
            model.PriceLowRange = (from l in lookupService.GetCommonListItems(CommonListItem.MinPrice) select new SelectListItem { Text = l.LookupText, Value = l.LookupValue });
            model.PriceHighRange = (from l in lookupService.GetCommonListItems(CommonListItem.MaxPrice) select new SelectListItem { Text = l.LookupText, Value = l.LookupValue });
            model.SearchText = string.Empty;
            model.IsInactive = true;

            return model;
        }

        public static IDetailViewModel RedirectInactiveProperty(IDetailViewModel model, BaseController controller, IStateService stateService, ILookupService lookupService)
        {
            if (string.IsNullOrEmpty(model.SearchText))
            {
                controller.ModelState.AddModelError("SearchText", @"location required.");
                return GetInactivePropertyViewModel(true, stateService, lookupService);
            }

            IList<RouteParam> @params = new List<RouteParam>();

            // Searching by Zip
            if (CommonUtils.IsZip(model.SearchText))
            {
                @params.Add(new RouteParam(RouteParams.SearchText, model.SearchText, RouteParamType.QueryString));
            }
            // Searching by city or zip
            else if (!string.IsNullOrEmpty(model.SearchText))
            {
                @params.Add(new RouteParam(RouteParams.SearchText, model.SearchText, RouteParamType.QueryString));
                @params.Add(new RouteParam(RouteParams.State, model.SelectedStateAbbr, RouteParamType.Friendly));
                UserSession.PersonalCookie.State = model.SelectedStateAbbr;
            }
            // Input error handler
            else
            {
                controller.ModelState.AddModelError("SearchText", @"invalid data, city or zip required.");
                return GetInactivePropertyViewModel(true, stateService, lookupService);
            }

            if (!string.IsNullOrEmpty(model.PriceLow))
            {
                @params.Add(new RouteParam(RouteParams.PriceLow, model.PriceLow, RouteParamType.Friendly));
                UserSession.PersonalCookie.PriceLow = int.Parse(model.PriceLow);
            }

            if (!string.IsNullOrEmpty(model.PriceHigh))
            {
                @params.Add(new RouteParam(RouteParams.PriceHigh, model.PriceHigh, RouteParamType.Friendly));
                UserSession.PersonalCookie.PriceHigh = int.Parse(model.PriceHigh);
            }

            //redirect to location handler page
            controller.Redirect(Pages.LocationHandler, @params);
            return null;
        }

        public static MvcHtmlString CommunityAmenityOpenRow(this HtmlHelper helper, int count)
        {
            StringBuilder str = new StringBuilder();
            bool isAlternate = ((count / 2) % 2) == 1;
            if (count % 2 == 0) str.Append("<div class=\"" + (isAlternate ? "nhs_CommunityInfoRow" : "nhs_CommunityInfoAlternateRow") + "\">");

            return MvcHtmlString.Create(str.ToString());
        }


        public static MvcHtmlString CommunityAmenityCloseRow(this HtmlHelper helper, int count)
        {
            StringBuilder str = new StringBuilder();
            if (count % 2 == 0) str.Append("</div>");

            return MvcHtmlString.Create(str.ToString());
        }

        public static MvcHtmlString GetCommHomeLiteralText(this IDetailViewModel model, bool returnPlural, bool returnTitleCase)
        {
            string text;
            if (model.Globals.PartnerLayoutConfig.AmIOnCommunityDetailPage)
                text = returnPlural ? LanguageHelper.Communities : LanguageHelper.Community;
            else if (model.Globals.PartnerLayoutConfig.AmIOnHomeDetailPage)
                text = returnPlural ? LanguageHelper.Homes : LanguageHelper.Home;
            else
                text = LanguageHelper.Page;
            
            return MvcHtmlString.Create(returnTitleCase ? text.ToTitleCase() : text);
        }

        public static string CommunityDetailHeader(this HtmlHelper helper, CommunityDetailViewModel model)
        {
            return string.Concat(model.CommunityCity, ", ", model.SelectedStateAbbr, " ", model.ZipCode);
        }


        public static MvcHtmlString LeadFormLabel(this string detailType)
        {
            return detailType == "community" ?
                (NhsRoute.CurrentRoute.Function == Pages.CommunityDetailv1 ? LanguageHelper.SendInfoFromCommunities.ToMvcHtmlString() : LanguageHelper.IWouldLikeFreeInfo.ToMvcHtmlString()) :
                LanguageHelper.SendInfoFromCommunities.ToMvcHtmlString();
        }

        public static MvcHtmlString LocationsMetroSeeAllLink(this HtmlHelper helper, int marketId, int brandId)
        {
            return helper.NhsLink("(" + LanguageHelper.SeeAllNoMoreWords + ")", Pages.CommunityResults,
                                  new List<RouteParam>()
                                      {
                                          new RouteParam(RouteParams.Market, marketId.ToString()),
                                          new RouteParam(RouteParams.BrandId, brandId.ToString())

                                      }, null);
        }

        public static string LocationsCommDetailLink(this Community community)
        {
            const string link = "/communitydetail/builder-{0}/community-{1}";
            var builder = community.BuilderId;
            var commId = community.CommunityId;
            return string.Format(link, builder, commId);
        }

        public static IDetailViewModel GetInactiveBasicCommunityPropertyViewModel(bool isCommunity, IStateService stateService, ILookupService lookupService)
        {
            IDetailViewModel model;

            if (isCommunity)
            {
                model = new BasicCommunityDetailViewModel();
                ((BasicCommunityDetailViewModel)model).ExternalMediaLinks = new List<MediaPlayerObject>();
            }
            else
            {
                model = new HomeDetailViewModel();
                ((HomeDetailViewModel)model).ExternalMediaLinks = new List<MediaPlayerObject>();
            }
            model.GreenPrograms = new List<GreenProgram>();
            model.Promotions = new List<Promotion>();
            model.States = (from s in stateService.GetPartnerStates(NhsRoute.PartnerId) select new SelectListItem { Text = s.StateName, Value = s.StateAbbr });
            model.PriceLowRange = (from l in lookupService.GetCommonListItems(CommonListItem.MinPrice) select new SelectListItem { Text = l.LookupText, Value = l.LookupValue });
            model.PriceHighRange = (from l in lookupService.GetCommonListItems(CommonListItem.MaxPrice) select new SelectListItem { Text = l.LookupText, Value = l.LookupValue });
            model.SearchText = string.Empty;
            model.IsInactive = true;
            return model;
        }

        public static MvcHtmlString GetInterestListLink(this HtmlHelper htmlHelper, CommunityDetailViewModel model)
        {
            var logMethod = string.Format("jQuery.SetDataLayer('{2}','Mobile – {0} {1}');", model.IsPageCommDetail ? "CD" : "HD", "Add me to the interest list", DataLayerConst.CtaCode);
            var paramz = new List<RouteParam> { 
                new RouteParam(RouteParams.Builder, model.BuilderId.ToString(), RouteParamType.QueryString),
                new RouteParam(RouteParams.MarketId, model.MarketId.ToString(), RouteParamType.QueryString),
                new RouteParam(RouteParams.Community, model.CommunityId.ToString(), RouteParamType.QueryString)
            };
            return htmlHelper.GenerateFreeBrochureLink("Put Me on the Interest List", paramz, logMethod, "btn nhs_ContactBtn nhs_InterestListBtn");
        }

    }
}
