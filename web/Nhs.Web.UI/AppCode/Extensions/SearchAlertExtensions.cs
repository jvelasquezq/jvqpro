﻿using System.Collections.Generic;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Content;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.Extensions
{
    public static class SearchAlertExtensions
    {
        public static List<RouteParam> FreeBrochureLinkParams(this Community community)
        {
            // set brochure links
            var @params = new List<RouteParam>
            {
                new RouteParam(RouteParams.LeadType, LeadType.Community, RouteParamType.QueryString),
                new RouteParam(RouteParams.LeadAction, LeadAction.FreeBrochure, RouteParamType.QueryString),
                new RouteParam(RouteParams.CommunityList, community.CommunityId.ToString(), RouteParamType.QueryString),
                new RouteParam(RouteParams.Builder, community.BuilderId.ToString(), RouteParamType.QueryString),
                new RouteParam(RouteParams.FromPage, Pages.SavedHomes, RouteParamType.QueryString),
                new RouteParam(RouteParams.Market, community.MarketId.ToString(), RouteParamType.QueryString),
                new RouteParam(RouteParams.KeepThis, "true", RouteParamType.QueryString),
                new RouteParam(RouteParams.TB_iframe, "true", RouteParamType.QueryString)
            };

            return @params;
        } 
    }
}