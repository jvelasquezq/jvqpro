﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Extensions;
using Nhs.Library.Helpers.Property;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Extensions
{
    public static class GlobalExtensions
    {
        public static string GetTopBannerPosition(this HtmlHelper helper, BaseViewModel model)
        {
            return !NhsRoute.CurrentRoute.Function.Equals(Pages.BasicCommunity, StringComparison.CurrentCultureIgnoreCase) ? "Top1" : "Top";
        }

        public static string GetRightTopSkyPosition(this HtmlHelper helper, BaseViewModel model)
        {

            return !NhsRoute.CurrentRoute.Function.Equals(Pages.BasicCommunity, StringComparison.CurrentCultureIgnoreCase) ? "Right1" : "Right2";
        }

        public static MvcHtmlString RecommendedMoreHomesLikeThisLink(this HtmlHelper helper, LeadViewModels.RequestInfoThanksViewModel model)
        {
            List<RouteParam> @params = new List<RouteParam>();
            @params.Add(new RouteParam(RouteParams.Market, model.MarketId));
            @params.Add(new RouteParam(RouteParams.PriceLow, model.PriceLow));
            @params.Add(new RouteParam(RouteParams.PriceHigh, model.PriceHigh));
            var propertyType = model.LeadType == LeadType.Community ? "communities" : "homes";
            var page = model.LeadType == LeadType.Community ? Pages.CommunityResults : Pages.HomeResults;

            return helper.NhsLink(string.Format("Find more new {0} like this one", propertyType), page, @params, new { target = "_top" });
        }

        public static MvcHtmlString GetAdParamsInJson(this AdController adController)
        {
            return adController.Parameters.ToJson().ToMvcHtmlString();
        }
  
        public static MvcHtmlString RecommendedBackToSearchLink(this HtmlHelper helper, LeadViewModels.RequestInfoThanksViewModel model)
        {
            string linkText;
            string page;
            var @params = new List<RouteParam>();

            CommunityResultsView commResults = UserSession.GetItem(SessionConst.CommResultsView) as CommunityResultsView;
            if (commResults != null)
            {
                var searchParams = UserSession.PropertySearchParameters;
                @params = searchParams.GetRouteParams(RouteParamType.Friendly);
                linkText = "Return to my last search";
                page = Pages.CommunityResults;
            }
            else
            {
                // If didn't do a search this is a way return to page where lead was generated
                linkText = "Go back to viewing my previous page";

                if (model.LeadType == LeadType.Community)
                {
                    @params.Add(new RouteParam(RouteParams.Builder, RouteParams.Builder.Value()));
                    @params.Add(new RouteParam(RouteParams.Community, RouteParams.CommunityList.Value()));
                    page = Pages.CommunityDetail;
                }
                else
                {
                    @params.Add(!string.IsNullOrEmpty(RouteParams.SpecList.Value()) ? new RouteParam(RouteParams.SpecId, RouteParams.SpecList.Value()) :
                                                                                      new RouteParam(RouteParams.PlanId, RouteParams.PlanList.Value()));
                    page = Pages.HomeDetail;
                }
            }

            return helper.NhsLink(linkText, page, @params, new { target = "_top" });
        }

        public static MvcHtmlString FreeBrochureThanksBrandLogo(this HtmlHelper helper, LeadViewModels.RequestInfoThanksViewModel model)
        {
            return !string.IsNullOrEmpty(model.BrandLogo) ? helper.NhsImage(model.BrandLogo, model.BrandName, true) :
                                                            MvcHtmlString.Empty;
        }


        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IEqualityComparer<TKey> comparer)
        {
            HashSet<TKey> knownKeys = new HashSet<TKey>(comparer);
            foreach (TSource element in source)
            {
                if (knownKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }

        public static IEnumerable<T> ForEach<T>(this IEnumerable<T> collection, Action<T> action)
        {
            foreach (var item in collection)
            {
                action(item);
            }
            return collection;
        }

        public static List<RouteParam> GetParamsBackToSearch(this PartialViewModels.BreadcrumbViewModel viewModel)
        {
            var paramz = new List<RouteParam>();
            
            // comes from a synthetic search
            if (UserSession.SearchParametersV2 != null && UserSession.SearchParametersV2.IsMultiLocationSearch)
            {
                paramz.Add(new RouteParam(RouteParams.SyntheticName,
                                          UserSession.SearchParametersV2.SyntheticInfo.Name.Replace(" ", "-").ToLower(),
                                          RouteParamType.FriendlyNameOnly));
            }
            else
            {
                if (viewModel.MarketId > 0)
                    paramz.Add(new RouteParam(RouteParams.Market, viewModel.MarketId));

                if (!string.IsNullOrEmpty(viewModel.CountyName))
                    paramz.Add(new RouteParam(RouteParams.County, viewModel.CountyName));
                else if (!string.IsNullOrEmpty(viewModel.ZipCode))
                    paramz.Add(new RouteParam(RouteParams.PostalCode, viewModel.ZipCode));
                else if (!string.IsNullOrEmpty(viewModel.CityName))
                {
                    paramz.Add(new RouteParam(RouteParams.City, viewModel.CityName));
                }
            }
            return paramz.ToResultsParams();
        }

        //51771
        public static MvcHtmlString GetOnPageLeadFormHeader(this HtmlHelper helper, string bcType, bool isOnlyNhs)
        {
            var headerTxt = isOnlyNhs ? LanguageHelper.FreeDetailedBrochure.ToMvcHtmlString() : LanguageHelper.FreeBrochureAndMap.ToMvcHtmlString();
            return bcType.ToLower() != BuilderCommunityType.ComingSoon.ToLower() ? headerTxt : LanguageHelper.AddMeToTheInterestList.ToMvcHtmlString();
        }

        public static MvcHtmlString GetOnPageLeadFormSubHeader(this HtmlHelper helper, IDetailViewModel model)
        {            
            var headerText = new StringBuilder();
            if(model.IsLoggedInPaid)
            {
                headerText.Append("Send a brochure to your clients, contact the builder, save this {0} and much more.");
            }
            else if (model.IsLoggedInUnpaid)
            {
                headerText.Append("Send a brochure to your clients, save this {0} and much more.");
            }
            else if (model.IsLoggedOutPaid)
            {
                var loginLink = helper.NhsModalWindowLink("Sign in", Pages.LoginModal, ModalWindowsConst.SignInModalWidth, ModalWindowsConst.SignInModalHeight, new List<RouteParam>(),
                                       new
                                       {
                                           title = "Sign in",
                                           onclick = "return false;"
                                       });
                var registerLink = helper.NhsModalWindowLink("register", Pages.RegisterModal, ModalWindowsConst.RegisterModalWidth, ModalWindowsConst.RegisterModalHeight, new List<RouteParam>(),                
                                       new
                                       {
                                           title = "register",
                                           onclick = "return false;"
                                       });
                headerText.Append(string.Format("{0} or {1} {2}",loginLink, registerLink,"for free to send a brochure to your clients, contact the builder, save this {0} and much more."));
            }
            else if (model.IsLoggedOutUnpaid)
            {
                var loginLink = helper.NhsModalWindowLink("Sign in", Pages.LoginModal, ModalWindowsConst.SignInModalWidth, ModalWindowsConst.SignInModalHeight, new List<RouteParam>(),
                                       new
                                       {
                                           title = "Sign in",
                                           onclick = "return false;"
                                       });
                var registerLink = helper.NhsModalWindowLink("register", Pages.RegisterModal, ModalWindowsConst.RegisterModalWidth, ModalWindowsConst.RegisterModalHeight, new List<RouteParam>(),
                                       new
                                       {
                                           title = "register",
                                           onclick = "return false;"
                                       });
                headerText.Append(string.Format("{0} or {1} {2}", loginLink, registerLink, "for free to send a brochure to your clients, save this {0} and much more."));
            }

            return string.Format(headerText.ToString(), model.IsPageCommDetail ? "community" : "home").ToMvcHtmlString();
        }

        public static IEnumerable<T> Randomize<T>(this IEnumerable<T> source)
        {
            var rnd = new Random(DateTime.Now.Millisecond);
            return source.OrderBy(item => rnd.Next());
        }
        public static string ToFormattedThousands(this int number)
        {
            if (number < 999)
                return string.Empty;

            if (number >= 1000000 && number < 1100000)
                return @"$1 millions";

            var priceRounded = ((int)Math.Round(number / 10000.0)) * 10000;
            var digitsToDisplay = "$" + string.Format("{0:n0}", priceRounded);

            if (priceRounded < 1000000)
                digitsToDisplay = digitsToDisplay.Split(',')[0] + "s";
            else
            {
                var digitsArray = digitsToDisplay.Split(',');
                digitsToDisplay = digitsArray[0] + "." + digitsArray[1][0] + " millions";
            }

            return digitsToDisplay;
        }

    }
}
