﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using Bdx.Web.Api.Objects;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Helpers.Content;
using Nhs.Library.Helpers.Lead;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Proxy;
using Nhs.Library.Web;
using Nhs.Library.Web.ImageResizer;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult;

namespace Nhs.Web.UI.AppCode.Extensions
{
    public static class CommunityResultExtension
    {
        //Google Analytics 
        //_gaq.push(['_trackPageview',' http://www.newhomesource.com/communityresults/market-269?q=Austin%2C%20TX' ]); 
        public static string GetTrackPageview(this CommunityResultsViewModel model)
        {
            if (NhsRoute.BrandPartnerId != NhsRoute.PartnerId)
                return string.Empty;

            var url = HttpContext.Current.Request.Url;
            var track = String.Format("{0}?q={1}", url, string.IsNullOrEmpty(model.PostalCode) ? model.LocationName.Replace(", ", "%2C%20") : model.PostalCode);
            return String.Format("$jq(document).ready(function(){{ _gaq.push(['_trackPageview','{0}' ]); }});", track);
        }

        public static MvcHtmlString GetCommResultsHeaderText(this CommunityResultsViewModel model)
        {
            var searchParams = (PropertySearchParams)new JavaScriptSerializer().Deserialize(model.SearchParametersJSon, typeof(PropertySearchParams));
            var locationText = string.Format("{0},{1}", model.Market.MarketName, model.Market.StateAbbr);

            if (!string.IsNullOrEmpty(searchParams.City))
                locationText = string.Format("{0},{1}", searchParams.City, model.Market.StateAbbr);

            if (!string.IsNullOrEmpty(searchParams.CityNameFilter))
                locationText = string.Format("{0},{1}", searchParams.CityNameFilter, model.Market.StateAbbr);

            if (!string.IsNullOrEmpty(searchParams.County))
                locationText = string.Format("{0} County,{1}", searchParams.County, model.Market.StateAbbr);

            if (!string.IsNullOrEmpty(searchParams.PostalCode))
                locationText = string.Format("{0},{1}", searchParams.PostalCode, model.Market.StateAbbr);

            return MvcHtmlString.Create(string.Format("New Homes in {0}", locationText));
        }

        /// <summary>
        /// Retrieves the community price range
        /// </summary>
        /// <param name="communityResult">Community item</param>
        /// <returns>Price range string</returns>
        public static MvcHtmlString CommunityItemPriceRange(this CommunityResult communityResult)
        {
            if (communityResult.PriceLow == int.MaxValue) //bug in search web service
                communityResult.PriceLow = 0;

            if (communityResult.PriceHigh == int.MaxValue)
                communityResult.PriceHigh = 0;

            if (communityResult.PriceLow == 0 && communityResult.PriceHigh == 0)
                return string.Empty.ToMvcHtmlString();

            const string format = "$###,####";
            return string.Format(LanguageHelper.From + " <strong>{0} - {1}</strong>", communityResult.PriceLow.ToString(format), communityResult.PriceHigh.ToString(format)).ToMvcHtmlString();
        }

        /// <summary>
        /// Retrieves the community price range
        /// </summary>
        /// <param name="communityResult">Community item</param>
        /// <returns>Price range string</returns>
        public static MvcHtmlString CommunityItemPriceRange(this ApiRecoCommunityResult communityResult)
        {
            if (communityResult.PriceLow == int.MaxValue) //bug in search web service
                communityResult.PriceLow = 0;

            if (communityResult.PriceHigh == int.MaxValue)
                communityResult.PriceHigh = 0;

            if (communityResult.PriceLow == 0 && communityResult.PriceHigh == 0)
                return string.Empty.ToMvcHtmlString();

            const string format = "$###,####";
            return string.Format(LanguageHelper.From+" {0} - {1}", communityResult.PriceLow.ToString(format), communityResult.PriceHigh.ToString(format)).ToMvcHtmlString();
        }
        

        /// <summary>
        /// Retrieves the community price range
        /// </summary>
        /// <param name="communityResult">Community item</param>
        /// <returns>Price range string</returns>
        public static MvcHtmlString CommunityItemPriceRangeShort(this CommunityResult communityResult)
        {
            if (communityResult.PriceLow == int.MaxValue) //bug in search web service
                communityResult.PriceLow = 0;

            if (communityResult.PriceLow == 0)
                return string.Empty.ToMvcHtmlString();

            const string format = "$###,###";
            return string.Format(LanguageHelper.From.ToTitleCase() + " {0}", communityResult.PriceLow.ToString(format)).ToMvcHtmlString();
        }

        /// <summary>
        /// Retrieves the picture or video thumbnail image to display in the community item
        /// </summary>
        /// <param name="communityResult">Community item</param>
        /// <returns>Thumbnail path</returns>
        public static string CommunityItemLocation(this CommunityResult communityResult)
        {
            return string.Format("{0}, {1} {2}", communityResult.City, communityResult.State, communityResult.PostalCode);
        }

        public static string CommunityItemLocationNoPostal(this CommunityResult communityResult)
        {
            return string.Format("{0}, {1}", communityResult.City, communityResult.State);
        }

        public static string CommunityItemLocation(this Community community)
        {
            return string.Format("{0}, {1} {2}", community.City, community.State.StateName, community.PostalCode);
        }

        public static string BasiListinItemLocation(this CommunityResult communityResult)
        {
            return string.Format("{0}, {1} {2}", communityResult.City, communityResult.State, communityResult.PostalCode);
        }

        public static string BasicListingBedrooms(this ExtendedCommunityResult basicListingResult)
        {
            if (string.IsNullOrEmpty(basicListingResult.BasicListingInfo.Bedrooms) || basicListingResult.BasicListingInfo.Bedrooms.ToType<Int32>() == 0)
                return string.Empty;

            return string.Format(@"{0}<abbr title=""bedrooms"">br</abbr>", basicListingResult.BasicListingInfo.Bedrooms);
        }

        public static string BasicListingBathrooms(this ExtendedCommunityResult basicListingResult)
        {
            var bathrooms = ListingUtility.ComputeBasicListingBathrooms(basicListingResult.BasicListingInfo.Bathrooms);

            if (string.IsNullOrEmpty(bathrooms))
                return string.Empty;

            return string.Format(@"{0}<abbr title=""bathrooms"">ba</abbr>", bathrooms);
        }

        /// <summary>
        /// Retrieves the picture or video thumbnail image to display in the community item
        /// </summary>
        /// <param name="communityResult">Community item</param>
        /// <returns>Thumbnail path</returns>
        public static string CommThumbnail(this ExtendedCommunityResult communityResult)
        {
            var thumbnail = communityResult.CommunityImageThumbnail;
            return CommThumbnail(thumbnail);
        }

        /// <summary>
        /// Retrieves the picture or video thumbnail image to display in the community item
        /// </summary>
        /// <param name="communityResult">Community item</param>
        /// <returns>Thumbnail path</returns>
        public static string AlterCommThumbnail(this ExtendedCommunityResult communityResult)
        {
            var thumbnail = communityResult.AlernativeCommunityImageThumbnail;
            if (!thumbnail.IsValidImage())
                thumbnail = communityResult.CommunityImageThumbnail;

            return CommThumbnail(thumbnail);
        }

        public static string CommThumbnail(this CommunityResult community)
        {
            //string thumbnail;
            //if (string.IsNullOrWhiteSpace(community.SpotlightThumbnail) || community.SpotlightThumbnail == "N")
            //    thumbnail = "[resource:]/globalresources/default/images/spotlight_nophoto.gif";
            //else
            //    thumbnail= Configuration.ResourceDomain +
            //        community.SpotlightThumbnail.ToLower().Replace(".gif", ".jpg").Replace("small", "spotlight");

            return CommThumbnail(community.BrandImageThumbnail);
        }

        public static string CommThumbnail(string thumbnail)
        {
            thumbnail = ImageResizerUrlHelpers.FormatToIrsImageName(thumbnail);
            if (thumbnail.IsValidImage())
                thumbnail = Configuration.IRSDomain + thumbnail + "?" + ImageSizes.GetResizeCommands(ImageSizes.CommDetailThumb);
            else
                thumbnail = Resources.GlobalResources14.Default.images.no_photo.no_photos_180x120_png;

            return thumbnail;
        }

        public static string BasicListingThumbnail(this ExtendedCommunityResult communityResult)
        {
            string thumbnail = communityResult.CommunityImageThumbnail;
            return !string.IsNullOrEmpty(thumbnail) ? (Configuration.ResourceDomain + thumbnail) : Resources.GlobalResources14.Default.images.no_photo.no_photos_75x58_png;
        }
        
        public static string RecCommThumbnail(this ApiRecoCommunityResult communityResult)
        {
            return RecCommThumbnail(communityResult.CommunityImageThumbnail);
        }

        public static string RecCommThumbnail(this CommunityResult communityResult)
        {
            return RecCommThumbnail(communityResult.CommunityImageThumbnail);
        }

        public static string RecCommThumbnail(string communityResult)
        {
            if (!string.IsNullOrEmpty(communityResult) && communityResult != "N")
            {
                communityResult = communityResult.Replace(ImageSizes.Small, ImageSizes.Spotlight);
                if (!communityResult.StartsWith("http"))
                    communityResult = Configuration.ResourceDomain + communityResult;
            }
            else
                communityResult =  Resources.GlobalResources14.Default.images.no_photo.no_photos_75x58_png;

            return communityResult;
        }

        public static string QuickViewThumbnail(this Image image)
        {
            var thumbnail = image.ImageName != "N" && !string.IsNullOrEmpty(image.ImageName)
                                ? Configuration.ResourceDomain + image.ImagePath + ImageSizes.QuickViewThumb + "_" + image.ImageName.Replace(Path.GetExtension(image.ImageName) ?? string.Empty, ".jpg")
                                : Resources.GlobalResources14.Default.images.no_photo.no_photos_240x160_png;
            return thumbnail;
        }

        public static string QuickViewHomeThumbnail(this HomeResult homeResult)
        {
            string thumbnail = homeResult.PlanImageThumbnail;
            if (thumbnail != string.Empty && thumbnail != "N")
                thumbnail = string.Concat(Configuration.ResourceDomain, thumbnail.Replace(Path.GetExtension(thumbnail) ?? string.Empty, ".jpg").Replace(ImageSizes.Small, ImageSizes.QuickViewThumb));
            else
                thumbnail = Resources.GlobalResources14.Default.images.no_photo.no_photos_240x160_png;

            return thumbnail;
        }

        public static string GetAlternateTextForFeaturedListing(this CommunityResult result)
        {
            return HttpUtility.HtmlEncode(result.CommunityName + " - " + result.City + ", " + result.State);
        }

        public static string FeaturedListingThumbnail(this CommunityResult communityResult)
        {
            string thumbnail = communityResult.CommunityImageThumbnail;

            if (thumbnail != string.Empty && thumbnail != "N")
            {

                thumbnail = ImageResizerUrlHelpers.FormatImageUrlForIrs(thumbnail);
                thumbnail = ImageResizerUrlHelpers.BuildIrsImageUrl(thumbnail, ImageSizes.HomeThumb);

                /*thumbnail = Configuration.ResourceDomain +
                            thumbnail.Replace(Path.GetExtension(thumbnail) ?? string.Empty, ".jpg")
                                .Replace(ImageSizes.Small, ImageSizes.HomeThumb)
                                .Replace(ImageSizes.Results + "_", string.Empty);*/
            }
            else
            {
                thumbnail =  Resources.GlobalResources14.Default.images.no_photo.no_photos_180x120_png;
            }

            return thumbnail;
        }

        public static ActionResult CdShowAction(this ExtendedCommunityResult communityResult)
        {
            return NhsMvc.CommunityDetail.Show(communityResult.CommunityId, communityResult.BuilderId);
        }

        public static object CdLinkAttribs(this ExtendedCommunityResult communityResult)
        {
            string function = "";
            if (communityResult.HasVideo)
                function = string.Format("showQuickView({0});return;", communityResult.CommunityId);
            return new
            {
                onclick = function
            };
        }

        public static object CdImgAttribs(this ExtendedCommunityResult communityResult)
        {
            return new { alt = communityResult.CommunityName + " by " + communityResult.BrandName };
        }

        public static List<RouteParam> BasicComParams(this ExtendedCommunityResult result)
        {
            var paramz = new List<RouteParam>
            {
                new RouteParam(RouteParams.Community, result.CommunityId)
            };
            return paramz;
        }

        public static List<RouteParam> SLParams(this SpotLightCommunity result)
        {
            var paramz = new List<RouteParam>
            {
                new RouteParam(RouteParams.CommunityId, result.CommunityId),
                new RouteParam(RouteParams.BuilderId, result.BuilderId),
                new RouteParam(RouteParams.MarketId, UserSession.PropertySearchParameters.MarketId),
                new RouteParam(RouteParams.LogEvent, LogImpressionConst.CommResVideoTour)
            };
            return paramz;
        }

        public static String BlParamsBasicListingHref(this ExtendedCommunityResult result)
        {
            return StringHelper.RemoveSpecialCharacters(String.Format("{0}-{1}-{2}-{3}", result.CommunityName, result.City, result.State, result.PostalCode).Trim()).Replace(" ", "-").Replace("--", "-");
        }

        public static String ParamsBasicCommunityHref(this ExtendedCommunityResult result)
        {
            return StringHelper.RemoveSpecialCharacters(String.Format("{0}-{1}-{2}-{3}", result.CommunityName, result.City, result.State, result.PostalCode).Trim()).Replace(" ", "-").Replace("--", "-");
        }

        public static string VideoLinkClickThruScript(this ExtendedCommunityResult result)
        {
            return result.Video != null
                       ? string.Format("NHS.Scripts.MediaPlayer.loadVideoByTag('{0}', 'sv', '{1}',1, false, {2});",
                                       result.Video.RefId, result.Video.Title, result.Video.CommunityId)
                       : string.Empty;
        }


        public static MvcHtmlString VideoIconLink(this HtmlHelper helper, ExtendedCommunityResult result)
        {
            var videoPlaybackScriptUrl = result.VideoLinkClickThruScript();
            var attributes = new
            {
                href = "#VideoAnchor",
                rel = "nofollow",
                @class = "nhs_ResultVideoLink",
                @title = "View Video",
                onclick = videoPlaybackScriptUrl + "NHS.Scripts.Helper.stopBubble(event);"
            };


            var logInfo = new
            {
                communityId = result.CommunityId.ToString(),
                communityName = result.CommunityName.RemoveSpecialCharacters(),
                builderId = result.BuilderId.ToString(),
                marketId = result.MarketId.ToString(),
                builderName = result.BrandName.RemoveSpecialCharacters(),
                marketName = result.MarketName.RemoveSpecialCharacters()
            };

            if (string.IsNullOrEmpty(videoPlaybackScriptUrl) && result.CommunityVideoTour != null && !string.IsNullOrEmpty(result.CommunityVideoTour.OriginalPath))
                return helper.NhsLinkLogAndRedirect("Video", PathHelpers.CleanNavigateUrl(result.CommunityVideoTour.OriginalPath), LogImpressionConst.CommunityResultsExternalVideo, logInfo, null,
                                            new
                                            {
                                                rel = "nofollow",
                                                @class = "nhs_ResultVideoLink",
                                                @title = "View External Video",
                                                target = "_blank"
                                            });

            return helper.NhsLink("Video", string.Empty, new List<RouteParam>(), attributes, true);
        }

        /// <summary>
        /// Returns the javascript code for the [Map this] button functionality
        /// </summary>
        /// <param name="communityResult">Community item</param>
        /// <returns>Map javascript code</returns>
        public static string CommunityOnMapJsHoverFunction(this ExtendedCommunityResult communityResult, bool show)
        {
            if (communityResult.Latitude != 0)
            {
                string functionName = show ? "viewCommunityOnMapOnHover" : "hideCommunityOnMapOnHover";
                StringBuilder sb1 = new StringBuilder();
                sb1.Append(string.Format("if(commResults) commResults.get_map().{0}(event,", functionName));
                sb1.Append("'" + communityResult.CommunityId + "',");
                sb1.Append("'" + communityResult.BuilderId + "');");
                sb1.Append("return false;");

                return sb1.ToString();
            }

            return "";
        }


        public static string BasicListinOnMapJsHoverFunction(this ExtendedCommunityResult communityResult, bool show)
        {
            return communityResult.CommunityOnMapJsHoverFunction(show);
        }
        public static MvcHtmlString MlsResultsLink(this HtmlHelper htmlHelper, CommunityResultsViewModel viewModel, string linkText, bool addLi = false)
        {
            var @params = new List<RouteParam>
            {
                new RouteParam(RouteParams.State, viewModel.Market.StateAbbr, RouteParamType.Friendly, true, false),
                new RouteParam(RouteParams.Market, viewModel.Market.MarketId, RouteParamType.Friendly, true, false)
            };

           var link = htmlHelper.NhsLink(linkText, @params, Pages.MlsHomeResults, true);

            return MvcHtmlString.Create(addLi ? string.Format("<li>{0}</li>", link) 
                                              : string.Format("{0}", link));
        }

        public static MvcHtmlString HomeResultsLink(this HtmlHelper htmlHelper, CommunityResultsViewModel viewModel, string linkText)
        {
            var @params = new List<RouteParam>();
            @params.Add(new RouteParam(RouteParams.State, viewModel.Market.StateAbbr, RouteParamType.Friendly, true, false));
            @params.Add(new RouteParam(RouteParams.Market, viewModel.Market.MarketId, RouteParamType.Friendly, true, false));
            return htmlHelper.NhsLink(linkText, @params, Pages.HomeResults, true);
        }

        public static MvcHtmlString CommunityResultsLink(this HtmlHelper htmlHelper, string linkText, int marketId)
        {
            var @params = new List<RouteParam>
            {
                new RouteParam(RouteParams.Market, marketId, RouteParamType.Friendly, true, false)
            };
            return htmlHelper.NhsLink(linkText, @params, Pages.CommunityResults, true);
        }
        public static MvcHtmlString HomeResultsLink(this HtmlHelper htmlHelper, string linkText, int marketId)
        {
            var @params = new List<RouteParam>
            {
                new RouteParam(RouteParams.Market, marketId, RouteParamType.Friendly, true, false)
            };
            return htmlHelper.NhsLink(linkText, @params, Pages.HomeResults, true);
        }

        public static string GetBrandThumbnail(this CommunityResult communityResult, Brand brand, bool useMedium = false)
        {
            communityResult.BrandImageThumbnail = brand == null ? string.Empty : brand.BrandThumbnail();
            return useMedium ? brand.BrandThumbnailMedium() : communityResult.BrandImageThumbnail;
        }

        public static string GetBrandThumbnail(this ApiRecoCommunityResultEx communityResult, Brand brand, bool useMedium = false)
        {
            communityResult.BrandImageThumbnail = brand == null ? string.Empty : brand.BrandThumbnail();
            return useMedium ? brand.BrandThumbnailMedium() : communityResult.BrandImageThumbnail;
        }

        /// <summary>
        /// Returns the [Free brochure] button Url
        /// </summary>
        /// <param name="communityResult">Community item</param>
        /// <returns>Brochure Url</returns>
        public static List<RouteParam> FreeBrochureParams(this CommunityResult communityResult)
        {
            var @params = new List<RouteParam>
                {
                    new RouteParam(RouteParams.LeadType, LeadType.Community,RouteParamType.QueryString),
                    new RouteParam(RouteParams.LeadAction, LeadAction.FreeBrochure, RouteParamType.QueryString),
                    new RouteParam(RouteParams.CommunityList, communityResult.CommunityId, RouteParamType.QueryString),
                    new RouteParam(RouteParams.Builder, communityResult.BuilderId, RouteParamType.QueryString),
                    new RouteParam(RouteParams.FromPage, Pages.CommunityResults, RouteParamType.QueryString),
                    new RouteParam(RouteParams.Market, communityResult.MarketId, RouteParamType.QueryString),
                    new RouteParam(RouteParams.IsBilled, communityResult.IsBilled, RouteParamType.QueryString)
                };
            return @params;
        }
        
        public static string CommunityDetailsExtLink(this NearbyCommunity community, string eventCode, string extraParams)
        {
            var url = HttpUtility.UrlEncode(PathHelpers.GetAbsoluteURL(community.ToCommunityDetail().ToUrl(RedirectionHelper.GetCommunityDetailPage())) + extraParams);
            var paramz = new List<RouteParam>
            {
                new RouteParam(RouteParams.LogEvent, eventCode),
                new RouteParam(RouteParams.Builder, community.BuilderId),
                new RouteParam(RouteParams.Community, community.CommunityId),
                new RouteParam(RouteParams.ExternalUrl, url)
            };
            return PathHelpers.GetAbsoluteURL(paramz.ToUrl(Pages.LogRedirect, false));
        }

        public static string ViewBrochureLink(this NearbyCommunity community, LeadEmailViewModel model, string extraParams)
        {
            var logUrl = new List<RouteParam> { new RouteParam(RouteParams.LogEvent, LogImpressionConst.EmailRecommendedClickThrough, RouteParamType.Friendly, true, false) };
            string urlPart;
            var brochureUrl = new List<RouteParam> { new RouteParam(RouteParams.Email, model.Email, RouteParamType.Friendly, true, true) };
            if (!model.OnDemandPdf)
            {
                if (!string.IsNullOrEmpty(UserSession.Refer))
                    brochureUrl.Add(new RouteParam(RouteParams.Refer, UserSession.Refer, RouteParamType.Friendly, true, true));

                brochureUrl.Add(new RouteParam(RouteParams.Community, community.CommunityId, RouteParamType.Friendly, true, true));
                brochureUrl.Add(new RouteParam(RouteParams.Builder, community.BuilderId, RouteParamType.Friendly, true, true));
                brochureUrl.Add(new RouteParam(RouteParams.LeadType, model.RequestType, RouteParamType.Friendly, true, false));
                urlPart = PathHelpers.GetAbsoluteURL(brochureUrl.ToUrl(Pages.BrochureGen) + extraParams);
            }
            else
            {
                urlPart = ViewBrochureHelper.ViewBrochureLink(model.FirstName, community.RequestItemId, model.Email, extraParams);
            }

            if (!string.IsNullOrEmpty(community.NonPdfBrochureUrl) && !NhsRoute.IsBrandPartnerNhsPro)
                urlPart = community.NonPdfBrochureUrl;

            logUrl.Add(new RouteParam(RouteParams.HUrl, CryptoHelper.HashUsingAlgo(HttpUtility.UrlDecode(urlPart) + ":" + LogImpressionConst.EmailRecommendedClickThrough, "md5")));

            return PathHelpers.GetAbsoluteURL(logUrl.ToUrl(Pages.LogRedirect)) + "/?url=" + HttpUtility.UrlEncode(urlPart);
        }

        public static MvcHtmlString RenderPromotedBrandsUrl(this HtmlHelper helper, CommunityResultsViewModel model, Brand brand)
        {
            string brandThumbnail = string.Concat(Configuration.ResourceDomain, brand.BrandThumbnail());
            var @params = new List<RouteParam>();

            if (brand.IsBoyl)
            {
                @params.Add(new RouteParam(RouteParams.Market, model.Market.MarketId, RouteParamType.Friendly, true, true));
                @params.Add(new RouteParam(RouteParams.BrandId, brand.BrandId,RouteParamType.Friendly, true, true));

                return helper.NhsImageLink(brandThumbnail, Pages.BOYLResults, @params, new { @class = "nhs_BoylLink" }, new { alt = brand.BrandName, rel = brand.BrandId.ToString() }, true);
            }
            return helper.NhsImageLink(brandThumbnail, Pages.CommunityResults, brand.CRParams(model.Market.MarketId), new { @class = "nhs_LogoFacet" }, new { alt = brand.BrandName, rel = brand.BrandId.ToString(), onclick = "commResults.get_log().logEvent('CRBR',0," + brand.BrandId + ");" }, true);
        }

        public static MvcHtmlString GetSavePlannerLink(this HtmlHelper htmlHelper, ExtendedCommunityResult viewModel)
        {

            return htmlHelper.GetSavePlannerLink(NhsRoute.IsBrandPartnerNhsPro
                                       ? "Save to Favorites"
                                       : "Save this", viewModel.CommunityId, viewModel.BuilderId, viewModel.SavedPropertysProCrm);
        }

        public static MvcHtmlString GetSavePlannerLink(this HtmlHelper htmlHelper, CommunityResult result)
        {

            return htmlHelper.GetSavePlannerLink(NhsRoute.IsBrandPartnerNhsPro
                                       ? "Save to Favorites"
                                       : "Save this", result.CommunityId, result.BuilderId);
        }
        public static MvcHtmlString AddRemoveSelectedCommunity(this HtmlHelper htmlHelper,
                                                               ExtendedCommunityResult viewModel)
        {
            //<input id="pro_ResultSendBrochure_@(Model.CommunityId)" class="pro_ResultSendBrochure" type="checkbox" onclick=" NHS.Scripts.Helper.stopBubble(event);" communityId="@Model.CommunityId" builderId="@Model.BuilderId" />

            return htmlHelper.CheckBox("pro_ResultSendBrochure_" + viewModel.CommunityId,
                                       UserSession.MultiBrochureList.Contains(viewModel.CommunityId, viewModel.BuilderId),
                                       new
                                           {
                                               id = "pro_ResultSendBrochure_" + viewModel.CommunityId,
                                               @class = "pro_ResultSendBrochure",
                                               onclick = " NHS.Scripts.Helper.stopBubble(event);",
                                               communityId = @viewModel.CommunityId,
                                               builderId = viewModel.BuilderId,
                                               isBilled = viewModel.IsBilled,
                                               isFeatured = viewModel.IsFeatured,
                                               position = viewModel.FeaturedPosition
                                           });
        }

        /// <summary>
        /// Returns the [Free brochure] button Url
        /// </summary>
        /// <param name="communityResult">Community item</param>
        /// <returns>Brochure Url</returns>
        public static List<RouteParam> FreeBrochureParams(this CommunityResultsViewModel communityResult)
        {
            var @params = new List<RouteParam>
                {
                    new RouteParam(RouteParams.LeadType, LeadType.Community, RouteParamType.QueryString),
                    new RouteParam(RouteParams.LeadAction, LeadAction.FreeBrochure, RouteParamType.QueryString),
                    new RouteParam(RouteParams.FromPage, Pages.CommunityResults, RouteParamType.QueryString),
                    new RouteParam(RouteParams.Market, communityResult.Market.MarketId.ToString(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.IsMultiBrochure, "true", RouteParamType.QueryString)
                };
            return @params;
        }

        public static MvcHtmlString AddRemoveSelectedCommunity(this HtmlHelper htmlHelper,
                                                               CommunityResult result)
        {
            //<input id="pro_ResultSendBrochure_@(Model.CommunityId)" class="pro_ResultSendBrochure" type="checkbox" onclick=" NHS.Scripts.Helper.stopBubble(event);" communityId="@Model.CommunityId" builderId="@Model.BuilderId" />

            return htmlHelper.CheckBox("pro_ResultSendBrochure_" + result.CommunityId,
                                       UserSession.MultiBrochureList.Contains(result.CommunityId, result.BuilderId),
                                       new
                                       {
                                           id = "pro_ResultSendBrochure_" + result.CommunityId,
                                           @class = "pro_ResultSendBrochure",
                                           onclick = " NHS.Scripts.Helper.stopBubble(event);",
                                           communityId = @result.CommunityId,
                                           builderId = result.BuilderId,
                                           isBilled = result.IsBilled,
                                           isFeatured = result.IsFeatured,
                                           position = result.FeaturedPosition
                                       });
        }
        /// <summary>
        /// Returns the javascript code for the row hover map functionality
        /// </summary>
        /// <param name="communityResult">Community item</param>
        /// <returns>Map javascript code</returns>
        public static string CommunityOnMapJsClickFunction(this ExtendedCommunityResult communityResult)
        {
            if (communityResult.Latitude != 0)
            {
                // javascript function: ViewCommunityOnMap(cid,bid,name,cty,st,zip,img,brandimg,price,brand,matches,promoid,type,lat,lng,market)
                StringBuilder sb = new StringBuilder();

                sb.Append("if(commResults) commResults.get_map().viewCommunityOnMap(event,");
                sb.Append("'" + communityResult.CommunityId + "',");
                sb.Append("'" + communityResult.BuilderId + "',");
                sb.Append("'" + communityResult.CommunityName.Replace("'", @"\'") + "',"); //Escape apostrophes
                sb.Append("'" + communityResult.City + "',");
                sb.Append("'" + communityResult.State + "',");
                sb.Append("'" + communityResult.PostalCode.Trim() + "',");
                sb.Append("'" + communityResult.CommunityImageThumbnail + "',");
                sb.Append("'" + communityResult.BrandImageThumbnail + "',");
                sb.Append("'" + String.Format("{0:$###,###}", communityResult.PriceLow) + " - " + String.Format("{0:$###,###}", communityResult.PriceHigh) + "',");
                sb.Append("'" + communityResult.BrandName.Replace("'", @"\'") + "',"); //Escape apostrophes
                sb.Append("'" + communityResult.MatchingHomes + "',");
                sb.Append("'" + communityResult.PromoId + "',");
                sb.Append("'" + communityResult.CommunityType + "',");
                sb.Append(communityResult.Latitude + ",");
                sb.Append(communityResult.Longitude + ",");
                sb.Append(communityResult.MarketId + ",");
                sb.Append(communityResult.IsBasicListing.ToString().ToLower() + ");");
                //sb.Append("return false;"); //removed for jump link

                return sb.ToString();
            }
            else
            {
                return "";
            }
        }
        /// <summary>
        /// Returns the javascript code for the row hover map functionality
        /// </summary>
        /// <param name="communityResult">Community item</param>
        /// <returns>Map javascript code</returns>
        public static string CommunityOnMapJsClickFunction(this CommunityResult communityResult)
        {
            if (communityResult.Latitude != 0)
            {
                // javascript function: ViewCommunityOnMap(cid,bid,name,cty,st,zip,img,brandimg,price,brand,matches,promoid,type,lat,lng,market)
                var sb = new StringBuilder();

                sb.Append("if(commResults) commResults.get_map().viewCommunityOnMap(event,");
                sb.Append("&quot;" + communityResult.CommunityId + "&quot;,");
                sb.Append("&quot;" + communityResult.BuilderId + "&quot;,");
                sb.Append("&quot;" + communityResult.CommunityName.Replace("'", @"\'") + "&quot;,"); //Escape apostrophes
                sb.Append("&quot;" + communityResult.City + "&quot;,");
                sb.Append("&quot;" + communityResult.State + "&quot;,");
                sb.Append("&quot;" + communityResult.PostalCode.Trim() + "&quot;,");
                sb.Append("&quot;" + communityResult.CommunityImageThumbnail + "&quot;,");
                sb.Append("&quot;" + communityResult.BrandImageThumbnail + "&quot;,");
                sb.Append("&quot;" + string.Format("{0:$###,###}", communityResult.PriceLow) + " - " + string.Format("{0:$###,###}", communityResult.PriceHigh) + "&quot;,");
                sb.Append("&quot;" + communityResult.BrandName.Replace("'", @"\'") + "&quot;,"); //Escape apostrophes
                sb.Append("&quot;" + communityResult.MatchingHomes + "&quot;,");
                sb.Append("&quot;" + communityResult.PromoId + "&quot;,");
                sb.Append("&quot;" + communityResult.CommunityType + "&quot;,");
                sb.Append(communityResult.Latitude + ",");
                sb.Append(communityResult.Longitude + ",");
                sb.Append(communityResult.MarketId + ",");
                sb.Append("'false');");
                //sb.Append("return false;"); //removed for jump link

                return sb.ToString();
            }
            return string.Empty;
        }

        public static string GetBasicCommunityImage(this ExtendedCommunityResult model)
        {
            var thumbnail = string.IsNullOrWhiteSpace(model.CommunityImageThumbnail) ? "" : model.CommunityImageThumbnail.Replace("//", "/").Replace(":/", "://");
            if (thumbnail.IsValidImage())
                thumbnail = (thumbnail.Contains(Configuration.ResourceDomain) ? string.Empty : Configuration.ResourceDomain) +
                            thumbnail.Replace(Path.GetExtension(thumbnail) ?? string.Empty, ".jpg").Replace(ImageSizes.Small, ImageSizes.HomeThumb).Replace(ImageSizes.Results, ImageSizes.CommDetailThumb).Trim();
            else
                thumbnail =  Resources.GlobalResources14.Default.images.no_photo.no_photos_180x120_png;
            return thumbnail;
        }
    }
}
