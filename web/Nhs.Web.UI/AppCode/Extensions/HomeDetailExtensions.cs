﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Bdx.Web.Api.Objects;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Web.ImageResizer;
using Nhs.Mvc.Biz.Services.Filters;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;
using HomeStatusType = Nhs.Search.Objects.Constants.HomeStatusType;

namespace Nhs.Web.UI.AppCode.Extensions
{
    public static class HomeDetailExtensions
    {
        public static string FormatPrice(this double price)
        {
            return price.ToString("$###,####");
        }

        public static string FormatBathrooms(this IPlan plan)
        {
            return FormatBathrooms(plan, true);
        }

        public static string FormatBathrooms(this IPlan plan, bool withAbbreviation)
        {
            var bathrooms = ListingUtility.ComputeBathrooms(plan.Bathrooms.ToType<int>(), plan.HalfBaths.ToType<int>());

            return withAbbreviation
                       ? string.Format("{0}<abbr title=\"bathrooms\">ba</abbr>", bathrooms)
                       : plan.Bathrooms == 1
                             ? string.Format(@"{0} bathroom", bathrooms)
                             : string.Format(@"{0} bathrooms", bathrooms);

        }
        public static string FormatBedrooms(this IPlan plan, bool withAbbreviation)
        {
            return ListingUtility.FormatBedrooms(plan.Bedrooms.ToType<int>(), withAbbreviation);
        }

        public static string FormatGarages(this IPlan plan)
        {
            return plan.Garages > 0 ? string.Format("{0:0.##}<abbr title=\"car garage\">gr</abbr>", plan.Garages) : string.Empty;
        }

        public static MvcHtmlString HomeDetailLink(this HtmlHelper helper, HomeItem model)
        {
            var pageName = RedirectionHelper.GetHomeDetailPage(model.IsSpec > 0);
         
            return helper.NhsLink(model.ListingTitle(),
                                  model.ToHomeDetail(), pageName);
        }

        public static MvcHtmlString SeeAllHomesAvailableLink(this HtmlHelper helper, HomeDetailViewModel model)
        {
            var linkText = string.Format(LanguageHelper.SeeAll + " {0} " + LanguageHelper.NewHomesAvailable, model.Plan.Community.HomeCount);
        return helper.NhsLink(linkText, RedirectionHelper.GetCommunityDetailPage(), model.ToCommunityDetail(), new { id = "lnkSimilarAvailableHomes" });
        }

        public static MvcHtmlString CommunityDetailLink(this HtmlHelper helper, HomeDetailViewModel model)
        {
            string linkText = model.Plan.Community.CommunityName;
            return helper.NhsLink(linkText, model.Plan.Community.ToCommunityDetail(), RedirectionHelper.GetCommunityDetailPage());
        }

        public static string FloorPlanImage(this IImage image, int height, ImageFormat format = ImageFormat.Jpg)
        {
            var path = image.ImagePath + image.ImageName;

            if (string.IsNullOrEmpty(image.ImagePath) || string.IsNullOrEmpty(image.ImageName))
            {
                path = image.OriginalPath;
            }            

            path = ImageResizerUrlHelpers.FormatImageUrlForIrs(path);
            path = ImageResizerUrlHelpers.BuildIrsImageUrl(path, new ResizeCommands { Width = 780, Height = height, Format = format, Scale = ScaleCommand.UpscaleCanvas, BackgroundColor = "white"});

            return path;            
        }

        public static string FloorPlanImage(this IImage image, string imageSize)
        {
            var path = image.ImagePath + image.ImageName;

            if (string.IsNullOrEmpty(image.ImagePath) || string.IsNullOrEmpty(image.ImageName))
            {
                path = image.OriginalPath;
            }

            path = ImageResizerUrlHelpers.FormatImageUrlForIrs(path);
            path = ImageResizerUrlHelpers.BuildIrsImageUrl(path, imageSize);

            return path;
        }

        public static string FloorPlanImage(this IImage image, ResizeCommands resizeCommands)
        {
            var path = image.ImagePath + image.ImageName;

            if (string.IsNullOrEmpty(image.ImagePath) || string.IsNullOrEmpty(image.ImageName))
            {
                path = image.OriginalPath;
            }

            path = ImageResizerUrlHelpers.FormatImageUrlForIrs(path);
            path = ImageResizerUrlHelpers.BuildIrsImageUrl(path, resizeCommands);

            return path;
        }

        public static MvcHtmlString RenderFloorPlanImage(this HtmlHelper helper, IImage image, int count,
            int planid = 0, int specid = 0, int index = 0, bool isPreviewMode = false)
        {
            var path = image.ImagePath + image.ImageName;

            if (string.IsNullOrEmpty(image.ImagePath) || string.IsNullOrEmpty(image.ImageName))
            {
                path = image.OriginalPath;
            }

            var planMedium = ImageResizerUrlHelpers.BuildIrsImageUrl(ImageResizerUrlHelpers.FormatImageUrlForIrs(path),
                new ResizeCommands { Width = 390, Height = 478, Scale = ScaleCommand.UpscaleCanvas, Format = ImageFormat.None, BackgroundColor = "white"});

            var @params = new List<RouteParam>
                {
                    new RouteParam(RouteParams.PlanId, planid, RouteParamType.QueryString),
                    new RouteParam(RouteParams.SpecId, specid, RouteParamType.QueryString),
                    new RouteParam(RouteParams.PreviewMode, isPreviewMode.ToString(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.SelectImage, index.ToString(), RouteParamType.QueryString),
                    new RouteParam(RouteParams.Width, ModalWindowsConst.FloorPlanGalleryWidth, RouteParamType.QueryString),
                    new RouteParam(RouteParams.Height, string.Empty, RouteParamType.QueryString)
                };

            var planLarge = @params.ToUrl(Pages.GaleryFloorPlan, true, true);
            var imageTitle = string.IsNullOrEmpty(image.ImageTitle) ? "Image" : image.ImageTitle;
            var imageAttributes = new Dictionary<string, object> {{"alt", imageTitle}};
            
            return helper.NhsImageLink(planMedium, planLarge, new List<RouteParam>(), new { title = imageTitle, @class="nhs_floorPlan" }, imageAttributes, false, true);
        }

        public static MvcHtmlString LeadFormLogo(this HtmlHelper helper, IDetailViewModel model)
        {
            if (string.IsNullOrEmpty(model.BuilderUrl))
            {
                if (string.IsNullOrEmpty(model.BrandThumbnail)) return MvcHtmlString.Empty;

                return helper.NhsImage(model.BrandThumbnail, model.BuilderName, false);
            }

            if (string.IsNullOrEmpty(model.BrandThumbnail))
            {
                return helper.NhsLink(string.Format("Visit<br/>{0}", model.BuilderName), model.BuilderUrl, new List<RouteParam>(), new { target = "_blank", title = "" });
            }

            return helper.NhsImageLink(model.BrandThumbnail, model.BuilderUrl, new List<RouteParam>(), new { target = "_blank", title = "" }, new { alt = model.BuilderName }, false, true);
        }


        public static string ChunkHomeDescription(this string description)
        {
            if (string.IsNullOrEmpty(description))
                return string.Empty;

            int descriptionLength = Configuration.PropertyDetailDescriptionLength;
            if (description.Length <= descriptionLength) return description;
            string chunk = description.Substring(0, descriptionLength);

            int lastWordCompleted = chunk.LastIndexOf(' ');
            return lastWordCompleted > 0 ? chunk.Substring(0, lastWordCompleted) : chunk;
        }
        public static int GetPropertyDetailDescriptionLength(this IDetailViewModel model)
        {
            return Configuration.PropertyDetailDescriptionLength;
        }

        public static MvcHtmlString CrossMarketingHomeLink(this HtmlHelper helper, HomeDetailViewModel model, ApiHomeResultV2 home)
        {
            var address = home.Addr;
            var logJs = "log.logEvent('HDNBI', " + model.CommunityId + ", " + model.BuilderId + ", " + model.PlanId + ", " + model.SpecId + "); return true; ";

            var pageName = RedirectionHelper.GetHomeDetailPage(model.SpecId > 0);
            if (string.IsNullOrEmpty(address)) return helper.NhsLink(home.PlanName, pageName, model.ToHomeDetail(), new { onclick = logJs });

            return helper.NhsLink(address, pageName, model.ToHomeDetail(), new { onclick = logJs });
        }

        public static MvcHtmlString SupplementalInfo(this HtmlHelper helper, HomeDetailViewModel model, ApiHomeResultV2 home)
        {
            if (home.Status.ToHomeStatusType() != HomeStatusType.UnderConstruction)
                return LanguageHelper.MoveInNow.ToMvcHtmlString();

            if (home.MoveInDate == null) return LanguageHelper.UnderConstruction.ToMvcHtmlString();

            return new MvcHtmlString(string.Format( LanguageHelper.UnderConstruction + " ({0:d})", home.MoveInDate.ToType<DateTime>()));
        }

        public static MvcHtmlString PriceInfo(this HtmlHelper helper, ApiHomeResultV2 home)
        {
            return new MvcHtmlString(string.Format( LanguageHelper.From + " {0}", home.Price.ToType<decimal>().ToString("$###,####")));
        }

        public static string HomeHeader(this HtmlHelper helper, HomeDetailViewModel model)
        {

            return string.Concat(model.CommunityCity, ", ", model.SelectedStateAbbr, " ", model.ZipCode);
        }

        public static bool VirtualTourExists(this ExtendedHomeResult homeResult, IPlan plan, ISpec spec)
        {
            var virtualTourUrl = string.Empty;
            if (spec != null)
            {
                virtualTourUrl = spec.VirtualTourUrl;
            }
            else
            {
                if (plan != null)
                    virtualTourUrl = plan.VirtualTourUrl;
            }

            return !string.IsNullOrEmpty(virtualTourUrl) && virtualTourUrl != "N";
        }


        public static bool VirtualTourExists(this ApiHomeResultV2 homeResult)
        {
            return !string.IsNullOrEmpty(homeResult.VirtualTour) && homeResult.VirtualTour != "N";
        }

        public static bool FloorPlanExists(this ApiHomeResultV2 homeResult)
        {
            var haveFloorPlan = !string.IsNullOrEmpty(homeResult.FloorPlanPath) && homeResult.FloorPlanPath != "N";
            var havePlanViewer = !string.IsNullOrEmpty(homeResult.PlanViewer) && homeResult.PlanViewer != "N";

            return haveFloorPlan || havePlanViewer;
        }

        public static bool FloorPlanExists(this ExtendedHomeResult homeResult, IPlan plan, ISpec spec, bool useHub)
        {
            var floorPlanImageCount = GetFloorPlanImageCount(plan, spec, useHub);

            if (floorPlanImageCount > 0)
                return true; //We just need to identify whether a floor plan exists

            return GetFloorPlanViewerUrl(plan, spec, useHub).Length > 0;
        }

        private static int GetFloorPlanImageCount(IPlan plan, ISpec spec, bool useHub)
        {
            var imageCount = 0;
            if (spec != null)
            {
                imageCount = spec.SpecImages.AsQueryable().GetByType(ImageTypes.FloorPlan).Count();

                if (imageCount > 0) //else fallback on plan images below
                    return imageCount;
            }

            if (plan != null)
            {
                if (useHub)
                    imageCount = plan.AllPlanHubImages.AsQueryable().GetByType(ImageTypes.FloorPlan).Count();
                else
                    imageCount = plan.AllPlanImages.AsQueryable().GetByType(ImageTypes.FloorPlan).Count();
            }

            return imageCount;
        }

        public static string GetFloorPlanViewerUrl(IPlan plan, ISpec spec, bool useHub)
        {
            IImage image = null;
            if (spec != null)
            {
                image = spec.SpecImages.AsQueryable().GetByType(ImageTypes.EBrochureUrl).FirstOrDefault();
            }
            else
            {
                if (plan != null)
                {
                    if (useHub)
                        image = plan.AllPlanHubImages.AsQueryable().GetByType(ImageTypes.EBrochureUrl).FirstOrDefault();
                    else
                        image = plan.AllPlanImages.AsQueryable().GetByType(ImageTypes.EBrochureUrl).FirstOrDefault();
                }
            }

            return image != null ? image.OriginalPath : string.Empty;
        }

        public static string GetHomePlanName(this ExtendedHomeResult model, IPlan plan, ISpec spec)
        {
            return spec != null ? spec.Plan.PlanName : plan.PlanName;
        }

        public static string GetHomeAddress(this ExtendedHomeResult model)
        {
            return model.SpecId > 0
                ? string.Concat(model.Address1, " ", ", ", model.State, " ", model.PostalCode)
                : string.Concat(model.Address1, " ", model.City, ", ", model.State, " ", model.PostalCode);
        }
    }
}
