﻿using System.Collections.Generic;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;
using System.Web;
using Nhs.Mvc.Routing.Configuration;

namespace  Nhs.Web.UI.AppCode.Extensions
{
    public static class SeoExtensions
    {
       public static MvcHtmlString RenderPromotedBrandsSeoUrl(this HtmlHelper helper, Brand brand, SeoViewModel model)
        {
            string brandThumbnail = string.Concat(Configuration.ResourceDomain, brand.BrandThumbnailMedium());
            var @params = new List<RouteParam>();

            if (brand.IsBoyl)
            {
                @params.Add(new RouteParam { Name = RouteParams.Market, Value = model.MarketId.ToString(), ParamType = RouteParamType.Friendly, LowerCaseParamName = true });
                @params.Add(new RouteParam { Name = RouteParams.BrandId, Value = brand.BrandId.ToString(), ParamType = RouteParamType.Friendly, LowerCaseParamName = true });

                return helper.NhsImageLink(brandThumbnail, Pages.BOYLResults, @params, new { @class = "" }, new { alt = brand.BrandName, rel = brand.BrandId.ToString() }, true);
            }
            return helper.NhsImageLink(brandThumbnail, Pages.CommunityResults, brand.CRParams(model.MarketId), new { @class = "" }, new { alt = brand.BrandName, rel = brand.BrandId.ToString(), onclick = "commResults.get_log().logEvent('CRBR',0," + brand.BrandId + "); " }, true);
        }
    }
}