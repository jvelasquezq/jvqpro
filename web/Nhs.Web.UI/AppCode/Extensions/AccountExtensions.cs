﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using Nhs.Library.Common;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Web.UI.AppCode.HtmlHelpers;

namespace Nhs.Web.UI.AppCode.Extensions
{
    public static class AccountExtensions
    {
        public static MvcHtmlString Link(this HtmlHelper helper, Boolean OtherContent, string linkText, string pageName, string urlparams = "")
        {
            string url = new List<RouteParam>().ToUrl(pageName);
            TagBuilder tagBuilder = new TagBuilder("a") { InnerHtml = linkText };
            tagBuilder.MergeAttribute("href", @"javascript:void(0)");
            if (!string.IsNullOrEmpty(urlparams))
                urlparams = "?" + urlparams;
             
           if(OtherContent)
             tagBuilder.MergeAttribute("onclick", string.Format("window.location = '{0}{1}'", url, urlparams)); 
            else
               tagBuilder.MergeAttribute("onclick", string.Format("tb_ChangeUrl('{0}{1}')", url, urlparams));
            return MvcHtmlString.Create(tagBuilder.ToString());
        }

        public static MvcForm BeginNhsForm(this HtmlHelper helper, Boolean IsAjaxRequest, AjaxHelper Ajax, AjaxOptions ajaxOptions)
        {
            if (IsAjaxRequest)
            {
                return Ajax.BeginNhsForm(NhsMvc.Account.ActionNames.Login, ajaxOptions, true);
            }
            
                return helper.BeginNhsForm(NhsMvc.Account.ActionNames.SignIn, NhsMvc.Account.Name, FormMethod.Post, "");
        }
    }
}
