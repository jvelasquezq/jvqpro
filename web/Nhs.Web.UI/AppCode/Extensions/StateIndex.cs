using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.Extensions
{
    public static class StateIndex
    {
        public static MvcForm BeginNhsFromSearch(this HtmlHelper helper, Dictionary<string, object> htmlAttributes = null)
        {
            var actionUrl = (!string.IsNullOrEmpty(NhsRoute.PartnerSiteUrl)) ? "/" + NhsRoute.PartnerSiteUrl + "/" : "/";

            var formhtmlAttributes = new Dictionary<string, object>
            {
                {"action", actionUrl},
                {"enctype", "multipart/form-data"}
            }.MergeDictionaries(htmlAttributes);

            return helper.BeginForm("action", "controller", FormMethod.Post, formhtmlAttributes);
        }
    }
}