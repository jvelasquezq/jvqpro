﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Routing;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Library.Web.ImageResizer;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Partials;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult;
using Resources;

namespace Nhs.Web.UI.AppCode.Extensions
{
    public static class CommunityResultExtensionsV2
    {
        public static String ParamsBasicCommunityHref(this CommunityItemViewModel result)
        {
            return String.Format("{0}-{1}-{2}-{3}", result.Name ?? result.Addr, result.City, result.State, result.Zip).Trim().ReplaceSpecialCharacters("-").Replace(" ", "-").Replace("--", "-");
        }

        public static string FeaturedCommThumbnail(this string thumbnail)
        {
            if (thumbnail.IsValidImage())
            {
                thumbnail = ImageResizerUrlHelpers.FormatImageUrlForIrs(thumbnail);
                thumbnail = ImageResizerUrlHelpers.BuildIrsImageUrl(thumbnail, ImageSizes.HomeMain);
            }
            else
            {
                thumbnail = GlobalResources14.Default.images.no_photo.no_photos_325x216_png;
            }

            return thumbnail;
        }

        public static MvcHtmlString GetCommunityPrice(this CommunityItemViewModel result)
        {
            string message;

            if (result.IsBasic.ToType<bool>() && result.NumHomes == 0)
                message = string.Empty;
            else
                message =
                    StringHelper.PrettyPrintRangeWithStyles(result.PrLo.ToType<decimal>(), result.PrHi.ToType<decimal>(),
                        "c0").Replace("from", LanguageHelper.From.ToTitleCase());

            return MvcHtmlString.Create(message);
        }

        public static MvcHtmlString GetCommunityPrice(this CommunityMapCard result)
        {
            string message;
            if (result.IsBasic.ToType<bool>() && result.NunHom == 0)
                message = string.Empty;
            else
                message = result.IsBasicListing
                    ? result.PrLo.ToType<decimal>().ToString("c0")
                    : StringHelper.PrettyPrintRangeWithStyles(result.PrLo.ToType<decimal>(),
                        result.PrHi.ToType<decimal>(), "c0").Replace("from", LanguageHelper.From.ToTitleCase());

            return MvcHtmlString.Create(message);
        }
        
        public static string ValidateCommResThumb1(this CommunityItemViewModel result)
        {
            return result.Thumb1.IsValidImage() ? result.Thumb1.RemoveAllSizes() :  GlobalResources14.Default.images.no_photo.no_photos_325x216_png;
        }

        public static string ValidateCommResThumb2(this CommunityItemViewModel result)
        {
            return result.Thumb2.IsValidImage() ? result.Thumb2.RemoveAllSizes() : GlobalResources14.Default.images.no_photo.no_add_120x80_png;
        }

        public static ResizeCommands GetCommResThumb1Size(this CommunityItemViewModel result)
        {
            return new ResizeCommands { MaxWidth = 500, MaxHeight = 333, Format = ImageFormat.Jpg, Scale = ScaleCommand.None };
        }

        public static ResizeCommands GetCommResThumb2Size(this CommunityItemViewModel result)
        {
            return new ResizeCommands { MaxWidth = 180, MaxHeight = 240, Format = ImageFormat.Jpg };
        }

        public static MvcHtmlString GetCommunityStatus(this CommunityItemViewModel result)
        {
            string status;

            switch (result.Status)
            {
                case "G":
                    status = LanguageHelper.GrandOpening;
                    break;
                case "X":
                    status = LanguageHelper.CloseOut;
                    break;
                case "C":
                    status = LanguageHelper.ComingSoon;
                    break;
                default:
                    return MvcHtmlString.Empty;
            }

            return MvcHtmlString.Create(string.Format(@" <p><strong>{0}</strong></p>", status));
        }


        public static MvcHtmlString GetSaveCommPlannerLink(this HtmlHelper htmlHelper, string text,
            bool isSavedToPlanner, int commId = 0,
            int builderId = 0)
        {

            if (!UserSession.UserProfile.IsLoggedIn()) //Redirect to login
                return htmlHelper.GetSignInLink(text, false, "nhs_saveItem" + commId, false, false, "nhs_Save", LanguageHelper.Favorites);

            // Sets link properties
            var parameters = isSavedToPlanner
                                    ? new
                                    {
                                        onclick = string.Format(
                                              "commResults.saveCommunity({0},{1}); jQuery.googlepush('Search Events', 'Search Results', 'Favorite Community');jQuery.SetDataLayerPair('favoriteSaved');NHS.Scripts.Helper.stopBubble(event);return false;",
                                              commId, builderId),
                                        id = "nhs_saveItem_" + commId,
                                        @class = "nhs_Saved",
                                        title = LanguageHelper.Favorites
                                    }
                                    : new
                                    {
                                        onclick = string.Format(
                                            "commResults.saveCommunity({0},{1}); jQuery.googlepush('Community Results Events', 'Community Item', 'Save to favorites');jQuery.googlepush('Search Events', 'Search Results', 'Favorite Community');jQuery.SetDataLayerPair('favoriteSaved');NHS.Scripts.Helper.stopBubble(event); return false;",
                                            commId, builderId),
                                        id = "nhs_saveItem_" + commId,
                                        @class = "nhs_Save",
                                        title = LanguageHelper.Favorites
                                    };

            return htmlHelper.NhsLink(text, string.Empty, new List<RouteParam>(), parameters);
        }

        public static MvcHtmlString GetSaveHomePlannerLink(this HtmlHelper htmlHelper, string text, bool isSavedToPlanner, int commId = 0, int builderId = 0, int homeId = 0, int isSpec = 0)
        {
            if (!UserSession.UserProfile.IsLoggedIn()) //Redirect to login
                return htmlHelper.GetSignInLink(text, false, "nhs_saveItem" + homeId, false, false, "nhs_Save");

            // Sets link properties
            object parameters = isSavedToPlanner
                                    ? new
                                    {
                                        onclick = string.Format("commResults.saveHome({0},{1},{2},{3}); NHS.Scripts.Helper.stopBubble(event); return false;",
                                        commId, builderId, homeId, isSpec),
                                        id = "nhs_saveItem_" + homeId,
                                        @class = "nhs_Saved",
                                        title = LanguageHelper.Favorites
                                    }
                                    : new
                                    {
                                        onclick = string.Format("commResults.saveHome({0},{1},{2},{3}); jQuery.googlepush('Community Results Events', 'Home Item', 'Save to favorites');NHS.Scripts.Helper.stopBubble(event); return false;",
                                        commId, builderId, homeId, isSpec),
                                        id = "nhs_saveItem_" + homeId,
                                        @class = "nhs_Save",
                                        title = LanguageHelper.Favorites
                                    };

            return htmlHelper.NhsLink(text, string.Empty, new List<RouteParam>(), parameters);
        }

        public static MvcHtmlString RenderBrandImage(this HtmlHelper helper, BaseCommunityHomeResultsViewModel model, Brand brand)
        {
            string page = brand.IsBoyl? Pages.BOYLResults : Pages.CommunityResults;
            var brandThumbnail = string.Concat(Configuration.ResourceDomain, brand.BrandThumbnailMedium());
            var attributes = new RouteValueDictionary();
            var @params = new List<RouteParam>
                {
                    new RouteParam(RouteParams.Market, model.Market.MarketId.ToString(CultureInfo.InvariantCulture), RouteParamType.Friendly, true, true),
                    new RouteParam(RouteParams.BrandId, brand.BrandId.ToString(CultureInfo.InvariantCulture), RouteParamType.Friendly, true, true)
                };


            if (!string.IsNullOrEmpty(brand.BrandName) && !attributes.ContainsKey("alt"))
                attributes.Add("alt", brand.BrandName);
            
            if (!string.IsNullOrEmpty(brand.BrandName) && !attributes.ContainsKey("class"))
                attributes.Add("class", "asyncBrand");

            var image = helper.NhsAsyncImageLink(brandThumbnail, page, @params,
                new
                {
                    @class = "nhs_BrandImage nhs_BrandLink" + (brand.IsBoyl ? " isBoyl" : string.Empty),
                    onclick =
                        "if(logger) logger.logEventWithParameters('" + LogImpressionConst.CommResByBuilder +
                        "',  { builderId : " + brand.BrandId + ", marketId : " + model.Market.MarketId +
                        ",  async : false} ); $jq.googlepush('Search Events', 'Search Refinement', 'All Builders') ; ",
                    rel = brand.BrandId.ToString(CultureInfo.InvariantCulture)
                },
                attributes, false);
            return image;
        }

        public static MvcHtmlString RenderShowOnlyResults(this HtmlHelper helper, string marketName, string stateAbbr, int marketId)
        {
            const string logGA = "jQuery.googlepush('Search Event','Search Results','Show only (city, state) Results');";
            var @params = new List<RouteParam>
            {
                new RouteParam(RouteParams.Market, marketId),
                new RouteParam(RouteParams.CityNameFilter, marketName)
            }.ToResultsParams();
           
            var page = RedirectionHelper.GetCommunityResultsPage();

            return helper.NhsLink(LanguageHelper.ShowOnlyResults.Replace("[marketName]", marketName).Replace("[stateAbbr]", stateAbbr), page, @params
             ,
                new { @class = "btnCss", id = "nhs_matchedMetroArea", data = marketName, onclick = logGA });

        }
        public static MvcHtmlString RenderBrandLink(this HtmlHelper helper, BaseCommunityHomeResultsViewModel model, Brand brand)
        {
            var page = brand.IsBoyl ? Pages.BOYLResults : RedirectionHelper.GetCommunityResultsPage();

            var @params = new List<RouteParam>
                {
                    new RouteParam(RouteParams.Market, model.Market.MarketId),
                    new RouteParam(RouteParams.BrandId, brand.BrandId)
                };

            if (!brand.IsBoyl)
                @params = @params.ToResultsParams();

            var url = helper.NhsLink(brand.BrandName, page, @params,
                new { title = brand.BrandName, @class = "nhs_BrandLink" + (brand.IsBoyl? " isBoyl" : string.Empty), rel = brand.BrandId.ToString(CultureInfo.InvariantCulture), onclick = "if(logger) logger.logEventWithParameters('" + LogImpressionConst.CommResByBuilder + "',  { builderId : " + brand.BrandId + ", marketId : " + model.Market.MarketId + ", async : false} ); $jq.googlepush('Search Events', 'Search Refinement', 'All Buildersh'); return false;" }, true);

            return url;
        }
        public static string ToFormattedThousands(this string number)
        {
            return String.Format("{0:n0}", number.ToType<Int32>());
        }

        public static MvcHtmlString RenderBrandSpan(this HtmlHelper helper, BaseCommunityHomeResultsViewModel model, Brand brand)
        {
            var @params = new List<RouteParam>
            {
                new RouteParam(RouteParams.Market, model.Market.MarketId),
                new RouteParam(RouteParams.BrandId, brand.BrandId)
            }.ToResultsParams();

            var tagBuilder = new TagBuilder("span");

            tagBuilder.MergeAttribute("id", "nhs_BasicBrandSpan" + brand.BrandId);
            tagBuilder.MergeAttribute("class", "nhs_BrandLink");
            tagBuilder.MergeAttribute("data-Link", @params.ToUrl(Pages.CommunityResults));
            tagBuilder.MergeAttribute("rel", brand.BrandId.ToString());
            tagBuilder.MergeAttribute("onclick", "if(logger) logger.logEventWithParameters('" + LogImpressionConst.CommResByBuilder + "',  { builderId : " + brand.BrandId + ", marketId : " + model.Market.MarketId + ", async : false} ); $jq.googlepush('Search Events', 'Search Refinement', 'All Buildersh'); return false;");
            tagBuilder.InnerHtml = brand.BrandName;

            return MvcHtmlString.Create(tagBuilder.ToString());
        }

        public static MvcHtmlString GetNoResultsGreaterArea(this HtmlHelper helper, BaseCommunityHomeResultsViewModel model)
        {
            var page = RedirectionHelper.GetCommunityResultsPage();
            var @params = RedirectionHelper.ToResultsParams(model.Market.MarketId);
            var marketArea = string.Empty;
            if (model.Market != null)
                marketArea = string.Format("<li>{0}</li> ",
                    helper.NhsLink(
                        LanguageHelper.SeeAllResultsInTheGreater.Replace("[MarketName]", model.Market.MarketName),
                        page, @params, new {@class = "btnCss", id = "ReloadComResults"}));

            return MvcHtmlString.Create(marketArea);
        }
        public static List<RouteParam> FreeBrochureParamsForComm(this CommunityItemViewModel result)
        {
            var @params = new List<RouteParam> {
                new RouteParam(RouteParams.LeadType, LeadType.Community, RouteParamType.QueryString),
                new RouteParam(RouteParams.LeadAction, LeadAction.FreeBrochure, RouteParamType.QueryString),
                new RouteParam(RouteParams.CommunityList, result.Id.ToString(), RouteParamType.QueryString),
                new RouteParam(RouteParams.Builder, result.BuilderId.ToString(), RouteParamType.QueryString),
                new RouteParam(RouteParams.FromPage, Pages.CommunityResults, RouteParamType.QueryString),
                new RouteParam(RouteParams.Market, result.MarketId.ToString(), RouteParamType.QueryString)
            };
            return @params;
        }

        public static List<RouteParam> FreeBrochureParamsForComm(this CommunityMapCard result)
        {
            var @params = new List<RouteParam> {
                new RouteParam(RouteParams.LeadType, LeadType.Community, RouteParamType.QueryString),
                new RouteParam(RouteParams.LeadAction, LeadAction.FreeBrochure, RouteParamType.QueryString),
                new RouteParam(RouteParams.CommunityList, result.Id.ToString(), RouteParamType.QueryString),
                new RouteParam(RouteParams.Builder, result.BId.ToString(), RouteParamType.QueryString),
                new RouteParam(RouteParams.FromPage, Pages.CommunityResults, RouteParamType.QueryString),
                new RouteParam(RouteParams.Market, result.MId.ToString(), RouteParamType.QueryString)
            };
            return @params;
        }

        public static List<RouteParam> FreeBrochureParamsForHome(this HomeItemViewModel result)
        {
            var @params = new List<RouteParam>();
            @params.Add(new RouteParam(RouteParams.LeadType, LeadType.Home, RouteParamType.QueryString));
            @params.Add(new RouteParam(RouteParams.LeadAction, LeadAction.FreeBrochure, RouteParamType.QueryString));
            if (result.IsSpec == 0)
            {
                @params.Add(new RouteParam(RouteParams.PlanList, result.HomeId.ToString(), RouteParamType.QueryString));
                @params.Add(new RouteParam(RouteParams.PlanId, result.HomeId.ToString(), RouteParamType.QueryString));
            }
            else
            {
                @params.Add(new RouteParam(RouteParams.SpecList, result.HomeId.ToString(), RouteParamType.QueryString));
                @params.Add(new RouteParam(RouteParams.SpecId, result.HomeId.ToString(), RouteParamType.QueryString));
            }
            @params.Add(new RouteParam(RouteParams.Builder, result.BuilderId.ToString(), RouteParamType.QueryString));
            @params.Add(new RouteParam(RouteParams.Community, result.CommId.ToString(), RouteParamType.QueryString));
            @params.Add(new RouteParam(RouteParams.Market, result.MarketId.ToString(), RouteParamType.QueryString));
            @params.Add(new RouteParam(RouteParams.FromPage, Pages.HomeDetail, RouteParamType.QueryString));
            return @params;
        }

        public static MvcHtmlString GetTabUrl(this HtmlHelper helper, CommunityHomeResultsViewModel model, string page)
        {
            string result = string.Empty;
            string hotDeals = string.Empty;
            var qmi = string.Empty;
            if (model.PagingUrl.Contains(UrlConst.HotDeals))
                hotDeals = "?" + UrlConst.HotDeals + "=true";

            if (model.PagingUrl.Contains(UrlConst.SpecHomes) || model.PagingUrl.Contains(UrlConst.Inventory))
                qmi = "/" +  UrlConst.Inventory + "-homes";

            result = model.TabsUrl.Replace("[function]", page);
            var index = model.TabsUrl.IndexOf("?");

            if (index >= 0)
                result = result.Remove(index);

            result += qmi + hotDeals;
            return result.ToMvcHtmlString();
        }
    }
}
