﻿using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;
using Bdx.Web.Api.Objects;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects;
using Nhs.Utility.Common;
using Nhs.Utility.Html;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult;
using Resources;
using HomeStatusType = Nhs.Search.Objects.Constants.HomeStatusType;

namespace Nhs.Web.UI.AppCode.Extensions
{
    public static class FormattingExtensions
    {
        public static string GetBrochureLinkText()
        {
            return NhsRoute.IsBrandPartnerNhsPro ? "Send Brochure" : LanguageHelper.FreeBrochure;
        }

        public static MvcHtmlString FormatSquareFootage(this ApiHomeResultV2 homeResult)
        {
            if (homeResult.Sft == 0) return string.Empty.ToMvcHtmlString();
            return string.Concat(homeResult.Sft.ToString("###,####"), @" <abbr title='square feet'>" + LanguageHelper.MSG_SQ_FT + "</abbr>").ToMvcHtmlString();
        }

        public static MvcHtmlString FormatBedrooms(this ApiHomeResultV2 homeResult)
        {
            return MvcHtmlString.Create(ListingUtility.FormatBedrooms(homeResult.Br.ToType<int>(), true, false, LanguageHelper.Studio, LanguageHelper.BedroomAbbreviation, LanguageHelper.Bedroom, LanguageHelper.Bedrooms));
        }

        public static MvcHtmlString FormatBathrooms(this ApiHomeResultV2 homeResult)
        {
            var bathrooms = ListingUtility.ComputeBathrooms(homeResult.Ba, homeResult.HBa);
            return MvcHtmlString.Create(string.Format("{0} <abbr title=\"bathrooms\">" + LanguageHelper.BathroomAbbreviation + "</abbr>", bathrooms));
        }

        public static MvcHtmlString FormatGarages(this ApiHomeResultV2 homeResult)
        {
            return MvcHtmlString.Create(homeResult.Gr > 0.0m ? string.Format("{0:0.##}" + LanguageHelper.GarageAbbreviation, homeResult.Gr) : string.Empty);
        }

        public static string FormatSquareFootage(this HomeResult homeResult)
        {
            if (homeResult.SquareFeet == 0) return string.Empty;
            return string.Concat(homeResult.SquareFeet.ToString("###,####"), @"<abbr title='square feet'> " + LanguageHelper.MSG_SQ_FT + "</abbr>");
        }

        public static string FormatSquareFootage(this HomeItemViewModel homeResult)
        {
            if (homeResult.Sft == 0) return string.Empty;
            return string.Concat(homeResult.Sft.ToString("###,####"), @"<abbr title='square feet'> sq ft</abbr>");
        }

        public static MvcHtmlString FormatBedrooms(this HomeResult homeResult)
        {
            return MvcHtmlString.Create(ListingUtility.FormatBedrooms(homeResult.NumBedrooms.ToType<int>(), true, false, LanguageHelper.Studio, LanguageHelper.BedroomAbbreviation, LanguageHelper.Bedroom, LanguageHelper.Bedrooms));
        }

        public static MvcHtmlString FormatBedrooms(this HomeItemViewModel homeResult)
        {
            return MvcHtmlString.Create(ListingUtility.FormatBedrooms(homeResult.Br, true));
        }

        public static MvcHtmlString FormatBedrooms(this HtmlHelper helper, string noBedrooms)
        {
            if (noBedrooms.ToType<int>() == 0)
                return MvcHtmlString.Create(LanguageHelper.Studio);
            return MvcHtmlString.Create(string.Format("{0}br", noBedrooms));
        }

        public static string FormatPrice(this int price, int specId)
        {
            if (price == 0)
                return LanguageHelper.PriceNotAvailable;
            return string.Concat(LanguageHelper.From.ToTitleCase(), " ", price.ToString("$###,####"));
        }

        public static string FormatPrice(this int price)
        {
            return price.ToString("$###,####");
        }

        public static string FormatPrice(this decimal price)
        {
            return price.ToString("$###,####");
        }

        public static MvcHtmlString FormatBathrooms(this HomeResult homeResult)
        {
            var bathrooms = ListingUtility.ComputeBathrooms(homeResult.NumBathrooms, homeResult.NumHalfBathrooms);
            return MvcHtmlString.Create(string.Format("{0}<abbr title=\"bathrooms\">" + LanguageHelper.BathroomAbbreviation + "</abbr>", bathrooms));
        }

        public static MvcHtmlString FormatBathrooms(this HomeItemViewModel homeResult)
        {
            var bathrooms = ListingUtility.ComputeBathrooms(homeResult.Ba, homeResult.HBa);
            return MvcHtmlString.Create(string.Format("{0}<abbr title=\"bathrooms\">ba</abbr>", bathrooms));
        }


        public static MvcHtmlString FormatBathrooms(this HtmlHelper helper, string noBathrooms, string noHalfBathrooms)
        {
            var bathrooms = ListingUtility.ComputeBathrooms(noBathrooms.ToType<int>(), noHalfBathrooms.ToType<int>());
            return MvcHtmlString.Create(string.Format("{0}ba", bathrooms));
        }

        public static MvcHtmlString FormatGarages(this HomeResult homeResult)
        {
            return MvcHtmlString.Create(homeResult.NumGarages > 0.0m ? string.Format("{0:0.##}" + LanguageHelper.GarageAbbreviation, homeResult.NumGarages) : string.Empty);
        }

        public static MvcHtmlString FormatGarages(this HomeItemViewModel homeResult)
        {
            return MvcHtmlString.Create(homeResult.Gr > 0 ? string.Format("{0:0.##}" + LanguageHelper.GarageAbbreviation, homeResult.Gr) : string.Empty);
        }

        public static MvcHtmlString FormatGarages(this HtmlHelper helper, string noGarages)
        {
            return MvcHtmlString.Create(noGarages.ToType<int>() > 0 ? string.Format("{0:0.##}" + LanguageHelper.GarageAbbreviation, noGarages) : string.Empty);
        }

        public static string ListingTitle(this HomeResult homeResult)
        {
            if (homeResult.SpecId != 0 & homeResult.Address1 != string.Empty)
                return string.Concat(homeResult.Address1, " <span class=\"nhs_SpecPlanName\">(", homeResult.PlanName,
                                     ")</span>");

            if (homeResult.SpecId != 0)
                return string.Concat("<span class=\"nhs_SpecPlanNameOnly\">", homeResult.PlanName,
                                     "</span>");

            if (homeResult.PlanId != 0 | homeResult.SpecId != 0)
                return homeResult.PlanName;

            return string.Empty;
        }

        public static string ListingTitle(this ExtendedHomeResult homeResult)
        {
            if (homeResult.SpecId != 0 & homeResult.Address1 != string.Empty)
                return string.Concat(homeResult.Address1, " <span class=\"nhs_SpecPlanName\">(", homeResult.PlanName,
                                     ")</span>");

            if (homeResult.SpecId != 0)
                return string.Concat("<span class=\"nhs_SpecPlanNameOnly\">", homeResult.PlanName,
                                     "</span>");

            if (homeResult.PlanId != 0 | homeResult.SpecId != 0)
                return homeResult.PlanName;

            return string.Empty;
        }

        public static string ListingTitle(this ApiHomeResultV2 homeResult)
        {
            if (homeResult.IsSpec > 0 && homeResult.HomeId != 0 & homeResult.Addr != string.Empty)
                return string.Concat(homeResult.Addr, " <span class=\"nhs_SpecPlanName\">(", homeResult.PlanName,
                                     ")</span>");

            if (homeResult.IsSpec > 0 && homeResult.HomeId != 0)
                return string.Concat("<span class=\"nhs_SpecPlanNameOnly\">", homeResult.PlanName,
                                     "</span>");

            if (homeResult.HomeId != 0)
                return homeResult.PlanName;

            return string.Empty;
        }

        public static string ListingTitle(IPlan plan, ISpec spec)
        {
            return spec != null
                ? string.Concat(spec.Address1, " <span class=\"nhs_SpecPlanName\">(", spec.Plan != null ? spec.Plan.PlanName : spec.HubPlan.PlanName,
                                       ")</span>")
                       : plan.PlanName;
        }

        public static string BuildListingTitle(HomeItemViewModel home)
        {
            return home.IsSpec > 0
                ? string.Concat(home.Addr, " <span class=\"nhs_SpecPlanName\">(", home.PlanName,
                                       ")</span>")
                       : home.PlanName;
        }

        public static MvcHtmlString HotHomeLink(this HtmlHelper helper, IPlan plan, ISpec spec)
        {
            var linkText = ListingTitle(plan, spec);

            return spec != null
                ? helper.NhsLink(linkText, new List<RouteParam>
                {
                    new RouteParam(RouteParams.SpecId, spec.SpecId.ToString())
                }, Pages.HomeDetail)
                : helper.NhsLink(linkText, new List<RouteParam>
                {
                    new RouteParam(RouteParams.PlanId, plan.PlanId.ToString())
                }, Pages.HomeDetail);
        }

        public static MvcHtmlString HotHomeLink(this HtmlHelper helper, string planId, string specId, string planName)
        {
            //var linkText = ListingTitle(plan, spec);

            return specId.ToType<int>() > 0
                ? helper.NhsLink(planName, new List<RouteParam>
                {
                    new RouteParam(RouteParams.SpecId, specId)
                },
                    Pages.HomeDetail)
                : helper.NhsLink(planName, new List<RouteParam>
                {
                    new RouteParam(RouteParams.PlanId, planId)
                },
                    Pages.HomeDetail);
        }

        public static MvcHtmlString HotHomeLinkForHomeResult(this HtmlHelper helper, ApiHomeResultV2 homeResult)
        {
            //var linkText = ListingTitle(plan, spec);
            //if (linkText.Contains("()"))
            var linkText = ListingTitle(homeResult);

            return homeResult.IsSpec > 0
                ? helper.NhsLink(linkText, new List<RouteParam>
                {
                    new RouteParam(RouteParams.SpecId, homeResult.HomeId.ToString(CultureInfo.InvariantCulture))
                },
                    Pages.HomeDetail)
                : helper.NhsLink(linkText, new List<RouteParam>
                {
                    new RouteParam(RouteParams.PlanId, homeResult.HomeId.ToString(CultureInfo.InvariantCulture))
                },
                    Pages.HomeDetail);
        }

        public static MvcHtmlString AppendNewLineTag(this string data)
        {
            return MvcHtmlString.Create(string.IsNullOrEmpty(data) ? string.Empty : string.Format("{0}<br />", data));
        }

        public static string TruncatePromotionDescription(this string description)
        {
            return ParseHTML.StripHTML(description);
        }

        public static MvcHtmlString FormatBrandName(this IDetailViewModel model)
        {
            //if (NhsRoute.ShowMobileSite)
            //    model.HasShowCaseInformation = false;
            return model.HasShowCaseInformation
                ? MvcHtmlString.Create(string.IsNullOrEmpty(model.BrandName) ? string.Empty
                : string.Format(LanguageHelper.By + " <a id=\"nhs_builderShowCase\" href=\"{1}\" title=\"" + LanguageHelper.About + " {0}\">{0}</a>",
                                model.BrandName,
                                new List<RouteParam>().ToUrl(string.Format("builder/{0}/about/{1}",
                                    model.BrandName.Replace(" ", "-").Replace("'", string.Empty).Replace("/", string.Empty),
                                    model.BrandId), true)))
                : MvcHtmlString.Create(LanguageHelper.By + " " + model.BrandName);
        }

        public static string FormatSqFeet(this IDetailViewModel model)
        {
            return string.IsNullOrEmpty(model.DisplaySqFeet) ? string.Empty : model.DisplaySqFeet;
        }

        public static string FormatHomesAvailable(this IDetailViewModel model)
        {
            if (string.IsNullOrEmpty(model.HomesAvailable)) return string.Empty;

            return string.Concat(", ", model.HomesAvailable);
        }

        public static string CommunityLocation(this IDetailViewModel model)
        {
            return string.Concat(model.CommunityCity, ", ", model.StateAbbr, " ", model.ZipCode).Trim();
        }

        public static string HoursOfOperation(this IDetailViewModel model)
        {
            if (string.IsNullOrEmpty(model.HoursOfOperation)) return string.Empty;

            return string.Concat(" - Hours: ", model.HoursOfOperation);
        }

        public static MvcHtmlString HomeStatusImage(this HtmlHelper helper, IDetailViewModel model, bool isBcTypeBanner)
        {
            model.BcTypeBanner = isBcTypeBanner;
            return HomeStatusImage(helper, model);
        }
        public static MvcHtmlString HomeStatusImage(this HtmlHelper helper, IDetailViewModel model)
        {
            string resource = string.Empty;
            string altText = string.Empty;
            if (model.DetailType != "home") return CommunityStatusImage(helper, model);
            switch (model.HomeStatus)
            {
                case (int)HomeStatusType.AvailableNow:
                    resource = GlobalResourcesMvc.Default.images.icons.status_available_png;
                    altText = LanguageHelper.AvailableNow;
                    break;
                case (int)HomeStatusType.UnderConstruction:
                    resource = GlobalResourcesMvc.Default.images.icons.status_under_construction_png;
                    altText = LanguageHelper.UnderConstruction;
                    break;
                case (int)HomeStatusType.ReadyToBuild:
                    resource = GlobalResourcesMvc.Default.images.icons.status_ready_png;
                    altText = LanguageHelper.ReadyToBuild;
                    break;
                case (int)HomeStatusType.ModelHome:
                    resource = GlobalResourcesMvc.Default.images.icons.status_model_home_png;
                    altText = LanguageHelper.ModelHome;
                    break;
            }

            if (string.IsNullOrEmpty(resource)) return MvcHtmlString.Empty;

            return helper.NhsImage(resource, altText, new { id = "nhs_HomeTypeImg" }, false);
        }
        public static MvcHtmlString HomeStatusImageBcTypeComing(this HtmlHelper helper, IDetailViewModel model, bool isBcTypeBanner)
        {
            model.BcTypeBanner = isBcTypeBanner;
            return HomeStatusImageBcTypeComing(helper, model);
        }
        public static MvcHtmlString HomeStatusImageBcTypeComing(this HtmlHelper helper, IDetailViewModel model)
        {
            string resource = string.Empty;
            string altText = string.Empty;

            resource = model.BcTypeBanner ? GlobalResourcesMvc.Default.images.icons.icon2_coming_soon_png : GlobalResourcesMvc.Default.images.icons.icon_coming_soon_png;
            altText = LanguageHelper.ComingSoon;

            if (string.IsNullOrEmpty(resource)) return MvcHtmlString.Empty;

            return helper.NhsImage(resource, altText, new { id = "nhs_HomeTypeImgComing" }, false);
        }
        public static MvcHtmlString HomeStatus(this HtmlHelper helper, int homeStatus)
        {
            string homeStatusText = "";
            switch (homeStatus)
            {
                case (int)HomeStatusType.QuickMoveIn:
                case (int)HomeStatusType.AvailableNow:
                    homeStatusText = LanguageHelper.ReadyToMoveIn;
                    break;
                case (int)HomeStatusType.UnderConstruction:
                    homeStatusText = LanguageHelper.UnderConstruction;
                    break;
                case (int)HomeStatusType.ReadyToBuild:
                    homeStatusText = LanguageHelper.ReadyToBuild;
                    break;
                case (int)HomeStatusType.ModelHome:
                    homeStatusText = LanguageHelper.ModelHome;
                    break;
            }

            return MvcHtmlString.Create(homeStatusText);
        }

        private static MvcHtmlString CommunityStatusImage(HtmlHelper helper, IDetailViewModel model)
        {
            string resource;
            string altText = string.Empty;
            switch (model.BcType)
            {
                case "G":
                    resource = GlobalResourcesMvc.Default.images.icons.icon_grand_opening_png;
                    altText = "Grand Opening";
                    break;
                case "X":
                    resource = GlobalResourcesMvc.Default.images.icons.icon_closeout_png;
                    altText = "Close Out";
                    break;
                case "C":
                    resource = model.BcTypeBanner ? GlobalResourcesMvc.Default.images.icons.icon2_coming_soon_png : GlobalResourcesMvc.Default.images.icons.icon_coming_soon_png;
                    altText = "Coming Soon";
                    break;
                default:
                    resource = string.Empty;
                    break;
            }

            if (string.IsNullOrEmpty(resource)) return MvcHtmlString.Empty;

            return helper.NhsImage(resource, altText, new { id = "nhs_CommTypeImg" }, false);

        }

        public static MvcHtmlString GetLeadFormTitleFormConfirmation(this IDetailViewModel model, HtmlHelper helper)
        {
            return !model.NewUser ? (model.BcType.ToLower() == BuilderCommunityType.ComingSoon.ToLower() ? LanguageHelper.YourRequestHasBeenSent.ToMvcHtmlString() : LanguageHelper.YourFreeBrochureIsReadyToView.ToMvcHtmlString()) : LanguageHelper.AccountCreationSuccessful.ToMvcHtmlString();
        }

        public static string FormattedText(this string text)
        {
            if (string.IsNullOrEmpty(text)) return string.Empty;
            var descriptionText = ParseHTML.StripHTML(text);
            return descriptionText.Replace("\r", string.Empty).Replace("\n", string.Empty);
        }
    }
}
