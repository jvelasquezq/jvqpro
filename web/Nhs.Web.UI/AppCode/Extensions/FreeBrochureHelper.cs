﻿using System.Collections.Generic;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Web.Partials;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Utility.Constants;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult;
using Nhs.Web.UI.AppCode.ViewModels.LeadView;

namespace Nhs.Web.UI.AppCode.Extensions
{
    public static class FreeBrochureHelper
    {

        #region Mobile

        public static MvcHtmlString GetContactBuilderModalLink(this HtmlHelper htmlHelper, IDetailViewModel model)
        {
            var logMethod = string.Format("jQuery.SetDataLayer('{1}','Mobile – {0} Contact Builder');", model.IsPageCommDetail ? "CD" : "HD", DataLayerConst.CtaCode);

            var paramz = new List<RouteParam>
            {
                new RouteParam(RouteParams.BuilderId, model.BuilderId,RouteParamType.QueryString),
                new RouteParam(RouteParams.CommunityId, model.CommunityId, RouteParamType.QueryString),
                new RouteParam(RouteParams.MarketId, model.MarketId, RouteParamType.QueryString),
                new RouteParam(RouteParams.PhoneNumber, model.PhoneNumber, RouteParamType.QueryString),
                new RouteParam(RouteParams.PlanId, model.PlanId, RouteParamType.QueryString),
                new RouteParam(RouteParams.SpecId, model.SpecId, RouteParamType.QueryString),
                new RouteParam(RouteParams.IsPageCommDetail, model.IsPageCommDetail, RouteParamType.QueryString),
                new RouteParam(RouteParams.IsPlan, model.IsPlan, RouteParamType.QueryString)
            };

            return htmlHelper.NhsModalWindowLink(LanguageHelper.ContactBuilder, Pages.ContactBuilderModal, 0, 0, paramz,
                new Dictionary<string, object>
                {
                    {"class", "btn nhs_ContactBtn"},
                    {"onclick", logMethod},
                });
        }



        public static MvcHtmlString GetContactBuilderLink(this HtmlHelper htmlHelper, IFreeBrochureLinkInformation model,string text = "")
        {
            if (string.IsNullOrEmpty(text))
                text = LanguageHelper.ContactBuilder;
            var logMethod = string.Format("jQuery.SetDataLayer('{1}','Mobile – {0} Contact – Contact Form');", model.IsPageCommDetail ? "CD" : "HD", DataLayerConst.CtaCode);

            var paramz = GetParamerters(model, LeadAction.ContactBuilder,
                model.IsPageCommDetail ? Pages.CommunityDetail : Pages.HomeDetail);

            return htmlHelper.GenerateFreeBrochureLink(text, paramz, logMethod, "btn nhs_ContactBtn");
        }


        public static MvcHtmlString GetRequestAppointmentLink(this HtmlHelper htmlHelper, IFreeBrochureLinkInformation model, bool onPage, string className = "")
        {
            var logMethod = string.Format(
                "jQuery.SetDataLayer('{2}','Mobile – {0} {1}');", model.IsPageCommDetail ? "CD" : "HD", onPage ? "Page Appointment" : "Contact - Appointment", DataLayerConst.CtaCode);

            var paramz = GetParamerters(model, LeadAction.RequestApointment, model.IsPageCommDetail ? Pages.CommunityDetail : Pages.HomeDetail);
            return htmlHelper.GenerateFreeBrochureLink(LanguageHelper.RequestAppointment, paramz, logMethod, "btn " + className);
        }
        #endregion

        public static MvcHtmlString NhsRequestAppModalLink(this HtmlHelper htmlHelper, string linkText, string className,
            string leadComment, IDetailViewModel model, string id, bool isNextSteps)
        {
            List<RouteParam> @params;
            if (model.Globals.PartnerLayoutConfig.AmIOnCommunityDetailPage)
            {
                if (!isNextSteps)
                    className += " btn_RequestAppointment";
                @params = new List<RouteParam>()
                {
                    new RouteParam {Name = RouteParams.LeadComments, Value = leadComment, ParamType =  RouteParamType.QueryString},
                    new RouteParam {Name = RouteParams.LeadType, Value = LeadType.Community, ParamType =  RouteParamType.QueryString},
                    new RouteParam {Name = RouteParams.LeadAction, Value = LeadAction.CaRequestApointment, ParamType =  RouteParamType.QueryString},
                    new RouteParam {Name = RouteParams.CommunityList, Value = model.CommunityId.ToString(), ParamType =  RouteParamType.QueryString},
                    new RouteParam {Name = RouteParams.FromPage, Value = Pages.CommunityDetail, ParamType =  RouteParamType.QueryString},
                    new RouteParam {Name = RouteParams.Market, Value = model.MarketId.ToString(), ParamType =  RouteParamType.QueryString}
                };
            }
            else
            {
                if (!isNextSteps)
                    className += " btn_RequestAppointment";
                @params = new List<RouteParam>()
                {
                    new RouteParam {Name = RouteParams.LeadComments, Value = leadComment, ParamType =  RouteParamType.QueryString},
                    new RouteParam {Name = RouteParams.LeadType, Value = LeadType.Home, ParamType =  RouteParamType.QueryString},
                    new RouteParam {Name = RouteParams.LeadAction, Value = LeadAction.CaRequestApointment, ParamType =  RouteParamType.QueryString},
                    new RouteParam
                    {
                        Name = RouteParams.SpecList,
                        Value = model.SpecId > 0 ? model.SpecId.ToString() : string.Empty
                    },
                    new RouteParam
                    {
                        Name = RouteParams.SpecId,
                        Value = model.SpecId > 0 ? model.SpecId.ToString() : string.Empty
                    },
                    new RouteParam
                    {
                        Name = RouteParams.PlanList,
                        Value = model.SpecId <= 0 && model.PlanId > 0 ? model.PlanId.ToString() : string.Empty
                    },
                    new RouteParam
                    {
                        Name = RouteParams.PlanId,
                        Value = model.SpecId <= 0 && model.PlanId > 0 ? model.PlanId.ToString() : string.Empty
                    },
                    new RouteParam {Name = RouteParams.FromPage, Value = Pages.HomeDetail},
                };
            }
            @params.Add(new RouteParam { Name = RouteParams.Builder, Value = model.BuilderId.ToString() });
            @params.Add(new RouteParam { Name = RouteParams.Market, Value = model.MarketId.ToString() });
            @params.Add(new RouteParam(RouteParams.Community, model.CommunityId.ToString(), RouteParamType.QueryString));

            return GenerateFreeBrochureLink(htmlHelper, linkText, @params, null, className);
        }



        public static MvcHtmlString GetFreeBrochureBoylLink(this HtmlHelper helper, BoylResult boylResult)
        {
            var brochureParams = boylResult.FreeBrochureParams();
            var linkText = FormattingExtensions.GetBrochureLinkText();
            var onclick = string.Format("commResults.get_log().logEvent('{0}', 0, 0);", LogImpressionConst.CommResFreeBrochure);

            if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
                return GenerateFreeBrochureLink(helper, linkText, brochureParams, onclick, "btnCss btn_FreeBrochureAlt");

            return helper.SendCustomBrochureLink(brochureParams, false, false, UserSession.UserProfile.ActorStatus != WebActors.ActiveUser);
        }

        public static MvcHtmlString GetFreeBrochureLink(this HtmlHelper helper, CommunityItemViewModel model)
        {
            var brochureParams = model.FreeBrochureParamsForComm();
            var linkText = FormattingExtensions.GetBrochureLinkText();
            var onclick = "jQuery.googlepush('Lead Events','Search Results CTA Free Brochure','Open Form - Free Brochure');";
            return GenerateFreeBrochureLink(helper, linkText, brochureParams, onclick, "btnCss btn_FreeBrochure");
        }

        public static MvcHtmlString GetFreeBrochureLink(this HtmlHelper helper, HomeItemViewModel model)
        {
            var brochureParams = model.FreeBrochureParamsForHome();
            var linkText = FormattingExtensions.GetBrochureLinkText();
            var onclick = "jQuery.googlepush('Lead Events','Search Results CTA Free Brochure','Open Form - Free Brochure');";
            return GenerateFreeBrochureLink(helper, linkText, brochureParams, onclick, "btnCss btn_FreeBrochure");
        }

        public static MvcHtmlString GetFreeBrochureLink(this HtmlHelper helper, CommunityMapCard comm)
        {
            var brochureParams = comm.FreeBrochureParamsForComm();
            var linkText = FormattingExtensions.GetBrochureLinkText();
            const string onclick = "jQuery.googlepush('Search Events','Search Results - Map','Map - Quick Comm Brochure'); jQuery.googlepush('Lead Events', NHS.Scripts.Helper.readCookie('CTAName'), 'Open Form - Free Brochure');";
            return GenerateFreeBrochureLink(helper, linkText, brochureParams, onclick, "btnCss btn_FreeBrochure");
        }

        public static MvcHtmlString GetFreeBrochureLink(this HtmlHelper helper, ExtendedCommunityResult result,
            string linkText = "", string className = "")
        {
            if (string.IsNullOrEmpty(linkText))
                linkText = FormattingExtensions.GetBrochureLinkText();
            var onclick =
                string.Format(
                    "jQuery.googlepush('Lead Events','Search Results CTA Free Brochure','Open Form - Free Brochure'); commResults.get_log().logEvent('{0}', 0, 0);",
                    LogImpressionConst.CommResFreeBrochure);

            if (!NhsRoute.IsBrandPartnerNhsPro)
            {
                return GenerateFreeBrochureLink(helper, linkText, result.FreeBrochureParams(), onclick, "btnCss " + (string.IsNullOrEmpty(className) ? "btn_FreeBrochureAlt" : className));
            }

            return helper.SendCustomBrochureLink(result.FreeBrochureParams(), false, false,
                UserSession.UserProfile.ActorStatus != WebActors.ActiveUser);
        }

        public static MvcHtmlString GetNextStepsFreeBrochureImageLink(this HtmlHelper htmlHelper, IFreeBrochureLinkInformation model)
        {
            var linkText = ((BaseViewModel)model).Globals.PartnerLayoutConfig.IsPartnerRealtor
                ? "Contact an agent"
                : LanguageHelper.GetAFreeBrochure;

            var logMethod =
                string.Format(
                    "jQuery.googlepush('Lead Events','{0} - CTA Next Steps Free Brochure','Open Form - Free Brochure');",
                    model.IsPageCommDetail ? "Community" : "Home");

            return GetLeadLink(htmlHelper, model, LeadAction.FreeBrochure, linkText, "LogNextStepsBrochure", logMethod);
        }

        public static MvcHtmlString GetProFreeBrochureLink(this HtmlHelper htmlHelper, IFreeBrochureLinkInformation model)
        {
            var paramz = GetParamerters(model, LeadAction.FreeBrochure,
                model.IsPageCommDetail ? Pages.CommunityDetail : Pages.HomeDetail);
            return htmlHelper.SendCustomBrochureLink(paramz, false);
        }

        public static MvcHtmlString GetFreeBrochureLink(this HtmlHelper htmlHelper, IFreeBrochureLinkInformation model, string logMethod = "", string css = "")
        {
            return htmlHelper.GetLeadImageLink(model, LeadAction.FreeBrochure, logMethod, css);
        }

        public static MvcHtmlString GetLeadLink(this HtmlHelper htmlHelper, IFreeBrochureLinkInformation model,
            string leadAction, string logMethod = "")
        {
            return GetLeadLink(htmlHelper, model, leadAction, LanguageHelper.FreeBrochure, "", logMethod);
        }

        public static MvcHtmlString GetLeadImageLink(this HtmlHelper htmlHelper, IFreeBrochureLinkInformation model,
            string leadAction, string logMethod = "", string css = "btn btn_FreeBrochure")
        {
            return GetLeadLink(htmlHelper, model, leadAction, LanguageHelper.FreeBrochure, css, logMethod);
        }

        public static MvcHtmlString GetFreeBrochureLink(this HtmlHelper htmlHelper, FavoritesSaveHome model, string @classFreeBrochure = "btnCss btn_FreeBrochure ", string @classViewBrochure = "btnCss btn_ViewBrochure ")
        {
            if (model.ShowViewBrochure)
            {
                return htmlHelper.NhsLinkCustom(LanguageHelper.ViewBrochure, model.ViewBrochureUrl,
                    new { @class = @classViewBrochure, target = "_blank" });
            }

            var onclick = "var isSearchAlert = jQuery(this).parents('#nhs_SavedAlertsContent').length > 0; jQuery.googlepush('Lead Events', isSearchAlert? 'Search Alert CTA Free Brochure' : 'Saved Properties CTA Free Brochure','Open Form - Free Brochure');";

            return GetLeadLink(htmlHelper, model, LeadAction.FreeBrochure, LanguageHelper.FreeBrochure, @classFreeBrochure, onclick, Pages.SavedHomes);
        }

        public static MvcHtmlString GetFreeBrochureLink(this HtmlHelper htmlHelper, FavoritesSaveComm model, string @classFreeBrochure = "btnCss btn_FreeBrochure ", string @classViewBrochure = "btnCss btn_ViewBrochure ")
        {
            if (model.ShowViewBrochure)
            {
                return htmlHelper.NhsLinkCustom(LanguageHelper.ViewBrochure, model.ViewBrochureUrl,
                    new { @class = @classViewBrochure, target = "_blank" });
            }

            var onclick = "var isSearchAlert = jQuery(this).parents('#nhs_SavedAlertsContent').length > 0; jQuery.googlepush('Lead Events', isSearchAlert? 'Search Alert CTA Free Brochure' : 'Saved Properties CTA Free Brochure','Open Form - Free Brochure');";

            return GetLeadLink(htmlHelper, model, LeadAction.FreeBrochure, LanguageHelper.FreeBrochure, @classFreeBrochure, onclick, Pages.SavedHomes);
        }

        public static MvcHtmlString GetFreeBrochureLink(this HtmlHelper htmlHelper, IFreeBrochureLinkInformation model, string leadAction, string linkText, string cssClass, string logMethod, string fromPage = null)
        {

            return htmlHelper.GetLeadLink(model, leadAction, linkText, cssClass, logMethod, fromPage);
        }

        public static MvcHtmlString GetLeadLink(this HtmlHelper htmlHelper, IFreeBrochureLinkInformation model, string leadAction, string linkText, string cssClass, string logMethod, string fromPage = null)
        {

            var paramz = GetParamerters(model, leadAction, fromPage);
            return GenerateFreeBrochureLink(htmlHelper, linkText, paramz, logMethod, cssClass);
        }

        public static MvcHtmlString GenerateFreeBrochureLink(this HtmlHelper htmlHelper, string linkText, List<RouteParam> param, string logMethod = "", string cssClass = "", string id = "")
        {
            const string pageName = Pages.LeadsRequestBrochureModal;

            if (NhsRoute.IsBrandPartnerNhsPro)
                return htmlHelper.SendCustomBrochureLink(param);

            const int width = ModalWindowsConst.LeadsRequestInfoShortWidth;
            const int height = ModalWindowsConst.LeadsRequestInfoShortHeight;

            if (string.IsNullOrEmpty(logMethod))
            {
                return htmlHelper.NhsModalWindowLink(linkText, pageName, width, height, param,
                    new Dictionary<string, object>
                    {
                        {"class", cssClass},
                        {"data-hideTitle", "true"},
                        {"id", id}
                    });
            }

            return htmlHelper.NhsModalWindowLink(linkText, pageName, width, height, param,
                new Dictionary<string, object>
                {
                    {"class", cssClass},
                    {"data-hideTitle", "true"},
                    {"onclick", logMethod},
                    {"id", id}
                });
        }

        private static List<RouteParam> GetParamerters(IFreeBrochureLinkInformation model, string leadAction, string fromPage = null)
        {
            var paramz = new List<RouteParam>();

            if (model.IsPageCommDetail) //Comm detail
            {
                paramz.Add(new RouteParam(RouteParams.LeadType, LeadType.Community, RouteParamType.QueryString));
                paramz.Add(new RouteParam(RouteParams.LeadAction, leadAction, RouteParamType.QueryString));
                paramz.Add(new RouteParam(RouteParams.CommunityList, model.CommunityId.ToString(), RouteParamType.QueryString));
                paramz.Add(new RouteParam(RouteParams.Builder, model.BuilderId.ToString(), RouteParamType.QueryString));
                paramz.Add(new RouteParam(RouteParams.Market, model.MarketId.ToString(), RouteParamType.QueryString));
                paramz.Add(new RouteParam(RouteParams.FromPage, fromPage ?? Pages.CommunityDetail, RouteParamType.QueryString));
            }
            else //Home detail
            {

                paramz.Add(new RouteParam(RouteParams.LeadType, LeadType.Home, RouteParamType.QueryString));
                paramz.Add(new RouteParam(RouteParams.LeadAction, leadAction, RouteParamType.QueryString));
                if (model.IsPlan)
                {
                    paramz.Add(new RouteParam(RouteParams.PlanList, model.PlanId.ToString(), RouteParamType.QueryString));
                    paramz.Add(new RouteParam(RouteParams.PlanId, model.PlanId.ToString(), RouteParamType.QueryString));
                }
                else
                {
                    paramz.Add(new RouteParam(RouteParams.SpecList, model.SpecId.ToString(), RouteParamType.QueryString));
                    paramz.Add(new RouteParam(RouteParams.SpecId, model.SpecId.ToString(), RouteParamType.QueryString));
                }
                paramz.Add(new RouteParam(RouteParams.Builder, model.BuilderId.ToString(), RouteParamType.QueryString));
                paramz.Add(new RouteParam(RouteParams.Community, model.CommunityId.ToString(), RouteParamType.QueryString));
                paramz.Add(new RouteParam(RouteParams.Market, model.MarketId.ToString(), RouteParamType.QueryString));
                paramz.Add(new RouteParam(RouteParams.FromPage, fromPage ?? Pages.HomeDetail, RouteParamType.QueryString));
            }

            if (NhsRoute.IsBrandPartnerNhsPro)
                paramz.Add(new RouteParam(RouteParams.IsBilled, model.IsBilled.ToString(), RouteParamType.QueryString));

            return paramz;
        }
    }
}
