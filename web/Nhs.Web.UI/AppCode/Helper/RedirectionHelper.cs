﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nhs.Mvc.Routing.Configuration;
using Bdx.Web.Api.Objects;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Domain.Model.Web.Partials;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResultMobile;

namespace Nhs.Web.UI.AppCode.Helper
{
    public static class RedirectionHelper
    {
        #region ResultsPage
        public static string GetHomeResultsPage()
        {
            return Pages.HomeResults;
        }
        
        public static string GetCommunityResultsPage()
        {
            return  Pages.CommunityResults;
        }

        public static string RedirecToNewFormat(int marketId, IMarketDfuService marketDfuService)
        {
            
            var isDfuMarket = marketId != 0 && marketDfuService.IsMarketIdValidDFU(NhsRoute.BrandPartnerId, marketId) && NhsRoute.PartnerId == NhsRoute.BrandPartnerId;

            return isDfuMarket ? NhsRoute.CurrentRoute.Function : string.Empty;
        }

        private static bool UseNewFormatResultPage(int marketId)
        {
            var marketDfuService = new MarketDfuService(new MarketDfuReader());
            var isDfuMarket = marketId != 0 && marketDfuService.IsMarketIdValidDFU(NhsRoute.BrandPartnerId, marketId) &&
                                NhsRoute.PartnerId == NhsRoute.BrandPartnerId;

            var useNewFormat = false;
            return useNewFormat || isDfuMarket;
        }

        public static List<RouteParam> ToResultsParams(this Market market)
        {
            var parameters = new List<RouteParam>();

            if (UseNewFormatResultPage(market.MarketId))
            {
                //Case 83669: Don't remove ToTrimCase() on MarketName
                var marketName = market.MarketName.ToTrimCase();
                var stateName = market.State.StateName;
                return parameters.ToResultsParams(marketName, stateName);
            }

            parameters.Add(new RouteParam(RouteParams.Market, market.MarketId));
            return parameters;
        }

        public static List<RouteParam> ToResultsParams(this Market market, List<RouteParam> parameters)
        {
            if (UseNewFormatResultPage(market.MarketId))
            {
                //Case 83669: Don't remove ToTrimCase() on MarketName
                var marketName = market.MarketName.ToTrimCase();
                var stateName = market.State.StateName;
                return parameters.ToResultsParams(marketName, stateName);
            }

            parameters.Insert(0, new RouteParam(RouteParams.Market, market.MarketId));
            return parameters;
        }

        public static List<RouteParam> ToResultsParams(int marketId)
        {
            var parameters = new List<RouteParam>();

            if (UseNewFormatResultPage(marketId))
            {
                var marketName = string.Empty;
                var stateName = string.Empty;
                var marketService = StructureMap.ObjectFactory.GetInstance<IMarketService>();
                var mkt = marketService.GetMarket(marketId);
                if (mkt != null)
                {
                    marketName = mkt.MarketName.ToTrimCase();
                    stateName = mkt.State.StateName;
                }
                return parameters.ToResultsParams(marketName, stateName);
            }
            
            parameters.Add(new RouteParam(RouteParams.Market, marketId));
            return parameters;
        }

        public static List<RouteParam> ToResultsParams(this List<RouteParam> parameters)
        {
            var marketName = string.Empty;
            var stateName = string.Empty;
            int marketId;
            if (parameters.Exists(p => p == RouteParams.Market || p == RouteParams.MarketId))
            {
                var marketParam = parameters.FirstOrDefault(p => p == RouteParams.Market || p == RouteParams.MarketId);
                if (marketParam != null)
                {
                    marketId = marketParam.Value.ToType<int>();

                    if (!UseNewFormatResultPage(marketId))
                    {
                        if (parameters.Exists(p => p == RouteParams.CityNameFilter || p == RouteParams.City))
                        {
                            var parameter = parameters.FirstOrDefault(p => p == RouteParams.CityNameFilter || p == RouteParams.City);
                            if (parameter != null)
                            {
                                parameter.Value = parameter.Value.RemoveSpaceAndDash();
                            }
                        }
                        return parameters;
                    }

                    var marketService = StructureMap.ObjectFactory.GetInstance<IMarketService>();
                    var mkt = marketService.GetMarket(marketId);
                    if (mkt != null)
                    {
                        marketName = mkt.MarketName;
                        stateName = mkt.State.StateName;
                    }
                }

            }
            else if (parameters.Exists(p => p == RouteParams.Area || p == RouteParams.MarketName))
            {
                var areaParam = parameters.FirstOrDefault(p => p == RouteParams.Area || p == RouteParams.MarketName);
                if (areaParam != null)
                    marketName = areaParam.Value;
                var stateNameParam =
                    parameters.FirstOrDefault(p => p == RouteParams.StateName || p == RouteParams.Estado);
                if (stateNameParam != null)
                    stateName = stateNameParam.Value;

                var marketService = StructureMap.ObjectFactory.GetInstance<IMarketService>();
                var mkt = marketService.GetMarket(NhsRoute.PartnerId, stateName.Replace("-", " "),
                    marketName.Replace("-", " "), false);

                if (mkt != null)
                {
                    marketName = mkt.MarketName;
                    stateName = mkt.State.StateName;
                }
            }

            //Case 83669: Don't remove ToTrimCase() on MarketName
            return string.IsNullOrEmpty(marketName) ? parameters : parameters.ToResultsParams(marketName.ToTrimCase(), stateName);
        }

        private static List<RouteParam> ToResultsParams(this List<RouteParam> parameters, string marketName, string stateName)
        {
            var removeParams = new List<RouteParams>
            {
                RouteParams.Area,
                RouteParams.MarketName,
                RouteParams.MarketId,
                RouteParams.Estado,
                RouteParams.StateName,
                RouteParams.Market,
                RouteParams.State
            };

            parameters = parameters.Where(p => !removeParams.Contains(p.Name)).ToList();

           marketName += "-area";

            var @params = new List<RouteParam>
            {
                new RouteParam(RouteParams.Estado, stateName.Trim().Replace(" ", "-"), RouteParamType.FriendlyNameOnly),
                new RouteParam(RouteParams.Area, marketName.ToTrimCase().Replace(" ", "-"), RouteParamType.FriendlyNameOnly)
            };
         
            @params.AddRange(parameters);

            return @params.ResultPage();
        }

        private static List<RouteParam> ResultPage(this List<RouteParam> parameters)
        {
            if (parameters.Exists(p => p == RouteParams.County))
            {
                var parameter = parameters.FirstOrDefault(p => p == RouteParams.County);
                if (parameter != null)
                {
                    parameter.Value = parameter.Value.RemoveSpaceAndDash() + "-county";
                    parameter.ParamType = RouteParamType.FriendlyNameOnly;
                }

            }
            else if (parameters.Exists(p => p == RouteParams.CityNameFilter || p == RouteParams.City))
            {
                var parameter = parameters.FirstOrDefault(p => p == RouteParams.CityNameFilter || p == RouteParams.City);
                if (parameter != null)
                {
                    parameter.Value = parameter.Value.RemoveSpaceAndDash();
                    parameter.ParamType = RouteParamType.FriendlyNameOnly;
                }
            }
            else if (parameters.Exists(p => p == RouteParams.PostalCode))
            {
                var parameter = parameters.FirstOrDefault(p => p == RouteParams.PostalCode);
                if (parameter != null)
                    parameter.ParamType = RouteParamType.FriendlyNameOnly;
            }

            return parameters;
        }

        private static List<RouteParam> CnaResultPage(this List<RouteParam> parameters)
        {
            if (parameters.Exists(p => p == RouteParams.County))
            {
                var parameter = parameters.FirstOrDefault(p => p == RouteParams.County);
                if (parameter != null)
                {
                    parameter = parameter.RemoveSpaceAndDash();
                    parameter.Name = RouteParams.Condado;
                }
            }
            else if (parameters.Exists(p => p == RouteParams.City || p == RouteParams.CityNameFilter))
            {
                var parameter = parameters.FirstOrDefault(p => p == RouteParams.City || p == RouteParams.CityNameFilter);
                if (parameter != null)
                {
                    parameter = parameter.RemoveSpaceAndDash();
                    parameter.Name = RouteParams.Ciudad;
                }
            }
            else if (parameters.Exists(p => p == RouteParams.PostalCode || p == RouteParams.Postal || p == RouteParams.PostalCodeFilter))
            {
                var parameter = parameters.FirstOrDefault(p => p == RouteParams.PostalCode || p == RouteParams.Postal || p == RouteParams.PostalCodeFilter);
                if (parameter != null)
                    parameter.ParamType = RouteParamType.FriendlyNameOnly;
            }

            return parameters;
        }

        #endregion

        #region DetailPages

        public static string GetCommunityDetailPage()
        {
            return Pages.CommunityDetail;
        }

        public static string GetHomeDetailPage(bool isSpec = false)
        {
            return Pages.HomeDetail;
        }

        public static string GetDetailPageRedirectName(bool isComDetail, bool isSpec = false)
        {
           
            return "";
        }

        #region HomeDetail

        public static List<RouteParam> ToHomeDetail(this MortgageViewModel result)
        {
            List<RouteParam> paramz = null;
            
                var paramSpecOrPlan = result.IsSpec
                    ? new RouteParam(RouteParams.SpecId, result.SpecID.ToString())
                    : new RouteParam(RouteParams.PlanId, result.PlanID.ToString());

                paramz = new List<RouteParam> {paramSpecOrPlan};
           

            return paramz;
        }

        public static List<RouteParam> ToHomeDetail(this SpotLightHomes result)
        {
            List<RouteParam> paramz = null;
            
                var paramSpecOrPlan = result.IsSpec > 0
                  ? new RouteParam(RouteParams.SpecId, result.HomeId.ToString())
                  : new RouteParam(RouteParams.PlanId, result.HomeId.ToString());

                paramz = new List<RouteParam> { paramSpecOrPlan };

            return paramz;
        }

        public static List<RouteParam> ToHomeDetail(this HomeItemMobileViewModel result)
        {
            List<RouteParam> paramz = null;
           
                var paramSpecOrPlan = result.IsSpec > 0
                  ? new RouteParam(RouteParams.SpecId, result.HomeId.ToString())
                  : new RouteParam(RouteParams.PlanId, result.HomeId.ToString());

                paramz = new List<RouteParam> { paramSpecOrPlan };
            return paramz;
        }

        public static List<RouteParam> ToHomeDetail(this HomeDetailViewModel result)
        {
            List<RouteParam> paramz = null;
           
                var paramSpecOrPlan = result.SpecId > 0
                    ? new RouteParam(RouteParams.SpecId, result.SpecId.ToString())
                    : new RouteParam(RouteParams.PlanId, result.PlanId.ToString());

                paramz = new List<RouteParam> {paramSpecOrPlan};
            

            return paramz;
        }

        public static List<RouteParam> ToHomeDetail(this ExtendedHomeResult result)
        {
            List<RouteParam> paramz = null;
            
                var paramSpecOrPlan = result.SpecId > 0
                  ? new RouteParam(RouteParams.SpecId, result.SpecId.ToString())
                  : new RouteParam(RouteParams.PlanId, result.PlanId.ToString());

                paramz = new List<RouteParam> { paramSpecOrPlan };
            return paramz;
        }

        public static List<RouteParam> ToHomeDetail(this HomeItem result)
        {

            List<RouteParam> paramz = null;
            
                var paramSpecOrPlan = result.IsSpec > 0
                  ? new RouteParam(RouteParams.SpecId, result.HomeId.ToString())
                  : new RouteParam(RouteParams.PlanId, result.HomeId.ToString());

                paramz = new List<RouteParam> { paramSpecOrPlan };
            return paramz;
        }

        public static List<RouteParam> ToHomeDetail(this ApiHomeResultV2 result, HomesAvailable model)
        {
            List<RouteParam> paramz = null;
           
                var paramSpecOrPlan = result.IsSpec > 0
                  ? new RouteParam(RouteParams.SpecId, result.HomeId.ToString())
                  : new RouteParam(RouteParams.PlanId, result.HomeId.ToString());

                paramz = new List<RouteParam> { paramSpecOrPlan };

            return paramz;
        }

        public static List<RouteParam> ToHomeDetail(this HomeItemViewModel result)
        {
            List<RouteParam> paramz = null;
            
                var paramSpecOrPlan = result.IsSpec > 0
                    ? new RouteParam(RouteParams.SpecId, result.HomeId.ToString())
                    : new RouteParam(RouteParams.PlanId, result.HomeId.ToString());

                paramz = new List<RouteParam> { paramSpecOrPlan };

            return paramz;
        }

        public static List<RouteParam> ToHomeDetail(this IPlan plan)
        {
            return new List<RouteParam> {new RouteParam(RouteParams.PlanId, plan.PlanId)};
        }

        public static List<RouteParam> ToHomeDetail(this ISpec spec)
        {
            return new List<RouteParam> { new RouteParam(RouteParams.SpecId, spec.SpecId) };
        }

        public static List<RouteParam> GetMoveHomeDetailPlanParameters(int planId, string stateAbbr, string marketName, string city, string planName, string commName)
        {
            city = city.ReplaceSpecialCharacters("-");
            marketName = marketName.ReplaceSpecialCharacters("-");
            var name = string.Format("{0}-at-{1}", planName, commName).ReplaceSpecialCharacters("-");
            var paramz = new List<RouteParam>
            {
                new RouteParam(RouteParams.PlanId, planId, RouteParamType.FriendlyNameOnly),
                new RouteParam(RouteParams.State, stateAbbr, RouteParamType.FriendlyNameOnly),
                new RouteParam(RouteParams.Market, marketName,RouteParamType.FriendlyNameOnly),
                new RouteParam(RouteParams.City, city, RouteParamType.FriendlyNameOnly),
                new RouteParam(RouteParams.Name, name, RouteParamType.FriendlyNameOnly)
            };
            return paramz;
        }

        public static List<RouteParam> GetMoveHomeDetailSpecParameters(int specId, string address, string marketName,string city,string planName, string stateAbbr, string postalCode)
        {
            var name = string.Format("{0}-at-{1}-{2}-{3}-{4}", planName, address, city, stateAbbr, postalCode).ReplaceSpecialCharacters("-");

            var paramz = new List<RouteParam>
            {
                new RouteParam(RouteParams.SpecId, specId, RouteParamType.FriendlyNameOnly),
                new RouteParam(RouteParams.Market, marketName.ReplaceSpecialCharacters("-"), RouteParamType.FriendlyNameOnly),
                new RouteParam(RouteParams.Name, name, RouteParamType.FriendlyNameOnly)
            };
            return paramz;

        }
        
        public static List<RouteParam> GetCnaHomeDetailParameters(string stateName, string marketName, string planName, string address, int homeId, bool isSpec)
        {
            var paramz = new List<RouteParam>
            {
                new RouteParam(RouteParams.Estado, stateName.Replace(" ", "-"), RouteParamType.FriendlyNameOnly),
                new RouteParam(RouteParams.Area, marketName.Replace(" ", "-"),
                    RouteParamType.FriendlyNameOnly),
                new RouteParam(RouteParams.Name, planName.ReplaceSpecialCharacters("-"), RouteParamType.FriendlyNameOnly),
                
            };

            if (isSpec)
            {
               paramz.Add(new RouteParam(RouteParams.Address, address.ReplaceSpecialCharacters("-"), RouteParamType.FriendlyNameOnly));  
               paramz.Add( new RouteParam(RouteParams.SpecId, "s" + homeId, RouteParamType.FriendlyNameOnly));  
            }
            else
            {
              paramz.Add(new RouteParam(RouteParams.PlanId, "p" + homeId, RouteParamType.FriendlyNameOnly));  
            }
            return paramz;
        }

        #endregion

        #region CommunityDetail


        public static List<RouteParam> ToCommunityDetail(this SpotLightCommunity result)
        {
            
            return new List<RouteParam>
            {
                new RouteParam(RouteParams.Builder, result.BuilderId),
                new RouteParam(RouteParams.Community, result.CommunityId)
            };
        }

        public static List<RouteParam> ToCommunityDetail(this PartialViewModels.BreadcrumbViewModel result)
        {
           
            return new List<RouteParam>
            {
                new RouteParam(RouteParams.Builder, result.BuilderId),
                new RouteParam(RouteParams.Community, result.CommunityId)
            };
        }

        public static List<RouteParam> ToCommunityDetail(this CommunityDetailViewModel result)
        {
           
            return new List<RouteParam>
            {
                new RouteParam(RouteParams.Builder, result.BuilderId),
                new RouteParam(RouteParams.Community, result.CommunityId)
            };
        }
        
        public static List<RouteParam> ToCommunityDetail(this FavoritesSaveComm result)
        {
          
            return new List<RouteParam>
            {
                new RouteParam(RouteParams.Builder, result.BuilderId),
                new RouteParam(RouteParams.Community, result.CommunityId)
            };
        }

        public static List<RouteParam> ToCommunityDetail(this MapPopupViewModel result)
        {
           
            return new List<RouteParam>
            {
                new RouteParam(RouteParams.Builder, result.BuilderId),
                new RouteParam(RouteParams.Community, result.CommunityId)
            };
        }

        public static List<RouteParam> ToCommunityDetail(this CommunityMapCard result)
        {
           
            return new List<RouteParam>
            {
                new RouteParam(RouteParams.Builder, result.BId),
                new RouteParam(RouteParams.Community, result.Id)
            };
        }

        public static List<RouteParam> ToCommunityDetail(this MortgageViewModel result)
        {
           
            return new List<RouteParam>
            {
                new RouteParam(RouteParams.Builder, result.BuilderID.ToString()),
                new RouteParam(RouteParams.Community, result.CommunityID.ToString())
            };
        }

        public static List<RouteParam> ToCommunityDetail(this HomeDetailViewModel result)
        {
           
            return new List<RouteParam>
            {
                new RouteParam(RouteParams.Builder, result.BuilderId.ToString()),
                new RouteParam(RouteParams.Community, result.CommunityId.ToString())
            };
        }

        public static List<RouteParam> ToCommunityDetail(this NearbyCommunity result)
        {
           
            return new List<RouteParam>
            {
                new RouteParam(RouteParams.Builder, result.BuilderId.ToString()),
                new RouteParam(RouteParams.Community, result.CommunityId.ToString())
            };
        }

        public static List<RouteParam> ToCommunityDetail(this ExtendedCommunityResult result)
        {

            return new List<RouteParam>
            {
                new RouteParam(RouteParams.Builder, result.BuilderId.ToString()),
                new RouteParam(RouteParams.Community, result.CommunityId.ToString())
            };
        }

        public static List<RouteParam> ToCommunityDetail(this CommunityPromotionInfo result)
        {
            
            return new List<RouteParam>
            {
                new RouteParam(RouteParams.Builder, result.BuilderId.ToString()),
                new RouteParam(RouteParams.Community, result.CommunityId.ToString())
            };
        }

        public static List<RouteParam> ToCommunityDetail(this BoylResult result)
        {
           
            return new List<RouteParam>
            {
                new RouteParam(RouteParams.Builder, result.BuilderId.ToString()),
                new RouteParam(RouteParams.Community, result.CommunityId.ToString())
            };
        }

        public static List<RouteParam> ToCommunityDetail(this CommunityItemViewModel community)
        {
            
            return new List<RouteParam>
            {
                new RouteParam(RouteParams.Builder, community.BuilderId.ToString()),
                new RouteParam(RouteParams.Community, community.Id.ToString())
            };
        }

        public static List<RouteParam> ToCommunityDetail(this Community community)
        {
            return new List<RouteParam>
            {
                new RouteParam(RouteParams.Builder, community.BuilderId),
                new RouteParam(RouteParams.Community, community.CommunityId)
            };
        }

        public static List<RouteParam> GetMoveCommunityDetailParams(int communityId, string stateAbbr, string marketName, string city, string commName, string builderName)
        {
            marketName = marketName.ReplaceSpecialCharacters("-");
            city = city.ReplaceSpecialCharacters("-");
            commName = commName.ReplaceSpecialCharacters("-");
            builderName = builderName.ReplaceSpecialCharacters("-");
            var name = string.Format("{0}-by-{1}", commName, builderName);

            var paramz = new List<RouteParam>
                {
                    new RouteParam(RouteParams.CommunityId,communityId, RouteParamType.FriendlyNameOnly),
                    new RouteParam(RouteParams.Area, stateAbbr, RouteParamType.FriendlyNameOnly),
                    new RouteParam(RouteParams.MarketName,marketName, RouteParamType.FriendlyNameOnly),
                    new RouteParam(RouteParams.City, city, RouteParamType.FriendlyNameOnly),
                    new RouteParam(RouteParams.Name, name, RouteParamType.FriendlyNameOnly)
                };

            return paramz;
        }

        public static List<RouteParam> GetCnaCommunityDetailParams(string stateName, string marketName, string communityName, int communityId)
        {
            var paramz = new List<RouteParam>
                {
                    new RouteParam(RouteParams.Estado, stateName.Replace(" ", "-"), RouteParamType.FriendlyNameOnly),
                    new RouteParam(RouteParams.Area, marketName.Replace(" ", "-"), RouteParamType.FriendlyNameOnly),
                    new RouteParam(RouteParams.Name, communityName.ReplaceSpecialCharacters("-"), RouteParamType.FriendlyNameOnly),
                    new RouteParam(RouteParams.Community, communityId, RouteParamType.FriendlyNameOnly)
                };

            return paramz;
        }
        #endregion

        #endregion

        
    }
}
