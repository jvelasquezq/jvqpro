﻿using System.Globalization;
using System.Resources;
using Nhs.Mvc.Routing.Interface;
namespace Nhs.Web.UI.AppCode.Helper
{
    public static partial class LanguageHelper
    {
        private static readonly ResourceManager ResMan = new ResourceManager("Nhs.Web.UI.Configs.Resource.Res", typeof(LanguageHelper).Assembly);

        private static string GetString(string key)
        {
            var lng = "en";
            var cul = CultureInfo.CreateSpecificCulture(lng);
            var value = ResMan.GetString(key, cul);
            return value;
        }
    }
}