﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Nhs.Library.Business.Seo;
using Nhs.Library.Constants;
using Nhs.Library.Helpers.Seo;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.Helper
{
    /// <summary>
    /// Google Analytics Enhanced Ecommerce commands 
    /// </summary>
    public class GoogleAnalyticEcommerce
    {
        private GAProduct gaProductDirect;
        private List<GAProduct> gaProductsReco;
        private GAAction gaAction;

        public List<Metric> TDVMetrics
        {
            get
            {
                return TdvMetricsReader.GetTdvMetrics();
            }
        }

        public GoogleAnalyticEcommerce(Community mainLeadComm, List<Community> recoComms, string leadId, string partnerName)
        {
            var ctaName = HttpContext.Current.Request.Cookies["CTAName"] == null ? "NULL" : HttpContext.Current.Request.Cookies["CTAName"].Value;
            var metricValue = "NULL";
            var metricName = "NULL";

            if (TDVMetrics.FirstOrDefault(m => m.Event == LogImpressionConst.DirectLead) != null)
                metricValue = TDVMetrics.FirstOrDefault(m => m.Event == LogImpressionConst.DirectLead).Value;

            if (TDVMetrics.FirstOrDefault(m => m.Event == LogImpressionConst.DirectLead) != null)
                metricName = TDVMetrics.FirstOrDefault(m => m.Event == LogImpressionConst.DirectLead).Name;

            gaProductDirect = new GAProduct()
            {
                Id = leadId,
                Name = metricName,
                Category = string.Format(@"{0}\\{1}\\{2}\\{3}\\{4}", mainLeadComm.CommunityName.RemoveSpecialCharacters(), mainLeadComm.Builder.BuilderName.RemoveSpecialCharacters(), mainLeadComm.Market.MarketName.RemoveSpecialCharacters(), mainLeadComm.MarketId, partnerName),
                Brand = partnerName,
                Variant = ctaName,
                Price = metricValue,
                Cupon = string.Format("{0}|{1}", mainLeadComm.Market.MarketName, mainLeadComm.MarketId)
            };

            gaAction = new GAAction()
            {
                Id = Guid.NewGuid().ToString(),
                Affiliation = string.Format("{0}|{1}", mainLeadComm.Market.MarketName, mainLeadComm.MarketId),
                Revenue = metricValue
            };

            if (recoComms != null && recoComms.Count > 0)
            {
                gaProductsReco = new List<GAProduct>();
                foreach (var comm in recoComms)
                {
                    if (TDVMetrics.FirstOrDefault(m => m.Event == LogImpressionConst.RecommendedLead) != null)
                        metricValue = TDVMetrics.FirstOrDefault(m => m.Event == LogImpressionConst.RecommendedLead).Value;

                    if (TDVMetrics.FirstOrDefault(m => m.Event == LogImpressionConst.RecommendedLead) != null)
                        metricName = TDVMetrics.FirstOrDefault(m => m.Event == LogImpressionConst.RecommendedLead).Name;

                    gaProductsReco.Add(new GAProduct()
                    {
                        Id = leadId,
                        Name = metricName,
                        Category = string.Format(@"{0}\\{1}\\{2}\\{3}\\{4}", comm.CommunityName.RemoveSpecialCharacters(), comm.Builder.BuilderName.RemoveSpecialCharacters(), comm.Market.MarketName.RemoveSpecialCharacters(), comm.MarketId, partnerName),
                        Brand = partnerName,
                        Variant = ctaName,
                        Price = metricValue,
                        Cupon = string.Format("{0}|{1}", comm.Market.MarketName, comm.MarketId)
                    });
                }
            }
        }

        public string GetScript()
        {
            var push = new StringBuilder();

            push.AppendFormat("dataLayer.push({{ 'ecommerce': {{ 'purchase': {{ 'actionField': {{ 'id': '{0}', 'affiliation': '{1}', 'revenue': '{2}' }},",
                               gaAction.Id, gaAction.Affiliation, gaAction.Revenue);
            push.AppendFormat("'products': [{{ 'name': '{0}', 'id': '{1}', 'price': '{2}', 'category': '{3}'}}", gaProductDirect.Name, gaProductDirect.Id,
                               gaProductDirect.Price, gaProductDirect.Category);

            if (gaProductsReco != null && gaProductsReco.Count > 0)
                foreach (var product in gaProductsReco)
                    push.AppendFormat(",{{ 'name': '{0}', 'id': '{1}', 'price': '{2}', 'category': '{3}'}}", product.Name, product.Id, product.Price,
                                       product.Category);

            push.Append("]}}, 'event' : 'transactionComplete'})");
            return push.ToString();
        }
    }

    /// <summary>
    /// Defines the info for a call to a ec:addProduct from Google Enhanced Ecommerce
    /// </summary>
    public class GAProduct
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Brand { get; set; }
        public string Variant { get; set; }
        public string Price { get; set; }
        public string Cupon { get; set; }
    }

    /// <summary>
    /// Defines the info for a call to a ec:setAction from Google Enhanced Ecommerce
    /// </summary>
    public class GAAction
    {
        public string Id { get; set; }
        public string Affiliation { get; set; }
        public string Revenue { get; set; }
    }
}