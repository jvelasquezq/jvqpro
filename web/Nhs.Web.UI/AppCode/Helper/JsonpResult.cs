﻿using System;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Nhs.Web.UI.AppCode.Helper
{
    public class JsonpResult:JsonResult
    {
        private readonly object _data;

        public JsonpResult(object data)
        {
            _data = data;
        }

        public override void ExecuteResult(ControllerContext controllerContext)
        {
            if (controllerContext == null) return;
            
            var response = controllerContext.HttpContext.Response;
            var request = controllerContext.HttpContext.Request;
            var callbackfunction = request["callback"];
            
            if (string.IsNullOrEmpty(callbackfunction))
                throw new Exception("Callback function name must be provided in the request!");
            
            
            if (_data == null) return;

            response.ContentType = "application/x-javascript";
            var serializer = new JavaScriptSerializer();
            response.Write(string.Format("{0}({1});", callbackfunction, serializer.Serialize(_data)));
        }
    }
}