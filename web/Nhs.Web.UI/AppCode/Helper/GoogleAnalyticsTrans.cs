﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Library.Constants;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.Helper
{
    /// <summary>
    /// _addTrans(transactionId, affiliation, total, tax, shipping, city, state, country)
    /// </summary>
    public class GoogleAnalyticsTrans
    {
        public GoogleAnalyticsTrans(Community community, string requestUniqueKey) : this(community, false, requestUniqueKey,string.Empty) { }

        public GoogleAnalyticsTrans(Community community, bool isModal, string requestUniqueKey)
            : this(community.Market.MarketName, community.Market.MarketId, isModal, requestUniqueKey, string.Empty)
        {

        }

        public GoogleAnalyticsTrans(Community community, bool isModal, string requestUniqueKey, string originatingId)
            : this(community.Market.MarketName, community.Market.MarketId, isModal, requestUniqueKey, originatingId)
        {
           
        }

        public GoogleAnalyticsTrans(string marketName, int marketId, bool isModal, string requestUniqueKey)
            : this(marketName, marketId, isModal, requestUniqueKey, string.Empty)
        {
        }

        public GoogleAnalyticsTrans(string marketName, int marketId, bool isModal, string requestUniqueKey, string originatingId)
        {
            OrderId = requestUniqueKey;
            OriginatingId = originatingId;
            Shipping = string.Empty;
            MarketName = string.Format("{0}-{1}", marketName, marketId);
            if (!string.IsNullOrEmpty(LeadFormType))
                LeadFormType = isModal
                    ? GoogleGaConst.GoogleGaILeadFormTypeConst.Modal
                    : GoogleGaConst.GoogleGaILeadFormTypeConst.OnPage;
            LeadType = string.Empty;
            PartnerSite = string.Format("{0}-{1}", "NewHomeSourceProfessional", NhsRoute.PartnerId);
            ListAddItem = new List<GoogleAnalyticsItem>();
            SpecList = new List<string>();
            PlanList = new List<string>();
            CommunityList = new List<string>();
            Format = "\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\"";
        }
 
        #region Propertys
        /// <summary>
        /// Required. Internal unique transaction ID number for this transaction. 
        /// Google Param: transactionId
        /// </summary>
        public string OrderId { get; set; }

        /// <summary>
        ///  Optional. Partner or store affiliation (undefined if absent). 
        ///  Google Param: PartnerSite
        /// </summary>
        public string PartnerSite { get; private set; }

        /// <summary>
        /// Required. Total dollar amount of the transaction. Does not include tax and shipping and should only be considered the "grand total" if you explicity include shipping and tax. 
        /// Google Param: affiliation
        /// </summary>
        public int LeadCount
        {
            get { return ListAddItem.Count > 0 ? ListAddItem.Count() : 1; }
        }

        /// <summary>
        /// Optional. Tax amount of the transaction. 
        /// Google Param: tax
        /// </summary>
        public string OriginatingId { get; set; }

        /// <summary>
        /// Optional. Shipping charge for the transaction. 
        /// Google Param: shipping
        /// </summary>
        public string Shipping { get; set; }

        /// <summary>
        /// Optional. City to associate with transaction.
        /// Google Param: city
        /// </summary>
        public string MarketName { get; set; }

        /// <summary>
        /// Optional. State to associate with transaction. 
        /// Google Param: state
        /// </summary>
        public string LeadFormType { get; set; }

        /// <summary>
        /// Optional. Country to associate with transaction. 
        /// Google Param: country
        /// </summary>
        public string LeadType { get; set; }

        private List<GoogleAnalyticsItem> ListAddItem { get; set; }

        private List<string> SpecList { get; set; }

        private List<string> PlanList { get; set; }

        private List<string> CommunityList { get; set; }

        public string Format { get; set; } 
        #endregion

        #region Methods

        /// <summary>
        /// String separate by ','
        /// </summary>
        public void AddCommunity(string communityList)
        {
            if (!string.IsNullOrWhiteSpace(communityList))
                AddCommunities(communityList.Split(','));
        }

        public void AddCommunities(params string[] community)
        {
            CommunityList.AddRange(community);
        }

        /// <summary>
        /// String separate by ','
        /// </summary>
        public void AddPlan(string planList)
        {
            if (!string.IsNullOrWhiteSpace(planList))
                AddPlans(planList.Split(','));
        }

        public void AddPlans(params string[] plan)
        {
            PlanList.AddRange(plan);
        }

        /// <summary>
        /// String separate by ','
        /// </summary>
        /// <param name="specList">separate by ','</param>
        public void AddSpec(string specList)
        {
            if (!string.IsNullOrWhiteSpace(specList))
                AddSpecs(specList.Split(','));
        }


        public void AddSpecs(params string[] spec)
        {
            SpecList.AddRange(spec);
        }

        public void AddGoogleAnalyticsItem(params GoogleAnalyticsItem[] googleAnalyticsItem)
        {
            foreach (var item in googleAnalyticsItem)
            {
                item.OrderId = OrderId;
                item.LeadType = LeadType;
                ListAddItem.Add(item);
            }
        }


        #endregion

        public override string ToString()
        {
            var push = new StringBuilder();
            push.AppendFormat(
                string.Format("_gaq.push(['_addTrans',{0}]);", Format),
                OrderId, PartnerSite, LeadCount,
                string.IsNullOrWhiteSpace(OriginatingId) ? OrderId : OriginatingId, Shipping, MarketName,
                LeadFormType, LeadType);
            push.AppendLine("");
            foreach (var addItem in ListAddItem)
            {
                push.AppendLine(addItem.ToString());
            }

            return push.ToString();

        }
    }
}