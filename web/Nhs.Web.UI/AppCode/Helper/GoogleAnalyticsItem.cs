﻿using Bdx.Web.Api.Objects;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Web.UI.AppCode.Helper
{
    /// <summary>
    /// _addItem(transactionId, sku, name, category, price, quantity)
    /// </summary>
    public class GoogleAnalyticsItem
    {
        public GoogleAnalyticsItem():this(false)
        {
        }

        public GoogleAnalyticsItem(bool isComm)
        {
            OrderId = string.Empty;
            ItemDescription = string.Empty;
            InterestType = isComm ? GoogleGaConst.GoogleGaItemConst.Community : GoogleGaConst.GoogleGaItemConst.Home;
            LeadType = string.Empty;
            Price = 0;
            Quantity = 1;
        }

        public GoogleAnalyticsItem(Community community, bool isComm, string homeId)
        {
            OrderId = string.Empty;
            ItemDescription = string.Format("{0}-{1}|{2}-{3}|{4}", community.Builder.BuilderName.TrimEnd('-'),
                                            community.BuilderId, community.CommunityName.TrimEnd('-'),
                                            community.CommunityId, isComm ? "00000" : homeId);
            LeadType = string.Empty;
            InterestType = isComm ? GoogleGaConst.GoogleGaItemConst.Community : GoogleGaConst.GoogleGaItemConst.Home; 
            Price = 0;
            Quantity = 1;
        }

        public GoogleAnalyticsItem(ApiRecoCommunityResult community, bool isComm, string homeId)
        {
            OrderId = string.Empty;
            ItemDescription = string.Format("{0}-{1}|{2}-{3}|{4}", community.BrandName.TrimEnd('-'),
                                            community.BuilderId, community.CommunityName.TrimEnd('-'),
                                            community.CommunityId, isComm ? "00000" : homeId);
            LeadType = string.Empty;
            InterestType = isComm ? GoogleGaConst.GoogleGaItemConst.Community : GoogleGaConst.GoogleGaItemConst.Home;
            Price = 0;
            Quantity = 1;
        }

        /// <summary>
        /// Required. Internal unique transaction ID number for this transaction. 
        /// Google Param: transactionId
        /// </summary>
        public string OrderId { get; set; }

        /// <summary>
        /// Required. Item's SKU code. 
        /// Google Param: sku 
        /// </summary>
        public string ItemDescription { get; set; }

        /// <summary>
        /// Required. Product name. Required to see data in the product detail report. 
        /// Google Param: name 
        /// </summary>
        public string LeadType { get; set; }

        /// <summary>
        /// Optional. Product category. 
        /// Google Param: category 
        /// </summary>
        public string InterestType { get; set; }

        /// <summary>
        /// Required. Product price. 
        /// Google Param: price 
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// equired. Purchase quantity. 
        /// Google Param: quantity 
        /// </summary>
        public int Quantity { get; set; }

        public override string ToString()
        {
            var push = string.Format("_gaq.push(['_addItem',\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\"]);", OrderId,
                                     ItemDescription, LeadType, InterestType, Price, Quantity);
            return push;
        }
    }
}