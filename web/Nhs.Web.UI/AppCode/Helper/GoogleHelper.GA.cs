﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bdx.Web.Api.Objects;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.Helper
{
    public partial class GoogleHelper
    {
        private readonly ICommunityService _communityService;
        private readonly IListingService _listingService;

        public GoogleHelper(IListingService listingService)
            : this(null, listingService)
        {
         
        }

        public GoogleHelper(ICommunityService communityService)
            : this(communityService, null)
        {
           
        }

        public GoogleHelper(ICommunityService communityService, IListingService listingService)
            : this()
        {
            _communityService = communityService;
            _listingService = listingService;
        }

        public GoogleHelper()
        {
            AddTransList = new List<GoogleAnalyticsTrans>();
        }

        private List<GoogleAnalyticsTrans> AddTransList { get; set; }

        public void CreateGoogleAnalyticsDirectLead(string requestUniqueKey, int communityId, int specId, int planId, bool isModal = false, string leadType = GoogleGaConst.Direct, string originatingId = "", string leadFormType = null)
        {
            string homeId;
            decimal price;
            var community = GetCommunity(communityId, specId, planId, out homeId, out price);
            var isComm = (planId == 0 && specId==0);

            var addTrans = new GoogleAnalyticsTrans(community, isModal, requestUniqueKey, originatingId)
            {
                LeadType = leadType,
                LeadFormType = leadFormType
            };
            var addItem = new GoogleAnalyticsItem(community, isComm, homeId) { Price = price };
            addTrans.AddGoogleAnalyticsItem(addItem);
            AddTransList.Add(addTrans);
        }

        public void CreateGoogleAnalyticsDirectLead(string requestUniqueKey, Community community, ISpec spec, IPlan plan, bool isModal = false, string leadType = GoogleGaConst.Direct, string originatingId = "", string leadFormType =null)
        {
            var homeId="";
            decimal price =0;
            if (plan != null)
            {
                homeId =   "P" + plan.PlanId.ToType<string>();
                price = plan.Price;
            }
            if (spec != null)
            {
                homeId = "S" + spec.SpecId.ToType<string>();
                price = spec.Price;
            }

            var isComm = (spec == null && plan == null);

            var addTrans = new GoogleAnalyticsTrans(community, isModal, requestUniqueKey, originatingId)
            {
                LeadType = leadType,
                LeadFormType = leadFormType
            };
            var addItem = new GoogleAnalyticsItem(community, isComm, homeId) {Price = price};
            addTrans.AddGoogleAnalyticsItem(addItem);
            AddTransList.Add(addTrans);
        }

        public void CreateGoogleAnalyticsRecommendedCommunityLeads(int communityId,string requestUniqueKey, List<int> communityList, bool isComm, bool isModal = false, string leadType=GoogleGaConst.RCM)
        {
            if (communityList.Any())
            {
                var community = GetCommunity(communityId);
                var orderId = string.Format("{0}-c{1}-", Guid.NewGuid(), community.CommunityId);
                var addTrans = new GoogleAnalyticsTrans(community, isModal, orderId) { OriginatingId = requestUniqueKey, LeadType = leadType };

                foreach (var gaItem in _communityService.GetCommunities(communityList).Select(reccommunity => new GoogleAnalyticsItem(reccommunity, isComm, "") { LeadType = leadType }))
                {
                    addTrans.AddGoogleAnalyticsItem(gaItem);
                }
                AddTransList.Add(addTrans); 
            }
        }

        public void CreateGoogleAnalyticsRecommendedCommunityLeads(Community community, string requestUniqueKey, List<ApiRecoCommunityResult> communityList, bool isComm, bool isModal = false, string leadType = GoogleGaConst.RCM)
        {
            if (communityList.Any())
            {
                var orderId = string.Format("{0}-c{1}-", Guid.NewGuid(), community.CommunityId);
                var addTrans = new GoogleAnalyticsTrans(community, isModal, orderId) { OriginatingId = requestUniqueKey, LeadType = leadType };
                foreach (var gaItem in communityList.Select(reccommunity => new GoogleAnalyticsItem(reccommunity, isComm, "") { LeadType = leadType }))
                {
                    addTrans.AddGoogleAnalyticsItem(gaItem);
                }
                AddTransList.Add(addTrans); 
            }
        }

        public void CreateGoogleAnalyticsRecommendedCommunityLeadsMultiSelect(string requestUniqueKey, List<int> communityList, bool isModal = false, string leadType = GoogleGaConst.MultiSelect)
        {
            if (communityList.Any())
            {
                var addTrans = new GoogleAnalyticsTrans("", 0, isModal, requestUniqueKey)
                    {
                        OriginatingId = requestUniqueKey,
                        LeadType = leadType,
                        Format = "\"{0}\", \"{1}\", \"{2}\", \"{0}\""
                    };
                foreach (var gaItem in _communityService.GetCommunities(communityList).Select(reccommunity => new GoogleAnalyticsItem(reccommunity, true, "") { LeadType = leadType }))
                {
                    addTrans.AddGoogleAnalyticsItem(gaItem);
                }
                AddTransList.Add(addTrans);
            }
        }

        public void CreateGoogleAnalyticsDirectLeadPro(string requestUniqueKey, int communityId, int specId, int planId)
        {
            string homeId;
            decimal price;
            var community = GetCommunity(communityId, specId, planId, out homeId, out price);
            var isComm = (planId == 0 && specId == 0);

            var addTrans = new GoogleAnalyticsTrans(community, true, requestUniqueKey)
                {
                    LeadType = GoogleGaConst.DirectLeadPro,
                    Format = "\"{0}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\"",
                    Shipping = community.Builder.BuilderName,
                    LeadFormType = community.CommunityName
                };

            var addItem = new GoogleAnalyticsItem(community, isComm, homeId)
                {
                    Price = price,
                    ItemDescription = community.Market.MarketName + " - " + community.Market.MarketId
                };
            addTrans.AddGoogleAnalyticsItem(addItem);
            AddTransList.Add(addTrans);
        }

        private Community GetCommunity(int communityId, int specId, int planId, out string homeId, out decimal price)
        {
            Community community;
            if (specId > 0)
            {
                var spec = _listingService.GetSpec(specId);
                community = spec.Community ?? _communityService.GetCommunity(spec.CommunityId);
                homeId = "S" + specId.ToType<string>();
                price = spec.Price;
            }
            else if (planId > 0)
            {
                var plan = _listingService.GetPlan(planId);
                community = plan.Community ?? _communityService.GetCommunity(plan.CommunityId);
                homeId = "P" + planId.ToType<string>();
                price = plan.Price;
            }
            else
            {
                homeId = "";
                price = 0;
                community = _communityService.GetCommunity(communityId);
            }

            return community;
        }

        private Community GetCommunity(int communityId = 0, int specId = 0, int planId = 0)
        {
            string homeId;
            decimal price;
            return GetCommunity(communityId, specId, planId, out homeId, out price);
        }
        
        public override string ToString()
        {
            var push = new StringBuilder();

            foreach (var addTrans in AddTransList)
            {
                push.AppendLine(addTrans.ToString());
            }

            if (push.Length > 0)
            {
                push.AppendLine("_gaq.push(['_trackTrans']);");
                push.AppendLine("_gaq.push(['_clearTrans']);");
            }
            return push.ToString();
        }

    }
}
