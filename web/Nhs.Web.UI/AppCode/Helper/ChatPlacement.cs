﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Nhs.Library.Common;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.Helper
{
    //Ticket : 69936

    public abstract class EnumBaseTypeChatPlacement<T> where T : EnumBaseTypeChatPlacement<T>
    {
        protected static List<T> EnumValues = new List<T>();

        public readonly int Value;
        public readonly string Key;

        protected EnumBaseTypeChatPlacement(int value, string key)
        {
            Key = key;
            Value = value;
            EnumValues.Add((T)this);
        }

        protected static ReadOnlyCollection<T> GetBaseValues()
        {
            return EnumValues.AsReadOnly();
        }

        protected static T GetBaseByKey(string key)
        {
            return EnumValues.FirstOrDefault(t => t.Key == key);
        }

        /// <summary>
        /// In this case use the Value as string 
        /// </summary>
        /// <returns>the Value property as a String</returns>
        public override string ToString()
        {
            return Value.ToType<string>();
        }
    }

    public class ChatPlacement : EnumBaseTypeChatPlacement<ChatPlacement>
    {
        public static readonly ChatPlacement MoveNhsDropInWindow = new ChatPlacement(Configuration.ChatPlacementMoveNhsDropInWindowId, "MoveNhsDropInWindow");
        public static readonly ChatPlacement NhsChatLink = new ChatPlacement(Configuration.ChatPlacementNhsChatLink, "NhsChatLink");
        public static readonly ChatPlacement NhsChatTab = new ChatPlacement(Configuration.ChatPlacementNhsChatTab, "NhsChatTab");
        public static readonly ChatPlacement MoveButton = new ChatPlacement(Configuration.ChatPlacementMoveButton, "MoveButton");
        public static readonly ChatPlacement MoveTab = new ChatPlacement(Configuration.ChatPlacementMoveTab, "MoveTab");

        public ChatPlacement(int value, string key)
            : base(value, key)
        {
        }

        public static ReadOnlyCollection<ChatPlacement> GetValues()
        {
            return GetBaseValues();
        }

        public static ChatPlacement GetByKey(string key)
        {
            return GetBaseByKey(key);
        }       
    }  
}