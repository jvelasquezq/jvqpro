﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nhs.Web.UI.AppCode.Helper
{
    /// <summary>
    /// 65146
    /// Change GA Transaction 'Lead type' and GA Item 'Lead Type' to the following values
    /// </summary>
    public class GoogleGaConst
    {
        public const string Direct = "Direct";
        public const string RCM = "RCM";
        public const string MultiSelect = "Multi-Select";
        public const string DirectLeadPro = "Send Custom Brochure";
        public const string RecoModal = "RCM-Modal";
        public const string DirectThankYou = "Direct-Thank You";

        /// <summary>
        /// Return User
        /// </summary>
        public const string DirectReturn = "Direct-Return";
        public const string RcmReturnUser = "RCM-Return User";


        /// <summary>
        /// Page Details Modal with RCM
        /// </summary>
        public const string RcmRecComModal = "RCM-Rec Com Modal";
      

        /// <summary>
        /// Reco Email 
        /// </summary>
        public const string RcmMatchEmail = "RCM-Match Email";

        public const string RcmEmail = "RCM-Email";

        public class GoogleGaItemConst
        {
            public const string Community = "COM";
            public const string Home = "HOM";
        }

        public class GoogleGaILeadFormTypeConst
        {
            public const string Modal = "Modal";
            public const string OnPage = "On Page";
            public const string FullScreen = "Full Screen";
        }

        
    }
}
