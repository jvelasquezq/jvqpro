﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Nhs.Library.Common;
using Nhs.Library.Helpers;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.Helper
{
    public class ListingServiceHelper
    {
        public bool UseHub { get; set; }
     
        public string GetAutoDescriptionForHome(IPlan plan, ISpec spec)
        {
            DataTable homeSummary;
            IPlan tmpPlan;

            if (spec != null)
            {
                homeSummary = DataProvider.Current.GetHomeSummaryBySpec(spec.SpecId, UseHub);
                tmpPlan = UseHub ? plan : spec.Plan;
            }
            else
            {
                homeSummary = DataProvider.Current.GetHomeSummaryByPlan(plan.PlanId, UseHub);
                tmpPlan = plan;
            }
            return GenerateAutoDescription(homeSummary, tmpPlan, spec);
        }

        private string GenerateAutoDescription(DataTable dtHomeSummary, IPlan plan, ISpec spec)
        {
            string features = string.Empty;
            string planName = plan.PlanName;

            if (string.IsNullOrEmpty(planName))
                if (plan.Specs.Count > 0)
                    planName = plan.Specs.First().PlanName;

            string homeDesc = String.Format("<strong>{0}</strong>", planName);

            if (spec != null && spec.SqFt != null && spec.SqFt > 0)
                homeDesc = String.Format("{0} ({1} {2})", homeDesc, spec.SqFt, LanguageHelper.MSG_SQ_FT);

            else if (plan.SqFt != null && plan.SqFt > 0)
                homeDesc = String.Format("{0} ({1} {2})", homeDesc, plan.SqFt, LanguageHelper.MSG_SQ_FT);

            homeDesc = String.Format("{0} {1}", homeDesc, LanguageHelper.MSG_IS_A);

            if (spec != null && spec.Stories != null && spec.Stories > 0)
                homeDesc = homeDesc + " " + spec.Stories + "-" + LanguageHelper.MSG_STORY;
            else if (plan.Stories != null && plan.Stories > 0)
                homeDesc = String.Format("{0} {1}-{2}", homeDesc, plan.Stories, LanguageHelper.MSG_STORY);

            if (plan.PlanTypeCode.Trim() != "SF")
            {
                switch (plan.Location)
                {
                    case 1:
                        homeDesc = String.Format("{0} {1}", homeDesc, LanguageHelper.MSG_INTERIOR);
                        break;
                    case 2:
                        homeDesc = String.Format("{0} {1}", homeDesc, LanguageHelper.MSG_END);
                        break;
                    case 3:
                        homeDesc = String.Format("{0} {1}", homeDesc, LanguageHelper.MSG_CORNER);
                        break;
                }

                if ((plan.Townhome == 1) && (plan.Condo == 1))
                    homeDesc = String.Format("{0} {1}", homeDesc, LanguageHelper.CondoTownhome);

                if ((plan.Townhome == 1) && (plan.Condo != 1))
                    homeDesc = String.Format("{0} {1}", homeDesc, LanguageHelper.MSG_TOWNHOME);

                if ((plan.Townhome != 1) && (plan.Condo == 1))
                    homeDesc = String.Format("{0} {1}", homeDesc, LanguageHelper.MSG_CONDOMINIUM);

                if ((plan.Townhome != 1) && (plan.Condo != 1))
                    homeDesc = String.Format("{0} {1}", homeDesc, LanguageHelper.MSG_HOME);
            }
            else if (plan.PlanTypeCode.Trim() == "SF")
            {
                homeDesc = homeDesc + " " + LanguageHelper.MSG_HOME;
            }

            if (plan.Floors > 0)
            {
                switch (plan.Floors)
                {
                    case 1:
                        homeDesc = homeDesc + " " + LanguageHelper.MSG_ON_THE_FIRST_FLOOR + ",";
                        break;
                    case 2:
                        homeDesc = homeDesc + " " + LanguageHelper.MSG_ON_THE_SECOND_FLOOR + ",";
                        break;
                    case 3:
                        homeDesc = homeDesc + " " + LanguageHelper.MSG_ON_THE_THIRD_FLOOR + ",";
                        break;
                    default:
                        homeDesc = homeDesc + " " + string.Format(LanguageHelper.MSG_ON_THE_NTH_FLOOR, plan.Floors);
                        break;
                }
            }

            if (spec != null)
            {

                if (spec.Bedrooms == 1)
                {
                    homeDesc = homeDesc + " " + LanguageHelper.MSG_WITH + " 1 " + LanguageHelper.MSG_BEDROOM;
                }
                else if (spec.Bedrooms > 1)
                {
                    homeDesc = homeDesc + " " + LanguageHelper.MSG_WITH + " " + spec.Bedrooms + " " +
                               LanguageHelper.MSG_BEDROOMS;
                }

                if (spec.Bathrooms == 1 && spec.HalfBaths == 0)
                {
                    homeDesc = homeDesc + ", 1 " + LanguageHelper.MSG_BATHROOM;
                }
                else if (spec.Bathrooms > 1)
                {
                    homeDesc = homeDesc + ", " + ListingHelper.ComputeBathrooms(spec.Bathrooms.ToType<int>(), spec.HalfBaths.ToType<int>()) + " " + LanguageHelper.MSG_BATHROOMS;
                }

                if (spec.Garages == 1)
                {
                    homeDesc = homeDesc + " " + LanguageHelper.MSG_AND_1CAR_GARAGE;
                }
                else if (spec.Garages.ToType<int>() > 1)
                {
                    homeDesc = homeDesc + " " + LanguageHelper.MSG_AND + " " +string.Format(LanguageHelper.MSG_NCAR_GARAGE,spec.Garages);
                }
            }
            else
            {
                if (plan.Bedrooms == 1)
                {
                    homeDesc = homeDesc + " " + LanguageHelper.MSG_WITH + " 1 " + LanguageHelper.MSG_BEDROOM;
                }
                else if (plan.Bedrooms > 1)
                {
                    homeDesc = homeDesc + " " + LanguageHelper.MSG_WITH + " " + plan.Bedrooms + " " +
                               LanguageHelper.MSG_BEDROOMS;
                }

                if (plan.Bathrooms == 1 && plan.HalfBaths == 0)
                {
                    homeDesc = homeDesc + ", 1 " + LanguageHelper.MSG_BATHROOM;
                }
                else if (plan.Bathrooms > 0)
                {
                    homeDesc = homeDesc + ", " + ListingHelper.ComputeBathrooms(plan.Bathrooms.ToType<int>(), plan.HalfBaths.ToType<int>()) + " " + LanguageHelper.MSG_BATHROOMS;
                }

                if (plan.Garages == 1)
                {
                    homeDesc = homeDesc + " " + LanguageHelper.MSG_AND_1CAR_GARAGE;
                }
                else if (plan.Garages != null && plan.Garages > 1)
                {
                    homeDesc = homeDesc + " " + LanguageHelper.MSG_AND + " " + string.Format(LanguageHelper.MSG_NCAR_GARAGE,plan.Garages);
                }

            }

            int index = 1;
            int totalcount = dtHomeSummary.Rows.Count;

            if (totalcount > 0)
            {
                if (totalcount == 1 && dtHomeSummary.Rows[0]["feature_name"].ToString() == "Excludes Land")
                    homeDesc += ". ";
                else
                    homeDesc = homeDesc + ". " + LanguageHelper.MSG_FEATURES_INCLUDE + " ";
            }
            foreach (DataRow dr in dtHomeSummary.Rows)
            {
                var feature = dr["feature_name"].ToString().ToLower();

                var mblDesc = GetMasterUpstairs(dr);

                if (!string.IsNullOrEmpty(mblDesc))
                    feature = mblDesc;

                if (index == totalcount)
                {
                    features = totalcount > 1 ? features + " " + LanguageHelper.MSG_AND + " " + feature
                                   : features + feature;
                }
                else if (index == totalcount - 1)
                {
                    features = features + feature;
                }
                else if (index != totalcount)
                {
                    features = features + feature + ", ";
                }

                index++;
            }

            if (totalcount == 1 && dtHomeSummary.Rows[0]["feature_name"].ToString() == "Excludes Land")
                features = string.Concat(features.Substring(0, 1).ToUpper(), features.Substring(1));

            homeDesc = homeDesc + features;
            homeDesc += ".";
            return homeDesc;
        }

        private string GetMasterUpstairs(DataRow dr)
        {
            string feature = string.Empty;

            if (dr["feature_list_code"].ToString() == "MAS")
            {
                feature = int.Parse(dr["feature_value"].ToString()) == 0
                        ? LanguageHelper.MasterBedDownstairs.ToLower()
                        : LanguageHelper.MasterBedUpstairs.ToLower();

            }

            return feature;
        }
    }
}