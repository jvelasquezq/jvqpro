﻿using System;
using System.Collections.ObjectModel;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Utility.Common;
using ResourceCombiner.Core.API;

namespace Nhs.Web.UI.AppCode.Helper
{
    public partial class ResourceCombinerEnum : StringEnumBase<ResourceCombinerEnum>
    {

        public ResourceCombinerEnum(string key, int value, object extraData = null)
            : base(key, value, extraData)
        {
        }

        public static ReadOnlyCollection<ResourceCombinerEnum> GetValues()
        {
            return GetBaseValues();
        }

        public static ResourceCombinerEnum GetByKey(string key)
        {
            return GetBaseByKey(key);
        }

        public MvcHtmlString GetCrunchedResource(bool escapeScriptTag = false)
        {
            var link = BuildCrunchedResource(ToString(), escapeScriptTag);

            return MvcHtmlString.Create(link);
        }

        public static string BuildCrunchedResource(string resourceSetName, bool escapeScriptTag = false)
        {
            var webExtension = new WebExtensions();
            var debug = HttpContext.Current.Request.QueryString["jsdebug"];
            bool isdebug;
            bool.TryParse(debug, out isdebug);
            var link = webExtension.GetCombinedLink(resourceSetName, null, Configuration.ResourceDomain,
                "?" + Configuration.ResourceVersionNumber, isdebug);

            if (escapeScriptTag)
                link = link.Replace(@"</script>", @"<\/script>").Replace(Environment.NewLine, string.Empty);

            if (isdebug)
            {
                link = link.Contains("rel=\"stylesheet\"")
                    ? link.Replace("href=", "data-NHS-Resource=\"" + resourceSetName + "\" href=")
                    : link.Replace("src=", "data-NHS-Resource=\"" + resourceSetName + "\" src=");
            }

            return link;
        }
    }
}