﻿using StructureMap;
using System.Web.Mvc;

namespace Nhs.Web.UI.AppCode.Lib.StructureMap
{
    public class InjectingActionInvoker:ControllerActionInvoker
    {
        private readonly IContainer _container;

        public InjectingActionInvoker(IContainer container)
        {
            _container = container;
        }

        protected override FilterInfo GetFilters(
            ControllerContext controllerContext,
            ActionDescriptor actionDescriptor)
        {
            FilterInfo info = base.GetFilters(controllerContext, actionDescriptor);

            foreach (var filter in info.AuthorizationFilters)
                _container.BuildUp(filter);

            foreach (var filter in info.ActionFilters)
                _container.BuildUp(filter);

            foreach (var filter in info.ResultFilters)
                _container.BuildUp(filter);

            foreach (var filter in info.ExceptionFilters)
                _container.BuildUp(filter);

            return info;
        }
    }
}
