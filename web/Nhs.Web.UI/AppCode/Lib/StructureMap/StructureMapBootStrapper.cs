﻿using Nhs.Library.Common;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services;
using Nhs.Mvc.Data.Repositories;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Filters;
using StructureMap;

namespace Nhs.Web.UI.AppCode.Lib.StructureMap
{
    public static class StructureMapBootStrapper
    {
        public static void Configure()
        {
            ObjectFactory.Container.Configure(r => r.AddRegistry(new NhsAppRegistry()));
            ObjectFactory.Container.Configure(r => r.AddRegistry(new NhsDataRegistry()));
            ObjectFactory.Container.Configure(r => r.AddRegistry(new NhsServicesRegistry()));
            ConfigureActionFilterAttribs();
        }

        private static void ConfigureActionFilterAttribs()
        {
            //PageHeaderAttribute
            IPathMapper pathMapper = new ContextPathMapper();
            MetaReader tagGen = new MetaReader(pathMapper);
            ObjectFactory.Container.Configure(r => r.ForConcreteType<PageHeaderAttribute>().
                                              Configure.WithProperty("MetaReader").
                                              EqualTo(tagGen));

            //ShellFilterAttribute
            ShellFactory shellFactory = new ShellFactory();
            ObjectFactory.Container.Configure(r => r.ForConcreteType<ShellFilterAttribute>().
                                              Configure.WithProperty("ShellFactory").
                                              EqualTo(shellFactory));

            //LiveChatAttribute
            ObjectFactory.Container.Configure(r => r.ForConcreteType<LiveChatAttribute>().
                                              Configure.WithProperty("PathMapper").
                                              EqualTo(pathMapper));
        }
    }
}
