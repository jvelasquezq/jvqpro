﻿using System.Web.Mvc;

namespace Nhs.Web.UI.AppCode.Lib
{
    public class ViewPageBase : ViewPage
    {

    }

    public class ViewPageBase<T> : ViewPage<T> where T : class
    {

    }

    public class ViewMasterPageBase : ViewMasterPage
    {

    }

    public class ViewMasterPageBase<T> : ViewMasterPage<T> where T : class
    {

    }
}