﻿using System.Web.Mvc;
using Nhs.Library.Business.Interface;
using Nhs.Library.Helpers.Seo;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Web.UI.AppCode.Filters;
using Nhs.Web.UI.AppCode.Lib.StructureMap;
using StructureMap.Configuration.DSL;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.Lib
{
    public class NhsAppRegistry : Registry
    {
        public NhsAppRegistry()
        {
            //Setup Path Mapper to use Context
            For<IPathMapper>().Use<ContextPathMapper>();
            For<IMarketDfuReader>().Use<MarketDfuReader>();

            //Tap into pipeline
            For<IActionInvoker>().Use<InjectingActionInvoker>();
            For<ITempDataProvider>().Use<SessionStateTempDataProvider>();

            //Setup setter injection for action filters
            SetAllProperties(c =>
            {
                c.OfType<IActionInvoker>();
                c.OfType<ITempDataProvider>();
                c.OfType<ICmsService>();
                c.WithAnyTypeFromNamespaceContainingType<PageHeaderAttribute>();
                c.WithAnyTypeFromNamespaceContainingType<ShellFilterAttribute>();
                c.WithAnyTypeFromNamespaceContainingType<LiveChatAttribute>();               
            });
        }
    }
}
