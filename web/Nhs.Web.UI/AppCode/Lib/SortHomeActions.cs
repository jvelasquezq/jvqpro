﻿using System.Collections.Generic;
using System.Linq;
using Nhs.Search.Objects.Constants;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Extensions;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Web.UI.AppCode.Lib
{
    public delegate List<HomeItem> SortOptionHomesDelegate(IEnumerable<HomeItem> homeResutls);

    public class SortHomeActions
    {
        private readonly Dictionary<SortOrder, SortOptionHomesDelegate> _sortOptions;
        public Dictionary<SortOrder, SortOptionHomesDelegate> SortOptions { get { return _sortOptions; } }
        public SortHomeActions()
        {
            _sortOptions = new Dictionary<SortOrder, SortOptionHomesDelegate> {
                {SortOrder.Status, StatusSortOrder},
                {SortOrder.Name, PlanNameSortOrder},
                {SortOrder.Size, SizeSortOrder},
                {SortOrder.Price, PriceSortOrder},
            };
        }

        public List<HomeItem> StatusSortOrder(IEnumerable<HomeItem> homeResults)
        {
            if (homeResults == null)
                return new List<HomeItem>();

            return  homeResults.OrderByDescending(f1 => f1.IsHotHome.ToType<bool>())
                                .ThenByDescending(f2 => f2.Status.ToHomeStatusType() == HomeStatusType.ModelHome)
                                .ThenByDescending(f3 => f3.Status.ToHomeStatusType() == HomeStatusType.AvailableNow)
                                .ThenByDescending(f4 => f4.Status.ToHomeStatusType() == HomeStatusType.UnderConstruction)
                                .ThenByDescending(f5 => f5.Price.ToType<decimal>())
                                .ThenByDescending(f7 => f7.Br).ToList();
        }

        public List<HomeItem> PriceSortOrder(IEnumerable<HomeItem> homeResutls)
        {
            if (homeResutls == null)
            {
                return new List<HomeItem>();
            }

            return homeResutls.OrderByDescending(f1 => f1.Price.ToType<decimal>()).ThenBy(f2 => f2.PlanName).ToList();
        }

        public List<HomeItem> PlanNameSortOrder(IEnumerable<HomeItem> homeResutls)
        {
            if (homeResutls == null)
            {
                return new List<HomeItem>();
            }

            return homeResutls.OrderBy(f1 => f1.PlanName).ThenByDescending(f2 => f2.Price.ToType<decimal>()).ToList();
        }
        public List<HomeItem> SizeSortOrder(IEnumerable<HomeItem> homeResutls)
        {
            if (homeResutls == null)
            {
                return new List<HomeItem>();
            }

            return homeResutls.OrderByDescending(f1 => f1.Br).ThenByDescending(
                                     f2 => f2.Ba + ((f2.HBa > 0) ? 0.5 : 0.0)).
                                     ThenByDescending(f3 => f3.Gr).ThenByDescending(f4 => f4.Price.ToType<decimal>()).ToList();
        }
    }
}
