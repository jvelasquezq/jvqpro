﻿namespace Nhs.Web.UI.AppCode.ViewModels.Session
{
    public class SessionViewModel : BaseViewModel
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SessionId { get; set; }
    }
}