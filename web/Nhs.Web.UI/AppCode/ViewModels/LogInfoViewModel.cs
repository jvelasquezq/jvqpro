﻿using System.Collections.Generic;
using Nhs.Library.Business;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class LogInfoViewModel : BaseViewModel
    {
        public int CommunityId { get; set; }
        public string CommunityName { get; set; }
        public int BuilderId { get; set; }
        public string BuilderName { get; set; }
        public int MarketId { get; set; }
        public string MarketName { get; set; }
        
    }
}