﻿using System;
using System.Collections.Generic;
using Nhs.Library.Business.Map;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class PropertyMapViewModel : BaseViewModel, IPropertyMapViewModel
    {
        public string PropertyName { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double RouteLat { get; set; }
        public double RouteLng { get; set; }
        public string DrivingDirections { get; set; }
        public string BrandName { get; set; }
        public string BuilderMapUrl { get; set; }
        public string UnderneathMapCommName { get; set; }
        public string UnderneathMapAddress { get; set; }
        public bool HideDrivingDirections { get; set; }
        public bool PreviewMode { get; set; }
        public int MarketId { get; set; }
        public int CommunityId { get; set; }
        public int BuilderId { get; set; }
        public int PlanId { get; set; }
        public int SpecId { get; set; }
        public bool EnableRequestAppointment { get; set; }
        public string StartingPoint { get; set; }

        public DrivingDirections DrivingDirectionsList { get; set; }
        public List<NearbyCommunity> NearByComms { get; set; }
        public string NearByCommsObject { get; set; }
        public IList<NearbyCommunity> NearByCommsSmall { get; set; }
        public IList<NearbyHome> NearByHomes { get; set; }
        public bool IsBilled { get; set; }
        public bool IsPlan { get; set; }
        public string BuilderName { get; set; }
        public bool IsCommunityMultiFamily { get; set; }

        public string DrivingDirectionsShort
        {
            get
            {
                if (!string.IsNullOrEmpty(DrivingDirections) && DrivingDirections.Length > 39)
                    return DrivingDirections.Substring(0, 39) + "...";

                return DrivingDirections;
            }
        }
    }

    public interface IPropertyMapViewModel
    {
        double Latitude { get; set; }
        double Longitude { get; set; }
        double RouteLat { get; set; }
        double RouteLng { get; set; }
        string PropertyName { get; set; }
        string DrivingDirections { get; set; }
        bool HideDrivingDirections { get; set; }
        string DrivingDirectionsShort { get; }
        DrivingDirections DrivingDirectionsList { get; set; }
        List<NearbyCommunity> NearByComms { get; set; }
        string NearByCommsObject { get; set; }
        IList<NearbyCommunity> NearByCommsSmall { get; set; }
        string BrandName { get; set; }
        string BuilderMapUrl { get; set; }
        string UnderneathMapCommName { get; set; }
        string UnderneathMapAddress { get; set; }
        int MarketId { get; set; }
        int CommunityId { get; set; }
        int BuilderId { get; set; }
        int PlanId { get; set; }
        int SpecId { get; set; }
        bool EnableRequestAppointment { get; set; }
        bool IsBilled { get; set; }
        bool IsPlan { get; set; }
        string BuilderName { get; set; }
        bool IsCommunityMultiFamily { get; set; }
        bool PreviewMode { get; set; }
    }

}
