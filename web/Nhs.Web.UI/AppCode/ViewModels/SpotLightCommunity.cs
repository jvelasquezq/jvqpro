﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Content;
using Nhs.Library.Web;
using Nhs.Library.Web.ImageResizer;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels;
namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class SpotLightCommunity : BaseViewModel
    {
        private readonly string _communityName;
        private readonly string _communityId;
        private readonly string _brandName;
        private readonly string _builderId;
        private readonly string _city;
        private readonly string _state;
        private readonly string _stateName;
        private readonly int _marketId;
        private readonly string _marketName;
        private readonly int _priceLow;
        private readonly int _priceHigh;
        private readonly string _spotLightImage;
        private readonly bool _hasHotHome;
        private readonly bool _hasVideo;
        private readonly bool _gree;
        private readonly string _type;
        private readonly int _promoId;

        public string CommunityName
        {
            get { return _communityName; }
        }

        public string CommunityId
        {
            get { return _communityId; }
        }

        public string BuilderId
        {
            get { return _builderId; }
        }

        public int MarketId
        {
            get { return _marketId; }
        }

        public string MarketName
        {
            get { return _marketName; }
        }

        public int PromoId
        {
            get { return _promoId; }
        }

        public string BrandName
        {
            get { return _brandName; }
        }

        public string City
        {
            get { return _city; }
        }

        public string State
        {
            get { return _state; }
        }

        public string StateName
        {
            get { return _stateName; }
        }

        public int PriceLow
        {
            get { return _priceLow; }
        }

        public int PriceHigh
        {
            get { return _priceHigh; }
        }

        public string SpotLightImage
        {
            get
            {
                if (!string.IsNullOrEmpty(_spotLightImage) && _spotLightImage != "N")
                {
                    var image = ImageResizerUrlHelpers.FormatToIrsImageName(_spotLightImage);
                    return ImageResizerUrlHelpers.BuildIrsImageUrl(image, ImageSizes.CommDetailThumb,true);
                }
                
                return  Resources.GlobalResources14.Default.images.no_photo.no_photos_180x120_png;
            }
        }

        public bool HasHotHome
        {
            get { return _hasHotHome; }
        }

        public bool Green
        {
            get { return _gree; }
        }

        public string Type
        {
            get { return _type; }
        }

        public bool HasVideo
        {
            get { return _hasVideo; }
        }

        public string Url
        {
            get
            {

                return this.ToCommunityDetail().ToUrl(RedirectionHelper.GetCommunityDetailPage());
            }
        }


        public SpotLightCommunity(string communityName, int communityId, int marketId, string marketName, string brandName, string builderId, string city, string state, string stateName, int priceLow, int priceHigh, string spotLightImage, bool hasHotHome, bool hasVideo, int promoId, bool green, string type)
        {
            _communityName = communityName;
            _communityId = communityId.ToString();
            _brandName = brandName;
            _builderId = builderId;
            _city = city;
            _state = state;
            _stateName = stateName;
            _priceLow = priceLow;
            _priceHigh = priceHigh;
            _spotLightImage = spotLightImage;
            _hasHotHome = hasHotHome;
            _hasVideo = hasVideo;
            _promoId = promoId;
            _marketId = marketId;
            _marketName = marketName;
            _gree = green;
            _type = type;
        }
    }
}
