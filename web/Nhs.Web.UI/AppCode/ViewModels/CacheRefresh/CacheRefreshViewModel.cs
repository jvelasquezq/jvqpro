﻿using System;
using System.Collections.Generic;

namespace Nhs.Web.UI.AppCode.ViewModels.CacheRefresh
{
    public class CacheRefreshViewModel
    {
        public string SeverName { get; set; }
        public string RemoteUrl { get; set; }
        public string ServerResponse { get; set; }
    }

    public class ServerCacheList : BaseViewModel
    {
        public List<CacheRefreshViewModel> CacheList { get; set; }
        public int WebServerCount { get; set; }
        public string WebServerPrefix { get; set; }
        public string LastRefresh { get; set; }
        public bool IsAuthorized { get; set; }

        public ServerCacheList()
        {
            IsAuthorized = false;
            CacheList = new List<CacheRefreshViewModel>(4);
        }
    }

    public class PasswordCacheViewModal
    {
        public string Password { get; set; }
        
    }
}
