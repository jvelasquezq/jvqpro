﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library;
using Nhs.Web.UI.AppCode.Helper;
using Market = Nhs.Mvc.Domain.Model.Web.Market;
using State = Nhs.Mvc.Domain.Model.Web.State;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class QuickMoveInSearchViewModel: BaseViewModel
    {
        public string StateAbbr { get; set; }
        public int MarketId { get; set; }
        public string PostalCode { get; set; }
        public string PriceFromValue { get; set; }
        public string PriceFromText { get; set; }
        public int PriceLow { get; set; }
        public int PriceHigh { get; set; }
        public int BedDrop { get; set; }
        public int BathDrop { get; set; }

        public IEnumerable<State> StateList { get; set; }
        public IEnumerable<Market> MarketList { get; set; }
        public SelectList LowPriceRange;
        public SelectList HighPriceRange;
        public SelectList BedList;
        public SelectList BathList;

        public bool ShowValidationMessage { get; set; }
        public string ValidationMessage
        {
            get { return "<p class=\"nhs_Error\">" + LanguageHelper.NoHomesWereFoundInThatZip + "</p>"; }
        }
        public bool ShowCustomAdd { get; set; }
        public bool ShowStaticContentCol { get; set; }
    }
}