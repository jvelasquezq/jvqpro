﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class WhatsNearbyViewModel : BaseViewModel
    {
        public string MapType { get; set; }
        public int CommunityId { get; set; }
        public int BuilderId { get; set; }
    }
}