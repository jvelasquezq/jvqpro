﻿using System.Collections.Generic;
using Nhs.Library.Web;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class BasicCommunityDetailViewModel : CommunityDetailViewModel
    {
        public bool IsBottom { get; set; }
        public bool ShowCta { get; set; }
        public IList<ContentTag> SeoContentTags { get; set; }
        public string H1 { get; set; } //Used for SEO
    }
}
