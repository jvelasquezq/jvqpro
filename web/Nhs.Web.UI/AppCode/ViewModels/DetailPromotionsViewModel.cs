﻿using System.Collections.Generic;
using Nhs.Library.Business;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class DetailPromotionsViewModel : IDetailPromotionsViewModel
    {
        public ICollection<GreenProgram> GreenPrograms { get; set; }
        public ICollection<Promotion> Promotions { get; set; }
        public ICollection<ExtendedHomeResult> HotHomes { get; set; }
        public ICollection<HomeItem> HotHomesApi { get; set; }
        public ICollection<Event> Events { get; set; }
    }
}