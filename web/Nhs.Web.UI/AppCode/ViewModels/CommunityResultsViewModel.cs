﻿using System.Collections.Generic;
using System.Web.Routing;
using Bdx.Web.Api.Objects;
using Nhs.Library.Business;
using Nhs.Library.Business.Map;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Search.Objects;
using Nhs.Search.Objects.Constants;
using Nhs.Utility.Data.Paging;
using Market = Nhs.Mvc.Domain.Model.Web.Market;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class CommunityResultsViewModel : BaseViewModel, IMediaGalleryViewModel
    {
        public bool TrigerByFacets { get; set; }
        public string PostalCode { get; set; }
        public bool ShowCode { get; set; }
        public string ListHubBasicListinIds { get; set; }
        public string ListHubBasicListinIdsList { get; set; }
        public string ListHubProviderId { get; set; }
        public string ListHubTestLogging { get; set; }
        public int CurrentPage { get; set; }

        public string BasicListingsIdList { get; set; }
        public string BasicCommunitiesIdList { get; set; }
        public string FeaturedListingsIdList { get; set; }
        public string CommunitiesIdList { get; set; }

        public CommunityResultsViewExt Results { get; set; }
        public IPagedList<ExtendedCommunityResult> Communities { get; set; }
        public IList<Brand> BrandsPromoted = new List<Brand>();
        public IList<Brand> BrandsCol1 = new List<Brand>();
        public IList<Brand> BrandsCol2 = new List<Brand>();
        public IList<Brand> BrandsCol3 = new List<Brand>();
        public int TotalBrandsCount = 0;
        public Market Market { get; set; }
        public List<Market> MarketsList { get; set; }
        public IList<City> CitiesCol1 = new List<City>();
        public IList<City> CitiesCol2 = new List<City>();
        public IList<City> CitiesCol3 = new List<City>();
        public IList<int> ZipCodesCol1 = new List<int>();
        public IList<int> ZipCodesCol2 = new List<int>();
        public IList<int> ZipCodesCol3 = new List<int>();
        public MapData Map { get; set; }
        public string SearchParametersJSon { get; set; }
        public string ClearSearchParametersJSon { get; set; }
        public string FindDealsSearchParametersJSon { get; set; }
        public string ClearFacetLinkText { get; set; }
        public SeoTemplateType SeoTemplateType { get; set; }
        public string City { get; set; }
        public string CommunityName { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string County { get; set; }
        public IList<ContentTag> SeoContentTags { get; set; }
        public List<MediaPlayerObject> ShowCaseMediaObjects { get; set; }
        public string ValidationMessage { get; set; }
        // Result counts
        public int AllCommunitiesResults { get; set; }
        public int HotHomesResults { get; set; }
        public int QuickMoveInResults { get; set; }

        public CommunityResultsTabs SelectedTab { get; set; }
        public SortOrder CurrentSortOrder { get; set; }

        public string PageLinkAction { get; set; }
        public RouteValueDictionary PagingRouteData { get; set; }
        public string MarketMapPdfFilePath { get; set; }

        public bool IsMarketSearch { get; set; }
        // if display or nor the cities facet option (only when it is a market search)
        public bool IsMapSearch { get; set; }
        public string LocationName { get; set; } // location name in the location facet box
        public string LocationHeading { get; set; }
        public string LocationDescripcion { get; set; }
        public int NumPostBacks { get; set; }
        public string AdUpdateTop { get; set; }
        public string AdUpdateMiddle { get; set; }
        public string AdUpdateMiddle3 { get; set; }
        public string AdUpdateRight2 { get; set; }

        public bool IsAjaxRequest { get; set; }
        public bool LocationChanged { get; set; }
        public bool IsAOnlyBasicListingsMarket { get; set; }

        public IList<ExtendedCommunityResult> FeaturedListingsTop = new List<ExtendedCommunityResult>();
        public IList<ExtendedCommunityResult> FeaturedListingsBottom = new List<ExtendedCommunityResult>();

        public bool IsMove { get; set; }

        #region IMediaGalleryViewModel

        public KeyValuePair<int, int> Size { get; set; }
        public string FirstImage { get; set; }
        public string PinterestDescription { get; set; }
        public string PageUrl { get; set; }
        public bool PreviewMode { get; set; }
        public bool IsBasicCommunity { get; set; }
        public bool IsCommunityResults { get; set; }

        #endregion

        public FindCustomBuilders FindCustomBuilders { get; set; }

        public bool AlreadyRequestBrosurePreviosComm { get; set; }
        public bool HasCommToRecommend { get; set; }
        public bool HasTheSameMarcketId { get; set; }
        public bool IsNewSession { get; set; }
        public bool ShowReturnUserModal { get; set; }
    }

    public class FindCustomBuilders
    {
        public FindCustomBuilders()
        {
            FindCustomBuildersBuilders = new List<FindCustomBuildersBuilders>();
        }

        public int MarketId { get; set; }
        public bool Show { get; set; }
        public string MarketName { get; set; }
        public int BuilderCount { get; set; }
        public List<FindCustomBuildersBuilders> FindCustomBuildersBuilders { get; set; }
    }

    public class FindCustomBuildersBuilders
    {
        public string BuildeName { get; set; }
        public string CommunityImage { get; set; }
        public string BuildeImage { get; set; }
    }

    public class ReturnedUserRecoComm : BaseViewModel
    {
        public bool DefaultChecked { get; set; }
        public bool MatchEmail { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Zip { get; set; }

        public string Address { get; set; }
        public int CommunityId { get; set; }
        public int BuilderId { get; set; }
        public string CommThumbnail { get; set; }
        public string CommunityName { get; set; }
        public string CommPrice { get; set; }
        public int MarketId { get; set; }


        public List<ApiRecoCommunityResult> RecoCommunitys { get; set; }
        public string HeadlineText { get; set; }
        public string SubheadText { get; set; }

        public string GetHeadlineText
        {
            get
            {
                return !string.IsNullOrEmpty(HeadlineText)
                           ? HeadlineText
                           : "Recommended Communities Found!";
            }
        }

        public string GetSubheadText
        {
            get
            {
                return !string.IsNullOrEmpty(SubheadText)
                           ? SubheadText
                           : "Get free brochures for your recently viewed community along with similar communities currently available.";
          }
        }
    }

    public class ReturnedUserRecoCommViewModel : LeadInfoViewModel
    {
        public bool IsCheckMainCommBase { get; set; }
        public string FullName { get; set; }
    }
}
