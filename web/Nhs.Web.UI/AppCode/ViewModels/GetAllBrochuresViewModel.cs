﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class GetAllBrochuresViewModel:BaseViewModel
    {
        public string Google { get; set; }
        public string ConversionIFrameSource { get; set; }
        public string MarketName { get; set; }
    }
}