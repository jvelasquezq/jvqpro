﻿using System.Collections.Generic;
using Nhs.Library.Business;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class CommunityQuickViewViewModel: BaseViewModel
    {
        public IEnumerable<ExtendedHomeResult> HomeResults { get; set; }
    }
}
