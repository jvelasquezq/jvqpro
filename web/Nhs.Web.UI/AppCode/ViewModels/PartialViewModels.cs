﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Nhs.Library.Business.Seo;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class PartialViewModels
    {
        public class PartnerNavigation : BaseViewModel
        {
            public bool NewHomeGuideLink
            { get; set; }

            public bool SignInLink
            { get; set; }

            public bool SignOutLink
            { get; set; }

            public bool PartnerAllowsRegistration
            { get; set; }

            public bool PartnerUsesSso { get; set; }
        }

        public class PersonalBar : BaseViewModel
        {
            public bool SpotlightHomesVisible { get; set; }
            public bool NoOfHomesVisible { get; set; }
            public string NoHomesText { get; set; }
            public bool RecentItemsLinkVisible { get; set; }
            public bool RecentItemsLinkActiveUserVisible { get; set; }
            public bool SavedHomeVisible { get; set; }
            public string SavedHomeLnkText { get; set; }
            public bool SuccessMessageVisible { get; set; }
            public string SuccessMessageText { get; set; }
        }

        public class BDXFooter : BaseViewModel
        {
            public bool NhsFooterAdBoxVisible { get; set; }
            public bool NhsFooterVisible { get; set; }
            public bool MoveFooterVisible { get; set; }
            public bool ShowAboutFooter { get; set; }
            public bool ShowRegistrationLinks { get; set; }
        }


        public class AdViewModel : BaseViewModel
        {
            public string Position { get; set; }
            public bool UseFrame { get; set; }
            public bool JavaScriptParams { get; set; }
        }

        public class HwNhlFooter : BaseViewModel
        {
            public Partner Partner { get; set; }
        }

        public class PartnerFooter : BaseViewModel
        {
            public Partner Partner { get; set; }
            public bool IsPassiveUser { get; set; }
        }

        public class TrackingScripts : BaseViewModel
        {
            public IList<string> ScriptFiles;
            public string PartnerSiteUrl { get { return NhsRoute.PartnerSiteUrl; } }
        }

        public class SeoLinkPromotion : BaseViewModel
        {
            public string Title;
            public List<SeoLinkPromotionItem> Links;
            public bool ShowPromoArea
            {
                get
                {
                    return !string.IsNullOrEmpty(Title) && Links.Count > 0 && (base.Globals.PartnerLayoutConfig.PartnerIsABrand || base.Globals.PartnerLayoutConfig.IsPartnerNhl);
                }
            }
        }

        public class BreadcrumbViewModel : BaseViewModel
        {
            public string StateName { get; set; }
            public string State { get; set; }
            public string MarketName { get; set; }
            public int MarketId { get; set; }
            public string CityName { get; set; }
            public string CountyName { get; set; }
            public string ZipCode { get; set; }
            public string CommunityName { get; set; }
            public int CommunityId { get; set; }
            public int BuilderId { get; set; }
            public string ListingName { get; set; }
            public string CommunityCity { get; set; }
            public string CommunityState { get; set; }
            public string BrandName { get; set; }

            public bool IsVisible { get; set; }
            public bool IsInDetailsPage { get; set; }
            public bool IsCityVisible { get; set; }
            public bool IsCountyVisible { get; set; }
            public bool IsZipVisible { get; set; }
            public bool IsSyntheticSearch { get; set; }
            public bool IsCommunityVisible { get; set; }
            public bool IsRecommVisible { get; set; }
            public bool IsListingVisible { get; set; }
            public List<RouteParam> BackToSearchParams { get; set; }
            public bool HasValidMarketIdInSession { get; set; }
        }

        public class AlertResultViewModel : BaseViewModel
        {
            public string SupportEmail { get; set; }
            public string FromEmail { get; set; }
            public string SiteName { get; set; }
            //public string UserId { get; set; }
            public Boolean IsWeeklyEmail { get; set; }
            public IList<SearchAlert> AlertList { get; set; }

            public int? MarketId { get; set; }
            public string MarketName { get; set; }
            public string StateName { get; set; }

            public UserProfile UserProfile { get; set; }
            public string AlertName { get; set; }
            public List<Community> FinalResults { get; set; }
            public string WelcomeText { get; set; }
            public int TotalNumberOfHomes { get; set; }
            public int TotalNumberOfCommunities { get; set; }
            public string UtmParameters { get; set; }
            public bool UserFriendlyUrls { get; set; }
        }

        public class SendFriend : BaseViewModel
        {
            public string SupportEmail { get; set; }
            public string FriendEmail { get; set; }
            public string LeadComments { get; set; }
            public bool SendMe { get; set; }

            public int SpecId { get; set; }
            public int PlanId { get; set; }
            public int CommunityId { get; set; }
            public int BuilderId { get; set; }
            public string FromEmail { get; set; }
            public string SiteName { get; set; }

            public Community Community { get; set; } 
            public Spec Spec { get; set; } 
            public IEnumerable<Promotion> Promotions { get; set; } 
            public string HomeLink { get; set; }
            public bool HasHotHomes { get; set; }
            public HomeResult HotHomeSpec { get; set; }
            public int TotalNumberOfCommunities { get; set; }
            public int TotalNumberOfHomes { get; set; }

            public bool ShowSuccessMessage { get; set; }
        }

        public class RecoverPasswordEmail : BaseViewModel
        {
            public string Email { get; set; }
            public string FromEmail { get; set; }
            public string SiteName { get; set; }
            public string Password { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string UserId { get; set; }

        }

        public class ManageAccount : BaseViewModel
        {
            public string LogonName { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string EmailConfirm { get; set; }
            public bool InternationalPhone { get; set; }
            public string Password { get; set; }
            public string PasswordConfirm { get; set; }
            public string MoveInDate { get; set; }
            public IEnumerable<SelectListItem> MoveInDates { get; set; }
            public string HomeFinancial { get; set; }
            public SelectList HomeFinancials { get; set; }
            public string WhyLookingNewHome { get; set; }
            public IEnumerable<SelectListItem> WhyLookingNewHomes { get; set; }
            public bool ContactMe { get; set; }
            public bool SendMePromos { get; set; }      
            public string AgencyName { get; set; }
            public string RealEstateLicense { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string CityPro { get; set; }
            public string StatePro { get; set; }
            public IEnumerable<SelectListItem> States { get; set; }
            public string HomeMarket { get; set; }
            public string StreetAdress { get; set; }
            public string ZipCode { get; set; }
            public string BusinessPhone { get; set; }
            public string MobilePhone { get; set; }
            public bool Newsletter { get; set; }
            public bool Promos { get; set; }
           public bool PromosPro { get; set; }
            public bool IsPopupWindow { get; set; }
        }
    }
}
