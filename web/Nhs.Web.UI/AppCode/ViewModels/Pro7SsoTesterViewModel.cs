﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nhs.Mvc.Domain.Model.Profiles;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class Pro7SsoTesterViewModel : BaseViewModel
    {
        public string UserInfo { get; set; }
        public string Passcode { get; set; }
        public string SelectedAlgorithm { get; set; }
        public IEnumerable<SelectListItem> Algorithms;
        public string UtcDatetime { get; set; }
        public string HashedPasscode { get; set; }
        public string PostUrl { get; set; }
        public UserProfile UserProfile { get; set; }
    }
}
