﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class ChatLeadGenViewModel : BaseViewModel
    {
        // User information fields
        [Required(ErrorMessage = @"The First Name field is required.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = @"The Last Name field is required.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = @"The Email Address field is required.")]
        [RegularExpression(StringHelper.EmailRegexfull, ErrorMessage = @"Please provide a valid email address.")]
        public string EmailAddress { get; set; }

        public string ZipCodeUserInfo { get; set; }
        public bool IsInternational { get; set; }
        public string StreetAddress { get; set; }
        public string CityUserInfo { get; set; }
        public string StateUserInfo { get; set; }
        public string Phone { get; set; }
        public string Comments { get; set; }

        public bool DoneAddingInfo { get; set; }

        // Alert properties
        public string Location { get; set; }
        public string State { get; set; }
        public string Area { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public int Radius { get; set; }
        public string PriceFrom { get; set; }
        public string PriceTo { get; set; }
        public int Bedrooms { get; set; }
        public bool TypeSingle { get; set; }
        public bool Condo { get; set; }
        public bool AmenitiyPool { get; set; }
        public bool AmenitiyGolf { get; set; }
        public bool AmenitiyGated { get; set; }
        public string School { get; set; }
        public string Builder { get; set; }
        public SelectList StateList { get; set; }
        public IList<SelectListItem> AreaList { get; set; }
        public IList<SelectListItem> CityList { get; set; }
        public SelectList RadiusList { get; set; }
        public SelectList MinPriceList { get; set; }
        public SelectList MaxPriceList { get; set; }
        public SelectList BedroomsList { get; set; }
        public IList<SelectListItem> SchoolList { get; set; }
        public IList<SelectListItem> BuilderList { get; set; }
    }
}