﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Content;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.CMS;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using StructureMap;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class BaseViewModel
    {

        public BaseViewModelElements Globals { get; set; }

        //TODO: This can be removed in the next sprint once ads are published to all markets
        public int CurrentMarketId
        {
            get
            {
                var pathMapper = ObjectFactory.GetInstance<IPathMapper>();
                var currentMarketId = 0;
                if (this.Globals != null && this.Globals.PartnerLayoutConfig.AmIOnDetailPage)
                {
                    currentMarketId = pathMapper.GetItemFromContext(ContextItems.CurrentMarketId).ToType<Int32>();
                    if (currentMarketId == 0)
                    {
                        if (this as IDetailViewModel != null)
                            currentMarketId = ((IDetailViewModel)this).MarketId;

                        pathMapper.AddItemToContext(ContextItems.CurrentMarketId, currentMarketId);
                    }
                }
                return currentMarketId;
            }
        }

        public AffiliateLinksData AffiliateLinksData { get; set; }

        public string BrandPartnerLogo
        {
            get
            {
                return Resources.GlobalResourcesMvc.Pro.images.pro_logo_blue_png;
            }
        }    

        public string TypeAheadUrl
        {
            get
            {
                return (string.IsNullOrEmpty(NhsRoute.PartnerSiteUrl) ? "" : (NhsRoute.PartnerSiteUrl + "/")) + NhsMvc.PartialViews.ActionNames.PartnerLocationsIndex;
            }
        }
        public bool IsitInactiveData { get; set; }

        public string CurrentMarketName
        {
            get
            {
                var pathMapper = ObjectFactory.GetInstance<IPathMapper>();
                var currentMarketName = string.Empty;
                if (this.Globals != null && this.Globals.PartnerLayoutConfig.AmIOnDetailPage)
                {
                    currentMarketName = pathMapper.GetItemFromContext(ContextItems.CurrentMarketName).ToType<String>();
                    if (string.IsNullOrEmpty(currentMarketName))
                    {
                        var model = this as IDetailViewModel;
                        if (model != null)
                            currentMarketName = model.MarketName;

                        pathMapper.AddItemToContext(ContextItems.CurrentMarketName, currentMarketName);
                    }
                }
                return currentMarketName;
            }
        }
        //END HERE

        public BaseViewModel()
        {
            this.Globals = new BaseViewModelElements();
            this.InitDefaults();
        }
       
    }

    public class BaseViewModelElements
    {
        public int SdcMarketId { get; set; }

        #region Shell
        private Shell _shell;


        public bool IsIE8
        {
            get
            {
                if (HttpContext.Current.Request.Browser.Type.ToUpper().Contains("IE") && HttpContext.Current.Request.Browser.MajorVersion <= 8)
                    return true;
                return false;
            }
        }

        public Shell Shell
        {
            get
            {
                var pathMapper = ObjectFactory.GetInstance<IPathMapper>();

                _shell = pathMapper.GetItemFromContext(ContextItems.Shell) as Shell;
                if (_shell == null)
                {
                    _title = MetaReader.GetBrandPartnerTitle();
                    var shellFactory = new ShellFactory();
                    _shell = shellFactory.GetShell(NhsRoute.PartnerId, Configuration.PartnerShellType,
                                                   MetaReader.StripTag(_title, MetaName.Title));
                    _title = MetaReader.ConstructTag(_shell.Title.InnerMarkup, MetaName.Title);
                    _shell.Head.InnerMarkup = MetaReader.RemoveTitle(_shell.Head.InnerMarkup);

                    pathMapper.AddItemToContext(ContextItems.Shell, _shell);
                }

                return _shell;
            }

            set { _shell = value; }
        }
        #endregion

        #region Meta
        private string _title;
        private string _keywords;
        private string _description;
        private string _robots;
        private string _canonicalLink;
        private string _facebook;

        public string Title
        {
            get { return _title ?? string.Empty; }
            set { _title = value; }
        }
        public string Keywords
        {
            get { return _keywords ?? string.Empty; }
            set { _keywords = value; }
        }
        public string Description
        {
            get { return _description ?? string.Empty; }
            set { _description = value; }
        }
        public string Robots
        {
            get { return _robots ?? string.Empty; }
            set { _robots = value; }
        }
        public string CanonicalLink
        {
            get { return _canonicalLink ?? string.Empty; }
            set { _canonicalLink = value; }
        }

        public string Facebook
        {
            get { return _facebook ?? string.Empty; }
            set { _facebook = value; }
        }

        public string PartnerSiteUrl
        {
            get { return NhsRoute.PartnerSiteUrl.ToLower(); }
        }

        public string H1 { get; set; }
        public string Seo { get; set; }

        public bool ShowEbookSection
        {
            get
            {
                return Configuration.ShowEBook && !NhsRoute.CurrentRoute.Function.Equals(Pages.eBook);
            }
        }

        public string ShortcutIcon
        {
            get
            {
                string link = "<link rel=\"shortcut icon\" href=\"{0}\" />";
                string ico = Configuration.ResourceDomain + "/globalresourcesmvc/Pro/images/favicon.ico";
                return String.Format(link, ico);
            }
        }

        public string AppleIcons
        {
            get
            {
                string link = @"<link rel=""apple-touch-icon-precomposed"" sizes=""144x144"" href=""{0}apple-touch-icon-144x144-precomposed.png"" />
                                <link rel=""apple-touch-icon-precomposed"" sizes=""114x114"" href=""{0}apple-touch-icon-114x114-precomposed.png"" />
                                <link rel=""apple-touch-icon-precomposed"" sizes=""72x72"" href=""{0}apple-touch-icon-72x72-precomposed.png"" />
                                <link rel=""apple-touch-icon-precomposed"" href=""{0}apple-touch-icon-precomposed.png"" />";
                string path = Configuration.ResourceDomain + "/globalresourcesmvc/pro/images/icons/ios/";

                return String.Format(link, path);
            }
        }

        public string CurrentUrl
        {
            get { return HttpContext.Current.Request.Url.ToString(); }
        }
        #endregion

        public MvcHtmlString GoogleUtms
        {
            get { return HtmlGoogleHelper.GetSetTrafficCategorization().ToMvcHtmlString(); }
        }

        private string _google;
        public string Google
        {
            get { return _google ?? string.Empty; }
            set { _google = value; }
        }
        #region Logging
        private string _dcsWebevents;
        private string _dcsEventcode;
        private string _dcsPid;

        public string DcsWebevents
        {
            get { return _dcsWebevents ?? string.Empty; }
            set { _dcsWebevents = value; }
        }

        public string DcsEventcode
        {
            get { return _dcsEventcode ?? string.Empty; }
            set { _dcsEventcode = value; }
        }

        public string DcsPid
        {
            get { return _dcsPid ?? string.Empty; }
            set { _dcsPid = value; }
        }
        public string GetAddCommunityToPlannerUrl(int communityId, int builderId)
        {
            var paramz = new List<RouteParam> {
                new RouteParam(RouteParams.CommunityId, communityId.ToString(),RouteParamType.QueryString),
                new RouteParam(RouteParams.BuilderId, builderId.ToString(), RouteParamType.QueryString)};
            return string.Format(@"""{0}""", paramz.ToUrl(Pages.CommunityDetail+"/"+NhsMvc.CommunityDetail.ActionNames.AddCommunityToUserPlanner));
        }

        public string GetAddCommunityItemToPlannerUrl()
        {
            var paramz = new List<RouteParam>();
            return string.Format(@"""{0}/{1}""", paramz.ToUrl(Pages.CommunityResults), NhsMvc.CommunityResults.ActionNames.SaveCommunityToPlanner);
        }

        public string GetAddHomeToPlannerUrl(int planId, int specId)
        {
           var paramz = specId > 0 ?
                                    new List<RouteParam> { new RouteParam(RouteParams.SpecId, specId.ToString(), RouteParamType.QueryString) } :
                                    new List<RouteParam> { new RouteParam(RouteParams.PlanId, planId.ToString(), RouteParamType.QueryString) };

            return string.Format(@"""{0}""", paramz.ToUrl(Pages.HomeDetail + "/" + NhsMvc.HomeDetail.ActionNames.AddHomeToUserPlanner));
        }

        public string ActiveEngageHeader { get; set; }
        #endregion

        #region Ad
        //TODO: This has to go to a better place and stop using httpcontext
        public AdController AdController
        {
            get
            {
                var pathMapper = ObjectFactory.GetInstance<IPathMapper>();
                var adController = pathMapper.GetItemFromContext(ContextItems.AdController) as AdController;
                if (adController == null)
                {
                    adController = new AdController
                        {
                            AdPageName = (NhsRoute.CurrentRoute.AdPageName == string.Empty)
                                             ? "udf"
                                             : (NhsRoute.CurrentRoute.AdPageName)
                        };

                    pathMapper.AddItemToContext(ContextItems.AdController, adController);
                }
                return adController;
            }
        }

        #endregion

        #region Partner Config

        //Added to get access from master pages to check settings at partner level
        public Nhs.Mvc.Domain.Model.Web.Partner PartnerInfo
        {
            get;
            set;
        }




        //TODO: This has to go to a better place and stop using httpcontext
        public PartnerLayoutConfig PartnerLayoutConfig
        {
            get
            {
                var pathMapper = ObjectFactory.GetInstance<IPathMapper>();
                var partnerLayoutConfig = pathMapper.GetItemFromContext(ContextItems.PartnerLayoutConfig) as PartnerLayoutConfig;
                if (partnerLayoutConfig == null)
                {
                    partnerLayoutConfig = PartnerLayoutHelper.GetPartnerLayoutConfig();
                    pathMapper.AddItemToContext(ContextItems.PartnerLayoutConfig, partnerLayoutConfig);
                }
                return partnerLayoutConfig;
            }
        }

        #endregion

        #region Instrumentation

        private string _serverName;
        public string ServerName
        {
            get { return _serverName ?? string.Empty; }
            set { _serverName = value; }
        }

        private string _ipAddress;
        public string IpAddress
        {
            get { return _ipAddress ?? string.Empty; }
            set { _ipAddress = value; }
        }

        private string _routeInfo;
        public string RouteInfo
        {
            get { return _routeInfo ?? string.Empty; }
            set { _routeInfo = value; }
        }

        #endregion

        #region Resource Center

        public List<RCCategory> ResourceCenterCategories { get; set; }

        #endregion

        #region Co-op

        public bool HasCoop { get; set; }
        public bool HasBuilderPactLink { get; set; }

        #endregion        
        
    }
}
