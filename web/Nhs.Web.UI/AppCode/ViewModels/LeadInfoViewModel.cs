﻿using System.Collections.Generic;
using Nhs.Library.Business.Interface;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class LeadInfoViewModel : BaseViewModel
    {
        public LeadInfoViewModel()
        {
            RecoComms = new List<RecoCommItem>();
        }

        public List<RecoCommItem> RecoComms { get; set; }
        public int CommunityId { get; set; }
        public int BuilderId { get; set; }
        public int PlanId { get; set; }
        public int SpecId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public bool MatchEmail { get; set; }
        public int MarketId { get; set; }
        public string RequestUniqueKey { get; set; }
        
    }
}