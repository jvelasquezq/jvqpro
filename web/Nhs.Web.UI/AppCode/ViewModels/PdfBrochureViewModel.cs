﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bdx.Web.Api.Objects;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Common;
using Nhs.Utility.Html;
using Nhs.Web.UI.AppCode.Helper;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class PdfBrochureViewModel : BaseViewModel
    {
        public PdfBrochureViewModel()
        {
            HomesAvailable = new List<HomesAvailable>();
            CityInformation = new MvcHtmlString(string.Empty);
            PreviewMode = true;
        }

        /// <summary>
        /// Brand Logo reflect the parent Brand for the website (i.e. NHS/MNH or NHL). 
        /// </summary>
        public string Logo { get; set; }

        /// <summary>
        /// Template Image for Pro Brochure. 
        /// </summary>
        public string GetTemplateImage
        {
            get
            {
                return !string.IsNullOrEmpty(AgentPhoto)
                           ? string.Format("{0}/{1}", Configuration.ResourceDomain, AgentPhoto)
                           : !string.IsNullOrEmpty(AgencyLogo)
                                 ? string.Format("{0}/{1}", Configuration.ResourceDomain, AgencyLogo)
                                 : string.Empty;
            }
        }

        public bool PreviewMode { get; set; }

        #region AgentInfo

        public string AgentName { get; set; }
        public string AgencyName { get; set; }
        public string AgentCity { get; set; }
        public string AgentState { get; set; }
        public string AgentPhoneNumber { get; set; }
        public string AgentEmailAddress { get; set; }
        public string AgentPhoto { get; set; }
        public string AgencyLogo { get; set; }
        public string AgentLicenseNumber { get; set; }
        public string StateAbbr { get; set; }


        #endregion

        #region Builder

        public string UrlMap { get; set; }
        public string BuilderUrl { get; set; }
        public string BuilderName { get; set; }
        public string BuilderLogo { get; set; }
        public string BuilderLogoSmall { get; set; }
        public string PhoneNumber { get; set; }
        public string HoursOfOperation { get; set; }
        public string SalesEmailAddress { get; set; }
        public string SalesAgentName { get; set; }
        public int BuilderId { get; set; }
        public string BuilderMapUrl { get; set; }
        public List<RouteParam> GetParamBuilderLink
        {
            get
            {
                var @params = new List<RouteParam>();
                @params.Add(new RouteParam(RouteParams.Community, CommunityId));
                @params.Add(new RouteParam(RouteParams.Builder, BuilderId));
                @params.Add(new RouteParam(RouteParams.PlanId, PlanId));
                @params.Add(new RouteParam(RouteParams.SpecId, SpecId));
                @params.Add(new RouteParam(RouteParams.Market, MarketId));
                @params.Add(new RouteParam(RouteParams.ExternalUrl, HttpUtility.UrlEncode(BuilderUrl), RouteParamType.QueryString));
                @params.Add(new RouteParam(RouteParams.LogEvent, LogImpressionConst.BuilderLogo));
                @params.Add(new RouteParam(RouteParams.HUrl, CryptoHelper.HashUsingAlgo(HttpUtility.UrlDecode(BuilderUrl) + ":" + LogImpressionConst.BuilderLogo, "md5")));
                return @params;
            }
        }
        #endregion

        #region Community
        public string CommunityName { get; set; }
        public string CommunityState { get; set; }
        public string CommunityCity { get; set; }
        public string CommunityDescription { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string ZipCode { get; set; }
        public int CommunityId { get; set; }
        public int MarketId { get; set; }
        public bool IsPageCommDetail { get; set; }
        #endregion

        #region Home
        public string HomeTitle { get; set; }
        public int PlanId { get; set; }
        public int SpecId { get; set; }
        public bool IsPlan { get; set; }
        public int HomeStatus { get; set; }
        public IList<IImage> FloorPlanImages { get; set; }
        #endregion

        #region PageComponents
        public string DisplaySqFeet { get; set; }
        public string PriceDisplay { get; set; }
        public int AllNewHomesCount { get; set; }
        public string BedroomRange { get; set; }
        public string BathroomRange { get; set; }
        public string GarageRange { get; set; }
        public int Stories { get; set; }
        public string ImagePath { get; set; }
        public string Description { get; set; }
        public IList<School> Schools { get; set; }
        #endregion

        #region Map&Directions
        public string DrivingDirections { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double RouteLat { get; set; }
        public double RouteLng { get; set; }
        #endregion

        #region Gallery
        public IEnumerable<MediaPlayerObject> ImagesHomeGallery { get; set; }
        public IEnumerable<MediaPlayerObject> ImagesCommunityGallery { get; set; }
        public IDictionary<AmenityGroup, IList<IAmenity>> Amenities { get; set; }
        public ICollection<Mvc.Domain.Model.Web.Utility> Utilities { get; set; }
        #endregion

        #region Homes Available
        public bool HaveQuickMoveIn { get; set; }
        public bool HaveUnderConstruction { get; set; }
        public bool HaveReadyToBuild { get; set; }
        public List<HomesAvailable> HomesAvailable { get; set; }
        #endregion

        #region PromotionsAndNotes
        public IEnumerable<GreenProgram> GreenPrograms { get; set; }
        public IEnumerable<Promotion> Promotions { get; set; }
        #endregion

        public MvcHtmlString CityInformation { get; set; }
        #region Metods
        public MvcHtmlString GetBathroom
        {
            get { return MvcHtmlStringFormat(LanguageHelper.Bathrooms+": ", BathroomRange); }
        }

        public MvcHtmlString GetGarage
        {
            get { return MvcHtmlStringFormat(LanguageHelper.Garage+": ", GarageRange); }
        }

        public MvcHtmlString GetBedrooms
        {
            get { return MvcHtmlStringFormat(LanguageHelper.Bedrooms + ": ", BedroomRange); }
        }

        public MvcHtmlString GetPriceDisplay
        {
            get
            {
                return HomeStatus == HomeStatusType.ModelHome.ToType<int>() 
                    ? new MvcHtmlString(string.Empty)
                    : MvcHtmlStringFormat(LanguageHelper.Price + ": ", PriceDisplay);
            }
        }

        public MvcHtmlString GetDisplaySqFeet
        {
            get { return MvcHtmlStringFormat(LanguageHelper.SquareFeet+": ", DisplaySqFeet); }
        }

        public MvcHtmlString GetDescription
        {
            get
            {
                return MvcHtmlStringFormat(string.Empty, string.IsNullOrEmpty(Description)
                                               ? string.Empty
                                               : "<p>" + FormattedText(Description) + "</p>");
            }
        }

        private string FormattedText(string text)
        {
            if (string.IsNullOrEmpty(text)) return string.Empty;

            string descriptionText = ParseHTML.StripHTML(text);

            return descriptionText.Replace("\r", string.Empty).Replace("\n", string.Empty);
        }

        public MvcHtmlString GetCommunityDescription
        {
            get { return MvcHtmlStringFormat("", string.IsNullOrEmpty(CommunityDescription) ? "" : "<p>" + CommunityDescription.Replace(Environment.NewLine, "</p><p>") + "</p>"); }
        }

        public MvcHtmlString GetHoursOfOperation
        {
            get { return MvcHtmlStringFormat(LanguageHelper.Hours + ": ", HoursOfOperation); }
        }

        public MvcHtmlString GetSalesEmailAddress
        {
            get { return MvcHtmlString.Create(SalesEmailAddress); }
        }

        public MvcHtmlString GetSalesAgentName
        {
            get { return MvcHtmlString.Create(SalesAgentName); }
        }

        public MvcHtmlString GetPhoneNumber
        {
            get { return MvcHtmlStringFormat(LanguageHelper.Phone + ": ", PhoneNumber); }
        }

        public string GetCityState
        {
            get
            {
                var res = string.Empty;
                if (!string.IsNullOrEmpty(AgentCity) && !string.IsNullOrEmpty(AgentState))
                    res = string.Format("{0}, {1}", AgentCity, AgentState);
                return res;
            }
        }

        public MvcHtmlString GetSchools
        {
            get
            {
                var schools = Schools.GroupBy(item => item.SchoolDistrict.DistrictName);
                var data = new List<string>();
                foreach (var school in schools)
                {
                    var key = school.Key;
                    var sch = string.Join(", ", school.Where(p => !string.IsNullOrEmpty(p.SchoolName)).Select(p => p.SchoolName));
                    if (!string.IsNullOrEmpty(sch))
                        sch = "(" + sch + ")";
                    data.Add(key + " " + sch);
                }
                return MvcHtmlStringFormat(LanguageHelper.Schools + ": ", string.Join(", ", data));
            }
        }

        private MvcHtmlString MvcHtmlStringFormat(string text, string value)
        {
            return
                MvcHtmlString.Create(string.IsNullOrEmpty(value)
                                         ? string.Empty
                                         : string.Format("{0}{1}", text, value));
        }

        public bool HaveAmenities
        {
            get
            {
                return Amenities[AmenityGroup.CommunityServicesCommAmenities].Count > 0 ||
                       Amenities[AmenityGroup.CommunityServicesOpenAmenities].Count > 0 ||
                       Amenities[AmenityGroup.SocialActivitiesCommAmenities].Count > 0 ||
                       Amenities[AmenityGroup.SocialActivitiesOpenAmenities].Count > 0 ||
                       Amenities[AmenityGroup.LocalAreaCommAmenities].Count > 0 ||
                       Amenities[AmenityGroup.LocalAreaOpenAmenities].Count > 0 ||
                       Amenities[AmenityGroup.HealthAndFitnessCommAmenities].Count > 0 ||
                       Amenities[AmenityGroup.HealthAndFitnessOpenAmenities].Count > 0 ||
                       Amenities[AmenityGroup.EducationalOpenAmenities].Count > 0;
            }
        }

        #endregion
    }

    public class HomesAvailable
    {
        public List<List<ApiHomeResultV2>> HomeResultPages { get; set; }
        public string Tittle { get; set; }
        public bool IsReadyToBuild { get; set; }
        public bool IsUnderConstruction { get; set; }
        public string MarketName { get; set; }

        public MvcHtmlString GetTittle
        {
            get { return MvcHtmlString.Create(string.IsNullOrEmpty(Tittle) ? string.Empty : Tittle); }
        }
    }
}
