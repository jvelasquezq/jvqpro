﻿using System.Collections.Generic;
using System.Linq;
using Nhs.Library.Business.Tv;
using Nhs.Mvc.Biz.Services.Helpers.Tv;

namespace Nhs.Web.UI.AppCode.ViewModels.Tv
{
    public class NhsTvMarketViewModel : NhsTvViewModel
    {
        public TvMarket Market { get; set; }
        public TvMarketContent MarketContent { get; set; }
        public string CommunitiesForTvBuilderPoints { get; set; }
        public string KeyWord { get; set; }
        public List<List<TvMarketPromotion>> Promos { get; set; }
    }

    public class NhsTvViewModel : BaseViewModel
    {
        public string TvMarketPoints { get; set; }
    }
}