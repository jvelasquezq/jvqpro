﻿using System.Collections.Generic;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResultMobile;

namespace Nhs.Web.UI.AppCode.ViewModels.Tv
{
    public class TvMarketMobileViewModel : NhsTvMarketViewModel
    {
        public IEnumerable<CommunityItemMobileViewModel> CommunitiesForMobile { get; set; }
    }
}