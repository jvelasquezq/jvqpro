﻿using System.Collections.Generic;
using Nhs.Library.Web;

namespace Nhs.Web.UI.AppCode.ViewModels.Tv
{
    public class NhsTvHomeViewModel : BaseViewModel
    {
        public IEnumerable<string> NearbyAreas { get; set; }
        public List<ContentTag> SeoContentTags { get; set; }
        public string SearchText{get; set; }
        public string TypeAheadUrl { get; set; }

        public NhsTvHomeViewModel()
        {

        }

    }
}
