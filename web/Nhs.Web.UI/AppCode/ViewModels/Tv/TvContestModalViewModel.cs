﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nhs.Web.UI.AppCode.ViewModels.Tv
{
    public class TvContestModalViewModel: BaseViewModel
    {
        public string MarketName { get; set; }
        public int MarketId { get; set; }
        
        public string FullName { get; set; }

        [Required(ErrorMessage = @"Required Email")]  
        [RegularExpression(@"^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6}$", ErrorMessage = @"Invalid Email")]
        public string Email { get; set; }

        public bool SignMeUpSpetialOffers { get; set; }

        public string PdfLinkUrl { get; set; }

        public string PartnerName { get; set; }

        public bool ShowSignUp { get; set; }
    }
}