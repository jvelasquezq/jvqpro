﻿
using System.Collections.Generic;
using Nhs.Library.Business;
using Nhs.Library.Business.Map;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Helpers.Content;
using Nhs.Library.Web;
using Nhs.Library.Enums;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Domain.Model.Web;
using System.Web.Mvc;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class HomeDetailViewModel : BaseViewModel, IDetailViewModel, IPropertyMapViewModel
    {

        public bool SavedToPlanner { get; set; }
        public IList<MediaPlayerObject> PlayerMediaObjects { get; set; }
        public IEnumerable<PlannerListing> SavedPropertysProCrm { get; set; }
        public string RequestUniqueKey { get; set; }
        public string CorporationName { get; set; }
        public bool HasShowCaseInformation { get; set; }
        public BoylResult CnhBoyl { get; set; }
        public bool NewSearchRCC { get; set; }
        public bool ShowUserPhoneNumber { get; set; }
        public bool IsInactive { get; set; }
        public bool RequestAnAppointment { get; set; }
        public bool IsBilled { get; set; }
        public bool BcTypeBanner { get; set; }
        public string NonPdfBrochureUrl { get; set; }
        public string MarketName { get; set; }
        public string DetailType { get { return LanguageHelper.Home; } }
        public IPlan Plan { get; set; }
        public string CommunityUrl { get; set; }
        public bool HideDrivingDirections { get; set; }
        public string HomeAddress { get; set; }
        public string HomeTitle { get; set; }
        public string InfoAreaTitle { get; set; }
        public bool ShowSocialIcons { get; set; }
        public ICollection<Video> Videos { get; set; }
        //public IList<HubImage> PreviewAwardImages { get; set; }
        public bool UserAlreadyExists { get; set; }
        public string CreditScoreText { get; set; }
        public bool ShowCreditScoreSection { get; set; }
        public bool IsCommunityMultiFamily { get; set; }
        public string ImageThumbnail { get; set; }
        public string VirtualTourUrl { get; set; }
        #region IDetailViewModel Interface
        public bool ShowPlaceHolderInputText { get; set; }
        public bool PreviewMode { get; set; }
        public bool IsBasicCommunity { get; set; }
        public bool IsCommunityResults { get; set; }
        public int PlanId { get; set; }
        public string BasicHomeSpecs { get; set; }
        public bool IsPlan { get; set; }
        public string BuilderName { get; set; }
        public string CommunityName { get; set; }
        public string PopUpViewerName { get; set; }
        public bool ShowBrochure { get; set; }
        public int MarketId { get; set; }
        public int BuilderId { get; set; }
        public int CommunityId { get; set; }
        public int HomeStatus { get; set; }
        public string Name { get; set; }
        public string MailAddress { get; set; }
        public string UserPostalCode { get; set; }
        public string UserPhoneNumber { get; set; }

        public string Comments { get; set; }
        public string Message { get; set; }
        public bool LiveOutside { get; set; }
        public bool GeneralInquiry { get; set; }
        public bool ScheduleAppointment { get; set; }
        public bool SendAlerts { get; set; }

        //public bool SpecialOffers { get; set; }
        //public bool QuickMoveIn { get; set; }
        public string BrandThumbnail { get; set; }
        public string HomesAvailable { get; set; }
        public string DisplaySqFeet { get; set; }

        public List<RouteParam> BrochureParams
        {
            get
            {
                var paramz = new List<RouteParam>
                    {
                        new RouteParam(RouteParams.Email, MailAddress),
                        new RouteParam(RouteParams.Refer, UserSession.Refer),
                        IsPlan
                            ? new RouteParam(RouteParams.PlanId, PlanId.ToString())
                            : new RouteParam(RouteParams.SpecId, SpecId.ToString()),
                        new RouteParam(RouteParams.MarketId, MarketId.ToString()),
                        new RouteParam(RouteParams.LeadType, LeadType.Home),
                        new RouteParam(RouteParams.Community, CommunityId.ToString()),
                        new RouteParam(RouteParams.Builder, BuilderId.ToString()),
                        new RouteParam(RouteParams.RequestUniqueKey, RequestUniqueKey)
                    };
                return paramz;
            }
        }

        public List<RouteParam> ExpiredBrochureParams
        {
            get
            {
                var paramz = new List<RouteParam>
                {
                    new RouteParam(RouteParams.Email, MailAddress, RouteParamType.QueryString),
                    new RouteParam(RouteParams.Refer, UserSession.Refer, RouteParamType.QueryString)
                };

                if (CommunityId > 0 && BuilderId > 0)
                {
                    paramz.Add(new RouteParam(RouteParams.Community, CommunityId.ToString(), RouteParamType.QueryString));
                    paramz.Add(new RouteParam(RouteParams.Builder, BuilderId.ToString(), RouteParamType.QueryString));
                }

                paramz.Add(IsPlan
                               ? new RouteParam(RouteParams.PlanId, PlanId.ToString(), RouteParamType.QueryString)
                               : new RouteParam(RouteParams.SpecId, SpecId.ToString(), RouteParamType.QueryString));

                paramz.Add(IsPlan
                               ? new RouteParam(RouteParams.PlanList, PlanId.ToString(), RouteParamType.QueryString)
                               : new RouteParam(RouteParams.SpecList, SpecId.ToString(), RouteParamType.QueryString));

                paramz.Add(new RouteParam(RouteParams.LeadType, LeadType.Home, RouteParamType.QueryString));
                paramz.Add(new RouteParam(RouteParams.FromPage, Pages.SavedHomes, RouteParamType.QueryString));
                paramz.Add(new RouteParam(RouteParams.LeadAction, LeadAction.FreeBrochure, RouteParamType.QueryString));

                return paramz;
            }
        }

        public string MortgagePage
        {
            get
            {
                var paramz = new List<RouteParam>();
                paramz.Add(IsPlan ? new RouteParam(RouteParams.PlanId, PlanId) : new RouteParam(RouteParams.SpecId, SpecId));
                return paramz.ToUrl(Pages.MortgageTable).ToLower();
            }
        }

        public string CreditScoreLink1 { get; set; }
        public string CreditScoreLink2 { get; set; }

        public string PhoneNumber { get; set; }
        public string TrackingPhoneNumber { get; set; }
        public string HoursOfOperation { get; set; }
        public bool ShowMortgageLink { get; set; }
        public string LogActionMethodUrl { get; set; }
        public IList<Testimonial> Testimonials { get; set; }
        // Details media viewer
        public IList<MediaPlayerObject> PropertyMediaLinks { get; set; }
        public IList<MediaPlayerObject> ExternalMediaLinks { get; set; }
        
        public ICollection<Event> Events { get; set; }

        public ICollection<Promotion> Promotions { get; set; }
     
        public ICollection<HomeItem> HotHomesApi { get; set; }
        public IList<HomeItem> CrossMarketingHomes { get; set; }
        public List<HomeItem> SimilarHomesApi { get; set; }

        public ICollection<GreenProgram> GreenPrograms { get; set; }
        public bool ShowHotHomeSection { get; set; }
        public IEnumerable<SelectListItem> States { get; set; }
        public string SelectedStateAbbr { get; set; }
        public string SearchText { get; set; }
        public string PriceHigh { get; set; }
        public string PriceLow { get; set; }
        public IEnumerable<SelectListItem> PriceLowRange { get; set; }
        public IEnumerable<SelectListItem> PriceHighRange { get; set; }
        public string EnvisionUrl { get; set; }
        public bool PartnerUsesMatchmaker { get; set; }
        public string ConversionIFrameSource { get; set; }
        public string BcType { get; set; }
        public int LeadSectionState { get; set; }
        public string PlanName { get; set; }

        #region IDetailViewModel

        public bool IsLoggedInPaid
        {
            get
            {
                return (LeadSectionState == LeadSectionStateConst.LoggedInPaid.ToType<int>());
            }

        }

        public bool IsLoggedInUnpaid
        {
            get
            {
                return (LeadSectionState == LeadSectionStateConst.LoggedInUnpaid.ToType<int>());
            }

        }

        public bool IsLoggedOutPaid
        {
            get
            {
                return (LeadSectionState == LeadSectionStateConst.LoggedOutPaid.ToType<int>());
            }

        }

        public bool IsLoggedOutUnpaid
        {
            get
            {
                return (LeadSectionState == LeadSectionStateConst.LoggedOutUnpaid.ToType<int>());
            }

        }

        #endregion

        #endregion

        #region IMediaGalleryViewModel

        public KeyValuePair<int, int> Size { get; set; }
        public string FirstImage { get; set; }
        public string PinterestDescription { get; set; }
        public string PageUrl { get; set; }

        #endregion

        #region ISecondaryInfoViewModel
        public bool IsPageCommDetail { get; set; }
        public string CommunityCity { get; set; }

        public string ZipCode { get; set; }
        public string State { get; set; }
        public string StateAbbr { get; set; }

        public string CommunityDescription { get; set; }
        public IList<School> Schools { get; set; }
        public IDictionary<AmenityGroup, IList<IAmenity>> Amenities { get; set; }
        public ICollection<Mvc.Domain.Model.Web.Utility> Utilities { get; set; }

        public ICollection<GreenProgram> GreenPromos { get; set; }

        public bool HasCoop { get; set; }
        public bool HasBuilderPactLink { get; set; }
        public string BuilderPackLink { get; set; }

        #endregion
        
        public string DrivingDirections { get; set; }
        public string DrivingDirectionsShort
        {
            get
            {
                if (!string.IsNullOrEmpty(DrivingDirections) && DrivingDirections.Length > 39)
                    return DrivingDirections.Substring(0, 39) + "...";

                return DrivingDirections;
            }
        }
        public int PropertyId { get; set; }
        public string PropertyName { get; set; }
        public bool AllowModals
        {
            get
            {
                if (Globals != null && Globals.PartnerLayoutConfig != null)
                    return Globals.PartnerLayoutConfig.PartnerAllowsModals;

                return false;
            }
        }

        public bool IsCommunityPage { get; set; }
        public DrivingDirections DrivingDirectionsList { get; set; }
        public List<NearbyCommunity> NearByComms { get; set; }
        public string NearByCommsObject { get; set; }
        public IList<NearbyCommunity> NearByCommsSmall { get; set; }
        public string BuilderLogo { get; set; }
        public string BuilderLogoSmall { get; set; }

        public string AgentCompensation { get; set; }
        public string AgentPolicyLink { get; set; }
        public string AgentCompensationAdditionalComments { get; set; }
        public string AgentCommissionBasis { get; set; }
        public string AgentCommissionPayoutTiming { get; set; }
        public string BuilderUrl { get; set; }
        public string BrandName { get; set; }
        public int BrandId { get; set; }
        public string PriceDisplay { get; set; }
        public string SqFtDisplay { get; set; }
        public string StoryDisplay { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double RouteLat { get; set; }
        public double RouteLng { get; set; }
        public string BuilderMapUrl { get; set; }
        public string UnderneathMapCommName { get; set; }
        public string UnderneathMapAddress { get; set; }

        public string BathroomsAbbr { get; set; }
        public string BedroomsAbbr { get; set; }
        public string GaragesAbbr { get; set; }
        public bool ExcludesLand { get; set; }
        public string HomeSpecsText { get; set; }
        public string HomeMarketingDescription { get; set; }
        public string HomeDescription { get; set; }
        public string DetailDescription { get { return HomeDescription; } }
        public bool ShowLeadForm { get; set; }
        public bool ShowSaveToAccount { get; set; }
        public bool ShowRegInfoDetail { get; set; }
        public bool ShowExpiredRequestBrochureLink { get; set; }

        public string BrochureUrl { get; set; }
        public IList<HomeOption> HomeOptions { get; set; }
        public bool HasOptions
        {
            get { return HomeOptions != null && HomeOptions.Count > 0; }
        }
        public bool IsPhoneNumberVisible { get; set; }

        public bool HomeExistsOnUserProfile { get; set; }
        public string AddHomeToPlannerUrl { get; set; }
        public string SaveHomeSuccessMessage { get; set; }
        public int PageCount { get; set; }

        public double PlanPrice
        {
            get { return this.Plan == null ? 0 : (double)this.Plan.Price; }
        }

        public int SpecId { get; set; }
        public bool EnableRequestAppointment { get; set; }
        public string SalesOfficeAddress1 { get; set; }
        public string SalesOfficeAddress2 { get; set; }
        public string HomeAddress1 { get; set; }
        public string HomeAddress2 { get; set; }
        public IList<IImage> AwardImages { get; set; }

        public string SaleAgents { get; set; }

        public bool IsAdult { get; set; }
        public bool IsCondo { get; set; }
        public bool IsGreenProgram { get; set; }
        public bool IsGreen { get; set; }
        public bool IsGated { get; set; }
        public bool IsAgeRestricted { get; set; }
        public bool IsMasterPlanned { get; set; }
        public bool NewUser { get; set; }
    }

    public class HomeResultCarouselItem
    {
        public HomeItem Item { get; set; }
        public int MarketId { get; set; }
        public bool PreviewMode { get; set; }
        public string BcType { get; set; }

        public HomeResultCarouselItem(HomeItem item, int marketId, bool previewMode, string bcType)
        {
            Item = item;
            MarketId = marketId;
            PreviewMode = previewMode;
            BcType = bcType;
        }
    }

    public class HomeGaleryFloorPlanViewModel : BaseViewModel
    {
        public IList<IImage> FloorPlanImages { get; set; }
        public int SelectImage { get; set; }
        public int Height { get; set; }
    }
}
