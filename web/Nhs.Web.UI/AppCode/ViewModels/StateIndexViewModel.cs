﻿using System.Collections.Generic;
using Nhs.Library.Business;
using Nhs.Library.Business.Map;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Market = Nhs.Mvc.Domain.Model.Web.Market;
using Nhs.Mvc.Domain.Model.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Bdx.Web.Api.Objects;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class StateIndexViewModel : BaseStateIndexViewModel
    {
        public StateIndexViewModel()
        {
            NewHomeBuilders = new List<List<Brand>>();
        }

        #region public Attributes
        public int PartnerId { get; set; }
        public string MarketId { get; set; }
        public string StateName { get; set; }
        public string StateAbbreviation { get; set; }
        public string City { get; set; }
        public string PriceFrom { get; set; }
        public string PriceTo { get; set; }
        public string SEOContent_NHS_Header { get; set; }
        public string SEOContent_NHS_Tab1 { get; set; }
        public string SEOContent_NHS_Tab2 { get; set; }
        public string SEOContent_NHS_Tab3 { get; set; }
        public string SEOContent_NHS_Articles { get; set; }
        public string SEOContent_NHS_Footer { get; set; }
        public string SEOContent_NHS_H1 { get; set; }
        public IList<ContentTag> SeoContentTags { get; set; }
        public List<List<Market>> MarketList { get; set; }
        public IEnumerable<int> PrinceRange { get; set; }

        public IList<City> NewHomeCities { get; set; }
        public List<List<Brand>> NewHomeBuilders { get; set; }

        //public SelectList Areas;

        [Required(ErrorMessage = @"Area is required")]
        public int SelectedMarketId;
        public SelectList LowPriceRange;
        public SelectList HighPriceRange;

        public List<ExtendedCommunityResult> SpotLightCommunities { get; set; }
        public MapData Map { get; set; }
        public string SiteRoot { get; set; }
        public string ResourceRoot { get; set; }
        public int BrandPartnerId { get; set; }
        public List<Market> Markets { get; set; }

        #endregion
    }

    public class BaseStateIndexViewModel:BaseViewModel
    {
        public BaseStateIndexViewModel()
        {
            ReplacementTags=  new List<ContentTag> { new ContentTag { TagKey = ContentTagKey.StateName, TagValue = StateName } };
        }

        public string StateName { get; set; }
        public string PriceLow;
        public string PriceHigh;
        public decimal CenterLng { get; set; }
        public decimal CenterLat { get; set; }
        public string ZoomLevel { get; set; }
        public int NumOfBeds { get; set; }
        public int NumOfBaths { get; set; }
        public string SearchText { get; set; }
        public string MarketPoints { get; set; }
        public List<ContentTag> ReplacementTags { get; set; }
        public string SeoHeaderContent { get; set; }
        public string SeoFootherContent { get; set; }
        public ApiResultCounts ResultCounts { get; set; }
        
    }
}
