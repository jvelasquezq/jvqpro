﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library;
using State = Nhs.Mvc.Domain.Model.Web.State;
using Nhs.Search.Objects;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class HotGreenSearchViewModel: BaseViewModel
    {
        public string FlashSpot { get; set; }
        public string StateAbbr { get; set; }
        public string PostalCode { get; set; }        

        public string SearchType { get; set; }

        public int PriceLow { get; set; }
        public int PriceHigh { get; set; }

        public IEnumerable<State> StateList { get; set; }
        
        public SelectList LowPriceRange;
        public SelectList HighPriceRange;

        public SpotlightCommunitiesViewModel SpotModel { get; set; }
    }
}