using System;

namespace Nhs.Web.UI.AppCode.ViewModels.LeadView
{
    public class LeadViewModalModel : BaseViewModel
    {
        public bool IsFreeBrochure { get; set; }
        public bool IsAppointment { get; set; }
        public bool IsSpecialOffer { get; set; }
        public bool IsAddMeToInterestList { get; set; }
        public string PropertyImage { get; set; }
        public string BuilderName { get; set; }
        public string PropertyName { get; set; }

        public bool PartnerUsesMatchmaker { get; set; }
        
        #region Input Fields
        public string Name { get; set; }
        public string Email { get; set; }
        public string Zip { get; set; }
        public bool International { get; set; }
        public string Phone { get; set; }
        public string Questions { get; set; }
        public string Comments { get; set; }
        public bool IncludeRecoComm { get; set; }
        public bool ShowCalendar { get; set; }
        public DateTime AppointmentDateTime { get; set; }
        #endregion

        public int PlanId { get; set; }
        public int SpecId { get; set; }
        public int CommunityId { get; set; }
        public string LeadType { get; set; }
        public string LeadFormType { get; set; }
        public string LeadAction { get; set; }
        public string FromPage { get; set; }
        public string BuilderList { get; set; }
        public string CommunityList { get; set; }
        public string PlanList { get; set; }
        public string SpecList { get; set; }
        public int MarketId { get; set; }
        public int BuilderId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}