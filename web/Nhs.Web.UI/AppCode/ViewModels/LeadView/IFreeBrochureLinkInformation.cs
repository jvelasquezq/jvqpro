﻿namespace Nhs.Web.UI.AppCode.ViewModels.LeadView
{
    public interface IFreeBrochureLinkInformation
    {
        bool IsPageCommDetail { get; set; }
        int MarketId { get; set; }
        int CommunityId { get; set; }
        int BuilderId { get; set; }
        int PlanId { get; set; }
        int SpecId { get; set; }
        bool IsBilled { get; set; }
        bool IsPlan { get; set; }
    }
}