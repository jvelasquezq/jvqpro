using System.Collections.Generic;
using Bdx.Web.Api.Objects;

namespace Nhs.Web.UI.AppCode.ViewModels.LeadView
{
    public class LeadRecoModalViewModel : LeadInfoViewModel
    {
        public string Google { get; set; }
        public List<ApiRecoCommunityResult> RecoCommunities { get; set; }
        public bool International { get; set; }
        public string BuilderName { get; set; }
        public string ConversionIFrameSource { get; set; }
    }
}