﻿namespace Nhs.Web.UI.AppCode.ViewModels.LeadView
{
    public class FreeBrochureLinkInformation : IFreeBrochureLinkInformation
    {
        public bool IsPageCommDetail { get; set; }
        public int MarketId { get; set; }
        public int CommunityId { get; set; }
        public int BuilderId { get; set; }
        public int PlanId { get; set; }
        public int SpecId { get; set; }
        public bool IsBilled { get; set; }
        public bool IsPlan { get; set; }
        public string PhoneNumber { get; set; }
    }
}