using System;

namespace Nhs.Web.UI.AppCode.ViewModels.LeadView
{
    public class ThankYouModalViewModel : BaseViewModel
    {
        public string Name { get; set; }
        public string Google { get; set; }
        public string ConversionIFrameSource { get; set; }
        public string Email { get; set; }
        public bool IsExistingUser { get; set; }
        public bool IsSignIn { get; set; }
        public bool IsFreeBrochure { get; set; }
        public bool IsAddMeToInterestList { get; set; }
        public bool IsAppointment { get; set; }
        public bool IsSpecialOffer { get; set; }
    }
}
