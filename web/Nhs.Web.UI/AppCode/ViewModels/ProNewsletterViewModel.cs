﻿using System.Collections.Generic;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Web.UI.AppCode.Extensions;
using Nhs.Web.UI.AppCode.Helper;
using Resources;
using TweetSharp;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class ProNewsletterViewModel : BaseViewModel
    {
        public int MarketId { get; set; }
        public bool IsRegisteredUser { get; set; }
        public Market Market { get; set; }
        public bool ShowEventsSection { get; set; }
        

        public List<ExtendedCommunityResult> Communities { get; set; }
        public List<ExtendedCommunityResult> FeaturedCommunities { get; set; }
        public List<ExtendedHomeResult> QuickMoveinHomes { get; set; }

        public List<CommunityEventInfo> Events { get; set; }
        public List<CommunityPromotionInfo> Promotions { get; set; }

        public List<TwitterStatus> Tweets { get; set; }

        public string EmailTemplateLinkColor { get; set; }
        public string EmailTemplateButtonBgColor { get; set; }
        public string EmailTemplateButtonTextColor { get; set; }
        public string EmailTemplateTopHeaderColor { get; set; }
        public string EmailTemplateTopHeader2Color { get; set; }
        public string EmailTemplateSubHeaderColor { get; set; }
        public string EmailTemplateSubHeaderTextColor { get; set; }
        public string FacebookUrl { get; set; }
        public string MarketMapPdfFilePath { get; set; }

        public string PartnerName
        {
            get
            {
                return Globals.PartnerInfo.PartnerName;
            }
        }

        /// <summary>
        /// Nhs Pro Logo
        /// </summary>
        public string PartnerEmailLogo
        {
            get
            {

                return GlobalResourcesMvc.Pro.images.email_logo_png;
            }
        }


        /// <summary>
        /// Brand Email Logo 
        /// </summary>
        public string BrandEmailLogo
        {
            get { return BrandLogo.Replace("_logo", "_emaillogo"); }
        }

        /// <summary>
        /// Brand email Logo reflect the parent Brand for the website (i.e. NHS/MNH or NHL). 
        /// </summary>
        public string BrandLogo
        {
            get
            {
                return Globals.PartnerInfo.PartnerLogo.Replace("[resource:]", Configuration.ResourceDomain);
            }
        }
        
        public ProNewsletterViewModel(int marketId, bool isRegisteredUser)
        {
            MarketId = marketId;
            IsRegisteredUser = isRegisteredUser;
        }
    }
}
