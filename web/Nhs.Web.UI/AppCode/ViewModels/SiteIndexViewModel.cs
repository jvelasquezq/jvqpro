﻿using System.Collections.Generic;
using Nhs.Library.Business;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Search.Objects;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class SiteIndexViewModel : BaseViewModel
    {
        public string PartnerName { get; set; }
        public string StateName { get; set; }

        public string SeoContentMarkets { get; set; }
        public string SeoContentCustomLinksHeader { get; set; }
        public string SeoContentCustomLinksList { get; set; }
        public string SeoContentFooterTextHeader { get; set; }
        public string SeoContentFooterText { get; set; }
        
        public IList<State> States = new List<State>();
        public IList<ContentTag> SeoContentTags { get; set; }
        public string H1 { get; set; }
        public string OnPageText { get; set; }
        
        public Market Market { get; set; }

        public List<City>[] MarketCities { get; set; }
        public List<Brand>[] MarketBrands { get; set; }
        public Dictionary<string, List<CommunityItemViewModel>[]> MarketCommunities { get; set; }
        public List<Market>[] BrandMarkets { get; set; }
        
        public XBrand Brand { get; set; }

        public bool ContainsGolfCommunities { get; set; }
        public bool ContainsWaterFrontCommunities { get; set; }
        public bool ContainsCondoTownCommunities { get; set; }
        
    }
}
