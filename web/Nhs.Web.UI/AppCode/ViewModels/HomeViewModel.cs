﻿using System.Collections.Generic;
using System.Web.Mvc;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.CMS;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class HomeViewModel:BaseViewModel
    {
        public bool IsCurrentLocation { get; set; }
        public string SearchText { get; set; }
        public IEnumerable<SelectListItem> NumOfBaths { get; set; }
        public IEnumerable<SelectListItem> SqFtMin { get; set; }
        public IEnumerable<SelectListItem> NumOfBeds { get; set; }
        public IEnumerable<SelectListItem> PriceLow { get; set; }
        public IEnumerable<SelectListItem> PriceHigh { get; set; }
        public SelectList PriceLoRange { get; set; }
        public SelectList PriceHiRange { get; set; }
        public SelectList BathList { get; set; }
        public SelectList BedRoomsList { get; set; }
        public SelectList SqFtList { get; set; }
        public IList<Brand> Brands { get; set; }
        public IList<CategoryPosition> Articles { get; set; }
        public IEnumerable<SpotLightCommunity> SpotLightCommunities { get; set; }  //Use only for New Home Listings
        public IEnumerable<SpotLightHomes> SpotLightHomes { get; set; }  //Use only for Mobile
        public IEnumerable<Community> FeaturedCommunities { get; set; }
        public string MarketName { get; set; }
        public int MarketId { get; set; }
        public bool ShowWelcomeMessage { get; set; }
        public string UserName { get; set; }
        
    }
}
