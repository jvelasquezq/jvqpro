﻿using System.Collections.Generic;
using System.Web.Mvc;
using Nhs.Library.Business.Map;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class LocationHandlerViewModel : BaseViewModel
    {
        public LocationHandlerViewModel()
        {
            Locations = new List<Location>();
        }

        public string SearchText { get; set; }
        public string DisplayText { get; set; }
        public string StateName { get; set; }
        public int MarketId { get; set; }
        public int PartnerId { get; set; }
        public int BrandPartnerId { get; set; }
        public string StateAbbr { get; set; }
        public string City { get; set; }
        public string TypeAheadUrl { get; set; }
        
        public string ResourceRoot { get; set; }
        public string SiteRoot { get; set; }

        public MapData StateMapData { get; set; }

        public decimal PriceLow { get; set; }
        public decimal PriceHigh { get; set; }
        public SelectList PriceLoRange { get; set; }
        public SelectList PriceHiRange { get; set; }
        public string ExtraParameters { get; set; }
        public List<Location> Locations { get; set; }
        public List<Market>[] SimilarMarkets { get; set; }

        public string MarketPoints { get; set; }
    }
}
