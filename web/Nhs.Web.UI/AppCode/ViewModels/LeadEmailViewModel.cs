﻿using System.Collections.Generic;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Partials;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class LeadEmailViewModel: BaseViewModel
    {
        public int CommunityId { get; set; }
        public int RequestId { get; set; }
        public string FirstName { get; set; }
        public string SiteName { get; set; }
        public string SiteLogo { get; set; }
        public string SiteLogoBackground { get; set; }
        public LeadEmailProperty SourceProperty { get; set; }
        public string BrochureUrl { get; set; }
        public string DetailLinkText { get; set; }
        public string LearnMoreUrl { get; set; }
        public string PolicyUrl { get; set; }
        public string UnsubscribeUrl { get; set; }
        public string SiteHomePageUrl { get; set; }
        public string CommResultsUrl { get; set; }
        public string QuestionsUrl { get; set; }
        public bool IsComLead { get; set; }
        public bool IsRecommendedEmail { get; set; }
        public bool IsSpecHome { get; set; }
        public string Email { get; set; }
        public string BrochureHeadingText { get; set; }
        public string BrochureTextFor { get; set; }
        public string AdditionalText { get; set; }
        public string PropertyHeaderText { get; set; }
        public string BuilderUrl { get; set; }
        public string RequestType { get; set; }
        public string GetQualifiedUrl { get; set; }
        public string MortgageRatesUrl { get; set; }
        public int RecoCount { get; set; }
        public bool IsMultiBrochue { get; set; }

        public bool ShowMatchingCommunities { get; set; }
        public bool OnDemandPdf { get; set; }
        public IList<LeadEmailProperty> Properties { get; set; }

        public IList<NearbyCommunity> RecommendedCommunities { get; set; }
        public LeadEmailProperty SourceLead { get; set; }

        public string BrandName { get; set; }
        public string PropertyName { get; set; }

        public string BeaconUrl { get; set; }

        public string GetUrl(string  url)
        {
           return string.IsNullOrEmpty(url) ? "javascript:void(0)" : url;
        }

        public AgentBrochureTemplate AgentBrochureTemplate { get; set; }
    }
}
