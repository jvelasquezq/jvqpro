using Bdx.Web.Api.Objects;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class HomeItem : ApiHomeResultV2
    {
        public string HotHomeDescription { get; set; }
        public string MarketName { get; set; }
        public string StateName { get; set; }
    }
}