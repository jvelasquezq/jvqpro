﻿using System.Collections.Generic;
using Bdx.Web.Api.Objects;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResultMobile
{
    public class BaseCommunityMobileViewModel : BaseViewModel
    {
        public bool IsTriggerFacets { get; set; }
        public string SearchParametersJSon { get; set; }  
        public string CommunitiesJSon { get; set; }
        public ApiResultCounts ResultCounts { get; set; }
        public List<CommunityItemMobileViewModel> CommunityResults { get; set; }
        public int Pages { get; set; }
        public int CurrentPage { get; set; }
        public string Zip { get; set; }
        public Market Market { get; set; }
        public string City { get; set; }
        public string BasicCommunitiesIdList { get; set; }
        public string CommunitiesIdList { get; set; }
        public int MarketId { get; set; }
        public bool ShowResultsBanner { get; set; }
        public int ResultsBannerPosition { get; set; }
    }
}