﻿using Bdx.Web.Api.Objects;

namespace Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResultMobile
{
    public class HomeItemMobileViewModel : ApiHomeResultV2
    {
        public string MarketName { get; set; }
    }
}