﻿using System.Collections.Generic;
using Bdx.Web.Api.Objects.Constants;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult;

namespace Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResultMobile
{
    public class CommunityMobileViewModel : BaseCommunityMobileViewModel, ICommunityHomeResultsViewModel
    {
        public CommunityMobileViewModel()
        {
            CommunityResults= new List<CommunityItemMobileViewModel>();
            CommunitySortBy = new List<SortByOption>
                {
                    new SortByOption {Name = LanguageHelper.Default, SortOrder = false, SortBy = SortBy.Random},
                    new SortByOption {Name = LanguageHelper.CityAToZ, SortOrder = false, SortBy = SortBy.City},
                    new SortByOption {Name = LanguageHelper.CityZToA, SortOrder = true, SortBy = SortBy.City},
                    new SortByOption {Name = LanguageHelper.PriceLowHigh, SortOrder = false, SortBy = SortBy.Price},
                    new SortByOption {Name = LanguageHelper.PriceHighLow, SortOrder = true, SortBy = SortBy.Price},
                    new SortByOption {Name = LanguageHelper.BuilderAToZ, SortOrder = false, SortBy = SortBy.Brand},
                    new SortByOption {Name = LanguageHelper.BuilderZToA, SortOrder = true, SortBy = SortBy.Brand},
                    new SortByOption {Name = LanguageHelper.CommunityAToZ, SortOrder = false, SortBy = SortBy.Comm},
                    new SortByOption {Name = LanguageHelper.CommunityZToA, SortOrder = true, SortBy = SortBy.Comm}
                };

        }
        public string State { get; set; }

        public string County { get; set; }
        public List<SortByOption> CommunitySortBy { get; internal set; }
        public SortBy SortBy { get; set; }
        public bool SortOrder { get; set; }
        public bool IsMapView { get; set; }
        public bool IsCurrentLocation { get; set; }   
        public double CenterLat { get; set; }
        public double CenterLng { get; set; }
        public bool SearchMapArea { get; set; }
        public string CommunityName { get; set; }
        public int Zoom { get; set; }
        public IList<ContentTag> SeoContentTags { get; set; }
        public SeoTemplateType SeoTemplateType { get; set; }
        public SearchResultsPageType SrpPageType { get; set; }
        public SearchParams SearchParams { get; set; }
        public bool ShowBasics { get; set; }

        public bool ShowTopBanner { get; set; }
        public bool ShowBottomBanner { get; set; }

        public bool ShowBrandSeo {get; set; }
        public int BrandId { get; set; }
        public string BrandName { get; set; }

        public SrpTypeEnum SeoType { get; set; }
    }

    public class FilterParameters
    {
        public SearchParams Parameters { get; set; }
        public bool MoreResults { get; set; }
        public string SearchText { get; set; }
        public Location Location { get; set; }
    }
}