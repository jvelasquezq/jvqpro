﻿using System.Collections.Generic;

namespace Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResultMobile
{
    public class QuickViewViewModel
    {
        public List<HomeItemMobileViewModel> Homes { get; set; }
        public string CommunityName { get; set; }
        public int CommunityId { get; set; }
        public int BuilderId { get; set; }
        public string PhoneNumber { get; set; }
        public bool SavedToPlanner { get; set; }
        public bool IsBoylResult { get; set; }
    }
}