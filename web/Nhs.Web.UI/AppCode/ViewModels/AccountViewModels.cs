﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Content;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Web.UI.AppCode.Helper;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        //[Required(ErrorMessage = "Email required")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        //[Required(ErrorMessage = "Password required")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool LoginSuccessful { get; set; }

        public string FromPage { get; set; }

        public bool OtherContent { get; set; }

        public bool IsAjaxRequest { get; set; }
    }

    public class BrochureRegisterViewModel : BaseViewModel
    {
        [Required(ErrorMessage = @"Password required")]
        public string Password { get; set; }

        [Required(ErrorMessage = @"Confirm password required")]
        public string ConfirmPassword { get; set; }

        public string FromPage { get; set; }
    }

    public class UpdateAccountViewModel : BaseViewModel
    {
        public string FirstName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ZipCodeReg { get; set; }
        public bool Newsletter { get; set; }
        public bool Promos { get; set; }       
        public bool IsAjaxRequest { get; set; }
    }

    public class RegisterViewModel : BaseViewModel
    {
        public string Name { get; set; }
        public string RealEstateLicense { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ZipCodeReg { get; set; }
        [Required(ErrorMessage = @"Agency name required")]
        public string AgencyName { get; set; }
        public bool LiveOutsideReg { get; set; }
        public bool RegisterSuccessful { get; set; }
        public bool IsModal { get; set; }
        public SmallLoginViewModel Login { get; set; }
        public bool Newsletter { get; set; }
        public bool Promos { get; set; }
        public bool ComingFromLead { get; set; }
        public string FromPage { get; set; }
        public string FromEmail { get; set; }
        public bool OtherContent { get; set; }
        public bool IsAjaxRequest { get; set; }
        public string MarketId { get; set; }
        public string MarketName { get; set; }
        public string HomeMarket { get; set; }
        public string PartnerSiteName { get; set; }
        public string UserId { get; set; }
        public string BeaconUrl { get; set; }
    }

    public class ForgotPasswordViewModel : BaseViewModel
    {
        [Required(ErrorMessage = "Enter a valid email address.")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public bool IsAjaxRequest { get; set; }
        public bool PasswordChanged { get; set; }
        public bool OtherContent { get; set; }
        public bool ShowGoback { get; set; }
        public string FromPage { get; set; }
    }

    public class SmallLoginViewModel : BaseViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class RegisterCreateAlertViewModel : BaseViewModel
    {
        public RegisterCreateAlertViewModel()
        {
            CreateAlert = true;
        }

        public string Account { get; set; }
        public string Password { get; set; }
        public string PasswordConfirm { get; set; }
        public bool Newsletter { get; set; }
        public bool Promos { get; set; }
        public bool CreateAlert { get; set; }
        public string Zip { get; set; }

        public int Radius { get; set; }
        public string PriceFrom { get; set; }
        public string PriceTo { get; set; }
        public string SuggestedName { get; set; }
        public string SkipLinkUrl { get; set; }

        public SelectList RadiusList { get; set; }
        public SelectList MinPriceList { get; set; }
        public SelectList MaxPriceList { get; set; }
        public bool NewLead { get; set; }
        public string ConversionIFrameSource { get; set; }
        public int SourceCommunityId { get; set; }
    }

    public class ManageAccountViewModel : BaseViewModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        [RegularExpression(StringHelper.EmailRegex, ErrorMessage = @"Not a valid email")]
        public string Email { get; set; }

        [Required(ErrorMessage = @"Confirmation email required")]
        [RegularExpression(StringHelper.EmailRegex, ErrorMessage = @"Not a valid email in confirmation")]
        public string ConfirmEmail { get; set; }

        public bool LiveOutside { get; set; }

        public string Address1 { get; set; }

        public string City { get; set; }

        public string StateAbbr { get; set; }
        public IEnumerable<SelectListItem> StateList { get; set; }

        public string CityId { get; set; }

        public string StateId { get; set; }

        public string MarketId { get; set; }

        public string MarketName { get; set; }

        public string ZipCode { get; set; }

        public string Phone { get; set; }

        public string RequestPhone { get; set; }

        public string MobilePhone { get; set; }

        [Required(ErrorMessage = @"Password required")]
        public string Password { get; set; }

        [Required(ErrorMessage = @"Confirm password required")]
        //TODO: Mustmach validation con password
        public string ConfirmPassword { get; set; }

        public int MoveInDate { get; set; }
        public IEnumerable<SelectListItem> MoveInDateList { get; set; }

        public int FinancePref { get; set; }
        public IEnumerable<SelectListItem> FinancePrefList { get; set; }

        public string Reason { get; set; }
        public IEnumerable<SelectListItem> ReasonList { get; set; }

        public bool Newsletter { get; set; }

        public bool Promos { get; set; }

        public bool ShowPromos { get; set; }
        public bool ShowNewsletter { get; set; }
        public bool International { get; set; }

        [Required(ErrorMessage = @"Agency name required")]
        public string AgencyName { get; set; }
        public string RealEstateLicense { get; set; }
        public bool Redirect { get; set; }
    }

    public class UnsubscribeViewModel : BaseViewModel
    {
        public bool ShowThankYou { get; set; }
        public string ErrorMessage { get; set; }
        public string Email { get; set; }
        public string ConfirmationMessage { get; set; }
        public string FromEmail { get; set; }
    }

    public class FavoritesViewModel : SavePropertiesViewModel
    {
        public bool SelectFavoriteTab { get; set; }
        public bool SelectClientsTab { get; set; }
        public bool SelectBrochureTab { get; set; }
        public bool SelectMyAccount { get; set; }
        public bool ShowMyClients { get; set; }
        public bool IsClientDetailPage { get; set; }
        public int ClientId { get; set; }
        public ProCrmViewModel ProCrmViewModel { get; set; }
    }

    public class SavePropertiesViewModel : BaseViewModel
    {
        public SavePropertiesViewModel()
        {
            FavoritesSaveHome = new List<FavoritesSaveHome>();
            FavoritesSaveComm = new List<FavoritesSaveComm>();
        }

        public List<FavoritesSaveHome> FavoritesSaveHome { get; set; }
        public List<FavoritesSaveComm> FavoritesSaveComm { get; set; }
        public IEnumerable<SearchAlert> SearchAlerts { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public int NumberOfSavedHomes
        {
            get { return FavoritesSaveHome.Count; }
        }

        public int NumberOfSavedComms
        {
            get { return FavoritesSaveComm.Count; }
        }
    }

    public class MyAccountHeaderAndTabsViewModel : BaseViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int SearchAlertNumber { get; set; }
        public bool IsMySavedPropertiesTabActive { get; set; }
    }

    public class FavoritesSaveHome : HomeDetailViewModel
    {
        public string ImagePath { get; set; }
        public string PlanName { get; set; }
        public string StatusIcon { get; set; }
        public string StatusIconAlt { get; set; }
        public bool SpecialOffer { get; set; }
        public bool HotHome { get; set; }
        public bool HasConsumerPromos { get; set; }
        public bool HasAgentPromos { get; set; }
        public bool ShowFreebrochure { get; set; }
        public bool ShowViewBrochure { get; set; }
        public string ViewBrochureUrl { get; set; }
    }

    public class FavoritesSaveComm : BaseViewModel, ISecondaryInfoViewModel
    {
        public bool ShowFreebrochure { get; set; }
        public bool ShowViewBrochure { get; set; }
        public string ViewBrochureUrl { get; set; }
        public string CommThumbnail { get; set; }
        public string BuilderName { get; set; }
        public string MarketName { get; set; }
        public string StateName { get; set; }
        public string StateAbbr { get; set; }
        public string Address { get; set; }
        public string CommPrice { get; set; }
        public decimal PriceHigh { get; set; }
        public decimal PriceLow { get; set; }
        public string HomeCount { get; set; }
        public bool InactiveSavedCommMessage { get; set; }
        public string CommVideo { get; set; }
        public bool HasVideo { get; set; }
        public bool IsSpecialOffer { get; set; }
        public bool IsComingSoon { get; set; }
        public bool IsHotHome { get; set; }
        public bool IsBilled { get; set; }
        public bool IsPlan { get; set; }
        public bool HasConsumerPromos { get; set; }
        public bool HasAgentPromos { get; set; }

        public bool HasCoop { get; set; }
        public bool HasBuilderPactLink { get; set; }
        public string BuilderPackLink { get; set; }

        #region ISecondaryInfoViewModel

        public bool IsPageCommDetail { get; set; }
        public int MarketId { get; set; }
        public int CommunityId { get; set; }
        public int BuilderId { get; set; }
        public int PlanId { get; set; }
        public int SpecId { get; set; }
        public string DetailDescription { get; set; }
        public string CommunityName { get; set; }
        public string BuilderUrl { get; set; }
        public string CommunityUrl { get; set; }
        public string CommunityCity { get; set; }
        public string CommunityDescription { get; set; }
        public string BcType { get; set; }
        public string AgentPolicyLink { get; set; }
        public string AgentCompensation { get; set; }
        public string AgentCompensationAdditionalComments { get; set; }
        public string AgentCommissionBasis { get; set; }
        public string AgentCommissionPayoutTiming { get; set; }
        public ICollection<GreenProgram> GreenPromos { get; set; }
        public IList<IImage> AwardImages { get; set; }
        public IList<Testimonial> Testimonials { get; set; }
        public IList<School> Schools { get; set; }
        public IDictionary<AmenityGroup, IList<IAmenity>> Amenities { get; set; }
        public ICollection<Mvc.Domain.Model.Web.Utility> Utilities { get; set; }
        public bool PreviewMode { get; set; }
        public string PhoneNumber { get; set; }
        #endregion
    }

    public class MyAccountBrochure : BaseViewModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        [RegularExpression(StringHelper.EmailRegex, ErrorMessage = @"Not a valid email")]
        public string Email { get; set; }

        public string AgencyName { get; set; }
        public string City { get; set; }
        public string AgentLicenseNumber { get; set; }

        public string StateAbbr { get; set; }
        public IEnumerable<SelectListItem> StateList { get; set; }
        public string BusinessPhone { get; set; }

        public string MobilePhone { get; set; }

        [Display(Name = "Agent Photo size have to be width < 103 pixels and height < 131 pixels")]
        public HttpPostedFileBase AgentPhoto { get; set; }

        [Display(Name = "Select Agency size have to be width < 180 pixels and height < 100 pixels")]
        public HttpPostedFileBase AgencyLogo { get; set; }

        public string AgentPhotoPath { get; set; }
        public string AgencyLogoPath { get; set; }
        public bool HavAgentPhotoOrAgencyLog
        {
            get
            {
                return !string.IsNullOrEmpty(AgentPhotoPath) || !string.IsNullOrEmpty(AgencyLogoPath);
            }
        }
        public bool Saved { get; set; }
    }
}
