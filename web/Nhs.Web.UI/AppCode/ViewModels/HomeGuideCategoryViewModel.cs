﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nhs.Library.Common;
using Nhs.Library.Web;
using Nhs.Library.Enums;
using Nhs.Library.Constants;
using System.Web.Mvc;
using Nhs.Library;
using Nhs.Mvc.Routing.Configuration;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class HomeGuideCategoryViewModel : BaseViewModel
    {
        public HomeGuideCategoryViewModel()
        {
            CatLayoutImages = new List<CatLayoutImages>();
            CatLayoutText = new List<CatLayoutText>();
            CategoryList = new List<CategoryList>();
        }

        public CategoryBreadCrumd CategoryBreadCrumd { get; set; }
        public List<CatLayoutImages> CatLayoutImages { get; set; }
        public List<CatLayoutText> CatLayoutText { get; set; }
        public List<CategoryList> CategoryList { get; set; }
    }

    public class CatLayoutImages
    {
        public int ArticleId { get; set; }
        public string ArticleTitle { get; set; }
        public string Teaser { get; set; }

        public MvcHtmlString GetTeaser
        {
            get { return MvcHtmlString.Create(Teaser); }
        }

        public CMSCategoryPositions CmsCategoryPositions { get; set; }

        public String Url
        {
            get { return new List<RouteParam> { new RouteParam(RouteParams.Article, ArticleTitle) }.ToUrl(Pages.HomeGuideArticle); }
        }
    }

    public class CatLayoutText : CatLayoutImages
    {
        public string Headline { get; set; }

        public MvcHtmlString GetHeadline
        {
            get { return MvcHtmlString.Create(Headline); }
        }

        public string TeaserLinkText { get; set; }

        public MvcHtmlString GetTeaserLinkText
        {
            get { return MvcHtmlString.Create(TeaserLinkText); }
        }

        public string SponsorName { get; set; }

        public MvcHtmlString GetSponsorName
        {
            get { return MvcHtmlString.Create("<p class=\"nhsArticleSponsorText\">(Courtesy of " + SponsorName + ")</p>"); }
        }
    }

    public class CategoryList
    {
        public CategoryList()
        {
            SubCategoryList = new List<CategoryList>();
        }

        public string CategoryName { get; set; }
        public string CategoryId { get; set; }
        public List<CategoryList> SubCategoryList { get; set; }
    }

    public class CategoryBreadCrumd
    {
        public CategoryBreadCrumd()
        {
           Crumbs = new List<string>();
           Categories = new List<Category>();
        }

        public List<string> Crumbs { get; set; }
        public List<Category> Categories { get; set; }
    }
}