﻿using System.Collections.Generic;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class StateBuilderViewModel : BaseStateIndexViewModel
    {
        public string PartnerName { get; set; }
        public string StateName { get; set; }
        public string StateAbbreviation { get; set; }
        public string MapImageUlr { get; set; }
        public List<ExtendedCommunityResult> SpotLightCommunities { get; set; }
        public List<List<Market>> BrandMarkets { get; set; }
        public List<ContentTag> SeoContentTags { get; set; }

        public string SEOContent_NHS_Header { get; set; }
        public string SEOContent_NHS_Footer { get; set; }
        public string SEOContent_NHS_H1 { get; set; }
        public int SelectedMarketId;
        public List<Market> MarketList { get; set; }
        public SelectList LowPriceRange;
        public SelectList HighPriceRange;
        

        public Brand Brand { get; set; }

        public bool ContainsGolfCommunities { get; set; }
        public bool ContainsWaterFrontCommunities { get; set; }
        public bool ContainsCondoTownCommunities { get; set; }
        
    }
}
