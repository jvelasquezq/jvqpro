﻿using System.Collections.Generic;
using Nhs.Library.Proxy;
using Nhs.Web.UI.AppCode.Helper;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class RecommendedCommunitiesViewModal : HomeDetailViewModel
    {
        public bool IsComunityDetailV2 { get; set; }
        public string HeadlineText { get; set; }
        public string SubheadText { get; set; }
        public bool DefaultChecked { get; set; }
        public string MaxRecs { get; set; }
        public string SessionId { get; set; }
        public string Refer { get; set; }
        public string Email { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string CommList { get; set; }
        public List<ApiRecoCommunityResultEx> RecoComms { get; set; }
        public bool IsModal { get; set; }
        public string UrlIframe { get; set; }
        public string RecoCommunityList { get; set; }
        public string Google { get; set; }
        public bool FromMatchEmail { get; set; }
        public bool MatchEmail { get; set; }
        public string GetHeadlineText
        {
            get
            {
                return !string.IsNullOrEmpty(HeadlineText)
                           ? HeadlineText
                           : LanguageHelper.YourFreeBrochureHasBeenSent;
            }
        }

        public string GetSubheadText
        {
            get
            {
                return !string.IsNullOrEmpty(SubheadText)
                           ? SubheadText
                           : LanguageHelper.WeFoundOtherGreatCommunities;
            }
        }
    }
}