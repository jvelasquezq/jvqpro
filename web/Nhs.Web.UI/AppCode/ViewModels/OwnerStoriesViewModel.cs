﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using Nhs.Mvc.Domain.Model;
using Nhs.Mvc.Domain.Model.BhiTransaction;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class OwnerStoriesViewModel : BaseViewModel
    {
        public OwnerStoriesViewModel()
        {
            
        }

        #region Input Fields
        [Required(ErrorMessage = @"First Name can't be null")]
        [StringLength(50,ErrorMessage = @"First Name can't contain more than 50 characters")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = @"Last Name can't be null")]
        [StringLength(50, ErrorMessage = @"Last Name can't contain more than 50 characters")]
        public string LastName { get; set; }

        [Required(ErrorMessage = @"Email can't be null")]
        [StringLength(100, ErrorMessage = @"Email can't contain more than 100 characters")]
        public string Email { get; set; }

        [Required(ErrorMessage = @"Builder Name can't be null")]
        [StringLength(100, ErrorMessage = @"Builder Name can't contain more than 100 characters")]
        public string BuilderName { get; set; }

        public int BuilderId { get; set; }

        //[Required(ErrorMessage = @"Community Name can't be null")]
        [StringLength(100, ErrorMessage = @"Community Name can't contain more than 100 characters")]
        public string CommunityName { get; set; }

        public int CommunityId { get; set; }

        public string FilePath { get; set; }
        public bool HasVideo { get; set; }
        public bool HasImage { get; set; }

        public int OwnerStoryId { get; set; }
        public string OnlineVideoId { get; set; }
        public int VideoId { get; set; }
        public string VideoUrl { get; set; }
        public string VideoThumbnailUrl { get; set; }
        public string VideoTitle { get; set; }

        public string ValidCommunityName { get; set; }
        public string ValidBuilderName { get; set; }
        public string ValidCityName { get; set; }

        [Required(ErrorMessage = @"City can't be null")]
        [StringLength(50, ErrorMessage = @"City can't contain more than 100 characters")]
        public string City { get; set; }

        [Required(ErrorMessage = @"Desciption can't be null")]
        [StringLength(1000, ErrorMessage = @"Story Description can't contain more than 1000 characters")]
        public string StoryDescription { get; set; }

        [Required(ErrorMessage = @"Category can't be null")]
        public string Category { get; set; }

        public IEnumerable<SelectListItem> CategoryList { get; set; }      
  
        public HttpPostedFileWrapper uploadFile { get; set; }

        public bool? OwnerStoryStatus { get; set; }
        public bool NoNullOwnerStoryStatus { get; set; }
        #endregion        

        public string ContestRulesContent { get; set; }
        public string SubmissionContent { get; set; }

        public bool SubmitOk { get; set; }
        public string Display { get; set; }

        public List<OwnerStoryContent> OwnerStoryContentList { get; set; }
        public List<OwnerStory> OwnerStoriesList { get; set; }
        public bool ShowOwnerStoryDetail { get; set; }
    }

    public enum OwnerStoriesStepsEnum
    {
        FistStep = 1,
        SecondStep = 2,
        ThirdStep= 3
    }

    public class OwnerStoriesStepsViewModel
    {
        public OwnerStoriesStepsViewModel(string submitBtnId, string submitBtnText, 
            OwnerStoriesStepsEnum step, bool showBtn = true)
        {
            SubmitButtonId = submitBtnId ?? string.Empty;
            SubmitButtonText = submitBtnText ?? string.Empty;
            Step = step;
            ShowButton = showBtn;
        }
        public string SubmitButtonId { get; private set; }
        public string SubmitButtonText { get; private set; }
        public OwnerStoriesStepsEnum Step { get; private set; }
        public bool ShowButton { get; private set; }
    }
    
    public class OwnerStoriesCurationChangesViewModel
    {
        public OwnerStoriesCurationChangesViewModel(string submitBtnId, string submitBtnText, bool showBtn = true)
        {
            SubmitButtonId = submitBtnId ?? string.Empty;
            SubmitButtonText = submitBtnText ?? string.Empty;
            
            ShowButton = showBtn;
        }
        public string SubmitButtonId { get; private set; }
        public string SubmitButtonText { get; private set; }
        public bool ShowButton { get; private set; }
    }
}
