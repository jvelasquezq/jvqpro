﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nhs.Mvc.Routing.Interface;
using Nhs.Web.UI.AppCode.Helper;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class SiteHelpViewModels
    {
        public class ContactBdxViewModel : BaseViewModel
        {
            public string Title { get; set; }

            public string FirstName { get; set; }

            public string LastName { get; set; }

            public string Company { get; set; }

            public string Email { get; set; }

            public string Phone { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public IEnumerable<SelectListItem>  Products { get; set; }
            public string Product { get; set; }
            public string InterestedIn { get; set; }
            public string Comments { get; set; }
            public bool ShowSuccessMessage { get; set; }
            public string PartnerSiteUrl { get { return NhsRoute.PartnerSiteUrl; } }
        }

        public class ContactUsViewModel : BaseViewModel
        {
            public string ContactUsEmail { get; set; }
        }
    }
}
