﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Library.Business;
using Nhs.Mvc.Domain.Model.Web;
using System.IO;
using Nhs.Library.Common;
using System.ComponentModel.DataAnnotations;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects.Constants;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class AdvancedSearchViewModel: BaseViewModel
    {
        public string SchoolId { get; set; }
        public string CommunityId { get; set; }
        public string BrandId { get; set; }
        public string SearchText { get; set; }
        public string SearchType { get; set; }
        public string TypeAheadUrl { get; set; }
        public bool CommSearch { get; set; }
        public bool CblPool { get; set; }
        public bool CblGolf { get; set; }
        public bool CblGreen { get; set; }
        public bool CblNature { get; set; }
        public bool CblParks { get; set; }
        public bool CblViews { get; set; }
        public bool CblWater { get; set; }
        public bool CblSports { get; set; }
        public bool CblAdultSenior { get; set; }
        public bool RblHomeAll { get; set; }
        public bool RblHomeQuick { get; set; }
        public bool RblOfferAll { get; set; }
        public bool RblOfferHot { get; set; }
        public PromotionType Promo { get; set; }
        public IEnumerable<Nhs.Mvc.Domain.Model.Web.SchoolDistrict> SchoolList { get; set; }        
        public IEnumerable<Community> CommunityList { get; set; }
        public IEnumerable<Brand> BuilderList { get; set; }
        public int PriceLow { get; set; }
        public int PriceHigh { get; set; }
        public int BedDrop { get; set; }
        public int BathDrop { get; set; }
        public decimal GarageDrop { get; set; }
        public int LivingDrop { get; set; }
        public string HomeDrop { get; set; }
        public int StoriesDrop { get; set; }
        public int MasterBedDrop { get; set; }
        public int SqFtMin { get; set; }
        public SelectList LowPriceRange;
        public SelectList HighPriceRange;
        public SelectList BedList;
        public SelectList BathList;
        public SelectList SqFtList { get; set; }
        public SelectList GarageList;
        public SelectList LivingList;
        public SelectList HomeList;
        public SelectList StoriesList;
        public SelectList MasterBedList;
    }
}
