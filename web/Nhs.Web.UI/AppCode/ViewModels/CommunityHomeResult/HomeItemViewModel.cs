﻿using System.Collections.Generic;
using Bdx.Web.Api.Objects;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Entities;
using Nhs.Mvc.Domain.Model.Web.Interfaces;

namespace Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult
{
    public class HomeItemViewModel : ApiHomeResultV2 ,IBaseApiItem
    {
        public IEnumerable<HotHome> HotHomes { get; set; }
        public IEnumerable<CommunityPromotion> Promotions { get; set; }
        public string MarketName { get; set; }
        public string StateName { get; set; }
        public bool IsFavoriteListing { get; set; }
        public string GroupingBarTitle { get; set; }
        public bool IsNearby { get; set; }
        public bool ShowBasicListingIndicator { get; set; }
        public bool ShowBasicNearByListingIndicator { get; set; }
        public int NumHomes { get; set; }

        public HomeItemViewModel()
        {
            HotHomes = new List<HotHome>();
            Promotions = new List<CommunityPromotion>();
        }

        public string PrHi
        {
            get { return Price; }
            set { throw new System.NotImplementedException(); }
        }

        public string PrLo
        {
            get { return Price; }
            set { throw new System.NotImplementedException(); }
        }

        public int IsBasic { get; set; }        
    }
}