﻿using System.Collections.Generic;
using Bdx.Web.Api.Objects;
using Bdx.Web.Api.Objects.Constants;
using Nhs.Library.Business.Map;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Web.UI.AppCode.Helper;

namespace Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult
{
    public class CommunityHomeResultsViewModel : BaseCommunityHomeResultsViewModel, ICommunityHomeResultsViewModel
    {
        public CommunityHomeResultsViewModel()
        {
            CommunitySortBy = new List<SortByOption>
                {
                    new SortByOption {Name = LanguageHelper.Default, SortOrder = false, SortBy = SortBy.Random},
                    new SortByOption {Name = LanguageHelper.MatchingHomes, SortOrder = true, SortBy = SortBy.NumHomes},
                    new SortByOption {Name = LanguageHelper.CityAToZ, SortOrder = false, SortBy = SortBy.City},
                    new SortByOption {Name = LanguageHelper.CityZToA, SortOrder = true, SortBy = SortBy.City},
                    new SortByOption {Name = LanguageHelper.PriceLowHigh, SortOrder = false, SortBy = SortBy.Price},
                    new SortByOption {Name = LanguageHelper.PriceHighLow, SortOrder = true, SortBy = SortBy.Price},
                    new SortByOption {Name = LanguageHelper.BuilderAToZ, SortOrder = false, SortBy = SortBy.Brand},
                    new SortByOption {Name = LanguageHelper.BuilderZToA, SortOrder = true, SortBy = SortBy.Brand},
                    new SortByOption {Name = LanguageHelper.CommunityAToZ, SortOrder = false, SortBy = SortBy.Comm},
                    new SortByOption {Name = LanguageHelper.CommunityZToA, SortOrder = true, SortBy = SortBy.Comm}
                };

            HomeSortBy = new List<SortByOption>
                {
                    new SortByOption {Name = LanguageHelper.Default, SortOrder = false, SortBy = SortBy.Random},
                    new SortByOption {Name = LanguageHelper.CityAToZ, SortOrder = false, SortBy = SortBy.City},
                    new SortByOption {Name = LanguageHelper.CityZToA, SortOrder = true, SortBy = SortBy.City},
                    new SortByOption {Name = LanguageHelper.PriceLowHigh, SortOrder = false, SortBy = SortBy.Price},
                    new SortByOption {Name = LanguageHelper.PriceHighLow, SortOrder = true, SortBy = SortBy.Price},
                    new SortByOption {Name = LanguageHelper.BedroomsLowHigh, SortOrder = false, SortBy = SortBy.Bed},
                    new SortByOption {Name = LanguageHelper.BedroomsHighLow, SortOrder = true, SortBy = SortBy.Bed},
                    new SortByOption {Name = LanguageHelper.BuilderAToZ, SortOrder = false, SortBy = SortBy.Brand},
                    new SortByOption {Name = LanguageHelper.BuilderZToA, SortOrder = true, SortBy = SortBy.Brand},
                    new SortByOption {Name = LanguageHelper.PlanNameAToZ, SortOrder = false, SortBy = SortBy.Plan},
                    new SortByOption {Name = LanguageHelper.PlanNameZToA, SortOrder = true, SortBy = SortBy.Plan}
                };

            ZipCodesCols = new List<IEnumerable<int>>();
            CitiesCols = new List<IEnumerable<City>>();
            SeoCommunityCols = new List<IEnumerable<SEOCommunity>>();
        }

        public List<SortByOption> CommunitySortBy { get; internal set; }
        public List<SortByOption> HomeSortBy { get; internal set; }

        public string HotDealsUrl { get; set; }
        public string QuickMoveInUrl { get; set; }
        public MapData Map { get; set; }
        public IList<ContentTag> SeoContentTags { get; set; }
        public SeoTemplateType SeoTemplateType { get; set; }
        public string CommunityName { get; set; }
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public bool PlotRadius { get; set; }
        public bool ShowBasics { get; set; }
        public bool ShowGolfSeo { get; set; }
        public int MinZoom { get; set; }
        public string State { get; set; }
        public string County { get; set; }
        public string Zip { get; set; }
        public int MarketId { get; set; }
        public string GoogleStaticMapUlr { get; set; }
        public string MarketBackgroundImagePath { get; set; }
        public ApiResultCounts ResultsCount { get; set; }

        public bool ShowComingSoonLink { get; set; }

        public IEnumerable<IEnumerable<int>> ZipCodesCols { get; set; }
        public IEnumerable<IEnumerable<City>> CitiesCols { get; set; }
        public IEnumerable<IEnumerable<SEOCommunity>> SeoCommunityCols { get; set; }

        public bool RightAdToDisplayIsPosition { get; set; }
        public string PagingUrl { get; set; }
        public string CityNameFilterUrl { get; set; }
        public string TabsUrl { get; set; }
        public SortBy SortBy { get; set; }
        public bool SortOrder { get; set; }
        public bool SearchMapArea { get; set; }
        public bool SearchMapAreaConfirmed { get; set; }
        public bool ShowReturnUserModal { get; set; }

        public string ListHubProviderId { get; set; }
        public string ListHubTestLogging { get; set; }

        public SrpTypeEnum SeoType { get; set; }
        public bool IsBuilderTabSearch { get; set; }
        public bool IsMapVisible { get; set; }
    }


    public class SortByOption
    {
        public SortBy SortBy { get; set; }
        public bool SortOrder { get; set; }
        public string Name { get; set; }
    }
}
