﻿using System.Collections.Generic;
using Nhs.Library.Constants;
using Nhs.Library.Helpers.Content;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult
{
    public class MapPopupViewModel : BaseViewModel
    {
        public string CommunityName { get; set; }
        public string BuilderName { get; set; }
        public string ZipCode { get; set; }
        public string Addr { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string StateName { get; set; }
        public string MarketName { get; set; }
        public string PriceRange { get; set; }
        public int CommunityId { get; set; }
        public int BuilderId { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public int PropertyId { get; set; }
        public bool IsBasic { get; set; }
        public int NumHom { get; set; }

        public List<RouteParam> CdParams()
        {
            List<RouteParam> paramz;
            var partnerInfo = PartnerLayoutHelper.GetPartnerLayoutConfig();

            if (partnerInfo.IsBrandPartnerMove && NhsRoute.PartnerId == NhsRoute.BrandPartnerId) // not a private label site
            {
                paramz = new List<RouteParam> { new RouteParam(RouteParams.Page, Pages.CommunityDetailMove),
                                           new RouteParam(RouteParams.CommunityId, CommunityId.ToString()),
                                           new RouteParam(RouteParams.State, State),
                                           new RouteParam(RouteParams.MarketName, MarketName.RemoveSpecialCharacters()),
                                           new RouteParam(RouteParams.City, City.RemoveSpecialCharacters()),
                                           new RouteParam(RouteParams.CommunityName, CommunityName.RemoveSpecialCharacters()),
                                           new RouteParam(RouteParams.BuilderName, BuilderName.RemoveSpecialCharacters())
                                         };
            }
            else
            {
                paramz = new List<RouteParam> { new RouteParam(RouteParams.Builder, BuilderId.ToString()),
                                           new RouteParam(RouteParams.Community, CommunityId.ToString())
                                         };
            }

            return paramz;
        }
    }
}
