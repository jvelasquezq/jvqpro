﻿using System.Collections.Generic;
using Bdx.Web.Api.Objects;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult
{
    public interface ICommunityHomeResultsViewModel
    {
        IList<ContentTag> SeoContentTags { get; set; }
        SeoTemplateType SeoTemplateType { get; set; }
        Market Market { get; set; }
        string City { get; set; }
        string State { get; set; }
        string Zip { get; set; }
        string County { get; set; }
        string CommunityName { get; set; }
        SearchResultsPageType SrpPageType { get; set; }
        int BrandId { get; set; }
        string BrandName { get; set; }
        ApiResultCounts ResultCounts { get; set; }
        SrpTypeEnum SeoType { get; set; }
    }
}