﻿using System.Collections.Generic;
using Bdx.Web.Api.Objects;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult
{
    public class BaseCommunityHomeResultsViewModel : BaseViewModel
    {
        public BaseCommunityHomeResultsViewModel()
        {
            CommunityResults = new List<CommunityItemViewModel>();
            HomeResults = new List<HomeItemViewModel>();
        }  

        public bool IsTriggerFacets { get; set; }
        public SearchResultsPageType SrpPageType { get; set; }
        public string BrandsJSon { get; set; }
        public IEnumerable<IEnumerable<Brand>> BrandsList { get; set; }
        public IEnumerable<IEnumerable<Brand>> BasicBrandsList { get; set; }
        public string CommunitiesJSon { get; set; }
        public string SchoolDistrictsJSon { get; set; }
        public string CitysJSon { get; set; }
        public IEnumerable<CommunityItemViewModel> CommunityResults { get; set; }
        public IEnumerable<HomeItemViewModel> HomeResults { get; set; }
        public ApiResultCounts ResultCounts { get; set; }
        public FindCustomBuilders FindCustomBuilders { get; set; }
        public Market Market { get; set; }
        public int PageSize { get; set; }
        public int CurrentPage { get; set; }
        public string SearchParametersJSon { get; set; }
        public bool ShowCitySearch { get; set; }
        public string BrandPartnerName { get; set; }
        public string City { get; set; }

        public IList<ExtendedCommunityResult> FeaturedListingsTop = new List<ExtendedCommunityResult>();
        public IList<ExtendedCommunityResult> FeaturedListingsBottom = new List<ExtendedCommunityResult>();

        // logs lists for comms, basic listings, basic comms and homes
        public string BasicCommunitiesIdList { get; set; }
        public string CommunitiesIdList { get; set; }
        public string HomesIdList { get; set; }
        public string BasicListingsIdList { get; set; }
        public string FeaturedListingsIdList { get; set; }       
    }
}
