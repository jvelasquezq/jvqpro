﻿using System.Collections.Generic;
using Bdx.Web.Api.Objects;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Entities;

namespace Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult
{
    public class CommunityItemViewModel : ApiCommunityResultV2, IBaseApiItem
    {
        public IEnumerable<HotHome> HotHomes { get; set; }
        public IEnumerable<CommunityPromotion> Promotions { get; set; }
        public string MarketName { get; set; }
        public string StateName { get; set; }
        public bool IsFavoriteListing { get; set; }
        public string GroupingBarTitle { get; set; }
        public bool IsNearby { get; set; }
        public bool ShowBasicListingIndicator { get; set; }
        public bool ShowBasicNearByListingIndicator { get; set; }
        public int IsBl { get; set; }
        public int Br { get; set; }

        public CommunityItemViewModel()
        {
            HotHomes = new List<HotHome>();
            Promotions = new List<CommunityPromotion>();
        }
    }
}
