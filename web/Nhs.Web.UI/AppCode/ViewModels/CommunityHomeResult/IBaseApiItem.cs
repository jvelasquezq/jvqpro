﻿using System.Collections.Generic;
using Bdx.Web.Api.Objects;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Entities;

namespace Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult
{
    public interface IBaseApiItem
    {
        IEnumerable<HotHome> HotHomes { get; set; }
        IEnumerable<CommunityPromotion> Promotions { get; set; }
        string MarketName { get; set; }
        string StateName { get; set; }
        bool IsFavoriteListing { get; set; }
        string GroupingBarTitle { get; set; }
        bool IsNearby { get; set; }
        bool ShowBasicListingIndicator { get; set; }
        bool ShowBasicNearByListingIndicator { get; set; }
        ApiBrand Brand { get; set; }
        int NumHomes { get; set; }
        string City { get; set; }
        string State { get; set; }
        string PrHi { get; set; }
        string PrLo { get; set; }
        int IsBasic { get; set; }
        int IsBl { get; set; }
        int Br { get; set; }
        string Zip { get; set; }
        string County { get; set; }
    }
}