﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.CMS;
using Nhs.Mvc.Routing.Interface;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class ResourceCenterViewModels
    {
        public class ResourceCenterBaseViewModel : BaseViewModel
        {
            public ResourceCenterBaseViewModel()
            {
                SlideShow = new RCSlideShow();
                TrendingNow = new List<RcArticle>();
                ParentCategories = new List<RCCategory>();
                SearchBoxId = "MktSearch";
            }

            public string ArticleSearch { get; set; }
            public string MktSearch { get; set; }
            public RCSlideShow SlideShow { get; set; }
            public List<RcArticle> TrendingNow { get; set; }
            public List<RCCategory> ParentCategories { get; set; }

            /// <summary>
            /// If you instanciate this class make sure to fill the properties, 
            /// see ResourceCenterAttribute and Rail.cshtml
            /// </summary>
            public CmsProSearch CmsProSearchParams { get; set; }
            
            public bool IsResourceGuidePage { get; set; }
            public bool IsArticlesPage { get; set; }
            public bool IsCategoryPage { get; set; }

            public string SearchFillText
            {
                get
                {
                    if(Globals.PartnerLayoutConfig.IsBrandPartnerCna)
                        return NhsRoute.ShowMobileSite ? "Búsqueda por tema" : "Búsqueda por tema (cocinas, aprobación de la hipoteca, el proceso de construcción, etc.)";
                    return NhsRoute.ShowMobileSite?"Search by Topic": "Search by topic (kitchens, mortgage approval, building process, etc)";
                }
            } 
          
            public bool UseWideHomesSearchBox { get; set; }
            public string SearchBoxId { get; set; }
        }

        public class ResourceCenterFeaturedHomesSlideShowViewModel : ResourceCenterBaseViewModel
        {
            public ResourceCenterFeaturedHomesSlideShowViewModel()
            {
                TrendingNow = new List<RcArticle>();
            }
            public RcFeaturedHomesSlideShow FeaturedHomes { get; set; }
        }

        public class HomeViewModel : ResourceCenterBaseViewModel
        {
            public IEnumerable<RCCategory> Categories { get; set; }
        }

        public class HomeCategoryItemViewModel : ResourceCenterBaseViewModel
        {
            public RCCategory Category { get; set; }
            public HomeCategoryItemViewModel(RCCategory category)
            {
                Category = category;
            }

            public string FormattedCategoryName
            {
                get { return NhsRoute.ShowMobileSite ? Category.CategoryName : Category.CategoryName.ToUpper(); }
            }
        }

        public class ArticleViewModel : ResourceCenterBaseViewModel, IRCArticleViewModel
        {
            public ArticleViewModel()
            {
                Article = new RcArticle();
                Autor = new RcAuthor();
                RelatedArticles = new List<RcArticle>();
            }
            public RcArticle Article { get; set; }
            public RcAuthor Autor { get; set; }
            public IEnumerable<RcArticle> RelatedArticles { get; set; }
        }

        public class ArticleSlideShowViewModel : ArticleViewModel, IRCArticleViewModel
        {
            //Reuse article template along with SlideShow on base (extended SlideShow to hold additional properties)
        }

        public class ArticleResultsViewModel : ResourceCenterBaseViewModel
        {
            public string KeyWords { get; set; }
            public string NoResultsMsg { get; set; }
            public IEnumerable<RcArticle> ArticlesList { get; set; }
        }

        public class CategoryViewModel : ResourceCenterBaseViewModel
        {
            public string CategoryName { get; set; }
            public IEnumerable<string> SubCategories { get; set; }
            public IEnumerable<RcArticle> FeaturedArticles { get; set; }
            public RcGlobalBusinessConfig GlobalBusinessConfig { get; set; }
        }

        public class MultiPartArticleViewModel : ResourceCenterBaseViewModel
        {
            public RcMpArticleContainer MultiPartArticle { get; set; }
        }

        public class MultiPartArticleSectionViewModel : ResourceCenterBaseViewModel
        {
            public RcMpArticleContainer MultiPartArticle { get; set; }
            public RcMpArticleSection ArticleSection { get; set; }
            public RcMpArticleSection PreviousArticleSection { get; set; }
            public RcMpArticleSection NextArticleSection { get; set; }
            public int SectionIndex { get; set; }
        }

        public class AuthorPageViewModel : ResourceCenterBaseViewModel
        {
            public string AuthorFirstName
            {
                get
                {
                    var firstName = string.Empty;

                    if (AuthorInfo != null && string.IsNullOrWhiteSpace(AuthorInfo.Name) == false)
                    {
                        var fname = AuthorInfo.Name.Split(new char[] { ' ' });
                        firstName = fname.FirstOrDefault();
                    }

                    return firstName;
                }
            }
            public string ArticleSmartFormType { get; set; }
            public RcAuthor AuthorInfo { get; set; }
            public IEnumerable<RcArticle> RelatedArticles { get; set; }
            public long Id { get; set; }

        }
    }
}
