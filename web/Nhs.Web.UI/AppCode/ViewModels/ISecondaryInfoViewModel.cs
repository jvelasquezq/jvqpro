﻿using System.Collections.Generic;
using Nhs.Library.Constants.Enums;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Web.UI.AppCode.ViewModels.LeadView;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public interface ISecondaryInfoViewModel : IFreeBrochureLinkInformation
    {
        int MarketId { get; set; }
        int CommunityId { get; set; }
        int BuilderId { get; set; }
        int PlanId { get; set; }
        int SpecId { get; set; }
        string BcType { get; set; }
        string DetailDescription { get; }
        string CommunityName { get; set; }
        string MarketName { get; set; }
        string BuilderUrl { get; set; }
        string BuilderName { get; set; }
        string CommunityUrl { get; set; }
        string CommunityCity { get; set; }
        string CommunityDescription { get; set; }
        string AgentCompensation { get; set; }
        string AgentPolicyLink { get; set; }
        string AgentCompensationAdditionalComments { get; set; }
        string AgentCommissionBasis { get; set; }
        string AgentCommissionPayoutTiming { get; set; }

        IList<IImage> AwardImages { get; set; }
        IList<Testimonial> Testimonials { get; set; }

        IList<School> Schools { get; set; }
        IDictionary<AmenityGroup, IList<IAmenity>> Amenities { get; set; }
        ICollection<Mvc.Domain.Model.Web.Utility> Utilities { get; set; }
        bool PreviewMode { get; set; }
        bool IsBilled { get; set; }
        bool HasCoop { get; set; }
        bool HasBuilderPactLink { get; set; }
        string BuilderPackLink { get; set; }
    }
}
