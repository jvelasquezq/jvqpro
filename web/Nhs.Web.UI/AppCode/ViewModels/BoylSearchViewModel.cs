﻿using System.Collections.Generic;
using Nhs.Library.Business;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Web.UI.AppCode.Helper;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class BoylSearchViewModel : BaseViewModel
    {
        public string StateAbbr { get; set; }
        public int MarketId { get; set; }
        public string PostalCode { get; set; }

        public IEnumerable<State> StateList { get; set; }
        public IEnumerable<Market> MarketList { get; set; }

        public bool ShowValidationMessage { get; set; }
        public string ValidationMessage
        {
            get { return "<p class=\"field-validation-error errAlign nhs_Error\">" + LanguageHelper.NoBuildOnYourLot + "</p>"; }
        }
        public bool ShowCustomAdd { get; set; }
        public bool ShowStaticContentCol  { get; set; }
        public bool ShowFixedPartnerContent { get; set; }

        public IEnumerable<BoylLink> BoylBuilders { get; set; }
    }
}