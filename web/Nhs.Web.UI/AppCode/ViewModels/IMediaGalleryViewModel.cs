﻿using System.Collections.Generic;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public interface IMediaGalleryViewModel 
    {
        KeyValuePair<int, int> Size { get; set; }
        string FirstImage { get; set; }
        string PinterestDescription { get; set; }
        string PageUrl { get; set; }
        bool PreviewMode { get; set; }
        bool IsBasicCommunity { get; set; }
        bool IsCommunityResults { get; set; } 
    }

}
