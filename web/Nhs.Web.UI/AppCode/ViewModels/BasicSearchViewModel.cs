﻿
using System.Collections.Generic;
using System.Web.Mvc;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class BasicSearchViewModel : BaseViewModel
    {
        public SelectList  States;
        public string State;
        
        public SelectList Cities;
        public string City;

        public SelectList Areas;

        public int Area;

        public string Zip;
        
        public SelectList LowPriceRange;
        public SelectList HighPriceRange;
        public int PriceLow;
        public int PriceHigh;

        public IEnumerable<SpotLightCommunity> SpotLightCommunities; 
    }

}