﻿using System.Collections.Generic;
using System.Web.Mvc;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Web.UI.AppCode.Helper;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class SearchAlertViewModel
    {
        public class CreateAlertViewModel : BaseViewModel
        {
            public bool AlertSaved { get; set; }
            public string Location { get; set; }
            public string State { get; set; }
            public string Area { get; set; }
            public string City { get; set; }
            public string Zip { get; set; }
            public int Radius { get; set; }
            public string PriceFrom { get; set; }
            public string PriceTo { get; set; }
            public int Bedrooms { get; set; }
            public bool TypeSingle { get; set; }
            public bool Condo { get; set; }
            public bool AmenitiyPool { get; set; }
            public bool AmenitiyGolf { get; set; }
            public bool AmenitiyGated { get; set; }
            public string School { get; set; }
            public string Builder { get; set; }
            public string SuggestedName { get; set; }
            public bool FromRegistration { get; set; }
            public string SkipLink { get; set; }
            public string FromPage { get; set; }
            public bool OtherContent { get; set; }
            public SelectList StateList { get; set; }
            public IList<SelectListItem> AreaList { get; set; }
            public IList<SelectListItem> CityList { get; set; }
            public SelectList RadiusList { get; set; }
            public SelectList MinPriceList { get; set; }
            public SelectList MaxPriceList { get; set; }
            public SelectList BedroomsList { get; set; }
            public IList<SelectListItem> SchoolList { get; set; }
            public IList<SelectListItem> BuilderList { get; set; }
        }

        public class SavedSearchAlert : BaseViewModel
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public SearchAlert SelectedSearchAlert { get; set; }
            public string MarketName { get; set; }
            public string SearchTerms { get; set; }

            public IList<SelectListItem> SortOptionsList
            {
                get
                {

                    var sortOptions = new List<SelectListItem>
                    {
                        new SelectListItem() {Selected = true, Text = LanguageHelper.SortBy, Value = ""},
                        new SelectListItem() {Selected = false, Text = LanguageHelper.Location, Value = "location"},
                        new SelectListItem() {Selected = false, Text = LanguageHelper.PricedFrom, Value = "price"},
                        new SelectListItem()
                        {
                            Selected = false,
                            Text = LanguageHelper.HomeMatches,
                            Value = "homematches"
                        },
                        new SelectListItem() {Selected = false, Text = LanguageHelper.Builder, Value = "builder"},
                        new SelectListItem() {Selected = false, Text = LanguageHelper.CommunityName, Value = "name"},
                        new SelectListItem()
                        {
                            Selected = false,
                            Text = LanguageHelper.SpecialOffer.TrimStart('¡').TrimEnd('!'),
                            Value = "specialoffer"
                        }
                    };

                    return sortOptions;
                }
            } 
            public IEnumerable<SearchAlert> SearchAlerts { get; set; }
            public IEnumerable<Community> SelectedSearchAlertCommunities { get; set; } 
        }
    }
}
