﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class NearbyCommunityModel
    {
        public string BrandName { get; set; }
        public IEnumerable<NearbyCommunity> NearByComms { get; set; }
        public string NearByCommsObject { get; set; }
    }
}