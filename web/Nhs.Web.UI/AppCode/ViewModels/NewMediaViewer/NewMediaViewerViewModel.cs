﻿using System.Collections.Generic;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Routing.Configuration;

namespace Nhs.Web.UI.AppCode.ViewModels.NewMediaViewer
{
    public class NewMediaViewerViewModel : BaseViewModel
    {
        public NewMediaViewerViewModel()
        {
            BcType = string.Empty;
            MediaPlayerList = new List<MediaPlayerObject>();
        }

        public string FirstImage { get; set; }
        public string PinterestDescription { get; set; }
        public string PageUrl { get; set; }
        public bool IsAjaxRequest { get; set; }
        public string PropertyName { get; set; }
        public string PriceDisplay { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public bool SavedToPlanner { get; set; }
        public int CommunityId { get; set; }
        public int BuilderId { get; set; }
        public int MarketId { get; set; }
        public int PlanId { get; set; }
        public int SpecId { get; set; }
        public bool IsCommunityDetail { get; set; }
        public bool IsLoggedIn { get; set; }
        public List<MediaPlayerObject> MediaPlayerList { get; set; }
        public string BcType { get; set; }
        public string NextPage { get; set; }

        public string GetAddToPlannerUrl()
        {
            if (IsCommunityDetail)
            {
                var paramz = new List<RouteParam>
                {
                    new RouteParam(RouteParams.Community, CommunityId),
                    new RouteParam(RouteParams.Builder, BuilderId)
                };
                return string.Format(@"""{0}/{1}""", paramz.ToUrl(Pages.CommunityDetail),
                    NhsMvc.CommunityDetail.ActionNames.AddCommunityToUserPlanner);
            }
            else
            {
                IList<RouteParam> paramz = SpecId > 0 ?
                                    new List<RouteParam> { new RouteParam(RouteParams.SpecId, SpecId) } :
                                    new List<RouteParam> { new RouteParam(RouteParams.PlanId, PlanId) };

                return string.Format(@"""{0}/{1}""", paramz.ToUrl(Pages.HomeDetail), NhsMvc.HomeDetail.ActionNames.AddHomeToUserPlanner);
            }
        }

        public string GetDetailUrl()
        {
            if (IsCommunityDetail)
            {
                var paramz = new List<RouteParam>
                {
                    new RouteParam(RouteParams.Community, CommunityId),
                    new RouteParam(RouteParams.Builder, BuilderId)
                };
                return paramz.ToUrl(Pages.CommunityDetail);
            }
            else
            {
                IList<RouteParam> paramz = SpecId > 0 ?
                                    new List<RouteParam> { new RouteParam(RouteParams.SpecId, SpecId) } :
                                    new List<RouteParam> { new RouteParam(RouteParams.PlanId, PlanId) };

                return paramz.ToUrl(Pages.HomeDetail);
            }
        }
    }
}