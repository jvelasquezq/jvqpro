﻿using System;
using Nhs.Library.Common;
using Nhs.Utility.Common;
using Nhs.Library.Web;
using Nhs.Library.Constants;
using System.Web.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using Bankrate;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class MortgageViewModel : BaseViewModel
    {
        public String Function { get; set; }
        public String Name { get; set; }
        public String ImageUrl { get; set; }
        public String BuilderName { get; set; }
        public String MarketName { get; set; }
        public String CommunityName { get; set; }
        public String Address { get; set; }
        public Int32 SpecID { get; set; }
        public Int32 PlanID { get; set; }
        public Int32 MarketID { get; set; }
        public Decimal Price { get; set; }
        public Int32 HalfBaths { get; set; }
        public Int32 Baths { get; set; }
        public Int32 Bedrooms { get; set; }
        public String Garages { get; set; }
        public Boolean IsCommunity { get; set; }
        public Boolean IsPlan { get; set; }
        public Boolean IsSpec { get; set; }
        public Int32 BuilderID { get; set; }
        public Int32 CommunityID { get; set; }
        public String ProductID { get; set; }
        public String PointID { get; set; }
        public Double DownPay { get; set; }
        public String City { get; set; }
        public String CommunityCity { get; set; }
        public String UserPostalCode { get; set; }
        public String State { get; set; }
        public String StateName { get; set; }
        public String PostalCode { get; set; }
        public Decimal PriceLow { get; set; }
        public Decimal PriceHigh { get; set; }
        public Int32 SqFtLow { get; set; }
        public Int32 SqFtHigh { get; set; }
        public Boolean HasData { get; set; }
        public String BankRateTableSrc { get; set; }
        public String PLoanInfo { get; set; }
        public String PLoanMatches { get; set; }

        public MvcHtmlString GetPrice
        {
            get
            {
                return MvcHtmlString.Create(IsCommunity ? StringHelper.PrettyPrintRange(Convert.ToDouble(PriceLow), Convert.ToDouble(PriceHigh), "c0").Replace("From", "").Trim()
                                   : "From " + string.Format("{0:c0}", Price));
            }
        }
        public MvcHtmlString GetSqFtDisplay
        {
            get
            {
                var result = string.Empty;

                if (SqFtLow != 0 && SqFtHigh != 0)
                    result = (SqFtLow == SqFtHigh) ? string.Format("{0} sq.ft.", SqFtHigh) : string.Format("{0} - {1} sq.ft.", SqFtLow, SqFtHigh);

                return MvcHtmlString.Create(result);

            }

            //if (sqFtLow == 0 && sqFtHigh == 0) return string.Empty;

            //if ( sqFtLow == sqFtHigh) return string.Format("({0} sq.ft.)", sqFtHigh);

            //return string.Format("({0} - {1} sq.ft.)", sqFtLow, sqFtHigh);
        }
        public MvcHtmlString GetBaths { get { return MvcHtmlString.Create(string.Format(@"<strong>{0}<abbr title=""bathrooms"">ba</abbr></strong> ", XGlobals.ComputeBathrooms(Baths, HalfBaths))); } }
        public MvcHtmlString GetBedrooms { get { return MvcHtmlString.Create(string.Format(@"<strong>{0}</strong> ", XGlobals.FormatBedrooms(Bedrooms, true))); } }
        public MvcHtmlString GetGarages { get { return MvcHtmlString.Create(string.Format(@"<strong>{0}<abbr title=""garage"">gr</abbr></strong > ", Garages)); } }
        public String IframeSRC { get; set; }
        public String GetImage
        {
            get
            {
                return Configuration.ResourceDomain + (!String.IsNullOrEmpty(ImageUrl) && ImageUrl.IndexOf("1x1.gif") == -1 ? ImageUrl : "/globalresources/default/images/small_nophoto.gif");
            }
        }
        public MvcHtmlString SetURLIframe()
        {
            int width = 970;
            int height = 1200;
            if (!String.IsNullOrEmpty(IframeSRC))
            {
                string iframe = String.Format("<iframe src=\"{0}\" width=\"{1}\" height=\"{2}\" class=\"{3}\" frameborder=\"0\"></iframe>", IframeSRC, width, height, "iframemortgagetable");
                return MvcHtmlString.Create(iframe);
            }
            else
                return MvcHtmlString.Create("<span style=\"color:Red;\">Sorry, there is not data available.</span>");
        }
    }

    public class MortgageRatesViewModel : MortgageViewModel
    {
        public MortgageRatesViewModel(MortgageViewModel model = null)
        {
            RadioButonList = new Dictionary<String, Object>();
            RadioButonList.Add("30 Year Fixed", 1);
            RadioButonList.Add("15 Year Fixed", 2);
            RadioButonList.Add("5 Year ARM (adjustable rate)", 6);
            RadioButonList.Add("Interest Only 5 Year ARM", 390);

            SelectOtherLoanTypesItems = new List<SelectListItem>();
            SelectOtherLoanTypesItems.Add(new SelectListItem { Value = "other", Selected = true, Text = "Other loan types..." });
            SelectOtherLoanTypesItems.Add(new SelectListItem { Value = "387", Text = "Fixed - 20 year" });
            SelectOtherLoanTypesItems.Add(new SelectListItem { Value = "8", Text = "ARM 3/1" });
            SelectOtherLoanTypesItems.Add(new SelectListItem { Value = "9", Text = "ARM 7/1" });
            SelectOtherLoanTypesItems.Add(new SelectListItem { Value = "10", Text = "ARM 10/1" });
            SelectOtherLoanTypesItems.Add(new SelectListItem { Value = "389", Text = "Interest Only ARM 3/1" });
            SelectOtherLoanTypesItems.Add(new SelectListItem { Value = "447", Text = "Interest Only ARM 7/1" });
            SelectOtherLoanTypesItems.Add(new SelectListItem { Value = "565", Text = "Interest Only Fixed 30-year" });

            ddlPointsItems = new List<SelectListItem>();
            ddlPointsItems.Add(new SelectListItem { Value = "1", Selected = true, Text = "0 Points" });
            ddlPointsItems.Add(new SelectListItem { Value = "2", Text = "0.1 to 1.0 Point" });
            ddlPointsItems.Add(new SelectListItem { Value = "3", Text = "1.1 to 2.0 Points" });
            ddlPointsItems.Add(new SelectListItem { Value = "6", Text = "All" });

            if (model != null)
            {
                Function = model.Function;
                Name = model.Name;
                ImageUrl = model.ImageUrl;
                BuilderName = model.BuilderName;
                SpecID = model.SpecID;
                PlanID = model.PlanID;
                MarketID = model.MarketID;
                Price = model.Price;
                HalfBaths = model.HalfBaths;
                Baths = model.Baths;
                Bedrooms = model.Bedrooms;
                Garages = model.Garages;
                IsCommunity = model.IsCommunity;
                IsPlan = model.IsPlan;
                IsSpec = model.IsSpec;
                BuilderID = model.BuilderID;
                CommunityID = model.CommunityID;
                City = model.City;
                State = model.State;
                PostalCode = model.PostalCode;
                PriceLow = model.PriceLow;
                PriceHigh = model.PriceHigh;
                PLoanInfo = model.PLoanInfo;
                PLoanMatches = model.PLoanMatches;
                BankRateTableSrc = model.BankRateTableSrc;
            }
        }

        public Dictionary<String, Object> RadioButonList { get; set; }
        public List<SelectListItem> SelectOtherLoanTypesItems { get; set; }
        public List<SelectListItem> ddlPointsItems { get; set; }



    }

    public class MortgageCalculatorViewModel : BaseViewModel
    {
        public string MortgageScriptUrl { get; set; }
    }
}
