﻿using System;
using System.Collections.Generic;
using Nhs.Mvc.Domain.Model.CMS;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    interface IRCArticleViewModel
    {
        string ArticleSearch { get; set; }
        bool IsArticlesPage { get; set; }
        bool IsCategoryPage { get; set; }
        bool IsResourceGuidePage { get; set; }
        string MktSearch { get; set; }
        List<RCCategory> ParentCategories { get; set; }
        RCSlideShow SlideShow { get; set; }
        List<RcArticle> TrendingNow { get; set; }
        string TypeAheadUrl { get;}
        RcArticle Article { get; set; }
        RcAuthor Autor { get; set; }
        IEnumerable<RcArticle> RelatedArticles { get; set; }

        CmsProSearch CmsProSearchParams { get; set; }
    }
}
