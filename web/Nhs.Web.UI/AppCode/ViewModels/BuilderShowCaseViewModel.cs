﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Business.Map;
using Nhs.Mvc.Domain.Model.BhiTransaction;
using Nhs.Mvc.Domain.Model.Hub;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Search.Objects;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class BuilderShowCaseViewModel : BaseViewModel
    {
        public int BrandId { get; set; }
        public string BrandUrl { get; set; }
        public string BrandName { get; set; }
        public string BrandLogo { get; set; }
        public bool IsAbout { get; set; }
        public bool IsLocations { get; set; }
        public bool IsGallery { get; set; }
        public bool IsOwnerStories { get; set; }

        public bool IsSingleMarketBrand { get; set; }
        public List<Market> Markets { get; set; }
        public int MarketId { get; set; }       

        public string PromotionalText { get; set; }       
        public string OverviewText { get; set; }

        public string Comments { get; set; }
        public string Name { get; set; }
        public string MailAddress { get; set; }
        public bool SendLedForm { get; set; }
        public bool IsSubmitYourOwnVisible { get; set; }
        public string BrandEmailAddress { get; set; }
        public ICollection<BrandTestimonial> Testimonials { get; set; }
        public ICollection<OwnerStory> OwnerStories { get; set; }
        public IEnumerable<IImage> AwardImages { get; set; }
        public IEnumerable<IImage> AboutGalleryImages { get; set; }
        public List<Video> ShowcaseVideos { get; set; }
        public List<OwnerStoryMediaObject> OwnerStoryMediaObjects { get; set; }
        public int OwnerStoryMediaObjectsTotalRows { get; set; }


        public IVideo Video { get; set; }
        public int ImagesGalleryIndex { get; set; }
        public string RssUrl { get; set; }
        public string TwitterWidget { get; set; }
        public string FacebookWidget { get; set; }

        public MapData Map { get; set; }

        public string GetBrandUrl
        {
            get
            {
                if (string.IsNullOrEmpty(BrandUrl))
                    return string.Empty;
                return BrandUrl.StartsWith("http") || BrandUrl.StartsWith("https") ? BrandUrl : "http://" + BrandUrl; }
        }

        public BuilderShowCaseMetroAreaList MetroAreaList { get; set; }

        public IEnumerable<Community> Communities { get; set; }

        public IEnumerable<ExtendedCommunityResult> SpotlightsCarousel { get; set; } 
    }

    public class BuilderShowCaseFormViewModel : BaseViewModel
    {
        //public BuilderShowCaseFormViewModel()
        //{
        //    //Testimonials = new BuilderShowCaseFormTestimonialViewModel();
        //}

        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public string EmailAddress { get; set; }
        public string WebsiteUrl { get; set; }
        [Required]
        public string PromotionalText { get; set; }
        public string RssUrl { get; set; }
        public string FacebookUrl { get; set; }
        public string FacebookPlugin { get; set; }
        
        public string TwitterEmbed { get; set; }
        public string Overview { get; set; }
        public SelectList Brands { get; set; }
        public IEnumerable<HubBrand> BrandList { get; set; }
        public bool IsEdit { get; set; }
        public IList<BuilderShowCaseFormTestimonial> Testimonials { get; set; }
    }

    public class BuilderShowCaseFormTestimonial
    {
        public BuilderShowCaseFormTestimonial()
        {
            Citation = "";
            Description = "";
        }

        public int TestimonialId { get; set; }
        public int BrandId { get; set; }
        [StringLength(100)]
        public string Citation { get; set; }
        [StringLength(550)]
        public string Description { get; set; }
    }

    public class BuilderShowCaseMetroAreaList
    {
        public bool HasOne { get; set; }
        public string HeaderText { get; set; }
        public IEnumerable<BuilderShowCaseMetroArea> MetroAreas { get; set; } 
    }

    public class BuilderShowCaseMetroArea
    {
        public Market Market { get; set; }
        public IEnumerable<Community> Communities { get; set; }
        public int BrandId { get; set; }
    }

}
