﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Business.Map;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Content;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects.Constants;
using Nhs.Utility.Common;
using Nhs.Utility.Data.Paging;
using Nhs.Web.UI.AppCode.Extensions;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public abstract class CommunityDetailBaseViewModel : BaseViewModel, ISecondaryInfoViewModel
    {
        public string CorporationName { get; set; }
        public bool NewSearchRCC { get; set; }
        public bool ShowUserPhoneNumber { get; set; }
        public bool IsBilled { get; set; }
        public bool IsPlan { get; set; }
        public bool IsPageCommDetail { get; set; }
        public int MarketId { get; set; }
        public int CommunityId { get; set; }
        public int BuilderId { get; set; }        
        public int PlanId { get; set; }
        public int SpecId { get; set; }
        public int BrandId { get; set; }
        public string CommunityName { get; set; }
        public string MarketName { get; set; }
        public string BuilderName { get; set; }
        public bool ShowBrochure { get; set; }
        public string PopUpViewerName { get; set; }
        public string CommunityUrl { get; set; }
        public string SearchParametersJSon { get; set; }
        public IList<School> Schools { get; set; }
        public IDictionary<AmenityGroup, IList<IAmenity>> Amenities { get; set; }
        public ICollection<Mvc.Domain.Model.Web.Utility> Utilities { get; set; }
        public string AgentCompensation { get; set; }
        public string AgentPolicyLink { get; set; }
        public string AgentCompensationAdditionalComments { get; set; }
        public string AgentCommissionBasis { get; set; }
        public string AgentCommissionPayoutTiming { get; set; }
        public string BuilderUrl { get; set; }
        public string CommunityCity { get; set; }
        public string CommunityDescription { get; set; }
        public string DetailDescription { get { return CommunityDescription; } }
        public ICollection<Promotion> Promotions { get; set; }
        public ICollection<ExtendedHomeResult> HotHomes { get; set; }
        public ICollection<GreenProgram> GreenPrograms { get; set; }

        public IList<IImage> AwardImages { get; set; }
        public IList<Testimonial> Testimonials { get; set; }

        public string BcType { get; set; }

        public bool PreviewMode { get; set; }

        public bool HasCoop { get; set; }
        public bool HasBuilderPactLink { get; set; }
        public string BuilderPackLink { get; set; }

    }

    public class CommunityDetailViewModel : CommunityDetailBaseViewModel, IDetailViewModel, IPropertyMapViewModel
    {
        public CommunityDetailViewModel()
        {
            Promotions= new List<Promotion>();
            GreenPrograms = new List<GreenProgram>();
            HotHomesApi = new List<HomeItem>();
            Amenities = new Dictionary<AmenityGroup, IList<IAmenity>>();
        }

        public bool UserAlreadyExists { get; set; }
        public bool SavedToPlanner { get; set; }
        public string HomeTitle { get; set; }
        public IEnumerable<PlannerListing> SavedPropertysProCrm { get; set; }
        public string RequestUniqueKey { get; set; }
        public bool HasShowCaseInformation { get; set; }
        public BoylResult CnhBoyl { get; set; }
        public bool IsInactive { get; set; }
        public bool RequestAnAppointment { get; set; }
        public bool HideDrivingDirections { get; set; }

        public string DetailType
        {
            get
            {
                return LanguageHelper.Community;
            }
        }

        public string NonPdfBrochureUrl { get; set; }
        public int TotalComunities { get; set; }
        public int HasHotHomes { get; set; }
        public bool BcTypeBanner { get; set; }
        public bool IsPhoneNumberVisible { get; set; }
        public bool CommunityExistsOnUserProfile { get; set; }
        public bool ShowNewHomesExtraTab { get; set; }
        public bool ShowPlaceHolderInputText { get; set; }
        public bool IsBilled { get; set;}
        public bool ShowSocialIcons { get; set; }

        #region IDetailViewModel Interface
        public string Name { get; set; }
        public string BasicHomeSpecs { get; set; }
        public int HomeStatus { get; set; }
        public string MailAddress { get; set; }
        public string UserPostalCode { get; set; }
        public string UserPhoneNumber { get; set; }
        public string Comments { get; set; }
        public string Message { get; set; }
        public int PlanId { get; set; }
        public int SpecId { get; set; }

        public string StateAbbr { get; set; }
        public bool EnableRequestAppointment { get; set; }
        public bool IsPlan { get; set; }
        public bool LiveOutside { get; set; }
        public bool GeneralInquiry { get; set; }
        public bool ScheduleAppointment { get; set; }
        public bool SendAlerts { get; set; }
        public string BuilderName { get; set; }
        public string PriceDisplay { get; set; }

        public string PlanName { get; set; }

        public string BrandThumbnail { get; set; }
        public string HomesAvailable { get; set; }
        public string DisplaySqFeet { get; set; }
        public string VirtualTourUrl { get; set; }
        public string BathroomRange { get; set; }
        public string BedroomRange { get; set; }
        public string GarageRange { get; set; }

        public List<RouteParam> BrochureParams
        {
            get
            {
                var paramz = new List<RouteParam>
                    {
                        new RouteParam(RouteParams.Email, MailAddress),
                        new RouteParam(RouteParams.Refer, UserSession.Refer),
                        new RouteParam(RouteParams.MarketId, MarketId.ToString()),
                        new RouteParam(RouteParams.LeadType, LeadType.Community)
                    };
                if (CommunityId > 0 && BuilderId > 0)
                {
                    paramz.Add(new RouteParam(RouteParams.Community, CommunityId.ToString()));
                    paramz.Add(new RouteParam(RouteParams.Builder, BuilderId.ToString()));
                }


                return paramz;
            }
        }

        public List<RouteParam> ExpiredBrochureParams
        {
            get
            {
                var paramz = new List<RouteParam>();
                paramz.Add(new RouteParam(RouteParams.Email, MailAddress, RouteParamType.QueryString));
                paramz.Add(new RouteParam(RouteParams.Refer, UserSession.Refer, RouteParamType.QueryString));

                if (CommunityId > 0 && BuilderId > 0)
                {
                    paramz.Add(new RouteParam(RouteParams.Community, CommunityId.ToString(), RouteParamType.QueryString));
                    paramz.Add(new RouteParam(RouteParams.CommunityList, CommunityId.ToString(), RouteParamType.QueryString));
                    paramz.Add(new RouteParam(RouteParams.Builder, BuilderId.ToString(), RouteParamType.QueryString));
                }

                paramz.Add(new RouteParam(RouteParams.LeadType, LeadType.Community, RouteParamType.QueryString));
                paramz.Add(new RouteParam(RouteParams.FromPage, Pages.SavedHomes, RouteParamType.QueryString));
                paramz.Add(new RouteParam(RouteParams.LeadAction, LeadAction.FreeBrochure, RouteParamType.QueryString));

                return paramz;
            }
        }

        public string MortgagePage
        {
            get
            {
                var paramz = new List<RouteParam>();
                paramz.Add(new RouteParam(RouteParams.Community, CommunityId));
                return paramz.ToUrl(Pages.MortgageTable).ToLower();
            }
        }

        public string CreditScoreLink1 { get; set; }

        public string CreditScoreLink2 { get; set; }

        public string CommunityDetailPage
        {
            get
            {
                return this.ToCommunityDetail().ToUrl(RedirectionHelper.GetCommunityDetailPage()).ToLower();
            }
        }

        
        public string BathroomsAbbr { get; set; }
        public string BedroomsAbbr { get; set; }
        public string GaragesAbbr { get; set; }
        public string PhoneNumber { get; set; }
        public string TrackingPhoneNumber { get; set; }
        public string HoursOfOperation { get; set; }
        public bool ShowMortgageLink { get; set; }
        public string LogActionMethodUrl { get; set; }
        public bool ShowLeadForm { get; set; }
        public bool ShowSaveToAccount { get; set; }
        public bool ShowExpiredRequestBrochureLink { get; set; }
        public bool ShowRegInfoDetail { get; set; }
        public List<string> CommunityStyles { get; set; }
        public string BrochureUrl { get; set; }
        public IList<Testimonial> Testimonials { get; set; }
        // Details media viewer
        public IList<MediaPlayerObject> ExternalMediaLinks { get; set; }
        public IList<MediaPlayerObject> PropertyMediaLinks { get; set; }

        // New Player Popup
        public IList<MediaPlayerObject> PlayerMediaObjects { get; set; }

        public bool IsBasicCommunity { get; set; }
        public bool IsCommunityResults { get; set; }

        public bool ShowHotHomeSection { get; set; }
        public IEnumerable<SelectListItem> States { get; set; }
        public string SelectedStateAbbr { get; set; }
        public string SearchText { get; set; }
        public string PriceHigh { get; set; }
        public string PriceLow { get; set; }
        public IEnumerable<SelectListItem> PriceLowRange { get; set; }
        public IEnumerable<SelectListItem> PriceHighRange { get; set; }
        public string EnvisionUrl { get; set; }
        public bool PartnerUsesMatchmaker { get; set; }
        public string ConversionIFrameSource { get; set; }
        public int LeadSectionState { get; set; }

        #region IDetailViewModel

        public bool IsLoggedInPaid
        {
            get
            {
                return (LeadSectionState == LeadSectionStateConst.LoggedInPaid.ToType<int>());
            }

        }

        public bool IsLoggedInUnpaid
        {
            get
            {
                return (LeadSectionState == LeadSectionStateConst.LoggedInUnpaid.ToType<int>());
            }

        }

        public bool IsLoggedOutPaid
        {
            get
            {
                return (LeadSectionState == LeadSectionStateConst.LoggedOutPaid.ToType<int>());
            }

        }

        public bool IsLoggedOutUnpaid
        {
            get
            {
                return (LeadSectionState == LeadSectionStateConst.LoggedOutUnpaid.ToType<int>());
            }

        }

        #endregion


        #region IMediaGalleryViewModel

        public KeyValuePair<int, int> Size { get; set; }
        public string FirstImage { get; set; }
        public string PinterestDescription { get; set; }
        public string PageUrl { get; set; }

        #endregion

        #endregion

        public string CreditScoreText { get; set; }
        public bool ShowCreditScoreSection { get; set; }

        public bool ExcludesLand { get; set; }
        public string BrandName { get; set; }
        public string MarketName { get; set; }
        public string StateName { get; set; }
        
        public string BuilderMapUrl { get; set; }
        public string UnderneathMapCommName { get; set; }
        public string UnderneathMapAddress { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double RouteLat { get; set; }
        public double RouteLng { get; set; }
        public int PropertyId { get; set; }
        public string PropertyName { get; set; }

        public bool AllowModals
        {
            get
            {
                if (Globals != null && Globals.PartnerLayoutConfig != null)
                    return Globals.PartnerLayoutConfig.PartnerAllowsModals;
                else
                    return false;
            }
        }

        public string DrivingDirections { get; set; }

        public string DrivingDirectionsShort
        {
            get
            {
                if (!string.IsNullOrEmpty(DrivingDirections) && DrivingDirections.Length > 39)
                    return DrivingDirections.Substring(0, 39) + "...";
                else
                    return DrivingDirections;
            }
        }

        public DrivingDirections DrivingDirectionsList { get; set; }
        public List<NearbyCommunity> NearByComms { get; set; }
        public string NearByCommsObject { get; set; }
        public IList<NearbyCommunity> NearByCommsSmall { get; set; }
        public string SaveCommunitySuccessMessage { get; set; }
        public SortOrder CurrentSortOrder { get; set; }
        public string SortOptionSelected {
            get {return CurrentSortOrder.ToType<string>(); }
        }
        public IPagedList<ExtendedHomeResult> HomeResults { get; set; }
        public string AddCommunityToPlannerUrl { get; set; }

        public IEnumerable<SelectListItem> SortOptionList
        {
            get
            {
                return new List<SelectListItem> {
                    new SelectListItem {Text = LanguageHelper.Price, Value = SortOrder.Price.ToType<string>()},
                    new SelectListItem {Text = LanguageHelper.StatusAvailability, Value = SortOrder.Status.ToType<string>(), Selected = true},
                    new SelectListItem {Text = LanguageHelper.BedAndBathAndGarage, Value =SortOrder.Size.ToType<string>()},
                    new SelectListItem {Text = LanguageHelper.PlanName, Value = SortOrder.Name.ToType<string>()}};
            }
        }
        public ICollection<Event> Events { get; set; }

        public ICollection<Video> Videos { get; set; }
        public CommunityDetailTabs SelectedTab { get; set; }

        // Result counts
        public int FilteredHomesCount { get; set; }
        public int QuickMoveInCount { get; set; }
        public int AllNewHomesCount { get; set; }
        public string PageLinkAction { get; set; }
        public string BuilderLogo { get; set; }
        public string BuilderLogoSmall { get; set; }

        public string SalesOfficeAddress1 { get; set; }
        public string SalesOfficeAddress2 { get; set; }
        public string HomeAddress1 { get; set; }
        public string HomeAddress2 { get; set; }
        public string ZipCode { get; set; }
        public string State { get; set; }
        public IList<IImage> AwardImages { get; set; }
        public string HomeSearchViewMetricDisplayed { get; set; }
        public string HomeSearchViewMetricHidden { get; set; }
        public string SaleAgents { get; set; }

        public bool IsAdult { get; set; }
        public bool IsTownHome { get; set; }
        public bool IsCondo { get; set; }
        public bool IsGreenProgram { get; set; }
        public bool IsGreen { get; set; }
        public bool IsGated { get; set; }
        public bool IsAgeRestricted { get; set; }
        public bool IsMasterPlanned { get; set; }
        public bool NewUser { get; set; }
        public int TotalPages { get; set; }
        public bool IsTriggerByTab { get; set; }
        public bool IsCommunityMultiFamily { get; set; }

        public IEnumerable<HomeItem> HomeResultsApi { get; set; }
        public ICollection<HomeItem> HotHomesApi { get; set; }
        public IEnumerable<IGrouping<string, HomeItem>> HomeResultsApiV2 { get; set; }
        public bool HasCoop { get; set; }
    }

    public class HomeResultViewModel : CommunityDetailBaseViewModel
    {
        public ExtendedHomeResult HomeResultRow { get; set; }
        public HomeResultViewModel(ExtendedHomeResult homeResult, int marketId, bool previewMode, string bcType, string marketName)
        {
            HomeResultRow = homeResult;
            HomeResultRow.MarketName = marketName;
            MarketId = marketId;
            PreviewMode = previewMode;
            BcType = bcType;
        }
    }

    public class HomeResultApiViewModel : CommunityDetailBaseViewModel
    {
        public HomeItem HomeResultRow { get; set; }

        public HomeResultApiViewModel(HomeItem homeResult, int marketId, bool previewMode, string bcType)
        {
            HomeResultRow = homeResult;
            MarketId = marketId;
            PreviewMode = previewMode;
            BcType = bcType;
            
        }
    }

    public class AmenitiesViewModel : CommunityDetailBaseViewModel
    {
        public string AmenityGroupHeader { get; set; }
        public IList<IAmenity> OpenAmenitiesByGroup { get; set; }
        public IList<IAmenity> CommAmenitiesByGroup { get; set; }
        public bool DisplayHeader { get; set; } //Displays header regardless of amenities/open amenities' availability

        public AmenitiesViewModel(IDictionary<AmenityGroup, IList<IAmenity>> amenities, AmenityGroupType amenityGroupType)
        {
            OpenAmenitiesByGroup = new List<IAmenity>();
            CommAmenitiesByGroup = new List<IAmenity>();

            switch (amenityGroupType)
            {
                case AmenityGroupType.HealthAndFitness:
                    OpenAmenitiesByGroup = amenities.GetAmenitiesByType(AmenityGroup.HealthAndFitnessOpenAmenities);
                    CommAmenitiesByGroup = amenities.GetAmenitiesByType(AmenityGroup.HealthAndFitnessCommAmenities);
                    AmenityGroupHeader =  LanguageHelper.HealthAndFitness;
                    DisplayHeader = true;
                    break;

                case AmenityGroupType.Educational:
                    OpenAmenitiesByGroup = amenities.GetAmenitiesByType(AmenityGroup.EducationalOpenAmenities);
                    AmenityGroupHeader = LanguageHelper.Educational;
                    DisplayHeader = true;
                    break;

                case AmenityGroupType.LocalArea:
                    OpenAmenitiesByGroup = amenities.GetAmenitiesByType(AmenityGroup.LocalAreaOpenAmenities);
                    CommAmenitiesByGroup = amenities.GetAmenitiesByType(AmenityGroup.LocalAreaCommAmenities);
                    AmenityGroupHeader =  LanguageHelper.LocalAreaAmenities;
                    DisplayHeader = true;
                    break;

                case AmenityGroupType.CommunityServices:
                    OpenAmenitiesByGroup = amenities.GetAmenitiesByType(AmenityGroup.CommunityServicesOpenAmenities);
                    CommAmenitiesByGroup = amenities.GetAmenitiesByType(AmenityGroup.CommunityServicesCommAmenities);
                    AmenityGroupHeader =  LanguageHelper.CommunityServices;
                    DisplayHeader = true;
                    break;

                case AmenityGroupType.SocialActivities:
                    OpenAmenitiesByGroup = amenities.GetAmenitiesByType(AmenityGroup.SocialActivitiesOpenAmenities);
                    CommAmenitiesByGroup = amenities.GetAmenitiesByType(AmenityGroup.SocialActivitiesCommAmenities);
                    AmenityGroupHeader = LanguageHelper.SocialActivities;
                    DisplayHeader = true;
                    break;
            }

            OpenAmenitiesByGroup = OpenAmenitiesByGroup ?? new List<IAmenity>();
            CommAmenitiesByGroup = CommAmenitiesByGroup ?? new List<IAmenity>();

            if (!string.IsNullOrEmpty(AmenityGroupHeader))
                AmenityGroupHeader = string.Format(@"<h4 class=""titles"">{0}</h4>", AmenityGroupHeader);
        }

        public AmenitiesViewModel()
        {
        }
    }
}
