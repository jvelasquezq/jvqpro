﻿using System.Collections.Generic;
using Nhs.Library.Constants;
using Nhs.Library.Web;
using Nhs.Library.Enums;
using Nhs.Mvc.Domain.Model.Web;
using System.Web.Mvc;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Web.UI.AppCode.Helper;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class StateAmenityViewModel : BaseStateIndexViewModel
    {
        public StateAmenityViewModel()
        {
            ShowCondoTownLink = true;
            ShowGolfCourseLink = true;
            ShowWaterFrontLink = true;
        }

        public string PartnerName { get; set; }
        public string ContentFilePath { get; set; }
        public string ContentFilePathFooter { get; set; }
        
       
        public string StateAbbreviation { get; set; }
        public string AmenityDesc { get; set; }
        public string AmenityDesc2 { get; set; }
        public string AmenityMarket { get; set; }
        public string AmenityName { get; set; }
        public bool ShowCondoTownLink { get; set; }
        public bool ShowGolfCourseLink { get; set; }
        public bool ShowWaterFrontLink { get; set; }

       
        public List<List<Market>> LearnMoreMarket { get; set; }
        public int SelectedMarketId;
        public List<Market> AreaList { get; set; }
        public SelectList LowPriceRange;
        public SelectList HighPriceRange;
        public string MapImageUlr { get; set; }

        public List<RouteParam> GetParams(int marketId)
        {
            var lista = RedirectionHelper.ToResultsParams(marketId);
            if (string.IsNullOrEmpty(AmenityName)) return lista;
            RouteParams name; 
            switch (AmenityName)
            {
                case SEOAmenityType.StateCondoTown:
                    name = RouteParams.TownHomes;
                    break;
                case SEOAmenityType.StateGolf:
                    name = RouteParams.GolfCourse;
                    break;
                default:
                    name = RouteParams.WaterFront;
                    break;

            }
            lista.Add(new RouteParam(name, "true",RouteParamType.QueryString));

            return lista;
        }

        public string MapNameAmenity
        {
            get
            {
                string name;
                switch (AmenityName)
                {
                    case SEOAmenityType.StateCondoTown:
                        name = "townhomes";
                        break;
                    case SEOAmenityType.StateGolf:
                        name = "golfcourse";
                        break;
                    default:
                        name = "waterfront";
                        break;

                }
                return name;
            }
        }

        public string SeoH1Content { get; set; }
    }
}
