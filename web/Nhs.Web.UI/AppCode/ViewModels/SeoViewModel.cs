﻿using System;
using System.Collections.Generic;
using Nhs.Library.Constants;
using Nhs.Mvc.Domain.Model.Web;
using System.Web.Mvc;
using Nhs.Library.Web;
using Nhs.Library.Enums;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Utility.Common;
using Nhs.Mvc.Routing.Interface;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class SeoViewModel : BaseViewModel
    {
        public IList<Brand> Brands { get; set; }
        public Int32 TotalCommunities { get; set; }
        public Int32 TotalHomes { get; set; }
        public String MarketName { get; set; }
        public String State { get; set; }
        public Int32 MarketId { get; set; }
        public String UtmCtr { get; set; }
        public String City { get; set; }
        public Boolean IsMove {
            get { return false; }
        }

        public MvcHtmlString TotalCommunitiesText
        {
            get { return MvcHtmlString.Create(String.Format("{0} communities", TotalCommunities)); }
        }

        public MvcHtmlString TotalHomesText
        {
            get { return MvcHtmlString.Create(String.Format("{0} homes", TotalHomes)); }
        }

        public MvcHtmlString Keywords
        {
            get { return MvcHtmlString.Create((!String.IsNullOrEmpty(UtmCtr.Trim()) ? UtmCtr : String.Format("\"{0}, {1}\"", !String.IsNullOrEmpty(City) ? City.ToTitleCase() : MarketName.ToTitleCase(), State.ToTitleCase()))); }
        }

        public List<RouteParam> SearchLinkParams
        {
            get
            {
                var param = new List<RouteParam>();
                if (MarketId > 0)
                {
                    param.Add(new RouteParam(RouteParams.Market, MarketId.ToString(), RouteParamType.Friendly, true, true));

                    if (!string.IsNullOrEmpty(City))
                        param.Add(new RouteParam(RouteParams.City, City, RouteParamType.Friendly, true, true));

                }
                return param;
            }
        }

        public string SearchPageFunction
        {
            get
            {
                return MarketId > 0 ? Pages.CommunityResults : Pages.Home;
            }
        }
    }
}