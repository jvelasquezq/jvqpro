﻿using System;
using System.Collections.Generic;
using Nhs.Library.Business.Map;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class MediaPlayerViewModel : BaseViewModel
    {
        public string CommunityName { get; set; }
        public string PriceFormated { get; set; }
        public string Address { get; set; }
        public string PinterestDescription { get; set; }
        public bool PreviewMode { get; set; }

        public int CommunityId { get; set; }
        
        public List<MediaPlayerObject> MediaObjects { get; set; }
    }

}
