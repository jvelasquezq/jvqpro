﻿using System.Collections.Generic;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public interface IDetailPromotionsViewModel
    {
        ICollection<GreenProgram> GreenPrograms { get; set; }
        ICollection<Promotion> Promotions { get; set; }
        ICollection<Event> Events { get; set; }
        ICollection<HomeItem> HotHomesApi { get; set; }
    }
}
