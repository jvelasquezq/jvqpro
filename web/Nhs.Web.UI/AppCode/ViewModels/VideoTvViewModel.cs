﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nhs.Library.Web;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class NhsTvViewModel : BaseViewModel
    {
        public IEnumerable<string> NearbyAreas { get; set; }
        public List<ContentTag> SeoContentTags { get; set; }
        public string SearchText{get; set; }
        public string TypeAheadUrl { get; set; }

        public NhsTvViewModel()
        {

        }

    }
}
