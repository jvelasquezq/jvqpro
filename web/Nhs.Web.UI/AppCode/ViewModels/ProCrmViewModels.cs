﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Text;
using System.Web.Mvc;
using System.Xml.Serialization;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Profiles;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class ProCrmViewModel : BaseViewModel
    {
        public ProCrmViewModel()
        {
            ClientInfo = new AgentClient();
            ClientFavorites = new FavoritesViewModel();
        }

        public ProCrmClientsStatusType ClientsStatus
        {
            get { return UserSession.ProCrmClientsStatus; }
        }
        public AgentClient ClientInfo { get; set; }
        public FavoritesViewModel ClientFavorites { get; set; }
    }

    public class ActivityInformation
    {
        public int ClientId { get; set; }
        public int ClientTaskId { get; set; }
        public string TaskType { get; set; }

        public string TaskDateString
        {
            get { return TaskDate.ToString("g"); }
        }

        public DateTime TaskDate { get; set; }
        public string Description { get; set; }
        public IEnumerable<ActivityListingInformation> PropertyList { get; set; }
    }

    public class ActivityListingInformation
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
        public int CommunityId { get; set; }
        public string CommunityName { get; set; }
    }

    public class ClientInformation
    {
        public int AccountStatus { get; set; }
        public int ClientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RelatedFirstName { get; set; }
        public string RelatedLastName { get; set; }
        public DateTime DateLastChanged { get; set; }

        public string FullName
        {
            get { return string.Format("{0} {1}", FirstName, LastName); }
        }

        public string DateLastChangedString
        {
            get { return DateLastChanged.ToShortDateString(); }
        }

        public bool HaveTasks
        {
            get { return ClientTaskId1 > 0 || ClientTaskId2 > 0; }
        }

        public int ClientTaskId1 { get; set; }
        public int ClientTaskId2 { get; set; }
        public string ClientTaskDescription1 { get; set; }
        public string ClientTaskDescription2 { get; set; }
    }

    public class ClientTaskViewModel : ClientTask
    {
        public ClientTaskViewModel()
        {
            ListingList = new List<ListingTaskViewModel>();
            DueAmPm = string.Empty;
            DueDate = string.Empty;            
        }

        public IEnumerable<string> TaskHours
        {
            get
            {
                for (int i = 1; i <= 12; i++)
                {
                    var ret = i < 10 ? "0" + i : i.ToString(CultureInfo.InvariantCulture);

                    yield return ret;
                }
            }
        }
        public IEnumerable<string> TaskMinutes
        {
            get
            {
                for (int i = 0; i <= 59; i++)
                {
                    var ret = i < 10 ? "0" + i : i.ToString(CultureInfo.InvariantCulture);

                    yield return ret;
                }
            }
        }

        public List<ListingTaskViewModel> ListingList { get; set; }

        [Required]
        public string DueAmPm { get; set; }

        [Required]
        public string DueDate { get; set; }

        [Required]
        public string DueTimeHour { get; set; }

        [Required]
        public string DueTimeMinutes { get; set; }

        [Required]
        public TimeSpan? DueTimeSpan { get; set; }

        public ProCrmTaskTypes ProCrmTaskTypes { get; set; }

        [Required]
        public override string Description
        {
            get { return base.Description; }
            set { base.Description = value; }
        }

    }

    public class ListingTaskViewModel
    {
        public int Id { get; set; }
        public int TaskListingId { get; set; }
        public string Name { get; set; }
        public bool Delete { get; set; }
    }

    public class DetailsClientInfo
    {
        public DetailsClientInfo()
        {
            EmailList = new List<EmailList>();
            Status = 1;
        }

        [Required]
        [Display(Name = "* First Name:")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "* Last Name:")]
        public string LastName { get; set; }

        [Display(Name = "Related First Name:")]
        public string RelatedFirstName { get; set; }

        [Display(Name = "Related Last Name:")]
        public string RelatedLastName { get; set; }

        [Display(Name = "Phone 1:")]
        public string Phone1 { get; set; }

        public string Phone1Type { get; set; }

        [Display(Name = "Phone 2:")]
        public string Phone2 { get; set; }

        public string Phone2Type { get; set; }

        [Display(Name = "Address:")]
        public string Address { get; set; }

        [Display(Name = "City:")]
        public string City { get; set; }

        [Display(Name = "State:")]
        public string State { get; set; }

        [Display(Name = "Zip:")]
        public string ZipCode { get; set; }

        [Display(Name = "Notes:")]
        public string Notes { get; set; }

        public int Status { get; set; }
        public long ClientId { get; set; }

        public List<EmailList> EmailList { get; set; }

        public IEnumerable<SelectListItem> StateList { get; set; }

        public bool IsEditMode { get; set; }
    }

    public class EmailList
    {
        [DataType(DataType.EmailAddress)]
        [Display(Name = "* Email:")]
        public string Email { get; set; }

        public string EmailType { get; set; }
    }

    public class AddNewClientsViewModel : LeadViewModels.SendCustomBrochurePost
    {
        public AddNewClientsViewModel()
        {
            NewClients = new List<AddClientToList>();
            NewEmails = new List<AddClientToList>();
            ClientSourceList = new List<SelectListItem>();
            GoogleAnalytics = new StringBuilder();
        }

        public StringBuilder GoogleAnalytics { get; set; }
        public List<AddClientToList> NewClients { get; set; }
        public List<AddClientToList> NewEmails { get; set; }
        public List<SelectListItem> ClientSourceList { get; set; }
        public string ListingIds { get; set; }
    }

    public class AddClientToList : Client
    {
        public AddClientToList()
        {
            SelectClient = true;
        }

        public bool SelectClient { get; set; }
        public string PrimaryClient { get; set; }
    }

    public class AddToFavoritesClient : BasicAddTo
    {
        public bool ShowClearNames { get; set; }
        public bool IsNextSteps { get; set; }
        public bool IsCommunityResultsPage { get; set; }
        public AddToFavoritesClient()
        {
            Clients = new List<Client>();
        }
        public List<Client> Clients { get; set; }
    }

    public class ContactBuilderAndRequestAppointmentClient : AddToFavoritesClient
    {
    }

    public class BasicAddTo : BaseViewModel
    {
        public string BuilderName { get; set; }
        public string CommunityName { get; set; }
        public string HomeTitle { get; set; }
        public string ListingType { get; set; }
        public int PropertyId { get; set; }
        public bool IsFromSaveToFavorites { get; set; }
        public bool IsContactBuilder { get; set; }
        public bool IsRequestAppointment { get; set; }
        public int SpecId { get; set; }
        public int PlanId { get; set; }
        public int CommunityId { get; set; }
        public int BuilderId { get; set; }
        public string Message { get; set; }
        public bool IsCommunity
        {
            get { return SpecId == 0 && PlanId == 0; }
        }

        public bool IsPlan
        {
            get { return SpecId == 0; }
        }
    }

    public class AddEmailToNewClients : BasicAddTo
    {
        public AddEmailToNewClients()
        {
            Clients = new List<AddClientToList>();
            ClientSourceList = new List<SelectListItem>();
        }
        public bool HasErrors { get; set; }
        public List<SelectListItem> ClientSourceList { get; set; }
        public List<AddClientToList> Clients { get; set; }
        public List<AddClientToList> Emails { get; set; }
    }

    public class Client
    {
        public int Index { get; set; }
        public string Email { get; set; }
        public int ClientId { get; set; }
        public string Name { get; set; }
    }
}