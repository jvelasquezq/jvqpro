﻿using System.Collections.Generic;
using System.Web.Mvc;
using Nhs.Library.Web;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class EBookViewModel  : BaseViewModel
    {
        public List<ContentTag> ReplacementTags { get; set; }
        public IEnumerable<SelectListItem> MarketList { get; set; }
        public int MarketId { get; set; }
        public string Email { get; set; }
        public string ButtonText { get; set; }
    }
}