﻿using System.Collections.Generic;
using Nhs.Library.Common;
using Nhs.Library.Web;
using Nhs.Mvc.Domain.Model.Web.Partials;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Search.Objects;
using Nhs.Web.UI.AppCode.Helper;
using StructureMap.Query;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class EmailTemplateViewModel : BaseEmailTemplateViewModel
    {
        public EmailTemplateViewModel()
        {
            RecoProperties = new List<LeadEmailItem>();
            SourceProperties = new List<LeadEmailItem>();
        }

        public int RequestId { get; set; }
        public bool ShowMatchingComm { get; set; }
        public IEnumerable<LeadEmailItem> RecoProperties { get; set; }
        public List<LeadEmailItem> SourceProperties { get; set; }
    }

    public class BaseEmailTemplateViewModel : BaseViewModel
    {
        public BaseEmailTemplateViewModel()
        {
            ReplacementTags = new List<ContentTag>();
        }
        public bool UsedPartnerName { get; set; }
        public string UserEmail { get; set; }
        public List<ContentTag> ReplacementTags { get; internal set; }    
    }

    public class AccountEmailTemplateViewModel : BaseEmailTemplateViewModel
    {
        public string BeaconUrl { get; set; }
    }

    public class RecoverPasswordTemplateViewModel : BaseEmailTemplateViewModel
    {

    }

    public class SearchAlertTemplateViewModel : BaseEmailTemplateViewModel
    {
        public SearchAlertTemplateViewModel()
        {
            AlertList = new List<SearchAlert>();
            FinalResults = new List<Community>();
        }

        public bool IsWeeklyEmail { get; set; }
        public IList<SearchAlert> AlertList { get; set; }
        public int MarketId { get; set; }
        public string MarketName { get; set; }
        public string FirstName { get; set; }
        public UserProfile UserProfile { get; set; }
        public string AlertName { get; set; }
        public List<Community> FinalResults { get; set; }
        public string WelcomeText { get; set; }
        public int TotalNumberOfHomes { get; set; }
        public int TotalNumberOfCommunities { get; set; }
        public string UtmParameters { get; set; }
    }


    public class SendFriendTemplateViewModel : BaseEmailTemplateViewModel
    {
        public string SupportEmail { get; set; }
        public string FriendEmail { get; set; }
        public string LeadComments { get; set; }
        public bool SendMe { get; set; }

        public int SpecId { get; set; }
        public int PlanId { get; set; }
        public int CommunityId { get; set; }
        public int BuilderId { get; set; }
        public string FromEmail { get; set; }
        public string SiteName { get; set; }

        public Community Community { get; set; }
        public Spec Spec { get; set; }
        public IEnumerable<Promotion> Promotions { get; set; }
        public string HomeLink { get; set; }
        public bool HasHotHomes { get; set; }
        public HomeResult HotHomeSpec { get; set; }
        public int TotalNumberOfCommunities { get; set; }
        public int TotalNumberOfHomes { get; set; }

        public string HomeResultsPage()
        {
            var parameters = RedirectionHelper.ToResultsParams(Community.MarketId);
            parameters.Add(new RouteParam(RouteParams.HomeStatus,5));
            return parameters.ToUrl(RedirectionHelper.GetHomeResultsPage());
        }

        public string CommunityResultsPage()
        {
            var parameters = RedirectionHelper.ToResultsParams(Community.MarketId);
            return parameters.ToUrl(RedirectionHelper.GetCommunityResultsPage());
        }

        public bool ShowSuccessMessage { get; set; }
    }
}