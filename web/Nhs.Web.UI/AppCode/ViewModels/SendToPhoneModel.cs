﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Nhs.Web.UI.AppCode.Helper;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class SendToPhoneModel : BaseViewModel
    {
        public string Phone { get; set; }
        public string Provider { get; set; }

        public int PlanId { get; set; }
        public int SpecId { get; set; }
        public int CommunityId { get; set; }
        public int BuilderId { get; set; }

        public List<SelectListItem> GetProviders
        {
            get
            {
                var list = new List<SelectListItem>
                {
                    new SelectListItem {Selected = true, Text = LanguageHelper.ChooseProvider, Value = ""},
                    new SelectListItem {Value = "sms.alltelwireless.com", Text = @"Alltel"},
                    new SelectListItem {Value = "txt.att.net", Text = @"AT&T"},
                    new SelectListItem {Value = "messaging.nextel.com", Text = @"Nextel"},
                    new SelectListItem {Value = "messaging.sprintpcs.com", Text = @"Sprint"},
                    new SelectListItem {Value = "tmomail.net", Text = @"T-Mobile"},
                    new SelectListItem {Value = "vtext.com", Text = @"Verizon"},
                    new SelectListItem {Value = "vmobl.com", Text = @"Virgin Mobile USA"}
                };
                return list;
            }
        }
    }
}