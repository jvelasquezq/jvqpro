﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Nhs.Search.Objects;
using Nhs.Library.Business;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Utility.Common;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class BoylResultsViewModel : BaseViewModel
    {
        public int MarketId { get; set; }
        public string MarketName { get; set; }
        public Brand CurrentBrand { get; set; }
        public bool ShowComingSoonLink { get; set; }
        public bool CanAddAlert { get; set; }
        public bool AllowRegistration { get; set; }
        public string CantAddAlertClick { get; set; }
        public IEnumerable<IGrouping<string, BoylResult>> Results { get; set; }
        public IEnumerable<IGrouping<string, BoylResult>> BasicResults { get; set; }
        public IList<ExtendedHomeResult> SpotHomes { get; set; }
        public string BuilderCommunityList { get; set; }

        public int TotalBuilders
        {
            get { return Results.Count(); }
        }

        public int TotalListings { get; set; }

      
    }
}
