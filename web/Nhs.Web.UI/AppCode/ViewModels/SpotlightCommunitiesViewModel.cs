﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library;
using State = Nhs.Mvc.Domain.Model.Web.State;
using Nhs.Search.Objects;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class SpotlightCommunitiesViewModel: BaseViewModel
    {
        public string FlashSpot { get; set; }
        public List<ExtendedCommunityResult> SpotList { get; set; }
        public bool HotHomes { get; set; }
        public bool GreenHomes { get; set; }
        public bool UseLiInCarousel { get; set; }
    }
}