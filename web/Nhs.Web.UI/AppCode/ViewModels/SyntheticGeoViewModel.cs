﻿using System.Collections.Generic;
using Nhs.Library.Business.Seo;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class SyntheticGeoCollectionViewModel : BaseViewModel
    {
        public SyntheticGeoCollectionViewModel()
        {
            SyntheticGeoCollection = new List<SyntheticGeoViewModel>();
        }

        public List<SyntheticGeoViewModel> SyntheticGeoCollection;
    }

    public class SyntheticGeoViewModel
    {
        public Synthetic SyntheticGeography;
        public string PostalCodes;
    }
}