﻿using System.Collections.Generic;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Web.UI.AppCode.ViewModels.CommunityHomeResult;

namespace Nhs.Web.UI.AppCode.ViewModels
{

    public interface IDetailViewModel : IMediaGalleryViewModel, IDetailPromotionsViewModel, ISecondaryInfoViewModel
    {
        bool IsCommunityMultiFamily { get; set; }
        bool IsPageCommDetail { get; set; }
        double Latitude { get; set; }
        double Longitude { get; set; }
        IEnumerable<PlannerListing> SavedPropertysProCrm { get; set; }
        string RequestUniqueKey { get; set; }
        string HomeTitle { get; set; }
        bool HasShowCaseInformation { get; set; }
        int BrandId { get; set; }
        BoylResult CnhBoyl { get; set; }
        bool NewSearchRCC { get; set; }
        bool ShowUserPhoneNumber { get; set; }
        string DetailType { get; }
        BaseViewModelElements Globals { get; set; }
        int CommunityId { get; set; }
        int MarketId { get; set; }
        string MarketName { get; set; }
        int BuilderId { get; set; }
        int PlanId { get; set; }
        int SpecId { get; set; }
        int LeadSectionState { get; set; }
        bool IsPlan { get; set; }
        bool LiveOutside { get; set; }
        bool GeneralInquiry { get; set; }
        bool ScheduleAppointment { get; set; }
        bool SendAlerts { get; set; }
        bool ShowMortgageLink { get; set; }
        bool BcTypeBanner { get; set; }
        string BcType { get; set; }
        string Name { get; set; }
        string BasicHomeSpecs { get; set; }
        string CommunityUrl { get; set; }
        string MailAddress { get; set; }
        string UserPostalCode { get; set; }
        string UserPhoneNumber { get; set; }
        string Comments { get; set; }
        string Message { get; set; }
        string CommunityName { get; set; }
        string PopUpViewerName { get; set; }
        string PlanName { get; set; }
        bool ShowBrochure { get; set; }
        string BrandThumbnail { get; set; }
        string BuilderName { get; set; }
        string CorporationName { get; set; }
        string BrandName { get; set; }
        string HomesAvailable { get; set; }
        string DisplaySqFeet { get; set; }
        string MortgagePage { get; }
        string CreditScoreLink1 { get; set; }
        string CreditScoreLink2 { get; set; }
        List<RouteParam> BrochureParams { get; }
        List<RouteParam> ExpiredBrochureParams { get; }
        string PhoneNumber { get; set; }
        string TrackingPhoneNumber { get; set; }
        string LogActionMethodUrl { get; }
        string PriceDisplay { get; set; }
        string BedroomsAbbr { get; set; }
        string BathroomsAbbr { get; set; }
        string GaragesAbbr { get; set; }
        bool ExcludesLand { get; set; }
        string HoursOfOperation { get; set; }
        string SaleAgents { get; set; }
        string BuilderLogo { get; }
        string BuilderLogoSmall { get; }
        string SalesOfficeAddress1 { get; }
        string SalesOfficeAddress2 { get; }
        string HomeAddress1 { get; set; }
        string HomeAddress2 { get; set; }
        string CommunityCity { get; set; }
        string ZipCode { get; set; }
        string State { get; set; }
        string StateAbbr { get; set; }
        string BuilderUrl { get; }
        string DetailDescription { get; }
        string NonPdfBrochureUrl { get; set; }
        string VirtualTourUrl { get; set; }
        bool ShowLeadForm { get; set; }
        bool ShowSaveToAccount { get; set; }
        bool ShowRegInfoDetail { get; set; }
        bool ShowExpiredRequestBrochureLink { get; set; }
        ICollection<Video> Videos { get; set; }
        IList<IImage> AwardImages { get; set; }
        IList<Testimonial> Testimonials { get; set; }
        IList<MediaPlayerObject> ExternalMediaLinks { get; set; }
        IList<MediaPlayerObject> PropertyMediaLinks { get; set; }
        IList<MediaPlayerObject> PlayerMediaObjects { get; set; }
        bool ShowHotHomeSection { get; set; }
        IEnumerable<SelectListItem> States { get; set; }
        string SelectedStateAbbr { get; set; }
        string SearchText { get; set; }
        string PriceLow { get; set; }
        string PriceHigh { get; set; }
        int HomeStatus { get; set; }
        IEnumerable<SelectListItem> PriceLowRange { get; set; }
        IEnumerable<SelectListItem> PriceHighRange { get; set; }
        string EnvisionUrl { get; set; }
        bool PartnerUsesMatchmaker { get; set; }
        string ConversionIFrameSource { get; set; }
        string FirstImage { get; set; }
        string TypeAheadUrl { get; }
        ICollection<Promotion> Promotions { get; set; }
        ICollection<GreenProgram> GreenPrograms { get; set; }
        ICollection<Event> Events { get; set; }

        ICollection<HomeItem> HotHomesApi { get; set; }

        bool ShowPlaceHolderInputText { get; set; }

        string CreditScoreText { get; set; }
        bool ShowCreditScoreSection { get; set; }

        bool PreviewMode { get; set; }

        bool IsLoggedInPaid { get; }
        bool IsLoggedInUnpaid { get; }
        bool IsLoggedOutPaid { get; }
        bool IsLoggedOutUnpaid { get; }
        bool IsBilled { get; set; }
        bool RequestAnAppointment { get; set; }
        bool IsAdult { get; set; }
        bool IsCondo { get; set; }
        bool IsGreen { get; set; }
        bool IsGated { get; set; }
        bool IsAgeRestricted { get; set; }
        bool IsMasterPlanned { get; set; }
        bool IsGreenProgram { get; set; }
        bool IsInactive { get; set; }
        bool NewUser { get; set; }
        AffiliateLinksData AffiliateLinksData { get; set; }
        bool UserAlreadyExists { get; set; }
    }
}
