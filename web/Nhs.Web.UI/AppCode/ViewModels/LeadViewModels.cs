﻿using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Web.UI.AppCode.HtmlHelpers;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class LeadViewModels
    {

        public class RequestInfoThanksViewModel : BaseViewModel
        {
            private string _requestUniqueKey = string.Empty;
            public bool Registered { get; set; }
            public bool SkipStep { get; set; }
            public bool AlertCreated { get; set; }

            public bool UserAlreadyExists { get; set; }
            public bool CheckEmail { get; set; }

            public List<ExtendedCommunityResult> RecComms { get; set; }
            public List<ExtendedHomeResult> RecHomes { get; set; }
            public int MarketId { get; set; }
            public string MarketName { get; set; }
            public string StateName { get; set; }
            public int CommunityId { get; set; }
            public int SpecId { get; set; }
            public int PlanId { get; set; }
            public double PriceLow { get; set; }
            public double PriceHigh { get; set; }
            public string LeadType { get; set; }
            public string ContactEmail { get; set; }
            public string DiscoveryText { get; set; }
            public string BrandLogo { get; set; }
            public string BrandName { get; set; }
            public string ApptModal { get; set; }

            public string RequestUniqueKey
            {
                get { return _requestUniqueKey; }
                set { _requestUniqueKey = value; }
            }

            public string ConversionIFrameSource { get; set; }
            public bool CblReqComm_1 { get; set; }
            public bool CblReqComm_2 { get; set; }
            public bool CblReqComm_3 { get; set; }

            public RequestInfoThanksViewModel()
            {
                RecComms = new List<ExtendedCommunityResult>();
                RecHomes = new List<ExtendedHomeResult>();
            }
        }

        public class BrochureInterstitialViewModel : BaseViewModel
        {
            public string NextPageUrl { get; set; }
            public int RefreshDelay { get; set; }
            public int NumberOfSearch { get; set; }
        }

        public class ConversionTrackerViewModel : BaseViewModel
        {
        }

        public class SendCustomBrochurePost : BaseViewModel
        {
            public string Email { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string SessionId { get; set; }
            public int CommunityId { get; set; }
            public int MarketId { get; set; }
            public int BuilderId { get; set; }
            public int SpecId { get; set; }
            public int PlanId { get; set; }
            public string NameEmails { get; set; }
            public string LeadType { get; set; }
            public bool IsMvc { get; set; }
            public bool IsBilled { get; set; }
            public bool IsMultiBrochure { get; set; }
            public List<AddClientToList> ClientsEmails { get; set; }

            public bool IsCommunity
            {
                get { return SpecId == 0 && PlanId == 0; }
            }

            public bool IsPlan
            {
                get { return SpecId == 0; }
            }
        }

        public class SendCustomBrochure : BaseViewModel
        {
            public SendCustomBrochure()
            {
                ClientsEmails = new List<Client>();
            }

            public string Email { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string SessionId { get; set; }

            public bool ShowClearNames { get; set; }
            public List<Client> ClientsEmails { get; set; }
            public bool IsBilled { get; set; }
            public string MailAddress { get; set; }
            public int CommunityId { get; set; }
            public int BuilderId { get; set; }
            public int SpecId { get; set; }
            public int PlanId { get; set; }
            public int MarketId { get; set; }
            public string LeadType { get; set; }
            public bool HavAgentPhotoOrAgencyLogo { get; set; }

            public bool IsCommunity
            {
                get { return SpecId == 0 && PlanId == 0; }
            }

            public bool IsPlan
            {
                get { return SpecId == 0; }
            }

            public bool IsMvc { get; set; }
            public bool IsMultiBrochure { get; set; }

            public MvcHtmlString GetTitle()
            {

                var count = UserSession.MultiBrochureList.BrochureList.Count;
                if (!IsMultiBrochure || count == 1)
                    return "".ToMvcHtmlString();
                return string.Format("<h3>You have selected {0} brochures to send.</h3>", count).ToMvcHtmlString();
            }
        }
    }
}
