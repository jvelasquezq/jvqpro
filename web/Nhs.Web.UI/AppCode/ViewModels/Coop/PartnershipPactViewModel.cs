﻿using Nhs.Library.Business.BuilderCoop;

namespace Nhs.Web.UI.AppCode.ViewModels.Coop
{
    public class PartnershipPactViewModel : BaseViewModel
    {
        public BuilderCoOpInfo Info { get; set; }
    }
}