﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nhs.Library.Business;
using Nhs.Library.Business.Map;
using Nhs.Library.Common;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Helpers.Utility;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.ViewModels.LeadView;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class BasicHomeDetailViewModel : BaseViewModel, IPropertyMapViewModel, IDetailViewModel
    {

        private readonly string _nullImage = Resources.GlobalResourcesMvc.Default.images.agent_ph_gif;
        public bool ShowCreditScoreSection { get; set; }
        bool IDetailViewModel.PreviewMode { get; set; }
        public string PlanName { get; set; }
        public bool IsLoggedInPaid { get; private set; }
        public bool IsLoggedInUnpaid { get; private set; }
        public bool IsLoggedOutPaid { get; private set; }
        public bool IsLoggedOutUnpaid { get; private set; }
        bool ISecondaryInfoViewModel.PreviewMode { get; set; }
        public bool IsBilled { get; set; }
        public bool RequestAnAppointment { get; set; }
        public bool IsAdult { get; set; }
        public bool IsCondo { get; set; }
        public bool IsGreen { get; set; }
        public bool IsGated { get; set; }
        public bool IsAgeRestricted { get; set; }
        public bool IsMasterPlanned { get; set; }
        public bool UserAlreadyExists { get; set; }
        public bool IsGreenProgram { get; set; }
        public bool IsInactive { get; set; }
        public bool NewUser { get; set; }
        public bool ShowCta { get; set; }
        public bool ShowEbook { get; set; }
        public bool PreviewMode { get; set; }
        public int FeedProviderId { get; set; }
        public int BasicListingId { get; set; }
        public string AgentName { get; set; }
        public string AgentPhoneNumber { get; set; }
        public string AgentPictureUrl { get; set; }
        public string BrokerImageUrl { get; set; }
        public string BrokerUrl { get; set; }
        public string BrokerName { get; set; }
        public string BasicListingNumber { get; set; }
        public string ListHubProviderId { get; set; }
        public string ListHubTestLogging { get; set; }
        public string MlsNumber { get; set; }
        public string Beds { get; set; }
        public string Baths { get; set; }
        public string City { get; set; }
        public string MarketName { get; set; }
        public string State { get; set; }
        public string StateAbbr { get; set; }
        public string PostalCode { get; set; }
        public decimal Sqft { get; set; }
        public int Price { get; set; }
        public string Address { get; set; }
        public List<string> SchoolDistrict { get; set; }
        public string Description { get; set; }
        public string WebBugUrl { get; set; }
        public string ImgUrl { get; set; }
        public string H1 { get; set; } //Used for SEO

        public bool HasCoop { get; set; }

        public bool HasBuilderPactLink { get; set; }
        public string BuilderPackLink { get; set; }

        public string GetPrice { get { return string.Format("{0}", Price.ToString("$###,####")); } }
        public string GetBeds
        {
            get
            {
                return (string.IsNullOrEmpty(Beds) || Beds.ToType<int>() == 0)
                           ? string.Empty
                           : string.Format("<li>{0} {1}</li>", Beds, Beds.ToType<int>() == 1 ? LanguageHelper.Bedroom.ToLower() : LanguageHelper.Bedrooms.ToLower());
            }
        }

        public string GetBaths
        {
            get
            {
                var bathrooms = ListingUtility.ComputeBasicListingBathrooms(Baths);

                if (string.IsNullOrEmpty(bathrooms))
                    return string.Empty;

                return string.Format("<li>{0} {1}</li>", bathrooms, Baths.ToType<int>() == 1 ? LanguageHelper.Bath.ToLower() : LanguageHelper.Bathrooms.ToLower());
            }
        }

        public string GetSqft
        {
            get
            {
                return (Sqft == 0)
                               ? string.Empty
                               : string.Format("<li>{0} sq. ft.</li>", Sqft.ToString("F0"));
            }

        }

        public string GetMlsNumber
        {
            get
            {
                return (string.IsNullOrEmpty(MlsNumber))
                               ? string.Empty
                               : string.Format("<li>MLS# : {0}</li>", MlsNumber);
            }

        }

        public string GetAgentPhoneNumber
        {
            get
            {
                if ((!string.IsNullOrEmpty(AgentPhoneNumber)))
                {
                    try
                    {
                        var numberArray = AgentPhoneNumber.ToCharArray().Where(Char.IsNumber).ToArray();
                        var number = string.Join("", numberArray).ToType<Decimal>().ToString("###-###-####");
                        return number;
                    }
                    catch { return string.Empty; }
                }
                else
                    return string.Empty;
            }
        }

        public Boolean HaveAgentPhoneNumber
        {
            get { return !string.IsNullOrEmpty(AgentPhoneNumber); }
        }

        public string GetImgUrl
        {
            get
            {
                return (!string.IsNullOrEmpty(ImgUrl))
                            ? ImgUrl
                            : Resources.GlobalResources14.Default.images.no_photo.no_photos_325x216_png;
            }
        }

        public int GetPropertyDetailDescriptionLength()
        {
            return Configuration.PropertyDetailDescriptionLength;
        }
        public string GetAgentPictureUrl
        {
            get { return string.IsNullOrEmpty(AgentPictureUrl) ? _nullImage : AgentPictureUrl; }
        }

        public string GetBrokerImageUrl
        {
            get { return string.IsNullOrEmpty(BrokerImageUrl) ? _nullImage : BrokerImageUrl; }
        }

        public string YearBuilt { get; set; }
        ICollection<Event> IDetailViewModel.Events { get; set; }
        public ICollection<HomeItem> HotHomesApi { get; set; }
        public bool ShowPlaceHolderInputText { get; set; }
        public string CreditScoreText { get; set; }

        #region IPropertyMapViewModel
        public bool HideDrivingDirections { get; set; }
        public bool IsCommunityMultiFamily { get; set; }
        bool IDetailViewModel.IsPageCommDetail { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public IEnumerable<PlannerListing> SavedPropertysProCrm { get; set; }
        public string RequestUniqueKey { get; set; }
        public string HomeTitle { get; set; }
        public bool HasShowCaseInformation { get; set; }
        public int BrandId { get; set; }
        public BoylResult CnhBoyl { get; set; }
        public bool NewSearchRCC { get; set; }
        public bool ShowUserPhoneNumber { get; set; }
        public double RouteLat { get; set; }
        public double RouteLng { get; set; }
        public string PropertyName { get; set; }
        public string DrivingDirections { get; set; }
        public string DrivingDirectionsShort { get; set; }
        public DrivingDirections DrivingDirectionsList { get; set; }
        public List<NearbyCommunity> NearByComms { get; set; }
        public string NearByCommsObject { get; set; }
        public IList<NearbyCommunity> NearByCommsSmall { get; set; }
        public string CorporationName { get; set; }
        public string BrandName { get; set; }
        public string BuilderMapUrl { get; set; }
        public string UnderneathMapCommName { get; set; }
        public string UnderneathMapAddress { get; set; }
        bool IFreeBrochureLinkInformation.IsPageCommDetail { get; set; }
        public int MarketId { get; set; }
        public int CommunityId { get; set; }
        public int BuilderId { get; set; }
        public int PlanId { get; set; }
        public int SpecId { get; set; }
        public int LeadSectionState { get; set; }
        public bool EnableRequestAppointment { get; set; }

        #endregion

        #region IDetailViewModel Interface
        public string BasicHomeSpecs { get; set; }
        public bool IsPlan { get; set; }
        public string BuilderName { get; set; }
        public string Message { get; set; }
        public string CommunityName { get; set; }
        public string PopUpViewerName { get; set; }
        public bool ShowBrochure { get; set; }
        public int HomeStatus { get; set; }
        public string Name { get; set; }
        public string MailAddress { get; set; }
        public string UserPostalCode { get; set; }
        public string UserPhoneNumber { get; set; }
        public string Comments { get; set; }
        public bool LiveOutside { get; set; }
        public bool GeneralInquiry { get; set; }
        public bool ScheduleAppointment { get; set; }
        public bool SendAlerts { get; set; }
        public IList<Spec> CrossMarketingHomes { get; set; }
        public string BrandThumbnail { get; set; }
        public string HomesAvailable { get; set; }
        public string DisplaySqFeet { get; set; }
        public List<RouteParam> BrochureParams { get; set; }
        public List<RouteParam> ExpiredBrochureParams { get; set; }
        public string MortgagePage { get; set; }

        public string CreditScoreLink1 { get; set; }
        public string CreditScoreLink2 { get; set; }

        public string PhoneNumber { get; set; }
        public string TrackingPhoneNumber { get; set; }
        public string HoursOfOperation { get; set; }
        public bool ShowMortgageLink { get; set; }
        public bool BcTypeBanner { get; set; }
        public string LogActionMethodUrl { get; set; }
        //public IList<HubImage> PreviewAwardImages { get; set; }
        public IList<Testimonial> Testimonials { get; set; }
        public IList<MediaPlayerObject> ExternalMediaLinks { get; set; }
        public IList<School> Schools { get; set; }
        public IDictionary<AmenityGroup, IList<IAmenity>> Amenities { get; set; }
        public ICollection<Mvc.Domain.Model.Web.Utility> Utilities { get; set; }
        public IList<MediaPlayerObject> PropertyMediaLinks { get; set; }
        public IList<MediaPlayerObject> PlayerMediaObjects { get; set; }
        public IList<MediaPlayerObject> PropertyMediaVideoLinks { get; set; }
        public ICollection<Promotion> Promotions { get; set; }
        ICollection<Event> IDetailPromotionsViewModel.Events { get; set; }
        public ICollection<ExtendedHomeResult> HotHomes { get; set; }
        public ICollection<GreenProgram> GreenPrograms { get; set; }
        public bool ShowHotHomeSection { get; set; }
        public IEnumerable<SelectListItem> States { get; set; }
        public string SelectedStateAbbr { get; set; }
        public string SearchText { get; set; }
        public string PriceHigh { get; set; }
        public string PriceLow { get; set; }
        public IEnumerable<SelectListItem> PriceLowRange { get; set; }
        public IEnumerable<SelectListItem> PriceHighRange { get; set; }
        public string EnvisionUrl { get; set; }
        public bool PartnerUsesMatchmaker { get; set; }
        public string ConversionIFrameSource { get; set; }
        public string VirtualTourUrl { get; set; }
        public string BcType { get; set; }
        public string DetailType { get; set; }
        public string CommunityUrl { get; set; }
        public string PriceDisplay { get; set; }
        public string BedroomsAbbr { get; set; }
        public string BathroomsAbbr { get; set; }
        public string GaragesAbbr { get; set; }
        public bool ExcludesLand { get; set; }
        public string SaleAgents { get; set; }
        public string BuilderLogo { get; set; }
        public string BuilderLogoSmall { get; set; }
        public string SalesOfficeAddress1 { get; set; }
        public string SalesOfficeAddress2 { get; set; }
        public string HomeAddress1 { get; set; }
        public string HomeAddress2 { get; set; }
        public string CommunityCity { get; set; }
        public string CommunityDescription { get; set; }
        public string AgentCompensation { get; set; }
        public string AgentPolicyLink { get; set; }
        public string AgentCompensationAdditionalComments { get; set; }
        public string AgentCommissionBasis { get; set; }
        public string AgentCommissionPayoutTiming { get; set; }
        public ICollection<GreenProgram> GreenPromos { get; set; }
        public string ZipCode { get; set; }
        public string BuilderUrl { get; set; }
        public string DetailDescription { get; set; }
        public string NonPdfBrochureUrl { get; set; }
        public bool ShowLeadForm { get; set; }
        public bool ShowSaveToAccount { get; set; }
        public bool ShowRegInfoDetail { get; set; }
        public bool ShowExpiredRequestBrochureLink { get; set; }
        public ICollection<Video> Videos { get; set; }
        public IList<IImage> AwardImages { get; set; }
        #endregion

        #region IMediaGalleryViewModel

        public KeyValuePair<int, int> Size { get; set; }
        public string FirstImage { get; set; }
        public string PinterestDescription { get; set; }
        public string PageUrl { get; set; }
        public IList<ContentTag> SeoContentTags { get; set; }
		bool IMediaGalleryViewModel.PreviewMode { get; set; }
        public bool IsBasicCommunity { get; set; }
        public bool IsCommunityResults { get; set; }


        #endregion
    }
}
