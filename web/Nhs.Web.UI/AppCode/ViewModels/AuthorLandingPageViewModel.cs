﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nhs.Mvc.Domain.Model.CMS;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class AuthorLandingPageViewModel : ResourceCenterViewModels.ResourceCenterBaseViewModel
    {
        public IEnumerable<RcAuthor> Authors { get; set; }
        public RcAuthorLandingPage LandingPageInfo { get; set; }        
    }
}