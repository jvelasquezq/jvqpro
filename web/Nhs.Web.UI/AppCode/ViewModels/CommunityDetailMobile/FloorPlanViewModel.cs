﻿using System.Collections.Generic;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;

namespace Nhs.Web.UI.AppCode.ViewModels.CommunityDetailMobile
{
    public class FloorPlanViewModel
    {
        public IList<IImage> FloorPlans { get; set; }
        public int PlanId { get; set; }
        public int SpecId { get; set; }
        public bool IsSpec { get; set; }
        public string PlanName { get; set; }
        public bool IsPreviewMode { get; set; }
    }
}