﻿using System.Collections.Generic;
using Bdx.Web.Api.Objects;
using Nhs.Search.Objects;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class PresentedEmailViewModel :BaseViewModel
    {
        public PresentedEmailViewModel()
        {
            CommunityList = new List<ApiRecoCommunityResult>();
        }

        #region Header
        public string CommunityName { get; set; }
        public string MarketName { get; set; }
        public string FirstName { get; set; }
        public string RecoCommunityBuilderList { get; set; }
        public string LastName { get; set; }
        public string SourceCommunityId { get; set; }
        public string Email { get; set; }
        public string PostalCode { get; set; }
        #endregion

        public string RecoCommunityList { get; set; }
        public IList<ApiRecoCommunityResult> CommunityList { get; set; } 

    }
}