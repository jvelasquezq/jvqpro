﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Web.UI.AppCode.ViewModels
{
    public class NewsletterViewModel : BaseViewModel
    {
        public IEnumerable<Market> MarketList { get; set; }
        public string Email { get; set; }
        public int MarketId { get; set; }
        public int BrandPartnerId { get; set; }
        public string MarketName { get; set; }
        public string PartnerName { get; set; }
        public string PartnerSiteUrl { get; set; }
        public bool IsSmallSignUpRequest { get; set; }
        public bool IsSmallTvSignUpRequest { get; set; }
    }
}