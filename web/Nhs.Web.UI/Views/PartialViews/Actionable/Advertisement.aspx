﻿<%@ Page Language="C#" Inherits="Nhs.Web.UI.AppCode.Lib.ViewPageBase<Nhs.Web.UI.AppCode.ViewModels.PartialViewModels.AdViewModel>" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.HtmlHelpers" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.Extensions" %>

<%=Html.AdControl(Model.Position, Model.UseFrame, Model.JavaScriptParams, Model.Globals.AdController)%>