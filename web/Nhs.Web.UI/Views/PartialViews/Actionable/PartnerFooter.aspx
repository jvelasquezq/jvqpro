﻿<%@ Page Language="C#" Inherits="Nhs.Web.UI.AppCode.Lib.ViewPageBase<Nhs.Web.UI.AppCode.ViewModels.PartialViewModels.PartnerFooter>" %>

<%@ Import Namespace="Nhs.Library.Web" %>
<%@ Import Namespace="Nhs.Mvc.Routing.Interface" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.HtmlHelpers" %>
<%@ Import Namespace="Nhs.Library.Constants" %>
<%@ Import Namespace="Nhs.Utility.Common" %>
<%@ Import Namespace="Nhs.Mvc.Routing.Configuration" %>


<%if (Model.Globals.PartnerLayoutConfig.IsPartnerYahoo)
  {%>
<div id="nhs_Footer">
    <div id="nhs_FooterAdBox">
        <p class="nhs_Reader">
            </p>
             <div id="<%=Model.Globals.PartnerLayoutConfig.AmIOnCommunityResultsPage?"nhs_Footerx02":"nhs_AdFooter" %>"></div>  
             <%-- <%= Html.AdControl(Model.Globals.PartnerLayoutConfig.AmIOnCommunityResultsPage ? "x02" : "x01", true, false, Model.Globals.AdController, Model)%>--%>
    </div>
    <hr class="nhs_ReaderAlt" />
    <aside id="nhs_FooterAside">
        <% }
  else
  {%>
    <aside id="nhs_FooterAside">
        <% } %>
        <hr class="nhs_ReaderAlt" />
        <nav>
            <ul>
                <li>
                    <%= Html.NhsLink("Search for new homes", Pages.BasicSearch, new List<RouteParam>(), new { onclick = "$jq.googlepush('Site Links', 'Footer','Search');" })%></li>
                <%if (Model.Partner.UsesHomeGuide.ToBool())
                    {%>
                <li>
                    <a href="<%=("/" + Pages.GetResourceCenterLink(NhsRoute.BrandPartnerId)).Replace("//","/") %>" target="_blank">New Home Guide</a> 
                <%}%>
                <%if (Model.Partner.FooterShowListHomes.ToBool())
                    {%>
                <li>
                    <%= Html.NhsLink("List your homes", Pages.ListYourHomes, new List<RouteParam>(), new { onclick = "$jq.googlepush('Site Links', 'Footer','List your homes');" })%></li>
                <%}%>
                <%if (Model.Partner.AllowRegistration.ToBool())
                    {%>
                <%
                if (Model.IsPassiveUser)
                {%>
                <li>
                    <%=Html.NhsLink("Sign In", Pages.SignIn)%></li>
                <li>
                    <%=Html.NhsLink("Create Account", Pages.Register)%></li>
                <%
            }%>
                <%
            else
            {%>
                <li>
                    <%=Html.NhsLink("Sign Out", Pages.LogOff)%></li>
                <%
            }%>
                <%
            }%>
                <li>
                    <%= Html.NhsLink("Unsubscribe", Pages.Unsubscribe, new List<RouteParam>(), new { onclick = "$jq.googlepush('Site Links', 'Footer','Unsubscribe');" })%></li>
                <li>
                    <%= Html.NhsLink("Help", Pages.SiteHelp, new List<RouteParam>(), new { onclick = "$jq.googlepush('Site Links', 'Footer','Help');" })%></li>
            </ul>
        </nav>
        <nav>
            <ul>
                <%if (Model.Partner.ShowFooterPrivacyPolicy.ToBool())
                    {%>
                <li>
                    <%=Html.NhsLink("Privacy policy", Pages.PrivacyPolicy, new List<RouteParam>(), new { onclick = "$jq.googlepush('Site Links', 'Footer','Privacy policy');" })%></li>
                <%}%>
                <%if (Model.Partner.ShowFooterTermsOfUse.ToBool())
                    {%>
                <li>
                    <%=Html.NhsLink("Terms of use", Pages.TermsOfUse, new List<RouteParam>(), new { onclick = "$jq.googlepush('Site Links', 'Footer','Terms of use');" })%></li>
                <%}%>
                <%if (!Model.Partner.UsesLightFooter.ToBool())
                    {%>
                <%if (Model.Partner.FooterShowPartners.ToBool())
                    {%>
                <li>
                    <%=Html.NhsLink("Partners", Pages.Partners)%></li>
                <%}%>
                <%if (Model.Partner.FooterShowSiteIndex.ToBool())
                    {%>
                <li>
                    <%= Html.NhsLink("Site Index", Pages.SiteIndex, new List<RouteParam>(), new { onclick = "$jq.googlepush('Site Links', 'Footer','Site Index');" })%></li>
                <%}%>
                <%if (Model.Partner.FooterShowAboutUs.ToBool())
                    {%>
                <li>
                    <%= Html.NhsLink("About us", Pages.AboutUs, new List<RouteParam>(), new { onclick = "$jq.googlepush('Site Links', 'Footer','About us');" })%></li>
                <%}%>
                <li>
                    <%= Html.NhsLink("Contact us", Pages.ContactUs, new List<RouteParam>(), new { onclick = "$jq.googlepush('Site Links', 'Footer','Contact us');" })%></li>
                <%if (Model.Partner.FooterShowAdvertise.ToBool())
                    {%>
                <li><a href="http://www.builderhomesite.com/newhomesource/mediakit/request.htm">Advertise</a></li>
                <%}%>
                <%}%>
            </ul>
        </nav>
    </aside>
    <footer id="nhs_FooterCopyright">
        <p>
            <%
                string.Concat("Copyright &copy; ", DateTime.Now.Year);%>
            <a href="http://www.newhomesource.com/" onclick="$jq.googlepush('Outbound Links', 'Disclaimer','NewHomeSource.com');">
                NewHomeSource.com -- More New Homes Than Anywhere</a> is a trademark of <a href="http://www.thebdx.com"
                    onclick="$jq.googlepush('Outbound Links', 'Disclaimer','Builders Digital Experience');">
                    Builders Digital Experience, LLC</a> and all other marks are either trademarks
            or registered trademarks of their respective owners. All rights reserved.
        </p>
    </footer>
<%if (Model.Globals.PartnerLayoutConfig.IsPartnerYahoo)
{%>
</div>
<% } %>
