﻿<%@ Page Language="C#" Inherits="Nhs.Web.UI.AppCode.Lib.ViewPageBase<Nhs.Web.UI.AppCode.ViewModels.PartialViewModels.BreadcrumbViewModel>" %>
<div id="nhs_Crumbs" class="clearfix">
<%{ Html.RenderPartial(NhsMvc.PartialViews.Views.Actionable.BreadcrumbV2, Model); }%>
</div>