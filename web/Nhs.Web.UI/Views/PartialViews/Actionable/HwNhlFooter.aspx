﻿<%@ Page Language="C#" Inherits="Nhs.Web.UI.AppCode.Lib.ViewPageBase<Nhs.Web.UI.AppCode.ViewModels.PartialViewModels.HwNhlFooter>" %>

<%@ Import Namespace="Nhs.Library.Web" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.HtmlHelpers" %>
<%@ Import Namespace="Nhs.Library.Constants" %>
<%@ Import Namespace="Nhs.Utility.Common" %>
<%@ Import Namespace="Nhs.Library.Constants.Enums" %>
<%--<div id="hwnhl_FooterAdBox">
    <p class="nhs_Reader">Advertisement</p>
    <%=Html.AdControl("x01", true, false, Model.Globals.AdController, Model)%>
</div>--%>
<!-- HW NHL Footer //-->
<footer id="hwnhl_Footer">
    <hr class="nhs_ReaderAlt" />
    <div id="hwnhl_Searchbar">
	
	</div>
	<nav id="hwnhl_Menu">
           
	        <%=Html.NhsLink("Home", Pages.Home)%> |
	        <%=Html.NhsLink("Contact Us", Pages.ContactUs, true)%> 

            <%if (Model.Partner.FooterShowListHomes.ToBool())
              {
            %>	
                | <a href="http://www.thebdx.com/hw">List Your Homes</a> 
            <%}%>

            <%if (Model.Partner.FooterShowAboutUs.ToBool())
              {
            %>
               | <%= Html.NhsLink("About Us", Pages.AboutUs, true)%> 
            <%}%>
        
           <% if (Model.Globals.PartnerLayoutConfig.AmIOnHomePage)
              { %>

                 | <%= Html.NhsLink("Unsubscribe", Pages.Unsubscribe, true) %> 
           <% } %>

           <%if (Model.Partner.ShowFooterPrivacyPolicy.ToBool())
             {%>
	            | <%=Html.NhsLink("Privacy Policy", Pages.PrivacyPolicy, true)%> 
           <%}%>

           <%if (Model.Partner.ShowFooterTermsOfUse.ToBool())
             {%>
		       | <%=Html.NhsLink("Terms of Use", Pages.TermsOfUse, true)%>		
          <%}%>
	</nav>
	<p class="notes">
		 &copy; <%=DateTime.Today.Year%> Hanley-Wood, Inc. All Rights Reserved.
	</p>
	<aside class="links">
		<p><%= Html.NhsLink("New Homes", Pages.Home)%> | 
        <%=Html.RenderSeoContent(SeoTemplateType.NHL_Home, "footerlinks")%>
        </p>
        
        <p>Partner sites: <a href="http://www.eplans.com">House Plans</a> by ePlans.com</p>        
    </aside>
    <div class="nhs_Clear"></div>
</footer>
