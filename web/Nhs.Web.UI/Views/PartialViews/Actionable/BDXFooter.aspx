﻿<%@ Page Language="C#" Inherits="Nhs.Web.UI.AppCode.Lib.ViewPageBase<Nhs.Web.UI.AppCode.ViewModels.PartialViewModels.BDXFooter>" %>

<%@ Import Namespace="Nhs.Library.Constants.Enums" %>
<%@ Import Namespace="Nhs.Mvc.Routing.Interface" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.HtmlHelpers" %>
<%@ Import Namespace="Nhs.Library.Constants" %>
<%@ Import Namespace="Nhs.Mvc.Routing.Configuration" %>
<%@ Import Namespace="Nhs.Utility.Constants" %>
<%@ Import Namespace="Nhs.Utility.Common" %>
<% if (Model.NhsFooterVisible)
   {%>
<aside id="nhs_SocialFooter">
    <ul class="clearfix">
        <li><a id="nhs_Facebook" target="_blank" onclick="jQuery.SetDataLayerPair('siteSocialMediaS');" title="Like us on Facebook" href="https://www.facebook.com/NewHomeSource">Facebook</a></li>
        <li><a id="nhs_Twitter" target="_blank" onclick="jQuery.SetDataLayerPair('siteSocialMediaS');" title="Follow us on Twitter" href="https://www.twitter.com/NewHomeSource">Twitter</a></li>
        <li><a id="nhs_Pinterest" target="_blank" onclick="jQuery.SetDataLayerPair('siteSocialMediaS');" title="See us on Pinterest" href="https://www.pinterest.com/newhomesource/">Pinterest</a></li>
        <li><a id="nhs_YouTube" target="_blank" onclick="jQuery.SetDataLayerPair('siteSocialMediaS');" title="View Our Videos on YouTube" href="https://www.youtube.com/user/NewHomeSource">YouTube</a></li>
        <li><a id="nhs_GooglePlus" target="_blank" onclick="jQuery.SetDataLayerPair('siteSocialMediaS');" title="Connect with us on Google+" href="https://plus.google.com/102652212865916066440/posts">Google+</a></li>
    </ul>
</aside>
<%} %>
<% if (!Model.NhsFooterVisible)
   {%>
<div id="nhs_FooterWrapper">
    <%} %>
    <footer id="nhs_Footer">
        <div>
            <aside id="nhs_FooterAdBox">
                <p class="visuallyhidden">Advertisement</p>
                <div id="nhs_AdFooter">
                </div>
            </aside>

            <% if (Model.NhsFooterVisible)
               {%>
            <!-- BDX - NHS Footer //-->

            <nav id="nhs_FooterSearch">
                <p><strong>New Home Search</strong></p>
                <ul>
                    <% if (NhsRoute.IsMobileDevice)
                       {%>
                    <li><%=Html.NhsMobileSite()%></li>
                    <%} %>
                    <li><a href="/" onclick="$jq.googlepush('Site Links', 'Footer','Search');">Search</a></li>
                    <% if (NhsRoute.IsBrandPartnerNhsPro)
                       { %>
                    <li>
                        <%= Html.NhsLink("Advanced Search", "AdvancedSearch", new List<RouteParam>(), new {onclick = "$jq.googlepush('Site Links', 'Footer','Advanced Search');"}, true) %></li>
                    <li>
                        <% } %>
                        <%= Html.NhsLink("Build on your lot", "BoylSearch", new List<RouteParam>(), new { onclick = "$jq.googlepush('Site Links', 'Footer','Build on your lot');" }, true)%>
                    </li>
                    <li><a target="_blank" href="http://www.customnewhomes.com/" onclick="$jq.googlepush('Outbound Links', 'Footer','Custom Builders');">Custom Builders</a></li>
                    <li class="nhs_Last">
                        <%= Html.NhsLink("List your homes", "ListYourHomes", new List<RouteParam>(), new { onclick = "$jq.googlepush('Site Links', 'Footer','List your homes');" }, true)%></li>
                </ul>
            </nav>
            <nav id="nhs_FooterMore">
                <p><strong>More Choices</strong></p>
                <ul>
                    <li><a target="_blank" href="http://www.newretirementcommunities.com/" onclick="$jq.googlepush('Outbound Links', 'Footer','Retirement Communities');">Retirement Communities</a></li>
                    <li><a target="_blank" href="http://www.urbancondoliving.com/" onclick="$jq.googlepush('Outbound Links', 'Footer','Search New Condos');">Search New Condos</a></li>
                    <li>
                        <%= Html.NhsLink("NewHomeSource TV", Pages.NewHomeSourceTv, new List<RouteParam>(), new { onclick = "$jq.googlepush('Outbound Links', 'Footer','NewHomeSource TV');", target = "_blank" }, true)%></li>
                    <li><a target="_blank" href="http://www.casasnuevasaqui.com/" onclick="$jq.googlepush('Outbound Links', 'Footer','Casas Nuevas');">Casas Nuevas en Espa&ntilde;ol</a></li>
                    <li><a target="_blank" href="http://blog.newhomesource.com/" onclick="$jq.googlepush('Outbound Links', 'Footer','Home Trends Blog');">Home Trends Blog</a></li>
                    <%if (Nhs.Library.Common.Configuration.ShowEBook)
                      {%>
                    <li class="nhs_Last"><a target="_blank" href="/ebook" onclick="jQuery.googlepush('Site Links','Footer','Free eBook')">Free New Home 101 eBook</a></li>
                    <%} %>
                </ul>
            </nav>
            <nav id="nhs_FooterAbout">
                <p><strong>About This Site</strong></p>
                <ul>
                    <li>
                        <%= Html.NhsLink("Unsubscribe", Pages.Unsubscribe, new List<RouteParam>(), new { onclick = "$jq.googlepush('Site Links', 'Footer','Unsubscribe');" }, true)%></li>
                    <li>
                        <%= Html.NhsLink("Help", Pages.SiteHelp,new List<RouteParam>(), new { accesskey = "6", onclick = "$jq.googlepush('Site Links', 'Footer','Help');" }, true)%></li>
                    <li>
                        <%= Html.NhsLink("Site Index", Pages.SiteIndex,new List<RouteParam>(), new { accesskey = "3", onclick = "$jq.googlepush('Site Links', 'Footer','Site Index');" }, true)%></li>
                    <li>
                        <%= Html.NhsLink("Mortgage Calculator", Pages.MortgageCalculator, new List<RouteParam>(), new { onclick = "$jq.googlepush('Site Links', 'Footer','Mortgage Calculator');" }, true)%></li>
                    <%if (Model.ShowAboutFooter)
                      { %>
                    <li>
                        <%= Html.NhsLink("About us", Pages.AboutUs, new List<RouteParam>(), new { onclick = "$jq.googlepush('Site Links', 'Footer','About us');" }, true)%></li>
                    <li>
                        <%} %>
                        <%= Html.NhsLink("Contact us", Pages.ContactUs, new List<RouteParam>(), new { onclick = "$jq.googlepush('Site Links', 'Footer','Contact us');" }, true)%></li>
                    <li>
                        <%= Html.NhsLink("Privacy policy", Pages.PrivacyPolicy,new List<RouteParam>(), new { onclick = "$jq.googlepush('Site Links', 'Footer','Privacy policy');" }, true)%></li>
                    <li class="nhs_Last">
                        <%= Html.NhsLink("Terms of use", Pages.TermsOfUse, new List<RouteParam>(), new { accesskey = "8", onclick = "$jq.googlepush('Site Links', 'Footer','Terms of use');" }, true)%></li>
                </ul>
            </nav>

            <p id="nhs_FooterLegal">
                <% if (@Model.Globals.PartnerLayoutConfig.AmIOnCommunityResultsPage)
                   {%>
                <text>
		                While Builders Digital Experience seeks to ensure that all of the information concerning new homes and new home communities, 
		                as well as other data on the site is current and accurate, we do not assume any liability for inaccuracies. It is your responsibility 
                        to independently verify information on the site.
		            </text>
                <% }%>
            Copyright &copy; 2001-<%=DateTime.Today.Year%>
            Builders Digital Experience, LLC. <a href="http://www.newhomesource.com/" onclick="$jq.googlepush('Site Links', 'Disclaimer','NewHomeSource.com');">NewHomeSource.com</a> is a trademark of <a target="_blank" href="http://www.thebdx.com" onclick="$jq.googlepush('Outbound Links', 'Disclaimer','Builders Digital Experience');">Builders Digital Experience, LLC</a> and all other marks are either trademarks or registered trademarks of their respective owners. All rights reserved.
            </p>

            <% } %>

            <% if (Model.MoveFooterVisible)
               {%>
            <!-- BDX - Move Footer //-->

            <hr class="nhs_ReaderAlt" />
            <div class="mv_FooterGroup">
                <p>
                    <%= Html.NhsLink("Find New Homes in Popular Cities:", Pages.SiteIndex, true)%>
                </p>
            </div>
            <%=Html.RenderSeoContent(SeoTemplateType.MNH_HomeV2, "footerlinks")%>
            <div class="nhs_Clear">
            </div>
            <nav class="mv_FooterGroup">
                Corporate: <a href="javascript:bookmark();">Bookmark this Site</a> | 
        <%= Html.NhsLink("Site Map", Pages.SiteIndex, new List<RouteParam>(), new { onclick = "$jq.googlepush('Site Links', 'Footer','Site Index');" })%> | 
        <%= Html.NhsLink("Help", Pages.SiteHelp, new List<RouteParam>(), new { onclick = "$jq.googlepush('Site Links', 'Footer','Help');" })%> | 
        <%if (Model.ShowRegistrationLinks)
          {%>
                <% if (!UserSession.UserProfile.IsLoggedIn())
                   { %>
                    <%= Html.NhsLink("Sign In", Pages.SignIn) %> |
                <% } %>
                <%else
                  { %>
                    <%= Html.NhsLink("Sign Out", Pages.LogOff) %> |
                <% } %>
        <%} %>
                <%= Html.NhsLink("Unsubscribe", Pages.Unsubscribe, new List<RouteParam>(), new { onclick = "$jq.googlepush('Site Links', 'Footer','Unsubscribe');" })%>
                <br />
                <%if (Model.ShowAboutFooter)
                  {%>
                <a href="http://www.move.com/company/corporateinfo.aspx" onclick="$jq.googlepush('Outbound Links', 'Footer','About Move');">About
            Move</a> | 
        <%} %>
                <%= Html.NhsLink("Contact Us", Pages.ContactUs, new List<RouteParam>(), new { onclick = "$jq.googlepush('Site Links', 'Footer','Contact us');" })%> | 
        <a href="http://www.move.com/company/linktous.aspx" title="Link to Us">Link to Us</a> | 
        <%= Html.NhsLink("List Your Homes", Pages.ListYourHomes, new List<RouteParam>(), new { onclick = "$jq.googlepush('Site Links', 'Footer','List your homes');" })%> | 
        <%= Html.NhsLink("Privacy Policy", Pages.PrivacyPolicy, new List<RouteParam>(), new { onclick = "$jq.googlepush('Site Links', 'Footer','Privacy policy');" })%> | 
        <%= Html.NhsLink("Terms of Use", Pages.TermsOfUse,new List<RouteParam>(), new { onclick = "$jq.googlepush('Site Links', 'Footer','Terms of use');" })%>
            </nav>
            <div class="mv_FooterGroup mv_FooterCopyright">
                <div>
                    &copy; <%=DateTime.Today.Year%> Builders Digital Experience, LLC. All rights reserved.
                </div>
                <% if (@Model.Globals.PartnerLayoutConfig.AmIOnHomePage)
                   { %>
                <div class="mv_FooterLegal">
                    <p>
                        While BDX works to ensure accurate new homes information, we do not assume any liability for inaccuracies.
                    </p>
                </div>
                <% } %>
            </div>
            <script type="text/javascript">
                function bookmark(A, B) {
                    if (A == null) {
                        A = window.location.href;
                    }
                    if (B == null) {
                        B = document.title;
                    }
                    if (document.all) {
                        window.external.AddFavorite(A, B);
                    } else {
                        if (window.sidebar) {
                            window.sidebar.addPanel(B, A, "");
                        } else {
                            window.alert("Press CTRL+T to bookmark this page");
                        }
                    }
                }
            </script>

            <div class="nhs_Clear">
            </div>

            <% } %>
        </div>
    </footer>
    <% if (!Model.NhsFooterVisible)
       {%>
</div>
<%} %>
