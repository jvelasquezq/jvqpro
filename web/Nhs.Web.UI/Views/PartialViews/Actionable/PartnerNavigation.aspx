﻿<%@ Page Language="C#" Inherits="Nhs.Web.UI.AppCode.Lib.ViewPageBase<Nhs.Web.UI.AppCode.ViewModels.PartialViewModels.PartnerNavigation>" %>
<%@ Import Namespace="Nhs.Mvc.Routing.Interface" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.HtmlHelpers"%>
<%@ Import Namespace="Nhs.Library.Constants"%>
<%@ Import Namespace="Nhs.Utility.Common"%>
<%@ Import Namespace="StructureMap.Query" %>

<nav id="nhs_PartnerNav">
    <ul>
		<li><%=Html.NhsLink("Search for new homes", Pages.BasicSearch)%></li>
		<% if (Model.NewHomeGuideLink) { %>
            <li>                
                <a href="<%=("/" + Pages.GetResourceCenterLink(NhsRoute.BrandPartnerId)).Replace("//","/") %>" target="_blank">New Home Guide</a>                
            </li>
        <% } %>
	</ul>
	<ul id="nhs_PartnerToolNav">

        <%if (Model.PartnerAllowsRegistration)
          {%>
        <%if (Model.SignInLink)
              {%>
            <li><%=Html.GetSignInLink(Model, new { onclick = "$jq.googlepush('Account Events','Create Account','Open Form - Header Link')" })%></li>

            <%if (!Model.PartnerUsesSso)
              { //if Partner uses SSO, don't show Create Account link%> 
                <li><%=Html.GetRegisterLink(Model, new { onclick = "$jq.googlepush('Account Events','Create Account','Open Form - Header Link')" })%></li>
            <%}%>

            <%
              }%>

        <%
              if (Model.SignOutLink)
              {%>
            <li><%=Html.NhsLink("Sign out", Pages.LogOff)%></li>
        <%
              }%>

        <%
          }%>

        <li id="nhs_AddThisBox">
            <%= Html.AddThisControl() %>

            <%--Save To Planner--%>
            <%--Html.RenderPartial(NhsMvc.PartialViews.Views.Common.SaveToPlannerHeart); --%>
        </li>
	</ul>
</nav>