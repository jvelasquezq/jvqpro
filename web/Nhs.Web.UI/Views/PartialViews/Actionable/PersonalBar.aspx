﻿<%@ Page Language="C#" Inherits="Nhs.Web.UI.AppCode.Lib.ViewPageBase<Nhs.Web.UI.AppCode.ViewModels.PartialViewModels.PersonalBar>" %>

<%@ Import Namespace="Nhs.Mvc.Routing.Interface" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.HtmlHelpers" %>
<%@ Import Namespace="Nhs.Library.Constants" %>
<%@ Import Namespace="Nhs.Utility.Constants" %>
<% if (Model.NoOfHomesVisible)
   { %>
<%= NhsRoute.PartnerId == (int)PartnersConst.Move ? "Previous Listings : " : "Your visit: "%>
<%= NhsRoute.PartnerId == (int)PartnersConst.Move
           ? "[" + Html.NhsLink(Model.NoHomesText, Pages.RecentItems).ToString() + "] Homes Viewed"
           : (Model.RecentItemsLinkVisible || Model.RecentItemsLinkActiveUserVisible) ? Html.NhsLink(Model.NoHomesText, Pages.RecentItems).ToString() : Model.NoHomesText
                %>
<% } %>&nbsp; <span id="divSuccessMessage" class="nhs_PersonalMessage" style="display: none">
</span>
<% if (Model.SuccessMessageVisible)
   { %>
<span class="nhs_PersonalMessage">
    <%= Model.SuccessMessageText%></span>
<% }%>
