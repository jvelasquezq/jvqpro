﻿<%@ Page Title="" Language="C#" Inherits="Nhs.Web.UI.AppCode.Lib.ViewPageBase<Nhs.Web.UI.AppCode.ViewModels.HomeViewModel>" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.HtmlHelpers" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.Helper" %>
<asp:content id="Content2" contentplaceholderid="cphHead" runat="server">
      
</asp:content>
<asp:content id="ContentLayer" contentplaceholderid="cphDataLayer" runat="server">      
    <%=HtmlGoogleHelper.GetGtmDataLayerForHomePage(Model)%> 
</asp:content>
<asp:content id="Content1" contentplaceholderid="cphMain" runat="server">
<div id="hwnhl_LeftCol">
    <div id="hwnhl_HomeModule">
        <h2 class="hwnhl_Title">Find your dream new home...</h2>
        <div class="fadein">
            <img src="<%=GlobalResourcesMvc.NewHomeListings.images.slideshow.homegallery1_jpg%>" alt="Find Your Dream New Home" />
            <img src="<%=GlobalResourcesMvc.NewHomeListings.images.slideshow.homegallery2_jpg%>" alt="Find Your Dream New Home" />
            <img src="<%=GlobalResourcesMvc.NewHomeListings.images.slideshow.homegallery3_jpg%>" alt="Find Your Dream New Home" />
            <img src="<%=GlobalResourcesMvc.NewHomeListings.images.slideshow.homegallery4_jpg%>" alt="Find Your Dream New Home" />
            <img src="<%=GlobalResourcesMvc.NewHomeListings.images.slideshow.homegallery5_jpg%>" alt="Find Your Dream New Home" /> 
            <%--<div class="next">Next</div> --%>          
		</div>        
    </div>

    <div id="hwnhl_HomeSearch">
        <h3 class="hwnhl_Title">Search Over 10,000 Communities from Over 2,000 Builders</h3>
        <%
            using (
                Html.BeginNhsForm(NhsMvc.Home.ActionNames.Show, NhsMvc.Home.Name, FormMethod.Post, "frmHomePage", false)
                )
            {%>
            <fieldset>
            <legend class="nhs_Reader">New Home Search</legend>  

            <p class="hwnhl_HomeSearchDesc">What are you looking for?</p>
            <%=Html.ValidationMessageFor(model => model.SearchText, "Please enter a valid location or select an item from the list.")%>
             <span class="field-validation-error" style="display: none;">Please enter a valid location (city, state, zip code) or community name.</span>
            <p id="hwnhl_HomeFormLocation">
                <label for="SearchText" class="nhs_Reader">City, St or Zip </label>
                <%=Html.TextBoxFor(model => model.SearchText, new { placeholder = @"Examples: ""Albany, NY"" ""90210"" ""Fox Run Estates""", @class = "nhs_LocationDefault", maxlength = 50 })%>
                <%=Html.Hidden("SearchType", string.Empty)%>
                <input type="submit" id="Search" value="VIEW PROPERTIES" class="btn btn_Search" onclick="$jq.googlepush('Search Events','Search Action','Search - Basic')"/>
            </p>               
            <p id="hwnhl_HomeFormPrice">
                <label class="nhs_Reader" for="PriceLow">Price from</label>
                <%=Html.DropDownListFor(model => model.PriceLow, Model.PriceLoRange, "Minimum Price",
                                                                       new { @class = "nhs_PriceFrom", onchange = "$jq.googlepushList(this, 1,'Search Events','Search Parameters','Select Price - Low');" })%>               
            <label class="nhs_Reader" for="PriceHigh"> to </label>
                <%=Html.DropDownListFor(model => model.PriceHigh, Model.PriceHiRange, "Maximum Price",
                                                                       new { @class = "nhs_PriceTo", onchange = "$jq.googlepushList(this, 1,'Search Events','Search Parameters','Select Price - High');" })%>              
            </p> 
            </fieldset>
            <%
            }%>

            <div id="hwnhl_HomeBoutiqueBox">
                <ul>
                    <li class="hwnhl_HomeHotDeals"><%=Html.NhsLink("Builder Specials", Pages.HotDealsSearch)%></li>
                    <li class="hwnhl_HomeQuickMove"><%=Html.NhsLink("Quick Move In", Pages.QuickMoveInSearch)%></li> 
                    <li class="hwnhl_HomeCustom"><%=Html.NhsLink("Custom Homes", Pages.BOYLSearch)%></li>                       
                </ul>
            </div>   
    </div>

    <div class="hwnhl_HomeSubModuleLeft">
		<h4 class="hwnhl_Title">About New Homes</h4>		
		<img alt="About New Homes" src="<%=GlobalResourcesMvc.NewHomeListings.images.homepage.newhomesabout_jpg%>" />
		<p>NewHomeListings.com is the leading marketplace focused specifically on the new home market. NewHomeListings.com provides consumers a large selection to find their dream home based on a wide selection of styles, locations, options and features that aren't available elsewhere.</p>		
	</div>
    <div class="hwnhl_HomeSubModuleRight">
		<h5 class="hwnhl_Title">Attention Developers</h5>		
		<img alt="Attention Developers" src="<%=GlobalResourcesMvc.NewHomeListings.images.homepage.developers_jpg%>" />
		<p>With thousands of communities and the largest selection of new homes, our site provides our users with a convenient and easy way to find what they are looking for.</p>
		<p>You can feature your community on NewHomeListings.com and quickly connect with motivated buyers. We are already working with some of the most recognized builders and developers.</p>
		<p>Contact us now to get started with your customized package!</p>		
	</div>


</div>
<div id="hwnhl_RightCol">
    <div class="hwnhl_HomeSubModule hwnhl_FeaturedPartner">
		<h4 class="hwnhl_Title">Featured Partner</h4>		
		<a target="_blank" href="http://www.eplans.com/"><img alt="ePlans" src="<%=GlobalResourcesMvc.NewHomeListings.images.eplans_logo_png%>" /></a>
	</div>

    <div class="hwnhl_HomeSubModule hwnhl_HomeSpotlight">
		<h4 class="hwnhl_Title">Spotlight Communities</h4>
        
        <%foreach (var comm in Model.SpotLightCommunities)
          {%>			
        <div class="nhs_SpotlightItem">
            <a href="<%:comm.Url%>"><img alt="<%:comm.CommunityName%><" src="<%:comm.SpotLightImage%>" /></a>
            <p>
            <%if (comm.HasVideo)
              {%>
            <a href="<%:comm.Url%>" rel="nofollow"><img src="<%=GlobalResourcesMvc.Default.images.icons.icon_video_png%>" alt="Video" /></a><br />
            <%}%>
            <strong><a <%: comm.HasHotHome? "class =\"nhs_SpotHotHomeCommName\"":string.Empty%> href="<%:comm.Url%>" class="&quot;nhs_SpotHotHomeCommName&quot;"><%:comm.CommunityName %></a></strong><br />
            <%: comm.BrandName%><br />
            <%: comm.City %>, <%= comm.State %><br />
            From <%:comm.PriceLow.ToString("C0")%></p>
        </div>
        <%}%>
	</div>
</div>

<%=ResourceCombinerEnum.HOMEPAGEJS.GetCrunchedResource() %>

<script type="text/javascript">
    $jq(document).ready(function () {

        var search = new NHS.Scripts.Search('/<%=Model.TypeAheadUrl %>');
        search.setupSearchBox("SearchText", "SearchType");

        homePageJs = new NHS.Scripts.HomePage(search);
        homePageJs.initialize();
        homePageJs.initNhlSlideShow();

        
    });                      
</script>

</asp:content>
