﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Nhs.Web.UI.AppCode.ViewModels.HomeResultViewModel>" %>
<%@ Import Namespace="Nhs.Mvc.Routing.Interface" %>
<%@ Import Namespace="Nhs.Search.Objects" %>
<%@ Import Namespace="Nhs.Search.Objects.Constants" %>
<%@ Import Namespace="Nhs.Utility.Common" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.HtmlHelpers" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.Extensions" %>

<%--Home Image --%>
<div class="nhs_GalleryCellImgBox">
    <%=Html.HomeDetailImageLink(Model.HomeResultRow, false, Model.PreviewMode)%>
    <%if (Model.HomeResultRow.ImageCount > 1) {%>
        <span class="nhs_GalleryImgOverlay"><%=Model.HomeResultRow.ImageCount %> photos</span>
    <%}%>
</div>
<div class="specs">
   <p class="propertyTitles">
   <%--Title Part--%>
       <%=Html.HomeDetailLink(Model.HomeResultRow, Model.PreviewMode)%>       
   </p>
    <div class="specsLeft">

        <%if(Model.HomeResultRow.HomeStatus != (int) HomeStatusType.ModelHome){ %>
            <%--Price Section--%>
            <p class="price"><%: Model.HomeResultRow.Price.FormatPrice(Model.HomeResultRow.SpecId) %></p>
        <%} %>
            <%--Square Footage--%>
        <%if (Model.HomeResultRow.FormatSquareFootage() != string.Empty)
          {%>
        <p class="squareFootage">
            <%=Model.HomeResultRow.FormatSquareFootage()%> </p>
        <%}%>
        <p class="BedBath">
            <%--BedRooms--%>
            <%=Model.HomeResultRow.FormatBedrooms() %>

            <%--BathRooms--%>
            <%=Model.HomeResultRow.FormatBathrooms()%>
            
            <%--Garage--%>
            <%=Model.HomeResultRow.FormatGarages()%>        
        </p>
    </div>
    <div class="specsRight">
        <%--Hot home--%>
        <%if (Model.HomeResultRow.ImageCount > 0) {%>
        <p class="media_icon"><%=Model.HomeResultRow.ImageCount + (Model.HomeResultRow.ImageCount > 1 ? " Photos" : " Photo") %></p>
        <%}%>  

        <%if (Model.HomeResultRow.IsHotHome && NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
            {%>
            <span class="nhs_DetailHotHomeIcon"></span>
        <%}%>

        <%if (Model.HomeResultRow.VirtualTourExists(Model.HomeResultRow.Plan, Model.HomeResultRow.Spec))
            {%>
            <p class="vitual_tour_icon">Virtual Tour</p> 
        <%
            }%>

        <%if(Model.HomeResultRow.FloorPlanExists(Model.HomeResultRow.Plan, Model.HomeResultRow.Spec, Model.PreviewMode))
            {%>
            <p class="floor_plan_icon">Floor Plan</p> 
        <%
            }%>
    </div>
    <p>
        
        <%if (Model.BcType.ToLower() != BuilderCommunityType.ComingSoon.ToLower() || Model.Globals.PartnerLayoutConfig.IsBrandPartnerNhsPro)
          {%>
            <%--Home Status: Ready to Build/Ready To Move in/ etc.--%>
            <% var status = Model.HomeResultRow.HomeStatus();%>
            <%= Html.NhsAsyncImage(status["url"], status["desc"], false, false) %>
        <% } %>
        
    </p>  
     </div>
