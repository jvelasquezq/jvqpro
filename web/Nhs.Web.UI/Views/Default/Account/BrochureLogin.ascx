﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Nhs.Web.UI.AppCode.ViewModels.LoginViewModel>" %>
<%@ Import Namespace="Nhs.Library.Constants" %>
<%@ Import Namespace="Nhs.Mvc.Routing.Interface" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.HtmlHelpers" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>
<%@ Import Namespace="System.Web.Mvc.Ajax" %>
<%@ Import Namespace="Nhs.Utility.Common" %>
<div id="divSignIn" class="nhs_SignIn">
    <div class="nhsLoginForm">
        <% using (Ajax.BeginNhsForm(NhsMvc.Account.ActionNames.BrochureLogin, new AjaxOptions { UpdateTargetId = "divLogin", OnSuccess = "function() { tb_init('#divLogin a.thickbox'); }" }, false))
           {%>
        <fieldset>
            <p>
                <span class="nhs_DetailsFormTopTitle">Sign In</span>
                <span class="nhs_DetailsFormTopTitleSmall">
                <%if (NhsRoute.BrandPartnerId == PartnersConst.Pro.ToType<int>())
                {%>
                    <%:"Please login or register to take advantage of this functionality and more."%>
                 <%}
                 else
                {%>
                    <%:"Enter your account information to continue."%>
               <% }%>
                </span></p>
            <ol>                
                <li>
                    <label for="Email">
                        Email address:</label><%= Html.TextBoxFor(m=>m.Email)%></li>
                <li>
                    <label for="Password">
                        Password:</label><%= Html.PasswordFor(m=>m.Password) %></li>
                <li class="nhs_LeadFormButtons"><%=Html.HiddenFor(m => m.FromPage, new { value=Model.FromPage })%>
                    <input type="submit" id="btnLogin" value="Sign In" onclick="" class="btn signIn" />
                    <input type="button" id="btnCancel" value="Cancel" class="btn cancel" onclick="hideLogin();return false;" /> </li>
                <li class="nhs_LeadFormButtons forgotPassword"><%=Html.NhsModalWindowLink("Forgot password?", Pages.ForgotPasswordModal, ModalWindowsConst.RecoverPasswordModalWidth, ModalWindowsConst.RecoverPasswordModalHeight)%></li>
            </ol>
            <%= Html.ValidationSummary() %>
        </fieldset>
        <% } %>
    </div>
</div>
