﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Nhs.Web.UI.AppCode.ViewModels.RegisterViewModel>" %>
<%@ Import Namespace="Nhs.Library.Enums" %>
<%@ Import Namespace="Nhs.Library.Web" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.HtmlHelpers" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>
<%@ Import Namespace="System.Web.Mvc.Ajax" %>
<%@ Import Namespace="StructureMap.Query" %>

<div id="divRegisterForm" class="nhsRegisterForm">
    <% using (Ajax.BeginNhsForm(NhsMvc.Account.ActionNames.BrochureRegister, new AjaxOptions { UpdateTargetId = "divRegisterForm" }, false))
       {%>
    <fieldset>
        <p>
            <span class="nhs_DetailsFormTopTitle">Create an account</span>
            <span class="nhs_DetailsFormTopTitleSmall">Enter your information to continue.</span></p>
        <ol>

        <%
           var showLongerForm = string.IsNullOrEmpty(UserSession.UserProfile.LogonName);

           if (showLongerForm)
           {%>
            <li>
                <label for="FirstName">First Name:</label>
                <%=Html.TextBoxFor(m => m.FirstName)%></li>
            <li>
                <label for="LastName">Last Name:</label>
                <%=Html.TextBoxFor(m => m.LastName)%></li>
            <li>
                <label for="EmailReg">Email Address:</label>
                <%=Html.TextBoxFor(m => m.Email, new {id = "EmailReg"})%></li>
            <li>
                <label for="ConfirmEmail">Confirm Email Address:</label>
                <%=Html.TextBoxFor(m => m.ConfirmEmail)%></li>
        <%}%>
 
            <li>
                <label for="PasswordReg">Password:</label>
                <%=Html.PasswordFor(m => m.Password, new {id = "PasswordReg"})%></li>
            <li>
                <label for="ConfirmPassword">Confirm Password:</label>
                <%=Html.PasswordFor(m => m.ConfirmPassword)%></li>
            
          <%if (showLongerForm)
          {%>
            <li>
                <label for="ZipCodeReg">My Zip Code:</label>
                <%=Html.TextBoxFor(m => m.ZipCodeReg)%></li>
            <li class="checkBoxes">
                <span class="createAccountCheckBox"><%=Html.CheckBoxFor(m => m.LiveOutsideReg,
                                              new {onclick = "toggleRequestCreateAccountZipField()"})%></span>
                <label for="LiveOutsideReg" class="createAccountLabels"> I live outside the US</label></li>
        <%}%>

            <li class="nhs_LeadFormButtons">
                <br />
                <%=Html.HiddenFor(m => m.FromPage, new { value=Model.FromPage })%>
                <input type="submit" class="btn createAccount" value="Create account" name="btnLogin" onclick="" />
                <input type="button" class="btn cancel" value="Cancel" name="btnCancel" onclick="hideCreateAccount();return false;" /></li>
        </ol>
    </fieldset>
    <%= Html.ValidationSummary() %>
    <%}%>
</div>
