<%@ Page Title="" ValidateRequest="false" Language="C#" Inherits="System.Web.Mvc.ViewPage<Nhs.Web.UI.AppCode.ViewModels.CommunityResultsViewModel>" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.HtmlHelpers" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="Nhs.Library.Common" %>
<%@ Import Namespace="Nhs.Library.Helpers.Utility" %>
<%@ Import Namespace="Nhs.Mvc.Routing.Interface" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.Helper" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="Nhs.Utility.Constants" %>
<asp:content id="ContentLayer" contentplaceholderid="cphDataLayer" runat="server">      
    <%=HtmlGoogleHelper.GetGtmDataLayerForCommResults(Model)%>
        </asp:content>
<asp:content id="Content2" contentplaceholderid="cphHead" runat="server">
    <%=ResourceCombinerEnum.COMMUNITYRESULTSJS.GetCrunchedResource() %>
</asp:content>
<asp:content id="Content1" contentplaceholderid="cphMain" runat="server">
    

<div id="nhs_BreadcrumbArea">    
    <%Html.RenderAction(NhsMvc.PartialViews.ActionNames.BreadCrumb, NhsMvc.PartialViews.Name);%>
</div>
<div id="nhs_CommResContent">

    <%--Results area--%>
    <div id="nhs_CommResMain">        
        <div id="nhs_CommResMapMain">
            <%Html.RenderPartial(NhsMvc.Default.Views.CommunityResults.Map);%>
        </div>
        <div id="nhs_CommResData">
            <%Html.RenderPartial(NhsMvc.Default.Views.CommunityResults.ResultData);%>            
        </div>
    </div>

    <%--SEO Box--%>    
    
    <div class="nhs_CommResContentBox">
         <%=Html.RenderSeoContent(Model.Market.MarketName.Replace(" ", ""), Model.City, Model.State, Model.County, Model.Zip, Model.CommunityName,string.Empty,string.Empty,  Model.SeoContentTags, Model.SeoTemplateType, "footer")%>
    </div>
    

    <div class="nhs_Clear"></div>

    <%--Copyright Text--%>
    <p class="nhs_LegalText nhs_CommResLegal">
    <%if (Model.IsMove)
      {%>  
        <% Html.RenderPartial(NhsMvc.PartnerBrandGroupViews_333.Views.CommunityResultsV2.PartialCopyrightDisclaimer); %>
      <% }%>
    </p>
    <div class="nhs_Clear"></div>

</div>
<div id="nhs_Loading">
    <%--<img alt="loading bar" src="<%=Resources.GlobalResourcesMvc.Default.images.loading_bar_gif%>" />--%>
    <p>
        <strong>Loading...</strong>
    </p>
</div>
<div id="windowRetUserBro"></div>
<input id="hdnAdCurrentMiddle" type="hidden" value="" />
<input id="hdnAdCurrentMiddle3" type="hidden" value="" />
<input id="hdnAdCurrentRight2" type="hidden" value="" />

<!-- MAPQUEST TOOLKIT -->
 <%=Html.GetGoogleMapScript()%>
<script type="text/javascript">    
    var commResults;
    var searchTypeahead;
    history.navigationMode = 'compatible';
    $jq(document).ready(function() {

        // Once the doc is ready, loads the map.
        var parameters = {
            searchUrl:'<%=UserSession.GetItem("SearchUrl").ToString() %>',
            partnerID: <%= NhsRoute.PartnerId %>,
            validateSearchTextFunction : null,
            brandPartnerId: <%= NhsRoute.BrandPartnerId %>,
            siteRoot: '/<%= (string.IsNullOrEmpty(NhsRoute.PartnerSiteUrl)? "" : (NhsRoute.PartnerSiteUrl + "/")) %>',
            communityPage: '<%= NhsMvc.CommunityDetail.Name %>',
            basicListingPage: '<%= Pages.BasicDetail %>',
            basicCommPage: '<%= Pages.BasicCommunity %>',
            fromPage: '<%= NhsRoute.CurrentRoute.Function %>',
            resourceRoot: '<%= Nhs.Library.Common.Configuration.ResourceDomain %>/',
            marketId: <%= Model.Market.MarketId %>,
            urlForBrosure : '/<%= (string.IsNullOrEmpty(NhsRoute.PartnerSiteUrl)? "" : (NhsRoute.PartnerSiteUrl + "/")) %>communityresults/addremovemultibrochure',
            currentPageForBrosure : <%=Model.CurrentPage %>,
            searchAction: '<%= "/" + NhsRoute.PartnerSiteUrl + "/" + NhsRoute.CurrentRoute.Function + "/" + NhsMvc.CommunityResults.ActionNames.Search + "/" + NhsRoute.CurrentRoute.Params.ToUrl() %>',
            adAction: '<%= "/" + NhsRoute.PartnerSiteUrl + "/common/Advertisement" %>',
            autoCompleteAction: '/<%= (string.IsNullOrEmpty(NhsRoute.PartnerSiteUrl)? "" : (NhsRoute.PartnerSiteUrl + "/")) + NhsMvc.PartialViews.ActionNames.PartnerLocationsIndex %>',
            tabsControlId: 'nhs_CommResTabs',
            loadingControlId: 'nhs_Loading',
            pageSize: <%= UserSession.PageSize %>,
            maxResults: <%= Nhs.Library.Common.Configuration.MaxMapPoints %>,
            freeBrochureLink: '<%=Html.GetMapRequestInfoLinkCr()%>',
            requestPromoLink: '<%=Html.GetMapRequestPromoLinkCr()%>',
            requestAppointmentLink: '<%=Html.GetMapRequestApptLink()%>',
            method:<%=Model.Globals.GetAddCommunityItemToPlannerUrl() %>,
            getMapCardAction: '<%=Html.GetActionUrl(NhsMvc.CommunityResults.Name, NhsMvc.CommunityResults.ActionNames.GetCommunityMapCards)%>',
            icon: '<%= (Model.Globals.PartnerLayoutConfig.IsBrandPartnerMove? GlobalResources14.Default.images.icons.map_poi_green_png : GlobalResources14.Default.images.icons.map_poi_blue_png )%>',
            iconMulti: '<%= (Model.Globals.PartnerLayoutConfig.IsBrandPartnerNhs ? GlobalResources14.Default.images.icons.map_plus_blue_png : GlobalResources14.Default.images.icons.map_plus_green_png)%>',
            iconBasic: '<%= GlobalResources14.Default.images.icons.map_poi_gray_png%>',
            iconMultiBasic: "@GlobalResources14.Default.images.icons.map_plus_gray_png",
            Map: {
                minLat: <%= Model.Map.MinLat.ToString(new NumberFormatInfo()) %>,
                minLng: <%= Model.Map.MinLng.ToString(new NumberFormatInfo()) %>,
                maxLat: <%= Model.Map.MaxLat.ToString(new NumberFormatInfo()) %>,
                maxLng: <%= Model.Map.MaxLng.ToString(new NumberFormatInfo()) %>,
                centerLat: <%= Model.Map.CenterLat.ToString(new NumberFormatInfo()) %>,
                centerLng: <%= Model.Map.CenterLng.ToString(new NumberFormatInfo()) %>,
                zoomLevel: <%= Model.Map.ZoomLevel %>
            },

            galleryParams: {
                marketID: <%= Model.Market.MarketId %>,
                marketName: '<%= Model.Market.MarketName %>',
                imgBaseUrl: '<%= Nhs.Library.Common.Configuration.ResourceDomain %>/',
                imgStaticUrl: '<%=Nhs.Library.Common.Configuration.ResourceDomain %>/',
                mediaObjs: <%=Model.ShowCaseMediaObjects.ToJson() %>,
                container: 'nhs_VideoWrapperNew',
                titleBarID: 'nhs_ImagePlayerTitle',
                captionBarID: 'nhs_ImagePlayerCaption',
                imgtitleBar: 'nhs_MediaImageTitle',
                firstImgUrl: '<%=Model.FirstImage %>',
                description: '<%=HttpUtility.JavaScriptStringEncode(Model.PinterestDescription) %>',
                url: '<%=HttpUtility.HtmlEncode(Model.Globals.CurrentUrl)%>',
                type: 'results',
                videoWidth : 300, 
                videoHeight : 200,
                brightcoveToken : '<%=Nhs.Library.Common.Configuration.BrightcoveReadToken %>',
                brightCoveSuffix : '<%=Nhs.Library.Common.Configuration.BrightcoveEnvSuffix%>',
                isPro: <%=Model.Globals.PartnerLayoutConfig.IsBrandPartnerNhsPro ? 1: 0%>,
                isCommResults: 1,
                CommunityGalleryText: "<%=LanguageHelper.CommunityGallery.ToMvcHtmlString()%>",
                CommunityVideoGalleryText: "<%=LanguageHelper.CommunityVideoGallery.ToMvcHtmlString()%>",
                TourVideoGalleryText: "<%=LanguageHelper.TourVideoGallery.ToMvcHtmlString()%>",
                HomeGalleryText: "<%=LanguageHelper.HomeGallery.ToMvcHtmlString()%>",
                CommunityImageText: "<%=LanguageHelper.CommunityImage.ToMvcHtmlString()%>",
                HomeImageText: "<%=LanguageHelper.HomeImage.ToMvcHtmlString()%>",
                NowShowingText: "<%=LanguageHelper.NowShowing.ToMvcHtmlString()%>",
                HomeVideoText: "<%=LanguageHelper.HomeVideo.ToMvcHtmlString()%>",
                CommunityVideoText: "<%=LanguageHelper.CommunityVideo.ToMvcHtmlString()%>",
                ElevationText: "<%=LanguageHelper.Elevation.ToMvcHtmlString()%>",
                VirtualTourText: "<%=LanguageHelper.VirtualTour.ToMvcHtmlString()%>",
                FloorPlanText: "<%=LanguageHelper.FloorPlan.ToMvcHtmlString()%>",
                ExternalVideoText: "<%=LanguageHelper.ExternalVideo.ToMvcHtmlString()%>",
                PlanViewerText: "<%=LanguageHelper.PlanViewer.ToMvcHtmlString()%>",
                InteriorText: "<%=LanguageHelper.Interior.ToMvcHtmlString()%>",
                ExteriorText: "<%=LanguageHelper.Exterior.ToMvcHtmlString()%>",
                CommunityLotMapText: "<%=LanguageHelper.CommunityLotMap.ToMvcHtmlString()%>",
                OfText: "<%=LanguageHelper.Of.ToMvcHtmlString()%>"
            },

            searchParameters: <%= Model.SearchParametersJSon %>,
            partnerSiteUrl: '<%=NhsRoute.PartnerSiteUrl%>',
            listHubBasicListinIds: <%=Model.ListHubBasicListinIds%>,
            listHubProviderId: '<%= Model.ListHubProviderId%>',
            listHubTestLogging: <%= Model.ListHubTestLogging%>,
            desckey: '<%=LogImpressionConst.BasicListingCommunitySeasrchDisplay%>',
            listHubBasicListinIdsList: '<%= Model.ListHubBasicListinIdsList%>'
        };

        searchTypeahead = new NHS.Scripts.Search('/<%= Model.TypeAheadUrl%>');
        searchTypeahead.setupSearchBox("LocationName", "LocationType");
        parameters.validateSearchTextFunction = searchTypeahead.showErrorSearchTextMessage;
        commResults = new NHS.Scripts.CommunityResults.CommResults(parameters);
        commResults.initialize();
        commResults.AddRemoveBrochure("/<%= (string.IsNullOrEmpty(NhsRoute.PartnerSiteUrl)? "" : (NhsRoute.PartnerSiteUrl + "/")) %>communityresults/addremovemultibrochure",<%= Model.Market.MarketId %>, <%=Model.CurrentPage %>);
                
       
    });    
</script>

<% if (NhsRoute.PartnerId == (int)PartnersConst.Move || NhsRoute.PartnerId == (int)PartnersConst.NewHomeSource)
   { %>
   
    <input type="hidden" name="ShowAlternateImage" id="ShowAlternateImage" value="false" />

<div id="nhs_valuesRecoReturnUserBrochure">
    <input type="hidden" name="PopUpHeaderRetUser" id="PopUpHeaderRetUser" value="" />
    <input type="hidden" name="PopUpExtraInfoRetUser" id="PopUpExtraInfoRetUser" value="" />
    <input type="hidden" name="DefaultChecked" id="DefaultChecked" value="false" />
    <input type="hidden" name="MaxRecs" id="MaxRecs" value="0" />
    <input type="hidden" name="toggle" id="toggle" value="off" />
    <input type="hidden" name="MatchEmail" id="MatchEmail" value="False" />
</div>
<%=Html.Hidden("PopUpTimeout", "3", new { id = "PopUpTimeout"  })%>
<script type="text/javascript">
    var win;
    $jq(document).ready(function() {
        VWOReady(function() {
            setTimeout("ShowPopupWithTimeout()", 500);
            setTimeout("commResults.ChangeCommuntyImage()", 500);
        });
    });

  

    function ValidToShowPopUp() {
        var showWindow = false;
        var showReturnUserModal = <%=Model.ShowReturnUserModal.ToString().ToLower() %>;
        if ($jq('#toggle').val() == "on" || showReturnUserModal) {
            var isValidCommId = parseInt($jq.cookie('<%=(CookieConst.LastCommunityView + "_" + NhsRoute.PartnerId) %>')) > 0;
            var wasAlreadyShow = <%=UserSession.PopUpRetUserBroshure.ToString().ToLower() %>;
            var hasAlreadyBroshure = <%=Model.AlreadyRequestBrosurePreviosComm.ToString().ToLower() %>;
            var hasCommtoReco = <%=Model.HasCommToRecommend.ToString().ToLower() %>;
            var hasValidMarket = <%=Model.HasTheSameMarcketId.ToString().ToLower() %>;
            var isNewSession = <%=Model.IsNewSession.ToString().ToLower() %>;
            showWindow = showReturnUserModal || (isValidCommId && (wasAlreadyShow == false) && (hasAlreadyBroshure == false) && hasCommtoReco && hasValidMarket && isNewSession);
        }
        return showWindow;
    }

    function ShowPopupWithTimeout() {
        if (ValidToShowPopUp()) {
            var timeout = parseInt($jq('#PopUpTimeout').val());
            setTimeout("showPopUpRetUser()", timeout);
        }
    }           

    function showPopUpRetUser() {
        var data = $jq("#nhs_valuesRecoReturnUserBrochure input").serialize()+"&width=<%=ModalWindowsConst.RecoRetunUserWidth%>&height=<%=ModalWindowsConst.RecoRetunUserHeight%>";
        tb_show('', '<%=Html.GetActionUrl(NhsMvc.CommunityResults.Name, NhsMvc.CommunityResults.ActionNames.ShowModalRecoRetunUserBroshure)%>?' + data, false);
    }
</script>
  <% } %>
</asp:content>
