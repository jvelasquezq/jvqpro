﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Nhs.Web.UI.AppCode.ViewModels.CommunityResultsViewModel>" %>
<%@ Import Namespace="Nhs.Library.Constants" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.Extensions" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.HtmlHelpers" %>


<div id="nhs_BuilderPromoBrands">
    <p id="nhs_BuilderPromoHeader">Find communities by builder</p>
    <div>        
    <ul>
    <% foreach (var brand in Model.BrandsPromoted) { %>
        <li>
        <%= Html.RenderPromotedBrandsUrl(Model, brand)%>
        <%--<%=Html.NhsImageLink(Nhs.Library.Common.Configuration.ResourceDomain + b.BrandThumbnail(), Pages.CommunityResults, b.CdParams(Model.Market.MarketId), null, new { alt = b.BrandName, rel = b.BrandId.ToString(), onclick = "commResults.get_log().logEvent('CRBR',0," + b.BrandId + ");" }, true)%>--%>
        </li>
    <%}%>    
    </ul>
    </div>
</div>

<div id="nhs_BuilderPromoList">
    <br />
    <ul>
    <%foreach (var b in Model.BrandsCol1) {%>
        <li><%= Html.GetBrandUrl(b, Model) %></li>
    <%}%>
    </ul>
    <ul>
    <%foreach (var b in Model.BrandsCol2) {%>
        <li><%= Html.GetBrandUrl(b, Model) %></li>
    <%}%>
    </ul>
    <%--<ul>
    <%foreach (var b in Model.BrandsCol3) {%>
        <li><%=Html.NhsLink(b.BrandName, Pages.CommunityResults, b.CdParams(Model.Market.MarketId), new { @class = "nhs_Facet", title = b.BrandName, rel = b.BrandId.ToString(), onclick = "commResults.get_log().logEvent('CRBR',0," + b.BrandId + ");" })%></li>
    <%}%>
    </ul>--%>
</div>
<p id="nhs_BuilderPromoLink"><a id="nhs_FacetLinkBuilder" onclick="$jq.googlepush('Search Events', 'Search Refinement', 'All Builders');">All area builders ></a></p>

