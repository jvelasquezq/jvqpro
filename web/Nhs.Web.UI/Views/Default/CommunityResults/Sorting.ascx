﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Nhs.Web.UI.AppCode.ViewModels.CommunityResultsViewModel>" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.ViewModels" %>
<%@ Import Namespace="Nhs.Search.Objects.Constants" %>

<div class="nhs_Sorting">
    <p><label for="nhs_SortingSelect">Sort by: </label>
    <select id="nhs_SortingSelect">
        <option type="Random" <%=(Model.CurrentSortOrder == SortOrder.Random) ? "selected=\"selected\"" : ""%> value="Random">Default</option>

        <%if (!Model.IsAOnlyBasicListingsMarket)
         { %>
            <option type="Number of Homes" <%=(Model.CurrentSortOrder == SortOrder.HomeMatches) ? "selected=\"selected\"" : ""%> value="HomeMatches"># of Homes</option>
        <%} %>
        <option type="Price" <%=(Model.CurrentSortOrder == SortOrder.Price) ? "selected=\"selected\"" : ""%> value="Price">Price</option>
        <option type="Location" <%=(Model.CurrentSortOrder == SortOrder.Location) ? "selected=\"selected\"" : ""%> value="Location">Location</option>
        <%if (!Model.IsAOnlyBasicListingsMarket)
         { %>
        <option type="Builder" <%=(Model.CurrentSortOrder == SortOrder.Builder) ? "selected=\"selected\"" : ""%> value="Builder">Builder</option>
        <%} %>
        <option type="Community Name" <%=(Model.CurrentSortOrder == SortOrder.Name) ? "selected=\"selected\"" : ""%> value="Name">Community Name</option>
    </select>
    </p>
</div>    
