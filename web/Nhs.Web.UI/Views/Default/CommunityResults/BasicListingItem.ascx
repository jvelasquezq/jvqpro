﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Nhs.Library.Business.ExtendedCommunityResult>" %>
<%@ Import Namespace="Nhs.Library.Common" %>
<%@ Import Namespace="Nhs.Library.Constants" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.Extensions" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.HtmlHelpers" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.ViewModels" %>


<% if (Model.ShowBasicListingIndicator)
    {%>
<div id="pnlGroupingBarBasicListings" class="nhs_BasicListingBar">
    <p>Other Homes for Sale</p> 
</div>
<div class="nhs_Clear"></div>
<%}%>

<div class="nhs_ResultBasicItem" id="nhs_ResultBasicItem<%=Model.BasicListingInfo.ListingId%>"
    onmouseover="$jq(this).addClass('nhs_ResultBasicItemOn');"
    onmouseout="$jq(this).removeClass('nhs_ResultBasicItemOn');"
    onclick="window.location.href='<%=String.Format("/{0}/{1}/{2}",Pages.BasicDetail,Model.BasicListingInfo.ListingId,Model.BlParamsBasicListingHref())%>'; ">
    <div class="nhs_ResultThumb">
        <%=Html.NhsActionImageLink(Model.BasicListingThumbnail(), NhsMvc.BasicHomeDetail.Show(Model.BasicListingInfo.ListingId, Model.BlParamsBasicListingHref()), null, new { width = "75", height = "58" })%>
    </div>
    <p class="nhs_ResultHomeName">
        <%= Html.NhsActionLink(Model.BasicListingInfo.BasicListingText, NhsMvc.BasicHomeDetail.Show(Model.BasicListingInfo.ListingId, Model.BlParamsBasicListingHref()), new { title = Model.CommunityName, onclick = "NHS.Scripts.Helper.stopBubble(event);" })%>
    </p>
    <p class="nhs_ResultHomePrice">
        <%=Model.PriceHigh.ToString("$###,####")%>
    </p>
      <%if (Model.BasicListingInfo.DisplayAddress)
        {%>
    <p class="nhs_ResultHomeBedPriceHot">
      
              <%=Model.BasiListinItemLocation()%>
    </p> <%}%>
    <p class="nhs_ResultHomeBedBath">
        <%=Model.BasicListingBedrooms() %>
        <%=Model.BasicListingBathrooms() %>
    </p>
    <div class="nhs_Clear">
    </div>
</div>
