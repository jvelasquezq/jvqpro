﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Nhs.Web.UI.AppCode.ViewModels.CommunityResultsViewModel>" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>
<%@ Import Namespace="Nhs.Library.Constants" %>
<%@ Import Namespace="Nhs.Library.Enums" %>
<%@ Import Namespace="Nhs.Library.Web" %>
<%@ Import Namespace="Nhs.Search.Objects.Constants" %>
<%@ Import Namespace="Nhs.Utility.Common" %>
<%@ Import Namespace="Nhs.Mvc.Routing.Interface" %>
<%@ Import Namespace="Nhs.Search.Objects" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.HtmlHelpers" %>
<%@ Import Namespace="StructureMap.Query" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.Extensions" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="Nhs.Library.Helpers.Content" %>
<%@ Import Namespace="Nhs.Mvc.Routing.Configuration" %>
<%@ Import Namespace="Nhs.Utility.Constants" %>
<div id="nhs_Facets">
    <p id="nhs_FacetsRefine">
        <%if (!Model.IsMove)
          {%>
        Refine my search
        <% }
          else
          {%>
        Refine search
        <%  }%>
    </p>
    <div id="nhs_FacetLocation" class="nhs_FacetGroup">
        <p class="nhs_FacetGroupTitle">
            <strong>Location</strong></p>
        <%if (!Model.IsMapSearch)
          {%>
        <p>
            <%=Html.Hidden("LocationType", string.Empty)%>
            <%=Html.TextBoxFor(m => m.LocationName, new { @class = "nhs_LocationField", maxlength = 50, onkeypress = "if($jq(this).text() != 'Invalid Location') $jq(this).css('color', ''); ", Style = (Model.LocationName == "Invalid Location") ? "color : Red " : "" })%>
            <input id="btnGo" value="Go" type="submit" /></p>
        <div id="nhs_FacetLocationName">
            <a id="nhs_FacetLinkCities" href="javascript:"><span>
                <%= (Model.IsMarketSearch)? "Cities..." : "Related Area"%></span></a>
        </div>
        <div class="nhs_Clear">
        </div>
        <ul>
            <%  foreach (var city in Model.Results.CityFacet.FacetOptions)
                {
                    if (city.Selected)
                    {%>
            <li><span rel="" onmouseover="$jq(this).addClass('nhs_Hover');" onmouseout="$jq(this).removeClass('nhs_Hover');"
                class="<%= (city.Text.IndexOf("All ") != -1 || city.Text.IndexOf("Include ") != -1) ? "nhs_FacetAny" : "nhs_FacetRemove" %>">
                <%= city.Text%></span></li>
            <% }
                } %>
        </ul>
        <%}
          else
          { %>
        <ul>
            <li><span rel="latlong" onmouseover="$jq(this).addClass('nhs_Hover');" onmouseout="$jq(this).removeClass('nhs_Hover');"
                class="nhs_FacetRemove">My map search area</span></li>
        </ul>
        <p class="nhs_FacetLocationBack">
            <a href="javascript:" rel="latlong" class="nhs_FacetRemove" title="<%= Model.LocationName %>">
                Go back to
                <%= string.IsNullOrEmpty(Model.LocationName) ? UserSession.GetItem("Location").ToString() : Model.LocationName%></a></p>
        <% } %>
        <div id="nhs_FacetModalCities" class="nhs_FacetMenuBox">
            <div>
                <span class="nhs_FacetModalClose"></span>
                <ul>
                    <%  foreach (var city in Model.Results.CityFacet.FacetOptions)
                        {%>
                    <li <%= (city.Text == "All areas" && !Model.IsMarketSearch)? "class=\"separator\"" : "" %>>
                        <a rel="<%= string.IsNullOrEmpty(city.Value)? "0" : city.Value  %>" class="<%= (city.Actionable? "nhs_Facet" : "nhs_FacetEmpty") %>"
                            title="<%= city.Text%>">
                            <%=  (city.Text == "All areas" && !Model.IsMarketSearch) ? ("Related area: Greater " + Model.Market.MarketName + " " + Model.Market.StateAbbr) : city.Text%></a></li>
                    <% } %>
                </ul>
            </div>
        </div>
        <%// Update map coordinates  when there is a change in the cities facet %>
        <script type="text/javascript">
            var tempvar = ''
            $jq(document).ready(function () {
                setTimeout(function () {
                    if('<%=Model.LocationName %>' != '')
                        commResults.set_lastLocationName('<%=Model.LocationName %>');
                }, 100);
            });


            <% if (Model.LocationChanged && Model.IsAjaxRequest)
            {%>
            var searchParameters = commResults.get_searchParameters();
            var map = commResults.get_map();
            searchParameters.Radius = <%= UserSession.PropertySearchParameters.Radius %>;
            searchParameters.SortOrder = <%= UserSession.PropertySearchParameters.SortOrder.ToType<int>() %>;
            map._mqMinLat = <%= Model.Map.MinLat.ToString(new NumberFormatInfo()) %>;
            map._mqMinLng = <%= Model.Map.MinLng.ToString(new NumberFormatInfo()) %>;
            map._mqMaxLat = <%= Model.Map.MaxLat.ToString(new NumberFormatInfo()) %>;
            map._mqMaxLng = <%= Model.Map.MaxLng.ToString(new NumberFormatInfo()) %>;
            map._mqCenterLat = <%= Model.Map.CenterLat.ToString(new NumberFormatInfo()) %>;
            map._mqCenterLng = <%= Model.Map.CenterLng.ToString(new NumberFormatInfo()) %>;
            <% } %> 
        </script>
        <br />
        <div id="nhs_FacetLocationSlider">
        </div>
        <p id="nhs_FacetLocationText">
            Exact Location</p>
        <div class="nhs_Clear">
        </div>
    </div>
    <div id="nhs_FacetPrice" class="nhs_FacetGroup">
        <p class="nhs_FacetGroupTitle">
            <strong>Price Range</strong></p>
        <div id="nhs_FacetPriceSlider">
        </div>
        <p class="nhs_FacetPriceTextFrom">
            <%if (!Model.IsMove)
              {%>
            From<br />
            <span id="nhs_FacetPriceTextFrom">Under $100K</span>
            <%
              }
              else
              {%>
            <span id="nhs_FacetPriceTextFrom">No Minimum</span>
            <%
              }%>
        </p>
        <p class="nhs_FacetPriceTextTo">
            <%if (!Model.IsMove)
              {%>
            To<br />
            <span id="nhs_FacetPriceTextTo">$1.0M & Up</span>
            <%
              }
              else
              {%>
            <span id="nhs_FacetPriceTextTo">No Maximum</span>
            <%
              }%>
        </p>
        <div class="nhs_Clear">
        </div>
    </div>
    <% if (PartnerLayoutHelper.GetPartnerLayoutConfig().DoesPartnerHaveCommunityStatusFacet)
       {%>
    <div id="nhs_FacetStatus" class="nhs_FacetGroup">
        <p class="nhs_FacetGroupTitle">
            <span id="nhs_FacetLinkStatus">Community status</span></p>
        <ul>
            <%  if (Model.Results.CommunityStatusFacet != null)
                {
                    foreach (var status in Model.Results.CommunityStatusFacet.FacetOptions)
                    {
                        if (status.Selected)
                        {%>
            <li><span rel="" onmouseover="$jq(this).addClass('nhs_Hover');" onmouseout="$jq(this).removeClass('nhs_Hover');"
                class="<%= (status.Text.IndexOf("Any ") != -1) || (status.Text.IndexOf("1+ ") != -1)? "nhs_FacetAny" : "nhs_FacetRemove" %>">
                <%= status.Text%></span></li>
            <% }
                    }
                } %>
        </ul>
        <div id="nhs_FacetModalStatus" class="nhs_FacetMenuBox">
            <div>
                <span class="nhs_FacetModalClose"></span>
                <ul>
                    <%  if (Model.Results.CommunityStatusFacet != null)
                        {
                            foreach (var status in Model.Results.CommunityStatusFacet.FacetOptions)
                            { %>
                    <li><a rel="<%= string.IsNullOrEmpty(status.Value)? "" : status.Value %>" class="<%= (status.Actionable? "nhs_Facet" : "nhs_FacetEmpty") %>"
                        title="<%= status.Text%>">
                        <%= status.Text + (status.Text.IndexOf("Any ") == -1 && status.Text.IndexOf("1+ ") == -1 ? " (" + status.Count + ")" : string.Empty)%></a></li>
                    <% }
                        } %>
                </ul>
            </div>
        </div>
        <div class="nhs_Clear">
        </div>
    </div>
    <% } %>
    <div id="nhs_FacetBedrooms" class="nhs_FacetGroup">
        <p class="nhs_FacetGroupTitle">
            <span id="nhs_FacetLinkBedrooms">Bedrooms</span></p>
        <ul>
            <%  foreach (var size in Model.Results.HomeSizeFacet.FacetOptions)
                {
                    if (size.Selected)
                    {%>
            <li><span rel="0" onmouseover="$jq(this).addClass('nhs_Hover');" onmouseout="$jq(this).removeClass('nhs_Hover');"
                class="<%= (size.Text.IndexOf("Any ") != -1) || (size.Text.IndexOf("1+ ") != -1)? "nhs_FacetAny" : "nhs_FacetRemove" %>">
                <%= size.Text%></span></li>
            <% }
                } %>
        </ul>
        <div id="nhs_FacetModalBedrooms" class="nhs_FacetMenuBox">
            <div>
                <span class="nhs_FacetModalClose"></span>
                <ul>
                    <%  foreach (var size in Model.Results.HomeSizeFacet.FacetOptions)
                        { %>
                    <li><a rel="<%= string.IsNullOrEmpty(size.Value)? "0" : size.Value %>" class="<%= (size.Actionable? "nhs_Facet" : "nhs_FacetEmpty") %>"
                        title="<%= size.Text%>">
                        <%= size.Text + (size.Text.IndexOf("Any ") == -1 && size.Text.IndexOf("1+ ") == -1 ? " (" + size.Count + ")" : string.Empty)%></a></li>
                    <% } %>
                </ul>
            </div>
        </div>
        <div class="nhs_Clear">
        </div>
    </div>
    <div id="nhs_FacetBathrooms" class="nhs_FacetGroup">
        <p class="nhs_FacetGroupTitle">
            <span id="nhs_FacetLinkBathrooms">Bathrooms</span></p>
        <ul>
            <%  foreach (var number in Model.Results.BathroomFacet.FacetOptions)
                {
                    if (number.Selected)
                    {%>
            <li><span rel="0" onmouseover="$jq(this).addClass('nhs_Hover');" onmouseout="$jq(this).removeClass('nhs_Hover');"
                class="<%= (number.Text.IndexOf("Any ") != -1) ? "nhs_FacetAny" : "nhs_FacetRemove" %>">
                <%= number.Text%></span></li>
            <% }
                } %>
        </ul>
        <div id="nhs_FacetModalBathrooms" class="nhs_FacetMenuBox">
            <div>
                <span class="nhs_FacetModalClose"></span>
                <ul>
                    <%  foreach (var number in Model.Results.BathroomFacet.FacetOptions)
                        { %>
                    <li><a rel="<%= string.IsNullOrEmpty(number.Value)? "0" : number.Value %>" class="<%= (number.Actionable? "nhs_Facet" : "nhs_FacetEmpty") %>"
                        title="<%= number.Text%>">
                        <%= number.Text + (number.Text.IndexOf("Any ") == -1? " (" + number.Count + ")" : string.Empty)%></a></li>
                    <% } %>
                </ul>
            </div>
        </div>
        <div class="nhs_Clear">
        </div>
    </div>
    <% if (PartnerLayoutHelper.GetPartnerLayoutConfig().DoesPartnerHaveSquareFeetFacet)
       {%>
    <div id="nhs_FacetSqFeet" class="nhs_FacetGroup">
        <p class="nhs_FacetGroupTitle">
            <strong>Square Feet</strong></p>
        <div id="nhs_FacetSqFtSlider">
        </div>
        <p class="nhs_FacetSqFtTextFrom">
            <span id="nhs_FacetSqFeetTextFrom">&nbsp;</span>
        </p>
        <p class="nhs_FacetPriceTextTo">
            <span id="nhs_FacetSqFeetTextTo">&nbsp;</span>
        </p>
        <div class="nhs_Clear">
        </div>
    </div>
    <div id="nhs_FacetPromotions" class="nhs_FacetGroup">
        <p class="nhs_FacetGroupTitle">
            <span id="nhs_FacetLinkPromotions">Promotions</span></p>
        <ul>
            <%  foreach (var number in Model.Results.PromotionFacet.FacetOptions)
                {
                    if (number.Selected)
                    {%>
            <li><span rel="0" onmouseover="$jq(this).addClass('nhs_Hover');" onmouseout="$jq(this).removeClass('nhs_Hover');"
                class="<%= (number.Text.IndexOf("Any") != -1)? "nhs_FacetAny" : "nhs_FacetRemove" %>">
                <%= number.Text%></span></li>
            <% }
                } %>
        </ul>
        <div id="nhs_FacetModalPromotions" class="nhs_FacetMenuBox">
            <div>
                <span class="nhs_FacetModalClose"></span>
                <ul>
                    <%  foreach (var number in Model.Results.PromotionFacet.FacetOptions)
                        {
                            if (UserSession.UserProfile.ActorStatus != WebActors.ActiveUser && (number.Value == ((int)PromotionType.Agent).ToString() || number.Value == ((int)PromotionType.Both).ToString()))
                            {%>
                    <li>
                        <%= Html.NhsModalWindowLink(number.Text, Pages.LoginModal,
                                                                        ModalWindowsConst.SignInModalWidth,
                                        ModalWindowsConst.SignInModalHeight,  new List<RouteParam>(),
                                                                        new { onclick = "$jq.googlepush('AccolumnCount Events','Create AccolumnCount','Open Form - Header Link')", @class = "nhs_LockIconLink" })%>
                    </li>
                    <% }
                            else
                            { %>
                    <li><a rel="<%= string.IsNullOrEmpty(number.Value)? "0" : number.Value %>" class="<%= (number.Actionable? "nhs_Facet" : "nhs_FacetEmpty") %>"
                        title="<%= number.Text%>">
                        <%= number.Text + (number.Text.IndexOf("Any") == -1 ? " (" + number.Count + ")" : string.Empty)%></a></li>
                    <% } %>
                    <% } %>
                </ul>
            </div>
        </div>
        <div class="nhs_Clear">
        </div>
    </div>
    <div id="nhs_FacetEvents" class="nhs_FacetGroup">
        <p class="nhs_FacetGroupTitle">
            <span id="nhs_FacetLinkEvents">Events</span></p>
        <ul>
            <%  foreach (var number in Model.Results.EventFacet.FacetOptions)
                {
                    if (number.Selected)
                    {%>
            <li><span rel="" onmouseover="$jq(this).addClass('nhs_Hover');" onmouseout="$jq(this).removeClass('nhs_Hover');"
                class="<%= (number.Text.IndexOf("Any") != -1)? "nhs_FacetAny" : "nhs_FacetRemove" %>">
                <%= number.Text%></span></li>
            <% }
                } %>
        </ul>
        <div id="nhs_FacetModalEvent" class="nhs_FacetMenuBox">
            <div>
                <span class="nhs_FacetModalClose"></span>
                <ul>
                    <%  foreach (var number in Model.Results.EventFacet.FacetOptions)
                        { %>
                    <li><a rel="<%= string.IsNullOrEmpty(number.Value)? "" : number.Value %>" class="<%= (number.Actionable? "nhs_Facet" : "nhs_FacetEmpty") %>"
                        title="<%= number.Text%>">
                        <%= number.Text + (number.Text.IndexOf("Any") == -1 ? " (" + number.Count + ")" : string.Empty)%></a></li>
                    <% } %>
                </ul>
            </div>
        </div>
        <div class="nhs_Clear">
        </div>
    </div>
    <% } %>
    <%if (!Model.IsAOnlyBasicListingsMarket)
      {%>
    <div id="nhs_FacetVideo" class="nhs_FacetGroup">
        <p class="nhs_FacetGroupTitle">
            <span id="nhs_FacetLinkVideo">Video available</span></p>
        <ul>
            <%  foreach (var video in Model.Results.VideoFacet.FacetOptions)
                {
                    if (video.Selected)
                    {%>
            <li><span rel="" onmouseover="$jq(this).addClass('nhs_Hover');" onmouseout="$jq(this).removeClass('nhs_Hover');"
                class="<%= (video.Text.IndexOf("Any") != -1 || video.Text.IndexOf("With") != -1)? "nhs_FacetAny" : "nhs_FacetRemove" %>">
                <%= video.Text%></span></li>
            <% }
                } %>
        </ul>
        <div id="nhs_FacetModalVideo" class="nhs_FacetMenuBox">
            <div>
                <span class="nhs_FacetModalClose"></span>
                <ul>
                    <%  foreach (var video in Model.Results.VideoFacet.FacetOptions)
                        { %>
                    <li><a rel="<%= string.IsNullOrEmpty(video.Value)? "" : video.Value %>" class="<%= (video.Actionable? "nhs_Facet" : "nhs_FacetEmpty") %>"
                        title="<%= video.Text%>">
                        <%= video.Text + (video.Text.IndexOf("Any") == -1 && video.Text.IndexOf("With") == -1 ? " (" + video.Count + ")" : string.Empty)%></a></li>
                    <% } %>
                </ul>
            </div>
        </div>
        <div class="nhs_Clear">
        </div>
    </div>
    <% } %>
    <div id="nhs_FacetSchools" class="nhs_FacetGroup">
        <p class="nhs_FacetGroupTitle">
            <span id="nhs_FacetLinkSchools">Schools</span></p>
        <ul>
            <%  foreach (var school in Model.Results.SchoolDistrictFacet.FacetOptions)
                {
                    if (school.Selected)
                    {%>
            <li><span rel="0" onmouseover="$jq(this).addClass('nhs_Hover');" onmouseout="$jq(this).removeClass('nhs_Hover');"
                class="<%= (school.Text.IndexOf("Any ") != -1 || school.Text.IndexOf("All ") != -1)? "nhs_FacetAny" : "nhs_FacetRemove" %>">
                <%= school.Text%></span></li>
            <% }
                } %>
        </ul>
        <div id="nhs_FacetModalSchools" class="nhs_FacetMenuBox">
            <div>
                <span class="nhs_FacetModalClose"></span>
                <ul>
                    <%  foreach (var school in Model.Results.SchoolDistrictFacet.FacetOptions)
                        { %>
                    <li><a rel="<%= string.IsNullOrEmpty(school.Value)? "0" : school.Value %>" class="<%= (school.Actionable? "nhs_Facet" : "nhs_FacetEmpty") %>"
                        title="<%= school.Text%>">
                        <%= school.Text + (school.Text.IndexOf("Any ") == -1 && school.Text.IndexOf("All ") == -1 ? " (" + school.Count + ")" : string.Empty)%></a></li>
                    <% } %>
                </ul>
            </div>
        </div>
        <div class="nhs_Clear">
        </div>
    </div>
    <%if (!Model.IsAOnlyBasicListingsMarket)
      {%>
    <div id="nhs_FacetTypeAmenities" class="nhs_FacetGroup">
        <p class="nhs_FacetGroupTitle">
            <span id="nhs_FacetLinkAmenities">Home type/amenities</span></p>
        <ul>
            <%  foreach (var type in Model.Results.HomeTypeFacet.FacetOptions)
                {
                    if (type.Selected)
                    {%>
            <li><span rel="" onmouseover="$jq(this).addClass('nhs_Hover');" onmouseout="$jq(this).removeClass('nhs_Hover');"
                class="type <%= (type.Text.IndexOf("Any ") != -1 || type.Text.IndexOf("All ") != -1)? "nhs_FacetAny" : "nhs_FacetRemove" %>">
                <%= type.Text%></span></li>
            <% }
                } %>
            <%  foreach (var type in Model.Results.CommunityAmenityFacet.FacetOptions)
                {
                    if (type.Selected)
                    {%>
            <li><span rel="<%= type.Value %>" onmouseover="$jq(this).addClass('nhs_Hover');"
                onmouseout="$jq(this).removeClass('nhs_Hover');" class="amenity <%= (type.Text.IndexOf("Any ") != -1 || type.Text.IndexOf("restriction") != -1)? "nhs_FacetAny" : "nhs_FacetRemove" %>">
                <%= type.Text%></span></li>
            <% }
                } %>
        </ul>
        <div id="nhs_FacetModalAmenities" class="nhs_FacetMenuBox">
            <div>
                <span class="nhs_FacetModalClose"></span>
                <p class="nhs_FacetGroupTitle">
                    Home type</p>
                <ul>
                    <%  foreach (FacetOption type in Model.Results.HomeTypeFacet.FacetOptions)
                        { %>
                    <li><a rel="<%= type.Value %>" class="type <%=((type.Text.IndexOf("Any ") == -1 && type.Text.IndexOf("All ") == -1)? string.Empty  : "nhs_FacetRemove") %> <%= type.Actionable? "nhs_Facet" : "nhs_FacetEmpty" %>"
                        title="<%= type.Text%>">
                        <%= type.Text + (type.Text.IndexOf("Any ") == -1 && type.Text.IndexOf("All ") == -1 ? " (" + type.Count + ")" : string.Empty)%></a></li>
                    <% } %>
                </ul>
                <p class="nhs_FacetGroupTitle">
                    Amenities</p>
                <ul>
                    <%  foreach (var amenity in Model.Results.CommunityAmenityFacet.FacetOptions)
                        { %>
                    <li><a rel="<%= amenity.Value %>" class="amenity <%=((amenity.Text.IndexOf("Any ") == -1 && amenity.Text.IndexOf("restriction") == -1)? string.Empty  : "nhs_FacetRemove") %> <%= amenity.Actionable? "nhs_Facet" : "nhs_FacetEmpty" %>"
                        title="<%= amenity.Text%>">
                        <%= amenity.Text + (amenity.Text.IndexOf("Any ") == -1 && amenity.Text.IndexOf("restriction") == -1 ? " (" + amenity.Count + ")" : string.Empty)%></a></li>
                    <% } %>
                </ul>
            </div>
        </div>
        <div class="nhs_Clear">
        </div>
    </div>
    <% } %>
    <% if (!string.IsNullOrEmpty(Model.Results.BrandName))
       {%>
    <div id="nhs_FacetBuilderDisplay" class="nhs_FacetGroup">
        <p class="nhs_FacetGroupTitle">
            Builder</p>
        <ul>
            <li><span rel="0" onmouseover="$jq(this).addClass('nhs_Hover');" onmouseout="$jq(this).removeClass('nhs_Hover');"
                class="nhs_FacetRemove">
                <%= Model.Results.BrandName%></span></li>
        </ul>
        <div class="nhs_Clear">
        </div>
    </div>
    <% } %>
    <div class="nhs_FacetLinks">
        <p>
            <%=Html.NhsLink("Advanced Search...", Pages.AdvancedSearch, null)%>
        </p>
        <%if (!Model.IsAOnlyBasicListingsMarket)
          {%>
        <%if (NhsRoute.IsBrandPartnerMove)
          {%>
        <p>
            View a list of matching homes instead:
            <%= Html.HomeResultsLink(Model, "Builder homes")%>
            <%--,&nbsp;<%=Html.MlsResultsLink(Model, "REALTOR.com® homes")%>--%>
        </p>
        <%}
          else
          {%>
        <p>
            View a list of matching homes instead:
            <%= Html.HomeResultsLink(Model, "View homes")%>
        </p>
        <%
          }
          }%>
    </div>
    <%if (Model.Globals.PartnerLayoutConfig.DoesPartnerAllowsSeoLinkPromotions)
      {%>
    <% Html.RenderAction(NhsMvc.PartialViews.ActionNames.SeoLinkPromotion, NhsMvc.PartialViews.Name);%>
    <%}%>
    <%if (Model.Globals.PartnerLayoutConfig.PartnerAllowsCrossMarketingLinks)
      {%>
    <div class="nhs_FacetLinks">
        <p class="nhs_FacetGroupTitle">
            Find more types of
            <%=Model.Market.MarketName%>
            new homes for sale
        </p>
        <ul>
            <li><a href="http://www.customnewhomes.com/" target="_blank">
                <%if (!Model.IsMove)
                  {%>
                Custom builder homes
                <%}
                  else
                  {%>
                Custom Built Homes
                <%}%>
            </a></li>
            <li><a href="http://www.newretirementcommunities.com/" target="_blank">Retirement communities</a>
            </li>
            <li><a href="http://www.urbancondoliving.com/" target="_blank">
                <%if (!Model.IsMove)
                  {%>
                New condominium homes
                <%}
                  else
                  {%>
                Urban Condo Living
                <%}%>
            </a></li>
            <li><a href="http://www.casasnuevasaqui.com/" target="_blank">
                <%if (!Model.IsMove)
                  {%>
                Casas nuevas en Espa&ntilde;ol
                <%}
                  else
                  {%>
                Casas nuevas aqui
                <%}%>
            </a></li>
            <%--<%=Html.MlsResultsLink(Model, "Find homes on REALTOR.com®", true)%>--%>
        </ul>
    </div>
    <%
      }%>
    <div class="nhs_Clear">
    </div>
</div>
