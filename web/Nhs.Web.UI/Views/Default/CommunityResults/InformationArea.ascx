﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Nhs.Web.UI.AppCode.ViewModels.CommunityResultsViewModel>" %>
<%@ Import Namespace="Nhs.Library.Constants" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.HtmlHelpers" %>
<%--Post to Action --%>
<%--<div id="nhs_CommResMatches">--%>
<p>
    <%if (!Model.IsMove){
        if (!Model.IsAOnlyBasicListingsMarket)
        {%>
            <%=Model.Results.TotalCommunities%> matching communities and 
        <% } %>
        <%=Model.Results.TotalHomes%> homes for sale
    <%}else
       {
        if (!Model.IsAOnlyBasicListingsMarket)
        {%>
            Search Result: <%=Model.Results.TotalCommunities%> results and
        <% } %> 
        <%=Model.Results.TotalHomes%> homes
    <%}%>
</p>
<%--</div>--%>
