﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Nhs.Web.UI.AppCode.ViewModels.CommunityQuickViewViewModel>" %>
<%@ Import Namespace="Nhs.Library.Business" %>
<%@ Import Namespace="Nhs.Library.Common" %>
<%@ Import Namespace="Nhs.Library.Constants" %>
<%@ Import Namespace="Nhs.Library.Web" %>
<%@ Import Namespace="Nhs.Search.Objects" %>
<%@ Import Namespace="Nhs.Search.Objects.Constants" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.HtmlHelpers" %>
<%@ Import Namespace="Nhs.Library.Enums" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.Extensions" %>
<%foreach (var result in Model.HomeResults)
  {
%>
<li class="nhs_ResultQVListItem" onmouseover="this.className='nhs_ResultQVListItemOn';"
    onmouseout="this.className='nhs_ResultQVListItem';" onclick="window.location.href='<%=Html.GetHomeDetailUrl(result) %>';NHS.Scripts.Helper.stopBubble(event);">
    <div class="nhs_ResultThumb">
        <%=Html.HomeDetailImageLink(result, true)%>
    </div>
    <p class="nhs_ResultHomeName">
        <%=Html.HomeDetailLink(result)%>
        <%--<%if (result.SpecId <= 0)
          {%>(Plan)<%} %>--%>
    </p>
    <p class="nhs_ResultQVHomeStatus">
        <%--Status Image--%>
        <% var status = result.HomeStatus();%>
        <%=Html.NhsImage(status["url"], status["desc"], false)%>
    </p>
    <p class="nhs_ResultHomeBedPriceHot">
        <span class="nhs_ResultHomeBedBath">
            <%--BedRooms--%>
            <%=result.FormatBedrooms() %>
            <%--BathRooms--%>
            <%=result.FormatBathrooms()%>
            <%--Garage--%>
            <%=result.FormatGarages()%>
        </span><span class="nhs_ResultQVHotHome">
            <%if (result.IsHotHome)
              {%>
            <img src="<%=Resources.GlobalResourcesMvc.Default.images.icons.icon_hot_home_png%>"
                alt="Hot Home" />
            <%} %>
        </span>
        <%if (result.HomeStatus != (int)HomeStatusType.ModelHome)
          {%><%--Price--%>
        <span class="nhs_ResultHomePrice">
            <%:result.Price.FormatPrice(result.SpecId)%>
        </span>
        <%}%>
    </p>
    <div class="nhs_Clear"></div></li>
<%} %>
