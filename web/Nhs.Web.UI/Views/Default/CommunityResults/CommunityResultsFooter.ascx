﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Nhs.Web.UI.AppCode.ViewModels.CommunityResultsViewModel>" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>
<%@ Import Namespace="Nhs.Library.Constants" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.Extensions" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.HtmlHelpers" %>

<div class="nhs_CommResListBox">
    <div class="nhs_CommResListAd">
        <%if (Model.Globals.PartnerLayoutConfig.PartnerIsABrand || Model.Globals.PartnerLayoutConfig.IsBrandPartnerNhsPro
              || Model.Globals.PartnerLayoutConfig.IsBrandPartnerMove || Model.Globals.PartnerLayoutConfig.IsPartnerNhl)
          {%>            
            <div id="nhs_Footerx02"></div>
        <%}%>
    </div>

    <div class="nhs_CommResListBuilders">
        <h4>
            <%if (!Model.IsMove){%>
                Find new home communities by city
            <%}else{%>
                Search for new home communities by city
            <%}%>
        </h4>
        <ul>
        <%foreach (var c in Model.CitiesCol1)
                {%>
            <li>
            <%=Html.NhsLink(string.Concat(c.CityName, " homes"), Pages.CommunityResults, c.CRParams(Model.Market.MarketId), null, true )%>
            </li>
        <%}%>
        </ul>
        <ul>
        <%foreach (var c in Model.CitiesCol2)
                {%>
            <li>
            <%=Html.NhsLink(string.Concat(c.CityName, " homes"), Pages.CommunityResults, c.CRParams(Model.Market.MarketId), null, true)%>
            </li>
        <%}%>
        </ul>
        <ul>
        <%foreach (var c in Model.CitiesCol3)
                {%>
            <li>
            <%=Html.NhsLink(string.Concat(c.CityName, " homes"), Pages.CommunityResults, c.CRParams(Model.Market.MarketId), null, true)%>
            </li>
        <%}%>
        </ul>
    </div>
    <%=Html.RenderZipCodeByMarketId(Model)%>
    <%=Html.RenderSeoCommunitiesContent(Model.MarketsList, string.Empty)%>
    <div class="nhs_Clear"></div>

</div>
