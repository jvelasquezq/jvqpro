﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Nhs.Web.UI.AppCode.ViewModels.CommunityResultsViewModel>" %>
<%@ Import Namespace="Nhs.Library.Constants.Enums" %>

<ul id="nhs_CommResTabs" class="nhs_Tabs">
    <%--<li id="allCommunitiesTab" <%=(Model.SelectedTab == CommunityResultsTabs.AllCommunities) ? "class=\"nhs_Selected\"" : ""%>><a rel="nofollow"><span><%= "All " + Model.AllCommunitiesResults + " results"%></span></a></li>--%>
    <li id="allCommunitiesTab" <%=(Model.SelectedTab == CommunityResultsTabs.AllCommunities) ? "class=\"nhs_Selected\"" : ""%>><a rel="nofollow" onclick="$jq.googlepush('Search Events','Search Results','All Results Tab')"><span>All Results</span></a></li>
    <%if (Nhs.Library.Helpers.Content.PartnerLayoutHelper.GetPartnerLayoutConfig().DoesPartnerHaveHotDealsLink)
      {%>
    <li id="hotDealsTab" <%= (Model.SelectedTab == CommunityResultsTabs.HotDeals)
                             ? "class=\"nhs_Selected\""
                             : (Model.HotHomesResults <= 0 ? "class=\"nhs_dealsTabDisabled\"" : "") %>><a <%= (Model.HotHomesResults <= 0
                              ? "disabled='disabled'"
                              : "") %> onclick="commResults.get_log().logEvent('CRPROMO', 0, 0);$jq.googlepush('Search Events','Search Results','Hot Deals Tab');" rel="nofollow"><span><span><%= Model.HotHomesResults + " Hot Deals" %></span></span></a></li>
     <% }%>
    <li id="quickMoveInTab" <%=(Model.SelectedTab == CommunityResultsTabs.QuickMoveIn) ? "class=\"nhs_Selected\"" : (Model.QuickMoveInResults <= 0 ? "class=\"nhs_quickTabDisabled\"" : "") %>><a <%= (Model.QuickMoveInResults <= 0 ? "disabled='disabled'" : "" ) %> onclick="$jq.googlepush('Search Events','Search Results','Quick Move-in Tab')" rel="nofollow"><span><span><%= Model.QuickMoveInResults + " Quick Move-In" %></span></span></a></li>
</ul>