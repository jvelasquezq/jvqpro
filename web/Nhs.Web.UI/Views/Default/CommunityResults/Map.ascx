﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Nhs.Web.UI.AppCode.ViewModels.BaseViewModel>" %>
<%@ Import Namespace="Nhs.Mvc.Routing.Interface" %>
<%@ Import Namespace="System.Globalization" %>
<a name="MapAnchor" id="MapAnchor"></a>
<div  style="position:relative;" >
    <div id="nhs_MapTipBox" class="nhs_MapTipBox">
        <div class="nhs_MapTipTop">
	        <p class="nhs_MapTipTxt">Click, drag & zoom <br />to find matching homes!</p>
	    </div>
    </div>
    <div id="nhs_CommResMap" >
   
    </div>

    <div id="nhs_CommResHideMap" onclick="commResults.get_log().logEvent('CRMAPC', 0, 0); commResults.hideMap(); NHS.Scripts.Helper.createCookie('mapState', 'collapsed', 0);"></div>
</div>
<div class="nhs_Clear"></div>

