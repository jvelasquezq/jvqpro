﻿NHS.Mobile.BuilderShowCase = function (parameters) {
    this.parameters = parameters;
    this.parameters.currentPage = 1;
    this.searchTypeahead = new NHS.Mobile.Search(this.parameters.AutoCompleteUrl);
    this.logger = new NHS.Scripts.Globals.EventLogger({ siteRoot: parameters.siteRoot, partnerId: parameters.partnerId, marketId: parameters.marketId });
    this.player = new NHS.Scripts.WebPlayers({
        videoWidth: (parameters.viewerParams.videoWidth || 0),
        videoHeight: (parameters.viewerParams.videoHeight || 0),
        youtubeControlId: "nhs_VideP",
        vimeoControlId: "nhs_VideP",
        brightcoveControlId: "nhs_VideP",
        brightcoveToken: parameters.viewerParams.brightcoveToken
    }); 
};

NHS.Mobile.BuilderShowCase.prototype =
{

    askQuestion: function () {
        var self = this;
        var mail = jQuery("#MailAddress").val();
        var name = jQuery("#Name").val();
        var comments = jQuery("#Comments").val();
        var formData = { Comments: comments, MailAddress: mail, Name: name, BrandName: self.parameters.brandName, BrandEmailAddress: self.parameters.brandEmail, BrandId: self.parameters.brandId };

        jQuery.ajax({
            url: "/buildershowcase/askaquestion",
            type: "POST",
            data: formData,
            success: function (data) {
                if (data.HasErrors) {
                    var ul = jQuery("#nhs_Errors");
                    ul.html("");
                    jQuery("#formToSend").show();
                    jQuery("#formSent").hide();

                    if (data.Errors && data.Errors.length > 0) {
                        for (var i in data.Errors) {
                            if (data.Errors.hasOwnProperty(i)) {
                                var error = data.Errors[i];
                                ul.append("<li>" + error + "</li>");
                            }
                        }
                    }

                    ul.show();
                }
            }
        });
    },

    attachGooglePushEvents: function () {
        jQuery("#nhs_TabAbout").click(function () {
            jQuery.googlepush("Builder Showcase Events", "Builder Showcase - Tab", "About");
        });

        jQuery("#nhs_TabLocation").click(function () {
            jQuery.googlepush("Builder Showcase Events", "Builder Showcase - Tab", "Locations");
        });

        jQuery("#nhs_VisitWebsite").click(function () {
            jQuery.googlepush("Builder Showcase Events", "Builder Website - Tab", "Outbound Click");
        });
    },

    init: function () {
        this.initEvents();
        this.attachGooglePushEvents();
    },

    initEvents: function () {
        var self = this;
        jQuery(".nhs_BuildershowcaseTab").unbind("click");
        jQuery(".nhs_BuildershowcaseTab").click(function (event) {
            jQuery.NhsCancelEvent(event);

            var control = jQuery(this);
            if (!control.is(".nhs_Selected")) {
                if (control.is("#nhs_TabLocation")) {
                    self.loadLocations();
                } else {
                    self.loadAbout();
                }
            }
        });

        jQuery("#btnSend").unbind("click");
        jQuery(document).on("click", "#btnSend", function () {
            jQuery("#nhs_Errors").html("").hide();
            jQuery("#formToSend").hide();
            jQuery("#formSent").show();

            if (self.validateFakeFields() && self.ValidateLeadModel()) {
                jQuery.googlepush("Lead Events", "Builder Showcase - Right", "Submit Form - Ask a Question");
                self.askQuestion();
            }
        });
        
        jQuery(".nhs_ShowcaseLinkAbout").click(function () {
            jQuery.googlepush("Builder Showcase Events", "Builder Showcase - Link", "About");
            self.loadAbout();
        });

        jQuery(".nhs_ShowcaseLinkLocation").click(function () {
            jQuery.googlepush("Builder Showcase Events", "Builder Showcase - Link", "Locations");
            self.loadLocations();
        });

        jQuery("#nhs_MoreResults").click(function (event) {
            jQuery.CancelEventAndStopPropagation(event);

            jQuery.ajax({
                url: self.parameters.GetMoreLocations + "&page=" + self.parameters.currentPage,
                type: "GET",
                success: function (data) {
                    if (typeof data == "string" && data.length > 0) {
                        self.parameters.currentPage += 1;
                        jQuery("#nhs_metroAreas").append(data);

                        if (jQuery(".nhs_metroMarket").length % 8 !== 0)
                            jQuery("#nhs_MoreResults").hide();
                    }
                }
            });
        });

        if (self.parameters.IsAbout) {
            self.initGallery();
            jQuery("#nhs_TabAbout a").addClass("active");
            jQuery("#nhs_TabLocation a").removeClass("active");
        } else {
            self.initMap();
            jQuery("#nhs_TabAbout a").removeClass("active");
            jQuery("#nhs_TabLocation a").addClass("active");
        }
    },

    initGallery: function () {
        var self = this;
        jQuery.ajax({
            url: self.parameters.GetMediaPlayerObjects,
            type: "GET",
            success: function (data) {
                if (typeof data == "string" && data.length > 0) {
                    jQuery("#nhs_GalleryList").html(data);
                    jQuery("#nhs_GalleryList").toggleClass("expanded").slideDown();
                    jQuery("#nhs_GalleryList .nhs_LinkGallery").click(function (event) {
                        jQuery.CancelEventAndStopPropagation(event);
                        self.ShowModal(this);
                    });

                    jQuery("#nhs_GalleryList .bxslider").bxSlider({
                        responsive: false,
                        infiniteLoop: false,
                        slideWidth: 126,
                        slideMargin: 5,
                        minSlides: 1,
                        maxSlides: 5,
                        pager: false,
                        controls: false
                    });
                }
            }
        });
    },

    loadBrandMarketPoints: function(googleApi, parameters) {
        var bounds = googleApi.getBoundsFromMap();
        var data = {
            partnerId: parameters.partnerID,
            brandId: parameters.brandId,
            minLat: bounds.minLat,
            minLng: bounds.minLng,
            maxLat: bounds.maxLat,
            maxLng: bounds.maxLng
        };
        jQuery.getJSON("/MapSearch/GetBrandMarketMapPoints", data, function (results) {
            googleApi.processResult(results);
        });
    },

    initMap: function () {
        var mapOptions = this.parameters.OptionsForMap;
        var parameters = this.parameters;
        var googleApi = new NHS.Scripts.GoogleMapApi(mapOptions);
        var self = this;
        var firstTime = true;

        googleApi.options.Events.OnMapCreate = function () {
            self.loadBrandMarketPoints(googleApi, parameters);
            googleApi.AutoFit();
        };

        googleApi.options.Events.OnBoundsChanged = function () {
            self.loadBrandMarketPoints(googleApi, parameters);
        };

        googleApi.processResult = function (results) {
            this.showLoading();
            for (var i = 0; i < results.length; i++) {
                var lat = results[i].Lat;
                var lng = results[i].Lng;
                var name = results[i].Title.replace("<strong>", "").replace("</strong>", "");
                var urlCommResult = results[i].ActionUrl;

                var communityResultsUrl = parameters.siteRoot + urlCommResult;
                communityResultsUrl = communityResultsUrl.replace("//", "/");
                var contentHtml = "<div class=\"nhs_MapCardMarket clearfix\">";
                var marketName = name.substring(0, name.indexOf("("));
                var commsCount = name.substring(name.indexOf("("));
                contentHtml += "<h2><a href=\"" + communityResultsUrl + "\" >" + marketName + "</a></h2> <p>by " + parameters.brandName + "</p> <p>" + commsCount + "</p>";
                contentHtml += "</div>";
                this.createMarkerPoint(lat, lng, name, contentHtml, parameters.icon, results[i]);
            }

            this.hideLoading();

            if (firstTime) {
                firstTime = false;
                googleApi.AutoFit();
            }
        };

        googleApi.createMap();
    },

    loadAbout: function () {
        var self = this;
        self.parameters.IsAbout = true;

        jQuery.ajax({
            url: self.parameters.urlAbout,
            type: "GET",
            success: function (data) {
                if (typeof data == "string" && data.length > 0) {
                    jQuery("#nhs_BuilderShowcaseTab").html(data);
                    self.init();
                    jQuery("#nhs_TabAbout").addClass("nhs_Selected");
                    jQuery("#nhs_TabLocation").removeClass("nhs_Selected");
                }
            }
        });
    },

    loadLocations: function () {
        var self = this;
        self.parameters.IsAbout = false;

        jQuery.ajax({
            url: self.parameters.urlLocation,
            type: "GET",
            success: function (data) {
                if (typeof data == "string" && data.length > 0) {
                    jQuery("#nhs_BuilderShowcaseTab").html(data);
                    self.init();
                    jQuery("#nhs_TabAbout").removeClass("      nhs_Selected");
                    jQuery("#nhs_TabLocation").addClass("nhs_Selected");
                    self.parameters.currentPage = 1;
                    jQuery(document).scrollTop(0);
                }
            }
        });
    },

    ShowModal: function (link) {
        var self = this;
        var modal = modalPopUp.CreateModal();
        link = jQuery(link);

        var control = link.parent().parent().parent().clone();
        var startSlide = parseInt(link.attr("data-slide"));
        var w = jQuery("<div class=\"nhs_GalleryList clearfix\"></div>").append(control);
        modal.find("#nhs_ModalBody").append(w);
        modal.find(".nhs_LinkGallery").click(function (event) {
            jQuery.CancelEventAndStopPropagation(event);
            self.ShowVideo(this);
        });

        modal.addClass("expanded");
        control.bxSlider({
            responsive: true,
            infiniteLoop: false,
            slideWidth: 234,
            slideMargin: 10,
            minSlides: 1,
            maxSlides: 5,
            startSlide: startSlide,
            pager: false,
            controls: false
        });
    },

    ShowVideo: function (control) {
        var self = this;

        self.player.stopVideo(true);
        control = jQuery(control);
        var type = control.data("type");


        if (type.indexOf("l-") != -1) {
            var url = control.data("url");
            var eventCode = control.data("event");
            self.logger.logAndRedirect(event, url, eventCode, self.parameters.communityId, self.parameters.brandId, self.parameters.planId, self.parameters.specId, self.parameters.MarketId, true);
        }
        else if (type == "v-yt" || type == "v-vm" || type == "v-bc") {
            var videoId = control.data("id");
            var parent = control.parent();
            parent.append("<div id=\"nhs_VideP\"></div>");
            control.hide();
            var div = control.parent()[0];
            var videoWidth = div.clientWidth;
            var videoHeight = div.clientHeight;

            self.player.parameters.videoWidth = videoWidth;
            self.player.parameters.videoHeight = videoHeight;

            if (type == "v-yt") {
                self.player.playYouTubeVideo(videoId);
            } else if (type == "v-vm") {
                self.player.playVimeoVideo(videoId);
            } else if (type == "v-bc") {
                self.player.playBrightcoveVideo(videoId);
            }
        } 
    },

    validateFakeFields: function () {
        // Case 73789 - Builder Showcase Contacts are being spammed
        // Fake fields where added to the form ids = MailAddress2 & FirstName & LastName
        // If those fields are filled probably the submit was filled by a robot
        // Also check if at least 8 seconds has passed since the form was loaded
        return (jQuery("#MailAddress2").val() === "" && jQuery("#FirstName").val() === "" && jQuery("#LastName").val() === "");
    },

    ValidateLeadModel: function () {
        var isValid = true;
        var email = jQuery("#MailAddress");
        var name = jQuery("#Name");
        var ul = jQuery("#nhs_Errors");
        ul.html("");

        if (email.val().trim() === "") {
            ul.append("<li>" + self.parameters.EmailRequeredText + "</li>");
            email.attr("style", "border: 1px solid #FF0000");
            isValid = false;
        } else {
            email.removeAttr("style");
        }

        if (name.val().trim() === "") {
            ul.append("<li>" + self.parameters.PleaseProvideFirstAndLastNameText + "</li>");
            name.attr("style", "border: 1px solid #FF0000");
            isValid = false;
        } else {
            name.removeAttr("style");
        }

        if (email.val().trim() !== "") {
            if (!jQuery.ValidEmail(email.val().trim())) {
                ul.append("<li>" + self.parameters.EmailAddressIsNotValidText + "</li>");
                email.attr("style", "border: 1px solid #FF0000");
                isValid = false;
            } else {
                email.removeAttr("style");
            }
        }

        if (name.val().trim() !== "") {
            if (!jQuery.ValidFullNameWithNumberLastName(name.val().trim())) {
                ul.append("<li>" + self.parameters.PleaseEnterFirstAndLastNameText + "</li>");
                name.attr("style", "border: 1px solid #FF0000");
                isValid = false;
            } else {
                name.removeAttr("style");
            }
        }

        if (!isValid) {
            jQuery("#formToSend").show();
            jQuery("#formSent").hide();
            ul.show();
        }
        return isValid;
    }
}
