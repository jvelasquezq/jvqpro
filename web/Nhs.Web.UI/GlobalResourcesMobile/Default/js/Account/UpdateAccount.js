﻿
NHS.Mobile.UpdateAccount = function (parameters) {
    this.parameters = parameters;
};

NHS.Mobile.UpdateAccount.prototype =
{
   init: function () {
        var self = this;

        jQuery('#nhs_UpdateAccount').keypress(function (e) {
            if (e.which === 13) {
                jQuery("#btnsubmit").click();
            }
        });

        jQuery("#International").change(function () {
            if (jQuery(this).is(":checked")) {
                jQuery("#nhs_UpdateAccount #ZipCodeReg").val("");
                jQuery("#ZipCodeReg").attr("disabled", "disabled");
            } else {
                jQuery("#ZipCodeReg").removeAttr("disabled");
            }
        });

        jQuery("#nhs_UpdateAccount #ZipCode").mask("99999");

        jQuery("#btnsubmit").click(function () {

            var valid = true;
            var control = jQuery(this);
            control.attr("disabled", "disabled");
            var ul = jQuery("#nhs_Errors");
            ul.html("");

            //Start Name
            var name = jQuery("#nhs_UpdateAccount #FirstName");
            name.removeAttr("style");
            if (!jQuery.ValidFullName(name.val())) {
                ul.append("<li>" + self.parameters.FirstAndLastNameRequired + "</li>");
                name.attr("style", "border: 1px solid #FF0000");
                valid = false;
            }
            //End Name

            //Start Email
            var email = jQuery("#nhs_UpdateAccount #Email");
            email.removeAttr("style");
            if (!jQuery.ValidEmail(email.val())) {
                ul.append("<li>" + self.parameters.MSG_ACCOUNT_NO_EMAIL + "</li>");
                email.attr("style", "border: 1px solid #FF0000");
                valid = false;
            }
            //End Email

            //Start Password
            var password = jQuery("#nhs_UpdateAccount #Password");
            password.removeAttr("style");
            if (password.val().length < 5) {
                password.attr("style", "border: 1px solid #FF0000");
                ul.append("<li>" + self.parameters.MSG_ACCOUNT_INVALID_PASSWORD + "</li>");
                valid = false;
            }
            //End Password

            //Start Zip
            if (!jQuery("#International").is(":checked")) {
                var zip = jQuery("#nhs_UpdateAccount #ZipCodeReg");
                zip.removeAttr("style");
                if (!jQuery.ValidZipcode(zip.val())) {
                    zip.attr("style", "border: 1px solid #FF0000");
                    ul.append("<li>" + self.parameters.MSG_REGISTER_INVALID_ZIP + "</li>");
                    valid = false;
                }
            }
            //End Zip

            if (valid) {
                var data = jQuery("#nhs_UpdateAccount input").serialize();

                jQuery.ajax({
                    type: "POST",
                    url: self.parameters.UpdateAccount,
                    data: data,
                    success: function () {
                        var message = self.parameters.Message;
                        alert(message);
                    }
                });
            } else {
                control.removeAttr("disabled");
            }
        });
    }
};