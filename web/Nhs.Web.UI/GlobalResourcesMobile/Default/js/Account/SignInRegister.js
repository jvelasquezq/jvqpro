﻿
NHS.Mobile.SignInRegister = function (parameters) {
    this.parameters = parameters;
};

var redirect = null;

NHS.Mobile.SignInRegister.prototype =
{
    ForgotPassword: function () {
        var self = this;

        jQuery('#nhs_Recover').keypress(function (e) {
            if (e.which === 13) {
                jQuery("#btnsubmit").click();
            }
        });

        if (self.parameters.IsAjaxRequest) {
            jQuery("#nhs_SignIn").click(function (event) {
                jQuery.CancelEventAndStopPropagation(event);
                jQuery.ajax({
                    type: "GET",
                    url: self.parameters.SignIn,
                    success: function (html) {
                        jQuery("#nhs_Recover").replaceWith(html);
                    }
                });
            });

        }

        jQuery("#btnsubmit").click(function () {
            var valid = true;
            var control = jQuery(this);
            control.attr("disabled", "disabled");
            var ul = jQuery("#nhs_Errors");
            ul.html("");

            //Start Email
            var email = jQuery("#nhs_RecoverForm #Email");
            email.removeAttr("style");
            if (!jQuery.ValidEmail(email.val())) {
                ul.append("<li>" + self.parameters.Translation.MSG_ACCOUNT_INVALID_EMAIL + "</li>");
                email.attr("style", "border: 1px solid #FF0000");
                valid = false;
            }
            //End Email

            if (valid) {
                var data = jQuery("#nhs_RecoverForm input").serialize();

                jQuery.ajax({
                    type: "POST",
                    url: self.parameters.ForgotPassword,
                    data: data,
                    success: function (data) {
                        if (data && data.Message) {
                            jQuery("#lblInstructions").html(data.Message).css("font-weight", "900");
                        } else if (data) {
                            jQuery("#nhs_Recover").parent().html(data);
                        }
                    }
                }).always(function () {
                    control.removeAttr("disabled");
                });
            } else {
                control.removeAttr("disabled");
            }
        });
    },

    SignIn: function() {
        var self = this;

        jQuery('#nhs_SignIn').keypress(function (e) {
            if (e.which === 13) {
                jQuery("#btnsubmit").click();
            }
        });
        jQuery("#nhs_SignInLink").click(function(event) {
            jQuery.CancelEventAndStopPropagation(event);
        });

        if (self.parameters.IsAjaxRequest) {
            jQuery("#nhs_NewAccount").click(function(event) {
                jQuery.CancelEventAndStopPropagation(event);
                jQuery.ajax({
                    type: "GET",
                    url: self.parameters.NewAccount,
                    success: function(html) {
                        jQuery("#nhs_SignIn").replaceWith(html);
                    }
                });
            });

            jQuery("#nhs_RecoverPassword").click(function(event) {
                jQuery.CancelEventAndStopPropagation(event);
                jQuery.ajax({
                    type: "GET",
                    url: self.parameters.RecoverPassword,
                    success: function(html) {
                        jQuery("#nhs_SignIn").replaceWith(html);
                    }
                });
            });

        }

        jQuery("#btnsubmit").click(function() {

            var valid = true;
            var control = jQuery(this);
            control.attr("disabled", "disabled");
            var ul = jQuery("#nhs_Errors");
            ul.html("");

            //Start Email
            var email = jQuery("#nhs_LoginForm #Email");
            email.removeAttr("style");
            if (!jQuery.ValidEmail(email.val())) {
                ul.append("<li>" + self.parameters.Translation.MSG_ACCOUNT_INVALID_EMAIL + "</li>");
                email.attr("style", "border: 1px solid #FF0000");
                valid = false;
            }
            //End Email

            //Start Password
            var password = jQuery("#nhs_LoginForm #Password");
            password.removeAttr("style");
            if (password.val().length === 0) {
                password.attr("style", "border: 1px solid #FF0000");
                ul.append("<li>" + self.parameters.Translation.EnterPassword + "</li>");
                valid = false;
            }
            else if (password.val().length < 5 || password.val().length > 20) {
                password.attr("style", "border: 1px solid #FF0000");
                ul.append("<li>" + self.parameters.Translation.MSG_ACCOUNT_INVALID_PASSWORD + "</li>");
                valid = false;
            }
            //End Password

            if (valid) {
                var data = jQuery("#nhs_LoginForm input").serialize();

                jQuery.ajax({
                    type: "POST",
                    url: self.parameters.SignIn,
                    data: data,
                    success: function(data) {
                        if (data.indexOf("typeof") === -1) {
                            control.removeAttr("disabled");
                        }
                    }
                }).always(function () {
                    control.removeAttr("disabled");
                });
            } else {
                control.removeAttr("disabled");
            }
        });
    },

    Register: function() {
        var self = this;

        jQuery('#nhs_Register').keypress(function (e) {
            if (e.which === 13) {
                jQuery("#btnsubmit").click();
            }
        });

        jQuery("#nhs_NewAccount").click(function(event) {
            jQuery.CancelEventAndStopPropagation(event);
        });

        if (self.parameters.IsAjaxRequest) {
            jQuery("#nhs_SignInLink").click(function(event) {
                jQuery.CancelEventAndStopPropagation(event);
                jQuery.ajax({
                    type: "GET",
                    url: self.parameters.SignIn,
                    success: function(html) {
                        jQuery("#nhs_Register").replaceWith(html);
                    }
                });
            });
        }

        jQuery("#International").change(function() {

            if (jQuery(this).is(":checked")) {
                jQuery("#nhs_AccountForm #ZipCodeReg").val("");
                jQuery("#ZipCodeReg").attr("disabled", "disabled");
            } else {
                jQuery("#ZipCodeReg").removeAttr("disabled");
            }
        });

        jQuery("#nhs_AccountForm #ZipCode").mask("99999");

        jQuery("#btnsubmit").click(function() {

            var valid = true;
            var control = jQuery(this);
            control.attr("disabled", "disabled");
            var ul = jQuery("#nhs_Errors");
            ul.html("");

            //Start Name
            var name = jQuery("#nhs_AccountForm #Name");
            name.removeAttr("style");
            if (!jQuery.ValidFullName(name.val())) {
                ul.append("<li>" + self.parameters.Translation.FirstAndLastName + "</li>");
                name.attr("style", "border: 1px solid #FF0000");
                valid = false;
            }
            //End Name

            //Start Email
            var email = jQuery("#nhs_AccountForm #Email");
            email.removeAttr("style");
            if (!jQuery.ValidEmail(email.val())) {
                ul.append("<li>" + self.parameters.Translation.MSG_ACCOUNT_INVALID_EMAIL + "</li>");
                email.attr("style", "border: 1px solid #FF0000");
                valid = false;
            }
            //End Email

            //Start Password
            var password = jQuery("#nhs_AccountForm #Password");
            password.removeAttr("style");
            if (password.val().length < 5) {
                password.attr("style", "border: 1px solid #FF0000");
                ul.append("<li>" + self.parameters.Translation.MSG_ACCOUNT_INVALID_PASSWORD + "</li>");
                valid = false;
            }
            //End Password

            //Start Zip
            if (!jQuery("#International").is(":checked")) {
                var zip = jQuery("#nhs_AccountForm #ZipCodeReg");
                zip.removeAttr("style");
                if (!jQuery.ValidZipcode(zip.val())) {
                    zip.attr("style", "border: 1px solid #FF0000");
                    ul.append("<li>" + self.parameters.Translation.MSG_REGISTER_INVALID_ZIP + "</li>");
                    valid = false;
                }
            }
            //End Zip

            if (valid) {
                var data = jQuery("#nhs_AccountForm input").serialize();

                jQuery.ajax({
                    type: "POST",
                    url: self.parameters.NewAccount,
                    data: data
                }).always(function () {
                    control.removeAttr("disabled");
                });
            } else {
                control.removeAttr("disabled");
            }
        });
    }
};
