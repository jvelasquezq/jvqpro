﻿NHS.Mobile.Share = function(parameters) {
    this.parameters = parameters;
}

NHS.Mobile.Share.prototype =
{
    init: function () {
        var self = this;
        jQuery(".nhs_Share").click(function (event) {
            jQuery.CancelEventAndStopPropagation(event);
            var url = self.parameters.ShareUrl;
            modalPopUp.Show(url, this);
        });
    },
    CreateShareLinks: function (titleShare, community, home) {

        var self = this;
        var text = titleShare.replace("{0}", (self.parameters.IsComm ? community : home));
        jQuery("#nhs_TitleShareModal").html(text);
        var cusoymUrl = self.parameters.customUlr;
        var url = "";
        if (typeof cusoymUrl == "undefined") {
            url = jQuery("link[rel='canonical']").attr("href");
            if (url.length === 0)
                url = location.origin + location.pathname + location.search;
        } else {
            url = cusoymUrl;
        }
        url = encodeURIComponent(url);
        var title = jQuery("title").text();
        var urlFacebook = "http://www.facebook.com/sharer/sharer.php?u=" + url + "%23.VElciHTHXTU.facebook&p[title]=" + title;
        var urlTwitter = "https://twitter.com/intent/tweet?original_referer=" + url + "&amp;text=" + title + "&amp;tw_p=tweetbutton&amp;url=" + url;

        jQuery("#nhs_Share_Facebook, #nhs_Share_Twitter").attr("data-commid", self.parameters.communityId).attr("data-builderid", self.parameters.builderId);

        //http://www.webdevelopersnotes.com/tips/html/html_mailto_attribute_html_tips.php3
        var mail = "mailto:?subject=" + title + "&body=" + url;
        jQuery("#nhs_Share_Mail").attr("href", mail);
        jQuery("#nhs_Share_Facebook").attr("href", urlFacebook);
        jQuery("#nhs_Share_Twitter").attr("href", urlTwitter);


        jQuery("#nhs_Share_Sms").click(function (event) {
            jQuery.CancelEventAndStopPropagation(event);
            modalPopUp.SetContentModal(self.parameters.SentToPhone, this);
        });
    }
};