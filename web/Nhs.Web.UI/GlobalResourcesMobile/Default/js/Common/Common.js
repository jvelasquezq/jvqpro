﻿NHS = {};
NHS.Mobile = {};
NHS.Scripts = {};
NHS.Scripts.Globals = {};

jQuery.extend({
    AdjustHeader: function() {
        if (jQuery(".nhs_ResContext, .nhs_CommContext, .nhs_HomesContext, .nhs_CmsContext, .nhs_HomeAccountCreated").length) {
            var headerBottom = jQuery(".nhs_Header").outerHeight()
                + jQuery(".nhs_ResContext, .nhs_CommContext, .nhs_HomesContext, .nhs_CmsContext, .nhs_HomeAccountCreated").outerHeight() - 2;
            jQuery(".nhs_ResList, .nhs_ResMap, .nhs_CommDetail, .nhs_HomesDetail, .nhs_CmsMain, .nhs_HomeMain").css("margin-top", headerBottom);
        }
    },
    ScreenSize: function(maxX, maxY) {
        var w = window,
            d = document,
            e = d.documentElement,
            g = d.getElementsByTagName('body')[0],
            x = w.innerWidth || e.clientWidth || g.clientWidth,
            y = w.innerHeight || e.clientHeight || g.clientHeight;

        if (typeof maxY != "undefined") {
            if (y > maxY)
                y = maxY;
        }

        if (typeof maxX != "undefined") {
            if (x > maxX)
                x = maxX;
        }
        return { x: x, y: y };
    },

    StopBubble: function(event) {
        event = event || window.event; // cross-browser event                
        if (event.stopPropagation) {
            // W3C standard variant                    
            event.stopPropagation();
        } else {
            // IE variant                   
            event.cancelBubble = true;
        }
    },

    CancelEvent: function(event) {
        var ev = event ? event : window.event; //Moz support requires passing the event argument manually     
        //Cancel the event   
        ev.cancelBubble = true;
        ev.returnValue = false;
        //if (ev.stopPropagation) ev.stopPropagation();
        if (ev.preventDefault) ev.preventDefault();
    },
    CancelEventAndStopPropagation: function(event) {
        var ev = event ? event : window.event; //Moz support requires passing the event argument manually     
        //Cancel the event   
        ev.cancelBubble = true;
        ev.returnValue = false;
        if (ev.stopPropagation) ev.stopPropagation();
        if (ev.preventDefault) ev.preventDefault();
    },

    ToString: function(obj) {
        var json = [];
        for (n in obj) {
            var v = obj[n];
            var t = typeof (v);
            if (t !== "object" && v !== null) {
                if (v !== false && v !== 0 && v !== "")
                    json.push(n + '=' + v);
            }
        }
        return json.join("&");
    },

    ShowLoading: function() {
        var control = jQuery("#nhs_Loading,#nhs_Overlay");
        if (control.length == 0) {
            var overlay = jQuery('<div id="nhs_Overlay" class="nhs_Overlay"></div>');
            var loading = jQuery('<div id="nhs_Loading" class="nhs_Loading"><p>Updating</p></div>');
            jQuery("body").append(overlay);
            jQuery("body").append(loading);
            loading.show();
            overlay.show();
        } else {
            control.show();
        }
    },

    HideLoading: function() {
        jQuery("#nhs_Overlay, #nhs_Loading").remove();
    },

    ValidFullName: function(name) {
        var pattern = new RegExp(/^[a-zA-Z]+\s[a-zA-Z]+$/i);
        return pattern.test(name);
    },

    ValidEmail: function(email) {
        var pattern = new RegExp(/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/i);
        return pattern.test(email);
    },

    ValidPhoneNumber: function(phoneNumber) {
        var pattern = new RegExp(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/i);
        return pattern.test(phoneNumber);
    },

    ValidFullNameWithNumberLastName: function(name) {
        var pattern = new RegExp(/^([A-Za-z]+(-?'?[A-Za-z+'?-?]+)?)(\s([A-Za-z]+(-?'?[A-Za-z+'?-?]+)?))+?$/i);
        return pattern.test(name) && name.split(' ').length > 1;
    },

    ValidZipcode: function(zip) {
        var pattern = new RegExp(/^\d{5}(-\d{4})?$/i);
        return pattern.test(zip);
    },

    ClearSearchBoxLocation: function() {
        jQuery("#locationInput").val('');
        var homePageTextBox = jQuery("#SearchText");
        if (homePageTextBox)
            homePageTextBox.val('');
        var searchBarLocationBox = jQuery("#searchBarLocationBox");
        if (searchBarLocationBox)
            searchBarLocationBox.val('');
    }
});

jQuery.fn.extend({
    ScrollTo: function (targetOffset) {
        if (typeof targetOffset == "undefined")
            targetOffset = this.offset().top;
        jQuery.ScrollTo(targetOffset, false);
    },
    ScrollToFast: function (targetOffset) {
        if (typeof targetOffset == "undefined")
            targetOffset = this.offset().top;
        jQuery.ScrollTo(targetOffset, true);
    },
    ScrollClickTo: function (targetOffset) {
        var self = this;
        this.click(function(mozEvent) {
            var ev = mozEvent ? mozEvent : window.event; //Moz support requires passing the event argument manually     
            //Cancel the event   
            ev.cancelBubble = true;
            ev.returnValue = false;
            if (ev.stopPropagation) ev.stopPropagation();
            if (ev.preventDefault) ev.preventDefault();
            self.ScrollTo(targetOffset);
        });
        return this;
    },
      ShowLoading: function () {
        var control = this.find(".nhs_Loading,.nhs_Overlay");
        if (control.length == 0) {
            var overlay = jQuery('<div class="nhs_Overlay" style="background-color: #cccccc"></div>');
            var loading = jQuery('<div class="nhs_Loading"><p>Updating</p></div>');
            this.append(overlay);
            this.append(loading);
            loading.show();
            overlay.show();
        } else {
            control.show();
        }
    },

    HideLoading: function() {
        this.find(".nhs_Overlay, .nhs_Loading").remove();
    },
});

if (!window.JSON) {
    window.JSON = {
        parse: function (sJSON) { return eval("(" + sJSON + ")"); },
        stringify: function (vContent) {
            if (vContent instanceof Object) {
                var sOutput = "";
                if (vContent.constructor === Array) {
                    for (var nId = 0; nId < vContent.length; sOutput += this.stringify(vContent[nId]) + ",", nId++);
                    return "[" + sOutput.substr(0, sOutput.length - 1) + "]";
                }
                if (vContent.toString !== Object.prototype.toString) {
                    return "\"" + vContent.toString().replace(/"/g, "\\$&") + "\"";
                }
                for (var sProp in vContent) {
                    sOutput += "\"" + sProp.replace(/"/g, "\\$&") + "\":" + this.stringify(vContent[sProp]) + ",";
                }
                return "{" + sOutput.substr(0, sOutput.length - 1) + "}";
            }
            return typeof vContent === "string" ? "\"" + vContent.replace(/"/g, "\\$&") + "\"" : String(vContent);
        }
    };
}

function detectIE() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    var trident = ua.indexOf('Trident/');

    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    if (trident > 0) {
        // IE 11 (or newer) => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    // other browser
    return false;
}

function getGUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g,
        function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
}

function loadExternalScript(url, callback) {

    var script = document.createElement("script");
    script.type = "text/javascript";

    if (script.readyState) {  //IE
        script.onreadystatechange = function () {
            if (script.readyState == "loaded" ||
                    script.readyState == "complete") {
                script.onreadystatechange = null;
                if (callback) {
                    callback();
                }
            }
        };
    } else {  //Others
    script.onload = function () {
        if (callback) {
            callback();
        }
    };
    }

    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}