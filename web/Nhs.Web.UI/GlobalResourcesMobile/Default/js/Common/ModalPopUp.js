﻿NHS.Mobile.ModalPopUp = function () { }

NHS.Mobile.ModalPopUp.prototype =
{
    CreateModal: function() {
        var self = this;
        self.modal = jQuery("#nhs_Modal");
        if (self.modal.length > 0)
            self.modal.remove();

        self.modal = jQuery('<div id="nhs_Modal" class="nhs_Modal clearfix">' +
            '<header class="clearfix">' +
            //'<p>Modal Window</p>' +
            '<span id="nhs_CloseModal" class="ir nhs_Close">Close</span>' +
            '</header>' +
            '<div id="nhs_ModalBody">' +
            '</div>' +
            '</div>');

        jQuery("body").append(self.modal);

        jQuery("#nhs_CloseModal").on('click', function(event) {
            self.CloseModal(event);
        });

        return self.modal;
    },

    Show: function(url, control) {
        var self = this;
        if (self.modal == null) {
            var modal = self.CreateModal();
            self.SetContentModal(url, control);
            modal.addClass("expanded");
        } else {
            self.SetContentModal(url, control);
        }
    },
    SetContentModal: function(url, control) {
        var self = this;
        if (self.modal == null) {
            self.Show(url, control);
        } else {
            jQuery("#nhs_ModalBody").html('<div id="nhs_Loading" style="display:block" class="nhs_Loading"><p>Updating</p></div>');
            jQuery.ajax({
                url: url,
                cache: false,
                success: function(html) {
                    jQuery("#nhs_ModalBody").html(html);
                    if (jQuery.isFunction(self.InsertedHtml))
                        self.InsertedHtml(control);
                }
            });
        }
    },

    CloseModal: function(event) {
        var self = this;
        jQuery.CancelEventAndStopPropagation(event);
        self.modal.removeClass("expanded");
        self.modal.remove();
        self.modal = null;
    }
};


jQuery(document).ready(function () {
    window.modalPopUp = new NHS.Mobile.ModalPopUp();
    tb_initLive('a.nhs_ModalPopUp');
});

function tb_initLive(domChunk) {
    jQuery(document).on("click", domChunk, function (e) {
        jQuery.CancelEventAndStopPropagation(e);
        var a = this.href || this.alt;
        modalPopUp.Show(a, this);
        this.blur();
    });
}
