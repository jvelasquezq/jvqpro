﻿NHS.Mobile.LogginEvents = function (parameters) {
    this._parameters = parameters;
    this.logger = new NHS.Scripts.Globals.EventLogger({ siteRoot: parameters.siteRoot, partnerId: parameters.partnerId, partnerName: parameters.partnerName, tdvTable: parameters.tdvTable });
    this.type = (parameters.planId == 0 && parameters.specId == 0 ? 'comm' : 'home');
};

NHS.Mobile.LogginEvents.prototype = {
    initialize: function() {
        var self = this;

        jQuery(".nhs_CallBtn").click(function() {
            //self.LogClick('LogPhoneNumber');
            self.logger.logEventWithParameters('PhoneClick', self._parameters);
        });


        jQuery(document).on("click","a.nhs_DirectionsBtn",function () {
            self.LogClick('ShowRoute');
        });

        jQuery(document).on("click", "a#nhs_Share_Mail", function () {
            self.LogClick('ShareByEmail');
        });

        jQuery(document).on("click", "a#nhs_Share_Facebook, a#nhs_Share_Twitter", function () {
            self.LogClick('SocialMediaShare');
        });

        jQuery(".nhs_builderShowCase").click(function () {
            self.LogClick('LogBuilderShowCase');
        });
        

//        jQuery(".nhs_Green a").click(function() {
//            var onclick = jQuery(this).attr("onclick");
//            if (onclick.indexOf("http") > -1) {
//                self.LogClick('GreenPromo');
//            }
//        });
    },
    LogClick: function (method) {
        var self = this;
        var dataPost = {
            communityId: self._parameters.commId,
            builderId: self._parameters.builderId,
            planId: self._parameters.planId,
            specId: self._parameters.specId
        };
        jQuery.ajax({
            type: "POST",
            url: self._parameters.logActionMethodUrl + method,
            data: dataPost,
            dataType: "json"
        });
    }
};