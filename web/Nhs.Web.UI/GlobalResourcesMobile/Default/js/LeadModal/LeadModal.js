﻿ NHS.Mobile.LeadModal = function (parameters) {
    this.parameters = parameters;
};

NHS.Mobile.LeadModal.prototype =
{
    initialize: function () {
        var self = this;
        if (self.parameters.DisplayDatePickerInSpanish) {
            jQuery.datepicker.regional["es"] = {
                closeText: "Cerrar",
                prevText: "<Ant",
                nextText: "Sig>",
                currentText: "Hoy",
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                dayNamesShort: ["Dom", "Lun", "Mar", "Mié", "Juv", "Vie", "Sáb"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
                weekHeader: "Sm",
                dateFormat: "dd/mm/yy",
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ""
            };
            jQuery.datepicker.setDefaults(jQuery.datepicker.regional["es"]);
        }
        
        jQuery("#datepicker").datepicker({
            minDate: 0,
            showOtherMonths: true,
            altField: "#AppointmentDateTime"
        });

        jQuery("#nhs_AccountForm").on("change", "input[type=checkbox]", function () {
            var control = jQuery(this);
            control.val(control.is(":checked"));
        });

        var form = jQuery("#nhs_AccountForm");
        form.find("#Phone").mask("(999) 999-9999");
        var international = form.find("#International");
        var zip = form.find("#Zip");


        jQuery("#nhs_AccountForm").keypress(function (e) {
            if (e.which == 13) {
                jQuery("#nhs_GetBrochure").click();
            }
        });

        international.change(function () {
            if (international.is(":checked")) {
                zip.attr("disabled", "disabled");
                zip.val("");
                zip.removeAttr("style");
            } else {
                zip.removeAttr("disabled");
            }
        });

        form.find("#Zip, #Name, #Email").keyup(function () {
            jQuery(this).removeAttr("style");
        });

        form.find("#AppointmentDateTime").attr("disabled", "disabled");
        jQuery("#nhs_ajaxContent").on("change", "input[type=checkbox]", function () {
            var control = jQuery(this);
            control.val(control.is(":checked"));
        });

        form.find("#nhs_GetBrochure").click(function () {
            var ul = jQuery("#nhs_Errors");
            ul.html("");

            var valid = true;
            var control = jQuery(this);
            control.attr("disabled", "disabled");

            //Start Name
            var name = form.find("#Name");
            name.removeAttr("style");
            if (!jQuery.ValidFullName(name.val())) {
                ul.show();
                ul.append("<li>" + self.parameters.FirstAndLastNameRequiredText + "</li>");
                name.attr("style", "border: 1px solid #FF0000");
                valid = false;
            }
            //End Name

            //Start Email
            var email = form.find("#Email");
            email.removeAttr("style");
            if (!jQuery.ValidEmail(email.val())) {
                ul.show();
                ul.append("<li>" + self.parameters.EmailRequiredText + "</li>");
                email.attr("style", "border: 1px solid #FF0000");
                valid = false;
            }
            //End Email

            //Start Zip
            if (!international.is(":checked")) {
                zip.removeAttr("style");
                if (!jQuery.ValidZipcode(zip.val())) {
                    ul.show();
                    zip.attr("style", "border: 1px solid #FF0000");
                    ul.append("<li>" + self.parameters.EnterAValidZipCodeText + "</li>");
                    valid = false;
                }
            }
            //End Zip

            if (valid) {
                var data = form.find("input,textarea").serialize() + "&AppointmentDateTime=" + jQuery("#AppointmentDateTime").val();
                self.GeneralAjaxCallAction(control, data, self.parameters.BrochureUrl, self.initializeThankYouModal, self);
            } else {
                control.removeAttr("disabled");
            }
        });
    },

    initializeThankYouModal: function () {
        var self = this;

        jQuery("#nhs_AccountInformation").keypress(function (e) {
            if (e.which == 13) {
                jQuery("#nhs_AccountInformation #nhs_SignIn").click();
            }
        });

        jQuery("#nhs_AccountInformation #nhs_Forgot_Password").click(function (event) {
            jQuery.CancelEventAndStopPropagation(event);
            var control = jQuery(this);
            var data = { email: jQuery("#nhs_AccountInformation #Email").val() };
            self.GeneralAjaxCallAction(control, data, self.parameters.RecoverPasswordUrl, function () {
                control.replaceWith(self.parameters.YourPasswordWillBeSendText);
            }, self);
        });

        jQuery("#nhs_Close").click(function (event) {
            window.modalPopUp.CloseModal(event);
        });

        jQuery("#nhs_AccountInformation #nhs_SignIn").click(function () {
            var control = jQuery(this);
            control.attr("disabled", "disabled");
            var valid = true;

            var ul = jQuery("#nhs_AccountInformation #nhs_error");
            ul.html("");
            //Start Password
            var password = jQuery("#nhs_AccountInformation #password");
            password.removeAttr("style");
            if (password.val().length == 0) {
                password.attr("style", "border: 1px solid #FF0000");
                ul.show();
                ul.append("<li>Enter your password</li>");
                valid = false;
            }
            else if (password.val().length < 5 || password.val().length > 20) {
                password.attr("style", "border: 1px solid #FF0000");
                ul.show();
                ul.append("<li>Your password must be between 5 and 20 characters</li>");
                valid = false;
            }
            //End Password

            if (valid) {
                var data = jQuery("#nhs_AccountInformation input").serialize();
                self.GeneralAjaxCallAction(control, data, self.parameters.ThankYouModalUrl, function (validation) {
                    if (validation) {
                        var isExistingUser = jQuery("#IsExistingUser").val() == "True";
                        var message = jQuery("#Name").val() + " is now signed in";
                        if (!isExistingUser)
                            message = "Account created! " + message;

                        alert(message);
                        window.location.reload();
                    }
                    else {
                        control.removeAttr("disabled");
                        ul.show();
                        ul.append("<li>Sorry, the password you entered is not correct. Please check your spelling and try again.</li>");
                    }
                }, self);
            } else
                control.removeAttr("disabled");
        });
    },

    GeneralAjaxCallAction: function (control, data, url, action, self) {

        control.attr("disabled", "disabled");
        jQuery.ajax({
            url: url,
            type: "POST",
            data: data,
            success: function (html) {
                if (typeof html === "string") {
                    jQuery("#nhs_FormReplace").replaceWith(html);
                    action.call(self);
                } else {
                    action.call(self, html);
                }
            }, error: function () {
                control.removeAttr("disabled");
            }
        });
    }
};
