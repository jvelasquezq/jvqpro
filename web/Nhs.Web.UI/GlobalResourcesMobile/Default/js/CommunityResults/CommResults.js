﻿// Global Class for Community Results Page
NHS.Mobile.CommResults = function (parameters) {
    this.parameters = parameters;
    this.searchParameters = jQuery.extend(true, {}, parameters.searchParameters);
    this.defaultSearchParameters = jQuery.extend(true, {}, parameters.searchParameters);
    this.LocationChange = false;
    this.isMapCreate = false;
    this.searchWithinMap = parameters.searchWithinMap;
    this.isCurrentPosition = parameters.IsCurrentLocation;
};

NHS.Mobile.CommResults.prototype =
{
    init: function() {
        var self = this;
        self.initMap();
        self.AttachEventsToFacets();
        self.SortBy();

        if (self.isCurrentPosition)
            self.GetCurrentLocationAction(false);
        
        jQuery(".nhs_MoreLink, .nhs_MoreClose").on('click', function (event) {
            jQuery.CancelEventAndStopPropagation(event);
            jQuery(".nhs_MoreLink > span").toggleClass("expanded");
            if (jQuery(".nhs_MorePanel").hasClass("expanded")) {
                jQuery(".nhs_MoreLink > span").html(self.parameters.Translation.Show);

                if (jQuery(this).hasClass("nhs_MoreClose")) {
                    jQuery(".nhs_MorePanel").toggleClass("expanded").slideUp("slow");
                } else {
                    jQuery(".nhs_MorePanel").toggleClass("expanded").slideUp();
                }
            } else {
                jQuery(".nhs_MoreLink > span").html(self.parameters.Translation.Hide);
                jQuery(".nhs_MorePanel").toggleClass("expanded").slideDown("slow");
            }

        });

        modalPopUp.InsertedHtml = function (link) {
            jQuery("#nhs_SavedQuickView").click(function (event) {
                jQuery.CancelEventAndStopPropagation(event);
                var url = this.href;
                jQuery.ajax({
                    url: url,
                    type: "POST",
                    success: function (data) {
                        jQuery("#nhs_SavedQuickView").addClass("nhs_Saved").html(self.parameters.Translation.Saved);
                    }
                });
            });

            jQuery('#nhs_ModalBody .nhs_CallBtn').click(function(event) {
                event.stopPropagation();
            });
        };

        jQuery("#nhs_MoreResults").click(function(event) {
            jQuery.CancelEventAndStopPropagation(event);
            self.BindResults(true);
        });

        jQuery(".nhs_Sort").click(function(event) {
            jQuery.CancelEventAndStopPropagation(event);
            jQuery("#nhs_Header").css("position", "static");
            jQuery(this).parent().find(".nhs_ResSort").addClass("expanded");
        });

        jQuery(document).on('click', '.nhs_Row', function (event) {
            jQuery.CancelEventAndStopPropagation(event);
            if (event.type == "click") {
                var url;
                url = jQuery(this).find("a:first-of-type").attr("href");
                window.location.href = url;
            }
        });

        jQuery(document).on("click", ".nhs_ModalPopUp", function (event) {
            jQuery.CancelEventAndStopPropagation(event);
        });

        jQuery(document).on("click", ".nhs_CallBtn", function (event) {
            event.stopPropagation();
            var control = jQuery(this);
            var dataPost = {
                communityId: control.data("commid"),
                builderId: control.data("builderid"),
                eventName: self.parameters.MobileCallSalesOfficeEvent
            };
            self.LogEvents(dataPost);
        });

        jQuery(document).on("click", ".nhs_DirectionsBtn", function () {
            var control = jQuery(this);
            var dataPost = {
                communityId: control.data("commid"),
                builderId: control.data("builderid"),
                eventName: self.parameters.ShowRouteEvent
            };
            self.LogEvents(dataPost);
        });

        jQuery(document).on("click", "a#nhs_Share_Facebook, a#nhs_Share_Twitter", function () {
            var control = jQuery(this);
            var dataPost = {
                communityId: control.data("commid"),
                builderId: control.data("builderid"),
                eventName: self.parameters.ShareEvent
            };
            self.LogEvents(dataPost);
        });


        jQuery(".nhs_MapView").click(function(event) {
            jQuery.CancelEventAndStopPropagation(event);
            jQuery("#nhs_ResList, #nhs_Sort, #nhs_MapView, #nhs_Footer, .nhs_AdSection, #nhs_ResMktTxt").hide();
            jQuery("#nhs_ResMap, #nhs_ListView").show();
            jQuery.AdjustHeader();

            self.searchParameters.IsMapVisible = true;
            self.defaultSearchParameters.IsMapVisible = true;
            self.ChangeMapStatus();

            if (!self.isMapCreate) {
                self.updateMap = true;
                self.isMapCreate = true;
                self.googleApi.createMap();
                if (self.getCurrentPosition != null) {

                    self.googleApi.map.setCenter(self.getCurrentPosition);

                    if (self.marker != null) {
                        self.marker.setMap(null);
                    }

                    var options = {
                        icon: self.parameters.iconCurrentPosition,
                        position: self.getCurrentPosition,
                        optimized: true,
                        zIndex: 99,
                        animation: google.maps.Animation.BOUNCE
                    };
                    self.marker = new google.maps.Marker(options);
                    self.marker.setMap(self.googleApi.map);
                    setTimeout(function () { self.marker.setAnimation(null); }, 5000);
                }
            } else
                self.UpdateResults(false);
        });

        jQuery(".nhs_ListView").click(function (event) {
            jQuery.CancelEventAndStopPropagation(event);
            jQuery("#nhs_ResMap, #nhs_ListView").hide();
            jQuery("#nhs_ResList, #nhs_Sort, #nhs_MapView, #nhs_Footer, .nhs_AdSection, #nhs_ResMktTxt").show();
            jQuery.AdjustHeader();

            self.searchParameters.IsMapVisible = false;
            self.defaultSearchParameters.IsMapVisible = false;
            self.ChangeMapStatus();                        
            self.UpdateResults(false);
        });
        self.GetBrands();
    },

    SearchCurrentLocation: function() {
        var self = this;
        self.GetCurrentLocationAction(true);
    },

    LogEvents: function (dataPost) {
        var self = this;
        jQuery.ajax({
            type: "POST",
            url: self.parameters.logActionMethodUrl,
            data: dataPost,
            dataType: "json"
        });
    },

    ChangeMapStatus: function () {
         var self = this;
        jQuery.post( self.parameters.ChangeMapStatusUrl, { isMapVisible: self.searchParameters.IsMapVisible } );
     },

    SetPersonalCoockie: function () {
        var self = this;
        jQuery.ajax({
            type: "POST",
            url: self.parameters.SetPersonalCoockie,
            data: { lat: self.searchParameters.OriginLat, lng: self.searchParameters.OriginLng },
            async: false
        });
    },

    UpdateResults: function (moreResults, action) {

        var self = this;
        if (self.isCurrentPosition == true) {
            self.SetPersonalCoockie();
        }

        self.PlotMap();
        if (self.updateResults) {
            self.updateResults = false;
            self.BindResults(moreResults, action);
        }
    },

    BindResults: function(moreResults, action) {

        jQuery.ShowLoading();
        var self = this;
        if (moreResults) {
            self.searchParameters.PageNumber++;
            if (self.parameters.Pages === self.searchParameters.PageNumber) {
                jQuery("#nhs_MoreResults").hide();
            }
        } else {
            self.searchParameters.PageNumber = 1;
            if (self.LocationChange) {
                self.RedirectSearch();
                return;
            }
        }
        var url = self.parameters.getResultsUrl;

        jQuery.ajax({
            url: url,
            type: "POST",
            data: JSON.stringify({ 'parameters': self.searchParameters, 'moreResults': moreResults }),
            contentType: 'application/json',
            cache: false,
            success: function(results) {
                if (moreResults)
                    jQuery("#nhs_results").append(results);
                else {
                    jQuery("#nhs_results").html(results);
                    jQuery.ScrollToFast(0);

                    self.searchParameters = jQuery.extend(true, {}, SearchParameters);
                    self.defaultSearchParameters = jQuery.extend(true, {}, SearchParameters);

                    self.parameters.Pages = Pages;
                    if (Pages > 1)
                        jQuery("#nhs_MoreResults").show();
                    else
                        jQuery("#nhs_MoreResults").hide();

                    self.ResetFacetsToDefault(true);
                    self.GetBrands();
                }

                if (jQuery.isFunction(action)) {
                    action();
                }

                jQuery.HideLoading();
            }
        }).fail(function() {
            if (jQuery.isFunction(action)) {
                action();
            }
            jQuery.HideLoading();
        });
    },

    GetBrands: function () {
        var self = this;
        var url = self.parameters.getBrandsUrl + "?" + jQuery.ToString(self.searchParameters);
        jQuery.ajax({
            url: url,
            type: "GET",
            cache: true,
            success: function (results) {
                self.UpdateDropDowns("BuilderName", results, self.parameters.Translation.AllBuilders);
                //Start Builder Name
                jQuery("#BuilderName").val(self.searchParameters.BrandId);
                //End Builder Name
            }
        });
    }
};