﻿NHS.Mobile.CommResults.prototype.AttachEventsToFacets = function () {
    var self = this;
    self.search = new NHS.Mobile.Search(self.parameters.typeAheadParameters);
    self.search.initialize();
    self.updateFacets = false;
    self.LocationChange = false;

    jQuery("#FilterLocation").click(function () {
        this.select();
    });

    jQuery("#nhs_LocationSearchFormShow").click(function (event) {
        jQuery.CancelEventAndStopPropagation(event);
        self.searchWithinMap = false;
        self.searchParameters.MinLat = self.searchParameters.MinLng = self.searchParameters.MaxLat = self.searchParameters.MaxLng = 0;
        self.searchParameters.GetRadius = true;
        self.searchParameters.GetLocationCoordinates = true;
        self.updateResults = true;
        self.updateMap = self.searchParameters.IsMapVisible;
        self.BindResults(false, function () {
            self.updateMap = true;
            self.PlotMap();
            self.ResetFacetsToDefault();
        });
        jQuery(this).parent().parent().removeClass("expanded");
    });

    jQuery('#nhs_ClearFacets').click(function (event) {
        jQuery.CancelEventAndStopPropagation(event);
        self.ResetFacets(false);
    });

    self.search.LocationChange = function () {
        self.LocationChange = true;
        self.updateFacets = true;
    };

    self.search.AutoCompleteSelect = function () {
        var textBox = jQuery("#" + self.search.parameters.textBoxName);
        if (textBox.length > 0 && textBox.val().trim().toLowerCase() === self.parameters.Translation.CurrentLocation) {
            self.SearchCurrentLocation();
        } else {
            self.LocationChange = true;
            self.updateFacets = true;
            self.GetLocation();
            self.isCurrentPosition = false;
            self.getCurrentPosition = null;
            self.searchParameters.CustomRadiusSelected = false;
        }
    };

    jQuery(".nhs_Filter").click(function (event) {
        jQuery.CancelEventAndStopPropagation(event);
        jQuery("#nhs_PanelScroll").scrollTop(0);
        jQuery("#nhs_CommunityCards").html("");
        jQuery("#nhs_Header").css("position","static");
        self.ResetFacetsToDefault();
        jQuery(this).parent().find(".nhs_ResFilter").addClass("expanded");
    });


    jQuery("#nsh_ApplyFilters").click(function (event) {
        jQuery.CancelEventAndStopPropagation(event);
        var textBox = jQuery("#" + self.search.parameters.textBoxName);
        if (textBox.length > 0 && textBox.val().trim().toLowerCase() === self.parameters.Translation.CurrentLocation) {
            self.GetCurrentLocationAction();
            self.LocationChange = false;
        }

        if (self.updateFacets) {
            self.updateResults = true;
            self.updateMap = true;
            self.isCurrentPosition = self.getCurrentPosition != null;
            self.UpdateResults(false);
        }
        jQuery("#nhs_Header").css("position", "fixed");
        jQuery(this).parent().parent().removeClass("expanded");
        jQuery(this).removeClass("nhs_Apply");
    });

    jQuery(".nhs_Cancel").click(function(event) {
        jQuery.CancelEventAndStopPropagation(event);
        jQuery("#nhs_Header").css("position", "fixed");
        jQuery(this).parent().parent().removeClass("expanded");
        jQuery("#nsh_ApplyFilters").removeClass("nhs_Apply");
        self.updateFacets = false;
        self.searchParameters = jQuery.extend(true, {}, self.defaultSearchParameters);
        self.search.Location = null;
        if (!self.isCurrentPosition) {
            self.getCurrentPosition = null;
        }
        self.GetBrands();
        self.LocationChange = false;
        self.updateMap = false;
        self.SetTitleResults(parseInt(jQuery("#nhs_CommCount").val()));
    });

    //Start Radius
    jQuery("#nhs_SliderRadius").slider({
        min: 0,
        max: (self.parameters.FacetsOptions.LocationSliderSmallText.length - 1),
        step: 1,
        value: 0,
        disabled: self.searchParameters.IsMultiLocationSearch,
        slide: function (event, ui) {
            if (ui.value > 0 || self.getCurrentPosition != null) {
                self.searchParameters.WebApiSearchType = 'Radius';
            } else {
                self.searchParameters.WebApiSearchType = 'Exact';
            }
            self.searchParameters.Radius = self.parameters.FacetsOptions.LocationSliderValues[ui.value];
            jQuery('#nhs_RadiusValue').html(self.parameters.FacetsOptions.LocationSliderSmallText[ui.value]);
            self.searchParameters.GetRadius = false;
            self.searchParameters.GetLocationCoordinates = false;
            if (self.getCurrentPosition== null)
                self.searchParameters.CustomRadiusSelected = true;
        },
        stop: function() {
            self.UpdateCounts(function() {
                self.GetBrands();
            });
            self.searchWithinMap = false;
        }
    });
    //Ends Radius


    //Start Price Padding
    jQuery("#nhs_SliderPrice").slider({
        range: true,
        animate: true,
        min: 0,
        max: (self.parameters.FacetsOptions.PriceSliderValues.length - 1),
        step: 1,
        values: [0, (self.parameters.FacetsOptions.PriceSliderValues.length - 1)],
        start: function (event, ui) {
            if (ui.values[1] === ui.values[0] && ui.values[0] > 27) {
                jQuery("#nhs_PriceSlider").slider("option", "values", [27, ui.values[1]]);
                self.searchParameters.PriceLow = self.parameters.FacetsOptions.PriceSliderValues[27];
            }
        },
        slide: function (event, ui) {
            if ((self.parameters.FacetsOptions.PriceSliderValues.length - 1) === ui.values[1]) {
                self.searchParameters.PriceHigh = 0;
                jQuery('#PriceHigh').html("No Max");
            } else {
                self.searchParameters.PriceHigh = self.parameters.FacetsOptions.PriceSliderValues[ui.values[1]];
                self.facetPriceFormat(jQuery('#PriceHigh').html(self.parameters.FacetsOptions.PriceSliderValues[ui.values[1]]));
            }

            if (ui.values[0] === 0) {
                self.searchParameters.PriceLow = 0;
                jQuery('#PriceLow').html("No Min");
            } else {
                self.searchParameters.PriceLow = self.parameters.FacetsOptions.PriceSliderValues[ui.values[0]];
                self.facetPriceFormat(jQuery('#PriceLow').html(self.parameters.FacetsOptions.PriceSliderValues[ui.values[0]]));
            }

        },
        stop: function () {
            self.UpdateCounts();
        }
    });
    //End Price Padding

    //Start Beds
    jQuery("input[name=Beds]").click(function () {
        self.searchParameters.Bedrooms = jQuery(this).val();
        self.UpdateCounts();
    });
    //End Beds

    //Start Baths
    jQuery("input[name=Baths]").click(function () {
        self.searchParameters.Bathrooms = jQuery(this).val();
        self.UpdateCounts();
    });
    //End Baths

    //Start Home Type
    jQuery("input[name=HomeType]").click(function () {
        self.searchParameters.SingleFamily = jQuery("#HomeTypeSingle").is(":checked");
        self.searchParameters.MultiFamily = jQuery("#HomeTypeCondo").is(":checked");
        self.UpdateCounts();
    });
    //End Home Type

    //Start Home Status
    jQuery("input[name=HomeStatus]").click(function () {
        self.searchParameters.Qmi = jQuery("#HomeStatusQuick").is(":checked");
        self.UpdateCounts();
    });
    //End Home Status

    //Start Special Offers
    jQuery("input[name=SpecialOffers]").click(function () {
        self.searchParameters.HotDeals = jQuery("#OffersDeals").is(":checked");
        self.UpdateCounts();
    });
    //End Special Offers

    //Start Garages
    jQuery("input[name=Garages]").click(function () {
        var value = jQuery(this).val();
        self.searchParameters.Garages = value;
        self.UpdateCounts();
    });
    //End Garages
    //Start Builder Name
    jQuery(".nhs_PanelLabelInline").on('change', "#BuilderName", function () {
        self.searchParameters.BrandId = jQuery(this).val();
        self.searchParameters.CommId = 0;
        self.searchParameters.CommName = '';
        jQuery("#CommName").val(0);
        self.UpdateCounts();
    });
    //End Builder Name

    //Start Community Status
    jQuery(".nhs_PanelLabelInline").on('change', "#CommStatus", function () {
        self.searchParameters.CommunityStatus = jQuery(this).val();
        self.UpdateCounts();
    });
    //End Community Status


    //Start Amenities
    jQuery("#nhs_Facet_Amenities").on("change", "input", function () {
        if (jQuery(this).is("#AmenitiesAny")) {
            //Start Amenities
            self.searchParameters.Pool = false;
            self.searchParameters.Views = false;
            self.searchParameters.Parks = false;
            self.searchParameters.Green = false;
            self.searchParameters.Gated = false;
            self.searchParameters.GolfCourse = false;
            self.searchParameters.Waterfront = false;
            self.searchParameters.NatureAreas = false;
            self.searchParameters.Sports = false;
            self.searchParameters.Adult = false;
            jQuery("#nhs_Facet_Amenities input").not("#AmenitiesAny").removeAttr("checked");
            jQuery("#AmenitiesAny").prop("checked", "checked");
        } else {
            //Start Amenities
            self.searchParameters.Pool = jQuery("#AmenitiesPool").is(":checked");
            self.searchParameters.Adult = jQuery("#AmenitiesAdultCommunity").is(":checked");
            self.searchParameters.Views = jQuery("#AmenitiesView").is(":checked");
            self.searchParameters.Parks = jQuery("#AmenitiesPark").is(":checked");
            self.searchParameters.Gated = jQuery("#AmenitiesGated").is(":checked");
            self.searchParameters.Green = jQuery("#AmenitiesGreen").is(":checked");
            self.searchParameters.Sports = jQuery("#AmenitiesSportFacility").is(":checked");
            self.searchParameters.Waterfront = jQuery("#AmenitiesWaterfront").is(":checked");
            self.searchParameters.GolfCourse = jQuery("#AmenitiesGolf").is(":checked");
            self.searchParameters.NatureAreas = jQuery("#AmenitiesNatureArea").is(":checked");

            if (self.searchParameters.Pool || self.searchParameters.GolfCourse || self.searchParameters.Gated || self.searchParameters.Adult
                    || self.searchParameters.Waterfront || self.searchParameters.Views || self.searchParameters.Parks || self.searchParameters.Green
                    || self.searchParameters.NatureAreas || self.searchParameters.Sports) {

                jQuery("#AmenitiesAny").removeAttr("checked");
            } else {
                jQuery("#AmenitiesAny").prop("checked", "checked");
            }
        }

        self.UpdateCounts();
    });
    //End Amenities

    self.ChangeState();
};

NHS.Mobile.CommResults.prototype.facetPriceFormat = function(control) {
    control.priceFormat({
        prefix: '$',
        thousandsSeparator: ',',
        centsLimit: 0,
        insertPlusSign: false,
        limit: 7
    });
};

NHS.Mobile.CommResults.prototype.ChangeState = function() {

    var self = this;
    if (self.searchWithinMap) {
        jQuery("#nhs_LocationSearch").hide();
        jQuery("#nhs_LocationSearchBlock").show();
        jQuery("#nhs_SliderRadius").slider("option", "disabled", true);
        self.searchParameters.Radius = 0;

    } else {
        jQuery("#nhs_LocationSearchBlock").hide();
        jQuery("#nhs_LocationSearch").show();
        if(!self.searchParameters.IsMultiLocationSearch)
            jQuery("#nhs_SliderRadius").slider("option", "disabled", false);
    }

    if (self.isCurrentPosition) {
        jQuery(".btn_Locate").addClass("nhs_ActiveLocation");
    } else {
        jQuery(".btn_Locate").removeClass("nhs_ActiveLocation");
    }
};

NHS.Mobile.CommResults.prototype.BuildUpdateDropDowns = function () {
    var self = this;
    self.UpdateDropDowns("BuilderName", self.parameters.Brands, self.parameters.Translation.AllBuilders);
    self.UpdateDropDowns("CommName", self.parameters.Communities, self.parameters.Translation.AllCommunities);
};

NHS.Mobile.CommResults.prototype.ResetFacetsToDefault = function() {
    var self = this;
    self.searchParameters = jQuery.extend(true, {}, self.defaultSearchParameters);
    
    var commCount = parseInt(jQuery("#nhs_CommCount").val());
    var homeCount = parseInt(jQuery("#nhs_CommHomeCount").val());
    self.SetTitleResults(commCount);

    var html = commCount === 0 && homeCount === 0 ? self.parameters.Translation.NoResultsFound
    : (commCount + ' ' + (commCount > 1 ? self.parameters.Translation.Communities : self.parameters.Translation.Community)
    + ' <span>|</span> ' + homeCount + ' ' + self.parameters.Translation.Home + (homeCount > 1 ? 's' : ''));
    jQuery("#nhs_FacetCounts > span").html(html);

    self.ResetFacetsToDefaultBase();
};

NHS.Mobile.CommResults.prototype.ResetFacetsToDefaultBase = function () {
    var self = this;

    if (self.isCurrentPosition || self.getCurrentPosition != null) {
        jQuery(".btn_Locate").addClass("nhs_ActiveLocation");
    } else {
        jQuery(".btn_Locate").removeClass("nhs_ActiveLocation");
    }

    //self.GetBrands();
    self.setPriceSliderValues();
    jQuery("#nhs_SliderRadius").slider("option", "value", self.parameters.FacetsOptions.LocationSliderValues.indexOf(self.searchParameters.Radius));
    jQuery('#nhs_RadiusValue').html(self.parameters.FacetsOptions.LocationSliderSmallText[self.parameters.FacetsOptions.LocationSliderValues.indexOf(self.searchParameters.Radius)]);
  
    //Start Beds
    jQuery("input[name=Beds]").filter('[value="' + self.searchParameters.Bedrooms + '"]').prop("checked", "checked");
    //End Beds

    //Start Baths
    jQuery("input[name=Baths]").filter('[value="' + self.searchParameters.Bathrooms + '"]').prop("checked", "checked");
    //End Baths

    //Start Home Type
    if (self.searchParameters.SingleFamily) {
        jQuery("#HomeTypeSingle").prop("checked", "checked");
    } else if (self.searchParameters.MultiFamily) {
        jQuery("#HomeTypeCondo").prop("checked", "checked");
    } else {
        jQuery("#HomeTypeAny").prop("checked", "checked");
    }
    //End Home Type

    //Start Home Status
    if (self.searchParameters.Qmi) {
        jQuery("#HomeStatusQuick").prop("checked", "checked");
    } else {
        jQuery("#HomeStatusAll").prop("checked", "checked");
    }
    //End Home Status

    //Start Special Offers
    if (self.searchParameters.HotDeals) {
        jQuery("#OffersDeals").prop("checked", "checked");
    } else {
        jQuery("#OffersAll").prop("checked", "checked");
    }
    //End Special Offers

    //Start Garages
    jQuery("input[name=Garages]").filter('[value="' + self.searchParameters.Garages + '"]').prop("checked", "checked");
    //End Garages

    //Start Community Status
    jQuery("#CommStatus").val(self.searchParameters.CommunityStatus);
    //End Community Status

    //Start Community Name
    jQuery("#CommName").val(self.searchParameters.CommId);
    //End Community Name

    //Start Builder Name
    jQuery("#BuilderName").val(self.searchParameters.BrandId);
    //End Builder Name

    //Start Amenities
    jQuery("#AmenitiesPool").prop("checked", self.searchParameters.Pool);
    jQuery("#AmenitiesView").prop("checked", self.searchParameters.Views);
    jQuery("#AmenitiesPark").prop("checked", self.searchParameters.Parks);
    jQuery("#AmenitiesGolf").prop("checked", self.searchParameters.GolfCourse);

    jQuery("#AmenitiesGreen").prop("checked", self.searchParameters.Green);
    jQuery("#AmenitiesGated").prop("checked", self.searchParameters.Gated);

    jQuery("#AmenitiesWaterfront").prop("checked", self.searchParameters.Waterfront);
    jQuery("#AmenitiesNatureArea").prop("checked", self.searchParameters.NatureAreas);

    jQuery("#AmenitiesSportFacility").prop("checked", self.searchParameters.Sports);
    jQuery("#AmenitiesAdultCommunity").prop("checked", self.searchParameters.Adult);

    if (self.searchParameters.Pool || self.searchParameters.GolfCourse || self.searchParameters.Gated || self.searchParameters.Adult
                || self.searchParameters.Waterfront || self.searchParameters.Views || self.searchParameters.Parks || self.searchParameters.Green
                || self.searchParameters.NatureAreas || self.searchParameters.Sports) {

        jQuery("#AmenitiesAny").removeAttr("checked");
    } else
        jQuery("#AmenitiesAny").prop("checked", "checked");
    //End Amenities

}; 

NHS.Mobile.CommResults.prototype.SetTitleResults = function (commCount) {
    var self = this;

    var text = '';
    if (self.searchParameters.MarketId > 0)
        text = self.searchParameters.MarketName + ", " + self.searchParameters.State + ' Area';

    if (self.searchParameters.WebApiSearchType === 'Radius') {
        if (self.searchParameters.City)
            text = self.searchParameters.City + ", " + self.searchParameters.State;

        if (self.searchParameters.County)
            text = self.searchParameters.County + " " + self.parameters.Translation.County+ ", " + self.searchParameters.State;

        if (self.searchParameters.PostalCode)
            text = self.searchParameters.PostalCode + ", " + self.searchParameters.State;
    }

    if (self.getCurrentPosition != null)
        text = self.parameters.Translation.CurrentLocation;

    if (self.searchParameters.IsMultiLocationSearch)
        text = self.searchParameters.SyntheticInfo.Name;
    
    if (self.searchWithinMap)
        text = self.parameters.Translation.ThisArea;

    if (commCount === 0)
        jQuery("#nhs_Title").html(self.parameters.Translation.NoResultsFound + " " + self.parameters.Translation.In);
    else {
        var location = " " + self.parameters.Translation.FoundIn + " ";
        if (self.getCurrentPosition != null)
            location = " " + self.parameters.Translation.NearYour + " ";
        jQuery("#nhs_Title").html(commCount + " " + (commCount === 1 ? self.parameters.Translation.Community : self.parameters.Translation.Communities) + location);
    }

    jQuery("#nhs_TitleLocation").html(text);
    jQuery("#FilterLocation").val(text);

    if (commCount === 0 && self.getCurrentPosition != null)
        jQuery("#nhs_TitleLocation").hide();
    else
        jQuery("#nhs_TitleLocation").show();
};

NHS.Mobile.CommResults.prototype.GetPricePadding = function(price) {
    var self = this;

    if (price < 100000)
        return 0;

    if (price > 999999)
        return self.parameters.FacetsOptions.PriceSliderValues.length;

    var index = parseInt(self.parameters.FacetsOptions.PriceSliderValues.indexOf(price));
    if (index !== -1)
        return index;

    var priceLow, priceHigh;
    for (var i = 1; i < self.parameters.FacetsOptions.PriceSliderValues.length; i++) {
        priceLow = self.parameters.FacetsOptions.PriceSliderValues[i - 1];
        priceHigh = self.parameters.FacetsOptions.PriceSliderValues[i];
        if (price >= priceLow && price <= priceHigh)
            return i;
    }

    return 0;
};

NHS.Mobile.CommResults.prototype.setPriceSliderValues = function() {
    //Start Price Padding
    var self = this;
    var priceLow = self.parameters.Translation.NoMin;
    var hidePrice = self.parameters.Translation.NoMax;

    if (self.searchParameters.PriceLow > 0) {
        priceLow = self.searchParameters.PriceLow;
        jQuery('#PriceLow').html(priceLow).keyup();
    } else {
        jQuery('#PriceLow').html(priceLow);
    }

    if (self.searchParameters.PriceHigh > 0) {
        hidePrice = self.searchParameters.PriceHigh;
        jQuery('#PriceHigh').html(hidePrice).keyup();
    } else {
        jQuery('#PriceHigh').html(hidePrice);
    }

    var indexPriceLow = self.GetPricePadding(self.searchParameters.PriceLow);
    var indexPriceHide = self.GetPricePadding(self.searchParameters.PriceHigh);
    if (self.searchParameters.PriceHigh === 0)
        indexPriceHide = self.parameters.FacetsOptions.PriceSliderValues.length - 1;

    jQuery("#nhs_SliderPrice").slider("option", "values", [indexPriceLow, indexPriceHide]);
};

NHS.Mobile.CommResults.prototype.UpdateDropDowns = function(id, optios, defaultOption) {
    var dropDown = jQuery("#" + id);
    dropDown.html("<option value=\"0\" selected=\"selected\">" + defaultOption + "</option>");
    for (var i = 0; i < optios.length; i++) {
        dropDown.append('<option value="' + optios[i].Key + '">' + optios[i].Value + '</option>');
    }
};

NHS.Mobile.CommResults.prototype.GetLocation = function() {
    var self = this;
    var url = self.parameters.GetLocationUrl + "?" + jQuery.ToString(self.search.Location);
    jQuery.ajax({
        url: url,
        type: "GET",
        cache: false,
        success: function (results) {
            if (self.searchParameters.MarketId === results.MarketId) {
                self.searchParameters.City = results.City;
                self.searchParameters.PostalCode = results.PostalCode;
                self.searchParameters.County = results.County;
                self.searchParameters.CommName = results.CommName;
                self.searchParameters.WebApiSearchType = results.WebApiSearchType;
                self.searchParameters.OriginLat = results.OriginLat;
                self.searchParameters.OriginLng = results.OriginLng;
                self.searchParameters.MarketName = results.MarketName;
                self.searchParameters.Radius = results.Radius;
                self.searchParameters.State = results.State;
                self.searchParameters.MinLat = self.searchParameters.MinLng = self.searchParameters.MaxLat = self.searchParameters.MaxLng = 0;
                self.searchParameters.Zoom = 8;
            } else {
                self.searchParameters = jQuery.extend(true, {}, results);
            }
            self.UpdateCounts(function() { self.GetBrands(); });
        }
    });
};

NHS.Mobile.CommResults.prototype.UpdateCounts = function (action) {

    jQuery("#nhs_FacetCounts > span").html("");
    jQuery("#nhs_FacetCounts > span").addClass("nhs_CountLoader");

    // Highlight Apply Button
    jQuery("#nsh_ApplyFilters").addClass("nhs_Apply");

    var self = this;

    self.updateFacets = true;
    var url = self.parameters.getCountsUrl;
    
    if (self._ajaxCall)
        self._ajaxCall.abort();

    this._ajaxCall = jQuery.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(self.searchParameters),
        contentType: 'application/json',
        cache: false,
        success: function (results) {
            var commCount = results.facetsCounts.CommCount;
            var homeCount = results.facetsCounts.HomeCount;
            var blCount = results.facetsCounts.BlCount;
            homeCount += blCount;

            var html = commCount === 0 && homeCount === 0 ? self.parameters.Translation.NoResultsFound
            : (commCount + ' ' + (commCount > 1 ? self.parameters.Translation.Communities : self.parameters.Translation.Community)
              + ' <span>|</span> ' + homeCount + ' ' + self.parameters.Translation.Home + (homeCount > 1 ? 's' : ''));
            
            jQuery("#nhs_FacetCounts > span").removeClass("nhs_CountLoader").html(html);
            self.ResetFacetsToDefaultBase();

            if (jQuery.isFunction(action)) {
                action();
            }
        }
    });
};

NHS.Mobile.CommResults.prototype.RedirectSearch = function() {
    var self = this;

    jQuery.ajax({
        type: "POST",
        url: self.parameters.changeRadiusAndSearchTypeUrl,
        data: self.searchParameters,
        async: false
    });

    var url = self.parameters.Search;
    if (self.search.Location != null) {
        url += "?" + jQuery.ToString(self.search.Location);
    } else
        url += "?searchText=" + jQuery("#FilterLocation").val() + "&Location=null";
    window.location = url;
};

NHS.Mobile.CommResults.prototype.ResetFacets = function(resetLocation) {
    var self = this;

    self.getCurrentPosition = null;
    if (self.marker != null) {
        self.marker.setMap(null);
    }


    //reset price
    self.searchParameters.PriceHigh = 0;
    self.searchParameters.PriceLow = 0;
    self.setPriceSliderValues();

    //Beds & Bath       
    self.searchParameters.Bedrooms = 0;
    self.searchParameters.Bathrooms = 0;

    //school             
    self.searchParameters.SchoolDistrictIds = "";

    self.searchParameters.SingleFamily = false;
    self.searchParameters.MultiFamily = false;
    self.searchParameters.Qmi = false;
    self.searchParameters.HotDeals = false;
    self.searchParameters.Garages = 0.0;
    self.searchParameters.LivingAreas = 0;
    self.searchParameters.Stories = 0;

    self.searchParameters.MasterBedLocation = 0;
    jQuery('input[name=MasterBed][value=0]').prop("checked", true);

    self.searchParameters.CommunityStatus = '';
    self.searchParameters.CommId = 0;
    self.searchParameters.CommName = '';
    self.searchParameters.BrandId = 0;

    //Start Amenities
    self.searchParameters.Pool = false;
    self.searchParameters.Views = false;
    self.searchParameters.Parks = false;
    self.searchParameters.Green = false;
    self.searchParameters.Gated = false;
    self.searchParameters.GolfCourse = false;
    self.searchParameters.Waterfront = false;
    self.searchParameters.NatureAreas = false;
    self.searchParameters.Sports = false;
    self.searchParameters.Adult = false;

    if (resetLocation) {
        self.searchParameters.County = '';
        self.searchParameters.City = '';
        self.searchParameters.PostalCode = '';
        self.searchParameters.Radius = 0;
        self.searchParameters.WebApiSearchType = 'Exact';
    }
    self.searchWithinMap = false;
    self.searchParameters.MinLat = self.searchParameters.MinLng = self.searchParameters.MaxLat = self.searchParameters.MaxLng = 0;
    self.searchParameters.GetRadius = true;
    self.searchParameters.GetLocationCoordinates = true;
    self.updateResults = true;
    self.updateMap = self.searchParameters.IsMapVisible;
    if (self.isCurrentPosition) {
        self.searchParameters.GetRadius = false;
        self.searchParameters.GetLocationCoordinates = false;
        self.GetCurrentLocationAction(false);
    } else {
        self.BindResults(false, function() {
            if (self.googleApi.map != null) {
                var position = new google.maps.LatLng(self.searchParameters.OriginLat, self.searchParameters.OriginLng);
                self.googleApi.map.setCenter(position);
            }
            self.updateMap = true;
            self.PlotMap();
        });
    }
};
