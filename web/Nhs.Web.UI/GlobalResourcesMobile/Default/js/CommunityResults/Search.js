﻿NHS.Mobile.Search = function (parameters) {
    this.parameters = parameters;
};

NHS.Mobile.Search.prototype =
{
    initialize: function () {
        var self = this;

        if (self.parameters.submitName) {
            jQuery("#" + self.parameters.submitName).click(function (event) {
                self.executeSubmit();
            });
        }

        if (self.parameters.secondarySubmitName) {
            jQuery("#" + self.parameters.secondarySubmitName).click(function (event) {
                self.executeSubmit();
            });
        }

        self.setupSearchBox();
    },


    executeSubmit: function () {
        var self = this;

        jQuery.CancelEventAndStopPropagation(event);
        if (self.parameters.errorId != null && self.parameters.errorId.length > 0) {
            jQuery('#' + self.parameters.errorId).hide();
        }
        var value = jQuery('#' + self.parameters.textBoxName).val();

        if (value.trim().toLowerCase() === self.parameters.Translation.CurrentLocation) {
            window.location = self.parameters.CurrentLocationSearch;
            return;
        }

        if (self.hasValue()) {
            self.submitRequest(self.Location, value);
        } else {
            if (self.parameters.errorId != null && self.parameters.errorId.length > 0) {
                jQuery('#' + self.parameters.errorId).show();
            } else
                alert(self.parameters.Translation.MGS_HOMESEARCH_DEFAULT_ERROR_MSN);
        }
    },

    submitRequest: function (location, value) {
        var self = this;
        var url = self.parameters.Search;
        if (location != null) {
            url += "?" + jQuery.ToString(location);
        } else
            url += "?searchText=" + value + "&Location=null";
        window.location = url;
    },

    hasValue: function () {
        var self = this;
        var text = String(jQuery('#' + self.parameters.textBoxName).val()).trim();

        var hasValue = true;

        if (typeof text == "undefined"
            || text === "") {
            hasValue = false;
        } else if (text.indexOf(',') === -1 && !isNaN(text) && new RegExp(/(?:^|[^\d])(\d{5})(?:$|[^\d])/mg).test(text) === false) {
            hasValue = false;
        } else if ((text.split(',').length > 1 && (text.split(',')[0].trim() === "" || text.split(',')[1].trim() === "" || text.split(',')[1].trim().length < 2))) {
            hasValue = false;
        } else if ((text.indexOf(',') === -1 && new RegExp("[a-zA-Z0-9 ]+$").test(text) === false)) {
            hasValue = false;
        }

        return hasValue;
    },

    setupSearchBox: function () {
        var self = this;

        // Autocomplete
        var searchTextBox = jQuery('#' + self.parameters.textBoxName);

        // Check search text box exists
        if (searchTextBox !== null && searchTextBox.length > 0) {
            jQuery(document).on("click", ".nhs_ResetLocationBox, .nhs_ResetSearchBar", function () {
                var trigger = this;
                setTimeout(function () { jQuery(trigger).prev("input").focus() }, 100);
            });

            searchTextBox.keypress(function (event) {
                jQuery(".field-validation-error").hide();
                self.Location = null;
                if (jQuery.isFunction(self.LocationChange)) {
                    self.LocationChange(event);
                }
            });

            searchTextBox.focus(function () {
                var parent = searchTextBox.parent();
                jQuery(parent).bind("DOMNodeInserted", function (e) {
                    var link = jQuery(e.target).find("a");
                    if (link !== undefined && link !== null && link.length > 0 && link[0].innerText.indexOf(self.parameters.Translation.CurrentLocation) >= 0) {
                        link.html("<span class='nhs_LocateAutoComplete'><span class='ir btn btn_Locate'>" + self.parameters.Translation.LocateMe + "</span> <strong>" + self.parameters.Translation.CurrentLocation + "</strong></span>");
                    }
                });
            });

            searchTextBox.autocomplete({
                source: self.parameters.typeAheadUrl,
                select: function (event, ui) {
                    event.preventDefault();
                    if (ui.item.label.indexOf(self.parameters.Translation.CurrentLocation) >= 0) {
                        searchTextBox.val(self.parameters.Translation.CurrentLocation);
                    } else {
                        self.Location = ui.item;
                        searchTextBox.val(ui.item.label);
                    }
                    if (jQuery.isFunction(self.AutoCompleteSelect)) {
                        self.AutoCompleteSelect(ui.item);
                    }
                },
                focus: function (event, ui) {
                    event.preventDefault();
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                var l = item;
                item.label = l.Name + (l.Type >= 5 ? " in " + l.MarketName + ", " + l.State : (l.Type != 4 ? ", " + l.State : "") + (l.Type == 1 ? " Area" : ""));
                var label = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + jQuery.ui.autocomplete.escapeRegex(this.term.trim()) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong id='ui-id'>$1</strong>");
                if (label.indexOf(" in ") != -1)
                    label = label.substring(0, label.indexOf(" in ")) + "<span id='ui-id'> in " + label.substring(label.indexOf(" in ") + 4) + "</span>";

                return jQuery("<li></li>").data("item.autocomplete", item).append("<a>" + label + "</a>").appendTo(ul);
            };
            searchTextBox.autocomplete().data("ui-autocomplete").menu.element.detach().appendTo(searchTextBox.parent());
        }
    }
};
