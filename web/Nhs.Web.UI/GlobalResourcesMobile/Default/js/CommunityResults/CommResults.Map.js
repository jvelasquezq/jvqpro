﻿NHS.Mobile.CommResults.prototype.ResizeMap = function () {
    var size = jQuery.ScreenSize(768);
    var y = size.y;
    var x = size.x;

    var y2 = 0; // jQuery(".nhs_ResRefineBar").outerHeight();
    var y3 = jQuery("#nhs_Header").outerHeight();
    var y4 = jQuery("#nhs_ResContextTitle").outerHeight();
    var y5 = jQuery("#nhs_TopX11").outerHeight();
    y = y - y2 - y3 - y4 + y5;
    jQuery("#nhs_Map").height(y).width(x);
};

NHS.Mobile.CommResults.prototype.initMap = function () {
    var parentSelf = this;
    parentSelf.lastMarker = null;

    jQuery("#nhs_getLocation").click(function (event) {
        jQuery.CancelEventAndStopPropagation(event);
        if (parentSelf.isCurrentPosition) {
            if (parentSelf.marker != null) {
                parentSelf.marker.setMap(null);
            }
            parentSelf.isCurrentPosition = false;
            parentSelf.getCurrentPosition = null;
            parentSelf.searchWithinMap = true;
            parentSelf.updateResults = true;
            parentSelf.UpdateResults(false);
        } else
            parentSelf.GetCurrentLocationAction(false);
    });

    //var supportsOrientationChange = "onorientationchange" in window, orientationEvent = supportsOrientationChange ? "orientationchange" : "resize";
    window.addEventListener("resize", function() { parentSelf.ResizeMap(); }, false);
    parentSelf.ResizeMap();
    var mapOptions = this.parameters.OptionsForMap;
    parentSelf.googleApi = new NHS.Scripts.GoogleMapApi(mapOptions);
    parentSelf.updateMap = true;

    parentSelf.wathsNearby = new NHS.Mobile.WhatsNearby(parentSelf, jQuery("#nhs_ResMap"), this.parameters);
    parentSelf.wathsNearby.init();

    this.googleApi.options.Events.ClickCallBack = function () {
        if (parentSelf.lastMarker != null) {
            parentSelf.lastMarker.setIcon(parentSelf.lastMarker.defaultIcon);
        }
        jQuery("#nhs_CommunityCards").html("");
        jQuery(".nhs_Flyout").removeClass("expanded").hide();
    };

    this.googleApi.options.Events.IdleCallBack = function () {
        if (parentSelf.lastMarker != null) {
            parentSelf.lastMarker.setIcon(parentSelf.lastMarker.defaultIcon);
        }
        jQuery("#nhs_CommunityCards").html("");
        jQuery(".nhs_Flyout").hide();
    };

    this.googleApi.getMarkerPointAsyn = function() {
        var self = this;
        var optionsMpa = self.options.MarkerPointAsynOptions;
        google.maps.event.addListener(self.map, optionsMpa.Listener, function () {
            parentSelf.searchParameters.CommunityId = 0;
            parentSelf.searchParameters.BuilderId = 0;
            if (parentSelf.marker != null) {
                parentSelf.marker.setMap(null);
            }
            parentSelf.searchWithinMap = true;
            parentSelf.isCurrentPosition = false;
            parentSelf.getCurrentPosition = null;
            parentSelf.updateResults = true;
            parentSelf.updateMap = true;
            parentSelf.UpdateResults(false, function() {
                parentSelf.GetBrands();
            });
        });
    };


    this.googleApi.options.Events.OnMapCreate = function() {
        parentSelf.PlotMap();
    };

    this.googleApi.options.Events.ZoomChangesCallBack = function() {
        if (parentSelf.searchWithinMap) {
            parentSelf.updateResults = true;
            parentSelf.UpdateResults(false);
        }
    };

    this.googleApi.options.Events.MarkerClickCallBack = function(info, infowindow, infowindowTooltip, marker) {
        jQuery(".nhs_Flyout").hide();
        marker.setCursor("wait");
        if (parentSelf.lastMarker != null) {
            parentSelf.lastMarker.setIcon(parentSelf.lastMarker.defaultIcon);
        }
        marker.setIcon(marker.selectedIcon);
        parentSelf.lastMarker = marker;

        var commIds = [];
        var basicIds = [];
        for (var i = 0; i < info.MarketPoints.length; i++) {
            if (info.MarketPoints[i].IsBl)
                basicIds.push(info.MarketPoints[i]['Id']);
            else
                commIds.push(info.MarketPoints[i]['Id'] + "|" + info.MarketPoints[i]['NumHomes']);
        }

        jQuery.ajax({
            url: parentSelf.parameters.getCommunityMapCards,
            type: "GET",
            cache: false,
            data: {
                commIds: commIds.join(),
                basicIds: basicIds.join(),
                forceHeader: true
            },
            success: function(html) {
                if (html) {
                    jQuery("#nhs_CommunityCards").html(html);
                    marker.setCursor("pointer");
                    var m1 = jQuery("#nhs_ResMap").css("margin-top");
                    jQuery("#nhs_MapCards").css("top", m1);
                }
            }
        });
    };

    this.googleApi.processResult = function (results) {
        var self = this;
        if (results.CommCount === 0)
            jQuery("#nhs_FacetCounts > span").html(parentSelf.parameters.Translation.NoResultsFound);
        else {
            var html = results.CommCount + ' ' + (results.CommCount > 1 ? parentSelf.parameters.Translation.Communities : parentSelf.parameters.Translation.Community)
              + ' <span>|</span> ' + results.HomeCount + ' ' + parentSelf.parameters.Translation.Home + (results.HomeCount > 1 ? 's' : '');

            jQuery("#nhs_FacetCounts > span").html(html);
        }


        var prOptions = self.options.ProcessResultOptions;
        results = results.Results;
        for (var i = 0; i < results.length; i++) {
            var icon = self.getIconMarkerPoint(results, results[i]);

            for (var j = 0; j < results[i].MarketPoints.length; j++) {
                self.createMarkerPoint(results[i][prOptions.Latitude], results[i][prOptions.Longitude], null, null, icon, results[i]);
            }
        }

        self.hideLoading();
    };

    this.googleApi.options.Events.OnMarkersCreate = function(info, infowindow, marker) {
        var count = info.MarketPoints.length;
        marker.defaultIcon = marker.getIcon();
        if (info.IsBasic)
            marker.selectedIcon = count > 1 ? parentSelf.parameters.iconSelectedMultiBasic : parentSelf.parameters.iconSelectedBasic;
        else
            marker.selectedIcon = count > 1 ? parentSelf.parameters.iconSelectedMulti : parentSelf.parameters.iconSelected;
    };

    this.googleApi.getIconMarkerPoint = function(results, result) {

        var count = result.MarketPoints.length;
        if (result.IsBl)
            return parentSelf.parameters.iconBasic;

        if (result.IsBasic) {
            if (count > 1)
                return parentSelf.parameters.iconMultiBasic;
            return parentSelf.parameters.iconBasic;
        }

        if (count > 1)
            return parentSelf.parameters.iconMulti;
        return parentSelf.parameters.icon;
    };


    if (this.searchParameters.IsMapVisible) {
        jQuery("#nhs_Footer").hide();
        this.isMapCreate = true;
        this.updateMap = true;
        this.googleApi.createMap();
    } else
        jQuery("#nhs_Footer").show();
};

NHS.Mobile.CommResults.prototype.GetCurrentLocationAction = function (getCounts) {
    var self = this;
    self.marker = null;
    var optionsLoaction = {
        maximumAge: 3000,
        //timeout: 5000,
        enableHighAccuracy: true
    };

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            //navigator.geolocation.watchPosition(function(position) {

            jQuery(".btn_Locate").addClass("nhs_ActiveLocation");

            if (self.marker != null) {
                self.marker.setMap(null);
            }

            self.searchWithinMap = false;

            self.searchParameters.MinLat = self.searchParameters.MinLng = self.searchParameters.MaxLat = self.searchParameters.MaxLng = 0;
            self.searchParameters.OriginLat = position.coords.latitude;
            self.searchParameters.OriginLng = position.coords.longitude;
            self.searchParameters.CustomRadiusSelected = false;
            self.searchParameters.Radius = 3;
            self.searchParameters.WebApiSearchType = 'Radius';
            self.searchParameters.MarketId = 0;
            self.searchParameters.PostalCode = '';
            self.searchParameters.City = '';
            self.searchParameters.County = '';
            self.searchParameters.CommName = '';
            position = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            self.getCurrentPosition = position;

            jQuery("#FilterLocation").val(self.parameters.Translation.CurrentLocation);

            if (getCounts) {
                self.UpdateCounts();
            } else {
                self.updateMap = true;
                self.isCurrentPosition = true;
                self.updateResults = true;
                self.UpdateResults(false, function() {
                    if (self.googleApi.map != null) {
                        var options = {
                            icon: self.parameters.iconCurrentPosition,
                            position: position,
                            optimized: true,
                            zIndex: 99,
                            animation: google.maps.Animation.BOUNCE
                        };
                        self.googleApi.map.setCenter(position);
                        self.marker = new google.maps.Marker(options);
                        self.marker.setMap(self.googleApi.map);
                        self.googleApi.bounds.extend(position);
                        self.googleApi.AutoFit();
                        setTimeout(function () { self.marker.setAnimation(null); }, 5000);
                    }

                    self.GetBrands();
                });
            }

        }, function() {}, optionsLoaction);
    }
};

NHS.Mobile.CommResults.prototype.PlotMap = function () {

    var self = this;
    self.ChangeState();

    if ((self.searchWithinMap || self.updateMap) && self.searchParameters.IsMapVisible && !self.LocationChange) {

        var mapObject = self.googleApi;
        self.wathsNearby.POICreate();
        this.updateMap = false;
        var optionsMpa = mapObject.options.MarkerPointAsynOptions;
        mapObject.showLoading();
        if (self.searchWithinMap) {
            self.searchParameters.WebApiSearchType = 'Map';
            var data = mapObject.getBoundsFromMap();
            self.searchParameters.MinLat = data.minLat;
            self.searchParameters.MinLng = data.minLng;
            self.searchParameters.MaxLat = data.maxLat;
            self.searchParameters.MaxLng = data.maxLng;
            var center = mapObject.map.center;
            var zoom = mapObject.map.getZoom();
            self.searchParameters.Zoom = zoom;
            self.searchParameters.OriginLat = center.lat();
            self.searchParameters.OriginLng = center.lng();
        } else
            self.searchParameters.WebApiSearchType = self.searchParameters.Radius > 0 ? 'Radius' : 'Exact';

        var url = optionsMpa.UrlServer;

        jQuery.ajax({
            url: url,
            type: "POST",
            data: JSON.stringify(self.searchParameters),
            contentType: 'application/json',
            dataType: "json",
            success: function(results) {
                mapObject.map.clearOverlays();
                var count = results.Results.length;
                mapObject.processResult(results);
                if (!self.searchWithinMap && count > 0) {
                    mapObject.AutoFit();
                }
                self.ShowRadius(self.searchParameters.Radius);
            }});
    }
};

NHS.Mobile.CommResults.prototype.ShowRadius = function(radius) {
    var self = this;

    if (self.cityCircle)
        self.cityCircle.setMap(null);

    if (!self.searchWithinMap) {
        //var center = self.googleApi.map.center;
        var center = new google.maps.LatLng(self.searchParameters.OriginLat, self.searchParameters.OriginLng);


        var populationOptions = {
            strokeColor: '#0091da',
            strokeOpacity: 0,
            strokeWeight: 0,
            fillColor: '#0091da',
            fillOpacity: 0.35,
            map: self.googleApi.map,
            center: center,
            radius: radius * 1609.34
        };

        self.cityCircle = new google.maps.Circle(populationOptions);
    }
};
