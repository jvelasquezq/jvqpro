﻿

NHS.Mobile.CommResults.prototype.SortBy = function () {
    var self = this;
    jQuery(".nhs_SortBy").click(function (event) {
        jQuery.CancelEventAndStopPropagation(event);
        var control = jQuery(this);
		jQuery(".nhs_SortBy").removeClass("selected");
		control.addClass("selected");
        var value = control.attr("data-sortvalue");
        var order = control.attr("data-sortorder");
        var pages = self.searchParameters.PageNumber;
        self.searchParameters.PageNumber = 1;
        self.searchParameters.SortBy = value;
        self.searchParameters.SortOrder = order;
        self.searchParameters.PageSize = pages * 10;
        self.BindResults(false, function() {
            self.searchParameters.PageNumber = pages;
            self.defaultSearchParameters.PageNumber = pages;
            self.searchParameters.PageSize = 10;
            self.defaultSearchParameters.PageSize = 10;
            if (self.parameters.Pages === self.searchParameters.PageNumber)
                jQuery("#nhs_MoreResults").hide();
        });
        jQuery("#nhs_Header").css("position", "fixed");
        jQuery(this).parent().parent().parent().parent().removeClass("expanded");
    });
};
