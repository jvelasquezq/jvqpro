﻿NHS.Mobile.WhatsNearby = function (parent, container, parameters) {
    this.parent = parent;
    this.container = container;
    this.parameters = parameters;
};

NHS.Mobile.WhatsNearby.prototype = {
    init: function () {
        var self = this;
        self.school = {
            Array: [],
            Show: false,
            Types: ["school", "university"],
            Name: "school"
        };

        self.shopping = {
            Array: [],
            Show: false,
            Types: ["shoe_store", "art_gallery", "convenience_store", "shopping_mall", "bicycle_store", "pet_store", "department_store", "book_store", "jewelry_store", "electronics_store", "home_goods_store", "furniture_store", "clothing_store", "hardware_store"],
            Name: "shopping"
        };

        self.entertainment = {
            Array: [],
            Show: false,
            Types: ["casino", "amusement_park", "aquarium", "museum", "park", "movie_rental", "bowling_alley", "stadium", "mosque", "laundry", "night_club", "library", "movie_theater", "zoo"],
            Name: "entertainment"
        };

        self.container.find(".nhs_MapChk1,.nhs_MapChk2,.nhs_MapChk3").click(function () {
            self.POICreate();
        });

        self.container.find(".nhs_MapChk4").click(function () {
            self.parent.googleApi.clearOverlays();
            if (jQuery(this).is(":checked")) {
                var bounds = self.parent.googleApi.getBoundsFromMap();
                jQuery.ajax({
                    url: self.parameters.getNearbyCommsUrl,
                    type: "GET",
                    cache: false,
                    data: {
                        minLat: bounds.minLat,
                        minLng: bounds.minLng,
                        maxLat: bounds.maxLat,
                        maxLng: bounds.maxLng,
                        lat: self.parent.CurrentPoint[0].Latitude,
                        lng: self.parent.CurrentPoint[0].Longitude
                    },
                    success: function (points) {
                        self.parent.googleApi.processResult(points);
                    }
                });
            } else {
                self.parent.googleApi.processResult(self.parent.CurrentPoint);
                jQuery("#nhs_CommunityCardsMapPopup").html("");
            }
        });

        self.container.find(".btn_Nearby").click(function (event) {
            jQuery.CancelEventAndStopPropagation(event);
            var control = self.container.find(".nhs_Flyout");
            if (control.hasClass("expanded")) {
                control.removeClass("expanded").hide();
            } else {
                control.addClass("expanded").show();
            }
        });

        self.parent.googleApi.GetPOIIcon = function (place, opt) {
            var selfG = this;
            var iconUrl;
            switch (opt.Name) {
                case 'school':
                    iconUrl = selfG.options.pOIIcons.school;
                    break;
                case 'shopping':
                    iconUrl = selfG.options.pOIIcons.shopping;
                    break;
                case 'entertainment':
                    iconUrl = selfG.options.pOIIcons.entertainment;
                    break;
                default:
                    iconUrl = selfG.options.pOIIcons._default;
            }
            return iconUrl;
        };
    },

    POICreate: function () {
        var self = this;
        self.school.Show = self.container.find(".nhs_MapChk1").is(":checked");
        self.parent.googleApi.addShowPointOfInt(self.school);

        self.shopping.Show = self.container.find(".nhs_MapChk2").is(":checked");
        self.parent.googleApi.addShowPointOfInt(self.shopping);

        self.entertainment.Show = self.container.find(".nhs_MapChk3").is(":checked");
        self.parent.googleApi.addShowPointOfInt(self.entertainment);
    }
};