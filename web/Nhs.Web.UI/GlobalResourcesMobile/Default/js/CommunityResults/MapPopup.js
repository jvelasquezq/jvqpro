﻿NHS.Mobile.MapPopup = function (parameters) {
    this.parameters = parameters;
};

NHS.Mobile.MapPopup.prototype = {
    init: function () {
        var self = this;
        self.lastMarker = null;
        this.ResizeMap();

        var mapOptions = this.parameters.OptionsForMap;
        self.googleApi = new NHS.Scripts.GoogleMapApi(mapOptions);

        self.wathsNearby = new NHS.Mobile.WhatsNearby(self, jQuery("#nhs_MapModal"), this.parameters);
        self.wathsNearby.init();

        self.googleApi.options.Events.OnMarkersCreate = function (info, infowindow, marker) {
            var count = info.MarketPoints.length;
            marker.defaultIcon = marker.getIcon();
            if (info.MarketPoints[0].IsBasic)
                if (info.CurrentPosition) {
                    marker.selectedIcon = count > 1 ? self.parameters.iconSelectedMultiBasic : self.parameters.iconSelectedBasic;
                }
                else {
                    marker.selectedIcon = count > 1 ? self.parameters.iconMultiBasic : self.parameters.iconBasic;
                }
            else {
                if (info.CurrentPosition) {
                    marker.selectedIcon = count > 1 ? self.parameters.iconSelectedMulti : self.parameters.icon;
                } else {
                    marker.selectedIcon = count > 1 ? self.parameters.iconNearMulti : self.parameters.icon;
                }
            }
        };

        self.googleApi.getIconMarkerPoint = function (results, result) {
            if (result.CurrentPosition) {
                if (result.MarketPoints[0].IsBasic) {
                    return self.parameters.iconSelectedBasic;
                } else {
                    if (result.Count === 1) {
                        return self.parameters.icon;
                    }
                    else {
                        return self.parameters.iconSelectedMulti;
                    }
                }
            }
            else
                if (result.MarketPoints[0].IsBasic) {
                    return self.parameters.iconBasic;
                } else {
                    if (result.Count === 1) {
                        return self.parameters.iconNear;
                    }
                }
            return self.parameters.iconNearMulti;
        };

        self.googleApi.options.Events.MarkerClickCallBack = function (info, infowindow, infowindowTooltip, marker) {
            jQuery(".nhs_Flyout").hide();
            marker.setCursor("wait");

            if (self.lastMarker != null) {
                self.lastMarker.setIcon(self.lastMarker.defaultIcon);
            }
            marker.setIcon(marker.selectedIcon);
            self.lastMarker = marker;

            var commIds = [];
            var basicIds = [];
            for (var i = 0; i < info.MarketPoints.length; i++) {
                if (info.MarketPoints[i].IsBl) {
                    basicIds.push(info.MarketPoints[i]["CommunityId"]);
                } else {
                    commIds.push(info.MarketPoints[i]["CommunityId"] + "|" + info.MarketPoints[i]['Count']);
                }
            }

            jQuery.ajax({
                url: self.parameters.getCommunityMapCards,
                type: "GET",
                cache: false,
                data: {
                    commIds: commIds.join(),
                    basicIds: basicIds.join(),
                    forceHeader: true
                },
                success: function (html) {
                    if (html) {
                        jQuery("#nhs_CommunityCardsMapPopup").html(html);
                        marker.setCursor("pointer");
                    }
                }
            });
        };

        self.googleApi.options.Events.ClickCallBack = function () {
            if (self.lastMarker != null) {
                self.lastMarker.setIcon(self.lastMarker.defaultIcon);
            }
            jQuery(".nhs_ResMapNearby .nhs_Flyout").removeClass("expanded").hide();
            jQuery("#nhs_CommunityCardsMapPopup").html("");
        };

        self.googleApi.options.Events.IdleCallBack = function () {
            if (self.lastMarker != null) {
                self.lastMarker.setIcon(self.lastMarker.defaultIcon);
            }
            jQuery(".nhs_ResMapNearby .nhs_Flyout").hide();
            jQuery("#nhs_CommunityCardsMapPopup").html("");
            self.wathsNearby.POICreate();
        };

        self.googleApi.createMap();

        var point = [
            {
                Latitude: self.parameters.latitude,
                Longitude: self.parameters.longitude,
                Name: self.parameters.communityName,
                PriceRange: self.parameters.priceRange,
                CurrentPosition: true,
                MarketPoints: [{ CommunityId: self.parameters.communityId, Count: self.parameters.numHom, IsBasic: self.parameters.isBasic === "True" ? 1 : 0 }],
                Count: 1
            }
        ];
        self.googleApi.processResult(point);
        self.CurrentPoint = point;
        jQuery("#nhs_getLocationPopUpMap").click(function (event) {
            jQuery.CancelEventAndStopPropagation(event);
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {

                    if (self.marker != null) {
                        self.marker.setMap(null);
                    }

                    position = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    var options = {
                        icon: self.parameters.iconCurrentPosition,
                        position: position,
                        optimized: true,
                        zIndex: 99,
                        animation: google.maps.Animation.BOUNCE

                    };
                    self.marker = new google.maps.Marker(options);
                    self.marker.setMap(self.googleApi.map);
                    setTimeout(function () { self.marker.setAnimation(null); }, 5000);

                    self.googleApi.bounds = new google.maps.LatLngBounds();

                    var positionLocation = new google.maps.LatLng(self.parameters.latitude, self.parameters.longitude);
                    self.googleApi.bounds.extend(positionLocation);
                    self.googleApi.bounds.extend(position);
                    self.googleApi.AutoFit();
                });
            }
        });
    },
    ResizeMap: function () {
        var size = jQuery.ScreenSize();
        var y = size.y;
        var x = size.x;
        var h = jQuery(".nhs_MapActionBar").outerHeight();

        y = y - 44 - h;
        jQuery("#nhs_MapPopup").height(y).width(x);
    }
};