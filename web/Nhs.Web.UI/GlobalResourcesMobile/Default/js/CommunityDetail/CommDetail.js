﻿
NHS.Mobile.CommDetail = function (parameters) {
    this.parameters = parameters;    
    this.logger = new NHS.Scripts.Globals.EventLogger({ siteRoot: parameters.logInfo.siteRoot, partnerId: parameters.logInfo.partnerId, partnerName: parameters.logInfo.partnerName, marketId: parameters.logInfo.marketId, tdvTable: parameters.logInfo.tdvTable });
    this.player = new NHS.Scripts.WebPlayers({
        videoWidth: (parameters.viewerParams.videoWidth || 0),
        videoHeight: (parameters.viewerParams.videoHeight || 0),
        youtubeControlId: 'nhs_VideP',
        vimeoControlId: 'nhs_VideP',
        brightcoveControlId: 'nhs_VideP',
        brightcoveToken: parameters.viewerParams.brightcoveToken
    });
};

NHS.Mobile.CommDetail.prototype =
{
    init: function () {
        var self = this;

        jQuery(".nhs_CommMoreLink, .nhs_CommMoreClose").on('click', function (event) {
            jQuery.CancelEventAndStopPropagation(event);
            jQuery(".nhs_CommMoreLink > span").toggleClass("expanded");
            if (jQuery(".nhs_CommPanel").hasClass("expanded")) {
                jQuery("#nhs_CommunityGalleryTitle").html(self.parameters.ViewCommunityInfoText);

                if (jQuery(this).hasClass("nhs_CommMoreClose")) {
                    jQuery.ScrollToWithCallBak(100, function () {
                        jQuery(".nhs_CommPanel").toggleClass("expanded").slideUp("slow");
                    });
                } else {
                    jQuery.ScrollTo(100);
                    jQuery(".nhs_CommPanel").toggleClass("expanded").slideUp();
                }
            } else {
                jQuery("#nhs_CommunityGalleryTitle").html(self.parameters.HideCommunityInfoText);
                jQuery(".nhs_CommPanel").toggleClass("expanded").slideDown("slow");
                jQuery('#nhs_GalleryList .bxslider').bxSlider({
                    responsive: false,
                    infiniteLoop: false,
                    slideWidth: 126,
                    slideMargin: 5,
                    minSlides: 1,
                    maxSlides: 5,
                    pager: false,
                    controls: false
                });
                jQuery.ScrollTo(100);
            }

        });



        if (self.parameters.expand) {
            jQuery(".nhs_CommMoreLink > span").toggleClass("expanded");
            jQuery("#nhs_CommunityGalleryTitle").html(self.parameters.HideCommunityInfoText);
            jQuery(".nhs_CommPanel").toggleClass("expanded").show();
        }

        jQuery(".nhs_HomeTabs").change(function () {
            self.BindResults(false);
        });

        jQuery.ajax({
            url: self.parameters.GetMediaPlayerObjects + "?communityId=" + self.parameters.searchParameters.CommId,
            type: "GET",
            success: function (data) {
                if (typeof data == "string" && data.length > 0) {
                    jQuery("#nhs_Gallery").show();
                    jQuery('#nhs_GalleryList').html(data);
                    if (self.parameters.expand) {
                        jQuery('#nhs_GalleryList .bxslider').bxSlider({
                            responsive: false,
                            infiniteLoop: false,
                            slideWidth: 126,
                            slideMargin: 5,
                            minSlides: 1,
                            maxSlides: 5,
                            pager: false,
                            controls: false
                        });
                    }

                    jQuery(".nhs_LinkGallery").click(function (event) {
                        jQuery.CancelEventAndStopPropagation(event);
                        self.ShowModal(this);
                    });
                }
            }
        });

        jQuery("#nhs_Saved").click(function (event) {
            var control = jQuery(this);
            jQuery.CancelEventAndStopPropagation(event);
            jQuery.ajax({
                url: self.parameters.SaveCommunity,
                type: "POST",
                success: function (result) {
                    if (result.data) {
                        control.replaceWith('<span class="nhs_Save nhs_Saved">' + self.parameters.SavedText + '</span>');
                    } else {
                        modalPopUp.Show(result.redirectUrl);
                    }
                }
            });
        });

        modalPopUp.InsertedHtml = function (link) {
            var control = jQuery('#nhs_ModalBody .bxslider');
            var isFloorplan = jQuery(link).hasClass("nhs_FloorplanLink");
            control.wrap('<div class="' + (isFloorplan ? "nhs_FloorplanList" : "nhs_GalleryList") + ' clearfix"></div>');
            jQuery('#nhs_ModalBody .bxslider').bxSlider({
                responsive: true,
                infiniteLoop: false,
                slideWidth: 234,
                slideMargin: 10,
                minSlides: 1,
                maxSlides: 5,
                //startSlide: startSlide,
                pager: false,
                controls: false
            });
            if (isFloorplan) {
                jQuery("#nhs_ModalBody").find(".nhs_FloorplanList").wrap('<div class="nhs_FloorPlan"></div>');
                jQuery("#nhs_ModalBody").find(".nhs_FloorPlan").prepend('<h2>' + self.parameters.TouchToViewFullSizeText + '</h2>');
            }
        };

        jQuery("#nhs_OpenHours").click(function (event) {
            jQuery.CancelEventAndStopPropagation(event);
            var modal = modalPopUp.CreateModal();
            var hoursHtml = jQuery("#nhs_HoursOfOperation").html();
            modal.find("#nhs_ModalBody").html(hoursHtml);
            modal.addClass("expanded");
        });

        jQuery(document).on('click', '.nhs_Row', function (event) {
            jQuery.CancelEventAndStopPropagation(event);
            if (event.type == "click") {
                var url;
                url = jQuery(this).find("a:first-of-type").attr("href");
                window.location.href = url;
            }
        });
        jQuery(document).on("click", ".nhs_ModalPopUp", function (event) {
            jQuery.CancelEventAndStopPropagation(event);
        });

        jQuery(document).on("click", "#nhs_ModalBody img.nhs_LinkImg", function (event) {
            if (event.type === "click") {
                window.open(this.src.match(/^[^\#\?]+/)[0], "_blank");
            }
        });


        jQuery("#nhs_MoreResults").click(function (event) {
            jQuery.CancelEventAndStopPropagation(event);
            self.MoreResults();
        });

    },

    MoreResults: function () {
        var self = this;
        self.parameters.searchParameters.PageNumber++;
        if (self.parameters.searchParameters.PageNumber == self.parameters.TotalPages) {
            jQuery("#nhs_MoreResults").hide();
        }
        self.BindResults(true);
    },
    BindResults: function (moreResults) {
        var self = this;
        var selectedTab = "";
        if (jQuery("#Matching").is(":checked"))
            selectedTab = "FilterCommunities";
        else if (jQuery("#QuickMove").is(":checked"))
            selectedTab = "QuickMoveIn";
        else if (jQuery("#All").is(":checked"))
            selectedTab = "AllCommunities";
        jQuery.ShowLoading();
        jQuery.ajax({
            url: self.parameters.searchAction + "?" + jQuery.ToString(self.parameters.searchParameters) + "&selectedTab=" + selectedTab + "&moreResults=" + moreResults,
            type: "GET",
            success: (function (html) {
                if (moreResults)
                    jQuery("#nhs_HomeResultsRows").append(html);
                else {
                    jQuery('#nhs_HomeResultsRows').html(html);
                    self.parameters.TotalPages = Pages;
                    self.parameters.searchParameters.Pages = 1;
                    if (Pages > 1)
                        jQuery("#nhs_MoreResults").show();
                    else
                        jQuery("#nhs_MoreResults").hide();
                }
                jQuery.HideLoading();
            }).bind(this),
            error: function () {
                jQuery.HideLoading();
            }
        }).fail(function () {
            jQuery.HideLoading();
        });
    },
    ShowVideo: function (control) {
        var self = this;

        self.player.stopVideo(true);
        control = jQuery(control);
        var type = control.data('type');


        if (type.indexOf('l-') != -1) {
            var url = control.data('url');
            var eventCode = control.data('event');
            self.logger.logWithParametersAndRedirect(event, url, eventCode, self.parameters.logInfo, true); 
        }
        else if (type == 'v-yt' || type == 'v-vm' || type == 'v-bc') {
            var videoId = control.data('id');
            var parent = control.parent();
            parent.append('<div id="nhs_VideP"></div>');
            control.hide();
            var div = control.parent()[0];
            var videoWidth = div.clientWidth;
            var videoHeight = div.clientHeight;

            self.player.parameters.videoWidth = videoWidth;
            self.player.parameters.videoHeight = videoHeight;

            if (type == 'v-yt') {
                self.player.playYouTubeVideo(videoId);
            } else if (type == 'v-vm') {
                self.player.playVimeoVideo(videoId);
            } else if (type == 'v-bc') {
                self.player.playBrightcoveVideo(videoId);
            }
        } 
    },
    ShowModal: function (link) {
        var self = this;
        var modal = modalPopUp.CreateModal();
        var control = jQuery('#nhs_GalleryList .bxslider').clone();
        var startSlide = parseInt(jQuery(link).attr("data-slide"));
        var w = jQuery('<div class="nhs_GalleryList clearfix"></div>').append(control);
        modal.find("#nhs_ModalBody").append(w);
        modal.find(".nhs_GalleryList").wrap('<div class="nhs_Gallery"></div>');
        modal.find(".nhs_Gallery").prepend('<h2>Touch to view full-size image</h2>');
        modal.find(".nhs_LinkGallery").click(function(event) {
            self.ShowVideo(this);
        });
        modal.addClass("expanded");
        control.bxSlider({
            responsive: true,
            infiniteLoop: false,
            slideWidth: 234,
            slideMargin: 10,
            minSlides: 1,
            maxSlides: 5,
            startSlide: startSlide,
            pager: false,
            controls: false
        });
    }
};
