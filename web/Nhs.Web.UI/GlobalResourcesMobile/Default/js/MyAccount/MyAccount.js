﻿NHS.Mobile.MyAccount = function (params) {
    this.params = params;
};

NHS.Mobile.MyAccount.prototype =
{
    initialize: function () {
        var self = this;

        jQuery(document).on('click', '.nhs_Row', function (event) {
            jQuery.CancelEventAndStopPropagation(event);
            if (event.type === "click") {
                var url;
                url = jQuery(this).find("a:first-of-type").attr("href");
                window.location.href = url;
            }
        });

        jQuery(document).on("click", ".nhs_CallBtn, .nhs_ViewBrochureBtn", function (event) {
            event.stopPropagation();
        });


        jQuery(".nhs_HomeResItemDelete").click(function (event) {
            jQuery.CancelEventAndStopPropagation(event);
            var control = jQuery(this);
            var parent = control.parent().parent().parent();
            parent.ShowLoading();
            var isSpec = control.data('isplan');
            var listingId = control.data('listingid');
            var data = { isSpec: isSpec, listingId: listingId };
            jQuery.ajax({
                type: "POST",
                url: self.params.DeleteFavoriteHome,
                data: data,
                success: function (result) {
                    if (result.valid) {
                        parent.remove();
                        var rows = jQuery(".nhs_HomeItem");
                        jQuery("#nhs_GroupingBarHome").html(self.params.Homes + ": " + rows.length + " " + self.params.SavedPluralFemale);
                    }
                }
            });
        });

        jQuery(".nhs_CommResItem .nhs_DeleteBtn").click(function(event) {
            jQuery.CancelEventAndStopPropagation(event);
            var control = jQuery(this);
            var parent = control.parent().parent().parent();
            parent.ShowLoading();
            var builderId = control.data('builderid');
            var communityId = control.data('communityid');
            var data = { builderId: builderId, communityId: communityId };
            jQuery.ajax({
                type: "POST",
                url: self.params.DeleteFavoriteComm,
                data: data,
                success: function(result) {
                    if (result.valid) {
                        parent.remove();
                        var rows = jQuery(".nhs_CommResItem");
                        jQuery("#nhs_GroupingBarComm").html(self.params.Communities + ": " + rows.length + " " + self.params.SavedPluralFemale);
                    }
                }
            });

        });
    }
};
