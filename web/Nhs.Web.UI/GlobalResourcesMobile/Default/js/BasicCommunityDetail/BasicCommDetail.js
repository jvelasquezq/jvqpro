﻿
NHS.Mobile.BasicCommDetail = function (parameters) {
    this.parameters = parameters;
    this.logger = new NHS.Scripts.Globals.EventLogger({ siteRoot: parameters.SiteRoot, partnerId: parameters.PartnerId, marketId: parameters.MarketId });
    this.player = new NHS.Scripts.WebPlayers({
        videoWidth: (parameters.viewerParams.videoWidth || 0),
        videoHeight: (parameters.viewerParams.videoHeight || 0),
        youtubeControlId: 'nhs_VideP',
        vimeoControlId: 'nhs_VideP',
        brightcoveControlId: 'nhs_VideP',
        brightcoveToken: parameters.viewerParams.brightcoveToken
    });
};

NHS.Mobile.BasicCommDetail.prototype =
{
    init: function () {
        var self = this;

        jQuery.ajax({
            url: self.parameters.GetMediaPlayerObjects + "?communityId=" + self.parameters.CommId,
            type: "GET",
            success: function (data) {
                if (typeof data == "string" && data.length > 0) {
                    jQuery('#nhs_GalleryList').html(data);
                    jQuery(".nhs_CommPanel").slideDown();
                    jQuery("#nhs_GalleryList .nhs_LinkGallery").click(function (event) {
                        jQuery.CancelEventAndStopPropagation(event);
                        self.ShowModal(this);
                    });

                    jQuery('#nhs_GalleryList .bxslider').bxSlider({
                        responsive: false,
                        infiniteLoop: false,
                        slideWidth: 126,
                        slideMargin: 5,
                        minSlides: 1,
                        maxSlides: 5,
                        pager: false,
                        controls: false
                    });
                }
            }
        });

        jQuery.ajax({
            url: self.parameters.GetNearbyCommunities + "?communityId=" + self.parameters.CommId + "&count=5",
            type: "GET",
            success: function (data) {
                if (typeof data == "string" && data.length > 0) {
                    jQuery('#NearByComms').html(data);
                    jQuery("#NearByComms").slideDown();
                    jQuery('#NearByComms .bxslider').bxSlider({
                        responsive: false,
                        infiniteLoop: false,
                        slideWidth: 190,
                        slideMargin: 5,
                        minSlides: 1,
                        maxSlides: 5,
                        pager: false,
                        controls: false
                    });

                    jQuery(".nhs_GalleryItem.nhs_Row").click(function (event) {
                        jQuery.CancelEventAndStopPropagation(event);
                        if (event.type == "click") {
                            var url;
                            url = jQuery(this).find("a:first-of-type").attr("href");
                            window.location.href = url;
                        }
                    });
                }
            }
        });



        modalPopUp.InsertedHtml = function (link) {
            var control = jQuery('#nhs_ModalBody .bxslider');
            control.wrap('<div class="nhs_GalleryList" clearfix"></div>');
            jQuery('#nhs_ModalBody .bxslider').bxSlider({
                responsive: false,
                infiniteLoop: false,
                slideWidth: 234,
                slideMargin: 10,
                minSlides: 1,
                maxSlides: 5,
                //startSlide: startSlide,
                pager: false,
                controls: false
            });
        };

        jQuery(document).on("click", ".nhs_ModalPopUp", function (event) {
            jQuery.CancelEventAndStopPropagation(event);
        });


    },
    ShowVideo: function (control) {
        var self = this;

        self.player.stopVideo(true);
        control = jQuery(control);
        var type = control.data('type');


        if (type.indexOf('l-') != -1) {
            var url = control.data('url');
            var eventCode = control.data('event');
            self.logger.logAndRedirect(event, url, eventCode, self.parameters.communityId, self.parameters.builderId, self.parameters.planId, self.parameters.specId, self.parameters.MarketId, true);
        }
        else if (type == 'v-yt' || type == 'v-vm' || type == 'v-bc') {
            var videoId = control.data('id');
            var parent = control.parent();
            parent.append('<div id="nhs_VideP"></div>');
            control.hide();
            var div = control.parent()[0];
            var videoWidth = div.clientWidth;
            var videoHeight = div.clientHeight;

            self.player.parameters.videoWidth = videoWidth; 
            self.player.parameters.videoHeight = videoHeight; 
            
            if (type == 'v-yt') {
                self.player.playYouTubeVideo(videoId);
            } else if (type == 'v-vm') {
                self.player.playVimeoVideo(videoId);
            } else if (type == 'v-bc') {
                self.player.playBrightcoveVideo(videoId);
            }
        }
    },
    ShowModal: function (link) {
        var self = this;
        var modal = modalPopUp.CreateModal();
        var control = jQuery('#nhs_GalleryList .bxslider').clone();
        //        control.find(".thumb").hide();
        //        control.find(".image").show();
        var startSlide = parseInt(jQuery(link).attr("data-slide"));
        var w = jQuery('<div class="nhs_GalleryList clearfix"></div>').append(control);
        modal.find("#nhs_ModalBody").append(w);
        modal.find(".nhs_LinkGallery").click(function (event) {
            jQuery.CancelEventAndStopPropagation(event);
            self.ShowVideo(this);
        });
        modal.addClass("expanded");
        control.bxSlider({
            responsive: true,
            infiniteLoop: false,
            slideWidth: 234,
            slideMargin: 10,
            minSlides: 1,
            maxSlides: 5,
            startSlide: startSlide,
            pager: false,
            controls: false
        });
    }
};
