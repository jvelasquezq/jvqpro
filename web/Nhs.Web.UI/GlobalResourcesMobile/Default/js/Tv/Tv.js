﻿NHS.Mobile.Tv = function (parameters) {
    this.parameters = parameters;
    this.searchTypeahead = new NHS.Mobile.Search(this.parameters);
};

NHS.Mobile.Tv.prototype =
{
    nhsYoutubePlayers: {},
    capitalizeFirst: function (inputName) {
        inputName = inputName.toLowerCase().trim();
        if (inputName === "washington-dc")
            return "Washington DC";
        else
            return inputName.charAt(0).toUpperCase() + inputName.slice(1);
    },

    init: function () {
        this.loadYoutubeAPI();
        this.initEvents();
        this.createNavigationMenu();
    },

    createNavigationMenu: function () {
        var self = this;
        var featureHomes = jQuery("#FeaturedHomes");
        var findCommunities = jQuery("#FindCommunities");
        var viewSegments = jQuery("#ViewSegments");
        var showSponsors = jQuery("#ShowSponsors");
        var features, findComms, segments, sponsors;

        if (featureHomes !== null && featureHomes.length > 0) {
            features = jQuery("<li><a id=\x22GoToFeatured\x22 onclick=\x22jQuery.googlepush(\x27NHS TV Page\x27, \x27Button Row\x27, \x27Featured Homes\x27)\x22>Featured Homes</a></li>");
        }
        if (findCommunities !== null && findCommunities.length > 0) {
            findComms = jQuery("<li><a id=\x22GoToFind\x22 onclick=\x22jQuery.googlepush(\x27NHS TV Page\x27, \x27Button Row\x27, \x27Find Communities\x27)\x22>Find Communities</a></li>");
        }

        if (viewSegments !== null && viewSegments.length > 0) {
            segments = jQuery("<li><a id=\x22GoToSegments\x22 onclick=\x22jQuery.googlepush(\x27NHS TV Page\x27, \x27Button Row\x27, \x27View Segments\x27)\x22>View Segments</a></li>");
        }

        if (showSponsors !== null && showSponsors.length > 0) {
            sponsors = jQuery("<li><a id=\x22GoToSponsors\x22 onclick=\x22jQuery.googlepush(\x27NHS TV Page\x27, \x27Button Row\x27, \x27Show Sponsors\x27)\x22>Show Sponsors</a></li>");
        }

        if (self.parameters.PartnerId != 1) {
            jQuery("#nhs_UlNavigation").append(segments, features, findComms);
        } else {
            jQuery("#nhs_UlNavigation").append(features, findComms, segments);
        }

        jQuery("#nhs_UlNavigation").append(sponsors);
    },

    scrollToElement: function (selector) {
        var element = jQuery(selector);
        var offset = element.offset().top;
        $("html, body").animate({
            scrollTop: offset - 60
        }, 100);
    },

    initEvents: function () {
        var self = this;
        if (jQuery("#nhs_tvSignUp").length > 0) {
            jQuery("#nhs_tvSignUp").click(function () {
                self.validateNewsLetterValues("#nhs_tvSignUp", false);
            });
        }

        if (jQuery("#nhs_tvSmallSignUp").length > 0) {
            jQuery("#nhs_tvSmallSignUp").click(function () {
                self.validateNewsLetterValues("#nhs_tvSmallSignUp", true);
            });
        }

        jQuery(document).on("click", ".nhs_Row", function (event) {
            if (event.type === "click" && event.target.className !== "nhs_CallBtn btn ir" && event.target.className !== "ir btn nhs_MapBtn nhs_ModalPopUp") {
                jQuery.CancelEventAndStopPropagation(event);
                var url = jQuery(this).find("a:first-of-type").attr("href");
                window.location.href = url;
            }
        });

        jQuery("#nhs_UlNavigation").on("click", "#GoToFeatured", function () {
            self.scrollToElement("#FeaturedHomes");
        });

        jQuery("#nhs_UlNavigation").on("click", "#GoToFind", function () {
            self.scrollToElement("#FindCommunities");
        });

        jQuery("#nhs_UlNavigation").on("click", "#GoToSegments", function () {
            self.scrollToElement("#ViewSegments");
        });

        jQuery("#nhs_UlNavigation").on("click", "#GoToSponsors", function () {
            self.scrollToElement("#ShowSponsors");
        });

        jQuery("#nhs_TvMarketSlides > div").bxSlider({
            responsive: true,
            infiniteLoop: true,
            minSlides: 1,
            maxSlides: 1,
            pager: true,
            controls: false
        });

        jQuery("#nhs_TvMarketSlides .slidesjs-next").click(function () {
            jQuery.googlepush("NHS TV Page", "Slider", "Slider Right");
        });

        jQuery("#nhs_TvMarketSlides").find("ul").find("li").find("a").click(function () {
            jQuery.googlepush("NHS TV Page", "Slider", "Slider Button");
        });

        jQuery("#nhs_TvMarketPromos > .bxslider").bxSlider({
            responsive: true,
            infiniteLoop: self.parameters.PromosCount > 1,
            minSlides: 1,
            maxSlides: 1,
            pager: self.parameters.PromosCount > 1,
            controls: false
        });      
    },

    initMap: function (parameters) {
        var self = this;
        var mapOptions = parameters.OptionsForMap;
        var googleApi = new NHS.Scripts.GoogleMapApi(mapOptions);

        googleApi.getIconMarkerPoint = function () {
            return parameters.icon;
        };

        googleApi.options.Events.MarkerClickCallBack = function (info) {
            jQuery.googlepush("NHS TV Page", "Map Button", self.capitalizeFirst(info.Keyword));
            var url = self.parameters.urlTvMarket + "/" + info.Keyword;
            window.location = url;
        };

        jQuery("#nhs_TvMap").on("click", ".nhs_commDetail", function () {
            jQuery.googlepush("NHS TV Page", "Map Button", "Featured Community-Click");
        });

        googleApi.createMap();
        googleApi.processResult(self.parameters.TvMarketPoints);
        googleApi.AutoFit();

        setTimeout(
            function () {
                var zoom = googleApi.map.getZoom() - 1;
                googleApi.map.setZoom(zoom);
            }, 1000);
    },

    loadYoutubeAPI: function () {
        //var tag = document.createElement("script");
        //tag.src = "https://www.youtube.com/iframe_api";
        //var firstScriptTag = document.getElementsByTagName("script")[0];
        //firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        jQuery(".nhs_TvVideoItem a.btnCss").click(function () {
            var control = jQuery(this);
            control.parent().find(".nhs_ItemImagesImage").click();
        });

        jQuery(".nhs_ItemImages").click(function () {
            jQuery(".nhs_ItemImagesImage").show();
            jQuery("#nhs_VideoP").remove();
            var control = jQuery(this);
            control.find(".nhs_ItemImagesImage").hide();
            control.append("<div id=\"nhs_VideoP\"></div>");
            var videoId = control.data("videoid");
            new YT.Player("nhs_VideoP", {
                height: 180,
                width: 320,
                videoId: videoId,
                playerVars: { autoplay: 1, 'html5': 1 },
                events: {
                    'onReady': function (event) {
                        event.target.playVideo();
                    }
                }
            });
        });
    },

    //onPlayerStateChange: function (event) {
    //    var element = event.target.d.id;
    //    element = parseInt(element.substr(8, 1)) + 1;
    //    if (event.data === 1)
    //        jQuery.googlepush("NHS TV Page", "Segments", "Place " + element);
    //},

    //onPlayerReady: function () {
    //    jQuery("#nhs_TvSegments a.btnCss").click(function () {
    //            var id = jQuery(this).attr("data");
    //            var currentPlayer = NHS.Mobile.Tv.prototype.nhsYoutubePlayers[id];

    //            if (currentPlayer && currentPlayer.getPlayerState && currentPlayer.getPlayerState() !== 1) {
    //                currentPlayer.playVideo();
    //            }
    //            return false;
    //    });
    //},

    submitNewsletter: function (isSmallSignUp) {
        var data;
        if (isSmallSignUp) {
            data = jQuery("#nhs_TvMarketFormSmall input, #nhs_TvMarketFormSmall textarea").serialize();
        } else {
            data = jQuery("#nhs_TvMarketForm input, #nhs_TvMarketForm textarea").serialize();
        }


        jQuery.ajax({
            url: this.parameters.ContestModalUrl,
            type: "POST",
            data: data,
            success: function (html) {
                if (isSmallSignUp)
                    jQuery("#nhs_TvMarketSignUpSmall").html(html);
                else
                    jQuery("#nhs_TvMarketSignUp").html(html);
            }
        });
    },

    validateNewsLetterValues: function (inputBtn, isSmallTvSignup) {
        var self = this;
        jQuery(inputBtn).hide();
        if (isSmallTvSignup)
            jQuery("#fakeInputLoadingSmall").show();
        else
            jQuery("#fakeInputLoading").show();

        var returnValue = true;
        var email = (isSmallTvSignup) ? jQuery("#smallSignUpEmail").val() : jQuery("#Email").val();
        email = jQuery.trim(email);
        if (!jQuery.ValidEmail(email)) {
            if (isSmallTvSignup)
                jQuery(".nhs_NewsletterSmallSignUpErrors").html("<p class=\"nhs_Error\">Please enter a valid email address.</p>");
            else
                jQuery(".nhs_NewsletterSignUpErrors").html("<p class=\"nhs_Error\">Please enter a valid email address.</p>");
            returnValue = false;
            if (isSmallTvSignup)
                jQuery("#fakeInputLoadingSmall").hide();
            else
                jQuery("#fakeInputLoading").hide();

            jQuery(inputBtn).show();
        }

        if (returnValue) {
            if (isSmallTvSignup) {
                jQuery.googlepush("NHS TV Page", "About the Show", "About-Email Submit");
                self.submitNewsletter(true);
            } else {
                jQuery.googlepush("NHS TV Page", "Main", "Offers-Email Submit");
                self.submitNewsletter(false);
            }

        }
    },
}
