﻿NHS.Mobile.SearchBar = function (parameters) {
    this.parameters = parameters;
    this.search = new NHS.Mobile.Search(this.parameters.typeAheadParameters);
    this.search.initialize();
    this.isCurrentLocation = false;
};

NHS.Mobile.SearchBar.prototype =
{
    initialize: function () {

        var self = this;

        self.PriceHigh = 0;
        self.PriceLow = 0;
        self.Bedrooms = 0;
        self.Bathrooms = 0;

        self.search.LocationChange = function (event) {
            if (event.which == 13 || event.keyCode == 13) {
                jQuery("#HomeSearchIcon").click();
                return false;
            }
            self.isCurrentLocation = false;
            return false;
        };

        jQuery("#HomeSearchIcon, #HomeSearchBtn").click(function (event) {
            var params = [];
            jQuery.CancelEventAndStopPropagation(event);
            var url = "";
            var value = jQuery('#' + self.search.parameters.textBoxName).val();
            if (value.trim().toLowerCase() === "current location") {
                url = self.parameters.CurrentLocationSearch;
            } else if (!self.search.hasValue()) {
                jQuery("#nhs_field_validation_error_home").show();
                return false;
            } else {
                jQuery("#nhs_field_validation_error_home").hide();

                url = self.parameters.Search;
                if (self.search.Location != null) {
                    params.push(jQuery.ToString(self.search.Location));
                } else
                    params.push("searchText=" + jQuery("#SearchText").val() + "&Location=null");
            }

            if (self.Bathrooms > 0)
                params.push("BathRooms=" + self.Bathrooms);

            if (self.Bedrooms > 0)
                params.push("BedRooms=" + self.Bedrooms);

            if (self.PriceHigh > 0)
                params.push("PriceHigh=" + self.PriceHigh);

            if (self.PriceLow > 0)
                params.push("PriceLow=" + self.PriceLow);

            if (jQuery("#AmenityName").length>0)
                params.push("amenity=" + jQuery("#AmenityName").val());

            if (params.length > 0) {
                url += "?" + params.join("&");
            }

            window.location = url;
        });

        jQuery('#SearchText').on('focus', function () {
            jQuery('#nhs_HomeSearchOptions').slideDown(250, function () {
                jQuery('#nhs_HomeSearchOptions > div, #nhs_HomeSearchOptions > p').fadeIn(500);
            });
        });

        jQuery(".nhs_HomeLinks").on('click', function () {
            jQuery('#nhs_HomeSearchOptions').hide();
        });

        jQuery(".nhs_HomeNearbyList").on('click', function () {
            jQuery('#nhs_HomeSearchOptions').hide();
        });

        //Start Beds
        jQuery("#BedsAny").attr("checked", "checked");
        jQuery("input[name=NumOfBeds]").click(function () {
            self.Bedrooms = jQuery(this).val();

        });
        //End Beds

        //Start Baths
        jQuery("#BathAny").attr("checked", "checked");
        jQuery("input[name=NumOfBaths]").click(function () {
            self.Bathrooms = jQuery(this).val();
        });
        //End Baths

        var priceSliderValues = [100000, 110000, 120000, 130000, 140000, 150000, 160000, 170000, 180000, 190000, 200000, 220000, 240000, 260000, 280000, 300000, 325000, 350000, 375000, 400000, 425000, 450000, 475000, 500000, 600000, 700000, 800000, 900000, 1000000];

        //Start Price Padding
        jQuery("#nhs_SliderPrice").slider({
            range: true,
            animate: true,
            min: 0,
            max: (priceSliderValues.length - 1),
            step: 1,
            values: [0, (priceSliderValues.length - 1)],
            slide: function (event, ui) {
                if ((priceSliderValues.length - 1) == ui.values[1]) {
                    self.PriceHigh = 0;
                    jQuery('#PriceHigh').html("No Max");
                } else {
                    self.PriceHigh = priceSliderValues[ui.values[1]];
                    self.facetPriceFormat(jQuery('#PriceHigh').html(self.PriceHigh));
                }

                if (ui.values[0] == 0) {
                    self.PriceLow = 0;
                    jQuery('#PriceLow').html("No Min");
                } else {
                    self.PriceLow = priceSliderValues[ui.values[0]];
                    self.facetPriceFormat(jQuery('#PriceLow').html(self.PriceLow));
                }
            },
            stop: function () {
            }
        });
        //End Price Padding
    },

   facetPriceFormat: function (self) {
        self.priceFormat({
            prefix: '$',
            thousandsSeparator: ',',
            centsLimit: 0,
            insertPlusSign: false,
            limit: 7
        });
    }
};