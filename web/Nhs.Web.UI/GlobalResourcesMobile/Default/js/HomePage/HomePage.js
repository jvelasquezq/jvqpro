﻿NHS.Mobile.HomePage = function () {
};

NHS.Mobile.HomePage.prototype =
{
    initialize: function () {
        var self = this;

        jQuery(document).on('click', '.nhs_HomeNearbyItem ', function (event) {
            jQuery.CancelEventAndStopPropagation(event);
            if (event.type == "click") {
                var url;
                url = jQuery(this).find("a:first-of-type").attr("href");
                window.location.href = url;
            }
        });


       setTimeout(function () { self.CreateSlider(); }, 50);
    },
    CreateSlider: function() {
        jQuery('ul.bxslider').bxSlider({
            responsive: true,
            slideWidth: 210,
            slideMargin: 12,
            minSlides: 1,
            maxSlides: 5,
            pager: false,
            controls: false,
            infiniteLoop: false
    });

    }
};
