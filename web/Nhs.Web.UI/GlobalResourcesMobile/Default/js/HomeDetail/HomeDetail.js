﻿NHS.Mobile.HomeDetail = function(parameters) {
    this.parameters = parameters;
    this.logger = new NHS.Scripts.Globals.EventLogger({ siteRoot: parameters.logInfo.siteRoot, partnerId: parameters.logInfo.partnerId, partnerName: parameters.logInfo.partnerName, marketId: parameters.logInfo.marketId, tdvTable: parameters.logInfo.tdvTable });
    this.player = new NHS.Scripts.WebPlayers({
        videoWidth: (parameters.viewerParams.videoWidth || 0),
        videoHeight: (parameters.viewerParams.videoHeight || 0),
        youtubeControlId: 'nhs_VideP',
        vimeoControlId: 'nhs_VideP',
        brightcoveControlId: 'nhs_VideP',
        brightcoveToken: parameters.viewerParams.brightcoveToken
    });
};

NHS.Mobile.HomeDetail.prototype =
{
    init: function () {
        var self = this;

        jQuery(".nhs_CommMoreLink, .nhs_CommMoreClose").on('click', function (event) {
          
            jQuery.CancelEventAndStopPropagation(event);
            jQuery(".nhs_CommMoreLink > span").toggleClass("expanded");
            if (jQuery(".nhs_CommPanel").hasClass("expanded")) {
                jQuery("#nhs_CommunityGalleryTitle").html("View Community Info");

                if (jQuery(this).hasClass("nhs_CommMoreClose")) {
                    jQuery.ScrollToWithCallBak(230, function () {
                        jQuery(".nhs_CommPanel").toggleClass("expanded").slideUp("slow");
                    });
                } else {
                    jQuery.ScrollTo(230);
                    jQuery(".nhs_CommPanel").toggleClass("expanded").slideUp();
                }
            }
            else {
                jQuery("#nhs_CommunityGalleryTitle").html(self.parameters.HideCommunityInfoText);
                jQuery(".nhs_CommPanel").toggleClass("expanded").slideDown();
                jQuery('.nhs_CommPanel .bxslider').bxSlider({
                    responsive: false,
                    infiniteLoop: false,
                    slideWidth: 126,
                    slideMargin: 5,
                    minSlides: 1,
                    maxSlides: 5,
                    pager: false,
                    controls: false
                });

                jQuery.ScrollTo(230);
            }
        });

        jQuery.ajax({
            url: self.parameters.GetCommunityMediaPlayerObjects + "?communityId=" + self.parameters.logInfo.commId,
            type: "GET",
            success: function (data) {
                if (typeof data == "string" && data.length > 0) {
                    jQuery("#nhs_Gallery").show();
                    jQuery('#nhs_GalleryList').html(data);
                    jQuery("#nhs_GalleryList .nhs_LinkGallery").click(function (event) {
                        jQuery.CancelEventAndStopPropagation(event);
                        self.ShowModal(this);
                    });
                }
            }
        });

        jQuery(document).on("click", "#nhs_ModalBody img.nhs_LinkImg", function (event) {
            if (event.type === "click") {
                window.open(this.src.match(/^[^\#\?]+/)[0], "_blank");
            }
        });

        jQuery.ajax({
            url: self.parameters.GetMediaPlayerObjects,
            type: "GET",
            success: function (data) {
                if (typeof data == "string" && data.length > 0) {
                    jQuery('#nhs_HomeGalleryList').html(data);
                    jQuery("#nhs_HomeGalleryList").toggleClass("expanded").slideDown();
                    jQuery("#nhs_HomeGalleryList .nhs_LinkGallery").click(function (event) {
                        jQuery.CancelEventAndStopPropagation(event);
                        self.ShowModal(this);
                    });

                    jQuery('#nhs_HomeGalleryList .bxslider').bxSlider({
                        responsive: false,
                        infiniteLoop: false,
                        slideWidth: 126,
                        slideMargin: 5,
                        minSlides: 1,
                        maxSlides: 5,
                        pager: false,
                        controls: false
                    });
                }
            }
        });


        jQuery(document).on('click', '.nhs_Row', function (event) {
            jQuery.CancelEventAndStopPropagation(event);
            if (event.type == "click") {
                var url;
                url = jQuery(this).find("a:first-of-type").attr("href");
                window.location.href = url;
            }
        });

        jQuery.ajax({
            url: self.parameters.GetFloorPlanGalleryUrl,
            type: "GET",
            success: function (data) {
                if (typeof data == "string" && data.length > 0) {
                    jQuery('#nhs_FloorplanGallery').html(data);
                    jQuery("#nhs_Floorplan").toggleClass("expanded").slideDown();
                    jQuery("#nhs_FloorplanGallery .bxslider a").click(function (event) {
                        jQuery.CancelEventAndStopPropagation(event);
                        self.ShowModal(this);
                    });

                    jQuery('#nhs_FloorplanGallery .bxslider').bxSlider({
                        responsive: false,
                        infiniteLoop: false,
                        slideWidth: 146,
                        slideMargin: 5,
                        minSlides: 1,
                        maxSlides: 5,
                        pager: false,
                        controls: false
                    });
                }
            }
        });

        jQuery("#nhs_Saved").click(function (event) {
            var control = jQuery(this);
            jQuery.CancelEventAndStopPropagation(event);
            jQuery.ajax({
                url: self.parameters.SaveCommunity,
                type: "POST",
                success: function (result) {
                    if (result.data) {
                        control.replaceWith('<span class="nhs_Save nhs_Saved">' + self.parameters.SavedText + '</span>');
                    } else {
                        modalPopUp.Show(result.redirectUrl);
                    }
                }
            });
        });


        jQuery("#nhs_OpenHours").click(function (event) {
            jQuery.CancelEventAndStopPropagation(event);
            var modal = modalPopUp.CreateModal();
            var hoursHtml = jQuery("#nhs_HoursOfOperation").html();
            modal.find("#nhs_ModalBody").html(hoursHtml);
            //modal.find("#nhs_ModalBody").css("padding-top", "60px");
            modal.addClass("expanded");
        });

        jQuery('.nhs_HomesSimilar .bxslider').bxSlider({
            responsive: false,
            infiniteLoop: false,
            slideWidth: 190,
            slideMargin: 5,
            minSlides: 1,
            maxSlides: 5,
            pager: false,
            controls: false
        });

    },
    ShowVideo: function (control) {
        var self = this;

        self.player.stopVideo(true);
        control = jQuery(control);
        var type = control.data('type');

        if (type.indexOf('l-') != -1) {
            var url = control.data('url');
            var eventCode = control.data('event');
            self.logger.logAndRedirect(event, url, eventCode, self.parameters.logInfo, true);
        }
        else if (type == 'v-yt' || type == 'v-vm' || type == 'v-bc') {
            var videoId = control.data('id');
            var parent = control.parent();
            parent.append('<div id="nhs_VideP"></div>');
            control.hide();
            var div = control.parent()[0];
            var videoWidth = div.clientWidth;
            var videoHeight = div.clientHeight;

            self.player.parameters.videoWidth = videoWidth;
            self.player.parameters.videoHeight = videoHeight;

            if (type == 'v-yt') {
                self.player.playYouTubeVideo(videoId);
            } else if (type == 'v-vm') {
                self.player.playVimeoVideo(videoId);
            } else if (type == 'v-bc') {
                self.player.playBrightcoveVideo(videoId);
            }
        } 
    },
    ShowModal: function (link) {
        var self = this;
        var modal = modalPopUp.CreateModal();
        link = jQuery(link);

        var isFloorplan = link.parent().parent().parent().parent().parent().parent().hasClass("nhs_FloorplanList");
        var control = link.parent().parent().parent().clone();
        var startSlide = parseInt(link.attr("data-slide"));
        var w = jQuery('<div class="' + (isFloorplan ? "nhs_FloorplanList" : "nhs_GalleryList") + ' clearfix"></div>').append(control);
        modal.find("#nhs_ModalBody").append(w);
        if (!isFloorplan) {
            modal.find(".nhs_GalleryList").wrap('<div class="nhs_Gallery"></div>');
            modal.find(".nhs_Gallery").prepend('<h2>' + self.parameters.TouchToViewFullSizeText + '</h2>');
            modal.find(".nhs_LinkGallery").click(function(event) {
                self.ShowVideo(this);
            });
        } else {
            modal.find(".nhs_FloorplanList").wrap('<div class="nhs_FloorPlan"></div>');
            modal.find(".nhs_FloorPlan").prepend('<h2>' + self.parameters.TouchToViewFullSizeText + '</h2>');
        }

        modal.addClass("expanded");
        control.bxSlider({
            responsive: true,
            infiniteLoop: false,
            slideWidth: 234,
            slideMargin: 10,
            minSlides: 1,
            maxSlides: 5,
            startSlide: startSlide,
            pager: false,
            controls: false
        });
    }
};
