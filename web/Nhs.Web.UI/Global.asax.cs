using System;
using System.Configuration;
using System.Diagnostics;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Routing;
using log4net.Config;
using Nhs.Library.Common;
using Nhs.Mvc.Routing.Interface;
using Nhs.Mvc.Routing.Web;
using Nhs.UrlRewriter.Configuration;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Lib.StructureMap;
using RouteDebug;
using Configuration = System.Configuration.Configuration;

namespace Nhs.Web.UI
{
    public class Global : HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            XmlConfigurator.Configure();
            WebCacheManager.LastWebRefresh = DateTime.Now;
            Debug.Assert(true);
            //*******************************************************************************************
            //Reference this line must be use ONLY when debugging/profiling EF, remove otherwise
            //HibernatingRhinos.Profiler.Appender.EntityFramework.EntityFrameworkProfiler.Initialize(); // <-+ 
            //*******************************************************************************************
            RegisterRoutes(RouteTable.Routes);
            RegisterViewEngines();
            RegisterStructureMapBootStrapper();
            WurlfSetup.Configuration();
        }

        private static void RegisterStructureMapBootStrapper()
        {
            StructureMapBootStrapper.Configure();
            ControllerBuilder.Current.SetControllerFactory(new StructureMapControllerFactory());
        }

        private static void RegisterViewEngines()
        {
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new NhsViewEngine());
        }

        private static void RegisterRoutes(RouteCollection routes)
        {
            MvcRoutes.Initialize(routes, new ContextPathMapper());
            if(ConfigurationManager.AppSettings["RouteDebug"].ToType<bool>())
                RouteDebugger.RewriteRoutesForTesting(RouteTable.Routes);
        }

        protected void Application_End(object sender, EventArgs e)
        {
          
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            //ObjectFactory.ReleaseAndDisposeAllHttpScopedObjects();  
            if (NhsRoute.IsBrandPartnerNhsPro)
            {
                var nhsProCookie = new HttpCookie("NHSPro_Cookie");
                DateTime expirationDate = DateTime.Now.AddMonths(1);
                nhsProCookie.Expires = expirationDate;
                nhsProCookie.Value = expirationDate.ToShortDateString();

                Response.Cookies.Remove("NHSPro_Cookie");
                Response.Cookies.Add(nhsProCookie);
                
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            ErrorLogger.LogError(Server.GetLastError());
            Response.StatusCode = 500;
            RedirectPerCustomErrorsSection(sender);
        }

        /// <summary>
        /// Redirects to custom error page as defined in custom errors section.
        /// Applies PartnerSiteUrl if needed.
        /// </summary>
        /// <param name="sender">The HttpApp.</param>
        private void RedirectPerCustomErrorsSection(object sender)
        {
            var app = (HttpApplication)sender;
            Configuration configuration = WebConfigurationManager.OpenWebConfiguration("~");
            var section = (CustomErrorsSection)configuration.GetSection("system.web/customErrors");
            string defaultRedirectUrl = ("/" + (RewriterConfiguration.CurrentPartnerSiteUrl ?? string.Empty) +
                                        section.DefaultRedirect.TrimStart('~')).Replace("//", "/");

            if (defaultRedirectUrl.StartsWith("/") == false)
            {
                defaultRedirectUrl = "/" + defaultRedirectUrl;
            }

            if (section.Mode == CustomErrorsMode.On || (section.Mode == CustomErrorsMode.RemoteOnly && app.Request.UserHostAddress != "127.0.0.1"))
                Response.Redirect(defaultRedirectUrl);//*/
        }
    }
}
