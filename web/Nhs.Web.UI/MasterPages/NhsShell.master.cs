using System;
using System.Web.Mvc;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using System.Web.UI.WebControls;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Domain.Model.CMS;
using Nhs.Mvc.Routing.Interface;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;

public partial class NhsShell : MasterPageBase
{
    protected Nhs.Web.Controls.Default.Common.btnAddThis phBtnAddThis;
    protected Nhs.Web.Controls.BDX.Common.footer footer;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            ModalWindowHelper.InitActiveUserFlag(Page);
        Initialize();
        
        ChatWithUsLink.Text = LiveChatHelper.LiveChatLink("Chat with us",
            "$.googlepush('Chat Links','Navigation New','Live Chat - New Home Search')", ChatPlacement.NhsChatLink).ToString();
    }

    private void Initialize()
    {
        //SetUpNoHomes();
        ToggleCreateAccountLink();

        if (!XGlobals.Partner.AllowRegistration)
        {
            //lnkRecentItems.Visible = false;
            //lblNoOfHomes.Text = "No homes viewed.";
            //lblNoOfHomes.Visible = true;
            fbLoginButton.Visible = false;
            lnkSignIn.Visible = false;
            lnkSignOff.Visible = false;
            lnkRegister.Visible = false;
            liNewHomePlanner.Visible = false;
        }

#pragma warning disable 618
        var cmsService = new CmsService(new WebCacheService());
#pragma warning restore 618
        var categories = cmsService.GetCategoriesHierarchy(NhsRoute.BrandPartnerId);
        //ulCategories.DataSource = categories;
        //ulCategories.DataBind();
    }

    private void ToggleCreateAccountLink()
    {
        var isActiveUser = UserSession.UserProfile.ActorStatus == WebActors.ActiveUser;
        fbLoginButton.Visible = lnkRegister.Visible = !isActiveUser;
        liNewHomePlanner.Visible = isActiveUser;
    }

    //private void SetUpNoHomes()
    //{
    //    int homecount = UserSession.UserProfile.Planner.RecentlyViewedHomes.Count;
    //    if (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser || homecount >= 1)
    //    {
    //        lnkRecentItems.Visible = true;
    //        lnkRecentItems.Text = string.Format("{0} home{1} viewed.", homecount, (homecount == 1) ? string.Empty : "s");
    //        lblNoOfHomes.Visible = false;
    //    }
    //    else
    //    {
    //        lnkRecentItems.Visible = false;
    //        lblNoOfHomes.Text = "No homes viewed.";
    //        lblNoOfHomes.Visible = true;
    //    }
    //}

    protected void ulCategories_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var category = (RCCategory)e.Item.DataItem;
            var lnkCategory = (HyperLink)e.Item.FindControl("lnkCategory");
            lnkCategory.NavigateUrl = "~/resourcecenter/" + category.CategoryName.ToLower().Replace(" ", "-");
            lnkCategory.Text = category.CategoryName;
        }
    }
}
