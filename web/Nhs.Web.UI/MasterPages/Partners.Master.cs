using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Nhs.Library.Constants;
using Nhs.Library.Web;
using Nhs.Library.Common;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.HtmlHelpers;

namespace NHS.Web.UI.MasterPages
{
    public partial class Partners : MasterPageBase
    {
        protected System.Web.UI.WebControls.PlaceHolder plhDocType;
        protected System.Web.UI.HtmlControls.HtmlHead PageHead;
        protected System.Web.UI.WebControls.PlaceHolder plhShellBodyTop;
        protected System.Web.UI.WebControls.ContentPlaceHolder cplhMainContainer;
        protected System.Web.UI.WebControls.PlaceHolder plhShellBodyBottom;
        protected Nhs.Web.Controls.Default.Common.personal_bar partnerPersonalBar;
       
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
  
                Shell partnerShell = base.GetShell(Nhs.Library.Common.Configuration.PartnerShellType);
                this.imgPoweredBy.Visible = partnerShell.HasNHSBranding;
                //this.Page.Header.Title = partnerShell.Title.InnerMarkup; //Set page title

                partnerShell.Head.InnerMarkup = this.RemoveTitle(partnerShell.Head.InnerMarkup);
                    //Remove <title> from head if any 
                this.PageHead.Controls.Add(new LiteralControl(partnerShell.Head.InnerMarkup)); //Add Head
                this.ltrPageBodyOpenTag.Text = "<" + "body" + partnerShell.Body.Attributes + ">";
                    //Add <body attribs></body> tags
                this.ltrPageBodyClosingTag.Text = "</body>";
                this.plhShellBodyTop.Controls.Add(new LiteralControl(partnerShell.UpperBodyContent));
                this.plhShellBodyBottom.Controls.Add(new LiteralControl(partnerShell.LowerBodyContent));
                Top.Visible = (XGlobals.Partner.PartnerId == Convert.ToInt32(PartnersConst.Yahoo));
            }
            catch(Exception exception)
            {
                ErrorLogger.LogError(exception);
                Response.Redirect("~/Error");
            }
        }

        private string RemoveTitle(string head)
        {
            int startOfTitle = head.IndexOf("<title>");
            int endOfTitle = head.IndexOf("</title>");
            int lengthOfTitleEnd = "</title>".Length;
            //Check if <title> present
            if (startOfTitle >= 0)
            {
                //Find substring <title>XXXXXXX</title>
                string title = head.Substring(startOfTitle, endOfTitle + lengthOfTitleEnd - startOfTitle);
                //Replace with blank
                return head.Replace(title, string.Empty);
            }
            else
                return string.Empty;
        }
    }
}
