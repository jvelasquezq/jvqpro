using System;
using System.Web.UI;
using Nhs.Library.Web;
namespace NHS.Web.UI.MasterPages
{
    public partial class PartnersAds : MasterPageBase
    {
        protected Nhs.Web.Controls.Default.Common.footer ftrPartner;
      
        protected void Page_Load(object sender, EventArgs e)
        {
            Shell partnerShell = base.GetShell(Nhs.Library.Common.Configuration.PartnerShellType);
            this.imgPoweredBy.Visible = partnerShell.HasNHSBranding;
            this.Page.Header.Title = partnerShell.Title.InnerMarkup; //Set page title

            partnerShell.Head.InnerMarkup = this.RemoveTitle(partnerShell.Head.InnerMarkup);            //Remove <title> from head if any 
            this.PageHead.Controls.Add(new LiteralControl(partnerShell.Head.InnerMarkup));              //Add Head
            this.ltrPageBodyOpenTag.Text = "<" + "body" + partnerShell.Body.Attributes + ">";           //Add <body attribs></body> tags
            this.ltrPageBodyClosingTag.Text = "</body>";
            this.plhShellBodyTop.Controls.Add(new LiteralControl(partnerShell.UpperBodyContent));
            this.plhShellBodyBottom.Controls.Add(new LiteralControl(partnerShell.LowerBodyContent));
        }

        private string RemoveTitle(string head)
        {
            int startOfTitle = head.IndexOf("<title>");
            int endOfTitle = head.IndexOf("</title>");
            int lengthOfTitleEnd = "</title>".Length;
            //Check if <title> present
            if (startOfTitle >= 0)
            {
                //Find substring <title>XXXXXXX</title>
                string title = head.Substring(startOfTitle, endOfTitle + lengthOfTitleEnd - startOfTitle);
                //Replace with blank
                return head.Replace(title, string.Empty);
            }
            else
                return string.Empty;
        }
    }
}
