using System.Web;
using Nhs.Utility.Common;

namespace Nhs.UrlRewriter.Enums
{
    public static class ManageReferrerLookup
    {
        private static string _path;
        public static string GetPath
        {
            get
            {
                return !string.IsNullOrEmpty(_path) ? _path : new ContextPathMapper().MapPath("~/BHIContent/SEOContent/TrafficSource/ReferrerLookup.xml");
            }
            set { _path = value; }
        }

        public static ReferrerLookup GetTable
        {
            get { return XmlSerialize.DeserializeFromXmlFile<ReferrerLookup>(GetPath); }
        }

        public static string GetParameters(ReferrerInfo referrerInfo)
        {
            return
                string.Format("utm_medium={0}&utm_source={1}&utm_campaign={2}",
                              HttpUtility.HtmlEncode(referrerInfo.Medium), HttpUtility.HtmlEncode(referrerInfo.Source),
                              HttpUtility.HtmlEncode(referrerInfo.Campaign));
        }
    }
}