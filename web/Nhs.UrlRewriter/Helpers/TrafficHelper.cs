﻿using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Nhs.Library.Web;
using Nhs.UrlRewriter.Configuration;
using Nhs.UrlRewriter.Enums;
using Nhs.Utility.Common;

namespace Nhs.UrlRewriter.Helpers
{
    public static class TrafficHelper
    {
        public static string Validate(HttpRequest request, Uri urlReferrer, NhsUrl nhsUrl)
        {
            var host = urlReferrer != null ? urlReferrer.Host.ToLowerInvariant() : string.Empty;
            if (nhsUrl.Parameters.Any(p => p.DefaultName.ToLower() == "referrerredirect"))
                return null;

            //Is not a internat URL and not Pro (reqs applies only for nhs, mnh and nhl)
            if (!IsInternalUrl(host) && !IsNhsProSite(request) || urlReferrer == null)
            {
                var @params = new List<string> {"utm_source", "utm_medium", "utm_term", "utm_content", "utm_campaign", "gclid"};

                if (!request.QueryString.AllKeys.Where(p => !string.IsNullOrEmpty(p)).Any(p => @params.Contains(p.ToLower()))) 
                {
                    var referringurl = urlReferrer != null ? urlReferrer.Host : "";
                    var refer = nhsUrl.Parameters.FirstOrDefault(p => p.DefaultName.ToLower() == "refer");
                    var refercode = refer != null ? refer.Value : request.QueryString["refer"] ?? "";

                    var destinationurl = RewriterConfiguration.CurrentPartnerSiteUrl;
                    var referrerLookup = ReferrerLookupHelper.ReferrerLookupTable;
                    ReferrerInfo referrerInfo = null;
                  
                    //MATCHES DESTINATION URL IN LOOKUP TABLE
                    if (!string.IsNullOrEmpty(destinationurl))
                        referrerInfo = referrerLookup.ReferrerInfo.FirstOrDefault(p => p.DestinationUrl == destinationurl);

                    //HAS REFER CODE AND DOES NOT MATCH ANYTHING IN LOOKUP (NO PART - 54543)
                    if (referrerInfo == null && !string.IsNullOrEmpty(refercode) && !referrerLookup.ReferrerInfo.Where(p => !string.IsNullOrEmpty(p.ReferCode)).Any(p => StringHelper.IsSqlLikeMatch(refercode.ToLower(), p.ReferCode.ToLower().Replace("*", "%"))))
                        return request.Url.ToString();

                    //HAS REFER CODE AND HAS MATCHING VALUE IN LOOKUP (YES PART - 54543)
                    if (referrerInfo == null && !string.IsNullOrEmpty(refercode))
                        referrerInfo = referrerLookup.ReferrerInfo.Where(p => !string.IsNullOrEmpty(p.ReferCode)).FirstOrDefault(p => StringHelper.IsSqlLikeMatch(refercode.ToLower(), p.ReferCode.ToLower().Replace("*", "%")));                 

                    //HAS REFERRING URL AND MATCHES VALUE IN LOOKUP
                    if (referrerInfo == null && !string.IsNullOrEmpty(referringurl) )
                        referrerInfo = referrerLookup.ReferrerInfo.Where(p => !string.IsNullOrEmpty(p.ReferringUrl)).FirstOrDefault(p => referringurl.ToLower().Contains(p.ReferringUrl.ToLower()));

                    if (referrerInfo != null)
                        return string.Format("{0}{1}{2}", request.Url, (request.QueryString.AllKeys.Any() ? "&" : "?"), ReferrerLookupHelper.GetParameters(referrerInfo));
                    
                }
            }
            return null;
        }

        public static bool IsInternalUrl(string url)
        {
            return  RewriterConfiguration.DomainNamePro.Contains(url) ||
                    url.Contains(RewriterConfiguration.DomainNamePro);
        }

        public static bool IsNhsProSite(HttpRequest request)
        {
            return RewriterConfiguration.DomainNamePro.Contains(request.Url.Host) ||
                    request.Url.Host.Contains(RewriterConfiguration.DomainNamePro);
        }
    }
}