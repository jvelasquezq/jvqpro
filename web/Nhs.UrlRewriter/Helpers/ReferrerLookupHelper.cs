using System.Web;
using Nhs.UrlRewriter.Configuration;
using Nhs.UrlRewriter.Enums;
using Nhs.Utility.Common;
using Nhs.Utility.Data;

namespace Nhs.UrlRewriter.Helpers
{
    public static class ReferrerLookupHelper
    {

        public static ReferrerLookup ReferrerLookupTable
        {
            get { return GetReferrerLookupFile(); }
        }

        public static string GetParameters(ReferrerInfo referrerInfo)
        {
            return
                string.Format("utm_medium={0}&utm_source={1}&utm_campaign={2}",
                              HttpUtility.HtmlEncode(referrerInfo.Medium), HttpUtility.HtmlEncode(referrerInfo.Source),
                              HttpUtility.HtmlEncode(referrerInfo.Campaign));
        }


        private static ReferrerLookup GetReferrerLookupFile()
        {

            string cacheKey = string.Concat("ReferrerLookup_", RewriterConfiguration.StaticContentFolder);
            var referrerLookup = Caching.GetObjectFromCache(cacheKey) as ReferrerLookup;
            if (referrerLookup != null) return referrerLookup;

            var path = new ContextPathMapper();
            string lookupPath = path.MapPath("~/" + RewriterConfiguration.StaticContentFolder + "/SEOContent/TrafficSource/ReferrerLookup.xml");
            referrerLookup = XmlSerialize.DeserializeFromXmlFile<ReferrerLookup>(lookupPath);

            //24 hours default
            Caching.AddObjectToCache(referrerLookup, cacheKey);

            return referrerLookup;

        }
    }
}