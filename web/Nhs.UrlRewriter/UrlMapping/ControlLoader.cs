﻿/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright (c) 2001-2006
 **by BuilderHomeSite Inc. ( http://www.BuilderHomeSite.com )
 * ========================================================
 * Author: RMenon
 * Date: 10/11/2006 22:53:35
 * Purpose:
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Nhs.UrlRewriter.Configuration;
using Nhs.Utility.Common;

[assembly: InternalsVisibleTo("Nhs.Library")]
namespace Nhs.UrlRewriter.UrlMapping
{
    internal class ControlLoader
    {

        #region Properties
        #endregion

        #region Constructor
        #endregion

        #region Public Methods
        /// <summary>
        /// Loads the Partner/Group/Default user control.
        /// </summary>
        /// <param name="controlFileName">Name of the control file.</param>
        /// <returns></returns>
        public static string GetUserControlPath(string controlFileName)
        {
            string controlPath;
            //controlPath gets value from controlPathHashTable if its not null in which 
            //case it gets from FileSystem under Partner/Group/Default folders in that order
            controlPath = FindUserControl(controlFileName);
            return controlPath;
        }


        #endregion

        #region "Private Methods"
        /// <summary>
        /// Finds the user control.
        /// </summary>
        /// <returns></returns>
        private static string FindUserControl(string controlFileName)
        {
            List<string> controlVirtualFolders = new List<string>();
            //order matters!!
            if (!string.IsNullOrEmpty(RewriterConfiguration.CurrentPartnerSiteUrl))
                controlVirtualFolders.Add("~/Controls/" + RewriterConfiguration.CurrentPartnerSiteUrl);

            //TO DO: use Configuration.GroupControlsFolder for that partner instead of explicity using Realtor
            controlVirtualFolders.Add(string.Format("{0}_{1}", UrlMappingConfiguration.Partner_GroupControlFolder, RewriterConfiguration.PartnerId));
            controlVirtualFolders.Add(string.Format("{0}_{1}_{2}", UrlMappingConfiguration.PartnerBrand_GroupControlFolder, RewriterConfiguration.PartnerType, RewriterConfiguration.PartnerId));
            controlVirtualFolders.Add(string.Format("{0}_{1}_{2}", UrlMappingConfiguration.PartnerBrand_GroupControlFolder, RewriterConfiguration.PartnerType, RewriterConfiguration.CurrentBrandPartnerId));
            controlVirtualFolders.Add(string.Format("{0}_{1}", UrlMappingConfiguration.PartnerBrand_GroupControlFolder, RewriterConfiguration.CurrentBrandPartnerId));
            controlVirtualFolders.Add(UrlMappingConfiguration.DefaultControlsFolder);
            string userControlPath = string.Empty;
            foreach (string controlVirtualFolder in controlVirtualFolders)
            {
                userControlPath = PathHelpers.GetFileInVirtualFolder(controlVirtualFolder, controlFileName, new ContextPathMapper());
                //check all other folders if userControlPath is empty
                if (!string.IsNullOrEmpty(userControlPath))
                { break; }
            }
            return userControlPath;
        }
        #endregion
    }
}
