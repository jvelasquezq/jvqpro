using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.XPath;
using Nhs.Library.Web;
using Nhs.UrlRewriter.Configuration;
using Nhs.Utility.Common;

namespace Nhs.UrlRewriter.UrlMapping
{
    /// <summary>
    /// Object to Read Config Settings
    /// </summary>
    class NhsUrlMapper
    {
        const string MappingConfigFileName = "MappingConfig";
        private string _mappingConfigFileUsed = string.Empty;
        private readonly IPathMapper _pathMapper;

        #region Public Methods
        /// <summary>
        /// Configuration Reader to read mapping configuration.
        /// </summary>
        /// <param name="pathMapper">The path mapper.</param>
        public NhsUrlMapper(IPathMapper pathMapper)
        {
            _pathMapper = pathMapper;
        }

        /// <summary>
        /// Retrieve a function or parameter map value from _defaultMappingConfigXmlDoc. The
        /// default value will be matched first. If that isn't found, an alias
        /// will be matched. If no alias if found, string.Empty will be
        /// returned.
        /// </summary>
        /// <param name="groupName">The name of the main type that the value is
        /// being retrieved for, either a funtion (functionmaps) or a parameter
        /// (parammaps).</param>
        /// <param name="subGroupName">The type of element that contains the
        /// value being matched, function for the functionmaps section, params
        /// for the parammaps section.</param>
        /// <param name="keyToMatch">The name of the attribute that contains the
        /// value being matched.</param>
        /// <param name="valueToMatch">The value being matched.</param>
        /// <returns></returns>
        public string GetValueForKey(string groupName, string subGroupName, string keyToMatch, string valueToMatch)
        {
            string result = GetValueForKeyFromPartnerMappingConfig(groupName, subGroupName, keyToMatch, valueToMatch);
            if (string.IsNullOrEmpty(result))
            {
                result = GetValueForKeyFromBrandPartnerMappingConfig(groupName, subGroupName, keyToMatch, valueToMatch);
            }
            return result;
        }

        public string GetValueForKey(string groupName, string subGroupName, string keyToMatch, string valueToMatch, out string mappingConfigFileUsed)
        {
            string result = GetValueForKeyFromPartnerMappingConfig(groupName, subGroupName, keyToMatch, valueToMatch);
            if (string.IsNullOrEmpty(result))
            {
                result = GetValueForKeyFromBrandPartnerMappingConfig(groupName, subGroupName, keyToMatch, valueToMatch);
            }
            //_mappingConfigFileUsed is set in either GetValueForKeyFromPartnerMappingConfig or GetValueForKeyFromBrandPartnerMappingConfig
            mappingConfigFileUsed = _mappingConfigFileUsed;
            return result;
        }

        public string GetDefaultName(string aliasUsed)
        {
            string partnerMappingConfig = GetPartnerMappingConfigFilePath();
            XmlDocument partnerMappingConfigXmlDoc = GetMappingConfigAsXmlDoc(partnerMappingConfig);

            var defaultName = GetDefaultName(aliasUsed, partnerMappingConfigXmlDoc);
            if (string.IsNullOrEmpty(defaultName))
            {
                string brandPartnerMappingConfigFilePath = GetBrandPartnerMappingConfigFilePath();
                XmlDocument brandPartnerMappingConfig = GetMappingConfigAsXmlDoc(brandPartnerMappingConfigFilePath);

                defaultName = GetDefaultName(aliasUsed, brandPartnerMappingConfig);
            }
            return defaultName;
        }

        public List<string> GetAliasesForDefaultNames(string defaultName)
        {
            string partnerMappingConfig = GetPartnerMappingConfigFilePath();
            XmlDocument partnerMappingConfigXmlDoc = GetMappingConfigAsXmlDoc(partnerMappingConfig);

            var aliases = GetAliasesForDefaultNames(defaultName, partnerMappingConfigXmlDoc);
            if (aliases == null || aliases.Count == 0)
            {
                string brandPartnerMappingConfigFilePath = GetBrandPartnerMappingConfigFilePath();
                XmlDocument brandPartnerMappingConfig = GetMappingConfigAsXmlDoc(brandPartnerMappingConfigFilePath);

                aliases = GetAliasesForDefaultNames(defaultName, brandPartnerMappingConfig);
            }
            return aliases;
        }

        public List<string> GetOtherAliasesForThisAlias(string currentAlias, string defaultName)
        {
            List<string> aliases = GetAliasesForDefaultNames(defaultName);
            aliases.Remove(currentAlias);
            return aliases;

        }

        #endregion

        #region Private Methods

        private string GetBrandPartnerMappingConfigFilePath()
        {
            if (NhsUrl.PartnerId != 0)
            {
                var partnerMappingConfigSuffix = string.Format("_BrandPartner*${0}.xml", RewriterConfiguration.CurrentBrandPartnerId);
                var partnerMappingConfigFilePattern = MappingConfigFileName + partnerMappingConfigSuffix;
                return GetFileMatchingPattern(partnerMappingConfigFilePattern);
            }
            return null;
        }

        private string GetPartnerMappingConfigFilePath()
        {
            if (NhsUrl.PartnerId != 0)
            {
                var partnerMappingConfigSuffix = string.Format("*_{0}.xml", NhsUrl.PartnerId);
                var partnerMappingConfigFilePattern = MappingConfigFileName + partnerMappingConfigSuffix;
                return GetFileMatchingPattern(partnerMappingConfigFilePattern);
            }
            return null;
        }

        private string GetFileMatchingPattern(string fileNamePattern)
        {
            var mappingConfigDir = UrlMappingConfiguration.MappingConfigFolder;
            mappingConfigDir = _pathMapper.MapPath(mappingConfigDir);
            DirectoryInfo dir = new DirectoryInfo(mappingConfigDir);

            if (dir.Exists)
            {
                FileInfo fi = dir.GetFiles(fileNamePattern, SearchOption.TopDirectoryOnly).FirstOrDefault();
                return (fi != null ? fi.FullName : null);
            }

            return null;
        }

        private XmlDocument GetMappingConfigAsXmlDoc(string docPath)
        {
            XmlDocument doc = null;
            if (!string.IsNullOrEmpty(docPath) && File.Exists(docPath))
            {
                doc = GetFromAppCache<XmlDocument>(docPath);
                if (doc == null)
                {
                    //convert xml file content to lower case and load it into xmlDoc object
                    //xpath1.0 queries are strictly case sensitive. Hence the need to
                    //convert to lowercase.
                    using (StreamReader str = new StreamReader(docPath))
                    {
                        doc = new XmlDocument();
                        doc.Load(new StringReader(str.ReadToEnd().ToLower()));

                    }
                    AddToAppCache(docPath, doc); // add to cache
                }

            }
            return doc;
        }

        private string GetValueForKeyFromPartnerMappingConfig(string groupName, string subGroupName, string keyToMatch, string valueToMatch)
        {
            string partnerMappingConfig = GetPartnerMappingConfigFilePath();
            if (partnerMappingConfig == null) return string.Empty;
            XmlDocument partnerMappingConfigXmlDoc = GetMappingConfigAsXmlDoc(partnerMappingConfig);
            _mappingConfigFileUsed = partnerMappingConfig;
            return GetValueForKeyFromMappingConfig(groupName, subGroupName, keyToMatch, valueToMatch,
                                                   partnerMappingConfigXmlDoc);
        }

        private string GetValueForKeyFromBrandPartnerMappingConfig(string groupName, string subGroupName, string keyToMatch, string valueToMatch)
        {
            string brandPartnerMappingConfigFilePath = GetBrandPartnerMappingConfigFilePath();
            XmlDocument brandPartnerMappingConfig = GetMappingConfigAsXmlDoc(brandPartnerMappingConfigFilePath);
            _mappingConfigFileUsed = brandPartnerMappingConfigFilePath;
            return GetValueForKeyFromMappingConfig(groupName, subGroupName, keyToMatch, valueToMatch,
                                                   brandPartnerMappingConfig);
        }



        private string GetValueForKeyFromMappingConfig(string groupName, string subGroupName, string keyToMatch, string valueToMatch, XmlDocument mappingConfig)
        {
            if (mappingConfig != null)
            {
                groupName = groupName.ToLower();
                subGroupName = subGroupName.ToLower();
                keyToMatch = keyToMatch.ToLower();
                valueToMatch = valueToMatch.ToLower().RemoveSpecialCharacters();

                XmlNode root = mappingConfig.DocumentElement;

                string defaultQuery =
                    string.Format("/{0}/{1}/{2}[@defaultname='{3}']",
                                  root.Name,
                                  groupName,
                                  subGroupName,
                                  valueToMatch);

                string aliasQuery =
                    string.Format("/{0}/{1}/{2}/aliases[alias='{3}']",
                                  root.Name,
                                  groupName,
                                  subGroupName,
                                  valueToMatch);
                string paramDefaultNameQuery = string.Format("/{0}/{1}/{2}[@defaultname='{3}']",
                                  root.Name,
                                  "parammaps",
                                  subGroupName,
                                  valueToMatch);

                XmlNode node = null;

                switch (groupName)
                {
                    case "functionmaps":
                        node = root.SelectSingleNode(defaultQuery);
                        if (node == null)
                        {
                            node = root.SelectSingleNode(aliasQuery);
                            if (node != null)
                                node = node.ParentNode;
                        }
                        break;
                    case "parammaps":
                        node = root.SelectSingleNode(aliasQuery);
                        if (node != null)
                            node = node.ParentNode;
                        else
                            node = root.SelectSingleNode(defaultQuery);
                        break;
                    case "defaultparamname":
                        node = root.SelectSingleNode(paramDefaultNameQuery);

                        break;
                }

                string result = string.Empty;

                if (node != null)
                {
                    node = node.Attributes.GetNamedItem(keyToMatch);
                    if (node != null)
                        result = node.Value;
                }
                return result;
            }

            return null;
        }

        private string GetDefaultName(string aliasUsed, XmlDocument mappingConfig)
        {
            // 
            if (mappingConfig != null)
            {
                XPathNavigator navigator = mappingConfig.CreateNavigator();
                string query = string.Format("siteurl/parammaps/param/aliases[alias='{0}']/../@defaultname", aliasUsed);
                //get all aliases for defaultname passed
                try
                {
                    var node = navigator.SelectSingleNode(query);
                    if (node != null)
                        return node.Value;

                }
                catch
                {
                    // Ignore error
                }


            }
            return null;
        }



        private List<string> GetAliasesForDefaultNames(string defaultName, XmlDocument mappingConfig)
        {
            if (mappingConfig != null)
            {
                XmlNode root = mappingConfig.DocumentElement;

                XPathNavigator navigator = mappingConfig.CreateNavigator();
                List<string> aliases = new List<string>();
                defaultName = defaultName.ToLower();

                string query =
                    string.Format("/{0}/{1}/{2}[@defaultname='{3}']/aliases//alias",
                                  root.Name,
                                  "parammaps",
                                  "param",
                                  defaultName); //get all aliases for defaultname passed
                try
                {
                    XPathNodeIterator nodes = navigator.Select(query);

                    while (nodes.MoveNext())
                    {
                        aliases.Add(nodes.Current.Value);
                    }

                }
                catch
                {
                    // Ignore error
                }

                return aliases;
            }
            return null;
        }


        private void AddToAppCache(string cacheKey, object data)
        {
            HttpContext.Current.Application[cacheKey] = data;
        }

        private T GetFromAppCache<T>(string cacheKey) where T : class
        {
            return HttpContext.Current.Application[cacheKey] as T;
        }
        #endregion
    }
}
