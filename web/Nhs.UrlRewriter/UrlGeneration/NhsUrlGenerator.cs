using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Utils;
using Nhs.UrlRewriter.Configuration;
using Nhs.UrlRewriter.UrlMapping;
using Nhs.UrlRewriter.Utils;
using Nhs.Utility.Common;
using NHS.UrlRewriter.DAL;

namespace Nhs.UrlRewriter.UrlGeneration
{
    public class NhsUrlGenerator
    {
        private readonly NhsUrl _nUrl;
        private readonly ReaderWriterLockSlim _partnerSiteUrlRwLock = new ReaderWriterLockSlim();
        private readonly ReaderWriterLockSlim _partnerTypeCodeLock = new ReaderWriterLockSlim();

        private NhsUrlGenerator(Uri uri, NhsUrl nhsUrlToPopulate, IPathMapper pathMapper)
        {
            //urlPathSegments contains all in the pathinfo part of the url. i.e. no query params or host name etc.
            string[] urlPathSegments = uri.AbsolutePath.ToLowerInvariant()
                                          .Split(new[] {"/"}, StringSplitOptions.RemoveEmptyEntries);
            //_nUrl can be assinged by the Generate overload
            _nUrl = nhsUrlToPopulate;

            //if segments array is empty then
            //check the host name and populate the partnerid
            //default it to NHS
            if (urlPathSegments.Length == 0)
            {
                RewriterConfiguration.CurrentBrandPartnerId = NhsUrl.PartnerId = RewriterConfiguration.PartnerIdPro;
                _nUrl.Function = string.Empty;
                RewriterConfiguration.CurrentPartnerSiteUrl = string.Empty;
                RewriterConfiguration.PartnerType = string.Empty;
            }

            else if (urlPathSegments.Length > 0)
            {
                //parts could be PartnerSiteUrl or FunctionName(that decides control to load) or Friendly/Query/Hash parameters
                //first segment of the app path could be 
                //1.PartnerSiteUrl
                //2.Function
                //3.Friendly Parameter
                byte paramIndex = 0;
                var pathFirstPart = urlPathSegments[paramIndex];

                var partnerId = GetPartnerIDFromPartnerSiteUrl(pathFirstPart);

                if (partnerId == null)
                {
                    _nUrl.IsInactivePartnerOrNotAPrivateLabel = true;
                    RewriterConfiguration.CurrentPartnerSiteUrl = pathFirstPart;
                    return;
                }

                //check if first part is partnersiteurl
                //eg:newhomesource.com/chroniclehomes
                if (partnerId.HasValue && partnerId.Value != 0)
                {
                    NhsUrl.PartnerId = partnerId.Value;
                    _nUrl.Function = string.Empty;

                    RewriterConfiguration.CurrentPartnerSiteUrl = pathFirstPart;
                    //partnerid has been set by now. Get brand partner id
                    RewriterConfiguration.CurrentBrandPartnerId = GetBrandPartnerId(NhsUrl.PartnerId);
                    RewriterConfiguration.PartnerType = GetPartnerTypeCode(NhsUrl.PartnerId);

                }
                    //check if first part is a friendly param without a function or partnersiteurl
                    //eg: newhomesource.com/refer-xyz, newhomesource.com/util/refrescache.aspx
                else if (partnerId.HasValue && partnerId.Value == 0)
                {
                    //if partner id is 0 then default to current available brand partner id depending on the url
                    NhsUrl.PartnerId = RewriterConfiguration.PartnerIdPro;
                    //partnerid has been set by now. Get brand partner id

                    RewriterConfiguration.CurrentPartnerSiteUrl = string.Empty;
                    RewriterConfiguration.CurrentBrandPartnerId = GetBrandPartnerId(NhsUrl.PartnerId);
                    RewriterConfiguration.PartnerType = GetPartnerTypeCode(NhsUrl.PartnerId);

                    string[] extensionsToIgnore = RewriterConfiguration.ExtensionsToIgnore;
                    var newExtensionsToIgnore = extensionsToIgnore.Concat(new[] {".aspx"});
                    //ExtensionsToIgnore doesn't have .aspx because we wanna handle leadsubmission pages
                    //eg:yahoo/leadsubmit.aspx etc
                    //ReWriterModule will finally hand over .aspx to DefaultHandler if function ==string.Empty
                    _nUrl.Function =
                        (IsDefaultName(
                            pathFirstPart.Split(RewriterConfiguration.FriendlyUrlDelimiter.ToCharArray())[0], pathMapper) ||
                         newExtensionsToIgnore.Any(uri.AbsolutePath.Contains))
                            ? string.Empty
                            : null;
                }


                //eg:newhomesource.com/siteindex OR newhomesource.com/ OR newhomesource/trebsubmitlead (note: empty string is a function)
                //Find function 
                if (NhsUrlFunctionMap.IsFunction(pathFirstPart, pathMapper))
                {
                    _nUrl.Function = pathFirstPart;
                }


                //check from 2nd segment of the uri which was split earlier
                //1. Segment could be friendly param
                //2.Segement could be hashed param
                //TODO: Friendly url extension check seems legacy. Could be removed
                for (paramIndex = 1; paramIndex < urlPathSegments.Length; paramIndex++)
                {
                    //if param contains file name, break - last param
                    string urlPart = urlPathSegments[paramIndex];
                    //check if second param is a function eg:chroniclehomes/communitydetail
                    if (NhsUrlFunctionMap.IsFunction(urlPart, pathMapper))
                    {
                        _nUrl.Function = urlPart;
                        continue;
                    }

                    if (!string.IsNullOrEmpty(_nUrl.Function))
                    {
                        if (paramIndex == urlPathSegments.Length - 1 &&
                            !urlPart.Contains(RewriterConfiguration.FriendlyUrlDelimiter) &&
                            IsBase64String(urlPart)) //Check for hash - Check if last one
                        {
                            _nUrl.HashCodePart = urlPart;
                        }
                        else
                            //Check and add friendly params to tmpFriendlyParam dictionary
                            AddFriendlyParamsToNhsUrl(urlPart, pathMapper);
                    }
                    else
                        AddFriendlyParamsToNhsUrl(urlPart, pathMapper);
                }
            }

            if (!string.IsNullOrEmpty(_nUrl.Function))
            {
                //Get Default Name
                _nUrl.DefaultName = NhsUrlFunctionMap.GetDefaultName(_nUrl.Function, pathMapper);
                //Set control location
                string mappingConfigFileUsed;
                _nUrl.ControlLocation = NhsUrlFunctionMap.GetControlLocation(_nUrl.Function, out mappingConfigFileUsed,
                                                                             pathMapper);
                _nUrl.MappingConfigFileUsed = mappingConfigFileUsed;
                //populate  properties correspoding to function
                _nUrl.AdPageName = NhsUrlFunctionMap.GetAdPageName(_nUrl.Function, pathMapper);
                _nUrl.EnableTheme = NhsUrlFunctionMap.EnableTheme(_nUrl.Function, pathMapper);
                _nUrl.UseMasterPage = NhsUrlFunctionMap.UseMasterPage(_nUrl.Function, pathMapper);
                _nUrl.MasterPageFile = NhsUrlFunctionMap.MasterPageFile(_nUrl.Function, pathMapper);
                _nUrl.Theme = NhsUrlFunctionMap.Theme(_nUrl.Function, pathMapper);
            }

            //Check to see if there is querystring add to paramlist of nUrl
            AddQueryStringParamsToNhsUrl(uri.Query, pathMapper);
            //Add hashcode parameters to nUrl
            AddHashCodeParamsToNhsUrl(pathMapper);
        }

        private void AddHashCodeParamsToNhsUrl(IPathMapper pathMapper)
        {
            if (!string.IsNullOrEmpty(_nUrl.HashCodePart))
            {
                string decodedUrlString = _nUrl.HashCodePart.DecodeBase64();
                if (!string.IsNullOrEmpty(decodedUrlString))
                    AddQueryStringParamsToNhsUrl(decodedUrlString, pathMapper);
            }
        }

        /// <summary>
        /// Gets param corresponding to a given alias and adds to temp list
        /// </summary>
        /// <param name="aliaswithvalue">The aliaswithvalue.</param>
        private void AddFriendlyParamsToNhsUrl(string aliaswithvalue, IPathMapper pathMapper)
        {
            AddFriendlyParamsToNhsUrl(aliaswithvalue, false, pathMapper);
        }

        /// <summary>
        /// Gets param corresponding to a given alias and adds to temp list
        /// </summary>
        /// <param name="aliaswithvalue">The aliaswithvalue.</param>
        /// <param name="reverseParamNameValueOrder">dfu</param>
        private void AddFriendlyParamsToNhsUrl(string aliaswithvalue, bool reverseParamNameValueOrder, IPathMapper pathMapper)
        {
            //splits aliaswithvalue using delimiter and returns a NhsUrlParam
            NhsUrlParam nhsUrlParam = ParseNameValueToNhsUrlParam(aliaswithvalue,
                                                                  RewriterConfiguration.FriendlyUrlDelimiter,
                                                                  RouteParamType.Friendly, reverseParamNameValueOrder);
            if (nhsUrlParam != null)
            {
                //adds entries for remaining aliases and also for the defaultname if needed
                HydrateAliasesAndDefaultNames(nhsUrlParam, pathMapper);
            }
        }

        private void AddQueryStringParamsToNhsUrl(string queryString, IPathMapper pathMapper)
        {
            string[] queryStringNameValues = queryString.TrimStart('?').Split(new[] { '&' }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string queryStringNameValue in queryStringNameValues)
            {
                NhsUrlParam nhsUrlParam = ParseNameValueToNhsUrlParam(queryStringNameValue, "=", RouteParamType.QueryString);

                if (nhsUrlParam != null)
                {
                    HydrateAliasesAndDefaultNames(nhsUrlParam, pathMapper);
                }
            }
        }


        private void HydrateAliasesAndDefaultNames(NhsUrlParam nhsUrlParam, IPathMapper pathMapper)
        {
            var mappingConfiguration = new NhsUrlMapper(pathMapper);
            //getvaluekey returns defaultname if aliasused is defaultname
            //and returns the defaultname if alias used is an alias
            //checks for default param name
            //for the current alias and adds an entry for same

            if (IsDefaultName(nhsUrlParam.AliasUsed, pathMapper))
            {
                nhsUrlParam.DefaultName = nhsUrlParam.AliasUsed;
            }
            //add entry in the tmp dic for  all its aliases
            //aliasUsed is different from defaultname
            //provide entry for current alias as well as other aliases
            else
            {
                nhsUrlParam.DefaultName = mappingConfiguration.GetDefaultName(nhsUrlParam.AliasUsed) ??
                                          nhsUrlParam.AliasUsed;
            }
            List<string> aliases = mappingConfiguration.GetAliasesForDefaultNames(nhsUrlParam.DefaultName);
            if (nhsUrlParam.Aliases == null)
                nhsUrlParam.Aliases = new List<string>();
            nhsUrlParam.Aliases.AddRange(aliases);

            _nUrl.Parameters.Add(nhsUrlParam);
        }

        private void AddDfuParam(string nameValueParam, byte paramIndex, IPathMapper pathMapper)
        {
            switch (paramIndex)
            {
                case 1: //State
                    nameValueParam = nameValueParam.Replace("-", " ");
                    nameValueParam = nameValueParam + "-" + "statename";
                    break;

                case 2: //Market
                    //Check if market name has a dash
                    string marketName = nameValueParam.Remove(nameValueParam.LastIndexOf("-"));
                    marketName = marketName.Replace("-", " ");
                    nameValueParam = marketName + "-" + "area";
                    break;

                case 3: //City or Zip or County
                    int postalCode = -1;
                    var cmprMthd = StringComparison.InvariantCultureIgnoreCase;
                    if (nameValueParam.StartsWith("refer" + "-", cmprMthd) ||
                        nameValueParam.StartsWith("brandid" + "-", cmprMthd) ||
                        nameValueParam.StartsWith("pricehigh" + "-", cmprMthd) ||
                        nameValueParam.StartsWith("pricelow" + "-", cmprMthd) ||
                        nameValueParam.StartsWith("green" + "-", cmprMthd)
                    )
                    {
                        AddFriendlyParamsToNhsUrl(nameValueParam, false, pathMapper);
                        return;
                    }
                    if (nameValueParam.EndsWith("-" + "county"))
                    {
                        //Check if county name has a dash
                        if (nameValueParam.IndexOf("-") != nameValueParam.LastIndexOf("-"))
                        {
                            string countyName = nameValueParam.Remove(nameValueParam.LastIndexOf("-"));
                            countyName = countyName.Replace("-", " ");
                            nameValueParam = countyName + "-" + "county";
                        }
                    }
                    else if (nameValueParam.Length == 5 && int.TryParse(nameValueParam, out postalCode))
                    {
                        nameValueParam = nameValueParam + "-" + "postalcode";
                    }
                    else
                    {
                        nameValueParam = nameValueParam.Replace("-", " ");
                        nameValueParam = nameValueParam + "-" + "city";
                    }
                    break;

                default:
                    AddFriendlyParamsToNhsUrl(nameValueParam, false, pathMapper);
                    return;
            }

            AddFriendlyParamsToNhsUrl(nameValueParam, true, pathMapper);
        }

        private NhsUrlParam ParseNameValueToNhsUrlParam(string nameValue, string delimiter, RouteParamType urlType,
                                                        bool reverseParamNameValueOrder)
        {
            NhsUrlParam param = null;
            var paramNameValue = new string[2];
            //down the line friendlydelimeter was changed to -
            //a split on friendlydelimeter then will not retrieve correct paramValue
            //hence the hack
            if (nameValue.IndexOf(delimiter) != -1)
            {
                paramNameValue[0] = nameValue.Substring(0, nameValue.IndexOf(delimiter));
                paramNameValue[1] = nameValue.Replace(paramNameValue[0] + delimiter, string.Empty);
                if (paramNameValue.Length > 1)
                {
                    param = new NhsUrlParam();
                    string paramName;
                    string paramValue;
                    if (reverseParamNameValueOrder)
                    {
                        paramName = paramNameValue[1];
                        paramValue = paramNameValue[0];
                    }
                    else
                    {
                        paramName = paramNameValue[0];
                        paramValue = paramNameValue[1];
                    }

                    param.AliasUsed = paramName;
                    param.DefaultName = paramName;
                    param.Value = HttpUtility.UrlDecode(paramValue);
                    param.ParamType = urlType;
                }
            }
            return param;
        }


        private bool IsBase64String(string urlPart)
        {
            return ((urlPart.Length % 4) == 0);
        }

        private bool IsInactivePartnerOrNotaPrivateLabel(string pathFirstPart)
        {
            object partnerSiteUrls;

            _partnerSiteUrlRwLock.TryEnterReadLock(new TimeSpan(0, 0, 10));
            try
            {
                partnerSiteUrls = HttpContext.Current.Application[RewriterConfiguration.InactivePartnerSiteUrls];

            }
            finally
            {
                _partnerSiteUrlRwLock.ExitReadLock();
            }

            if (partnerSiteUrls == null)
            {
                try
                {
                    _partnerSiteUrlRwLock.TryEnterWriteLock(new TimeSpan(0, 0, 10));
                    partnerSiteUrls = (HttpContext.Current.Application[RewriterConfiguration.InactivePartnerSiteUrls] = GetInactiveOrNonPrivateLabelPartnerSiteUrls());
                }
                finally
                {

                    _partnerSiteUrlRwLock.ExitWriteLock();
                }
            }


            if (partnerSiteUrls != null)
            {
                var p = partnerSiteUrls.ToType<Dictionary<string, int>>();
                int value;
                if (p.TryGetValue(pathFirstPart.ToLowerInvariant(), out value))
                {

                    return true;
                }
            }

            return false;
        }

        private int? GetPartnerIDFromPartnerSiteUrl(string pathFirstPart)
        {
            object partnerSiteUrls;
            if (IsInactivePartnerOrNotaPrivateLabel(pathFirstPart))
            {
                return null;
            }


            _partnerSiteUrlRwLock.TryEnterReadLock(new TimeSpan(0, 0, 10));
            try
            {
                partnerSiteUrls = HttpContext.Current.Application[RewriterConfiguration.PartnerSiteUrls];

            }
            finally
            {
                _partnerSiteUrlRwLock.ExitReadLock();
            }

            if (partnerSiteUrls == null)
            {
                try
                {
                    _partnerSiteUrlRwLock.TryEnterWriteLock(new TimeSpan(0, 0, 10));
                    partnerSiteUrls = (HttpContext.Current.Application[RewriterConfiguration.PartnerSiteUrls] = GetPartnerSiteUrls());
                }
                finally
                {

                    _partnerSiteUrlRwLock.ExitWriteLock();
                }
            }


            if (partnerSiteUrls != null)
            {
                var p = partnerSiteUrls.ToType<Dictionary<string, int>>();
                int value;
                if (p.TryGetValue(pathFirstPart, out value))
                {

                    return value;
                }
            }

            return 0;
        }

        private string GetPartnerTypeCode(int partnerId)
        {
            object partnerTypeCode;
            _partnerTypeCodeLock.TryEnterReadLock(new TimeSpan(0, 0, 10));

            try
            {
                partnerTypeCode = HttpContext.Current.Application[RewriterConfiguration.PartnerTypeCodes];

            }
            finally
            {
                _partnerTypeCodeLock.ExitReadLock();
            }

            if (partnerTypeCode == null)
            {
                _partnerTypeCodeLock.TryEnterWriteLock(new TimeSpan(0, 0, 10));
                try
                {
                    partnerTypeCode =
                        (HttpContext.Current.Application[RewriterConfiguration.PartnerTypeCodes] = GetPartnerTypeCodes());
                }
                finally
                {
                    _partnerTypeCodeLock.ExitWriteLock();
                }

            }


            if (partnerTypeCode != null)
            {
                var p = partnerTypeCode.ToType<Dictionary<int, string>>();
                string value;
                if (p.TryGetValue(partnerId, out value))
                {

                    return value;
                }
            }

            return string.Empty;
        }

        private Dictionary<int, string> GetPartnerTypeCodes()
        {
            var db = new NHSWeb(RewriterConfiguration.WebDbConnectionString);

            return
                (from p in db.Partners
                 where p.Status_id == 1 && p.Partner_type_code != null && p.Partner_type_code != string.Empty
                 select new { PartnerID = p.Partner_id, PartnerTypeCode = p.Partner_type_code }).ToDictionary(k => k.PartnerID, k => k.PartnerTypeCode);
        }

        private int GetBrandPartnerId(int partnerId)
        {
            object partnerBrandPartnerDict;
            _partnerSiteUrlRwLock.TryEnterReadLock(new TimeSpan(0, 0, 10));
            try
            {
                partnerBrandPartnerDict = HttpContext.Current.Application[RewriterConfiguration.PartnerBrandPartnerDict];
                if (partnerBrandPartnerDict != null)
                {
                    var p = partnerBrandPartnerDict.ToType<Dictionary<int, int>>();
                    int value;
                    if (p.TryGetValue(partnerId, out value))
                    {

                        return value;
                    }
                }
            }
            finally
            {
                _partnerSiteUrlRwLock.ExitReadLock();
            }

            _partnerSiteUrlRwLock.TryEnterWriteLock(new TimeSpan(0, 0, 10));
            try
            {
                partnerBrandPartnerDict =
                  (HttpContext.Current.Application[RewriterConfiguration.PartnerBrandPartnerDict] = GetPartnerBrandPartner());
                if (partnerBrandPartnerDict != null)
                {
                    var p = partnerBrandPartnerDict.ToType<Dictionary<int, int>>();
                    int value;
                    if (p.TryGetValue(partnerId, out value))
                    {

                        return value;
                    }
                }

            }
            finally
            {
                _partnerSiteUrlRwLock.ExitWriteLock();
            }


            return 0;
        }

        private Dictionary<int, int> GetPartnerBrandPartner()
        {
            var db = new NHSWeb(RewriterConfiguration.WebDbConnectionString);

            return
                (from p in db.Partners
                 where p.Status_id == 1
                 select new { PartnerID = p.Partner_id, BrandPartnerID = p.Brand_Partner_id })
                 .ToDictionary(k => k.PartnerID, k => k.BrandPartnerID);
        }


        private Dictionary<string, int> GetPartnerSiteUrls()
        {
            var db = new NHSWeb(RewriterConfiguration.WebDbConnectionString);

            return
                (from pst in db.Partner_site_terms
                 join p in db.Partners on pst.Partner_id equals p.Partner_id
                 where pst.Term_key.ToLower() == "partnersiteurl"
                 & (pst.Term_value != null && pst.Term_value != string.Empty)
                 & (p.Status_id == 1 && p.Private_label_site != null && p.Private_label_site.ToString().ToLower() == "y")
                 select new { TermValue = pst.Term_value.ToLowerInvariant(), PartnerId = pst.Partner_id })
                 .ToDictionary(k => k.TermValue, k => k.PartnerId);

        }

        private Dictionary<string, int> GetInactiveOrNonPrivateLabelPartnerSiteUrls()
        {
            var db = new NHSWeb(RewriterConfiguration.WebDbConnectionString);

            return
                (from pst in db.Partner_site_terms
                 join p in db.Partners on pst.Partner_id equals p.Partner_id
                 where pst.Term_key.ToLower() == "partnersiteurl"
                 & (pst.Term_value != null && pst.Term_value != string.Empty)
                 & (p.Status_id == 2 || p.Private_label_site.ToString().ToLower() != "y")
                 select new { TermValue = pst.Term_value.ToLowerInvariant(), PartnerId = pst.Partner_id })
                 .ToDictionary(k => k.TermValue, k => k.PartnerId);
        }

        private bool IsDefaultName(string parameterName, IPathMapper pathMapper)
        {
            return
                !string.IsNullOrEmpty(new NhsUrlMapper(pathMapper).GetValueForKey("defaultparamname", "param",
                                                                                "defaultname", parameterName));
        }


        private NhsUrlParam ParseNameValueToNhsUrlParam(string nameValue, string delimiter, RouteParamType urlType)
        {
            return ParseNameValueToNhsUrlParam(nameValue, delimiter, urlType, false);
        }

        public static NhsUrl Generate(Uri uri, bool validate, NhsUrl nhsUrl, IPathMapper pathMapper)
        {
            return new NhsUrlGenerator(uri, nhsUrl, pathMapper)._nUrl;
        }
    }


}
