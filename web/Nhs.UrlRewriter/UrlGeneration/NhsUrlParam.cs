using System;
using System.Collections.Generic;
using Nhs.Library.Enums;
using Nhs.Mvc.Routing.Configuration;

namespace Nhs.Library.Web
{
    /// <summary>
    /// This class represents the Params under ParamMaps collection in MappingConfig.xml
    /// ParamMaps collection holds params and looks as below:
    /// <code>
    ///<![CDATA[<parammaps>
    ///             <param defaultname="st">
    ///              <aliases>
    ///                <alias>state</alias>
    ///              </aliases>
    ///            </param>
    ///            <param defaultname="marketid">
    ///              <aliases>
    ///                <alias>market</alias>
    ///             </aliases>
    ///            </param>
    ///         </parammaps>
    /// ]]>
    /// </code>
    /// </summary>
    [Serializable]
    public class NhsUrlParam:IEquatable<NhsUrlParam>
    {
        #region "Public Properties"

        public int Order { get; set; }
        /// <summary>
        /// Corresponds to defaultname attribute in param
        /// </summary>
        /// <value>The name of the param.</value>
        public string DefaultName { get; set; }

        /// <summary>
        /// Gets or sets the param value.
        /// </summary>
        /// <value>The param value.</value>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets the type of the param.
        /// </summary>
        /// <value>The type of the param.</value>
        public RouteParamType ParamType { get; set; }

        private string _aliasUsed;

        /// <summary>
        /// Gets or sets the alias used.
        /// Multiple aliases can be used for same parameter
        /// See code below:
        /// <code>
        ///<![CDATA[
        ///         <parammaps>
        ///             <param defaultname="postalcode">
        ///              <aliases>
        ///                <alias>zip</alias>
        ///                <alias>zipcode</alias>
        ///              </aliases>
        ///            </param>
        ///         </parammaps>
        /// ]]>
        /// </code>
        /// Parameter with name PostalCode can be also referred to as zipcode and zip. But for current url AliasUsed 
        /// will refer to the current alias in the url.
        /// </summary>
        /// <value>The alias used.</value>
        public string AliasUsed
        {
            get { return _aliasUsed??DefaultName; }
            set { _aliasUsed = value; }
        }

        public List<string> Aliases { get; set;}

        #endregion

        #region "Constructor"

        public NhsUrlParam()
        {
        }

        public NhsUrlParam(string paramName, string paramValue)
        {
            DefaultName = paramName;
            Value = paramValue;
        }

        public NhsUrlParam(string paramName, string paramValue, RouteParamType paramType)
        {
            DefaultName = paramName;
            Value = paramValue;
            ParamType = paramType;
        }

        #endregion

        public bool Equals(NhsUrlParam other)
        {
            return DefaultName == other.DefaultName ;
        }
    }
}