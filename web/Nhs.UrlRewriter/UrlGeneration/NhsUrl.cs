using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Nhs.Library.Enums;
using Nhs.Mvc.Routing.Configuration;
using Nhs.UrlRewriter.Configuration;
using Nhs.UrlRewriter.UrlGeneration;
using Nhs.UrlRewriter.Utils;
using Nhs.Utility.Common;

namespace Nhs.Library.Web
{
    /// <summary>
    /// Class to represent/generate a friendly URL. 
    /// </summary>
    [Serializable]
    public class NhsUrl
    {
        private string _adPageName;
        /// <summary>
        /// private field for holding current httpcontext
        /// </summary>
        private string _controlLocation;
        private string _defaultName;
        private bool _enableTheme;
        private string _friendlyParameterPart;
        private string _function = string.Empty;
        private string _hashedParameterPart;
        private bool _isSecure;
        private string _masterPageFile;
        private string _pageName;
        private readonly NhsUrlParamList _parameters;
        private string _queryParameterPart;

        private string _theme;
        private bool _useMasterPage;
        private string _mappingConfigFileUsed;

        /// <summary>
        /// Initializes a new instance of the <see cref="NhsUrl"/> class.
        /// </summary>
        public NhsUrl()
        {
            _parameters = new NhsUrlParamList();
            _queryParameterPart = string.Empty;
            _hashedParameterPart = string.Empty;
            _friendlyParameterPart = string.Empty;
            Function = string.Empty;
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="NhsUrl"/> class.
        /// </summary>
        /// <param name="function">The function.</param>
        public NhsUrl(string function)
            : this()
        {
            Function = function;
        }

        public NhsUrl(Uri uri, IPathMapper pathMapper)
            : this()
        {
            NhsUrlGenerator.Generate(uri, false, this, pathMapper);
        }



        /// <summary>
        /// Initializes a new instance of the <see cref="NhsUrl"/> class.
        /// </summary>
        /// <param name="function">The function.</param>
        /// <param name="friendlyParameters">The friendly parameters.</param>
        public NhsUrl(string function, params NhsUrlParam[] friendlyParameters)
            : this(function)
        {
            foreach (NhsUrlParam param in friendlyParameters)
            {
                _parameters.Add(param);
            }
        }

        /// <summary>
        /// Used to specify the Source Page Name
        /// </summary>
        /// <value>The theme.</value>
        public string Theme
        {
            get { return _theme; }
            internal set { _theme = value; }
        }

        /// <summary>
        /// Used to specify the Source Page Name
        /// </summary>
        /// <value>The theme.</value>
        public string MappingConfigFileUsed
        {
            get { return _mappingConfigFileUsed; }
            internal set { _mappingConfigFileUsed = value; }
        }

        public string FriendlyParameterPartAll
        {
            get { return HttpContext.Current.Request.Url.AbsolutePath; }
        }

        public string SiteRoot
        {
            get
            {
                return (VirtualPathUtility.ToAbsolute("~/") 
                    + (RewriterConfiguration.CurrentPartnerSiteUrl ?? string.Empty) 
                    + "/").Replace("//", "/");
            }
        }

        public string SiteRootWithDomainInfo
        {
            get
            {
                Uri currentUri = HttpContext.Current.Request.Url;
                string domainInfo = currentUri.AbsoluteUri.Replace(currentUri.PathAndQuery, string.Empty);
                string siteRootWithDomainInfo = RewriterConfiguration.CurrentPartnerSiteUrl + "/";
                siteRootWithDomainInfo = domainInfo.TrimEnd('/') + "/" + new Regex("/+").Replace(siteRootWithDomainInfo, "/");
                return siteRootWithDomainInfo;
            }
        }

        public string DomainInfo
        {
            get
            {
                Uri currentUri = HttpContext.Current.Request.Url;
                return currentUri.AbsoluteUri.Replace(currentUri.PathAndQuery, string.Empty) + "/";
            }
        }

        /// <summary>
        /// Gets or sets the master page file.
        /// </summary>
        /// <value>The master page file.</value>
        public string MasterPageFile
        {
            get { return _masterPageFile; }
            set { _masterPageFile = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to use master page.
        /// </summary>
        /// <value><c>true</c> if master page is in use; otherwise, <c>false</c>.</value>
        public bool UseMasterPage
        {
            get { return _useMasterPage; }
            internal set { _useMasterPage = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to enable theme.
        /// </summary>
        /// <value><c>true</c> if teme is enabled; otherwise, <c>false</c>.</value>
        public bool EnableTheme
        {
            get { return _enableTheme; }
            internal set { _enableTheme = value; }
        }

        /// <summary>
        /// Gets or sets the name of the page.
        /// </summary>
        /// <value>The name of the page.</value>
        public string PageName
        {
            get { return _pageName; }
            internal set { _pageName = value; }
        }

        /// <summary>
        /// Gets or sets the name of the ad page.
        /// </summary>
        /// <value>The name of the ad page.</value>
        public string AdPageName
        {
            get { return _adPageName; }
            internal set { _adPageName = value; }
        }

        /// <summary>
        /// Used to specify whether target URL is secure (https)
        /// </summary>
        public bool IsSecure
        {
            get { return _isSecure; }
            internal set { _isSecure = value; }
        }

        /// <summary>
        /// Used to set the function name, if any.
        /// </summary>
        public string Function
        {
            get { return _function; }
            set { _function = value; }
        }

        /// <summary>
        /// Location of control corresponding to a function
        /// </summary>
        public string ControlLocation
        {
            get { return _controlLocation; }
            internal set { _controlLocation = value; }
        }


        /// <summary>
        /// Contains UrlParameters
        /// </summary>
        public NhsUrlParamList Parameters
        {
            get { return _parameters; }
        }

        /// <summary>
        /// Gets the DefaultName for a function - Use when you want to obtain DefaultName for a given alias
        /// </summary>
        public string DefaultName
        {
            get { return _defaultName; }
            internal set { _defaultName = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to persist params or not.
        /// </summary>
        /// <value><c>true</c> if set to persist params; otherwise, <c>false</c>.</value>
        public bool PersistParams { get; set; }

        /// <summary>
        /// Gets the query parameter part.
        /// </summary>
        /// <value>The query parameter part.</value>
        public string QueryParameters
        {
            get { return _queryParameterPart; }
            internal set { _queryParameterPart = value; }
        }

        /// <summary>
        /// Gets the hashed parameter part.
        /// </summary>
        /// <value>The hashed parameter part.</value>
        public string HashCodePart
        {
            get { return _hashedParameterPart; }
            internal set { _hashedParameterPart = value; }
        }

        /// <summary>
        /// Gets the friendly parameter part.
        /// </summary>
        /// <value>The friendly parameter part.</value>
        public string FriendlyParameters
        {
            get { return _friendlyParameterPart; }
        }


        public static int PartnerId
        {
            get { return HttpContext.Current.Items[RewriterConfiguration.ContextItemsCurrentPartnerId].ToType<int>(); }
            internal set { HttpContext.Current.Items[RewriterConfiguration.ContextItemsCurrentPartnerId] = value; }
        }


        /// <summary>
        /// Gets the friendly URL from.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static NhsUrl GetFriendlyUrlFrom(HttpContext context)
        {
            NhsUrl s = context.Items[RewriterConfiguration.ContextItemsCurrentUrl] as NhsUrl;
            return s.DeepCloneObject<NhsUrl>();

        }



        /// <summary>
        /// Gets the friendly referrer URL from.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static NhsUrl GetFriendlyReferrerUrlFrom(HttpContext context)
        {
            NhsUrl s = context.Items[RewriterConfiguration.ContextItemsReferrerUrl] as NhsUrl;
            return s.DeepCloneObject<NhsUrl>();
        }



        #region "Public Methods"

        /// <summary>
        /// Finds the param.
        /// </summary>
        /// <param name="paramName">Name of the param.</param>
        /// <returns></returns>
        public NhsUrlParam FindParam(string paramName)
        {
            string p = paramName.ToLowerInvariant();
            return Parameters.FirstOrDefault(x => (x.DefaultName.ToLowerInvariant() == p ||
                                                   x.Aliases.Contains(p, StringComparer.InvariantCultureIgnoreCase)));
        }


        /// <summary>
        /// Adds the parameter.
        /// </summary>
        /// <param name="paramName">Name of the param.</param>
        /// <param name="paramValue">The param value.</param>
        public NhsUrl AddParameter(string paramName, string paramValue)
        {
            return AddParameter(paramName, paramValue, RouteParamType.Friendly);
        }

        /// <summary>
        /// Adds the parameter.
        /// </summary>
        /// <param name="paramName">Name of the param.</param>
        /// <param name="paramValue">The param value.</param>
        /// <param name="paramType">Type of the param.</param>
        public NhsUrl AddParameter(string paramName, string paramValue, RouteParamType paramType)
        {
            return AddParameter(new NhsUrlParam(paramName, paramValue, paramType));
        }

        /// <summary>
        /// Adds the parameter.
        /// </summary>
        /// <param name="param">The param.</param>
        public NhsUrl AddParameter(NhsUrlParam param)
        {
            _parameters.Add(param);
            return this;
        }

        /// <summary>
        /// Adds the passthrough param.
        /// </summary>
        /// <param name="paramName">Name of the param.</param>
        public void AddPassthroughParam(string paramName)
        {

            if (HttpContext.Current != null)
            {
                var currentUrl = (NhsUrl)HttpContext.Current.Items[RewriterConfiguration.ContextItemsCurrentUrl];
                if (currentUrl != null)
                {
                    NhsUrlParam nparam = currentUrl.Parameters.FirstOrDefault(x => x.DefaultName == paramName);
                    if (nparam != null)
                    {
                        AddParameter(nparam);
                    }
                }
            }
        }

        /// <summary>
        /// Do not use this method. Use Remove(NhsUrlParam param)"
        /// </summary>
        /// <param name="paramName">Name of the param.</param>
        /// <returns></returns>
        public NhsUrl RemoveParameter(string paramName)
        {
            return Remove(new NhsUrlParam { DefaultName = paramName });
        }

        /// <summary>
        /// Removes the specified param.
        /// </summary>
        /// <param name="param">The param.</param>
        public NhsUrl Remove(NhsUrlParam param)
        {
            _parameters.Remove(param);
            return this;
        }


        /// <summary>
        /// Do not use this method. Use NhsUrl(function).Redirect()
        /// </summary>
        /// <param name="function">The function.</param>
        public void Redirect(string function)
        {
            new NhsUrl(function).Redirect();
        }

        /// <summary>
        /// Redirects to the NhsUrl.
        /// </summary>
        public void Redirect()
        {
            HttpContext.Current.Response.Redirect("~/" + this);
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            try
            {
                //Process current url and split to correct types.
                if (PersistParams)
                {
                    if (HttpContext.Current != null)
                    {
                        var currentUrl = (NhsUrl)HttpContext.Current.Items[RewriterConfiguration.ContextItemsCurrentUrl];
                        if (currentUrl != null)
                        {
                            List<NhsUrlParam> currentParams = currentUrl.Parameters.ToList();

                            if (Parameters.Count > 0)
                            {
                                currentParams.AddRange(Parameters);
                            }

                            SplitNhsUrlParamsToUrlComponents(currentParams);
                        }
                    }
                }
                //Traverse through UrlParam collection and add to splits(querystring,friendlyparams & hashedParams strings)
                else if (Parameters.Count > 0)
                {
                    SplitNhsUrlParamsToUrlComponents(Parameters);
                }

                Function = Function.Trim('/');

                //Build Target Url - DO NOT CHANGE SEQUENCE
                var pageName = string.IsNullOrEmpty(PageName) ? string.Empty : "/" + PageName + "/";
                var hashCode = string.IsNullOrEmpty(HashCodePart) ? string.Empty : "/" + HashCodePart + "/";
                string targetUrl = string.Format("/{0}/{1}/{2}{3}{4}{5}", RewriterConfiguration.CurrentPartnerSiteUrl, Function, FriendlyParameters,
                    pageName, hashCode, QueryParameters).Trim('/');
                //remove multiple /, ie // to / or /// to / etc. 
                targetUrl = new Regex("/+").Replace(targetUrl, "/");
                return targetUrl.Replace("/?", "?").ToLower();
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        #endregion

        #region "Private Methods"



        /// <summary>
        /// Splits the NHS URL params to URL components.
        /// Sets _queryParameterPart, _hashedParameterPart, _friendlyParameterPart
        /// </summary>
        /// <param name="urlParams">The URL params.</param>
        private void SplitNhsUrlParamsToUrlComponents(IList<NhsUrlParam> urlParams)
        {
            //#21277 add to params only if not any of the following ones

            var disticntParams = urlParams.
                GroupBy(x => x.DefaultName).
                Select(x => x.First());


            _queryParameterPart = _hashedParameterPart = _friendlyParameterPart = string.Empty;

            foreach (NhsUrlParam p in disticntParams)
            {
                if (string.IsNullOrEmpty(p.Value)) continue;

                //while tostring'ing we do not want all the aliases 
                //just the current alias used for the param is enough
                if (this.Function.IndexOf("requestinfo", StringComparison.InvariantCultureIgnoreCase) != -1)
                {
                    p.ParamType = RouteParamType.QueryString;
                }
                if (p.AliasUsed.IndexOf(UrlConst.NextPage, StringComparison.InvariantCultureIgnoreCase) != -1)
                {
                    p.ParamType = RouteParamType.QueryString;
                }
                switch (p.ParamType)
                {
                    case RouteParamType.Friendly:

                        _friendlyParameterPart += string.Format("/{0}{1}{2}", p.AliasUsed ?? p.DefaultName,
                                                                RewriterConfiguration.FriendlyUrlDelimiter,
                                                                p.Value);
                        break;
                    case RouteParamType.Hash:
                        _hashedParameterPart += string.Format("&{0}={1}", p.AliasUsed ?? p.DefaultName,
                                                              HttpUtility.UrlEncode(p.Value));
                        break;
                    case RouteParamType.QueryString:
                        _queryParameterPart += string.Format("&{0}={1}", p.AliasUsed ?? p.DefaultName,
                                                             HttpUtility.UrlEncode(p.Value));
                        break;
                }

            }

            //clean Querystring
            _queryParameterPart = _queryParameterPart == string.Empty
                                      ? _queryParameterPart
                                      :
                                          (PageName != null && PageName.Contains("?")) ? "&" : "?" + _queryParameterPart.TrimStart('&');

            //clean Hash
            _hashedParameterPart = _hashedParameterPart.Trim(new[] { '&', '/' }).EncodeBase64();

            //clean Friendly Params
            _friendlyParameterPart = _friendlyParameterPart.Trim('/');
        }

        #endregion



        public void ClearParameters()
        {
            if (Parameters != null) Parameters.Clear();
        }


        #region Corresponding helper Getters for UrlConst

        public static string GetCanonicalString(List<string> paramz, bool excludeTheseParams)
        {
            if (paramz == null)
                paramz = new List<string>();

            string currentUrl = RewriterConfiguration.DomainNamePro.AddTrailingSlash();

            List<string> currParams = new List<string>(HttpContext.Current.Request.RawUrl.StripLeadingEndingChar('/').Split('/'));
            List<string> crappyParams = new List<string> { "refer-", "logevent-", "searchtype-" };

            List<string> matches = new List<string>();
            var siteurlAndFunction = from m in currParams
                                     where !m.Contains("-")
                                     select m;
            matches.AddRange(siteurlAndFunction); //we need to keep these for all

            foreach (string param in paramz)
            {
                string tmpParam = param;
                if (!tmpParam.EndsWith("-"))
                    tmpParam += "-";

                if (excludeTheseParams)
                    currParams.RemoveAll(value => value.Contains(tmpParam));
                else
                {
                    var p = currParams.Where(value => value.Contains(tmpParam)).FirstOrDefault();
                    if (p != null)
                        matches.Add(p);
                }
            }

            if (!excludeTheseParams)
                currParams.RemoveAll(x => (currParams.Except(matches)).Contains(x));

            foreach (string crappyParam in crappyParams) //remove crappy params
                currParams.RemoveAll(x => x.Contains(crappyParam));

            string partnerSiteUrl = RewriterConfiguration.CurrentPartnerSiteUrl; //do not include partner site urls
            var cmprer = StringComparison.InvariantCultureIgnoreCase;
            foreach (string currParam in currParams)
                if ((string.IsNullOrEmpty(partnerSiteUrl) || currParam.IndexOf(partnerSiteUrl, cmprer) < 0))
                    currentUrl += currParam.AddTrailingSlash();

            currentUrl = currentUrl.StripLeadingEndingChar('/');
            return currentUrl;
        }

        public static string GetSearchPage { get { return GetUrlConstValueFromUrlParam(UrlConst.SearchPage); } }
        public static string GetState { get { return GetUrlConstValueFromUrlParam(UrlConst.State); } }
        public static string GetStateName { get { return GetUrlConstValueFromUrlParam(UrlConst.StateName); } }
        public static int GetMarketID { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.MarketID)); } }
        /// <summary>
        /// This method is deprecated. Please use RouteParams.MarketId.ToType<int>()
        /// </summary>
        public static int GetFeaturedListingId { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.FeaturedListingId)); } }
        public static string GetMarketName { get { return GetUrlConstValueFromUrlParam(UrlConst.MarketName); } }

        public static string GetCity { get { return GetUrlConstValueFromUrlParam(UrlConst.City); } }
        public static int GetCityId { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.CityId)); } }
        public static string GetCityNameFilter { get { return GetUrlConstValueFromUrlParam(UrlConst.CityNameFilter); } }
        public static string GetPostalCode { get { return GetUrlConstValueFromUrlParam(UrlConst.PostalCode); } }
        public static string GetPostalCodeFilter { get { return GetUrlConstValueFromUrlParam(UrlConst.PostalCodeFilter); } }
        public static int GetCommunityID { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.CommunityID)); } }
        public static int GetBuilderID { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.BuilderID)); } }
        public static string GetBuilderName { get { return GetUrlConstValueFromUrlParam(UrlConst.BuilderName); } }
        public static int GetBrandId { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.BrandId)); } }
        public static string GetBrandName { get { return GetUrlConstValueFromUrlParam(UrlConst.BrandName); } }
        public static int GetSchoolDistrictId { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.SchoolDistrictId)); } }
        public static string GetSchoolDistrictName { get { return GetUrlConstValueFromUrlParam(UrlConst.SchoolDistrictName); } }
        public static string GetListingTypeFlag { get { return GetUrlConstValueFromUrlParam(UrlConst.ListingTypeFlag); } }

        public static string GetGated { get { return GetUrlConstValueFromUrlParam(UrlConst.Gated); } }
        public static string GetPool { get { return GetUrlConstValueFromUrlParam(UrlConst.Pool); } }
        public static string GetGolfCourse { get { return GetUrlConstValueFromUrlParam(UrlConst.GolfCourse); } }
        public static string GetGreenFeatures { get { return GetUrlConstValueFromUrlParam(UrlConst.GreenProgram); } }
        public static string GetNatureAreas { get { return GetUrlConstValueFromUrlParam(UrlConst.NatureAreas); } }
        public static string GetViews { get { return GetUrlConstValueFromUrlParam(UrlConst.Views); } }
        public static string GetWaterFront { get { return GetUrlConstValueFromUrlParam(UrlConst.WaterFront); } }
        public static string GetSportsFacilities { get { return GetUrlConstValueFromUrlParam(UrlConst.SportsFacilities); } }
        public static string GetParks { get { return GetUrlConstValueFromUrlParam(UrlConst.Parks); } }
        public static string GetAdult { get { return GetUrlConstValueFromUrlParam(UrlConst.Adult); } }

        public static int GetPriceHigh { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.PriceHigh)); } }
        public static int GetPriceLow { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.PriceLow)); } }
        public static int GetNumOfBeds { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.NumOfBeds)); } }
        public static int GetNumOfBaths { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.NumOfBaths)); } }
        public static string GetNumOfGarages { get { return GetUrlConstValueFromUrlParam(UrlConst.NumOfGarages); } }
        public static string GetNumOfLiving { get { return GetUrlConstValueFromUrlParam(UrlConst.NumOfLiving); } }

        public static string GetHomeType { get { return GetUrlConstValueFromUrlParam(UrlConst.HomeType); } }
        public static string GetStories { get { return GetUrlConstValueFromUrlParam(UrlConst.Stories); } }
        public static string GetMasterBedLocation { get { return GetUrlConstValueFromUrlParam(UrlConst.MasterBedLocation); } }
        public static int GetBrandID { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.BrandID)); } }
        public static string GetHomeStatus { get { return GetUrlConstValueFromUrlParam(UrlConst.HomeStatus); } }

        // Others
        public static int GetPartnerID { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.PartnerID)); } }
        public static int GetParentCommunityID { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.ParentCommunityID)); } }
        public static string GetCommunityList { get { return GetUrlConstValueFromUrlParam(UrlConst.CommunityList); } }
        public static string GetCommunityName { get { return GetUrlConstValueFromUrlParam(UrlConst.CommunityName); } }
        public static string GetSchoolDistrict { get { return GetUrlConstValueFromUrlParam(UrlConst.SchoolDistrict); } }
        public static int GetSpecID { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.SpecID)); } }
        public static string GetSpecList { get { return GetUrlConstValueFromUrlParam(UrlConst.SpecList); } }
        public static int GetListingID { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.ListingID)); } }
        public static int GetPlanID { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.PlanID)); } }
        public static string GetPlanList { get { return GetUrlConstValueFromUrlParam(UrlConst.PlanList); } }
        public static string GetCityList { get { return GetUrlConstValueFromUrlParam(UrlConst.CityList); } }
        public static int GetPriceRangeID { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.PriceRangeID)); } }
        public static string GetSearchSeed { get { return GetUrlConstValueFromUrlParam(UrlConst.SearchSeed); } }
        public static string GetAvailability { get { return GetUrlConstValueFromUrlParam(UrlConst.Availability); } }
        public static string GetSearchType { get { return GetUrlConstValueFromUrlParam(UrlConst.SearchType); } }
        public static string GetLeadType { get { return GetUrlConstValueFromUrlParam(UrlConst.LeadType); } }
        public static string GetLeadAction { get { return GetUrlConstValueFromUrlParam(UrlConst.LeadAction); } }
        public static string GetLeadComments { get { return GetUrlConstValueFromUrlParam(UrlConst.LeadComments); } }
        public static string GetRegistration { get { return GetUrlConstValueFromUrlParam(UrlConst.Registration); } }
        public static string GetFromPage { get { return GetUrlConstValueFromUrlParam(UrlConst.FromPage); } }
        public static string GetBuilderList { get { return GetUrlConstValueFromUrlParam(UrlConst.BuilderList); } }
        public static string GetBrandList { get { return GetUrlConstValueFromUrlParam(UrlConst.BrandList); } }
        public static int GetCampaignID { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.CampaignID)); } }
        public static int GetOptionID { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.OptionID)); } }
        public static string GetAdultCommunity { get { return GetUrlConstValueFromUrlParam(UrlConst.AdultCommunity); } }
        public static string GetSortBy { get { return GetUrlConstValueFromUrlParam(UrlConst.SortBy); } }
        public static string GetPageNumber { get { return GetUrlConstValueFromUrlParam(UrlConst.PageNumber); } }
        public static string GetRecordCount { get { return GetUrlConstValueFromUrlParam(UrlConst.RecordCount); } }
        public static string GetSortOrder { get { return GetUrlConstValueFromUrlParam(UrlConst.SortOrder); } }
        public static string GetCached { get { return GetUrlConstValueFromUrlParam(UrlConst.Cached); } }
        public static string GetRefer { get { return GetUrlConstValueFromUrlParam(UrlConst.Refer); } }
        public static string GetExternalURL { get { return GetUrlConstValueFromUrlParam(UrlConst.ExternalURL); } }
        public static string GetLogEvent { get { return GetUrlConstValueFromUrlParam(UrlConst.LogEvent); } }
        public static string Gettype { get { return GetUrlConstValueFromUrlParam(UrlConst.Type); } }
        public static string GetCommunityTabs { get { return GetUrlConstValueFromUrlParam(UrlConst.CommunityTabs); } }
        public static string GetHomeTabs { get { return GetUrlConstValueFromUrlParam(UrlConst.HomeTabs); } }
        public static string GetPlannerTabs { get { return GetUrlConstValueFromUrlParam(UrlConst.PlannerTabs); } }
        public static string GetRegisterPasswordOnly { get { return GetUrlConstValueFromUrlParam(UrlConst.RegisterPasswordOnly); } }
        public static string GetNextPage { get { return GetUrlConstValueFromUrlParam(UrlConst.NextPage); } }
        public static string GetNextPageName { get { return GetUrlConstValueFromUrlParam(UrlConst.NextPageName); } }
        public static string GetHideShell { get { return GetUrlConstValueFromUrlParam(UrlConst.HideShell); } }
        public static string GetFirstName { get { return GetUrlConstValueFromUrlParam(UrlConst.FirstName); } }
        public static int GetRequestID { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.RequestID)); } }
        public static int GetPromoID { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.PromoID)); } }
        public static string GetCampaign { get { return GetUrlConstValueFromUrlParam(UrlConst.Campaign); } }
        public static string GetSignInOption { get { return GetUrlConstValueFromUrlParam(UrlConst.SignInOption); } }
        public static string GetEmail { get { return GetUrlConstValueFromUrlParam(UrlConst.Email); } }
        public static string GetUserNameAndLastName { get { return GetUrlConstValueFromUrlParam(UrlConst.UserNameAndLastName); } }
        public static string GetSearchRange { get { return GetUrlConstValueFromUrlParam(UrlConst.SearchRange); } }
        public static string GetPhoneNumber { get { return GetUrlConstValueFromUrlParam(UrlConst.PhoneNumber); } }
        public static string GetUserZip { get { return GetUrlConstValueFromUrlParam(UrlConst.UserZip); } }
        public static string GetDisplayMessage { get { return GetUrlConstValueFromUrlParam(UrlConst.DisplayMessage); } }
        public static string GetFriendName { get { return GetUrlConstValueFromUrlParam(UrlConst.FriendName); } }
        public static string GetFromName { get { return GetUrlConstValueFromUrlParam(UrlConst.FromName); } }
        public static string GetNote { get { return GetUrlConstValueFromUrlParam(UrlConst.Note); } }
        public static string GetMoveInDate { get { return GetUrlConstValueFromUrlParam(UrlConst.MoveInDate); } }
        public static string GetFinancialPref { get { return GetUrlConstValueFromUrlParam(UrlConst.FinancialPref); } }
        public static string GetMoveReason { get { return GetUrlConstValueFromUrlParam(UrlConst.MoveReason); } }
        public static string GetCloseWindow { get { return GetUrlConstValueFromUrlParam(UrlConst.CloseWindow); } }
        public static string GetLeadSent { get { return GetUrlConstValueFromUrlParam(UrlConst.LeadSent); } }
        public static string GetRegFromLeads { get { return GetUrlConstValueFromUrlParam(UrlConst.RegFromLeads); } }
        public static string GetUserId { get { return (GetUrlConstValueFromUrlParam(UrlConst.UserId)); } }
        public static string GetDeactivate { get { return GetUrlConstValueFromUrlParam(UrlConst.Deactivate); } }
        public static string GetComingSoon { get { return GetUrlConstValueFromUrlParam(UrlConst.ComingSoon); } }
        public static string GetComingSoonCount { get { return GetUrlConstValueFromUrlParam(UrlConst.ComingSoonCount); } }
        public static int GetAdvertiserID { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.AdvertiserID)); } }
        public static string GetBuilderCommunityIds { get { return GetUrlConstValueFromUrlParam(UrlConst.BuilderCommunityIds); } }
        public static string GetToPage { get { return GetUrlConstValueFromUrlParam(UrlConst.ToPage); } }
        public static string GetLeadsGenerated { get { return GetUrlConstValueFromUrlParam(UrlConst.LeadsGenerated); } }
        public static int GetPresentationID { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.PresentationID)); } }
        public static string GetConfirmation { get { return GetUrlConstValueFromUrlParam(UrlConst.Confirmation); } }
        public static string GetAgeRestricted { get { return GetUrlConstValueFromUrlParam(UrlConst.AgeRestricted); } }
        public static string GetShowMediaTab { get { return GetUrlConstValueFromUrlParam(UrlConst.ShowMediaTab); } }
        public static string GetShowAmenitiesTab { get { return GetUrlConstValueFromUrlParam(UrlConst.ShowAmenitiesTab); } }
        public static string GetShowViewHomesTab { get { return GetUrlConstValueFromUrlParam(UrlConst.ShowViewHomesTab); } }
        public static string GetRedirectUrl { get { return GetUrlConstValueFromUrlParam(UrlConst.RedirectUrl); } }
        public static string GetCategory { get { return GetUrlConstValueFromUrlParam(UrlConst.Category); } }
        public static string GetSubCategory { get { return GetUrlConstValueFromUrlParam(UrlConst.SubCategory); } }
        public static int GetCategoryId { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.CategoryId)); } }
        public static int GetSubCategoryId { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.SubCategoryId)); } }
        public static int GetArticleId { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.ArticleId)); } }
        public static int GetMinLat { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.MinLat)); } }
        public static int GetMinLng { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.MinLng)); } }
        public static int GetMaxLat { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.MaxLat)); } }
        public static int GetMaxLng { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.MaxLng)); } }
        public static int GetZoomLevel { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.ZoomLevel)); } }
        public static string GetArticle { get { return GetUrlConstValueFromUrlParam(UrlConst.Article); } }
        public static string GetSearchText { get { return GetUrlConstValueFromUrlParam(UrlConst.SearchText); } }
        public static string GetMoreResultsFlag { get { return GetUrlConstValueFromUrlParam(UrlConst.MoreResultsFlag); } }
        public static string GetMessage { get { return GetUrlConstValueFromUrlParam(UrlConst.Message); } }
        public static string GetAutoRedirect { get { return GetUrlConstValueFromUrlParam(UrlConst.AutoRedirect); } }
        public static string GetShowAds { get { return GetUrlConstValueFromUrlParam(UrlConst.ShowAds); } }
        public static string GetAddToProfile { get { return GetUrlConstValueFromUrlParam(UrlConst.AddToProfile); } }
        public static string GetAmenity { get { return GetUrlConstValueFromUrlParam(UrlConst.AmenityType); } }
        public static string GetWebId { get { return GetUrlConstValueFromUrlParam(UrlConst.WebId); } }
        public static string GetSupportEmail { get { return GetUrlConstValueFromUrlParam(UrlConst.SupportEmail); } }
        public static string GetFriendEmail { get { return GetUrlConstValueFromUrlParam(UrlConst.FriendEmail); } }
        public static string GetSpecialOfferComm { get { return GetUrlConstValueFromUrlParam(UrlConst.SpecialOfferComm); } }
        public static string GetSpecialOfferListing { get { return GetUrlConstValueFromUrlParam(UrlConst.SpecialOfferListing); } }
        public static string GetGreenProgram { get { return GetUrlConstValueFromUrlParam(UrlConst.GreenProgram); } }
        public static string GetCountyNameFilter { get { return GetUrlConstValueFromUrlParam(UrlConst.CountyNameFilter); } }
        public static int GetBedrooms { get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.Bedrooms)); } }
        public static string GetAlertSelected { get { return GetUrlConstValueFromUrlParam(UrlConst.AlertSelected); } }
        public static string GetRequestItemId { get { return GetUrlConstValueFromUrlParam(UrlConst.RequestItemId); } }
        public static string GetHUrl { get { return GetUrlConstValueFromUrlParam(UrlConst.HUrl); } }

        public static bool GetOnDemandPdf
        {
            get
            {
                try
                {
                    return bool.Parse(GetUrlConstValueFromUrlParam(UrlConst.OnDemandPdf));
                }
                catch
                {
                    return false;
                }
            }
        }

        public static bool NewSearchRCC
        {
            get
            {
                try
                {
                    return bool.Parse(GetUrlConstValueFromUrlParam(UrlConst.NewSearchRCC));
                }
                catch
                {
                    return false;
                }
            }
        }


        public static bool ShowUserPhoneNumber
        {
            get
            {
                try
                {
                    return bool.Parse(GetUrlConstValueFromUrlParam(UrlConst.ShowUserPhoneNumber));
                }
                catch
                {
                    return false;
                }
            }
        }

        public static bool GetNewLead
        {
            get
            {
                try
                {
                    return bool.Parse(GetUrlConstValueFromUrlParam(UrlConst.NewLead));
                }
                catch
                {
                    return false;
                }
            }
        }

        public static bool GetUnCheckSMH
        {
            get
            {
                try
                {
                    return bool.Parse(GetUrlConstValueFromUrlParam(UrlConst.UnCheckSMH));
                }
                catch
                {
                    return false;
                }
            }
        }

        public static bool GetOtherContent
        {
            get
            {
                try
                {
                    return bool.Parse(GetUrlConstValueFromUrlParam(UrlConst.OtherContent));
                }
                catch
                {
                    return false;
                }
            }
        }

        public static bool GetClientRedirect
        {
            get
            {
                try
                {
                    return bool.Parse(GetUrlConstValueFromUrlParam(UrlConst.ClientRedirect));
                }
                catch
                {
                    return false;
                }
            }
        }

        public static bool GetUseSession
        {
            get
            {
                try
                {
                    return bool.Parse(GetUrlConstValueFromUrlParam(UrlConst.UseSession));
                }
                catch
                {
                    return false;
                }

            }
        }
        public static bool GetPreviewShell
        {
            get
            {
                return GetUrlConstValueFromUrlParam(UrlConst.PreviewShell).ToLower() == "y" ? true : false;
            }
        }
        public static string GetFormName
        {
            get { return GetUrlConstValueFromUrlParam(UrlConst.FormName); }
        }
        public static string GetPopupWindow
        {
            get { return GetUrlConstValueFromUrlParam(UrlConst.PopupWindow); }
        }
        public static string GetAlertCreated
        {
            get { return GetUrlConstValueFromUrlParam(UrlConst.AlertCreated); }
        }
        public static int GetAlertId
        {
            get { return ConvertToInt32(GetUrlConstValueFromUrlParam(UrlConst.AlertId)); }
        }
        public static string GetUserAlreadyExists { get { return GetUrlConstValueFromUrlParam(UrlConst.UserAlreadyExists); } }
        public static string GetChangedToActiveUser { get { return GetUrlConstValueFromUrlParam(UrlConst.ChangedToActiveUser); } }
        public static string GetSearchLocation { get { return GetUrlConstValueFromUrlParam(UrlConst.SearchLocation); } }

        public bool IsInactivePartnerOrNotAPrivateLabel { get; set; }

        #endregion

        private static int ConvertToInt32(string p)
        {
            return p.ToType<int>();
        }

        public static string GetFunction
        {
            get
            {
                var nUrl = NhsUrl.GetFriendlyUrlFrom(HttpContext.Current);
                return string.IsNullOrEmpty(nUrl.Function) ? "" : nUrl.Function;
            }
        }

        private static string GetUrlConstValueFromUrlParam(string propertyName)
        {
            var nUrl = NhsUrl.GetFriendlyUrlFrom(HttpContext.Current);

            if (nUrl == null)
            {
                if (HttpContext.Current.Request.QueryString[propertyName] != null)
                    return HttpContext.Current.Request.QueryString[propertyName].ToString();
                return string.Empty;
            }
            else
            {
                if (propertyName == UrlConst.RedirectUrl)
                {
                    string urlParam = nUrl.ToString();
                    if (urlParam.IndexOf("url=") != -1)
                    {
                        return HttpUtility.UrlDecode(urlParam.Substring(urlParam.IndexOf("url=") + 4));
                    }
                    else
                        return string.Empty;
                }
                else
                {
                    var parameter = nUrl.FindParam(propertyName); //#19747
                    if (parameter != null) return parameter.Value;
                    //Was add becase some times this function:  var nUrl = NhsUrl.GetFriendlyUrlFrom(HttpContext.Current);
                    //was not returning a parameter list even if the url had parameters
                    else if (HttpContext.Current.Request.QueryString[propertyName] != null)
                        return HttpContext.Current.Request.QueryString[propertyName].ToString();
                    return string.Empty;
                }
            }


        }

        public class UrlConst
        {
            public const string ClientRedirect = "clientredirect";
            // Please Update enum NhsLinkParams for intellisense with nhs:link
            // Please Update Getters in NhsUrl too
            #region Members
            // Property Search
            public const string State = "state";
            public const string StateName = "statename";
            public const string MarketID = "market";
            public const string MarketName = "area";
            public const string City = "city";
            public const string CityId = "cityid";
            public const string CityNameFilter = "citynamefilter";
            public const string PostalCode = "postalcode";
            public const string PostalCodeFilter = "postalcodefilter";
            public const string County = "county";
            public const string CountyNameFilter = "countynamefilter";
            public const string CommunityID = "community";
            public const string BuilderID = "builder";
            public const string BuilderName = "buildername";
            public const string BrandId = "brandid";
            public const string BrandName = "brandname";
            public const string SchoolDistrictId = "schooldistrictid";
            public const string SchoolDistrictName = "schooldistrictname";
            public const string ListingTypeFlag = "listingtype";

            public const string Gated = "gated";
            public const string Pool = "pool";
            public const string GolfCourse = "golfcourse";
            public const string NatureAreas = "natureareas";
            public const string Views = "views";
            public const string WaterFront = "waterfront";
            public const string SportsFacilities = "sportsfacility";
            public const string Parks = "parks";
            public const string Adult = "adult";

            public const string PriceHigh = "pricehigh";
            public const string PriceLow = "pricelow";
            public const string NumOfBeds = "bedrooms";
            public const string NumOfBaths = "bathrooms";
            public const string NumOfGarages = "garages";
            public const string NumOfLiving = "livingareas";
            public const string SearchRange = "range";
            public const string PhoneNumber = "phone";


            public const string HomeType = "hometype";
            public const string Stories = "stories";
            public const string MasterBedLocation = "masterbedlocation";
            public const string BrandID = "brandid";
            public const string HomeStatus = "homestatus";

            public const string OriginLat = "originlat";
            public const string OriginLong = "originlong";

            // Others
            public const string PartnerID = "partnerid";
            public const string ParentCommunityID = "parentcommunity";
            public const string CommunityList = "communitylist";
            public const string CommunityName = "communityname";
            public const string SchoolDistrict = "schooldistrict";
            public const string SpecID = "specid";
            public const string SpecList = "speclist";
            public const string ListingID = "listingid";
            public const string PlanID = "planid";
            public const string PlanList = "planlist";
            public const string CityList = "citylist";
            public const string PriceRangeID = "pricerangeid";
            public const string SearchSeed = "searchseed";
            public const string Availability = "availability";
            public const string SearchType = "searchtype";
            public const string LeadType = "leadtype";
            public const string LeadAction = "leadaction";
            public const string LeadComments = "leadcomments";
            public const string Registration = "registration";
            public const string AlertCreated = "alertcreated";
            public const string AlertId = "alertid";
            public const string FromPage = "frompage";
            public const string BuilderList = "builderlist";
            public const string BrandList = "brandlist";
            public const string CampaignID = "campaignid";
            public const string OptionID = "optionid";
            public const string AdultCommunity = "adultcommunity";
            public const string SortBy = "sort";
            public const string PageNumber = "pagenumber";
            public const string RecordCount = "recordcount";
            public const string SortOrder = "sortorder";
            public const string Cached = "cached";
            public const string Refer = "refer";
            public const string ExternalURL = "externalurl";
            public const string LogEvent = "logevent";
            public const string EventCode = "eventcode";
            public const string AmenityType = "amenity"; // amenity type param for amenity index
            public const string Type = "type";//Image Type
            public const string CommunityTabs = "view";//Image Type
            public const string PlannerTabs = "view"; //Image Type
            public const string HomeTabs = "view";  //currently communityTabs and HomeTabs have same param name view
            public const string RegisterPasswordOnly = "registerpasswordonly";
            public const string NextPage = "nextpage";
            public const string NextPageName = "nextpagename";
            public const string HideShell = "hideshell";
            public const string FirstName = "firstname";
            public const string RequestID = "requestid";
            public const string PromoID = "promoid";
            public const string Campaign = "Campaign";
            public const string SignInOption = "signinoption";
            public const string Email = "email";
            public const string DisplayMessage = "displaymessage";
            public const string FriendName = "friendname";
            public const string FromName = "fromname";
            public const string Note = "leadcomments";
            public const string MoveInDate = "moveindate";
            public const string FinancialPref = "financialpref";
            public const string MoveReason = "movereason";
            public const string CloseWindow = "closewindow";
            public const string LeadSent = "leadsent";
            public const string RegFromLeads = "regfromleads";
            public const string UserId = "userid"; //changed from guid to userid.
            public const string Deactivate = "deactivate";
            public const string ComingSoon = "comingsoon";
            public const string ComingSoonCount = "comingsooncount";
            public const string AdvertiserID = "advertiserid";
            public const string BuilderCommunityIds = "buildercommunitylist";
            public const string ToPage = "topage";
            public const string LeadsGenerated = "leadsgenerated";
            public const string PresentationID = "presentationid";
            public const string Confirmation = "confirmation";
            public const string AgeRestricted = "agerestricted";
            public const string ShowMediaTab = "showmediatab";
            public const string ShowAmenitiesTab = "showamenitiestab";
            public const string ShowViewHomesTab = "showviewhomestab";
            public const string RedirectUrl = "url";
            public const string Category = "category";
            public const string SubCategory = "subcategory";
            public const string CategoryId = "categoryid";
            public const string SubCategoryId = "subcategoryid";
            public const string ArticleId = "articleid";
            public const string Article = "article";
            public const string SearchText = "searchtext";
            public const string SearchPage = "searchpage";
            public const string SessionId = "sessionid";
            public const string MoreResultsFlag = "moreresultsflag";
            public const string ZoomLevel = "zoom";
            public const string MinLat = "minlat";
            public const string MinLng = "minlng";
            public const string MaxLat = "maxlat";
            public const string MaxLng = "maxlng";
            public const string UseSession = "usesession";
            public const string Message = "message";
            public const string AutoRedirect = "autoredirect";
            public const string ShowAds = "showads";
            //used in community detail and home detail to save listing
            //to planner
            public const string AddToProfile = "addtoprofile";
            // Please Update enum NhsLinkParams for intellisense with nhs:link
            // Please Update Getters in NhsUrl too

            public const string PreviewShell = "preview"; //to preview site from extranet mgmt tool
            public const string SpecialOfferComm = "specialofferc";
            public const string SpecialOfferListing = "specialofferl";
            public const string HotHomeComm = "hothomec";
            public const string HotHomeListing = "hothomel";
            public const string WebId = "webid";
            public const string FormName = "formname";
            public const string PopupWindow = "popupwindow";
            public const string SupportEmail = "supportemail";
            public const string UserAlreadyExists = "useralreadyexists";
            public const string FriendEmail = "friendemail";
            public const string ChangedToActiveUser = "changedtoactiveuser";
            public const string SearchLocation = "searchlocation";

            public const string ErrorStatusCode = "statuscode";
            public const string GreenProgram = "green";
            public const string FeaturedListingId = "featuredlisting";
            public const string Bedrooms = "bedrooms";
            public const string UserNameAndLastName = "name";
            public const string UserZip = "userzip";

            public const string NewLead = "newlead";
            public const string NotAddScript = "notaddscript";
            public const string AlertSelected = "AlertSelected";
            public const string OtherContent = "OtherContent";
            public const string UnCheckSMH = "UnCheckSMH";
            public const string NewSearchRCC = "NewSearchRCC";
            public const string ShowUserPhoneNumber = "ShowUserPhoneNumber";
            public const string RequestItemId = "requestitemid";
            public const string OnDemandPdf = "ondemandpdf";
            public const string HUrl = "HUrl";
            public const string InventoryHomes = "inventory-homes";
            public const string SpecHomes = "spec-homes";
            public const string Height = "height";

            
            #endregion

            #region Helper Methods
            //used in NHSLink Params to convert UrlParamNames enum to actual UrlParameter  values
            public static string GetFieldValue(string propertyName)
            {
                return typeof(UrlConst).GetField(propertyName).GetValue(null) as string;
            }



            #endregion

        }
    }
    [Serializable]
    public class NhsUrlParamList : List<NhsUrlParam>
    {

        /// <summary>
        /// Do not use this method. Use NhsUrl.FindParam(string paramName)
        /// </summary>
        /// <value></value>
        public NhsUrlParam this[string paramName]
        {
            get { return Find(x => (x.DefaultName.ToLowerInvariant() == paramName.ToLowerInvariant() || x.AliasUsed.ToLowerInvariant() == paramName.ToLowerInvariant())); }

            set { Insert(FindIndex((x => x.DefaultName.ToLowerInvariant() == paramName.ToLowerInvariant())), value); }
        }

        public bool Contains(string paramName)
        {
            return this.Any(param => param.DefaultName == paramName);
        }
    }
}
