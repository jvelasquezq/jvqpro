using System;
using Nhs.UrlRewriter.Configuration;
using Nhs.UrlRewriter.UrlMapping;
using Nhs.Utility.Common;

namespace Nhs.UrlRewriter.UrlGeneration
{
    internal static class NhsUrlFunctionMap
    {
        /// <summary>
        /// Gets Function Information - Sets Control Location
        /// </summary>
        internal static string GetControlLocation(string functionName, out string mappingConfigFileUsed, IPathMapper pathMapper)
        {
            mappingConfigFileUsed = string.Empty;
            NhsUrlMapper nhsUrlMapper = new NhsUrlMapper(pathMapper);
            string value = nhsUrlMapper.GetValueForKey("FunctionMaps", "Function", "ControlPath", functionName, out mappingConfigFileUsed);
            return value;
        }

        internal static string GetDefaultName(string functionName, IPathMapper pathMapper)
        {
            NhsUrlMapper nhsUrlMapper = new NhsUrlMapper(pathMapper);
            return nhsUrlMapper.GetValueForKey("FunctionMaps", "Function", "DefaultName", functionName);
        }

        internal static bool IsFunction(string functionName, IPathMapper pathMapper)
        {
            NhsUrlMapper nhsUrlMapper = new NhsUrlMapper(pathMapper);
            string valueForFunction = nhsUrlMapper.GetValueForKey("FunctionMaps", "Function", "DefaultName", functionName);

            if (valueForFunction.Length > 0 || functionName.Length == 0)
                return true;
            return false;
        }

        internal static string GetAdPageName(string functionName, IPathMapper pathMapper)
        {
            NhsUrlMapper nhsUrlMapper = new NhsUrlMapper(pathMapper);
            return nhsUrlMapper.GetValueForKey("FunctionMaps", "Function", "AdPageName", functionName);
        }

        internal static bool UseMasterPage(string functionName, IPathMapper pathMapper)
        {
            NhsUrlMapper nhsUrlMapper = new NhsUrlMapper(pathMapper);
            string useMasterPage = nhsUrlMapper.GetValueForKey("FunctionMaps", "Function", "usemasterpage", functionName);
            if (string.IsNullOrEmpty(useMasterPage))
            {
                return true;
            }
            return Convert.ToBoolean(useMasterPage);
        }

        internal static bool EnableTheme(string functionName, IPathMapper pathMapper)
        {
            NhsUrlMapper nhsUrlMapper = new NhsUrlMapper(pathMapper);
            string enableTheme = nhsUrlMapper.GetValueForKey("FunctionMaps", "Function", "enabletheme", functionName);
            if (string.IsNullOrEmpty(enableTheme))
            {
                return true;
            }
            return Convert.ToBoolean(enableTheme);
        }

        internal static string MasterPageFile(string functionName, IPathMapper pathMapper)
        {
            NhsUrlMapper nhsUrlMapper = new NhsUrlMapper(pathMapper);
            string masterPageFile = nhsUrlMapper.GetValueForKey("FunctionMaps", "Function", "masterpagefile", functionName);
            if (string.IsNullOrEmpty(masterPageFile))
            {

                return UrlMappingConfiguration.DefaultMasterPage;
            }
            return masterPageFile;
        }

        internal static string Theme(string functionName, IPathMapper pathMapper)
        {
            NhsUrlMapper nhsUrlMapper = new NhsUrlMapper(pathMapper);
            string theme = nhsUrlMapper.GetValueForKey("FunctionMaps", "Function", "theme", functionName);
            return (theme == null ? string.Empty : theme);
        }
    }
}
