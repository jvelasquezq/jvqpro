﻿/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright © 2001-2010 Builders Digital Experience, LLC.
 * ========================================================
 * Author: Rajiv Menon
 * Date:3/29/2010
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.UrlRewriter.Configuration;
using Nhs.Library.Web;
using Nhs.UrlRewriter.Helpers;
using Nhs.UrlRewriter.Utils;
using Nhs.Utility.Common;
using System.Web.SessionState;

namespace Nhs.UrlRewriter
{
    public class RewriterModule : IHttpModule, IRequiresSessionState
    {
        private string _destinationUrl;

        #region IHttpModule Members

        public void Dispose()
        {
            //clean-up code here.
        }

        /// <summary>
        /// Bootstrap for adding HttpApp Event Handlers.
        /// </summary>
        /// <param name="application">httpapplication.</param>
        public void Init(HttpApplication application)
        {
            application.BeginRequest += OnBeginRequest;
            application.AcquireRequestState += AcquireRequestState;
        }

        #endregion

        /// <summary>
        /// Event handler forapplication beginrequest event registered in the IHttpModile.Init method
        /// This method calls RewriteUrl
        /// </summary>
        /// <param name="sender">HttpApplication.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnBeginRequest(object sender, EventArgs e)
        {
            var app = (HttpApplication)sender;
            var rawUrl = app.Request.Url.AbsolutePath.ToLowerInvariant();
            if (RewriterConfiguration.ExtensionsToIgnore.Any(rawUrl.Contains) && !rawUrl.Contains("sitemapindex.xml"))
                return;

            RewriteUrl(app);
            if (rawUrl.Contains("sitemapindex.xml"))
            {
                RedirectToSiteMapIndex();
            }
        }

        public void RedirectToSiteMapIndex()
        {
            if (NhsRoute.IsPartnerNhsPro)
            {
                var pathMapper = new ContextPathMapper();
                var siteMapsIndex = pathMapper.MapPath(RewriterConfiguration.SiteMapFolder + "/sitemapindex.xml");
                if (File.Exists(siteMapsIndex))
                {
                    var buffer = File.ReadAllBytes(siteMapsIndex);
                    HttpContext.Current.Response.AppendHeader("Content-Disposition", "inline;filename=sitemapindex.xml");
                    HttpContext.Current.Response.ContentType = "text/xml";
                    HttpContext.Current.Response.BinaryWrite(buffer);
                    HttpContext.Current.Response.End();
                }
            }
        }

        private void AcquireRequestState(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_destinationUrl)) return;
            
            var app = (HttpApplication)sender;
            UserSession.ReferrerUrl = app.Request.UrlReferrer != null ? app.Request.UrlReferrer.ToString() : string.Empty;
            UserSession.DestinationUrl = _destinationUrl;
        }

        private void RewriteUrl(HttpApplication app)
        {
            var nUrl = ConvertUrlToNhsUrl(app);


            try
            {
                if (!RewriterCookieManager.RedirectFromRefer)
                {
                    _destinationUrl = TrafficHelper.Validate(app.Request, app.Request.UrlReferrer, nUrl);

                    if (_destinationUrl != null && _destinationUrl != app.Request.Url.ToString())
                        app.Response.Redirect(_destinationUrl);
                }
                else
                {
                    RewriterCookieManager.DeleteCookie("301FromRefer");
                }
            }

            catch
            {
            }

            string absolutePath = app.Request.Url.AbsolutePath.ToLowerInvariant();
            string host = app.Request.Url.Host.ToLowerInvariant();
            string url = app.Request.Url.ToString().ToLowerInvariant();
            string query = app.Request.Url.Query;

            //Hack for aol until we have a permanent fix for partners that are both private labels and data feed
            if (RewriterConfiguration.CurrentPartnerSiteUrl.ToLower() == "aolrealestate")
            {
                //replace only path, query string etc might have partnersiteurl eg: logredirect urls
                var path = app.Request.Url.AbsolutePath;
                //using regex.replace so that we can do a case insensitive replacement without affecting the original case of the path
                var partnerBrandPath = Regex.Replace(path, RewriterConfiguration.CurrentPartnerSiteUrl, string.Empty, RegexOptions.IgnoreCase)
                                                  .Replace("//", "/");
                //add back the query info
                var partnerBrandPathNQuery = partnerBrandPath + app.Request.Url.Query;
                app.Response.RedirectPermanent(partnerBrandPathNQuery);
            }


            var isRouteAnMvcRoute = NhsRoute.IsRouteAnMvcRoute() &&  !IsCurrentRouteNonMvcHomePage(NhsRoute.CurrentRoute.Function);
            
            //PRO and PRO partners get old shitty home and community results pages
            if (nUrl.Function == "homeresults" && RewriterConfiguration.CurrentBrandPartnerId.ToType<int>() == RewriterConfiguration.PartnerIdPro)
                isRouteAnMvcRoute = false;


            if (nUrl.Function == string.Empty &
                absolutePath.Contains(".aspx")
                )
            {
                return;
            }

            if ((string.IsNullOrEmpty(nUrl.Function) || nUrl.Function == "invalidfunction") && !isRouteAnMvcRoute)
            {
                var redirectUrl =
                    Get404RedirectUrl("~/" + (RewriterConfiguration.CurrentPartnerSiteUrl), app);
                if (redirectUrl != null)
                {
                    app.Response.StatusCode = 404;
                    app.Response.Redirect(redirectUrl);
                }
                return;
            }
            
            //59608
            if (app.Request.HttpMethod == "GET")
                ApplyAnyUpperCaseRedirects(nUrl, isRouteAnMvcRoute, app);

            //Ignore all routes MVC
            if (isRouteAnMvcRoute)
            {
                RedirectNonMvcFormattedUrls(app, nUrl); //Redirect malformed urls
                return;
            }
            
            //46093
            if (app.Request.RawUrl.ToLower().Contains("?url="))
            {
                var newUrl = app.Request.RawUrl.ReplaceString("?url=", "?externalurl=", StringComparison.InvariantCultureIgnoreCase);
                app.Response.RedirectPermanent("~/" + newUrl);
            }

            //Rewrite non-mvc page
            app.Context.RewritePath(nUrl.UseMasterPage
                                        ? RewriterConfiguration.DefaultPageWithMasterPage
                                        : RewriterConfiguration.DefaultPageSansMasterPage);
        }

        private void ApplyAnyUpperCaseRedirects(NhsUrl nhsUrl, bool isRouteAnMvcRoute, HttpApplication app)
        {
            //If no uppercase character, don't process further
            var currentUrl = app.Request.Url.ToString();
            if (!string.IsNullOrEmpty(currentUrl) && !currentUrl.Any(char.IsUpper))
                return;

            var function = isRouteAnMvcRoute ? NhsRoute.CurrentRoute.Function : nhsUrl.Function;
            function = function != null ? function.ToLower() : string.Empty;

            switch (function)
            {
                case "communityresults":
                case "stateindex":
                case "homedetail":
                case "communitydetail":
                case "basicdetail":
                case "homeresults":
                case "new-home-builders":
                case "home-builders":
                case "new-home-builder":
                case "sitehelp":
                case "advancedsearch":
                case "quickconnect":
                case "greensearch":
                case "quickmoveinsearch":
                case "hotdealssearch":
                case "boylsearch":
                case "listyourhomes":
                case "unsubscribe":
                case "siteindex":
                case "aboutus":
                case "contactus":
                case "privacypolicy":
                case "termsofuse":
                    if (!string.IsNullOrEmpty(currentUrl) && app.Request.Url.LocalPath.Any(char.IsUpper))
                    {
                        var url = app.Request.Url.LocalPath.ToLowerInvariant();
                        if (app.Request.QueryString.Count > 0)
                        {
                            url += app.Request.Url.Query;
                        }
                        app.Response.RedirectPermanent(url);
                    }

                    break;
            }
        }

        private void RedirectNonMvcFormattedUrls(HttpApplication app, NhsUrl nUrl)
        {
            if (app.Request.HttpMethod == "GET")
            {
                var mvcUrl = nUrl.GetFormattedMvcUrl(false);

                if (!string.IsNullOrEmpty(mvcUrl))
                {
                    app.Response.RedirectPermanent("~/" + mvcUrl);
                }
            }
        }

        private bool IsCurrentRouteNonMvcHomePage(string functionName)
        {
            if (!string.IsNullOrEmpty(functionName) && functionName.ToLower() != "home") //not a home page
                return false;
            
            //hack for las vegas review journal, non mvc for now
            if (RewriterConfiguration.PartnerId.ToType<int>() == 16)
                return true;

            return false; //All other partners are MVC
        }

        private string Get404RedirectUrl(string prefix, HttpApplication app)
        {
            string absolutePath = app.Request.Url.AbsolutePath.ToLowerInvariant();
            bool existRewriterRule = NhsRewriterRoutes.CheckIfRewriterRuleExist(absolutePath);


            System.Configuration.Configuration configuration = WebConfigurationManager.OpenWebConfiguration("~");
            var section = (CustomErrorsSection)configuration.GetSection("system.web/customErrors");
            if ((section.Mode == CustomErrorsMode.On || (section.Mode == CustomErrorsMode.RemoteOnly 
                && app.Request.UserHostAddress != "127.0.0.1")) && !existRewriterRule)
            {
                CustomError customError = section.Errors.Get("404");
                if (customError == null)
                    return "/mvcerror?statuscode=404";

                return prefix + customError.Redirect.TrimStart('~');
            }
            return null;
        }

        private NhsUrl ConvertUrlToNhsUrl(HttpApplication app)
        {
            var nUrl = new NhsUrl(app.Request.Url, new ContextPathMapper());
            //we will add it as string so that its immutable
            //when casted the explicit conversion operator we have for NhsUrl will take care of the
            //cast appropriately
            app.Context.Items.Add(RewriterConfiguration.ContextItemsCurrentUrl, nUrl);

            return nUrl;
        }
    }

    internal class RouteParamComparer : IEqualityComparer<NhsUrlParam>
    {
        public bool Equals(NhsUrlParam x, NhsUrlParam y)
        {
            return x.AliasUsed == y.AliasUsed;
        }

        public int GetHashCode(NhsUrlParam obj)
        {
            return obj.AliasUsed.GetHashCode();
        }
    }
}
