﻿using System;
using System.Text.RegularExpressions;
using System.Web;
using Nhs.Mvc.Routing.Interface;
using Nhs.UrlRewriter.Configuration;
using Nhs.UrlRewriter.Enums;

namespace Nhs.UrlRewriter
{
    public class MobileRewriterModule : IHttpModule
    {
        // You will need to configure this module in the web.config file of your
        // web and register it with IIS before being able to use it. For more information
        // see the following link: http://go.microsoft.com/?linkid=8101007

        #region IHttpModule Members

        public void Dispose()
        {
            //clean-up code here.
        }

        /// <summary>
        /// Inits the specified app.
        /// </summary>
        /// <param name="app">The app.</param>
        public void Init(HttpApplication app)
        {
            app.BeginRequest += OnBeginRequest;
        }

        #endregion

        /// <summary>
        /// Called when HttpApplication's BeginRequest is triggered.
        /// </summary>
        /// <param name="sender">HttpApplication.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnBeginRequest(object sender, EventArgs e)
        {
        }

        private static void CheckAndRedirect(HttpApplication app)
        {
            var nextPage = HttpUtility.UrlEncode(app.Context.Request.RawUrl);

            //Check user agent
            if (!string.IsNullOrEmpty(app.Context.Request.UserAgent) && app.Context.Request.UserAgent.ToLower().Contains("iphone") && RewriterConfiguration.ShowIPhoneInterstitial)
                app.Response.Redirect(string.Format("~/{0}?nextpage={1}&isIphone=true", RewriterPages.IPhoneInterstitial, nextPage));
            else
                app.Response.Redirect(string.Format("~/{0}?nextpage={1}&isIphone=false", RewriterPages.MobileRedirect,
                                                    nextPage));
        }

        private static bool AmIOnHomePage()
        {
            var retValue = false;

            if (NhsRoute.CurrentRoute != null && NhsRoute.CurrentRoute.Function != null)
            {
                retValue = NhsRoute.CurrentRoute.Function.ToLower() == "home" ||
                            NhsRoute.CurrentRoute.Function.ToLower() == "basicsearch" ||
                            NhsRoute.CurrentRoute.Function.ToLower() == "";
            }

            return retValue;
        }

        private static bool IsMobileRedirectEnable()
        {
            bool retValue;

            var page = HttpContext.Current.Request.Url.PathAndQuery;
            var homeresults = HttpContext.Current.Request.RawUrl;
            if (page.Contains("/newhomesourcetv") || page.Contains("/tv"))
            {
                retValue = false;
            }
            else if (page.Contains("/communityresults") || page.Contains("/communityresultsv2") || page.Contains("/communitydetail") || page.Contains("/homedetail")
                || page.Contains("/plan/") || page.Contains("/spec/") || page == "/" || AmIOnHomePage()
                || page.Contains("/home/") || page.Contains("/community/") || homeresults.Contains("/homeresults/") || page.Contains("/homeresultsv2/"))
            {
                retValue = true;
            }
            else
            {
                retValue = false;
            }

            return retValue;
        }

        private static bool IsUserAgentMobile()
        {
            var retValue = false;

            if (IsMobileRedirectEnable())
            {
                if (!string.IsNullOrEmpty(HttpContext.Current.Request.UserAgent))
                {
                    var userAgent = HttpContext.Current.Request.UserAgent.ToLower();
                    var b = new Regex(RewriterConfiguration.IsMobileRegularExpression1, RegexOptions.IgnoreCase | RegexOptions.Multiline);
                    var v = new Regex(RewriterConfiguration.IsMobileRegularExpression2, RegexOptions.IgnoreCase | RegexOptions.Multiline);
                    var t = new Regex(RewriterConfiguration.IsMobileRegexExclude, RegexOptions.IgnoreCase | RegexOptions.Multiline);
                    var isMobile = b.IsMatch(userAgent) || v.IsMatch(userAgent.Substring(0, 4));

                    var isNotTablet = t.IsMatch(userAgent) == false;

                    retValue = isMobile && isNotTablet;
                }
            }

            return retValue;
        }
    }
}
