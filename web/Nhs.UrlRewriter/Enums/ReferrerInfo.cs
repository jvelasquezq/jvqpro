﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Nhs.UrlRewriter.Enums
{
    [Serializable]
    public class ReferrerLookup
    {
        public ReferrerLookup()
        {
            ReferrerInfo = new List<ReferrerInfo>();
        }

        [XmlElement("ReferrerInfo")]
        public List<ReferrerInfo> ReferrerInfo { get; set; }
    }

    [Serializable]
    public class ReferrerInfo
    {
        public string ReferCode { get; set; }
        public string ReferringUrl { get; set; }
        public string DestinationUrl { get; set; }
        public string Medium { get; set; }
        public string Source { get; set; }
        public string Campaign { get; set; }
    }
}