﻿namespace Nhs.Library.Enums
{
     public enum UrlParamType
    {
        Friendly,
        Hash,
        QueryString
    }
}
