﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.UrlRewriter.Enums
{
    internal class RewriterPages
    {
        public const string IPhoneInterstitial = "iphoneinterstitial";
        public const string IPhoneRedirect = "iphoneredirect";
        public const string MobileRedirect = "mobileredirect";
    }
}
