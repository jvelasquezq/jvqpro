﻿using System;
using System.Collections;
using System.Configuration;
using System.Web;
using BHI.Configuration;
using Nhs.Library.Web;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;

namespace Nhs.UrlRewriter.Configuration
{
    public class RewriterConfiguration
    {
        public const string ContextItemsCurrentUrl = "ContextItemsCurrentUrl";
        public const string ContextItemsReferrerUrl = "ContextItemsReferrerUrl";
        public const string Email = "email";
        public static readonly string FriendlyUrlExtension = ".do";
        public static string PartnerSiteUrls = "RewriterConfigurationPartnerSiteUrls";
        public static string InactivePartnerSiteUrls = "RewriterConfigurationInactivePartnerSiteUrls";
        public static string PartnerTypeCodes = "RewriterConfigurationPartnerTypeCodes";
        private static Hashtable _configEntryTable;
        public static string PartnerBrandPartnerDict = "RewriterConfigurationPartnerBrandPartnerDict";
        internal const string ContextItemsCurrentPartnerId = "ContextItemsCurrentPartnerId";

        public static string SiteMapFolder { get { return GetValue<string>("SiteMapFolder"); } }

        public static string CurrentPartnerSiteUrl
        {
            get { return HttpContext.Current.Items["ContextItemsCurrentPartnerSiteUrl"].ToType<string>(); }
            set { HttpContext.Current.Items["ContextItemsCurrentPartnerSiteUrl"] = value; }
        }
        public static int CurrentBrandPartnerId
        {
            get { return HttpContext.Current.Items["ContextItemsCurrentBrandPartnerId"].ToType<int>(); }
            internal set { HttpContext.Current.Items["ContextItemsCurrentBrandPartnerId"] = value; }
        }

        public static string FriendlyUrlDelimiter
        {
            get { return GetValue<string>("FriendlyURLDelimiter"); }
        }

        public static string ErrorStatusCode { get; set; }

        public static string[] ExtensionsToIgnore
        {
            get { return GetValue<string>("ExtensionsToIgnore").Split(','); }
        }

        public static int PartnerId
        {
            get { return NhsUrl.PartnerId; }
        }

        public static string MnhMobileSite
        {
            get { return GetValue<string>("MnhMobileSite"); }
        }

        public static string DefaultPageWithMasterPage
        {
            get { return GetValue<string>("DefaultPageWithMasterPage"); }
        }

        public static string DefaultPageSansMasterPage
        {
            get { return GetValue<string>("DefaultPageSansMasterPage"); }
        }

        public static string WebDbConnectionString
        {
            get { return GetValue<string>("WebDBConnectionString"); }
        }

        public static int MoveIPhonePartnerId
        {
            get { return GetValue<int>("MoveIPhonePartnerId"); }
        }
        public static string DomainNamePro
        {
            get { return GetValue<string>("DomainNamePro"); }
        }

        public static int PartnerIdPro
        {
            get { return GetValue<int>("PartnerIdPro"); }
        }
        
        public static int NhsIPhonePartnerId
        {
            get { return GetValue<int>("NhsIPhonePartnerId"); }
        }

        public static string IsMobileRegularExpression1
        {
            get { return GetValue<string>("IsMobileRegularExpression1");}
        }

        public static bool ShowIPhoneInterstitial
        {
            get { return GetValue<bool>("ShowIPhoneInterstitial"); }
        }

        public static string IsMobileRegularExpression2
        {
            get { return GetValue<string>("IsMobileRegularExpression2"); }
        }

        public static string StaticContentFolder
        {
            get { return GetValue<string>("StaticContentFolder"); }
        }

        /// <summary>
        /// Gets value from config DB corresponding to configKey.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="configKey">The config key.</param>
        /// <returns></returns>
        private static T GetValue<T>(string configKey)
        {
            var configValue = ConfigEntryTable[(configKey + "_" + PartnerId)]; //Try partner value

            if (configValue == null || configValue.Equals(default(T))) //not found, try brand partner value
                configValue = ConfigEntryTable[configKey + "_$" + CurrentBrandPartnerId];

            if(configValue == null || configValue.Equals(default(T))) //not found, get default
                configValue = ConfigEntryTable[configKey];

            return configValue.ToType<T>();
        }

        //TO Do: Use a hashtable thats store it in app cache.
        private static Hashtable ConfigEntryTable
        {
            get
            {
                if (_configEntryTable == null)
                {
                    Config c = new Config();
                    _configEntryTable =
                        new Hashtable(
                            c.GetGroupDictionary(ConfigurationManager.AppSettings["Nhs.UrlRewriter.ConfigGroup"]),
                            StringComparer.InvariantCultureIgnoreCase);
                }
                return _configEntryTable;
            }
        }

        // ReSharper disable InconsistentNaming

        public static string ResourceDomain
        {
            get { return GetValue<string>("ResourceDomain"); }
        }

        public static string PartnerType
        {
            get { return HttpContext.Current.Items["ContextItemsCurrentPartnerType"].ToType<string>(); }
            internal set { HttpContext.Current.Items["ContextItemsCurrentPartnerType"] = value; }
        }

        public static string IsMobileRegexExclude
        {
            get { return GetValue<string>("IsMobileRegexExclude"); }
        }


        // ReSharper disable InconsistentNaming
    }
}
