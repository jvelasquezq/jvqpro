﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using BHI.Configuration;
using Nhs.UrlRewriter.Utils;
using Nhs.Utility.Common;

namespace Nhs.UrlRewriter.Configuration
{

    internal class UrlMappingConfiguration
    {
        private static Hashtable _configEntryTable;

        public static string MappingConfigFolder
        {
            get { return GetValue<string>("MappingConfigFolder"); }
        }

        // ReSharper disable InconsistentNaming
        public static string Partner_GroupControlFolder
        // ReSharper restore InconsistentNaming
        {
            get { return GetValue<string>("Partner_GroupControlFolder"); }
        }

        public static string DefaultControlsFolder
        {
            get { return GetValue<string>("DefaultControlsFolder"); }
        }

        // ReSharper disable InconsistentNaming
        public static string PartnerBrand_GroupControlFolder
        // ReSharper restore InconsistentNaming
        {
            get { return GetValue<string>("PartnerBrand_GroupControlFolder"); }
        }



        public static string DefaultMasterPage
        {
            get
            {

                string value = GetValue<string>("DefaultMasterPage");
                if (String.IsNullOrEmpty(value))
                {
                    value = GetValue<string>("PartnerBrandDefaultMasterPage_" + RewriterConfiguration.CurrentBrandPartnerId);
                }


                return value;
            }
        }


        public static string DefaultStyleSheet
        {
            get
            {
                string value = GetValue<string>("DefaultStyleSheet");

                if (string.IsNullOrEmpty(value))
                    value = GetValue<string>("DefaultStyleSheet_" + RewriterConfiguration.PartnerType + "_$" + RewriterConfiguration.CurrentBrandPartnerId);

                if (string.IsNullOrEmpty(value))
                    value = GetValue<string>("DefaultStyleSheet_$" + RewriterConfiguration.CurrentBrandPartnerId);

                return value;
            }
        }

        /// <summary>
        /// Gets value from config DB corresponding to configKey.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="configKey">The config key.</param>
        /// <returns></returns>
        private static T GetValue<T>(string configKey)
        {
            var valueForPartner = ConfigEntryTable[(configKey + "_" + RewriterConfiguration.PartnerId)];
            if (valueForPartner == null || valueForPartner.Equals(default(T)))
                valueForPartner = ConfigEntryTable[configKey];

            return valueForPartner.ToType<T>();
        }

        //TO Do: Use a hashtable thats store it in app cache.
        private static Hashtable ConfigEntryTable
        {
            get
            {
                if (_configEntryTable == null)
                {
                    Config c = new Config();
                    _configEntryTable =
                        new Hashtable(
                            c.GetGroupDictionary(ConfigurationManager.AppSettings["Nhs.UrlMapping.ConfigGroup"]),
                            StringComparer.InvariantCultureIgnoreCase);
                }
                return _configEntryTable;
            }
        }
    }
}
