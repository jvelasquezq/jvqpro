using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;

namespace Nhs.UrlRewriter.Utils
{
    public static class ExtensionMethods
    {
        public static string EncodeBase64(this string decodedString)
        {
            byte[] byteToEncode = new byte[decodedString.Length];
            byteToEncode = Encoding.UTF8.GetBytes(decodedString);
            string encodedData = Convert.ToBase64String(byteToEncode);
            return encodedData;
        }

        public static string DecodeBase64(this string encodedString)
        {
            try
            {
                UTF8Encoding encoder = new UTF8Encoding();
                Decoder utf8Decode = encoder.GetDecoder();

                byte[] byteToDecode = Convert.FromBase64String(encodedString);
                int charCount = utf8Decode.GetCharCount(byteToDecode, 0, byteToDecode.Length);
                char[] decodedChars = new char[charCount];
                utf8Decode.GetChars(byteToDecode, 0, byteToDecode.Length, decodedChars, 0);
                string decodedData = new String(decodedChars);
                return decodedData;
            }
            catch
            {
                return null;
            }
        }


        /// <summary>
        /// Do not use this method. This is an extension method hack for backward compatibility. Use NhsUrl.AddParameter(NhsUrlParam p).AddParameter(name, value).AddParameter(name,value,type) overloads and chaining if needed
        /// </summary>
        /// <param name="sourceList">The source list.</param>
        /// <param name="paramName">Name of the param.</param>
        /// <param name="paramValue">The param value.</param>
        /// <param name="paramType">Type of the param.</param>
        public static void Add(this List<NhsUrlParam> sourceList, string paramName, string paramValue, RouteParamType paramType)
        {

            NhsUrlParam param = new NhsUrlParam(paramName, paramValue, paramType);
            sourceList.Add(param);

        }

        public static T DeepCloneObject<T>(this object obj)
        {
            if (obj == null) return default(T);
            using (MemoryStream memStream = new MemoryStream())
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter(null,
                     new StreamingContext(StreamingContextStates.Clone));
                binaryFormatter.Serialize(memStream, obj);
                memStream.Seek(0, SeekOrigin.Begin);
                return binaryFormatter.Deserialize(memStream).ToType<T>();
            }
        }

        public static string GetFormattedMvcUrl(this NhsUrl nhsUrl, bool tossRefer)
        {
            var friendlyParamCount = nhsUrl.Parameters.Where(p => p.ParamType == RouteParamType.Friendly).Count();
            var ajaxMethod = string.Empty;
            var ajaxMethods = new List<string> { "addhometouserplanner", "addcommunitytouserplanner", "search" }; //44813 - post urls seem to be getting crawled. Anything that ends with these methods needs to be 301ed.

            if (NhsRoute.CurrentRoute.Function == "community" || NhsRoute.CurrentRoute.Function == "comunidad" || NhsRoute.CurrentRoute.Function == "casa" || NhsRoute.CurrentRoute.Function == "home" || NhsRoute.CurrentRoute.Function == "plan") return string.Empty;

            switch (NhsRoute.CurrentRoute.Controller.Name.ToLower())
            {
                case "communityresults":
                    var crParams = new List<string>() { "brandid", "logevent" };

                    var logBrandParams = (from p in nhsUrl.Parameters
                                          where crParams.Contains(p.AliasUsed)
                                          select p.DefaultName).ToList();

                    if (logBrandParams.Count() == 2)
                    {
                        var mvcParams = (from p in nhsUrl.Parameters
                                         where p.AliasUsed != "logevent"
                                         select p).ToList();
                        return BuildMvcRouteUrl(nhsUrl.Parameters, mvcParams, tossRefer);
                    }

                    if (!string.IsNullOrEmpty(nhsUrl.FriendlyParameterPartAll) && nhsUrl.FriendlyParameterPartAll.ToLower().Contains("/search/"))
                        return nhsUrl.FriendlyParameterPartAll.ToLower().Replace("/search/", "/").StripLeadingEndingChar('/');

                    break;

                case "communitydetail":
                    try
                    {
                        ajaxMethod = nhsUrl.FriendlyParameterPartAll.Substring(nhsUrl.FriendlyParameterPartAll.LastIndexOf("/") + 1).ToLower();
                    }
                    catch (Exception ex)
                    {
                        ErrorLogger.LogError(ex);
                    }

                    if (friendlyParamCount > 2 || ajaxMethods.Contains(ajaxMethod) || tossRefer && NhsRoute.BrandPartnerId !=10001)
                    {
                        List<NhsUrlParam> mvcParams = new List<NhsUrlParam>()
                                                          {
                                                              new NhsUrlParam() {AliasUsed = "builder", Order = 1},
                                                              new NhsUrlParam() {AliasUsed = "community", Order = 2}
                                                          };

                        return BuildMvcRouteUrl(nhsUrl.Parameters, mvcParams, tossRefer);
                    }

                    break;

                case "homedetail":
                    try
                    {
                        ajaxMethod = nhsUrl.FriendlyParameterPartAll.Substring(nhsUrl.FriendlyParameterPartAll.LastIndexOf("/") + 1).ToLower();
                    }
                    catch (Exception ex)
                    {
                        ErrorLogger.LogError(ex);
                    }

                    if (friendlyParamCount > 2 || ajaxMethods.Contains(ajaxMethod) || tossRefer && NhsRoute.BrandPartnerId != 10001)
                    {
                        List<NhsUrlParam> mvcParams = new List<NhsUrlParam>()
                                                          {
                                                              new NhsUrlParam() {AliasUsed = "planid", Order = 1},
                                                              new NhsUrlParam() {AliasUsed = "specid", Order = 1}
                                                          };
                        return BuildMvcRouteUrl(nhsUrl.Parameters, mvcParams, tossRefer);
                    }

                    if (friendlyParamCount > 1 && NhsRoute.BrandPartnerId != 10001)
                    {
                        var pageParam = nhsUrl.Parameters.Where(p => p.AliasUsed == "page" && p.ParamType == RouteParamType.Friendly).FirstOrDefault();

                        if (pageParam != null)
                            return string.Empty;

                        List<NhsUrlParam> mvcParams = new List<NhsUrlParam>()
                                                          {
                                                              new NhsUrlParam() {AliasUsed = "planid", Order = 1},
                                                              new NhsUrlParam() {AliasUsed = "specid", Order = 1},
                                                              new NhsUrlParam(){AliasUsed = "page", Order = 2}
                                                          };

                        return BuildMvcRouteUrl(nhsUrl.Parameters, mvcParams, tossRefer);
                    }
                    break;
            }

            return string.Empty;
        }

        private static string BuildMvcRouteUrl(IEnumerable<NhsUrlParam> parameters, IEnumerable<NhsUrlParam> mvcParams, bool tossRefer)
        {
            var queryParams = parameters.Except(mvcParams, new RouteParamComparer()).ToList();
            if (!tossRefer)
                queryParams.RemoveAll(p => p.DefaultName != "refer");
                    // Anything that is not the refer parameter, ignore it.
            else
                queryParams.Clear();

            var friendlyParams = parameters.Where(p => mvcParams.Contains(p, new RouteParamComparer()));

            foreach (var param in mvcParams)
            {
                var fp = (from p in friendlyParams
                          where p.AliasUsed == param.AliasUsed
                          select p).FirstOrDefault();

                if (fp != null)
                    fp.Order = param.Order;
            }

            friendlyParams = friendlyParams.OrderBy(p => p.Order);
            string query = queryParams.Aggregate(string.Empty, (current, parameter) => current + string.Format("{0}={1}&", parameter.AliasUsed, parameter.Value.Replace(" ", "-")));
            string friendlyPart = friendlyParams.Aggregate(string.Empty, (current, parameter) => current + string.Format("{0}-{1}/", parameter.AliasUsed, parameter.Value.Replace(" ", "-")));

            var mvcUrl = friendlyPart + "?" + StringHelper.StripLeadingEndingChar(query, '&');
            mvcUrl = NhsRoute.CurrentRoute.Function + "/" + mvcUrl;

            if (NhsRoute.BrandPartnerId != NhsRoute.PartnerId)
                mvcUrl = NhsRoute.PartnerSiteUrl + "/" + mvcUrl;

            return mvcUrl.TrimEnd("?".ToCharArray()).TrimEnd("/".ToCharArray());
        }
    }
}


namespace Nhs.Library.Web
{
    public static class ExtensionMethods2
    {

        /// <summary>
        /// Do not use this method. This is an extension method hack for backward compatibility. Use NhsUrl.AddParameter(NhsUrlParam p).AddParameter(name, value).AddParameter(name,value,type) overloads and chaining if needed
        /// </summary>
        /// <param name="sourceList">The source list.</param>
        /// <param name="paramName">Name of the param.</param>
        /// <param name="paramValue">The param value.</param>
        /// <param name="paramType">Type of the param.</param>
        public static void Add(this List<NhsUrlParam> sourceList, string paramName, string paramValue, RouteParamType paramType)
        {

            NhsUrlParam param = new NhsUrlParam(paramName, paramValue, paramType);
            sourceList.Add(param);
        }

        /// <summary>
        /// Do not use this method. This is an extension method hack for backward compatibility. Use NhsUrl.Remove(NhsUrlParam p)
        /// </summary>
        /// <param name="sourceList">The source list.</param>
        /// <param name="paramName">Name of the param.</param>
        public static void Remove(this List<NhsUrlParam> sourceList, string paramName)
        {
            sourceList.RemoveAll(x => x.DefaultName == paramName);
        }
    }
}