﻿using System;
using System.Linq;
using System.Net;
using System.Web;
using Nhs.UrlRewriter.Configuration;

namespace Nhs.UrlRewriter.Utils
{
    internal class RewriterCookieManager
    {
        //public static bool ShowNewMobileSite
        //{
        //    get
        //    {
        //        var data = GetItem("ShowNewMobileSite");
        //        return !string.IsNullOrEmpty(data) && Convert.ToBoolean(data);
        //    }
        //    set { SetItem("ShowNewMobileSite", value); }
        //}

        public static bool RedirectFromRefer
        {
            get
            {
                var data = GetItem("301FromRefer");
                if (string.IsNullOrEmpty(data))
                    return false; 
                else
                     return Convert.ToBoolean(data);
            }
            set { SetItem("301FromRefer", value, DateTime.Now.AddSeconds(5)); }
        }


        #region "Private Methods"
        /// <summary>
        /// Delete Cookie
        /// </summary>
        /// <param name="key">Use values in CookieConst</param>
        public static void DeleteCookie(string key)
        {
            RemoveItem(CookieKeyName(key));
        }


        /// <summary>
        /// Build the Cookie Name with the PartnerId as {key}_{partnerid}
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private static string CookieKeyName(string key)
        {
            return string.Format("{0}_{1}", key, RewriterConfiguration.PartnerId);
        }

        /// <summary>
        /// Get Coockie Values
        /// </summary>
        /// <param name="key">Use values in CookieConst</param>
        /// <returns></returns>
        private static string GetItem(string key)
        {
            var cookie = HttpContext.Current.Request.Cookies[CookieKeyName(key)];
            var cookieValue = string.Empty;
            if (cookie != null)
            {
                cookieValue = cookie.Value;
            }
            return cookieValue;
        }

        /// <summary>
        /// Set Coockie Values
        /// </summary>
        /// <param name="key">Use values in CookieConst</param>
        /// <param name="value">Cookie value</param>
        /// <returns></returns>
        private static void SetItem(string key, object value, DateTime? expires = null)
        {
            HttpCookie cookie = null;

            key = CookieKeyName(key);

            cookie = HttpContext.Current.Response.Cookies[key];
            if (cookie != null)
            {
                cookie.Value = value.ToString();
            }
            else
                cookie = new HttpCookie(key, value.ToString()) { Expires = DateTime.Now.AddYears(100) };


            if (expires.HasValue && expires != null)
                cookie.Expires = (DateTime)expires;

            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        /// <summary>
        /// Remove Coockie 
        /// </s
        /// ummary>
        /// <param name="key">Use CookieKeyName(key);</param>
        private static void RemoveItem(string key)
        {
            var httpCookie = HttpContext.Current.Response.Cookies[key];
            if (httpCookie != null)
            {
                httpCookie.Value = string.Empty;
                httpCookie.Expires = DateTime.Now.AddDays(-1d);
                HttpContext.Current.Response.Cookies.Add(httpCookie);
            }
        }
        #endregion
    }
}
