﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Nhs.Library.Web;
using Nhs.UrlRewriter.Configuration;

namespace Nhs.UrlRewriter.Utils
{
    internal class UserSession
    {
        private static HttpSessionStateBase _current = null;
        public static HttpSessionStateBase Current
        {
            get { return _current ?? (_current = new HttpSessionStateWrapper(HttpContext.Current.Session)); }
            set { _current = value; }
        }

        public static string ReferrerUrl
        {
            get { return GetItem("ReferrerUrl") == null ? string.Empty : (string)GetItem("ReferrerUrl"); }
            set { SetItem("ReferrerUrl", value); }
        }

        public static string DestinationUrl
        {
            get { return GetItem("DestinationUrl") == null ? string.Empty : (string)GetItem("DestinationUrl"); }
            set { SetItem("DestinationUrl", value); }
        }


        //This Refer will be rewriter all the time that the user enter with a new Refer
        public static string DynamicRefer
        {
            get
            {
                return GetItem("DynamicRefer") == null ? string.Empty : GetItem("DynamicRefer").ToString();
            }
            set { SetItem("DynamicRefer", value); }
        }

        //The refer url for traffic categorization
        public static string DynamicReferUrl
        {
            get
            {
                return GetItem("DynamicReferUrl") == null ? string.Empty : GetItem("DynamicReferUrl").ToString();
            }
            set { SetItem("DynamicReferUrl", value); }
        }

        #region "Public Methods"
        public static object GetItem(string key)
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
                return HttpContext.Current.Session[key + "_" + RewriterConfiguration.PartnerId];
            return Current[key + "_" + RewriterConfiguration.PartnerId];
        }
        public static void SetItem(string key, object value)
        {
            Current[key + "_" + RewriterConfiguration.PartnerId] = value;
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
                HttpContext.Current.Session[key + "_" + RewriterConfiguration.PartnerId] = value;
        }
        #endregion

    }


}

