﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Nhs.Mvc.Domain.Model
{
    /// <remarks/>
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class OwnerStoriesContent
    {
        public OwnerStoriesContent()
        {
            OwnerStoryContentList = new List<OwnerStoryContent>();
        }
        [XmlArray("stories")]
        [XmlArrayItem("ownerstory", typeof(OwnerStoryContent))]
        public List<OwnerStoryContent> OwnerStoryContentList { get; set; }
        [XmlElement("contestRules")]
        public string ContestRules { get; set; }
        [XmlElement("submission")]
        public string Submission { get; set; }
    }
}
