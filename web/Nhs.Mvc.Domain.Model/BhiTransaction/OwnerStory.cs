﻿
using System.IO;

namespace Nhs.Mvc.Domain.Model.BhiTransaction
{
    public class OwnerStory
    {
        public int OwnerStoryId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int BuilderId { get; set; }
        public string BuilderName { get; set; }
        public int CommunityId { get; set; }
        public string CommunityName { get; set; }
        public string City { get; set; }
        public string StoryDescription { get; set; }       
        public string MediaContentType { get; set; }
        public string Category { get; set; }
        public Stream FileStream { get; set; }
        public string FilePath { get; set; }
        public string CreationDateTimeime { get; set; }
        public string ModificationDateTimeime { get; set; }
        public bool? Status { get; set; }
        public bool HasImage { get; set; }
        public bool HasVideo { get; set; }
        public string ProcessedVideoUrl { get; set; }
        public string ProcessedVideoThumbnailUrl { get; set; }
        public string ProcessedVideoTitle { get; set; }
        public int ProcessedVideoId { get; set; }
        public string StatusText 
        {
            get
            {
                switch (Status)
                {
                    case null:
                        return "Pending";
                    case false:
                        return "Not Published";
                    default:
                        return "Published";
                }
            }
        }


        public bool IsImage
        {
            get
            {
                if (string.IsNullOrWhiteSpace(MediaContentType) == false)
                {
                    return MediaContentType.StartsWith("image");
                }

                return false;
            }
        }
    }
}
