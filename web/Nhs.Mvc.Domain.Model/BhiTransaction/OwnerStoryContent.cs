﻿using System;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using Nhs.Utility.Web;

namespace Nhs.Mvc.Domain.Model
{
    [XmlType(AnonymousType = true)]
    public class OwnerStoryContent
    {
        [XmlAttribute("title")]
        public string Title { get; set; }
        [XmlElement("story")]
        public string Story { get; set; }
        [XmlElement("media", typeof(OwnerStoryMedia))]
        public OwnerStoryMedia Media { get; set; }

        public int Sequence { get; set; }

        public OwnerStoryContent()
        {
            Media = new OwnerStoryMedia();
        }
    }
    [XmlType(AnonymousType = true)]
    public class OwnerStoryMedia
    {

        [XmlAttribute("url")]
        public string MediaUrl { get; set; }

        public bool IsNullOrEmpty
        {
            get { return string.IsNullOrEmpty(MediaUrl); }
        }

        public string ImageUrl
        {
            get
            {
                if (UrlHelper.IsYoutubeVideo(MediaUrl))
                {
                    var youtubeVideoId = UrlHelper.GetYoutubeVideoId(MediaUrl);
                    return string.Format("http://img.youtube.com/vi/{0}/0.jpg", youtubeVideoId);
                }
                
                return MediaUrl;
            }
        }

        public string VideoId
        {
            get
            {
                if (UrlHelper.IsYoutubeVideo(MediaUrl))
                {
                    return UrlHelper.GetYoutubeVideoId(MediaUrl);
                }

                return null;
            }
        }
    }
}
