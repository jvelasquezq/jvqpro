﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Mvc.Domain.Model.CMS
{
    /// <summary>
    /// This is the class for multipart article container.
    /// </summary>
    public class RcMpArticleContainer
    {
        public string ContentTitle { get; set; }
        public long ArticleID { get; set; }
        public string ArticleTitle { get; set; }
        public string ArticleSubTitle { get; set; }
        public string ArticleShortTitle { get; set; }
        public string ArticleMainImage { get; set; }
        public string ArticleSummary { get; set; }
        public RcAuthor ArticleAuthor { get; set; }
        //public string Link { get { return "/resourcecenter/articles/" + ArticleTitle.ToLower().Trim().Replace(" ", "-"); } }
        public List<RcMpArticleSection> ArticleSections { get; set; }
    }

    /// <summary>
    /// This is the class for multipart article section.
    /// </summary>
    public class RcMpArticleSection
    {
        public string ContentTitle { get; set; }
        public long SectionID { get; set; }
        public string SectionTitle { get; set; }
        public string SectionImage { get; set; }
        public string PhotoCaptionAndSource { get; set; }
        public string SectionSummary { get; set; }
        public string SectionContent { get; set; }
    }
}
