﻿using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Nhs.Mvc.Domain.Model.CMS
{
    public class RcArticle
    {
        public RcArticle()
        {
            Type = string.Empty;         
        }        

        public string ContentTitle { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Subhead { get; set; }
        public string Copy { get; set; }
        public string Teaser { get; set; }
        public string TeaserLinkText { get; set; }
        public string HeadLine { get; set; }
        public string FeaturedImage { get; set; }
        public string FeaturedImageCaption { get; set; }
        public string FeaturedImageAlign { get; set; }
        public string Source { get; set; }
        public string Author { get; set; }
        public string Type { get; set; }
        public IEnumerable<long> RelatedArticles { get; set; }
        public bool Promoted { get; set; }
        public int SequenceNumber { get; set; }
        public int SearchRank { get; set; }

        public string Link(bool isCna)
        {
            var pageName = isCna? "articulos" : "articles";

            if (string.IsNullOrWhiteSpace(Type) == false)
            {
                switch (Type.ToLower())
                {
                    case "featuredhomeslidearticle":
                    case "articleslideshow":
                        pageName = isCna ? "presentaciones-de-diapositivas" : "slideshows";
                        break;
                    default:
                        pageName = isCna ? "articulos" : "articles";
                        break;
                }
            }

            return string.Format("{0}/{1}", pageName, (ContentTitle ?? string.Empty).ToLower().Trim().Replace(" ", "-"));
        }
    }

    public class RcAuthor
    {
        public RcAuthor()
        {
            SocialNetworks = new List<AuthorSocialNetwork>();
        }
        public string Title { get; set; }
        public long AuthorId { get; set; }
        public string Name { get; set; }
        public string Bio { get; set; }
        public string LongBio { get; set; }
        public string ImagePath { get; set; }
        public string Headline { get; set; }
        public IList<AuthorSocialNetwork> SocialNetworks;
        public bool Active { get; set; }
        public string Expertise { get; set; }
    }

    public class AuthorSocialNetwork
    {
        public string NetworkName { get; set; }
        public string Url { get; set; }
        public string Rel { get; set; }
    }

    public class NhsCmsSelectList
    {
        private readonly IEnumerable _items;
        private readonly string _dataValueField ;
        private readonly string _dataTextField;
        private readonly object _selectedValue;

        public NhsCmsSelectList(IEnumerable items, string dataValueField, string dataTextField, object selectedValue)
        {
            _items = items;
            _dataValueField = dataValueField;
            _dataTextField = dataTextField;
            _selectedValue = selectedValue;
        }

        public SelectList GetSelectList()
        {
            return new SelectList(_items,_dataValueField, _dataTextField, _selectedValue);
        }

    }

    public class CmsProSearch
    {
        public string SearchText { get; set; }
        public IEnumerable<SelectListItem> NumOfBaths { get; set; }
        public IEnumerable<SelectListItem> SqFtMin { get; set; }
        public IEnumerable<SelectListItem> NumOfBeds { get; set; }
        public IEnumerable<SelectListItem> PriceLow { get; set; }
        public IEnumerable<SelectListItem> PriceHigh { get; set; }
        public NhsCmsSelectList PriceLoRange { get; set; }
        public NhsCmsSelectList PriceHiRange { get; set; }
        public NhsCmsSelectList BathList { get; set; }
        public NhsCmsSelectList BedRoomsList { get; set; }
        public NhsCmsSelectList SqFtList { get; set; }
    }
}
