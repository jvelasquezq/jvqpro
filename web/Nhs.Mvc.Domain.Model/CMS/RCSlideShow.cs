﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Nhs.Mvc.Domain.Model.CMS
{
    public class RCSlideShow
    {
        public List<RCSlideImage> Images { get; set; }

        public RCSlideShow()
        {
            Images = new List<RCSlideImage>();
        }
    }

    public class RCSlideImage
    {
        public string Category { get; set; } //Used on Category page where category assignment cannot be done via Ektron natively
        public string ImagePath { get; set; }
        public string ImageTitle { get; set; }
        public string ImageDescription { get; set; } //Reused on ArticleSlideShow page to show slideshow article detail
        public string Link { get; set; }
        
        //Followed are used only on ArticleSlideShow page
        public string ImageCaption { get; set; } 
        public string ImageSource { get; set; } 
        public string ImageWidth { get; set; } 
        public string ImageAlignment { get; set; } 
    }
}
