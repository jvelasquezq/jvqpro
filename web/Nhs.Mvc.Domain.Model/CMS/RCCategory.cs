﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Mvc.Domain.Model.CMS
{
    public class RCCategory
    {
        public double CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string CategoryDescription { get; set; }
        public string CategoryHtml { get; set; }        
        public IList<RcArticle> FeaturedArticles { get; set; }
        public IList<RCCategory> SubCategories { get; set; }
        public RCCategory()
        {
            FeaturedArticles = new List<RcArticle>();
        }
    }
}
