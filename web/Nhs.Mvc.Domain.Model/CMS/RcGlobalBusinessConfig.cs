﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Mvc.Domain.Model.CMS
{
    public class RcGlobalBusinessConfig
    {
        public RcGlobalBusinessConfig()
        {
            NumberFeaturedArticlesToDisplay = 12;
        }

        /// <summary>
        /// Number of featured articles to displayed
        /// </summary>
        public int NumberFeaturedArticlesToDisplay { get; set; }
    }
}
