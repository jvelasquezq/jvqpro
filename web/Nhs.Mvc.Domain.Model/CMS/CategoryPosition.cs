﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Mvc.Domain.Model.CMS
{
    public class CategoryPosition
    {
        public int PositionNumber { get; set; }
        public string ShortTeaser { get; set; }
        public string TeaserB { get; set; }
        public string Headline { get; set; }
        public string ArticleTitle { get; set; }
        public int ArticleId { get; set; }

    }
}
