﻿
namespace Nhs.Mvc.Domain.Model.CMS
{
    /// <summary>
    /// When you use EktronService.GetContentList you can use this class
    /// to parse the List objects content
    /// </summary>
    public class RcContentInfo
    {
       public long Id { get; set; }
	   public int ContentType{ get; set; } 
	   public string Title{ get; set; }
	   public int LanguageId{ get; set; }
       public string ArticleHtml { get; set; }       
    }
}
