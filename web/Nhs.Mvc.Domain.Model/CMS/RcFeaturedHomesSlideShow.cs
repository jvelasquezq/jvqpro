﻿using System.Collections.Generic;

namespace Nhs.Mvc.Domain.Model.CMS
{
    public class RcFeaturedHomesSlideShow : RcArticle
    {
        public RcFeaturedHomesSlideShow()            
        {
            Slides = new List<RcFeaturedHomesSlideShowImage>();
            RelatedArticles = new List<long>();
            Type = "featurearticleslideshow";
        }

        public string SlideshowMainTitle { get; set; }
        public string SlideshowSubTitle
        {
            get { return base.Subtitle; }
            set
            {
                base.Subtitle = value;
                base.Teaser = value;
            }
        }

        public string SlideInformationAreaText { get; set; }

        public string ArticleType { get; set; }
        
        public bool UseIntroductorySlide { get; set; }

        public string IntroductoryPicture { get; set; }
        public string IntroductoryMainTitle { get; set; }
        public string IntroductoryTitle { get; set; }
        public string IntroductoryText { get; set; }
        public string IntroductoryAuthor { get; set; }
        
        public int SlidesCount
        {
            get { return Slides.Count; }
        }

        public IList<RcFeaturedHomesSlideShowImage> Slides { get; private set; } 
    }

    public class RcFeaturedHomesSlideShowImage
    {
        public RcFeaturedHomesSlideShowImage()
        {
            OverlayAreaTwoItems = new List<OverlayAreaTwoItem>();
        }

        public string DetailsColor { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }

        public int SequenceNumber { get; set; }
        public string PictureUrl { get; set; }
        public string OverlayTitle { get; set; }
        public string OverlayLink { get; set; }
        public string OverlayLinkText { get; set; }
        public string OverlayAreaOne { get; set; }
        public IList<OverlayAreaTwoItem> OverlayAreaTwoItems { get; private set; }
        public long AuthorId { get; set; }
        public string Author { get; set; }
    }

    public class OverlayAreaTwoItem
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public bool RightCol { get; set; }
    }
}
