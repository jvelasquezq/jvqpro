﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Mvc.Domain.Model.CMS
{
    public class RcAuthorLandingPage
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string ImagePath { get; set; }

    }
}
