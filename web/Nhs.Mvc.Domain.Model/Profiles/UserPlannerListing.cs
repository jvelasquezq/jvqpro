//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Nhs.Mvc.Domain.Model.Profiles
{
    public partial class UserPlannerListing
    {
        #region Primitive Properties
    
        public virtual int PlannerId
        {
            get;
            set;
        }
    
        public virtual string UserGuid
        {
            get { return _userGuid; }
            set
            {
                if (_userGuid != value)
                {
                    if (UserProfile != null && UserProfile.UserGuid != value)
                    {
                        UserProfile = null;
                    }
                    _userGuid = value;
                }
            }
        }
        private string _userGuid;
    
        public virtual string ListingType
        {
            get;
            set;
        }
    
        public virtual int ListingId
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> SaveDate
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> LeadRequestDate
        {
            get;
            set;
        }
    
        public virtual bool RecommendedFlag
        {
            get;
            set;
        }
    
        public virtual Nullable<int> BuilderId
        {
            get;
            set;
        }
    
        public virtual bool SuppressFlag
        {
            get;
            set;
        }
    
        public virtual string source_request_key
        {
            get;
            set;
        }

        #endregion
        #region Navigation Properties
    
        public virtual UserProfile UserProfile
        {
            get { return _userProfile; }
            set
            {
                if (!ReferenceEquals(_userProfile, value))
                {
                    var previousValue = _userProfile;
                    _userProfile = value;
                    FixupUserProfile(previousValue);
                }
            }
        }
        private UserProfile _userProfile;

        #endregion
        #region Association Fixup
    
        private void FixupUserProfile(UserProfile previousValue)
        {
            if (previousValue != null && previousValue.UserPlannerListing.Contains(this))
            {
                previousValue.UserPlannerListing.Remove(this);
            }
    
            if (UserProfile != null)
            {
                if (!UserProfile.UserPlannerListing.Contains(this))
                {
                    UserProfile.UserPlannerListing.Add(this);
                }
                if (UserGuid != UserProfile.UserGuid)
                {
                    UserGuid = UserProfile.UserGuid;
                }
            }
        }

        #endregion
    }
}
