﻿
namespace Nhs.Mvc.Domain.Model.Profiles
{
    public partial class SearchAlert
    {
        public string BuilderName { get; set; }
        public string MarketName { get; set; }
        public string SchoolDistrictName { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
    }
}
