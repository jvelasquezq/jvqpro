//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Nhs.Mvc.Domain.Model.Profiles
{
    public partial class ClientTaskListing
    {
        #region Primitive Properties
    
        public virtual int TaskListingId
        {
            get;
            set;
        }
    
        public virtual int ClientTaskId
        {
            get { return _clientTaskId; }
            set
            {
                if (_clientTaskId != value)
                {
                    if (ClientTask != null && ClientTask.ClientTaskId != value)
                    {
                        ClientTask = null;
                    }
                    _clientTaskId = value;
                }
            }
        }
        private int _clientTaskId;
    
        public virtual string ListingType
        {
            get;
            set;
        }
    
        public virtual int PropertyId
        {
            get;
            set;
        }

        #endregion
        #region Navigation Properties
    
        public virtual ClientTask ClientTask
        {
            get { return _clientTask; }
            set
            {
                if (!ReferenceEquals(_clientTask, value))
                {
                    var previousValue = _clientTask;
                    _clientTask = value;
                    FixupClientTask(previousValue);
                }
            }
        }
        private ClientTask _clientTask;

        #endregion
        #region Association Fixup
    
        private void FixupClientTask(ClientTask previousValue)
        {
            if (previousValue != null && previousValue.ClientTaskListings.Contains(this))
            {
                previousValue.ClientTaskListings.Remove(this);
            }
    
            if (ClientTask != null)
            {
                if (!ClientTask.ClientTaskListings.Contains(this))
                {
                    ClientTask.ClientTaskListings.Add(this);
                }
                if (ClientTaskId != ClientTask.ClientTaskId)
                {
                    ClientTaskId = ClientTask.ClientTaskId;
                }
            }
        }

        #endregion
    }
}
