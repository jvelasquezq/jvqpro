﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Mvc.Domain.Model.Web.Interfaces
{
    public interface IAmenity
    {
        int CommunityId { get; set; }
        int AmenityId { get; set; }
        string Description { get; set; }
        string Url { get; set; }
        Nullable<int> GroupId { get; set; }
        int ShortNumber { get; set; }
    }
}
