﻿using System;

namespace Nhs.Mvc.Domain.Model.Hub.Interfaces
{
    public interface IImage
    {
        int ImageId { get; set; }
        string ImageName { get; set; }
        string ImageNameSafe { get; }
        string ImageTitle { get; set; }
        string ImageDescription { get; set; }
        string ImagePath { get; set; }
        string ImagePathSafe { get; }
        string OriginalPath { get; set; }
        string ImageTypeCode { get; set; }
        Nullable<int> ImageSequence { get; set; }
        string ClickThruUrl { get; set; }
        Nullable<int> CommunityId { get; set; }
        Nullable<int> SpecificationId{get;set;}
        Nullable<int> PlanId { get; set; }
        Nullable<int> BuilderId { get; set; }
        string PrimaryFlag { get; set; }
    }

}
