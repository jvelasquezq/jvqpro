﻿using System;
using System.Collections.Generic;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Domain.Model.Hub;

namespace Nhs.Mvc.Domain.Model.Web.Interfaces
{
    public interface ISpec:IListing
    {
        int PlanId { get; set; }
        int SpecId { get; set; }
        string Status { get; set; }
        string PlanNumber { get; set; }
        string SpecNumber { get; set; }
        string SpecType { get; set; }
        string HomeMarketingDescription { get; set; }
        string Address1 { get; set; }
        string Address2 { get; set; }
        string Address3 { get; set; }
        string Address4 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string PostalCode { get; set; }
        DateTime? MoveInDate { get; set; }
        Nullable<int> NumLivingAreas { get; set; }
        Nullable<int> MasterBedroomLocation { get; set; }
        string HotHomeDescription { get; set; }
        string PromoTextLong { get; set; }
        string PromoTextShort { get; set; }
        string PromoUrl { get; set; }
        string VirtualTourUrl { get; set; }
        int LandExcluded { get; set; }
        Plan Plan { get; set; }
        HubPlan HubPlan { get; set; }
        ICollection<IImage> SpecImages { get; }
        ICollection<IVideo> SpecVideos { get; }
        int HomeStatus { get; }
        string ImageThumbnail { get; set; }
    }

}
