﻿using System;
using Nhs.Mvc.Domain.Model.Hub;

namespace Nhs.Mvc.Domain.Model.Web.Interfaces
{
    public interface IListing
    {
        Community Community { get;  }
        HubCommunity HubCommunity { get;  }
        int Id { get; }
        string PlanName { get; }
        int CommunityId { get; }
        decimal Price { get; }
        int? SqFt { get; }
        int? Bedrooms { get; }
        int? Bathrooms { get; }
        decimal? Garages { get; }
        int IsHotHome { get; }
        string Description { get; }
        int? HalfBaths { get; }
        int? Stories { get; }
        string PlanTypeCode { get; }
        string HotHomeTitle { get; }
    }
}
