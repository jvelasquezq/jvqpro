﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Mvc.Domain.Model.Hub.Interfaces
{
    public interface IVideo
    {
        int VideoId { get; set; }
        int BuilderId { get; set; }
        Nullable<int> CommunityId { get; set; }
        Nullable<int> PlanId { get; set; }
        Nullable<int> SpecificationId { get; set; }
        string VideoURL { get; set; }
        string Title { get; set; }
        string ThumbnailUrl { get; set; }
        int Order { get; }
        string RefId { get; set; }
        bool IsFromYoutube { get; }
        bool IsFromVimeo { get; }
        int VideoWidth { get; set; }
        int VideoHeight { get; set; }
        string VideoOnlineId { get; set; }
    }
}
