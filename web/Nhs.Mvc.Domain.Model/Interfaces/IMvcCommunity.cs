﻿using System;
using System.Collections.Generic;
using Nhs.Mvc.Domain.Model.Hub;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;

namespace Nhs.Mvc.Domain.Model
{
    //public interface IMvcCommunity
    //{
    //    HubMarket HubMarket { get; set; }
    //    Market Market { get; set; }
    //    IEnumerable<IListing> Listings { get; }
    //    int CommunityId { get; set; }
    //    string CommunityName { get; set; }
    //    int MarketId { get; set; }
    //    int BuilderId { get; set; }
    //    int BrandId { get; set; }
    //    int SalesOfficeId { get; set; }
    //    string Address1 { get; set; }
    //    string Address2 { get; set; }
    //    string City { get; set; }
    //    string PostalCode { get; set; }
    //    string StateAbbr { get; set; }
    //    decimal Latitude { get; set; }
    //    decimal Longitude { get; set; }
    //    int SqFtLow { get; set; }
    //    int SqFtHigh { get; set; }
    //    decimal PriceLow { get; set; }
    //    decimal PriceHigh { get; set; }
    //    Nullable<int> IsGated { get; set; }
    //    Nullable<int> IsCondo { get; set; }
    //    Nullable<int> IsMasterPlanned { get; set; }
    //    Nullable<int> HasGolfCourse { get; set; }
    //    Nullable<int> IsGreen { get; set; }
    //    int HasVideo { get; set; }
    //    int HasHotHome { get; set; }
    //    Nullable<int> HomeCount { get; set; }
    //    string ListingTypeFlag { get; set; }
    //    string BCType { get; set; }
    //    string Status { get; set; }
    //    string PhoneNumber { get; set; }
    //    string FaxNumber { get; set; }
    //    string CommunityUrl { get; set; }
    //    string EnvisionLink { get; set; }
    //    string BannerImage { get; set; }
    //    string MapURL { get; set; }
    //    string SpotlightThumbnail { get; set; }
    //    string ShowMortgageLink { get; set; }
    //    Nullable<int> PlanHotHomeId { get; set; }
    //    string PlanHotHomeTitle { get; set; }
    //    string PlanHotHomeDescription { get; set; }
    //    Nullable<int> SpecHotHomeId { get; set; }
    //    string SpecHotHomeTitle { get; set; }
    //    string SpecHotHomeDescription { get; set; }
    //    string CommunityDescription { get; set; }
    //    Nullable<int> QuickMoveInCount { get; set; }
    //    Nullable<int> IsAdult { get; set; }
    //    Nullable<int> IsAgeRestricted { get; set; }
    //    Nullable<int> OutOfCommunityFlag { get; set; }
    //    SalesOffice SalesOffice { get; set; }
    //    State State { get; set; }
    //    HubState HubState { get; set; }
    //    Builder Builder { get; set; }
    //    HubBuilder HubBuilder { get; set; }
    //    ICollection<Video> Videos { get; set; }
    //    ICollection<HubVideo> HubVideos { get; set; }
    //    Brand Brand { get; set; }
    //    HubBrand HubBrand { get; set; }
    //    ICollection<Image> AllImages { get; set; }
    //    ICollection<HubImage> AllHubImages { get; set; }
    //    ICollection<Plan> Plans { get; set; }
    //    ICollection<HubPlan> HubPlans { get; set; }
    //    ICollection<Amenity> Amenities { get; set; }
    //    ICollection<OpenAmenity> OpenAmenities { get; set; }
    //    ICollection<Spec> Specs { get; set; }
    //}
}
