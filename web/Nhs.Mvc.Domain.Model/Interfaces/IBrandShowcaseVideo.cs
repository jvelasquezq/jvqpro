﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Mvc.Domain.Model.Interfaces
{
    public interface IBrandShowcaseVideo
    {
        int BrandId { get; set; }
        int VideoId { get; set; }
    }
}
