﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Mvc.Domain.Model.Hub;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;

namespace Nhs.Mvc.Domain.Model.Web.Interfaces
{
    public interface IPlan:IListing
    {
        ICollection<IImage> PlanImages { get; }
        ICollection<IVideo> PlanVideos { get; }
        string EnvisionUrl { get; }
        int PlanId { get; set; }        
        string Status { get; set; }
        string HomeMarketingDescription { get; set; }
        Nullable<int> NumLivingAreas { get; set; }
        Nullable<int> MasterBedroomLocation { get; set; }
        int LandExcluded { get; set; }
        int Floors { get; set; }
        int Location { get; set; }
        int Townhome { get; set; }
        int Condo { get; set; }
        string HotHomeDescription { get; set; }
        string PromoTextLong { get; set; }
        string PromoTextShort { get; set; }
        string PromoUrl { get; set; }
        string VirtualTourUrl { get; set; }
        string ImageThumbnail { get; set; }
        ICollection<Video> AllPlanVideos { get; set; }
        ICollection<Image> AllPlanImages { get; set; }
        ICollection<HubVideo> AllPlanHubVideos { get; set; }
        ICollection<HubImage> AllPlanHubImages { get; set; }
        ICollection<Spec> Specs { get; set; }
        ICollection<HubSpec> HubSpecs { get; set; }
        int StatusId { get; set; }
    }
}
