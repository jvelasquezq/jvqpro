﻿using System;
namespace Nhs.Mvc.Domain.Model.Hub.Interfaces
{
    public interface ITollFreeNumber
    {
        int BuilderId { get; set; }
        int CommunityId { get; set; }
        int PartnerId { get; set; }
        string PhoneNumber { get; set; }
    }
}
