//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Nhs.Mvc.Domain.Model.Web
{
    public partial class Plan
    {
        #region Primitive Properties
    
        public virtual int PlanId
        {
            get;
            set;
        }
    
        public virtual string PlanName
        {
            get;
            set;
        }
    
        public virtual int CommunityId
        {
            get { return _communityId; }
            set
            {
                if (_communityId != value)
                {
                    if (Community != null && Community.CommunityId != value)
                    {
                        Community = null;
                    }
                    _communityId = value;
                }
            }
        }
        private int _communityId;
    
        public virtual Nullable<int> SqFt
        {
            get;
            set;
        }
    
        public virtual string Status
        {
            get;
            set;
        }
    
        public virtual Nullable<int> Bedrooms
        {
            get;
            set;
        }
    
        public virtual Nullable<int> Bathrooms
        {
            get;
            set;
        }
    
        public virtual Nullable<decimal> Garages
        {
            get;
            set;
        }
    
        public virtual int IsHotHome
        {
            get;
            set;
        }
    
        public virtual Nullable<int> HalfBaths
        {
            get;
            set;
        }
    
        public virtual Nullable<int> Stories
        {
            get;
            set;
        }
    
        public virtual string PlanTypeCode
        {
            get;
            set;
        }
    
        public virtual string Description
        {
            get;
            set;
        }
    
        public virtual string HomeMarketingDescription
        {
            get;
            set;
        }
    
        public virtual decimal Price
        {
            get;
            set;
        }
    
        public virtual Nullable<int> NumLivingAreas
        {
            get;
            set;
        }
    
        public virtual Nullable<int> MasterBedroomLocation
        {
            get;
            set;
        }
    
        public virtual int LandExcluded
        {
            get;
            set;
        }
    
        public virtual int Floors
        {
            get;
            set;
        }
    
        public virtual int Location
        {
            get;
            set;
        }
    
        public virtual int Townhome
        {
            get;
            set;
        }
    
        public virtual int Condo
        {
            get;
            set;
        }
    
        public virtual string HotHomeTitle
        {
            get;
            set;
        }
    
        public virtual string HotHomeDescription
        {
            get;
            set;
        }
    
        public virtual string PromoTextLong
        {
            get;
            set;
        }
    
        public virtual string PromoTextShort
        {
            get;
            set;
        }
    
        public virtual string PromoUrl
        {
            get;
            set;
        }
    
        public virtual string VirtualTourUrl
        {
            get;
            set;
        }
    
        public virtual string ImageThumbnail
        {
            get;
            set;
        }

        #endregion

        #region Navigation Properties
    
        public virtual ICollection<Video> AllPlanVideos
        {
            get
            {
                if (_allPlanVideos == null)
                {
                    var newCollection = new FixupCollection<Video>();
                    newCollection.CollectionChanged += FixupAllPlanVideos;
                    _allPlanVideos = newCollection;
                }
                return _allPlanVideos;
            }
            set
            {
                if (!ReferenceEquals(_allPlanVideos, value))
                {
                    var previousValue = _allPlanVideos as FixupCollection<Video>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupAllPlanVideos;
                    }
                    _allPlanVideos = value;
                    var newValue = value as FixupCollection<Video>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupAllPlanVideos;
                    }
                }
            }
        }
        private ICollection<Video> _allPlanVideos;
    
        public virtual ICollection<Image> AllPlanImages
        {
            get
            {
                if (_allPlanImages == null)
                {
                    var newCollection = new FixupCollection<Image>();
                    newCollection.CollectionChanged += FixupAllPlanImages;
                    _allPlanImages = newCollection;
                }
                return _allPlanImages;
            }
            set
            {
                if (!ReferenceEquals(_allPlanImages, value))
                {
                    var previousValue = _allPlanImages as FixupCollection<Image>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupAllPlanImages;
                    }
                    _allPlanImages = value;
                    var newValue = value as FixupCollection<Image>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupAllPlanImages;
                    }
                }
            }
        }
        private ICollection<Image> _allPlanImages;
    
        public virtual Community Community
        {
            get { return _community; }
            set
            {
                if (!ReferenceEquals(_community, value))
                {
                    var previousValue = _community;
                    _community = value;
                    FixupCommunity(previousValue);
                }
            }
        }
        private Community _community;
    
        public virtual ICollection<Spec> Specs
        {
            get
            {
                if (_specs == null)
                {
                    var newCollection = new FixupCollection<Spec>();
                    newCollection.CollectionChanged += FixupSpecs;
                    _specs = newCollection;
                }
                return _specs;
            }
            set
            {
                if (!ReferenceEquals(_specs, value))
                {
                    var previousValue = _specs as FixupCollection<Spec>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupSpecs;
                    }
                    _specs = value;
                    var newValue = value as FixupCollection<Spec>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupSpecs;
                    }
                }
            }
        }
        private ICollection<Spec> _specs;

        #endregion

        #region Association Fixup
    
        private void FixupCommunity(Community previousValue)
        {
            if (previousValue != null && previousValue.Plans.Contains(this))
            {
                previousValue.Plans.Remove(this);
            }
    
            if (Community != null)
            {
                if (!Community.Plans.Contains(this))
                {
                    Community.Plans.Add(this);
                }
                if (CommunityId != Community.CommunityId)
                {
                    CommunityId = Community.CommunityId;
                }
            }
        }
    
        private void FixupAllPlanVideos(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Video item in e.NewItems)
                {
                    item.Plan = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (Video item in e.OldItems)
                {
                    if (ReferenceEquals(item.Plan, this))
                    {
                        item.Plan = null;
                    }
                }
            }
        }
    
        private void FixupAllPlanImages(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Image item in e.NewItems)
                {
                    item.Plan = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (Image item in e.OldItems)
                {
                    if (ReferenceEquals(item.Plan, this))
                    {
                        item.Plan = null;
                    }
                }
            }
        }
    
        private void FixupSpecs(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Spec item in e.NewItems)
                {
                    item.Plan = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (Spec item in e.OldItems)
                {
                    if (ReferenceEquals(item.Plan, this))
                    {
                        item.Plan = null;
                    }
                }
            }
        }

        #endregion

    }
}
