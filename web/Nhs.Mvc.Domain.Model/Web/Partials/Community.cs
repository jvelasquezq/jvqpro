﻿using System.Collections.Generic;
using System.Linq;
using Nhs.Mvc.Domain.Model.Web.Interfaces;

namespace Nhs.Mvc.Domain.Model.Web
{
    public partial class Community
    {
        public virtual int FeaturedListingId { get; set; }
        public virtual string ImageThumbnail { get; set; }
        public virtual bool Selected { get; set; }
        public virtual double DistanceFromCenter { get; set; }
        public virtual string ViewBrochureUrl { get; set; }

        public IEnumerable<IListing> Listings
        {
            get
            {
                IEnumerable<IListing> listings = this.Plans;
                listings.Union(this.Specs);
                return listings;
            }
        }

        public IEnumerable<IAmenity> IAmenities
        {
            get
            {
                return this.Amenities.ToList().Cast<IAmenity>();
            }
        }

        public IEnumerable<IAmenity> IOpenAmenities
        {
            get
            {
                return this.OpenAmenities.ToList().OrderBy(am => am.AmenitySequence).Cast<IAmenity>();
            }
        }
    }
}
