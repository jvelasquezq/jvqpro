﻿using System;
using System.Collections.Generic;

namespace Nhs.Mvc.Domain.Model.Web
{
    public partial class Builder : IEquatable<Builder>
    {
        public bool Equals(Builder other)
        {
            return this.BuilderId == other.BuilderId;
        }

        public bool IsBilled { get; set; }
    }

    public class BuilderComparer : EqualityComparer<Builder>
    {
        public override bool Equals(Builder b1, Builder b2)
        {
            return b1.BuilderId == b2.BuilderId;
        }

        public override int GetHashCode(Builder builder)
        {
            return builder.BuilderId.GetHashCode();
        }

    }
}
