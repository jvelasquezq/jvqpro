﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Mvc.Domain.Model.Web
{
    public partial class GreenProgram
    {
        public string ProgramName { get; set; }
        public string ProgramUrl{ get; set; }
        public string ProgramFlyerFilePath { get; set; }
        public int CommunityId { get; set; }
    }
}
