﻿using System.Collections.Generic;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Domain.Model.Web
{
    public partial class Market
    {
        public IList<SchoolDistrict> SchoolDistricts { get; set; }
        public IList<Brand> Brands { get; set; }
        public int TotalCommunities { get; set; }
        public int TotalComingSoon { get; set; }
        public int TotalCustomBuilders { get; set; }
        public int TotalCommsWithPromos { get; set; }
        public int TotalQuickMoveInHomes { get; set; }
        public int TotalHomes { get; set; }
        public int PriceLow { get; set; }
        public int PriceHigh { get; set; }
        public LatLong LatLong { get; set; }
        public LatLong StateLatLong { get; set; }
    }
}
