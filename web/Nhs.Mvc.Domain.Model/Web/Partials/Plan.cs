﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nhs.Mvc.Domain.Model.Hub;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;

namespace Nhs.Mvc.Domain.Model.Web
{
    public partial class Plan : IPlan
    {
        public int Id
        {
            get { return this.PlanId; }
        }

        public ICollection<IImage> PlanImages
        {
            get
            {
                return (from i in this.AllPlanImages
                        where i.SpecificationId == null
                        select i as IImage).ToList();
            }
        }

        public ICollection<IVideo> PlanVideos
        {
            get
            {
                return (from i in this.AllPlanVideos
                        where i.SpecificationId == null
                        select i as IVideo).ToList();
            }
        }

        public string EnvisionUrl
        {
            get
            {
                var image = (from i in this.AllPlanImages
                             where i.ImageTypeCode == "EDC"
                             select i).FirstOrDefault();

                return image != null ? image.OriginalPath : string.Empty;
            }
        }

        public ICollection<HubVideo> AllPlanHubVideos { get; set; }
        public ICollection<HubImage> AllPlanHubImages { get; set; }
        public ICollection<HubSpec> HubSpecs { get; set; }

        public HubCommunity HubCommunity
        {
            get { throw new NotImplementedException(); }
        }
        public int StatusId { get; set; }
    }
}