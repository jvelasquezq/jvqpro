﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Mvc.Domain.Model.Web
{
    public class NearbyCommunity:Community
    {
        public decimal? DistanceFromCommunity { get; set; }
        public string BrandName { get; set; }
        public string StateName { get; set; }
        public string MarketName { get; set; }
        public string NonPdfBrochureUrl { get; set; }
        public string RequestItemId { get; set; }
        public string Price {
            get { return PriceLow.ToString("$#,##0") + " - " + PriceHigh.ToString("$#,##0"); }
        }
    }
}
