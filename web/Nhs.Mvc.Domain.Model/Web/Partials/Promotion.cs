﻿using System;
using System.Collections.Generic;
using Nhs.Utility.Html;

namespace Nhs.Mvc.Domain.Model.Web
{
    /// <summary>
    /// We do not use EF for this entity since the DB schema is not optimized for a clean EF relationship
    /// </summary>
    public class Promotion
    {
        public string PromoTextFormatted
        {
            get
            {
                var promo = ParseHTML.StripHTML(PromoTextLong);
                if (promo.Length <= 90) return promo;
                string chunk = promo.Substring(0, 90);

                int lastWordCompleted = chunk.LastIndexOf(' ');
                string sentenceTruncated = chunk.Substring(0, lastWordCompleted).TrimEnd();
                if (sentenceTruncated[sentenceTruncated.Length - 1] == '.')
                    sentenceTruncated = sentenceTruncated.Remove(sentenceTruncated.Length - 1);

                return string.Concat(sentenceTruncated, "...");
            }
        }

        public string InfoFutureDate
        {
            get
            {
                var dateText = string.Empty;

                if (PromoStartDate > DateTime.Now)
                    dateText = string.Format("{0} {1}:", "Beginning", PromoStartDate.ToString("MM/dd"));
                return dateText;
            }
        }

        public string PromoDateTextFormatted
        {
            get
            { 
                string dateText = PromoStartDate.ToString("MM/dd");

                if (PromoStartDate < DateTime.Now)
                    dateText = "Through";

                dateText += " - " + PromoEndDate.ToString("MM/dd ");

                if (PromoEndDate.Date.CompareTo(DateTime.ParseExact("2020-01-01", "yyyy-MM-dd", null).Date) == 0)
                    dateText = "Ongoing ";

                return dateText;

            }
        }

        public string PromoShortterTextFormatted
        {
            get
            {
                string promo = ParseHTML.StripHTML(PromoTextShort);
                if (promo == null)
                    return null;
                if (promo.Length <= 45) return promo;
                string chunk = promo.Substring(0, 40);

                int lastWordCompleted = chunk.LastIndexOf(' ');
                string sentenceTruncated = chunk.Substring(0, lastWordCompleted).TrimEnd();
                if (sentenceTruncated[sentenceTruncated.Length - 1] == '.')
                    sentenceTruncated = sentenceTruncated.Remove(sentenceTruncated.Length - 1);

                return string.Concat(sentenceTruncated, "...");
            }
        }

        public string PromoShortTextFormatted
        {
            get
            {
                string promo = ParseHTML.StripHTML(PromoTextShort);
                if (promo.Length <= 90) return promo;
                string chunk = promo.Substring(0, 80);

                int lastWordCompleted = chunk.LastIndexOf(' ');
                string sentenceTruncated = chunk.Substring(0, lastWordCompleted).TrimEnd();
                if (sentenceTruncated[sentenceTruncated.Length - 1] == '.')
                    sentenceTruncated = sentenceTruncated.Remove(sentenceTruncated.Length - 1);

                return string.Concat(sentenceTruncated, "...");
            }
        }

        public int PromoId { get; set; }
        public string PromoName { get; set; }
        public string PromoUrl { get; set; }
        public string PromoType { get; set; }
        public string PromoTextShort { get; set; }
        public string PromoTextLong { get; set; }
        public string PromoFlyerUrl { get; set; }
        public DateTime PromoStartDate { get; set; }
        public DateTime PromoEndDate { get; set; }
        public int CommunityId { get; set; }
    
    }

    public class CommunityPromotionInfo : Promotion
    {
        public int BuilderId { get; set; }
        public int BrandId { get; set; }
        public string CommunityName { get; set; }
        public string BuilderName { get; set; }
        public string State { get; set; }
        public string MarketName { get; set; }
        public string City { get; set; }

        public bool IsBilled { get; set; }

    }
}
