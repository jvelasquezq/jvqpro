﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Mvc.Domain.Model.Web
{
    public class Testimonial
    {
        public string Description { get; set; }
        public string Citation { get; set; }
    }
}
