﻿namespace Nhs.Mvc.Domain.Model.Web
{
    public class HomeOption
    {
        public int OptionId { get; set; }
        public string OptionName { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public int Living { get; set; }
        public int Beds { get; set; }
        public decimal Garages { get; set; }
        public int Baths { get; set; }
        public int SquareFootage { get; set; }
    }
}
