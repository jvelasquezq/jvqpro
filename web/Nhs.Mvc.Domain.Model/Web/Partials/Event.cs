﻿using System;
using Nhs.Utility.Html;

namespace Nhs.Mvc.Domain.Model.Web
{
    /// <summary>
    /// We do not use EF for this entity since the DB schema is not optimized for a clean EF relationship
    /// </summary>
    public class Event
    {


        public string EventDateTextFormatted
        {
            get
            {
                return string.Format("{0} - {1}",
                                     EventStartDate.ToString("MM/dd hh:mm tt"),
                                     EventEndDate.ToString("hh:mm tt"));
                
            }
        }

        public string EventTextFormatted
        {
            get
            {
                string promo = ParseHTML.StripHTML(Title);
                if (Title == null)
                    return null;
                if (Title.Length <= 45) return Title;
                string chunk = Title.Substring(0, 40);

                int lastWordCompleted = chunk.LastIndexOf(' ');
                string sentenceTruncated = chunk.Substring(0, lastWordCompleted).TrimEnd();
                if (sentenceTruncated[sentenceTruncated.Length - 1] == '.')
                    sentenceTruncated = sentenceTruncated.Remove(sentenceTruncated.Length - 1);

                return string.Concat(sentenceTruncated, "...");
            }
        }

        public int EventId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string EventType { get; set; }

        public string EventFlyerUrl { get; set; }

        public DateTime EventStartDate { get; set; }

        public DateTime EventEndDate { get; set; }
    }

    public class CommunityEventInfo : Event
    {
        public int CommunityId { get; set; }
        public int BuilderId { get; set; }
        public int BrandId { get; set; }
        public bool IsBilled { get; set; }

        public string CommunityName { get; set; }
        public string BuilderName { get; set; }

        public string Address { get; set; }
    }
}
