﻿namespace Nhs.Mvc.Domain.Model.Web.Partials
{
    public class LeadEmail
    {
        public int MarketId { get; set; }
        public string RequestTypeCode { get; set; }
    }
}