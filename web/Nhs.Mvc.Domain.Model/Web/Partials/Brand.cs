﻿using System;
using System.Collections.Generic;

namespace Nhs.Mvc.Domain.Model.Web
{
    public partial class Brand : IEquatable<Brand>
    {
        public bool Equals(Brand other)
        {
            return this.BrandId == other.BrandId;
        }

        public virtual bool IsBoyl { get; set; }

        public virtual bool IsCustomBuilder { get; set; }

        public virtual bool HasBilledCommununities { get; set; }

        public virtual string State { get; set; }
        public virtual int MarketId { get; set; }
        public virtual string MarketName { get; set; }
        
        public bool IsBasic { get; set; }

        public bool HasShowCase
        {
            get { return BrandShowCase != null; }
        }

    }

    public class BrandComparer : EqualityComparer<Brand>
    {

        public override bool Equals(Brand b1, Brand b2)
        {
            return b1.BrandId == b2.BrandId;
        }

        public override int GetHashCode(Brand brd)
        {
            return brd.BrandId.GetHashCode();
        }

    }

   
}
