﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nhs.Mvc.Domain.Model.Hub;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;

namespace Nhs.Mvc.Domain.Model.Web
{
    public partial class Spec : ISpec
    {
        public ICollection<IImage> SpecImages
        {
            get
            {
                return (from i in Plan.AllPlanImages
                        where i.SpecificationId == SpecId
                        select i as IImage).ToList();
            }
        }

        public ICollection<IVideo> SpecVideos
        {
            get
            {
                return (from v in Plan.AllPlanVideos
                        where v.SpecificationId == SpecId
                        select v as IVideo).ToList();
            }
        }
        public int Id { get { return this.SpecId; } }

        public int HomeStatus
        {
            get
            {
                if (string.IsNullOrEmpty(SpecType.Trim()) && MoveInDate < DateTime.Now)
                    return 1; //Ready to move-in
                if (string.IsNullOrEmpty(SpecType.Trim()) && (MoveInDate > DateTime.Now || MoveInDate == null))
                    return 2; //Under construction
                if (SpecType.ToLower().Trim() == "m") 
                    return 4;

                return 3;
            }
        }

        public HubPlan HubPlan
        {
            get{throw new NotImplementedException();}
            set{throw new NotImplementedException();}
        }

        public HubCommunity HubCommunity
        {
            get { throw new NotImplementedException(); }
        }
    }
}
