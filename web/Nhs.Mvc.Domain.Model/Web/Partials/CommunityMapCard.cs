﻿namespace Nhs.Mvc.Domain.Model.Web.Partials
{
    public class CommunityMapCard
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string St { get; set; }
        public string StName { get; set; }
        public string Zip { get; set; }
        public int MId { get; set; }
        public string MName { get; set; }
        public int BId { get; set; }
        public string Brand { get; set; }
        public int PrLo { get; set; }
        public int PrHi { get; set; }
        public string Image { get; set; }
        public string Ct { get; set; }
        public int NunHom { get; set; }
        public bool IsBasic { get; set; }
        public bool IsBasicListing { get; set; }
        public bool ForceHeader { get; set; }
        public int MinBath { get; set; }
        public int MaxBath { get; set; }
        public int MinBedroom { get; set; }
        public int MaxBedroom { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsFavoriteListing { get; set; }
        public string BrandImageUrl { get; set; }
    }
}
