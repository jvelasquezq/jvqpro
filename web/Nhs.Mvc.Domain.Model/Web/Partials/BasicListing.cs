﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Mvc.Domain.Model.BasicListings
{
    public partial class BasicListing
    {
        public string City
        {
            get { return LocationInfo.City; }
        }

        public string State
        {
            get { return LocationInfo.State; }
        }

        public string PostalCode
        {
            get { return LocationInfo.PostalCode; }
        }

        public int FormattedListPrice
        {
            get { return ListPrice != null ? (int)ListPrice : 0; }
        }

        public decimal FormattedLivingArea
        {
            get { return LivingArea != null ? (decimal)LivingArea : 0; }
        }

        public string Bedrooms
        {
            get
            {
                var br = this.ListingCodes.Where(lc => lc.ListCode.Trim() == "BED").FirstOrDefault();
                return br == null ? string.Empty : br.ListValue;
            }
        }

        public string Bathrooms
        {
            get
            {
                var br = this.ListingCodes.Where(lc => lc.ListCode.Trim() == "BA").FirstOrDefault();
                return br == null ? string.Empty : br.ListValue;
            }
        }

        public BasicListingImage FirstListingImage
        {
            get { return (Images != null) ? Images.OrderBy(i => i.ImageSequence).Where(i => i.DisplayStatusId == 5).FirstOrDefault() : null; }
        }

        public string AgentName
        {
            get { return (Agent != null) ? Agent.AgentName : String.Empty; }
        }

        public string AgentPhone
        {
            get { return (Agent != null) ? Agent.OfficePhone : String.Empty; }
        }

        public AgentImage FirstAgentPhoto
        {
            get { return (Agent != null) ? Agent.Images.OrderBy(i => i.ImageSequence).Where(i => i.DisplayStatusId == 5).FirstOrDefault() : null; }
        }

        public string BrokerSiteUrl
        {
            get { return (Agent != null && Agent.Broker != null) ? Agent.Broker.WebsiteUrl : String.Empty; }
        }

        public AgentImage FirstBrokerPhoto
        {
            get { return (Agent != null && Agent.Broker != null) ? Agent.Broker.Images.OrderBy(i => i.ImageSequence).Where(i => i.DisplayStatusId == 5).FirstOrDefault() : null; }
        }

        public string BrokerName
        {
            get { return (Agent.Broker != null) ? Agent.Broker.AgentName : String.Empty; }
        }

        public int MarketId { get; set; }
    }
}