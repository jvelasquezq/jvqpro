﻿using System.Collections.Generic;

namespace Nhs.Mvc.Domain.Model.Web.Partials
{
    public class LeadEmailItem
    {
        public bool IsComm { get; set; }
        public bool IsSpec { get; set; }
        public string PropertyName { get; set; }
        public string RequestItemId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public int SpecId { get; set; }
        public int PlanId { get; set; }
        public int CommunityId { get; set; }
        public int BuilderId { get; set; }
        public int MarketId { get; set; }
        public string RequestTypeCode { get; set; }
        public string EmailAddress { get; set; }
        public string PlanName { get; set; }
        public string CommunityName { get; set; }
        public string BrandName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string ImageThumbnail { get; set; }
        public string DetailPageUrl { get; set; }
        public string BrandImage { get; set; }
        public string BuilderUrl { get; set; }
        public string HotHomeTitle { get; set; }
        public string HotHomeDescription { get; set; }
        public string FormatPrice { get; set; }
        public string FormatHomeAttrib { get; set; }
        public string BrochureUrl { get; set; }
        public bool ShowHotArea { get; set; }
        public bool ShowPromoArea { get; set; }
        public bool IsMove { get; set; }
        public IEnumerable<CommunityPromotion> Promos { get; set; }

        public string UniqueKey { get; set; }
        public double Distance { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        
        public string SourcePropertyName { get; set; }
    }
}