﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Mvc.Domain.Model.Web
{
    public class NearbyHome 
    {
        public int CommunityId { get; set; }
        public int BuilderId { get; set; }
        public int PlanId { get; set; }

        public string CommunityName { get; set; }
        public string PlanName { get; set; }
        public string ImageThumbnail { get; set; }
        public string BcType { get; set; }
        public int BedroomCount { get; set; }
        public int BathroomCount { get; set; }
        public int SquareFeet { get; set; }
        public decimal Price { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public bool HasVideo { get; set; }
        public decimal? DistanceFromCommunity { get; set; }
    }
}
