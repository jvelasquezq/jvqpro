﻿namespace Nhs.Mvc.Domain.Model.Web
{
    public class Utility
    {
        public string CommunityServiceDescription { get; set; }
        public string CommunityServicePhone { get; set; }
        public string CommunityServiceTypeCode { get; set; }
        public string CommunityServiceTypeName { get; set; }
        public decimal CommunityServiceMonthlyFee { get; set; }
        public decimal CommunityServiceYearlyFee { get; set; }
    }
}
