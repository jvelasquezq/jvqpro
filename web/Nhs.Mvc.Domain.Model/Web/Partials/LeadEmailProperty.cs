﻿using System;
using System.Collections.Generic;

namespace Nhs.Mvc.Domain.Model.Web.Partials
{
    public class LeadEmailProperty
    {
        public DateTime BrochureExpiration { get; set; }
        public string BrochureName { get; set; }
        public string RequestItemId { get; set; }
        public string RequestItemDeliveryId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public int SpecId { get; set; }
        public int PlanId { get; set; }
        public int CommunityId { get; set; }
        public int BuilderId { get; set; }
        public int MarketId { get; set; }
        public int IsHotHome { get; set; }
        public string RequestTypeCode { get; set; }
        public string EmailAddress { get; set; }
        public string PlanName { get; set; }
        public string PropertyHeaderText { get; set; }
        public string CommunityName { get; set; }
        public string BrandName { get; set; }
        
        public int NumBedrooms { get; set; }
        public int NumBaths { get; set; }
        public int NumHalfBaths { get; set; }
        public decimal NumGarages { get; set; }

        public string HomeStatus { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string ImageThumbnail { get; set; }        
        public string MarketName { get; set; }
        public string BuilderName { get; set; }
        public string DetailPageText { get; set; }
        public string DetailPageUrl { get; set; }

        public decimal PriceLow { get; set; }
        public decimal PriceHigh { get; set; }
        public decimal Price { get; set; }

        public string BuilderUrl { get; set; }
        public string HotHomeTitle { get; set; }
        public string FormatPrice { get; set; }
        public string FormatHomeAttrib{ get; set; }
        public string BrochureUrl { get; set; }
        
        public bool ShowHotArea { get; set; }
        public bool ShowPromoArea { get; set; }

        public IEnumerable<Promotion> Promos { get; set; }

        public string UniqueKey { get; set; }
    }
}
