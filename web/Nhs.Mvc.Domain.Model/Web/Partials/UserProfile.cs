﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Domain.Model.Profiles
{
    public partial class 
        UserProfile
    {
        public int PartnerId
        {
            get { return StrPartnerId.ToType<int>(); }
            set { StrPartnerId = value.ToString(); }
        }

    }
}
