﻿
namespace Nhs.Mvc.Domain.Model.Web.Entities
{
    public class TypeaheadBuilder
    {
        public int BuilderId { get; set; }
        public string BuilderName { get; set; }
    }
}
