﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Mvc.Domain.Model.Web
{
    public class SEOCommunity
    {
        public string Name { get; set; }
        public string MarketName { get; set; }
        public int MarketId { get; set; }
        public string State { get; set; }

    }
}
