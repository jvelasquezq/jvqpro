﻿namespace Nhs.Mvc.Domain.Model.Web.Entities
{
    public class TypeaheadBrand
    {
        public string BrandName { get; set; }
        public string BrandId { get; set; }
    }
}
