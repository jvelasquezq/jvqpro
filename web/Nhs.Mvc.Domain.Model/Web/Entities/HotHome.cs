﻿namespace Nhs.Mvc.Domain.Model.Web.Entities
{
    public class HotHome
    {
        public string NumBathrooms { get; set; }
        public string NumBedrooms { get; set; }
        public string NumGarages { get; set; }
        public string NumHalfBathrooms { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public int CommunityId { get; set; }
        public string Name { get; set; }
        public string PlanId { get; set; }
        public string SpecId { get; set; }
        public string SpecAddress { get; set; }
    }
}