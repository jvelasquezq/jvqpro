﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nhs.Mvc.Domain.Model.Web
{
    public class Location
    {
        public string Name { get; set; }

        // Type of the location as described below
        // 1 : Market
        // 2 : City
        // 3 : County
        // 4 : Zip
        // 5 : Comm name
        // 6 : Developer Name 
        public int Type { get; set; }
        public int MarketId { get; set; }
        public string MarketName { get; set; }
        public string State { get; set; }
        public Market Market { get; set; }
        public bool StartWith { get; set; }
    }
}
