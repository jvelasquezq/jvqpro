﻿namespace Nhs.Mvc.Domain.Model.Web.Entities
{
    public class TypeaheadCity
    {
        public string CityName { get; set; }
        public string CityId { get; set; }
    }
}
