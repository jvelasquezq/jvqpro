﻿
namespace Nhs.Mvc.Domain.Model.Web.Entities
{
    public class TypeaheadCommunity
    {
        public int CommunityId { get; set; }
        public string CommunityName { get; set; }
        public string CityName { get; set; }
        public string State { get; set; }
    }
}
