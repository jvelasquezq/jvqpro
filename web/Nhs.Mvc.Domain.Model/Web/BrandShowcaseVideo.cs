//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Nhs.Mvc.Domain.Model.Web
{
    public partial class BrandShowcaseVideo
    {
        #region Primitive Properties
    
        public virtual int BrandId
        {
            get;
            set;
        }
    
        public virtual int VideoId
        {
            get;
            set;
        }

        #endregion

    }
}
