//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Nhs.Mvc.Domain.Model.Web
{
    public partial class Brand
    {
        #region Primitive Properties
    
        public virtual int BrandId
        {
            get;
            set;
        }
    
        public virtual string BrandName
        {
            get;
            set;
        }
    
        public virtual string LogoMedium
        {
            get;
            set;
        }
    
        public virtual string LogoSmall
        {
            get;
            set;
        }
    
        public virtual string SiteURL
        {
            get;
            set;
        }

        #endregion

        #region Navigation Properties
    
        public virtual ICollection<Builder> Builders
        {
            get
            {
                if (_builders == null)
                {
                    var newCollection = new FixupCollection<Builder>();
                    newCollection.CollectionChanged += FixupBuilders;
                    _builders = newCollection;
                }
                return _builders;
            }
            set
            {
                if (!ReferenceEquals(_builders, value))
                {
                    var previousValue = _builders as FixupCollection<Builder>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupBuilders;
                    }
                    _builders = value;
                    var newValue = value as FixupCollection<Builder>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupBuilders;
                    }
                }
            }
        }
        private ICollection<Builder> _builders;
    
        public virtual ICollection<Community> Communities
        {
            get
            {
                if (_communities == null)
                {
                    var newCollection = new FixupCollection<Community>();
                    newCollection.CollectionChanged += FixupCommunities;
                    _communities = newCollection;
                }
                return _communities;
            }
            set
            {
                if (!ReferenceEquals(_communities, value))
                {
                    var previousValue = _communities as FixupCollection<Community>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupCommunities;
                    }
                    _communities = value;
                    var newValue = value as FixupCollection<Community>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupCommunities;
                    }
                }
            }
        }
        private ICollection<Community> _communities;
    
        public virtual ICollection<BrandTestimonial> BrandTestimonials
        {
            get
            {
                if (_brandTestimonials == null)
                {
                    var newCollection = new FixupCollection<BrandTestimonial>();
                    newCollection.CollectionChanged += FixupBrandTestimonials;
                    _brandTestimonials = newCollection;
                }
                return _brandTestimonials;
            }
            set
            {
                if (!ReferenceEquals(_brandTestimonials, value))
                {
                    var previousValue = _brandTestimonials as FixupCollection<BrandTestimonial>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupBrandTestimonials;
                    }
                    _brandTestimonials = value;
                    var newValue = value as FixupCollection<BrandTestimonial>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupBrandTestimonials;
                    }
                }
            }
        }
        private ICollection<BrandTestimonial> _brandTestimonials;
    
        public virtual BrandShowCase BrandShowCase
        {
            get { return _brandShowCase; }
            set
            {
                if (!ReferenceEquals(_brandShowCase, value))
                {
                    var previousValue = _brandShowCase;
                    _brandShowCase = value;
                    FixupBrandShowCase(previousValue);
                }
            }
        }
        private BrandShowCase _brandShowCase;

        #endregion

        #region Association Fixup
    
        private void FixupBrandShowCase(BrandShowCase previousValue)
        {
            if (previousValue != null && ReferenceEquals(previousValue.Brand, this))
            {
                previousValue.Brand = null;
            }
    
            if (BrandShowCase != null)
            {
                BrandShowCase.Brand = this;
            }
        }
    
        private void FixupBuilders(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Builder item in e.NewItems)
                {
                    item.Brand = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (Builder item in e.OldItems)
                {
                    if (ReferenceEquals(item.Brand, this))
                    {
                        item.Brand = null;
                    }
                }
            }
        }
    
        private void FixupCommunities(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Community item in e.NewItems)
                {
                    item.Brand = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (Community item in e.OldItems)
                {
                    if (ReferenceEquals(item.Brand, this))
                    {
                        item.Brand = null;
                    }
                }
            }
        }
    
        private void FixupBrandTestimonials(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (BrandTestimonial item in e.NewItems)
                {
                    item.Brand = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (BrandTestimonial item in e.OldItems)
                {
                    if (ReferenceEquals(item.Brand, this))
                    {
                        item.Brand = null;
                    }
                }
            }
        }

        #endregion

    }
}
