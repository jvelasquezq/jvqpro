//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Nhs.Mvc.Domain.Model.Web
{
    public partial class Amenity
    {
        #region Primitive Properties
    
        public virtual int CommunityId
        {
            get { return _communityId; }
            set
            {
                if (_communityId != value)
                {
                    if (Community != null && Community.CommunityId != value)
                    {
                        Community = null;
                    }
                    _communityId = value;
                }
            }
        }
        private int _communityId;
    
        public virtual int AmenityId
        {
            get;
            set;
        }
    
        public virtual string Description
        {
            get;
            set;
        }
    
        public virtual string Url
        {
            get;
            set;
        }

        #endregion

        #region Navigation Properties
    
        public virtual Community Community
        {
            get { return _community; }
            set
            {
                if (!ReferenceEquals(_community, value))
                {
                    var previousValue = _community;
                    _community = value;
                    FixupCommunity(previousValue);
                }
            }
        }
        private Community _community;

        #endregion

        #region Association Fixup
    
        private void FixupCommunity(Community previousValue)
        {
            if (previousValue != null && previousValue.Amenities.Contains(this))
            {
                previousValue.Amenities.Remove(this);
            }
    
            if (Community != null)
            {
                if (!Community.Amenities.Contains(this))
                {
                    Community.Amenities.Add(this);
                }
                if (CommunityId != Community.CommunityId)
                {
                    CommunityId = Community.CommunityId;
                }
            }
        }

        #endregion

    }
}
