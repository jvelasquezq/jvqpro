//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Nhs.Mvc.Domain.Model.Web
{
    public partial class CommunityAgentPolicy
    {
        #region Primitive Properties
    
        public virtual int BuilderID
        {
            get;
            set;
        }
    
        public virtual int CommunityID
        {
            get;
            set;
        }
    
        public virtual int PolicyID
        {
            get;
            set;
        }
    
        public virtual string PromoURL
        {
            get;
            set;
        }
    
        public virtual string PolicyName
        {
            get;
            set;
        }
    
        public virtual string PolicyFlyerURL
        {
            get;
            set;
        }

        #endregion

    }
}
