//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Nhs.Mvc.Domain.Model.Web
{
    public partial class BuilderService
    {
        #region Primitive Properties
    
        public virtual int BuilderId
        {
            get;
            set;
        }
    
        public virtual int ServiceId
        {
            get;
            set;
        }
    
        public virtual Nullable<bool> Active
        {
            get;
            set;
        }
    
        public virtual Nullable<bool> EmailNotification
        {
            get;
            set;
        }
    
        public virtual System.DateTime DateCreated
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> DateLastChanged
        {
            get;
            set;
        }

        #endregion

    }
}
