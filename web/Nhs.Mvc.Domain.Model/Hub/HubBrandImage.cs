//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Nhs.Mvc.Domain.Model.Hub
{
    public partial class HubBrandImage
    {
        #region Primitive Properties
    
        public virtual int ImageID
        {
            get;
            set;
        }
    
        public virtual int BrandID
        {
            get { return _brandID; }
            set
            {
                if (_brandID != value)
                {
                    if (HubBrand != null && HubBrand.BrandId != value)
                    {
                        HubBrand = null;
                    }
                    _brandID = value;
                }
            }
        }
        private int _brandID;
    
        public virtual Nullable<int> BuilderID
        {
            get;
            set;
        }
    
        public virtual Nullable<int> CommunityID
        {
            get;
            set;
        }
    
        public virtual Nullable<int> PlanID
        {
            get;
            set;
        }
    
        public virtual Nullable<int> SpecificationID
        {
            get;
            set;
        }
    
        public virtual string ImageName
        {
            get;
            set;
        }
    
        public virtual string ImagePath
        {
            get;
            set;
        }
    
        public virtual string OriginalPath
        {
            get;
            set;
        }
    
        public virtual string ImageTitle
        {
            get;
            set;
        }
    
        public virtual string ImageDescription
        {
            get;
            set;
        }
    
        public virtual string ImageTypeCode
        {
            get;
            set;
        }
    
        public virtual Nullable<int> ImageSequence
        {
            get;
            set;
        }
    
        public virtual string ClickThruURL
        {
            get;
            set;
        }
    
        public virtual string PrimaryFlag
        {
            get;
            set;
        }
    
        public virtual Nullable<byte> ProcessorStatusID
        {
            get;
            set;
        }

        #endregion
        #region Navigation Properties
    
        public virtual HubBrand HubBrand
        {
            get { return _hubBrand; }
            set
            {
                if (!ReferenceEquals(_hubBrand, value))
                {
                    var previousValue = _hubBrand;
                    _hubBrand = value;
                    FixupHubBrand(previousValue);
                }
            }
        }
        private HubBrand _hubBrand;

        #endregion
        #region Association Fixup
    
        private void FixupHubBrand(HubBrand previousValue)
        {
            if (previousValue != null && previousValue.HubBrandImages.Contains(this))
            {
                previousValue.HubBrandImages.Remove(this);
            }
    
            if (HubBrand != null)
            {
                if (!HubBrand.HubBrandImages.Contains(this))
                {
                    HubBrand.HubBrandImages.Add(this);
                }
                if (BrandID != HubBrand.BrandId)
                {
                    BrandID = HubBrand.BrandId;
                }
            }
        }

        #endregion
    }
}
