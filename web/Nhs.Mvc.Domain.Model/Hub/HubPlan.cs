//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Nhs.Mvc.Domain.Model.Hub
{
    public partial class HubPlan
    {
        #region Primitive Properties
    
        public virtual int CommunityId
        {
            get { return _communityId; }
            set
            {
                if (_communityId != value)
                {
                    if (HubCommunity != null && HubCommunity.CommunityId != value)
                    {
                        HubCommunity = null;
                    }
                    _communityId = value;
                }
            }
        }
        private int _communityId;
    
        public virtual int PlanId
        {
            get;
            set;
        }
    
        public virtual string Status
        {
            get;
            set;
        }
    
        public virtual string PlanName
        {
            get;
            set;
        }
    
        public virtual string PlanTypeCode
        {
            get;
            set;
        }
    
        public virtual string Description
        {
            get;
            set;
        }
    
        public virtual string HomeMarketingDescription
        {
            get;
            set;
        }
    
        public virtual decimal Price
        {
            get;
            set;
        }
    
        public virtual Nullable<int> SqFt
        {
            get;
            set;
        }
    
        public virtual string PlanDetails
        {
            get;
            set;
        }
    
        public virtual int Townhome
        {
            get;
            set;
        }
    
        public virtual int Condo
        {
            get;
            set;
        }
    
        public virtual string HotHomeDescription
        {
            get;
            set;
        }
    
        public virtual string VirtualTourUrl
        {
            get;
            set;
        }
    
        public virtual int StatusId
        {
            get;
            set;
        }

        #endregion
        #region Navigation Properties
    
        public virtual HubCommunity HubCommunity
        {
            get { return _hubCommunity; }
            set
            {
                if (!ReferenceEquals(_hubCommunity, value))
                {
                    var previousValue = _hubCommunity;
                    _hubCommunity = value;
                    FixupHubCommunity(previousValue);
                }
            }
        }
        private HubCommunity _hubCommunity;
    
        public virtual ICollection<HubSpec> HubSpecs
        {
            get
            {
                if (_hubSpecs == null)
                {
                    var newCollection = new FixupCollection<HubSpec>();
                    newCollection.CollectionChanged += FixupHubSpecs;
                    _hubSpecs = newCollection;
                }
                return _hubSpecs;
            }
            set
            {
                if (!ReferenceEquals(_hubSpecs, value))
                {
                    var previousValue = _hubSpecs as FixupCollection<HubSpec>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupHubSpecs;
                    }
                    _hubSpecs = value;
                    var newValue = value as FixupCollection<HubSpec>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupHubSpecs;
                    }
                }
            }
        }
        private ICollection<HubSpec> _hubSpecs;
    
        public virtual ICollection<HubVideo> AllPlanHubVideos
        {
            get
            {
                if (_allPlanHubVideos == null)
                {
                    var newCollection = new FixupCollection<HubVideo>();
                    newCollection.CollectionChanged += FixupAllPlanHubVideos;
                    _allPlanHubVideos = newCollection;
                }
                return _allPlanHubVideos;
            }
            set
            {
                if (!ReferenceEquals(_allPlanHubVideos, value))
                {
                    var previousValue = _allPlanHubVideos as FixupCollection<HubVideo>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupAllPlanHubVideos;
                    }
                    _allPlanHubVideos = value;
                    var newValue = value as FixupCollection<HubVideo>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupAllPlanHubVideos;
                    }
                }
            }
        }
        private ICollection<HubVideo> _allPlanHubVideos;
    
        public virtual ICollection<HubImage> AllPlanHubImages
        {
            get
            {
                if (_allPlanHubImages == null)
                {
                    var newCollection = new FixupCollection<HubImage>();
                    newCollection.CollectionChanged += FixupAllPlanHubImages;
                    _allPlanHubImages = newCollection;
                }
                return _allPlanHubImages;
            }
            set
            {
                if (!ReferenceEquals(_allPlanHubImages, value))
                {
                    var previousValue = _allPlanHubImages as FixupCollection<HubImage>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupAllPlanHubImages;
                    }
                    _allPlanHubImages = value;
                    var newValue = value as FixupCollection<HubImage>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupAllPlanHubImages;
                    }
                }
            }
        }
        private ICollection<HubImage> _allPlanHubImages;

        #endregion
        #region Association Fixup
    
        private void FixupHubCommunity(HubCommunity previousValue)
        {
            if (previousValue != null && previousValue.Plans.Contains(this))
            {
                previousValue.Plans.Remove(this);
            }
    
            if (HubCommunity != null)
            {
                if (!HubCommunity.Plans.Contains(this))
                {
                    HubCommunity.Plans.Add(this);
                }
                if (CommunityId != HubCommunity.CommunityId)
                {
                    CommunityId = HubCommunity.CommunityId;
                }
            }
        }
    
        private void FixupHubSpecs(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (HubSpec item in e.NewItems)
                {
                    item.HubPlan = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (HubSpec item in e.OldItems)
                {
                    if (ReferenceEquals(item.HubPlan, this))
                    {
                        item.HubPlan = null;
                    }
                }
            }
        }
    
        private void FixupAllPlanHubVideos(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (HubVideo item in e.NewItems)
                {
                    item.HubPlan = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (HubVideo item in e.OldItems)
                {
                    if (ReferenceEquals(item.HubPlan, this))
                    {
                        item.HubPlan = null;
                    }
                }
            }
        }
    
        private void FixupAllPlanHubImages(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (HubImage item in e.NewItems)
                {
                    item.HubPlan = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (HubImage item in e.OldItems)
                {
                    if (ReferenceEquals(item.HubPlan, this))
                    {
                        item.HubPlan = null;
                    }
                }
            }
        }

        #endregion
    }
}
