﻿using Nhs.Mvc.Domain.Model.Hub.Interfaces;

namespace Nhs.Mvc.Domain.Model.Hub
{
    public partial class HubImage : IImage
    {
        #region IImage Members
        public string ImageNameSafe
        {
            get
            {
                return this.ImageName ?? string.Empty;
            }
        }

        public string ImagePathSafe
        {
            get
            {
                return this.ImagePath ?? string.Empty;
            }
        }

        #endregion
    }
}
