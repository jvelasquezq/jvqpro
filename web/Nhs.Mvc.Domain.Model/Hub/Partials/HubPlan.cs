﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Domain.Model.Hub
{
    public partial class HubPlan : IPlan
    {
        public Community Community { get; private set; }

        public int Id
        {
            get { return this.PlanId; }
        }

        public ICollection<IImage> PlanImages
        {
            get
            {
                if (this == null)
                    return new List<IImage>();

                return (from i in this.AllPlanHubImages
                        where i.SpecificationId == null
                        select i as IImage).ToList();
            }
        }

        public ICollection<IVideo> PlanVideos
        {
            get
            {
                if (this == null)
                    return new List<IVideo>();

                return (from i in this.AllPlanHubVideos
                        where i.SpecificationId == null
                        select i as IVideo).ToList();
            }
        }

        public string EnvisionUrl
        {
            get
            {
                var image = (from i in this.AllPlanHubImages
                             where i.ImageTypeCode == "EDC"
                             select i).FirstOrDefault();

                return image != null ? image.OriginalPath : string.Empty;
            }
        }

        public int? NumLivingAreas
        {
            get { return this.ParsePlanDetails("liv").ToType<int>(); }
            set { }
        }


        public int? MasterBedroomLocation
        {
            get { return this.ParsePlanDetails("mas" ).ToType<int>(); }
            set { }
        }

        public string ImageThumbnail { get; set; }

        public int LandExcluded { get; set; }

        public int Floors { get; set; }

        public int Location { get; set; }

        public string PromoTextLong { get; set; }

        public string PromoTextShort { get; set; }

        public string PromoUrl { get; set; }

        public ICollection<Video> AllPlanVideos { get; set; }
        public ICollection<Image> AllPlanImages { get; set; }

        public ICollection<Spec> Specs { get; set; }

        public int? Bedrooms
        {
            get { return this.ParsePlanDetails("bed").ToType<int>(); }
            set { }
        }

        public int? Bathrooms
        {
            get { return this.ParsePlanDetails("ba").ToType<int>(); }
            set { }
        }


        public decimal? Garages
        {
            get { return this.ParsePlanDetails("gar").ToType<decimal>(); }
            set { }
        }

        public int IsHotHome
        {
            get { return (!string.IsNullOrEmpty(this.HotHomeDescription)).ToType<int>(); }
            set { }
        }

        public int? HalfBaths
        {
            get { return this.ParsePlanDetails("hba" ).ToType<int>(); }
            set { }
        }


        public int? Stories
        {
            get { return this.ParsePlanDetails("sto").ToType<int>(); }
            set { }
        }


        public string HotHomeTitle
        {
            get
            {
                return !string.IsNullOrEmpty(this.HotHomeDescription) ? this.HotHomeDescription.Substring(0, this.HotHomeDescription.IndexOf("|")) : string.Empty;
            }
            set { }
        }







    }
}
