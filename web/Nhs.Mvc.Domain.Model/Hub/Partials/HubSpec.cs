﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Domain.Model.Hub
{
    public partial class HubSpec : ISpec
    {

        public ICollection<IImage> SpecImages
        {
            get
            {
                if (HubPlan == null)
                    return new List<IImage>();

                return (from i in HubPlan.AllPlanHubImages
                        where i.SpecificationId == SpecId
                        select i as IImage).ToList();
            }
        }

        public ICollection<IVideo> SpecVideos
        {
            get
            {
                if (HubPlan == null)
                    return new List<IVideo>();

                return (from v in HubPlan.AllPlanHubVideos
                        where v.SpecificationId == SpecId
                        select v as IVideo).ToList();
            }
        }

        public Community Community { get; private set; }
        public int Id { get { return this.SpecId; } }

        public int HomeStatus
        {
            get
            {
                if (string.IsNullOrEmpty(SpecType.Trim()) && MoveInDate < DateTime.Now)
                    return 1; //Ready to move-in
                if (string.IsNullOrEmpty(SpecType.Trim()) && (MoveInDate > DateTime.Now || MoveInDate == null))
                    return 2; //Under construction
                if (SpecType.ToLower().Trim() == "m")
                    return 4;

                return 3;
            }
        }

        public string ImageThumbnail { get; set; }

        public int? NumLivingAreas
        {
            get { return this.ParseSpecDetails("liv").ToType<int>(); }
            set { }
        }


        public int? MasterBedroomLocation
        {
            get { return this.ParseSpecDetails("mas").ToType<int>(); }
            set { }
        }


        public string PromoTextLong { get; set; }

        public string PromoTextShort { get; set; }

        public string PromoUrl { get; set; }

        public int LandExcluded { get; set; }

        public Plan Plan { get; set; }

        public int? Bedrooms
        {
            get { return this.ParseSpecDetails("bed").ToType<int>(); }
            set { }
        }

        public int? Bathrooms
        {
            get { return this.ParseSpecDetails("ba").ToType<int>(); }
            set { }
        }

        public decimal? Garages
        {
            get { return this.ParseSpecDetails("gar").ToType<decimal>(); }
            set { }
        }

        public int IsHotHome
        {
            get { return (!string.IsNullOrEmpty(this.HotHomeDescription)).ToType<int>(); }
            set { }
        }

        public int? HalfBaths
        {
            get { return this.ParseSpecDetails("hba").ToType<int>(); }
            set { }
        }

        public int? Stories
        {
            get { return this.ParseSpecDetails("sto").ToType<int>(); }
            set { }
        }

        public string HotHomeTitle
        {
            get
            {
                return !string.IsNullOrEmpty(this.HotHomeDescription) ? this.HotHomeDescription.Substring(0, this.HotHomeDescription.IndexOf("|")) : string.Empty;
            }
            set { }
        }
    }
}
