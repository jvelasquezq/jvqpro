﻿using System;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Domain.Model.Hub
{
    internal static class HubPlanSpecHelper
    {
        internal static string ParsePlanDetails(this HubPlan plan, string attributeName)
        {
            return plan.PlanDetails.ParseDetails(attributeName);
        }

        internal static string ParseSpecDetails(this HubSpec spec, string attributeName)
        {
            return spec.SpecDetails.ParseDetails(attributeName);
        }

        private static string ParseDetails (this string details,string attributeName)
        {
            if (string.IsNullOrEmpty(details))
                return string.Empty;

            var attribs = details.ToLower().Replace("bed", "").Replace("hba", "").Replace("ba", "").Replace("sto", "").Replace("gar", "").Replace("liv", "").Replace("mas", "").Replace("exl", "").Replace("flr", "").Replace("loc", "").Split('|');
            try
            {
                switch (attributeName)
                {
                    case "bed":
                        return attribs[0] ?? string.Empty;

                    case "ba":
                        return attribs[1] ?? string.Empty;

                    case "hba":
                        return attribs[2] ?? string.Empty;

                    case "sto":
                        return attribs[3] ?? string.Empty;

                    case "gar":
                        return attribs[4] ?? string.Empty;

                    case "liv":
                        return attribs[5] ?? string.Empty;

                    case "mas":
                        return attribs[6] ?? string.Empty;

                    case "exl":
                        return attribs[7] ?? string.Empty;

                    case "flr":
                        return attribs[8] ?? string.Empty;

                    case "loc":
                        return attribs[9] ?? string.Empty;
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);

            }
            return string.Empty;
        }
    }
}
