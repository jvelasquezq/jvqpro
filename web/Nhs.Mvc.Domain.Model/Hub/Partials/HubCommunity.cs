﻿using System;
using Nhs.Utility.Common;
using Nhs.Mvc.Domain.Model.Web.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Nhs.Mvc.Domain.Model.Hub
{
    public partial class HubCommunity
    {
        public IEnumerable<IAmenity> IAmenities
        {
            get
            {
                var amenities = new List<IAmenity>();
                foreach (var amenity in this.HubAmenities)
                {
                    amenities.Add(amenity);
                }

                return amenities;
            }
        }

        public IEnumerable<IAmenity> IOpenAmenities
        {
            get
            {
                var amenities = new List<IAmenity>();
                foreach (var amenity in this.HubOpenAmenities)
                {
                    amenities.Add(amenity);
                }

                return amenities;
            }
        }

        public virtual int SqFtLow
        {
            get { return this.ParseListingDetails("sqftlow").ToType<int>(); }
            set { }
        }

        public int? HasGolfCourse
        {
            get { return this.HasGolfCourseDb.ToType<int?>(); }
            set { }
        }

        public int? IsGreen
        {
            get { return this.IsGreenDb.ToType<int?>(); }
            set { }
        }

        public virtual int SqFtHigh
        {
            get { return this.ParseListingDetails("sqfthigh").ToType<int>(); }
            set { }
        }

        public virtual decimal PriceLow
        {
            get { return this.ParseListingDetails("pricelow").ToType<decimal>(); }
            set { }
        }

        public virtual decimal PriceHigh
        {
            get { return this.ParseListingDetails("pricehigh").ToType<decimal>(); }
            set { }
        }

        public virtual Nullable<int> HomeCount
        {
            get { return this.ParseListingDetails("listingcount").ToType<int>(); }
            set { }
        }

        public virtual Nullable<int> QuickMoveInCount
        {
            get { return this.ParseListingDetails("qmicount").ToType<int>(); }
            set { }
        }

        public virtual int HasHotHome
        {
            get
            {
                return
                    (!string.IsNullOrEmpty(this.PlanHotHomeDescription) ||
                     !string.IsNullOrEmpty(this.SpecHotHomeDescription)).ToType<int>();
            }
            set { }
        }

        private string ParseListingDetails(string attributeName)
        {
            if (string.IsNullOrEmpty(this.ListingDetails))
                return string.Empty;

            var attribs =
                this.ListingDetails.ToLower().Replace("sqft", "").Replace("price", "").Replace("bed", "").Replace("ba",
                                                                                                                  "").
                    Replace("listings", "").Replace("gar", "").Split('|');
            try
            {

                switch (attributeName)
                {
                    case "sqftlow":
                        return attribs[0] != null ? attribs[0].Split('-')[0] : string.Empty;

                    case "sqfthigh":
                        return attribs[0] != null ? attribs[0].Split('-')[1] : string.Empty;

                    case "pricelow":
                        return attribs[1] != null ? attribs[1].Split('-')[0] : string.Empty;

                    case "pricehigh":
                        return attribs[1] != null ? attribs[1].Split('-')[1] : string.Empty;

                    case "bedroomlow":
                        return attribs[2] != null ? attribs[2].Split('-')[0] : string.Empty;

                    case "bedroomhigh":
                        return attribs[2] != null ? attribs[2].Split('-')[1] : string.Empty;

                    case "bathlow":
                        return attribs[3] != null ? attribs[3].Split('-')[0] : string.Empty;

                    case "bathhigh":
                        return attribs[3] != null ? attribs[3].Split('-')[1] : string.Empty;

                    case "galow":
                        return attribs[4] != null ? attribs[4].Split('-')[0] : string.Empty;

                    case "gahigh":
                        return attribs[4] != null ? attribs[4].Split('-')[1] : string.Empty;

                    case "listingcount":
                        return attribs[5] != null ? attribs[5].Split('-')[0] : string.Empty;

                    case "qmicount":
                        return attribs[6] != null ? attribs[6].Split('-')[0] : string.Empty;
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex);

            }
            return string.Empty;
        }

        public virtual string PhoneNumber
        {
            get
            {
                if (!string.IsNullOrEmpty(PhoneFaxNumber))
                {
                  var phones=  PhoneFaxNumber.ToLower().Replace("phn", "").Replace("fax", "").Split('|');
                  return phones[0] ?? string.Empty;
                }
                return string.Empty;
            }
        }
    }

}

