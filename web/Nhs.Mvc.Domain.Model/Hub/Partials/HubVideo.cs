﻿using System;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Utility.Web;

namespace Nhs.Mvc.Domain.Model.Hub
{

    public partial class HubVideo : IVideo
    {
        private string _videoOnlineId = string.Empty;

        public int Order { private set; get; }

        public bool IsFromYoutube
        {
            get { return UrlHelper.IsYoutubeVideo(VideoURL); }
        }

        public bool IsFromVimeo
        {
            get { return UrlHelper.IsYoutubeVideo(VideoURL); }
        }

        public int VideoWidth { get; set; }
        public int VideoHeight { get; set; }

        public string RefId { get; set; }

        public string VideoOnlineId
        {
            get
            {
                if (IsFromVimeo)
                    return UrlHelper.GetVimeoVideoId(VideoURL);
                else if (IsFromYoutube)
                    return UrlHelper.GetYoutubeVideoId(VideoURL);
                else
                    return _videoOnlineId;
            }
            set { _videoOnlineId = value; }
        }

    }

}
