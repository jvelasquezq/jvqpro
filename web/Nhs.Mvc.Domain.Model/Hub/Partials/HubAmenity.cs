﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Mvc.Domain.Model.Web.Interfaces;

namespace Nhs.Mvc.Domain.Model.Hub
{
    public partial class HubAmenity:IAmenity
    {
        public Nullable<int> GroupId { get; set; }
        public int ShortNumber { get; set; }
    }
}
