﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Domain.Model.BHIContent
{
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class AffiliateLinks
    {
        public AffiliateLinks()
        {
            LinksList = new List<AffiliateLink>();
        }

        [XmlElement("affiliatelink")]
        public List<AffiliateLink> LinksList { get; set; }
    }

    [Serializable]
    public class AffiliateLink
    {
        [XmlAttribute("iconpath")]
        public string IconPath { get; set; }

        [XmlAttribute("keyword")]
        public string Keyword { get; set; }

        [XmlAttribute("text")]
        public string LinkText { get; set; }

        [XmlAttribute("target")]
        public string Target { get; set; }

        [XmlAttribute("href")]
        public string Href { get; set; }

        [XmlAttribute("class")]
        public string CssClass { get; set; }

        [XmlAttribute("onclick")]
        public string OnClick { get; set; }
    }
}
