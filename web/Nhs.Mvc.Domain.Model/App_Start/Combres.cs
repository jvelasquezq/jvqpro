[assembly: WebActivator.PreApplicationStartMethod(typeof(Nhs.Mvc.Domain.Model.App_Start.Combres), "PreStart")]
namespace Nhs.Mvc.Domain.Model.App_Start {
	using System.Web.Routing;
	using global::Combres;
	
    public static class Combres {
        public static void PreStart() {
            RouteTable.Routes.AddCombresRoute("Combres");
        }
    }
}