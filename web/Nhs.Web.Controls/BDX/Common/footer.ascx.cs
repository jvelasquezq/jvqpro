using System;
using System.Data;
using System.Web.UI.WebControls;
using Nhs.Library.Common;
using Nhs.Library.Web;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;

namespace Nhs.Web.Controls.BDX.Common
{
    public partial class footer : UserControlBase
    {
        private DateTime _dateTime = new DateTime();

        protected void Page_Load(object sender, EventArgs e)
        {
            _dateTime = DateTime.Now;
            litCurrentYear.Text = litCurrentYear2.Text = _dateTime.Year.ToString();

            var tvLifeStyle = new NhsUrl(Pages.NewHomeSourceTv);
            lnkLifestyles.NavigateUrl = ResolveAbsoluteUrl("/" + tvLifeStyle);

            if (XGlobals.Partner.PartnerId != Convert.ToInt32(PartnersConst.NewHomeSource))
                plhNhsFooter.Visible = false;

            if (XGlobals.Partner.BrandPartnerID != Convert.ToInt32(PartnersConst.Move))
                plhMoveFooter.Visible = false;

            if (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser)
            {
                Link lnkSignIn = this.FindControl("lnkSignIn") as Link;
                Link lnkRegister = (Link)this.FindControl("lnkRegister");
                if (lnkSignIn != null)
                {
                    lnkSignIn.Text = @"Sign Out";
                    lnkSignIn.Function = Pages.LogOff;
                    lnkRegister.Visible = false; //hide register link if signed in
                }
            }

            if (!IsPostBack)
            {

            }

            if (!XGlobals.Partner.AllowRegistration)
            {
                spnSignIn.Visible = false;
            }
            if (!XGlobals.Partner.FooterShowAboutUs)
            {
                lnkAboutUs.Visible = false;
                spnAbout.Visible = false;
            }
        }

        protected void rptCMSLinks_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Link lnkCMS = (Link)e.Item.FindControl("lnkCMS");
                DataRowView row = (DataRowView)e.Item.DataItem;
                lnkCMS.Text = row["category_name"].ToString();
                lnkCMS.Function = Pages.HomeGuideCategory;
                lnkCMS.Parameters.Add(new RouteParam(RouteParams.Category, row["category_name"].ToString().Replace(' ', '-')));
                lnkCMS.Parameters.Add(new RouteParam(RouteParams.CategoryId, row["category_id"].ToString()));
            }
        }
    }
}
