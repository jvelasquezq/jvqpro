<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="footer.ascx.cs" Inherits="Nhs.Web.Controls.BDX.Common.footer" %>

	

    
<asp:PlaceHolder ID="plhNhsFooter" runat="server">
<!-- BDX - NHS Footer //-->
<aside id="nhs_SocialFooter">
	<ul class="clearfix">
			<li><a id="nhs_Facebook" title="Like us on Facebook" href="https://www.facebook.com/NewHomeSource">Facebook</a></li>
			<li><a id="nhs_Twitter" title="Follow us on Twitter" href="https://www.twitter.com/NewHomeSource">Twitter</a></li>
			<li><a id="nhs_Pinterest" title="See us on Pinterest" href="https://www.pinterest.com/newhomesource/">Pinterest</a></li>
			<li><a id="nhs_YouTube" title="View Our Videos on YouTube" href="https://www.youtube.com/user/NewHomeSource">YouTube</a></li>
			<li><a id="nhs_GooglePlus" title="Connect with us on Google+" href="https://plus.google.com/102652212865916066440/posts">Google+</a></li>
		</ul>
</aside>

<footer id="nhs_Footer">
    <div>
    <aside id="nhs_FooterAdBox">
        <p class="visuallyhidden">Advertisement</p>
        <div id="nhs_AdFooter">
		<nhs:ad ID="x01" UseIFrame="true" runat="server" />
        </div>
    </aside>
    
	    <nav id="nhs_FooterSearch">
            <p><strong>New Home Search</strong></p>
	        <ul>
		        <li><nhs:Link ID="lnkSearch" runat="server" Text="Search" Function="Home" /></li>
				<li><nhs:Link ID="lnkAdvancedSearch" rel="nofollow" runat="server"  Text="Advanced Search" Function="advancedsearch" /></li>
				<li><nhs:Link ID="lnkBOYL" runat="server" Text="Build on your lot" Function="boylsearch" /></li>
				<li><a target="_blank" href="http://www.customnewhomes.com/">Custom Builders</a></li>
				<li class="nhsLast"><nhs:Link ID="lnkListHomes" Text="List your homes" runat="server" Function="listyourhomes"></nhs:Link></li>
			</ul>					
	    </nav>
        <nav id="nhs_FooterMore">
            <p><strong>More Choices</strong></p>
	        <ul>
	            <li><asp:HyperLink ID="lnkRetirement" runat="server" Target="_blank" Text="Retirement Communities" NavigateUrl="http://www.newretirementcommunities.com/"></asp:HyperLink></li>
	            <li><asp:HyperLink ID="lnkUrbanCondo" runat="server" Target="_blank" Text="Search New Condos" NavigateUrl="http://www.urbancondoliving.com/"></asp:HyperLink></li>   
	            <li><asp:HyperLink ID="lnkLifestyles" runat="server" Target="_blank" Text="NewHomeSource TV" NavigateUrl="http://www.newhomelifestyles.com/"></asp:HyperLink></li>
	            <li><asp:HyperLink ID="lnkSpanish" runat="server" Target="_blank" Text="Casas Nuevas en Espa&ntilde;ol" NavigateUrl="http://www.casasnuevasaqui.com/"></asp:HyperLink></li>   
	            <li><asp:HyperLink ID="lnkNHSBlog" runat="server" Target="_blank" Text="Home Trends Blog" NavigateUrl="http://blog.newhomesource.com/"></asp:HyperLink></li>  
                 <%if(Nhs.Library.Common.Configuration.ShowEBook)
                                     {%> 
                <li class="nhsLast"><asp:HyperLink ID="lnkEBook"  runat="server" Target="_blank" Text="Free New Home 101 eBook" onclick="jQuery.googlepush('Site Links','Footer','Free eBook')" NavigateUrl="#"></asp:HyperLink></li>
                  <%} %>   		            	            
			</ul>
	    </nav>
        <nav id="nhs_FooterAbout">
            <p><strong>About This Site</strong></p>
	        <ul>
				<%-- <li><nhs:Link ID="lnkHome2" runat="server" Text="Home" Function="Home"/></li>
				<li><nhs:Link ID="lnkSignIn" runat="server" Text="Sign In" Function="SignIn"/></li>
				<li><nhs:Link ID="lnkRegister" runat="server" Text="Create Account" Function="Register"/></li> --%>
				<li><nhs:Link ID="lnkUnsubscribe" runat="server" Rel="nofollow" Text="Unsubscribe" Function="Unsubscribe"/></li>
				<li><nhs:Link ID="lnkHelp" runat="server" rel="nofollow" Text="Help" Function="sitehelp"/></li>
				<li><nhs:Link ID="lnkSiteIndex" runat="server" Text="Site Index" Function="siteindex"/></li>
				<li><nhs:Link ID="lnkMortgageCalculator" runat="server" Text="Mortgage Calculator" Function="mortgagecalculator"/></li>	        	                         
				<li><nhs:Link ID="lnkAboutUs" runat="server" Rel="nofollow" Text="About us" Function="aboutus"/></li>
				<li><nhs:Link ID="lnkContactUs" runat="server" rel="nofollow" Text="Contact us" Function="contactus"/></li>
				<li><nhs:Link ID="lnkPrivacyPolicy" runat="server" Rel="nofollow" Text="Privacy policy" Function="privacypolicy" Target="_blank" /></li> 
				<li class="nhsLast"><nhs:Link ID="lnkTermsofuse" runat="server" Rel="nofollow" Text="Terms of use" Function="termsofuse" AccessKey="8" /></li>
				<%-- <li><asp:HyperLink ID="lnkAdvertise" runat="server" Text="Advertise" NavigateUrl="http://www.builderhomesite.com/newhomesource/mediakit/request.htm"></asp:HyperLink></li>--%>
			</ul>
		</nav>

    <p id="nhs_FooterLegal">Copyright &copy; 2001-<asp:Literal ID="litCurrentYear" runat="server" /> Builders Digital Experience, LLC.  <a href="http://www.newhomesource.com/" onclick="$jq.googlepush('Site Links', 'Disclaimer', 'NewHomeSource.com');">NewHomeSource.com</a> is a trademark of <a target="_blank" href="http://www.thebdx.com" onclick="$jq.googlepush('Outbound Links', 'Disclaimer', 'Builders Digital Experience');">Builders Digital Experience, LLC</a> and all other marks are either trademarks or registered trademarks of their respective owners. All rights reserved.</p> 
    </div>
</footer>
</asp:PlaceHolder>
    
<asp:PlaceHolder ID="plhMoveFooter" runat="server">
    <!-- BDX - Move Footer //-->

<div id="nhs_FooterWrapper">
    <footer id="nhs_Footer">
    <div>
    <aside id="nhs_FooterAdBox">
        <p class="visuallyhidden">Advertisement</p>
        <div id="nhs_AdFooter">
		<nhs:ad ID="x02" UseIFrame="true" runat="server" />
        </div>
    </aside>    

    <div class="mvFooterGroup">        
        <nhs:Link ID="lnkPopular" runat="server" Function="SiteIndex" Text="Find New Homes in Popular Cities:"></nhs:Link>
    </div>
    <nhs:seo_content_manager ID="scMNH_HomeFooterLinks" runat="server" CodeBlock="footerlinks" TemplateType="MNH_HomeV2" />
    
    <div class="nhs_Clear"></div>
    <div class="mvFooterGroup"> 
        Corporate: <a href="javascript:bookmark();">Bookmark this Site</a> | 
        <nhs:Link ID="lnkSiteIndex2" runat="server" Text="Site Map" Function="SiteIndex"/> | 
        <nhs:Link ID="lnkHelp2" runat="server" rel="nofollow" Text="Help" Function="SiteHelp" /> | 
        <span id="spnSignIn" runat="server"><nhs:Link ID="lnkSignIn2" runat="server" Text="Sign In" Function="SignIn"/> |</span>
        <nhs:Link ID="lnkUnsubscribeFooter" onclick="$jq.googlepush('Site Links', 'Footer','Unsubscribe');" runat="server" Rel="nofollow" Text="Unsubscribe" Function="Unsubscribe"/>
        <!-- | 
        <a href="javascript:O_LC();" title="Provide Feedback"> Provide Feedback </a><img src="[resource:]/globalresources/move/images/icons/opinionLab_icon.gif" runat="server" alt="" /> //-->
        <br />
        <span id="spnAbout" runat="server"><a href="http://www.move.com/company/corporateinfo.aspx">About Move</a> | </span>
        <nhs:Link ID="lnkContact" runat="server" Text="Contact Us" Function="ContactUs" /> | 
        <a href="http://www.move.com/company/linktous.aspx" title="Link to Us">Link to Us</a> | 
        <nhs:Link ID="lnkList" runat="server" Text="List Your Homes" Function="ListYourHomes" /> | 
        <nhs:Link ID="lnkPrivacy" runat="server" Text="Privacy Policy" Function="PrivacyPolicy" /> | 
        <nhs:Link ID="lnkTerms" runat="server" Text="Terms of Use" Function="TermsOfUse" />    
    </div>
    <div class="mvFooterGroup mvFooterCopyright">
        
    <div> &copy; <asp:Literal ID="litCurrentYear2" runat="server" /> Builders Digital Experience, LLC. All rights reserved.</div>
    </div> 
    </div>
    </footer>
</div> 

<script type="text/javascript">
    function bookmark(A, B) {
        if (A == null) {
            A = window.location.href;
        }
        if (B == null) {
            B = document.title;
        }
        if (document.all) {
            window.external.AddFavorite(A, B);
        } else {
            if (window.sidebar) {
                window.sidebar.addPanel(B, A, "");
            } else {
            window.alert("Press CTRL+T to bookmark this page");
            } 
        } 
    }
</script>
        
</asp:PlaceHolder>
    