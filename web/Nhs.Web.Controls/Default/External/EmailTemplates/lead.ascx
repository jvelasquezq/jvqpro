<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="lead.ascx.cs" Inherits="Nhs.Web.Controls.Default.External.EmailTemplates.lead" %>
<%@ Register Src="../../Common/email_header.ascx" TagPrefix="nhs" TagName="emailHeader"%>
<%@ Register Src="../../Common/email_footer.ascx" TagPrefix="nhs" TagName="emailFooter"%>
<%@ Register Src="../../Common/email_safesenders.ascx" TagPrefix="nhs" TagName="emailSafeSender"%>

<table id="nhsEmailAccountCreate" width="100%" height="100%;" cellpadding="0" cellspacing="0" bgcolor="#e7e5da" style="background:#e7e5da;width:100%;">
<tr><td style="padding:8px;vertical-align:top;">
    <table width="545" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="background:#fff;width:545px;">
    <tr>
        <td style="padding:8px;vertical-align:middle;">
            <nhs:emailHeader id="header" runat="server" />
        </td>
        <td align="right" style="padding:8px;vertical-align:middle;text-align:right;">
            <img id="img4" src="[resource:]/globalresources/default/images/email/header_brochure.gif" alt="Welcome" width="309" border="0" style="width:309px;" runat="server" />
        </td>
    </tr>
    
    <tr>
        <td colspan="2" style="padding:12px;vertical-align:middle;">
            <nhs:emailSafeSender id="safesender" runat="server" />
        </td>
    </tr>
    
    <tr>
        <td colspan="2">
            <table width="521" border="0" cellspacing="0" cellpadding="0">	
	            <tr>
	                <td style="width:521px;padding:12px;font-family:Arial,Helvetica;font-size:12px;">
	                    
	                    <!-- Matchmaker Leads placeholders -->
	                    <asp:Literal ID="ltrNewHomeMatches" runat="server" />
	                    <asp:PlaceHolder ID="plhNoMatches" runat="server" Visible="false">
	                        <p>We're sorry, but we found no homes or communities matching your criteria. To search again, please click <asp:HyperLink ID="lnkLeadUrl" runat="server"></asp:HyperLink> and modify your search criteria or return to <asp:HyperLink ID="lnkHomePage" runat="server"></asp:HyperLink>.</p>
	                        <p>Thank you for visiting <asp:Literal ID="ltrSiteNameNoMatch" runat="server" Visible="false" /> <asp:HyperLink ID="lnkHomePageNoMatch" runat="server" Visible="false"></asp:HyperLink>.</p>
                        </asp:PlaceHolder>
	                    <asp:PlaceHolder ID="plhMarketRequest" runat="server" Visible="false">
	                        <p>Thank you for using our New Home Matchmaker Service. Matchmaker has selected the following communities that match your search criteria.</p>	    
	                    </asp:PlaceHolder>
	                    
	                    <!-- Other Leads placeholders -->
	                    <asp:PlaceHolder ID="plhOtherRequest" runat="server" Visible="false">
	                        <p style="font-size:18px;color:#114981;"><strong><asp:Label ID="lblFirstName" runat="server" />, thank you for visiting <asp:Literal ID="ltrSiteNameOther" runat="server" Visible="false" /><asp:HyperLink ID="lnkHomePageOther" style="color:#276AB1;" runat="server" Visible="false"/></strong></p>
	                        <p><asp:Literal ID="ltrlBrochuresText" runat="server"></asp:Literal> <asp:Label ID="lblBrandName" runat="server" /> may also follow up with additional information about the new homes you're interested in.</p>
                        </asp:PlaceHolder>
                        
                        <!-- Other Leads placeholders -->
	                    <asp:PlaceHolder ID="plhQuickConnect" runat="server" Visible="false">
	                     <p style="font-size:18px;color:#114981;"><strong><asp:Label ID="lblFirstNameQc" runat="server" />, below are the communities you selected to receive a free brochure from your Quick Connect.</strong></p>
	                    </asp:PlaceHolder>
	                    
	                    <!--Market lead type-->
                        <asp:PlaceHolder ID="plhMarketLeadTypeText" runat="server" Visible="false">
	                        <p>You can view and print brochures for each community.<br />
	                        The builder(s) may also contact you with additional information.</p>	
		                    <p>Thank you for using New Home Matchmaker!</p>
                        </asp:PlaceHolder>

                        <!--Other lead types-->
                        <asp:PlaceHolder ID="plhOtherLeadTypeText" runat="server" Visible="false">
	                        
                        </asp:PlaceHolder>
                        
                        <!-- Repeaters -->
                        <asp:Repeater ID="rptHome" Runat="server" EnableViewState="false" OnItemDataBound="rptHome_ItemDataBound">
                        <ItemTemplate>
                            <table width="521" bgcolor="#fafafa" border="0" style="border-top:1px solid #ececec;border-bottom:1px solid #ececec;" cellspacing="0" cellpadding="0">
                                <tr>      
                                    <td rowspan="3" align="center" style="width:120px;align:center;vertical-align:middle;font-family:Arial,Helvetica;font-size:12px;">
                                        <asp:HyperLink ID="lnkHomeThumbnail" runat="server">
                                        <img id="imgHomeThumbnail" src="[resource:]/globalresources/default/images/small_nophoto.gif" runat="server" width="75" height="58" border="0" style="width:75px;height:58px;border:0;" alt="No photo available" /></asp:HyperLink>      
                                    </td>
	                                <td valign="top" style="padding:5px 0 0 0;font-family:Arial,Helvetica;font-size:12px;">
	                                    <p style="font-size:14px;margin:0 0 3px 0;"><strong><asp:Hyperlink style="color:#276AB1;" ID="lnkHomeDetail" runat="server"></asp:Hyperlink></strong> &nbsp;	
	                                    <strong><asp:Label ID="lblNumBedrooms" runat="server" /></strong> <strong><asp:Label ID="lblNumBaths" runat="server" />ba </strong><strong><asp:Label ID="lblNumGarages" runat="server" />gr</strong></p>
	                                    
	                                    <span>By <asp:Label ID="lblBrandName" runat="server" /><br />
	                                        <asp:Label ID="lblCommunityName" runat="server" /><br />
	                                        <asp:Label ID="lblCity" runat="server" />, <asp:Label ID="lblState" runat="server" /> <asp:Label ID="lblPostalCode" runat="server" /></span>
	                                </td>
	                                <td align="right" valign="top" style="padding:5px 5px 0 5px;font-family:Arial,Helvetica;font-size:12px;">
	                                    <span>from <asp:Label ID="lblPrice" runat="server" /></span>
	                                    <table cellpadding="8" cellspacing="0" bgcolor="#F8E211" style="margin:5px 0 0 0;"><tr><td style="padding: 8px 12px;font-family:Arial,Helvetica;font-size:12px;"><strong><asp:Hyperlink ID="lnkViewBrochure" style="color:#276AB1;" runat="server">View brochure</asp:Hyperlink></strong></td></tr></table>
	                                </td>
                                </tr> 
                                <tr>
                                    <td colspan="2">
                                    <asp:PlaceHolder runat="server" Visible="false" id="plhHotHomes"> 
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td valign="top" style="padding: 5px 5px 5px 0;">
                                                    <img id="imgHotHomes" src="[resource:]/globalresources/default/images/icons/icon_hot_homes.gif" runat="server" width="72" height="19" border="0" style="width:72px;height:19px;border:0;" alt="Hot Homes" visible="false" />
                                                </td>
                                                <td valign="middle" style="padding:5px 5px 5px 0;font-family:Arial,Helvetica;font-size:12px;">
                                                    <strong><asp:Literal ID="ltrHotHomesHeadline" runat="server" /></strong>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:PlaceHolder>
                                    </td>
                                </tr>      
                                <tr>
                                    <td colspan="2">
                                    <asp:PlaceHolder runat="server" Visible="false" id="plhHomeSpecialPromo"> 
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td valign="top" width="123" style="padding: 5px 5px 5px 0;">
                                                    <img id="imgSpecialOffer" src="[resource:]/globalresources/default/images/icons/icon_special_offer_3.gif" runat="server" width="123" height="16" border="0" style="width:123px;height:16px;border:0;" alt="Special Offer" Visible="false"/>
                                                </td>
                                                 <td valign="middle" style="padding:1px 5px 5px 0;font-family:Arial,Helvetica;font-size:12px;">
                                                    <asp:Repeater ID="rptPromotionResults" runat="server" OnItemDataBound="rptPromotionResults_ItemDataBound" EnableViewState="false" Visible="false">
                                                    <HeaderTemplate></HeaderTemplate>
                                                    <ItemTemplate> 
                                                     <asp:Literal ID="uxParagraph" runat="server"/>
                                                    <strong><asp:Literal ID="ltrPromoHeadline" runat="server" />&nbsp;<asp:Hyperlink ID="lnkPromoPDF" style="color:#276AB1;" runat="server" Visible="false" Target="_blank">Read more</asp:Hyperlink></strong>
                                                    <asp:Literal ID="uxParagraphClose" runat="server"/></ItemTemplate></asp:Repeater></td>
                                            </tr>
                                        </table>
                                    </asp:PlaceHolder>
                                    </td>
                                </tr>                                
                                <!--<tr>
                                    <td style="padding-top: 5px;">
                                    <asp:HyperLink ID="lnkBrochureUrl" runat="server" Text="Your printable brochure, including a map and driving directions, is now available online."></asp:HyperLink>
                                    <br />
                                        To view, click the link above or copy this URL to your web browser: <asp:Label ID="lblBrochureUrl" runat="server" />
                                    </td>				
                                </tr> //--> 
                            </table>
                        </ItemTemplate>
                        </asp:Repeater>
                        
                        <asp:Repeater ID="rptCommunity" Runat="server" EnableViewState="False" OnItemDataBound="rptCommunity_ItemDataBound">
	                    <ItemTemplate>
		                    <table width="521" bgcolor="#fafafa" border="0" style="border-top:1px solid #ececec;border-bottom:1px solid #ececec;" cellspacing="0" cellpadding="0">
                                <tr>      
                                    <td rowspan="3" align="center" style="width:120px;align:center;vertical-align:middle;font-family:Arial,Helvetica;font-size:12px;">                                        
                                        <asp:HyperLink ID="lnkCommunityThumbnail" runat="server">
                                        <img id="imgCommunityThumbnail" src="[resource:]/globalresources/default/images/small_nophoto.gif" runat="server" width="91" height="70" border="0" style="width:91px;height:70px;border:0;" alt="No photo available" /></asp:HyperLink>
                                    </td>
				                    <td valign="top" style="padding:5px 0 0 0;font-family:Arial,Helvetica;font-size:12px;">
                                        <p style="font-size:14px;margin:0 0 3px 0;"><strong><asp:Hyperlink ID="lnkCommDetail" style="color:#276AB1;" runat="server"></asp:Hyperlink></strong></p>
				                        <span>By <asp:Label ID="lblBrandName" runat="server" /><br />
				                        <!--<asp:Label ID="lblCommunityName" runat="server" /> //-->
				                        <asp:PlaceHolder ID="plhCommAddress" runat="server" enableViewState="false" Visible="false">
						                    <asp:Label ID="lblCity" runat="server" />, <asp:Label ID="lblState" runat="server" />&nbsp;<asp:Label ID="lblPostalCode" runat="server" />
				                        </asp:PlaceHolder>
				                        </span>
    				                </td>
    				                <td align="right" valign="top" style="padding:5px 5px 0 5px;font-family:Arial,Helvetica;font-size:12px;">
				                        from<asp:Label ID="lblPrice" runat="server" />
				                        <table cellpadding="8" cellspacing="0" bgcolor="#F8E211" style="margin:5px 0 0 0;"><tr><td style="padding: 8px 12px;font-family:Arial,Helvetica;font-size:12px;"><strong><asp:Hyperlink ID="lnkViewBrochure" style="color:#276AB1;" runat="server">View brochure</asp:Hyperlink></td></tr></table>
                                    </td>
			                    </tr>			                    		
			                    <tr>		
				                    <td colspan="2">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <asp:PlaceHolder runat="server" Visible="false" id="plhCommSpecialPromo"> 
                                            <tr>
                                                <td valign="top" width="123" style="padding:5px 5px 5px 0;">
				                                    <img id="imgSpecialOffer" src="[resource:]/globalresources/default/images/icons/icon_special_offer_3.gif" runat="server" width="123" height="16" border="0" style="width:123px;height:16px;border:0;" alt="Special Offer" visible="false"/>
				                                </td>
                                                <td valign="middle" style="padding:1px 5px 5px 0;font-family:Arial,Helvetica;font-size:12px;">
                                                    <asp:Repeater ID="rptPromotionResults" runat="server" OnItemDataBound="rptPromotionResults_ItemDataBound" EnableViewState="false" Visible="false">
                                                    <HeaderTemplate></HeaderTemplate>
                                                    <ItemTemplate> 
                                                     <asp:Literal ID="uxParagraph" runat="server"/>
                                                    <strong><asp:Literal ID="ltrPromoHeadline" runat="server" />&nbsp;<asp:Hyperlink ID="lnkPromoPDF" style="color:#276AB1;" runat="server" Visible="false" Target="_blank">Read more</asp:Hyperlink></strong>
                                                    <asp:Literal ID="uxParagraphClose" runat="server"/></ItemTemplate></asp:Repeater></td>
				                            </tr>
				                            </asp:PlaceHolder>
					                        <asp:PlaceHolder ID="plhArea" runat="server" EnableViewState="false" Visible="false">
						                    <tr>
						                        <td colspan="2" style="padding: 5px 5px 5px 0;font-family:Arial,Helvetica;font-size:12px;">
						                            <em><asp:Label ID="lblMarket" runat="server" /> area</em>
						                        </td>
						                    </tr>
					                        </asp:PlaceHolder>
					                        </table>		
				                    </td>
			                    </tr>
			                    <tr>		
				                    <td colspan="2">                                        
                                         <img id="imgHotHomes" src="[resource:]/globalresources/default/images/icons/icon_hot_homes.gif" runat="server" width="72" height="19" border="0" style="width:72px;height:19px;border:0;" alt="Hot Homes" visible="false" />	
				                    </td>
			                    </tr>
                    			
                                <!--<asp:HyperLink ID="lnkBrochureUrl" runat="server" Text="Your printable brochure, including a map and driving directions, is now available online."></asp:HyperLink>-->
                    			
	                        </table>
	                    </ItemTemplate>
                        </asp:Repeater>
                        
                        <br />
                        <asp:PlaceHolder ID="plhAdditionalInfoMarket" runat="server" Visible="false">
				            <p>You may receive additional information about specific communities from individual builders.</p>
	                    </asp:PlaceHolder>
	                    	                    
	                    <table width="521" border="0" cellspacing="0" cellpadding="0">
	                        <tr>
		                        <td style="width:320px;padding:5px 15px 0 0;border-right:1px dashed #CFD9B7;font-family:Arial,Helvetica;font-size:12px;">
                                    <p style="font-size:14px;margin-top:0;color:#114981;"><strong>More new homes are waiting to be discovered</strong></p>                                    
                                    <ul style="margin:0 0 10px 0;padding:0;list-style:none;">
                                        <li><img id="Img2" src="[resource:]/globalresources/default/images/buttons/btn_63_arrow.gif" alt=">" width="10" height="10" style="width:10px;height:10px;border:0;" runat="server" />&nbsp; <asp:Hyperlink ID="lnkHomes" style="color:#276AB1;" runat="server"/></li>
                                        <li><img id="Img3" src="[resource:]/globalresources/default/images/buttons/btn_63_arrow.gif" alt=">" width="10" height="10" style="width:10px;height:10px;border:0;" runat="server" />&nbsp; <asp:Hyperlink ID="lnkCommunities" style="color:#276AB1;" runat="server"/></li>
                                    </ul>
                                    <table cellpadding="8" cellspacing="0" bgcolor="#F8E211" style="margin:5px 0 0 0;"><tr><td style="padding: 8px 12px;font-family:Arial,Helvetica;font-size:12px;"><strong><asp:Hyperlink ID="lnkSearchNewHomes" style="color:#276AB1;" runat="server">Search new homes</asp:Hyperlink></strong></td></tr></table>
                                    
                                    <div runat="server" id="divStartAlert" visible="false">
                                    <p style="font-size:14px;margin:35px 0 0 0;color:#114981;"><strong>Get new home recommendations by email</strong></p>
                                    <p style="margin:0;">Create a search alert with your preferences and we'll send you new home communities matching your needs. <br />
                                    <asp:Hyperlink ID="lnkStartSearchAlert" style="color:#276AB1;" runat="server">Start a search alert</asp:Hyperlink></p>
                                    </div>
                                    <div runat="server" id="divFreeBuyingTips" visible="false">
                                    <p style="font-size:14px;margin:35px 0 0 0;color:#114981;"><strong>Free homebuying tips</strong></p>
                                    <p style="margin:0;">Get the latest in new home news, design trends, product profiles and more from the NewHomeSource newsletter. <br />
                                    <asp:Hyperlink ID="lnkGetOurNewspaper" style="color:#276AB1;" runat="server">Get our newsletter</asp:Hyperlink></p>
                                    </div>
                                    
                                </td>
                                <td style="width:171px;padding:5px 0 0 15px;vertical-align:top;font-family:Arial,Helvetica;font-size:12px;" id="tdChoices" runat="server">
                                    <p style="color:#114981;margin:0;"><strong>Keep tabs on your home choices</strong></p>
                                    <p style="margin:0;">Sign in to save &amp; track your favorite new homes, and view all of your search alert results. <br />
                                    <asp:Hyperlink ID="lnkOpenMySaveProperties" style="color:#276AB1;" runat="server">Open my saved properties</asp:Hyperlink></p>
                                    
                                    <p style="color:#114981;margin:35px 0 0 0;"><strong>Power up your home search</strong></p>
                                    <p style="margin:0;">Keep track of your favorites and get search alerts by email. <br />
                                    <asp:Hyperlink ID="lnkCreateFreeAccount" style="color:#276AB1;" runat="server">Create a free account</asp:Hyperlink></p>
             
                                </td>
                            </tr>
                        </table>
                    
	                </td>
	            </tr>
	        </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <img id="Img1" src="[resource:]/globalresources/default/images/email/footer_545.gif" alt="Great New Homes Ahead" width="545" height="100" style="width:545px;height:100px;" border="0" runat="server" /></td>
    </tr>    
    </table>
    <table width="545" cellpadding="0" cellspacing="0" border="0" style="width:545px;">
    <tr>
        <td style="padding:10px;font-family:Arial,Helvetica;font-size:11px;color:#444444;">
            <asp:PlaceHolder ID="plhPDFNoteNotAttached" runat="server" Visible="false">    
                <p><strong>Note: Instant brochures are in Adobe PDF format.</strong> To open, <a href="http://www.adobe.com/products/acrobat/readstep2.html">download & install the free Adobe Reader software.</a> Instant brochures remain available to view, save or print for 7 days.</p>	   
            </asp:PlaceHolder>

            <asp:PlaceHolder ID="plhPDFNoteAttached" runat="server" Visible="false">
                    <p><strong>Note: Instant brochures are in Adobe PDF format.</strong> To open, <a href="http://www.adobe.com/products/acrobat/readstep2.html">download & install the free Adobe Reader software</a>.</p>	
            </asp:PlaceHolder>
            <p><strong>We're here to help you find the right home.</strong> The new home builder(s) you've chosen may follow up with additional information about the new homes &amp; communities you've selected.</p>
            <nhs:emailFooter id="footer" runat="server" />
            <asp:HyperLink ID="lnkBeacon" runat="server" style="visibility:hidden;" />
        </td>
        </tr>
    </table>
</td></tr>
</table>
