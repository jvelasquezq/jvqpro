using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Lead;
using Nhs.Library.Proxy;
using System.Linq;
using Nhs.Library.Web;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Search.Objects;
using Nhs.Search.Objects.Constants;
using Nhs.Utility.Common;


namespace Nhs.Web.Controls.Default.External.EmailTemplates
{
    /// <summary>
    /// Lead Email
    /// </summary>
    public partial class lead : UserControlBase
    {
        private string _requestType = string.Empty;
        private string _firstName = string.Empty;
        private bool _pdfAttached;
        private string _email = string.Empty;
        private string _refer = string.Empty;
        private string _brandName = string.Empty;
        private string _communityName = string.Empty;
        private int _marketId;
        private const string LEAD_PAGE_ACTION = "LeadPageAction";
        private string _planName = string.Empty;
        private bool _IsRCM = false;
        private DataTable requestInfo;
        private bool OnDemandPdf;
        protected void Page_Load(object sender, EventArgs e)
        {
            _refer = UserSession.Refer;
            if (Request.QueryString["attpdf"] != null)
                _pdfAttached = true;
            this.BindControls();
            this.SetLabelCaptions();
            SetStaticLinks();
            setTextToDisplayBrochures();
            this.lblBrandName.Text = _brandName;
            this.lnkSearchNewHomes.NavigateUrl = ResolveAbsoluteUrl("~/" + Pages.Home);
            SetBeaconLink();
            this.SetLinksTracking();
            if (XGlobals.Partner.PartnerId == Convert.ToInt32(PartnersConst.NewHomeSource))
            {
                divFreeBuyingTips.Visible = true;
                divStartAlert.Visible = true;
            }
            if (XGlobals.Partner.BrandPartnerID == Convert.ToInt32(PartnersConst.Pro))
            {
                tdChoices.Visible = false;
            }
        }

        #region Private Methods

        #region Bind Method
        private void BindControls()
        {
            OnDemandPdf = NhsUrl.GetOnDemandPdf;

            int requestId= NhsUrl.GetRequestID;
            _firstName = NhsUrl.GetFirstName;
            List<string> lstCommunities = new List<string>();
            requestInfo = DataProvider.Current.GetRequest(requestId);
            DataTable requestItems = DataProvider.Current.GetRequestItems(requestId);            
            if (requestInfo.Rows.Count > 0)
            {
                if (requestInfo.Rows[0]["request_type_code"].ToString().ToLower() == LeadType.Market)
                    _IsRCM = true;
            }
            else
            if (requestItems.Rows.Count > 0)
                if (requestItems.Rows[0]["request_type_code"].ToString().ToLower() == LeadType.Market)
                    _IsRCM = true;


            if (requestInfo.Rows.Count == 0)
            {
                _requestType = "";
                NhsUrl leadUrl = new NhsUrl();
                if (requestItems.Rows.Count > 0)
                {
                    // gets the lead_page_action 's value, this value was sent to queue and added to the request table.
                    ViewState[LEAD_PAGE_ACTION] = requestItems.Rows[0]["lead_page_action"].ToString();
                    leadUrl.AddParameter(UrlConst.MarketID, requestItems.Rows[0]["market_id"].ToString());

                    if (requestItems.Rows[0]["request_type_code"].ToString().ToLower() == LeadType.Market)
                    {                        
                        leadUrl.Function = Pages.CommunityResults;
                        lnkLeadUrl.NavigateUrl = ResolveAbsoluteUrl("~" + leadUrl);
                    }
                    else
                    {
                        string brandList = string.Empty;
                        foreach (DataRow row in requestItems.Rows)
                            brandList = brandList + row["brand_id"] + ",";

                        leadUrl.AddParameter(UrlConst.LeadType, LeadType.Builder);
                        leadUrl.AddParameter(UrlConst.BrandList, brandList);
                        leadUrl.Function = Pages.LeadsNarrowRequest;
                        lnkLeadUrl.NavigateUrl = ResolveAbsoluteUrl("~" + leadUrl);
                    }
                }
            }
            else
            {
                ViewState[LEAD_PAGE_ACTION] = requestInfo.Rows[0]["lead_page_action"].ToString();
                _requestType = requestInfo.Rows[0]["request_type_code"].ToString();
                _email = requestInfo.Rows[0]["email_address"].ToString();
                footer.Email = _email;
                switch (_requestType.ToLower())
                {
                    case LeadType.Home:
                        string planList = string.Empty;
                        string specList = string.Empty;

                        foreach (DataRow row in requestInfo.Rows)
                        {
                            if (row.IsNull("specification_id"))
                                planList = planList + row["plan_id"] + ",";
                            else
                                specList = specList + row["specification_id"] + ",";
                        }
                        planList = planList.TrimEnd(',');
                        specList = specList.TrimEnd(',');
                        DataTable savedHomes = DataProvider.Current.GetSavedHomes(XGlobals.Partner.PartnerId, planList, specList);

                        string lastPlanId = string.Empty;
                        if (savedHomes.Rows.Count > 0)
                        {
                            foreach (DataRow row in savedHomes.Rows)
                            {
                                if (row.IsNull("specification_id"))
                                {
                                    if (row["plan_id"].ToString() == lastPlanId)
                                        row.Delete();
                                    else
                                        lastPlanId = row["plan_id"].ToString();
                                }
                            }
                            savedHomes.AcceptChanges();
                            this.rptHome.DataSource = savedHomes;
                            this.rptHome.DataBind();

                        }
                        break;
                    default:
                        string builders = ",";

                        string communities = requestInfo.Rows.Cast<DataRow>().Aggregate(",", (current, row) => current + row["community_id"] + ",");

                        communities = communities.TrimStart(',');
                        communities = communities.TrimEnd(',');

                        builders = builders.TrimStart(',');
                        builders = builders.TrimEnd(',');

                        DataTable savedCommunities = DataProvider.Current.GetSavedCommunities(XGlobals.Partner.PartnerId.ToString(), communities);


                        for (int i = 0; i < savedCommunities.Rows.Count; i++)
                        {
                            string communityId = savedCommunities.Rows[i]["community_id"].ToString();
                            string builderId = savedCommunities.Rows[i]["builder_id"].ToString();

                            if (!lstCommunities.Contains(communityId + "," + builderId))
                            {
                                lstCommunities.Add(savedCommunities.Rows[i]["community_id"].ToString() + "," +
                                                   savedCommunities.Rows[i]["builder_id"].ToString());
                            }
                            else
                            {
                                savedCommunities.Rows.RemoveAt(i);
                            }
                        }

                        if (savedCommunities.Rows.Count > 0)
                        {
                            rptCommunity.DataSource = savedCommunities;
                            rptCommunity.DataBind();
                        }
                        break;
                }
            }
            _marketId = Convert.ToInt32(requestItems.Rows[0]["market_id"].ToString());

            PropertySearchParams _searchParamsComm = UserSession.PropertySearchParameters;
            _searchParamsComm.PartnerId = XGlobals.Partner.PartnerId;
            _searchParamsComm.MarketId = _marketId;

            // Get the results from the Search service. 
            SearchService searchService = new SearchService();
            CommunityResultsView _searchResults = searchService.CommunitySearch(_searchParamsComm);

            PropertySearchParams _searchParamsHome = UserSession.PropertySearchParameters;
            _searchParamsHome.PartnerId = XGlobals.Partner.PartnerId;
            _searchParamsHome.MarketId = _marketId;
            _searchParamsHome.HomeStatus = 5;
            SearchService searchServiceHome = new SearchService();
            HomeResultsView _resultsView = searchServiceHome.HomeSearch(_searchParamsHome);

            lnkHomes.Text = "View " + _resultsView.TotalHomes + " Quick Move-In homes available nearby";
            lnkCommunities.Text = "View all " + _searchResults.TotalCommunities + " communities in the " + _searchResults.MarketName + " area";

            NhsUrl loggerUrl = new NhsUrl(Pages.LogRedirect);
            loggerUrl.AddParameter(UrlConst.PartnerID, XGlobals.Partner.PartnerId.ToString(), RouteParamType.Friendly);
            loggerUrl.AddParameter(UrlConst.LogEvent, SetCode(), RouteParamType.Friendly);

            string url = ResolveAbsoluteUrl("/" + loggerUrl + "/?url=");

            NhsUrl homeResults = new NhsUrl(Pages.HomeResults);
            homeResults.AddParameter(UrlConst.MarketID, _marketId.ToString(), RouteParamType.Friendly);
            homeResults.AddParameter(UrlConst.HomeStatus, "5", RouteParamType.Friendly);
            homeResults.AddParameter(UrlConst.EventCode, SetCode(), RouteParamType.Friendly);
            lnkHomes.NavigateUrl = url + ResolveAbsoluteUrl("~/" + homeResults);

            NhsUrl commResults = new NhsUrl(Pages.CommunityResults);
            commResults.AddParameter(UrlConst.MarketID, _marketId.ToString(), RouteParamType.Friendly);
            commResults.AddParameter(UrlConst.EventCode, SetCode(), RouteParamType.Friendly);
            lnkCommunities.NavigateUrl = url + ResolveAbsoluteUrl("~/" + commResults);
        }

        #endregion

        private string SetCode()
        {
            return _IsRCM ? LogImpressionConst.EmailRecommendedClickThrough : LogImpressionConst.EmailBrochureClickThrough;
        }

        private void SetLabelCaptions()
        {
            if (_requestType == LeadType.Market)
                this.ltrNewHomeMatches.Text = "<p style=\"font-size:18px;color:#114981;\"><strong>New Home Matches</strong></p>";
            this.lblFirstName.Text = this.lblFirstNameQc.Text = StringHelper.ToTitleCase(_firstName);
            this.lnkHomePage.Text = XGlobals.Partner.SiteName;
            this.SetResultsSection();
            this.SetLeadText();
            this.SetNotesSection();
            this.SetAdditionalInfoSection();
        }

        private void setTextToDisplayBrochures()
        {
            // plhOtherRequest
            string _LeadAction = string.Empty;
            string textToInclude = string.Empty;
            if (ViewState[LEAD_PAGE_ACTION] != null)
                _LeadAction = ViewState[LEAD_PAGE_ACTION].ToString();
            switch (_LeadAction)
            {
                // Free brochure
                case LeadAction.FreeBrochure:
                case LeadAction.CaFreeBrochure:
                case LeadAction.HovFreeBrochure:
                    textToInclude = "You're on your way to finding a great new home. Click on \"View brochure\" to open your printable instant brochure (PDF). ";
                    this.img4.Src = "[resource:]/globalresources/default/images/email/header_brochure.gif";
                    this.PrintSubject("New home brochure: ");
                    break;
                //Request an appointment
                case LeadAction.RequestApointment:
                case LeadAction.CaRequestApointment:
                case LeadAction.HovRequestApointment:
                    if (XGlobals.Partner.IsRealtor)
                    {
                        textToInclude = "<p>We've received your request to visit " + this._communityName + " by " + this._brandName + ". " + this._brandName + " may follow up to help schedule your property tour and answer questions.</p> <p><strong>In the meantime:</strong> Click \"View brochure\" to see an instant brochure (PDF) with full details � feel free to save it, share it with others, or print it out & take it with you!</p>";
                    }
                    else
                    {
                        textToInclude = "<p>We've received your request to visit " + this._communityName + " by " + this._brandName + ". " + this._brandName + " may follow up to help schedule your property tour and answer questions.</p> <p><strong>In the meantime:</strong> Click \"View brochure\" to see an instant brochure (PDF) with full details & driving directions � feel free to save it, share it with others, or print it out & take it with you!</p>";
                    }
                    this.img4.Src = "[resource:]/globalresources/default/images/email/header_appt.gif";
                    this.PrintSubject("Appointment request: ");
                    break;
                //Contact the builder
                case LeadAction.ContactBuilder:
                case LeadAction.CaContactBuilder:
                case LeadAction.CaRequestBuilderInfo:
                case LeadAction.RequestInfo:
                    if (XGlobals.Partner.IsRealtor)
                    {
                        textToInclude = "<p>We've received your information request for " + this._brandName + ".  " + this._brandName + " may follow up with additional information and answer questions about " + this._communityName + ".</p> <p><strong>In the meantime:</strong> Click \"View brochure\" to see an instant brochure (PDF) with full details � feel free to save it, share it with others, or print it out & take it with you!</p>";
                    }
                    else
                    {
                        textToInclude = "<p>We've received your information request for " + this._brandName + ".  " + this._brandName + " may follow up with additional information and answer questions about " + this._communityName + ".</p> <p><strong>In the meantime:</strong> Click \"View brochure\" to see an instant brochure (PDF) with full details & driving directions � feel free to save it, share it with others, or print it out & take it with you!</p>";
                    }
                    this.img4.Src = "[resource:]/globalresources/default/images/email/header_contact.gif";
                    this.PrintSubject("Builder contact request: ");
                    break;
                //Promotions/Events
                case LeadAction.RequestPromotion:
                case LeadAction.CaRequestPromotion:
                case LeadAction.HovRequestPromotion:
                    if (XGlobals.Partner.IsRealtor)
                    {
                        textToInclude = "<p>We've received your request to learn more about offers & promotions from " + this._brandName + ". " + this._brandName + " may follow up with additional information about the new homes you're interested in, and answer any questions you have.</p> <p><strong>In the meantime:</strong> Click \"View brochure\" to see an instant brochure (PDF) with full details � feel free to save it, share it with others, or print it out & take it with you!</p>";
                    }
                    else
                    {
                        textToInclude = "<p>We've received your request to learn more about offers & promotions from " + this._brandName + ". " + this._brandName + " may follow up with additional information about the new homes you're interested in, and answer any questions you have.</p> <p><strong>In the meantime:</strong> Click \"View brochure\" to see an instant brochure (PDF) with full details & driving directions � feel free to save it, share it with others, or print it out & take it with you!</p>";
                    }
                    this.img4.Src = "[resource:]/globalresources/default/images/email/header_offers.gif";
                    this.PrintSubject("Promotions inquiry: ");
                    break;
                // Request latest inventory
                case LeadAction.QuickMoveIn:
                case LeadAction.CaQuickMoveIn:
                case LeadAction.HovQuickMoveIn:
                    textToInclude = "We've received your request to learn more about the latest inventory from " + this._brandName + ". ";
                    this.img4.Src = "[resource:]/globalresources/default/images/email/header_latest.gif";
                    this.PrintSubject("Inventory request: ");
                    break;

                // Free brochure
                case LeadAction.QuickConnect:

                    textToInclude = "";
                    plhOtherRequest.Visible = false;
                    plhQuickConnect.Visible = true;

                    this.img4.Src = "[resource:]/globalresources/default/images/email/header_brochure.gif";
                    PrintSubjectQc();
                    break;
            }
            ltrlBrochuresText.Text = textToInclude;
        }

        private void PrintSubject(string subjectTitle)
        {
            switch (_requestType.ToLower())
            {
                case LeadType.Home:
                    Response.Write("<SUBJECT>" + subjectTitle + this._planName + " by " + this._brandName + "</SUBJECT>");
                    break;
                default:
                    Response.Write("<SUBJECT>" + subjectTitle + this._communityName + " by " + this._brandName + "</SUBJECT>");
                    break;
            }
        }

        private void PrintSubjectQc()
        {
            Response.Write("<SUBJECT>Requested QuickConnect Properties</SUBJECT>");
        }

        private void SetResultsSection()
        {
            switch (_requestType.ToLower())
            {
                case "":
                    plhNoMatches.Visible = true;

                    if (_pdfAttached)
                    {
                        this.ltrSiteNameNoMatch.Text = XGlobals.Partner.SiteName;
                        this.ltrSiteNameNoMatch.Visible = true;
                    }
                    else
                    {
                        this.lnkHomePageNoMatch.Text = XGlobals.Partner.SiteName;
                        this.lnkHomePageNoMatch.Visible = true;
                    }

                    break;
                case LeadType.Market:
                    this.plhMarketRequest.Visible = true;
                    break;
                default:

                    this.plhOtherRequest.Visible = true;
                    if (_pdfAttached)
                    {
                        this.ltrSiteNameOther.Text = XGlobals.Partner.SiteName + "."; // <br /><br />";
                        this.ltrSiteNameOther.Visible = true;
                    }
                    else
                    {
                        this.lnkHomePageOther.Text = XGlobals.Partner.SiteName + "!"; // <br /><br />";
                        this.lnkHomePageOther.Visible = true;
                    }
                    break;
            }
        }

        private void SetLeadText()
        {
            switch (_requestType)
            {
                case LeadType.Market:
                    plhMarketLeadTypeText.Visible = true;
                    break;
                case LeadType.Builder:
                case LeadType.Home:
                case LeadType.Community:
                    plhOtherLeadTypeText.Visible = true;
                    break;
            }
        }

        private void SetNotesSection()
        {
            if (!_pdfAttached)
            {
                if (_requestType != string.Empty)
                    this.plhPDFNoteNotAttached.Visible = true;
            }
            else
            {
                if (_requestType != string.Empty)
                    this.plhPDFNoteAttached.Visible = true;
            }

        }

        private void SetAdditionalInfoSection()
        {
            if (!_pdfAttached)
            {
                if (_requestType == LeadType.Market)
                {
                    plhAdditionalInfoMarket.Visible = true;
                }
            }
        }

        private string SetHomeDetailLink(HyperLink lnkHomeDetail, int specOrPlanId, string planName, bool isSpec)
        {
            NhsUrl homeDetailUrl = new NhsUrl(Pages.HomeDetail);
            if (isSpec)
            {
                homeDetailUrl.AddParameter(UrlConst.SpecID, specOrPlanId.ToString());
            }
            else
            {
                homeDetailUrl.AddParameter(UrlConst.PlanID, specOrPlanId.ToString());
            }

            lnkHomeDetail.Text = planName;
            return ResolveAbsoluteUrl("~/" + homeDetailUrl);
        }

        private string SetCommDetailLink(HyperLink lnkCommDetail, string communityId, string builderId, string communityName)
        {
            NhsUrl commDetailUrl = new NhsUrl(Pages.CommunityDetail);
            commDetailUrl.AddParameter(UrlConst.CommunityID, communityId);
            commDetailUrl.AddParameter(UrlConst.BuilderID, builderId);
            lnkCommDetail.Text = communityName;
            return ResolveAbsoluteUrl("~/" + commDetailUrl);
        }

        private string GetBrochureUrl(NhsUrl targetUrl, string planId, string specId, string communityId, string builderId)
        {

            NhsUrl brochureUrl = new NhsUrl(Pages.GetBrochure);
            brochureUrl.AddParameter(UrlConst.Email, _email);
            targetUrl.AddParameter(UrlConst.Email, _email);

            brochureUrl.AddParameter(UrlConst.Refer, _refer);
            targetUrl.AddParameter(UrlConst.Refer, _refer);

            if (communityId.Length > 0 && builderId.Length > 0)
            {
                brochureUrl.AddParameter(UrlConst.CommunityID, communityId);
                brochureUrl.AddParameter(UrlConst.BuilderID, builderId);
                targetUrl.AddParameter(UrlConst.CommunityID, communityId);
                targetUrl.AddParameter(UrlConst.BuilderID, builderId);
            }
            if (planId.Length > 0)
            {
                brochureUrl.AddParameter(UrlConst.PlanID, planId);
                targetUrl.Parameters.Add(new NhsUrlParam(NhsLinkParams.PlanID.ToString(), planId, RouteParamType.QueryString));
            }
            if (specId.Length > 0)
            {
                brochureUrl.AddParameter(UrlConst.SpecID, specId);
                targetUrl.AddParameter(UrlConst.SpecID, specId);
            }
            brochureUrl.AddParameter(UrlConst.LeadType, _requestType);
            targetUrl.AddParameter(UrlConst.LeadType, _requestType);
            return ResolveAbsoluteUrl("~/" + brochureUrl);
        }

        private string FormatCurrency(string homeStatus, object price)
        {
            if (homeStatus != HomeStatusType.ModelHome.ToString())
                return StringHelper.FormatCurrency(price);
            
                return string.Empty;
        }
        #endregion

        #region Events
        protected void rptHome_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            ListingInfo alternateListing;
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //Home Detail Link
                DataRowView row = (DataRowView)e.Item.DataItem;
                int planId = DBValue.GetInt(row["plan_id"]);
                int specId = DBValue.GetInt(row["specification_id"]);
                string planName = DBValue.GetString(row["plan_name"]);
                _planName = planName;


                _brandName = DBValue.GetString(row["brand_name"]);
                _communityName = DBValue.GetString(row["community_name"]);

                //Num of BRs
                Label lblNumBedrooms = (Label)e.Item.FindControl("lblNumBedrooms");
                lblNumBedrooms.Text = XGlobals.FormatBedrooms(DBValue.GetInt(row["no_bedrooms"]), true);

                //Num of baths
                Label lblNumBaths = (Label)e.Item.FindControl("lblNumBaths");
                lblNumBaths.Text = XGlobals.ComputeBathrooms(DBValue.GetInt(row["no_baths"]), DBValue.GetInt(row["no_half_baths"]));

                //Num of garages
                Label lblNumGarages = (Label)e.Item.FindControl("lblNumGarages");
                lblNumGarages.Text = DBValue.GetString(row["no_car_garage"]);

                //Price
                Label lblPrice = (Label)e.Item.FindControl("lblPrice");
                lblPrice.Text = FormatCurrency(DBValue.GetString(row["home_status"]), row["price"]);

                //Brand Name
                Label lblBrandName = (Label)e.Item.FindControl("lblBrandName");
                lblBrandName.Text = DBValue.GetString(row["brand_name"]);

                //Community Name
                Label lblCommunityName = (Label)e.Item.FindControl("lblCommunityName");
                lblCommunityName.Text = DBValue.GetString(row["community_name"]);

                //City
                Label lblCity = (Label)e.Item.FindControl("lblCity");
                lblCity.Text = DBValue.GetString(row["city"]);

                //State
                Label lblState = (Label)e.Item.FindControl("lblState");
                lblState.Text = DBValue.GetString(row["state"]);

                //Postal code
                Label lblPostalCode = (Label)e.Item.FindControl("lblPostalCode");
                lblPostalCode.Text = DBValue.GetString(row["postal_code"]);

                //Image homeThumbnailImage = (Image)e.Item.FindControl("imgHomeThumbnail");
                HtmlImage homeThumbnailImage = (HtmlImage)e.Item.FindControl("imgHomeThumbnail");

                string homeImage = DBValue.GetString(row["image_thumbnail"]);
                if (homeImage.Length > 0 && homeImage != "N")
                {
                    homeThumbnailImage.Src = "[resource:]" + homeImage;
                }

                homeThumbnailImage.Alt = DBValue.GetString(row["community_name"]);
                //expect spec id in homeindex.ascx.cs will work with plan id too, since spec id is empty?

                if (!planId.Equals(0) && specId.Equals(0))
                {
                    alternateListing = new ListingInfo(planId, false);
                }
                else
                {
                    alternateListing = new ListingInfo(specId, true);
                }

                //specid with promo text
                //ListingInfo alternateListing = new ListingInfo(304611, true);
                alternateListing.DataBind();

                Literal ltrHotHomesHeadline = (Literal)e.Item.FindControl("ltrHotHomesHeadline");

                if (alternateListing.IsHotHome)
                {
                    PlaceHolder plhHotHomes = (PlaceHolder)e.Item.FindControl("plhHotHomes");
                    plhHotHomes.Visible = true;
                    ltrHotHomesHeadline.Text = alternateListing.HotHomeTitle;
                    HtmlImage imgHotHomes = (HtmlImage)e.Item.FindControl("imgHotHomes");
                    imgHotHomes.Visible = true;
                }

                ICommunity community = XCommunityFactory.CreateCommunity(alternateListing.CommunityID, Configuration.PartnerId, alternateListing.BuilderID);
                //Literal ltrPromoHeadline = (Literal)e.Item.FindControl("ltrPromoHeadline");

                NhsUrl loggerUrlLog = new NhsUrl(Pages.LogRedirect);
                loggerUrlLog.AddParameter(UrlConst.LogEvent, SetCode(), RouteParamType.Friendly);

                if (community != null)
                {
                    if (!String.IsNullOrEmpty(community.PromoTextShort))
                    {
                        PlaceHolder plhSpecialPromo = (PlaceHolder)e.Item.FindControl("plhHomeSpecialPromo");
                        plhSpecialPromo.Visible = true;
                        HtmlImage imgSpecialOffer = (HtmlImage)e.Item.FindControl("imgSpecialOffer");
                        imgSpecialOffer.Visible = true;
                        //ltrPromoHeadline.Text = community.PromoTextShort;

                        //if (!String.IsNullOrEmpty(community.PromoFlyerUrl)) // Always show the PDF link if one exists
                        //{
                        //    HyperLink lnkPromoPDF = (HyperLink)e.Item.FindControl("lnkPromoPDF");
                        //    lnkPromoPDF.Visible = true;
                        //    lnkPromoPDF.NavigateUrl = urlLog + Library.Common.Configuration.PromoFlyerUrl + community.PromoFlyerUrl;
                        //}
                    }
                }

                // Set View Home Detail
                HyperLink lnkHomeDetail = (HyperLink)e.Item.FindControl("lnkHomeDetail");
                NhsUrl loggerUrl = new NhsUrl(Pages.LogRedirect);

                loggerUrl.AddParameter(UrlConst.LogEvent, SetCode(), RouteParamType.Friendly);
                loggerUrl.AddParameter(UrlConst.BuilderID, alternateListing.BuilderID.ToString(), RouteParamType.Friendly);
                loggerUrl.AddParameter(UrlConst.PartnerID, Configuration.PartnerId.ToString(), RouteParamType.Friendly);
                loggerUrl.AddParameter(UrlConst.CommunityID, alternateListing.CommunityID.ToString(), RouteParamType.Friendly);
                string url = ResolveAbsoluteUrl("/" + loggerUrl + "/?url=");
                if (specId != 0)
                {
                    lnkHomeDetail.NavigateUrl = url + this.SetHomeDetailLink(lnkHomeDetail, specId, planName, true) + "/" + UrlConst.EventCode + "-" + SetCode();
                }
                else
                {
                    lnkHomeDetail.NavigateUrl = url + this.SetHomeDetailLink(lnkHomeDetail, planId, planName, false) + "/" + UrlConst.EventCode + "-" + SetCode();
                }
                // Set View Brochure
                HyperLink lnkViewBrochure = (HyperLink)e.Item.FindControl("lnkViewBrochure");

                loggerUrl = new NhsUrl(Pages.LogRedirect);
                loggerUrl.AddParameter(UrlConst.LogEvent, SetCode(), RouteParamType.Friendly);
                url = ResolveAbsoluteUrl("/" + loggerUrl + "/?url=");
                var commCall = new XCommunity();
                var nonPdfUrl = commCall.GetNonPdfBrochureUrl(community.CommunityID, planId, specId, community.Builder.BuilderId);
                var targetUrl = new NhsUrl {Function = Pages.GetBrochure};
             
                var requestid = "";
                if (specId != 0)
                {
                    requestid = (requestInfo.Rows.Cast<DataRow>()
                                            .Where(dr => DBValue.GetInt(dr["specification_id"]) == specId)
                                            .Select(dr => DBValue.GetString(dr["request_item_id"]))).FirstOrDefault();
                }
                else
                {
                    requestid = (requestInfo.Rows.Cast<DataRow>()
                                            .Where(dr => DBValue.GetInt(dr["plan_id"]) == planId)
                                            .Select(dr => DBValue.GetString(dr["request_item_id"]))).FirstOrDefault();
                }

                var urlPart = OnDemandPdf ? ViewBrochureHelper.ViewBrochureLink(_firstName, requestid, _email, "")
                                        : GetBrochureUrl(targetUrl, DBValue.GetString(row["plan_id"]),
                                                         DBValue.GetString(row["specification_id"]), string.Empty,
                                                         string.Empty) + "/" + UrlConst.EventCode + "-" + SetCode();

                if (!string.IsNullOrEmpty(nonPdfUrl) && XGlobals.Partner.BrandPartnerID != PartnersConst.Pro.ToType<int>())
                    urlPart = nonPdfUrl;

                lnkViewBrochure.NavigateUrl = url +  HttpUtility.UrlEncode(urlPart);

                List<XPromotion> lstPromotions = PromotionHelper.GetCommunityPromotions(community.CommunityID, community.Builder.BuilderId);
                if (lstPromotions.Count > 0)
                {
                    Repeater rptPromotionResults = (Repeater)e.Item.FindControl("rptPromotionResults");
                    rptPromotionResults.Visible = true;
                    rptPromotionResults.DataSource = lstPromotions;
                    rptPromotionResults.DataBind();
                    RepeaterItem item = rptPromotionResults.Items[rptPromotionResults.Items.Count - 1];
                    Literal uxParagraph = (Literal)item.FindControl("uxParagraph");
                    uxParagraph.Text = "<div style='width:100%;padding:4px 0 0 0;'>";
                    Literal uxParagraphClose = (Literal)item.FindControl("uxParagraphClose");
                    uxParagraphClose.Text = "</div>";
                }
            }
        }

        protected void rptCommunity_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView row = (DataRowView)e.Item.DataItem;

                string communityId = DBValue.GetString(row["community_id"]);
                string builderId = DBValue.GetString(row["builder_id"]);
                string communityName = DBValue.GetString(row["community_name"]);

                // Set View Community Detail
                HyperLink lnkCommDetail = (HyperLink)e.Item.FindControl("lnkCommDetail");
                NhsUrl loggerUrl = new NhsUrl(Pages.LogRedirect);
                loggerUrl.AddParameter(UrlConst.LogEvent, SetCode(), RouteParamType.Friendly);
                loggerUrl.AddParameter(UrlConst.BuilderID, builderId, RouteParamType.Friendly);
                loggerUrl.AddParameter(UrlConst.PartnerID, Configuration.PartnerId.ToString(), RouteParamType.Friendly);
                loggerUrl.AddParameter(UrlConst.CommunityID, communityId, RouteParamType.Friendly);
                string url = ResolveAbsoluteUrl("/" + loggerUrl + "/?url=");
                lnkCommDetail.NavigateUrl = url + this.SetCommDetailLink(lnkCommDetail, communityId, builderId, communityName) + "/" + UrlConst.EventCode + "-" + SetCode();

                _brandName = DBValue.GetString(row["brand_name"]);
                _communityName = DBValue.GetString(row["community_name"]);
                PlaceHolder plhCommAddress = (PlaceHolder)e.Item.FindControl("plhCommAddress");
                PlaceHolder plhArea = (PlaceHolder)e.Item.FindControl("plhArea");
                if (row["listing_type_flag"].ToString() == "B")
                    plhArea.Visible = true;
                else
                    plhCommAddress.Visible = true;

                //Brand Name
                Label lblBrandName = (Label)e.Item.FindControl("lblBrandName");
                lblBrandName.Text = DBValue.GetString(row["brand_name"]);

                //Community Name
                Label lblCommunityName = (Label)e.Item.FindControl("lblCommunityName");
                lblCommunityName.Text = DBValue.GetString(row["community_name"]);

                //City
                Label lblCity = (Label)e.Item.FindControl("lblCity");
                lblCity.Text = DBValue.GetString(row["city"]);

                //State
                Label lblState = (Label)e.Item.FindControl("lblState");
                lblState.Text = DBValue.GetString(row["state"]);

                //Postal code
                Label lblPostalCode = (Label)e.Item.FindControl("lblPostalCode");
                lblPostalCode.Text = DBValue.GetString(row["postal_code"]);

                //Market Name
                Label lblMarket = (Label)e.Item.FindControl("lblMarket");
                lblMarket.Text = DBValue.GetString(row["market_name"]);

                // Set View Brochure
                HyperLink lnkViewBrochure = (HyperLink)e.Item.FindControl("lnkViewBrochure");
                loggerUrl = new NhsUrl(Pages.LogRedirect);
                loggerUrl.AddParameter(UrlConst.LogEvent, SetCode(), RouteParamType.Friendly);
                url = ResolveAbsoluteUrl("/" + loggerUrl + "/?url=");

                var commCall = new XCommunity();
                var nonPdfUrl = commCall.GetNonPdfBrochureUrl(communityId.ToType<int>(), 0, 0, builderId.ToType<int>());
                var targetUrl = new NhsUrl { Function = Pages.GetBrochure };

                var requestid = (requestInfo.Rows.Cast<DataRow>()
                                        .Where(dr => DBValue.GetString(dr["community_id"])== communityId)
                                        .Select(dr => DBValue.GetString(dr["request_item_id"]))).FirstOrDefault();

                var urlPart = OnDemandPdf
                                        ? ViewBrochureHelper.ViewBrochureLink(_firstName, requestid, _email, "")
                                        : GetBrochureUrl(targetUrl, string.Empty, string.Empty, communityId, builderId) +
                                          "/" +
                                          UrlConst.EventCode + "-" + SetCode();
               
                if (!string.IsNullOrEmpty(nonPdfUrl) && XGlobals.Partner.BrandPartnerID != PartnersConst.Pro.ToType<int>())
                    urlPart = nonPdfUrl;

                lnkViewBrochure.NavigateUrl = url +  HttpUtility.UrlEncode(urlPart);
                string communityImage = DBValue.GetString(row["spotlight_thumbnail"]);

                if (communityImage.Length > 0 && communityImage != "N")
                {
                    //Image communityThumbnailImage = (Image)e.Item.FindControl("imgCommunityThumbnail");
                    HtmlImage communityThumbnailImage = (HtmlImage)e.Item.FindControl("imgCommunityThumbnail");
                    communityThumbnailImage.Src = "[resource:]" + communityImage;
                    communityThumbnailImage.Alt = DBValue.GetString(row["community_name"]);
                }

                //Literal ltrPromoHeadline = (Literal)e.Item.FindControl("ltrPromoHeadline");
                ICommunity community = XCommunityFactory.CreateCommunity(Convert.ToInt32(communityId), XGlobals.Partner.PartnerId, Convert.ToInt32(builderId));
                if (community != null)
                {

                    if (!String.IsNullOrEmpty(community.PromoTextShort))
                    {
                        PlaceHolder plhCommSpecialPromo = (PlaceHolder)e.Item.FindControl("plhCommSpecialPromo");
                        plhCommSpecialPromo.Visible = true;

                        HtmlImage imgSpecialOffer = (HtmlImage)e.Item.FindControl("imgSpecialOffer");
                        imgSpecialOffer.Visible = true;
                        //ltrPromoHeadline.Text = community.PromoTextShort;

                        //if (!String.IsNullOrEmpty(community.PromoFlyerUrl)) // Always show the PDF link if one exists
                        //{
                        //    HyperLink lnkPromoPDF = (HyperLink)e.Item.FindControl("lnkPromoPDF");
                        //    lnkPromoPDF.Visible = true;
                        //    lnkPromoPDF.NavigateUrl = url + Library.Common.Configuration.PromoFlyerUrl + community.PromoFlyerUrl;
                        //}
                    }


                    var imgHotHomes = (HtmlImage)e.Item.FindControl("imgHotHomes");
                    imgHotHomes.Visible = DBValue.GetBool(row[CommunityDBFields.HasHotHome]); 
                }

                var lblPrice = (Label)e.Item.FindControl("lblPrice");
                if (!string.IsNullOrEmpty(DBValue.GetString(row["price_low"])) && !string.IsNullOrEmpty(DBValue.GetString(row["price_high"])))
                {
                    string priceLabel = StringHelper.PrettyPrintRange(Convert.ToDouble(DBValue.GetString(row["price_low"])), System.Convert.ToDouble(DBValue.GetString(row["price_high"])), "c0");
                    String[] array = new String[2];
                    String delimiter = "m";
                    char[] delimiterChar = delimiter.ToCharArray();
                    array = priceLabel.Split(delimiterChar);

                    try { lblPrice.Text = array[1]; }
                    catch { }
                }

                List<XPromotion> lstPromotions = PromotionHelper.GetCommunityPromotions(community.CommunityID, community.Builder.BuilderId);
                if (lstPromotions.Count > 0)
                {
                    Repeater rptPromotionResults = (Repeater)e.Item.FindControl("rptPromotionResults");
                    rptPromotionResults.Visible = true;
                    rptPromotionResults.DataSource = lstPromotions;
                    rptPromotionResults.DataBind();
                    RepeaterItem item = rptPromotionResults.Items[rptPromotionResults.Items.Count - 1];
                    Literal uxParagraph = (Literal)item.FindControl("uxParagraph");
                    uxParagraph.Text = "<div style='width:100%;padding:4px 0 0 0;'>";
                    Literal uxParagraphClose = (Literal)item.FindControl("uxParagraphClose");
                    uxParagraphClose.Text = "</div>";
                }
            }
        }

        private void SetStaticLinks()
        {
            NhsUrl homePageUrl = new NhsUrl(Pages.Home);
            homePageUrl.AddParameter(UrlConst.EventCode, SetCode(), RouteParamType.QueryString);
            lnkHomePage.NavigateUrl = ResolveAbsoluteUrl("~/" + homePageUrl);
            lnkHomePageOther.NavigateUrl = ResolveAbsoluteUrl("~/" + homePageUrl);
            lnkHomePageNoMatch.NavigateUrl = ResolveAbsoluteUrl("~/" + homePageUrl);
        }

        private void SetBeaconLink()
        {
            if (!String.IsNullOrEmpty(_email))
            {
                NhsUrl loggerUrl = new NhsUrl(Pages.EventLogger);
                loggerUrl.AddParameter(UrlConst.LogEvent, LogImpressionConst.OpenEmailBrochure, RouteParamType.Friendly);
                loggerUrl.AddParameter(UrlConst.Email, _email, RouteParamType.QueryString);
                this.lnkBeacon.ImageUrl = ResolveAbsoluteUrl("~/" + loggerUrl);
            }
        }

        public void SetLinksTracking()
        {
            NhsUrl loggerUrl = new NhsUrl(Pages.LogRedirect);
            loggerUrl.AddParameter(UrlConst.LogEvent, SetCode(), RouteParamType.Friendly);
            var loggerStr = loggerUrl.ToString();
            if (XGlobals.Partner.BrandPartnerID != XGlobals.Partner.PartnerId)
                loggerStr = loggerStr.Substring(loggerStr.IndexOf("/")+1);
            string url = ResolveAbsoluteUrl("/" + loggerStr + "/?url=");

            NhsUrl home = new NhsUrl(Pages.Home);
            home.AddParameter(UrlConst.EventCode, SetCode(), RouteParamType.QueryString);
            lnkSearchNewHomes.NavigateUrl = url + PathHelpers.CleanNavigateUrl(ResolveAbsoluteUrl("~/" + home));

            NhsUrl savedHomes = new NhsUrl(Pages.SavedHomes);
            savedHomes.AddParameter(UrlConst.EventCode, SetCode(), RouteParamType.QueryString);
            lnkOpenMySaveProperties.NavigateUrl = url + PathHelpers.CleanNavigateUrl(ResolveAbsoluteUrl("~/" + savedHomes));

            NhsUrl createAlert = new NhsUrl(Pages.CreateAlert);
            createAlert.AddParameter(UrlConst.EventCode, SetCode(), RouteParamType.QueryString);
            lnkStartSearchAlert.NavigateUrl = url + PathHelpers.CleanNavigateUrl(ResolveAbsoluteUrl("~/" + createAlert));

            NhsUrl editAccount = new NhsUrl(Pages.EditAccount);
            editAccount.AddParameter(UrlConst.EventCode, SetCode(), RouteParamType.QueryString);
            lnkGetOurNewspaper.NavigateUrl = url + PathHelpers.CleanNavigateUrl(ResolveAbsoluteUrl("~/" + editAccount));

            NhsUrl register = new NhsUrl(Pages.Register);            
            lnkCreateFreeAccount.NavigateUrl = url + PathHelpers.CleanNavigateUrl(ResolveAbsoluteUrl("~/" + register));

        }

        protected void rptPromotionResults_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                XPromotion promotion = (XPromotion)e.Item.DataItem;

                if (!String.IsNullOrEmpty(promotion.PromoTextShort))
                {
                    // Get a reference to the controls. 
                    Literal uxParagraph = (Literal)e.Item.FindControl("uxParagraph");
                    Literal ltrPromoHeadline = (Literal)e.Item.FindControl("ltrPromoHeadline");
                    Literal uxParagraphClose = (Literal)e.Item.FindControl("uxParagraphClose");
                    uxParagraph.Text = "<div style='border-bottom:1px solid #f16936;width:100%;padding:4px 0 4px 0;'>";
                    uxParagraphClose.Text = "</div>";
                    ltrPromoHeadline.Text = promotion.PromoTextShort;
                    NhsUrl loggerUrlLog = new NhsUrl(Pages.LogRedirect);
                    loggerUrlLog.AddParameter(UrlConst.LogEvent, SetCode(), RouteParamType.Friendly);
                    string urlLog = ResolveAbsoluteUrl("/" + loggerUrlLog + "/?url=");

                    if (!String.IsNullOrEmpty(promotion.PromoFlyerFilePath)) // Always show the PDF link if one exists
                    {
                        HyperLink lnkPromoPDF = (HyperLink)e.Item.FindControl("lnkPromoPDF");
                        lnkPromoPDF.Visible = true;
                        lnkPromoPDF.NavigateUrl = urlLog + Configuration.PromoFlyerUrl +
                                                  promotion.PromoFlyerFilePath;
                    }

                }
            }
        }


        #endregion
    }
}
