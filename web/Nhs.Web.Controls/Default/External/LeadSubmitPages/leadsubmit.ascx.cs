using System;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Web;

namespace Nhs.Web.Controls.Default.External.LeadSubmitPages
{
    public partial class leadsubmit : LeadSubmitPageBase
    {
        private string leadSubmitPartner = NhsUrlHelper.GetFriendlyUrl().Function.ToLower();

        protected void Page_Init(object s, EventArgs e)
        {
            switch (leadSubmitPartner)
            {
                case "yahoo":
                    partnerId = 9;
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Address);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.City);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.State);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.PostalCode);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Phone);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.PhoneExtension);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Title);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.WillMoveInDays);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.FinancingPref);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Comment);
                    break;
                case "myrealty":
                    partnerId = 314;
                    break;
                case "myareaguide":
                    partnerId = 87;
                    break;
                case "hpc":
                    partnerId = 37;
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Address);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.City);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.State);
                    break;
                case "expotucasa":
                    partnerId = 10040;
                    break;
                case "trebsubmitlead":
                    partnerId = 321;
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Address);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.City);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.State);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Phone);
                    break;
                case "ncigabriels":
                    partnerId = 335;
                    break;
                case "terabitz":
                    partnerId = 336;
                    break;
                case "scripps":
                    partnerId = 340;
                    break;
                case "urbancondoliving":
                    partnerId = 347;
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Address);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.City);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.State);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Phone);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.PhoneExtension);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.WillMoveInDays);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.FinancingPref);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Title);
                    break;
                case "realestate":
                    partnerId = 357;
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Address);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.City);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.State);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Phone);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.PhoneExtension);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Title);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.WillMoveInDays);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.FinancingPref);
                    break;
                case "palmbeachpost":
                    partnerId = 320;
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Address);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.City);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.State);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Phone);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.PhoneExtension);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Title);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.WillMoveInDays);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.FinancingPref);
                    break;
                case "nhsmobile":
                    partnerId = 8503;
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Address);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.City);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.State);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Phone);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.PhoneExtension);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Title);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.WillMoveInDays);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.FinancingPref);
                    break;
                case "nhsiphone":
                    partnerId = 8550;
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Address);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.City);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.State);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Phone);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.PhoneExtension);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Title);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.WillMoveInDays);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.FinancingPref);
                    break;
                case "khovmobile":
                    partnerId = 8505;
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Address);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.City);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.State);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Phone);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.PhoneExtension);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Title);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.WillMoveInDays);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.FinancingPref);
                    break;
                case "mobilepulte":
                    partnerId = 8521;
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Address);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.City);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.State);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Phone);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.PhoneExtension);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Title);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.WillMoveInDays);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.FinancingPref);
                    break;
                case "mobiledelwebb":
                    partnerId = 8522;
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Address);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.City);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.State);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Phone);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.PhoneExtension);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Title);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.WillMoveInDays);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.FinancingPref);
                    break;
                case "mobiledrees":
                    partnerId = 8523;
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Address);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.City);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.State);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Phone);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.PhoneExtension);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Title);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.WillMoveInDays);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.FinancingPref);
                    break;
                case "mobilemeritage":
                    partnerId = 8524;
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Address);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.City);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.State);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Phone);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.PhoneExtension);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Title);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.WillMoveInDays);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.FinancingPref);
                    break;
                case "mobilebeazer":
                    partnerId = 8525;
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Address);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.City);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.State);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Phone);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.PhoneExtension);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Title);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.WillMoveInDays);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.FinancingPref);
                    break;
                case "lennarmobi":
                    partnerId = 8526;
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Address);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.City);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.State);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Phone);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.PhoneExtension);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.Title);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.WillMoveInDays);
                    fieldsNotToValidate.Add(LeadSubmitPageFields.FinancingPref);
                    break;
            }
        }
    }
}
