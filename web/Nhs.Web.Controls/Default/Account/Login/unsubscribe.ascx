<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="unsubscribe.ascx.cs" Inherits="Nhs.Web.Controls.Default.Account.Login.unsubscribe" %>
<div class="nhsContent">
    <asp:PlaceHolder ID="plhUnsubscribe" runat="server">
        <h2>Unsubscribe</h2>
            
        <asp:Literal id="ltrErrorMessage" runat="server" />
        
        <p>Enter the email address receiving emails from our site and press "Unsubscribe" to stop receiving emails and newsletters from this site and related partners.</p>
        
         <div class="nhsRecoverForm">
            <fieldset>
                <p>
                <strong><label for="<%=txtEmailEdit.ClientID%>">Email address:</label></strong>
                    <asp:TextBox ID="txtEmailEdit" Runat="server"></asp:TextBox></p>
                    <p class="nhsErrorSmall"><asp:Literal ID="litEmail" runat="server" Visible="False"></asp:Literal></p>
            </fieldset> 
            <asp:Button ID="btnUnsubscribe" Runat="server" CssClass="btn btnUnsubscribe" Text="Unsubscribe" OnClick="btnUnsubscribe_Click" />       
        </div>
        
        <p class="nhsLegalText" style="float:left;width:400px;margin-left:15px;">If you continue to receive messages at this address, please contact <a href="mailto:support@thebdx.com">support@thebdx.com</a> for assistance. For best service, please forward a copy of the newsletter email you received.</p>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="plhThankYou" runat="server" Visible="false">
    <h2>Unsubscribe Successful</h2>
        <p><asp:Label ID="lblMail" runat="server" Text=""></asp:Label>
        has been removed from our mailing lists. Thank you!
        </p>
        <p><nhs:Link ID="lnkSearch" runat="server" Function="Home" Visible="true" Text="Search for new homes"></nhs:Link></p>

    </asp:PlaceHolder>
</div>




