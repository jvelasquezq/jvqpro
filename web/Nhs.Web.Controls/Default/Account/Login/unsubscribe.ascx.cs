using System;
using Nhs.Library.Web;
using Nhs.Library.Enums;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Library.Common;
using Nhs.Utility.Common;

namespace Nhs.Web.Controls.Default.Account.Login
{
    public partial class unsubscribe : UserControlBase
    {
        private IProfile _userProfile = UserSession.UserProfile;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // Case 31202: SEO "noindex" robots tag 
                if (XGlobals.Partner.PartnerId == Convert.ToInt32(PartnersConst.NewHomeSource) || XGlobals.Partner.PartnerId == Convert.ToInt32(PartnersConst.Move) ||
                    XGlobals.Partner.BrandPartnerID == Convert.ToInt32(PartnersConst.Move))
                {
                    MetaRegistrar metaRegistrar = new MetaRegistrar(this.Page);
                    metaRegistrar.NHSAddRobotsDescription("noindex");
                }

                string userId = NhsUrl.GetUserId;

                // Case 25187 - Combines the unsubscribe functionality for
                // all three subscription types and supports backwards functionality
                // for the alerts_unsubscribe.ascx URLs

                //this is from chat agent search alert
                if(StringHelper.IsValidEmail(userId))
                {
                    txtEmailEdit.Text = userId;
                }
                else if (string.IsNullOrEmpty(_userProfile.LogonName) && !string.IsNullOrEmpty(userId))
                {
                    Profile tmpProfile = new Profile(userId);
                    tmpProfile.DataBind(userId);
                    txtEmailEdit.Text = tmpProfile.LogonName;
                }
                else
                {
                    txtEmailEdit.Text = _userProfile.LogonName;
                }

                base.AddCanonicalTag();
            }
        }
        /// <summary>
        /// validates for registered mail and unsubsribe user.
        /// </summary>        
        protected void btnUnsubscribe_Click(object sender, EventArgs e)
        {
            Boolean fValid = true;

            /*if (this.txtEmailEdit.Text != string.Empty)
            {
                if (!StringHelper.IsValidEmail(this.txtEmailEdit.Text))
                {
                    fValid = false;
                    this.litEmail.Text = StringResource.MSG_UNSUBSCRIBE_INVALID_EMAIL;
                    litEmail.Visible = true;
                }
            }
            else*/
            if(string.IsNullOrEmpty(this.txtEmailEdit.Text))
            {
                this.litEmail.Text = StringResource.MSG_UNSUBSCRIBE_NO_EMAIL;
                litEmail.Visible = true;
                fValid = false;
            }

            if (fValid)
            {
                UnsubscribeResult unsubsResult = _userProfile.UnSubscribe(XGlobals.Partner.PartnerId, txtEmailEdit.Text ,"Y", "N");

                switch (unsubsResult)
                {
                    case UnsubscribeResult.Successful:
                    case UnsubscribeResult.InvalidEmail:
                        this.plhUnsubscribe.Visible = false;
                        this.plhThankYou.Visible = true;
                        lblMail.Text = txtEmailEdit.Text;
                        // sets to new session 
                        lnkSearch.Parameters.Add(new Param(NhsLinkParams.SessionId, "new", UrlParamType.Friendly));

                        this._userProfile.MailList = "0";
                        this._userProfile.MarketOptIn = "0";
                        this._userProfile.WeeklyNotifierOptIn = false;
                        break;
                    /*case UnsubscribeResult.InvalidEmail:
                        this.litEmail.Text = StringResource.MSG_UNSUBSCRIBE_NO_EMAIL;
                        litEmail.Visible = true;
                        break;*/
                }
            }
        }
    }
}
