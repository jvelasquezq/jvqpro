using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Nhs.Library.Helpers;
using Nhs.Library.Helpers.Property;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Web;
using Nhs.Library.Business;
using Nhs.Library.Enums;
using Nhs.Library.Constants;
using Nhs.Library.Common;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects;
using Nhs.Search.Objects.Constants;
using Nhs.Utility.Common;
using Nhs.Web.Controls.Default.Common;
using Nhs.Library.Proxy;
using Nhs.Library.Business.Factory;
using Nhs.Mvc.Biz.Services.Concrete;
using System.Linq;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using StructureMap;
using UrlConst = Nhs.Library.Constants.UrlConst;

namespace Nhs.Web.Controls.Default.PropertySearch
{
    public partial class home_results : UserControlBase
    {
        #region "Members"
        private NhsPropertySearchParams _searchParams;
        private Dictionary<int, bool> commsStatus; 
        private PlaceHolder _anySchoolDistrictFacet = new PlaceHolder();
        private HomeResultsView _resultsView;
        private PagingTools _pagingToolsTop;
        private PagingTools _pagingToolsBottom;
        private int _pageNumber = 1;
        private string _searchType = string.Empty;
        private string _previousGroupingValue = "xxx";
        int Width = ModalWindowsConst.LeadsRequestInfoShortWidth ;
        int Height = ModalWindowsConst.LeadsRequestInfoShortHeight + 1;

        readonly string _leazdPage = XPartnerHelper.IsNonStandardPartner(XGlobals.Partner.PartnerId) || XGlobals.Partner.IsRealtor ? Pages.LeadsRequestInfo :
        Pages.LeadsRequestBrochureModal;
        #endregion

        #region "Properties"
        public int MarketId
        {
            get
            {
                return _searchParams.MarketId;
            }
        }
        #endregion

        #region "Events"
        protected void Page_Load(object sender, EventArgs e)
        {
            //Add canonical string
            base.AddCanonicalWithExcludeParams(new List<string> { "state" });
            var marketDfuReader = new MarketDfuReader();
            rptCityFacets.ItemDataBound += rptCityFacets_ItemDataBound;
         
            _pagingToolsTop = (PagingTools)ctlPagingToolsTop;
            _pagingToolsTop.ResultsPerPage = Configuration.CommunityResultsPageSize;
            _pagingToolsTop.PageNumberChanged += PageNumberChanged;

            _pagingToolsBottom = (PagingTools)ctlPagingToolsBottom;
            _pagingToolsBottom.ResultsPerPage = Configuration.CommunityResultsPageSize;
            _pagingToolsBottom.PageNumberChanged += PageNumberChanged;

            if (!Page.IsPostBack)
            {
                if (NhsRoute.IsBrandPartnerNhsPro)
                {
                    Width += 160;
                    Height += 10;
                }

                InitializeNonPostback(false);
                BindControls();
            }
            else
            {
                InitializePostback();
            }

            lnkSearchCommunities.Text = @"Search for new home community locations in the greater " + _resultsView.MarketName + @" area with a map";

            string resultsUrl;
            //Case 77642: Changes are related to migrate DFU Functionality from BHIConfig to BHIContent
            var dfuMarketList = (XGlobals.Partner.PartnerId == XGlobals.Partner.BrandPartnerID) ? marketDfuReader.GetDfuMarketsForBrandPartner(XGlobals.Partner.BrandPartnerID) : null;
            if (dfuMarketList != null && dfuMarketList.MarketList.Exists(item => item.MarketId == _searchParams.MarketId))  
            {
                var market = XMarketFactory.CreateMarket(_searchParams.MarketId);

                resultsUrl = string.Format(@"/{0}/{1}/{2}-area",
                                    Pages.CommunityResults,
                                    market.StateName.RemoveSpecialCharacters().Replace(" ", "-"),
                                    market.MarketName.RemoveSpecialCharacters().Replace(" ", "-"));


                lnkSearchCommunities.NavigateUrl = resultsUrl.ToLower();
                lnkComingSoon.NavigateUrl = (resultsUrl + "/" + UrlConst.ComingSoon + "-true").ToLower();
            }
            else
            {
                var commSearchParams = _searchParams.GetUrlParams();
                resultsUrl = "~/" + new NhsUrl(Pages.CommunityResults, commSearchParams).ToString();

                lnkSearchCommunities.NavigateUrl = resultsUrl.ToLower();
                lnkComingSoon.NavigateUrl = (resultsUrl + "/" + UrlConst.ComingSoon + "-true").ToLower();
            }


            plhHomeResultsTab.Visible = XGlobals.Partner.BrandPartnerID == Convert.ToInt32(PartnersConst.Move);

            NhsUrl mlsUrl = NhsUrlHelper.GetFriendlyUrl();            
            mlsUrl.Function = Pages.MlsHomeResults;
            lnkMLSHomes.NavigateUrl = "~/" + mlsUrl;

            if (XGlobals.Partner.PartnerId == Convert.ToInt32(PartnersConst.Yahoo))
                divPartnerAd.Visible = false;
        }

        protected void Page_UnLoad(object sender, EventArgs e)
        {
            UserSession.PropertySearchParameters = _searchParams;
            var lastSearchUrl = new NhsUrl(Pages.CommunityResults, _searchParams.GetUrlParams());
            UserSession.SetItem(SessionConst.LastSearchUrl, ResolveUrl("~/" + lastSearchUrl));
        }

        protected void PageNumberChanged(object sender, EventArgs e)
        {
            // Get the new page number from the paging tools control. 
            int newPageNumber = ((PagingTools)sender).PageNumber;

            // Syncronize the top & bottom paging tools controls. 
            _pagingToolsBottom.PageNumber = newPageNumber;
            _pagingToolsTop.PageNumber = newPageNumber;

            // Update results. 
            BindControls();
        }

        protected void rptMoreSchools_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Footer)
            {
                var anySchool = (PlaceHolder)e.Item.FindControl("plhAnySchoolDistrictFacet");
                anySchool.Controls.Add(_anySchoolDistrictFacet);
            }
        }

        protected void rptCityFacets_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var comma = (Literal)e.Item.FindControl("litComma");
                comma.Visible = (e.Item.ItemIndex < ((List<FacetOption>)((Repeater)sender).DataSource).Count - 1) ||
                                rptMoreCityFacets.Visible;
            }
        }

        protected void rptHomeResults_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                #region Control Reference
                // Get a reference to the controls. 
                HyperLink lnkHomeThumbnail = (HyperLink)e.Item.FindControl("lnkHomeThumbnail");
                Image imgHomeThumbnail = (Image)e.Item.FindControl("imgHomeThumbnail");
                Literal litHomeImageCount = (Literal) e.Item.FindControl("litHomeImageCount");
                HyperLink lnkHomeName = (HyperLink)e.Item.FindControl("lnkHomeName");
                Literal litHomePrice = (Literal)e.Item.FindControl("litHomePrice");
                Literal litHomeBedrooms = (Literal)e.Item.FindControl("litHomeBedrooms");
                Literal litHomeBathrooms = (Literal)e.Item.FindControl("litHomeBathrooms");
                Literal litHomeGarages = (Literal)e.Item.FindControl("litHomeGarages");
                Image imgHomeStatusImage = (Image)e.Item.FindControl("imgHomeStatusImage");
                Literal litBrandName = (Literal)e.Item.FindControl("litBrandName");
                Literal litHomeSquareFeet = (Literal)e.Item.FindControl("litHomeSquareFeet");
                Literal litHomeCommunityName = (Literal)e.Item.FindControl("litHomeCommunityName");
                Literal litHomeCity = (Literal)e.Item.FindControl("litHomeCity");
                Literal litHomeState = (Literal)e.Item.FindControl("litHomeState");
                Literal litHomeZip = (Literal)e.Item.FindControl("litHomeZip");
                Literal litHomeMarketingDescription = (Literal)e.Item.FindControl("litHomeMarketingDescription");
                Literal litHotHomeTitle = (Literal)e.Item.FindControl("litHotHomeTitle");
                Literal litHotHomeDescription = (Literal)e.Item.FindControl("litHotHomeDescription");
                HtmlGenericControl divResultsRow = (HtmlGenericControl)e.Item.FindControl("divResultsRow");
                HtmlGenericControl divHotHomeSummary = (HtmlGenericControl)e.Item.FindControl("divHotHomeSummary");
                HyperLink lnkBrochure = (HyperLink)e.Item.FindControl("lnkBrochure");
                HyperLink lnkHotHomeInfo = (HyperLink)e.Item.FindControl("lnkHotHomeInfo");
                HyperLink lnkVirtualTour = (HyperLink)e.Item.FindControl("lnkVirtualTour");
                HyperLink lnkPlanViewer = (HyperLink)e.Item.FindControl("lnkPlanViewer");
                HyperLink lnkSpecialOffer = (HyperLink)e.Item.FindControl("lnkSpecialOffer");
                Image communityGreenImage = (Image)e.Item.FindControl("imgCommunityGreen");
                Image imgSpecialOffer = (Image)e.Item.FindControl("imgSpecialOffer");
                
                HomeResult currentHome = (HomeResult)e.Item.DataItem;
                var result = commsStatus.FirstOrDefault(p => p.Key == currentHome.CommunityId);
                if (!result.Value)
                    divResultsRow.Attributes.Add("class", "nhsHomeResultsRow nhsUnbilled");
                Control nhs_LockIcon = (Control) e.Item.FindControl("nhs_LockIcon");
                nhs_LockIcon.Visible =NhsRoute.IsBrandPartnerNhsPro && UserSession.UserProfile.ActorStatus == WebActors.GuestUser;

                #endregion

                IListing listing = null;

                //create listing object from SpecId or PlanId 
                if (currentHome.SpecId != 0)
                {
                    listing = XListingFactory.CreateListing(currentHome.SpecId, ListingType.Spec);
                }
                else if (currentHome.PlanId != 0)
                {
                    listing = XListingFactory.CreateListing(currentHome.PlanId, ListingType.Plan);
                }

                #region Handle Result Row Info
                // add vtour and brochure links
                if (!string.IsNullOrEmpty(listing.VirtualTourURL))
                {
                    var tourUrl = new NhsUrl(Pages.LogRedirect);

                    tourUrl.Parameters.Add(UrlConst.ExternalURL, listing.VirtualTourURL, RouteParamType.QueryString);
                    tourUrl.Parameters.Add(UrlConst.LogEvent, LogImpressionConst.VirtualTour, RouteParamType.Friendly);
                    tourUrl.Parameters.Add(UrlConst.HUrl, CryptoHelper.HashUsingAlgo(listing.VirtualTourURL + ":" + LogImpressionConst.VirtualTour, "md5"), RouteParamType.Friendly);
                    if (listing.IsPlan)
                        tourUrl.Parameters.Add(UrlConst.PlanID, listing.PlanID.ToString(), RouteParamType.Friendly);
                    else
                    {
                        tourUrl.Parameters.Add(UrlConst.SpecID, listing.SpecID.ToString(), RouteParamType.Friendly);
                        tourUrl.Parameters.Add(UrlConst.PlanID, listing.PlanID.ToString(), RouteParamType.Friendly);
                    }
                    tourUrl.Parameters.Add(UrlConst.BuilderID, listing.BuilderID.ToString(), RouteParamType.Friendly);
                    tourUrl.Parameters.Add(UrlConst.PartnerID, listing.PartnerID.ToString(), RouteParamType.Friendly);
                    tourUrl.Parameters.Add(UrlConst.MarketID, listing.MarketID.ToString(), RouteParamType.Friendly);

                    lnkVirtualTour.NavigateUrl = ResolveUrl("~/" + tourUrl);
                    lnkVirtualTour.Attributes.Add("target", "_blank");
                    lnkVirtualTour.Attributes.Add("onclick", "Redirect(event,'" + ResolveUrl("~/" + tourUrl) + "',true);return false;");
                }
                else
                    lnkVirtualTour.Visible = false;
                
                if (XGlobals.Partner.BrandPartnerID == (int)PartnersConst.Pro)
                {
                    imgSpecialOffer.ImageUrl = "[resource:]/globalresourcesmvc/default/images/icons/icon_promos.png";
                    lnkBrochure.Text = @"Send Brochure";
                }

                if (!result.Value || (listing.PromoID == 0 && !listing.ConsumerPromo && !listing.AgentPromo))
                    lnkSpecialOffer.Visible = false;

                if (listing.ConsumerPromo)
                    lnkSpecialOffer.Visible = true;

                if (!string.IsNullOrEmpty(listing.ExternalBrochureURL))
                {
                    var planUrl = new NhsUrl(Pages.LogRedirect);

                    planUrl.Parameters.Add(UrlConst.ExternalURL, listing.ExternalBrochureURL, RouteParamType.QueryString);
                    planUrl.Parameters.Add(UrlConst.LogEvent, LogImpressionConst.PlanViewer, RouteParamType.Friendly);
                    planUrl.Parameters.Add(UrlConst.HUrl, CryptoHelper.HashUsingAlgo(listing.ExternalBrochureURL + ":" + LogImpressionConst.PlanViewer, "md5"), RouteParamType.Friendly);
                    if (listing.IsPlan)
                        planUrl.Parameters.Add(UrlConst.PlanID, listing.PlanID.ToString(), RouteParamType.Friendly);
                    else
                    {
                        planUrl.Parameters.Add(UrlConst.SpecID, listing.SpecID.ToString(), RouteParamType.Friendly);
                        planUrl.Parameters.Add(UrlConst.PlanID, listing.PlanID.ToString(), RouteParamType.Friendly);
                    }
                    planUrl.Parameters.Add(UrlConst.BuilderID, listing.BuilderID.ToString(), RouteParamType.Friendly);
                    planUrl.Parameters.Add(UrlConst.PartnerID, listing.PartnerID.ToString(), RouteParamType.Friendly);
                    planUrl.Parameters.Add(UrlConst.MarketID, listing.MarketID.ToString(), RouteParamType.Friendly);

                    lnkPlanViewer.NavigateUrl = ResolveUrl("~/" + planUrl.ToString());
                    lnkPlanViewer.Attributes.Add("target", "_blank");
                    lnkPlanViewer.Attributes.Add("onclick", "Redirect(event,'" + ResolveUrl("~/" + planUrl.ToString()) + "',true);return false;");
                }
                else
                    lnkPlanViewer.Visible = false;

                if (_searchParams.SortOrder != SortOrder.Random)
                {
                    var groupingBarPanel = (Panel)e.Item.FindControl("pnlGroupingBar");
                    var groupingBarTitle = (Literal)groupingBarPanel.FindControl("litGroupingBarTitle");

                    switch (_searchParams.SortOrder)
                    {
                        case SortOrder.Builder:
                            if (_previousGroupingValue != currentHome.BrandName)
                            {
                                groupingBarTitle.Text = currentHome.BrandName;
                                groupingBarPanel.Visible = true;
                                _previousGroupingValue = currentHome.BrandName;
                            }
                            break;

                        case SortOrder.Location:
                            if (_previousGroupingValue != (currentHome.City + ", " + currentHome.State))
                            {
                                groupingBarTitle.Text = currentHome.City + @", " + currentHome.State;
                                groupingBarPanel.Visible = true;
                                _previousGroupingValue = currentHome.City + ", " + currentHome.State;
                            }
                            break;

                        case SortOrder.Price:
                            string priceGroupValue;

                            if (currentHome.Price < 60000)
                                priceGroupValue = "Less than $60,000";
                            else
                            {
                                if (currentHome.Price >= 1000000)
                                    priceGroupValue = "From $1,000,000 and up";
                                else
                                {
                                    int priceGroup = (currentHome.Price / 20000) * 20000;
                                    priceGroupValue = "Priced from " + String.Format("{0:$###,###}", priceGroup);
                                }
                            }

                            if (_previousGroupingValue != priceGroupValue)
                            {
                                groupingBarTitle.Text = priceGroupValue;
                                groupingBarPanel.Visible = true;
                                _previousGroupingValue = priceGroupValue;

                            }
                            break;
                        case SortOrder.Size:
                            string sizeGroupValue;
                            switch (currentHome.NumBedrooms)
                            {
                                case 0:
                                    sizeGroupValue = "studio";
                                    break;
                                case 1:
                                    sizeGroupValue = "1 bedroom";
                                    break;
                                case 2:
                                    sizeGroupValue = "2 bedrooms";
                                    break;
                                case 3:
                                    sizeGroupValue = "3 bedrooms";
                                    break;
                                case 4:
                                    sizeGroupValue = "4 bedrooms";
                                    break;
                                case 5:
                                    sizeGroupValue = "5 bedrooms";
                                    break;
                                default:
                                    sizeGroupValue = "5+ bedrooms";
                                    break;
                            }
                            if (_previousGroupingValue != sizeGroupValue)
                            {
                                groupingBarTitle.Text = sizeGroupValue;
                                groupingBarPanel.Visible = true;
                                _previousGroupingValue = sizeGroupValue;

                            }
                            break;
                        case SortOrder.Status:
                            string statusGroupValue = string.Empty;
                            switch (currentHome.HomeStatus)
                            {
                                case (int)HomeStatusType.AvailableNow:
                                    statusGroupValue = "Available Now";
                                    break;
                                case (int)HomeStatusType.UnderConstruction:
                                    statusGroupValue = "Under Construction";
                                    break;
                                case (int)HomeStatusType.ModelHome:
                                    statusGroupValue = "Model";
                                    break;
                                default:
                                    statusGroupValue = "Ready to Build";
                                    break;
                            }
                            if (_previousGroupingValue != statusGroupValue)
                            {
                                groupingBarTitle.Text = statusGroupValue;
                                groupingBarPanel.Visible = true;
                                _previousGroupingValue = statusGroupValue;

                            }
                            break;
                        case SortOrder.SpecialOffer:
                            string specialOfferGroupValue = string.Empty;

                            specialOfferGroupValue = currentHome.PromoId != 0 || currentHome.IsHotHome
                                                         ? "Special Offer"
                                                         : "Other Homes";

                            if (_previousGroupingValue != specialOfferGroupValue)
                            {
                                groupingBarTitle.Text = specialOfferGroupValue;
                                groupingBarPanel.Visible = true;
                                _previousGroupingValue = specialOfferGroupValue;
                            }

                            break;

                    }
                }
                var homeDetailUrl = new NhsUrl(Pages.HomeDetail);

                var @params = new List<RouteParam>();
                @params.Add(new RouteParam(RouteParams.LeadType, LeadType.Home, RouteParamType.QueryString));
                @params.Add(new RouteParam(RouteParams.LeadAction, LeadAction.HotHome, RouteParamType.QueryString));
                @params.Add(new RouteParam(RouteParams.PlanList, currentHome.PlanId.ToString(), RouteParamType.QueryString));
                @params.Add(new RouteParam(RouteParams.PlanId, currentHome.PlanId.ToString(), RouteParamType.QueryString));
                @params.Add(new RouteParam(RouteParams.Builder, currentHome.BuilderId.ToString(), RouteParamType.QueryString));
                @params.Add(new RouteParam(RouteParams.Market, MarketId.ToString(), RouteParamType.QueryString));
                @params.Add(new RouteParam(RouteParams.CommunityList, currentHome.CommunityId.ToString(), RouteParamType.QueryString));
                @params.Add(new RouteParam(RouteParams.Community, currentHome.CommunityId.ToString(), RouteParamType.QueryString));
                @params.Add(new RouteParam(RouteParams.FromPage, Pages.HomeResults, RouteParamType.QueryString));
                @params.Add(new RouteParam(RouteParams.IsBilled, result.Value, RouteParamType.QueryString));
                // if the current home is a spec I add the id to the URL
                if (currentHome.SpecId != 0)
                {
                    @params.Add(new RouteParam(RouteParams.SpecId, currentHome.SpecId, RouteParamType.QueryString));
                    @params.Add(new RouteParam(RouteParams.SpecList, currentHome.SpecId, RouteParamType.QueryString));
                }

                @params.Add(new RouteParam(RouteParams.Width, Width, RouteParamType.QueryString));
                @params.Add(new RouteParam(RouteParams.Height, Height, RouteParamType.QueryString));
                // Hot Homes stuff
              
                if (listing.IsHotHome && (XGlobals.Partner.BrandPartnerID != Convert.ToInt32(PartnersConst.Pro)))
                {
                    divResultsRow.Attributes.Add("class", "nhsHomeResultsHotRow");
                    divResultsRow.Attributes.Add("onmouseover", "this.className='nhsHomeResultsHotRowOn';");
                    divResultsRow.Attributes.Add("onmouseout", "this.className='nhsHomeResultsHotRow';");
                    divHotHomeSummary.Visible = true;
                    litHotHomeTitle.Text = listing.HotHomeTitle;
                    litHotHomeDescription.Text = (listing.HotHomeTitle.ToLower() == listing.HotHomeDescription.ToLower())
                        ? string.Empty
                        : listing.HotHomeDescription;

                    lnkBrochure.CssClass = "btn btnHotHome";
                    lnkHotHomeInfo.NavigateUrl = @params.ToUrl(_leazdPage);
                    lnkHotHomeInfo.Attributes.Add("data-usedTitle", "false");
                }


                if (NhsRoute.BrandPartnerId != PartnersConst.Pro.ToType<int>())
                {
                    @params.Add(new RouteParam(RouteParams.KeepThis, "true", RouteParamType.QueryString));
                    @params.Add(new RouteParam(RouteParams.TB_iframe, "true", RouteParamType.QueryString));
                    lnkBrochure.NavigateUrl = @params.ToUrl(_leazdPage);
                    lnkBrochure.Attributes.Add("data-usedTitle", "false");
                }
                else
                {
                    if (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser)
                    {
                        @params.Add(new RouteParam(RouteParams.KeepThis, "true", RouteParamType.QueryString));
                        @params.Add(new RouteParam(RouteParams.TB_iframe, "true", RouteParamType.QueryString));
                        @params.Add(new RouteParam(RouteParams.Email, UserSession.UserProfile.Email));
                        @params.Add(new RouteParam(RouteParams.IsMvc, "false", RouteParamType.QueryString));
                        if (currentHome.PlanId > 0)
                            @params.Add(new RouteParam(RouteParams.PlanId, currentHome.PlanId.ToString()));

                        if (currentHome.SpecId > 0)
                            @params.Add(new RouteParam(RouteParams.SpecId, currentHome.SpecId.ToString()));
                        lnkBrochure.NavigateUrl = @params.ToUrl(Pages.SendCustomBrochure);
                        lnkBrochure.Attributes.Add("title", "Send Customized Brochure to Clients");

                    }
                    else
                    {
                        @params.Add(new RouteParam(RouteParams.Width, ModalWindowsConst.SignInModalWidth().ToString(), RouteParamType.QueryString));
                        @params.Add(new RouteParam(RouteParams.Height, ModalWindowsConst.SignInModalHeight.ToString(), RouteParamType.QueryString));
                        lnkBrochure.Attributes.Add("onclick", "$.googlepush('Account Events','SignIn','Open Form - Header Link')");
                        lnkBrochure.Attributes.Add("data-hidetitle", "true");
                        lnkBrochure.NavigateUrl = @params.ToUrl(Pages.LoginModal);
                    }
                }

                lnkBrochure.CssClass += XPartnerHelper.IsNonStandardPartner(XGlobals.Partner.PartnerId) ||
                                        XGlobals.Partner.IsRealtor
                    ? ""
                    : " thickbox";

                lnkHotHomeInfo.CssClass += XPartnerHelper.IsNonStandardPartner(XGlobals.Partner.PartnerId) ||
                                        XGlobals.Partner.IsRealtor
                    ? ""
                    : " thickbox";

                //Green
                ICommunity currentComm = XCommunityFactory.CreateCommunity(listing.CommunityID, listing.PartnerID, listing.BuilderID);
                var detailUrl = string.Empty;
 

                if (currentHome.SpecId != 0)
                {

                    if (XGlobals.Partner.PartnerId == Convert.ToInt32(PartnersConst.Move) && XGlobals.Partner.PartnerId == XGlobals.Partner.BrandPartnerID) // not a private label site
                    {
                        detailUrl = string.Format("/{0}/{1}/{2}-area/{3}-at-{4}-{5}-{6}-{7}",
                                                  Pages.SpecDetailMove,
                                                  currentHome.SpecId.ToString(),
                                                  currentComm.Market.MarketName.RemoveSpecialCharacters(),
                                                  currentHome.PlanName.RemoveSpecialCharacters(),
                                                  currentHome.Address1.RemoveSpecialCharacters(),
                                                  currentHome.City.RemoveSpecialCharacters(),
                                                  currentComm.Market.State,
                                                  currentHome.PostalCode.Trim());

                        if (!string.IsNullOrEmpty(NhsRoute.PartnerSiteUrl))
                            detailUrl = "/" + NhsRoute.PartnerSiteUrl + detailUrl;


                        detailUrl = Regex.Replace(detailUrl.ToLower().Replace(" ", "-"), @"\-+", "-");
                    }
                    else
                    {
                        homeDetailUrl.Parameters.Add(UrlConst.SpecID, currentHome.SpecId.ToString(), RouteParamType.Friendly);
                        detailUrl = ResolveUrl("~/" + homeDetailUrl.ToString());
                    }
                }
                else
                {

                    if (XGlobals.Partner.PartnerId == Convert.ToInt32(PartnersConst.Move) && XGlobals.Partner.PartnerId == XGlobals.Partner.BrandPartnerID) // not a private label site
                    {
                        detailUrl = string.Format(@"/{0}/{1}/{2}/{3}-area/{4}/{5}-at-{6}",
                                                  Pages.PlanDetailMove,
                                                  currentHome.PlanId.ToString(),
                                                  currentComm.Market.State,
                                                  currentComm.Market.MarketName.RemoveSpecialCharacters(),
                                                  currentHome.City.RemoveSpecialCharacters(),
                                                  currentHome.PlanName.RemoveSpecialCharacters(),
                                                  currentComm.Name.RemoveSpecialCharacters());
                    
                        if (!string.IsNullOrEmpty(NhsRoute.PartnerSiteUrl))
                            detailUrl = "/" + NhsRoute.PartnerSiteUrl + detailUrl;

                        detailUrl = Regex.Replace(detailUrl.ToLower().Replace(" ", "-"), @"\-+", "-");
                    }
                    else
                    {
                        homeDetailUrl.Parameters.Add(UrlConst.PlanID, currentHome.PlanId.ToString(), RouteParamType.Friendly);
                        detailUrl = ResolveUrl("~/" + homeDetailUrl.ToString());

                    }
                }

                //// Case 24420 - Forward Market ID - Dropping Market Id since the new pages do not require it
                //homeDetailUrl.Parameters.Add(UrlConst.MarketID, this.MarketId.ToString(), UrlParamType.Friendly);

                lnkHomeThumbnail.ToolTip = currentHome.PlanName;
                lnkHomeThumbnail.NavigateUrl = detailUrl;
                lnkHomeThumbnail.Attributes.Add("onclick", "stopBubble(event);");
                lnkHomeName.Text = currentHome.PlanName;
                lnkHomeName.NavigateUrl = detailUrl;
                lnkHomeName.Attributes.Add("onclick", "stopBubble(event);");
                divResultsRow.Attributes.Add("onclick", "Redirect(event,'" + detailUrl + "');");

                
                if (currentComm.IsGreen)
                {
                    communityGreenImage.ImageUrl = "[resource:]/globalresourcesmvc/default/images/icons/icon_green.png";
                    communityGreenImage.AlternateText = @"Green Features";
                    communityGreenImage.Visible = true;
                }

                //spec is a model home then dont diplay price
                if (currentHome.HomeStatus == (int)HomeStatusType.ModelHome)
                {
                    litHomePrice.Text = @"Model";
                }
                else
                {
                    litHomePrice.Text = @"from " + String.Format("{0:$###,###}", currentHome.Price);
                }

                //check image count - case 35280 for NHS & Move partners
                if ((currentHome.ImageCount > 1) &&
                    ((XGlobals.Partner.BrandPartnerID == Convert.ToInt32(PartnersConst.NewHomeSource)) ||
                    (XGlobals.Partner.BrandPartnerID == Convert.ToInt32(PartnersConst.Move))))
                {
                    litHomeImageCount.Text = "<div class=\"nhsResultsImgCount\"><span>" + currentHome.ImageCount + @"</span></div>";
                }
                else
                {
                    litHomeImageCount.Visible = false;
                } 
                litBrandName.Text = currentHome.BrandName;
                litHomeBathrooms.Text = XGlobals.ComputeBathrooms(currentHome.NumBathrooms, currentHome.NumHalfBathrooms);
                litHomeBedrooms.Text = XGlobals.FormatBedrooms(currentHome.NumBedrooms, true);
                litHomeCity.Text = currentHome.City;
                litHomeCommunityName.Text = currentHome.CommunityName;
                litHomeGarages.Text = string.Format("{0:0.##}", currentHome.NumGarages);
                litHomeSquareFeet.Text = currentHome.SquareFeet.ToString();
                litHomeState.Text = currentHome.State;
                litHomeZip.Text = currentHome.PostalCode;

                // check for image path - jbecker
                if (currentHome.PlanImageThumbnail != string.Empty && currentHome.PlanImageThumbnail != "N")
                {
                    imgHomeThumbnail.ImageUrl = Configuration.IRSDomain + currentHome.PlanImageThumbnail;
                    imgHomeThumbnail.AlternateText = currentHome.PlanName + @" image";
                }

                if (currentHome.MarketingDescription != string.Empty && !currentHome.IsHotHome)
                {
                    litHomeMarketingDescription.Text = "<p class=\"nhsHomeResMktDes\">" + currentHome.MarketingDescription + "</p>";
                    litHomeMarketingDescription.Visible = true;

                }
              
                switch (currentHome.HomeStatus)
                {
                    case (int)HomeStatusType.AvailableNow:
                        imgHomeStatusImage.ImageUrl = "[resource:]/globalresources/default/images/icons/status_available.gif";
                        imgHomeStatusImage.AlternateText = @"Available Now";
                        break;
                    case (int)HomeStatusType.UnderConstruction:
                        imgHomeStatusImage.ImageUrl = "[resource:]/globalresources/default/images/icons/status_under_construction.gif";
                        imgHomeStatusImage.AlternateText = @"Under Construction";
                        break;
                    case (int)HomeStatusType.ModelHome:
                        imgHomeStatusImage.ImageUrl = "[resource:]/globalresources/default/images/icons/status_model_home.gif";
                        imgHomeStatusImage.AlternateText = @"Model Home";
                        break;
                    default:
                        imgHomeStatusImage.ImageUrl = "[resource:]/globalresources/default/images/icons/status_ready.gif";
                        imgHomeStatusImage.AlternateText = @"Ready to Build";
                        break;
                }
                #endregion
            }
        }

        protected void btnFacet_Click(object sender, CommandEventArgs e)
        {
            string commandName = e.CommandName;
            string commandValue = e.CommandArgument.ToString();
            bool clearFacet = commandValue == string.Empty;
            NhsUrl currentUrl = NhsUrlHelper.GetFriendlyUrl();
            
            switch (commandName.ToLower())
            {
                case "zip":
                    _searchParams.PostalCodeFilter = clearFacet ? string.Empty : commandValue;
                    break;
                case "city":
                    _searchParams.City = _searchParams.CityNameFilter = clearFacet ? string.Empty : commandValue;
                    break;
                case "price":
                    if (clearFacet)
                    {
                        _searchParams.PriceLow = 0;
                        _searchParams.PriceHigh = 0;
                        if (currentUrl.Parameters. Contains(UrlConst.PriceLow))
                            currentUrl.Parameters.Remove(UrlConst.PriceLow);
                        if (currentUrl.Parameters.Contains(UrlConst.PriceHigh))
                            currentUrl.Parameters.Remove(UrlConst.PriceHigh);                        
                    }
                    else
                    {
                        string[] priceRange = commandValue.Split(':');
                        int priceLow = int.Parse(priceRange[0]);
                        int priceHigh = int.Parse(priceRange[1]);

                        _searchParams.PriceLow = priceLow;
                        _searchParams.PriceHigh = priceHigh;

                        if (currentUrl.Parameters.Contains(UrlConst.PriceLow))
                        {
                            currentUrl.Parameters[UrlConst.PriceLow].Value = priceLow.ToString();
                            currentUrl.Parameters[UrlConst.PriceLow].ParamType = RouteParamType.QueryString;
                        }
                        else
                        {
                            currentUrl.Parameters.Add(new NhsUrlParam(UrlConst.PriceLow, priceLow.ToString(), RouteParamType.QueryString));
                        }

                        if (currentUrl.Parameters.Contains(UrlConst.PriceHigh))
                        {
                            currentUrl.Parameters[UrlConst.PriceHigh].Value = priceHigh.ToString();
                            currentUrl.Parameters[UrlConst.PriceHigh].ParamType = RouteParamType.QueryString;
                        }
                        else
                        {
                            currentUrl.Parameters.Add(new NhsUrlParam(UrlConst.PriceHigh, priceHigh.ToString(), RouteParamType.QueryString));
                        }
                    }
                    break;
                case "homesize":
                    _searchParams.NumOfBeds = clearFacet ? 0 : int.Parse(commandValue);

                    if (_searchParams.NumOfBeds == 0)
                    {
                        if (currentUrl.Parameters.Contains(UrlConst.NumOfBeds))
                            currentUrl.Parameters.Remove(UrlConst.NumOfBeds);
                    }
                    else
                    {
                        if (currentUrl.Parameters.Contains(UrlConst.NumOfBeds))
                        {
                            currentUrl.Parameters[UrlConst.NumOfBeds].Value = _searchParams.NumOfBeds.ToString();
                            currentUrl.Parameters[UrlConst.NumOfBeds].ParamType = RouteParamType.QueryString;
                        }
                        else
                        {
                            currentUrl.Parameters.Add(new NhsUrlParam(UrlConst.NumOfBeds, _searchParams.NumOfBeds.ToString(), RouteParamType.QueryString));
                        }
                    }
                    break;
                case "hometype":
                    _searchParams.HomeType = clearFacet ? string.Empty : commandValue;

                    if (_searchParams.HomeType.Equals(""))
                    {
                        if (currentUrl.Parameters.Contains(UrlConst.HomeType))
                            currentUrl.Parameters.Remove(UrlConst.HomeType);
                    }
                    else
                    {
                        if (currentUrl.Parameters.Contains(UrlConst.HomeType))
                        {
                            currentUrl.Parameters[UrlConst.HomeType].Value = _searchParams.HomeType.ToString();
                            currentUrl.Parameters[UrlConst.HomeType].ParamType = RouteParamType.QueryString;
                        }
                        else
                        {
                            currentUrl.Parameters.Add(new NhsUrlParam(UrlConst.HomeType, _searchParams.HomeType.ToString(), RouteParamType.QueryString));
                        }
                    }
                    break;
                case "communityamenity":
                    if (clearFacet)
                    {
                        _searchParams.Pool = false;
                        _searchParams.GolfCourse = false;
                        _searchParams.GreenNatureAreas = false;
                        _searchParams.Parks = false;
                        _searchParams.Views = false;
                        _searchParams.Gated = false;
                        _searchParams.Waterfront = false;
                        _searchParams.SportsFacilities = false;
                        _searchParams.Adult = false;
                        _searchParams.GreenProgram = false;
                    }
                    else
                    {
                        switch (commandValue.ToLower())
                        {
                            case "pool":
                                _searchParams.Pool = true;
                                break;
                            case "golf":
                                _searchParams.GolfCourse = true;
                                break;
                            case "gated":
                                _searchParams.Gated = true;
                                break;
                            case "green":
                                _searchParams.GreenProgram = true;
                                break;
                        }
                    }
                    break;
                case "school":
                    _searchParams.SchoolDistrictId = clearFacet ? 0 : int.Parse(commandValue);
                    break;
                case "expandtomarket":
                    _searchParams.PostalCodeFilter = string.Empty;
                    _searchParams.CityNameFilter = string.Empty;
                    _searchParams.City = string.Empty;
                    _searchParams.ComingSoon = false;
                    _searchParams.BrandId = 0;
                    break;
                case "specialoffer":
                    _searchParams.SpecialOfferListing = clearFacet ? 0 : 1;
                    break;
                case "comingsoon":
                    int marketId = _searchParams.MarketId;
                    _searchParams.Clear();
                    _searchParams.MarketId = marketId;
                    _searchParams.ComingSoon = true;
                    break;
            }

            UserSession.PropertySearchParameters = _searchParams;
            GetSearchResults(_pagingToolsTop.PageNumber);
            BindControls();
        }

        protected void ddlSortBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedSort = ddlSortBy.SelectedValue;

            switch (selectedSort.ToLower())
            {
                case "":
                    _searchParams.SortOrder = SortOrder.Random;
                    break;
                case "location":
                    _searchParams.SortOrder = SortOrder.Location;
                    break;
                case "price":
                    _searchParams.SortOrder = SortOrder.Price;
                    break;
                case "builder":
                    _searchParams.SortOrder = SortOrder.Builder;
                    break;
                case "status":
                    _searchParams.SortOrder = SortOrder.Status;
                    break;
                case "size":
                    _searchParams.SortOrder = SortOrder.Size;
                    break;
                case "name":
                    _searchParams.SortOrder = SortOrder.Name;
                    break;
                case "specialoffer":
                    _searchParams.SortOrder = SortOrder.SpecialOffer;
                    break;
                case "quickmovein":
                    _searchParams.SortOrder = SortOrder.QuickMoveIn; // this one should behave similarly to the Status one
                    break;
            }

            GetSearchResults(_pagingToolsTop.PageNumber);
            BindControls();
        }

        #endregion

        #region "Initialize Methods"
        private void InitializePostback()
        {
            // Get results view from user session. 
            _resultsView = (HomeResultsView)UserSession.GetItem(SessionConst.HomeResultsView);

            // Get property search values from user session. 
            _searchParams = UserSession.PropertySearchParameters;

            // Redirect to home page if search parameters 
            // are invalid. 
            if (!_searchParams.Validate())
            {
                Response.Redirect(new NhsUrl(Pages.Home).ToString());
            }

            // Get non-property search values from viewstate. 
            _searchType = ViewState[UrlConst.SearchType].ToString();

        }

        private void InitializeNonPostback(bool preserveSession)
        {
            string tmpCityNameFilter = string.Empty;

            // Get the property search values from session. 
            _searchParams = UserSession.PropertySearchParameters;

            //Preserve city filter if coming from MLS Home Results Tab
            if (UserSession.PreviousPage == Pages.MlsHomeResults)
                tmpCityNameFilter = UserSession.PropertySearchParameters.CityNameFilter;

            // Check if we need to clear any existing search parameters
            // stored in the session. 
            if (!preserveSession)
                _searchParams.Clear();

            // Url parameters always take precedence. 
            _searchParams.SetFromUrlParam(base.UrlParams);
            ddlSortBy.SelectedValue = _searchParams.SortOrder.ToString().ToLower();

            //Retrieve city filter back
            if (UserSession.PreviousPage == Pages.MlsHomeResults)
            {
                _searchParams.CityNameFilter = tmpCityNameFilter;
                UserSession.PreviousPage = string.Empty;
            }

            // Redirect to home page if search parameters 
            // are invalid. 
            if (!_searchParams.Validate())
            {
                Response.Redirect(new NhsUrl(Pages.Home).ToString());
            }

            _searchParams.FacetPriceLow = _searchParams.PriceLow;
            _searchParams.FacetPriceHigh = _searchParams.PriceHigh;

            // Get non-property search parameters from url. 
            _pagingToolsTop.PageNumber = UrlParams.GetInt(UrlConst.PageNumber) == 0 ? 1 : UrlParams.GetInt(UrlConst.PageNumber);
            _pagingToolsBottom.PageNumber = UrlParams.GetInt(UrlConst.PageNumber) == 0 ? 1 : UrlParams.GetInt(UrlConst.PageNumber);
            _searchType = UrlParams.GetString(UrlConst.SearchType);

            // Set viewstate values for use on postback. 
            ViewState[UrlConst.PageNumber] = _pageNumber;
            ViewState[UrlConst.SearchType] = _searchType;

            // Get the search results.
            GetSearchResults(_pagingToolsTop.PageNumber);
        }
        #endregion

        #region "Control Binding"
        private void BindControls()
        {
            // Bind advanced search summary text.
            if (_searchParams.GetAdvSearchSummary() != string.Empty)
            {
                litCommunityResultsSummary.Text = "<p class=\"nhsCommResSummary\">You selected: " + _searchParams.GetAdvSearchSummary() + "</p>";
                litCommunityResultsSummary.Visible = true;
            }
            else
            {
                litCommunityResultsSummary.Visible = false;
            }

            // Bind price facets.
            rptPriceFact.DataSource = _resultsView.PriceFacet.FacetOptions;
            rptPriceFact.DataBind();
            rptHomeSizeFacet.DataSource = _resultsView.HomeSizeFacet.FacetOptions;
            rptHomeSizeFacet.DataBind();
            rptHomeTypeFacet.DataSource = _resultsView.HomeTypeFacet.FacetOptions;
            rptHomeTypeFacet.DataBind();
            if (XGlobals.Partner.BrandPartnerID != Convert.ToInt32(PartnersConst.Pro))
            {
                hotDealsFacet.Visible = true;
                hotDealsFacetItems.Visible = true;
                rptSpecialOfferFacet.DataSource = _resultsView.SpecialOfferFacet.FacetOptions;
                rptSpecialOfferFacet.DataBind();
            }
            rptCommunityAmenityFacet.DataSource = _resultsView.CommunityAmenityFacet.FacetOptions;
            rptCommunityAmenityFacet.DataBind();

            BindSchoolDistrictFacet(_resultsView.SchoolDistrictFacet.FacetOptions);
            BindLocationScope();
            BindCrossMarketingLinks();

            // Determine location of "back to search" button. 
            var url = NhsUrlHelper.GetFriendlyUrl(this.Context);
            lnkBackToSearch.NavigateUrl = _searchType == SearchTypeSource.AdvancedSearchCommunityResults
                ? url.SiteRoot + "advancedsearch"
                : url.SiteRoot + "home";
            
            litComingSoonAreaName.Text = _resultsView.MarketName;

            if (_resultsView.TotalCommunities > 0)
            {
                BindResultsList();
            }

            hfMarketId.Value = _resultsView.MarketId.ToString();
            hfMarketName.Value = _resultsView.MarketName + ", " + _resultsView.State;
            hfMatchingResults.Value = _resultsView.TotalHomes.ToString();

            var cookie = Request.Cookies[CookieConst.GoogleUtmaCookie];
            hfSid.Value = cookie == null ? string.Empty : cookie.Value.Split('.')[4];
            hfUid.Value = cookie == null ? string.Empty : cookie.Value.Split('.')[1];

            litComingSoonAreaName.Text = _resultsView.MarketName;

            // Register Meta Tags for Home Results page
            // NOTE: CHECK if the required parameter for tags Comm Name is really requirement
            MetaRegistrar metaRegistrar = new MetaRegistrar(this.Page);
            
            metaRegistrar.NHSAddMetaTagsForSEOHomeResults(_resultsView.CityName,
                                                          _resultsView.MarketName,
                                                          string.Empty,
                                                          XBuilderHelper.GetBuilderName(_resultsView.BuilderId),
                                                           _resultsView.State);
            if (UserSession.SearchType == SearchTypeSource.AdvancedSearchHomeResults)
            {
                lnkBackToSearch.NavigateUrl = url.SiteRoot + "advancedsearch";
            }
        }

        private void BindSchoolDistrictFacet(List<FacetOption> schoolDistrictFacets)
        {
            /* if school facet count is > 5 then split
               the facet to fourhomes + more homes + any school*/
            if (schoolDistrictFacets.Count > 5)
            {
                List<FacetOption> firstFourSchools = new List<FacetOption>();
                List<FacetOption> moreSchools = new List<FacetOption>();

                for (int i = 0; i < schoolDistrictFacets.Count; i++)
                {
                    FacetOption currentFacet = schoolDistrictFacets[i];

                    if (firstFourSchools.Count < 4 && currentFacet.Value != string.Empty)
                    {
                        firstFourSchools.Add(currentFacet);
                    }
                    else
                    {
                        if (currentFacet.Value == "")
                        {
                            if (currentFacet.Selected)
                            {
                                Label lblAnySchool = new Label();
                                lblAnySchool.Text = currentFacet.Text;
                                lblAnySchool.CssClass = "nhsFacetOn";
                                _anySchoolDistrictFacet.Controls.Add(lblAnySchool);
                            }
                            else
                            {
                                LinkButton btnAnySchool = new LinkButton();
                                btnAnySchool.Text = currentFacet.Text;
                                btnAnySchool.CommandName = "School";
                                btnAnySchool.CommandArgument = string.Empty;
                                _anySchoolDistrictFacet.Controls.Add(btnAnySchool);
                            }
                        }
                        else
                        {
                            moreSchools.Add(currentFacet);
                        }
                    }
                }

                rptSchoolDistrictFacet.DataSource = firstFourSchools;
                rptSchoolDistrictFacet.DataBind();

                rptMoreSchools.DataSource = moreSchools;
                rptMoreSchools.DataBind();
                rptMoreSchools.Visible = true;
            }
            /*else just databind, but ensure "any school" stays at
            the bottom */
            else
            {
                if (schoolDistrictFacets.Count > 0)
                {
                    FacetOption anySchool = schoolDistrictFacets[0]; //by default assign to [0]
                    foreach (FacetOption fo in schoolDistrictFacets)
                    {
                        if (fo.Text == "Any school district")
                        {
                            anySchool = fo;
                            break;
                        }
                    }
                    schoolDistrictFacets.Remove(anySchool);
                    schoolDistrictFacets.Insert(schoolDistrictFacets.Count, anySchool);
                }
                rptSchoolDistrictFacet.DataSource = schoolDistrictFacets;
                rptSchoolDistrictFacet.DataBind();
                rptMoreSchools.Visible = false;
            }
        }

        private void BindCityFacet(List<FacetOption> cityFacets)
        {
            var allAreas = cityFacets.First(f => f.Text == "All areas");

            if (allAreas != null && allAreas.Count == 0)
            {
                var allCount = cityFacets.Sum(s => s.Count);
                allAreas.Count = allCount;
            }

            if (cityFacets.Count > 5)
            {
                List<FacetOption> firstFourCities = new List<FacetOption>();
                List<FacetOption> moreCities = new List<FacetOption>();

                for (int i = 0; i < cityFacets.Count; i++)
                {
                    FacetOption currentFacet = cityFacets[i];

                    if (firstFourCities.Count < 4)
                    {
                        firstFourCities.Add(currentFacet);
                    }
                    else
                    {
                        moreCities.Add(currentFacet);
                    }
                }

                rptMoreCityFacets.Visible = true;

                rptCityFacets.DataSource = firstFourCities;
                rptCityFacets.DataBind();

                rptMoreCityFacets.DataSource = moreCities;
                rptMoreCityFacets.DataBind();
            }
            else
            {
                rptMoreCityFacets.Visible = false;

                rptCityFacets.DataSource = cityFacets;
                rptCityFacets.DataBind();
            }
        }

        private void BindLocationScope()
        {
            // Map Search. 
            if (_searchParams.MinLatitude != 0
                || _searchParams.MinLongitude != 0
                || _searchParams.MaxLatitude != 0
                || _searchParams.MaxLongitude != 0)
            {
                litLocationScopeHeading.Text = "Your selected map area";
                pnlExpandLocationTo.Visible = false;
                pnlNarrowToCity.Visible = false;
                btnExpandLocationTo.Text = "greater " + _resultsView.MarketName + " area.";
            }
            else
            {
                // Builder search. 
                if (_searchParams.BrandId != 0)
                {
                    litLocationScopeHeading.Text = _resultsView.BrandName;
                    pnlExpandLocationTo.Visible = true;
                    pnlNarrowToCity.Visible = false;
                    btnExpandLocationTo.Text = "greater " + _resultsView.MarketName + " area.";
                }
                else
                {
                    // Coming soon search. 
                    if (_searchParams.ComingSoon)
                    {
                        litLocationScopeHeading.Text = "Coming soon & Grand opening";
                        pnlExpandLocationTo.Visible = true;
                        pnlNarrowToCity.Visible = false;
                        btnExpandLocationTo.Text = "greater " + _resultsView.MarketName + " area.";
                    }
                    else
                    {
                        // City search. 
                        if (_searchParams.CityNameFilter != string.Empty)
                        {
                            litLocationScopeHeading.Text = _resultsView.CityName;
                            pnlExpandLocationTo.Visible = true;
                            pnlNarrowToCity.Visible = false;
                            btnExpandLocationTo.Text = "greater " + _resultsView.MarketName + " area.";
                        }
                        else
                        {
                            // Zip search. 
                            if (_searchParams.PostalCodeFilter != string.Empty)
                            {
                                litLocationScopeHeading.Text = "Zip code " + _searchParams.PostalCodeFilter;
                                pnlExpandLocationTo.Visible = true;
                                pnlNarrowToCity.Visible = false;
                                btnExpandLocationTo.Text = "greater " + _resultsView.MarketName + " area.";
                            }
                            else
                            {
                                // Default market search. 
                                litLocationScopeHeading.Text = _resultsView.MarketName + " area";

                                // Bind the city facets on the narrow to panel. 
                                BindCityFacet(_resultsView.CityFacet.FacetOptions);

                                // Bind zip code facets if there are any. 
                                if (_resultsView.PostalCodeFacet.FacetOptions.Count > 0)
                                {
                                    rptZipFacets.DataSource = _resultsView.PostalCodeFacet.FacetOptions;
                                    rptZipFacets.DataBind();
                                    rptZipFacets.Visible = true;
                                }

                                // Show narrow to panel & hide expand to panel. 
                                pnlNarrowToCity.Visible = true;
                                pnlExpandLocationTo.Visible = false;
                            }
                        }
                    }
                }

            }

            litTotalCommunities.Text = _resultsView.TotalCommunities.ToString();
            litTotalHomes.Text = _resultsView.TotalHomes.ToString();
        }

        private void BindCrossMarketingLinks()
        {
            // Clear list
            lstCrossMarketingLinks.Items.Clear();

            if (XGlobals.Partner.BrandPartnerID == Convert.ToInt32(PartnersConst.Pro))//Hide links on Pro 
            {
                pnlMoreSearches.Visible = false;
                return;
            }

            // Casas Nuevas Aqui?
            if (_resultsView.MarketContainsSpanish)
            {
                // Add link
                ListItem casasItem = new ListItem();
                casasItem.Text = "<a href=\"" + "http://www.casasnuevasaqui.com/search/community_results.aspx?srchtype=basic&mid=" + _resultsView.MarketId + "\" target=\"_blank\">Casas nuevas en Espa�ol</a>";
                lstCrossMarketingLinks.Items.Add(casasItem);
            }

            // Retirement Communities?
            if (_resultsView.MarketContainsRetirement)
            {
                // Add link
                ListItem nrcItem = new ListItem();
                nrcItem.Text = "<a href=\"" + "http://www.newretirementcommunities.com/search/community_results.aspx?st=" + _resultsView.State + "\" target=\"_blank\">Retirement communities</a>";
                lstCrossMarketingLinks.Items.Add(nrcItem);
            }

            // if Multifamily exists in current market, add link to UrbanContdoLiving
            if (_resultsView.HomeTypeFacet.FacetOptions[2].Count > 0)
            {
                ListItem marketItem = new ListItem();
                marketItem.Text = "<a href=\"http://www.urbancondoliving.com/market/" + _resultsView.MarketId + "\">New condominium homes</a>";
                lstCrossMarketingLinks.Items.Add(marketItem);
            }
        }

        private void BindResultsList()
        {
            // Results view may contain more than 1 page of results.  
            List<HomeResult> pageOfResults = new List<HomeResult>();
            IList page = _pagingToolsTop.GetDataPage(_resultsView.HomeResults, _resultsView.StartRecord, _resultsView.EndRecord);

            // Check to see if the requested page is already in results. 
            if (page == null)
            {
                // Results do not contain the page we are looking for. 
                // Request the page from the datasource. 
                GetSearchResults(_pagingToolsTop.PageNumber);
                page = _pagingToolsTop.GetDataPage(_resultsView.HomeResults, _resultsView.StartRecord, _resultsView.EndRecord);

                if (page == null)
                {
                    Response.Redirect("~/home");
                }
            }
            else
            {
                if(commsStatus == null)
                    GetSearchResults(_pagingToolsTop.PageNumber);
            }

            ImpressionLogger impressionLog = new ImpressionLogger();

            // Add records to the page of results list.
            foreach (object x in page)
            {
                HomeResult homeResult = (HomeResult)x;
                pageOfResults.Add(homeResult);

                if (homeResult.SpecId != 0)
                {
                    impressionLog.AddSpec(homeResult.SpecId);
                }
                else
                {
                    impressionLog.AddPlan(homeResult.PlanId);
                }

                AdController.AddBuilderParameter(homeResult.BuilderId);
                AdController.AddCityParameter(homeResult.City);
                AdController.AddZipParameter(homeResult.PostalCode);
            }

            // Add parameters to ad controls. 
            AdController.AddPriceParameter(_searchParams.PriceLow, _searchParams.PriceHigh);
            AdController.AddStateParameter(_resultsView.State);
            AdController.AddMarketParameter(_searchParams.MarketId);

            // Additional impression log info. 
            impressionLog.Refer = UserSession.Refer;
            impressionLog.PartnerId = Configuration.PartnerId.ToString();
            impressionLog.MarketId = _searchParams.MarketId;
            impressionLog.LogView(LogImpressionConst.HomeResults, this.Page);

            // Bind the page of results to the repeater. 
            rptHomeResults.DataSource = page;
            rptHomeResults.DataBind();

        }

        #endregion

        #region "Search Results & Paging"
        private void GetSearchResults(int pageNumber)
        {
            int startPage = pageNumber - 1;
            int endPage = pageNumber + 1;

            if (startPage < 1)
            {
                startPage = 1;
                endPage = 3;
            }

            // Add partner id, start & end records to search parameters. 
            _searchParams.PartnerId = Configuration.PartnerId;
            _searchParams.StartRecord = _pagingToolsTop.GetPageStart(startPage) - 1;
            _searchParams.EndRecord = _pagingToolsTop.GetPageEnd(endPage) - 1;
            //_searchParams.CityNameFilter = UserSession.PropertySearchParameters.CityNameFilter;

            // Get the results from the Search provider.
            //SearchHelper searchHelper = new SearchHelper();
            //_resultsView = searchHelper.HomeSearch(_searchParams);

            var searchService = new SearchService();
            _resultsView = searchService.HomeSearch(_searchParams);
             
            var builderService = ObjectFactory.GetInstance<IBuilderService>(); 
            var commsTable = builderService.GetMarketBcs(NhsRoute.PartnerId, MarketId).AsEnumerable();
            var partnerService = new PartnerService(new WebCacheService(), null, null, null);
            var partnerMaskIndex = partnerService.GetPartnerMaskIndex(NhsRoute.PartnerId);

            if (NhsRoute.IsBrandPartnerNhsPro)
            {
                commsStatus = commsTable.ToDictionary(r => r["community_id"].ToType<int>(), v =>
                                                                                                 v["billed"].ToType<string>() == "Y" &&
                                                                                                 v["partner_mask"].ToType<string>().Substring(partnerMaskIndex - 1, 1) != "F");
            }
            else
            {
                commsStatus= new Dictionary<int, bool>();
                var basicCommunities = commsTable.Where(r => r["partner_mask"].ToType<string>().Substring(partnerMaskIndex - 1, 1) == "F").Select(r => r["community_id"].ToType<int>());
                
                _resultsView.HomeResults =
                    _resultsView.HomeResults.Where(p => !basicCommunities.Contains(p.CommunityId)).ToList();
            }

            nhsGoogleAnalytics.Text = String.Format(@"<script type=""text/javascript"">$(document).ready(function(){{ _gaq.push(['_setCustomVar', 1,'Market', '{0}, {1}:{2}',3]);}})</script>", _resultsView.MarketName, _resultsView.State, _resultsView.MarketId);
            if (_resultsView.TotalHomes <= 0)
            {
                NhsUrl nomatchPage = new NhsUrl(Pages.NoMatchHandler);
                nomatchPage.Parameters.Add(UrlConst.MarketID, _searchParams.MarketId.ToString(), RouteParamType.Friendly);
                nomatchPage.Parameters.Add(UrlConst.PriceLow, _searchParams.PriceLow.ToString(), RouteParamType.Friendly);
                nomatchPage.Parameters.Add(UrlConst.PriceHigh, _searchParams.PriceHigh.ToString(), RouteParamType.Friendly);
                nomatchPage.Redirect();
            }

            _pagingToolsTop.TotalResults = _resultsView.TotalHomes;
            _pagingToolsBottom.TotalResults = _resultsView.TotalHomes;

            UserSession.SetItem(SessionConst.HomeResultsView, _resultsView);
        }
        #endregion
    }
}
