<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="home_results.ascx.cs" Inherits="Nhs.Web.Controls.Default.PropertySearch.home_results" %>
<%@ Register Src="~/Controls/Default/Common/paging_tools.ascx" TagName="pagingtools" TagPrefix="nhs" %>  
<script type="text/javascript">
    function toggleDisplay(linkId,popId) {
        var pop = document.getElementById(popId);
        var link = document.getElementById(linkId);

        if(pop.className == 'nhsPopOff') {
	        pop.className = 'nhsPopOn';
	        link.className = 'nhsFacetOn';
        } else {
	        pop.className = 'nhsPopOff';
	        link.className = 'nhsFacetOff';	        
        }
    }
    
    function Redirect(e, url)
    {        
	    if (!e) var e = window.event;
	    e.cancelBubble = true;
	    if (e.stopPropagation) e.stopPropagation();
	    if (url) window.location.href=url;
    }
</script>

<asp:HiddenField ID="hfMarketId" runat="server" Value=""/>
<asp:HiddenField ID="hfMarketName" runat="server" Value=""/>
<asp:HiddenField ID="hfMatchingResults" runat="server" Value=""/>
<asp:HiddenField ID="hfUid" runat="server" Value=""/>
<asp:HiddenField ID="hfSid" runat="server" Value=""/>

<!--GTM Data Layer-->
<script type='text/javascript'>
    dataLayer = [{
        'pageType': 'Home_Results',
        'partnerName': 'NHS Pro',
        'brandPartnerName': 'NHS Pro',
        'partnerId': '88',
        'adaptiveTemplate': 'D',
        'marketId': $("input[id*='hfMarketId']").val(),
        'marketName': $("input[id*='hfMarketName']").val(),
        'matchingResults': $("input[id*='hfMatchingResults']").val(),
        'uid': $("input[id*='hfUid']").val(),
        'sid': $("input[id*='hfSid']").val()
    }];
</script>
             
    <!-- Google Tag Manager -->
<noscript>
    <iframe src="//www.googletagmanager.com/ns.html?id=GTM-NCCM8F" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<script type='text/javascript'>    (function (w, d, s, l, i) { w[l] = w[l] || []; w[l].push({ 'gtm.start': new Date().getTime(), event: 'gtm.js' }); var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src = '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f); })(window, document, 'script', 'dataLayer', 'GTM-NCCM8F');</script>
<!-- Google Tag Manager -->

<div class="nhsContent" id="nhsHomeResults">

    <div id="nhsCommResConsole">
        <p class="nhsReader"><a href="#nhsSkipFacets">Skip to home listings</a></p>
                
        <%-- ***** LOCATION SCOPE BAR BEGIN *****--%>
        <asp:Literal id="litCommunityResultsSummary" runat="server" Visible="false"></asp:Literal>     
        <div class="nhsHomeResResultsBar"><div class="nhsCommResResultsBarR">
            <h2><asp:Literal id="litLocationScopeHeading" runat="server" Text="" /></h2> 
            <div class="nhsCommResMatches"><asp:Literal id="litTotalHomes" runat="server" text="0"/> matches within <asp:Literal id="litTotalCommunities" runat="server" Text="0" /> new home communities <asp:HyperLink id="lnkBackToSearch" runat="server" Text="Back to search" /></div>
            
            <%--***** NARROW TO CITY/ZIP PANEL ***** --%>
            <asp:Panel ID="pnlNarrowToCity" CssClass="nhsCommResNarrowList" runat="server">
                 <%-- Repeater for zip codes. --%>
                    <asp:Repeater ID="rptZipFacets" runat="server" Visible="true"  EnableViewState="true">
                        <HeaderTemplate>
                            Narrow to zip:
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:LinkButton id="btnZipFacet" rel="nofollow" runat="server" OnCommand="btnFacet_Click" text='<%#DataBinder.Eval(Container.DataItem, "Text")%>' CommandName="Zip" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "Value")%>' Font-Bold='<%#DataBinder.Eval(Container.DataItem, "Selected")%>'/>
                            (<%#DataBinder.Eval(Container.DataItem, "Count")%>) 
                        </ItemTemplate>
                    </asp:Repeater>
                                                             
                    <%-- Repeater for first x cities. --%>
                    <asp:Repeater ID="rptCityFacets" runat="server"  EnableViewState="true">
                        <HeaderTemplate>
                            Narrow to city:    
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:LinkButton id="btnCityFacet" rel="nofollow" runat="server" OnCommand="btnFacet_Click" text='<%#DataBinder.Eval(Container.DataItem, "Text")%>' CommandName="City" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "Value")%>' Font-Bold='<%#DataBinder.Eval(Container.DataItem, "Selected")%>'/>
                            (<%#DataBinder.Eval(Container.DataItem, "Count")%>)<asp:Literal ID="litComma" runat="server" Text=", " /> 
                        </ItemTemplate>
                    </asp:Repeater>
                    
                    <%-- Repeater for "more cities" --%>
                    <asp:Repeater ID="rptMoreCityFacets" runat="server"  EnableViewState="true">
                        <HeaderTemplate>
                          <div id="nhsNarrowMoreDiv"><a id="nhsNarrowMoreLink" rel="nofollow" onclick="toggleDisplay('nhsNarrowMoreLink','nhsNarrowMorePop'); return false;" href="#">more cities</a>
                            <div id="nhsNarrowMorePop" class="nhsPopOff">
                                <ul>
                        </HeaderTemplate>
                        <ItemTemplate>
                                <li>
                                    <asp:LinkButton id="btnMoreCityFacet" rel="nofollow" OnCommand="btnFacet_Click" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "Text")%>' CommandName="City" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "Value")%>'></asp:LinkButton>
                                    (<%#DataBinder.Eval(Container.DataItem, "Count")%>),
                                </li>
                        </ItemTemplate>
                        <FooterTemplate>
                                </ul>
                                    <span onclick="toggleDisplay('nhsNarrowMoreLink','nhsNarrowMorePop'); return false;" class="nhsMenuClose">Close</span>
                                </div>
                            </div>
                        </FooterTemplate>
                    </asp:Repeater>                
            </asp:Panel>  

            <%--***** EXPAND TO PANEL ***** --%>            
            <asp:Panel ID="pnlExpandLocationTo" CssClass="nhsCommResNarrowList" runat="server" Visible="false"  EnableViewState="true">                
                    Expand to the <asp:LinkButton ID="btnExpandLocationTo" OnCommand="btnFacet_Click" CommandName="ExpandToMarket" runat="server"></asp:LinkButton>                        
            </asp:Panel>
        </div></div>    
        <%-- ***** LOCATION SCOPE BAR END *****--%> 
        <asp:Panel ID="pnlMoreSearches" runat="server" CssClass="nhsMoreSearches">
            <p><strong>More new home searches</strong></p>
            <nhs:NhsBulletedList ID="lstCrossMarketingLinks" EncodeText="false" runat="server" />
        </asp:Panel>      
    </div>
    
        <%-- ***** FACETS BEGIN *****--%>
    <div class="nhsHomeResFacets">
        <p class="nhsCommResRefine"><strong>Refine your search:</strong><nhs:Link ID="lnkTryAdvanceSearch" runat="server" Text="Or set more options with Advanced Search" Function="AdvancedSearch"/></p>
        <div class="nhsHomeResFacetGroup">
            <p><strong>Price:</strong></p>
            <ul>
                <asp:Repeater id="rptPriceFact" runat="server" EnableViewState="true">
                    <ItemTemplate>                                
                        <li>
                            <asp:LinkButton 
                                id="btnPriceFacet"
                                rel="nofollow"
                                OnCommand="btnFacet_Click" 
                                runat="server" 
                                text='<%#DataBinder.Eval(Container.DataItem, "Text")%>' 
                                CommandName="Price" 
                                CommandArgument='<%#DataBinder.Eval(Container.DataItem, "Value")%>' 
                                Visible='<%#((bool)DataBinder.Eval(Container.DataItem, "Actionable"))%>'>
                            </asp:LinkButton>
                            <asp:Label 
                                id="lblSelectedPriceFacet" 
                                runat="server" 
                                CssClass='<%#(bool)DataBinder.Eval(Container.DataItem, "Selected")?"nhsFacetOn":"nhsFacetEmpty"%>'
                                visible='<%#!(bool)DataBinder.Eval(Container.DataItem, "Actionable")%>'>
                                <%#DataBinder.Eval(Container.DataItem, "Text")%>
                            </asp:Label> 
                            <asp:Label 
                                id="lblPriceFacetCount" 
                                runat="server" 
                                Text='<%# "(" + DataBinder.Eval(Container.DataItem, "Count") + ")"%>' 
                                Visible='<%#DataBinder.Eval(Container.DataItem, "Value") != string.Empty%>'/>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>                
            </ul>
        </div>
        <div class="nhsHomeResFacetGroup">
            <p><strong>Home size:</strong></p>
            <ul>
                <asp:Repeater id="rptHomeSizeFacet" runat="server" EnableViewState="true">
                    <ItemTemplate>                                
                        <li>
                            <asp:LinkButton 
                                id="btnHomeSizeFacet"
                                rel="nofollow"
                                OnCommand="btnFacet_Click" 
                                runat="server" 
                                text='<%#DataBinder.Eval(Container.DataItem, "Text")%>' 
                                CommandName="HomeSize" 
                                CommandArgument='<%#DataBinder.Eval(Container.DataItem, "Value")%>' 
                                Visible='<%#((bool)DataBinder.Eval(Container.DataItem, "Actionable"))%>'>
                            </asp:LinkButton>
                            <asp:Label 
                                id="lblSelectedHomeSizeFacet" 
                                runat="server" 
                                CssClass='<%#(bool)DataBinder.Eval(Container.DataItem, "Selected")?"nhsFacetOn":"nhsFacetEmpty"%>'
                                visible='<%#!(bool)DataBinder.Eval(Container.DataItem, "Actionable")%>'>
                                <%#DataBinder.Eval(Container.DataItem, "Text")%>
                            </asp:Label> 
                            <asp:Label 
                                id="lblHomeSizeFacetCount" 
                                runat="server" 
                                Text='<%# "(" + DataBinder.Eval(Container.DataItem, "Count") + ")"%>' 
                                Visible='<%#DataBinder.Eval(Container.DataItem, "Value") != string.Empty%>'/>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
        <div class="nhsHomeResFacetGroup">
            <p><strong>Home type:</strong></p>
            <ul>
                <asp:Repeater id="rptHomeTypeFacet" runat="server" EnableViewState="true">
                    <ItemTemplate>                                
                        <li>
                            <asp:LinkButton 
                                id="btnHomeTypeFacet"
                                rel="nofollow"
                                OnCommand="btnFacet_Click" 
                                runat="server" 
                                text='<%#DataBinder.Eval(Container.DataItem, "Text")%>' 
                                CommandName="HomeType" 
                                CommandArgument='<%#DataBinder.Eval(Container.DataItem, "Value")%>' 
                                Visible='<%#((bool)DataBinder.Eval(Container.DataItem, "Actionable"))%>'>
                            </asp:LinkButton>
                            <asp:Label 
                                id="lblSelectedHomeTypeFacet" 
                                runat="server" 
                                CssClass='<%#(bool)DataBinder.Eval(Container.DataItem, "Selected")?"nhsFacetOn":"nhsFacetEmpty"%>'
                                visible='<%#!(bool)DataBinder.Eval(Container.DataItem, "Actionable")%>'>
                                <%#DataBinder.Eval(Container.DataItem, "Text")%>
                            </asp:Label> 
                            <asp:Label 
                                id="lblHomeTypeFacetCount" 
                                runat="server" 
                                Text='<%# "(" + DataBinder.Eval(Container.DataItem, "Count") + ")"%>' 
                                Visible='<%#DataBinder.Eval(Container.DataItem, "Value") != string.Empty%>'/>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
            <p id="hotDealsFacet" runat="server" Visible="False"><strong>Hot Deals:</strong></p>
            <ul id="hotDealsFacetItems" runat="server" Visible="False">
                <asp:Repeater id="rptSpecialOfferFacet" runat="server" EnableViewState="true">
                    <ItemTemplate>                                
                        <li>
                            <asp:LinkButton 
                                id="btnSpecialOfferFacet" 
                                rel="nofollow"
                                OnCommand="btnFacet_Click" 
                                runat="server" 
                                text='<%#DataBinder.Eval(Container.DataItem, "Text")%>' 
                                CommandName="SpecialOffer" 
                                CommandArgument='<%#DataBinder.Eval(Container.DataItem, "Value")%>' 
                                Visible='<%#((bool)DataBinder.Eval(Container.DataItem, "Actionable"))%>'>
                            </asp:LinkButton>
                            <asp:Label 
                                id="lblSelectedSpecialOfferFacet" 
                                runat="server" 
                                CssClass='<%#(bool)DataBinder.Eval(Container.DataItem, "Selected")?"nhsFacetOn":"nhsFacetEmpty"%>'
                                visible='<%#!(bool)DataBinder.Eval(Container.DataItem, "Actionable")%>'>
                                <%#DataBinder.Eval(Container.DataItem, "Text")%>
                            </asp:Label> 
                            <asp:Label 
                                id="lblSpecialOfferFacetCount" 
                                runat="server" 
                                Text='<%# "(" + DataBinder.Eval(Container.DataItem, "Count") + ")"%>' 
                                Visible='<%#DataBinder.Eval(Container.DataItem, "Value") != string.Empty%>'/>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>                
        <div class="nhsHomeResFacetGroup">
            <p><strong>Community amenity:</strong></p>
            <ul>
                <asp:Repeater id="rptCommunityAmenityFacet" runat="server" EnableViewState="true">
                    <ItemTemplate>                                
                        <li>
                            <asp:LinkButton 
                                id="btnCommunityAmenityFacet"
                                rel="nofollow"
                                OnCommand="btnFacet_Click" 
                                runat="server" 
                                text='<%#DataBinder.Eval(Container.DataItem, "Text")%>' 
                                CommandName="CommunityAmenity" 
                                CommandArgument='<%#DataBinder.Eval(Container.DataItem, "Value")%>' 
                                Visible='<%#((bool)DataBinder.Eval(Container.DataItem, "Actionable"))%>'>
                            </asp:LinkButton>
                            <asp:Label 
                                id="lblSelectedCommunityAmenityFacet" 
                                runat="server" 
                                CssClass='<%#(bool)DataBinder.Eval(Container.DataItem, "Selected")?"nhsFacetOn":"nhsFacetEmpty"%>'
                                visible='<%#!(bool)DataBinder.Eval(Container.DataItem, "Actionable")%>'>
                                <%#DataBinder.Eval(Container.DataItem, "Text")%>
                            </asp:Label> 
                            <asp:Label 
                                id="lblCommunityAmenityFacetCount" 
                                runat="server" 
                                Text='<%# "(" + DataBinder.Eval(Container.DataItem, "Count") + ")"%>' 
                                Visible='<%#DataBinder.Eval(Container.DataItem, "Value") != string.Empty%>'/>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>           
            </ul>
        </div>
        <div class="nhsHomeResFacetGroup">
            <p><strong>School district:</strong></p>
            <ul>
                <%-- Repeater for first 4 school districts --%>
                <asp:Repeater id="rptSchoolDistrictFacet" runat="server" EnableViewState="true">
                    <ItemTemplate>
                        <li>
                            <asp:LinkButton 
                                id="btnSchoolDistrictFacet" 
                                rel="nofollow"
                                OnCommand="btnFacet_Click" 
                                runat="server" 
                                text='<%#DataBinder.Eval(Container.DataItem, "Text")%>' 
                                CommandName="School" 
                                CommandArgument='<%#DataBinder.Eval(Container.DataItem, "Value")%>' 
                                Visible='<%#((bool)DataBinder.Eval(Container.DataItem, "Actionable"))%>'>
                            </asp:LinkButton>
                            <asp:Label 
                                id="lblSchoolDistrictFacet" 
                                runat="server" 
                                CssClass='<%#(bool)DataBinder.Eval(Container.DataItem, "Selected")?"nhsFacetOn":"nhsFacetEmpty"%>'
                                visible='<%#!(bool)DataBinder.Eval(Container.DataItem, "Actionable")%>'>
                                <%#DataBinder.Eval(Container.DataItem, "Text")%>
                            </asp:Label> 
                            <asp:Label  
                                id="lblSchoolDistrictFacetCount" 
                                runat="server" 
                                Text='<%# "(" + DataBinder.Eval(Container.DataItem, "Count") + ")"%>' 
                                Visible='<%#DataBinder.Eval(Container.DataItem, "Value") != string.Empty%>'/>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
                
                <%-- Repeater for "More Schools" if there are more than 4 schools. --%>
                <asp:Repeater id="rptMoreSchools" runat="server" visible="false" OnItemDataBound="rptMoreSchools_ItemDataBound">
                    <HeaderTemplate>
                        <li id="nhsFacetSchoolsMore"><a id="nhsMoreSchoolsLink" onclick="toggleDisplay('nhsMoreSchoolsLink','nhsFacetSchoolsPop'); return false;" href="#">More schools...</a>
                            <div id="nhsFacetSchoolsPop" class="nhsPopOff">
                                <ul>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li>
                            <asp:LinkButton 
                                id="btnMoreSchoolDistrictFacet" 
                                rel="nofollow"
                                OnCommand="btnFacet_Click" 
                                runat="server" 
                                text='<%#DataBinder.Eval(Container.DataItem, "Text")%>' 
                                CommandName="School" 
                                CommandArgument='<%#DataBinder.Eval(Container.DataItem, "Value")%>' 
                                Visible='<%#((bool)DataBinder.Eval(Container.DataItem, "Actionable"))%>'>
                            </asp:LinkButton>
                            <asp:Label 
                                id="lblMoreSchoolDistrictFacet" 
                                runat="server" 
                                CssClass='<%#(bool)DataBinder.Eval(Container.DataItem, "Selected")?"nhsFacetOn":"nhsFacetEmpty"%>'
                                visible='<%#!(bool)DataBinder.Eval(Container.DataItem, "Actionable")%>'>
                                <%#DataBinder.Eval(Container.DataItem, "Text")%>
                            </asp:Label> 
                            (<%#DataBinder.Eval(Container.DataItem, "Count")%>)
                        </li>
                    </ItemTemplate>
                    <FooterTemplate>
                                 </ul>
                                <span onclick="toggleDisplay('nhsMoreSchoolsLink','nhsFacetSchoolsPop'); return false;" class="nhsMenuClose">Close</span>
                            </div>
                        </li>
                        <%-- Since we are displaying the popup for more schools, we need to add the "Any School" facet since it was not in the first four. --%>
                        <li><asp:PlaceHolder id="plhAnySchoolDistrictFacet" runat="server"></asp:PlaceHolder></li>
                    </FooterTemplate>
                </asp:Repeater>                                
            </ul>           
        </div>
    </div>   
   <%-- ***** FACETS END *****--%>
    <a id="nhsSkipFacets"></a>       
    <asp:PlaceHolder ID="plhHomeResultsTab" runat="server">
    <div class="nhsHomeResTabs">
        <ul>
            <li class="commTabOn">
                <strong>Builder Homes</strong></li>
            <li>
                <asp:hyperlink ID="lnkMLSHomes" runat="server">REALTOR.com&reg; Homes</asp:hyperlink>
</li>
        </ul>
    </div>
    </asp:PlaceHolder>
   <%-- ***** PAGING BAR BEGIN ***** --%>
    <div class="nhsSortPagingBar">
        <asp:DropDownList id="ddlSortBy" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSortBy_SelectedIndexChanged">
            <asp:ListItem selected="true" Text="Sort by" Value=""></asp:ListItem>
            <asp:ListItem Text="Location" Value="location"></asp:ListItem>
            <asp:ListItem Text="Priced From" Value="price"></asp:ListItem>
            <asp:ListItem Text="Status/Availability" Value="status"></asp:ListItem>
            <asp:ListItem Text="Bed/Bath/Garage" Value="size"></asp:ListItem>
            <asp:ListItem Text="Builder" Value="builder"></asp:ListItem>  
            <asp:ListItem Text="Plan Name" Value="name"></asp:ListItem>           
            <asp:ListItem Text="Promotions" Value="specialoffer"></asp:ListItem>
        </asp:DropDownList>
        <nhs:pagingtools ID="ctlPagingToolsTop" runat="server"/>      
    </div>
  <%-- ***** PAGING BAR END ***** --%>
  
  
    <%-- ***** RESULTS BEGIN ***** --%>
    <asp:Repeater ID="rptHomeResults" runat="server" OnItemDataBound="rptHomeResults_ItemDataBound" EnableViewState="false">
        <HeaderTemplate></HeaderTemplate>
        <ItemTemplate>            
                <asp:Panel ID="pnlGroupingBar" runat="server" CssClass="nhsGroupingBar" Visible="false" EnableViewState="false">
                    <p><asp:Literal ID="litGroupingBarTitle" runat="server" EnableViewState="false"></asp:Literal></p><hr class="nhsReaderAlt" />
                </asp:Panel>          
                <div id="divResultsRow" runat="server" class="nhsHomeResultsRow" onmouseover="$(this).addClass('nhsHomeResultsRowOn');" onmouseout="$(this).removeClass('nhsHomeResultsRowOn');"
                     itemscope itemtype="http://schema.org/ImageObject">
                    <div id="divHotHomeSummary" runat="server" class="nhsHotHomeSummary" visible="false">
                        <p><strong itemprop="name"><asp:Literal ID="litHotHomeTitle" runat="server"/></strong>
                        <span itemprop="description" ><asp:Literal ID="litHotHomeDescription" runat="server" /></span>
                        <asp:HyperLink ID="lnkHotHomeInfo" runat="server" Text="Get Info"  /></p>
                    </div>
                    <div class="nhsHomeResultsHotInner">
                    <div class="nhsResultsThumb">
                        <asp:HyperLink ID="lnkHomeThumbnail" runat="server">
                        <asp:Image ID="imgHomeThumbnail" ImageUrl="[resource:]/globalresources14/default/images/no_photo/no_photos_75x58.png" runat="server" width="75" height="58" BorderWidth="0" AlternateText="No photo available" itemprop="contentUrl"/></asp:HyperLink>
                        <asp:Literal ID="litHomeImageCount" runat="server"/>
                    </div>                    
                    <div class="nhsHomeResultsInfo">
                        <h3 itemprop="name"><asp:HyperLink ID="lnkHomeName" runat="server"/></h3>
                        <div class="nhsResultsPrice">
                            <p><strong><asp:Literal ID="litHomePrice" runat="server"/></strong></p>
                        </div>
                        <div class="nhsResultsBrBaGr">
                            <p>
                            <strong><asp:Literal ID="litHomeBedrooms" runat="server"/></strong>
                            <strong><asp:Literal ID="litHomeBathrooms" runat="server"/></strong><abbr title="bathrooms">ba</abbr>
                            <strong><asp:Literal ID="litHomeGarages" runat="server"/></strong><abbr title="garage">gr</abbr>
                            </p>
                        </div>
                        <div class="nhsResultsHomeStatus">
                            <asp:Image ID="imgHomeStatusImage" ImageUrl="http://www.newhomesource.com/images/global/ready_icon.gif" runat="server" />     
                        </div>
                        <p class="nhsHomeResultsInfo2">
                            <span itemprop="description" class="nhsHomeResultsBuilder">By <asp:Literal ID="litBrandName" runat="server"/> (<asp:Literal ID="litHomeSquareFeet" runat="server"/> <abbr title="square feet">sq.ft.</abbr>)</span><br />
                            <strong><span class="nhsHomeResCommName"><asp:Literal ID="litHomeCommunityName" runat="server"/></span><br />
                            <span class="nhsResultsAddress"><asp:Literal ID="litHomeCity" runat="server"/>, <asp:Literal ID="litHomeState" runat="server"/> <asp:Literal ID="litHomeZip" runat="server"/></span></strong><br />
                            <asp:HyperLink ID="lnkVirtualTour" rel="nofollow" runat="server"><asp:Image ID="imgVirtualTour" ImageUrl="[resource:]/globalresources/default/images/icons/icon_virtual_tour.gif" AlternateText="Virtual Tour" runat="server" /></asp:HyperLink>
                            <asp:HyperLink ID="lnkPlanViewer" rel="nofollow" runat="server"><asp:Image ID="imgPlanViewer" ImageUrl="[resource:]/globalresources/default/images/icons/icon_planview.gif" AlternateText="Plan Viewer" runat="server" /></asp:HyperLink>   
                            <asp:HyperLink ID="lnkSpecialOffer" rel="nofollow" runat="server">
                                <asp:Image ID="imgSpecialOffer" ImageUrl="[resource:]/globalresourcesmvc/default/images/icons/icon_special_offer.png" AlternateText="Special Offer" runat="server" />
                            </asp:HyperLink> 
                            <asp:Image ID="imgCommunityGreen" runat="server" Visible="false" /></p>   
                        <div class="nhsResultsBrochureBtn">
                            <asp:HyperLink ID="lnkBrochure" runat="server"  Text="Free brochure" CssClass="btnCss btnFreeBrochure" />
                             <span class="nhs_LockIcon" id="nhs_LockIcon" runat="server"></span>
                            </div>
                    <div class="nhsClear"></div>
                    </div>
                    <asp:Literal ID="litHomeMarketingDescription" runat="server" Visible="false"></asp:Literal></div></div>            
        </ItemTemplate>
        <FooterTemplate></FooterTemplate>
    </asp:Repeater>
    <%-- ***** RESULTS END ***** --%>
    
    
    <%-- ***** BOTTOM PAGING BAR ***** --%>
    <div class="nhsPagingBar">
            <nhs:pagingtools ID="ctlPagingToolsBottom" runat="server"/>
    </div>
    
    <asp:Panel ID="pnlViewComingSoonResults" runat="server" CssClass="nhsHomeResSpecialRow" onmouseover="this.className='nhsHomeResSpecialRowOn';" onmouseout="this.className='nhsHomeResSpecialRow';">
        <h3><asp:HyperLink ID="lnkComingSoon"  runat="server" Text="Coming Soon &amp;amp; Grand Openings"></asp:HyperLink></h3>            
        <p>See communities just opening, or coming soon, to the greater <asp:Literal ID="litComingSoonAreaName" runat="server"/> area</p>        
    </asp:Panel>     
    
    <div class="nhsHomeResSpecialRow">
            <h3><asp:HyperLink ID="lnkSearchCommunities" runat="server" Text="Search for new home community locations with a map"></asp:HyperLink></h3>                  
    </div>
    
    <div class="nhsPartnerAds" id="divPartnerAd" runat="server">       
    <!-- Partner Ads here //-->
    </div>
    
    <p class="nhsLegalText nhsResLegal">
    While Builders Digital Experience seeks to ensure that all of the information concerning new homes and new home communities, as well as other data on the site is current and accurate, we do not assume any liability for inaccuracies.  It is your responsibility to independently verify information on the site.</p>    
    
</div>

<asp:Literal ID="nhsGoogleAnalytics" runat="server"></asp:Literal>
<script type="text/javascript">
    // Redirect function
    function Redirect(e, url, newWin)
    {        
        if (!e) var e = window.event;
        e.cancelBubble = true;
        if (e.stopPropagation) e.stopPropagation();
        if (url)	       
        {
            if (newWin)
            {
                window.open(url);
            }
            else
            {
                window.location.href=url;
            }
        }
    }
</script>

<script type="text/javascript">
    // Redirect function
    function Redirect(e, url, newWin)
    {        
        if (!e) var e = window.event;
        e.cancelBubble = true;
        if (e.stopPropagation) e.stopPropagation();
        if (url)	       
        {
            if (newWin)
            {
                window.open(url);
            }
            else
            {
                window.location.href=url;
            }
        }
    }
</script>

<script type="text/javascript">
    // Redirect function
    function Redirect(e, url, newWin)
    {        
        if (!e) var e = window.event;
        e.cancelBubble = true;
        if (e.stopPropagation) e.stopPropagation();
        if (url)	       
        {
            if (newWin)
            {
                window.open(url);
            }
            else
            {
                window.location.href=url;
            }
        }
    }
</script>
