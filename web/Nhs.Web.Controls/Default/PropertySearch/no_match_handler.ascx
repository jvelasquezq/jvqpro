<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="no_match_handler.ascx.cs" Inherits="Nhs.Web.Controls.Default.PropertySearch.no_match_handler" %>
<%@ Register Src="~/Controls/Default/Common/spotlight_communities.ascx" TagPrefix="nhs" TagName="spotlight" %>

<div class="nhsContent" id="nhsNoMatch">

    <div class="nhsSpotlight">
        <div class="nhsNoMatchSpot">
        <h3>Spotlight new home communities in the <asp:Literal ID="litSpotlightMarketArea" runat="server"/> area</h3>

        <nhs:spotlight ID="spotlightComms" runat="server" />
        
        </div>
    </div>
    
    
    
    <div id="nhsNoMatchMain">
        <h2>We're sorry</h2> 
        <h3>We could not find any <asp:Literal ID="litResultsType" runat="server"/> matching your exact search criteria.</h3>

        <p>All listings in the the greater <asp:Literal ID="litMarketName1" runat="server"/> area:</p>
        
        <h3><asp:Literal ID="litTotalResults" runat="server"/> new <asp:Literal ID="litResultsType2" runat="server"/> from <asp:Literal ID="litMarketPriceLow" runat="server"/> - <asp:Literal ID="litMarketPriceHigh" runat="server"/></h3>
        
        <asp:BulletedList ID="lstMarketSearchLinks" runat="server" DisplayMode=HyperLink>
        </asp:BulletedList>
        
        <ul>
            <asp:Literal ID="litViewAll" runat="server"></asp:Literal>
            <asp:Literal ID="litViewComingSoon" runat="server" Visible=false></asp:Literal>
            <asp:Literal ID="litViewCustomBuilder" runat="server" Visible=false></asp:Literal>
        </ul>
        
        <div class="nhsGroupingBar">
            <h3>Try a different search</h3>
        </div>   
        <div class="nhsNoMatchSearch">
            
            <p>For more results, use fewer search terms.</p>
            <fieldset>
                <legend>Search again</legend>
                <p>
                    <label for="<%=ddlMinPrice.ClientID%>"><abbr title="Minimum">Min.</abbr> price:</label>
                    <asp:DropDownList ID="ddlMinPrice" runat="server" />
                </p>
                <p>
                    <label for="<%=ddlMaxPrice.ClientID%>"><abbr title="Maximum">Max.</abbr> price:</label>
                    <asp:DropDownList ID="ddlMaxPrice" runat="server" />
                </p>
                <p>
                    <label for="<%=ddlHomeType.ClientID%>">Home type:</label>
                    <asp:DropDownList ID="ddlHomeType" runat="server" />
                </p>
                <p>
                    <label for="<%=ddlCity.ClientID%>">City:</label>
                    <asp:DropDownList ID="ddlCity" runat="server" />
                </p> 
            </fieldset>
            <asp:Button ID="btnSearchAgain" runat="server" CssClass="btn btnSearchAgain" Text="Search again" OnClick="btnSearchAgain_Click" />           
            <p class="nhsNoMatchLinkAdv"><nhs:Link ID="lnkAdvanced" Function="AdvancedSearch" Text="Advanced search" runat="server" /></p>
        </div>
        
        <div class="nhsNoMatchAd">
            <nhs:ad ID="Middle" UseIFrame="true" runat="server" />
        </div>
        
    </div>
    <div class="nhsPartnerAds" id="divPartnerAd" runat="server">       
        <!-- Partner Ads here //-->
    </div>
</div>
