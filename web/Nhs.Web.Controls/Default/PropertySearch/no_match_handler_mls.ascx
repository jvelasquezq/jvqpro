<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="no_match_handler_mls.ascx.cs" Inherits="Nhs.Web.Controls.Default.PropertySearch.no_match_handler_mls" %>
<%@ Register Src="~/Controls/Default/Common/spotlight_communities.ascx" TagPrefix="nhs" TagName="spotlight" %>

<div class="nhsContent" id="nhsNoMatch">

   
    
    <div id="nhsNoMatchMain">
        <h2>We're sorry</h2> 
        <h3>No matches were found for your search criteria.</h3>
        <asp:BulletedList ID="lstMarketSearchLinks" runat="server" DisplayMode=HyperLink>
        </asp:BulletedList>
        
        <ul>
            <asp:Literal ID="litViewAll" runat="server"></asp:Literal>
        </ul>
        
        <br/><br/><br/>
    </div>
    
</div>
