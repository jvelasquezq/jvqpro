using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Nhs.Library.Common;
using Nhs.Library.Business;
using Nhs.Library.Web;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Common;
using Nhs.Web.Controls.Default.Common;

namespace Nhs.Web.Controls.Default.PropertySearch
{
    public partial class no_match_handler : UserControlBase
    {
        private IMarket _market;
        private int _priceLow;
        private int _priceHigh;
        private string _homeType;
        private string _state;
        private string _city;
        private string _searchType;
        private string _resultsTypeText;
        private string _resultsPage;

        protected void Page_Load(object sender, EventArgs e)
        {
            int marketId = NhsUrl.GetMarketID;
            _priceLow = NhsUrl.GetPriceLow;
            _priceHigh = NhsUrl.GetPriceHigh;
            _homeType = NhsUrl.GetHomeType;

            if (!String.IsNullOrEmpty(NhsUrl.GetCity))
                _city = NhsUrl.GetCity;
            else if (!String.IsNullOrEmpty(NhsUrl.GetCityNameFilter))
                _city = NhsUrl.GetCityNameFilter;

            _searchType = NhsUrl.GetSearchType;
            _market = null;

            if (marketId != 0) _market = XGlobals.Partner.GetMarket(marketId);

            if (_market == null) Response.Redirect("~/home");

            if (marketId != 0 & _market != null)
            {
                _state = _market.State;
                AdController.AddStateParameter(_state);
                AdController.AddMarketParameter(marketId);
            }

            if (_searchType.ToLower() == SearchTypeSource.QuickSearchHomeResults
                || _searchType.ToLower() == SearchTypeSource.AdvancedSearchHomeResults)
            {
                _resultsTypeText = "homes";
                _resultsPage = Pages.HomeResults;
            }
            else
            {
                _resultsTypeText = "home communities";
                _resultsPage = Pages.CommunityResults;
            }

            // Set spotlight communities market id. 
            ((Spotlight_communities)this.spotlightComms).MarketId = _market.MarketId;

            if (!Page.IsPostBack)
            {
                BindDropDowns();

                if (_market.PriceLow == 0)
                {
                    litMarketPriceLow.Text = "0";
                }
                else
                {
                    litMarketPriceLow.Text = String.Format("{0:$###,###}", _market.PriceLow);
                }

                if (_market.PriceHigh == 0)
                {
                    litMarketPriceHigh.Text = "0";
                }
                else
                {
                    litMarketPriceHigh.Text = String.Format("{0:$###,###}", _market.PriceHigh);
                }
                
                litMarketName1.Text = _market.MarketName;
                litSpotlightMarketArea.Text = _market.MarketName;
                litResultsType.Text = _resultsTypeText;
                litResultsType2.Text = _resultsTypeText;

                if (_resultsPage == Pages.CommunityResults)
                {
                    litTotalResults.Text = _market.TotalCommunities.ToString();
                }
                else
                {
                    litTotalResults.Text = _market.TotalHomes.ToString();
                }


                BindSearchLinks();
            }
        }

        private void BindSearchLinks()
        {
            ListItem viewAll = new ListItem();
            NhsUrl viewAllUrl = new NhsUrl(_resultsPage);
            viewAllUrl.Parameters.Add(UrlConst.MarketID, _market.MarketId.ToString(), RouteParamType.Friendly);            
            viewAll.Text = "View all new " + _resultsTypeText + " in the greater " + _market.MarketName + " area";
            viewAll.Value = ResolveUrl("~/" + viewAllUrl.ToString());


            ListItem viewComingSoon = new ListItem();
            NhsUrl viewComingSoonUrl = new NhsUrl(_resultsPage);
            viewComingSoonUrl.Parameters.Add(UrlConst.MarketID, _market.MarketId.ToString(), RouteParamType.Friendly);
            viewComingSoonUrl.Parameters.Add(UrlConst.ComingSoon, "true", RouteParamType.Friendly);
            viewComingSoon.Text = "View grand opening and coming soon new home communities";
            viewComingSoon.Value = ResolveUrl("~/" + viewComingSoonUrl.ToString());

            ListItem viewCustomBuilders = new ListItem();
            NhsUrl viewCustomBuildersUrl = new NhsUrl(Pages.BOYLResults);
            viewCustomBuildersUrl.Parameters.Add(UrlConst.MarketID, _market.MarketId.ToString(), RouteParamType.Friendly);
            viewCustomBuilders.Text = _market.TotalCustomBuilders + " builders will custom build, or build on your lot";
            viewCustomBuilders.Value = ResolveUrl("~/" + viewCustomBuildersUrl.ToString());


            if (_market.Cities.Count > 0)
            {
                lstMarketSearchLinks.Items.Add(viewAll);            
            }

            if (_market.TotalComingSoon > 0)
            {
                lstMarketSearchLinks.Items.Add(viewComingSoon);
            }

            if (_market.TotalCustomBuilders > 0)
            {
                lstMarketSearchLinks.Items.Add(viewCustomBuilders);
            }
        }

        private void BindDropDowns()
        {
            ddlHomeType.Items.AddRange(XGlobals.GetCommonListItems(CommonListItem.HomeType));
            ddlMinPrice.Items.AddRange(XGlobals.GetCommonListItems(CommonListItem.MinPrice));
            ddlMaxPrice.Items.AddRange(XGlobals.GetCommonListItems(CommonListItem.MaxPrice));
            DataBindHelper.BindDropDown(_market.Cities, ddlCity, "Name", "Name");            
            ddlCity.Items.Insert(0, new ListItem(DropDownDefaults.City, string.Empty));
            ddlMinPrice.Items.Insert(0, new ListItem(DropDownDefaults.MinPrice, string.Empty));
            ddlMaxPrice.Items.Insert(0, new ListItem(DropDownDefaults.MaxPrice, string.Empty));

            SetDropDown(ddlCity, _city);
            SetDropDown(ddlMinPrice, _priceLow.ToString());
            SetDropDown(ddlMaxPrice, _priceHigh.ToString());
            SetDropDown(ddlHomeType, _homeType);
        }

        private void SetDropDown(DropDownList dropDownList, string value)
        {
            dropDownList.SelectedIndex = -1;

            if (!String.IsNullOrEmpty(value))
            {
                foreach (ListItem listItem in dropDownList.Items)
                {
                    if (listItem.Value.Trim().ToLower() == value.ToLower())
                    {
                        listItem.Selected = true;
                    }
                }
            }
        }

        protected void btnSearchAgain_Click(object sender, EventArgs e)
        {
            NhsUrl resultsUrl = new NhsUrl();

            if (_searchType.ToLower() == SearchTypeSource.QuickSearchHomeResults
                || _searchType.ToLower() == SearchTypeSource.AdvancedSearchHomeResults)
            {
                resultsUrl.Function = Pages.HomeResults;
            }
            else
            {
                resultsUrl.Function = Pages.CommunityResults;
            }

            resultsUrl.AddParameter(UrlConst.MarketID, _market.MarketId.ToString(), RouteParamType.Friendly);

            if (ddlMinPrice.SelectedValue != "") 
            {
                resultsUrl.AddParameter(UrlConst.PriceLow, ddlMinPrice.SelectedValue, RouteParamType.Friendly);
            }

            if (ddlMaxPrice.SelectedValue != "")
            {
                resultsUrl.AddParameter(UrlConst.PriceHigh, ddlMaxPrice.SelectedValue, RouteParamType.Friendly);
            }

            if (ddlCity.SelectedValue != string.Empty)
            {
                resultsUrl.AddParameter(UrlConst.City, ddlCity.SelectedValue, RouteParamType.Friendly);
            }

            if (ddlHomeType.SelectedValue != string.Empty && ddlHomeType.SelectedValue != "-1")
            {
                resultsUrl.AddParameter(UrlConst.HomeType, ddlHomeType.SelectedValue, RouteParamType.Friendly);
            }

            resultsUrl.Redirect();

        }
    }
}
