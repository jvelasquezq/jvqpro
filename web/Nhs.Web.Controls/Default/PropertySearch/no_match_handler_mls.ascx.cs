using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Nhs.Library.Common;
using Nhs.Library.Business;
using Nhs.Library.Web;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Web.Controls.Default.Common;

namespace Nhs.Web.Controls.Default.PropertySearch
{
    public partial class no_match_handler_mls : UserControlBase
    {
        private IMarket _market;
        private int _priceLow;
        private int _priceHigh;
        private string _homeType;
        private string _state;
        private string _city;
        private string _searchType;
        private string _resultsPage;

        protected void Page_Load(object sender, EventArgs e)
        {
            int marketId = NhsUrl.GetMarketID;
            _priceLow = NhsUrl.GetPriceLow;
            _priceHigh = NhsUrl.GetPriceHigh;
            _homeType = NhsUrl.GetHomeType;

            if (!String.IsNullOrEmpty(NhsUrl.GetCity))
                _city = NhsUrl.GetCity;
            else if (!String.IsNullOrEmpty(NhsUrl.GetCityNameFilter))
                _city = NhsUrl.GetCityNameFilter;

            _searchType = NhsUrl.GetSearchType;
            _market = null;

            if (marketId != 0) _market = XGlobals.Partner.GetMarket(marketId);

            if (_market == null) Response.Redirect("~/home");

            if (marketId != 0 & _market != null)
            {
                _state = _market.State;
                AdController.AddStateParameter(_state);
                AdController.AddMarketParameter(marketId);
            }

            if (_searchType.ToLower() == SearchTypeSource.QuickSearchHomeResults
                || _searchType.ToLower() == SearchTypeSource.AdvancedSearchHomeResults)
            {
                _resultsPage = Pages.HomeResults;
            }
            else
            {
                _resultsPage = Pages.CommunityResults;
            }

            if (!Page.IsPostBack)
            {
                BindSearchLinks();
            }
        }

        private void BindSearchLinks()
        {
            ListItem viewAll = new ListItem();
            NhsUrl viewAllUrl = new NhsUrl(_resultsPage);
            viewAllUrl.Parameters.Add(UrlConst.MarketID, _market.MarketId.ToString(), RouteParamType.Friendly);     

            viewAll.Text = "View new homes in the " + _market.MarketName + " area";

            viewAll.Value = UserSession.GetItem(SessionConst.BuilderHomesUrl).ToString();
            
            if (_market.Cities.Count > 0)
            {
                lstMarketSearchLinks.Items.Add(viewAll);            
            }           
        }

        private void SetDropDown(DropDownList dropDownList, string value)
        {
            dropDownList.SelectedIndex = -1;

            if (!String.IsNullOrEmpty(value))
            {
                foreach (ListItem listItem in dropDownList.Items)
                {
                    if (listItem.Value.Trim().ToLower() == value.ToLower())
                    {
                        listItem.Selected = true;
                    }
                }
            }
        }
    }
}
