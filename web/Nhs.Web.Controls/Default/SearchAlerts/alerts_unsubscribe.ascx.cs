using System;
using Nhs.Library.Constants;
using Nhs.Library.Web;
using Nhs.Mvc.Data.Repositories;

namespace Nhs.Web.Controls.Default.SearchAlerts
{
    public partial class alerts_unsubscribe : UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnUnsubscribe_Click(object sender, EventArgs e)
        {
            string userId = NhsUrl.GetUserId;
            if (!string.IsNullOrEmpty(userId))
            {
                // Unsubscribes user
                DataProvider.Current.SearchAlertUnsubscribeUser(userId);

                // Redirects user to home
                NhsUrl homeUrl = new NhsUrl(Pages.Home);
                homeUrl.Redirect();
            }
        }
    }
}