<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="alerts_unsubscribe.ascx.cs" Inherits="Nhs.Web.Controls.Default.SearchAlerts.alerts_unsubscribe" %>

<div class="nhsContent">
    <h2>Unsubscribe from alerts</h2>
    <br />
    <p>
        Are you sure you want to unsubscribe?<br />
        This will delete all your saved search alerts
    </p>
    <p>
        <asp:Button ID="btnUnsubscribe" Runat="server" CssClass="btn btnUnsubscribe" Text="Unsubscribe" OnClick="btnUnsubscribe_Click" />
        or, <nhs:Link ID="lnkCancel" runat="server" Function="home">Cancel</nhs:Link>
    </p>
</div>