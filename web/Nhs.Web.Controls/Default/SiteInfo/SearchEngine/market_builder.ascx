<%@ Import Namespace="Nhs.Library.Common"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="market_builder.ascx.cs" Inherits="Nhs.Web.Controls.Default.SiteInfo.SearchEngine.market_builder" %>
<%@ Import Namespace="Nhs.Utility.Common" %>
<%@ Import Namespace="Nhs.Utility.Constants" %>

<!--NewHomeSource market builder index-->
    <div class="nhsIndexContent">
        <div class="nhsIndexContentTop">
               
             <% if (XGlobals.Partner.PartnerId == PartnersConst.NewHomeSource.ToType<int>() ||
                    XGlobals.Partner.PartnerId == PartnersConst.Move.ToType<int>())
                { %>
             <div class="nhsIndexTitle">	
                <h1>
                   <nhs:seo_content_manager ID="scNhsHeading" runat="server" CodeBlock="heading1" TemplateType="nhs_market_brand" />
                </h1>            
            </div>  

            <nhs:staticcontent ID="scMarketBuilderPageHeaderSEO" runat="server" UrlPath="SiteIndex\DefaultMarketBuilderPageHeaderSEO.htm" />
            <% }
                else
                { %>
                <nhs:staticcontent ID="scMarketBuilderPageHeader" runat="server" UrlPath="SiteIndex\DefaultMarketBuilderPageHeader.htm" />
            <% } %>
            
            <div id="nhsIndexLogo">
		        <%--<nhs:Link ID="lnkHome" runat="server" Function="Home" title="New Homes"><img id="nhsLogo" class="nhsIndexLogo" src="[resource:]/globalresources/default/images/nhs_logo.gif" alt="New Homes" border="0" runat="server" /></nhs:Link>--%>
                <p>
                    <nhs:Link ID="lnkSearch" runat="server"><strong>Search for new homes across the country on <%=XGlobals.Partner.PartnerName %></strong></nhs:Link></p>
                <p><asp:HyperLink ID="lnkUrbanCondo" ToolTip="Search for new condos in {0}, {1} on UrbanCondoLiving.com" runat="server" Target="_blank" NavigateUrl="http://www.urbancondoliving.com"><strong>Search for new condos in {0}, {1} on UrbanCondoLiving.com</strong></asp:HyperLink></p>
            </div>
        </div>
        
        <hr class="nhsReaderAlt" />
        
        <div id="nhsIndexContentLeftAlt" class="nhsIndexContentLeftAlt" runat="server">
        
        <div class="nhsIndexSpotlight">
        <div class="nhsNoMatchSpot">
            <h3 id="nhsIndexSpotlightHeading">
                
                <nhs:staticcontent runat="server" ID="scSpotlightCommsHeader" StaticHTML="[MarketName], [StateID] new home communities spotlight"/>
                
            </h3>
            <p class="nhsIndexSpotLinkP"><strong><nhs:Link ID="lnkViewNewHomesSpotLight" runat="server" Text="View all {0} new home developments in the {1}, {2} area"></nhs:Link></strong></p>
        </div>
        </div>
            
        </div> 
        <div id="nhsIndexContentRightAlt" class="nhsIndexContentRightAlt" runat="server"> <!-- add class="nhsIndexContentRightWide" in no spotlights -->
                <div class="nhsIndexBox nhsFillBoxTR"><div class="nhsFillBoxTL"><div class="nhsFillBoxBL"><div class="nhsFillBoxBR">
                    <h2 class="nhsIndexHeading">
                    
                        <nhs:staticcontent runat="server" ID="scSearchForNewHomes" StaticHTML="Search for new [BuilderName] in [MarketName], [StateID]"/>
                    
                    </h2>
                    <p class="howto">To begin your new home search, select a city from the menu or list below. You can browse all new homes in the area by selecting <strong>All cities</strong>.</p>
                    <asp:Literal ID="litErrorMessage" runat="server" Visible="false"><p class="nhsError">Please enter a city.</p></asp:Literal>
               
                <div>
                <fieldset>
                <div class="nhsIndexForm">
                    <p>
                        <label for="<%=ddlCities.ClientID%>">City:</label>
                        <asp:DropDownList ID="ddlCities" CssClass="nhsIndexAreaSel" Runat="server" />
                    </p>
                    <p>
                        <label for="<%=ddlPriceFrom.ClientID%>">Price<span class="nhsReaderAlt"> From</span>:</label>
                        <asp:DropDownList ID="ddlPriceFrom" Runat="server" /></p>
                    <p> 
                        <label for="<%=ddlPriceTo.ClientID%>">- <span class="nhsReaderAlt">Price To:</span></label>
                        <asp:DropDownList ID="ddlPriceTo" Runat="server" /></p>
                    
                    <asp:Button ID="btnSearchHomes" Runat="server" CssClass="btn btnFindHomes find_homes" Text="Find Homes" OnClick="btnFindHomes_OnClick" />
                    <span style="float:right">
                        <nhs:Link ID="lnkAdvSearch" runat="server" Title="Advanced Search" Text="Advanced Search" Function="AdvancedSearch" />
                    </span> 
                </div>
                </fieldset>
                
                <div class="nhsIndexMap">
                    <p>
                        <asp:HyperLink ID="lnkSearchOnMap" runat="server">
                            <img id="imgMap" src="[resource:]/globalresources/default/images/staticmaps/{0}.gif" alt="{0} map" runat="server" /><br/>
                           Search on a map
                        </asp:HyperLink>
                    </p>
                </div>
                
                <div class="nhsIndexBldrLogo">
                    <asp:HyperLink ID="lnkBuilderLogo" runat="server" CssClass="nhsBuilderLogo" Target="_blank" Text="Builder Logo"></asp:HyperLink>
                    <p><asp:HyperLink ID="lnkBuilderSite" runat="server" target="_blank" >Visit our website</asp:HyperLink></p>
                    <ul>
                        <li><asp:LinkButton ID="lnkSpecialOffer" rel="nofollow" runat="server" OnCommand="btnCommResultsJump_Click"
                                    CommandName="hothomes" CommandArgument="1" Text="Special offers on new homes in {0}, {1} by {2}" Visible="true" /></li>
                        <li><asp:HyperLink ID="lnkQuickMoveIn" Text="Quick Move-In new homes in {0}, {1} by {2}" runat="server" /></li>
                        <li><strong><nhs:Link ID="lnkAllNewHomes" runat="server"  Text="All {0} in {1}"/></strong></li>
                    </ul>
                </div>	
                </div>

                <div class="nhsIndexInnerBox">
                    <asp:Repeater ID="rptCityNewHomes" runat="server" OnItemDataBound="rptCityNewHomes_ItemDataBound">
                        <ItemTemplate>
                            <asp:Literal ID="ltrOpen" runat="server" Visible="false" />                                
                            <li><asp:HyperLink ID="lnkCity" Text="{0} new homes" runat="server" /></li>
                            <asp:Literal ID="ltrClose" runat="server" Visible="false" />
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                
                <div class="nhsIndexInnerBox2" id="ltrBoutiqueIndexDiv" runat="server">

                    <h5 class="nhsIndexHeading">
                    
                    <nhs:staticcontent runat="server" ID="scBoutiqueLinksHeader" StaticHTML="Find special real estate communities in the [MarketName] area from NewHomeSource:"/>
                    
                    </h5>
                    <p id="pSectionGolfCourse" runat="server" class="nhsIndexStGolfCourse"><nhs:Link ID="lnkGolfCourse" Text="Golf course communities" runat="server"></nhs:Link></p>
                    <p id="pSectionWaterfront" runat="server" class="nhsIndexStWaterfront"><nhs:Link ID="lnkWaterfront" Text="Waterfront, lake and beach communities" runat="server"></nhs:Link></p>
                    <p id="pSectionCondoTown"  runat="server" class="nhsIndexStCondoTown"><nhs:Link ID="lnkCondoTown" Text="New condos and townhomes" runat="server"></nhs:Link></p>
                    <p class="nhsIndexStViewHomes"><strong><nhs:Link ID="lnkViewAll" Function="CommunityResults" Text="View all homes in<br /> {0}, {1}" runat="server"></nhs:Link></strong></p>
                </div>
                
                <div class="nhsClear"></div>
            </div></div></div></div>
            
            <asp:PlaceHolder ID="plhMoreInfo" runat="server" Visible="true">
                <div class="nhsIndexBox nhsBoxTR"><div class="nhsBoxTL"><div class="nhsBoxBL"><div class="nhsBoxBR">
                <p class="nhsIndexHeading"><strong>
                
                <nhs:staticcontent ID="scMoreInfoHeading" runat="server" StaticHTML="Local information about [MarketName], [StateName]" />
                
                </strong></p>	
                
                <nhs:staticcontent ID="scInfo" runat="server"></nhs:staticcontent>
                
                    <div class="nhsClear"></div>
                </div></div></div></div>
            </asp:PlaceHolder>	
            
            <div class="nhsIndexBox nhsBoxTR"><div class="nhsBoxTL"><div class="nhsBoxBL"><div class="nhsBoxBR">
                <h6 class="nhsIndexHeading">
                    
                    <nhs:staticcontent runat="server" ID="scTipHeader" StaticHTML="Tips on buying a new home in [MarketName], [StateID]"/>				
                    
                </h6>
                
                <nhs:staticcontent runat="server" ID="scTopRightArticle"/>
                
                <br />
                
                <nhs:staticcontent runat="server" ID="scBottomRightArticle"/>		
                
                <div class="nhsClear"></div>
            </div></div></div></div>
            
        </div>
        
        <%--<div id="nhsIndexAdRightCol">
            <hr class="nhsReaderAlt" />
		    <p class="nhsReader">Advertisement</p>
		    <div class="nhsAdBorder nhsAdRight2"><nhs:ad ID="Right2" runat="server" /></div>
		    <p class="nhsReader">Advertisement</p>		       
		    <div class="nhsAdBorder nhsAdBottom"><nhs:ad ID="Right3" runat="server" /></div>	       
		</div>--%>
        
        <br class="nhsClear" />
        <%--<nhs:footer ID="footer" runat="server" />--%>        
	</div>
	
	<%--<div id="nhsCopyright"><p>Copyright &copy; 2001-<asp:Literal ID="litCurrentYear" runat="server" /> Builder Homesite, Inc.  <a href="http://www.newhomesource.com/">NewHomeSource.com</a> is a trademark of <a href="http://www.builderhomesite.com/">Builder Homesite, Inc.</a> and all other marks are either trademarks or registered trademarks of their respective owners. All rights reserved.</p></div> --%>
