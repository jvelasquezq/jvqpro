using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.IO;
using Nhs.Library.Common;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Helpers;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Web;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Business;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Search.Objects.Constants;
using Nhs.Utility.Common;
using Nhs.Utility.Web;
using Nhs.Web.Controls.Default.Common;

namespace Nhs.Web.Controls.Default.SiteInfo.SearchEngine
{
    public partial class market_builder : UserControlBase
    {
        #region Member Variables

        // Global variables... with state and market values
        private int _marketID = 0;
        private string _marketName = string.Empty;
        private string _stateName = string.Empty;
        private string _state = string.Empty;
        private int _brandId = 0;
        private IMarket _market; // used to get cities list
        private IBrand _brand; // get brand information like name, logo and url
        protected Spotlight_communities spotlightComms;

        // Variables used for create the multicolumns lists 
        private int _rowCount = 0;
        private int _rowNum = 1;
        private int _columnNum = 1;
        private const double _NUM_OF_COLS = 3.0;

        // files path Constants
        private const string MARKET_HEADER_HTML_PATH = @"CustomMarketNewHomesIndexHeader.htm";
        private const string LINKS_FILE_SUFIX = "_Links.htm";
        private const string ARTICLES_LEFT1_FILE_SUFIX = "_Left1.htm";
        private const string ARTICLES_LEFT2_FILE_SUFIX = "_Left2.htm";
        private const string ARTICLES_LEFT1_DEFAULT_FILE_SUFIX = "Default_Left1.htm";
        private const string ARTICLES_LEFT2_DEFAULT_FILE_SUFIX = "Default_Left2.htm";
        private readonly string _STATIC_CONTENT_FOLDER = @"~/" + Nhs.Library.Common.Configuration.StaticContentFolder + @"/";

        #endregion

        #region Page Load

        /// <summary>
        /// Page load event use to bind controls
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            spotlightComms = (Spotlight_communities)LoadControl("../../Common/spotlight_communities.ascx");
            if (!IsPostBack)
                this.BindData();
            else
                this.SetUpPageParameters();
        }

        #endregion

        protected void Page_PreRender(object o, EventArgs e)
        {
            if (!spotlightComms.HasCommunties)
            {
                this.nhsIndexContentRightAlt.Attributes["class"] = this.nhsIndexContentRightAlt.Attributes["class"] + " nhsIndexContentRightWide";
                this.nhsIndexContentLeftAlt.Visible = false;
            }
        }

        #region Private Methods

        /// <summary>
        /// Data Bind for all market index controls
        /// </summary>
        private void BindData()
        {
            // Footer copyright years information parameter
            //litCurrentYear.Text = DateTime.Now.Year.ToString();
            lnkSearch.Title = "Search for new homes across the country on " + XGlobals.Partner.PartnerName;
            //if (XGlobals.Partner.PartnerId == Convert.ToInt32(PartnersConst.Move))
            //{
            //    nhsLogo.Src = ResolveResourceUrl("~/globalresources/move/images/move_logo.gif");
            //}

            //Set Market and State Names
            this.SetUpPageParameters();
            
            if (_marketName.Length == 0 || _stateName.Length == 0)
            {
                NhsUrl friendlyUrl = new NhsUrl(Pages.SiteIndex);
                friendlyUrl.Redirect();
            }

            //Replace Content Tags ([])
            this.ReplaceContentTags();

            scNhsHeading.Market = _marketName;
            scNhsHeading.City = _stateName;
            scNhsHeading.BrandId = _brandId.ToString();
            scNhsHeading.BrandName = _brand.BrandName;
            scNhsHeading.State = _state;

            //Set Custom Index Header Text if available
            if (File.Exists(Server.MapPath(_STATIC_CONTENT_FOLDER + SiteIndexConst.RootFolder + MARKET_HEADER_HTML_PATH)))
                this.scMarketBuilderPageHeader.UrlPath = SiteIndexConst.RootFolder + MARKET_HEADER_HTML_PATH;
            
            int activeCommunities = XGlobals.GetMarketBCs(XGlobals.Partner.PartnerId, _marketID, null).Select("[listing_type_flag] <> 'B' And brand_id = " + _brandId).Length;
            if (_marketID == 0 || activeCommunities == 0)
            {
                Response.RedirectPermanent(Pages.NoResultsFound);
            }

            //set up the spotlight control
            this.SetUpSpotlightControl();

            //Populate Dropdown
            this.BindCityDropDown();

            //Populate prices dropdown
            this.BindPricesRangeDropDowns();

            //Build city list
            this.BindCityRepeater();

            //Setup Links
            this.SetupLinks();

            //Load articles
            this.LoadArticles();

            //Load More Info Links File
            this.LoadMoreInfoLinks();

            // Register Meta Tags information
            this.LoadMeta();

            //Canonical
            var includeParams = new List<string>();
            includeParams.Add(UrlConst.State);
            includeParams.Add(UrlConst.Area);
            base.AddCanonicalWithIncludeParams(includeParams);
        }

        /// <summary>
        /// Setup the spotlight control, it add the proper filters if required and 
        /// check if there are not comms to show to hide the control 
        /// </summary>
        private void SetUpSpotlightControl()
        {
            this.spotlightComms.AlwaysFill = false;
            this.spotlightComms.MarketId = this._marketID;
            this.spotlightComms.CustomFilter = string.Format("brand_id = {0}", this._brandId);
        }

        /// <summary>
        /// Configure all the page's links with the function names and parameters...
        /// </summary>
        private void SetupLinks()
        {
            var resultsFriendlyUrl = string.Empty;
            var resultsOldUrl = string.Empty;

            //Urban condo link
            this.lnkUrbanCondo.NavigateUrl = "http://www.urbancondoliving.com/market/" + _marketID;
            this.lnkUrbanCondo.Text = string.Format(this.lnkUrbanCondo.Text, _marketName, XGlobals.GetStateID(_stateName));
            this.lnkUrbanCondo.ToolTip = string.Format(this.lnkUrbanCondo.ToolTip, _marketName, XGlobals.GetStateID(_stateName));

            this.lnkBuilderLogo.Text = this._brand.BrandName;

            if (!string.IsNullOrEmpty(PathHelpers.RemoveUrlPrefix(_brand.SiteUrl)))
            {
                var builderLogoUrl = new NhsUrl(Pages.LogRedirect);

                builderLogoUrl.Parameters.Add(UrlConst.ExternalURL, PathHelpers.RemoveUrlPrefix(_brand.SiteUrl), RouteParamType.QueryString);
                builderLogoUrl.Parameters.Add(UrlConst.LogEvent, LogImpressionConst.SEOStateBuilder, RouteParamType.Friendly);
                builderLogoUrl.Parameters.Add(UrlConst.LogEvent, CryptoHelper.HashUsingAlgo(PathHelpers.RemoveUrlPrefix(_brand.SiteUrl) + ":" + LogImpressionConst.SEOStateBuilder, "md5"), RouteParamType.Friendly);
                builderLogoUrl.Parameters.Add(UrlConst.BuilderID, _brand.BrandId.ToString(), RouteParamType.Friendly);

                lnkBuilderLogo.NavigateUrl = ResolveUrl("~/" + builderLogoUrl);
                lnkBuilderSite.NavigateUrl = ResolveUrl("~/" + builderLogoUrl);
            }
            else
            {
                lnkBuilderSite.Visible = false;
            }

            lnkBuilderLogo.Visible = false;
            if (!string.IsNullOrEmpty(_brand.LogoUrlMedium))
            {
                lnkBuilderLogo.ImageUrl = Configuration.ResourceDomain + _brand.LogoUrlMedium;

                if (HTTP.CheckURL(this.lnkBuilderLogo.ImageUrl) && !lnkBuilderLogo.ImageUrl.Contains("1x1.gif"))
                    lnkBuilderLogo.Visible = true;
            }


            // All new homes link located at map's right side
            // To prevent displaying home twice for some builders
            string builderName = _brand.BrandName;
            if (!StringHelper.EndsWithHomeWord(_brand.BrandName))
            {
                builderName += " new homes";
            }
            lnkAllNewHomes.Text = string.Format(this.lnkAllNewHomes.Text, builderName, _stateName);
            lnkAllNewHomes.Function = Pages.StateBuilder;
            var param = new RouteParam(RouteParams.BrandId, _brand.BrandId);
            lnkAllNewHomes.Parameters.Add(param);
            param = new RouteParam(RouteParams.State, this._stateName);
            lnkAllNewHomes.Parameters.Add(param);
            var marketDfuReader = new MarketDfuReader();
            //Case 77642: Changes are related to migrate DFU Functionality from BHIConfig to BHIContent
            var dfuMarketList = (XGlobals.Partner.PartnerId == XGlobals.Partner.BrandPartnerID) ? marketDfuReader.GetDfuMarketsForBrandPartner(XGlobals.Partner.BrandPartnerID) : null;

            // Special offers link located at map's right side
            if (_market.TotalCommsWithPromos > 0)
            {
                lnkSpecialOffer.Text = string.Format(this.lnkSpecialOffer.Text, _marketName, _state, builderName);
                //this.lnkSpecialOffer.Function = Pages.CommunityResults;
                //lnkSpecialOffer.Parameters.Add(new Param(NhsLinkParams.MarketID, _marketID.ToString(), UrlParamType.Friendly));
                //lnkSpecialOffer.Parameters.Add(new Param(NhsLinkParams.SpecialOfferComm, "1", UrlParamType.Friendly));
                //lnkSpecialOffer.Parameters.Add(new Param(NhsLinkParams.BrandID, _brand.BrandId.ToString(), UrlParamType.Friendly));
            }
            else
                lnkSpecialOffer.Visible = false;

            // Quick Move-In link located at map's right side
            if (_market.TotalQuickMoveInHomes > 0)
            {
                lnkQuickMoveIn.Text = string.Format(this.lnkQuickMoveIn.Text, _marketName, _state, builderName);

                var market = XMarketFactory.CreateMarket(_marketID);

                resultsFriendlyUrl = string.Format(@"/{0}/{1}/{2}-area/brandid-{3}", Pages.HomeResults,
                                    market.StateName.RemoveSpecialCharacters(), market.MarketName.RemoveSpecialCharacters().Replace(" ", "-"),
                                    _brand.BrandId.ToString());

                var paramList = new List<RouteParam>{
                    new RouteParam(RouteParams.Market, _marketID, RouteParamType.Friendly),
                    new RouteParam(RouteParams.HomeStatus, ((int)HomeStatusType.QuickMoveIn), RouteParamType.Friendly),
                    new RouteParam(RouteParams.BrandId, _brand.BrandId, RouteParamType.Friendly)};

                resultsOldUrl = paramList.ToUrl(Pages.HomeResults);

                //Case 77642: Changes are related to migrate DFU Functionality from BHIConfig to BHIContent
                if (dfuMarketList != null && dfuMarketList.MarketList.Exists(item => item.MarketId == _marketID))  
                    lnkQuickMoveIn.NavigateUrl = (resultsFriendlyUrl + string.Format("/homestatus-{0}", ((int)HomeStatusType.QuickMoveIn).ToString())).ToLower();
                else
                    lnkQuickMoveIn.NavigateUrl = (resultsOldUrl + string.Format("/homestatus-{0}", ((int)HomeStatusType.QuickMoveIn).ToString())).ToLower();                
            }
            else
                lnkQuickMoveIn.Visible = false;

            // view all link in the boutique links section
            lnkViewAll.Text = string.Format(lnkViewAll.Text, _marketName, _stateName);
            lnkViewAll.Function = Pages.NewHomes;
            param = new RouteParam(RouteParams.State, _stateName);
            lnkViewAll.Parameters.Add(param);
            param = new RouteParam(RouteParams.Area, _marketName);
            lnkViewAll.Parameters.Add(param);


            //Case 77642: Changes are related to migrate DFU Functionality from BHIConfig to BHIContent
            if (dfuMarketList != null && dfuMarketList.MarketList.Exists(item => item.MarketId == _marketID))  
            {
                lnkSearchOnMap.NavigateUrl = resultsFriendlyUrl.Replace(Pages.HomeResults, Pages.CommunityResults).ToLower();
            }
            else
            {
                var paramList = new List<RouteParam> { new RouteParam(RouteParams.Market, _marketID),
                                                       new RouteParam(RouteParams.BrandId, _brand.BrandId)};
                lnkSearchOnMap.NavigateUrl = paramList.ToUrl(Pages.CommunityResults);
            }

            // link in the bottom of the sportlight section
            lnkViewNewHomesSpotLight.Text = string.Format(lnkViewNewHomesSpotLight.Text, _brand.BrandName, _marketName, _stateName);
            lnkViewNewHomesSpotLight.Function = Pages.CommunityResults;
            param = new RouteParam(RouteParams.MarketId, _marketID);
            lnkViewNewHomesSpotLight.Parameters.Add(param);
            param = new RouteParam(RouteParams.BrandId, _brand.BrandId);
            lnkViewNewHomesSpotLight.Parameters.Add(param);

            // Boutique links
            // Get market information about amenity counts
            var mktHelper = new XMarketHelper();
            MarketInfo stateInfo = mktHelper.GetMarketInfo(_marketID, Nhs.Library.Common.Configuration.PartnerId);

            // show the boutique links sections if the market has these kind of comms
            ltrBoutiqueIndexDiv.Visible = stateInfo.ContainsCondoTownCommunities ||
                                               stateInfo.ContainsWaterFrontCommunities ||
                                               stateInfo.ContainsGolfCommunities;

            if (ltrBoutiqueIndexDiv.Visible)
            {
                // Golf amenities link
                pSectionGolfCourse.Visible =  stateInfo.ContainsGolfCommunities;
                lnkGolfCourse.Function = Pages.AmenityMarketGolf;
                lnkGolfCourse.Parameters.Add(new RouteParam(RouteParams.State, _stateName));
                lnkGolfCourse.Parameters.Add(new RouteParam(RouteParams.Area, _marketName));
                lnkGolfCourse.Parameters.Add(new RouteParam(RouteParams.AmenityType, SEOAmenityType.MarketGolfCourse));

                // Water front amenities link
                pSectionWaterfront.Visible = stateInfo.ContainsWaterFrontCommunities;
                lnkWaterfront.Function = Pages.AmenityMarketWaterfront;
                lnkWaterfront.Parameters.Add(new RouteParam(RouteParams.State, _stateName));
                lnkWaterfront.Parameters.Add(new RouteParam(RouteParams.Area, _marketName));
                lnkWaterfront.Parameters.Add(new RouteParam(RouteParams.AmenityType, SEOAmenityType.MarketWaterfront));

                // Condo townhome amenities link
                pSectionCondoTown.Visible = stateInfo.ContainsCondoTownCommunities;
                lnkCondoTown.Function = Pages.AmenityMarketCondo;
                lnkCondoTown.Parameters.Add(new RouteParam(RouteParams.State, _stateName));
                lnkCondoTown.Parameters.Add(new RouteParam(RouteParams.Area, _marketName));
                lnkCondoTown.Parameters.Add(new RouteParam(RouteParams.AmenityType, SEOAmenityType.MarketCondoTown));
            }

        }

        protected void btnCommResultsJump_Click(object sender, CommandEventArgs e)
        {
            string commandName = e.CommandName;
            var targetUrl = new NhsUrl(Pages.CommunityResults);
            targetUrl.AddParameter(UrlConst.MarketID, _marketID.ToString(), RouteParamType.Friendly);
            targetUrl.AddParameter(UrlConst.BrandID, _brand.BrandId.ToString(), RouteParamType.Friendly);

            switch (commandName.ToLower())
            {
                case "hothomes":
                    UserSession.PropertySearchParameters.SpecialOfferComm = 1;
                    UserSession.PropertySearchParameters.HomeStatus = 0;
                    targetUrl.Redirect();
                    break;
                case "quickmovein":
                    UserSession.PropertySearchParameters.SpecialOfferComm = 0;
                    UserSession.PropertySearchParameters.HomeStatus = (int)HomeStatusType.QuickMoveIn;
                    targetUrl.Redirect();
                    break;
            }
        }

        /// <summary>
        /// Load the articles section for the current market
        /// </summary>
        private void LoadArticles()
        {
            string articlePath1 = _marketName.Replace(" ", string.Empty).ToString() + ARTICLES_LEFT1_FILE_SUFIX;
            string articlePath2 = _marketName.Replace(" ", string.Empty).ToString() + ARTICLES_LEFT2_FILE_SUFIX;

            //Load right top article
            if (File.Exists(Server.MapPath(_STATIC_CONTENT_FOLDER + SiteIndexConst.ArticleFolder + articlePath1)))
                this.scTopRightArticle.UrlPath = SiteIndexConst.ArticleFolder + articlePath1;
            else
                this.scTopRightArticle.UrlPath = SiteIndexConst.ArticleFolder + ARTICLES_LEFT1_DEFAULT_FILE_SUFIX;

            //Load right bottom article 
            if (File.Exists(Server.MapPath(_STATIC_CONTENT_FOLDER + SiteIndexConst.ArticleFolder + articlePath2)))
                this.scBottomRightArticle.UrlPath = SiteIndexConst.ArticleFolder + articlePath2;
            else
                this.scBottomRightArticle.UrlPath = SiteIndexConst.ArticleFolder + ARTICLES_LEFT2_DEFAULT_FILE_SUFIX;
        }

        /// <summary>
        /// Load the html from the market links file's path
        /// </summary>
        private void LoadMoreInfoLinks()
        {
            // check if the hmlt file exist
            if (this.HasLinks())
            {
                this.plhMoreInfo.Visible = true;
                this.scInfo.UrlPath = GetLinksUrlPath();
                // replace the html tags ([])
                this.scMoreInfoHeading.AddReplaceTag(ContentTagKey.MarketName, _marketName);
                this.scMoreInfoHeading.AddReplaceTag(ContentTagKey.StateName, _stateName);
            }
            else
            {
                // hide the section
                this.plhMoreInfo.Visible = false;
            }
        }

        /// <summary>
        /// Populate the prices dropdown lists for search purpo
        /// </summary>
        private void BindPricesRangeDropDowns()
        {
            // add prices values..
            ddlPriceFrom.Items.AddRange(XGlobals.GetCommonListItems(CommonListItem.MinPrice));
            ddlPriceTo.Items.AddRange(XGlobals.GetCommonListItems(CommonListItem.MaxPrice));

            //Add defaults
            ddlPriceFrom.Items.Insert(0, new ListItem(DropDownDefaults.MinPrice, string.Empty));
            ddlPriceTo.Items.Insert(0, new ListItem(DropDownDefaults.MaxPrice, string.Empty));

            // select the last searched price range from Personal cookie
            ddlPriceFrom.SelectedIndex = ddlPriceFrom.Items.IndexOf(ddlPriceFrom.Items.FindByValue(UserSession.PersonalCookie.PriceLow.ToString()));
            ddlPriceTo.SelectedIndex = ddlPriceTo.Items.IndexOf(ddlPriceTo.Items.FindByValue(UserSession.PersonalCookie.PriceHigh.ToString()));
        }

        /// <summary>
        /// Populate the cities dropdown list
        /// </summary>
        /// <param name="marketId">Market id to retrieve the cities list</param>
        private void BindCityDropDown()
        {
            ReadOnlyCollection<XCity> cities;

            if (_marketID != 0 && _brandId != 0)
            {
                cities = _market.GetMarketCitiesByBrand(_brandId);
                DataBindHelper.BindDropDown(_market.GetMarketCitiesByBrand(_brandId), ddlCities, "Name", "Name");
            }

            ddlCities.Items.Insert(0, new ListItem(DropDownDefaults.AllCities, string.Empty));

            // if there arenot cities with active comms disable the search
            if (ddlCities.Items.Count == 1)
                btnSearchHomes.Enabled = false;
        }

        /// <summary>
        /// Retrieve cities data for the current market to bind the cities list
        /// </summary>
        /// <param name="marketId">Market id to retriece the cities list</param>
        private void BindCityRepeater()
        {
            ReadOnlyCollection<XCity> cities = _market.GetMarketCitiesByBrand(_brandId);

            if (_marketID != 0)
            {
                ResetListVars(cities.Count);
                this.rptCityNewHomes.DataSource = cities;
                this.rptCityNewHomes.DataBind();
            }
        }

        
        /// <summary>
        /// Check if the current market have links files.
        /// </summary>
        /// <returns>True if have a html file with links information, rather false</returns>
        private bool HasLinks()
        {
            string linksPath = _marketName.Replace(" ", "").ToString() + LINKS_FILE_SUFIX;
            if (File.Exists(Server.MapPath(_STATIC_CONTENT_FOLDER + SiteIndexConst.LinksFolder + linksPath)))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Get the html file's path for the links section 
        /// </summary>
        /// <returns>HTML file's Path for the links section</returns>
        private string GetLinksUrlPath()
        {
            string linksPath = _marketName.Replace(" ", "").ToString() + LINKS_FILE_SUFIX;
            return SiteIndexConst.LinksFolder + linksPath;
        }

        /// <summary>
        /// Inicialize the global variables, used to retrive the page data.
        /// </summary>
        private void SetUpPageParameters()
        {
            //Get Market
            _marketName = NhsUrl.GetMarketName;

            //Get State
            _stateName = NhsUrl.GetState;

            // Get the state abbreviation
            _state = XGlobals.GetStateAbbreviation(_stateName);

            // Get Brand information
            _brandId = NhsUrl.GetBrandId;


            //Get market 
            _market = XGlobals.Partner.GetMarket(_marketName, _stateName);
            
            _brand = new XBrand(_brandId);
            _brand.DataBind();

            // Get Market and Brand info
            _marketID = _market.MarketId;
        }

        /// <summary>
        /// Format the static content controls in the page with the required params
        /// </summary>
        private void ReplaceContentTags()
        {
            // To prevent displaying home twice for some builders
            string brandName = _brand.BrandName;
            string builderName = _brand.BrandName;

            if (!StringHelper.EndsWithHomeWord(_brand.BrandName)) 
            {
                brandName += " Homes";
                builderName += " homes";
            }

            string stateId = XGlobals.GetStateID(_stateName);
            this.scMarketBuilderPageHeader.AddReplaceTag(ContentTagKey.MarketName, _marketName);
            this.scMarketBuilderPageHeader.AddReplaceTag(ContentTagKey.BrandName, brandName);
            this.scMarketBuilderPageHeader.AddReplaceTag(ContentTagKey.BuilderName, builderName);
            this.scMarketBuilderPageHeader.AddReplaceTag(ContentTagKey.StateID, stateId);
            this.scMarketBuilderPageHeader.AddReplaceTag(ContentTagKey.StateName, _stateName);

            this.scMarketBuilderPageHeaderSEO.AddReplaceTag(ContentTagKey.MarketName, _marketName);
            this.scMarketBuilderPageHeaderSEO.AddReplaceTag(ContentTagKey.BrandName, brandName);
            this.scMarketBuilderPageHeaderSEO.AddReplaceTag(ContentTagKey.BuilderName, builderName);
            this.scMarketBuilderPageHeaderSEO.AddReplaceTag(ContentTagKey.StateID, stateId);
            this.scMarketBuilderPageHeaderSEO.AddReplaceTag(ContentTagKey.StateName, _stateName);

            this.scTipHeader.AddReplaceTag(ContentTagKey.MarketName, _marketName);
            this.scSearchForNewHomes.AddReplaceTag(ContentTagKey.MarketName, _marketName);
            this.scTipHeader.AddReplaceTag(ContentTagKey.StateID,stateId);
            this.scSearchForNewHomes.AddReplaceTag(ContentTagKey.StateID, XGlobals.GetStateID(_stateName));
            this.scSearchForNewHomes.AddReplaceTag(ContentTagKey.BuilderName, builderName);
            this.scBoutiqueLinksHeader.AddReplaceTag(ContentTagKey.MarketName, _marketName);
            this.scSpotlightCommsHeader.AddReplaceTag(ContentTagKey.MarketName, _marketName);
            this.scSpotlightCommsHeader.AddReplaceTag(ContentTagKey.StateID, _state);


            //Update for SEO content
            this.scNhsHeading.AddTag(ContentTagKey.BrandName, brandName);
            this.scNhsHeading.AddTag(ContentTagKey.MarketName, _marketName);
            this.scNhsHeading.AddTag(ContentTagKey.StateID, _state);


            var icon = Nhs.Library.Common.Configuration.ResourceDomainPublic + "/globalresources14/default/images/icons/" +
            (Nhs.Mvc.Routing.Interface.NhsRoute.IsBrandPartnerNhs
            ? "map_plus_blue.png"
            : "map_plus_green.png");

            var drow = XGlobals.GetStateMapLocations().AsEnumerable().FirstOrDefault(row => row["statename"].ToString().ToUpper() == _stateName.ToUpper());
            if (drow != null)
                imgMap.Src = GoogleHelperMaps.StaticMapUrl(string.Format("{0},{1}", drow["StateLat"], drow["StateLong"]), "170x136", true, 5, icon, "statebrand");
            else
                imgMap.Src = string.Format(imgMap.Src, _marketID);


            imgMap.Alt = string.Format(imgMap.Alt, _marketName);
            
            // Adding parameters to ad controls.
            AdController.AddStateParameter(stateId);
            AdController.AddMarketParameter(_marketID);


        }

        private List<ContentTag> SetContentManager()
        {
            
            var contentTags = new List<ContentTag>();

            // Replace tags
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.MarketId, TagValue = _marketID.ToString() });
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.MarketName, TagValue = _marketName });                        
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.StateID, TagValue = _state });
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.StateName, TagValue = _stateName });
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.BrandID, TagValue = _brandId.ToString() });
            contentTags.Add(new ContentTag { TagKey = ContentTagKey.BrandName, TagValue = _brand.BrandName });            
            
            return contentTags;
        }

        /// <summary>
        /// Add meta tags information to the page, like tittle, desc and meta keywords
        /// </summary>
        /// <param name="market">Market to get the cities list</param>
        private void LoadMeta()
        {
            // Get the cities list 
            string metaCities = string.Empty;

            foreach (XCity city in _market.Cities)
            {
                metaCities += " " + city.Name;
            }

            // register the meta tag information
            if (XGlobals.Partner.PartnerId == PartnersConst.NewHomeSource.ToType<int>() ||
                XGlobals.Partner.PartnerId == PartnersConst.Move.ToType<int>())
            {                
                base.OverrideDefaultMeta(SetContentManager(),SeoTemplateType.NHS_Market_Brand);
            }
            else
            {
                MetaRegistrar metaRegistrar = new MetaRegistrar(this.Page);
                metaRegistrar.NHSAddMetaTagsForSEOMarketBuilder(Pages.MarketBuilder, _market.MarketName, _market.State,
                                                                _brand.BrandName);
            }
        }

        /// <summary>
        /// Distribute the columns in the repeater list.
        /// </summary>
        /// <param name="e">Repeater item to retrieve the controls to format</param>
        private void CreateList(RepeaterItemEventArgs e)
        {
            int itemsPerColumn = (int)Math.Ceiling((double)(_rowCount / _NUM_OF_COLS));
            Literal ltrOpen = (Literal)e.Item.FindControl("ltrOpen");
            ltrOpen.Visible = false;
            Literal ltrClose = (Literal)e.Item.FindControl("ltrClose");
            ltrClose.Visible = false;

            if (_rowNum == 1)
            {
                ltrOpen.Visible = true;
                ltrOpen.Text = "<ul class=\"nhsIndexList nhsIndexList" + _columnNum.ToString() + "\">";
            }

            if (_rowNum == itemsPerColumn || (_columnNum == _NUM_OF_COLS && _rowNum == _rowCount % itemsPerColumn))
            {
                _rowNum = 0;
                _columnNum++;
                ltrClose.Text = "</ul>";
                ltrClose.Visible = true;
            }
            _rowNum++;
        }

        /// <summary>
        /// Inicialize the counters variables for the 3 columns list 
        /// </summary>
        /// <param name="itemCount">Total number of list elements</param>
        private void ResetListVars(int itemCount)
        {
            _rowNum = 1;
            _columnNum = 1;
            _rowCount = itemCount;
        }

        #endregion

        #region Events

        /// <summary>
        /// Search homes button, it redirect to the comm results with the provided params.
        /// </summary>
        protected void btnFindHomes_OnClick(object sender, EventArgs e)
        {
            List<NhsUrlParam> friendlyUrlList = new List<NhsUrlParam>();

            // if not active cities for this market.
            if (ddlCities.Items.Count == 1)
                this.litErrorMessage.Visible = true;
            else
            {

                // change the city param name if there isnt a selected city 
                if (ddlCities.SelectedIndex > 0)
                {
                    friendlyUrlList.Add(
                        new NhsUrlParam(UrlConst.CityNameFilter, this.ddlCities.SelectedValue, RouteParamType.Friendly));
                }

                friendlyUrlList.Add(new NhsUrlParam(UrlConst.MarketID, this._marketID.ToString(), RouteParamType.Friendly));
                friendlyUrlList.Add(new NhsUrlParam(UrlConst.BrandId, this._brandId.ToString(), RouteParamType.Friendly));

                NhsUrlParam[] friendlyUrlParameters = new NhsUrlParam[friendlyUrlList.Count];
                friendlyUrlList.CopyTo(friendlyUrlParameters);

                // redirect to comms results
                NhsUrl friendlyUrl = new NhsUrl(Pages.CommunityResults, friendlyUrlParameters);
                friendlyUrl.Redirect();
            }
        }

        /// <summary>
        /// Format the new homes builder section in a 3 columns list 
        /// </summary>
        protected void rptCityNewHomes_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                // Distribute the columns in the list
                CreateList(e);

                // format the link with the correct text and url params
                var city = (XCity)e.Item.DataItem;
                var lnkCity = (HyperLink)e.Item.FindControl("lnkCity");
                lnkCity.Text = string.Format(lnkCity.Text, city.Name);


                string resultsUrl;

                //if (Configuration.MarketsListforDFU.Contains(_marketID) && XGlobals.Partner.PartnerId == XGlobals.Partner.BrandPartnerID) // not a private label site
                //Case 77642: Changes are related to migrate DFU Functionality from BHIConfig to BHIContent
                var marketDfuReader = new MarketDfuReader();
                var dfuMarketList = (XGlobals.Partner.PartnerId == XGlobals.Partner.BrandPartnerID) ? marketDfuReader.GetDfuMarketsForBrandPartner(XGlobals.Partner.BrandPartnerID) : null;
                if (dfuMarketList != null && dfuMarketList.MarketList.Exists(item => item.MarketId == _marketID))  
                {
                    var market = XMarketFactory.CreateMarket(_marketID);

                    resultsUrl = string.Format(@"/{0}/{1}/{2}-area/{3}/brandid-{4}",
                                        Pages.CommunityResults,
                                        market.StateName.RemoveSpecialCharacters(),
                                        market.MarketName.RemoveSpecialCharacters().Replace(" ", "-"),
                                        city.Name.RemoveSpecialCharacters(),
                                        _brand.BrandId.ToString());
                }
                else
                {
                    var paramList = new List<RouteParam> {new RouteParam(RouteParams.Market, _marketID),
                                                          new RouteParam(RouteParams.City, city.Name),
                                                           new RouteParam(RouteParams.BrandId, _brand.BrandId)};

                    resultsUrl = paramList.ToUrl(Pages.CommunityResults);

                }

                lnkCity.NavigateUrl = resultsUrl.ToLower();
            }
        }

       #endregion

    }
}
