<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Spotlight_communities.ascx.cs" Inherits="Nhs.Web.Controls.Default.Common.Spotlight_communities" EnableViewState="false" %>

<asp:Repeater ID="repSpotlightCommunities" Runat="server" OnItemDataBound="repSpotlightCommunities_ItemDataBound">
<ItemTemplate>
    <div class="nhsSpotlightItem">
	    <div class="nhsSpotCommImg">
            <nhs:Link ID="lnkImageUrl" runat="server"></nhs:Link>
	    </div>
	    <p>
	        <asp:PlaceHolder ID="plhVideoLink" runat="server">
	            <nhs:Link ID="lnkCommVideo" rel="nofollow" Target="_blank" runat="server"><asp:Image ID="imgCommVideo" ImageUrl="[resource:]/globalresourcesmvc/default/images/icons/icon_video.png" AlternateText="Video" runat="server" /></nhs:Link><br />
	        </asp:PlaceHolder>	        
	        <strong><nhs:Link ID="lnkCommName" runat="server" PersistParams="false" /></strong><br />
	        <asp:Literal ID="lblBrandName" runat="server" /><br />				    
	        <asp:Literal ID="lblCity" runat="server" />, <asp:Literal ID="lblState" runat="server" /><br />
	        <asp:Literal ID="lblPrice" runat="server" />	        
	    </p>
    </div>
</ItemTemplate>
</asp:Repeater>    
