<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="partner_navigation_menu.ascx.cs" Inherits="Nhs.Web.Controls.Default.Common.partner_navigation_menu" %>
<div id="nhsPartnerNav">
    <ul>
	<asp:PlaceHolder ID="plhBasicSearchLink" Runat="server">
		<li><nhs:Link ID="lnkBasicSearch" Text="Search for new homes" runat="server" Function="basicsearch"></nhs:Link></li>
	</asp:PlaceHolder>
	<asp:PlaceHolder ID="plhNewHomeGuidLink" Runat="server" Visible="false">
        <li><nhs:Link ID="lnkHomeGuide" runat="server" Function="homeguide" Text="New home guide"></nhs:Link></li>
	</asp:PlaceHolder>
	</ul>
	<ul id="nhsPartnerToolNav">
    <asp:PlaceHolder ID="plhSignInLink" runat="server" Visible="false">
        <li><asp:HyperLink ID="lnkSignIn" runat="server" Rel="nofollow" Text="Sign In" Function="Login" /></li>
        <li><asp:HyperLink ID="lnkRegister" runat="server" Text="Create Account"/></li>
    </asp:PlaceHolder>						
    <asp:PlaceHolder ID="plhSignOutLink" runat="server" Visible="false">
        <li><nhs:Link ID="lnkSignOut" runat="server" Function="logoff" Text="Sign out"></nhs:Link></li>
    </asp:PlaceHolder>
	</ul>
</div>
