/*
 * ========================================================
 * Author: Jason Pierce
 * Date: 04/19/2008
 * Purpose:
 * Edit History (please mention edits succinctly):
 * =========================================================
 */

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Web;

using Nhs.Library.Web;
using Nhs.Library.Enums;
using Nhs.Library.Constants;
using Nhs.Library.Common;

namespace Nhs.Web.Controls.Default.Common
{
    public partial class ajax_tabs : UserControlBase
    {
        #region Constants

        private const string JavaScriptTag = "javascript:";

        #endregion

        #region Fields

        private BulletedListDisplayMode _displayMode;
        private Dictionary<string, string> _tabs = new Dictionary<string, string>();

        #endregion

        #region Properties

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
        NotifyParentProperty(true),
        PersistenceMode(PersistenceMode.Attribute),
        Description("Set DisplayMode of underlying BulletedList")]
        public BulletedListDisplayMode DisplayMode
        {
            get
            {
                return this._displayMode;
            }
            set
            {
                this._displayMode = value;
            }
        }

        public Dictionary<string, string> Tabs
        {
            get
            {
                return this._tabs;
            }
            set
            {
                this._tabs = value;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Renders the tabs contained within the Tabs property.
        /// </summary>
        /// <param name="tabControl">The Tab Control to render.</param>
        protected void RenderTabs(NhsBulletedList tabControl)
        {
            // Set defaults
            tabControl.DataTextField = "key";
            tabControl.DataValueField = "value";
            tabControl.DisplayMode = this.DisplayMode;

            // Render Tabs
            tabControl.DataSource = this.Tabs;
            tabControl.DataBind();

            // Ajaxify: supports use of tabs by SEO Bots and non-ajax enabled browsers
            string format = JavaScriptTag + "__doPostBack('" + this.UniqueID + "$" + tabControl.ID + "','{0}');return false;";
            for (int itemIndex = 0; itemIndex < tabControl.Items.Count; itemIndex++)
            {
                tabControl.Items[itemIndex].Attributes.Add("onclick", String.Format(format, itemIndex));
            }
        }

        /// <summary>
        /// Generates a URL for the specified Tab.
        /// </summary>
        /// <param name="urlParm">The URL parameter.</param>
        /// <param name="tabName">Name of the Tab.</param>
        /// <returns></returns>
        protected string GetTabUrl(string urlParm, string tabName)
        {
            // Declare vars
            NhsUrl href = NhsUrlHelper.GetFriendlyUrl();

           // Modify/Add Tab Parm
            href.Parameters.Add( new NhsUrlParam(urlParm, tabName));
            
            // Return URL
            return ("~/" + href.ToString());
        }

        #endregion
    }
}
