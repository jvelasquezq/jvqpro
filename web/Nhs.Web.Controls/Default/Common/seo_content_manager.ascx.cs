using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.UrlRewriter.Configuration;
using Nhs.Utility.Common;

namespace Nhs.Web.Controls.Default.Common
{
    public partial class seo_content_manager : UserControlBase
    {
        #region Members
        private Hashtable _contentTags = new Hashtable();
        #endregion

        #region Properties
        public string City { get; set; }
        public string Market { get; set; }
        public string State { get; set; }
        public string BrandId { get; set; }
        public string BrandName { get; set; }
        public string County { get; set; }
        public string CodeBlock { get; set; }
        public string TemplateType { get; set; }        
        #endregion

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(CodeBlock) || string.IsNullOrEmpty(TemplateType))
            {
                Literal error = new Literal();
                error.Text = @"There's been an error with the seo_content_manager, either CodeBlock or TemplateType are missing for an instance of this class.";
                this.Controls.Add(error);
            }
            else
            {
                string output = GetXmlBlock();

                var partnerSiteUrl = RewriterConfiguration.CurrentPartnerSiteUrl;

                if (!string.IsNullOrEmpty(partnerSiteUrl))
                    partnerSiteUrl = partnerSiteUrl + "/";

                AddTag(ContentTagKey.PartnerSiteUrl, partnerSiteUrl);

                output = ReplaceTags(output);
                HtmlMeta meta;
                string seoInfo = string.Format("SEO Content: {0} -> {1}", TemplateType, CodeBlock);

                string staticContent = System.Web.HttpContext.Current.Request.QueryString["showcontentareas"] as string;
                bool showstatic = false;
                if (staticContent != null)
                {
                    bool.TryParse(staticContent, out showstatic);
                    UserSession.ShowStaticOverlay = showstatic;
                }
                else
                    showstatic = UserSession.ShowStaticOverlay;

                if (showstatic)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<div class=\"nhsStaticContent\">");
                    sb.Append(string.Format("<a class=\"stmark\"><b class=\"stNumber\" title=\"{0}\" ></b></a>", seoInfo));
                    sb.Append(output);
                    sb.Append("</div>");
                    output = sb.ToString();
                }

                switch (CodeBlock.ToLower())
                {
                    case "title":
                        this.Page.Header.Title = output;
                        break;
                    case "meta-description":
                        if (this.Page.Header.FindControl("Description") != null)
                            this.Page.Header.Controls.Remove(this.Page.Header.FindControl("Description"));
                        meta = new HtmlMeta();
                        meta.Name = "Description";
                        meta.Content = output;
                        this.Page.Header.Controls.Add(meta);
                        break;
                    case "meta-keywords":
                        if (this.Page.Header.FindControl("Keywords") != null)
                            this.Page.Header.Controls.Remove(this.Page.Header.FindControl("Keywords"));
                        meta = new HtmlMeta();
                        meta.Name = "Keywords";
                        meta.Content = output;
                        this.Page.Header.Controls.Add(meta);
                        break;
                    case "meta-robots":
                        if (this.Page.Header.FindControl("Robots") != null)
                            this.Page.Header.Controls.Remove(this.Page.Header.FindControl("Robots"));
                        meta = new HtmlMeta();
                        meta.Name = "Robots";
                        meta.Content = output;
                        this.Page.Header.Controls.Add(meta);
                        break;
                    default:
                        Literal chunk = new Literal();
                        chunk.Text = @output;
                        this.Controls.Add(chunk);
                        break;
                }
            }
        }

        /// <summary>
        /// Registers a Tag in the Hashtable for further replacement
        /// </summary>
        /// <param name="tagName"></param>
        /// <param name="tagValue"></param>
        public void AddTag(ContentTagKey tagName, string tagValue)
        {
            if (!_contentTags.Contains(tagName))
                _contentTags.Add(tagName, tagValue);
            else
                _contentTags[tagName] = tagValue;
        }
        public void RemoveTag(ContentTagKey tagName)
        {
            _contentTags.Remove(tagName);
        }

        private string GetXmlBlock()
        {
            XmlDocument doc = new XmlDocument();
            string xmlPath;

            var partnerSiteUrl = NhsRoute.PartnerSiteUrl;
            if (!string.IsNullOrEmpty(partnerSiteUrl))
            {
                xmlPath = GetXmlPath(false, partnerSiteUrl); //try partner-specific custom file first

                if (!File.Exists(xmlPath))//partner-specific custom not found, try partner-specific default 
                    xmlPath = GetXmlPath(true, partnerSiteUrl);

                if (!File.Exists(xmlPath))//partner-specific default not found, try brand custom 
                    xmlPath = GetXmlPath(false, string.Empty);

                if (!File.Exists(xmlPath))//default custom not found, try brand default 
                    xmlPath = GetXmlPath(true, string.Empty);
            }
            else
            {
                xmlPath = GetXmlPath(false, string.Empty); //brand default
            }

            try
            {
                doc.Load(xmlPath);
                XmlNode root = doc.DocumentElement;
                if (root.SelectSingleNode(CodeBlock) != null && !string.IsNullOrEmpty(root.SelectSingleNode(CodeBlock).ChildNodes[0].Value.Trim()))
                    return root.SelectSingleNode(CodeBlock).ChildNodes[0].Value;

                return GetXmlBlockFromDefault();
            }
            catch (Exception)
            {
                // file doesn't exist for that market, try start with or open default.
                if (!File.Exists(xmlPath))
                {
                    if (NhsRoute.IsPartnerMove ||
                       NhsRoute.IsPartnerNhs)
                    {
                        var pathUsingStartWith = GetXmlPathUsingStartsWith(doc, xmlPath, CodeBlock);
                        if (!string.IsNullOrEmpty(pathUsingStartWith))
                            return pathUsingStartWith;
                    }

                    //xmlPath = GetXmlPath(seoTemplateType, false, string.Empty, false); //brand default
                    
                    return GetXmlBlockFromDefault();
                }
                return GetXmlBlockFromDefault();
            }
        }

        private string GetXmlBlockFromDefault()
        {
            try
            {
                var doc = new XmlDocument();                
                doc.Load(GetXmlPath(true, string.Empty));
                XmlNode root = doc.DocumentElement;
                return root.SelectSingleNode(CodeBlock).ChildNodes[0].Value;
            }
            catch (Exception)
            {                
                return "Failed to Load XML File.";
            }
        }
        
        private string GetXmlPath(bool useDefaultTemplate, string partnerSiteUrl)
        {
            string fullpath;

            switch (TemplateType.ToLower())
            {
                case "seo_market":
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/" + TemplateType + @"/Default_SEOMarketContentTemplate";
                    else
                        fullpath = "SEOContent/" + TemplateType + @"/" + Market + @"_SEOMarketContentTemplate";
                    break;
                case "nhs_market_srp":
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/" + TemplateType + @"/Default_CommResults-MarketContentTemplate";
                    else
                        fullpath = "SEOContent/" + TemplateType + @"/" + Market + @"_CommResults-MarketContentTemplate";
                    break;
                case "nhs_market_city_srp":
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/" + TemplateType + @"/Default_CommResults-MarketCityContentTemplate";
                    else
                        fullpath = "SEOContent/" + TemplateType + @"/" + Market + "-" + City + @"_CommResults-MarketCityContentTemplate";
                    break;
                case "nhs_market_brand":
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/" + TemplateType + @"/Default_Template";
                    else
                       fullpath = "SEOContent/" + TemplateType + @"/" + this.State + "_" + this.Market + "_" + this.BrandId + "_" + this.BrandName.Replace(" ","_").Replace("-","_") + @"_Template";                    
                    break;
                case "nhs_county_srp":
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/" + TemplateType + @"/Default_CommResults-CountyContentTemplate";
                    else
                        fullpath = "SEOContent/" + TemplateType + @"/" + State + "-" + County + @"_CommResults-CountyContentTemplate";
                    break;
                case "nhs_zip_srp":
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/" + TemplateType + @"/Default_CommResults-ZipContentTemplate";
                    else
                        fullpath = "SEOContent/" + TemplateType + @"/" + Market + @"_CommResults-ZipContentTemplate";
                    break;
                case "nhs_site_index":
                    if (useDefaultTemplate)
                        fullpath = "SEOContent/SiteIndex/Default";
                    else
                        fullpath = "SEOContent/SiteIndex/" + XGlobals.GetStateAbbreviation(State);
                    break;
                case "nhs_quickconnect_srp":
                    fullpath = "SEOContent/" + TemplateType + @"/Default_QuickResultsContentTemplate";
                    break;
                case "survey_modal_content":
                    fullpath = "SEOContent/" + TemplateType + @"/SurveyModalContent";
                    break;
                default:
                    fullpath = "SEOContent/" + TemplateType + @"/Default";
                    break;
            }

            fullpath = !string.IsNullOrEmpty(partnerSiteUrl) ? partnerSiteUrl + @"/" + fullpath + ".xml" : fullpath + ".xml";

            return Server.MapPath(@"~/" + Configuration.StaticContentFolder + @"/" + fullpath);
        }

        private string GetXmlPathUsingStartsWith(XmlDocument doc, string xmlPath, string htmlBlockName)
        {
            var myDir = new DirectoryInfo(xmlPath);
            var onlyDir = myDir.FullName.Replace(myDir.Name, "");
            myDir = new DirectoryInfo(onlyDir);
            var matchValue = this.State + "_" + this.Market + "_" + this.BrandId;
            var files = myDir.GetFiles(string.Format("{0}*", matchValue));

            if (files.Any())
            {
                var file = files[0].Name;
                try
                {
                    doc.Load(onlyDir + file);
                    XmlNode root = doc.DocumentElement;
                    if (root != null)
                        if (root.SelectSingleNode(htmlBlockName) != null && !string.IsNullOrEmpty(root.SelectSingleNode(htmlBlockName).ChildNodes[0].Value.Trim()))
                            return ReplaceTags(root.SelectSingleNode(htmlBlockName).ChildNodes[0].Value);
                }
                catch (Exception ex)
                {
                    ErrorLogger.LogError("SEO Content Read Error!", string.Format("Error reading XML File: {0}", xmlPath), "Ask SEO guy to fix XML file. Detailed stack trace in the following entry.");
                    ErrorLogger.LogError(ex);
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// Replaces the Tags from the Hashtable into the XML Chunk
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public string ReplaceTags(string input)
        {
            if (input.IndexOf('[') > -1)
            {
                foreach (ContentTagKey key in _contentTags.Keys)
                    input = input.Replace("[" + key + "]", string.IsNullOrEmpty(_contentTags[key].ToString()) ? "" : StringHelper.ToTitleCase(_contentTags[key].ToString()));
            }
            return input;
        }
    }
}
