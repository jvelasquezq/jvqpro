<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="email_footer.ascx.cs" Inherits="Nhs.Web.Controls.Default.Common.email_footer" %>
<%@ Import Namespace="Nhs.Library.Common" %>
<%@ Import Namespace="Nhs.Utility.Constants" %>

<%if (XGlobals.Partner.BrandPartnerID != (int)PartnersConst.Pro)
  {%>
  <p>
      To <strong>unsubscribe</strong> from all  further emails, <asp:HyperLink ID="hlnkUnsusbcribe" runat="server">click here.</asp:HyperLink>
  </p>
<p><strong>We value your privacy!</strong> You've received this message at this address 
    <asp:Label ID="lblUserEmail" runat="server"></asp:Label> because it was requested by you or another user on our website. 
    <asp:HyperLink ID="hlnkPrivacyPolicy" runat="server" style="color:#1C64BA;" >Privacy Policy</asp:HyperLink>
</p>
<% } %>
<p>Copyright &copy; <asp:Literal ID="litCurrentYear" runat="server" />. This email was sent by: Builders Digital Experience, LLC. 
11900 Ranch Road 620 N, Austin, TX 78750-1345 USA</p>
	    
