using System;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Library.Web;
using Nhs.Library.Common;
using Nhs.Mvc.Data.Repositories;
using Nhs.Utility.Constants;

namespace Nhs.Web.Controls.Default.Common
{
    public partial class email_header : UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var logo = XGlobals.Partner.PartnerLogo;
            imgHome.Src = NhsUrl.GetFunction == "brochure" && XGlobals.Partner.BrandPartnerID == Convert.ToInt32(PartnersConst.Pro) ? GetLogo() : logo;
        }

        private string GetLogo()
        {
            try
            {
                var requestId = NhsUrl.GetRequestID;
                var requestInfo = DataProvider.Current.GetRequest(requestId);

                var userProfile = new Profile
                {
                    LogonName = requestInfo.Rows[0]["email_address"].ToString(),
                    PartnerID = XGlobals.Partner.PartnerId
                };
                userProfile.DataBindByLogonName();
                var agentBrochure = DataProvider.Current.GetAgentBrochure(userProfile.UserID);
                if (agentBrochure.Rows.Count > 0)
                {
                    var agentPhoto = agentBrochure.Rows[0]["image_url"].ToString();
                    var agencyLogo = agentBrochure.Rows[0]["agency_logo"].ToString();

                    return !string.IsNullOrEmpty(agentPhoto)
                               ? string.Format("{0}/{1}", Configuration.IRSDomain, agentPhoto)
                               : !string.IsNullOrEmpty(agencyLogo)
                                     ? string.Format("{0}/{1}", Configuration.IRSDomain, agencyLogo)
                                     :  "";

                }
            }
            catch
            {
            }
            return "";
        }
    }
}