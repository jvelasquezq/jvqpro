<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="btnAddThis.ascx.cs"
    Inherits="Nhs.Web.Controls.Default.Common.btnAddThis" %>
<%--<li id="nhsbtnAddThis">--%>
<a href="http://www.addthis.com/bookmark.php?v=250&pub=newhomesource"
    onclick="return addthis_sendto()" onmouseout="addthis_close()" onmouseover="return addthis_open(this, '', '[URL]', '[TITLE]')">
    <img alt="Bookmark and Share" height="16" src="http://s7.addthis.com/static/btn/sm-share-en.gif"
        style="border: 0" width="83" />
</a>

<span class="addthis_separator">|</span>

<a class="addthis_button_facebook"></a>
<a class="addthis_button_twitter"></a>    
<a class="addthis_button_email"></a>
<a class="addthis_button_print"></a>

<script src="http://s7.addthis.com/js/250/addthis_widget.js?pub=newhomesource" type="text/javascript"></script>
<%--</li>--%>
