<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="personal_bar.ascx.cs" Inherits="Nhs.Web.Controls.Default.Common.personal_bar" %>
<div class="nhsPersonalBar" id="nhsPersonalBar"><div class="nhsPersonalBarInner">
    <asp:Literal ID="litSpotlightHomes" runat="server" Text='<h2 class="nhsHomeSpotTitle"><span>Spotlight</span> New Home Communities</h2>' Visible="false"></asp:Literal>
    <p>        
        <asp:Label ID="lblNoOfHomes" runat="server" Text="No"></asp:Label> 
        <nhs:Link ID="lnkRecentItems" Text="Save for your next visit." runat="server" Function="recentitems"></nhs:Link>
        <nhs:Link ID="lnkRecentItemsActiveUser" Text="Save now" runat="server" Function="recentitems"></nhs:Link>
        <span class="nhsSavedItems" id="spanSavedHome" runat="server">
            <asp:Literal ID="litSavedHome" runat="server" Visible="false">You have </asp:Literal><nhs:Link ID="lnkSavedHome" Text="" runat="server" Function="savedhomes"></nhs:Link>
        </span>
        <span id="divSuccessMessage" class="nhsPersonalMessage" style="display:none"></span>
        <asp:Label ID="lblSuccessMessage" CssClass="nhsPersonalMessage" runat="server" Visible="false"></asp:Label>
    </p>
    <hr class="nhsReaderAlt" />
    <div id="divQuickConnect" runat="server" visible="false" class="nhsGlobalQuickConnectDiv">
        <p>        
        <nhs:Link ID="lnkQuickConnect" Function="QuickConnect" runat="server"><asp:Image ID="imgQuickConnect" ImageUrl="[resource:]/globalresources/default/images/buttons/btn_quickconnect_mail.png" CssClass="nhsGlobalQuickConnectImg" runat="server" AlternateText="Quick Connect" /></nhs:Link>
        <nhs:Link ID="lnkQC" Function="QuickConnect" CssClass="nhsGlobalQuickConnectText" runat="server">Get Info FAST, try </nhs:Link>
        </p>
    </div>

</div></div>
