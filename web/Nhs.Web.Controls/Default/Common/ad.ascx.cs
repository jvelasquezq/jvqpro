using System;
using System.Web.UI;
using Nhs.Library.Web;

namespace Nhs.Web.Controls.Default.Common
{
    public partial class ad : UserControlBase
    {
        AdController _adController;
        private bool _useIFrame;
        private bool _javaScriptParams;
        
        public bool UseIFrame
        {
            get { return _useIFrame; }
            set { _useIFrame = value; }
        }

        public bool JavaScriptParams
        {
            get { return _javaScriptParams; }
            set { _javaScriptParams = value; }
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ad"/> class.
        /// <para>
        /// <example>
        /// <code>
        ///  <nhs:ad id="AdPositionName" runat="server"/>
        /// </code>
        /// </example>
        /// </para>
        /// <para>
        /// This control just renders AdController.
        /// AdController is set in multiple places(UserControlBase, ad and Control container for the ad control)
        /// in the current context
        /// 1. In UserControlBase exposes AdController property which is lazy loaded to Current HttpContext
        ///    1.1 AdController's AdPageName is set from FriendlyUrl obj
        ///    1.2 FriendlyUrl in the current context gets the AdPageName from MappingConfig.xml
        /// 
        /// 2.The UserControl (corresponding to the function in url thats being loaded into the default.aspx page)
        /// containing the ad control has the responsibility of adding the parameters to 
        /// the addcontroller's Parameters list
        /// 
        /// 3.In ad_Init current Id which is also the position of the ad is added to the AllPosition list of the AdController
        /// 4.Finally AdController is rendered in the overidden Render
        /// </para>
        /// </summary>
        public ad()
        {
            this.Init += new EventHandler(ad_Init);
        }


        /// <summary>
        /// Handles the Init event of the ad control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        void ad_Init(object sender, EventArgs e)
        {
            _adController = base.AdController;
            if (_javaScriptParams == false)
            {
                _adController.AllPositions.Add(this.ID);
            }
        }

        /// <summary>
        /// Sends server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter"></see> object, which writes the content to be rendered on the client.
        /// </summary>
        /// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"></see> object that receives the server control content.</param>
        protected override void Render(HtmlTextWriter writer)
        {
            //ads can be turned off partner site wide through config or per url using showads=false OR showads=0
            bool areAdsEnabled = Convert.ToBoolean(Nhs.Library.Common.Configuration.AdControlEnabled);
            if (!string.IsNullOrEmpty(NhsUrl.GetShowAds))
            {
                areAdsEnabled = areAdsEnabled & Convert.ToBoolean(NhsUrl.GetShowAds);
            }

            if (areAdsEnabled)
            {
                base.Render(writer);

                if (!_javaScriptParams)
                {
                    writer.Write("<div id=\"nhsAdContainer" + this.ID + "\">");
                    if (!_useIFrame)
                    {
                        writer.Write(_adController.Render(this.ID));
                    }
                    else
                    {
                        writer.Write(_adController.RenderIFrame(this.ID));
                    }
                    writer.Write("</div>");
                }
                else
                {
                    writer.Write(_adController.RenderJavaScriptParams());
                }

            }
        }




    }
}
