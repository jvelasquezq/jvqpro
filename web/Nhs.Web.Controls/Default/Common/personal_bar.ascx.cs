/*
 **NewHomeSource - http://www.NewHomeSource.com
 **Copyright (c) 2001-2006
 **by BuilderHomeSite Inc. ( http://www.BuilderHomeSite.com )
 * ========================================================
 * Author: MSharma
 * Date: 11/14/2006 12:42:06
 * Purpose:
 * Edit History (please mention edits succinctly):
 * =========================================================
 */
using System;
using System.Web;
using Nhs.Library.Web;
using Nhs.Library.Enums;
using Nhs.Library.Constants;
using Nhs.Library.Common;

namespace Nhs.Web.Controls.Default.Common
{
    public partial class personal_bar : UserControlBase
    {
   
        
        protected void Page_Load(object sender, EventArgs e)
        {
            var homecount = UserSession.UserProfile.Planner.RecentlyViewedHomes.Count;

            string pageFunction = NhsUrlHelper.GetFriendlyUrl().Function;
            if ((Configuration.PartnerId == 1 | Configuration.PartnerId == 333) & (pageFunction == string.Empty | pageFunction == Pages.Home))
            {
                litSpotlightHomes.Visible = true;                
            }

            if (Configuration.PartnerId == 1 & (pageFunction == string.Empty | pageFunction == Pages.Home))
            {
                divQuickConnect.Visible = true;                
            }

            if (XGlobals.Partner.AllowRegistration)
            {
                if (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser)
                {
                    lnkRecentItems.Visible = false;
                    litSavedHome.Visible = true;
                    int savedCommunityCount = UserSession.UserProfile.Planner.SavedCommunities.Count;
                    int savedHomeCount = UserSession.UserProfile.Planner.SavedHomes.Count;
                    this.lblNoOfHomes.Text = string.Format("Your visit: {0} home{1} viewed.", homecount, (homecount == 1) ? string.Empty : "s");

                    if (homecount == 0)
                    {
                        lnkRecentItemsActiveUser.Visible = false;
                    }
                    if (savedHomeCount > 0 & savedCommunityCount > 0)
                    {
                        lnkSavedHome.Text = string.Format("{0} saved homes and communities", savedHomeCount + savedCommunityCount);
                    }
                    else if (savedHomeCount > 0)
                    {

                        lnkSavedHome.Text = string.Format("{0} saved home{1}", savedHomeCount, (savedHomeCount == 1) ? string.Empty : "s");
                    }
                    else if (savedCommunityCount > 0)
                    {
                        lnkSavedHome.Text = string.Format("{0} saved communit{1}", savedCommunityCount, (savedCommunityCount == 1) ? "y" : "ies");
                    }
                    else
                    {
                        lnkSavedHome.Visible = false;
                        litSavedHome.Visible = false;
                        spanSavedHome.Visible = false;
                        lnkRecentItemsActiveUser.Visible = false;
                        lnkRecentItems.Visible = (homecount != 0);
                    }
                }
                else //not active user
                {
                    this.lblNoOfHomes.Text = homecount < 1 ? @"Your visit: No homes viewed." : string.Format("Your visit: {0} home{1} viewed.", homecount, (homecount == 1) ? string.Empty : "s");
                    lnkSavedHome.Visible = false;
                    spanSavedHome.Visible = false;
                    lnkRecentItemsActiveUser.Visible = false;
                    lnkRecentItems.Visible = (homecount != 0);
                }
            }
            else
            {
                this.lblNoOfHomes.Text = "Your visit: No homes viewed.";
                lnkRecentItems.Visible = false;
                lnkRecentItemsActiveUser.Visible = false;
                lnkSavedHome.Visible = false;
                litSavedHome.Visible = false;
                spanSavedHome.Visible = false;
            }

            Response.Cookies.Add(new HttpCookie("showedRecentItems", homecount.ToString()));
            ShowSuccessMessage();
        }

        /// <summary>
        /// Shows the success message.
        /// </summary>
        private void ShowSuccessMessage()
        {
            if (UserSession.HasSentToFriend)
            {
                lblSuccessMessage.Text = ModalSuccessMessages.SendToFriend;
                lblSuccessMessage.Visible = true;
                UserSession.HasSentToFriend = false;
            }
        }
    }
}
