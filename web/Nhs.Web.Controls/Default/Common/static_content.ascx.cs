using System;
using System.Collections;
using System.IO;
using System.Text;
using Nhs.Library.Common;
using Nhs.Library.Web;
using Nhs.Library.Enums;
using Nhs.Utility.Common;
using Nhs.Utility.Html;
using Nhs.Utility.Html.HtmlElements;
using System.Collections.Generic;

namespace Nhs.Web.Controls.Default.Common
{
    public partial class staticcontent : UserControlBase
    {
        

        #region Members
        int pos = 1;
        bool showAreas = false;
        private string _divKey = string.Empty;
        private string _urlPath = string.Empty;
        private string _cMSArticle = string.Empty;
        private string _staticHTML = string.Empty;
        private string _cssClass = string.Empty;
        private Hashtable _contentTags = new Hashtable();
        private bool _forceTitle;

        #endregion

        #region Properties
        /// <summary>
        /// Path to the static HTML file to load
        /// </summary>
        public string UrlPath
        {
            get { return _urlPath; }
            set { _urlPath = value; }
        }

        /// <summary>
        /// Name of the CMS Article to load - Overrides Url Path if both are specified
        /// </summary>
        public string CMSArticle
        {
            get { return _cMSArticle; }
            set { _cMSArticle = value; }
        }

        /// <summary>
        /// The Css class to be applied to the container
        /// </summary>
        public string CssClass
        {
            get { return _cssClass; }
            set { _cssClass = value; }
        }

        public string StaticHTML
        {
            get { return _staticHTML; }
            set { _staticHTML = value; }
        }

        public bool ForceTitle
        {
            get { return _forceTitle; }
            set { _forceTitle = value; }
        }

        //Final processed HTML (Available after call to ProcessContent())
        public string ContentHTML { get; set; }
        #endregion

        #region Public Methods
        /// <summary>
        /// Add a Tag to the Replacement Tag Collection
        /// </summary>
        /// <param name="tagName"></param>
        /// <param name="tagValue"></param>
        public void AddReplaceTag(ContentTagKey tagName, string tagValue)
        {
            if (!this._contentTags.Contains(tagName))
                this._contentTags.Add(tagName, tagValue);
            else
                this._contentTags[tagName] = tagValue;
        }

        /// <summary>
        /// Remove a Tag from the Replacement Tag Collection
        /// </summary>
        /// <param name="tagName"></param>
        public void RemoveReplaceTag(ContentTagKey tagName)
        {
            this._contentTags.Remove(tagName);
        }
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            this.ProcessContent();
        }
        #endregion

        #region Public Methods
        public string ProcessContent()
        {
            //CMS Article takes precedence over UrlPath
            if (_cMSArticle.Length > 0)
            {
                ProcessCMSArticle();
                //ProcessStaticEntry("CMS Article");
            }
            else
                if (_staticHTML.Length > 0)
                {
                    ProcessStaticHTML();
                    //ProcessStaticEntry("Static Html");
                }
                else
                {
                    ProcessStaticFile();
                    //ProcessStaticEntry(this.MapFilePath(_urlPath));
                }


            return ContentHTML;
        }
        #endregion

        #region Private Methods
        private void ProcessCMSArticle()
        {

        }

        private void ProcessStaticHTML()
        {
            _divKey = _staticHTML;
            this.ProcessHTML(this._staticHTML, _divKey);
        }

        private void ProcessStaticFile()
        {
            try
            {
                _divKey = _urlPath;
                _urlPath = this.MapFilePath(_urlPath);
                string html = this.GetStreamData(_urlPath);
                ProcessStaticEntry(_urlPath);
                this.ProcessHTML(html, _divKey);
            }
            catch (Exception)
            {
                //Log Exception Here
            }
        }

        /// <summary>
        /// Creates a list of elements in order to use them in the static content overlay
        /// </summary>
        /// <param name="filePath">The content file path</param>
        private void ProcessStaticEntry(string filePath)
        {
            string staticContent = System.Web.HttpContext.Current.Request.QueryString["showcontentareas"] as string;
            bool showstatic;
            bool.TryParse(staticContent, out showstatic);
            UserSession.ShowStaticOverlay = showstatic;
            showAreas = showstatic;

            if (showstatic)
            {
                List<StaticItem> staticDictionary = new List<StaticItem>();
                StaticItem item = new StaticItem();
                if (UserSession.StaticContentList == null)
                {
                    item.StaticKey = pos.ToString();
                    item.StaticValue = filePath;
                    staticDictionary.Add(item);
                }
                else
                {
                    staticDictionary = UserSession.StaticContentList;
                    item.StaticKey = pos.ToString();
                    item.StaticValue = filePath;
                    staticDictionary.Add(item);
                }
                UserSession.StaticContentList = staticDictionary;
                ++pos;
            }
        }

        private void ProcessHTML(string html, string divKey)
        {
            try
            {
                html = this.ParseReplacementTags(html);
                html = this.ParseStaticHtml(html);
                this.AttachCSS();
                if (showAreas)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<div class=\"nhsStaticContent\">");                    
                    sb.Append(string.Format("<a class=\"stmark\"><b class=\"stNumber\" title=\"{0}\" ></b></a>", divKey));
                    sb.Append(html);
                    sb.Append("</div>");
                    this.ltrstaticcontent.Text = ContentHTML = sb.ToString();
                }
                else
                    this.ltrstaticcontent.Text = ContentHTML = html;
                //ProcessStaticEntry("Static HTML");
            }
            catch (Exception)
            {
                //Log Exceptino Here
            }
        }

        private string ParseReplacementTags(string html)
        {
            try
            {                
                foreach (ContentTagKey key in _contentTags.Keys)
                {
                    string replaceTag = string.Empty;
                    if (_contentTags[key] != null)
                    {
                        replaceTag = _contentTags[key].ToString();
                        replaceTag = replaceTag.Length == 2 ? replaceTag.ToUpper() : StringHelper.ToTitleCase(replaceTag);
                    }

                    html = html.Replace("[" + key + "]", replaceTag);
                }
            }
            catch (Exception)
            {
                //Log Exception Here
            }
            return html;
        }

        /// <summary>
        /// Parses HTML
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        private string ParseStaticHtml(string html)
        {
            IElement title = new TitleElement();
            IElement head = new HeadElement();
            IElement body = new BodyElement();

            //--Parse HTML--
            ParseHTML parseHTML = new ParseHTML(html);

            //Get Head
            string headHTML = parseHTML.GetInnerHTML(head);
            if (headHTML.Length > 0)
            {
                if (!headHTML.Contains("title"))
                    base.AppendToPageHead(headHTML);
                else if (_forceTitle)
                {
                    if (parseHTML.GetInnerHTML(title).Length > 0)
                        Page.Title = parseHTML.GetInnerHTML(title);
                }
            }

            //Get Body
            body.InnerMarkup = parseHTML.GetInnerHTML(body);
            if (body.InnerMarkup.Length > 0)
                return body.InnerMarkup;
            return html;
        }

        /// <summary>
        /// Attaches CSS to output
        /// </summary>
        private void AttachCSS()
        {
            if (this._cssClass.Length > 0)
            {
                try
                {
                    _cssClass = MapFilePath(_cssClass);
                    string css = GetStreamData(_cssClass);
                    css = Environment.NewLine + @"<style type=""text/css"">" + Environment.NewLine + css + Environment.NewLine + "</style>" + Environment.NewLine;
                    base.AppendToPageHead(css);
                }
                catch (Exception)
                {
                    //Log Exception Here
                }
            }
        }
        #endregion

        #region Helper Methods
        private string MapFilePath(string path)
        {
            var mapper = new ContextPathMapper();
            path = @"~\" + Configuration.StaticContentFolder + @"\" + path;
            return mapper.MapPath(path);
        }

        private string GetStreamData(string path)
        {
            StreamReader streamReader;
            try
            {
                streamReader = new StreamReader(path, Encoding.ASCII);
            }
            catch (FileNotFoundException ex)
            {
                throw ex;
            }
            catch (NotSupportedException ex)
            {
                throw ex;
            }

            string streamData = streamReader.ReadToEnd();
            streamReader.Close();
            return streamData;
        }
        #endregion
    }
}
