<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="paging_tools.ascx.cs" Inherits="Nhs.Web.Controls.Default.Common.PagingTools" %>

<p class="nhsPagingBarTools">
    <asp:Button ID="btnPrevious" CssClass="btnPrevious" Text="Previous" runat="server" OnClick="btnPrevious_Click" AccessKey="b" UseSubmitBehavior="false"/>
    <strong>
    <label for="<%=txtPageNumber.ClientID%>">Page</label> 
    <asp:TextBox ID="txtPageNumber" Text="" CssClass="nhsPageNum" runat="server" /> of <asp:Literal ID="litTotalPages" runat="server" />
    <nhs:LinkButtonDefault id="btnPagingGo1" runat="server" Text="Go" OnClick="btnPagingGo_Click"></nhs:LinkButtonDefault></strong> 
    <asp:Button ID="btnNext" CssClass="btnNext" Text="Next" runat="server" OnClick="btnNext_Click" AccessKey="n"/>
</p> 
<%--hack for case 80654--%>
<script type="text/javascript">
$(document).ready(function() {
    $('#' + '<%=txtPageNumber.ClientID%>').keyup(function(e) {
        if (e.keyCode == 13) {
            var input = $(this).val();
            window.__doPostBack('<%=btnPagingGo1.UniqueID%>', input);
            return false;
    }});
});

</script>