using System;
using System.Collections.Generic;
using Nhs.Library.Web;
using Nhs.Library.Enums;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Mvc.Routing.Configuration;

namespace Nhs.Web.Controls.Default.Common
{
    public partial class partner_navigation_menu : UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (XGlobals.Partner.AllowRegistration)
            {
                if (!IsPostBack)
                {
                    ModalWindowHelper.InitActiveUserFlag(Page);
                    Initialize();
                }
                switch (UserSession.UserProfile.ActorStatus)
                {
                    case WebActors.ActiveUser:
                        this.plhSignOutLink.Visible = true;
                        break;
                    case WebActors.GuestUser:
                    case WebActors.PassiveUser:
                        this.plhSignInLink.Visible = true;
                        break;
                }
            }
            else
            {
                plhSignInLink.Visible = plhSignOutLink.Visible = false;
            }
            this.plhNewHomeGuidLink.Visible = XGlobals.Partner.UsesHomeGuide;
        }
        // After redesign on modals this is Deprecated see details on ticket 73022 and 69928
        private void Initialize()
        {
            if (lnkSignIn != null)
            {
                var parametes = new List<RouteParam>()
                    {
                        new RouteParam
                        {
                            Name = RouteParams.Width,
                            Value = ModalWindowsConst.SignInModalWidth().ToString(),
                            ParamType = RouteParamType.QueryString
                        },
                        new RouteParam
                        {
                            Name = RouteParams.Height,
                            Value = ModalWindowsConst.SignInModalHeight.ToString(),
                            ParamType = RouteParamType.QueryString
                        }
                    };
                lnkSignIn.NavigateUrl = parametes.ToUrl(Pages.LoginModal);
                lnkSignIn.Attributes.Add("onclick",
                    "jQuery.googlepush('Account Events','Access Account','Open Form - Header Link')");
                lnkSignIn.CssClass = "thickbox " + lnkSignIn.CssClass;
            }

            if (lnkRegister != null)
                {
                    var parametes = new List<RouteParam>()
                    {
                        new RouteParam
                        {
                            Name = RouteParams.Width,
                            Value = ModalWindowsConst.RegisterModalWidth.ToString(),
                            ParamType = RouteParamType.QueryString
                        },
                        new RouteParam
                        {
                            Name = RouteParams.Height,
                            Value = ModalWindowsConst.RegisterModalHeight.ToString(),
                            ParamType = RouteParamType.QueryString
                        }
                    };
                    lnkRegister.NavigateUrl = parametes.ToUrl(Pages.RegisterModal);
                    lnkRegister.Attributes.Add("onclick",
                        "jQuery.googlepush('Account Events','Create Account','Open Form - Header Link')");
                    lnkRegister.CssClass = "thickbox " + lnkRegister.CssClass;
                }
        }
    }
}