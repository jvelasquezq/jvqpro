using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Nhs.Web.Controls.Default.Common
{
    public partial class email_safesenders : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.lblFromEmail.Text = Nhs.Library.Common.XGlobals.Partner.FromEmail;
        }
    }
}