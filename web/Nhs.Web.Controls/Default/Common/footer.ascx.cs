using System;
using Nhs.Library.Common;
using Nhs.Library.Enums;
using Nhs.Library.Web;

namespace Nhs.Web.Controls.Default.Common
{
    public partial class footer : UserControlBase
    {
        private DateTime _dateTime = new DateTime();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!XGlobals.Partner.UsesLightFooter)
            {
                //Show-Hide home guide link
                this.liHomeGuide.Visible = XGlobals.Partner.UsesHomeGuide;
                this.liPartners.Visible = XGlobals.Partner.FooterShowPartners;
                this.liAdvertise.Visible = XGlobals.Partner.FooterShowAdvertise;
                this.liAboutUs.Visible = XGlobals.Partner.FooterShowAboutUs;
                this.liSiteIndex.Visible = XGlobals.Partner.FooterShowSiteIndex;
                this.liListHomes.Visible = XGlobals.Partner.FooterShowListHomes;
                switch (UserSession.UserProfile.ActorStatus)
                {
                    case WebActors.ActiveUser:
                        this.liSignOut.Visible = true;
                        break;
                    case WebActors.GuestUser:
                    case WebActors.PassiveUser:
                        this.plhSignIn.Visible = true;
                        break;
                }
                this.plhSignIn.Visible = XGlobals.Partner.AllowRegistration;
            }
            else
            {
                ulTopLinks.Visible = false;
                plhBottomLinks.Visible = false;
            }


            // standard NHS copyright
            this.litCopyright.Text = "<div id=\"nhsCopyright\"><p>Copyright &copy; 2001-" + _dateTime.Year.ToString() + " <a href=\"http://www.newhomesource.com/\">NewHomeSource.com -- More New Homes Than Anywhere</a> is a trademark of <a target=\"_blank\" href=\"http://www.thebdx.com\">Builders Digital Experience, LLC</a> and all other marks are either trademarks or registered trademarks of their respective owners. All rights reserved.</p></div>";


            liTermsOfUse.Visible = XGlobals.Partner.ShowFooterTermsOfUse;
            liPrivacyPolicy.Visible = XGlobals.Partner.ShowFooterPrivacyPolicy;

            if (!XGlobals.Partner.AllowRegistration)
            {
                plhSignIn.Visible = false;
                lnkUnsubscribe.Visible = false;
            }

            if (!XGlobals.Partner.FooterShowAboutUs)
            {
                liAboutUs.Visible = false;
            }
        }

        private bool ConvertToBool(string onOff)
        {
            if (onOff.ToLower() == "on")
                return true;
            else
                return false;
        }
    }
}
