using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Nhs.Library.Web;
using Nhs.Library.Constants;
using Nhs.Utility.Common;

namespace Nhs.Web.Controls.Default.Common
{
    public partial class PagingTools : UserControlBase
    {
        public event EventHandler PageNumberChanged;
        private string _commandName = string.Empty;
        private int _lastPage;

        public int TotalPages
        {
            get
            {
                return (int)Math.Ceiling((double)TotalResults / (double)ResultsPerPage);
            }
        }

        public int TotalResults
        {
            get
            {
                if (ViewState["pagingToolsTotalResults"] != null)
                {
                    return (int)ViewState["pagingToolsTotalResults"];
                }
                else
                {
                    return 1;
                }                
            }
            set
            {
                ViewState["pagingToolsTotalResults"] = value;

                if (PageNumber > TotalPages)
                {
                    PageNumber = TotalPages;
                }

                ShowHideButtons();

            }
        }

        public int ResultsPerPage
        {
            get
            {
                if (ViewState["pagingToolsResultsPerPage"] != null)
                {
                    UserSession.ResultsPerPage = (int) ViewState["pagingToolsResultsPerPage"];
                    return (int)ViewState["pagingToolsResultsPerPage"];
                }
                else
                {
                    UserSession.ResultsPerPage = 10;
                    return 10;
                }
                
            }
            set
            {
                ViewState["pagingToolsResultsPerPage"] = value;

                if (PageNumber > TotalPages)
                {
                    PageNumber = TotalPages;
                }
            }
        }

        public int PageNumber
        {
            get
            {
                if (ViewState["pagingToolsPageNumber"] != null)
                {
                    return (int)ViewState["pagingToolsPageNumber"];
                }
                else
                {
                    ViewState["pagingToolsPageNumber"] = 1;
                    return 1;
                }
            }

            set
            {
                ViewState["pagingToolsPageNumber"] = value;
                ShowHideButtons();
            }
        }

        public int PageEnd
        {
            get
            {
                return GetPageEnd(PageNumber);
            }            
        }

        public int PageStart
        {
            get
            {
                return GetPageStart(PageNumber);
            }
        }

        public string CommandName
        {
            get
            {
                return _commandName;
            }
            set
            {
                _commandName = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ShowHideButtons();
            }
        }

        public int GetPageStart(int pageNumber)
        {
            return ((pageNumber - 1) * ResultsPerPage) + 1;
        }

        public int GetPageEnd(int pageNumber)
        {
            return (pageNumber * ResultsPerPage);
        }

        public IList GetDataPage(IList data, int dataStart, int dataEnd)
        {
            if (!this.Request.RawUrl.Contains(Pages.MlsHomeResults))
            {
                // If there is no data, return null.
                if (data.Count == 0)
                {
                    return null;
                }

                // Data is zero-based index. 
                int pageStart = PageStart - 1;
                int pageEnd = PageEnd - 1;


                // Check to see if the starting index is within the data. 
                if (pageStart < dataStart || pageStart > dataEnd)
                {
                    // data does not contain starting record. 
                    return null;
                }


                // Data may not contain a full page of records. 
                if (pageEnd > dataEnd)
                {
                    pageEnd = dataEnd;
                }



                // Now we know the logical page start & end records.
                // Need to translate that to the actual records in data list. 
                int startIndex = pageStart - dataStart;
                int endIndex = startIndex + (pageEnd - pageStart);

                ArrayList pageResults = new ArrayList();

                for (int i = startIndex; i <= endIndex; i++)
                {
                    if (data.Count == i) break;
                    pageResults.Add(data[i]);
                }

                return pageResults;
            }
            else
            {
                ArrayList pageResults = new ArrayList();

                for (int i = dataStart; i <= dataEnd; i++)
                {
                    if (data.Count == i) break;
                    pageResults.Add(data[i]);
                }

                return pageResults;
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber < TotalPages ? PageNumber + 1 : TotalPages;
            _commandName = LogImpressionConst.PageNext;
            OnPageNumberChanged(new EventArgs());
        }

        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber > 1 ? PageNumber - 1 : 1;
            _commandName = LogImpressionConst.PagePrevious;
            OnPageNumberChanged(new EventArgs());
        }

        protected void btnPagingGo_Click(object sender, EventArgs e)
        {
            if (IsNumeric(txtPageNumber.Text))
            {
                int newPage = int.Parse(txtPageNumber.Text);
                _lastPage = newPage;
                if (newPage > 0 && newPage <= TotalPages)
                {
                    PageNumber = newPage;
                }
                else
                {
                    if (newPage <= 0)
                    {
                        PageNumber = 1;
                    }
                    else
                    {
                        PageNumber = TotalPages;
                    }
                }
            }
            else
            {
                if (PageNumber == 0)
                    PageNumber = 1;
            }
           
            _commandName = LogImpressionConst.PageGo;
            OnPageNumberChanged(new EventArgs());
        }


        private bool IsNumeric(object Expression)
        {
            // Variable to collect the Return value of the TryParse method.
            bool isNum;

            // Define variable to collect out parameter of the TryParse method. If the conversion fails, the out parameter is zero.
            int retNum;

            // The TryParse method converts a string in a specified style and culture-specific format to its int number equivalent.
            // The TryParse method does not generate an exception if the conversion fails. If the conversion passes, True is returned. If it does not, False is returned.
            isNum = int.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        protected virtual void OnPageNumberChanged(EventArgs args)
        {
            if (PageNumberChanged != null)
            {
                UserSession.CommPageNumber = PageNumber;
                UserSession.MlsPageNumber = PageNumber; 
                PageNumberChanged(this, args);
            }            
        }

        private void ShowHideButtons()
        {
            txtPageNumber.Text = PageNumber.ToString();
            litTotalPages.Text = TotalPages.ToString();
            btnPrevious.Visible = PageNumber > 1;
            btnNext.Visible = PageNumber < TotalPages;            
        }
    }
}
