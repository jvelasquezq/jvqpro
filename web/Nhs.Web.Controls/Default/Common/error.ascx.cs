using System;
using System.Data;
using System.Collections;
using Nhs.Library.Business;
using Nhs.Library.Web;
using Nhs.Library.Constants;
using Nhs.Library.Common;
using Nhs.Library.Enums;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Utility.Constants;
using Nhs.Utility.Common;

namespace Nhs.Web.Controls.Default.Common
{
    public partial class error : UserControlBase
    {
        private int _errorCode = -1;
        private int _marketId;
        private int _communityId;
        private string _errorDetails = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["statuscode"] != string.Empty)
                _errorCode = Convert.ToInt32(Request.QueryString["statuscode"]);

            _marketId = NhsUrl.GetMarketID;
            _communityId = NhsUrl.GetCommunityID; 

           
            if (!string.IsNullOrEmpty(Request.QueryString["message"]))
            {
                _errorDetails = Request.QueryString["message"];
                ErrorLogger.LogError(new Exception(_errorDetails));
                _errorDetails = _errorDetails.Replace(Environment.NewLine, "<br />" + Environment.NewLine);
            }


            this.pnlFileNotFoundError.Visible = false;
            this.pnlGeneralError.Visible = false;

            if (Configuration.PartnerId == (int)PartnersConst.Move)
            {
                lnkHomeGuide.Visible = false;
                lnkMailTo1.Text = @"webmaster@newhomes.move.com";
                lnkMailTo2.Text = @"webmaster@newhomes.move.com";
                lnkMailTo2.NavigateUrl = @"mailto:webmaster@newhomes.move.com";
                lnkMailTo1.NavigateUrl = @"mailto:webmaster@newhomes.move.com";
            }
          
            if (_marketId != 0 & _communityId == 0)
            {
                pnlFriendlyError.Visible = true;
                RouteParam market = new RouteParam();
                market.Name = RouteParams.MarketId;
                market.Value = _marketId.ToString();
                market.ParamType = RouteParamType.Friendly;
                lnkCommResults2.Parameters.Add(market);
                IMarket marketObj = XGlobals.GetMarket(_marketId);
                string marketName = marketObj.MarketName;
                string stateName = marketObj.StateName;
                lnkCommResults2.Text = string.Format("Search again in {0}, {1}", marketName, stateName);
                Response.Status = "404 Page Not Found";
            }
            else if (_communityId != 0 & _marketId != 0)
            {
                pnlCommFriendlyError.Visible = true;
                DataRowCollection builders = DataProvider.Current.GetBuilderByCommunityMarket(_communityId, _marketId);
                Hashtable bcLinks = new Hashtable();
                foreach (DataRow row in builders)
                {
                    NhsUrl bclink = new NhsUrl();
                    bclink.Function = Pages.CommunityDetail;
                    bclink.AddParameter(UrlConst.CommunityID, _communityId.ToString());
                    bclink.AddParameter(UrlConst.BuilderID, row[CommunityDBFields.BuilderId].ToString());
                    string key = string.Format("{0} by {1}", row[CommunityDBFields.CommunityName], row[CommunityDBFields.BuilderName]);
                    bcLinks[key] = Page.ResolveUrl("~/"+ bclink);
                }

                blBCLinks.DataSource = bcLinks;
                blBCLinks.DataTextField = "key";
                blBCLinks.DataValueField = "value";
                blBCLinks.DataBind();
                IMarket marketObj = XGlobals.GetMarket(_marketId);
                string marketName = marketObj.MarketName;
                string stateName = marketObj.StateName;
                RouteParam market = new RouteParam();
                market.Name = RouteParams.MarketId;
                market.Value = _marketId.ToString();
                market.ParamType = RouteParamType.Friendly;
                lnkCommResults.Parameters.Add(market);
                lnkCommResults.Text = string.Format("Search again in {0}, {1}", marketName, stateName);
                Response.Status = "404 Page Not Found";
            }
            else if (_errorCode == 404)
            {
                this.pnlFileNotFoundError.Visible = true;
                if (Configuration.PartnerId != 1 && Configuration.PartnerId != 10001 && Configuration.PartnerId != 333)
                    this.plh404Links.Visible = false;
                else
                    this.plh404Links.Visible = true;


                Response.Status = "404 Page Not Found";
            }
            else
            {
                this.pnlGeneralError.Visible = true;
                Response.Status = "500 Server Error";
            }

         }
    }
}
