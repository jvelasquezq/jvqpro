<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="footer.ascx.cs" Inherits="Nhs.Web.Controls.Default.Common.footer" %>
<div id="nhsFooter">
    <!-- Default Footer //-->
	<hr class="nhsReaderAlt" />
	    <ul id="ulTopLinks" runat="server">
		    <li><nhs:Link ID="lnkSearch" runat="server" Text="Search for new homes" Function="BasicSearch"/></li>
		    <li id="liHomeGuide" runat="server"><nhs:Link ID="lnkHomeGuide" runat="server" Text="New home guide" Function="HomeGuide" /></li>						
		    <li id="liListHomes" runat="server"><nhs:Link ID="lnkListHomes" Text="List your homes" runat="server" Function="ListYourHomes"></nhs:Link></li>
    		
		    <asp:PlaceHolder ID="plhSignIn" runat="server" Visible="false">
		    <li><nhs:Link ID="lnkSignIn" runat="server" Text="Sign In" Function="SignIn"/></li>
		    <li><nhs:Link ID="lnkRegister" runat="server" Text="Create Account" Function="Register"/></li>
		    </asp:PlaceHolder>
    		
		    <li id="liSignOut" runat="server" visible="false"><nhs:Link ID="lnkSignOut" runat="server" Text="Sign Out" Function="SignOut"/></li>		
    		
		    <li><nhs:Link ID="lnkUnsubscribe" runat="server" Text="Unsubscribe" Function="Unsubscribe"/></li>
		    <li><nhs:Link ID="lnkHelp" runat="server" Text="Help" Function="SiteHelp"/></li>
	    </ul>

	    <ul>
	        <li id="liPrivacyPolicy" runat="server"><nhs:Link ID="lnkPrivacyPolicy" runat="server" Text="Privacy policy" Function="PrivacyPolicy" Target="_blank" /></li> 
		    <li id="liTermsOfUse" runat="server"><nhs:Link ID="lnkTermsofuse" runat="server" Text="Terms of use" Function="TermsOfUse" AccessKey="8" /></li>
		    <asp:PlaceHolder ID="plhBottomLinks" runat="server">
		        <li id="liPartners" runat="server"><nhs:Link ID="lnkPartners" runat="server" Text="Partners" Function="Partners"/></li>
		        <li id="liSiteIndex" runat="server"><nhs:Link ID="lnkSiteIndex" Rel="nofollow" runat="server" Text="Site Index" Function="SiteIndex"/></li>
		        <li id="liAboutUs" runat="server"><nhs:Link ID="lnkAboutUs" runat="server" Text="About us" Function="AboutUs"/></li>
		        <li id="liContactUs" runat="server"><nhs:Link ID="lnkContactUs" runat="server" Text="Contact us" Function="ContactUs"/></li>		
		        <li id="liAdvertise" runat="server"><asp:HyperLink ID="lnkAdvertise" runat="server" Text="Advertise" NavigateUrl="http://www.builderhomesite.com/newhomesource/mediakit/request.htm"></asp:HyperLink></li>
		    </asp:PlaceHolder>
	    </ul>	
</div>
<asp:Literal ID="litCopyright" runat="server"></asp:Literal>
