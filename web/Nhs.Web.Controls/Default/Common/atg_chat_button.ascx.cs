﻿using System;
using System.Web.UI;
using Nhs.Library.Constants;
using Nhs.Utility.Constants;
using Nhs.Mvc.Routing.Interface;
using Nhs.Utility.Common;

namespace Nhs.Web.Controls.Default.Common
{
    public partial class atg_chat_button : UserControl
    {
        public string LiveChatAccountId
        {
            get
            {
                return NhsRoute.IsBrandPartnerNhs
                                      ? "825639"
                                      : "818295";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }


    }
}