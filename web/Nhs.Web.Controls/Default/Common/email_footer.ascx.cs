using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Utility.Common;

namespace Nhs.Web.Controls.Default.Common
{
    public partial class email_footer : UserControlBase
    {
        private string _email = string.Empty;
        private string _privacyPolicyUrl = string.Empty;
        private string _userid = string.Empty;

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public string PrivacyPolicyUrl
        {
            get { return _privacyPolicyUrl; }
            set { _privacyPolicyUrl = value; }
        }


        public string UserId
        {
            get { return _userid; }
            set { _userid = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            litCurrentYear.Text = DateTime.Now.Year.ToString();

            if (!String.IsNullOrEmpty(this.Email))
            {
                this.lblUserEmail.Text = "(" + this.Email + ")";
            }
            else if (!String.IsNullOrEmpty(this.UserId))
            {
                Profile tmpProfile = new Profile(this.UserId);
                tmpProfile.DataBind(this.UserId);
                this.lblUserEmail.Text = "(" + tmpProfile.Email + ")";
            }

            var privacyPolicy = new NhsUrl(Pages.PrivacyPolicy);
            var prvTemp = ResolveAbsoluteUrl("/" + privacyPolicy);
            this.hlnkPrivacyPolicy.NavigateUrl = string.IsNullOrEmpty(_privacyPolicyUrl) ? prvTemp : _privacyPolicyUrl;

            var unsubscribe = new NhsUrl(Pages.Unsubscribe);
            var unsTemp = ResolveAbsoluteUrl("/" + unsubscribe);
            this.hlnkUnsusbcribe.NavigateUrl = unsTemp;
            
        }
    }
}