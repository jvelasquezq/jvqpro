using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Data;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Proxy;
using Nhs.Library.Web;
using Nhs.Library.Business;
using Nhs.Mvc.Data.Repositories;
using Nhs.Mvc.Routing.Configuration;
using Nhs.Search.Objects;

namespace Nhs.Web.Controls.Default.Common
{
    public enum SpotlightType
    {
        Featured,
        Standard,
        HotHome,
        GreenHome
    }
    
    /// <summary>
    /// 
    /// </summary>
    public partial class Spotlight_communities : UserControlBase
    {
        #region Fields

        private string _filter = string.Empty;
        private bool _hasCommunties = false;
        private int _marketId;
        private  bool _alwaysFill = true;
        private bool _featuredSpotlights;
        private bool _hotHomes;
        private ImpressionLogger _logger = new ImpressionLogger(true);
        private SpotlightType _spotlightType = SpotlightType.Standard;

        #endregion

        #region Properties

        public int MarketId
        {
            get { return _marketId; }
            set { _marketId = value; }
        }

        public string CustomFilter
        {
            get { return _filter; }
            set { _filter = value; }
        }

        public bool HasCommunties
        {
            get { return _hasCommunties; }
        }

        //always fill spolight list with preset number of comms
        //currently this is four(4)
        public bool AlwaysFill
        {
            get { return _alwaysFill; }
            set { _alwaysFill = value; }
        }

        public bool HotHomes
        {
            get { return _hotHomes; }
            set { _hotHomes = value; }
        }

        public bool GreenHomes { get; set; }

        public SpotlightType SpotlightType
        {
            get { return _spotlightType; }
        }
        #endregion

        /// <summary>
        /// Loads spotlight communities.
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Declare vars
            List<CommunityResult> spotlightCommunities;

            // Get FSR
            FeaturedSpotlightRotator spotlightRotator = FeaturedSpotlightRotator.GetRotator();
            // Update properties
            this._featuredSpotlights = (spotlightRotator.Count > 0);

            // Featured Spotlight Communities?
            if (this._featuredSpotlights)
            {
                // Get Featured Spotlight Communities
                SearchService searchService = new SearchService();
                spotlightCommunities = searchService.GetFeaturedSpotlightCommunities(XGlobals.Partner.PartnerId, spotlightRotator.Filter());

                // Check to see if Cache and DB disagree on how many Featured Spotlights
                // exist. Should never happen, but if it does, fail gracefully by showing
                // Spotlight Communities, rather than nothing.
                if (spotlightCommunities.Count == 0)
                {
                    // Get Spotlight Communities
                    spotlightCommunities = GetSpotlightCommunities();

                    _spotlightType = SpotlightType.Standard;

                    // Update properties
                    this._featuredSpotlights = false;
                }
                else
                {
                    // Reorder
                    spotlightCommunities = spotlightRotator.Synchronize(spotlightCommunities);
                    _spotlightType = SpotlightType.Featured;
                }
            }
            else if (this._hotHomes)
            {
                // Get Spotlight Hot Home Communities
                spotlightCommunities = GetSpotlightHotHomesCommunities();
                _spotlightType = SpotlightType.HotHome;

                // If there are not hot homes, get the dang ol spotlight communities
                if (spotlightCommunities.Count == 0)
                {
                    spotlightCommunities = GetSpotlightCommunities();
                    _spotlightType = SpotlightType.Standard;
                }
            }
            else if (this.GreenHomes)
            {
                // Get Spotlight Green Home Communities
                SearchService searchService = new SearchService();
                spotlightCommunities = searchService.GetGreenSpotlightCommunitiesMax(XGlobals.Partner.PartnerId, _marketId);
                _spotlightType = SpotlightType.GreenHome;
            }
            else
            {
                // Get Spotlight Communities
                spotlightCommunities = GetSpotlightCommunities();
                _spotlightType = SpotlightType.Standard;
            }

            // Update Properties
            this._hasCommunties = (spotlightCommunities.Count > 0);

            // Bind DataSource
            this.repSpotlightCommunities.DataSource = spotlightCommunities;
            this.repSpotlightCommunities.DataBind();
        }

        protected List<CommunityResult> GetSpotlightHotHomesCommunities()
        {
            SearchService searchService = new SearchService();

            // this returns the list of hot homes
            List<CommunityResult> returnedSpotlightComms = searchService.GetHotDealsSpotlightCommunitiesMax(XGlobals.Partner.PartnerId, _marketId, 4);

            // if alwaysFill flag turned on then fill the list
            if (AlwaysFill)
            {
                if (returnedSpotlightComms.Count < 4)
                {
                    // returns 4 or less spotlights with just partnerId as the criteria
                    List<CommunityResult> randomSpotlightComms =
                        searchService.GetSpotlightCommunitesCustom(XGlobals.Partner.PartnerId, 0, CustomFilter);

                    for (int i = returnedSpotlightComms.Count; i < randomSpotlightComms.Count; i++)
                        returnedSpotlightComms.Add(randomSpotlightComms[i]);
                }
            }
            return returnedSpotlightComms;
        }

        protected List<CommunityResult> GetSpotlightCommunities()
        {
            // Declare vars
            SearchService searchService = new SearchService();

            // Returns <= 4 Spotlights
            List<CommunityResult> returnedSpotlightComms = searchService.GetSpotlightCommunitesCustom(XGlobals.Partner.PartnerId, _marketId, CustomFilter);

            // Fill the list?
            if (AlwaysFill)
            {
                // Less than Max?
                if (returnedSpotlightComms.Count < 4)
                {
                    // Returns <= 4 Spotlights using PartnerID as the criteria
                    List<CommunityResult> randomSpotlightComms = searchService.GetSpotlightCommunitesCustom(XGlobals.Partner.PartnerId, 0, CustomFilter);

                    for (int i = returnedSpotlightComms.Count; i < randomSpotlightComms.Count; i++)
                    {
                        returnedSpotlightComms.Add(randomSpotlightComms[i]);
                    }
                }
            }

            return returnedSpotlightComms;
        }

        protected DataTable GetSpotlightCommunitiesFromDB()
        {        

            //this returns spotlightcomms            
            DataTable returnedSpotlightComms = DataProvider.Current.GetSpotlightCommunities(XGlobals.Partner.PartnerId,this.MarketId);        
            return returnedSpotlightComms;
        }


        /// <summary>
        /// Gets Image Path
        /// </summary>
        protected string GetImage(object ImageName)
        {
            if (ImageName == DBNull.Value || ImageName.ToString() == "N")
                return "[resource:]/globalresources/default/images/spotlight_nophoto.gif";
            else
                return
                    Configuration.IRSDomain +
                    ImageName.ToString().ToLower().Replace(".gif", ".jpg").Replace("small", "spotlight");
        }

        /// <summary>
        /// Gets Valid Price"
        /// </summary>
        protected string GetValidPrice(Decimal dPrice)
        {
            if (dPrice <= 0)
                return (string.Empty);
            else
                return "From " + (String) (Convert.ToString(Convert.ToDecimal(dPrice).ToString("C0")));
        }

        /// <summary>
        /// sets links names and add parmeters for every spotlight community.
        /// </summary>
        protected void repSpotlightCommunities_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                CommunityResult cr = (CommunityResult)e.Item.DataItem;

                // Image Url
                Link lnkTmp = (Link)e.Item.FindControl("lnkImageUrl");                
                lnkTmp.ImageUrl = GetImage(cr.CommunityImageThumbnail);
                lnkTmp.Text = cr.CommunityName;
                lnkTmp.Function = Pages.CommunityDetail;

                lnkTmp.Parameters.Add(new RouteParam(RouteParams.CommunityId, cr.CommunityId));
                lnkTmp.Parameters.Add(new RouteParam(RouteParams.BuilderId, cr.BuilderId));
                if (_featuredSpotlights)
                {
                    lnkTmp.Parameters.Add(new RouteParam(RouteParams.LogEvent, LogImpressionConst.FeaturedSpotlightClick));
                }

                // Community Detail Link
                lnkTmp = (Link)e.Item.FindControl("lnkCommName");
                lnkTmp.Text = cr.CommunityName;
                lnkTmp.Function = Pages.CommunityDetail;

                lnkTmp.Parameters.Add(new RouteParam(RouteParams.CommunityId, cr.CommunityId.ToString()));
                lnkTmp.Parameters.Add(new RouteParam(RouteParams.BuilderId, cr.BuilderId.ToString()));
                if (_featuredSpotlights)
                {
                    lnkTmp.Parameters.Add(new RouteParam(RouteParams.LogEvent, LogImpressionConst.FeaturedSpotlightClick));
                }
                if ((cr.HasHotHome) || (cr.PromoId != 0))
                {
                    lnkTmp.CssClass = "nhsSpotHotHomeCommName";
                }

                // Brand Name
                Literal ltrTemp = (Literal) e.Item.FindControl("lblBrandName");
                ltrTemp.Text = cr.BrandName.ToString();

                // City
                ltrTemp = (Literal) e.Item.FindControl("lblCity");
                ltrTemp.Text = cr.City.ToString();

                // State
                ltrTemp = (Literal) e.Item.FindControl("lblState");
                ltrTemp.Text = cr.State.ToString();

                // Price
                ltrTemp = (Literal) e.Item.FindControl("lblPrice");
                ltrTemp.Text = GetValidPrice(Convert.ToDecimal(cr.PriceLow));

                // Video Icon
                Link lnkCommVideo = (Link)e.Item.FindControl("lnkCommVideo");
                PlaceHolder plhVideoLink = (PlaceHolder)e.Item.FindControl("plhVideoLink");
                //Only communities with community videos                 
                if (cr.SubVideoFlag.Equals("Y"))
                {
                    lnkCommVideo.Function = cr.Video_url;
                    lnkCommVideo.Parameters.Add(new RouteParam(RouteParams.CommunityId, cr.CommunityId.ToString()));
                    lnkCommVideo.Parameters.Add(new RouteParam(RouteParams.BuilderId, cr.BuilderId.ToString()));
                    lnkCommVideo.Parameters.Add(new RouteParam(RouteParams.LogEvent, LogImpressionConst.OffsiteVideo));
                    lnkCommVideo.Parameters.Add(new RouteParam(RouteParams.FromPage, NhsUrl.GetFromPage));
                }
                else
                {
                    plhVideoLink.Visible = false;
                }

                // Featured Spotlights?
                if (this._featuredSpotlights)
                {
                    // Add Impression Logging
                    _logger.BuilderId = cr.BuilderId;
                    _logger.CommunityId = cr.CommunityId;
                    _logger.PartnerId = Configuration.PartnerId.ToString();
                    _logger.FromPage = NhsUrl.GetFromPage;
                    _logger.LogView(LogImpressionConst.FeaturedSpotlightView, this.Page);
                }
            }
        }
    }
}
