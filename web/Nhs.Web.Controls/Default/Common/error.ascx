<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="error.ascx.cs" Inherits="Nhs.Web.Controls.Default.Common.error" %>
<div class="nhsContent">

<asp:literal ID="ltrErrorDetails" runat="server" />

		<asp:Panel ID="pnlGeneralError" runat="server" Visible="true">
		    <h2 class="nhsError">Error</h2>
		    <h3>We're sorry, a site error has occurred.</h3>
			
			<p>Please go back and try again.  If you continue to experience errors, please contact system support at <asp:HyperLink id="lnkMailTo1" runat="Server"  NavigateUrl="mailto:webmaster@newhomesource.com" Text="webmaster@newhomesource.com"></asp:HyperLink>.</p>
			<p>We apologize for any inconvenience.</p>
		</asp:Panel>
		
		<asp:Panel ID="pnlFileNotFoundError" runat="server" Visible="true">
		    <h2 class="nhsError">Page not found</h2>		    
			<h3>We're sorry, but the page you requested could not be found.</h3><br />
			
			<p><strong>Where would you like to go?</strong></p>
			
			<p><nhs:link ID="lnkBasicSearch" runat="server" Function="BasicSearch" Text="Search for new homes" /></p>
			<asp:PlaceHolder Runat="server" ID="plh404Links" EnableViewState="False">
			    <p><nhs:link ID="lnkHomeGuide" runat="server" function="resourcecenter" Text="Read articles on new home buying" /></p>
			    <p><nhs:link ID="lnkHome" runat="server" function="Home" Text="Home page" /></p>
			</asp:PlaceHolder>
			
			<br />
            <p>If you feel this error is incorrect, please send a message to <asp:HyperLink id="lnkMailTo2" runat="Server"  NavigateUrl="mailto:webmaster@newhomesource.com" Text="webmaster@newhomesource.com" ></asp:HyperLink>. For best service, please include the page location (URL) you were attempting to reach.</p>
		</asp:Panel>
		

		<%-- there is a community ID, but no builder ID --%>
		<asp:Panel ID="pnlCommFriendlyError" runat="server" Visible="false">
		    <h2 class="nhsError">The community page you're looking for has moved.</h2>
		    <h3>Select a builder community below:</h3>
		    <asp:BulletedList ID="blBCLinks" DisplayMode="HyperLink" runat="server">
		        <asp:ListItem Text="Community Name"></asp:ListItem>
		    </asp:BulletedList>
            <br />
            <p><nhs:link ID="lnkCommResults" runat="server" Function="CommunityResults">Search again in {Market}, {ST}</nhs:link></p>
            <p><nhs:link ID="lnkHome2" runat="server" Function="Home">Begin a new search</nhs:link></p>
		
		</asp:Panel>
		
		<asp:Panel ID="pnlFriendlyError" runat="server" Visible="false">
		    <h2 class="nhsError">The page you're looking for has moved.</h2>
            <h3><nhs:link ID="lnkCommResults2" runat="server" Function="CommunityResults">Search again in {Market}, {ST}</nhs:link></h3>
            <nhs:link ID="lnkHome3" runat="server" Function="Home">Begin a new search</nhs:link>
		
		
		
		</asp:Panel>
        <br />
</div>
