﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Nhs.Library.Constants;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Web;
using Nhs.Library.Common;
using Nhs.Utility.Constants;
using Nhs.Utility.Common;


namespace Nhs.Web.Controls.Pro.Common
{
    public partial class footer : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Initialize();
            if (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser)
            {
                Link lnkSignIn = this.FindControl("lnkSignInPro") as Link;
                Link lnkRegister = (Link)this.FindControl("lnkRegisterPro");
                if (lnkSignIn != null)
                {
                    //lnkSignIn.Text = @"Sign Out";
                    //lnkSignIn.Function = Pages.LogOff;
                    lnkSignIn.Visible = false;
                    lnkRegister.Visible = false; //hide register link if signed in
                }
            }
        }

        private void Initialize()
        {
            lnkSignInPro.WindowHeight = ModalWindowsConst.SignInModalHeight;
            lnkSignInPro.WindowWidth = ModalWindowsConst.SignInModalWidth();

            ltrFbkTwt.Text = RenderFooterBoxContent(XGlobals.Partner.PartnerId != PartnersConst.Pro.ToType<int>());

        }

        private string RenderFooterBoxContent(bool isProPL)
        {
            var seoContentManager = new SeoContentManager();
            StringBuilder sb = new StringBuilder();
            var activeUserOrCookie = (UserSession.UserProfile.ActorStatus == WebActors.ActiveUser) ||
                       (HttpContext.Current.Request.Cookies["NHSPro_Cookie"] != null &&
                        DateTime.Compare(
                            System.Convert.ToDateTime(HttpContext.Current.Request.Cookies["NHSPro_Cookie"].Value),
                            DateTime.Now) <= 30);
            if (!isProPL)
            {
                sb.Append(String.Format("<div class='{0}'>", activeUserOrCookie ? "proFacebook" : "proFooterBuyer"));
                sb.Append(seoContentManager.GetHtmlBlock(SeoTemplateType.Pro_Home, activeUserOrCookie ? "social-facebook" : "footerhomebuyertext"));
            }
            else
            {
                sb.Append(String.Format("<div class='{0}'>", "proFacebook"));
                sb.Append(seoContentManager.GetHtmlBlock(SeoTemplateType.Pro_Home,
                                                  "social-facebook"));
            }
            sb.Append("</div>");
            if (!isProPL)
            {
                sb.Append(String.Format("<div class='{0}'>", activeUserOrCookie ? "proTwitter" : "proFooterBuilder"));
                sb.Append(seoContentManager.GetHtmlBlock(SeoTemplateType.Pro_Home,
                                                  activeUserOrCookie ? "social-twitter" : "footerbuildertext"));
            }
            else
            {
                sb.Append(String.Format("<div class='{0}'>", "proTwitter"));
                sb.Append(seoContentManager.GetHtmlBlock(SeoTemplateType.Pro_Home,
                                                  "social-twitter"));
            }
            sb.Append("</div>");

            return sb.ToString();

        }
    }
}