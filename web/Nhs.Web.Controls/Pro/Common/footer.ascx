﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="footer.ascx.cs" Inherits="Nhs.Web.Controls.Pro.Common.footer" %>
<%@ Import Namespace="Nhs.Utility.Common" %>
<%@ Import Namespace="Nhs.Library.Common" %>
<%@ Import Namespace="Nhs.Web.UI.AppCode.HtmlHelpers" %>
<div class="proFooter">
    <div>        
        <div class="proFooterAd">
            <p class="nhsReader">
                Advertisement</p>
            <nhs:ad ID="x01" UseIFrame="true" runat="server" />
        </div>       
        <div class="proFooterBox">
            <asp:Literal ID="ltrFbkTwt" runat="server"></asp:Literal>            
            <div class="proNav">
                <ul>
                    <li>
                        <nhs:Link ID="lnkHome" runat="server" Text="Home" Function="Home" /></li>
                    <li id="liNewHomes" runat="server">
                        <nhs:Link ID="lnkNewHomes" runat="server" Text="Search New Homes" Function="Home" /></li>
                    <li id="liAdvancedSearch" runat="server">
                        <nhs:Link ID="lnkAdvancedSearch" Text="Advanced Search" runat="server" Function="AdvancedSearch"></nhs:Link></li>
                </ul>
                <ul>
                    <li>
                        <nhs:AjaxFormLink ID="lnkSignInPro" runat="server" Text="Sign In" Function="SignIn"
                            ModalFunction="LoginModalOld" /></li>
                    <li>
                        <nhs:AjaxFormLink ID="lnkRegisterPro" runat="server" Text="Register" Function="Register"
                            ModalFunction="RegisterModal" /></li>
                    <li id="liAboutUs" runat="server">
                        <nhs:Link ID="lnkAboutUs" runat="server" Text="About us" Function="AboutUs" /></li>
                </ul>
                <ul>
                    <li id="liContactUs" runat="server">
                        <nhs:Link ID="lnkContactUs" runat="server" Text="Contact us" Function="ContactUs" /></li>
                    <li id="liPrivacyPolicy" runat="server">
                        <nhs:Link ID="lnkPrivacyPolicy" runat="server" Text="Privacy policy" Function="PrivacyPolicy"
                            Target="_blank" /></li>
                    <li id="liTermsOfUse" runat="server">
                        <nhs:Link ID="lnkTermsofuse" runat="server" Text="Terms of use" Function="TermsOfUse"
                            AccessKey="8" /></li>
                </ul>
            </div>
        </div>
        <div class="nhsClear"></div>
    </div>
</div>
