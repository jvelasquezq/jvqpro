using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Nhs.Library.Web;
using Nhs.Library.Common;

namespace Nhs.Web.Controls.Default.Realtor.Common
{
    public partial class email_header : UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            imgHome.Src = XGlobals.Partner.PartnerLogo;
        }
    }
}