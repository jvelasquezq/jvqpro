<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="email_header.ascx.cs" Inherits="Nhs.Web.Controls.Default.Realtor.Common.email_header" %>

<style type="text/css">
body,div,p,table,ul,form,blockquote,h1,h2,h3 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	background-color:#fff;
	text-align: left;
}
a {
	color: #666;
	text-decoration: none;
}
.greeting {
	border: 1px solid #ccc;
	margin: 5px 0 10px 0;
	padding: 5px;
	background-color: #F2F6FA;
}
.header {
	background-color: #24619e;
	color: #fff;
	text-indent: 2pt;
	margin: 5px 0 0 0;
	padding: 2px 0;
	font-weight: bold;
}
.headline {
	font-size: 16px;
	}
.nhsMrktngDesc {
	font-size: 14px;
	font-weight: bold;
	color: #a03;
	}
.header1 {
	background-color: #e4ebf2;
	text-indent: 2pt;
	margin: 5px 0 0 0;
	padding: 2px 0;	
	border-bottom: 1px solid #24619e;
	font-weight: bold;
}
.legaltext { font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #666666; }
.homeheadline { font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #AA0033; }
.promotext { color: #990066; }
.big3result { padding-left: 2px; font-size: 12px; color: #666666; speak: none; }
.smtext { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px; }
.resultthumb { border: 1px solid #003399; margin-right: 4px; }
.resultrow { width:744px; margin-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #333333; }
.narrowfont { font-family: Arial Narrow; font-size: 14px; }
.planname { font-family: arial black; }

.nhsEmail a {
	color: #24619e;
	text-decoration: underline;
	}
.nhsEmailAdditionalInfo {
	background-color: #e4ebf2; 
	border: 1px solid #24619e;
	}
</style>

<table width="600" border="0" cellspacing="0" cellpadding="0" style="padding-bottom: 8px; border-bottom: 1px solid #333;">
	<tr>
	    <td><img id="imgHome" alt="logo" runat=server /></td>		
	</tr>
</table>
