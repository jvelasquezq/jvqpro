﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.SessionState;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Search.Objects;
using Nhs.Search.Objects.Constants;
using Nhs.Web.Service.Core.Constants;
using Nhs.Web.Service.Library;

namespace Bdx.Mvc.Ws.Test.Service
{
    
    
    /// <summary>
    ///This is a test class for WsAlertSearchFacadeTest and is intended
    ///to contain all WsAlertSearchFacadeTest Unit Tests
    ///</summary>
    [TestClass()]
    public class WsAlertSearchFacadeTest
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup()
        {
            HttpContext.Current = null;
        }
        //
        #endregion


        [TestInitialize]
        public void TestSetup()
        {
            // We need to setup the Current HTTP Context as follows:            

            // Step 1: Setup the HTTP Request
            var httpRequest = new HttpRequest("", "http://localhost/", "");

            // Step 2: Setup the HTTP Response
            var httpResponce = new HttpResponse(new StringWriter());

            // Step 3: Setup the Http Context
            var httpContext = new HttpContext(httpRequest, httpResponce);
            var sessionContainer =
                new HttpSessionStateContainer("id",
                                               new SessionStateItemCollection(),
                                               new HttpStaticObjectsCollection(),
                                               10,
                                               true,
                                               HttpCookieMode.AutoDetect,
                                               SessionStateMode.InProc,
                                               false);

            httpContext.Items["AspSession"] =
                typeof(HttpSessionState)
                .GetConstructor(
                                    BindingFlags.NonPublic | BindingFlags.Instance,
                                    null,
                                    CallingConventions.Standard,
                                    new[] { typeof(HttpSessionStateContainer) },
                                    null)
                .Invoke(new object[] { sessionContainer });

            // Step 4: Assign the Context
            HttpContext.Current = httpContext;
        }

        [TestMethod]
        public void BasicTest_Push_Item_Into_Session()
        {
            // Arrange
            var itemValue = "RandomItemValue";
            var itemKey = "RandomItemKey";

            // Act
            HttpContext.Current.Session.Add(itemKey, itemValue);

            // Assert
            Assert.AreEqual(HttpContext.Current.Session[itemKey], itemValue);
        }


        /// <summary>
        ///A test for AlertsSearch
        ///</summary>
        [TestMethod()]
        public void AlertsSearch_Kyle_Test()
        {            
            var target = new WsAlertSearchFacade(); 
            var searchParams = new PropertySearchParams()
            {
                //PartnerId = 1,
                ////City = "Kyle",
                ////CityNameFilter = "Kyle",
                //MarketId = 269,
                //PostalCodeFilter = "78640",
                //PostalCode = "78640",
                //FacetPriceHigh = 170000,
                //FacetPriceLow = 150000,
                //PriceHigh = 170000,
                //PriceLow = 150000,
                ////State = "TX",
                //Radius = 10,
                //OriginLat = 29.989105,
                //OriginLong = -97.877227,
                ////MarketName = "Austin",
                ////ListingTypeFlag = "N",
                ////ComingSoon = true,               
                //AlertId = 0,//1945,
                //SortOrder = SortOrder.Random

                //PartnerId = 1,
                //City = "Atlanta",
                //CityNameFilter = "Atlanta",
                //MarketId = 84,
                ////PostalCodeFilter = "78640",
                ////PostalCode = "78640",
                //FacetPriceHigh = 400000,
                //FacetPriceLow = 150000,
                //PriceHigh = 400000,
                //PriceLow = 150000,
                //State = "GA",
                //Radius = 10,
                //OriginLat = 33.748995,
                //OriginLong = -84.387982,
                ////MarketName = "Austin",
                ////ListingTypeFlag = "N",
                ////ComingSoon = true,               
                //AlertId = 0,//1945,
                //SortOrder = SortOrder.Random


                PartnerId = 1,
                City = "Round Rock",
                CityNameFilter = "Round Rock",
                MarketId = 269,
                //PostalCodeFilter = "78640",
                //PostalCode = "78640",
                FacetPriceHigh = 260000,
                FacetPriceLow = 220000,
                PriceHigh = 260000,
                PriceLow = 220000,
                State = "TX",
                Radius = 10,
                OriginLat = 30.508255,
                OriginLong = -97.678896,
                //MarketName = "Austin",
                //ListingTypeFlag = "N",
                //ComingSoon = true,               
                AlertId = 0,//1945,
                SortOrder = SortOrder.Random
            };             

            var actual = target.AlertsSearch(searchParams);
            Assert.IsTrue(actual != null,"The result is null");
            Assert.IsTrue(actual.CommunityResults != null, "The Comm result is null");
            Assert.IsTrue(actual.CommunityResults.Any(), "The Comm result has no results");
            Assert.IsTrue(10 == actual.CommunityResults.Count, "The alert return just " + actual.CommunityResults.Count + " and must be 10");            
        }

        /// <summary>
        ///A test for GetSearchDataset
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Nhs.Web.Service.Library.dll")]
        public void GetSearchDatasetTest()
        {
            var searchParams = new PropertySearchParams()
            {
                MarketId = 269,
                CityNameFilter = "Kyle",
                City = "Kyle"
            };
            List<DataTable> marketDataSet = null;
            var searchDataFetcher = new SearchDataFetcher();

            // Search alerts search
            if (!string.IsNullOrEmpty(searchParams.PostalCode) && (searchParams.Radius != null && searchParams.Radius != 0) && (searchParams.OriginLat == 0 || searchParams.OriginLong == 0))
                marketDataSet = searchDataFetcher.GetZipSearchData(searchParams.PostalCode, Convert.ToInt16(searchParams.Radius), SearchResultType.Alert);

            // Market search.  Make sure market is valid. 
            if (searchParams.MarketId != 0)
            {
                marketDataSet = searchDataFetcher.GetMarketSearchData(searchParams.MarketId, SearchResultType.Alert);
            }

            var comms = marketDataSet[SearchConst.CommunityTable.Index];

            Assert.IsTrue(comms != null);

            var sear = new AlertSearcher(searchParams, marketDataSet);
            sear.ExecuteSearch();
            Assert.IsTrue(sear.AlertResults.Count > 0);
            var facade = new WsAlertSearchFacade_Accessor();
            var acPArams = new AlertSearcher_Accessor(searchParams,marketDataSet);
            var finalResults = facade.BuildAlertResultsView(searchParams, acPArams);
            Assert.IsTrue(sear.AlertResults.Count == 10);
        }        
    }
}
