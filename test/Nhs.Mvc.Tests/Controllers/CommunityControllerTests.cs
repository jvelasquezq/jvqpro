﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Library.Web;
using Nhs.Mvc.Tests.Helpers;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Controllers;
using Nhs.Web.UI.AppCode.ViewModels;
using System.Web;
using Nhs.Library.Business;
using Nhs.Library.Constants;
using Nhs.Utility.Constants;
using Nhs.Utility.Data.Paging;
using Nhs.Search.Objects;

namespace Nhs.Mvc.Tests.Controllers
{
    /// <summary>
    /// Summary description for CommunityControllerTests
    /// </summary>
    [TestClass]
    public class CommunityControllerTests : NhsTestBase
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        [TestMethod]
        public void CommunityResultsController_IsCommunityControllerDisplayActionActionable()
        {
            //Arrange
            base.SetupHttpContext("http://localhost:9999/communityresults/market-269/1");

            var communityResults = new CommunityResultsController(_communityService, _mapService, _marketService, _iPathMapper, _boylService, _brandService, _builderService, _partnerService, _stateService, _apiService, _basicListingService, _listingService, _marketDFUService);
            //Act
            ActionResult displayResult = communityResults.Show();
            var communitiesViewModel = ((ViewResult)displayResult).ViewData.Model as CommunityResultsViewModel;

            //Assert
            (communitiesViewModel != null).ShouldEqual(true);
            (communitiesViewModel.Results.TotalCommunities > 0).ShouldEqual(true);
            communitiesViewModel.Results.MarketId.ShouldEqual(269);
            
        }
        
        [TestMethod]
        public void CommunityResultsController_4RulesForBuilderPromoArea()
        {
            //Arrange
            base.SetupHttpContext("http://localhost:9999/communityresults/market-223/city-Dayton");

            var communityResults = new CommunityResultsController(_communityService, _mapService, _marketService, _iPathMapper, _boylService, _brandService, _builderService, _partnerService, _stateService, _apiService, _basicListingService, _listingService, _marketDFUService);
            UserSession.SetItem("PreviousSearchParameters", new PropertySearchParams());
            UserSession.SetItem("PreviousSearchParameters", new PropertySearchParams());
            //Act
            ActionResult displayResult = communityResults.Show();
            
            var communitiesViewModel = ((ViewResult)displayResult).ViewData.Model as CommunityResultsViewModel;
            (communitiesViewModel != null).ShouldEqual(true);
        }

        [TestMethod]
        public void CommunityDetailsController_AreCommunityImagesCaptionAndTitleCorrect()
        {
            //Arrange
            base.SetupHttpContext("http://localhost:9999/communitydetail/builder-1000/community-15123");



            var communityController = new CommunityResultsController(_communityService, _mapService, _marketService, _iPathMapper, _boylService, _brandService, _builderService, _partnerService, _stateService, _apiService, _basicListingService, _listingService, _marketDFUService);

            UserSession.SetItem("Refer", "X");

            //Act
            //ActionResult displayCommunity = communityController.Show(15123, 1000);

            //var communitiesViewModel = ((ViewResult)displayCommunity).ViewData.Model as IDetailViewModel;

            ////Assert
            //(communitiesViewModel != null).ShouldEqual(true);
            //(communitiesViewModel.PropertyMediaLinks[0].Title).ShouldEqual("Description1&nbsp;:&nbsp;Caption1");
            //(communitiesViewModel.PropertyMediaLinks[1].Title).ShouldEqual("Caption2");
        }

    }
}
