﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Nhs.Library.Common;
using Nhs.Library.Web;
using Nhs.Mvc.Tests.Helpers;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Filters;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Mvc.Tests.Controllers
{
    /// <summary>
    /// Summary description for ApplicationControllerTests
    /// </summary>
    [TestClass]
    public class ApplicationControllerTests : NhsTestBase
    {
        public ApplicationControllerTests()
        {
            base.SetupHttpContext("http://localhost:9999/chroniclehomes/communityresults/state-texas/market-austin/page-1");
        }

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void ApplicationControllerShellAttributeShouldBeValid()
        {
            //Arrange
            var sut = new ShellFilterAttribute();
            sut.ShellFactory = new ShellFactory();

            var filterContextMock = new Mock<ActionExecutedContext>();

            //Setup viewresult
            var viewResultMock = new Mock<ViewResultBase>();
            filterContextMock.Object.Result = viewResultMock.Object;

            //Setup model
            var viewModel = new BaseViewModel();
            viewResultMock.Object.ViewData.Model = viewModel;

            //Act
            sut.OnActionExecuted(filterContextMock.Object);

            //Assert
            var actionResult = filterContextMock.Object.Result;
            var view = (ViewResultBase)actionResult;
            viewModel = (BaseViewModel)view.ViewData.Model;
            (viewModel.Globals.Shell != null).ShouldEqual(true);
        }

        [TestMethod]
        public void ApplicationControllerPageHeaderAttributeShouldBeValid()
        {
            //Arrange
            IPathMapper pathMapper = new ConcretePathMapper(base.ConfigPath);
            var sut = new PageHeaderAttribute
            {
                MetaReader = new MetaReader(pathMapper)
            };

            var filterContextMock = new Mock<ActionExecutedContext>();

            //Setup viewresult
            var viewResultMock = new Mock<ViewResultBase>();
            filterContextMock.Object.Result = viewResultMock.Object;

            //Setup model
            var viewModel = new BaseViewModel();
            viewResultMock.Object.ViewData.Model = viewModel;

            // Act
            sut.OnActionExecuted(filterContextMock.Object);

            //Assert
            var actionResult = filterContextMock.Object.Result;
            var view = (ViewResultBase)actionResult;
            viewModel = (BaseViewModel)view.ViewData.Model;
            (viewModel.Globals.Title.Length > 0).ShouldEqual(true);
        }

        [TestMethod]
        public void ApplicationControllerLiveChatHeaderAttributeShouldBeValid()
        {
            //Arrange
            var sut = new LiveChatAttribute();
            sut.MarketService = _marketService;
            sut.PathMapper = new ConcretePathMapper(base.ConfigPath);

            var filterContextMock = new Mock<ActionExecutedContext>();

            //Setup viewresult
            var viewResultMock = new Mock<ViewResultBase>();
            filterContextMock.Object.Result = viewResultMock.Object;

            //Setup model
            var viewModel = new BaseViewModel();
            viewResultMock.Object.ViewData.Model = viewModel;

            // Act
            sut.OnActionExecuted(filterContextMock.Object);

            //Assert
            var actionResult = filterContextMock.Object.Result;
            var view = (ViewResultBase)actionResult;
            viewModel = (BaseViewModel)view.ViewData.Model;
            (viewModel.Globals.ActiveEngageHeader.ToString().Contains("Javascript")).ShouldEqual(true);
        }

        [TestMethod]
        public void ApplicationControllerLiveChatHeaderAttributeShouldBeInvalid()
        {
            //Arrange
            base.SetupHttpContext("http://localhost:9999/communityResults/Maryland/Baltimore-Area/1");
            var sut = new LiveChatAttribute();
            sut.MarketService = _marketService;
            sut.PathMapper = new ConcretePathMapper(base.ConfigPath);

            var filterContextMock = new Mock<ActionExecutedContext>();

            //Setup viewresult
            var viewResultMock = new Mock<ViewResultBase>();
            filterContextMock.Object.Result = viewResultMock.Object;

            //Setup model
            var viewModel = new BaseViewModel();
            viewResultMock.Object.ViewData.Model = viewModel;

            // Act
            sut.OnActionExecuted(filterContextMock.Object);

            //Assert
            var actionResult = filterContextMock.Object.Result;
            var view = (ViewResultBase)actionResult;
            viewModel = (BaseViewModel)view.ViewData.Model;
            (viewModel.Globals.ActiveEngageHeader.ToString().Contains("Javascript")).ShouldEqual(false);
        }

        //[TestMethod]
        //public void TestRouteCollection()
        //{
        //    RouteConfigReader configReader = new RouteConfigReader(new ConcretePathMapper(base.ConfigPath));
        //    configReader.GetRoutes();
        //}
    }
}
