﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Mvc.Tests.Helpers;
using Nhs.Web.UI.AppCode.Controllers;
using Nhs.Web.UI.AppCode.ViewModels;
using System.Web.Mvc;

namespace Nhs.Mvc.Tests.Controllers
{
    /// <summary>
    /// Summary description for AccountTest
    /// </summary>
    [TestClass]
    public class SsoControllerTest : NhsTestBase
    {
        string Url = "http://localhost:9999/SSO";

        [TestMethod]
        public void RegisterTest()
        {
            base.SetupHttpContext(Url);
            var controller = new SsoController(_iPathMapper, _SsoService, _partnerService);
            var actionResult = controller.StartSso();
        }  


        [TestMethod]
        public void RegisterTestAccountUserExistInTheSistem()
        {
            base.SetupHttpContext(Url);
            var controller = new AccountController(_userProfileService, _partnerService, _marketService, _SsoService, _iPathMapper, _stateService, _lookupService);
            //base.SetupHttpContextInController(Url, controller);
            var model = new RegisterViewModel()
            {
                Email = "jdoe@joe.com",
                Name = "John Doe",
                Password = "test1234"
            };

            var actionResult = controller.Register(model);
            (actionResult.GetType() == typeof(PartialViewResult)).ShouldEqual(true);
        }

          [TestMethod]
        public void RegisterTestAccountCreatUser()
        {
            base.SetupHttpContext(Url);
            var controller = new AccountController(_userProfileService, _partnerService, _marketService, _SsoService, _iPathMapper,  _stateService, _lookupService);
            //base.SetupHttpContextInController(Url, controller);
            var model = new RegisterViewModel()
            {
                Email = "jdoe@joe.com",
                Name = "John Doe",
                Password = "test1234"
            };

            var actionResult = controller.Register(model);
            (actionResult.GetType() == typeof(RedirectResult)).ShouldEqual(true); 
        }
    }
}
