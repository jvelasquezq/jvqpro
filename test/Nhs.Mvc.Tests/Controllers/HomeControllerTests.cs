﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Library.Web;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Controllers;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Mvc.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTests : NhsTestBase
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        [TestMethod]
        public void HomeController_IsHomeControllerDisplayActionActionable()
        {
            //Arrange
            base.SetupHttpContext("http://localhost:9999");

            UserSession.PersonalCookie.MarketId = 269;
            UserSession.PersonalCookie.SearchText = "Austin, Tx";
            
            var home = new HomeController(_cmsService, _brandService, _lookupService, _stateService, _marketService, _partnerService, new ConcretePathMapper(base.ConfigPath),null);
            
            //Act
            ActionResult displayResult = home.Show();
            Assert.IsInstanceOfType(((ViewResult)displayResult).ViewData.Model, typeof(HomeViewModel));
            var homeViewModel = ((ViewResult)displayResult).ViewData.Model as HomeViewModel;

            //Assert
            Assert.IsNotNull(homeViewModel);
            
        }
    }
}
