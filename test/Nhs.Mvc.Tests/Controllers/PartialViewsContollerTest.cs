﻿using System;
using System.Web;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Mvc.Tests.Helpers;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Controllers;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Library.Business;
using Nhs.Web.UI.AppCode.Extensions;
using System.Linq;

namespace Nhs.Mvc.Tests.Controllers
{
    [TestClass]
    public class PartialViewsContollerTest : NhsTestBase
    {
        private ConcretePathMapper _mapper;

        [TestInitialize]
        public void MyTestInitialize()
        {
            _mapper = new ConcretePathMapper(ConfigPath);
         
        }

        [TestCleanup]
        public void MyTestCleanup()
        {
        }

        [TestMethod]
        public void BreadCrumb_ShouldRenderWithMarketInfo()
        {
            //Arrange
            base.SetupHttpContext("http://localhost:9999/CommunityResults/market-269/1");

            var partialViews = GetPartialViewsController();

            //Act
            ActionResult displayResult = partialViews.BreadCrumb();
            var breadCrumbViewModel = ((ViewResult)displayResult).ViewData.Model as PartialViewModels.BreadcrumbViewModel;

            //Assert
            (breadCrumbViewModel != null).ShouldEqual(true);
            breadCrumbViewModel.State.ToLower().ShouldEqual("tx");
            breadCrumbViewModel.StateName.ToLower().ShouldEqual("texas");
            breadCrumbViewModel.MarketName.ToLower().ShouldEqual("austin");
        }

        

        [TestMethod]
        public void BreadCrumb_ShouldRenderWithoutMarketInfo()
        {
            //Arrange
            base.SetupHttpContext("http://localhost:9999/CommunityResults/state-texas");

            var partialViews = GetPartialViewsController();

            //Act
            ActionResult displayResult = partialViews.BreadCrumb();
            var breadCrumbViewModel = ((ViewResult)displayResult).ViewData.Model as PartialViewModels.BreadcrumbViewModel;

            //Assert
            (breadCrumbViewModel != null).ShouldEqual(true);
            breadCrumbViewModel.IsVisible.ShouldEqual(false);
        }

        [TestMethod]
        public void BreadCrumb_ShouldRenderWithCityInfo()
        {
            //Arrange
            base.SetupHttpContext("http://localhost:9999/CommunityResults/State-Texas/market-269/City-TestCity");

            var partialViews = GetPartialViewsController();

            //Act
            ActionResult displayResult = partialViews.BreadCrumb();
            var breadCrumbViewModel = ((ViewResult)displayResult).ViewData.Model as PartialViewModels.BreadcrumbViewModel;

            //Assert
            (breadCrumbViewModel != null).ShouldEqual(true);
            breadCrumbViewModel.StateName.ToLower().ShouldEqual("texas");
            breadCrumbViewModel.MarketName.ToLower().ShouldEqual("austin");
            breadCrumbViewModel.CityName.ToLower().ShouldEqual("testcity");
        }

        [TestMethod]
        public void BreadCrumb_ShouldRenderWithZipInfo()
        {
            //Arrange
            base.SetupHttpContext("http://localhost:9999/CommunityResults/State-Texas/market-269/PostalCode-90210");

            var partialViews = GetPartialViewsController();

            //Act
            ActionResult displayResult = partialViews.BreadCrumb();
            var breadCrumbViewModel = ((ViewResult)displayResult).ViewData.Model as PartialViewModels.BreadcrumbViewModel;

            //Assert
            (breadCrumbViewModel != null).ShouldEqual(true);
            breadCrumbViewModel.StateName.ToLower().ShouldEqual("texas");
            breadCrumbViewModel.MarketName.ToLower().ShouldEqual("austin");
            breadCrumbViewModel.ZipCode.ToLower().ShouldEqual("90210");
        }

        [TestMethod]
        public void BreadCrumb_ShouldRenderWithCommunityInfo()
        {
            //Arrange
            base.SetupHttpContext("http://localhost:9999/CommunityResults/builder-1178/community-15123/market-269");

            var partialViews = GetPartialViewsController();

            //Act
            ActionResult displayResult = partialViews.BreadCrumb();
            var breadCrumbViewModel = ((ViewResult)displayResult).ViewData.Model as PartialViewModels.BreadcrumbViewModel;

            //Assert
            (breadCrumbViewModel != null).ShouldEqual(true);
            breadCrumbViewModel.StateName.ToLower().ShouldEqual("texas");
            breadCrumbViewModel.MarketName.ToLower().ShouldEqual("austin");
            breadCrumbViewModel.CommunityId.ShouldEqual(15123);
            breadCrumbViewModel.BuilderId.ShouldEqual(1178);
        }

        [TestMethod]
        public void BreadCrumb_ShouldRenderWithPlanInfo()
        {
            //Arrange
            base.SetupHttpContext("http://localhost:9999/CommunityResults/community-15123/plan-859151/market-269");

            var partialViews = GetPartialViewsController();

            //Act
            ActionResult displayResult = partialViews.BreadCrumb();
            var breadCrumbViewModel = ((ViewResult)displayResult).ViewData.Model as PartialViewModels.BreadcrumbViewModel;

            //Assert
            (breadCrumbViewModel != null).ShouldEqual(true);
            breadCrumbViewModel.StateName.ToLower().ShouldEqual("texas");
            breadCrumbViewModel.MarketName.ToLower().ShouldEqual("austin");
            breadCrumbViewModel.CommunityName.ShouldEqual("Steiner Ranch");
        }

        [TestMethod]
        public void SeoLinkPromotion_ReturnsCorrectLinksNumber()
        {
            //Arrange
            base.SetupHttpContext("http://localhost:9999/CommunityResults/market-269");

            var partialViews = GetPartialViewsController();

            //Act
            ActionResult displayResult = partialViews.SeoLinkPromotion();
            var seoLinkPromotionViewModel = ((ViewResult)displayResult).ViewData.Model as PartialViewModels.SeoLinkPromotion;

            //Assert
            seoLinkPromotionViewModel.Links.Count.ShouldEqual(4);
        }


        [TestMethod]
        public void CommunityItemFormatPricerangeCorrectly()
        {
            var exCommResult = new ExtendedCommunityResult();
            exCommResult.PriceLow = 100000;
            exCommResult.PriceHigh = 300000;

            exCommResult.CommunityItemPriceRange().ToString().ShouldEqual("from $100,000-$300,000");
        }

        [TestMethod]
        public void CommunityItemFormatLocationCorrectly()
        {
            var exCommResult = new ExtendedCommunityResult();
            exCommResult.City = "Austin";
            exCommResult.State = "TX";
            exCommResult.PostalCode = "78738";

            exCommResult.CommunityItemLocation().ShouldEqual("Austin, TX 78738");
        }

        [TestMethod]
        public void CommunityItemShowsTheRightThumbnail()
        {
            var exCommResult = new ExtendedCommunityResult();
            
            exCommResult.CommunityImageThumbnail = "/image.jpg";
            exCommResult.CommunityVideoThumbnail = "/video.vma";

            exCommResult.HasVideo = false;
            exCommResult.CommThumbnail().ShouldEqual("[resource:]/image.jpg");

            exCommResult.HasVideo = true;
            exCommResult.CommThumbnail().ShouldEqual("[resource:]/video.vma");
        }

    }
}
