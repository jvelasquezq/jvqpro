﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Library.Web;
using Nhs.Mvc.Tests.Helpers;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Controllers;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Mvc.Tests.Controllers
{
    /// <summary>
    /// Summary description for CommunityControllerTests
    /// </summary>
    [TestClass]
    public class HomeDetailControllerTests : NhsTestBase
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        [TestMethod]
        public void HomeDetailsController_AreInactivePlansNotDisplayed()
        {
            //Arrange
            base.SetupHttpContext("http://localhost:9999/homedetail/planid-12345");


            var homeDetails = new HomeDetailController(_partnerService, _listingService, _communityService, _userProfileService, _lookupService, _stateService, new ConcretePathMapper(base.ConfigPath));
            
            UserSession.SetItem("Refer", "X");

            //Act
            ActionResult displayResult = homeDetails.Show();
            var homeDetailViewModel = ((ViewResult)displayResult).ViewData.Model as HomeDetailViewModel;

            //Assert
            (homeDetailViewModel != null).ShouldEqual(true);
            (homeDetailViewModel.PlanId).ShouldEqual(0);
            
        }


    }
}