﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Mvc.Tests.Helpers;
using Nhs.Web.UI.AppCode.Controllers;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Mvc.Tests.Controllers
{
    [TestClass]
    public class BasicHomeDetailControllerTest : NhsTestBase
    {
        [TestMethod]
        public void BasicHomeDetial()
        {
            //Arrange
            base.SetupHttpContext("http://localhost:9999/basichomedetail/71");
            var basicHomeDetailController = new BasicHomeDetailController(_basicListingService, _communityService, _partnerService, _builderService, _marketService, _affiliateLinkService, _seoContentService);

            //Act
            ActionResult displayResult = basicHomeDetailController.Show(71,"");
            var basicHomeDetailViewModel = ((ViewResult)displayResult).ViewData.Model as BasicHomeDetailViewModel;

            //Assert
            (basicHomeDetailViewModel != null).ShouldEqual(true);
        }
    }
}
