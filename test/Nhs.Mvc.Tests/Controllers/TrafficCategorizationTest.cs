﻿using System;
using System.Linq;
using System.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Nhs.Library.Helpers.Traffic;
using Nhs.Library.Helpers.Traffic;
using Nhs.Library.Web;
using Nhs.Mvc.Tests.Helpers;
using Nhs.Web.UI.AppCode.HtmlHelpers;

namespace Nhs.Mvc.Tests.Controllers
{

    [TestClass]
    public class TrafficCategorizationTest : NhsTestBase
    {
        [TestMethod]
        public void Test_ReferringUrl_and_DestinationUrl_with_IncorrectReferCode()
        {
            const string url = "http://localhost:9999/gs?refer=localfinder";
            const string urlReferrer = "http://greatschools.org/";
            (QueryString(url, urlReferrer, "distribution", HttpUtility.HtmlEncode("great schools"), "free")).ShouldEqual(true);
        }

        [TestMethod]
        public void Test_ReferringUrl_and_DestinationUrl()
        {
            const string url = "http://localhost:9999/msn";
            const string urlReferrer = "http://realestate.msn.com/";
            (QueryString(url, urlReferrer, "distribution", "msn", "free")).ShouldEqual(true);
        }

        [TestMethod]
        public void Test_ReferringUrl_and_ReferCode()
        {
            const string url = "http://localhost:9999/?refer=clrsearch";
            const string urlReferrer = "http://realestate.com/";
            (QueryString(url, urlReferrer, "distribution", "realestate", "paid")).ShouldEqual(true);
        }

        [TestMethod]
        public void Test_DestinationUrl()
        {
            const string url = "http://localhost:9999/yahoorealestate?refer=clrsearch";
            const string urlReferrer = "http://realestate.com/";
            (QueryString(url, urlReferrer, "distribution", "yahoorealestate", "paid")).ShouldEqual(true);
        }

        [TestMethod]
        public void Test_RefferringUrl()
        {
            const string url = "http://localhost:9999/";
            const string urlReferrer = "http://movoto.com/";
            (QueryString(url, urlReferrer, "distribution", "movoto", "free")).ShouldEqual(true);
        }

        [TestMethod]
        public void Test_ReferCode()
        {
            const string url = "c";
            const string urlReferrer = "http://localhost:9999/";
            (QueryString(url, urlReferrer, "distribution", "oodle/facebook", "paid")).ShouldEqual(true);
        }

        [TestMethod]
        public void Test_NotProcess()
        {
            const string url = "http://www.newhomesource.com:80/BrochureGen/email-pmartinez@builderhomesite.com/community-23226/builder-225/leadtype-com?utm_source=recommended&utm_medium=email&utm_campaign=recommended&utm_content=viewbrochure";
            const string urlReferrer = "http://localhost:9999/";
            (QueryString(url, urlReferrer, "email", "recommended", "recommended")).ShouldEqual(true);
        }

        private Boolean QueryString(String url, string urlReferrer, string utmMedium, string utmSource, string utmCampaign)
        {
            NhsUrl nUrl = SetupHttpContext(url);
            ReferrerLookupHelperNew.ReferrerLookupPath = @"C:\Projects\NewHomeSource\NHS7_Features\src\web\Nhs.Web.UI\BHIContent\SEOContent\TrafficSource\ReferrerLookup.xml";
            var trafficHelper = HtmlGoogleHelper.GetSetTrafficCategorization();

            //var request = MvcMockHelpers.GetRequest(trafficHelper);
            //if (!request.QueryString.AllKeys.Any())
            //    return false;
            //if (request.QueryString["utm_medium"] != utmMedium)
            //    return false;
            //if (request.QueryString["utm_source"] != utmSource)
            //    return false;
            //if (request.QueryString["utm_campaign"] != utmCampaign)
            //    return false;
            return true;
        }
    }
}
