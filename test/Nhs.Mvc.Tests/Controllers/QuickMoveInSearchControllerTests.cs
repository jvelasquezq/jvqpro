﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Library.Web;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Controllers;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Mvc.Tests.Helpers;

namespace Nhs.Mvc.Tests.Controllers
{
    [TestClass]
    public class QuickMoveInSearchControllerTests : NhsTestBase
    {

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        [TestMethod]
        public void QuickMoveInSearchController_IsQuickMoveInSearchControllerDisplayActionActionable()
        {
            //Arrange
            base.SetupHttpContext("http://localhost:9999/QuickMoveInSearch");

            UserSession.PersonalCookie.State = "TX";
            UserSession.PersonalCookie.MarketId = 269;

            var quickmovein = new QuickMoveInSearchController(_quickMoveInService, _iPathMapper, _marketService, _stateService, _lookupService, _partnerService);

            //Act
            ActionResult displayResult = quickmovein.Show();
            Assert.IsInstanceOfType(((ViewResult)displayResult).ViewData.Model, typeof(QuickMoveInSearchViewModel));
            var quickViewModel = ((ViewResult)displayResult).ViewData.Model as QuickMoveInSearchViewModel;

            //Assert
            Assert.IsNotNull(quickViewModel);

        }

        [TestMethod]
        public void FindHomesByStateArea()
        {
            base.SetupHttpContext("http://localhost:9999/QuickMoveInSearch");
            var controller = new QuickMoveInSearchController(_quickMoveInService, _iPathMapper, _marketService, _stateService, _lookupService, _partnerService);
            ActionResult displayResult = controller.Show();
            var quickViewModel = ((ViewResult)displayResult).ViewData.Model as QuickMoveInSearchViewModel;
            quickViewModel.StateAbbr = "TX";
            quickViewModel.MarketId = 269;
            quickViewModel.PostalCode = "";

            var actionResult = controller.Show(quickViewModel);
            var valt1 = actionResult.GetType() == typeof(ViewResult);
            var view = (ViewResultBase)actionResult;
            var viewModel = (QuickMoveInSearchViewModel)view.ViewData.Model;
            var valt2 = viewModel.Globals.CanonicalLink.Contains("QuickMoveInSearch") == true;
            (valt1 == true && valt2 == true).ShouldEqual(true);
        }

        [TestMethod]
        public void FindHomesByZip()
        {
            base.SetupHttpContext("http://localhost:9999/QuickMoveInSearch");
            var controller = new QuickMoveInSearchController(_quickMoveInService, _iPathMapper, _marketService, _stateService, _lookupService, _partnerService);
            ActionResult displayResult = controller.Show();
            var quickViewModel = ((ViewResult)displayResult).ViewData.Model as QuickMoveInSearchViewModel;
            quickViewModel.PostalCode = "78756";
            quickViewModel.StateAbbr = "";                        

            var actionResult = controller.Show(quickViewModel);
            var valt1 = actionResult.GetType() == typeof(ViewResult);
            var view = (ViewResultBase)actionResult;
            var viewModel = (QuickMoveInSearchViewModel)view.ViewData.Model;
            var valt2 = viewModel.Globals.CanonicalLink.Contains("QuickMoveInSearch") == true;
            (valt1 == true && valt2 == true).ShouldEqual(true);
        }

    }
}
