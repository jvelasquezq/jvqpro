﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Mvc.Tests.Helpers;
using Nhs.Web.UI.AppCode.Controllers;
using Nhs.Web.UI.AppCode.ViewModels;
using System.Web.Mvc;

namespace Nhs.Mvc.Tests.Controllers
{
    /// <summary>
    /// Summary description for AccountTest
    /// </summary>
    [TestClass]
    public class AccountControllerTest : NhsTestBase
    {
        String Url = "http://localhost:9999/RegisterNew";

        [TestMethod]
        public void RegisterTest()
        {
            base.SetupHttpContext(Url);
            var controller = new AccountController(_userProfileService, _partnerService, _marketService, _SsoService, _iPathMapper, _stateService, _lookupService);
            var actionResult = controller.RegisterOnPage();
            var valt1 = actionResult.GetType() == typeof(ViewResult);
            var view = (ViewResultBase)actionResult;
            var viewModel = (BaseViewModel)view.ViewData.Model;
            var valt2 = viewModel.Globals.CanonicalLink.Contains("RegisterNew") == true;
            (valt1 == true && valt2 == true).ShouldEqual(true);
        }


        [TestMethod]
        public void RegisterTestAccountUserExistInTheSistem()
        {
            base.SetupHttpContext(Url);
            var controller = new AccountController(_userProfileService, _partnerService, _marketService, _SsoService, _iPathMapper,  _stateService, _lookupService);
            //base.SetupHttpContextInController(Url, controller);
            var model = new RegisterViewModel()
            {
                Email = "jdoe@joe.com",
                Name = "John Doe",
                Password = "test1234"
            };

            var actionResult = controller.Register(model);
            (actionResult.GetType() == typeof(PartialViewResult)).ShouldEqual(true);
        }

          [TestMethod]
        public void RegisterTestAccountCreatUser()
        {
            base.SetupHttpContext(Url);
            var controller = new AccountController(_userProfileService, _partnerService, _marketService, _SsoService, _iPathMapper, _stateService, _lookupService);
            //base.SetupHttpContextInController(Url, controller);
            var model = new RegisterViewModel()
            {
                Email = "jdoe@joe.com",
                Name = "John Doe",
                Password = "test1234"
            };

            var actionResult = controller.Register(model);
            (actionResult.GetType() == typeof(RedirectResult)).ShouldEqual(true); 
        }
    }
}
