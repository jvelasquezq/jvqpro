﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Mvc.Tests.Helpers;
using Nhs.Web.UI.AppCode.Controllers;
using Nhs.Web.UI.AppCode.ViewModels;
using Nhs.Web.UI.AppCode.ViewModels.LeadView;

namespace Nhs.Mvc.Tests.Controllers
{
     [TestClass]
    public class FreeBrochureControllerTests : NhsTestBase
    {
         [TestMethod]
         public void CallFreeRequestBrochure()
         {
             var model = GetModel(LeadAction.FreeBrochure);
             model.IsFreeBrochure.ShouldEqual(true);
         }

         [TestMethod]
         public void CallRequestApointment()
         {
             var model = GetModel(LeadAction.RequestApointment);
             model.IsAppointment.ShouldEqual(true);
         }

         [TestMethod]
         public void CallRequestPromotion()
         {
             var model = GetModel(LeadAction.RequestPromotion);
             model.IsSpecialOffer.ShouldEqual(true);
         }

         private LeadViewModalModel GetModel(string leadAction)
         {
             SetupHttpContext();
             var url = "http://localhost:9999/freebrochure/requestbrochure/?LeadType=com&LeadAction=" + leadAction + "&CommunityList=58630&Builder=12328&FromPage=communityresults&Market=269&KeepThis=true&TB_iframe=true&width=700&height=425";
             var controller = new FreeBrochureController(null, _listingService, _communityService, _marketService, _stateService, _partnerService, null, _iPathMapper, _userProfileService, _brandService, null, _apiService);
             SetupHttpContextInController(url, controller);

             var model = controller.RequestBrochure();
             return ((ViewResultBase)(model)).Model as LeadViewModalModel;
         }
    }
}
