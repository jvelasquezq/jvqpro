﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Library.Web;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Controllers;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Mvc.Tests.Controllers
{
    [TestClass]
    public class HotGreenSearchControllerTests : NhsTestBase
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        [TestMethod]
        public void HotGreenSearchController_IsHotDealsSearchControllerDisplayActionActionable()
        {
            //Arrange
            base.SetupHttpContext("http://localhost:9999/hotdealssearch");

            UserSession.PersonalCookie.State = "TX";            

            var hotdeals = new HotGreenSearchController(_hotGreenService, new ConcretePathMapper(base.ConfigPath), _stateService, _lookupService, _marketService);

            //Act
            ActionResult displayResult = hotdeals.Show();
            Assert.IsInstanceOfType(((ViewResult)displayResult).ViewData.Model, typeof(HotGreenSearchViewModel));
            var hotgreenViewModel = ((ViewResult)displayResult).ViewData.Model as HotGreenSearchViewModel;

            //Assert
            Assert.IsNotNull(hotgreenViewModel);

        }

        [TestMethod]
        public void HotGreenSearchController_IsGreenSearchControllerDisplayActionActionable()
        {
            //Arrange
            base.SetupHttpContext("http://localhost:9999/greensearchnew");

            UserSession.PersonalCookie.State = "TX";

            var greensearch = new HotGreenSearchController(_hotGreenService, new ConcretePathMapper(base.ConfigPath), _stateService, _lookupService, _marketService);

            //Act
            ActionResult displayResult = greensearch.Show();
            Assert.IsInstanceOfType(((ViewResult)displayResult).ViewData.Model, typeof(HotGreenSearchViewModel));
            var hotgreenViewModel = ((ViewResult)displayResult).ViewData.Model as HotGreenSearchViewModel;

            //Assert
            Assert.IsNotNull(hotgreenViewModel);

        }
    }
}
