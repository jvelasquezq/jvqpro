using System;
using System.Collections.Generic;
using System.Linq;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Tests.FakeRepositories
{
    internal class FakeMarketRepository : IMarketRepository
    {
        private List<Market> _markets;
        private List<PartnerMarket> _partnerMarkets;

        #region IMarketRepository Members

        public FakeMarketRepository()
        {
            _markets = new List<Market>() { 
                new Market() { MarketId = 269,  MarketName = "Austin", Latitude = new decimal(30.266898), Longitude = new decimal(-97.742798), State = new State () {StateAbbr = "TX", StateName = "Texas"}, Cities = new List<City> { new City() { CityName = "Cedar Park", MarketId = 269} } },
                new Market() { MarketId = 279,  MarketName = "Houston", Latitude = new decimal(29.7631), Longitude = new decimal(-95.3630), State = new State () {StateAbbr = "TX", StateName = "Texas"}, Cities = new List<City> { new City() { CityName = "Alvin", MarketId = 279} } },
                new Market() { MarketId = 275,  MarketName = "Dallas", Latitude = new decimal(32.783298), Longitude = new decimal(-96.800003), State = new State () {StateAbbr = "TX", StateName = "Texas"}, Cities = new List<City> { new City() { CityName = "Anna", MarketId = 279} } },
                new Market() { MarketId = 223,  MarketName = "Dayton-Springfield", State = new State () {StateAbbr = "OH", StateName = "Dayton"}}
            };

            _partnerMarkets = new List<PartnerMarket>
                                  {
                                      new PartnerMarket { PartnerId = 1, MarketId = 223, MarketName = "Dayton-Springfield", State = "Dayton", Market = new Market { MarketId = 223, MarketName = "Dayton-Springfield" } },
                                      new PartnerMarket { PartnerId = 1, MarketId = 269,  MarketName = "Austin", State = "Texas",  Market = new Market() { MarketId = 269,  MarketName = "Austin", State = new State () {StateAbbr = "TX", StateName = "Texas"},  Cities = new List<City> { new City() { CityName = "Cedar Park", MarketId = 269,  County =  ""} } }  }
                                  };
        }

        public IQueryable<Market> Markets
        {
            get { return _markets.AsQueryable(); }
        }

        public IEnumerable<PartnerMarketCity> GetPartnerMarketsInfo(int partnerId)
        {
            throw new NotImplementedException();
        }

        public GeoLocation GetGeoLocationByPostalCode(string postalCode)
        {
            throw new NotImplementedException();
        }

        public IQueryable<PartnerMarket> GetPartnerMarkets(int partnerId)
        {
            return _partnerMarkets.AsQueryable();
        }

        #endregion
    }
}
