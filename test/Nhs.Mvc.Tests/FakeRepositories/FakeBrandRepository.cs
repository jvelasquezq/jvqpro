﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Hub;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Tests.FakeRepositories
{
    class FakeBrandRepository : IBrandRepository
    {
        private List<Brand> _brands;

        public FakeBrandRepository()
        {
            List<Builder> builders = new List<Builder>();
            var bldr = new Builder();
            bldr.BuilderId = 34;
            bldr.BuilderName = "Lennar";
            builders.Add(bldr);

            _brands = new List<Brand>
                          {
                              new Brand { BrandId = 15, BrandName = "Lennar Homes",  Builders = builders,   LogoSmall = "1x1.gif" },
                              new Brand() {BrandId =9, BrandName = "David Weekley",   LogoSmall = "1x1.gif" },
                              new Brand() {BrandId = 8, BrandName = "Highland Homes",   LogoSmall = "1x1.gif" },
                              new Brand() {BrandId = 12, BrandName = "Morrison Homes",   LogoSmall = "1x1.gif" },
                              new Brand() {BrandId = 14, BrandName = "Medallion Homes",   LogoSmall = "1x1.gif" }
                               };
        }
        #region IBrandRepository Members

        public IQueryable<Domain.Model.Web.Brand> Brands
        {
            get { return _brands.AsQueryable<Brand>(); }
        }

        public IQueryable<HubBrand> HubBrands
        {
            get { return null; }
        }

        public IQueryable<HubBrandShowCase> HubBrandShowCase
        {
            get { return null; }
        }
        #endregion
    }
}
