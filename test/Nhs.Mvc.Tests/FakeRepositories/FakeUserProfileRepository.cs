﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Profiles;

namespace Nhs.Mvc.Tests.FakeRepositories
{
    public class FakeUserProfileRepository : IUserProfileRepository
    {
        private IList<UserProfile> _users;

        public FakeUserProfileRepository()
        {
            _users = new List<UserProfile>();

            UserProfile user1 = new UserProfile();
            user1.FirstName = "John";
            user1.LastName = "Doe";
            user1.MiddleName = "L";
            user1.PartnerId = 1;
            user1.LogonName = "jdoe@joe.com";
            user1.UserGuid = "{01056c5e-eeef-48c9-818e-746ce907a238}";
            user1.DateRegistered = DateTime.Now;
            user1.Password = "password";
            _users.Add(user1);
        }

        public IQueryable<UserProfile> Users
        {
            get { return _users.AsQueryable(); }
        }

        public void CreateProfile(UserProfile profile)
        {
            _users.Add(profile);
        }

        public void UpdateProfile(UserProfile profile)
        {
            throw new NotImplementedException();
        }
    }
}
