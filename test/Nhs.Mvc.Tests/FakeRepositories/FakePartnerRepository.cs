﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Tests.FakeRepositories
{
    public class FakePartnerRepository : IPartnerRepository
    {
        private IList<Partner> _partners;

        public FakePartnerRepository()
        {
            _partners = new List<Partner>()
                        {
                            new Partner(){PartnerId = 1, BrandPartnerId = 1, PartnerName    = "NewHomeSource", PartnerSiteUrl = "NewHomeSource"},
                            new Partner(){PartnerId = 2, BrandPartnerId = 1, PartnerName    = "Chronicle Homes", PartnerSiteUrl = "ChronicleHomes"}
                        };
        }

        public IQueryable<Partner> Partners
        {
            get { return _partners.AsQueryable(); }
        }


        public IQueryable<PartnerMarketAbout> PartnersMarketFacebookUrls
        {
            get { return new List<PartnerMarketAbout>().AsQueryable(); }
        }
    }
}
