﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Tests.FakeRepositories
{
    class FakeCommunityRepository : ICommunityRepository
    {
        private List<Community> _communities;
        private List<TollFreeNumber> _tollFreeNumbers;
        private IMarketRepository _marketRepository;

        public FakeCommunityRepository(IMarketRepository marketRepository)
        {
            _marketRepository = marketRepository;

            _communities = new List<Community>
                               {
                                   
                                    new Community
                                       {
                                           CommunityId = 235,
                                           CommunityName = "Steiner Ranch",
                                           City = "Austin",
                                           Market = new Market {MarketId = 269, PartnerMarkets = { new PartnerMarket() { MarketId  = 269, PartnerId = 1}}, MarketName = "Austin", State = new State(){StateAbbr = "TX", StateName = "Texas"}},
                                           PostalCode = "78750",
                                           Builder = new Builder() {BuilderId = 1000, BuilderName = "Builder 1330", Url = "builderUrl"},
                                           State = new State(){StateAbbr = "TX", StateName = "Texas"},
                                           Brand = new Brand(){BrandId = 15, BrandName = "Lennar Homes"},
                                           SalesOffice = new SalesOffice() { Address1 =  "Test Address 1", Address2 = "Test Address 2"},
                                           AllImages = {new Image() {BuilderId = 1000, CommunityId = 15123, ImageId = 1, ImageDescription = "Caption1", ImageTypeCode = "COM", ImageTitle = "Description1", ImageName = "Img 1.jpg"}, 
                                                        new Image() {BuilderId = 1000, CommunityId = 15123, ImageId = 2, ImageDescription = "Caption2", ImageTypeCode = "COM", ImageName = "Img 2.jpg"},
                                                        new Image() {BuilderId = 1000, CommunityId = 15123, ImageId = 3, ImageDescription = "Caption3", ImageTypeCode = "ASL", ImageName = "Img 3.jpg"}},
                                           
                                       },

                                   new Community
                                       {
                                           CommunityId = 15123,
                                           CommunityName = "Steiner Ranch",
                                           City = "Austin",
                                           Market = new Market {MarketId = 269, PartnerMarkets = { new PartnerMarket() { MarketId  = 269, PartnerId = 1}}, MarketName = "Austin", State = new State(){StateAbbr = "TX", StateName = "Texas"}},
                                           PostalCode = "78750",
                                           Builder = new Builder() {BuilderId = 1000, BuilderName = "Builder 1000", Url = "builderUrl"},
                                           State = new State(){StateAbbr = "TX", StateName = "Texas"},
                                           Brand = new Brand(){BrandId = 15, BrandName = "Lennar Homes"},
                                           SalesOffice = new SalesOffice() { Address1 =  "Test Address 1", Address2 = "Test Address 2"},
                                           AllImages = {new Image() {BuilderId = 1000, CommunityId = 15123, ImageId = 1, ImageDescription = "Caption1", ImageTypeCode = "COM", ImageTitle = "Description1", ImageName = "Img 1.jpg"}, 
                                                        new Image() {BuilderId = 1000, CommunityId = 15123, ImageId = 2, ImageDescription = "Caption2", ImageTypeCode = "COM", ImageName = "Img 2.jpg"},
                                                        new Image() {BuilderId = 1000, CommunityId = 15123, ImageId = 3, ImageDescription = "Caption3", ImageTypeCode = "ASL", ImageName = "Img 3.jpg"}},
                                           
                                       },
                                     
                                    new Community
                                       {
                                           CommunityId = 15124,
                                           CommunityName = "Avery Ranch",
                                           City = "Austin",
                                           Market = new Market {MarketId = 269, MarketName = "Austin", State = new State(){StateAbbr = "TX", StateName = "Texas"}},
                                           PostalCode = "78717",
                                           State = new State(){StateAbbr = "TX", StateName = "Texas"},
                                           Brand = new Brand(){BrandId = 15, BrandName = "Lennar Homes"}
                                       },
                                    new Community
                                       {
                                           CommunityId = 15124,
                                           CommunityName = "Avery Ranch Brookside",
                                           City = "Austin",
                                           Market = new Market {MarketId = 269, MarketName = "Austin", State = new State(){StateAbbr = "TX", StateName = "Texas"}},
                                           PostalCode = "78717",
                                           State = new State(){StateAbbr = "TX", StateName = "Texas"},
                                           Brand = new Brand(){BrandId = 12, BrandName = "Morrison Homes"}
                                       },
                                    new Community
                                       {
                                           CommunityId = 15125,
                                           CommunityName = "Lakemont",
                                           City = "Houston",
                                           Market = new Market {MarketId = 279, MarketName = "Houston", State = new State(){StateAbbr = "TX", StateName = "Texas"}},
                                           PostalCode = "77036",
                                           State = new State(){StateAbbr = "TX", StateName = "Texas"},
                                           Brand = new Brand(){BrandId = 15, BrandName = "Lennar Homes"}
                                       },
                                    new Community
                                       {
                                           CommunityId = 15126,
                                           CommunityName = "Creekside",
                                           City = "Houston",
                                           Market = new Market {MarketId = 279, MarketName = "Houston", State = new State(){StateAbbr = "TX", StateName = "Texas"}},
                                           PostalCode = "77478",
                                           State = new State(){StateAbbr = "TX", StateName = "Texas"},
                                           Brand = new Brand(){BrandId = 8, BrandName = "Highland Homes"}
                                       },
                                    new Community
                                       {
                                           CommunityId = 15127,
                                           CommunityName = "Lakes of Fairhaven",
                                           City = "Houston",
                                           Market = new Market {MarketId = 279, MarketName = "Houston", State = new State(){StateAbbr = "TX", StateName = "Texas"}},
                                           PostalCode = "77479",
                                           State = new State(){StateAbbr = "TX", StateName = "Texas"},
                                           Brand = new Brand(){BrandId = 9, BrandName = "David Weekley"}
                                       }
                               };

            _tollFreeNumbers = new List<TollFreeNumber>()
                                   {
                                       new TollFreeNumber(){PartnerId = 1, BuilderId = 500, CommunityId = 25000}
                                   };
        }

        public bool UseHub { get; set; }

        public IQueryable<Community> Communities
        {
            get { return _communities.AsQueryable(); }
        }

        public IQueryable<CommunityPromotion> CommunityPromotions
        {
            get { return null; }
        }

        public IQueryable<CommunityAgentPolicy> CommunityAgentPolicies
        {
            get { return null; }
        }

        public IQueryable<CommunityEvent> CommunityEvents
        {
            get { return null; }
        }
        public IQueryable<TollFreeNumber> TollFreeNumbers
        {
            get { return _tollFreeNumbers.AsQueryable(); }
        }



        #region ICommunityRepository Members


        public IQueryable<Domain.Model.Hub.HubCommunity> HubCommunities
        {
            get { throw new NotImplementedException(); }
        }

        #endregion

        #region ICommunityRepository Members


        public IQueryable<Domain.Model.Hub.HubTollFreeNumber> HubTollFreeNumbers
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
    }
}
