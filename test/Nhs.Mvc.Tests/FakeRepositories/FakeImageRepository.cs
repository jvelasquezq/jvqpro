﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Hub;
using Nhs.Mvc.Domain.Model.Hub.Interfaces;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Tests.FakeRepositories
{
    public class FakeImageRepository:IImageRepository
    {
        private IList<Image> _images;

        public FakeImageRepository()
        {
            _images = new List<Image>();
        }

        public bool UseHub { get; set; }

        public IQueryable<IImage> Images
        {
            get { return _images.AsQueryable(); }
        }

        public IQueryable<HubImage> HubImages { get; private set; }
    }
}
