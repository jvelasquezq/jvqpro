﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Tests.FakeRepositories
{
    class FakeLookupRepository : ILookupRepository
    {
        private IList<LookupGroup> _lookupGroups;

        public FakeLookupRepository()
        {
            _lookupGroups = new List<LookupGroup>
                          {
                              new LookupGroup {GroupName = "PRICEDD", LookupText = "$60,000", LookupValue = "60000", SortOrder = 1},
                              new LookupGroup {GroupName = "PRICEDD", LookupText = "$70,000", LookupValue = "70000", SortOrder = 2},
                              new LookupGroup {GroupName = "PRICEDD", LookupText = "$80,000", LookupValue = "80000", SortOrder = 3},
                              new LookupGroup {GroupName = "PRICEDD", LookupText = "$90,000", LookupValue = "90000", SortOrder = 4},
                              new LookupGroup {GroupName = "NUMSTORIES", LookupText = "1", LookupValue = "1", SortOrder = 1},
                              new LookupGroup {GroupName = "NUMSTORIES", LookupText = "2", LookupValue = "2", SortOrder = 2},
                              new LookupGroup {GroupName = "NUMSTORIES", LookupText = "3", LookupValue = "3", SortOrder = 3},
                              new LookupGroup {GroupName = "NUMSTORIES", LookupText = "Any", LookupValue = "-1", SortOrder = 0}
                          };
        }

        #region ILookupRepository Members

        public IQueryable<LookupGroup> LookupItems
        {
            get { return _lookupGroups.AsQueryable(); }
        }

        #endregion
    }
}
