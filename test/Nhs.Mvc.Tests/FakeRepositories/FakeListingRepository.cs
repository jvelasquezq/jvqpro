﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Domain.Model.Web.Interfaces;

namespace Nhs.Mvc.Tests.FakeRepositories
{
    public class FakeListingRepository:IListingRepository
    {
        private List<Plan> _plans;
        private List<Spec> _specs;

        public FakeListingRepository()
        {
            _plans = new List<Plan>()
                         {
                             new Plan() { AllPlanImages = new Collection<Image>(), 
                                          PlanId = 12345, PlanName = "Test Plan 1", Price = 100000, 
                                          Bathrooms = 3, Bedrooms = 4, CommunityId = 15123, Status = "Inactive", Garages = 1, PlanTypeCode = "SF",
                                          Community = new Community()
                                                          {
                                                               CommunityId = 15123,
                                                               CommunityName = "Steiner Ranch",
                                                               City = "Austin",
                                                               Market = new Market {MarketId = 269, PartnerMarkets = { new PartnerMarket() { MarketId  = 269, PartnerId = 1}}, MarketName = "Austin", State = new State(){StateAbbr = "TX", StateName = "Texas"}},
                                                               PostalCode = "78750",
                                                               Builder = new Builder() {BuilderId = 1000, BuilderName = "Builder 1000", Url = "builderUrl" , Brand = new Brand(){BrandId = 15, BrandName = "Lennar Homes"}},
                                                               State = new State(){StateAbbr = "TX", StateName = "Texas"},
                                                               Brand = new Brand(){BrandId = 15, BrandName = "Lennar Homes"},
                                                               SalesOffice = new SalesOffice() { Address1 =  "Test Address 1", Address2 = "Test Address 2", }
                                                          }
                                         }
                         };
            _specs = new List<Spec>();
        }

        public IQueryable<IPlan> Plans
        {
            get { return _plans.AsQueryable(); }
        }

        public IQueryable<ISpec> Specs
        {
            get { return _specs.AsQueryable(); }
        }

        public IQueryable<IPlan> PlansWithImages
        {
            get { throw new NotImplementedException(); }
        }

        public IQueryable<ISpec> SpecsWithImages
        {
            get { throw new NotImplementedException(); }
        }

        #region IListingRepository Members

        public IQueryable<Domain.Model.Hub.HubPlan> HubPlans
        {
            get { throw new NotImplementedException(); }
        }

        public IQueryable<Domain.Model.Hub.HubSpec> HubSpecs
        {
            get { throw new NotImplementedException(); }
        }

        public IQueryable<Domain.Model.Hub.HubPlan> HubPlansWithImages
        {
            get { throw new NotImplementedException(); }
        }

        public IQueryable<Domain.Model.Hub.HubSpec> HubSpecsWithImages
        {
            get { throw new NotImplementedException(); }
        }

        #endregion

        #region IListingRepository Members

        public bool UseHub
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion
    }
}
