using System;
using System.Collections.Generic;
using System.Linq;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Tests.FakeRepositories
{
    internal class FakeStateRepository : IStateRepository
    {
        private IList<State> _states;

        public FakeStateRepository()
        {
            _states = new List<State>
                          {
                              new State {StateAbbr = "TX", StateName = "Texas"},
                              new State {StateAbbr = "CA", StateName = "California"},
                              new State {StateAbbr = "NY", StateName = "New York"},
                              new State {StateAbbr = "AZ", StateName = "Arizona"}
                          };
        }

        public IQueryable<State> States
        {
            get { return _states.AsQueryable(); }
        }
    }
}