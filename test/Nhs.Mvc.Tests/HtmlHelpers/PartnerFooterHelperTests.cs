﻿using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Library.Web;
using Nhs.Mvc.Tests.Helpers;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Controllers;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Web.UI.AppCode.ViewModels;

namespace Nhs.Mvc.Tests.HtmlHelpers
{
    [TestClass]
    public class PartnerFooterHelperTests : NhsTestBase
    {
        private ConcretePathMapper _mapper;

        [TestInitialize]
        public void MyTestInitialize()
        {
            _mapper = new ConcretePathMapper(ConfigPath);

        }
        public PartnerFooterHelperTests()
        {

        }

        [TestMethod]
        public void PartnerFooterHelperShouldRenderCorrectAds()
        {
            MvcMockHelpers.FakeUserSession();


            var partialViews = GetPartialViewsController();

            //Act
            ActionResult displayResult = partialViews.PartnerFooter();
            var partnerFooter = ((ViewResult)displayResult).ViewData.Model as PartialViewModels.PartnerFooter;

            //Assert
            (partnerFooter != null).ShouldEqual(true);
        }
    }
}
