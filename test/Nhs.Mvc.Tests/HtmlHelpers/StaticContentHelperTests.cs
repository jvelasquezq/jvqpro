﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Mvc.Tests.Helpers;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.HtmlHelpers;

namespace Nhs.Mvc.Tests.HtmlHelpers
{
    /// <summary>
    /// Summary description for StaticContentHelperTests
    /// </summary>
    [TestClass]
    public class StaticContentHelperTests : NhsTestBase
    {
        public StaticContentHelperTests()
        {
            base.SetupHttpContext("http://localhost:9999/dummyresults/Texas/Austin/1");
        }

        [TestMethod]
        public void StaticContentHelper_ShouldRenderCorrectStaticContent()
        {
            //Arrange
            const string testHtml = "<html></html>";

            //Act
            var html = HtmlHelper.StaticContent(testHtml);

            //Assert
            html.ToString().ShouldEqual(testHtml);
        }

        [TestMethod]
        public void StaticContentHelper_ShouldRenderCorrectStaticContentAndContentTags()
        {
            //Arrange
            const string testHtml = "<html>[MarketName]</html>";
            var tags = new List<ContentTag> { new ContentTag
                                                    { TagKey = ContentTagKey.MarketName, 
                                                        TagValue = "Austin" } 
                                                    };

            //Act
            var html = HtmlHelper.StaticContent(tags, testHtml);

            //Assert
            html.ToString().ShouldEqual(string.Format("<html>{0}</html>", "Austin"));
        }

        [TestMethod]
        public void StaticContentHelper_ShouldRenderCorrectStaticFileContent()
        {
            //Arrange
            IPathMapper pathMapper = new ConcretePathMapper(base.ConfigPath);
            //Act
            var html = HtmlHelper.StaticContent("conversion.htm", pathMapper, "");

            //Assert
            html.ToString().ToLower().Contains("google").ShouldEqual(true);
        }

        [TestMethod]
        public void StaticContentHelper_ShouldRenderCorrectStaticFileContentAndContentTags()
        {
            //Arrange
            IPathMapper pathMapper = new ConcretePathMapper(base.ConfigPath);
            var tags = new List<ContentTag> { new ContentTag
                                                    { TagKey = ContentTagKey.MarketName, 
                                                        TagValue = "Austin" } 
                                                  };

            //Act
            var html = HtmlHelper.StaticContent("defaultmarketcondoheader.htm", tags, pathMapper, "");

            //Assert
            html.ToString().Contains("Austin").ShouldEqual(true);
        }
    }
}
