﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.HtmlHelpers;

namespace Nhs.Mvc.Tests.Extensions
{
    /// <summary>
    /// Summary description for TrackingScriptExtensionTests
    /// </summary>
    [TestClass]
    public class TrackingScriptExtensionTests : NhsTestBase
    {

        [TestMethod]
        public void TrackingScriptShouldReturnContent()
        {
            base.SetupHttpContext();
            
            var pathMapper = new ConcretePathMapper(ConfigPath);
            var trackingScript = HtmlHelper.TrackingScript("conversion.htm", pathMapper, "");
            Assert.IsFalse(string.IsNullOrEmpty(trackingScript.ToString()));
        }
    }
}
