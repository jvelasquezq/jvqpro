﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Mvc.Tests.Helpers;
using Nhs.Web.UI.AppCode.HtmlHelpers;
using Nhs.Library.Web;
using System.Globalization;
using Nhs.UrlRewriter.Configuration;
using Nhs.UrlRewriter.Utils;

namespace Nhs.Mvc.Tests.HtmlHelpers
{
    [TestClass]
    public class AdHelperTests : NhsTestBase
    {
        public AdHelperTests()
        {
            base.SetupHttpContext("http://localhost:9999/chroniclehomes/dummyresults/state-texas/market-austin/1");
        }

        [TestMethod]
        public void AdHelperShouldRenderCorrectAds()
        {

            //helper parameters
            string position = "Top";
            bool useIFrame = false;
            bool javaScriptParams = false;
            AdController adController = new AdController();
            adController.AddBuilderParameter(21);
            adController.AddMarketParameter(269);
            adController.AddZipParameter("78737");
            adController.AdPageName = "cr";

            //Act
            var html = HtmlHelper.AdControl(position, useIFrame, javaScriptParams, adController);

            int randomNumber = adController.RandomNumber;
            //string compareString = string.Format("http://oascentral.newhomesource.com/RealMedia/ads/adstream_nx.ads/nhs.com/cr/{0}@Top?prt=1&b0021&m=269&m269&z78737", randomNumber);
            string compareString = string.Format("http://oascentral.newhomesource.com/RealMedia/ads/adstream_jx.ads/nhs.com/cr/{0}@Top?prt=2&b0021&m=269&m269&z78737", randomNumber);

            //Assert
            html.ToString().Contains(compareString).ShouldEqual(true);
        }

        [TestMethod]
        public void AdHelperShouldRenderCorrectAdsOnIFrame()
        {

            //helper parameters
            string position = "Top";
            bool useIFrame = true;
            bool javaScriptParams = false;
            AdController adController = new AdController();
            adController.AddBuilderParameter(21);
            adController.AddMarketParameter(269);
            adController.AddZipParameter("78737");
            adController.AdPageName = "cr";

            //Act
            var html = HtmlHelper.AdControl(position, useIFrame, javaScriptParams, adController);

            int randomNumber = adController.RandomNumber;
            string compareString = string.Format("src=\"http://oascentral.newhomesource.com/RealMedia/ads/adstream_sx.cgi/nhs.com/cr/{0}@Top?prt=2&b0021&m=269&m269&z78737\"></iframe>", randomNumber);

            //Assert
            html.ToString().Contains(compareString).ShouldEqual(true);
        }

        [TestMethod]
        public void AdHelperShouldRenderCorrectAdsFromJavaScriptParams()
        {

            //helper parameters
            string position = "Top";
            bool useIFrame = false;
            bool javaScriptParams = true;
            AdController adController = new AdController();
            adController.AddBuilderParameter(21);
            adController.AddMarketParameter(269);
            adController.AddZipParameter("78737");
            adController.AdPageName = "cr";

            //Act
            var html = HtmlHelper.AdControl(position, useIFrame, javaScriptParams, adController);

            int randomNumber = adController.RandomNumber;

            string compareString = @"<script type=""text/javascript"">
				/* <![CDATA[ */
				var nhsPageName = ""cr"";
				var nhsState = """";
				var nhsMarket = 269;
				var nhsCommunity = ""0"";
				var nhsCityString = """";
				var nhsZipString = ""z78737"";
				var nhsBuilderString = ""b0021"";
				// Build Arrays
				var nhsCities = nhsCityString.split('&amp;');
				var nhsZipString = nhsZipString.split('&amp;');
				var nhsBuilders = nhsBuilderString.split('&amp;');
				/* ]]> */
				</script>";

            CompareOptions compareOptions = CompareOptions.IgnoreCase
                            | CompareOptions.IgnoreWidth
                            | CompareOptions.IgnoreNonSpace;

            //Assert
            string.Compare(html.ToString(), compareString, CultureInfo.CurrentCulture, compareOptions).ShouldEqual(0);

        }


    }
}
