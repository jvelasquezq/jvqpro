﻿using Nhs.Library.Constants;
using Nhs.Library.Helpers.Utility;
using Nhs.Utility.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Nhs.Mvc.Tests
{
    
    
    /// <summary>
    ///This is a test class for StringHelperExtensionsTest and is intended
    ///to contain all StringHelperExtensionsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class StringHelperExtensionsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for LastPart
        ///</summary>
        [TestMethod()]
        public void LastPartTest()
        {
            const string stringToProcess = "FistName LastName ";
            const string delimiter = " ";
            const string expected = "LastName";
            string actual = stringToProcess.LastPart(delimiter);
            Assert.AreEqual(expected, actual);           
        }

        /// <summary>
        ///A test for FirstPart
        ///</summary>
        [TestMethod()]
        public void FirstPartTest()
        {
            const string stringToProcess = "FistName LastName ";
            const string delimiter = " ";
            const string expected = "FirstName";
            string actual = stringToProcess.FirstPart(delimiter);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CryptoDecryptoTest()
        {
            const string text = "humberto@email.com";
            const string password = CookieConst.EncEmail;
            var ctext = CryptoHelper.Encrypt(text, password);
            var dtext = CryptoHelper.Decrypt(ctext, password);
            Assert.AreEqual(text,dtext);
        }
    }
}
