﻿using System;
using System.Reflection;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Helpers.Utility;
using Nhs.Search.Objects;
using Nhs.Utility.Data.Paging;
using Nhs.Mvc.Domain.Model.Web;

namespace Nhs.Mvc.Tests.Utilities
{
    [TestClass]
    public class PagingTests : NhsTestBase
    {
        [TestMethod]
        public void PagingTest_ToPagedListReturnsObject()
        {
            Assert.IsNotNull(this._stateRepository.States.ToPagedList<State>(0, 2));
        }

        [TestMethod]
        public void PagingTest_ToPagedListReturnsListOfCorrectSize()
        {
            IPagedList<State> pState = this._stateRepository.States.ToPagedList<State>(0, 2);
            Assert.IsTrue(pState.Count() == 2);
        }

        [TestMethod]
        public void PagingTest_ToPagedListReturnsListWithCorrectPage()
        {
            IPagedList<State> pState = this._stateRepository.States.ToPagedList<State>(1, 2);
            Assert.IsTrue(pState.First().StateAbbr == "NY");
        }

        [TestMethod]
        public void PagingTest_ToPagedListReturnsCorrectInformation()
        {
            IPagedList<State> pState = this._stateRepository.States.ToPagedList<State>(1, 2);
            Assert.IsTrue(pState.PageCount == 2);
            Assert.IsTrue(pState.PageIndex == 1);
            Assert.IsTrue(pState.PageNumber == 2);
            Assert.IsTrue(pState.PageSize == 2);
            Assert.IsFalse(pState.HasNextPage);
            Assert.IsTrue(pState.HasPreviousPage);
            Assert.IsFalse(pState.IsFirstPage);
            Assert.IsTrue(pState.IsLastPage);
            Assert.IsTrue(pState.TotalItemCount == 4);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "PageIndex cannot be below 0.")]
        public void PagingTest_PagedListErrorForInvalidPageIndex()
        {
            IPagedList<State> pState = this._stateRepository.States.ToPagedList<State>(-1, 2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "PageSize cannot be less than 1.")]
        public void PagingTest_PagedListErrorForInvalidPageSize()
        {
            IPagedList<State> pState = this._stateRepository.States.ToPagedList<State>(0, 0);
        }

        [TestMethod]
        public void PagingTest_PagedListIndexGreaterThanPageCount()
        {
            IPagedList<State> pState = this._stateRepository.States.ToPagedList<State>(4, 2);
            Assert.IsTrue(pState.PageIndex ==1);
            Assert.IsTrue(pState.IsLastPage);
            Assert.IsTrue(pState.First().StateAbbr == "NY");
        }

        [TestMethod]
        public void DELETETHISONE()
        {
            var type1 = typeof (PropertySearchParams);
            var type2 = typeof (SearchParams);

            var pt = type1.GetProperties(
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty);

            var st = type2.GetProperties(
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty);

            var c1 = pt.Count();
            var c2 = st.Count();
            var ll = new List<string>();
            var total = 0;

            foreach (var tt in pt)
            {
                var dest = st.FirstOrDefault(x => x.Name == tt.Name);
                if (dest != null)
                    total++;
                else
                {
                    ll.Add(tt.Name);
                }
            }

            var json = ll.ToJson();

            Assert.IsTrue(total == c2);

        }
    }
}
