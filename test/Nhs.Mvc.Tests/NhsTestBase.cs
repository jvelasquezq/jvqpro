﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Library.Business.Interface;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Routing.Interface;
using Nhs.Mvc.Routing.Web;
using Nhs.Mvc.Tests.FakeRepositories;
using Nhs.Mvc.Tests.Helpers;
using Nhs.Utility.Common;
using Nhs.Web.UI.AppCode.Lib.StructureMap;
using System.Web;
using Nhs.Web.UI.AppCode.Controllers;
using Nhs.Mvc.Data.Repositories.Concrete;
using Nhs.Mvc.Domain.Model.Profiles;
using Nhs.UrlRewriter.Configuration;
using Nhs.UrlRewriter.UrlGeneration;
using BuilderService = Nhs.Mvc.Biz.Services.Concrete.BuilderService;

namespace Nhs.Mvc.Tests
{
    [TestClass]
    public abstract class NhsTestBase
    {
        //Services
        protected IApiService _apiService;
        protected IPartnerService _partnerService;
        protected IMarketService _marketService;
        protected ICommunityService _communityService;
        protected IMapService _mapService;
        protected IBoylService _boylService;
        protected IUserProfileService _userProfileService;
        protected IBrandService _brandService;
        protected IListingService _listingService;
        protected ILookupService _lookupService;
        protected IStateService _stateService;
        protected ICmsService _cmsService;
        protected IVideoService _videoService;
        protected IWebCacheService _webCacheService;
        protected ISessionCacheService _sessionCacheService;
        protected IBasicListingService _basicListingService;
        protected IBuilderService _builderService;        
        protected ISearchAlertService _searchAlertService;
        protected ISearchAlertRepository _searchAlertRepository;
        protected IQuickMoveInSearchService _quickMoveInService;
        protected IHotGreenSearchService _hotGreenService;
        protected IAffiliateLinkService _affiliateLinkService;
        protected ISsoService _SsoService;
        protected IMarketDfuService _marketDFUService;
        protected ISeoContentService _seoContentService;

        //Repositories
        protected IPartnerRepository _partnerRepository;
        protected IStateRepository _stateRepository;
        protected IMarketRepository _marketRepository;
        protected ICommunityRepository _communityRepository;
        protected IUserProfileRepository _userProfileRepository;
        protected IBrandRepository _brandRepository;
        protected IListingRepository _listingRepository;
        protected ILookupRepository _lookupRepository;
        protected IVideoRepository _videoRepository;
        protected IBuilderRepository _builderRepository;
        protected IImageRepository _imageRepository;
        protected IBasicListingRepository _basicListingRepository;
        protected IPathMapper _iPathMapper;
        protected IMarketDfuReader _marketDfuReader;
        protected IPathMapper _pathMapperMock = new TestContextPathMapper();
        protected IPartnerMarketPremiumUpgradeService _partnerMarketPremiumUpgrade;
        /// <summary>
        /// The config files get copied over to this location as a post-build event in this project.
        /// </summary>
        public string ConfigPath
        {
            get
            {
                return @"C:\NhsMvcConfig";
            }
        }

        public HtmlHelper HtmlHelper
        {
            get
            {
                var viewContext = new ViewContext();
                viewContext.HttpContext = MvcMockHelpers.FakeHttpContext();
                return new HtmlHelper(viewContext, new FakeViewDataContainer());
            }
        }

        public void SetupHttpContext()
        {
            this.SetupHttpContext("http://localhost:9999");
        }

        public NhsUrl SetupHttpContext(string url)
        {
            //Create fake context and set http context.current
            var HttpContextBase = MvcMockHelpers.FakeHttpContextWithHttpCurrent(url);
            MvcMockHelpers.FakeUserSession();

            //Set all Nhs context items by calling in Nhs Url Rewriter Module
           return NhsUrlGenerator.Generate(new Uri(url), false, new NhsUrl(), new ConcretePathMapper(ConfigPath));
        }

        public void SetupHttpContextInController(string url, ApplicationController Controller)
        {
            //Create fake context and set http context.current
            var httpContextBase = MvcMockHelpers.FakeHttpContextWithHttpCurrent(url);
            
            //Set the Request to the controller
            var ControllerContext = new System.Web.Mvc.ControllerContext();
            ControllerContext.HttpContext = httpContextBase;       
            Controller.ControllerContext = ControllerContext;

            MvcMockHelpers.FakeUserSession();

            //Set all Nhs context items by calling in Nhs Url Rewriter Module
            NhsUrl nUrl = NhsUrlGenerator.Generate(new Uri(url), false, new NhsUrl(), new ConcretePathMapper(ConfigPath)); 
            HttpContext.Current.Items.Add(RewriterConfiguration.ContextItemsCurrentUrl, nUrl);
        }

        protected NhsTestBase()
        {
            RegisterRoutes(RouteTable.Routes);
            RegisterViewEngines();
            RegisterStructureMapBootStrapper();
        }

        private void RegisterStructureMapBootStrapper()
        {
            //StructureMapBootStrapper.Configure();
            ControllerBuilder.Current.SetControllerFactory(new StructureMapControllerFactory());
        }

        private void RegisterViewEngines()
        {
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new NhsViewEngine());
        }

        private void RegisterRoutes(RouteCollection routes)
        {
            if (routes.Count == 0)
                MvcRoutes.Initialize(routes, new ConcretePathMapper(ConfigPath));
        }

        public PartialViewsController GetPartialViewsController()
        {
            return new PartialViewsController(_partnerService, _marketService, _communityService, _listingService, _mapService, _iPathMapper,
                _basicListingService, _builderService, _userProfileService);
        }

        [TestInitialize]
        public void Initialize()
        {
            StructureMapBootStrapper.Configure();

            _iPathMapper = new ContextPathMapper();

            _partnerRepository = new FakePartnerRepository();
            _stateRepository = new FakeStateRepository();
            _marketRepository = new FakeMarketRepository();
            _communityRepository = new FakeCommunityRepository(_marketRepository);
            _userProfileRepository = new FakeUserProfileRepository();
            _brandRepository = new FakeBrandRepository();
            _listingRepository = new FakeListingRepository();
            _lookupRepository = new FakeLookupRepository();
            _builderRepository = null;
            _imageRepository = new FakeImageRepository();
            _basicListingRepository = new FakeSqlBasicListingRepository();
            _sessionCacheService = new SessionCacheService();
            _webCacheService = new WebCacheService();
            _partnerMarketPremiumUpgrade = new PartnerMarketPremiumUpgradeService(_webCacheService);
            _searchAlertRepository = new SqlSearchAlertRepository(new ProfileEntities());
            _searchAlertService = new SearchAlertService(_searchAlertRepository,_apiService, _userProfileService);
            _partnerService = new PartnerService(_webCacheService, _marketService, _partnerRepository, _iPathMapper);
            _listingService = new ListingService(_sessionCacheService, _listingRepository, _imageRepository, _marketService, _stateService, new ApiService(_partnerService));
            //_communityService = new CommunityService(_webCacheService, _communityRepository, _sessionCacheService, _imageRepository, _listingRepository, _videoRepository, _partnerMarketPremiumUpgrade, _stateService,_partnerService);
            _mapService = new MapService(_marketService);
            _userProfileService = new UserProfileService(_userProfileRepository, _partnerService,_marketService);
            _brandService = new BrandService(_brandRepository, _webCacheService, _stateService);
            _lookupService = new LookupService(_webCacheService, _lookupRepository);
            _stateService = new StateService(_webCacheService, _stateRepository,_marketService);
            _cmsService = new CmsService(_webCacheService, _lookupService);
            _videoService = new VideoService(_videoRepository);
            _basicListingService = new BasicListingService(_basicListingRepository);            
            _marketService = new MarketService(_marketRepository, _basicListingRepository, _builderService);
            _SsoService = new SsoService(_userProfileService, _iPathMapper, _partnerService, _marketService);
            _apiService = new ApiService(_partnerService);
            _marketDFUService = new MarketDfuService(_marketDfuReader);
            _seoContentService = new SeoContentService(_webCacheService, _pathMapperMock);
            _builderService = new BuilderService(_builderRepository, _webCacheService, _partnerMarketPremiumUpgrade, _seoContentService);
        }
    }
}


