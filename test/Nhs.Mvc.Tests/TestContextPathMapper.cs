﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using Nhs.Utility.Common;
using Nhs.Utility.Web;

namespace Nhs.Mvc.Tests
{
    public class TestContextPathMapper : IPathMapper
    {
        public string MapPath(string relativePath)
        {
            //return HttpContext.Current.Server.MapPath(relativePath);
            var dir = AppDomain.CurrentDomain.BaseDirectory.Split(
               new[]
               {
                  "\\src\\"
               }
               , StringSplitOptions.None)[0] + "\\src\\web\\Nhs.Web.UI";

            var fixedPath = relativePath.Replace("~", string.Empty).Replace("/", "\\");
            var finalPath = dir + fixedPath;
            
            return finalPath;
        }

        public void AddItemToContext(string itemKey, object itemValue)
        {
            ContextHelper.AddItemToContext(HttpContext.Current, itemKey, itemValue);
        }

        public object GetItemFromContext(string itemKey)
        {
            return ContextHelper.GetItemFromContext(HttpContext.Current, itemKey);
        }
    }


}
