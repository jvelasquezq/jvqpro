﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using Moq;
using Nhs.Library.Business;
using Nhs.Library.Web;
using Nhs.Mvc.Routing.Interface;
using Nhs.Search.Objects;
using Nhs.Web.UI;
using Nhs.Web.UI.AppCode.Controllers;

namespace Nhs.Mvc.Tests.Helpers
{
    /// <summary>
    /// Credit: ScottHa
    /// </summary>
    public static class MvcMockHelpers
    {
        public static HttpContextBase FakeHttpContext(String url = "")
        {
            var context = new Mock<HttpContextBase>();
            var request = new Mock<HttpRequestBase>(MockBehavior.Loose);
            var response = new Mock<HttpResponseBase>();
            var session = new Mock<HttpSessionStateBase>();
            var server = new Mock<HttpServerUtilityBase>(MockBehavior.Loose);
            var application = new Mock<HttpApplication>();

            //application.Setup(app => app.Context).Returns(context.Object);

            var folder = Environment.CurrentDirectory
                .Replace(@"test\Nhs.Mvc.Tests\bin\Debug", @"web\Nhs.Web.UI")
                .Replace(@"test\Nhs.Mvc.Tests\bin\Release", @"web\Nhs.Web.UI");

            server.Setup(i => i.MapPath(It.IsAny<String>()))
                .Returns((String a) =>
                {
                    if (a.Contains("~/")) a = a.Replace("~/", folder);
                    if (a.Contains("/")) a = a.Replace("/", "\\");
                    Debug.WriteLine("SERVER Map path ::> " + a);
                    return a;
                }
                      );

            request.Setup(i => i.MapPath(It.IsAny<String>()))
                .Returns((String a) =>
                {
                    if (a.Contains("~/")) a = a.Replace("~/", folder);
                    if (a.Contains("/")) a = a.Replace("/", "\\");
                    Debug.WriteLine("REQUEST Map path ::> " + a);
                    return a;
                }
                      );

            Debug.WriteLine("Start to configure the context");

            context.Setup(ctx => ctx.Request).Returns(request.Object);
            context.Setup(ctx => ctx.Response).Returns(response.Object);
            context.Setup(ctx => ctx.Session).Returns(session.Object);
            context.Setup(ctx => ctx.Server).Returns(server.Object);
            context.Setup(ctx => ctx.ApplicationInstance).Returns(application.Object);

            var form = new NameValueCollection();
            var querystring = new NameValueCollection();
            var cookies = new HttpCookieCollection();
            var user = new GenericPrincipal(new GenericIdentity("testuser"), new string[] { "Administrator" });

            if (!String.IsNullOrEmpty(url))
            {
                var uri = new Uri(url);
                request.Setup(r => r.Url).Returns(uri);
            }
            request.Setup(r => r.Cookies).Returns(cookies);
            request.Setup(r => r.Form).Returns(form);
            request.Setup(q => q.QueryString).Returns(querystring);
            response.Setup(r => r.Cookies).Returns(cookies);
            context.Setup(u => u.User).Returns(user);                        

            HttpContext.Current = context.Object.ApplicationInstance.Context;                

            return context.Object;
        }

        public static HttpContext FakeHttpRequest(String url = "")
        {           
            // We need to setup the Current HTTP Context as follows:            

            // Step 1: Setup the HTTP Request
            var httpRequest = GetRequest(url);

            // Step 2: Setup the HTTP Response
            var httpResponce = new HttpResponse(new StringWriter());

            // Step 3: Setup the Http Context
            var httpContext = new HttpContext(httpRequest, httpResponce);

            var sessionContainer =
                new HttpSessionStateContainer("id",
                                               new SessionStateItemCollection(),
                                               new HttpStaticObjectsCollection(),
                                               10,
                                               true,
                                               HttpCookieMode.AutoDetect,
                                               SessionStateMode.InProc,
                                               false);

            httpContext.Items["AspSession"] =
                typeof(HttpSessionState)
                .GetConstructor(
                                    BindingFlags.NonPublic | BindingFlags.Instance,
                                    null,
                                    CallingConventions.Standard,
                                    new[] { typeof(HttpSessionStateContainer) },
                                    null)
                .Invoke(new object[] { sessionContainer });

            return httpContext;
        }

        public static HttpRequest GetRequest(String url)
        {
            Uri uri = new Uri(url);
            return new HttpRequest(GetUrlFileName(url), url, uri.Query.Trim('?'));
        }

        public static void FakeUserSession()
        {
            Nhs.Library.Web.UserSession.Current = new MockHttpSession();
        }

        /// <summary>
        /// TODO: Will have to handle url and querystring params separately at some point
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static HttpContextBase FakeHttpContextWithHttpCurrent(string url)
        {
            HttpContextBase context = FakeHttpContext(url);
            var uri = new Uri(url);
            context.Request.SetupRequestUrl("~" + uri.PathAndQuery);

            if (HttpContext.Current == null)
            {
                HttpContext.Current = FakeHttpRequest(url); 
            }

            NhsRoute.CurrentContext = context;
            HttpContext.Current.Items.Add("ContextItemsCurrentPartnerId", "1");
            return context;
        }

        public static void SetFakeControllerContext(this Controller controller)
        {
            var httpContext = FakeHttpContext();
            ControllerContext context = new ControllerContext(new RequestContext(httpContext, new RouteData()), controller);
            controller.ControllerContext = context;
        }

        public static void SetFakeControllerContext(this Controller controller, RouteData routeData)
        {
            SetFakeControllerContext(controller, new Dictionary<string, string>(), new HttpCookieCollection(), routeData);
        }

        public static void SetFakeControllerContext(this Controller controller, HttpCookieCollection requestCookies)
        {
            SetFakeControllerContext(controller, new Dictionary<string, string>(), requestCookies, new RouteData());
        }

        public static void SetFakeControllerContext(this Controller controller, Dictionary<string, string> formValues)
        {
            SetFakeControllerContext(controller, formValues, new HttpCookieCollection(), new RouteData());
        }

        public static void SetFakeControllerContext(this Controller controller,
                                                    Dictionary<string, string> formValues,
                                                    HttpCookieCollection requestCookies,
                                                    RouteData routeData)
        {
            var httpContext = FakeHttpContext();

            foreach (string key in formValues.Keys)
            {
                httpContext.Request.Form.Add(key, formValues[key]);

            }
            foreach (string key in requestCookies.Keys)
            {
                httpContext.Request.Cookies.Add(requestCookies[key]);

            }
            ControllerContext context = new ControllerContext(new RequestContext(httpContext, routeData), controller);
            controller.ControllerContext = context;
        }

        public static void SetFakeControllerContextWithLogin(this Controller controller, string userName,
                                                             string password,
                                                             string returnUrl)
        {

            var httpContext = FakeHttpContext();


            httpContext.Request.Form.Add("username", userName);
            httpContext.Request.Form.Add("password", password);
            httpContext.Request.QueryString.Add("ReturnUrl", returnUrl);

            ControllerContext context = new ControllerContext(new RequestContext(httpContext, new RouteData()), controller);
            controller.ControllerContext = context;
        }


        static string GetUrlFileName(string url)
        {
            if (url.Contains("?"))
                return url.Substring(0, url.IndexOf("?"));
            else
                return url;
        }

        static NameValueCollection GetQueryStringParameters(string url)
        {
            if (url.Contains("?"))
            {
                NameValueCollection parameters = new NameValueCollection();

                string[] parts = url.Split("?".ToCharArray());
                string[] keys = parts[1].Split("&".ToCharArray());

                foreach (string key in keys)
                {
                    string[] part = key.Split("=".ToCharArray());
                    parameters.Add(part[0], part[1]);
                }

                return parameters;
            }
            else
            {
                return null;
            }
        }

        public static void SetHttpMethodResult(this HttpRequestBase request, string httpMethod)
        {
            Mock.Get(request)
                .Setup(req => req.HttpMethod)
                .Returns(httpMethod);
        }

        public static void SetupRequestUrl(this HttpRequestBase request, string url)
        {
            if (url == null)
                throw new ArgumentNullException("url");

            if (!url.StartsWith("~/"))
                throw new ArgumentException("Sorry, we expect a virtual url starting with \"~/\".");

            var mock = Mock.Get(request);

            mock.Setup(req => req.QueryString)
                .Returns(GetQueryStringParameters(url));
            mock.Setup(req => req.AppRelativeCurrentExecutionFilePath)
                .Returns(GetUrlFileName(url));
            mock.Setup(req => req.PathInfo)
                .Returns(string.Empty);
        }

    }

    public class FakeViewEngine : IViewEngine
    {
        public ViewEngineResult FindPartialView(ControllerContext controllerContext, string partialViewName, bool useCache)
        {
            throw new System.NotImplementedException();
        }

        public ViewEngineResult FindView(ControllerContext controllerContext, string viewName, string masterName, bool useCache)
        {
            throw new System.NotImplementedException();
        }

        public void ReleaseView(ControllerContext controllerContext, IView view)
        {
            throw new System.NotImplementedException();
        }
    }

    public class FakeViewDataContainer : IViewDataContainer
    {
        private ViewDataDictionary _viewData = new ViewDataDictionary();
        public ViewDataDictionary ViewData { get { return _viewData; } set { _viewData = value; } }
    }

    public class MockHttpSession : HttpSessionStateBase
    {

        private static Profile myProfile = new Profile { ActorStatus = Nhs.Library.Enums.WebActors.ActiveUser };

        private readonly Dictionary<string, object> m_SessionStorage = new Dictionary<string, object> { { "UserProfile_1", myProfile } };

        public MockHttpSession()
        {
            m_SessionStorage.Add("PropertySearchParams_1", new PropertySearchParams());
            m_SessionStorage.Add("NhsPropertySearchParams_1", new NhsPropertySearchParams());
            m_SessionStorage.Add("HasSendLead_1", false);
            m_SessionStorage.Add("PersonalCookie_1", new PersonalCookie());
            m_SessionStorage.Add("Refer_1", "");
            m_SessionStorage.Add("QueryParams_1", "");
        }


        public override object this[string name]
        {
            get { return m_SessionStorage[name]; }
            set { m_SessionStorage[name] = value; }
        }
    } 

}
