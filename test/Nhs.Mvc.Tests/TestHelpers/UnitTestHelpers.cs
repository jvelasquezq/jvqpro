﻿using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Nhs.Mvc.Tests.Helpers
{
    internal static class UnitTestHelpers
    {
        internal static void ShouldEqual<T>(this T actualValue, T expectedValue)
        {
            Assert.AreEqual(expectedValue, actualValue);
        }

        internal static void ShouldBeRedirectionTo(this ActionResult actionResult, object expectedRouteValues)
        {
            var actualValues = ((RedirectToRouteResult)actionResult).RouteValues;
            var expectedValues = new RouteValueDictionary(expectedRouteValues);
            foreach (string key in expectedValues.Keys)
                actualValues[key].ShouldEqual(expectedValues[key]);
        }
    }
}