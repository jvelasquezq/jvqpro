﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Web.UI.AppCode.Helper;
using Nhs.Web.UI.AppCode.HtmlHelpers;

//using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting.Web;

namespace Nhs.Mvc.Tests.TestHelpers
{
    
    
    /// <summary>
    ///This is a test class for LiveChatHelperTest and is intended
    ///to contain all LiveChatHelperTest Unit Tests
    ///</summary>
    [TestClass()]
    public class LiveChatHelperTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


 
        [TestMethod()]    
        public void ValidBizHourTest()
        {
            bool expected = true; 
            bool actual;
            actual = LiveChatHelper_Accessor.ValidBizHour();
            Assert.AreEqual(expected, actual);
            Assert.IsTrue(expected, "Error in BIZ hours");
        }

        [TestMethod()]
        public void GetScriptLinkNhsTest()
        {
            var script = LiveChatHelper.LiveChatLink("", "", ChatPlacement.NhsChatLink);

            if (LiveChatHelper_Accessor.ValidBizHour())
            {
                Assert.IsFalse(string.IsNullOrWhiteSpace(script.ToString()));
            }
            else
            {
                Assert.IsTrue(string.IsNullOrWhiteSpace(script.ToString()));
            }
        }
    }
}
