﻿using System.Collections.Generic;
using System.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Library.Constants.Enums;
using Nhs.Library.Enums;
using Nhs.Library.Helpers.Seo;
using Nhs.Library.Web;
using Nhs.Mvc.Tests.Helpers;

namespace Nhs.Mvc.Tests.Library
{
    [TestClass]
    public class SeoContentManagerTests:NhsTestBase
    {
        [TestMethod]
        public void SeoContentManager_ContentTagsShouldReplaceCorrectly()
        {
            var contentTags = new List<ContentTag>
                                  {
                                      new ContentTag() {TagKey = ContentTagKey.MarketId, TagValue = "269"},
                                  };

            var seo = new SeoContentManager();
            seo.ContentTags = contentTags;
            string dummyText = "Do you need help finding the perfect new home in the [MarketId] area of Austin, TX? Not only does N";
            //dummyText = seo.ReplaceTags(dummyText);
            dummyText.Contains("269").ShouldEqual(true);
        }
    }
}
