﻿using System.Collections.Generic;
using BHI.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Library.Business;
using Nhs.Library.Common;
using Nhs.Library.Enums;
using Nhs.Mvc.Domain.Model;
using Nhs.Mvc.Tests.FakeRepositories;
using Nhs.Mvc.Tests.Helpers;
using Nhs.Utility.Common;

namespace Nhs.Mvc.Tests.Library
{
    /// <summary>
    /// Summary description for CommunityTests
    /// </summary>
    [TestClass]
    public class MetaReaderTests : NhsTestBase
    {
        private MetaReader _reader;
        private ConcretePathMapper _mapper;

        public MetaReaderTests()
        {

        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            _mapper = new ConcretePathMapper(ConfigPath);
            
            _reader = new MetaReader(_mapper);
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            _reader = null;
        }


        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion


        [TestMethod]
        public void MetaReader_MetaTagsForStaticMetaPagesShouldLoad()
        {
            string expectedTitle = "Static Dummy Results Title";
            string expectedDescription = "Static Dummy Results - It cannot get dummier than this";
            string expectedKeywords = "Static Dummy, Silly, Stupid, Nonsensical"; 

            base.SetupHttpContext("http://localhost:9999/dummyresults/marketid-269/page-1");

            List<MetaTag> tags = _reader.GetMetaTagInformation("dummyresults", null, PageHeaderSectionName.StaticMetaPages);

            var returnedTitle = tags.Find(x => x.Name == MetaName.Title).Content;
            var returnedDesrtiption = tags.Find(x => x.Name == MetaName.Description).Content;
            var returnedKeywords = tags.Find(x => x.Name == MetaName.Keywords).Content;

            returnedTitle.ShouldEqual(expectedTitle);
            returnedDesrtiption.ShouldEqual(expectedDescription);
            returnedKeywords.ShouldEqual(expectedKeywords);
        }


        [TestMethod]
        public void MetaReader_MetaTagsForSeoMetaPagesShouldLoad()
        {
            string expectedTitle = "Dummy Results Title";
            string expectedDescription = "Dummy Results for market [marketname]- It cannot get dummier than this";
            string expectedKeywords = "Dummy, Silly, Stupid, Nonsensical"; 

            base.SetupHttpContext("http://localhost:9999/dummyresults/marketid-269/page-1");

            List<MetaTag> tags = _reader.GetMetaTagInformation("dummyresults", null, PageHeaderSectionName.SeoMetaPages);

            var returnedTitle = tags.Find(x => x.Name == MetaName.Title).Content;
            var returnedDesrtiption = tags.Find(x => x.Name == MetaName.Description).Content;
            var returnedKeywords = tags.Find(x => x.Name == MetaName.Keywords).Content;

            returnedTitle.ShouldEqual(expectedTitle);
            returnedDesrtiption.ShouldEqual(expectedDescription);
            returnedKeywords.ShouldEqual(expectedKeywords);
        }

        [TestMethod]
        public void MetaReader_MetaTagsForSeoPagesWithParametersShouldLoad()
        {
            string expectedDescription = "Dummy Results for market Krypton- It cannot get dummier than this";
            string marketName = "Krypton";

            base.SetupHttpContext("http://localhost:9999/dummyresults/marketid-1/");

            Dictionary<string, string> parameters = new Dictionary<string, string>() { { ContentTagKey.MarketName.Enclose(), marketName } };

            List<MetaTag> tags = _reader.GetMetaTagInformation("dummyresults", parameters, PageHeaderSectionName.SeoMetaPages);
           
            var returnedDescription = tags.Find(x => x.Name == MetaName.Description).Content;

            returnedDescription.ShouldEqual(expectedDescription);
        }


        [TestMethod]
        public void MetaReader_MetaTagsByPartnerForSeoPagesShouldLoad()
        {
            string expectedTitle = "Dummy Results Title for partner movoto";

            base.SetupHttpContext("http://localhost:9999/movoto/dummyresults/marketid-269/");

            List<MetaTag> tags = _reader.GetMetaTagInformation("dummyresults", null, PageHeaderSectionName.SeoMetaPages);

            var returnedTitle = tags.Find(x => x.Name == MetaName.Title).Content;

            returnedTitle.ShouldEqual(expectedTitle);
        }

        [TestMethod]
        public void MetaReader_MetaTagsByMarketForSeoPagesShouldLoad()
        {
            string expectedTitle = "Dummy Results Title for Market 269";

            base.SetupHttpContext("http://localhost:9999/dummyresults/marketid-269/");

            _reader = new MetaReader(_mapper, 269);

            List<MetaTag> tags = _reader.GetMetaTagInformation("dummyresults", null, PageHeaderSectionName.SeoMetaPages);

            var returnedTitle = tags.Find(x => x.Name == MetaName.Title).Content;

            returnedTitle.ShouldEqual(expectedTitle);
        }

    }
}