﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Library.Business.Seo.SiteIndex;
using Nhs.Library.Constants;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Concrete;

namespace Nhs.Mvc.Tests.Services
{        
    /// <summary>
    ///This is a test class for ISeoContentServiceTest and is intended
    ///to contain all ISeoContentServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SeoContentServiceTest: NhsTestBase
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetSeoContent
        ///</summary>
        public T GetSeoContentTestHelper<T>(SeoContentOptions options)
            where T : new()
        {
            var target = CreateISeoContentService();                       
            var actual = target.GetSeoContent<T>(options);
            return actual;
        }

        internal virtual ISeoContentService CreateISeoContentService()
        {    
            var target = _seoContentService;
            return target;
        }

        #region Amenities

        [TestMethod()]
        public void GetSeoContentTest_Amenities_Waterfront()
        {
            SetupHttpContext("http://mvc.newhomesource.com");
            var options = new SeoContentOptions
                (
                SiteIndexConst.AmenitiesWaterfrontFolder,
                "TX" + ".xml",
                replaceTags: new List<ContentTag>
                    {
                        new ContentTag(ContentTagKey.StateName, "TX")
                    }
                );
            var result = GetSeoContentTestHelper<AmenitiesContent>(options);
            Assert.IsTrue(string.IsNullOrWhiteSpace(result.Header)== false);
        }

        [TestMethod()]
        public void GetSeoContentTest_Amenities_Golf()
        {
            SetupHttpContext("http://mvc.newhomesource.com");
            var options = new SeoContentOptions
                (
                SiteIndexConst.AmenitiesGolfFolder,
                "TX" + ".xml",
                replaceTags: new List<ContentTag>
                    {
                        new ContentTag(ContentTagKey.StateName, "TX")
                    }
                );
            var result = GetSeoContentTestHelper<AmenitiesContent>(options);
            Assert.IsTrue(string.IsNullOrWhiteSpace(result.Header) == false);
        }

        [TestMethod()]
        public void GetSeoContentTest_Amenities_Condo()
        {
            SetupHttpContext("http://mvc.newhomesource.com");
            
            var options = new SeoContentOptions
                (
                SiteIndexConst.AmenitiesCondoFolder,
                "TX" + ".xml",
                replaceTags: new List<ContentTag>
                    {
                        new ContentTag(ContentTagKey.StateName, "TX")
                    }
                );

            var result = GetSeoContentTestHelper<AmenitiesContent>(options);
            Assert.IsTrue(string.IsNullOrWhiteSpace(result.Header) == false);
        }

        #endregion

        [TestMethod()]
        public void GetSeoContentTest_IndexState()
        {
            SetupHttpContext("http://mvc.newhomesource.com");

            var options = new SeoContentOptions
                (
                SiteIndexConst.SiteIndexStateFolder,
                "TX" + ".xml",
                replaceTags: new List<ContentTag>
                    {
                        new ContentTag(ContentTagKey.StateName, "TX")
                    }
                );

            var result = GetSeoContentTestHelper<SiteIndexStateContent>(options);
            Assert.IsTrue(string.IsNullOrWhiteSpace(result.Header) == false);
        }


        [TestMethod()]
        public void GetSeoContentTest_StateBrand()
        {
            //TX_2646_Abbott Homes.xml
            var file = string.Format("{0}_{1}_{2}.xml", "TX", 2646, "Abbott Homes");
            var failBackFiles = new List<string>
            {
                string.Format("{0}.xml", "TX"),
                "Default.xml"
            };
            const string brandName = "Abbott Homes";
            var result = _seoContentService.GetSeoContent<StateBrandContent>(new SeoContentOptions
                (
                    SiteIndexConst.StateBrandFolder,
                    file,
                    failBackFiles,
                    replaceTags: new List<ContentTag>
                    {
                        new ContentTag(ContentTagKey.StateName, "TX"),
                        new ContentTag(ContentTagKey.BrandName, brandName),
                        new ContentTag(ContentTagKey.BuilderName, "Builder NAME")
                    }
                ));
            Assert.IsTrue(string.IsNullOrWhiteSpace(result.Header) == false);
            Assert.IsTrue(result.Header.Contains(brandName));
        }

        /// <summary>
        ///A test for ReplaceTags
        ///</summary>
        [TestMethod()]
        public void ReplaceTagsTest()
        {
            ISeoContentService target = CreateISeoContentService();
            const string input = "[StateName] Replace tags test"; 
            IList<ContentTag> replaceTags = new List<ContentTag>()
            {
                new ContentTag(ContentTagKey.StateName, "TX")
            };
            const string expected = "TX Replace tags test";
            var actual = target.ReplaceTags(input, replaceTags);
            Assert.AreEqual(expected, actual);            
        }
    }
}
