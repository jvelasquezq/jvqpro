﻿using System.Linq;
using Nhs.Mvc.Biz.Services.Concrete;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Domain.Model.Web;
using System.Collections.Generic;
using System.Data;
using BuilderService = Nhs.Mvc.Biz.Services.Concrete.BuilderService;

namespace Nhs.Mvc.Tests
{
    
    
    /// <summary>
    ///This is a test class for BuilderServiceTest and is intended
    ///to contain all BuilderServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BuilderServiceTest: NhsTestBase
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for BuilderService Constructor
        ///</summary>
        [TestMethod()]
        public void BuilderServiceConstructorTest()
        {
            /*IBuilderRepository buildeRepository = null; // TODO: Initialize to an appropriate value
            IWebCacheService webCacheService = null; // TODO: Initialize to an appropriate value
            IPartnerMarketPremiumUpgradeService partnerMarketPremiumUpgrade = null; // TODO: Initialize to an appropriate value
            BuilderService target = new BuilderService(buildeRepository, webCacheService, partnerMarketPremiumUpgrade);
            Assert.Inconclusive("TODO: Implement code to verify target");*/
        }

        /// <summary>
        ///A test for GetBuilder
        ///</summary>
        [TestMethod()]
        public void GetBuilderTest()
        {
            /*IBuilderRepository buildeRepository = null; // TODO: Initialize to an appropriate value
            IWebCacheService webCacheService = null; // TODO: Initialize to an appropriate value
            IPartnerMarketPremiumUpgradeService partnerMarketPremiumUpgrade = null; // TODO: Initialize to an appropriate value
            BuilderService target = new BuilderService(buildeRepository, webCacheService, partnerMarketPremiumUpgrade); // TODO: Initialize to an appropriate value
            int builderId = 0; // TODO: Initialize to an appropriate value
            Builder expected = null; // TODO: Initialize to an appropriate value
            Builder actual;
            actual = target.GetBuilder(builderId);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");*/
        }

        /// <summary>
        ///A test for GetBuilders
        ///</summary>
        [TestMethod()]
        public void GetBuildersTest()
        {
            /*IBuilderRepository buildeRepository = null; // TODO: Initialize to an appropriate value
            IWebCacheService webCacheService = null; // TODO: Initialize to an appropriate value
            IPartnerMarketPremiumUpgradeService partnerMarketPremiumUpgrade = null; // TODO: Initialize to an appropriate value
            BuilderService target = new BuilderService(buildeRepository, webCacheService, partnerMarketPremiumUpgrade); // TODO: Initialize to an appropriate value
            List<int> builderIdList = null; // TODO: Initialize to an appropriate value
            IList<Builder> expected = null; // TODO: Initialize to an appropriate value
            IList<Builder> actual;
            actual = target.GetBuilders(builderIdList);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");*/
        }

        /// <summary>
        ///A test for GetMarketBcs
        ///</summary>
        [TestMethod()]
        public void GetMarketBcsTest()
        {
            /*IBuilderRepository buildeRepository = null; // TODO: Initialize to an appropriate value
            IWebCacheService webCacheService = null; // TODO: Initialize to an appropriate value
            IPartnerMarketPremiumUpgradeService partnerMarketPremiumUpgrade = null; // TODO: Initialize to an appropriate value
            BuilderService target = new BuilderService(buildeRepository, webCacheService, partnerMarketPremiumUpgrade); // TODO: Initialize to an appropriate value
            int partnerId = 0; // TODO: Initialize to an appropriate value
            int marketId = 0; // TODO: Initialize to an appropriate value
            DataTable expected = null; // TODO: Initialize to an appropriate value
            DataTable actual;
            actual = target.GetMarketBcs(partnerId, marketId);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");*/
        }

        /// <summary>
        ///A test for GetMarketBuilders
        ///</summary>
        [TestMethod()]
        public void GetMarketBuildersTest()
        {
            IBuilderRepository buildeRepository = _builderRepository; 
            IWebCacheService webCacheService = _webCacheService; 
            IPartnerMarketPremiumUpgradeService partnerMarketPremiumUpgrade = _partnerMarketPremiumUpgrade; 
            var target = new BuilderService(buildeRepository, webCacheService, partnerMarketPremiumUpgrade, _seoContentService); 
            const int partnerId = 1; 
            const int marketId = 269;

            IList<Builder> actual = target.GetMarketBuilders(partnerId, marketId);
            Assert.IsTrue(actual.Any());            
        }

        /// <summary>
        ///A test for GetMarketTable
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Nhs.Mvc.Biz.Services.dll")]
        public void GetMarketTableTest()
        {
            /*PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            BuilderService_Accessor target = new BuilderService_Accessor(param0); // TODO: Initialize to an appropriate value
            int partnerId = 0; // TODO: Initialize to an appropriate value
            int marketId = 0; // TODO: Initialize to an appropriate value
            DataTable expected = null; // TODO: Initialize to an appropriate value
            DataTable actual;
            actual = target.GetMarketTable(partnerId, marketId);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");*/
        }

        /// <summary>
        ///A test for GetServiceByBuilderId
        ///</summary>
        [TestMethod()]
        public void GetServiceByBuilderIdTest()
        {
            /*IBuilderRepository buildeRepository = null; // TODO: Initialize to an appropriate value
            IWebCacheService webCacheService = null; // TODO: Initialize to an appropriate value
            IPartnerMarketPremiumUpgradeService partnerMarketPremiumUpgrade = null; // TODO: Initialize to an appropriate value
            BuilderService target = new BuilderService(buildeRepository, webCacheService, partnerMarketPremiumUpgrade); // TODO: Initialize to an appropriate value
            int builderId = 0; // TODO: Initialize to an appropriate value
            int serviceId = 0; // TODO: Initialize to an appropriate value
            BuilderService expected = null; // TODO: Initialize to an appropriate value
            BuilderService actual;
            var actual = target.GetServiceByBuilderId(builderId, serviceId);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");*/
        }
    }
}
