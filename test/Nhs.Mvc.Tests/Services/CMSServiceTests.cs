﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Mvc.Domain.Model.CMS;
using Nhs.Library.Common;
using Nhs.Library.Enums;

namespace Nhs.Mvc.Tests.Services
{
    [TestClass]
    public class CMSServiceTests : NhsTestBase
    {
        [TestMethod]
        public void CMSServiceShouldReturnCategoryPositionData()
        {
            var d = _cmsService.GetCategoryPositionData(DateTime.Parse(DateTime.Now.ToShortDateString()), Convert.ToInt32(CMSSiteIDs.NewHomesource), Configuration.CmsNhsHomePageCatId);
            Assert.IsTrue(d is IList<CategoryPosition>);
            Assert.AreEqual(d.Count(), 3);
        }
    }
}
