﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Mvc.Biz.Services.Abstract;
using Nhs.Mvc.Biz.Services.Concrete;
using Nhs.Mvc.Tests.Helpers;
using Nhs.Mvc.Biz.Services.Filters;

namespace Nhs.Mvc.Tests.Services
{
    [TestClass]
    public class SessionCacheServiceTests : NhsTestBase
    {
        private readonly ISessionCacheService _sessionCache;

        public SessionCacheServiceTests()
        {
            _sessionCache = new SessionCacheService();
        }

        [TestMethod]
        public void CacheReturnsObject()
        {
            MvcMockHelpers.FakeUserSession();
            var community = _sessionCache.Get("test", true, () => _communityRepository.Communities.GetById(15123));
            Assert.IsTrue(community != null);
        }

        [TestMethod]
        public void CacheDoesNotRefresh()
        {
            MvcMockHelpers.FakeUserSession();
            var community = _sessionCache.Get("test", true, () => _communityRepository.Communities.GetById(15123));
            community = _sessionCache.Get("test", false, () => _communityRepository.Communities.GetById(111));
            Assert.IsTrue(community != null);
            Assert.IsTrue(community.CommunityId == 15123);
        }

        [TestMethod]
        public void CacheRefreshes()
        {
            MvcMockHelpers.FakeUserSession();
            var community = _sessionCache.Get("test", true, () => _communityRepository.Communities.GetById(15123));
            community = _sessionCache.Get("test", true, () => _communityRepository.Communities.GetById(111));
            Assert.IsTrue(community == null);
        }
    }
}
