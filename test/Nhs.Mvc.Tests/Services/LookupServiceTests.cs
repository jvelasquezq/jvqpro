﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Library.Enums;

namespace Nhs.Mvc.Tests.Services
{
    [TestClass]
    public class LookupServiceTests : NhsTestBase
    {
        [TestMethod]
        public void LookupServiceShouldReturnItemsInGroup()
        {
            var lookupItems = _lookupService.GetCommonListItems(CommonListItem.PriceRange);
            Assert.AreEqual(lookupItems.Count(), 4);
            foreach (var itm in lookupItems)
            {
                Assert.AreEqual(itm.GroupName, "PRICEDD");
            }
        }

        [TestMethod]
        public void LookupServiceShouldReturnItemsSortedBySortOrder()
        {
            var lookupItems = _lookupService.GetCommonListItems(CommonListItem.PriceRange);
            Assert.AreEqual(lookupItems.ElementAt(0).SortOrder, 1);
            Assert.AreEqual(lookupItems.ElementAt(1).SortOrder, 2);
            Assert.AreEqual(lookupItems.ElementAt(2).SortOrder, 3);
            Assert.AreEqual(lookupItems.ElementAt(3).SortOrder, 4);
        }
    }
}
