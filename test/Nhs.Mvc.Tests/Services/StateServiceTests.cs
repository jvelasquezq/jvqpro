﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Mvc.Domain.Model.Web;
using Nhs.Mvc.Biz.Services.Filters;

namespace Nhs.Mvc.Tests.Services
{
    [TestClass]
    public class StateServiceTests : NhsTestBase
    {
        [TestMethod]
        public void StateServiceShouldReturnStateByName()
        {
            var state = _stateService.GetStateAbbreviation("Texas");
            Assert.AreEqual(state, "TX");
        }

        [TestMethod]
        public void StateServiceShouldReturnNullForInvalidState()
        {
            var state = _stateService.GetStateAbbreviation("thisstate");
            Assert.IsNull(state);
        }

        [TestMethod]
        public void StateServiceShouldReturnStateCaseInsensitive()
        {
            var state = _stateService.GetStateAbbreviation("texas");
            Assert.AreEqual(state, "TX");
        }


        [TestMethod]
        public void StateFilterByAbbrShouldReturnState()
        {
            State state = _stateRepository.States.GetByAbbr("TX");
            Assert.AreEqual(state.StateAbbr, "TX");
        }

        [TestMethod]
        public void StateFilterByAbbrShouldReturnStateCaseInsensitive()
        {
            State state = _stateRepository.States.GetByAbbr("tx");
            Assert.AreEqual(state.StateAbbr, "TX");
        }

        [TestMethod]
        public void StateFilterByAbbrReturnNullForInvalidAbbr()
        {
            State state = _stateRepository.States.GetByAbbr("Texas");
            Assert.IsNull(state);
        }

        [TestMethod]
        public void StateFilterByNameOrAbbrReturnStateForAbbr()
        {
            State state = _stateRepository.States.GetByNameOrAbbr("TX");
            Assert.AreEqual(state.StateAbbr, "TX");
        }

        [TestMethod]
        public void StateFilterByNameOrAbbrReturnStateForAbbrCaseInsensitive()
        {
            State state = _stateRepository.States.GetByNameOrAbbr("Tx");
            Assert.AreEqual(state.StateAbbr, "TX");
        }

        [TestMethod]
        public void StateFilterByNameOrAbbrReturnStateForName()
        {
            State state = _stateRepository.States.GetByNameOrAbbr("Texas");
            Assert.AreEqual(state.StateAbbr, "TX");
        }

        [TestMethod]
        public void StateFilterByNameOrAbbrReturnStateForNameCaseInsensitive()
        {
            State state = _stateRepository.States.GetByNameOrAbbr("texas");
            Assert.AreEqual(state.StateAbbr, "TX");
        }

        [TestMethod]
        public void StateFilterByNameOrAbbrReturnNullForInvalidValue()
        {
            State state = _stateRepository.States.GetByNameOrAbbr("tt");
            Assert.IsNull(state);
        }
    }
}
