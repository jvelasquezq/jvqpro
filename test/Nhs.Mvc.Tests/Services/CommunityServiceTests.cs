﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Mvc.Data.Repositories.Abstract;
using Nhs.Mvc.Tests.FakeRepositories;

namespace Nhs.Mvc.Tests.Services
{
    /// <summary>
    /// Summary description for CommunityTests
    /// </summary>
    [TestClass]
    public class CommunityServiceTests
    {
        private ICommunityRepository _communityRepository;

        public CommunityServiceTests()
        {
            IMarketRepository marketRepository = new FakeMarketRepository();
            _communityRepository = new FakeCommunityRepository(marketRepository);
        }

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        //TODO: Remove this
        //[TestMethod]
        //public void DoesServiceReturnCommunitiesByMarket()
        //{
        //    ICommunityService communityService = new CommunityService(_communityRepository);
        //    List<Community> comms = communityService.GetCommunitiesForMarket(269, 1, 10);
        //    (comms.Count > 0).ShouldEqual(true);
        //}


        //[TestMethod]
        //public void IsCommunityBrochureMergedCorrectly()
        //{
        //    Nhs.Web.UI.WebServices.BrochureService brochureService = new Web.UI.WebServices.BrochureService();
        //    string brochurePath = brochureService.GetCommunityBrochure(1);
        //    //string filePath = TestContext.RequestedPage.Server.MapPath(brochurePath);
        //    bool exists = System.IO.File.Exists(brochurePath);
        //    exists.ShouldEqual(true);
        //}

    }
}