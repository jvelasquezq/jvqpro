﻿using System;
using System.Linq;
using System.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nhs.Library.Common;
using Nhs.Library.Enums;
using Nhs.Library.Web;
using Nhs.Mvc.Tests.Helpers;
using Nhs.Web.UI.AppCode.Helper;

namespace Nhs.Mvc.Tests.Services
{
    /// <summary>
    /// Summary description for UserProfileTests
    /// </summary>
    [TestClass]
    public class UserProfileServiceTests : NhsTestBase
    {
        private const int PartnerId = 1;

        public UserProfileServiceTests()
        {
            base.SetupHttpContext("http://localhost:9999/chroniclehomes/communityresults/state-texas/market-austin/page-1");
        }

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void UserProfileServiceShouldSignUserIn()
        {
            MvcMockHelpers.FakeUserSession();
            UserSession.SetItem("UserProfile", null);
            _userProfileService.SignIn("jdoe@joe.com", "password", PartnerId, LanguageHelper.WrongPassword, LanguageHelper.EmailDoNotExist);

            (HttpContext.Current.Response.Cookies["Profile_1"].Value!=null).ShouldEqual(true);
        }

        [TestMethod]
        public void UserProfileServiceShouldReturnUserByUserNamePassword()
        {
            const string loginName = "jdoe@joe.com";
            const string password = "password";

            var user = _userProfileService.GetUserProfile(loginName, password, PartnerId);
            user.FirstName.ToLower().ShouldEqual("john");
        }

        [TestMethod]
        public void UserProfileServiceShouldAddCommunityToUserPlanner()
        {
            //Arrange
            MvcMockHelpers.FakeUserSession();
            UserSession.SetItem("UserProfile", null);

            int communityId = 33695;
            int builderId = 194;
            const string loginName = "jdoe@joe.com";
            const string password = "password";
            var user = _userProfileService.GetUserProfile(loginName, password, PartnerId);

            //Sign In
            _userProfileService.SignIn(loginName, password, PartnerId, LanguageHelper.WrongPassword, LanguageHelper.EmailDoNotExist);

            //Act
            _userProfileService.AddCommunityToUserPlanner(user, communityId, builderId, ListingType.Community);

            //Assert
            var community = (from c in UserSession.UserProfile.Planner.SavedCommunities
                             where c.ListingId == communityId
                             select c).FirstOrDefault();

            community.ListingId.ShouldEqual(communityId);
        }

        [TestMethod]
        public void UserProfileServiceShouldSignUserOut()
        {
            //Arrange
            UserProfileServiceShouldSignUserIn();

            //Act
            _userProfileService.SignOut(PartnerId);

            //Assert
            var cookie = HttpContext.Current.Response.Cookies["Profile_" + PartnerId];
            (cookie.Expires.Date.Day == DateTime.Now.Day && cookie.Expires.Ticks < DateTime.Now.Ticks).ShouldEqual(true);
        }

        [TestMethod]
        public void UserProfileServiceShouldCreateRegistrationLead()
        {
            //Arrange
            MvcMockHelpers.FakeUserSession();
            UserSession.Refer = string.Empty;
            const string loginName = "jdoe@joe.com";

            var user = (from u in _userProfileRepository.Users
                       where u.LogonName == loginName
                       select u).FirstOrDefault();

            //Act
            var success = _userProfileService.CreateRegistrationLead(user);

            //Assert
            success.ShouldEqual(true);
        }
    }
}
