using System;
using System.Web;
using System.Web.Hosting;
using System.IO;
using Nhs.Library.Common;
using Nhs.Library.Constants;
using Nhs.Library.Web;
using NUnit.Framework;

namespace Nhs.UnitTests.Library
{
    [TestFixture]
    [Category("Library.ContextHelperTester")]
    public class ContextHelperTester
    {
        private HttpContext _context;

        [TestFixtureSetUp]
        public void Setup()
        {
            TextWriter output = new StringWriter();

            // TODO: Virtual directory, path, & file would be better as constants and possibly configuration items. Or figured out some other way if available.

            // Communicate with IIS to establish a valid HttpContext.
            HttpWorkerRequest workerRequest = 
                new SimpleWorkerRequest(
                "/NHS5M",                                   // virtual directory
                "C:/BHI/Solutions/NHS5M.Prod/Nhs.Web.UI",   // path
                "Default.aspx",                             // file
                "",                                         // request
                output);
            HttpRuntime.ProcessRequest(workerRequest);
            _context = new HttpContext(workerRequest);
            HttpContext.Current = _context;

            ContextHelper.AddItemToContext(_context, ContextItems.CurrentUrl, new NhsUrl("home"));
            ContextHelper.AddItemToContext(_context, ContextItems.CopyOfCurrentUrl, _context.Items[ContextItems.CurrentUrl]);

            workerRequest.CloseConnection();
            HttpRuntime.Close();
        }

        [TestFixtureTearDown]
        public void TearDown()
        {
        }

        /// <summary>
        /// This test ensures that the CurrentUrl key exists in _context and
        /// that its value is the expected "home" NhsUrl.
        /// </summary>
        /// <remarks>
        /// CurrentUrl and CopyOfCurrentUrl are added to _context in Setup()
        /// using the AddItemToContext() function.
        /// </remarks>
        [Test]
        public void TestAddItemToContext()
        {
            Assert.IsNotNull(_context.Items[ContextItems.CurrentUrl]);
            Assert.AreEqual(new NhsUrl("home").ToString(), _context.Items[ContextItems.CurrentUrl].ToString());
        }

        /// <summary>
        /// This test attempts to add an item to a null HTTP context and ensures
        /// that an ArgumentNullException is thrown.
        /// </summary>
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestAddItemToContextArgumentNullException_httpContext()
        {
            ContextHelper.AddItemToContext(null, ContextItems.CurrentUrl, UrlConst.Registration);
        }

        /// <summary>
        /// This test attempts to add an item with a null key to _context and
        /// ensures that an ArgumentNullException is thrown.
        /// </summary>
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestAddItemToContextArgumentNullException_keyToAdd()
        {
            ContextHelper.AddItemToContext(_context, null, UrlConst.Registration);
        }

        /// <summary>
        /// This test attempts to add a second CurrentUrl item to _context and
        /// ensures that an ArgumentException is thrown.
        /// </summary>
        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void TestAddItemToContextArgumentException()
        {
            ContextHelper.AddItemToContext(_context, ContextItems.CurrentUrl, UrlConst.Registration);
        }

        /// <summary>
        /// This test gets the value of the CurrentUrl NhsUrl from _context and
        /// ensures that the value is communicated intact. It also tests
        /// returning a null value for a key that does not exist in _context.
        /// </summary>
        [Test]
        public void TestGetItemFromContext()
        {
            NhsUrl currentUrl = ContextHelper.GetItemFromContext(_context, ContextItems.CurrentUrl) as NhsUrl;

            Assert.IsNotNull(currentUrl);
            Assert.AreEqual( currentUrl.ToString(), _context.Items[ContextItems.CurrentUrl].ToString());

            object shouldBeNull = ContextHelper.GetItemFromContext(_context, "ThisIsNotAValidKeyOrShouldntBeAnyway");

            Assert.IsNull(shouldBeNull);
        }

        /// <summary>
        /// This test attempts to get the value of the CurrentUrl item from a
        /// null HTTP context and ensures that an ArgumentNullException is
        /// thrown.
        /// </summary>
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestGetItemFromContextArgumentNullException_httpContext()
        {
            ContextHelper.GetItemFromContext(null, ContextItems.CurrentUrl);
        }

        /// <summary>
        /// This test attempts to get the value of an item from _context with
        /// a null key and ensures that an ArgumentNullException is thrown.
        /// </summary>
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestGetItemFromContextArgumentNullException_key()
        {
            ContextHelper.GetItemFromContext(_context, null);
        }

        /// <summary>
        /// This test attempts to get the value of the CopyOfCurrentUrl item
        /// from the HTTP context.
        /// </summary>
        /// <remarks>
        /// It first attempts to retrieve this value from the current HTTP
        /// context, which already has a copy of the CurrentUrl item in
        /// CopyOfCurrentUrl. This tests the 1st branch of the
        /// GetNewFriendlyUrl() function which the GetFriendlyUrl provide a
        /// public interface to. Once this value is retrieved, the test ensures
        /// that it is not null.
        /// The test then calls the overloaded GetFriendlyUrl() which accepts an HTTP
        /// context as an argument. The CopyOfCurrentUrl item is first removed
        /// from _context so that the second branch of the GetNewFriendlyUrl()
        /// funtion will be used, which handles an HTTP context that contains a
        /// CurrentUrl item but not a CopyOfCurrentUrl item. The test ensures
        /// that the NhsUrl retrieved is not null.
        /// The test then ensures that the CopyOfCurrentUrl has been replaced in
        /// the HTTP context, a side-effect of the GetNewFriendlyUrl() function.
        /// </remarks>
        [Test]
        public void TestGetFriendlyUrl()
        {
            NhsUrl friendlyUrl = ContextHelper.GetFriendlyUrl();
            Assert.IsNotNull(friendlyUrl);

            _context.Items.Remove(ContextItems.CopyOfCurrentUrl);
            friendlyUrl = ContextHelper.GetFriendlyUrl(_context);
            Assert.IsNotNull(friendlyUrl);

            Assert.IsTrue(_context.Items.Contains(ContextItems.CopyOfCurrentUrl));
            Assert.IsNotNull(_context.Items[ContextItems.CopyOfCurrentUrl] as NhsUrl);
        }

        /// <summary>
        /// This test attempts to get a friendly URL from a null HTTP context
        /// and ensures that an ArgumentNullException is thrown.
        /// </summary>
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestGetFriendlyUrlArgumentNullException()
        {
            ContextHelper.GetFriendlyUrl(null);
        }

        /// <summary>
        /// This test attempts to get a friendly URL from an HTTP context that
        /// contains neither a CopyOfCurrentUrl or a CurrentUrl item and
        /// ensures that an InvalidOperation exception is thrown.
        /// </summary>
        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TestGetFriendlyUrlInvalidOperationException01()
        {
            try
            {
                _context.Items.Remove(ContextItems.CurrentUrl);
                _context.Items.Remove(ContextItems.CopyOfCurrentUrl);

                ContextHelper.GetFriendlyUrl(_context);
            }
            catch ( InvalidOperationException exception )
            {
                ContextHelper.AddItemToContext(_context, ContextItems.CurrentUrl, new NhsUrl("home"));
                ContextHelper.AddItemToContext(_context, ContextItems.CopyOfCurrentUrl, _context.Items[ContextItems.CurrentUrl]);

                throw exception;
            }
        }

        /// <summary>
        /// This test attempts to get a friendly URL from the current HTTP
        /// context when that context is null and ensures that an
        /// InvalidOperationException is thrown.
        /// </summary>
        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TestGetFriendlyUrlInvalidOperationException02()
        {
            try
            {
                HttpContext.Current = null;

                ContextHelper.GetFriendlyUrl();
            }
            catch (InvalidOperationException exception)
            {
                HttpContext.Current = _context;

                throw exception;
            }
        }
    }
}