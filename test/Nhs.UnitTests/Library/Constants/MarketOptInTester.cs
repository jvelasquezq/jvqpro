using Nhs.Library.Common;
using Nhs.Library.Constants;
using NUnit.Framework;

namespace Nhs.UnitTests.Library.Common
{
    [TestFixture]
    [Category("Library.Common.MarketOptInTester")]
    public class MarketOptInTester
    {
        [Test()]
        public void VerifyShortForm()
        {
            MarketOptIn.TesterXml = "<?xml version='1.0' encoding='utf-8' ?>" +
                  "<MarketOptIn><Market ID='74'><Test Name='ShortForm-6.4.0.0' /></Market></MarketOptIn>";
            Assert.IsTrue(MarketOptIn.Contains(74,MarketOptInTests.ShortForm_6400),
                "The market with ID 74 should be in short form.");
        }
    }
}
